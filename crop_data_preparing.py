import csv
import logging
import multiprocessing
import os
import shutil
import sqlite3
import threading
import time
import uuid

import yaml
from pydriller import RepositoryMining

config_dict = yaml.safe_load(open('config.yml'))
query_dict = yaml.safe_load(open('query.yml'))
list_of_crop_projects = []
add_crop_review_data = "INSERT INTO crop_reviews(id_generated,id_review,status,before_commit_hash,after_commit_hash) VALUES (?,?,?,?,?)"
os_name = os.name
paths = "file_path"
lock = threading.Lock()
logging.basicConfig(filename='crop_data_preparing.log', filemode='w', format='%(process)d-%(levelname)s-%(message)s')


def main():
    global paths
    start_time = time.time()
    if os_name is "nt":
        paths = "windows_file_path"
    reset_database()
    create_tables()
    number_of_processor = multiprocessing.cpu_count()
    global list_of_crop_projects
    list_threads = []
    counter = 0
    for csv_file in os.listdir(config_dict[paths]['crop_csv_commit']):
        list_threads.append(threading.Thread(target=get_commit_data, args=(csv_file,)))
    for single_thread in list_threads:
        single_thread.name = counter
        single_thread.start()
        counter += 1
    for single_thread in list_threads:
        single_thread.join()
    end_time = time.time()
    logging.info("Time Lapsed " + str(end_time - start_time))


def get_commit_data(csv_file_name):
    # FOR LOGGING
    start_time = 0
    csv_opening_time = 0
    query_exec_time = 0
    repo_mining_time = 0
    if threading.current_thread().name == '0':
        start_time = time.time()
    global paths
    counter_row = 0
    name_project = csv_file_name.split("/")[-1]
    with open(config_dict[paths]['crop_csv_commit'] + name_project, mode='rt') as base_data:
        reader = csv.reader(base_data, delimiter=',', quoting=csv.QUOTE_MINIMAL, lineterminator="\n")
        _ = next(reader)
        data = list(reader)
        total_line = len(data)
        logging.info(name_project.replace(".csv", "") + " " + str(total_line) + " lines")
        if threading.current_thread().name == '0':
            csv_opening_time = time.time()
        for line in data:
            id_revision = line[0]
            status = line[2]
            before_commit_hash = line[9]
            after_commit_hash = line[10]
            execute_query_safe(add_crop_review_data,
                               (str(uuid.uuid4()), id_revision, status, before_commit_hash, after_commit_hash))
            if threading.current_thread().name == '0':
                query_exec_time = time.time()
            for commit in RepositoryMining(
                    config_dict[paths]["crop_repos_path"] + name_project.replace(".csv", "") + "/",
                    single=after_commit_hash).traverse_commits():
                os.makedirs(config_dict["file_path"]["crop_database"] + name_project.replace(".csv",
                                                                                             "") + "/" + after_commit_hash + "/",
                            exist_ok=True)
                for mod in commit.modifications:
                    file_name = mod.filename
                    with open(
                            config_dict[paths]["crop_database"] + name_project.replace(".csv",
                                                                                       "") + "/" + after_commit_hash + "/" + file_name,
                            mode='w', encoding="utf-8") as source_code_after_file:
                        if mod.source_code is None:
                            source_code_after_file.write("File now deleted!")
                        else:
                            source_code_after_file.write(mod.source_code)
            if threading.current_thread().name == '0':
                repo_mining_time = time.time()
            counter_row += 1
            if threading.current_thread().name == '0':
                logging.info("CSV opening take " + str(round(csv_opening_time - start_time, 2)) + " ms")
                logging.info("Query exec take " + str(round(query_exec_time - start_time, 2)) + " ms")
                logging.info("repo_mining_time take " + str(round(repo_mining_time - start_time, 2)) + " ms")


def create_tables():
    create_list_commits = "CREATE TABLE IF NOT EXISTS crop_reviews(id_generated VARCHAR(64),id_review VARCHAR(64) PRIMARY KEY, status TEXT,before_commit_hash VARCHAR(64), after_commit_hash VARCHAR(64))"
    execute_query_safe(create_list_commits)


def reset_database():
    my_dirs = ["fix_commit_database/crop_database"]
    for directory in my_dirs:
        for the_file in os.listdir(directory):
            file_path = os.path.join(directory, the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print(e)
    delete_all_from_mysql_db = "DELETE FROM crop_reviews"
    execute_query_safe(delete_all_from_mysql_db)


def execute_query_safe(query, args=""):
    global lock
    lock.acquire(True)
    db = sqlite3.connect(config_dict["file_path"]["database_path"])
    cursor = db.cursor()
    if not args == "":
        cursor.execute(query, args)
    else:
        cursor.execute(query)
    db.commit()
    lock.release()


if __name__ == "__main__":
    main()
