package org.orman.datasource;

/**
 * Placeholder interface for storing various database 
 * settings.
 * 
 * @author ahmet alp balkan <ahmetalpbalkan@gmail.com>
 *
 */
public interface DatabaseSettings {

}
