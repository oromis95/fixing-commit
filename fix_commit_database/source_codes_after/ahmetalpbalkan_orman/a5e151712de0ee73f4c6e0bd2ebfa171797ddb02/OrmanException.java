package org.orman.exception;

public class OrmanException extends RuntimeException {

	public OrmanException(){}
	
	public OrmanException(String message) {
		super(message);
	}

}
