package com.github.setial.intellijjavadocs.exception;

/**
 * The type Template not found exception.
 *
 * @author Sergey Timofiychuk
 */
public class TemplateNotFoundException extends RuntimeException {
}
