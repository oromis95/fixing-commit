package com.github.ideajavadocs.template.impl;

import com.github.ideajavadocs.template.DocTemplateProcessor;
import com.intellij.openapi.components.ApplicationComponent;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

public class DocTemplateProcessorImpl implements DocTemplateProcessor, ApplicationComponent {

    public static final String COMPONENT_NAME = "DocTemplateProcessor";

    // TODO move the section to the configuration menu
    public static final Map<String, String> REPLACE_TOKENS = new HashMap<String, String>();
    static {
        REPLACE_TOKENS.put("get", "Gets the");
        REPLACE_TOKENS.put("set", "Sets the");
        REPLACE_TOKENS.put("create", "Creates the");
        REPLACE_TOKENS.put("build", "Builds the");
        REPLACE_TOKENS.put("init", "Init the");
    }

    @Override
    public void initComponent() {
    }

    @Override
    public void disposeComponent() {
    }

    @NotNull
    @Override
    public String getComponentName() {
        return COMPONENT_NAME;

    }

    /**
     * Merge.
     *
     * @param params the Params
     * @return the String
     */
    @NotNull
    @Override
    public String merge(@NotNull String template, @NotNull Map<String, String> params) {
        for (Entry<String, String> entry : params.entrySet()) {
            template = template.replaceAll(getReplaceParameter(entry.getKey()), entry.getValue());
        }
        return template;

    }

    @NotNull
    @Override
    public String buildDescription(@NotNull String description) {
        if (StringUtils.isBlank(description)) {
            return StringUtils.EMPTY;
        }
        String[] parts = StringUtils.splitByCharacterTypeCamelCase(description);
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < parts.length; i++) {
            if (i == 0) {
                if (REPLACE_TOKENS.containsKey(parts[i])) {
                    result.append(REPLACE_TOKENS.get(parts[i]));
                } else {
                    result.append(StringUtils.capitalize(parts[i]));
                }
            } else {
                result.append(StringUtils.lowerCase(parts[i]));
            }
            if (i < parts.length - 1) {
                result.append(" ");
            }
        }
        return result.toString();
    }

    @NotNull
    @Override
    public String buildRawDescription(@NotNull String description) {
        if (StringUtils.isBlank(description)) {
            return StringUtils.EMPTY;
        }
        String[] parts = StringUtils.splitByCharacterTypeCamelCase(description);
        StringBuilder result = new StringBuilder();
        int firstElement = 0;
        if(REPLACE_TOKENS.containsKey(parts[0])) {
            firstElement ++;
        }
        for (int i = firstElement; i < parts.length; i++) {
            if (i == firstElement) {
                result.append(StringUtils.capitalize(parts[i]));
            } else {
                result.append(StringUtils.lowerCase(parts[i]));
            }
            if (i < parts.length - 1) {
                result.append(" ");
            }
        }
        return result.toString();
    }

    @NotNull
    private String getReplaceParameter(@NotNull String paramName) {
        return Pattern.quote("${" + paramName + "}");
    }

}
