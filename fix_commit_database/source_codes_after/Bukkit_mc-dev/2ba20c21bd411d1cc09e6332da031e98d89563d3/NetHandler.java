package net.minecraft.server;

public class NetHandler {

    public NetHandler() {}

    public void a(Packet51MapChunk packet51mapchunk) {}

    public void a(Packet packet) {}

    public void a(String s, Object[] aobject) {}

    public void a(Packet255KickDisconnect packet255kickdisconnect) {
        this.a((Packet) packet255kickdisconnect);
    }

    public void a(Packet1Login packet1login) {
        this.a((Packet) packet1login);
    }

    public void a(Packet10Flying packet10flying) {
        this.a((Packet) packet10flying);
    }

    public void a(Packet52MultiBlockChange packet52multiblockchange) {
        this.a((Packet) packet52multiblockchange);
    }

    public void a(Packet14BlockDig packet14blockdig) {
        this.a((Packet) packet14blockdig);
    }

    public void a(Packet53BlockChange packet53blockchange) {
        this.a((Packet) packet53blockchange);
    }

    public void a(Packet50PreChunk packet50prechunk) {
        this.a((Packet) packet50prechunk);
    }

    public void a(Packet20NamedEntitySpawn packet20namedentityspawn) {
        this.a((Packet) packet20namedentityspawn);
    }

    public void a(Packet30Entity packet30entity) {
        this.a((Packet) packet30entity);
    }

    public void a(Packet34EntityTeleport packet34entityteleport) {
        this.a((Packet) packet34entityteleport);
    }

    public void a(Packet15Place packet15place) {
        this.a((Packet) packet15place);
    }

    public void a(Packet16BlockItemSwitch packet16blockitemswitch) {
        this.a((Packet) packet16blockitemswitch);
    }

    public void a(Packet29DestroyEntity packet29destroyentity) {
        this.a((Packet) packet29destroyentity);
    }

    public void a(Packet21PickupSpawn packet21pickupspawn) {
        this.a((Packet) packet21pickupspawn);
    }

    public void a(Packet22Collect packet22collect) {
        this.a((Packet) packet22collect);
    }

    public void a(Packet3Chat packet3chat) {
        this.a((Packet) packet3chat);
    }

    public void a(Packet23VehicleSpawn packet23vehiclespawn) {
        this.a((Packet) packet23vehiclespawn);
    }

    public void a(Packet18ArmAnimation packet18armanimation) {
        this.a((Packet) packet18armanimation);
    }

    public void a(Packet2Handshake packet2handshake) {
        this.a((Packet) packet2handshake);
    }

    public void a(Packet24MobSpawn packet24mobspawn) {
        this.a((Packet) packet24mobspawn);
    }

    public void a(Packet4UpdateTime packet4updatetime) {
        this.a((Packet) packet4updatetime);
    }

    public void a(Packet6SpawnPosition packet6spawnposition) {
        this.a((Packet) packet6spawnposition);
    }

    public void a(Packet28 packet28) {
        this.a((Packet) packet28);
    }

    public void a(Packet39 packet39) {
        this.a((Packet) packet39);
    }

    public void a(Packet7 packet7) {
        this.a((Packet) packet7);
    }

    public void a(Packet38 packet38) {
        this.a((Packet) packet38);
    }

    public void a(Packet8 packet8) {
        this.a((Packet) packet8);
    }

    public void a(Packet9 packet9) {
        this.a((Packet) packet9);
    }

    public void a(Packet60 packet60) {
        this.a((Packet) packet60);
    }

    public void a(Packet100 packet100) {
        this.a((Packet) packet100);
    }

    public void a(Packet101 packet101) {
        this.a((Packet) packet101);
    }

    public void a(Packet102 packet102) {
        this.a((Packet) packet102);
    }

    public void a(Packet103 packet103) {
        this.a((Packet) packet103);
    }

    public void a(Packet104 packet104) {
        this.a((Packet) packet104);
    }

    public void a(Packet130 packet130) {
        this.a((Packet) packet130);
    }

    public void a(Packet105 packet105) {
        this.a((Packet) packet105);
    }

    public void a(Packet5PlayerInventory packet5playerinventory) {
        this.a((Packet) packet5playerinventory);
    }

    public void a(Packet106 packet106) {
        this.a((Packet) packet106);
    }
}
