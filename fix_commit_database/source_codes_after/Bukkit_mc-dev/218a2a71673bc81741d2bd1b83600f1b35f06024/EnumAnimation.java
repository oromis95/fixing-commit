package net.minecraft.server;

public enum EnumAnimation {

    a("none", 0), b("eat", 1), c("block", 2), d("bow", 3);

    private static final EnumAnimation[] e = new EnumAnimation[] { a, b, c, d};

    private EnumAnimation(String s, int i) {}
}
