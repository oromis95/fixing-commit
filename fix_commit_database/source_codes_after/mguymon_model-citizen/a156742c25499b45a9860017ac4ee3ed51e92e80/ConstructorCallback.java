package com.tobedevoured.modelcitizen.field;

/**
 * 
 * @author Michael Guymon
 *
 */
public abstract class ConstructorCallback {

	public abstract Object createInstance();
}
