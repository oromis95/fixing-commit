package com.bmesta.powermode;

import javax.swing.*;

/**
 * Created by nyxos on 11.03.16.
 */
public interface ValueSettable {
    void setValue(JSlider slider);
}
