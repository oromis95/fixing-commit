package com.jenkov.iap.read;

import com.jenkov.iap.TestPojo;
import com.jenkov.iap.TestPojoArray;
import com.jenkov.iap.write.IonObjectWriter;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by jjenkov on 05-11-2015.
 */
public class IapTypedObjectReaderTest {


    @Test
    public void test() {
        IonObjectWriter writer  = new IonObjectWriter(TestPojo.class);
        IonObjectReader reader  = new IonObjectReader(TestPojo.class);

        byte[] source = new byte[10 * 1024];

        TestPojo sourcePojo = new TestPojo();
        sourcePojo.field1 = 123;

        TestPojo destPojo = null;


        int length = writer.writeObject(sourcePojo, 2, source, 0);   //write object first

        //todo fix error in TypedObjectReader related to reading compact keys

        destPojo = (TestPojo) reader.read(source, 0);

        assertEquals(123, destPojo.field1);


    }


    @Test
    public void testTableField() {
        IonObjectWriter writer  = new IonObjectWriter(TestPojoArray.class);
        IonObjectReader reader  = new IonObjectReader(TestPojoArray.class);

        byte[] source = new byte[10 * 1024];

        TestPojoArray sourcePojoArray = new TestPojoArray();

        sourcePojoArray.testObjects    = new TestPojoArray.TestObject[3];
        sourcePojoArray.testObjects[0] = new TestPojoArray.TestObject();
        sourcePojoArray.testObjects[1] = new TestPojoArray.TestObject();
        sourcePojoArray.testObjects[2] = new TestPojoArray.TestObject();

        TestPojoArray destPojo         = new TestPojoArray();

        int length = writer.writeObject(sourcePojoArray, 2, source, 0);

        destPojo = (TestPojoArray) reader.read(source, 0);


    }

}
