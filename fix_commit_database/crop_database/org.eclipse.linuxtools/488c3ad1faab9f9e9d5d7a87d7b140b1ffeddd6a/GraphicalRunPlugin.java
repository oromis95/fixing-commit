/*******************************************************************************
 * Copyright (c) 2006 IBM Corporation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - Jeff Briggs, Henry Hughes, Ryan Morse, Anithra P J
 *******************************************************************************/

package org.eclipse.linuxtools.internal.systemtap.ui.graphicalrun;


/**
 * The main plugin class to be used in the desktop.
 */
public class GraphicalRunPlugin {
	public static final String PLUGIN_ID = "org.eclipse.linuxtools.systemtap.ui.graphicalrun"; //$NON-NLS-1$
}
