/*******************************************************************************
 * Copyright (c) 2012 Red Hat, Inc.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Red Hat initial API and implementation
 ******************************************************************************/
package org.eclipse.linuxtools.profiling.coverage;

public class CoverageProfileConstants {
	// The plug-in ID
	public static final String PLUGIN_ID = "org.eclipse.linuxtools.profiling.coverage"; //$NON-NLS-1$

	// Type of profiling this plug-in supports
	public static final String PROFILING_TYPE = "coverage"; //$NON-NLS-1$

	// Plug-in id of snapshot launch configuration type
	public static final String PLUGIN_CONFIG_ID = "org.eclipse.linuxtools.profiling.coverage.launchConfigurationType"; //$NON-NLS-1$
	
	// Plug-in name
	public static final String PLUGIN_NAME = "Code Coverage"; //$NON-NLS-1$

}
