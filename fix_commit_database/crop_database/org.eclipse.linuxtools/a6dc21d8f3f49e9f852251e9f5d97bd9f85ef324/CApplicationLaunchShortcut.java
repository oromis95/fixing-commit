/*******************************************************************************
 * Copyright (c) 2005, 2011 QNX Software Systems and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     QNX Software Systems - Initial API and implementation
 *     Ken Ryall (Nokia) - bug 178731
 *     Ken Ryall (Nokia) - bug 246201
 *******************************************************************************/
package org.eclipse.linuxtools.internal.profiling.launch;


/**
 * @since 2.0
 */
public class CApplicationLaunchShortcut extends org.eclipse.cdt.debug.internal.ui.launch.CApplicationLaunchShortcut {
}
