package org.eclipse.linuxtools.profiling.time;

/**
 * The activator class controls the plug-in life cycle
 */
public class TimeProviderPlugin {

	/**
	 * Plug-in name
	 */
	public static final String PLUGIN_NAME = "Time"; //$NON-NLS-1$

	/**
	 * Plug-in id
	 */
	public static final String PLUGIN_ID = "org.eclipse.linuxtools.profiling.time"; //$NON-NLS-1$

	/**
	 * Plug-in id of snapshot launch configuration type
	 */
	public static final String PLUGIN_CONFIG_ID = "org.eclipse.linuxtools.profiling.time.launchConfigurationType"; //$NON-NLS-1$

	/**
	 * Type of profiling this plug-in supports
	 */
	public static final String PROFILING_TYPE = "time"; //$NON-NLS-1$
}
