/*******************************************************************************
 * Copyright (c) 2012 Red Hat.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Red Hat - Sami Wagiaalla
 *******************************************************************************/

package org.eclipse.linuxtools.internal.systemtap.ui.ide.launcher;

import org.eclipse.osgi.util.NLS;


public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.linuxtools.internal.systemtap.ui.ide.launcher.messages"; //$NON-NLS-1$
	public static String SystemTapScriptLaunchConfigurationTab_0;
	public static String SystemTapScriptLaunchConfigurationTab_1;
	public static String SystemTapScriptLaunchConfigurationTab_2;
	public static String SystemTapScriptLaunchConfigurationTab_3;
	public static String SystemTapScriptLaunchConfigurationTab_4;
	public static String SystemTapScriptLaunchConfigurationTab_5;
	public static String SystemTapScriptLaunchConfigurationTab_6;
	public static String SystemTapScriptLaunchConfigurationTab_7;
	public static String SystemTapScriptLaunchConfigurationTab_8;
	public static String SystemTapScriptLaunchConfigurationTab_9;

	public static String SystemTapScriptLaunchConfigurationTab_preferences;
	public static String SystemTapScriptLaunchConfigurationTab_runWithChart;

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
