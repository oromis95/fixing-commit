package org.eclipse.linuxtools.profiling.snapshot;


/**
 * Container class for various constants
 */
public class SnapshotConstants {

	/**
	 * Plug-in name
	 */
	public static final String PLUGIN_NAME = "Snapshot"; //$NON-NLS-1$

	/**
	 * Plug-in id
	 */
	public static final String PLUGIN_ID = "org.eclipse.linuxtools.profiling.snapshot"; //$NON-NLS-1$

	/**
	 * Plug-in id of snapshot launch configuration type
	 */
	public static final String PLUGIN_CONFIG_ID = "org.eclipse.linuxtools.profiling.snapshot.launchConfigurationType"; //$NON-NLS-1$

	/**
	 * Type of profiling this plug-in supports
	 */
	public static final String PROFILING_TYPE = "snapshot"; //$NON-NLS-1$

}
