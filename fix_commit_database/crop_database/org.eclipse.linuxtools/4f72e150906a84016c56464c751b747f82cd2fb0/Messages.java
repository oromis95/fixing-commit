package org.eclipse.linuxtools.tmf.ui.project.wizards.importtrace;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
    private static final String BUNDLE_NAME = "org.eclipse.linuxtools.tmf.ui.project.wizards.importtrace.messages"; //$NON-NLS-1$
    public static String ImportTraceWizardPageOptions_0;
    public static String ImportTraceWizardPageOptions_1;
    public static String ImportTraceWizardPageOptions_2;
    public static String ImportTraceWizardPageScan_0;
    public static String ImportTraceWizardPageScan_1;
    public static String ImportTraceWizardPageScan_2;
    public static String ImportTraceWizardPageScan_3;
    public static String ImportTraceWizardPageScan_4;
    public static String ImportTraceWizardPageSelectDirectories_0;
    public static String ImportTraceWizardPageSelectDirectories_1;
    public static String ImportTraceWizardPageSelectDirectories_2;
    public static String ImportTraceWizardPageSelectDirectories_3;
    public static String ImportTraceWizardPageSelectDirectories_4;
    public static String ImportTraceWizardPageSelectTraceType_0;
    public static String ImportTraceWizardPageSelectTraceType_1;
    public static String ImportTraceWizardPageSelectTraceType_2;
    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
    }
}
