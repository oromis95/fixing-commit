<?xml version="1.0" encoding="UTF-8"?>
<!--
   Copyright (C) 2011, Red Hat, Inc. and others

   All rights reserved. This program and the accompanying materials
   are made available under the terms of the Eclipse Public License v1.0
   which accompanies this distribution, and is available at
   http://www.eclipse.org/legal/epl-v10.html

   Contributors:
       Red Hat Incorporated - Initial implementation.
-->

<project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <prerequisites>
    <maven>3.0</maven>
  </prerequisites>
  <parent>
    <groupId>org.eclipse</groupId>
    <artifactId>eclipse-parent</artifactId>
    <version>3</version>
  </parent>
  

  <groupId>org.eclipse.linuxtools</groupId>
  <artifactId>linuxtools-parent</artifactId>
  <version>1.1.1</version>
  <packaging>pom</packaging>

  <name>Eclipse Linux Tools Parent</name>

  <licenses>
    <license>
      <name>Eclipse Public License v1.0</name>
      <comments>
       All rights reserved.

       This program and the accompanying materials are made
       available under the terms of the Eclipse Public License v1.0
       which accompanies this distribution, and is available at
       http://www.eclipse.org/legal/epl-v10.htm
      </comments>
    </license>
  </licenses>

  <properties>
    <tycho-version>0.15.0</tycho-version>
    <tycho-extras-version>0.15.0</tycho-extras-version>
    <platform-version-name>juno</platform-version-name>
    <eclipse-site>http://download.eclipse.org/releases/${platform-version-name}</eclipse-site>
    <swtbot-site>http://download.eclipse.org/technology/swtbot/helios/dev-build/update-site</swtbot-site>
    <orbit-site>http://download.eclipse.org/tools/orbit/downloads/drops/R20120526062928/repository</orbit-site>
  </properties>
  <pluginRepositories>
    <pluginRepository>
      <id>maven.eclipse.org</id>
      <url>http://maven.eclipse.org/nexus/content/groups/public/</url>
    </pluginRepository>
  </pluginRepositories>
  <repositories>
    <repository>
      <id>swtbot</id>
      <layout>p2</layout>
      <url>${swtbot-site}</url>
    </repository>
    <repository>
      <id>orbit</id>
      <layout>p2</layout>
      <url>${orbit-site}</url>
    </repository>
    <repository>
      <id>maven.eclipse.org</id>
      <url>http://maven.eclipse.org/nexus/content/groups/public/</url>
    </repository>
  </repositories>
  
  <profiles>
    <profile>
      <id>platform-indigo</id>
      <activation>
        <property>
          <name>platform-version-name</name>
          <value>indigo</value>
        </property>
      </activation>
      <properties>
        <eclipse-site>http://download.eclipse.org/releases/indigo</eclipse-site>
        <platform-version>[3.7,3.8)</platform-version>
        <sdk-version>3.7</sdk-version>
        <cdt-site>http://download.eclipse.org/tools/cdt/updates/indigo/</cdt-site>
        <cdt-test-site>http://download.eclipse.org/tools/cdt/updates/indigo/</cdt-test-site>
      </properties>
      <repositories>
        <repository>
          <id>indigo</id>
          <layout>p2</layout>
          <url>${eclipse-site}</url>
        </repository>
        <repository>
          <id>cdt</id>
          <layout>p2</layout>
          <url>${cdt-site}</url>
        </repository>
      </repositories>
    </profile>
    <profile>
      <id>platform-juno</id>
      <activation>
        <activeByDefault>true</activeByDefault>
        <property>
          <name>platform-version-name</name>
          <value>juno</value>
        </property>
      </activation>
      <properties>
        <eclipse-site>http://download.eclipse.org/releases/juno</eclipse-site>
        <platform-version>[4.2,)</platform-version>
        <sdk-version>4.2</sdk-version>
        <cdt-site>http://download.eclipse.org/tools/cdt/builds/juno/milestones</cdt-site>
        <cdt-test-site>http://download.eclipse.org/tools/cdt/updates/indigo/</cdt-test-site>
        <tcf-site>http://download.eclipse.org/tcf/builds/juno/milestones/</tcf-site>
      </properties>
      <repositories>
        <repository>
          <id>juno</id>
          <layout>p2</layout>
          <url>${eclipse-site}</url>
        </repository>
        <repository>
          <id>cdt</id>
          <layout>p2</layout>
          <url>${cdt-site}</url>
        </repository>
        <repository>
          <id>cdttest</id>
          <layout>p2</layout>
          <url>${cdt-test-site}</url>
        </repository>
      </repositories>
    </profile>
    <profile>
      <id>build-server</id>
      <build>
        <plugins>
          <plugin>
	  <groupId>org.eclipse.tycho</groupId>
	  <artifactId>tycho-surefire-plugin</artifactId>
	  <version>${tycho-version}</version>
	  <configuration>
	    <useUIHarness>true</useUIHarness>
	    <useUIThread>true</useUIThread>
              <dependencies>
                <dependency>
                  <type>p2-installable-unit</type>
                  <artifactId>org.eclipse.sdk.feature.group</artifactId>
                  <version>${sdk-version}</version>
                </dependency>
              </dependencies>
              <product>org.eclipse.sdk.ide</product>
	      <argLine>-Xms512m -Xmx1024m -XX:PermSize=256m -XX:MaxPermSize=256m -Declipse.valgrind.tests.runValgrind=no -Dorg.eclipse.linuxtools.oprofile.launch.tests.runOprofile=no -Dorg.eclipse.linuxtools.oprofile.core.tests.runOprofile=no -Dorg.eclipse.swt.browser.UseWebKitGTK=true</argLine>
	    </configuration>
	  </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>

  <modules>
    <module>changelog</module>
    <module>gcov</module>
    <module>gprof</module>
    <module>libhover</module>
    <module>lttng</module>
    <module>man</module>
    <module>oprofile</module>
    <module>profiling</module>
    <module>rpm</module>
    <module>rpmstubby</module>
    <module>systemtap</module>
    <module>valgrind</module>
    <module>perf</module>
    <module>releng</module>
  </modules>

  <build>
    <plugins>
      <plugin>
        <groupId>org.eclipse.tycho</groupId>
        <artifactId>tycho-maven-plugin</artifactId>
        <version>${tycho-version}</version>
        <extensions>true</extensions>
      </plugin>
      <plugin>
        <groupId>org.eclipse.tycho</groupId>
        <artifactId>target-platform-configuration</artifactId>
        <version>${tycho-version}</version>
        <configuration>
          <resolver>p2</resolver>
        </configuration>
      </plugin>
      <plugin>
          <groupId>org.eclipse.tycho.extras</groupId>
          <artifactId>tycho-source-feature-plugin</artifactId>
          <version>${tycho-extras-version}</version>
          <executions>
            <execution>
              <id>source-feature</id>
              <phase>package</phase>
              <goals>
                <goal>source-feature</goal>
              </goals>
            </execution>
          </executions>
        </plugin>
	<plugin>
          <groupId>org.eclipse.tycho</groupId>
          <artifactId>tycho-source-plugin</artifactId>
          <version>${tycho-version}</version>
          <executions>
            <execution>
              <id>plugin-source</id>
              <goals>
                <goal>plugin-source</goal>
              </goals>
            </execution>
          </executions>
        </plugin>
      <plugin>
        <artifactId>maven-assembly-plugin</artifactId>
        <configuration>
          <descriptors>
            <descriptor>src.xml</descriptor>
          </descriptors>
        </configuration>
      </plugin>
    </plugins>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.eclipse.tycho</groupId>
          <artifactId>tycho-compiler-plugin</artifactId>
          <version>${tycho-version}</version>
          <configuration>
            <encoding>UTF-8</encoding>
          </configuration>
	 </plugin>
	<plugin>
	  <groupId>org.eclipse.tycho</groupId>
	  <artifactId>tycho-surefire-plugin</artifactId>
	  <version>${tycho-version}</version>
	  <configuration>
	    <useUIHarness>true</useUIHarness>
	    <useUIThread>true</useUIThread>
            <dependencies>
               <dependency>
                 <type>p2-installable-unit</type>
                 <artifactId>org.eclipse.sdk.feature.group</artifactId>
                 <version>${sdk-version}</version>
               </dependency>
            </dependencies>
            <product>org.eclipse.sdk.ide</product>
	    <argLine>-Xms512m -Xmx1024m -XX:PermSize=256m -XX:MaxPermSize=256m -Dorg.eclipse.swt.browser.UseWebKitGTK=true</argLine>
	  </configuration>
	</plugin>
        <plugin>
          <groupId>org.eclipse.tycho</groupId>
          <artifactId>tycho-versions-plugin</artifactId>
          <version>${tycho-version}</version>
        </plugin>
	<plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-resources-plugin</artifactId>
          <version>2.5</version>
          <configuration>
            <encoding>ISO-8859-1</encoding>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-antrun-plugin</artifactId>
          <version>1.7</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-assembly-plugin</artifactId>
          <version>2.3</version>
          <configuration>
            <tarLongFileMode>gnu</tarLongFileMode>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-site-plugin</artifactId>
          <version>3.0</version>
        </plugin>
	<plugin>
	  <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-javadoc-plugin</artifactId>
	  <version>2.8.1</version>
          <configuration>
            <excludePackageNames>org.eclipse.linuxtools.internal.*:*.test*</excludePackageNames>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
</project>
