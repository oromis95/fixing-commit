package org.eclipse.linuxtools.systemtap.ui.ide.actions;

public class RunScriptLocallyAction extends RunScriptAction {

	public RunScriptLocallyAction() {
		this.setLocalScript(true);
	}
}
