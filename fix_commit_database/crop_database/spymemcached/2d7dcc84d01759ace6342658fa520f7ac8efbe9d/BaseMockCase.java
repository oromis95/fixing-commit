/**
 * @author Couchbase <info@couchbase.com>
 * @copyright 2011 Couchbase, Inc.
 * All rights reserved.
 */

package net.spy.memcached.compat;

import org.jmock.MockObjectTestCase;

/**
 * Base test case for mock object tests.
 */
public abstract class BaseMockCase extends MockObjectTestCase {
  // Nothing special needed here.
}
