/**
 * @author Couchbase <info@couchbase.com>
 * @copyright 2011 Couchbase, Inc.
 * All rights reserved.
 */

package net.spy.memcached.ops;

/**
 * Deletion operation.
 */
public interface DeleteOperation extends KeyedOperation {
  // nothing in particular.
}
