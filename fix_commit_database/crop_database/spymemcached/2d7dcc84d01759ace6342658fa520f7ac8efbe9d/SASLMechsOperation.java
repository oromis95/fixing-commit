/**
 * @author Couchbase <info@couchbase.com>
 * @copyright 2011 Couchbase, Inc.
 * All rights reserved.
 */

package net.spy.memcached.ops;

/**
 * Operation for listing supported SASL mechanisms.
 */
public interface SASLMechsOperation extends Operation {
  // Nothing.
}
