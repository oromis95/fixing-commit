

package net.spy.memcached.internal;

import java.util.concurrent.Future;



public interface FutureListener<F> extends GenericFutureListener<Future<F>> {
}
