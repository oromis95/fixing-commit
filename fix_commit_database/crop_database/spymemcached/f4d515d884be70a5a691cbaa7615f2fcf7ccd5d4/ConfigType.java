package net.spy.memcached.vbucket.config;

/**
 *
 * @author ingenthr
 */
public enum ConfigType {
	/**
	 * Cache bucket type.
	 */
	CACHE,
	/**
	 * Membase bucket type.
	 */
	MEMBASE;

}
