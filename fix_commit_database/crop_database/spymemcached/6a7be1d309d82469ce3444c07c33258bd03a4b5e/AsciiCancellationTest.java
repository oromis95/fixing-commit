/**
 * @author Couchbase <info@couchbase.com>
 * @copyright 2011 Couchbase, Inc.
 * All rights reserved.
 */

package net.spy.memcached;

/**
 * Test cancellation in ascii protocol.
 */
public class AsciiCancellationTest extends CancellationBaseCase {
  // uses defaults
}
