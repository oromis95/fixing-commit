/**
 * @author Couchbase <info@couchbase.com>
 * @copyright 2011 Couchbase, Inc.
 * All rights reserved.
 */

package net.spy.memcached.ops;

/**
 * Operation for proceeding in a SASL auth negotiation.
 */
public interface SASLStepOperation extends Operation {
  // nothing
}
