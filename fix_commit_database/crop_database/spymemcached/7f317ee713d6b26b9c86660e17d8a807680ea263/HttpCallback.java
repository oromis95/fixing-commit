package net.spy.memcached.protocol.couchdb;

public interface HttpCallback {
	public void complete(String response);
}
