package net.spy.memcached.protocol.couchdb;

public enum Stale {
	OK, UPDATE_AFTER
}
