/**
 * @author Couchbase <info@couchbase.com>
 * @copyright 2011 Couchbase, Inc.
 * All rights reserved.
 */

package net.spy.memcached.ops;

/**
 * Flush operation marker.
 */
public interface FlushOperation extends Operation {
  // nothing
}
