/**
 * @author Couchbase <info@couchbase.com>
 * @copyright 2011 Couchbase, Inc.
 * All rights reserved.
 */

package net.spy.memcached.ops;

/**
 * Operation for beginning a SASL auth cycle.
 */
public interface SASLAuthOperation extends Operation {
  // nothing
}
