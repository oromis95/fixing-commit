import json
import unittest
import uuid
from TestInput import TestInputSingleton
import logger
import time
from membase.api.rest_client import RestConnection
from membase.helper.bucket_helper import BucketOperationHelper
from membase.helper.cluster_helper import ClusterOperationHelper
from memcached.helper.data_helper import MemcachedClientHelper

class ViewTests(unittest.TestCase):
    #if we create a bucket and a view let's delete them in the end
    def setUp(self):
        self.log = logger.Logger.get_logger()
        bucket_ram_ratio = (2.0 / 3.0)
        replica = 1
        self.log = logger.Logger.get_logger()
        self.servers = TestInputSingleton.input.servers
        self.membase = TestInputSingleton.input.membase_settings
        BucketOperationHelper.delete_all_buckets_or_assert(self.servers, self)
        ClusterOperationHelper.cleanup_cluster(self.servers)
        ClusterOperationHelper.wait_for_ns_servers_or_assert(self.servers, self)
        serverInfo = self.servers[0]

        self.log.info('picking server : {0} as the master'.format(serverInfo))
        #if all nodes are on the same machine let's have the bucket_ram_ratio as bucket_ram_ratio * 1/len(servers)
        node_ram_ratio = BucketOperationHelper.base_bucket_ratio(self.servers)
        BucketOperationHelper.create_multiple_buckets(serverInfo, replica, node_ram_ratio * bucket_ram_ratio, howmany=1)
        self.created_views = {}


    #create a bucket if it doesn't exist

    def test_create_view(self):
        master = self.servers[0]
        rest = RestConnection(master)
        #find the first bucket
        buckets = rest.get_buckets()
        bucket = buckets[0].name
        view_name = "dev_" + str(uuid.uuid4())[:5]
        key = str(uuid.uuid4())
        doc_name = "test_create_view_{0}".format(key)
        mc = MemcachedClientHelper.direct_client(master, bucket)

        value = {"name": doc_name, "age": 1000}
        mc.set(key, 0, 0, json.dumps(value))
        mc.get(key)
        function = """function (doc) { if (doc.name == """ + doc_name + """) { emit(doc.name, doc.age);}}"""
        req = {"language": "javascript",
               "_id": "_design/{0}".format(view_name),
               "views": {view_name: {"map": function}}}
        function = json.dumps(req)
        rest.create_view(bucket, view_name, function)
        self.created_views[view_name] = bucket
        view_str = rest.get_view(bucket, view_name)
        results = {}
        self.log.info("get_view json_response : {0}".format(view_str))
        for i in range(1, 10):
            try:
                results = rest.view_results(bucket, view_name, 20)
            except Exception as ex:
                self.log.info(ex)
                self.log.error("result set is not ready yet!")
                time.sleep(1)
        self.assertTrue(results,"unable to get view results after multiple attempts")
        self.log.info("view_results json_response : {0}".format(results))
        keys = results["rows"]
        actual_key = keys[0]["key"]
        self.assertEquals(actual_key, doc_name)
        self.log.info("was able to get view results after trying {0} times".format(i))



    def tearDown(self):
        master = self.servers[0]
        rest = RestConnection(master)
        for view in self.created_views:
            print view
            bucket = self.created_views[view]
            rest.delete_view(bucket, view)
            self.log.info("deleted view {0} in bucket {1}".format(view, bucket))

