#!/usr/bin/env python

import os
import time
from builds.build_query import BuildQuery
import unittest
import logger
from membase.api.exception import ServerUnavailableException
from membase.api.rest_client import RestConnection
from remote.remote_util import RemoteMachineShellConnection

log = logger.Logger.get_logger()

# we will have InstallUninstall as one test case
# Install as another test case
class InstallTest(unittest.TestCase):

    ips = None
    machine_infos = None

    def tearDown(self):
        pass

    def setUp(self):
        self.ips = self.extract_server_ips()
        self.machine_infos = {}
        for ip in self.ips:
            remote_client = RemoteMachineShellConnection(ip = ip,
                                                         username = 'root',
                                                         pkey_location = self.get_pkey())
            info = remote_client.extract_remote_info()
            self.machine_infos[ip] = info
            remote_client.disconnect()
            log.info('IP : {0} Distribution : {1} Arch: {2} Version : {3}'
                .format(info.ip, info.distribution_type, info.architecture_type,info.distribution_version))

    def test_install(self):
        # find the right deliverable for this os?
        query = BuildQuery()
        version = os.environ["VERSION"]
        builds, changes = query.get_latest_builds()
        for ip in self.ips:
            info = self.machine_infos[ip]
            build = query.find_membase_build(builds,
                                     'membase-server-enterprise',
                                     info.deliverable_type,
                                     info.architecture_type,
                                     version)
            print 'for machine : ', info.architecture_type , info.distribution_type , 'relevant build : ' , build
            remote_client = RemoteMachineShellConnection(ip = ip,
                                                         username = 'root',
                                                         pkey_location = self.get_pkey())
            remote_client.membase_uninstall()
            downloaded = remote_client.download_binary(build)
            self.assertTrue(downloaded,'unable to download binaries :'.format(build.url))
            remote_client.membase_install(build)
            #TODO: we should poll the 8091 port until it is up and running
            log.info('wait 5 seconds for membase server to start')
            time.sleep(5)
            rest = RestConnection(ip = ip,username = 'Administrator',
                                  password = 'password')
            start_time = time.time()
            cluster_initialized = False
            while time.time() < (start_time + (2 * 60)):
                try:
                    rest.init_cluster(username = 'Administrator',password = 'password')
                    cluster_initialized = True
                    break
                except ServerUnavailableException:
                    log.error("error happened while initializing the cluster @ {0}".format(ip))
            self.assertTrue(cluster_initialized,"error happened while initializing the cluster @ {0}".format(ip))
            if not cluster_initialized:
                log.error("error happened while initializing the cluster @ {0}".format(ip))
                raise Exception("error happened while initializing the cluster @ {0}".format(ip))
            rest.init_cluster_memoryQuota(410)
#            rest.create_bucket(bucket = 'default',ramQuotaMB = 400,replicaNumber = 1,proxyPort = 11211)


    def extract_server_ips(self):
        servers_string = os.getenv("SERVERS")
        servers = servers_string.split(" ")
        return [server[:server.index(':')] for server in servers]
    def get_pkey(self):
        return os.getenv("KEYFILE")