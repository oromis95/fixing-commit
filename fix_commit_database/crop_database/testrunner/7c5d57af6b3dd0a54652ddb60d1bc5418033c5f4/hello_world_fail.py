import unittest
import os
import logger

log = logger.Logger.get_logger()

class HelloWorldFailTest(unittest.TestCase):

    def setUp(self):
        log.info('test setUp invoked')
        log.info('getting server ips')
        ips = self.extract_server_ips()
        for ip in ips:
            log.info("server ip : {0}".format(ip))

    def test_hello_fail_1(self):
        log.info('running test_hello_fail_1')
        i = 1
        self.assertEquals(i,2)

    def test_hello_fail_2(self):
        log.info('running test_hello_fail_2')
        i = 2
        self.assertEquals(i,3)

    def tearDown(self):
        log.info('tearDown invoked')

    def extract_server_ips(self):
        servers_string = os.getenv("SERVERS")
        servers = servers_string.split(" ")
        return [server[:server.index(':')] for server in servers]