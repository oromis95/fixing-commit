import os
import random
import unittest
import mc_bin_client
import socket
import zlib
import ctypes
import uuid
import logger
import time
import crc32
from membase.api.rest_client import RestConnection
from membase.helper.bucket_helper import BucketOperationHelper

log = logger.Logger.get_logger()

class SimpleSetGetTestBase(object):
    keys = None
    clients = None
    ips = None
    test = None
    bucket_port = None
    bucket_name = None


    def setUp_bucket(self, bucket_name, port, bucket_type, unittest):
        self.test = unittest
        self.ips = self.extract_server_ips()
        self.bucket_port = port

        self.bucket_name = bucket_name

        BucketOperationHelper.delete_all_buckets_or_assert(self.ips, self.test)
        #let's first install membase server

        #lets create 'default' bucket
        #        bucket_name = 'SimpleSetGetTest-{0}'.format(uuid.uuid4())
        for ip in self.ips:
            rest = RestConnection(ip=ip,
                                  username='Administrator',
                                  password='password')

            time.sleep(5)
            if bucket_name != 'default' and self.bucket_port == 11211:
                rest.create_bucket(bucket=bucket_name,
                                   bucketType=bucket_type,
                                   ramQuotaMB=400,
                                   replicaNumber=1,
                                   proxyPort=self.bucket_port,
                                   authType='sasl',
                                   saslPassword='password')
            else:
                rest.create_bucket(bucket=bucket_name,
                                   bucketType=bucket_type,
                                   ramQuotaMB=400,
                                   replicaNumber=1,
                                   proxyPort=self.bucket_port)
            msg = 'create_bucket succeeded but bucket "default" does not exist'
            self.test.assertTrue(BucketOperationHelper.wait_for_bucket_creation(bucket_name, rest), msg=msg)

        self.wait_till_memcached_is_ready()
        self.clients = self.create_mc_bin_clients_for_ips(self.ips)
        #populate key
        testuuid = uuid.uuid4()
        self.keys = ["key_%s_%d" % (testuuid, i) for i in range(500)]


    def wait_till_memcached_is_ready(self):
        for ip in self.ips:
            start_time = time.time()
            memcached_ready = False
            #bucket port
            while time.time() <= (start_time + (5 * 60)):
                log.info("bucket port : {0}".format(self.bucket_port))
                client = mc_bin_client.MemcachedClient(ip, self.bucket_port)
                key = '{0}'.format(uuid.uuid4())
                vbucketId = crc32.crc32_hash(key) & 1023 # or & 0x3FF
                client.vbucketId = vbucketId
                try:
                    client.set(key, 0, 0, key)
                    log.info("inserted key {0} to vBucket {1}".format(key, vbucketId))
                    memcached_ready = True
                    break
                except mc_bin_client.MemcachedError as error:
                    log.info('memcachedError : {0}'.format(error.status))
                    log.error("unable to push key : {0} to bucket : {1}".format(key, client.vbucketId))
                except :
                    log.error("unable to push key : {0} to bucket : {1}".format(key, client.vbucketId))
                client.close()
                time.sleep(1)
            if not memcached_ready:
                self.test.fail('memcached not ready for {0} after waiting for 5 minutes'.format(ip))


    # test case to set 500 keys and verify that those keys are stored
    def set_get_small_keys(self):
        for ip in self.ips:
            log.info(
                'pushing 500 keys (sizes between 100 bytes to 1KB to different vbuckets in membase @ {0}'.format(ip))
            for key in self.keys:
                vbucketId = crc32.crc32_hash(key) & 1023 # or & 0x3FF
                self.clients[ip].vbucketId = vbucketId
                #                log.info("vbucket : {0}".format(vbucketId))
                self.clients[ip].vbucketId = vbucketId
                payload = self.generate_payload(key + '\0\r\n\0\0\n\r\0',
                                                random.randint(100, 1024))
                flag = socket.htonl(ctypes.c_uint32(zlib.adler32(payload)).value)
                try:
                    (opaque, cas, data) = self.clients[ip].set(key, 0, flag, payload)
                    log.info("inserted key {0} to vBucket {1}".format(key, vbucketId))
                except mc_bin_client.MemcachedError as error:
                    log.info('memcachedError : {0}'.format(error.status))
                    self.test.fail("unable to push key : {0} to bucket : {1}".format(key, self.clients[ip].vbucketId))

            for key in self.keys:
                try:
                    vbucketId = crc32.crc32_hash(key) & 1023 # or & 0x3FF
                    self.clients[ip].vbucketId = vbucketId
                    flag, keyx, value = self.clients[ip].get(key=key)
                    actual_flag = socket.ntohl(flag)
                    expected_flag = ctypes.c_uint32(zlib.adler32(value)).value
                    self.test.assertEquals(actual_flag, expected_flag, msg='flags dont match')
                except mc_bin_client.MemcachedError as error:
                    log.info('memcachedError : {0}'.format(error.status))
                    self.test.fail("unable to get a pre-inserted key : {0}".format(key))

    def set_get_large_keys(self):
        for ip in self.ips:
            log.info(
                'pushing 500 keys (sizes between 500KB and 1 MB to different vbuckets in membase @ {0}'.format(
                    ip))
            error_codes = []
            inserted_keys = []
            for key in self.keys:
                vbucketId = crc32.crc32_hash(key) & 1023 # or & 0x3FF
                self.clients[ip].vbucketId = vbucketId
                #                log.info("vbucket : {0}".format(vbucketId))
                self.clients[ip].vbucketId = vbucketId
                payload = self.generate_payload(key + '\0\r\n\0\0\n\r\0',
                                                random.randint(500 * 1024, 1024 * 1024))
                rest = RestConnection(ip=ip,
                                      username='Administrator',
                                      password='password')
                info = rest.get_bucket(self.bucket_name)
                emptySpace = info.stats.ram - info.stats.memUsed
                #break if there isn't at least 30 mg free space
                if emptySpace < ( 30 * 1024 * 1024):
                    log.info('emptySpace : {0}'.format(emptySpace))
                    break
                flag = socket.htonl(ctypes.c_uint32(zlib.adler32(payload)).value)
                try:
                    (opaque, cas, data) = self.clients[ip].set(key, 0, flag, payload)
                    log.info("inserted key {0} to vBucket {1}".format(key, vbucketId))
                    inserted_keys.append(key)
                except mc_bin_client.MemcachedError as error:
                    log.info('memcachedError : {0}'.format(error.status))
                    error_codes.append(error.status)
                    log.error("unable to push key : {0} to bucket : {1}".format(key, self.clients[ip].vbucketId))

            #fail if we were not able to insert even 1 key
            if error_codes:
                log.info('printing error codes seen during key insertions')
                for code in error_codes:
                    log.info('error code {0} while inserting keys'.format(code))
                self.test.fail('unable to insert {0} keys'.format(len(error_codes)))
            for key in inserted_keys:
                try:
                    vbucketId = crc32.crc32_hash(key) & 1023 # or & 0x3FF
                    self.clients[ip].vbucketId = vbucketId
                    flag, keyx, value = self.clients[ip].get(key=key)
                    actual_flag = socket.ntohl(flag)
                    expected_flag = ctypes.c_uint32(zlib.adler32(value)).value
                    self.test.assertEquals(actual_flag, expected_flag, msg='flags dont match')
                except mc_bin_client.MemcachedError as error:
                    log.info('memcachedError : {0}'.format(error.status))
                    self.test.fail("unable to get a pre-inserted key : {0}".format(key))


    def tearDown_bucket(self):
        #let's clean up the memcached
        for ip in self.ips:
            for key in self.keys:
                try:
                    self.clients[ip].delete(key=key)
                except mc_bin_client.MemcachedError:
                #                    log.info('unable to delete key : {0} from memcached @ {1}'.format(key, ip))
                    pass
            self.clients[ip].close()
        BucketOperationHelper.delete_all_buckets_or_assert(self.ips, self.test)


    def generate_payload(self, pattern, size):
        return (pattern * (size / len(pattern))) + pattern[0:(size % len(pattern))]

    def create_mc_bin_clients_for_ips(self, ips):
        clients = {}
        for ip in ips:
            #let's try to login with sasl
            clients[ip] = mc_bin_client.MemcachedClient(ip, self.bucket_port)
            if self.bucket_name != 'default' and self.bucket_port == 11211:
                #auth
                clients[ip].sasl_auth_start(self.bucket_name,'password')
                #its a password
        return clients

    #move this to a common class
    def extract_server_ips(self):
        servers_string = os.getenv("SERVERS")
        servers = servers_string.split(" ")
        return [server[:server.index(':')] for server in servers]


class SimpleSetGetMembaseBucketDefaultPort11211(unittest.TestCase):
    simpleSetGetTestBase = None

    def setUp(self):
        self.simpleSetGetTestBase = SimpleSetGetTestBase()
        self.simpleSetGetTestBase.setUp_bucket('default', 11211, 'membase', self)

    def test_set_get_small_keys(self):
        self.simpleSetGetTestBase.set_get_small_keys()

    def test_set_get_large_keys(self):
        self.simpleSetGetTestBase.set_get_large_keys()

    def tearDown(self):
        if self.simpleSetGetTestBase:
            self.simpleSetGetTestBase.tearDown_bucket()


class SimpleSetGetMembaseBucketNonDefaultDedicatedPort(unittest.TestCase):
    simpleSetGetTestBase = None

    def setUp(self):
        self.simpleSetGetTestBase = SimpleSetGetTestBase()
        self.simpleSetGetTestBase.setUp_bucket('setget-{0}'.format(uuid.uuid4()), 11220, 'membase', self)

    def test_set_get_small_keys(self):
        self.simpleSetGetTestBase.set_get_small_keys()

    def test_set_get_large_keys(self):
        self.simpleSetGetTestBase.set_get_large_keys()

    def tearDown(self):
        if self.simpleSetGetTestBase:
            self.simpleSetGetTestBase.tearDown_bucket()

class SimpleSetGetMembaseBucketNonDefaultPost11211(unittest.TestCase):

    simpleSetGetTestBase = None

    def setUp(self):
        self.simpleSetGetTestBase = SimpleSetGetTestBase()
        self.simpleSetGetTestBase.setUp_bucket('setget-{0}'.format(uuid.uuid4()), 11211, 'membase', self)

    def test_set_get_small_keys(self):
        self.simpleSetGetTestBase.set_get_small_keys()

    def test_set_get_large_keys(self):
        self.simpleSetGetTestBase.set_get_large_keys()

    def tearDown(self):
        if self.simpleSetGetTestBase:
            self.simpleSetGetTestBase.tearDown_bucket()