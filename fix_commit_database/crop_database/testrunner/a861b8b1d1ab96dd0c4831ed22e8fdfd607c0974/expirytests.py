#!/usr/bin/env python
import os
import unittest
import mc_bin_client
import uuid
import logger
import time
import crc32
from membase.api.rest_client import RestConnection, RestHelper
from membase.helper.bucket_helper import BucketOperationHelper
from remote.remote_util import RemoteMachineShellConnection

log = logger.Logger.get_logger()

class ExpiryTests(unittest.TestCase):
    keys = None
    clients = None
    ips = None
    bucket_port = None

    def setUp(self):
        self.ips = self.extract_server_ips()

        self._cleanup_cluster()
        BucketOperationHelper.delete_all_buckets_or_assert(self.ips, self)
        for ip in self.ips:
            remote = RemoteMachineShellConnection(ip=ip,
                                                  username='root',
                                                  pkey_location=os.getenv("KEYFILE"))
            remote.execute_command('killall -9 memcached')
            #let's first install membase server

        time.sleep(10)
        #lets create 'default' bucket
        bucket_name = 'default';
        self.bucket_port = 11211
        for ip in self.ips:
            rest = RestConnection(ip=ip,
                                  username='Administrator',
                                  password='password')

            time.sleep(5)
            rest.create_bucket(bucket=bucket_name,
                               ramQuotaMB=400,
                               replicaNumber=1,
                               proxyPort=self.bucket_port,
                               bucketType='membase')
            msg = 'create_bucket succeeded but bucket "default" does not exist'
            self.assertTrue(BucketOperationHelper.wait_for_bucket_creation(bucket_name, rest), msg=msg)

        self.wait_till_memcached_is_ready(self.bucket_port)
        self.clients = self.create_mc_bin_clients_for_ips(self.ips)
        #populate key
        testuuid = uuid.uuid4()
        self.keys = ["key_%s_%d" % (testuuid, i) for i in range(100)]

    # test case to set 1000 keys and verify that those keys are stored
    def test_expiry_small_keys(self):
        log.info('starting ... test_expiry_small_keys for ips : {0}'.format(self.ips))
        for ip in self.ips:
            log.info(
                'pushing 100 keys (sizes between 100 bytes to 1KB to different vbuckets in membase @ {0}'.format(ip))
            for key in self.keys:
                vbucketId = crc32.crc32_hash(key) & 1023 # or & 0x3FF
                self.clients[ip].vbucketId = vbucketId
                log.info("vbucket : {0}".format(vbucketId))
                try:
                    #TODO: also invoke add and update
                    (opaque, cas, data) = self.clients[ip].set(key, 2, 0, key)
                    log.info("inserted key {0} to vBucket {1}".format(key, vbucketId))
                except mc_bin_client.MemcachedError as error:
                    log.info('memcachedError : {0}'.format(error.status))
                    log.info("unable to push key : {0} to bucket : {1}".format(key, self.clients[ip].vbucketId))

            log.info('sleep for 10 seconds wait for those items with expiry set to 10 to expire')
            time.sleep(10)
            for key in self.keys:
                try:
                    vbucketId = crc32.crc32_hash(key) & 1023 # or & 0x3FF
                    self.clients[ip].vbucketId = vbucketId
                    (flag, keyx, value) = self.clients[ip].get(key=key)
                    self.fail("key {0} did not expire".format(key))
                except mc_bin_client.MemcachedError as error:
                    log.info('expected error : memcachedError : {0}'.format(error.status))


    def tearDown(self):
    #let's clean up the memcached
    #        BucketOperationHelper.delete_all_buckets_or_assert(self.ips, self)
        pass

    def generate_payload(self, pattern, size):
        return (pattern * (size / len(pattern))) + pattern[0:(size % len(pattern))]

    def create_mc_bin_clients_for_ips(self, ips):
        clients = {}
        for ip in ips:
            clients[ip] = mc_bin_client.MemcachedClient(ip, self.bucket_port)
        return clients

    #move this to a common class
    def extract_server_ips(self):
        servers_string = os.getenv("SERVERS")
        servers = servers_string.split(" ")
        return [server[:server.index(':')] for server in servers]

    def wait_till_memcached_is_ready(self, bucket_port):
        for ip in self.ips:
            start_time = time.time()
            memcached_ready = False
            #bucket port
            while time.time() <= (start_time + (5 * 60)):
                log.info(" port {0}:{1}".format(ip, bucket_port))
                client = mc_bin_client.MemcachedClient(ip, bucket_port)
                key = '{0}'.format(uuid.uuid4())
                vbucketId = crc32.crc32_hash(key) & 1023 # or & 0x3FF
                client.vbucketId = vbucketId
                try:
                    client.set(key, 0, 0, key)
                    log.info("inserted key {0} to vBucket {1}".format(key, vbucketId))
                    memcached_ready = True
                    break
                except mc_bin_client.MemcachedError as error:
                    log.error(
                        "memcached not ready yet .. (memcachedError : {0}) - unable to push key : {1} to bucket : {2}".format(
                            error.status, key, client.vbucketId))
                except:
                    log.error("memcached not ready yet .. unable to push key : {0} to bucket : {1}".format(key,
                                                                                                           client.vbucketId))
                #                client.close()
                time.sleep(5)
            if not memcached_ready:
                self.fail('memcached not ready for {0} after waiting for 5 minutes'.format(ip))

    def _cleanup_cluster(self):
        master = self.ips[0]
        rest = RestConnection(ip=master,
                              username='Administrator',
                              password='password')
        nodes = rest.node_statuses()
        allNodes = []
        toBeEjectedNodes = []
        for node in nodes:
            allNodes.append(node.id)
            if node.id.find(master) < 0 and node.id.find('127.0.0.1') < 0:
                toBeEjectedNodes.append(node.id)
            remote = RemoteMachineShellConnection(ip=node.ip,
                                                  username='root',
                                                  pkey_location=os.getenv("KEYFILE"))
            remote.execute_command('killall -9 memcached')
            #let's rebalance to remove all the nodes from the master
            #this is not the master , let's remove it
            #now load data into the main bucket

        if toBeEjectedNodes:
            time.sleep(60)
            log.info("removing all nodes from the cluster...")
            helper = RestHelper(rest)
            helper.remove_nodes(knownNodes=allNodes,
                                ejectedNodes=toBeEjectedNodes)
            #now we can create a default bucket on the master node ?