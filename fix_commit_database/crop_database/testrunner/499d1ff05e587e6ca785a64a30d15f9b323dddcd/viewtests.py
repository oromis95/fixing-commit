import json
import unittest
import uuid
from TestInput import TestInputSingleton
import logger
from membase.api.rest_client import RestConnection
from membase.helper.bucket_helper import BucketOperationHelper
from membase.helper.cluster_helper import ClusterOperationHelper
from memcached.helper.data_helper import MemcachedClientHelper


class ViewTests(unittest.TestCase):
    def setUp(self):
        self.log = logger.Logger.get_logger()
        bucket_ram_ratio = (2.0 / 3.0)
        replica = 1
        self.log = logger.Logger.get_logger()
        self.servers = TestInputSingleton.input.servers
        self.membase = TestInputSingleton.input.membase_settings
        BucketOperationHelper.delete_all_buckets_or_assert(self.servers, self)
        ClusterOperationHelper.cleanup_cluster(self.servers)
        ClusterOperationHelper.wait_for_ns_servers_or_assert(self.servers, self)
        serverInfo = self.servers[0]

        self.log.info('picking server : {0} as the master'.format(serverInfo))
        #if all nodes are on the same machine let's have the bucket_ram_ratio as bucket_ram_ratio * 1/len(servers)
        node_ram_ratio = BucketOperationHelper.base_bucket_ratio(self.servers)
        rest = RestConnection(serverInfo)
        BucketOperationHelper.create_multiple_buckets(serverInfo, replica, node_ram_ratio * bucket_ram_ratio, howmany=1)


    def test_create_view(self):
        master = self.servers[0]
        rest = RestConnection(master)
        #find the first bucket
        buckets = rest.get_buckets()
        bucket = buckets[0].name
        mc = MemcachedClientHelper.direct_client(master, bucket)
        key = str(uuid.uuid4())
        value = {"name": "test_create_view", "age": 1000}
        mc.set(key, 0, 0, json.dumps(value))
        mc.get(key)
        view_name = "dev_" + str(uuid.uuid4())[:5]
        function = "function (doc) { if (doc.name == \"test_create_view\") { emit(doc.name, doc.age);}}"
        req = {"language": "javascript",
               "_id": "_design/{0}".format(view_name),
               "views": {view_name: {"map": function}}}
        function = json.dumps(req)
        rest.create_view(bucket, view_name, function)
        view_str = rest.get_view(bucket, view_name)
        #assert that view string is not empty
        self.log.info(view_str)
        for i in range(0, 10):
            try:
                results = rest.view_results(bucket, view_name, 20)
                self.log.info(results)
                if "rows" in results:
                    #make sure we got that id and key back ?
                    keys = results["rows"]
                    actual_key = keys[0]
                    self.assertEquals(actual_key, key)
                break
            except:
                self.log.error("resultset is not ready yet!")


    def tearDown(self):
        #delete_view is not working now , we need to find out
        #a way to delete all views
        pass

