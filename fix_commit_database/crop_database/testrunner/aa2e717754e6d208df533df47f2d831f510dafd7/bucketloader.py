# this script will connect to the node in the given ini file and create "n" buckets
# and load data until program is terminated
# example python bucketloader.py -i node.ini -p buckets=10,
from httplib import BadStatusLine
import sys
import os
from threading import Thread
import urllib2
import zipfile
import base64
import TestInput
from memcached.helper.data_helper import MemcachedClientHelper

sys.path.append('.')
sys.path.append('lib')

from membase.api.rest_client import RestConnection

def create_buckets(server,count,prefix="membase-"):
    #check how many buckets already created in this cluster
    rest = RestConnection(server)
    buckets = rest.get_buckets()
    if len(buckets) < count:
        delta = count - len(buckets)
        info = rest.get_nodes_self()
        replica = 1
        #TODO: calculate the bucket_ram from available_ram / delta
        bucket_ram = 200
        #how much ram is used by all buckets
        for i in range(0,delta):
            name = "{0}-{1}".format(prefix, i)
            bucket = rest.create_bucket(bucket=name,
                               ramQuotaMB=bucket_ram,
                               replicaNumber=replica,
                               proxyPort=info.moxi)
            print "created bucket {0}".format(name)

def load_buckets(server,name):
    distro = {500:0.5,1024:0.5}
    MemcachedClientHelper.load_bucket(server,name,-1,1000000,distro,6,-1,False,False)


if __name__ == "__main__":
    input = TestInput.TestInputParser.get_test_input(sys.argv)
    server = input.servers[0]
    params = input.test_params
    count = int(params["buckets"])
    create_buckets(server)
    rest = RestConnection(server)
    buckets = rest.get_buckets()
    threads = []
    for bucket in buckets:
        t = Thread(target=load_buckets,args=(server,bucket.name))
        t.start()
        threads.append(t)

    for t in threads:
        t.join()



