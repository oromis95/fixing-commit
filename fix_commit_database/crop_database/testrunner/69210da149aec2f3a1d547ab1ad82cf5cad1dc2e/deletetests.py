# test case to create , and delete and verify delete
# test case to create , delete and re-create
import unittest
import os
import uuid
import logger
import time
from membase.api.rest_client import RestConnection, RestHelper
from remote.remote_util import RemoteMachineShellConnection


log = logger.Logger.get_logger()

class DeleteMembaseBucketsTests(unittest.TestCase):
    version = None
    ips = None

    #as part of the setup let's delete all the existing buckets
    def setUp(self):
        self.ips = self.extract_server_ips()
        self.delete_all_buckets_or_assert(self.ips)

    def tearDown(self):
        self.delete_all_buckets_or_assert(self.ips)
        pass

    # read each server's version number and compare it to self.version
    def test_default_on_11211(self):
        name = 'default'
        for ip in self.ips:
            rest = RestConnection(ip=ip,
                                  username='Administrator',
                                  password='password')
            rest.create_bucket(bucket=name,
                               ramQuotaMB=400,
                               replicaNumber=1,
                               proxyPort=11211)
            remote = RemoteMachineShellConnection(username='root',
                                                  ip=ip,
                                                  pkey_location=os.getenv("KEYFILE"))
            msg = 'create_bucket succeeded but bucket {0} does not exist'.format(name)
            self.assertTrue(self.wait_for_bucket_creation(name, rest), msg=msg)
            rest.delete_bucket(name)
            msg = 'bucket "{0}" was not deleted even after waiting for two minutes'.format(name)
            self.assertTrue(self.wait_for_bucket_deletion(name, rest, timeout_in_seconds=30), msg=msg)
            msg = 'bucket {0} data files are not deleted after bucket deleted from membase'.format(name)
            self.assertTrue(
                self.wait_for_data_files_deletion(name,
                                                  remote_connection=remote,
                                                  rest=rest, timeout_in_seconds=20), msg=msg)

    def test_non_default(self):
        name = 'new-bucket-{0}'.format(uuid.uuid4())
        for ip in self.ips:
            rest = RestConnection(ip=ip,
                                  username='Administrator',
                                  password='password')
            rest.create_bucket(bucket=name,
                               ramQuotaMB=400,
                               replicaNumber=1,
                               proxyPort=11216)
            remote = RemoteMachineShellConnection(username='root',
                                                  ip=ip,
                                                  pkey_location=os.getenv("KEYFILE"))
            msg = 'create_bucket succeeded but bucket {0} does not exist'.format(name)
            self.assertTrue(self.wait_for_bucket_creation(name, rest), msg=msg)
            rest.delete_bucket(name)
            msg = 'bucket "{0}" was not deleted even after waiting for two minutes'.format(name)
            self.assertTrue(self.wait_for_bucket_deletion(name, rest, timeout_in_seconds=30), msg=msg)
            msg = 'bucket {0} data files are not deleted after bucket deleted from membase'.format(name)
            self.assertTrue(
                self.wait_for_data_files_deletion(name,
                                                  remote_connection=remote,
                                                  rest=rest, timeout_in_seconds=20), msg=msg)


    #move these methods to a helper class

    def delete_all_buckets_or_assert(self, ips):
        for ip in ips:
            rest = RestConnection(ip=ip,
                                  username='Administrator',
                                  password='password')
            buckets = rest.get_buckets()
            for bucket in buckets:
                print bucket.name
                rest.delete_bucket(bucket.name)
                log.info('deleted bucket : {0}'.format(bucket.name))
                msg = 'bucket "{0}" was not deleted even after waiting for two minutes'.format(bucket.name)
                self.assertTrue(self.wait_for_bucket_deletion(bucket.name, rest, 200)
                                , msg=msg)

    def wait_for_data_files_deletion(self,
                                     bucket,
                                     remote_connection,
                                     rest,
                                     timeout_in_seconds=120):
        log.info('waiting for bucket data files deletion from the disk ....')
        start = time.time()
        while (time.time() - start) <= timeout_in_seconds:
            if self.verify_data_files_deletion(bucket, remote_connection, rest):
                return True
            else:
                data_file = '{0}-data'.format(bucket)
                log.info("still waiting for deletion of {0} ...".format(data_file))
                time.sleep(2)
        return False


    def verify_data_files_deletion(self,
                                   bucket,
                                   remote_connection,
                                   rest):
        node = rest.get_nodes_self()
        for item in node.storage:
            #get the path
            data_file = '{0}-data'.format(bucket)
            if remote_connection.file_exists(item.path, data_file):
                return False
        return True
        #get the data path from nodes_self
        #list all the files in that directory


    def wait_for_bucket_creation(self,
                                 bucket,
                                 rest,
                                 timeout_in_seconds=120):
        log.info('waiting for bucket "{0}" creation to complete....'.format(bucket))
        start = time.time()
        helper = RestHelper(rest)
        while (time.time() - start) <= timeout_in_seconds:
            if helper.bucket_exists(bucket):
                return True
            else:
                time.sleep(2)
        return False

    def wait_for_bucket_deletion(self,
                                 bucket,
                                 rest,
                                 timeout_in_seconds=120):
        log.info('waiting for bucket deletion to complete....')
        start = time.time()
        helper = RestHelper(rest)
        while (time.time() - start) <= timeout_in_seconds:
            if not helper.bucket_exists(bucket):
                return True
            else:
                time.sleep(2)
        return False

    def extract_server_ips(self):
        servers_string = os.getenv("SERVERS")
        servers = servers_string.split(" ")
        return [server[:server.index(':')] for server in servers]