import unittest
import os
import uuid
import logger
import time
from membase.api.exception import BucketCreationException
from membase.api.rest_client import RestConnection, RestHelper

log = logger.Logger.get_logger()

class CreateMembaseBucketsTests(unittest.TestCase):
    version = None
    ips = None

    #as part of the setup let's delete all the existing buckets
    def setUp(self):
        self.ips = self.extract_server_ips()
        self.delete_all_buckets_or_assert(self.ips)

    def tearDown(self):
        self.delete_all_buckets_or_assert(self.ips)
        pass

    # read each server's version number and compare it to self.version
    def test_default_on_11211(self):
        name = 'default'
        for ip in self.ips:
            rest = RestConnection(ip=ip,
                                  username='Administrator',
                                  password='password')
            rest.create_bucket(bucket=name,
                               ramQuotaMB=400,
                               replicaNumber=1,
                               proxyPort=11211)
            msg = 'create_bucket succeeded but bucket {0} does not exist'.format(name)
            self.assertTrue(self.wait_for_bucket_creation(name, rest), msg=msg)


    def test_default_case_sensitive_on_11211(self):
        name = 'Default'
        for ip in self.ips:
            rest = RestConnection(ip=ip,
                                  username='Administrator',
                                  password='password')
            rest.create_bucket(bucket=name,
                               ramQuotaMB=400,
                               replicaNumber=1,
                               proxyPort=11211,
                               authType='sasl',
                               saslPassword='test_non_default')
            msg = 'create_bucket succeeded but bucket {0} does not exist'.format(name)
            self.assertTrue(self.wait_for_bucket_creation(name, rest), msg=msg)

            name = 'default'

            try:
                rest.create_bucket(bucket=name,
                                   ramQuotaMB=400,
                                   replicaNumber=1,
                                   proxyPort=11221,
                                   authType='sasl',
                                   saslPassword='test_non_default')
                msg = "create_bucket created two buckets in different case : {0},{1}".format('default','Default')
                self.fail(msg)
            except BucketCreationException as ex:
            #check if 'default' and 'Default' buckets exist
                log.info('BucketCreationException was thrown as expected')
                log.info(ex.message)

    def test_default_on_non_default_port(self):
        name = 'default'
        for ip in self.ips:
            rest = RestConnection(ip=ip,
                                  username='Administrator',
                                  password='password')
            rest.create_bucket(bucket=name,
                               ramQuotaMB=400,
                               replicaNumber=1,
                               proxyPort=11217,
                               authType='sasl',
                               saslPassword='test_non_default')
            msg = 'create_bucket succeeded but bucket {0} does not exist'.format(name)
            self.assertTrue(self.wait_for_bucket_creation(name, rest), msg=msg)


    def test_non_default(self):
        name = 'test_non_default'
        for ip in self.ips:
            rest = RestConnection(ip=ip,
                                  username='Administrator',
                                  password='password')
            rest.create_bucket(bucket=name,
                               ramQuotaMB=400,
                               replicaNumber=1,
                               proxyPort=11217)
            msg = 'create_bucket succeeded but bucket {0} does not exist'.format(name)
            self.assertTrue(self.wait_for_bucket_creation(name, rest), msg=msg)

    def test_default_case_sensitive_different_port(self):
        name = 'default'
        for ip in self.ips:
            rest = RestConnection(ip=ip,
                                  username='Administrator',
                                  password='password')
            rest.create_bucket(bucket=name,
                               ramQuotaMB=400,
                               replicaNumber=1,
                               proxyPort=11220)
            msg = 'create_bucket succeeded but bucket {0} does not exist'.format(name)
            self.assertTrue(self.wait_for_bucket_creation(name, rest), msg=msg)

            try:
                name = 'DEFAULT'
                rest.create_bucket(bucket=name,
                                   ramQuotaMB=400,
                                   replicaNumber=1,
                                   proxyPort=11221)
                msg = "create_bucket created two buckets in different case : {0},{1}".format('default','DEFAULT')
                self.fail(msg)
            except BucketCreationException as ex:
                #check if 'default' and 'Default' buckets exist
                log.info('BucketCreationException was thrown as expected')
                log.info(ex.message)


    def test_non_default_case_sensitive_different_port(self):
        postfix = uuid.uuid4()
        lowercase_name = 'uppercase_{0}'.format(postfix)
        for ip in self.ips:
            rest = RestConnection(ip=ip,
                                  username='Administrator',
                                  password='password')
            rest.create_bucket(bucket=lowercase_name,
                               ramQuotaMB=400,
                               replicaNumber=1,
                               proxyPort=11220)
            msg = 'create_bucket succeeded but bucket {0} does not exist'.format(lowercase_name)
            self.assertTrue(self.wait_for_bucket_creation(lowercase_name, rest), msg=msg)

            uppercase_name = 'UPPERCASE_{0}'.format(postfix)
            try:
                rest.create_bucket(bucket=uppercase_name,
                                   ramQuotaMB=400,
                                   replicaNumber=1,
                                   proxyPort=11221)
                msg = "create_bucket created two buckets in different case : {0},{1}".format(lowercase_name,uppercase_name)
                self.fail(msg)
            except BucketCreationException as ex:
                #check if 'default' and 'Default' buckets exist
                log.info('BucketCreationException was thrown as expected')
                log.info(ex.message)

    def test_non_default_case_sensitive_same_port(self):
        postfix = uuid.uuid4()
        name = 'uppercase_{0}'.format(postfix)
        for ip in self.ips:
            rest = RestConnection(ip=ip,
                                  username='Administrator',
                                  password='password')
            rest.create_bucket(bucket=name,
                               ramQuotaMB=400,
                               replicaNumber=1,
                               proxyPort=11218)
            msg = 'create_bucket succeeded but bucket {0} does not exist'.format(name)
            self.assertTrue(self.wait_for_bucket_creation(name, rest), msg=msg)

            log.info("user should not be able to create a new bucket on a an already used port")
            name = 'UPPERCASE{0}'.format(postfix)
            try:
                rest.create_bucket(bucket=name,
                                   ramQuotaMB=400,
                                   replicaNumber=1,
                                   proxyPort=11218)
                self.fail('create-bucket did not throw exception while creating a new bucket on an already used port')
            #make sure it raises bucketcreateexception
            except BucketCreationException as ex:
                log.error(ex)


    def test_default_recreate(self):
        pass

    def test_non_default_recreate(self):
        pass

    def delete_all_buckets_or_assert(self, ips):
        for ip in ips:
            rest = RestConnection(ip=ip,
                                  username='Administrator',
                                  password='password')
            buckets = rest.get_buckets()
            for bucket in buckets:
                print bucket.name
                rest.delete_bucket(bucket.name)
                log.info('deleted bucket : {0}'.format(bucket.name))
                msg = 'bucket "{0}" was not deleted even after waiting for two minutes'.format(bucket.name)
                self.assertTrue(self.wait_for_bucket_deletion(bucket.name, rest, 120)
                                , msg=msg)


    def wait_for_bucket_creation(self,
                                 bucket,
                                 rest,
                                 timeout_in_seconds=120):
        log.info('waiting for bucket creation to complete....')
        start = time.time()
        helper = RestHelper(rest)
        while (time.time() - start) <= timeout_in_seconds:
            if helper.bucket_exists(bucket):
                return True
            else:
                time.sleep(2)
        return False

    def wait_for_bucket_deletion(self,
                                 bucket,
                                 rest,
                                 timeout_in_seconds=120):
        log.info('waiting for bucket deletion to complete....')
        start = time.time()
        helper = RestHelper(rest)
        while (time.time() - start) <= timeout_in_seconds:
            if not helper.bucket_exists(bucket):
                return True
            else:
                time.sleep(2)
        return False

    def extract_server_ips(self):
        servers_string = os.getenv("SERVERS")
        servers = servers_string.split(" ")
        return [server[:server.index(':')] for server in servers]