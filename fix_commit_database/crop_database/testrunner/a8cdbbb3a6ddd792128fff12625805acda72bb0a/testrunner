#!/usr/bin/env python

import sys
import time
import getopt
import unittest
sys.path.append("lib")
sys.path.append("pytests")
from TestInput import TestInputParser, TestInputSingleton

class Testcase(object):
    def __init__(self, name):
        self.name = name
        self.description = name
        self.log = ""

    def report(self):
        return "Starting test: " + self.description + "\n" + self.log

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


class Report(object):
    pass


def usage(err=None):
    if err:
        print "Error: %s\n" % err
        r = 1
    else:
        r = 0

    print """\
Syntax: testrunner [options]

Options:
 -c <file>        Config file name (located in the conf subdirectory)
 -i <file>        Path to .ini file containing server information
 -s <server>      Hostname of server (multiple -s options add more servers)
 -t <test>        Test name (multiple -t options add more tests)
"""
    sys.exit(r)


class Config(object):
    def __init__(self):
        self.test_group = "quick"
        self.tests = []


def parse_args(argv):
    config = Config()

    try:
        (opts, args) = getopt.getopt(argv[1:],
                                     'h:t:c:v:s:i:p:', [])
    except IndexError:
        usage()
    except getopt.GetoptError, err:
        usage(err)

    for o, a in opts:
        if o == "-h":
            usage()
        elif o == "-c":
            for line in file("conf/" + a):
                config.tests.append(Testcase(line.strip()))
            config.test_group = a.replace(".conf", "")
        elif o == "-t":
            config.tests.append(Testcase(a))
    test_input = TestInputParser.get_test_input(argv)
    if not test_input.servers:
        usage("no servers specified")
    if not config.tests:
        usage("no tests specified")
    return config, test_input


if __name__ == "__main__":
    config, test_input = parse_args(sys.argv)

    names = [test.name for test in config.tests]
    from xunit import XUnitTestSuite

    xunit = XUnitTestSuite()
    xunit.name = "testrunner-suite"
    xunit_loaded = True

    for name in names:
        import logger

        start_time = time.time()
        dotnames = name.split('.')
        log_name = dotnames[len(dotnames) - 1]
        logger.Logger.start_logger(log_name)

        TestInputSingleton.input = TestInputParser.get_test_input(sys.argv)
        suite = unittest.TestLoader().loadTestsFromNames(names=[name])
        result = unittest.TextTestRunner(verbosity=2).run(suite)
        logger.Logger.stop_logger()
        time_taken = time.time() - start_time
        if result.failures or result.errors:
            for failure in result.failures:
                test_case, failure_string = failure
                xunit.add_test(name=name, status='fail', time=time_taken,
                               errorType='membase.error', errorMessage=failure_string)
                break
        else:
            xunit.add_test(name=name, status='pass', time=time_taken)

    report_xml_file = open('report.xml', 'w')
    report_xml_file.write(xunit.to_xml())
    report_xml_file.close()