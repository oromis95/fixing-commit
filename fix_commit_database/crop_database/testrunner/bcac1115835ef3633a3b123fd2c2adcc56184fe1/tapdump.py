
import sys

sys.path.append('.')
sys.path.append('lib')

import TestInput
import time
from membase.api.tapclients import TapDump, TapDumpOptions

if __name__ == "__main__":
    try:
        input = TestInput.TestInputParser.get_test_input(sys.argv)
        output = []
        params = input.test_params
        options = TapDumpOptions()
        options.keys = True
        options.values = False
        if "vbuckets" in params:
            #split by comma
            vbuckets = []
            tmp = params["vbuckets"]
            print tmp
            tmp = tmp.replace("[","").replace("]","")
            print tmp
            string_list = tmp.split(".")
            for s in string_list:
                vbuckets.append(int(s))
            options.vbuckets = vbuckets
        if "output" in params:
            output = params["output"]
        dump = TapDump(input.servers[0],options)
        dump.start()

        #wait until 5 minutes
        start = time.time()
        last_callback = dump.last_callback
        while True:
            if time.time() - last_callback > 15:
                dump.aborted = True
                break
            else:
                last_callback = dump.last_callback
            time.sleep(1)
            print last_callback


        list = dump.dump()
        if output:
            dump_file = open("{0}".format(output), 'w')
        for l in list:
            print l
            if dump_file:
                dump_file.write(l["key"])
                dump_file.write("\n")

        if dump_file:
            dump_file.close()

    except Exception as ex:
        print ex
