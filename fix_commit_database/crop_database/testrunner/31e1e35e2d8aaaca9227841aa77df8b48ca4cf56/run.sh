#!/bin/sh
#
# Variables passed:
#    SERVERS
#    TESTNAME
#    VERSION
#    KEYFILE

# If VERSION isn't set, bail.

if [[ -z "$VERSION" && -z "$MEMBASE_DIR" ]]; then
    echo "[$TESTNAME] VERSION or MEMBASE_DIR is not set."
    exit 1
fi

for SERVER in $SERVERS ; do
    echo "[$TESTNAME] Running install -s $SERVER"
    tests/$TESTNAME/install -s $SERVER -v "$VERSION" -b "$MEMBASE_DIR" -k "$KEYFILE" -l "$BUILDPATH" &
done
wait

for SERVER in $SERVERS ; do
    SERVER_IP=$(echo $SERVER | cut -f 1 -d ":")
    echo "checking $SERVER_IP"
    ssh -i $KEYFILE root@$SERVER_IP /opt/membase/bin/ep_engine/management/stats localhost:11210 all default | grep "ep_warmup_thread:\W*complete" &> /dev/null
    if [[ $? -ne 0 ]] ; then
        echo "[$TESTNAME] server not running on $SERVER"
        RETCODE=1
    fi
done

# even when ramQuota is set it seems the bucket is still not guarenteed to be available :(
sleep 10

exit $RETCODE
