import sys


sys.path.append('.')
sys.path.append('lib')

import TestInput
import time
from membase.api.rest_client import RestConnection
from membase.api.tapclients import TapDump, TapDumpOptions

if __name__ == "__main__":
    try:
        input = TestInput.TestInputParser.get_test_input(sys.argv)
        output = []
        params = input.test_params
        options = TapDumpOptions()
        options.keys = True
        options.values = False
        if "vbuckets" in params:
            #split by comma
            vbuckets = []
            tmp = params["vbuckets"]
            print tmp
            tmp = tmp.replace("[", "").replace("]", "")
            print tmp
            string_list = tmp.split(".")
            for s in string_list:
                vbuckets.append(int(s))
            options.vbuckets = vbuckets
        if "output" in params:
            output = params["output"]
            dump_file = open("{0}".format(output), 'w')

        master = input.servers[0]
        nodes = RestConnection(master).node_statuses()
        servers = []
        for node in nodes:
            server = TestInput.TestInputServer()
            server.ip = node.ip
            print server.ip
            server.port = master.port
            server.rest_username = master.rest_username
            server.rest_password = master.rest_password
            servers.append(server)

        for server in servers:
            print "creating a tap connecton to {0}".format(server.ip)
            #check if server is part of the cluster ?
            dump = TapDump(server, options)
            dump.start()
            #wait until 5 minutes
            start = time.time()
            last_callback = dump.last_callback
            while True:
                if time.time() - last_callback > 10:
                    dump.aborted = True
                    break
                else:
                    last_callback = dump.last_callback
                time.sleep(2)
                print "....."

            list = dump.dump()
            for l in list:
                str = "ip:{0}:vb:{1}:key:{2}".format(server.ip, l["vb"], l["key"])
                print str
                if dump_file:
                    dump_file.write(str)
                    dump_file.write("\n")

        if dump_file:
            dump_file.close()


    except Exception as ex:
        print ex
