from threading import Thread
import time
import uuid
from membase.api.rest_client import RestConnection
from membase.api.tap import TapConnection

import memcacheConstants

class TapDumpOptions:
    def __init__(self):
        self.keys = True
        self.values = True
        self.vbuckets = []


class TapDump(Thread):
    #this class will return all key and values to the user in a stream format
    def __init__(self, server, options):
        Thread.__init__(self)
        self.server = server
        self.aborted = False
        self.list = []
        self.options = options
        self.last_callback = time.time()


    def callback(self, identifier, cmd, extra, key, vb, val, cas):
        command_names = memcacheConstants.COMMAND_NAMES[cmd]
        self.last_callback = time.time()
        if command_names == "CMD_TAP_MUTATION":
        #            print "%s: ``%s'' (vb:%d) -> (%d bytes from %s)" % (
        #            memcacheConstants.COMMAND_NAMES[cmd],
        #            key, vb, len(val), identifier)
            object = {}
            if self.options.keys:
                object["key"] = key
            if self.options.values:
                object["value"] = val
            object["vb"] = vb
            self.list.append(object)


    def run(self):
        print "starting tap process"
        #get memcached port from server
        tap_opts = {}
        info = RestConnection(self.server).get_nodes_self()
        tap_opts[memcacheConstants.TAP_FLAG_DUMP] = ""
        if self.options.vbuckets:
            tap_opts[memcacheConstants.TAP_FLAG_LIST_VBUCKETS] = self.options.vbuckets
        print tap_opts,info.memcached
        t = TapConnection(server=self.server,
                          port=11210,
                          callback=self.callback,
                          clientId=str(uuid.uuid4()),
                          opts=tap_opts)
        while True and not self.aborted:
            t.receive()

    def dump(self):
        #this will return the list
        return self.list
