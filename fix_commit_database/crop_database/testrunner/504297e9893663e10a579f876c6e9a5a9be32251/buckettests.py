#this test will create 10 threads,
#each thread create one

#this thread will create buckets on port x and will run all sort of operations
#and report a fail or pass
import time
from threading import Thread
import unittest
import os
import uuid
import crc32
import logger
from mc_bin_client import MemcachedClient, MemcachedError
import mc_bin_client
from membase.api.rest_client import RestConnection
from membase.helper.bucket_helper import BucketOperationHelper
from remote.remote_util import RemoteMachineShellConnection, RemoteMachineHelper

log = logger.Logger.get_logger()

class BucketParallelTest(unittest.TestCase):

    def setup(self):
        self.ip = self.extract_server_ips()[0]
        log.info('deleting all the buckets... on server : {0}'.format(self.ip))
        BucketOperationHelper.delete_all_buckets_or_assert([self.ip],self)
        log.info('sleep 10 seconds after bucket deletions')
        time.sleep(10)

    def tearDown(self):
        pass

    def test_10_bucket_threads(self):
        self.setup()
        self.ip = self.extract_server_ips()[0]
        #create 10 threads
        threads = []
        for port in range(11220,11230):
            thread = BucketTestThread(ip=self.ip,
                                      number_of_keys=400,
                                      run_minutes=2,
                                      bucket_port=port,
                                      delay_data_ops=0)
            thread.start()
            threads.append(thread)
        #wait for all the threads

        #let's monitor memcached process
        connection = RemoteMachineShellConnection(ip=self.ip,
                                                  username='root',
                                                  pkey_location=os.getenv("KEYFILE"))
        remote_helper = RemoteMachineHelper(connection)
        time.sleep(30)
        start_time = time.time()
        log.info('start_time : {0}'.format(start_time))
        ran_continuously = remote_helper.monitor_process('memcached',480)
        log.info('end_time : {0}'.format(start_time))
        end_time = time.time()
        diff = end_time - start_time
        for t in threads:
            t.join(timeout=2 * 60)
            
        message = 'memcached process crashed at least once during this test run.first time {0} seconds after test kickoff'
        self.assertTrue(ran_continuously,message.format(diff))


    def extract_server_ips(self):
        servers_string = os.getenv("SERVERS")
        servers = servers_string.split(" ")
        return [server[:server.index(':')] for server in servers]


class BucketTestThread(Thread):

    result = ''
    bucket_port = 11211
    number_of_keys = 100
    run_minutes = 1
    ip = ''
    delay_data_ops = -1
    bucket_name = ''

    # define which port to create and delete the bucket
    # how many keys to insert , this thread will keep inserting x number of keys
    # in a thread for y minutes
    # this will add a sleep time between each key operation
    def __init__(self,ip,
                 bucket_port,
                 number_of_keys = 10000,
                 run_minutes = 2,
                 delay_data_ops = 0):
        Thread.__init__(self)
        self.bucket_port = bucket_port
        self.number_of_keys = number_of_keys
        self.run_minutes = run_minutes
        self.ip = ip
        self.delay_data_ops = delay_data_ops
        self.bucket_name = ''

    def run(self):
        log.info('bootstrap time 10 seconds for each thread')
        time.sleep(10)
        start_time = time.time()
        end_time = start_time + self.run_minutes * 60
        while time.time() < end_time:
            self.bucket_name = 'BucketTestThread-{0}'.format(uuid.uuid4())
            #repeat these actions
            #create a bucket
            #run some key operations on this bucket with 1 seconds delay
            #create the bucket
            rest = RestConnection(ip=self.ip,
                                  username='Administrator',
                                  password='password')

            rest.create_bucket(bucket=self.bucket_name,
                               ramQuotaMB=128,
                               bucketType='membase',
                               proxyPort=self.bucket_port,
                               replicaNumber=1)
            BucketOperationHelper.wait_for_bucket_creation(bucket = self.bucket_name,
                                                           rest = rest,
                                                           timeout_in_seconds=120)
            memcached_ready = self.wait_till_memcached_is_ready(self.ip,self.bucket_port)
            if memcached_ready:
            #let's wait for memcached to be ready
                self._load_data(1,self.number_of_keys)
                rest.delete_bucket(bucket=self.bucket_name)
                BucketOperationHelper.wait_for_bucket_deletion(bucket = self.bucket_name,
                                                               rest = rest,
                                                               timeout_in_seconds=120)
            log.info("deleting the bucket {0}".format(self.bucket_name))
            rest.delete_bucket(bucket=self.bucket_name)
            BucketOperationHelper.wait_for_bucket_deletion(self.bucket_name, rest)


    #let's try to fill up ram up to y percentage
    def _load_data(self,fill_ram_percentage,minimum_number_of_keys):
        if fill_ram_percentage <= 0.0:
            fill_ram_percentage = 5.0
        client = MemcachedClient(self.ip, 11220)
        #populate key
        rest = RestConnection(ip=self.ip,
                              username='Administrator',
                              password='password')
        testuuid = uuid.uuid4()
        info = rest.get_bucket(self.bucket_name)
        emptySpace = info.stats.ram - info.stats.memUsed
        log.info('emptySpace : {0} fill_ram_percentage : {1}'.format(emptySpace, fill_ram_percentage))
        fill_space = (emptySpace * fill_ram_percentage)/100.0
        log.info("fill_space {0}".format(fill_space))
        # each packet can be 10 KB
        key_insertion_failed = False
        packetSize = int(10 *1024)
        calc_number_of_keys = int(fill_space) / packetSize
        #get max of number_of_keys_to_fill or self.number_of_keys
        number_of_keys_to_insert = max(calc_number_of_keys,minimum_number_of_keys)
        log.info('packetSize: {0}'.format(packetSize))
        log.info('memory usage before key insertion : {0}'.format(info.stats.memUsed))
        log.info('inserting {0} new keys to memcached @ {0}'.format(number_of_keys_to_insert,self.ip))
        self.keys = ["key_%s_%d" % (testuuid, i) for i in range(number_of_keys_to_insert)]
        for key in self.keys:
            vbucketId = crc32.crc32_hash(key) & 1023 # or & 0x3FF
            client.vbucketId = vbucketId
            try:
                (opaque, cas, data) = client.set(key, 0, 0, key)
                if self.delay_data_ops >= 0:
                    time.sleep(self.delay_data_ops)
            except MemcachedError as error:
                log.error(error)
                client.close()
                key_insertion_failed = True
                log.error("unable to push key : {0} to bucket : {1}".format(key, client.vbucketId))
                break
                #let' return now ? or just ignore

        if not key_insertion_failed:
            client.close()

    def wait_till_memcached_is_ready(self,ip,port):
        start_time = time.time()
        memcached_ready = False
        #bucket port
        while time.time() <= (start_time + (5 * 60)):
            log.info("bucket port : {0}".format(port))
            client = mc_bin_client.MemcachedClient(ip, port)
            key = '{0}'.format(uuid.uuid4())
            vbucketId = crc32.crc32_hash(key) & 1023 # or & 0x3FF
            client.vbucketId = vbucketId
            try:
                client.set(key, 0, 0, key)
                log.info("inserted key {0} to vBucket {1}".format(key, vbucketId))
                memcached_ready = True
                break
            except mc_bin_client.MemcachedError as error:
                log.info('memcachedError : {0}'.format(error.status))
                log.error("unable to push key : {0} to bucket : {1}".format(key, client.vbucketId))
            except :
                log.error("unable to push key : {0} to bucket : {1}".format(key, client.vbucketId))
            client.close()
            time.sleep(1)
        return memcached_ready