#this test will check the memory_left in the bucket and insert as many keys to fill up the memory
#it loops through all the servers in the list and fill the default bucket in each server
#cleanup will also delete these keys from the buckets
import os
import random
import unittest
import mc_bin_client
import socket
import zlib
import ctypes
import uuid
from membase.api.rest_client import RestConnection

class FillTheBucketTest(unittest.TestCase):
    keys = None
    clients = None
    ips = None

    def setUp(self):
        self.ips = self.extract_server_ips()
        #read the current memory usage
        self.clients = self.create_mc_bin_clients_for_ips(self.ips)
        #populate key
        testuuid = uuid.uuid4()
        self.keys = ["key_%s_%d" % (testuuid, i) for i in range(10000)]


    # test case to set 1000 keys
    def test_fill_bucket(self):
        for ip in self.ips:
            print 'rest connection : ' , ip
            rest = RestConnection(ip=ip,
                                  username='Administrator',
                                  password='password')
            info = rest.get_bucket('default')
            emptySpace = info.stats.ram - info.stats.memUsed
            packetSize = emptySpace / 10000
            packetSize = int(packetSize)
            print 'packetSize: ', packetSize
            print 'before key insertion : ', info.stats.memUsed
            # after every push print the memUsed
            print 'push 1000 new keys to memcached @ {0}'.format(ip)
            self.clients[ip].vbucketId = 0
            self.values = {}
            for key in self.keys:
                payload = self.generate_payload(key + '\0\r\n\0\0\n\r\0', packetSize)
                self.values[key] = payload
                flag = socket.htonl(ctypes.c_uint32(zlib.adler32(payload)).value)
                try:
                    (opaque, cas, data) = self.clients[ip].set(key, 0, flag, payload)
                except:
                    self.fail("unable to push key{0} to bucket{1}".format(key, self.client.vbucketId))
            #now verify each key
            for key in self.keys:
                flag, keyx, value = self.clients[ip].get(key)
                self.assertEquals(value,self.values[key],
                    msg = 'value retrieved different from value inserted for key {0}'.format(key))
            info = rest.get_bucket('default')
            print 'after key insertion : ', info.stats.memUsed

    def tearDown(self):
        #let's clean up the memcached
        for ip in self.ips:
            for key in self.keys:
                try:
                    self.clients[ip].delete(key=key)
                except mc_bin_client.MemcachedError:
                    self.fail('unable to delete key : {0} from memcached @ {1}'.format(key, ip))

            self.clients[ip].close()


    def generate_payload(self, pattern, size):
        return (pattern * (size / len(pattern))) + pattern[0:(size % len(pattern))]

    def create_mc_bin_clients_for_ips(self, ips):
        clients = {}
        for ip in ips:
            clients[ip] = mc_bin_client.MemcachedClient(ip, 11211)
        print clients
        return clients

    #move this to a common class
    def extract_server_ips(self):
        servers_string = os.getenv("SERVERS")
        servers = servers_string.split(" ")
        return [server[:server.index(':')] for server in servers]
