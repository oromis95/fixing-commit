import time
import os
import unittest
import socket
import zlib
import ctypes
import uuid
from TestInput import TestInputSingleton
import logger
import crc32
from mc_bin_client import MemcachedClient, MemcachedError
from membase.api.rest_client import RestConnection, RestHelper
from membase.helper.bucket_helper import BucketOperationHelper
from remote.remote_util import RemoteMachineShellConnection

log = logger.Logger.get_logger()

class FailoverNodesTests(unittest.TestCase):
    version = None
    ips = None
    keys = None
    clients = None
    bucket_name = None

    def setUp(self):
        self.input = TestInputSingleton.input
        self.assertTrue(self.input, msg="input parameters missing...")
        self.servers = self.input.servers
        #make sure the master node does not have any other node
        #loop through all nodes and remove those nodes left over
        #from previous test runs
        master = self.ips[0]
        rest = RestConnection(ip=master,
                              username='Administrator',
                              password='password')
        nodes = rest.node_statuses()
        allNodes = []
        toBeEjectedNodes = []
        for node in nodes:
            allNodes.append(node.id)
            if node.id.find(master) < 0 and node.id.find('127.0.0.1') < 0:
                toBeEjectedNodes.append(node.id)
                remote = RemoteMachineShellConnection(ip=node.ip,
                                                      username='root',
                                                      pkey_location=os.getenv("KEYFILE"))
                log.info('killing memcached on {0}'.format(node.ip))
                info = remote.extract_remote_info()
                remote.terminate_process(info, 'memcached')
                remote.terminate_process(info, 'moxi')

            #let's rebalance to remove all the nodes from the master
                #this is not the master , let's remove it
                #now load data into the main bucket
        log.info('sleep 10 seconds, enough time for memcached to restart')
        time.sleep(10)
        if toBeEjectedNodes:
            log.info("rebalancing all nodes")
            helper = RestHelper(rest)
            helper.remove_nodes(knownNodes=allNodes,
                               ejectedNodes=toBeEjectedNodes)
            log.info("invoking rebalance again just in case it didn't go through last time...")
            try:
                helper.remove_nodes(knownNodes=allNodes,
                                   ejectedNodes=toBeEjectedNodes)
            except :
                log.error("masking the error here...")

            #now we can create a default bucket on the master node ?

        BucketOperationHelper.delete_all_buckets_or_assert(self.ips, self)
        remote = RemoteMachineShellConnection(ip=master,
                                              username='root',
                                              pkey_location=os.getenv("KEYFILE"))
        log.info('killing memcached on {0}'.format(master))
        info = remote.extract_remote_info()
        remote.terminate_process(info, 'memcached')
        remote.terminate_process(info, 'moxi')

        log.info('sleep 10 seconds, enough time for memcached to restart')
        time.sleep(10)
        self.bucket_name = 'FailoverNodesTests-{0}'.format(uuid.uuid4())
        rest.create_bucket(bucket=self.bucket_name,
                               ramQuotaMB=200,
                               replicaNumber=1,
                               proxyPort=11220)
        msg = 'create_bucket succeeded but bucket "default" does not exist'
        self.assertTrue(BucketOperationHelper.wait_for_bucket_creation(self.bucket_name, rest), msg=msg)
        BucketOperationHelper.wait_till_memcached_is_ready_or_assert([master],11220,self)
        self._load_data(0.1)

    def _verify_data(self):
        #verify all the keys
        master = self.ips[0]
        client = MemcachedClient(master, 11220)
        #populate key

        index = 0
        all_verified = True
        keys_failed = []
        for key in self.keys:
            try:
                index += 1
                vbucketId = crc32.crc32_hash(key) & 1023 # or & 0x3FF
                client.vbucketId = vbucketId
                flag, keyx, value = client.get(key=key)
                actual_flag = socket.ntohl(flag)
                expected_flag = ctypes.c_uint32(zlib.adler32(value)).value
                self.assertEquals(actual_flag, expected_flag, msg='flags dont match')
                log.info("verified key #{0} : {1}".format(index,key))
            except MemcachedError as error:
                log.error(error)
                log.error("memcachedError : {0} - unable to get a pre-inserted key : {0}".format(error.status,key))
                keys_failed.append(key)
                all_verified = False
#            except :
#                log.error("unknown errors unable to get a pre-inserted key : {0}".format(key))
#                keys_failed.append(key)
#                all_verified = False

        client.close()
        self.assertTrue(all_verified,
                        'unable to verify #{0} keys'.format(len(keys_failed)))

    #let's try to fill up ram up to y percentage
    def _load_data(self,fill_ram_percentage = 10.0):
        if fill_ram_percentage <= 0.0:
            fill_ram_percentage = 5.0
        master = self.ips[0]
        client = MemcachedClient(master, 11220)
        #populate key
        rest = RestConnection(ip=master,
                              username='Administrator',
                              password='password')
        testuuid = uuid.uuid4()
        info = rest.get_bucket(self.bucket_name)
        emptySpace = info.stats.ram - info.stats.memUsed
        log.info('emptySpace : {0} fill_ram_percentage : {1}'.format(emptySpace, fill_ram_percentage))
        fill_space = (emptySpace * fill_ram_percentage)/100.0
        log.info("fill_space {0}".format(fill_space))
        # each packet can be 10 KB
        packetSize = int(10 *1024)
        number_of_buckets = int(fill_space) / packetSize
        log.info('packetSize: {0}'.format(packetSize))
        log.info('memory usage before key insertion : {0}'.format(info.stats.memUsed))
        log.info('inserting {0} new keys to memcached @ {0}'.format(number_of_buckets,master))
        self.keys = ["key_%s_%d" % (testuuid, i) for i in range(number_of_buckets)]
        for key in self.keys:
            vbucketId = crc32.crc32_hash(key) & 1023 # or & 0x3FF
            client.vbucketId = vbucketId
            payload = self.generate_payload(key + '\0\r\n\0\0\n\r\0',10 * 1024)
            flag = socket.htonl(ctypes.c_uint32(zlib.adler32(payload)).value)
            try:
                (opaque, cas, data) = client.set(key, 0, flag, payload)
            except MemcachedError as error:
                log.error(error)
                client.close()
                self.fail("unable to push key : {0} to bucket : {1}".format(key, client.vbucketId))
        client.close()

    def test_failover_0_1_percent_ram(self):
        self.add_rebalance_failover_remove(0.1)

    def test_failover_1_percent_ram(self):
        self.add_rebalance_failover_remove(1.0)

    def test_failover_5_percent_ram(self):
        self.add_rebalance_failover_remove(5.0)

    def test_failover_10_percent_ram(self):
        self.add_rebalance_failover_remove(10.0)

    def test_failover_50_percent_ram(self):
        self.add_rebalance_failover_remove(50.0)

    def test_failover_99_percent_ram(self):
        self.add_rebalance_failover_remove(99.0)

    def add_rebalance_failover_remove(self,load_percentage):
        #add nodes
        index = 0
        rest = None
        otpNodes = []
        master = ''
        for ip in self.ips:
            if index == 0:
                rest = RestConnection(ip=ip,
                                      username='Administrator',
                                      password='password')
                master = ip
                log.info('master ip : {0}'.format(master))
            else:
                log.info('adding node : {0} to the cluster'.format(ip))
                otpNode = rest.add_node(user='Administrator',
                                        password='password',
                                        remoteIp=ip)
                if otpNode:
                    log.info('added node : {0} to the cluster'.format(otpNode.id))
                    otpNodes.append(otpNode)
                remote = RemoteMachineShellConnection(ip=ip,
                                                      username='root',
                                                      pkey_location=os.getenv("KEYFILE"))
                log.info('killing memcached on {0}'.format(ip))
                info = remote.extract_remote_info()
                remote.terminate_process(info, 'memcached')
                remote.terminate_process(info, 'moxi')
                log.info('sleep 10 seconds, enough time for memcached to restart')
                time.sleep(10)


            index += 1
            #rebalance
        #create knownNodes
        otpNodeIds = ['ns_1@' + master]
        for otpNode in otpNodes:
            otpNodeIds.append(otpNode.id)
        rebalanceStarted = rest.rebalance(otpNodeIds)
        self.assertTrue(rebalanceStarted,
                        "unable to start rebalance on master node {0}".format(master))
        log.info('started rebalance operation on master node {0}'.format(master))
        rebalanceSucceeded = rest.monitorRebalance()
        self.assertTrue(rebalanceSucceeded,
                        "rebalance operation for nodes: {0} was not successful".format(otpNodeIds))
        log.info('rebalance operaton succeeded for nodes: {0}'.format(otpNodeIds))
        self._load_data(load_percentage)

        rest.node_statuses()
        for i in range(0,10):
            time.sleep(5)
            rest.node_statuses()
        # now we should failover the nodes
        #now remove the nodes
        #make sure its rebalanced and node statuses are healthy
        #for each node just call

        helper = RestHelper(rest)
        self.assertTrue(helper.is_cluster_healthy, "cluster status is not healthy")
        self.assertTrue(helper.is_cluster_rebalanced, "cluster is not balanced")

        allnodes = []
        allnodes.extend(otpNodeIds)
        otpNodeIds.remove('ns_1@' + master)
        # do we have the data in failover
        for otp in otpNodeIds:
            log.info('failing over {0}'.format(otp))
            rest.fail_over(otp)

        time.sleep(15)
        #now let's rebalance
        helper.remove_nodes(knownNodes=allnodes,
                           ejectedNodes=otpNodeIds)

        self._verify_data()

    def tearDown(self):
        pass
#        BucketOperationHelper.delete_all_buckets_or_assert(self.ips, self)


    def extract_server_ips(self):
        servers_string = os.getenv("SERVERS")
        servers = servers_string.split(" ")
        return [server[:server.index(':')] for server in servers]

    def generate_payload(self, pattern, size):
        return (pattern * (size / len(pattern))) + pattern[0:(size % len(pattern))]