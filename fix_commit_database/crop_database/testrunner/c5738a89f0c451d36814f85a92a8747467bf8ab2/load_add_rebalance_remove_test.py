from lib.membase_mgmt_curl import membase_curl_invoker
from lib.mc_test_cli import memcachetest_invoker
import time
class PyTest(object):
    def __int__(self):
        self.name = "add-remove-test"
        self.describe = "add a node to a cluster and remove it after a few seconds"
        self.config = None
        self.status = -1

    def run_test(self):
        self.status = 0
        serverString = self.config.servers[0]
        mainServer = serverString[:serverString.index(':')]
        username = self.config.user
        password = self.config.password
        #fail the test if rebalance fails ? , or any of the ejectNode rest calls fail
        #initialize memcachetest
        memcachetest = memcachetest_invoker(hostname= mainServer,
                                            iterations = 100000,
                                            items = 1000,
                                            item_size = 256,
                                            threads = 2)
        #step1
        memcachetest.start()
        #step2
        otpNodes = list()
        for server in self.config.servers:
            if server != serverString:
                ip = server[:server.index(':')]
                addNodeStatus , otpNode = membase_curl_invoker.controller_addNode(server_ip = mainServer,
                                                        server_username = username,
                                                        server_password = password,
                                                        server_port = 8091,
                                                        new_server_ip=ip)
                print 'addNode status : ' , addNodeStatus
                if not addNodeStatus:
                    self.status = 1
                otpNodes.append(otpNode)
        #step3
        memcachetest.wait()

        self.status = 0
        #step4
        rebalanceKnownNodes = list()
        rebalanceKnownNodes.extend(otpNodes)
        mainServerOtpString = 'ns_1@' + mainServer
        rebalanceKnownNodes.append(mainServerOtpString)
        rebalanceInvocationStatus = membase_curl_invoker.controller_rebalance(server_ip = mainServer,
                                                               server_username = username,
                                                               server_password = password,
                                                               server_port = 8091,
                                                               otpNodes = rebalanceKnownNodes)
        print 'rebalanceInvocationStatus : ' , rebalanceInvocationStatus

        rebalanceStatus = membase_curl_invoker.controller_monitorProgress(server_ip = mainServer,
                                                               server_username = username,
                                                               server_password = password,
                                                               server_port = 8091)

        print rebalanceStatus
        if rebalanceStatus == -1:
            self.status = 1

        # step 5 : make sure rebalance goes through fine
        #let's add validation later
        # step 6 : remove nodes
        for node in otpNodes:
            #ejectNode rest api doesn't return an error , so how do we verify whether the node is
            #removed or not!
            membase_curl_invoker.controller_ejectNode(server_ip = mainServer,
                                                               server_username = username,
                                                               server_password = password,
                                                               server_port = 8091,
                                                               otpNode = node)
        return

