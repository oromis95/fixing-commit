# we want a class where we set certain params
# and then start a thread which runs those commands and
# return when done , or we are able to stop it when necessary ?
# same as hitting CTRL+C
import subprocess
import time
from threading import Thread

def manage_process(self):
    if self.process is not None:
           # check every couple of seconds ?
        self.stdout = self.process.communicate()
        # nullify the process
        self.process = None

class memcachetest_invoker(object):

    # ip
    # number of threads
    # number of iterations
    # number of items
    # size of each packet

    #default items = 1000
    #default threads = 10
    #default iterations = 1000
    #path to memcachetest ?
    #should come from testrunner ?
    #now let's just hard code this path ?

    def __init__(self,hostname,threads = 10,iterations = 1000,items = 1000, item_size = -1):
        self.ip = hostname
        self.path = "/membase/memcachetest/memcachetest"
        if item_size > 0 :
            self.fixed_size = False
            self.item_size = item_size
        else:
            self.fixed_size = True
            self.item_size = -1
        self.threads = threads
        self.iterations = iterations
        self.items = items
        self.process = None
        self.thread = None
        self.stdout = ''

    #kick off the memcachetest process
    #calling start on an already started memcachetest will terminate the one
    #that is already running
    def start(self):
        
        if self.process is not None:
            self.stop()
        if self.fixed_size:
            cmd = '{0} -t{1} -c{2} -i{3} -h{4}' .format(self.path , self.threads , self.iterations, self.items, self.ip)
        else:
            cmd = '{0} -t{1} -c{2} -i{3} -F -M{4} -h{5}'.format(self.path , \
                  self.threads , self.iterations, self.items , self.item_size , self.ip)
        print "running {0} command " .format(cmd)
        popen_args = cmd.split(' ')
        self.process = subprocess.Popen(popen_args,
                                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.thread = Thread(target=manage_process,args=(self,))
        self.thread.start()

    def tail(self):
        return self.stdout

    def is_still_running(self):
        return self.process is not None

    def wait(self):
        if self.thread.isAlive:
            self.thread.join(timeout = 1000)
            if self.thread.isAlive:
                #if thread.join times out then force-stop the thread
                self.stop()

    #stop memcachetest execution by terminating the process
    def stop(self):
        if self.process is not None:
            return_code = self.process.poll()
            if return_code:
                print 'process is already terminated'
            else:
                try:
                    self.process.terminate()
                    print "terminated the process"
                except  OSError:
                    print 'raised OSError because process was already terminated'
            self.process = None
            if self.thread is not None:
                if self.thread.isAlive:
                    self.thread.join(timeout = 10)
                self.thread = None
            #this should also terminate the thread!

#usage example :
#test = MemcacheTestThread('10.1.5.176',threads = 10,iterations = 10000,item_size= 256,items = 1000)
#test.start()
#test.wait()
#print test.tail()
#./memcachtest -h 10.1.5.176 -t10 -i100 -v -c10