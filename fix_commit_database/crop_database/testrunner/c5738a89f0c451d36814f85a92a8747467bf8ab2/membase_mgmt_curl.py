import json
import time
import subprocess

__author__ = 'farshid'

#this class will have wrapper methods to invoke membase management
#apis using curl

class membase_curl_invoker(object):

#naming convention
# example /controller/addNode -> controller_addNode
# /controller/rebalance --> controller_rebalance
    @staticmethod
    def controller_addNode(server_ip,
                           server_username,
                           server_password,
                           server_port,
                           new_server_ip):
        otpNode = ''
        succeeded = True
        api = "controller/addNode"
        api_params = "-d user=%s -d password=%s -d hostname=%s" % (server_username, server_password, new_server_ip)
        authentication = "-u %s:%s" % (server_username, server_password)
        endpoint = "http://%s:%s/%s" % (server_ip, server_port, api)
        curl_cmd = "curl %s %s %s" % (authentication, api_params, endpoint)
        curl_cmd_args = curl_cmd.split(' ')
        print "executing ... ", curl_cmd
        process = subprocess.Popen(curl_cmd_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        curl_output = process.stdout
        result = ''
        for line in curl_output.readlines():
            result = result + line
        if result.lower().find('failed') >= 0:
            succeeded = False
        else:
            try:
                dict = json.loads(result)
                otpNode = dict['otpNode']
                #todo: which exceptions does json.loads raise
            except:
                succeeded = False
        print result
        return succeeded, otpNode

    @staticmethod
    def controller_ejectNode(server_ip,
                             server_username,
                             server_password,
                             server_port,
                             otpNode):
        api = "controller/ejectNode"
        api_params = "-d user=%s -d password=%s -d otpNode=%s" % (server_username, server_username, otpNode)
        authentication = "-u %s:%s" % (server_username, server_password)
        endpoint = "http://%s:%s/%s" % (server_ip, server_port, api)
        curl_cmd = "curl %s %s %s" % (authentication, api_params, endpoint)
        curl_cmd_args = curl_cmd.split(' ')
        print "executing ... ", curl_cmd
        process = subprocess.Popen(curl_cmd_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        process.communicate()
        #todo: right now this api returns an empty string if ejectNode succeeds
        #shouldn't it return some confirmation or status instead ?
        return True


    #monitor the progress until it reaches 100% or a failure ?
    @staticmethod
    def controller_monitorProgress(server_ip, server_username, server_password, server_port):
        start = time.time()
        progress = 0
        while progress is not -1 and progress is not 100:
            progress = membase_curl_invoker.controller_rebalanceProgress(server_ip,server_username,server_password,server_port)
            print progress
            #sleep for 2 seconds
            time.sleep(2)

        if progress == -1:
            return False
        else:
            duration = time.time() - start
            print 'rebalance progress took {0} seconds '.format(duration)
            return True

    @staticmethod
    def controller_rebalanceProgress(server_ip, server_username, server_password, server_port):
        start = time.time()
        percentage = -1
        api = "pools/default/rebalanceProgress"
        authentication = "-u %s:%s" % (server_username, server_password)
        endpoint = "http://%s:%s/%s" % (server_ip, server_port, api)
        curl_cmd = "curl %s %s" % (authentication, endpoint)
        curl_cmd_args = curl_cmd.split(' ')
        process = subprocess.Popen(curl_cmd_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        curl_output = process.stdout
        result = ''
        for line in curl_output.readlines():
            result = result + line
        print result
        #if status is none , is there an errorMessage
            #convoluted logic which figures out if the rebalance failed or suceeded
        dict = json.loads(result)
        if dict.has_key('status'):
            if dict.has_key('errorMessage'):
                print 'rebalance failed'
            else:
                status = dict['status']
                if status == 'running':
                    for key in dict:
                        if key.find('ns_1') >= 0:
                            ns_1_dictionary = dict[key]
                            percentage = ns_1_dictionary['progress'] * 100
                            print 'rebalabce percentage : {0} %' .format(percentage)
                            break
                else:
                    percentage = 100

        return percentage

    @staticmethod
    def controller_rebalance(server_ip,
                             server_username,
                             server_password,
                             server_port,
                             otpNodes):
        knownNodes = ''
        index = 0
        for node in otpNodes:
            if index == 0:
                knownNodes += node
            else:
                knownNodes += ',' + node
            index += 1

        succeeded = True
        api = "controller/rebalance"
        api_params = "-d knownNodes=%s" % knownNodes
        authentication = "-u %s:%s" % (server_username, server_password)
        endpoint = "http://%s:%s/%s" % (server_ip, server_port, api)
        curl_cmd = "curl %s %s %s" % (authentication, api_params, endpoint)
        curl_cmd_args = curl_cmd.split(' ')
        args = list()
        args.append('curl')
        args.append('-v')
        args.append(authentication)
        args.append(api_params);
        args.append(endpoint)
        print 'executing...' , args
        process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        curl_output = process.stdout
        result = ''
        for line in curl_output.readlines():
            result = result + line
        print result
        #todo: right now this api returns an empty string if ejectNode succeeds
        #shouldn't it return some confirmation or status instead ?
        #succeeded will always be true
        return True
