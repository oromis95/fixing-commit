
import sys

sys.path.append('.')
sys.path.append('lib')

import TestInput
import time
from membase.api.tapclients import TapDump, TapDumpOptions

if __name__ == "__main__":
    try:
        input = TestInput.TestInputParser.get_test_input(sys.argv)
        options = TapDumpOptions()
        options.keys = True
        options.values = False
        options.vbuckets = [0,1]
        dump = TapDump(input.servers[0],options)
        dump.start()

        #wait until 5 minutes
        start = time.time()
        last_callback = dump.last_callback
        while True:
            if time.time() - last_callback > 5:
                print "got last update more than 5 seconds ago"
                dump.aborted = True
                break
            else:
                last_callback = dump.last_callback
            time.sleep(1)


        list = dump.dump()
        for l in list:
            print l

    except Exception as ex:
        print ex
