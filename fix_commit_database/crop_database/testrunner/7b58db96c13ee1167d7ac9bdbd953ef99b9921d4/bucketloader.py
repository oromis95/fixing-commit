# this script will connect to the node in the given ini file and create "n" buckets
# and load data until program is terminated
# example python bucketloader.py -i node.ini -p buckets=10,
#set=true,get=true,threads,1
import sys
from threading import Thread
import TestInput
from memcached.helper.data_helper import MemcachedClientHelper

sys.path.append('.')
sys.path.append('lib')

from membase.api.rest_client import RestConnection

def create_buckets(server, count, prefix, quota):
    #check how many buckets already created in this cluster
    rest = RestConnection(server)
    buckets = rest.get_buckets()
    if len(buckets) < count:
        delta = count - len(buckets)
        info = rest.get_nodes_self()
        replica = 1
        #TODO: calculate the bucket_ram from available_ram / delta
        #verify that the given quota makes sense
        #how much ram is used by all buckets
        for i in range(0, delta):
            name = "{0}-{1}".format(prefix, i)
            rest.create_bucket(bucket=name,
                               ramQuotaMB=quota,
                               replicaNumber=replica,
                               proxyPort=info.moxi)
            print "created bucket {0}".format(name)


def load_buckets(server, name, get, threads, moxi):
    distro = {500: 0.5, 1024: 0.5}
    MemcachedClientHelper.load_bucket(server, name, -1, 10000000, distro, threads, -1, get, moxi)


if __name__ == "__main__":
    input = TestInput.TestInputParser.get_test_input(sys.argv)
    server = input.servers[0]
    params = input.test_params
    count = 0
    get = True
    moxi = False
    bucket_quota = 200
    run_load = False
    prefix = "membase-"
    threads = 6

    if "count" in params:
        count = int(params["count"])
    if "load" in params:
        run_load = params["load"]
    if "threads" in params:
        bucket_quota = params["threads"]
    if "bucket_quota" in params:
        bucket_quota = params["bucket_quota"]
    if "moxi" in params:
        get = params["moxi"]
    if "get" in params:
        get = params["get"]
    if "prefix" in params:
        prefix = params["prefix"]

    create_buckets(server, count, prefix, bucket_quota)
    if run_load:
        rest = RestConnection(server)
        buckets = rest.get_buckets()
        threads = []
        for bucket in buckets:
            t = Thread(target=load_buckets, args=(server, bucket.name, get, threads, moxi))
            t.start()
            threads.append(t)

        for t in threads:
            t.join()



