#!/usr/bin/env python
import exceptions

import os
import sys
import time
import getopt
import subprocess
import fcntl
import urllib2
sys.path.append("lib")
from TestInput import TestInputParser
from builds.build_query import BuildQuery

class Testcase(object):
    def __init__(self, name):
        self.name = name
        try:
            self.description = file("tests/" + name + "/info").next().strip()
        except Exception:
            self.description = name
        self.path = os.path.join('tests', name, 'run')
        if not os.path.exists(self.path):
            self.path += '.sh'
        if not os.path.exists(self.path):
            raise exceptions.Exception(self.path)
        self.status_text = "NOTRUN"
        self.status = -1
        self.log = ""
        self.time = 0

    def run(self,argv):
        os.environ["TESTNAME"] = self.name
        string = ''
        for x in argv[1:]:
            string += '{0} '.format(x)
        start_time = time.time()
        process = subprocess.Popen('%s %s'%(self.path,string), shell=True,
                                   stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        fcntl.fcntl(process.stdout.fileno(), fcntl.F_SETFL, os.O_NONBLOCK)
        fcntl.fcntl(process.stderr.fileno(), fcntl.F_SETFL, os.O_NONBLOCK)
        while process.poll() is None:
            try:
                stdoutdata = process.stdout.read()
            except Exception:
                stdoutdata = ""
            try:
                stderrdata = process.stderr.read()
            except Exception:
                stderrdata = ""
            if stdoutdata:
                print stdoutdata
                self.log += stdoutdata
            if stderrdata:
                print stderrdata
                self.log += stderrdata
        self.time = time.time() - start_time
        time.sleep(0.5)
        if not process.returncode:
            self.status = 0
            self.status_text = "PASS"
        else:
            self.status = 1
            self.status_text = "FAIL"
    def report(self):
        return "Starting test: " + self.description + "\n" + self.log
    def result(self):
        return "%s : %s" % (self.status_text, self.description)
    def __str__(self):
        return self.result()
    def __repr__(self):
        return self.result()

# store quick report
# store extended report
# allow logging to a file
class Report(object):
    pass

def usage(err=None):
    if err:
        print "Error: %s\n" % err
        r = 1
    else:
        r = 0

    print """\
Syntax: testrunner [options]

Options:
 -c <file>        Config file name (located in the conf subdirectory)
 -i <file>        Path to .ini file containing server information
 -s <server>      Hostname of server (multiple -s options add more servers)
 -t <test>        Test name (multiple -t options add more tests)
"""
    sys.exit(r)

def get_latest_builds():
    #parametrize the product name
    query = BuildQuery()
    builds,changes = query.get_all_builds()
    if config.version == "" or config.version == "latest":
        return query.sort_builds_by_version(builds)
    else:
        return [query.find_membase_build_with_version(builds,config.version)]

def get_changes(url):
    page = urllib2.urlopen(url)
    html = page.read()
    page.close()
    html = html.replace("\r","\n")
    changes = {}
    index = None
    for line in filter(None, html.split("\n")):
        if line[0] != "#":
            if line[0] == "-":
                index = line.split(" ", 2)[1]
                changes[index] = []
            else:
                changes[index].append(line)
    text = ""
    for section in [(k, v) for (k, v) in changes.items() if v]:
        text += "* " + section[0] + "\n"
        for line in section[1]:
            text += line + "\n"
#    else:
#        text = "unable to fetch CHANGES file\n"
    return text


class Config(object):
    def __init__(self):
        self.test_group = "quick"
        self.tests = []

def parse_args(argv):
    config = Config()

    try:
        (opts, args) = getopt.getopt(argv[1:],
                                     'h:t:c:v:s:i:p:', [])
    except IndexError:
        usage()
    except getopt.GetoptError, err:
        usage(err)

    for o, a in opts:
        if o == "-h":
            usage()
        elif o == "-c":
            for line in file("conf/" + a):
                config.tests.append(Testcase(line.strip()))
            config.test_group = a.replace(".conf","")
        elif o == "-t":
            config.tests.append(Testcase(a))
    test_input = TestInputParser.get_test_input(argv)
    if not test_input.servers:
        usage("no servers specified")
    if not config.tests:
        usage("no tests specified")
    return config,test_input


if __name__ == "__main__":
    config,test_input = parse_args(sys.argv)

    for test in config.tests:
        test.run(sys.argv)

    from xunit import XUnitTestSuite
    xunit = XUnitTestSuite()
    xunit.name = "testrunner-suite"
    xunit_loaded = True

    print "---- quick report ----"
    report = ""
    report += "---- quick report ----\n"
    num_passed = 0
    num_run = 0
    for test in config.tests:
        num_run += 1
        if not test.status:
            num_passed += 1
            if xunit_loaded:
                xunit.add_test(name=test.name,status = 'pass',time = test.time)
        else:
            if xunit_loaded:
                xunit.add_test(name=test.name,status = 'fail',time = test.time,
                               errorType = 'membase.error', errorMessage = test.log)
        print test.result()
        report += test.result() + "\n"
    report_xml_file = open('report.xml', 'w')
    report_xml_file.write(xunit.to_xml())
    report_xml_file.close()