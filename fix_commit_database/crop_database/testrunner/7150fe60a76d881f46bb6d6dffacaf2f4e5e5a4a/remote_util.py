import os
import uuid
import paramiko
from builds.build_query import MembaseBuild
import logger
import time

log = logger.Logger.get_logger()

class RemoteMachineInfo(object):
    def __init__(self):
        self.type = ''
        self.ip = ''
        self.distribution_type = ''
        self.architecture_type = ''
        self.distribution_version = ''
        self.deliverable_type = ''

class RemoteMachineProcess(object):
    def __init__(self):
        self.pid = ''
        self.name = ''

class RemoteMachineHelper(object):

    remote_shell = None

    def __init__(self,remote_shell):
        self.remote_shell = remote_shell

    def monitor_process(self,process_name,
                        duration_in_seconds=120):
        #monitor this process and return if it crashes
        end_time = time.time() + float(duration_in_seconds)
        last_reported_pid = None
        while time.time() < end_time:
            #get the process list
            process = self.is_process_running(process_name)
            if process:
                if not last_reported_pid:
                    last_reported_pid = process.pid
                elif not last_reported_pid == process.pid:
                    message = 'process {0} was restarted. old pid : {1} new pid : {2}'
                    log.info(message.format(process_name,last_reported_pid,process.pid))
                    return False
                    #check if its equal
            else:
                # we should have an option to wait for the process
                # to start during the timeout
                #process might have crashed
                log.info("process {0} is not running or it might have crashed!".format(process_name))
                return False
            time.sleep(1)
#            log.info('process {0} is running'.format(process_name))
        return True


    def is_process_running(self,process_name):
        processes = self.remote_shell.get_running_processes()
        for process in processes:
            if process.name == process_name:
                return process
        return None

class RemoteMachineShellConnection:
    _ssh_client = None

    def __init__(self,username = 'root',
                 pkey_location = '',
                 ip = ''):
        #let's create a connection
        self._ssh_client = paramiko.SSHClient()
        self.ip = ip
        self._ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        log.info('connecting to {0} with username : {1} pem key : {2}'.format(ip,username,pkey_location))
        self._ssh_client.connect(hostname = ip,
                                 username = username ,
                                 key_filename = pkey_location)

    def get_running_processes(self):
        #if its linux ,then parse each line
        #26989 ?        00:00:51 pdflush
        #ps -Ao pid,comm
        processes = []
        output,error = self.execute_command('ps -Ao pid,comm',debug=False)
        if output:
            for line in output:
                #split to words
                words = line.split(' ')
                if len(words) >= 2:
                    process = RemoteMachineProcess()
                    process.pid = words[0]
                    process.name = words[1]
                    processes.append(process)
        return processes

    def is_membase_installed(self):
        sftp = self._ssh_client.open_sftp()
        filenames = sftp.listdir('/opt/membase')
        for name in filenames:
            #if the name version is correct
            installed_files = sftp.listdir('/opt/membase{0}'.format(name))
            #check for maybe bin folder or sth
            for file in installed_files:
                print file
        return True
        #depending on the os_info
        #look for installation folder
        #or use rpm -? to figure out if its installed

    def download_binary(self,build):
        #try to push this build into
        #depending on the os
        #build.product has the full name
        #first remove the previous file if it exist ?
        #fix this :
        log.info('removing previous binaries')
        output, error = self.execute_command('rm -rf /tmp/*.{0}'.format(build.deliverable_type))
        self.log_command_output(output, error)
        output, error = self.execute_command('cd /tmp;wget -q {0};'.format(build.url))
        self.log_command_output(output, error)
        #check if the file exists there now ?
        return self.file_exists('/tmp',build.name)
        #for linux environment we can just
        #figure out what version , check if /tmp/ has the
        #binary and then return True if binary is installed

    #check if this file exists in the remote
    #machine or not
    def file_exists(self,remotepath,filename):
        sftp = self._ssh_client.open_sftp()
        filenames = sftp.listdir(remotepath)
        for name in filenames:
            if name == filename:
                sftp.close()
                return True
        sftp.close()
        return False

    def membase_install(self,build):
        #install membase server ?
        #run the right command
        info = self.extract_remote_info()
        log.info('deliverable_type : {0}'.format(info.deliverable_type))
        info = self.extract_remote_info()
        if info.deliverable_type == 'rpm':
            #run rpm -i to install
            log.info('/tmp/{0} or /tmp/{1}'.format(build.name,build.product))
            output,error = self.execute_command('rpm -i /tmp/{0}'.format(build.name))
            self.log_command_output(output, error)
        elif info.deliverable_type == 'deb':
            output,error = self.execute_command('dpkg -i /tmp/{0}'.format(build.name))
            self.log_command_output(output, error)
            #run dpkg -?
        #else if its windows then run the remote installer
    
    def membase_uninstall(self):
        info = self.extract_remote_info()
        log.info(info.distribution_type)
        if info.distribution_type.lower() == 'ubuntu':
            #first remove the package
            #then install membase
            #check if its installed
            #call installed
            kill_cmd = 'killall beam ; killall -9 memcached ; killall -9 vbucketmigrator ;'
            cleanup_cmd = 'rm -rf /var/opt/membase /opt/membase /etc/opt/membase /var/membase/data/* /opt/membase/var/lib/membase/*'
            uninstall_cmd = 'dpkg -r {0}'.format('membase-server')
            log.info('running {0}'.format(uninstall_cmd))
            self._ssh_client.exec_command(uninstall_cmd)
            log.info('running kill commands to force kill membase processes')
            log.info('running {0}'.format(kill_cmd))
            self._ssh_client.exec_command(kill_cmd)
            log.info('running rm command to remove /etc/membase and /etc/opt/membase')
            log.info('running {0}'.format(cleanup_cmd))
            self._ssh_client.exec_command(cleanup_cmd)
        elif info.distribution_type.lower() == 'red hat':
            #first remove the package
            #then install membase
            #check if its installed
            #call installed
            kill_cmd = 'killall beam ; killall -9 memcached ; killall -9 vbucketmigrator ;'
            cleanup_cmd = 'rm -rf /var/opt/membase /opt/membase /etc/opt/membase /var/membase/data/* /opt/membase/var/lib/membase/*'
            uninstall_cmd = 'rpm -e {0}'.format('membase-server')
            log.info('running rpm -e to remove membase-server')
            self._ssh_client.exec_command(uninstall_cmd)
            log.info('running kill commands to force kill membase processes')
            self._ssh_client.exec_command(kill_cmd)
            log.info('running rm command to remove /etc/membase and /etc/opt/membase')
            self._ssh_client.exec_command(cleanup_cmd)
        elif info.distribution_type.lower() == 'centos':
            #first remove the package
            #then install membase
            #check if its installed
            #call installed
            kill_cmd = 'killall beam ; killall -9 memcached ; killall -9 vbucketmigrator ;'
            cleanup_cmd = 'rm -rf /var/opt/membase /opt/membase /etc/opt/membase /var/membase/data/* /opt/membase/var/lib/membase/*'
            uninstall_cmd = 'rpm -e {0}'.format('membase-server')
            log.info('running rpm -e to remove membase-server')
            output,error  = self.execute_command(uninstall_cmd)
            self.log_command_output(output,error)
            log.info('running kill commands to force kill membase processes')
            output,error = self.execute_command(kill_cmd)
            self.log_command_output(output,error)
            log.info('running rm command to remove /etc/membase and /etc/opt/membase')
            output,error = self.execute_command(cleanup_cmd)
            self.log_command_output(output,error)

    def log_command_output(self,output,error):
        for line in error:
            log.error(line)
        for line in output:
            log.info(line)


    def execute_command(self,command,debug=True):
        if debug:
            log.info("running command  {0}".format(command))
        stdin,stdout,stderro  = self._ssh_client.exec_command(command)
        stdin.close()
        output = []
        error = []
        for line in stdout.read().splitlines():
            output.append(line)
        for line in stderro.read().splitlines():
            error.append(line)
        if debug:
            log.info('command executed successfully')
        stdout.close()
        stderro.close()
        return output,error


    def disconnect(self):
        self._ssh_client.close()

    def extract_remote_info(self):
        #use ssh to extract remote machine info
        #use sftp to if certain types exists or not
        sftp = self._ssh_client.open_sftp()
        filenames = sftp.listdir('/etc/')
        os_distro = ""
        os_version = ""
        is_linux_distro = False
        for name in filenames:
            if name == 'issue':
                #it's a linux_distro . let's downlaod this file
                #format Ubuntu 10.04 LTS \n \l
                filename = 'etc-issue-{0}'.format(uuid.uuid4())
                sftp.get(localpath = filename , remotepath = '/etc/issue')
                file = open(filename)
                etc_issue = ''
                #let's only read the first line
                for line in file.xreadlines():
                    etc_issue = line
                    break
                #strip all extra characters
                etc_issue = etc_issue.rstrip('\n').rstrip('\\l').rstrip('\\n')
                if etc_issue.lower().find('ubuntu') != -1:
                    os_distro = 'Ubuntu'
                    os_version = etc_issue
                    is_linux_distro = True
                elif etc_issue.lower().find('centos') != -1:
                    os_distro = 'CentOS'
                    os_version = etc_issue
                    is_linux_distro = True
                elif etc_issue.lower().find('red hat') != -1:
                    os_distro = 'Red Hat'
                    os_version = etc_issue
                    is_linux_distro = True
                file.close()
                 # now remove this file
                os.remove(filename)
                break
        if not is_linux_distro:
            log.error('unsupported')
            return 'unknown','unknown','unknown','unknown'
        else:
            #now run uname -m to get the architechtre type
            os_arch = ''
            stdin,stdout,stderro  = self._ssh_client.exec_command('uname -m')
            stdin.close()
            os_arch = ''
            for line in stdout.read().splitlines():
                os_arch += line
            # at this point we should know if its a linux or windows ditro
            ext = {'Ubuntu': 'deb', 'CentOS': 'rpm', 'Red Hat': 'rpm'}.get(os_distro, '')
            arch = {'i686': 'x86', 'i386': 'x86'}.get(os_arch, os_arch)
            info = RemoteMachineInfo()
            info.type = "Linux"
            info.distribution_type = os_distro
            info.architecture_type = arch
            info.ip = self.ip
            info.distribution_version = os_version
            info.deliverable_type = ext
            return info