#!/bin/bash

# To simulate a testrunner cluster install in your dev environment...
#
# -- in ns_server...
#     (cd test && make REST_PASSWORD=password prep) && ./cluster_run.sh
#
# -- in ns_server...
#     (cd test && make REST_PASSWORD=password all-cluster-init bucket-create) && sleep 12 && (echo -e "set a 0 0 1\r\n1\r" | nc 127.0.0.1 12001)
#
# Then, to run the test...
#
# -- in testrunner...
#     ./testrunner -b .. -s 127.0.0.1:9000:12001:12000 -s 127.0.0.1:9001:12003:12002 -t moxi-config-rebalance_MBT-14
#
ret=0

MASTER=$(echo $SERVERS | cut -d " " -f 1)
SLAVE=$(echo $SERVERS | cut -d " " -f 2)

MASTER_IP=$(echo $MASTER | cut -d ":" -f 1)
MASTER_REST=$(echo $MASTER | cut -d ":" -f 2)
MASTER_MOXI=$(echo $MASTER | cut -d ":" -f 3)
MASTER_MEMCACHED=$(echo $MASTER | cut -d ":" -f 4)

SLAVE_IP=$(echo $SLAVE | cut -d ":" -f 1)
SLAVE_REST=$(echo $SLAVE | cut -d ":" -f 2)
SLAVE_MOXI=$(echo $SLAVE | cut -d ":" -f 3)
SLAVE_MEMCACHED=$(echo $SLAVE | cut -d ":" -f 4)

echo "[$TESTNAME] Running MBT-14 - moxi receives config after rebalance with \"$MASTER $SLAVE\""

count=$(echo -e "stats proxy config\r" | nc $MASTER_IP $MASTER_MOXI | tee /tmp/MBT-14_config0.out | grep "vBucketMap" | wc -l)
if [[ $count -ne 1 ]] ; then
    echo "[$TESTNAME] Remote server moxi config not ready"
    ret=1
fi

count=$(echo incr some-key-not-there 1 | nc $MASTER_IP $MASTER_MOXI | grep "NOT_FOUND" | wc -l)
if [[ $count -ne 1 ]] ; then
    echo "[$TESTNAME] Remote server not installed/configured"
    ret=1
fi

echo "[$TESTNAME] Setting some keys into $MASTER_IP $MASTER_MOXI"
for i in {0..99} ; do
    count=$(echo -e "set $i 0 0 1\n1\r" | nc $MASTER_IP $MASTER_MOXI | grep "STORED" | wc -l)
    if [[ $count -ne 1 ]] ; then
        echo "[$TESTNAME] Could not store key $i"
        ret=1
    fi
done

echo "[$TESTNAME] Adding server $SLAVE to $MASTER"
echo "$MEMBASE_CLI rebalance -c $MASTER_IP:$MASTER_REST --server-add=$SLAVE_IP:$SLAVE_REST -u $REST_USER -p $REST_PASSWORD --server-add-username=$REST_USER --server-add-password=$REST_PASSWORD &> /dev/null"
$MEMBASE_CLI rebalance -c $MASTER_IP:$MASTER_REST --server-add=$SLAVE_IP:$SLAVE_REST -u $REST_USER -p $REST_PASSWORD --server-add-username=$REST_USER --server-add-password=$REST_PASSWORD &> /dev/null
count=$($MEMBASE_CLI server-list -c $MASTER_IP:$MASTER_REST -u $REST_USER -p $REST_PASSWORD 2> /dev/null | grep "healthy active" | wc -l)
if [[ $count -ne 2 ]] ; then
    echo "[$TESTNAME] Add server failed"
    ret=1
fi
status=$($MEMBASE_CLI server-list -c $MASTER_IP:$MASTER_REST -u $REST_USER -p $REST_PASSWORD -o json 2> /dev/null | sed -e 's/^.*"balanced":\(.*\),"rebalanceStatus.*$/\1/')
if [[ $status == "false" ]] ; then
    echo "[$TESTNAME] Cluster is not balanced after add"
    ret=1
fi

count=$(echo -e "stats proxy config\r" | nc $MASTER_IP $MASTER_MOXI | tee /tmp/MBT-14_config1.out | grep "vBucketMap" | wc -l)
if [[ $count -ne 1 ]] ; then
    echo "[$TESTNAME] Remote server moxi config not ready"
    ret=1
fi

count=$(diff /tmp/MBT-14_config0.out /tmp/MBT-14_config1.out | wc -l)
if [[ $count -eq 0 ]] ; then
    echo "[$TESTNAME] Moxi did not receive new config after add+rebalance"
    ret=1
fi

echo "[$TESTNAME] Same keys exist on $MASTER_IP $MASTER_MOXI"
for i in {0..99} ; do
    count=$(echo -e "add $i 0 0 1\nx\r" | nc $MASTER_IP $MASTER_MOXI | grep "NOT_STORED" | wc -l)
    if [[ $count -ne 1 ]] ; then
        echo "[$TESTNAME] Could not see key $i"
        ret=1
    fi
done

echo "[$TESTNAME] Removing server $SLAVE from $MASTER"
$MEMBASE_CLI rebalance -c $MASTER_IP:$MASTER_REST --server-remove=$SLAVE_IP:$SLAVE_REST -u $REST_USER -p $REST_PASSWORD &> /dev/null
count=$($MEMBASE_CLI server-list -c $MASTER_IP:$MASTER_REST -u $REST_USER -p $REST_PASSWORD 2> /dev/null | grep "healthy active" | wc -l)
if [[ $count -ne 1 ]] ; then
    echo "[$TESTNAME] Remove server failed"
    ret=1
fi
status=$($MEMBASE_CLI server-list -c $MASTER_IP:$MASTER_REST -u $REST_USER -p $REST_PASSWORD -o json 2> /dev/null| sed -e 's/^.*"balanced":\(.*\),"rebalanceStatus.*$/\1/')
if [[ $status == "false" ]] ; then
    echo "[$TESTNAME] Cluster is not balanced after remove"
    ret=1
fi

count=$(echo -e "stats proxy config\r" | nc $MASTER_IP $MASTER_MOXI | tee /tmp/MBT-14_config2.out | grep "vBucketMap" | wc -l)
if [[ $count -ne 1 ]] ; then
    echo "[$TESTNAME] Remote server moxi config not ready"
    ret=1
fi

count=$(diff /tmp/MBT-14_config0.out /tmp/MBT-14_config2.out | wc -l)
if [[ $count -ne 0 ]] ; then
    echo "[$TESTNAME] Moxi did not receive new (restored) config after remove+rebalance"
    ret=1
fi

echo "[$TESTNAME] Same keys exist on $MASTER_IP $MASTER_MOXI"
for i in {0..99} ; do
    count=$(echo -e "add $i 0 0 1\nx\r" | nc $MASTER_IP $MASTER_MOXI | grep "NOT_STORED" | wc -l)
    if [[ $count -ne 1 ]] ; then
        echo "[$TESTNAME] Could not see key $i"
        ret=1
    fi
done

exit $ret

