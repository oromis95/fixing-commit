import unittest
import os
import uuid
import logger
import time
from membase.api.rest_client import RestConnection
from membase.helper.bucket_helper import BucketOperationHelper


log = logger.Logger.get_logger()

class RecreateMembaseBuckets(unittest.TestCase):
    version = None
    ips = None

    #as part of the setup let's delete all the existing buckets
    def setUp(self):
        self.ips = self.extract_server_ips()
        self.delete_all_buckets_or_assert(self.ips)
        time.sleep(10)

    def tearDown(self):
        self.delete_all_buckets_or_assert(self.ips)
        pass

    #create bucket-load some keys-delete bucket-recreate bucket
    def test_recreate_default_on_11211(self):
        name = 'default'
        for ip in self.ips:
            rest = RestConnection(ip=ip,
                                  username='Administrator',
                                  password='password')
            rest.create_bucket(bucket=name,
                               ramQuotaMB=400,
                               replicaNumber=1,
                               proxyPort=11211)
            msg = 'create_bucket succeeded but bucket {0} does not exist'.format(name)
            self.assertTrue(BucketOperationHelper.wait_for_bucket_creation(name, rest), msg=msg)
            BucketOperationHelper.wait_till_memcached_is_ready_or_assert([ip],11211,self)
            inserted_keys = BucketOperationHelper.load_data_or_assert(ip,1,name,11211,self)
            self.assertTrue(inserted_keys,'unable to insert any key to memcached')
            self.assertTrue(BucketOperationHelper.verify_data(ip,inserted_keys,True,False,11211,self),
            msg='verified all the keys stored')
            #verify keys
            rest.delete_bucket(name)
            msg = 'bucket "{0}" was not deleted even after waiting for two minutes'.format(name)
            self.assertTrue(BucketOperationHelper.wait_for_bucket_deletion(name, rest, timeout_in_seconds=60), msg=msg)

            rest.create_bucket(bucket=name,
                               ramQuotaMB=400,
                               replicaNumber=1,
                               proxyPort=11211)
            msg = 'create_bucket succeeded but bucket {0} does not exist'.format(name)
            self.assertTrue(BucketOperationHelper.wait_for_bucket_creation(name, rest), msg=msg)
            BucketOperationHelper.wait_till_memcached_is_ready_or_assert([ip],11211,self)
            #now let's recreate the bucket
            log.info('recreated the default bucket...')
            #loop over the keys make sure they dont exist
            self.assertTrue(BucketOperationHelper.keys_dont_exist(inserted_keys,ip,11211,self),
                            msg='at least one key found in the bucket')

    def test_recreate_non_default_on_11220(self):
        name = 'recreate-non-default-{0}'.format(uuid.uuid4())
        for ip in self.ips:
            rest = RestConnection(ip=ip,
                                  username='Administrator',
                                  password='password')
            rest.create_bucket(bucket=name,
                               ramQuotaMB=400,
                               replicaNumber=1,
                               proxyPort=11220)
            msg = 'create_bucket succeeded but bucket {0} does not exist'.format(name)
            self.assertTrue(BucketOperationHelper.wait_for_bucket_creation(name, rest), msg=msg)
            BucketOperationHelper.wait_till_memcached_is_ready_or_assert([ip],11220,self)
            inserted_keys = BucketOperationHelper.load_data_or_assert(ip,1,name,11220,self)
            self.assertTrue(inserted_keys,'unable to insert any key to memcached')
            self.assertTrue(BucketOperationHelper.verify_data(ip,inserted_keys,True,False,11220,self),
            msg='verified all the keys stored')
            #verify keys
            rest.delete_bucket(name)
            msg = 'bucket "{0}" was not deleted even after waiting for two minutes'.format(name)
            self.assertTrue(BucketOperationHelper.wait_for_bucket_deletion(name, rest, timeout_in_seconds=60), msg=msg)

            rest.create_bucket(bucket=name,
                               ramQuotaMB=400,
                               replicaNumber=1,
                               proxyPort=11220)
            msg = 'create_bucket succeeded but bucket {0} does not exist'.format(name)
            self.assertTrue(BucketOperationHelper.wait_for_bucket_creation(name, rest), msg=msg)
            BucketOperationHelper.wait_till_memcached_is_ready_or_assert([ip],11220,self)
            #now let's recreate the bucket
            log.info('recreated the default bucket...')
            #loop over the keys make sure they dont exist
            self.assertTrue(BucketOperationHelper.keys_dont_exist(inserted_keys,ip,11220,self),
                            msg='at least one key found in the bucket')


    def test_recreate_non_default(self):
        name = 'new-bucket-{0}'.format(uuid.uuid4())
        for ip in self.ips:
            rest = RestConnection(ip=ip,
                                  username='Administrator',
                                  password='password')
            rest.create_bucket(bucket=name,
                               ramQuotaMB=400,
                               replicaNumber=1,
                               saslPassword='password',
                               proxyPort=11211)
            msg = 'create_bucket succeeded but bucket {0} does not exist'.format(name)
            self.assertTrue(BucketOperationHelper.wait_for_bucket_creation(name, rest), msg=msg)
            rest.delete_bucket(name)
            msg = 'bucket "{0}" was not deleted even after waiting for two minutes'.format(name)
            self.assertTrue(BucketOperationHelper.wait_for_bucket_deletion(name, rest, timeout_in_seconds=30), msg=msg)


    #move these methods to a helper class

    def delete_all_buckets_or_assert(self, ips):
        for ip in ips:
            rest = RestConnection(ip=ip,
                                  username='Administrator',
                                  password='password')
            buckets = rest.get_buckets()
            for bucket in buckets:
                print bucket.name
                rest.delete_bucket(bucket.name)
                log.info('deleted bucket : {0}'.format(bucket.name))
                msg = 'bucket "{0}" was not deleted even after waiting for two minutes'.format(bucket.name)
                self.assertTrue(BucketOperationHelper.wait_for_bucket_deletion(bucket.name, rest, 200)
                                , msg=msg)

    def extract_server_ips(self):
        servers_string = os.getenv("SERVERS")
        servers = servers_string.split(" ")
        return [server[:server.index(':')] for server in servers]

    #return keys loaded,value is also they key