try:
    # if couchdb isn't installed, then we will assert later when the user tries to attach to the database
    # a better way would be to check for couchdb and add it to a list of possible database types, then
    # parse the scheme from the URI and create the appropriate database connection
    import couchdb
except:
    pass

import urlparse
import uuid


# http://stackoverflow.com/questions/1417958/parse-custom-uris-with-urlparse-python
def register_scheme(scheme):
    for method in filter(lambda s: s.startswith('uses_'), dir(urlparse)):
        getattr(urlparse, method).append(scheme)
register_scheme('couchdb')

class TestDatabase(object):
    def __init__(self, server):
        parsed = urlparse.urlparse(server,"http")
        self.runid = uuid.uuid4()
        self.scheme = parsed.scheme
        self.username = parsed.username
        self.password = parsed.password
        self.hostname = parsed.hostname
        self.port = parsed.port
        self.database = parsed.path[1:]

    def add_testrun(self, testname, status, duration, error_type, error_message, servers, timestamp, parameters, logfile):
        couch = couchdb.Server("http://{0}:{1}@{2}:{3}".format(self.username,self.password,self.hostname,self.port))
        db = couch[self.database]

        log=file(logfile).read()

        doc = {
            "testname":testname,
            "runid":str(self.runid),
            "status":status,
            "duration":duration,
            "error_type":error_type,
            "error_message":error_message,
            "servers":servers,
            "timestamp":timestamp,
            "parameters":parameters,
            "type":"testrun",
        }

        doc_id, doc_rev = db.save(doc)
        db.put_attachment(db[doc_id], log, "log", "text/plain")
