#!/usr/bin/python
import sys
sys.path.append("lib")
sys.path.append("pytests")
import time
import getopt
import unittest
import os
import logger
from xunit import XUnitTestResult
from TestInput import TestInputParser, TestInputSingleton
from TestDatabase import TestDatabase

class Testcase(object):
    def __init__(self, name):
        self.name = name
        self.description = name
        self.log = ""

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


def usage(err=None):
    if err:
        print "Error: %s\n" % err
        r = 1
    else:
        r = 0

    print """\
Syntax: testrunner [options]

Options:
 -c <file>        Config file name (located in the conf subdirectory)
 -i <file>        Path to .ini file containing server information
 -t <test>        Test name (multiple -t options add more tests)
"""
    sys.exit(r)


def parse_args(argv):
    tests = []
    test_database = None

    try:
        (opts, args) = getopt.getopt(argv[1:],
                                     'h:t:c:v:i:p:d:', [])
        for o, a in opts:
            if o == "-h":
                usage()
            elif o == "-c":
                if find_runlist(a):
                    for line in find_runlist(a):
                        if not line.startswith("#"):
                            tests.append(Testcase(line.strip()))
            elif o == "-t":
                tests.append(Testcase(a))
            elif o == "-d":
                test_database = TestDatabase(a)
        test_input = TestInputParser.get_test_input(argv)
        if not test_input.servers:
            usage("no servers specified")
        if not tests:
            usage("no tests specified")
        return tests, test_input, test_database

    except IndexError:
        usage()
    except getopt.GetoptError, err:
        usage(err)

def find_runlist(filename):
    if os.path.exists(filename):
        return file(filename)
    if os.path.exists("conf/" + filename):
        return file("conf/" + filename)
    return None

if __name__ == "__main__":
    str_time = time.strftime("%H:%M:%S", time.localtime()).replace(":", "-")
    tests, test_input, test_database = parse_args(sys.argv)

    names = [test.name for test in tests]

    xunit = XUnitTestResult()

    tmp_folder = "tmp-{0}".format(str_time)
    os.makedirs(tmp_folder)
    #this only works on linux/mac
    os.environ["TEMP-FOLDER"] = os.getcwd() + "/" + tmp_folder
    print os.environ["TEMP-FOLDER"]

    for name in names:
        #let's create temporary folder for logs and xml results
        start_time = time.time()
        dotnames = name.split('.')
        log_name = dotnames[len(dotnames) - 1]
        logger.Logger.start_logger(log_name)
        TestInputSingleton.input = TestInputParser.get_test_input(sys.argv)
        suite = unittest.TestLoader().loadTestsFromName(name)
        result = unittest.TextTestRunner(verbosity=2).run(suite)
        logger.Logger.stop_logger()
        time_taken = time.time() - start_time
        if result.failures or result.errors:
            for failure in result.failures:
                test_case, failure_string = failure
                xunit.add_test(name=name, status='fail', time=time_taken,
                               errorType='membase.error', errorMessage=failure_string)
                if test_database:
                    try:
                        test_database.add_testrun(testname=name, status='fail', duration=time_taken,
                                              error_type='membase.error', error_message=failure_string,
                                              servers=[{"ip":server.ip,"port":server.port,"architecture":None,"os":None,"version":None} for server in test_input.servers],
                                              timestamp=start_time, parameters=test_input.test_params, logfile=os.path.join(tmp_folder,log_name+'.log'))
                    except Exception, e:
                        print "unable to store results in database"
                        print e
                break
            for error in result.errors:
                test_case, error_string = error
                xunit.add_test(name=name, status='fail', time=time_taken,
                               errorType='membase.error', errorMessage=error_string)
                if test_database:
                    try:
                        test_database.add_testrun(testname=name, status='fail', duration=time_taken,
                                              error_type='membase.error', error_message=error_string,
                                              servers=[{"ip":server.ip,"port":server.port,"architecture":None,"os":None,"version":None} for server in test_input.servers],
                                              timestamp=start_time, parameters=test_input.test_params, logfile=os.path.join(tmp_folder,log_name+'.log'))
                    except Exception, e:
                        print "unable to store results in database"
                        print e
                break
        else:
            xunit.add_test(name=name, time=time_taken)
            if test_database:
                try:
                    test_database.add_testrun(testname=name, status='pass', duration=time_taken,
                                          error_type=None, error_message=None,
                                          servers=[{"ip":server.ip,"port":server.port,"architecture":None,"os":None,"version":None} for server in test_input.servers],
                                          timestamp=start_time, parameters=test_input.test_params, logfile=os.path.join(tmp_folder,log_name+'.log'))
                except Exception, e:
                    print "unable to store results in database"
                    print e
        xunit.write("{0}/report-{1}.xml".format(tmp_folder, str_time))
        xunit.print_summary()
        print "logs and results are available under {0}".format(tmp_folder)
