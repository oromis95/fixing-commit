#!/usr/bin/env python

import sys

sys.path.append('management')

import os
import time
import random
import shutil
import socket
import struct
import tempfile
import unittest

import mc_bin_client
import memcacheConstants
import util

try:
    import sqlite3
except:
    sys.exit("%s requires python version 2.6 or greater" %
              (os.path.basename(sys.argv[0])))

MBI = "./management/mbbackup-incremental"

def listen(port=11310):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(('', port))
    s.listen(1)
    return s

class MockTap(mc_bin_client.MemcachedClient):
    """Wraps a server-side, accepted socket,
       so we can mock up tap server interactions."""

    def __init__(self, testCase, accepted):
        self.t = testCase
        self.a = accepted
        self.s = accepted[0]
        self.r = random.Random()
        self.t.assertNotEqual(self.s, None)

    def readMsg(self):
        ext = ''
        key = ''
        val = ''
        cmd, opaque, cas, keylen, extlen, data = \
           self._handleKeyedResponse(None, raisesExceptions=False)
        status = self.status
        if data:
            ext = data[0:extlen]
            key = data[extlen:extlen+keylen]
            val = data[extlen+keylen:]
        return cmd, opaque, cas, status, key, ext, val

    def sendMsg(self, cmd, key, val, opaque, extraHeader='', cas=0, vbucketId=0):
        self.vbucketId = vbucketId
        self._sendCmd(cmd, key, val, opaque, extraHeader, cas)

    def sendStartCheckpoint(self, vbucketId, checkpointId):
        self.sendMsg(memcacheConstants.CMD_TAP_CHECKPOINT_START,
                     '', str(checkpointId), 0, vbucketId=vbucketId)

    def sendEndCheckpoint(self, vbucketId, checkpointId):
        self.sendMsg(memcacheConstants.CMD_TAP_CHECKPOINT_END,
                     '', str(checkpointId), 0, vbucketId=vbucketId)

    def sendMutation(self, vbucketId, key, val):
        self.sendMsg(memcacheConstants.CMD_TAP_MUTATION,
                     key, val, 0, vbucketId=vbucketId)

class TestIncrementalBackup(unittest.TestCase):

    def test_compute_next_file(self):
        d = tempfile.mkdtemp()
        self.assertEqual(d + "/file-00000000.mbb",
                         util.expand_file_pattern(d + "/file-#.mbb"))
        open(d + "/file-00000000.mbb", 'w').close()
        self.assertEqual(d + "/file-00000001.mbb",
                         util.expand_file_pattern(d + "/file-#.mbb"))
        open(d + "/file-00000001.mbb", 'w').close()
        self.assertEqual(d + "/file-00000002.mbb",
                         util.expand_file_pattern(d + "/file-#.mbb"))
        open(d + "/file-00000009.mbb", 'w').close()
        self.assertEqual(d + "/file-00000010.mbb",
                         util.expand_file_pattern(d + "/file-#.mbb"))
        self.assertEqual(d + "/diff-00000000.mbb",
                         util.expand_file_pattern(d + "/diff-#.mbb"))
        shutil.rmtree(d)

    def test_server_unconnectable(self):
        self.assertNotEqual(0,
                            os.system(MBI
                                      + " -h fake-server:6666 xxx"
                                      + " 2> /dev/null"))

    def prep(self):
        d = tempfile.mkdtemp()
        o = d + "/out"
        s = listen()
        os.system(MBI + " -v -v -v -T 0.1"
                      + " -h 127.0.0.1:11310"
                      + " -o " + d + "/backup.mbb"
                      + " client0"
                      + " >> " + o + " 2>&1 &")
        mt = self.expectClient(s)
        return mt, d, o, s

    def cleanup(self, mt, d, o, s, expectError=False):
        mt.close()
        time.sleep(0.2)
        lines = open(o).readlines()
        self.assertEqual(expectError,
                         "\n".join(lines).find("ERROR:") != -1)
        self.assertTrue(len(lines) > 0)
        self.assertTrue(os.path.exists(d + "/backup.mbb"))
        shutil.rmtree(d)

    def expectClient(self, s, clientName="client0"):
        mt = MockTap(self, s.accept())
        cmd, opaque, cas, status, key, ext, val = mt.readMsg()
        self.assertEqual(cmd, memcacheConstants.CMD_TAP_CONNECT)
        self.assertEqual(key, clientName)
        flags = struct.unpack(">I", ext)[0]
        self.assertEqual(flags,
                         memcacheConstants.TAP_FLAG_CHECKPOINT |
                         memcacheConstants.TAP_FLAG_SUPPORT_ACK |
                         memcacheConstants.TAP_FLAG_REGISTERED_CLIENT)
        return mt

    def expectAckCheckpoint(self, mt, vbucketId, checkpointId):
        cmd, opaque, cas, vid, key, ext, val = mt.readMsg()
        self.assertEqual(cmd, memcacheConstants.CMD_TAP_CHECKPOINT_ACK)
        self.assertEqual(val, str(checkpointId))
        self.assertEqual(vid, vbucketId)

    def test_basic_tap_connect(self):
        try:
            mt, d, o, s = self.prep()
            self.cleanup(mt, d, o, s)
        except Exception as e:
            print d
            print "\n".join(open(o).readlines())
            raise e
        finally:
            s.close()

    def test_good_checkpoint(self):
        self.test_good_checkpoints(n=1)

    def test_good_checkpoints(self, n=5, nMutations=1):
        try:
            mt, d, o, s = self.prep()
            for i in range(n):
                self.fullCheckpoint(mt, 0, "c" + str(i), nMutations)
            self.cleanup(mt, d, o, s)
        except Exception as e:
            print d
            print "\n".join(open(o).readlines())
            raise e
        finally:
            s.close()

    def fullCheckpoint(self, mt, vbucketId, checkpointId, nMutations=1):
        mt.sendStartCheckpoint(vbucketId, checkpointId)
        for i in range(nMutations):
            mt.sendMutation(vbucketId, "hello-" + str(i), "world")
        mt.sendEndCheckpoint(vbucketId, checkpointId)
        self.expectAckCheckpoint(mt, vbucketId, checkpointId)

    def test_checkpoints_with_mutations(self):
        self.test_good_checkpoints(n=5, nMutations=5)

    def test_unexpected_mutation(self):
        try:
            mt, d, o, s = self.prep()
            mt.sendMutation(0, "hello", "world")
            self.cleanup(mt, d, o, s, True)
        except Exception as e:
            print d
            print "\n".join(open(o).readlines())
            raise e
        finally:
            s.close()

    def test_checkpoint_start_svrcrash(self, n=0, nMutations=1):
        try:
            mt, d, o, s = self.prep()
            for i in range(n):
                self.fullCheckpoint(mt, 0, "c" + str(i), nMutations)
            mt.sendStartCheckpoint(0, "c" + str(n))
            self.cleanup(mt, d, o, s)
        except Exception as e:
            print d
            print "\n".join(open(o).readlines())
            raise e
        finally:
            s.close()

    def test_checkpoint_start_end_svrcrash(self):
        try:
            mt, d, o, s = self.prep()
            mt.sendStartCheckpoint(0, "c0")
            mt.sendEndCheckpoint(0, "c0")
            self.cleanup(mt, d, o, s, True)
        except Exception as e:
            print d
            print "\n".join(open(o).readlines())
            raise e
        finally:
            s.close()

    def test_checkpoint_start_end_ack_start_svrcrash(self):
        self.test_checkpoint_start_svrcrash(n=3, nMutations=1)

    def test_repeated_checkpoint_start_received(self):
        try:
            mt, d, o, s = self.prep()
            mt.sendStartCheckpoint(0, "c0")
            mt.sendStartCheckpoint(0, "c1")
            self.cleanup(mt, d, o, s, True)
        except Exception as e:
            print d
            print "\n".join(open(o).readlines())
            raise e
        finally:
            s.close()

    def test_interleaved_vbucket_messages(self):
        try:
            mt, d, o, s = self.prep()
            mt.sendStartCheckpoint(0, "c0")
            mt.sendStartCheckpoint(1, "c0")
            mt.sendEndCheckpoint(0, "c0")
            mt.sendEndCheckpoint(1, "c0")
            self.expectAckCheckpoint(mt, 0, "c0")
            self.expectAckCheckpoint(mt, 1, "c0")
            self.cleanup(mt, d, o, s, False)
        except Exception as e:
            print d
            print "\n".join(open(o).readlines())
            raise e
        finally:
            s.close()

    def test_unexpected_sqlite_app_version_pragma(self):
        todo()
    def test_TAP_CONNECT_error_name_already_in_use(self):
        todo()
    def test_concurrent_file_access(self):
        todo()
    def test_out_of_disk_space(self):
        todo()
    def test_unknown_TAP_message_received(self):
        todo()
    def test_checkpoint_start_end_ack_close_restart(self):
        todo()
    def test_restart_clears_open_checkpoint(self):
        todo()
    def test_checkpoint_start_TAPACK_end_ack(self):
        todo()
    def test_checkpoint_start_end_TAPACK_ack(self):
        todo()
    def test_checkpoint_start_end_ack_TAPACK(self):
        todo()
    def test_TAP_messages(self):
        todo()
    def test_mutation_flags(self):
        todo()
    def test_mutation_expiry(self):
        todo()
    def test_mutation_cas(self):
        todo()
    def test_flush_all(self):
        todo()
    def test_timeout_on_inactivity(self):
        todo()
    def test_pre_1_7_compatibility(self):
        todo()

def todo():
    pass # Flip this to assert(false) later.

if __name__ == '__main__':
    unittest.main()
