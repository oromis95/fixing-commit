#ifndef COMMAND_IDS_H
#define COMMAND_IDS_H 1

#define CMD_STOP_PERSISTENCE  0x80
#define CMD_START_PERSISTENCE 0x81
#define CMD_SET_FLUSH_PARAM   0x82

#define CMD_START_REPLICATION 0x90
#define CMD_STOP_REPLICATION  0x91
#define CMD_SET_TAP_PARAM     0x92
#define CMD_EVICT_KEY         0x93
#define CMD_GET_LOCKED        0x94
#define CMD_UNLOCK_KEY        0x95

#define CMD_SYNC              0x96

/*
 * IDs for the events of the SYNC command.
 */
#define SYNC_PERSISTED_EVENT 1
#define SYNC_MODIFIED_EVENT  2
#define SYNC_DELETED_EVENT   3
#define SYNC_INVALID_KEY     4
#define SYNC_INVALID_CAS     5

#endif /* COMMAND_IDS_H */
