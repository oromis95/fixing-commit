=head1 NAME

mbbackup incremental - incrementally backup membase

=head1 SYNOPSIS

=over 2

B<mbbackup incremental> [options] clientname

B<mbbackup-incremental> [options] clientname

=back

=head1 DESCRIPTION

B<mbbackup-incremental> gives one the ability to create an incremental backup
from a checkpoint created and managed by the Membase server.  In 
conjunction with the output from B<mbbackup-incremental>, a Membase
server may be restored to a given consistent checkpoint.

It should be noted that a full backup and all points between are required 
in order to effect a restore. 

B<Warning:> creating a new backup and not updating it on a regular basis
will cause additional memory utilization on the server.  The server
will retain all previous values across checkpoints (defined by the server)
until the backup re-visits the server.  It is imperative that the backups
be updated.

=head1 OPTIONS

The following options are supported:

=over 4

=item -h hostname:port

Connect to the given host:port as the source to back up.

=item -o outputfile

Write the incremental backup to the specified output file.

Note that the client will substitute an incrementing integer on each
execution for any "%" character included in the backup file name.
For purposes of simple sorting, it will automatically pad 5 digits
in place of the %, starting with 00001.  This allows one to run 
B<mbbackup incremental> via cron or some other
job controller on a regular basis to create a set of incremental
backups.

B<Warning:> it is recommended, when moving old incremental
backups, to leave the most recent one.  This way, B<mbbackup>
B<incremental> will always increment the number in the filename
monotonically rather than have multiple backups with the same
name.

=item -T n

Terminate if no progress happens for n seconds.

=item -v

Increase the verbosity output.  Note that up to 3 v's may be used to 
increase verbosity.

=back

=head1 OPERANDS

clientname    The registered client name on the server

=head1 EXAMPLES

=over 4

B<Example 1> creating the first backup from a given server.

=over 2

# mbbackup incremental -f hourly_backup -o first_backup.mbb

=back

This example creates the first backup, registering the backup name
"hourly_backup" on the server.  If run again with the same arguments
later, it would fail, as the file already exists.

B<Example 2> creating the first, and subsequent backups.

=over 2

# mbbackup incremental -f hourly_backup -o got_my_back%.mbb

=back

This example creates the first backup, registering the backup name
"hourly_backup" on the server.  The first file created would be
"got_my_back0.mbb".  If run again with the same arguments later,
it would create a backup file with changes since the last 
incremental: "got_my_back1.mbb".

=back

=cut


