package com.couchbase.client.java.document.json;

public class JsonNull extends JsonValue {

    public static JsonNull INSTANCE = new JsonNull();

    private JsonNull() {}

}
