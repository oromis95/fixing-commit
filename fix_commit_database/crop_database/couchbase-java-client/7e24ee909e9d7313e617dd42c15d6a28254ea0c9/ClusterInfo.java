package com.couchbase.client.java.cluster;

import com.couchbase.client.java.document.json.JsonObject;

public interface ClusterInfo {

    JsonObject raw();

}
