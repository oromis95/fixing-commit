package com.couchbase.client.java.cluster;

/**
 * .
 *
 * @author Michael Nitschinger
 */
public enum ClusterBucketType {

    COUCHBASE,
    MEMCACHED
}
