package com.couchbase.client.java.env;

import com.couchbase.client.core.env.CoreEnvironment;

public interface ClusterEnvironment extends CoreEnvironment {
}
