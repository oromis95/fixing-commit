package com.couchbase.client.java.env;

import com.couchbase.client.core.env.CoreProperties;

/**
 * .
 *
 * @author Michael Nitschinger
 */
public interface ClusterProperties extends CoreProperties {
}
