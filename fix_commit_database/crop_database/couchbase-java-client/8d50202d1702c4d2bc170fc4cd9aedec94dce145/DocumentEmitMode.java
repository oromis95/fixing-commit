package com.couchbase.client.java.view;

public enum DocumentEmitMode {
    FLAT, CONCAT, CONCAT_EAGER
}
