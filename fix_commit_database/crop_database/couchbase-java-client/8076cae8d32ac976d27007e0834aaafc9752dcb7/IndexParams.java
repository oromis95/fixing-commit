package com.couchbase.client.java.search;

import com.couchbase.client.java.document.json.JsonObject;

/**
 * @author Sergey Avseyev
 */
public interface IndexParams {
    JsonObject json();
}
