package com.couchbase.client.java.bucket;

public enum BucketType {

    COUCHBASE,
    MEMCACHED

}
