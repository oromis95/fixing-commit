README
======

This plug-in requires the setup of an API Baseline. Setup can be performed via the Eclipse 
preferences in the "Plug-in Development-> API Baselines" section.

See http://wiki.eclipse.org/PDE/API_Tools/User_Guide for more infos.

