README
======

Plug-in requires the setup of an API Baseline which can be done in the Eclipse 
preferences in the Plug-in Development-> API Baselines section.

See http://wiki.eclipse.org/PDE/API_Tools/User_Guide for more info on the API Baseline

