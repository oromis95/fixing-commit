org.eclipse.ui.images
=====================

org.eclipse.ui.images provides the images for the Eclipse platform. These icons can be used in custom Eclipse plug-ins or in arbitrary rich client applications. The "eclipse-svg" folder contains svg version of the icons, while the other folders contain generated png files.

Generate png files
------------------

To generate the png files based on the svg files, see the README.md file in the org.eclipse.ui.images.renderer plug-in.

License
-------

[Eclipse Public License (EPL) v1.0][2]

[1]: http://wiki.eclipse.org/Platform_UI
[2]: http://wiki.eclipse.org/EPL
