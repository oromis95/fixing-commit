/*******************************************************************************
 * (c) Copyright 2013 l33t labs LLC and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     l33t labs LLC and others - initial contribution
 *******************************************************************************/

SVG Rendering Plugin Usage
-------------------------------------------

Install the renderer plugin (located in the bundles directory):

cd org.eclipse.ui.images.renderer
mvn clean install

After the renderer plugin is installed, change into the root of the images project:

cd org.eclipse.ui.images

Finally, execute the icon render mojo with:

mvn org.eclipse.ui:org.eclipse.ui.images.renderer:render-icons

This will render all of the svg icons in "eclipse-svg" into the root of the org.eclipse.ui.images project, maintaining the directory structure (i.e. eclipse-svg/icondir will be rendered into org.eclipse.ui.images/icondir).
