## IndexManager design

Each instance of IndexManager will be modeled as a state machine backed by a
data structure, called StateContext, that contains meta-data about the secondary
index, index topology, and meta-data for normal operation of IndexManager
cluster.

**relevant data structures**

```go
    type Timestamp []uint64 // timestamp vector for vbuckets

    type IndexInfo struct {
        Name       string    // Name of the index
        Uuid       uint64    // unique id for every index
        Using      IndexType // indexing algorithm
        OnExprList []string  // expression list
        Bucket     string    // bucket name
        IsPrimary  bool
        Exprtype   ExprType
        Active     bool     // whether index topology is created and index is ready
    }

    type StateContext struct {
        // value gets incremented after every updates.
        cas              uint64

        // list of projectors as `connectionAddr` that will accept
        // admin-messages.
        projectors       []string

        // list of projectors as `connectionAddr` that will accept
        // admin-messages.
        routers          []string

        // map of local-indexer-node id and its `connectionAddr`. Indexer node
        // will be identified from 0-250
        indexers         [byte]string             // indexed by indexer-id

        // Index info for all active indexes.
        indexeInfos      map[uint64]IndexInfo     // indexed by `indexid`

        // per index map of index topology
        indexesTopology  map[uint64]IndexTopology // indexed by `indexid`
    }
```

## topology

Topology of any given index consist of the following elements,

```go
    type IndexTopology struct {
        indexid       uint64

        // List of all local-indexer-nodes hosting this index.
        servers       []string

        // len(partitionMap) gives the number of partitions hosting the full
        // data-set of an index.
        //
        // local-indexer-node hosting a partition can be a master or replica,
        // first integer-value in each map will index to master-local-indexer in
        // `servers` field, and remaining elements will point to its active-replicas
        partitionMap  [][1+MAX_REPLICAS]int

        // given a key-version `partition-algorithm` calculates the slice-no
        // containing that key-version. `slice-no` index into this array and
        // pick a list partition hosting the slice. First element of this list
        // is master-partition id and remaining elements are for
        // replica-partitions id.
        sliceMap      [][1+MAX_REPLICAS]int

        // timestamp continuously updated by SYNC message.
        currentTimestamp    Timestamp

        // vector of vuuid corresponding to currentTimestamp and continuously
        // updated by SYNC message.
        currentVbuuid       []uint64

        // history of stabilityTimestamps
        stabilityTimestamps []Timestamp

        rollback            Rollback

        // number of re-balances active for this index
        rebalances          []Rebalance

        // wakeup structure for inactive upr connection. upr-reconnection,
        // projector crashes, kv-rollback will all get triggered via wakeup
        // structure.
        uprwakeup           Wakeup
    }
```

### failed, orphaned and outdated IndexManager

When an IndexManager crashes,
* it is restarted by ns-server, joins the cluster, subsequently it enters
  bootstrap state.
* becomes unreachable to ns-server, it shall restart itself by joining the
  cluster after a timeout period and enter bootstrap state.
* when an IndexManager is outdated with respect to a new Index-Coordinator it
  will update itself as part of bootstrap handshake.

## IndexManager bootstrap

When an IndexManager starts-up it will load its local StateContext from
persistent storage and enter into bootstrap. While in bootstrap it will wait
for a message about the new Index-Coordinator's `connectionAddr`
* if `connectionAddr` belongs to itself, it will assume the responsibility of
  Index-Coordinator.
* otherwise, it will wait for a handshake from the new Index-Coordinator.

## Index-Coordinator bootstrap

Once IndexManager assumes the responsibility of Index-Coordinator, it will
fetch the list of Index-Coordinator-Replicas from ns-server and start a
handshake with all the replica.

**bootstrap handshake with replica**, during this handshake Index-Coordinator
will get the CAS number of each replica's local StateContext and determine the
latest version of StateContext. If its local StateContext is not the latest
one, it will fetch the latest StateContext from one of its replica and publish
that to all of its other replicas.

**bootstrap handshake with local-indexer-node**, during this handshake
Index-Coordinator will get topology details from each of its
local-indexer-node and verify that with its StateContext. If there are any
changes it will update the topology and post new topology projectors, routers
and respective indexer nodes.

**currentTimestamp**, during the handshake with indexer-nodes, for each
IndexTopology, Index-Coordinator will get latest snapshot-timestamp from each
one of them as hw-timestamp and update its currentTimestamp by picking the
lowest of all sequence number.

Index-Coordinator finally moves to `active` state. Even after moving to
`active` state, whenever a new local indexer node registers with
Index-Coordinator it will go through bootstrap handshake for topology
verification.

## Index-Coordinator active state

Any update made to its StateContext will increment the CAS field and replicated
to Index-Coordinator replicas. If a modification is relevant for projectors
and/or routers and/or indexer-nodes post it to respective components.

* Accepts DDL changes from administrator and updates IndexInfo list.
* Accepts projector registration, calculates / re-calculates projector topology
  and spawn one or more projector instances.
* Accepts local-indexer-node registration and gather topology information for
  them. Another alternative is to get the list of local-indexer-nodes from
  ns-server.

Index-Coordinator periodically receives SYNC message and update its
`currentTimestamp` vector for each topology. If SYNC message for a vbucket does
not arrive for a prescribed period of time, it will submit the vbucket to
`stream-wakeup-routine`.

### stability timestamp

Index-Coordinator will maintain a currentTimestamp vector for each index
topology in its StateContext, which will be updated using SYNC message received
from projector/router. The sync message will contain the latest `sequence-number`
for the vbucket and its `vbuuid`.

* periodically `currentTimestamp` will be promoted to stability-timestamp
* newly promoted stability-timestamp will be added to `stabilityTimestamps`
  history in corresponding IndexTopology, and re-initialize IndexTopology's
  `currentTimestamp` and replicate StateContext.
* communicate `stabilityTimestamp` to all local-indexer-node hosting the
  indexes for that bucket.
* local-indexer-nodes will queue up incoming stability-timestamp and when its
  mutation queue aligns witl stabilityTimestamp it creates a snapshot.
* if Index-Coordinator fails in between, new Index-Coordinator can use
  `currentTimestamp` and `stabilityTimestamps` history to continue promoting
  the stabilityTimestamp with local-indexer-nodes.

The mutations in a snapshot must be smaller or equal to the new stability
timestamp, hence it is also called as snapshot-timestamp. As an optimization,
Index-Coordinator can consolidate stabilityTimestamp for all IndexTopologies
and publish them as single message to local-indexer-node.

If there a slow indexer-node that has not yet caught up with
stability-timestamps, it is possible that the indexer will no longer be able
to service consistent query.

Another alternative is,

Index-Coordinator will periodically recieve `<HWHeartbeat>` message from every
local-indexer-node. Based on HWHeartbeat metrics and/or query requirements,
Index-Coordinator will promote the currentTimestamp into a stability-timestamp
and publish it to all index-nodes hosting a index for that bucket.

The alternative  alternative will make sure that stability snapshots can
regularly taken even when Index-Coordinator is slow in receiving SYNC messages.

**algorithm to compute stability-timestamp based on hw-timestamp**

Algorithm takes following as inputs.

- per bucket HighWatermark-timestamp from each of the local-indexer-node.
- available free size in local-indexer's mutation-queue.

* For each vbucket, compute the mean seqNo
* Use the mean seqNo to create a stabilityTimestamp
* If heartbeat messages indicate that the faster indexer's mutation queue is
  growing rapidly, it is possible to use a seqNo that matches that fast indexer
  closer
* If the local indexer that has not sent heartbeat messages within a certain
  time, skip the local indexer, or consult the cluster mgr on the indexer
  availability.
* If the new stabilityTimestamp is the less than equal to the last one, do
  nothing.

```go
    type HWHeartbeat struct {
        bucket           string
        indexid          []uint64    // list of index hosted for `bucket`.
        hw               Timestamp
        lastPersistence  uint64      // hash of Timestamp
        lastStability    uint64      // hash of Timestamp
        mutationQueue    uint64
    }
```

### client interfacing with Index-Coordinator

* a client must first get current Index-Coordinator from ns-server. If it
  cannot get one, it must retry or fail.
* once network address of Index-Coordinator is obtained from ns-server, client
  can post update request to Index-Coordinator.

## Index-Coordinator replica

* when a node move to replica role, it will first fetch the local StateContext
  and wait for Index-Coordinator handshake.
* it will co-operate with Index-Coordinator to synchronize itself with latest
  StateContext.

## index rebalance

```go
    type Rebalance struct {
        slice_no           int // Slicen number undergoing rebalance
        srcPartition       int // from this partition
        dstPartition       int // to this partition
        // one of the stability-timestamp picked by Index-Coordinator
        rebalanceTimestamp Timestamp
        state              string // "start", "pending", "catchup", "done"
    }
```

Index-Coordinator to calculate re-balance strategy,
* by figuring out the slices (identified by slice-nos) to move from one
  partition to another.
* by identifying local-indexer-nodes hosting source and destination partition
  and use one of the stability-timestamp as rebalance-timestamp.
* construct a rebalance structure for each migrating slices and add them to
  index-topology structure and index-topology is published to local-index-nodes
  participating in rebalance.

Everytime IndexTopology is updated, it is broadcasted to all components.

Process of rebalance,
* Index-Coordinator instructs the index nodes to move the slices.
  * mark rebalance as "pending" state.
* local-indexer-node will scan the index data from the source node based on the
  rebalance timestamp and stream them across to destination node.
* once a slice have been streamed to their corresponding destination,
  destination node will intimate Index-Coordinator and request projector to
  open a catch-up connection. The catch-up connection is for bringing the pending
  slices up-to-date with the latest stability timestamp.
  * Index-Coordinator will mark this rebalance as "catchup" state.
* once destination node is caught-up with incremental index-stream it will
  activate the slice and post a request to Index-Coordinator.
* Index-Coordinator will update topology map and de-activate the slice in
  the source node.
  * mark rebalance as "done" state.
  * Removing the migrating slice's rebalance structure and
  * updating the slice's partition map and removing the slice's rebalance
    strucuture will be atomically done one after the other.
* once a slice is de-activiate, it can be removed from the local indexer.

### rebalance algorithm

TBD

## projector / router UPR re-connection

In cases of,
* projector loses connection with kv-node
* router looses connection with projector
* if either projector and/or router crashes

Mutations for vbucket will dry-up and subsequently KeyVersions from router
will also dry-up for one or more vbuckets. When Index-Coordinator does not
receive the SYNC message beyond a prescribed timeperiod it will notify
`stream-wakeup-routine`.

### stream wakeup routine

stream wakeup thread or routine will be started by Index-Coordinator during
bootstrap. It monitors vbucket stream activities via following structure

```go
    type Wakeup struct {
        // per bucket list of all vbuckets that has not received SYNC message.
        vbuckets map[string][MAX_VBUCKETS]uint16
    }
```

* during bootstrap, `Wakeup` struct will contain full list of vbuckets for
  every bucket (for which atleast one index is defined).
* once Index-Coordinator enters into `active` state, it will post
  UPR-connection request to all projectors for incremental-index build.
* when ever a SYNC message is seen by the stream-wakeup-thread it will remove
  bucket's vbucket from `Wakeup` struct
* periodically it will consolidate inactive list of vbucket and publish a
  restart request to projectors.

Index-Coordinator will calculate restart request based on,
* latest vbmap to check whether any kv rebalance has happened.
* failover-log for each bucket's vbucket to check whether kv-failover has
  happened.
  * in case of kv-failover, Index-Coordinator will move relevant IndexToplogies
    to `rollback` mode.
* optionally can get the hw-timestamp from local-indexer-nodes to calculate
  the restart sequence number.

Index-Coordinator will publish the restart request to all projectors.
* if Index-Coordinator could not reach a registered projector or router, it will
  consult with ns-server and remove them from StateContext.
* any new projectors / routers that are registering with Index-Coordinator
  will be included in known `projectors` list.

## kv-rollback

As part of stream-wakeup routine, Index-Coordinator will get the failoverlog
and check for vbucket branch histories. If it detects a branch history for a
vbucket it will move the IndexTopology for all indexes defined on that bucket
into `rollback` mode and replicates them to Index-Coordinator-Replicas.

Changes to IndexTopology will also be published to projectors and routers,
which will stop streaming UPR mutations for all the buckets. The reason we
freeze entire secondary-index system is because all buckets are hosted by all
kv-nodes.

Rollback context,
```go
    type Rollback struct {
        // Will be set by Index-Coordinator during kv-rollback.
        //   "started",  means an index is entering into rollback.
        //   "prepare",  means failover-timesamp is computed and servers hosting
        //               the index will be communicated.
        //   "restart",  means restart-timestamp is computed and nodes can
        //               rollback.
        //   "rollback", means local-indexer-nodes are commanded to rollback.
        rollback          string
        failoverTimestamp Timestamp
        restartTimestamp  Timestamp
    }
```


rollback sequence by Index-Coordinator for each bucket,
* fetch failover-log for every vbucket.
* poll for hw-timestamp from local-indexer-nodes hosting indexes for the
  bucket.
* calculate failover-timestamp using hw-timestamp and failover-log.
  * rollback context is set to "prepare" and replicated
  * failover-timestamp saved and replicated
* publish failover-timestamp to local-indexer-nodes
* local-indexer-nodes find a snapshot whose timestamp is smaller than the
  failover timestamp and return the snapshot timestamp back to
  Index-Coordinator.
  * if a snapshot timestamp cannot be found, then the local indexer returns a
    temporary-snapshot-timestamp.
* Index-Coordinator will gather snapshot-timestamps from local-indexer-nodes
  and compute restart-timestamp by finding the smallest sequence-no. for each
  vbucket based on all the snapshot timestamps.
  * rollback context is see to "restart" and replicated.
  * restart-timestamp is saved and replicated.
* Index-Coordinator shall synchronously communicate the restart-timestamp to
  local-indexer-nodes and command them to execute rollback to snapshot-timestamp
  that is greater than restart-timestamp but smaller than failover-timestamp.
  * rollback context is set to "rollback" and replicated.
* Optionally Index-Coordinator, if it finds a local-indexer's
  snapshot-timestamp way behind, can retire the node into catch-up mode and
  continue rollback with remaining indexer-nodes.

Calculating **failover-timestamp** using failover-logs and hw-timestamps.
* if vbucket has failed over, then use the failover log to look for a
  sequence-no.  for that vbucket.
* if vbucket has not failed over, then use the HW timestamp to look for a
  sequence-no. for that vbucket.

Calculating **temporary-snapshot-timestamps**,
* if vbucket has failed over, then set the sequence-no as 0.
* if vbucket has not failed over, then use HW-timestamp's sequence number.

## IndexManager APIs

### /cluster/heartbeat

To be called by ns-server. Node will respond back with SUCCESS irrespective of
role or state.

* if replica node does not respond back, it should be removed from active list.
* if master does not respond back, then ns-server can start a new
  master-election.

### /cluster/bootstrap

To be called by ns-server, ns-server is expected to make this request during
master election.

### /cluster/newmaster

**request:** <connectionAddr>

Once master election is completed ns-server will post the new master's
`connectionAddr` to the elected master and each of its new replica. After this,
one of the IndexManager will become Index-Coordinator and rest of them will
become Index-Coordinator's replicas



## Index-Coordinator APIs

**[TODO: Check whether all of this APU is idempotent.]**

### /coordinator/projector

When a new projector starts-up it will post its `connectionAddr` on this API.

**parameters**
- _"AddProjector"_, command name.
- _connectionAddr_, connection address to connect with the projector.

### /coordinator/local-indexer-node

When a new local-indexer-node starts-up it will post its `connectionAddr` on
this API.

**parameters**
- _"AddLocalIndexer"_, command name.
- _connectionAddr_, connection address to connect with local-indexer-node

### /coordinator/index

Create a new index specified by `Indexinfo` field in request body.
Index-Coordinator will initialize a topology for the index, co-ordinate with
local-indexer-nodes hosting the new index.

**parameters**
- _"CreateIndex"_, command name.
- _Indexinfo_, meta data about new index.

**response:**
- _Status_, status code for CREATE DDL
- _Indexinfo_, returns back index meta-data along with `indexid` that can
  uniquely identify the new index.

### /coordinator/index

Drop all index listed by `[]uint64` field.

**parameters**
- _"DropIndex"_, command name.
- _[]indexid_, list of uint64 number that uniquely identifies an index.

**response**
- Status, status code for DROP DDL

### /coordinator/index

List index-info structures identified by `[]uint64` field. If it is empty,
list all active indexes.

**parameters:**
- _"ListIndex"_, command name.
- _[]indexid_, list of uint64 number that uniquely identifies an index.

**response:**
- _[]IndexInfo_, list of index-meta data.

### /index-coordinator/hwtimestamp

Local-indexer-nodes can Periodically post its HW-timestamp to
Index-Coordinator as HWHeartbeat message.

### /index-coordinator/stabilityTimestamps

Index-Coordinator will return a last N timestamps that we promoted as
stability timestamps.



## ns-server API requirements:

### ns-server/indexCoordinator

Returns connection address for current master. If no master is currently elected
then return empty string.

### ns-server/indexCoordinatorReplicas

Returns list of connection address for all active replicas in the system.

### ns-server/indexerNodes

Returns list of connection address for all active local-indexer-nodes in the system.

### ns-server/join

For a node to join the cluster

### ns-server/leave

For a node to leave the cluster
