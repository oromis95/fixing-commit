package common

import "sync"

func NewByteBufferPool(size int) *BytesBufPool {
	newBufFn := func() interface{} {
		return make([]byte, 0, size)
	}

	return &BytesBufPool{
		pool: &sync.Pool{
			New: newBufFn,
		},
	}
}

type BytesBufPool struct {
	pool *sync.Pool
}

func (p *BytesBufPool) Get() []byte {

	return p.pool.Get().([]byte)
}

func (p *BytesBufPool) Put(buf []byte) {
	// Reset len = 0
	buf = buf[:0]
	p.pool.Put(buf)
}
