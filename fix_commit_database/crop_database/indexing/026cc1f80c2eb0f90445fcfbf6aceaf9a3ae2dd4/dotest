#!/bin/bash

echo '</pre><h3>Testing</h3><pre>'

if [ "$WORKSPACE" = "" ]; then
  echo 'WORKSPACE not set'
  exit 2
fi

if [ "$TS" = "" ]; then
  TS="adhoc"
fi

stop_cluster() {
  pkill -f cluster_
  pkill -f $WORKSPACE/install/bin
  pkill -f testrunner
  pkill -f /opt/couchbase
  killall beam.smp epmd memcached projector indexer gometa 1>/dev/null 2>&1
  sleep 10
  cd $WORKSPACE/ns_server
  make dataclean 1>/dev/null 2>&1
  sleep 10
}

collect_logs_cores() {
  gzip /tmp/core-* 2>&1 1>/dev/null
  tar cf $WORKSPACE/cores.tar /tmp/core-* 1>/dev/null 2>&1
  tar cf $WORKSPACE/logs.tar $WORKSPACE/ns_server/logs 1>/dev/null 2>&1
  tar uf $WORKSPACE/logs.tar $WORKSPACE/test.log 1>/dev/null 2>&1
  tar uf $WORKSPACE/logs.tar $WORKSPACE/run.log 1>/dev/null 2>&1
  tar uf $WORKSPACE/logs.tar /home/buildbot/bin/do* 1>/dev/null 2>&1
}

error_exit() {
  echo "</pre><h3>Testing Failed: $1</h3><pre>"
  stop_cluster
  collect_logs_cores
  exit 2
}

# Setup workspace
stop_cluster
rm -f $WORKSPACE/logs.tar $WORKSPACE/test.log $WORKSPACE/run.log /tmp/core-*
find ~/testdata/* -mtime +1 -exec rm {} \; 1>/dev/null 2>&1
warmed="never"
> $WORKSPACE/test.log

# Core dumps
export GOTRACEBACK=crash
ulimit -c unlimited
sudo bash -c "echo /tmp/core-%e.$TS > /proc/sys/kernel/core_pattern"

echo "</pre><h3>Functional tests</h3><pre>"
echo "Starting server"
cd $WORKSPACE/ns_server
./cluster_run -n2 1>$WORKSPACE/run.log 2>&1 &
disown
for i in {1..120}; do
  grep -qs 'Couchbase Server has started' logs/n_0/info.log && \
    grep -qs 'Couchbase Server has started' logs/n_1/info.log && \
      wget -qO- http://localhost:9000/ &>/dev/null &&
        wget -qO- http://localhost:9001/ &>/dev/null &&
          ok_run=1 && break
  sleep 3
done
test "$ok_run" || error_exit "Server startup failed"
sleep 30
./cluster_connect -n2 -s 3072 -T n0:kv,n1:kv+index >$WORKSPACE/run.log 2>&1 &
for i in {1..120}; do
  grep -qs 'Bucket "default" marked as warmed' logs/n_0/info.log && \
    grep -qs 'Bucket "default" marked as warmed' logs/n_1/info.log && \
      ok_connect=1 && break
  sleep 3
done
test "$ok_connect" || error_exit "Server connect failed"
warmed="`date`"
sleep 30

echo "Starting functional tests"
mkdir -p "$WORKSPACE/go" 
export GOROOT=/usr/local/go
export GOPATH=$WORKSPACE/go:$WORKSPACE/godeps:$WORKSPACE/goproj
cd $WORKSPACE/goproj/src/github.com/couchbase/indexing/secondary/tests
go get -t ./... 1>/dev/null 2>&1

# Go tests
cd $WORKSPACE/goproj/src/github.com/couchbase/indexing/secondary/tests/functionaltests
go test -timeout 30m -v -cbconfig ../config/build_validation.json | annotate-output +%T tee -a $WORKSPACE/test.log
cd $WORKSPACE/goproj/src/github.com/couchbase/indexing/secondary/tests/largedatatests
go test -timeout 2h -v -cbconfig ../config/build_validation.json | annotate-output +%T tee -a $WORKSPACE/test.log
stop_cluster

# Timing
echo "Test $TS" > $WORKSPACE/timing.log
sed 's/^--- PASS: \(.*\) (\(.*\) seconds)$/\1 \2/;tx;d;:x' $WORKSPACE/test.log | sort >> $WORKSPACE/timing.log

# Integration tests
echo "</pre><h3>Integration tests</h3><pre>"
cd $WORKSPACE/testrunner
(timeout 1h make test-2i-integrations-tests 2>&1) | tee -a $WORKSPACE/test.log
stop_cluster

# Verify results
cd $WORKSPACE/goproj/src/github.com/couchbase/indexing/secondary/tests/ci/
grep -- '--- FAIL:' $WORKSPACE/test.log > /tmp/fail.log
grep '^\s*2i' $WORKSPACE/test.log | grep 'fail\s*$' >> /tmp/fail.log
for tst in `cat skip.txt`; do
  echo "$tst" | grep -qs '^\s*#' && continue
  echo "$tst" | grep -qs '^\s*$' && continue
  grep -v "$tst" /tmp/fail.log > /tmp/fail-out.log
  mv /tmp/fail-out.log /tmp/fail.log
done
faillog="`cat /tmp/fail.log`"
if [ "$faillog" != "" ]; then error_exit "Integration test failed: $faillog"; fi

# Note versions
cd $WORKSPACE/goproj/src/github.com/couchbase/indexing
git rev-parse HEAD > ~/indexing.good
cd $WORKSPACE/goproj/src/github.com/couchbase/query
git rev-parse HEAD > ~/query.good
cd $WORKSPACE/goproj/src/github.com/couchbase/gometa
git rev-parse HEAD > ~/gometa.good
cd $WORKSPACE/godeps/src/github.com/couchbase/goforestdb
git rev-parse HEAD > ~/goforestdb.good
cd $WORKSPACE/forestdb
git rev-parse HEAD > ~/forestdb.good

echo "</pre><h3>Testing Succeeded</h3><pre>"
collect_logs_cores
exit 0

