// On the wire transport for mutation packet.
//  { uint32(packetlen), uint16(flags), []byte(mutation) }
//
// - `flags` used for specifying encoding format, compression etc.
// - packetlen == len(mutation)

package indexer

import (
	"encoding/binary"
	"fmt"
	c "github.com/couchbase/indexing/secondary/common"
	"log"
)

// error codes

var ErrorStreamUnexpected = fmt.Errorf("ErrorStreamUnexpected")
var ErrorStreamPacketRead = fmt.Errorf("ErrorStreamPacketRead")
var ErrorStreamPacketWrite = fmt.Errorf("ErrorStreamPacketWrite")
var ErrorStreamPacketOverflow = fmt.Errorf("ErrorStreamPacketOverflow")
var ErrorStreamdUnexpectedVbucket = fmt.Errorf("ErrorStreamdUnexpectedVbucket")
var ErrorStreamdCascadingFault = fmt.Errorf("ErrorStreamdCascadingFault")
var ErrorStreamdWorkerKilled = fmt.Errorf("ErrorStreamdWorkerKilled")
var ErrorStreamdExit = fmt.Errorf("ErrorStreamdExit")
var ErrorStreamcUnexpectedKeys = fmt.Errorf("ErrorStreamcUnexpectedKeys")
var ErrorStreamcEmptyKeys = fmt.Errorf("ErrorStreamcEmptyKeys")
var ErrorStreamcChannelClosed = fmt.Errorf("ErrorStreamcChannelClosed")
var ErrorEmptyMutation = fmt.Errorf("ErrorEmptyMutation")
var ErrorMissingPayload = fmt.Errorf("ErrorMissingPlayload")
var ErrorTransportVersion = fmt.Errorf("ErrorTransportVersion")

// packet field offset and size in bytes
const (
	pktLenOffset  int = 0
	pktLenSize    int = 4
	pktFlagOffset int = pktLenOffset + pktLenSize
	pktFlagSize   int = 2
	pktDataOffset int = pktFlagOffset + pktFlagSize
)

// StreamTransportPacket to send and receive mutation packets between router
// and indexer.
type StreamTransportPacket struct {
	flags StreamTransportFlag
	buf   []byte
}

type transporter interface { // facilitates unit testing
	Read(b []byte) (n int, err error)
	Write(b []byte) (n int, err error)
}

// NewStreamTransportPacket creates a new StreamTransportPacket and return its
// reference. Typically application should call this once and reuse it while
// sending or receiving a sequence of packets, so that same buffer can be
// reused.
//
// maxlen, maximum size of internal buffer used to marshal and unmarshal
//         packets.
// flags,  specifying encoding and compression.
func NewStreamTransportPacket(maxlen int, flags StreamTransportFlag) *StreamTransportPacket {
	return &StreamTransportPacket{
		flags: flags,
		buf:   make([]byte, maxlen),
	}
}

// Send payload to the other end using sufficient encoding and compression.
func (pkt *StreamTransportPacket) Send(conn transporter, payload interface{}) (err error) {
	var data []byte
	var n int

	// encoding
	if data, err = pkt.encode(payload); err != nil {
		return
	}
	// compression
	if data, err = pkt.compress(data); err != nil {
		return
	}
	// transport framing
	l := pktLenSize + pktFlagSize + len(data)
	if maxLen := c.MaxStreamDataLen; l > maxLen {
		log.Printf("packet length is greater than %v\n", maxLen)
		err = ErrorStreamPacketOverflow
		return
	}

	a, b := pktLenOffset, pktLenOffset+pktLenSize
	binary.BigEndian.PutUint32(pkt.buf[a:b], uint32(len(data)))
	a, b = pktFlagOffset, pktFlagOffset+pktFlagSize
	binary.BigEndian.PutUint16(pkt.buf[a:b], uint16(pkt.flags))
	if n, err = conn.Write(pkt.buf[:pktDataOffset]); err == nil {
		if n, err = conn.Write(data); err == nil && n != len(data) {
			log.Printf("stream packet wrote only %v bytes for data\n", n)
			err = ErrorStreamPacketWrite
		}
	} else if n != pktDataOffset {
		log.Printf("stream packet wrote only %v bytes for header\n", n)
		err = ErrorStreamPacketWrite
	}
	return
}

// Receive payload from remote, decode, decompress the payload and return the
// payload
func (pkt *StreamTransportPacket) Receive(conn transporter) (payload interface{}, err error) {
	var data []byte
	var n int

	// transport de-framing
	if n, err = conn.Read(pkt.buf[:pktDataOffset]); err != nil {
		return
	} else if n != pktDataOffset {
		log.Printf("read only %v bytes for packet header\n", n)
		err = ErrorStreamPacketRead
		return
	}
	a, b := pktLenOffset, pktLenOffset+pktLenSize
	pktlen := binary.BigEndian.Uint32(pkt.buf[a:b])
	a, b = pktFlagOffset, pktFlagOffset+pktFlagSize
	pkt.flags = StreamTransportFlag(binary.BigEndian.Uint16(pkt.buf[a:b]))
	if maxLen := uint32(c.MaxStreamDataLen); pktlen > maxLen {
		log.Printf("packet length is greater than %v\n", maxLen)
		err = ErrorStreamPacketOverflow
		return
	}
	if n, err = conn.Read(pkt.buf[:pktlen]); err != nil {
		return
	} else if n != int(pktlen) {
		log.Printf("read only %v bytes for packet data\n", n)
		err = ErrorStreamPacketRead
		return
	}
	data = pkt.buf[:pktlen]
	// de-compression
	if data, err = pkt.decompress(data); err != nil {
		return
	}
	// decoding
	if payload, err = pkt.decode(data); err != nil {
		return
	}
	return
}

// encode payload to array of bytes.
func (pkt *StreamTransportPacket) encode(payload interface{}) (data []byte, err error) {
	switch pkt.flags.GetEncoding() {
	case encodingProtobuf:
		data, err = protobufEncode(payload)
	}
	return
}

// decode array of bytes back to payload.
func (pkt *StreamTransportPacket) decode(data []byte) (payload interface{}, err error) {
	switch pkt.flags.GetEncoding() {
	case encodingProtobuf:
		payload, err = protobufDecode(data)
	}
	return
}

// compress array of bytes.
func (pkt *StreamTransportPacket) compress(big []byte) (small []byte, err error) {
	small = big
	switch pkt.flags.GetCompression() {
	}
	return
}

// decompress array of bytes.
func (pkt *StreamTransportPacket) decompress(small []byte) (big []byte, err error) {
	big = small
	switch pkt.flags.GetCompression() {
	}
	return
}
