// A gen server behavior for stream consumer.
//
// Daemon listens for new connections and spawns a reader routine
// for each new connection.
//
// concurrency model:
//
//                                   Application back channels,
//                               mutation channel and sideband channel
//                          -----------------------------------------------
//                                ^                      ^
//                             (sideband)            (mutation)
//     NewMutationStream() --*    |                      |
//             |             |    |                      |
//          (spawn)          |    |                      |
//             |             |    |                      |
//             |          (spawn) |                      |
//       streamListener()    |    |                      |
//                 |         |    |                      |
//  streamgCmdNewConnection  |    |                      |
//                 |         |    |                      |
//  Close() -------*------->gen-server()------*---- doReceiveKeyVersions()----*
//                                ^           |                               |
//                                |           *---- doReceiveKeyVersions()----*
//                streamgCmdVbmap |           |                               |
//            streamgCmdVbcontrol |           *---- doReceiveKeyVersions()----*
//          streamgCmdBufferCheck |                                           |
//                streamgCmdError |                                           |
//                                *-------------------------------------------*
//                                          (control & faults)
//
// server behavior:
//
// 1. can handle more than one connection from same router.
// 2. whenever a connection with router fails, all active connections with
//    that router will be closed and the same will be intimated to
//    application for catchup connection.
// 3. STREAM_END and STREAM_BEGIN transitions for a vbucket will be
//    automatically handled.
// 4. in case STREAM_END or STREAM_BEGIN is lost, it will be considered as
//    error case, will close all the connection with the corresponding
//    router(s) and the same will be intimated to application for catchup
//    connection.
// 5. side band information to application,
//    a. DropVbucketData, based on upstream notification
//    b. RestartVbuckets, when all connections with an upstream host is closed.
//    c. *protobuf.KeyVersions specifying either StreamBegin, StreamEnd for a
//       single vbucket.
//    d. ErrorStreamdCascadingFault, when there is a fault within fault.
//    e. error string, describing the cause of the error.
//    in case of d and e, all connections with all upstream host will be
//    closed and stream-instance will be shutdown.
// 6. when ever stream-instance shuts down it will signal application by
//    closing downstream's mutation channel and side-band channel.

package indexer

import (
	"fmt"
	c "github.com/couchbase/indexing/secondary/common"
	"github.com/couchbase/indexing/secondary/protobuf"
	"io"
	"log"
	"net"
	"time"
)

// Error messages
var ErrorStreamPayload = fmt.Errorf("ErrorStreamPayload")

// Side band information
type RestartVbuckets []uint64 // list of vbuckets back to application
type DropVbucketData []uint64 // list of vbuckets back to application
type VbStreamBegin uint64     // vbucket for which StreamBegin was received
type VbStreamEnd uint64       // vbucket for which StreamEnd was received

type workerBufferCheck byte

// gen-server commands
const (
	streamgCmdNewConnection byte = iota + 1
	streamgCmdVbmap
	streamgCmdVbcontrol
	streamgCmdBufferCheck
	streamgCmdError
	streamgCmdClose
)

// messages to gen-server
type streamServerMessage struct {
	cmd     byte          // gen server command
	raddr   string        // routine for remote connection, optional
	args    []interface{} // command arguments
	buffers map[uint64][]*protobuf.KeyVersions
	err     error // in case the message is to notify error
}

// maintain information about each remote connection.
type netConn struct {
	conn   net.Conn
	worker chan interface{}
	active bool
}

// maintain information about each active vbucket.
type activeVb struct {
	vbuuid uint64
	raddrs []string
}

// MutationStream handles an active stream of mutation for all vbuckets.
type MutationStream struct {
	laddr string // address to listen
	lis   net.Listener
	mutch chan<- []*protobuf.KeyVersions // backchannel to application
	sbch  chan<- interface{}             // carries

	// gen-server management
	reqch     chan []interface{}
	finch     chan bool
	conns     map[string]netConn   // resolve <host:port> to con. obj
	activeVbs map[uint64]*activeVb // map of vbno (vbuuid) to activeVb
}

// NewMutationStream creates a new mutation stream, which have an instance of
// concurrent model, described above, to handle live streaming of
// mutations from mutiple remote nodes.
//
// Failure and corresponding error messages are due to tcp connection.
func NewMutationStream(
	laddr string,
	mutch chan []*protobuf.KeyVersions,
	sbch chan<- interface{}) (s *MutationStream, err error) {

	s = &MutationStream{
		laddr: laddr,
		mutch: mutch,
		sbch:  sbch,

		// Managing vbuckets and connections for all routers
		reqch:     make(chan []interface{}, c.GenserverChannelSize),
		finch:     make(chan bool),
		conns:     make(map[string]netConn),
		activeVbs: make(map[uint64]*activeVb),
	}
	if s.lis, err = net.Listen("tcp", laddr); err != nil {
		return nil, err
	}
	go streamListener(laddr, s.lis, s.reqch) // spawn daemon
	go s.genServer(s.reqch)                  // spawn gen-server
	return s, nil
}

// Close the daemon listening for new connections and shuts down all read
// routines for the stream. Call does not return until shutdown is compeleted.
func (s *MutationStream) Close() (err error) {
	respch := make(chan []interface{}, 1)
	cmd := []interface{}{streamServerMessage{cmd: streamgCmdClose}, respch}
	resp, err := c.FailsafeOp(s.reqch, respch, cmd, s.finch)
	if err != nil {
		return err
	} else if resp[0] == nil {
		return nil
	}
	return resp[0].(error)
}

// gen server routine for stream server.
func (s *MutationStream) genServer(reqch chan []interface{}) {
	var appmsg interface{}

	defer func() {
		if r := recover(); r != nil {
			msg := streamServerMessage{cmd: streamgCmdClose}
			s.handleClose(msg)
			log.Printf("genServer for %q has paniced\n", s.laddr)
		}
	}()

loop:
	for {
		appmsg = nil
		select {
		case cmd := <-reqch:
			msg := cmd[0].(streamServerMessage)
			// handle message to gen-server
			switch msg.cmd {
			case streamgCmdNewConnection:
				appmsg = s.handleNewConnection(msg)

			case streamgCmdVbmap:
				appmsg = s.handleVbmap(msg)

			case streamgCmdVbcontrol:
				appmsg = s.handleVbcontrol(msg)
				for _, nc := range s.conns {
					nc.worker <- workerBufferCheck(0)
				}

			case streamgCmdBufferCheck:
				s.startWorker(msg.raddr, s.flushBuffers(msg.raddr, msg.buffers))

			case streamgCmdClose:
				respch := cmd[1].(chan []interface{})
				appmsg = s.handleClose(msg)
				respch <- []interface{}{nil}
				break loop

			case streamgCmdError:
				appmsg = s.jumboErrorHandler(msg.raddr, msg.err)

			default:
				panic("invalid genserver command!") // should never happen
			}
			if appmsg != nil {
				s.sbch <- appmsg
				log.Printf("appmsg: %T:%+v\n", appmsg, appmsg)
			}
		}
	}
}

/**** gen-server handlers ****/

// handle new connection
func (s *MutationStream) handleNewConnection(msg streamServerMessage) interface{} {
	conn := msg.args[0].(net.Conn)
	worker := make(chan interface{}, 10)
	if err := s.addConnection(conn, worker); err != nil {
		conn.Close()
		close(worker)
		return s.jumboErrorHandler(msg.raddr, err)
	}
	go doReceiveVbmap(conn, s.reqch)
	return nil
}

func (s *MutationStream) handleVbmap(msg streamServerMessage) interface{} {
	buffers := make(map[uint64][]*protobuf.KeyVersions)
	vbmap := msg.args[0].(*protobuf.VbConnectionMap)
	for _, vbuuid := range vbmap.GetVbuuids() {
		if err := addVbucket(msg.raddr, vbuuid, s.activeVbs); err != nil {
			return err
		}
		buffers[vbuuid] = nil
	}
	s.startWorker(msg.raddr, buffers)
	return nil
}

// handle control messages for vbucket, try restart the worker.
func (s *MutationStream) handleVbcontrol(msg streamServerMessage) interface{} {
	var err error
	var appmsg interface{}

	if _, ok := s.conns[msg.raddr]; ok == false {
		panic(fmt.Errorf("unknown connection %q\n", msg.raddr))
	}

	buffers := msg.buffers
	kvs := msg.args[1].([]*protobuf.KeyVersions)
	if len(kvs) > 1 {
		panic("cannot handle more than one control message\n")
	}
	kv := kvs[0]
	vbuuid := kv.GetVbuuid()
	switch byte(kv.GetCommand()) {
	case c.DropData:
		appmsg = DropVbucketData([]uint64{vbuuid})

	case c.StreamBegin:
		err = addVbucket(msg.raddr, vbuuid, s.activeVbs)
		buffers = s.flushBuffers(msg.raddr, buffers)
		appmsg = VbStreamBegin(vbuuid)

	case c.StreamEnd:
		err = delVbucket(msg.raddr, vbuuid, s.activeVbs)
		buffers = s.flushBuffers(msg.raddr, buffers)
		appmsg = VbStreamEnd(vbuuid)
	}

	if err != nil {
		return s.jumboErrorHandler(msg.raddr, err)
	}
	s.startWorker(msg.raddr, buffers)
	return appmsg
}

// shutdown this gen server and all its routines.
func (s *MutationStream) handleClose(msg streamServerMessage) interface{} {
	log.Printf("stream server %q shutting down\n", s.laddr)
	s.lis.Close() // close listener daemon
	s.lis = nil
	s.closeConnections() // close workers
	close(s.finch)
	return nil
}

// start a connection worker to read mutation message for a subset of vbuckets.
func (s *MutationStream) startWorker(raddr string, buffers map[uint64][]*protobuf.KeyVersions) {
	log.Printf("starting worker for connection %q\n", raddr)
	nc := s.conns[raddr]
	go doReceiveKeyVersions(nc, s.mutch, s.reqch, buffers)
	nc.active = true
}

// manage connection faults and out of order StreamBegin, StreamEnd for
// migrating vbuckets.
func (s *MutationStream) flushBuffers(
	raddr string,
	buffers map[uint64][]*protobuf.KeyVersions) map[uint64][]*protobuf.KeyVersions {

	for vbuuid, avb := range s.activeVbs {
		if c.HasString(raddr, avb.raddrs) { // vbucket mapped to this connection
			if avb.raddrs[0] == raddr {
				if kvs, ok := buffers[vbuuid]; ok && kvs != nil && len(kvs) > 0 {
					s.mutch <- kvs // flush buffered mutations dowstream
				}
				buffers[vbuuid] = nil // make this connection active for vbucket

				// IMPORTANT: We don't expect out of order StreamBegin and
				// StreamEnd.
				//
				//} else if _, ok := buffers[vbuuid]; ok == false {
				// out of order situation, buffer mutations and flush later.
				// buffers[vbuuid] = make([]*protobuf.KeyVersions, 0)
			}

		} else { // vbucket not mapped to this connection
			if kvs, ok := buffers[vbuuid]; ok && kvs != nil && len(kvs) > 0 {
				err := fmt.Errorf(
					"buffered mutations for vbucket `%v` has begun and ending",
					vbuuid)
				panic(err)
			}
			delete(buffers, vbuuid)
		}
	}
	return buffers
}

// jumbo size error handler, it either closes all connections and shutdown the
// server or it closes all open connections with faulting remote-host and
// returns back a message for application.
func (s *MutationStream) jumboErrorHandler(raddr string, err error) (msg interface{}) {
	var whatJumbo string

	// connection is already gone.
	if _, ok := s.conns[raddr]; ok == false {
		log.Printf("stream %q says, remote %q already gone\n", s.laddr, raddr)
		return nil
	}

	if err == io.EOF {
		log.Printf("stream %q says, remote %q closed\n", s.laddr, raddr)
		whatJumbo = "closeremote"
	} else if neterr, ok := err.(net.Error); ok && neterr.Timeout() {
		log.Printf("stream %q says, remote %q timedout\n", s.laddr, raddr)
		whatJumbo = "closeremote"
	} else if err != nil {
		log.Printf("stream %q says, error `%v` from %q\n", s.laddr, err, raddr)
		whatJumbo = "closeall"
	} else {
		log.Printf("no error, why did you call jumbo !!!\n")
		return
	}

	switch whatJumbo {
	case "closeremote":
		if vbuuids, err := s.closeRemoteHost(raddr); err != nil {
			msg = ErrorStreamdCascadingFault
			go s.Close()
		} else if len(vbuuids) > 0 {
			msg = RestartVbuckets(vbuuids)
		} else {
			msg = nil
		}

	case "closeall":
		msg = err
		go s.Close()
	}
	return
}

// add a new connection to gen-server state
func (s *MutationStream) addConnection(conn net.Conn, worker chan interface{}) (err error) {
	raddr := conn.RemoteAddr().String()
	log.Printf("connection request from %q\n", raddr)
	if _, _, err = net.SplitHostPort(raddr); err != nil {
		return err
	}

	if _, ok := s.conns[raddr]; ok {
		log.Printf("Connection %q already active with %q\n", raddr, s.laddr)
		return ErrorStreamUnexpected
	}

	s.conns[raddr] = netConn{conn: conn, worker: worker, active: false}
	log.Printf("total %v connections active with %q\n", len(s.conns), s.laddr)
	return
}

// close all connections and worker routines
func (s *MutationStream) closeConnections() {
	for raddr, _ := range s.conns {
		s.closeRemoteHost(raddr)
	}
	s.conns, s.activeVbs = nil, nil
}

// close all connections with remote host.
func (s *MutationStream) closeRemoteHost(raddr string) ([]uint64, error) {
	if _, ok := s.conns[raddr]; !ok {
		log.Printf("unknown connection %q\n", raddr)
		return nil, ErrorStreamUnexpected
	}

	host, _, _ := net.SplitHostPort(raddr)
	raddrs := remoteConnections(host, s.conns)
	vbuuids := vbucketsForRemote(raddrs, s.activeVbs)

	log.Printf("closing connections %q\n", raddrs)
	log.Printf("affected vbuckets %v\n", vbuuids)
	// close all connections and worker routines for this remote host
	for _, raddr := range raddrs {
		nc := s.conns[raddr]
		close(nc.worker)
		nc.conn.Close()
		if err := delVbuckets(raddr, s.activeVbs); err != nil {
			return nil, err
		}
		delete(s.conns, raddr)
	}
	return vbuuids, nil
}

// add a bunch of vbuckets to remote connection.
func addVbuckets(raddr string, vbuuids []uint64, activeVbs map[uint64]*activeVb) (err error) {
	for _, vbuuid := range vbuuids {
		if err := addVbucket(raddr, vbuuid, activeVbs); err != nil {
			return err
		}
	}
	return
}

// add vbucket to remote connection.
func addVbucket(raddr string, vbuuid uint64, activeVbs map[uint64]*activeVb) (err error) {
	avb, ok := activeVbs[vbuuid]
	if ok == false {
		avb = &activeVb{vbuuid: vbuuid, raddrs: []string{raddr}}
	} else if c.HasString(raddr, avb.raddrs) == false {
		avb.raddrs = append(avb.raddrs, raddr)
	}
	log.Printf("vbucket %v mapped on connection %q\n", vbuuid, raddr)
	activeVbs[vbuuid] = avb
	return
}

// delete vbuckets mapped to a remote connection
func delVbuckets(raddr string, activeVbs map[uint64]*activeVb) (err error) {
	for vbuuid, _ := range activeVbs {
		if err := delVbucket(raddr, vbuuid, activeVbs); err != nil {
			return err
		}
	}
	return
}

// delete a vbucket from remote connection.
func delVbucket(raddr string, vbuuid uint64, activeVbs map[uint64]*activeVb) (err error) {
	avb, ok := activeVbs[vbuuid]
	if ok == false {
		log.Printf("vbucket %v not yet mapped %q\n", vbuuid, raddr)
		return ErrorStreamUnexpected
	}
	if c.HasString(raddr, avb.raddrs) {
		avb.raddrs = c.ExcludeStrings(avb.raddrs, []string{raddr})
		log.Printf("delete vbucket %v from connection %q\n", vbuuid, raddr)
	}
	return
}

// get all remote connections for `host`
func remoteConnections(host string, conns map[string]netConn) []string {
	raddrs := make([]string, 0)
	for raddrx, _ := range conns {
		if h, _, _ := net.SplitHostPort(raddrx); h == host {
			raddrs = append(raddrs, raddrx)
		}
	}
	return raddrs
}

func vbucketsForRemote(raddrs []string, activeVbs map[uint64]*activeVb) []uint64 {
	vbuuids := make([]uint64, 0, c.MaxVbuckets)
	for vbuuid, avb := range activeVbs {
		if len(c.CommonStrings(avb.raddrs, raddrs)) == 0 {
			continue
		}
		vbuuids = append(vbuuids, vbuuid)
	}
	return vbuuids
}

// go-routine to listen for new connections, if this routine goes down -
// server is shutdown and reason notified back to application.
func streamListener(laddr string, lis net.Listener, reqch chan []interface{}) {
	defer func() {
		log.Println("listener panic:", recover())
		msg := streamServerMessage{
			cmd: streamgCmdError,
			err: ErrorStreamdExit,
		}
		reqch <- []interface{}{msg}
	}()
	for {
		// TODO: handle `err` for lis.Close() and avoid log.Panicln()
		if conn, err := lis.Accept(); err != nil {
			panic(err)
		} else {
			msg := streamServerMessage{
				cmd:   streamgCmdNewConnection,
				raddr: conn.RemoteAddr().String(),
				args:  []interface{}{conn},
			}
			reqch <- []interface{}{msg}
		}
	}
}

// go-routine to read VbConnectionMap map message, that provides a list
// of vbuckets that be expected on this connection.
//
// routine exits after receiving vbmap
func doReceiveVbmap(conn net.Conn, reqch chan []interface{}) {
	flags := StreamTransportFlag(0).SetProtobuf()
	pkt := NewStreamTransportPacket(c.MaxStreamDataLen, flags)
	msg := streamServerMessage{raddr: conn.RemoteAddr().String()}

	timeoutMs := c.StreamReadDeadline * time.Millisecond
	conn.SetReadDeadline(time.Now().Add(timeoutMs))
	if payload, err := pkt.Receive(conn); err != nil {
		msg.cmd, msg.err = streamgCmdError, err
	} else if vbmap, ok := payload.(*protobuf.VbConnectionMap); ok == false {
		msg.cmd, msg.err = streamgCmdError, ErrorStreamPayload
	} else {
		msg.cmd, msg.args = streamgCmdVbmap, []interface{}{vbmap}
	}
	reqch <- []interface{}{msg}
}

// per connection go-routine to read KeyVersions and control messages like
// StreamBegin, DropData, Sync, and StreamEnd.
//
// go-routine will exit in case of error, or upon receiving control messages,
// or upon receiving kill-command.
func doReceiveKeyVersions(
	nc netConn,
	mutch chan<- []*protobuf.KeyVersions,
	reqch chan<- []interface{},
	buffers map[uint64][]*protobuf.KeyVersions) {

	conn, worker := nc.conn, nc.worker
	flags := StreamTransportFlag(0).SetProtobuf() // TODO: make it configurable
	pkt := NewStreamTransportPacket(c.MaxStreamDataLen, flags)
	msg := streamServerMessage{raddr: conn.RemoteAddr().String()}

loop:
	for {
		msg.cmd, msg.err, msg.args, msg.buffers = 0, nil, nil, nil
		timeoutMs := c.StreamReadDeadline * time.Millisecond
		conn.SetReadDeadline(time.Now().Add(timeoutMs))
		if payload, err := pkt.Receive(conn); err != nil {
			msg.cmd, msg.err = streamgCmdError, err
			msg.buffers = buffers
			reqch <- []interface{}{msg}
			break loop

		} else if isControlMessage(payload) {
			// control message will have only one KeyVersions entry
			kvs := payload.([]*protobuf.KeyVersions)
			msg.cmd, msg.args = streamgCmdVbcontrol, []interface{}{kvs}
			msg.buffers = buffers
			reqch <- []interface{}{msg}
			break loop

		} else if buffers, yes := updateVbBuffers(payload, buffers); yes {
			msg.cmd, msg.buffers = streamgCmdBufferCheck, buffers
			reqch <- []interface{}{msg}
			break loop

		} else if kvs := expectedVbucket(payload, buffers); kvs != nil {
			select {
			case mutch <- kvs:
			case wmsg, ok := <-worker:
				if ok {
					switch wmsg.(type) {
					case workerBufferCheck:
						msg.cmd, msg.buffers = streamgCmdBufferCheck, buffers
						reqch <- []interface{}{msg}
						break loop
					default:
						panic("unexpected worker message")
					}
				} else {
					msg.cmd, msg.err = streamgCmdError, ErrorStreamdWorkerKilled
					msg.buffers = buffers
					reqch <- []interface{}{msg}
					break loop
				}
			}
		} else {
			msg.cmd, msg.err = streamgCmdError, ErrorStreamdUnexpectedVbucket
			msg.buffers = buffers
			reqch <- []interface{}{msg}
			break loop
		}
	}
	nc.active = false
}

func expectedVbucket(payload interface{}, buffers map[uint64][]*protobuf.KeyVersions) []*protobuf.KeyVersions {
	kvs := payload.([]*protobuf.KeyVersions)
	vbuuid := kvs[0].GetVbuuid()
	if _, ok := buffers[vbuuid]; ok {
		return kvs
	}
	return nil
}

func updateVbBuffers(
	payload interface{},
	buffers map[uint64][]*protobuf.KeyVersions) (map[uint64][]*protobuf.KeyVersions, bool) {

	kvs := payload.([]*protobuf.KeyVersions)
	vbuuid := kvs[0].GetVbuuid()
	if buffers[vbuuid] != nil {
		buffers[vbuuid] = append(buffers[vbuuid], kvs...)
		return buffers, true
	}
	return buffers, false
}

func isControlMessage(payload interface{}) bool {
	if kvs, ok := payload.([]*protobuf.KeyVersions); ok && len(kvs) == 1 {
		cmd := byte(kvs[0].GetCommand())
		if (cmd == c.DropData) || (cmd == c.StreamBegin) ||
			(cmd == c.StreamEnd) {
			return true
		}
	}
	return false
}
