// flags:
//           +---------------+---------------+
//       byte|       0       |       1       |
//           +---------------+---------------+
//       bits|0 1 2 3 4 5 6 7|0 1 2 3 4 5 6 7|
//           +-------+-------+---------------+  COMP. - Compression
//          0| COMP. |  ENC. | undefined     |  ENC.  - Encoding
//           +-------+-------+---------------+

package indexer

const ( // types of encoding over the wire.
	encodingProtobuf byte = 0
)

const ( // types of compression over the wire.
	compressionSnappy byte = 0
	compressionGzip        = 1
	compressionBzip2       = 2
)

type StreamTransportFlag uint16

func (flags StreamTransportFlag) GetCompression() byte {
	return byte(flags & StreamTransportFlag(0x000F))
}

func (flags StreamTransportFlag) SetSnappy() StreamTransportFlag {
	return (flags & StreamTransportFlag(0xFFF0)) | StreamTransportFlag(compressionSnappy)
}

func (flags StreamTransportFlag) SetGzip() StreamTransportFlag {
	return (flags & StreamTransportFlag(0xFFF0)) | StreamTransportFlag(compressionGzip)
}

func (flags StreamTransportFlag) SetBzip2() StreamTransportFlag {
	return (flags & StreamTransportFlag(0xFFF0)) | StreamTransportFlag(compressionBzip2)
}

func (flags StreamTransportFlag) GetEncoding() byte {
	return byte(flags & StreamTransportFlag(0x00F0))
}
func (flags StreamTransportFlag) SetProtobuf() StreamTransportFlag {
	return (flags & StreamTransportFlag(0xFF0F)) | StreamTransportFlag(encodingProtobuf)
}
