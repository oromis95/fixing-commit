// constants used by secondary indexing system.

package common

import (
	"fmt"
)

// error codes

// ErrorEmptyN1QLExpression, returned by N1QL Evaluator for secondary keys.
var ErrorEmptyN1QLExpression = fmt.Errorf("errorEmptyN1QLExpression")

// ErrorUnexpectedPayload, returned by mutation constructor.
var ErrorUnexpectedPayload = fmt.Errorf("errorUnexpectedPayload")

// ErrorMaxKeyVersions, returned by mutation constructor.
var ErrorMaxKeyVersions = fmt.Errorf("errorMaxKeyVersions")

// ErrorClosed, returned by gen-server APIs.
var ErrorClosed = fmt.Errorf("errorClosed")

// ErrorInvalidRequest, returne by adminport or gen-servers in mutation path.
var ErrorInvalidRequest = fmt.Errorf("errorInvalidRequest")

const (
	// Debug boolean to enable debug mode
	Debug = true

	// MaxVbuckets is maximum number of vbuckets for any bucket in kv.
	MaxVbuckets = 1024

	// GenserverChannelSize is typical channel size for channels that carry
	// gen-server request.
	GenserverChannelSize = 64

	// MutationChannelSize is typical channel size for channels that carry
	// kv-mutations.
	MutationChannelSize = 10000

	// KeyVersionsChannelSize is typical channel size for channles that carry
	// projected key-versions.
	KeyVersionsChannelSize = 10000

	// VbucketSyncTimeout timeout, in milliseconds, is for sending Sync
	// messages for inactive vbuckets.
	VbucketSyncTimeout = 5

	// EndpointKVTimeout timeout, in milliseconds, is for endpoints to send
	// buffered key-versions to downstream.
	EndpointKVTimeout = 1

	// MaxEndpointBufferSize is endpoint buffer size.
	MaxEndpointBufferSize = 100

	// MaxStreamDataLen is maximum payload length, in bytes, for transporting
	// data from router to indexer.
	MaxStreamDataLen = 10 * 1024

	// StreamReadDeadline timeout, in milliseconds, is timeout while reading
	// from socket.
	StreamReadDeadline = 2000

	// ConnsPerEndpoint number of parallel connections per endpoint.
	ConnsPerEndpoint = 1

	// EngineHarakiriTimeout timeout, in milliseconds, is timeout after which
	// engine-routine will commit harakiri.
	EngineHarakiriTimeout = 10 * 1000

	// EndpointHarakiriTimeout timeout, in milliseconds, is timeout after which
	// endpoint-routine will commit harakiri.
	EndpointHarakiriTimeout = 10 * 1000
)
