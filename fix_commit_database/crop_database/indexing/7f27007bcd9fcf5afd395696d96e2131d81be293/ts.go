package main

import "time"
import "flag"
import "log"
import "sync"
import "strings"

import "github.com/couchbase/cbauth"
import "github.com/couchbase/indexing/secondary/logging"
import "github.com/couchbase/indexing/secondary/common"
import "github.com/couchbase/indexing/secondary/indexer"

var options struct {
	bucket  string
	cluster string
	auth    string
	trials  int
	par     int
	stat    bool
	getseq  bool
}

func argParse() {
	flag.StringVar(&options.bucket, "bucket", "default",
		"bucket to connect")
	flag.StringVar(&options.cluster, "cluster", "127.0.0.1:9000",
		"cluster to connect")
	flag.StringVar(&options.auth, "auth", "Administrator:asdasd",
		"Auth user and password")
	flag.IntVar(&options.trials, "trials", 1,
		"try stats call for `n` trials")
	flag.IntVar(&options.par, "par", 1,
		"run parallel calls")
	flag.BoolVar(&options.stat, "stat", false,
		"run BucketTs()")
	flag.BoolVar(&options.getseq, "getseq", true,
		"run GetCurrentKVTs()")

	flag.Parse()
}

func main() {
	argParse()

	logging.SetLogLevel(logging.Debug)

	// setup cbauth
	if options.auth != "" {
		up := strings.Split(options.auth, ":")
		if _, err := cbauth.InternalRetryDefaultInit(options.cluster, up[0], up[1]); err != nil {
			log.Fatalf("Failed to initialize cbauth: %s", err)
		}
	}

	numVb := numVbuckets(options.cluster, options.bucket)
	log.Printf("bucket %q has %v vbuckets\n", options.bucket, numVb)

	var wg sync.WaitGroup

	if options.stat {
		start := time.Now()
		for i := 0; i < options.par; i++ {
			go bucketTs(&wg, options.cluster, options.bucket, numVb)
			wg.Add(1)
		}
		wg.Wait()
		durtn := time.Since(start) / time.Duration(options.trials*options.par)
		log.Printf("bucketTs: %v\n", durtn)
	}

	//getCurrentKVTs_old(options.cluster, options.bucket, numVb)

	if options.getseq {
		start := time.Now()
		for i := 0; i < options.par; i++ {
			go getCurrentKVTs(&wg, options.cluster, options.bucket, numVb)
			wg.Add(1)
		}
		wg.Wait()
		durtn := time.Since(start) / time.Duration(options.trials*options.par)
		log.Printf("getCurrentKVTs: %v\n", durtn)
	}
}

func bucketTs(wg *sync.WaitGroup, cluster, bucketn string, numVb int) {
	b, err := common.ConnectBucket(cluster, "default" /*pooln*/, bucketn)
	if err != nil {
		log.Fatal(err)
	}
	defer b.Close()

	for i := 0; i < options.trials; i++ {
		if _, _, err = common.BucketTs(b, numVb); err != nil {
			log.Fatal(err)
		}
	}
	wg.Done()
}

func getCurrentKVTs(wg *sync.WaitGroup, cluster, bucketn string, numVb int) {
	for i := 0; i < options.trials; i++ {
		_, err := indexer.GetCurrentKVTs(cluster, "default", bucketn, numVb)
		if err != nil {
			log.Fatal(err)
		}
	}
	wg.Done()
}

func numVbuckets(cluster, bucketn string) (numVb int) {
	b, err := common.ConnectBucket(cluster, "default" /*pooln*/, bucketn)
	if err != nil {
		log.Fatal(err)
	}
	defer b.Close()
	if numVb, err = common.MaxVbuckets(b); err != nil {
		log.Fatal(err)
	}
	return numVb
}
