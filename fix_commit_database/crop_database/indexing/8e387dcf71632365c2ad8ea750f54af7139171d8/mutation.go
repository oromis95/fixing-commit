package common

// List of possible mutation commands. Mutation messages are broadly divided
// into data and control messages. The division is based on the command field.
const (
	Upsert         byte = iota + 1 // data command
	Deletion                       // data command
	UpsertDeletion                 // data command
	Sync                           // control command
	StreamBegin                    // control command
	StreamEnd                      // control command
)

// Mutation structure describing KV mutations.
type Mutation struct {
	// Required fields
	Version byte // protocol version: TBD
	Command byte
	Vbucket uint16 // vbucket number
	Vbuuid  uint64 // unique id to detect branch history

	// Optional fields
	Docid    []byte   // primary document id
	Seqno    uint64   // vbucket sequence number for this mutation
	Keys     [][]byte // list of key-versions
	Oldkeys  [][]byte // previous key-versions, if available
	Indexids []uint32 // each key-version is for an active index defined on this bucket.
	IndexId  uint32
}
