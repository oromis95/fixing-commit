// admin server to handle admin and system messages.
//
// applications can spawn a server go-routine by doing,
//
//  reqch := make(chan AdminRequest)
//  server := NewHttpServer("localhost:8888", 0, 0, reqch)
//  server.Register(&protobuf.RequestMessage{})
//
//  loop:
//  for {
//      select {
//      case req, ok := <-reqch:
//          if ok {
//              msg := req.GetMessage().(protobuf.RequestMessage)
//              // interpret request and compose a response
//              respMsg := protobuf.ResponseMessage{}
//              err := req.Send(&respMsg)
//          } else {
//              break loop
//          }
//      }
//  }

// IMPORTANT: Go 1.3 is supposed to have graceful shutdown of http server.
// refer https://code.google.com/p/go/issues/detail?id=4674

package adminport

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"reflect"
	"strings"
	"sync"
	"time"
)

// httpServer is a concrete type implementing AdminServer interface.
type httpServer struct {
	mu       sync.Mutex   // handle concurrent updates to this object
	lis      net.Listener // TCP listener
	srv      *http.Server // http server
	messages map[string]Message
	reqch    chan<- AdminRequest // request channel back to application
}

// NewHttpServer creates an instance of admin-server. Start() will actually
// start the server.
func NewHttpServer(connAddr string, rt, wt time.Duration, reqch chan<- AdminRequest) AdminServer {
	s := &httpServer{reqch: reqch, messages: make(map[string]Message)}

	mux := http.NewServeMux()
	mux.HandleFunc("/", s.systemHandler)
	s.srv = &http.Server{
		Addr:           connAddr,
		Handler:        mux,
		ReadTimeout:    rt * time.Second,
		WriteTimeout:   wt * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	return s
}

func (s *httpServer) Register(msg Message) (err error) {
	if s.lis != nil {
		return fmt.Errorf("Server is already running")
	}

	s.mu.Lock()
	defer s.mu.Unlock()
	s.messages[msg.Name()] = msg
	return
}

func (s *httpServer) Unregister(msg Message) (err error) {
	if s.lis != nil {
		return fmt.Errorf("server is already running")
	}
	s.mu.Lock()
	defer s.mu.Unlock()
	name := msg.Name()
	if s.messages[name] == nil {
		return fmt.Errorf("message name not registered")
	}
	delete(s.messages, name)
	return
}

func (s *httpServer) Start() (err error) {
	if s.lis, err = net.Listen("tcp", s.srv.Addr); err != nil {
		return err
	}

	// Server routine
	go func() {
		err := s.srv.Serve(s.lis) // serve until listener is closed.
		if err != nil {
			log.Println("server error:", err) // TODO: use an appropriate logging module.
		}
		close(s.reqch)
	}()
	return
}

func (s *httpServer) Stop() {
	log.Println("Stopping server", s.srv.Addr)
	s.lis.Close()
	s.lis = nil
}

// handle incoming request.
func (s *httpServer) systemHandler(w http.ResponseWriter, r *http.Request) {
	msg := s.messages[strings.Trim(r.URL.Path, "/")]
	if msg == nil {
		http.Error(w, "path not found", http.StatusNotFound)
		return
	}
	log.Printf("admin server (%v) %v", s.srv.Addr, r.URL.Path)

	data := make([]byte, r.ContentLength, r.ContentLength)
	r.Body.Read(data)

	// Get an instance of message type and decode request into that.
	typeOfMsg := reflect.ValueOf(msg).Elem().Type()
	m := reflect.New(typeOfMsg).Interface().(Message)
	m.Decode(data)

	waitch := make(chan Message, 1)
	s.reqch <- &httpAdminRequest{srv: s, msg: m, waitch: waitch}

	// Wait for response message
	respMsg := <-waitch
	if data, err := respMsg.Encode(); err == nil {
		header := w.Header()
		header["Content-Type"] = []string{"application/protobuf"}
		w.Write(data)
	} else {
		log.Printf("error: (%v) %v", r.URL.Path, err)
	}
}

type httpAdminRequest struct {
	srv    *httpServer
	msg    Message
	waitch chan Message
}

func (r *httpAdminRequest) GetMessage() Message {
	return r.msg
}

func (r *httpAdminRequest) Send(msg Message) (err error) {
	r.waitch <- msg
	return
}
