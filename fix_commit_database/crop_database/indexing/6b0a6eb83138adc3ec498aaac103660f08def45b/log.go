package common

import (
	"fmt"
)

// ToDo: Point out the exact difference between two responses
func PrintScanResults(results ScanResponse, resultType string) {
	fmt.Printf("Count of %v is %d\n", resultType, len(results))
	for key, value := range results {
    		fmt.Println("Key:", key, "Value:", value)
	}
}