package main

import (
	c "github.com/couchbase/indexing/secondary/common"
	qclient "github.com/couchbase/indexing/secondary/queryport/client"
	"time"
	"sync"
	"fmt"
)

var result [20]ScanResult

func RunScan(client *qclient.GsiClient,
	spec *ScanConfig, tid int) {
        var err error
        callb := func(res qclient.ResponseReader) bool {
		if res.Error() != nil {
			result[tid].Error = res.Error().Error()
			return false
		} else {
			_, pkeys, err := res.GetEntries()
			_ = len(pkeys)
                        if err != nil {
				result[tid].Error = err.Error()
				return false
			}

			result[tid].Rows += uint64(len(pkeys))
		}

		return true
	}
loop:
	for i := 0; i < spec.Repeat+1; i++ {
			client.Range(spec.DefnId, spec.Low, spec.High,
				qclient.Inclusion(spec.Inclusion), false, spec.Limit, c.AnyConsistency, nil, callb)

                        if err != nil {
		                result[tid].Error = err.Error()
			        break loop
	         	}	
       }

}

func RunCommands(cluster string, cfg *Config) (*Result, error) {
        var wg sync.WaitGroup

	config := c.SystemConfig.SectionConfig("queryport.client.", true)
	client, err := qclient.NewGsiClient(cluster, config)
	if err != nil {
		return nil, err
	}

	indexes, err := client.Refresh()
	if err != nil {
		return nil, err
	}

	for i, spec := range cfg.ScanSpecs {
		if spec.Id == 0 {
			spec.Id = uint64(i)
		}

		for _, index := range indexes {
			if index.Definition.Bucket == spec.Bucket &&
				index.Definition.Name == spec.Index {
				spec.DefnId = uint64(index.Definition.DefnId)
			}
		}
                var j int = 0
                wg.Add(20)
	        for ; j< 20; j++ {
                    go func(wg *sync.WaitGroup, tid int) {
                       RunScan(client, spec, tid)
                       wg.Done()
                    }(&wg, j)
                }
	        startTime := time.Now()
                wg.Wait()
	        dur := time.Now().Sub(startTime)
                var totalRows uint64 = 0
                for j := 0; j< 20; j++ {

                    totalRows += result[j].Rows
                }
                fmt.Printf("total throughput duration in seconds: %f\n", dur.Seconds())
                fmt.Printf("total number of Rows read: %d\n", totalRows)
        }

	client.Close()
        var dummyresult Result
	return &dummyresult, err
}
