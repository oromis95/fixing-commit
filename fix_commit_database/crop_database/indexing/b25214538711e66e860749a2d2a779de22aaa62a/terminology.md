##Terminology Document

This document defines the terminology used in Secondary Indexes.

- High-Watermark Timestamp
- Stability Timestamp
- Restart Timestamp
- Mutation Queue
- Catchup Queue
- Stability Snapshot
- Persistent Snapshot
- Partition
- Slice
