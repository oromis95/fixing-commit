##Mutation Execution Flow

This document describes how KV mutations are received and processed by the Secondary Indexes.

####Insert/Update Mutation
![](https://rawgithub.com/couchbase/indexing/master/secondary/docs/design/images/InsertWorkflow.svg)

####Delete Mutation
![](https://rawgithub.com/couchbase/indexing/master/secondary/docs/design/images/DeleteWorkflow.svg)
