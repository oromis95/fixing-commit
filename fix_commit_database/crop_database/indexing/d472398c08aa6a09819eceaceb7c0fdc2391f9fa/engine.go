package projector

import (
	c "github.com/couchbase/indexing/secondary/common"
	"log"
	"time"
)

// Engine is immutable structure defined for each index, or any other entity
// that wants projection and routing over kv-mutations.
type Engine struct {
	uuid      uint64
	evaluator c.Evaluator        // do document projection
	router    c.Router           // route projected values to zero or more end-points
	reqch     chan []interface{} // carries *MutationEvent, engnCmdClose
	finch     chan bool
	endpoints map[string]*Endpoint
}

// NewEngine creates a new engine instance for `uuid`.
func NewEngine(
	uuid uint64,
	evaluator c.Evaluator,
	router c.Router,
	endpoints map[string]*Endpoint) *Engine {

	engine := &Engine{
		uuid:      uuid,
		evaluator: evaluator,
		router:    router,
		reqch:     make(chan []interface{}, c.MutationChannelSize),
		finch:     make(chan bool),
		endpoints: endpoints,
	}
	go engine.run(engine.reqch)
	log.Printf("engine started for `%v`\n", uuid)
	return engine
}

const (
	engnCmdEvent byte = iota + 1
	engnCmdClose
)

// Event will post MutationEvent event to this engine.
func (e *Engine) Event(m *MutationEvent) error {
	var respch chan []interface{}
	if m == nil {
		return ErrorArgument
	}
	cmd := []interface{}{engnCmdEvent, m}
	_, err := c.FailsafeOp(e.reqch, respch, cmd, e.finch)
	return err
}

// Close will close this engine and free up the resources
func (e *Engine) Close() error {
	var respch chan []interface{}
	cmd := []interface{}{engnCmdClose}
	_, err := c.FailsafeOp(e.reqch, respch, cmd, e.finch)
	return err
}

// routine handles data path for the engine
func (engine *Engine) run(reqch chan []interface{}) {
	var err error

	defer func() { // panic safe
		if r := recover(); r != nil {
			log.Printf("Engine:run() crashed `%v`\n", engine.uuid)
			engine.doClose()
		}
	}()

	router := engine.router
	sendToEndpoints := func(raddrs []string, kv *c.KeyVersions) {
		for _, raddr := range raddrs {
			endpoint := engine.endpoints[raddr]
			if err = endpoint.Send(kv); err != nil {
				log.Printf("error `%v` from endpoint %q\n", err, raddr)
			}
		}
	}

	routeKV := func(kv *c.KeyVersions) {
		switch kv.Command {
		case c.Upsert:
			sendToEndpoints(router.UpsertEndpoints(kv), kv)
			delkv := c.NewUpsertDeletion(kv)
			sendToEndpoints(router.UpsertDeletionEndpoints(delkv), kv)

		case c.Deletion:
			sendToEndpoints(router.DeletionEndpoints(kv), kv)

		case c.StreamBegin, c.Sync, c.StreamEnd:
			// don't route, these events are are already send to endpoints.
		}
	}

loop:
	for {
		harakiri := time.After(c.EngineHarakiriTimeout * time.Millisecond)
		select {
		case msg := <-reqch:
			switch msg[0].(byte) {
			case engnCmdEvent:
				m := msg[1].(*MutationEvent)
				kv := keyVersions(engine.uuid, engine.evaluator, m)
				if kv != nil {
					routeKV(kv)
				}

			case engnCmdClose:
				engine.doClose()
				break loop
			}

		case <-harakiri:
			engine.doClose()
			log.Printf("engine `%v` committed harakiri\n", engine.uuid)
			break loop
		}
	}
}

func (engine *Engine) doClose() {
	close(engine.finch)
}

// create KeyVersions for single `uuid`.
func keyVersions(uuid uint64, evaluator c.Evaluator, m *MutationEvent) (kv *c.KeyVersions) {
	switch m.Opcode {
	case OpStreamBegin:
		kv = c.NewStreamBegin(m.Vbucket, m.Vbuuid, m.Seqno)
	case OpStreamEnd:
		kv = c.NewStreamEnd(m.Vbucket, m.Vbuuid, m.Seqno)
	case OpSync:
		kv = c.NewSync(m.Vbucket, m.Vbuuid, m.Seqno)
	case OpMutation:
		kv = c.NewUpsert(m.Vbucket, m.Vbuuid, m.Key, m.Seqno)
		seckey_o, seckey_n, err := doEvaluate(m, evaluator)
		if err != nil {
			log.Printf("error: evaluating - %v\n", err)
			kv = nil
		} else {
			kv.Uuids = []uint64{uuid}
			kv.Keys, kv.Oldkeys = [][]byte{seckey_n}, [][]byte{seckey_o}
		}
	case OpDeletion:
		kv = c.NewDeletion(m.Vbucket, m.Vbuuid, m.Key, m.Seqno)
		seckey_o, seckey_n, err := doEvaluate(m, evaluator)
		if err != nil {
			log.Printf("error: evaluating - %v\n", err)
			kv = nil
		} else {
			kv.Uuids = []uint64{uuid}
			if seckey_n != nil {
				kv.Keys = [][]byte{seckey_n}
			}
			if seckey_o != nil {
				kv.Oldkeys = [][]byte{seckey_o}
			}
		}
	}
	return
}

func doEvaluate(m *MutationEvent, evaluator c.Evaluator) ([]byte, []byte, error) {
	var seckey_n, seckey_o []byte
	var err error
	if len(m.OldValue) > 0 { // project old secondary key
		if seckey_o, err = evaluator.Evaluate(m.Key, m.OldValue); err != nil {
			return nil, nil, err
		}
	}
	if len(m.Value) > 0 { // project new secondary key
		if seckey_n, err = evaluator.Evaluate(m.Key, m.Value); err != nil {
			return nil, nil, err
		}
	}
	return seckey_o, seckey_n, nil
}
