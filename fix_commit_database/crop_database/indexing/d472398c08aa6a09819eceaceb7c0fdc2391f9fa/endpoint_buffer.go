package projector

import (
	"bytes"
	c "github.com/couchbase/indexing/secondary/common"
	"github.com/couchbase/indexing/secondary/indexer"
)

type endpointBuffers struct {
	size    int
	buffers [][]*c.KeyVersions
}

func newEndpointBuffers(size int) *endpointBuffers {
	buffer := make([]*c.KeyVersions, 0, size)
	b := &endpointBuffers{size: size, buffers: [][]*c.KeyVersions{buffer}}
	return b
}

func (b *endpointBuffers) getBuffers() [][]*c.KeyVersions {
	return b.buffers
}

func (b *endpointBuffers) addKeyVersions(kv *c.KeyVersions) {
	buffers := b.getBuffers()
	l := len(buffers)
	if b.isControlCommand(kv) {
		if len(buffers[l-1]) == 0 {
			buffers[l-1] = append(buffers[l-1], kv)
		} else {
			buffers = append(buffers, []*c.KeyVersions{kv})
		}
		buffers = append(buffers, make([]*c.KeyVersions, 0, b.size))
	} else {
		buffers[l-1] = append(buffers[l-1], kv)
	}
}

func (b *endpointBuffers) isControlCommand(kv *c.KeyVersions) bool {
	return kv.Command == c.Sync ||
		kv.Command == c.StreamBegin ||
		kv.Command == c.StreamEnd
}

// flush the buffers to the other end.
func (b *endpointBuffers) flushBuffers(client *indexer.StreamClient) error {
	for _, buffer := range b.buffers {
		acc := accUuid(buffer)
		for _, kvs := range acc {
			if len(kvs) == 0 {
				continue
			}
			if err := client.SendKeyVersions(kvs); err != nil {
				return err
			}
		}
	}
	return nil
}

// gather a list of KeyVersions for each bucket/vbucket, such that,
// Vbuuid, Vbucket, Seqno and Docid are same among listed KeyVersions
//
// TODO:
// - make sure that keyversions don't exceed c.MaxStreamDataLen
func accUuid(buffer []*c.KeyVersions) (acc map[uint64][]*c.KeyVersions) {
	// map of {vbuuid -> []*c.KeyVersions}
	acc = make(map[uint64][]*c.KeyVersions)

	for _, kv := range buffer {
		kvs, ok := acc[kv.Vbuuid]
		if ok == false {
			kvs = make([]*c.KeyVersions, 0, len(buffer))
			acc[kv.Vbuuid] = kvs
		}
		appended := false
		for _, k := range kvs {
			if isSameMutation(kv, k) {
				k.Uuids = append(k.Uuids, kv.Uuids[0])
				k.Keys = append(k.Keys, kv.Keys[0])
				k.Oldkeys = append(k.Oldkeys, kv.Oldkeys[0])
				appended = true
				break
			}
		}
		if appended == false {
			kvs = append(kvs, kv)
		}
	}
	return acc
}

func isSameMutation(kv1, kv2 *c.KeyVersions) bool {
	return kv1.Command == kv2.Command &&
		bytes.Compare(kv1.Docid, kv2.Docid) == 0 &&
		kv1.Seqno == kv2.Seqno
}
