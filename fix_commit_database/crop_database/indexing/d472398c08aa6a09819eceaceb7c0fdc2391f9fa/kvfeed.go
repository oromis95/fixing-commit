// concurrency model:
//
//                        downstream component (BucketFeed)
//                        ---------------------------------
//                                            ^
//                                            |
//                                            *--------*
//                                      Begin |        |
//                                   Mutation |        | ErrorClientExited
//                                   Deletion |        | ErrorShiftingVbucket
//                                       Sync |   sbKVFeedClosed
//                      NewKVFeed()       end |        |
//                           |  |             |        |
//                          (spawn)      MutationEvent |
//                           |  |             |        |
//                           |  *----------- runReceiver()
//                           |                ^     ^
//                           |                |     |
//    RequestFeed() -----*-> genServer()--- killch  *-- vbucket stream
//             |         |      ^                   |
//  <--failTs,kvTs       |      |                   *-- vbucket stream
//                       |      |                   |
//    CloseFeed() -------*      |                   *-- vbucket stream
//                              |                   |
//                              *------------> couchbase-client
//
// Notes:
//
// - new kv-feed spawns a gen-server routine for control path and
//   gather-scatter routine for data path.
// - RequestFeed can start, restart or shutdown one or more vbuckets.
// - for a successful RequestFeed,
//   - failover-timestamp, restart-timestamp must contain timestamp for
//     "active vbuckets".
//   - if request is to shutdown vbuckets, failover-timetamp and
//     restart-timetamp will be empty.
//   - StreamBegin and StreamEnd events are gauranteed by couchbase-client.
// - for idle vbuckets periodic Sync events will be published downstream.
// - KVFeed will be closed, notifying downstream component with,
//   - nil, when downstream component does CloseFeed()
//   - ErrorClientExited, when upstream closes the mutation channel
//   - ErrorShiftingVbucket, when vbuckets are shifting

package projector

import (
	"fmt"
	c "github.com/couchbase/indexing/secondary/common"
	"log"
	"time"
)

var ErrorVBmap = fmt.Errorf("ErrorVBmap")
var ErrorClientExited = fmt.Errorf("ErrorClientExited")
var ErrorShiftingVbucket = fmt.Errorf("ErrorShiftingVbucket")

// KVFeed is per bucket, per node feed for a subset of vbuckets
type KVFeed struct {
	bfeed   *BucketFeed
	kvaddr  string // node address
	pooln   string
	bucketn string
	bucket  BucketAccess
	feeder  KVFeeder
	// gen-server
	reqch    chan []interface{}
	finch    chan bool
	vbuckets map[uint16]*activeVbucket
}

type activeVbucket struct {
	vbuuid uint64
	seqno  uint64
}

// sbKVFeedClosed is sent back to downstream, when ever a KVFeed instance is
// getting closed.
type sbKVFeedClosed struct {
	kvaddr string
	err    error
}

// NewKVFeed create a new feed from `kvaddr` node for a single bucket. Uses
// couchbase client API to identify the subset of vbuckets mapped to this
// node.
//
// `eventch` carries *MutationEvent or *sbKVFeedClosed back to BucketFeed
//
// if error, KVFeed is not started
// - error returned by couchbase client
func NewKVFeed(
	bfeed *BucketFeed,
	kvaddr, pooln, bucketn string,
	eventch chan<- interface{}) (*KVFeed, error) {

	p := bfeed.getFeed().getProjector()
	bucket, err := p.getBucket(kvaddr, pooln, bucketn)
	if err != nil {
		return nil, err
	}
	feeder, err := bucket.OpenKVFeed(kvaddr)
	if err != nil {
		return nil, err
	}
	kvfeed := &KVFeed{
		bfeed:   bfeed,
		kvaddr:  kvaddr,
		pooln:   pooln,
		bucketn: bucketn,
		bucket:  bucket,
		feeder:  feeder.(KVFeeder),
		// gen-server
		reqch:    make(chan []interface{}, c.GenserverChannelSize),
		finch:    make(chan bool),
		vbuckets: make(map[uint16]*activeVbucket),
	}
	killch := make(chan bool)
	go kvfeed.genServer(kvfeed.reqch, killch)
	go kvfeed.runReceiver(eventch, killch)
	return kvfeed, nil
}

// APIs to gen-server
const (
	kvfCmdRequestFeed byte = iota + 1
	kvfCmdCloseFeed
)

// RequestFeed will stream MutationEvent from a subset of vbuckets. Used by
// BucketFeed to start, restart and shutdown streams.
//
// returns failover-timetamp and kv-timestamp.
// - ErrorInvalidRequest if request is malformed.
// - error returned by couchbase client.
// - error if KVFeed is already closed.
func (kvfeed *KVFeed) RequestFeed(req RequestReader) (failoverTs, kvTs *c.Timestamp, err error) {
	if req == nil {
		return nil, nil, ErrorArgument
	}
	respch := make(chan []interface{}, 1)
	cmd := []interface{}{kvfCmdRequestFeed, req, respch}
	resp, err := c.FailsafeOp(kvfeed.reqch, respch, cmd, kvfeed.finch)
	if err != nil {
		return nil, nil, err
	}
	if resp[2] == nil {
		failoverTs, kvTs = resp[0].(*c.Timestamp), resp[1].(*c.Timestamp)
		return failoverTs, kvTs, nil
	}
	return nil, nil, resp[2].(error)
}

// CloseFeed will close this feed from kv-node. Note that this feed can get
// closed automatically due to upstream error.
func (kvfeed *KVFeed) CloseFeed() error {
	respch := make(chan []interface{}, 1)
	cmd := []interface{}{kvfCmdCloseFeed, respch}
	resp, err := c.FailsafeOp(kvfeed.reqch, respch, cmd, kvfeed.finch)
	if err != nil {
		return err
	} else if resp[0] == nil {
		return nil
	}
	return resp[0].(error)
}

// routine handles control path.
func (kvfeed *KVFeed) genServer(reqch chan []interface{}, killch chan bool) {
	defer func() { // panic safe
		if r := recover(); r != nil {
			log.Printf("KVFeed:genServer() crashed `%v`\n", r)
			kvfeed.doClose()
		}
		close(killch)
	}()

loop:
	for {
		msg := <-reqch
		switch msg[0].(byte) {
		case kvfCmdRequestFeed:
			req, respch := msg[1].(RequestReader), msg[2].(chan []interface{})
			failTs, kvTs, err := kvfeed.requestFeed(req)
			respch <- []interface{}{failTs, kvTs, err}

		case kvfCmdCloseFeed:
			respch := msg[1].(chan []interface{})
			respch <- []interface{}{kvfeed.doClose()}
			break loop
		}
	}
}

// start, restart or shutdown streams
func (kvfeed *KVFeed) requestFeed(req RequestReader) (failTs, kvTs *c.Timestamp, err error) {
	// fetch restart-timestamp from request
	feeder := kvfeed.feeder
	ts := req.RestartTimestamp(kvfeed.bucketn)
	if ts == nil {
		log.Printf("error: no req.timestamp for bucket %q\n", kvfeed.bucketn)
		return nil, nil, c.ErrorInvalidRequest
	}
	// fetch list of vbuckets mapped on this connection
	m, err := kvfeed.bucket.GetVBmap([]string{kvfeed.kvaddr})
	if err != nil {
		return nil, nil, err
	}
	vbnos := m[kvfeed.kvaddr]
	if vbnos == nil {
		return nil, nil, ErrorVBmap
	}

	// execute the request
	ts = ts.SelectByVbuckets(vbnos)
	if req.IsStart() { // start
		failTs, kvTs, err = feeder.StartVbStreams(ts)
	} else if req.IsRestart() { // restart implies a shutdown and start
		if err = feeder.EndVbStreams(ts); err == nil {
			failTs, kvTs, err = kvfeed.feeder.StartVbStreams(ts)
		}
	} else if req.IsShutdown() { // shutdown
		err = feeder.EndVbStreams(ts)
		failTs, kvTs = ts, ts
	} else {
		err = c.ErrorInvalidRequest
	}
	return
}

// execute close.
func (kvfeed *KVFeed) doClose() error {
	defer func() {
		if r := recover(); r != nil {
			log.Printf("KVFeed:doClose() paniced %q\n", kvfeed.kvaddr)
		}
	}()

	kvfeed.feeder.CloseKVFeed()
	kvfeed.bucket.Close()
	close(kvfeed.finch)
	return nil
}

// routine handles data path
func (kvfeed *KVFeed) runReceiver(eventch chan<- interface{}, killch chan bool) {
	var err error

	mutch := kvfeed.feeder.GetChannel()
	// send StreamEnd markers for a vbuckets
	sendStreamEnd := func() {
		for vbno, v := range kvfeed.vbuckets {
			eventch <- &MutationEvent{
				Opcode:  OpStreamEnd,
				Vbucket: vbno,
				Vbuuid:  v.vbuuid,
				Seqno:   v.seqno,
			}
		}
		kvfeed.vbuckets = nil
	}

	// sendSync will send Sync markers for idle vbuckets.
	sendSync := func() {
		for vbno, v := range kvfeed.vbuckets {
			eventch <- &MutationEvent{
				Opcode:  OpSync,
				Vbucket: vbno,
				Vbuuid:  v.vbuuid,
				Seqno:   v.seqno,
			}
		}
	}

	timeout := time.After(c.VbucketSyncTimeout * time.Millisecond)
loop:
	for {
		select {
		case m, ok := <-mutch: // mutation from upstream
			if ok == false {
				err = ErrorClientExited
			} else if err = kvfeed.updateVbucketActivity(m); err == nil {
				eventch <- m
			}
			if err != nil {
				kvfeed.CloseFeed()
				sendStreamEnd()
				// tell downstream that we are shutting down.
				eventch <- &sbKVFeedClosed{kvfeed.kvaddr, err}
				break loop
			}

		case <-timeout: // downstream heart-beat for inactive vbuckets
			sendSync()
			timeout = time.After(c.VbucketSyncTimeout * time.Millisecond)

		case <-killch:
			sendStreamEnd()
			break loop
		}
	}
}

func (kvfeed *KVFeed) updateVbucketActivity(m *MutationEvent) (err error) {
	vbno := m.Vbucket

	switch m.Opcode {
	case OpStreamBegin:
		if kvfeed.vbuckets[vbno] != nil {
			log.Printf("duplicate OpStreamBegin for vbucket `%v`\n", m.Vbucket)
			return ErrorShiftingVbucket
		}
		kvfeed.vbuckets[vbno] = &activeVbucket{m.Vbuuid, m.Seqno}

	case OpStreamEnd:
		if kvfeed.vbuckets[vbno] == nil {
			log.Printf("duplicate OpStreamEnd for vbucket `%v`\n", m.Vbucket)
			return ErrorShiftingVbucket
		}
		delete(kvfeed.vbuckets, vbno)

	case OpMutation, OpDeletion:
		if v, ok := kvfeed.vbuckets[vbno]; ok == false {
			log.Printf("mutation for unknown vbucket `%v`\n", m.Vbucket)
			return ErrorShiftingVbucket
		} else if v.vbuuid != m.Vbuuid {
			log.Printf("vbuuid mismatch for vbucket `%v`\n", m.Vbucket)
			return ErrorShiftingVbucket
		} else {
			v.vbuuid = m.Vbuuid
			v.seqno = m.Seqno
		}
	}
	return
}
