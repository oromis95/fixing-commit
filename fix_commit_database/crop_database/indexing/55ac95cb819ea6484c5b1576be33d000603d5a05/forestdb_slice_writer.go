// Copyright (c) 2014 Couchbase, Inc.
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
// except in compliance with the License. You may obtain a copy of the License at
//   http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software distributed under the
// License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
// either express or implied. See the License for the specific language governing permissions
// and limitations under the License.

package indexer

import (
	"errors"
	"github.com/couchbase/indexing/secondary/common"
	"github.com/couchbaselabs/goforestdb"
	"sync"
)

type fdbSlice struct {
	name string
	id   SliceId //slice id

	main  *forestdb.Database //db handle for forward index
	back  *forestdb.Database //db handle for reverse index
	rback *forestdb.Database //read only db handle for reverse index

	idxDefnId common.IndexDefnId
	idxInstId common.IndexInstId

	status   SliceStatus
	isActive bool

	sc SnapshotContainer //snapshot container

	mLock   sync.Mutex //forward index lock
	bLock   sync.Mutex //back index lock
	bROLock sync.Mutex //back index read-only lock
}

func NewForestDBSlice(name string, sliceId SliceId, idxDefnId common.IndexDefnId,
	idxInstId common.IndexInstId) (*fdbSlice, error) {

	slice := &fdbSlice{}

	var err error
	config := forestdb.DefaultConfig()
	config.SetDurabilityOpt(forestdb.DRB_ASYNC)

	if slice.main, err = forestdb.Open(name, config); err != nil {
		return nil, err
	}

	//create a separate back-index
	if slice.back, err = forestdb.Open(name+"_back", config); err != nil {
		return nil, err
	}

	config = forestdb.DefaultConfig()
	config.SetOpenFlags(forestdb.OPEN_FLAG_RDONLY)

	//open a read-only handle for back-index
	if slice.rback, err = forestdb.Open(name+"_back", config); err != nil {
		return nil, err
	}

	slice.name = name
	slice.idxInstId = idxInstId
	slice.idxDefnId = idxDefnId
	slice.id = sliceId

	slice.sc = NewSnapshotContainer()

	return slice, nil
}

func (s *fdbSlice) Id() SliceId {
	return s.id
}

func (s *fdbSlice) Name() string {
	return s.name
}

func (s *fdbSlice) Status() SliceStatus {
	return s.status
}

func (s *fdbSlice) IndexInstId() common.IndexInstId {
	return s.idxInstId
}

func (s *fdbSlice) IndexDefnId() common.IndexDefnId {
	return s.idxDefnId
}

func (s *fdbSlice) IsActive() bool {
	return s.isActive
}

func (s *fdbSlice) SetActive(isActive bool) {
	s.isActive = isActive
}

func (s *fdbSlice) SetStatus(status SliceStatus) {
	s.status = status
}

func (s *fdbSlice) GetSnapshotContainer() SnapshotContainer {
	return s.sc
}

//Persist a key/value pair
func (fdb *fdbSlice) Insert(k Key, v Value) error {
	var err error
	var oldkey Key

	common.Tracef("ForestDBSlice: Set Key - %s Value - %s", k.String(), v.String())

	//check if the docid exists in the back index
	if oldkey, err = fdb.getBackIndexEntry(v.Docid()); err != nil {
		//TODO ForestDB returns a missing key as an error. Fix this properly.
		if err != errors.New("key not found") {
			//common.Errorf("ForestDBSlice: Error locating backindex entry %v", err)
			//return err
		}
	} else if oldkey.EncodedBytes() != nil {
		//there is already an entry in main index for this docid
		//delete from main index
		func() {
			fdb.mLock.Lock()
			defer fdb.mLock.Unlock()
			err = fdb.main.DeleteKV(oldkey.EncodedBytes())

		}()
		if err != nil {
			common.Errorf("ForestDBSlice: Error deleting entry from main index %v", err)
			return err
		}
	}

	//if secondary-key is nil, no further processing is required. If this was a KV insert,
	//nothing needs to be done./if this was a KV update, only delete old back/main index entry
	if v.KeyBytes() == nil {
		common.Tracef("ForestDBSlice: Received NIL secondary key %v value %v. "+
			"Skipping Index Insert.", k, v)
		return nil
	}

	//TODO: Handle the case if old-value from backindex matches with the
	//new-value(false mutation). Skip It.

	//set the back index entry <docid, encodedkey>
	func() {
		fdb.bLock.Lock()
		defer fdb.bLock.Unlock()
		err = fdb.back.SetKV([]byte(v.Docid()), k.EncodedBytes())
	}()

	if err != nil {
		return err
	}

	//set in main index
	func() {
		fdb.mLock.Lock()
		defer fdb.mLock.Unlock()
		err = fdb.main.SetKV(k.EncodedBytes(), v.EncodedBytes())
	}()

	if err != nil {
		return err
	}

	return err
}

//Delete a key/value pair by docId
func (fdb *fdbSlice) Delete(docid []byte) error {
	common.Tracef("ForestDBSlice: Delete Key - %s", docid)

	var oldkey Key
	var err error

	if oldkey, err = fdb.getBackIndexEntry(docid); err != nil {
		common.Errorf("ForestDBSlice: Error locating backindex entry %v", err)
		return err
	}

	//delete from main index
	func() {
		fdb.mLock.Lock()
		defer fdb.mLock.Unlock()
		err = fdb.main.DeleteKV(oldkey.EncodedBytes())
	}()

	if err != nil {
		common.Errorf("ForestDBSlice: Error deleting entry from main index %v", err)
		return err
	}

	//delete from the back index
	func() {
		fdb.bLock.Lock()
		defer fdb.bLock.Unlock()
		err = fdb.back.DeleteKV(docid)
	}()

	if err != nil {
		common.Errorf("ForestDBSlice: Error deleting entry from back index %v", err)
		return err
	}

	return nil
}

//Get an existing key/value pair by key
func (fdb *fdbSlice) getBackIndexEntry(docid []byte) (Key, error) {

	var k Key
	var kbyte []byte
	var err error

	common.Tracef("ForestDBSlice: Get BackIndex Key - %s", docid)

	func() {
		fdb.bROLock.Lock()
		defer fdb.bROLock.Unlock()
		kbyte, err = fdb.rback.GetKV([]byte(docid))
	}()

	if err != nil {
		return k, err
	}

	k, err = NewKeyFromEncodedBytes(kbyte)

	return k, err
}

//Snapshot
func (fdb *fdbSlice) Snapshot() (Snapshot, error) {

	s := &fdbSnapshot{id: fdb.id,
		idxDefnId: fdb.idxDefnId,
		idxInstId: fdb.idxInstId}

	var err error
	var dbinfo *forestdb.DatabaseInfo

	//store snapshot seqnum for main index
	func() {
		fdb.mLock.Lock()
		defer fdb.mLock.Unlock()

		dbinfo, err = fdb.main.DbInfo()
	}()

	if err != nil {
		return nil, err
	}
	s.mainSeqNum = dbinfo.LastSeqNum()

	//store snapshot seqnum for back index
	func() {
		fdb.bLock.Lock()
		defer fdb.bLock.Unlock()

		dbinfo, err = fdb.back.DbInfo()
	}()

	if err != nil {
		return nil, err
	}
	s.backSeqNum = dbinfo.LastSeqNum()

	common.Debugf("ForestDB: Created New Snapshot %v", s)

	return s, nil
}

//Commit
func (fdb *fdbSlice) Commit() error {

	var bErr, mErr error
	statusCh := make(DoneChannel)

	//Commit the back index
	go func() {
		fdb.bLock.Lock()
		defer fdb.bLock.Unlock()
		bErr = fdb.back.Commit(forestdb.COMMIT_MANUAL_WAL_FLUSH)
		close(statusCh)
	}()

	func() {
		fdb.mLock.Lock()
		defer fdb.mLock.Unlock()
		//Commit the main index
		mErr = fdb.main.Commit(forestdb.COMMIT_MANUAL_WAL_FLUSH)
	}()

	<-statusCh

	if bErr != nil {
		//TODO: what else needs to be done here
		return bErr
	}

	if mErr != nil {
		return mErr
	}

	return nil
}

//Close the db. Should be able to reopen after this operation
func (fdb *fdbSlice) Close() error {

	//close the main index
	func() {
		fdb.mLock.Lock()
		defer fdb.mLock.Unlock()
		if fdb.main != nil {
			fdb.main.Close()
		}
	}()

	//close the back index
	func() {
		fdb.bLock.Lock()
		defer fdb.bLock.Unlock()
		if fdb.back != nil {
			fdb.back.Close()
		}
	}()
	return nil
}

func (fdb *fdbSlice) Destroy() error {
	//TODO
	return nil

}
