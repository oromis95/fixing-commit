// Copyright (c) 2014 Couchbase, Inc.
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
// except in compliance with the License. You may obtain a copy of the License at
//   http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software distributed under the
// License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
// either express or implied. See the License for the specific language governing permissions
// and limitations under the License.

package indexer

import (
	"github.com/couchbase/indexing/secondary/common"
)

type SliceId int

type SliceStatus int16

const (
	SLICE_STATUS_INIT SliceStatus = iota
	SLICE_STATUS_PREPARING
	SLICE_STATUS_INACTIVE
	SLICE_STATUS_TERMINATE
	SLICE_STATUS_ACTIVE
)

//Slice represents the unit of physical storage for index
type Slice interface {
	Id() SliceId
	Status() SliceStatus
	IndexInstId() IndexInstId
	IndexDefnId() IndexDefnId
	IsActive() bool

	IndexWriter
	SnapshotContainer
}
