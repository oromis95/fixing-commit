// Code generated by protoc-gen-go.
// source: index.proto
// DO NOT EDIT!

package protobuf

import proto "code.google.com/p/goprotobuf/proto"
import json "encoding/json"
import math "math"

// Reference proto, json, and math imports to suppress error if they are not otherwise used.
var _ = proto.Marshal
var _ = &json.SyntaxError{}
var _ = math.Inf

// IndexDefinition will be in one of the following state
type IndexState int32

const (
	// Create index accepted, replicated and reponse sent back to admin
	// console.
	IndexState_IndexInitial IndexState = 1
	// Index DDL replicated, and then communicated to participating indexers.
	IndexState_IndexPending IndexState = 2
	// Initial-load request received from admin console, DDL replicated,
	// loading status communicated with partiticipating indexer and
	// initial-load request is posted to projector.
	IndexState_IndexLoading IndexState = 3
	// Initial-loading is completed for this index from all partiticipating
	// indexers, DDL replicated, and finaly initial-load stream is shutdown.
	IndexState_IndexActive IndexState = 4
	// Delete index request is received, replicated and then communicated with
	// each participating indexer nodes.
	IndexState_IndexDeleted IndexState = 5
)

var IndexState_name = map[int32]string{
	1: "IndexInitial",
	2: "IndexPending",
	3: "IndexLoading",
	4: "IndexActive",
	5: "IndexDeleted",
}
var IndexState_value = map[string]int32{
	"IndexInitial": 1,
	"IndexPending": 2,
	"IndexLoading": 3,
	"IndexActive":  4,
	"IndexDeleted": 5,
}

func (x IndexState) Enum() *IndexState {
	p := new(IndexState)
	*p = x
	return p
}
func (x IndexState) String() string {
	return proto.EnumName(IndexState_name, int32(x))
}
func (x *IndexState) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(IndexState_value, data, "IndexState")
	if err != nil {
		return err
	}
	*x = IndexState(value)
	return nil
}

// List of possible index storage algorithms.
type StorageType int32

const (
	StorageType_View    StorageType = 1
	StorageType_Llrb    StorageType = 2
	StorageType_LevelDB StorageType = 3
)

var StorageType_name = map[int32]string{
	1: "View",
	2: "Llrb",
	3: "LevelDB",
}
var StorageType_value = map[string]int32{
	"View":    1,
	"Llrb":    2,
	"LevelDB": 3,
}

func (x StorageType) Enum() *StorageType {
	p := new(StorageType)
	*p = x
	return p
}
func (x StorageType) String() string {
	return proto.EnumName(StorageType_name, int32(x))
}
func (x *StorageType) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(StorageType_value, data, "StorageType")
	if err != nil {
		return err
	}
	*x = StorageType(value)
	return nil
}

// Type of expression used to evaluate document.
type ExprType int32

const (
	ExprType_Simple     ExprType = 1
	ExprType_JavaScript ExprType = 2
	ExprType_N1QL       ExprType = 3
)

var ExprType_name = map[int32]string{
	1: "Simple",
	2: "JavaScript",
	3: "N1QL",
}
var ExprType_value = map[string]int32{
	"Simple":     1,
	"JavaScript": 2,
	"N1QL":       3,
}

func (x ExprType) Enum() *ExprType {
	p := new(ExprType)
	*p = x
	return p
}
func (x ExprType) String() string {
	return proto.EnumName(ExprType_name, int32(x))
}
func (x *ExprType) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(ExprType_value, data, "ExprType")
	if err != nil {
		return err
	}
	*x = ExprType(value)
	return nil
}

// Type of topology, including paritition type to be used for the index.
type PartitionType int32

const (
	PartitionType_SimpleKeyParitition PartitionType = 1
)

var PartitionType_name = map[int32]string{
	1: "SimpleKeyParitition",
}
var PartitionType_value = map[string]int32{
	"SimpleKeyParitition": 1,
}

func (x PartitionType) Enum() *PartitionType {
	p := new(PartitionType)
	*p = x
	return p
}
func (x PartitionType) String() string {
	return proto.EnumName(PartitionType_name, int32(x))
}
func (x *PartitionType) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(PartitionType_value, data, "PartitionType")
	if err != nil {
		return err
	}
	*x = PartitionType(value)
	return nil
}

type IndexDefinition struct {
	Bucket           *string        `protobuf:"bytes,1,req,name=bucket" json:"bucket,omitempty"`
	IsPrimary        *bool          `protobuf:"varint,2,req,name=isPrimary" json:"isPrimary,omitempty"`
	Name             *string        `protobuf:"bytes,3,req,name=name" json:"name,omitempty"`
	Uuid             *uint64        `protobuf:"varint,4,req,name=uuid" json:"uuid,omitempty"`
	Using            *StorageType   `protobuf:"varint,5,req,name=using,enum=protobuf.StorageType" json:"using,omitempty"`
	ExprType         *ExprType      `protobuf:"varint,6,req,name=exprType,enum=protobuf.ExprType" json:"exprType,omitempty"`
	PartitionType    *PartitionType `protobuf:"varint,7,req,name=partitionType,enum=protobuf.PartitionType" json:"partitionType,omitempty"`
	Expressions      []string       `protobuf:"bytes,8,rep,name=expressions" json:"expressions,omitempty"`
	XXX_unrecognized []byte         `json:"-"`
}

func (m *IndexDefinition) Reset()         { *m = IndexDefinition{} }
func (m *IndexDefinition) String() string { return proto.CompactTextString(m) }
func (*IndexDefinition) ProtoMessage()    {}

func (m *IndexDefinition) GetBucket() string {
	if m != nil && m.Bucket != nil {
		return *m.Bucket
	}
	return ""
}

func (m *IndexDefinition) GetIsPrimary() bool {
	if m != nil && m.IsPrimary != nil {
		return *m.IsPrimary
	}
	return false
}

func (m *IndexDefinition) GetName() string {
	if m != nil && m.Name != nil {
		return *m.Name
	}
	return ""
}

func (m *IndexDefinition) GetUuid() uint64 {
	if m != nil && m.Uuid != nil {
		return *m.Uuid
	}
	return 0
}

func (m *IndexDefinition) GetUsing() StorageType {
	if m != nil && m.Using != nil {
		return *m.Using
	}
	return StorageType_View
}

func (m *IndexDefinition) GetExprType() ExprType {
	if m != nil && m.ExprType != nil {
		return *m.ExprType
	}
	return ExprType_Simple
}

func (m *IndexDefinition) GetPartitionType() PartitionType {
	if m != nil && m.PartitionType != nil {
		return *m.PartitionType
	}
	return PartitionType_SimpleKeyParitition
}

func (m *IndexDefinition) GetExpressions() []string {
	if m != nil {
		return m.Expressions
	}
	return nil
}

type Index struct {
	State            *IndexState      `protobuf:"varint,1,req,name=state,enum=protobuf.IndexState" json:"state,omitempty"`
	Indexinfo        *IndexDefinition `protobuf:"bytes,2,req,name=indexinfo" json:"indexinfo,omitempty"`
	Partition        *IndexPartition  `protobuf:"bytes,3,req,name=partition" json:"partition,omitempty"`
	XXX_unrecognized []byte           `json:"-"`
}

func (m *Index) Reset()         { *m = Index{} }
func (m *Index) String() string { return proto.CompactTextString(m) }
func (*Index) ProtoMessage()    {}

func (m *Index) GetState() IndexState {
	if m != nil && m.State != nil {
		return *m.State
	}
	return IndexState_IndexInitial
}

func (m *Index) GetIndexinfo() *IndexDefinition {
	if m != nil {
		return m.Indexinfo
	}
	return nil
}

func (m *Index) GetPartition() *IndexPartition {
	if m != nil {
		return m.Partition
	}
	return nil
}

// container message for one of the many parition structures.
type IndexPartition struct {
	Skp              *SimpleKeyPartition `protobuf:"bytes,1,opt,name=skp" json:"skp,omitempty"`
	XXX_unrecognized []byte              `json:"-"`
}

func (m *IndexPartition) Reset()         { *m = IndexPartition{} }
func (m *IndexPartition) String() string { return proto.CompactTextString(m) }
func (*IndexPartition) ProtoMessage()    {}

func (m *IndexPartition) GetSkp() *SimpleKeyPartition {
	if m != nil {
		return m.Skp
	}
	return nil
}

type SimpleKeyPartition struct {
	XXX_unrecognized []byte `json:"-"`
}

func (m *SimpleKeyPartition) Reset()         { *m = SimpleKeyPartition{} }
func (m *SimpleKeyPartition) String() string { return proto.CompactTextString(m) }
func (*SimpleKeyPartition) ProtoMessage()    {}

func init() {
	proto.RegisterEnum("protobuf.IndexState", IndexState_name, IndexState_value)
	proto.RegisterEnum("protobuf.StorageType", StorageType_name, StorageType_value)
	proto.RegisterEnum("protobuf.ExprType", ExprType_name, ExprType_value)
	proto.RegisterEnum("protobuf.PartitionType", PartitionType_name, PartitionType_value)
}
