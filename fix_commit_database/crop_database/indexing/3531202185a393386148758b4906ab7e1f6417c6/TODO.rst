CBIDXT-284: Projector should be able to discover about node service
            addresses from ns_server.
CBIDXT-291: Projector - Integrate with ClusterInfo from ns_server.
CBIDXT-289: queryport, the stream response error value is not
            comparable with defined error objects.
CBIDXT-282: queryport - multiplexing requests on the same connection.
CBIDXT-97:  Common / Statistics - component wise statistics gathering.
CBIDXT-195: Projector / Statistics - Gather traffic information.
CBIDXT-218: Gocouchbase, connection pool overflow.
CBIDXT-139: Projector / DataFlow - Error Loading Data During Rebalance.

CBIDXT-126: Projector / ControlFlow - Handle Bucket Deletion.
CBIDXT-124: Projector / DataFlow - Handle Bucket Flush.

Move projector/Error* to projector/client/*
