package kvutility

import (
	"fmt"
	"log"

	"github.com/couchbaselabs/go-couchbase"
)

// Move to common
type KeyValue struct {
	Key       string
	JsonValue map[string]interface{}
}

// Move to common
func HandleError(err error, msg string) {
	if err != nil {
		log.Fatalf("%v: %v", msg, err)
	}
}

// ToDo: Refactor Code
func Set(key string, v interface{}, bucketName string, password string, hostname string) {
	url := "http://" + bucketName + ":" + password + "@" + hostname + ":9000"

	c, err := couchbase.Connect(url)
	HandleError(err, "connect - "+url)

	p, err := c.GetPool("default")
	HandleError(err, "pool")

	b, err := p.GetBucket(bucketName)
	HandleError(err, "bucket")

	err = b.Set(key, 0, v)
	HandleError(err, "set")
}

func SetKeyValue(keyValue KeyValue, bucketName string, password string, hostname string) {
	Set(keyValue.Key, keyValue.JsonValue, bucketName, password, hostname)
}

func SetKeyValues(keyValues []KeyValue, bucketName string, password string, hostname string) {
	for _, value := range keyValues {
		SetKeyValue(value, bucketName, password, hostname)
	}
}

func Get(key string, rv interface{}, bucketName string, password string, hostname string) {

	url := "http://" + bucketName + ":" + password + "@" + hostname + ":9000"

	c, err := couchbase.Connect(url)
	HandleError(err, "connect - "+url)

	p, err := c.GetPool("default")
	HandleError(err, "pool")

	b, err := p.GetBucket("test")
	HandleError(err, "bucket")

	err = b.Get(key, &rv)
	HandleError(err, "get")
}

func Delete(key string, bucketName string, password string, hostname string) {

	url := "http://" + bucketName + ":" + password + "@" + hostname + ":9000"

	c, err := couchbase.Connect(url)
	HandleError(err, "connect - "+url)

	p, err := c.GetPool("default")
	HandleError(err, "pool")

	b, err := p.GetBucket(bucketName)
	HandleError(err, "bucket")

	fmt.Printf("Setting key %v", key)

	err = b.Delete(key)
	HandleError(err, "set")
}
