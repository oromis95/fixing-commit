package main

import (
	"github.com/couchbase/indexing/secondary/tests/functionaltests"
)

func main() {
	FunctionalTests.Setup()
	FunctionalTests.TestSimpleIndex()
}
