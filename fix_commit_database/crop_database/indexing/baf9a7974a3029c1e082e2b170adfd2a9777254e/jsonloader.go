package datautility

import (
	"fmt"
	"encoding/json"
	"io/ioutil"
	
	"github.com/couchbase/indexing/secondary/tests/framework/kvutility"
)

func LoadJSONFromFile(path, docidfield string) []kvutility.KeyValue {
	file, err := ioutil.ReadFile(path)
	kvutility.HandleError(err, "Error reading file "+path)

	var data interface{}
	json.Unmarshal(file, &data)

	m := data.([]interface{})

	keyValues := make([]kvutility.KeyValue, len(m))

	var i = 0
	if len(docidfield) > 0 {
		for _, v := range m {
			keyValues[i].Key = fmt.Sprintf("%v", v.(map[string]interface{})[docidfield])
			keyValues[i].JsonValue = v.(map[string]interface{})
			i++
		}
	} else {
		for _, v := range m {
			keyValues[i].Key = fmt.Sprintf("%v", i)
			keyValues[i].JsonValue = v.(map[string]interface{})
			i++
		}
	}

	return keyValues
}
