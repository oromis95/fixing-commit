// messages that describe index definition.
// Index definition is populated from DDL. Other than `IndexState` other fields
// of this structure are immutable once the index definition structure is
// created.

package protobuf;

// IndexDefn will be in one of the following state
enum IndexState {
    // Create index accepted, replicated and response sent back to admin
    // console.
    IndexInitial     = 1;

    // Index DDL replicated, and then communicated to participating indexers.
    IndexPending     = 2;

    // Initial-load request received from admin console, DDL replicated,
    // loading status communicated with participating indexer and
    // initial-load request is posted to projector.
    IndexLoading     = 3;

    // Initial-loading is completed for this index from all partiticipating
    // indexers, DDL replicated, and finaly initial-load stream is shutdown.
    IndexActive      = 4;

    // Delete index request is received, replicated and then communicated with
    // each participating indexer nodes.
    IndexDeleted     = 5;
}

// List of possible index storage algorithms.
enum StorageType {
    View    = 1;
    Llrb    = 2;
    LevelDB = 3;
}

// Type of expression used to evaluate document.
enum ExprType {
    Simple     = 1;
    JavaScript = 2;
    N1QL       = 3;
}

// Type of topology, including paritition type to be used for the index.
enum  PartitionScheme {
    SimpleKeyParitition = 1;
}

message IndexDefn {
    required uint64          defnId          = 1; // unique index id across the secondary index cluster
    required string          bucket          = 2; // bucket on which index is defined
    required bool            isPrimary       = 3; // whether index secondary-key == docid
    required string          name            = 4; // Name of the index
    required StorageType     using           = 5; // indexing algorithm
    required PartitionScheme partitionScheme = 6;
    required string          partnExpression = 7; // use expressions to evaluate doc
    required ExprType        exprType        = 8; // how to interpret `expressions` strings
    repeated string          secExpressions  = 9; // use expressions to evaluate doc
}

message IndexInst {
    required uint64          instId     = 1;
    required IndexState      state      = 2;
    required IndexDefn       definition = 3; // contains DDL
    required IndexPartition  partition  = 4; // contains partition structure and topology
}

// container message for one of the many parition structures.
message IndexPartition {
    optional SimpleKeyPartition skp = 1;
    // TBD: Add more types of parition structures.
}

message SimpleKeyPartition {
    required string partitionKey = 1; // partition key for key-partition
    // TBD
}
