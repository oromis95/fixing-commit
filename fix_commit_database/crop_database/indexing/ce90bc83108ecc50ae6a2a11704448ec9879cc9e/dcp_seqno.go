package common

import "sync"
import "time"
import "fmt"
import "sort"

import "github.com/couchbase/indexing/secondary/dcp"
import "github.com/couchbase/indexing/secondary/logging"

// cache Bucket{} and DcpFeed{} objects, its underlying connections
// to make Stats-Seqnos fast.
var dcp_buckets_seqnos struct {
	mu      sync.Mutex
	feeds   map[string]map[string]*couchbase.DcpFeed // bucket->kvaddr->feed
	buckets map[string]*couchbase.Bucket
}

func init() {
	dcp_buckets_seqnos.feeds = make(map[string]map[string]*couchbase.DcpFeed)
	dcp_buckets_seqnos.buckets = make(map[string]*couchbase.Bucket)
	go pollForDeletedBuckets()
}

// BucketSeqnos return list of {{vbno,seqno}..} for all vbuckets.
// this call might fail due to,
// - concurrent access that can preserve a deleted/failed bucket object.
// - pollForDeletedBuckets() did not get a chance to cleanup
//   a deleted bucket.
// in both the cases if the call is retried it should get fixed, provided
// a valid bucket exists.
func BucketSeqnos(cluster, pooln, bucketn string) (l_seqnos []uint64, err error) {
	dcp_buckets_seqnos.mu.Lock()
	bucket, ok := dcp_buckets_seqnos.buckets[bucketn]
	kvfeeds, ok := dcp_buckets_seqnos.feeds[bucketn]
	dcp_buckets_seqnos.mu.Unlock()

	defer func() {
		if err != nil {
			for _, feed := range kvfeeds {
				if feed != nil {
					feed.Close()
				}
			}
			if bucket != nil {
				bucket.Close()
			}
			kvfeeds, bucket = nil, nil
		}
		dcp_buckets_seqnos.mu.Lock()
		if err != nil {
			delete(dcp_buckets_seqnos.feeds, bucketn)
			delete(dcp_buckets_seqnos.buckets, bucketn)
			logging.Infof("Deleting bucket %q from dcp_seqno cache...", bucketn)
		} else if kvfeeds != nil && bucket != nil {
			// there is a chance that a failed and closed {feed,bucket}
			// might get readded to the global-cache. In which case it will
			// fail in the next call.
			dcp_buckets_seqnos.buckets[bucketn] = bucket
			dcp_buckets_seqnos.feeds[bucketn] = kvfeeds
		}
		dcp_buckets_seqnos.mu.Unlock()
	}()

	if !ok && cluster != "" && pooln != "" {
		bucket, err = ConnectBucket(cluster, pooln, bucketn)
		if err != nil {
			logging.Errorf("Unable to connect with bucket %q\n", bucketn)
			return nil, err
		}
		kvfeeds = make(map[string]*couchbase.DcpFeed)
		logging.Infof("bucket %q created for dcp_seqno cache...", bucketn)
	}
	// get all kv-nodes
	if err = bucket.Refresh(); err != nil {
		logging.Errorf("bucket.Refresh(): %v\n", err)
	}
	// get current list of kv-nodes
	m, e := bucket.GetVBmap(nil)
	if e != nil {
		logging.Errorf("GetVBmap() failed: %v\n", err)
		err = e
		return nil, err
	}
	// make sure a feed is available for all kv-nodes
	var feed *couchbase.DcpFeed
	for kvaddr := range m {
		if _, ok = kvfeeds[kvaddr]; !ok {
			uuid, _ := NewUUID()
			name := uuid.Str()
			if name == "" {
				err = fmt.Errorf("invalid uuid")
				return nil, err
			}
			name = "dcp-get-seqnos:" + name
			config := map[string]interface{}{"genChanSize": 10, "dataChanSize": 10}
			feed, err = bucket.StartDcpFeedOver(
				name, uint32(0), []string{kvaddr}, uint16(0xABBA), config)
			if err != nil {
				logging.Errorf("StartDcpFeedOver(): %v\n", err)
				return nil, err
			}
			kvfeeds[kvaddr] = feed
		}
	}
	seqnos := make(map[uint16]uint64)
	for _, feed := range kvfeeds {
		kv_seqnos, e := feed.DcpGetSeqnos()
		if e != nil {
			logging.Errorf("feed.DcpGetSeqnos(): %v\n", err)
			err = e
			return nil, err
		}
		for vbno, seqno := range kv_seqnos {
			if prev, ok := seqnos[vbno]; !ok || prev < seqno {
				seqnos[vbno] = seqno
			}
		}
	}
	// sort them
	vbnos := make([]int, 0, 1024)
	for vbno := range seqnos {
		vbnos = append(vbnos, int(vbno))
	}
	sort.Ints(vbnos)
	// gather seqnos.
	l_seqnos = make([]uint64, 0, 1024)
	for _, vbno := range vbnos {
		l_seqnos = append(l_seqnos, seqnos[uint16(vbno)])
	}
	return l_seqnos, nil
}

func pollForDeletedBuckets() {
	for {
		time.Sleep(5 * time.Minute)
		bucketns := make([]string, 0)
		dcp_buckets_seqnos.mu.Lock()
		for bucketn, _ := range dcp_buckets_seqnos.feeds {
			bucketns = append(bucketns, bucketn)
		}
		dcp_buckets_seqnos.mu.Unlock()
		for _, bucketn := range bucketns {
			// this call will clean up the deleted bucket from the
			// cache, if the bucket got deleted from KV.
			BucketSeqnos("", "", bucketn)
		}
	}
}
