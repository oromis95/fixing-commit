// Connector connecting KVFeed to downstream nodes, i.e., KVFeed

package projector

import (
	pc "github.com/Xiaomei-Zhang/couchbase_goxdcr/common"
	c "github.com/couchbase/indexing/secondary/common"
	"sync"
)

// Collector of KVFeed statistics
type KVFeedStatisticsCollector struct {
	eventCount uint64
	countLock  sync.Mutex
}

// implements pc.PartEventListener
func (kvsc *KVFeedStatisticsCollector) OnEvent(eventType pc.PartEventType,
	item interface{}, part pc.Part,
	derivedItems []interface{},
	otherInfos map[string]interface{}) {
	kvsc.countLock.Lock()
	defer kvsc.countLock.Unlock()

	c.Debugf("KVFeedStatisticsCollector OnEvent called on collector %v for eventType %v \n",
		kvsc, eventType)
	switch eventType {
	case pc.DataProcessed:
		kvsc.eventCount++
	default:
	}
}

func (kvsc *KVFeedStatisticsCollector) Statistics() uint64 {
	kvsc.countLock.Lock()
	defer kvsc.countLock.Unlock()

	return kvsc.eventCount
}
