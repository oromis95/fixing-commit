package test 

import (
	"testing"
	"time"
	"github.com/couchbase/indexing/secondary/common"
	"github.com/couchbase/indexing/secondary/manager"
)

func TestMetadataRepoForIndexDefn(t *testing.T) {

	var addr = "localhost:9885"
	var leader = "localhost:9884"

	repo, err := manager.NewMetadataRepo(addr, leader)
	if err != nil {
		t.Fatal(err)	
	}
	
	// clean up
	repo.DropIndexByName("metadata_repo_test")
	repo.DropIndexByName("metadata_repo_test_2")
	
	time.Sleep(time.Duration(1000) * time.Millisecond)
	
	// Add a new index definition : 100	
	idxDefn := &common.IndexDefn{
    	DefnId:				common.IndexDefnId(100),
        Name:           	"metadata_repo_test",	
        Using:            	common.ForestDB,	
        Bucket:         	"Default",	
        IsPrimary:      	false,	
        OnExprList:     	[]string{"Testing"},	
        ExprType:        	common.N1QL,	
        PartitionScheme:  	common.HASH,	
        PartitionKey:   	"Testing"}
        
	if err := repo.CreateIndex(idxDefn); err != nil {
		t.Fatal(err)	
	}
	
	time.Sleep(time.Duration(1000) * time.Millisecond)
	
	// Get the index definition	by name
	idxDefn, err = repo.GetIndexDefnByName("metadata_repo_test")
	if err != nil { 
		t.Fatal(err)	
	}
	
	if idxDefn == nil {
	 	t.Fatal("Cannot find index definition")	
	}
	
	// Delete the index definition by name	
	if err := repo.DropIndexByName("metadata_repo_test"); err != nil {
		t.Fatal(err)
	}
	
	time.Sleep(time.Duration(1000) * time.Millisecond)
	
	// Get the index definition	by name
	idxDefn, err = repo.GetIndexDefnByName("metadata_repo_test")
	
	if idxDefn != nil {
	 	t.Fatal("Find deleted index definition")	
	}
	
	// Add a new index definition : 101	
	idxDefn = &common.IndexDefn{
    	DefnId:				common.IndexDefnId(101),
        Name:           	"metadata_repo_test_2",	
        Using:            	common.ForestDB,	
        Bucket:         	"Default",	
        IsPrimary:      	false,	
        OnExprList:     	[]string{"Testing"},	
        ExprType:        	common.N1QL,	
        PartitionScheme:  	common.HASH,	
        PartitionKey:   	"Testing"}
        
	if err := repo.CreateIndex(idxDefn); err != nil {
		t.Fatal(err)	
	}
	
	time.Sleep(time.Duration(1000) * time.Millisecond)
	
	// Get the index definition by Id	
	idxDefn, err = repo.GetIndexDefnById(common.IndexDefnId(101))
	if err != nil { 
		t.Fatal(err)	
	}
	
	if idxDefn == nil {
	 	t.Fatal("Cannot find index definition")	
	}
	
	// Delete the index definition by Id 
	if err := repo.DropIndexById(common.IndexDefnId(101)); err != nil {
		t.Fatal(err)
	}
	
	time.Sleep(time.Duration(1000) * time.Millisecond)
	
	// Get the index definition by Id	
	idxDefn, err = repo.GetIndexDefnById(common.IndexDefnId(101))
	
	if idxDefn != nil {
	 	t.Fatal("Find deleted index definition")	
	}
	
	// clean up
	repo.DropIndexByName("metadata_repo_test")
	repo.DropIndexByName("metadata_repo_test_2")
	
}