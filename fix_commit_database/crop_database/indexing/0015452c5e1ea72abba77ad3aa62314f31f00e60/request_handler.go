// Copyright (c) 2014 Couchbase, Inc.

// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
// except in compliance with the License. You may obtain a copy of the License at
//   http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software distributed under the
// License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
// either express or implied. See the License for the specific language governing permissions
// and limitations under the License.
package manager

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/couchbase/indexing/secondary/common"
	"github.com/couchbase/indexing/secondary/logging"
	"math"
	"net/http"
	"strings"
	"sync"
)

///////////////////////////////////////////////////////
// Type Definition
///////////////////////////////////////////////////////

//
// Index create / drop
//

type RequestType string

const (
	CREATE RequestType = "create"
	DROP   RequestType = "drop"
)

type IndexRequest struct {
	Version uint64           `json:"version,omitempty"`
	Type    RequestType      `json:"type,omitempty"`
	Index   common.IndexDefn `json:"index,omitempty"`
}

type IndexResponse struct {
	Version uint64             `json:"version,omitempty"`
	Status  ResponseStatus     `json:"status,omitempty"`
	Indexes []common.IndexDefn `json:"indexes,omitempty"`
	Errors  []IndexError       `json:"errors,omitempty"`
}

//
// Index Backup / Restore
//

type LocalIndexMetadata struct {
	IndexerId        string             `json:"indexerId,omitempty"`
	IndexTopologies  []IndexTopology    `json:"topologies,omitempty"`
	IndexDefinitions []common.IndexDefn `json:"definitions,omitempty"`
}

type ClusterIndexMetadata struct {
	Metadata []LocalIndexMetadata `json:"metadata,omitempty"`
}

type RestoreContext struct {
	idxToRestore map[common.IndexerId][]*common.IndexDefn
	idxToResolve map[common.IndexerId][]*common.IndexDefn
}

//
// Index Status
//

type IndexStatusList struct {
	Version uint64        `json:"version,omitempty"`
	Status  []IndexStatus `json:"status,omitempty"`
}

type IndexStatus struct {
	DefnId     common.IndexDefnId `json:"defnId,omitempty"`
	Name       string             `json:"name,omitempty"`
	Bucket     string             `json:"bucket,omitempty"`
	IsPrimary  bool               `json:"isPrimary,omitempty"`
	SecExprs   []string           `json:"secExprs,omitempty"`
	WhereExpr  string             `json:"where,omitempty"`
	Status     string             `json:"Status,omitempty"`
	Host       string             `json:"Status,omitempty"`
	Completion uint64             `json:"Completion,omitempty"`
}

//
// Error
//

type IndexError struct {
	Code string `json:"code,omitempty"`
	Msg  string `json:"msg,omitempty"`
}

type ResponseStatus string

const (
	RESP_SUCCESS ResponseStatus = "success"
	RESP_ERROR   ResponseStatus = "error"
)

//
// Internal data structure
//

type requestHandlerContext struct {
	initializer sync.Once
	mgr         *IndexManager
}

var handlerContext requestHandlerContext

///////////////////////////////////////////////////////
// Registration
///////////////////////////////////////////////////////

func registerRequestHandler(mgr *IndexManager) {

	handlerContext.initializer.Do(func() {
		defer func() {
			if r := recover(); r != nil {
				logging.Warnf("error encountered when registering http createIndex handler : %v.  Ignored.\n", r)
			}
		}()

		http.HandleFunc("/createIndex", handlerContext.createIndexRequest)
		http.HandleFunc("/dropIndex", handlerContext.dropIndexRequest)
		http.HandleFunc("/getLocalIndexMetadata", handlerContext.handleLocalIndexMetadataRequest)
		http.HandleFunc("/getIndexMetadata", handlerContext.handleIndexMetadataRequest)
		http.HandleFunc("/restoreIndexMetadata", handlerContext.handleRestoreIndexMetadataRequest)
		http.HandleFunc("/getIndexStatus", handlerContext.handleIndexStatusRequest)
	})

	handlerContext.mgr = mgr
}

///////////////////////////////////////////////////////
// Create / Drop Index
///////////////////////////////////////////////////////

func (m *requestHandlerContext) createIndexRequest(w http.ResponseWriter, r *http.Request) {

	// convert request
	request := convertIndexRequest(r)
	if request == nil {
		ierr := IndexError{Code: string(RESP_ERROR),
			Msg: "RequestHandler::createIndexRequest: Unable to convert request"}

		res := IndexResponse{
			Status: RESP_ERROR,
			Errors: []IndexError{ierr},
		}

		sendResponse(w, res)
		return
	}

	indexDefn := request.Index

	if indexDefn.DefnId == 0 {
		defnId, err := common.NewIndexDefnId()
		if err != nil {

			ierr := IndexError{Code: string(RESP_ERROR),
				Msg: "Fail to generate index definition id"}

			res := IndexResponse{
				Status: RESP_ERROR,
				Errors: []IndexError{ierr},
			}
			sendResponse(w, res)
			return
		}
		indexDefn.DefnId = defnId
	}

	// call the index manager to handle the DDL
	logging.Debugf("RequestHandler::createIndexRequest: invoke IndexManager for create index bucket %s name %s",
		indexDefn.Bucket, indexDefn.Name)

	if err := m.mgr.HandleCreateIndexDDL(&indexDefn); err == nil {
		// No error, return success
		res := IndexResponse{
			Status:  RESP_SUCCESS,
			Indexes: []common.IndexDefn{indexDefn},
		}
		sendResponse(w, res)
	} else {
		// report failure
		ierr := IndexError{Code: string(RESP_ERROR),
			Msg: err.Error()}

		res := IndexResponse{
			Status: RESP_ERROR,
			Errors: []IndexError{ierr},
		}
		sendResponse(w, res)
	}
}

func (m *requestHandlerContext) dropIndexRequest(w http.ResponseWriter, r *http.Request) {

	// convert request
	request := convertIndexRequest(r)
	if request == nil {
		ierr := IndexError{Code: string(RESP_ERROR),
			Msg: "RequestHandler::dropIndexRequest: Unable to convert request"}

		res := IndexResponse{
			Status: RESP_ERROR,
			Errors: []IndexError{ierr},
		}

		sendResponse(w, res)
		return
	}

	// call the index manager to handle the DDL
	indexDefn := request.Index
	if err := m.mgr.HandleDeleteIndexDDL(indexDefn.DefnId); err == nil {
		// No error, return success
		res := IndexResponse{
			Status: RESP_SUCCESS,
		}
		sendResponse(w, res)
	} else {
		// report failure
		ierr := IndexError{Code: string(RESP_ERROR),
			Msg: err.Error()}

		res := IndexResponse{
			Status: RESP_ERROR,
			Errors: []IndexError{ierr},
		}
		sendResponse(w, res)
	}
}

func convertIndexRequest(r *http.Request) *IndexRequest {
	req := IndexRequest{}
	buf := make([]byte, r.ContentLength)
	logging.Debugf("RequestHandler::convertIndexRequest: request content length %d", len(buf))

	// Body will be non-null but can return EOF if being empty
	if n, err := r.Body.Read(buf); err != nil && int64(n) != r.ContentLength {
		logging.Debugf("RequestHandler::convertIndexRequest: unable to read request body, err %v", err)
		return nil
	}

	if err := json.Unmarshal(buf, &req); err != nil {
		logging.Debugf("RequestHandler::convertIndexRequest: unable to unmarshall request body. Buf = %s, err %v", buf, err)
		return nil
	}

	return &req
}

func (m *requestHandlerContext) convertIndexResponse(r *http.Response) *IndexResponse {

	resp := IndexResponse{}
	buf := make([]byte, r.ContentLength)

	logging.Debugf("RequestHandler::convertIndexResponse: response content length %d", len(buf))

	// Body will be non-null but can return EOF if being empty
	if n, err := r.Body.Read(buf); err != nil && int64(n) != r.ContentLength {
		logging.Debugf("RequestHandler::convertIndexResponse: unable to read response body, err %v", err)
		return nil
	}

	if err := json.Unmarshal(buf, &resp); err != nil {
		logging.Debugf("RequestHandler::convertIndexResponse: unable to unmarshall response body. Buf = %s, err %v", buf, err)
		return nil
	}

	return &resp
}

//////////////////////////////////////////////////////
// Index Status
///////////////////////////////////////////////////////

func (m *requestHandlerContext) handleIndexStatusRequest(w http.ResponseWriter, r *http.Request) {

	list, err := m.getIndexStatus(m.mgr.getServiceAddrProvider().(*common.ClusterInfoCache))
	if err == nil {
		sendResponse(w, *list)
	} else {
		replyWithError(w, "Fail to retrieve cluster-wide metadata from index service")
	}
}

func (m *requestHandlerContext) getIndexStatus(cinfo *common.ClusterInfoCache) (*IndexStatusList, error) {

	// find all nodes that has a index http service
	nids := cinfo.GetNodesByServiceType(common.INDEX_HTTP_SERVICE)

	list := &IndexStatusList{Status: make([]IndexStatus, 0)}

	for _, nid := range nids {

		addr, err := cinfo.GetServiceAddress(nid, common.INDEX_HTTP_SERVICE)
		if err == nil {

			if !strings.HasPrefix(addr, "http://") {
				addr = "http://" + addr + "/getLocalIndexMetadata"
			}

			resp, err := http.Get(addr)
			if err != nil {
				return nil, errors.New(fmt.Sprintf("Fail to retrieve index definition from url %s", addr))
			}

			localMeta := m.convertLocalIndexMetadataResponse(resp)
			if localMeta == nil {
				return nil, errors.New(fmt.Sprintf("Fail to unmarshall index definition from url %s", addr))
			}

			curl, err := cinfo.GetServiceAddress(nid, "mgmt")
			if err != nil {
				return nil, errors.New(fmt.Sprintf("Fail to retrieve mgmt endpoint for index node"))
			}

			for _, defn := range localMeta.IndexDefinitions {

				if topology := m.findTopologyByBucket(localMeta.IndexTopologies, defn.Bucket); topology != nil {
					state := topology.GetStateByDefn(defn.DefnId)

					if state != common.INDEX_STATE_DELETED && state != common.INDEX_STATE_NIL {

						status := IndexStatus{
							DefnId:     defn.DefnId,
							Name:       defn.Name,
							Bucket:     defn.Bucket,
							IsPrimary:  defn.IsPrimary,
							SecExprs:   defn.SecExprs,
							WhereExpr:  defn.WhereExpr,
							Status:     state.String(),
							Host:       curl,
							Completion: 100} // TODO: Get real completion rate from indexer

						list.Status = append(list.Status, status)
					}
				}
			}
		} else {
			return nil, errors.New(fmt.Sprintf("Fail to retrieve http endpoint for index node"))
		}
	}

	return list, nil
}

///////////////////////////////////////////////////////
// ClusterIndexMetadata
///////////////////////////////////////////////////////

func (m *requestHandlerContext) handleIndexMetadataRequest(w http.ResponseWriter, r *http.Request) {

	indexerHostMap := make(map[common.IndexerId]string)
	meta, err := m.getIndexMetadata(m.mgr.getServiceAddrProvider().(*common.ClusterInfoCache), indexerHostMap)
	if err == nil {
		sendResponse(w, *meta)
	} else {
		replyWithError(w, "Fail to retrieve cluster-wide metadata from index service")
	}
}

func (m *requestHandlerContext) getIndexMetadata(cinfo *common.ClusterInfoCache,
	indexerHostMap map[common.IndexerId]string) (*ClusterIndexMetadata, error) {

	// find all nodes that has a index http service
	nids := cinfo.GetNodesByServiceType(common.INDEX_HTTP_SERVICE)

	clusterMeta := &ClusterIndexMetadata{Metadata: make([]LocalIndexMetadata, len(nids))}

	for i, nid := range nids {

		addr, err := cinfo.GetServiceAddress(nid, common.INDEX_HTTP_SERVICE)
		if err == nil {

			if !strings.HasPrefix(addr, "http://") {
				addr = "http://" + addr + "/getLocalIndexMetadata"
			}

			resp, err := http.Get(addr)
			if err != nil {
				return nil, errors.New(fmt.Sprintf("Fail to retrieve index definition from url %s", addr))
			}

			localMeta := m.convertLocalIndexMetadataResponse(resp)
			if localMeta == nil {
				return nil, errors.New(fmt.Sprintf("Fail to unmarshall index definition from url %s", addr))
			}

			indexerHostMap[common.IndexerId(localMeta.IndexerId)] = addr
			clusterMeta.Metadata[i] = *localMeta

		} else {
			return nil, errors.New(fmt.Sprintf("Fail to retrieve http endpoint for index node"))
		}
	}

	return clusterMeta, nil
}

func (m *requestHandlerContext) convertIndexMetadataRequest(r *http.Request) *ClusterIndexMetadata {

	meta := ClusterIndexMetadata{}
	buf := make([]byte, r.ContentLength)
	logging.Debugf("RequestHandler::convertIndexMetadataRequest: request content length %d", len(buf))

	// Body will be non-null but can return EOF if being empty
	if n, err := r.Body.Read(buf); err != nil && int64(n) != r.ContentLength {
		logging.Debugf("RequestHandler::convertClusterIndexMetadataRequest: unable to read request body, err %v", err)
		return nil
	}

	if err := json.Unmarshal(buf, &meta); err != nil {
		logging.Debugf("RequestHandler::convertIndexMetadataRequest: unable to unmarshall request body. Buf = %s, err %v", buf, err)
		return nil
	}

	return &meta
}

///////////////////////////////////////////////////////
// LocalIndexMetadata
///////////////////////////////////////////////////////

func (m *requestHandlerContext) handleLocalIndexMetadataRequest(w http.ResponseWriter, r *http.Request) {

	meta, err := m.getLocalIndexMetadata()
	if err == nil {
		sendResponse(w, *meta)
	} else {
		replyWithError(w, "Fail to retrieve metadata from index service")
	}
}

func (m *requestHandlerContext) getLocalIndexMetadata() (meta *LocalIndexMetadata, err error) {

	repo := m.mgr.getMetadataRepo()

	meta = &LocalIndexMetadata{IndexTopologies: nil, IndexDefinitions: nil}
	indexerId, err := repo.GetLocalIndexerId()
	if err != nil {
		return nil, err
	}
	meta.IndexerId = string(indexerId)

	iter, err := repo.NewIterator()
	if err != nil {
		return nil, err
	}

	var defn *common.IndexDefn
	_, defn, err = iter.Next()
	for err == nil {
		meta.IndexDefinitions = append(meta.IndexDefinitions, *defn)
		_, defn, err = iter.Next()
	}

	iter1, err := repo.NewTopologyIterator()
	if err != nil {
		return nil, err
	}

	var topology *IndexTopology
	topology, err = iter1.Next()
	for err == nil {
		meta.IndexTopologies = append(meta.IndexTopologies, *topology)
		topology, err = iter1.Next()
	}

	return meta, nil
}

func (m *requestHandlerContext) convertLocalIndexMetadataResponse(r *http.Response) *LocalIndexMetadata {

	meta := LocalIndexMetadata{}
	buf := make([]byte, r.ContentLength)

	logging.Debugf("RequestHandler::convertLocalIndexMetadataResponse: response content length %d", len(buf))

	// Body will be non-null but can return EOF if being empty
	if n, err := r.Body.Read(buf); err != nil && int64(n) != r.ContentLength {
		logging.Debugf("RequestHandler::convertLocalIndexMetadataResponse: unable to read response body, err %v", err)
		return nil
	}

	if err := json.Unmarshal(buf, &meta); err != nil {
		logging.Debugf("RequestHandler::convertLocalIndexMetadataResponse: unable to unmarshall response body. Buf = %s, err %v", buf, err)
		return nil
	}

	return &meta
}

///////////////////////////////////////////////////////
// Restore
///////////////////////////////////////////////////////

//
// Restore semantic:
// 1) Each index is associated with the <IndexDefnId, IndexerId>.  IndexDefnId is unique for each index defnition,
//    and IndexerId is unique among the index nodes.  Note that IndexDefnId cannot be reused.
// 2) Index defn exists for the given <IndexDefnId, IndexerId> in current repository.  No action will be applied during restore.
// 3) Index defn is deleted or missing in current repository.  Index Defn restored from backup if bucket exists.
//    - Index defn of the same <bucket, name> exists.   It will rename the index to <index name>_restore_<seqNo>
//    - Bucket does not exist.   It will restore an index defn with a non-existent bucket.
//
func (m *requestHandlerContext) handleRestoreIndexMetadataRequest(w http.ResponseWriter, r *http.Request) {

	image := m.convertIndexMetadataRequest(r)
	if image == nil {
		replyWithError(w, "Fail to marshall index metadata for restore")
	}

	indexerHostMap := make(map[common.IndexerId]string)
	current, err := m.getIndexMetadata(m.mgr.getServiceAddrProvider().(*common.ClusterInfoCache), indexerHostMap)
	if err != nil {
		replyWithError(w, "Unable to get the latest index metadata for restore")
	}

	context := &RestoreContext{idxToRestore: make(map[common.IndexerId][]*common.IndexDefn),
		idxToResolve: make(map[common.IndexerId][]*common.IndexDefn)}

	// Figure out what index to restore that has the same IndexDefnId
	for _, imeta := range image.Metadata {
		found := false
		for _, cmeta := range current.Metadata {
			if imeta.IndexerId == cmeta.IndexerId {
				m.findIndexToRestoreById(&imeta, &cmeta, context)
				found = true
			}
		}

		if !found {
			for _, idefn := range imeta.IndexDefinitions {
				context.idxToResolve[common.IndexerId(imeta.IndexerId)] =
					append(context.idxToResolve[common.IndexerId(imeta.IndexerId)], &idefn)
			}
		}
	}

	// Figure out what index to restore that has the same bucket and name
	for indexerId, idxs := range context.idxToResolve {
		for _, idx := range idxs {
			m.findIndexToRestoreByName(current, idx, indexerId, context)
		}
	}

	// recreate index
	m.restoreIndex(current, context, indexerHostMap)
}

func (m *requestHandlerContext) findIndexToRestoreById(image *LocalIndexMetadata,
	current *LocalIndexMetadata, context *RestoreContext) {

	context.idxToRestore[common.IndexerId(image.IndexerId)] = make([]*common.IndexDefn, 0)
	context.idxToResolve[common.IndexerId(image.IndexerId)] = make([]*common.IndexDefn, 0)

	for _, idefn := range image.IndexDefinitions {
		match := false
		for _, cdefn := range current.IndexDefinitions {
			if idefn.DefnId == cdefn.DefnId {
				match = true
				// find index defn in the current repository, check the status
				if topology := m.findTopologyByBucket(current.IndexTopologies, idefn.Bucket); topology != nil {
					state := topology.GetStateByDefn(idefn.DefnId)
					if state == common.INDEX_STATE_DELETED || state == common.INDEX_STATE_NIL {
						// Index Defn exists it the current repository, but it must be
						// 1) Index Instance does not exist
						// 2) Index Instance has DELETED state
						context.idxToRestore[common.IndexerId(image.IndexerId)] =
							append(context.idxToRestore[common.IndexerId(image.IndexerId)], &idefn)
					}
				}
			}
		}

		if !match {
			// Index Defn does not exist.  Need to find if there is another index with matching <bucket, name>
			context.idxToResolve[common.IndexerId(image.IndexerId)] =
				append(context.idxToResolve[common.IndexerId(image.IndexerId)], &idefn)
		}
	}
}

func (m *requestHandlerContext) findIndexToRestoreByName(current *ClusterIndexMetadata,
	defn *common.IndexDefn, indexerId common.IndexerId, context *RestoreContext) {

	for _, meta := range current.Metadata {
		for _, ldefn := range meta.IndexDefinitions {
			if ldefn.Bucket == defn.Bucket && ldefn.Name == defn.Name {
				if topology := m.findTopologyByBucket(meta.IndexTopologies, ldefn.Bucket); topology != nil {
					state := topology.GetStateByDefn(ldefn.DefnId)
					if state != common.INDEX_STATE_DELETED && state != common.INDEX_STATE_NIL {
						return
					}
				}
			}
		}
	}

	context.idxToRestore[common.IndexerId(indexerId)] = append(context.idxToRestore[common.IndexerId(indexerId)], defn)
}

func (m *requestHandlerContext) restoreIndex(current *ClusterIndexMetadata,
	context *RestoreContext, indexerHostMap map[common.IndexerId]string) {

	indexerCountMap := make(map[common.IndexerId]int)
	for _, meta := range current.Metadata {
		indexerCountMap[common.IndexerId(meta.IndexerId)] = len(meta.IndexDefinitions)
	}

	for indexerId, idxs := range context.idxToRestore {
		for _, defn := range idxs {
			host, ok := indexerHostMap[indexerId]
			if !ok {
				indexerId = m.findMinIndexer(indexerCountMap)
				host = indexerHostMap[indexerId]
			}

			m.makeCreateIndexRequest(defn, host)
		}
	}
}

func (m *requestHandlerContext) makeCreateIndexRequest(defn *common.IndexDefn, host string) {

	id, err := common.NewIndexDefnId()
	if err != nil {
		logging.Debugf("requestHandler.makeCreateIndexRequest(): fail to generate index definition id %v", err)
		return
	}
	defn.DefnId = id

	req := IndexRequest{Version: uint64(1), Type: CREATE, Index: *defn}
	body, err := json.Marshal(req)
	if err != nil {
		logging.Debugf("requestHandler.makeCreateIndexRequest(): cannot marshall create index request %v", err)
		return
	}

	bodybuf := bytes.NewBuffer(body)
	resp, err := http.Post(host+"/createIndex", "application/json", bodybuf)
	if err != nil {
		logging.Debugf("requestHandler.makeCreateIndexRequest(): create index request fails %v", err)
		return
	}

	iresp := m.convertIndexResponse(resp)
	if iresp == nil {
		logging.Debugf("requestHandler.makeCreateIndexRequest(): fail to marshall create index response.")
		return
	}

	if len(iresp.Errors) > 0 {
		logging.Debugf("requestHandler.makeCreateIndexRequest(): create index request fails %v", iresp.Errors[0].Msg)
	}
}

func (m *requestHandlerContext) findTopologyByBucket(topologies []IndexTopology, bucket string) *IndexTopology {

	for _, topology := range topologies {
		if topology.Bucket == bucket {
			return &topology
		}
	}

	return nil
}

func (m *requestHandlerContext) findMinIndexer(indexerCountMap map[common.IndexerId]int) common.IndexerId {

	minIndexerId := common.INDEXER_ID_NIL
	minCount := math.MaxInt32

	for indexerId, count := range indexerCountMap {
		if count < minCount {
			minCount = count
			minIndexerId = indexerId
		}
	}

	return minIndexerId
}

///////////////////////////////////////////////////////
// Utility
///////////////////////////////////////////////////////

func replyWithError(w http.ResponseWriter, msg string) {

	ierr := IndexError{Code: string(RESP_ERROR), Msg: msg}

	res := IndexResponse{
		Status: RESP_ERROR,
		Errors: []IndexError{ierr},
	}

	sendResponse(w, res)
}

func sendResponse(w http.ResponseWriter, res interface{}) {
	logging.Debugf("RequestHandler::sendResponse: sending response back to caller")

	header := w.Header()
	header["Content-Type"] = []string{"application/json"}

	if buf, err := json.Marshal(&res); err == nil {
		w.Write(buf)
	} else {
		// note : buf is nil if err != nil
		sendHttpError(w, "RequestHandler::sendResponse: Unable to marshall response", http.StatusInternalServerError)
	}
}

func sendHttpError(w http.ResponseWriter, reason string, code int) {
	http.Error(w, reason, code)
}
