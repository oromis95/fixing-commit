package indexer

import (
	"fmt"
	c "github.com/couchbase/indexing/secondary/common"
	"github.com/couchbase/indexing/secondary/protobuf"
	"io/ioutil"
	"log"
	"reflect"
	"testing"
	"time"
)

var addr = "localhost:8888"

func TestStreamTimeout(t *testing.T) {
	var client *StreamClient
	var err error

	maxconns, maxvbuckets, mutChanSize := 2, 4, 100
	log.SetOutput(ioutil.Discard)

	// start server
	msgch := make(chan interface{}, mutChanSize)
	errch := make(chan interface{}, 1000)
	daemon := doServer(addr, t, msgch, errch, 100)
	flags := StreamTransportFlag(0).SetProtobuf()
	vbmap := makeVbmap(maxvbuckets) // vbmap

	// start client and test timeouts
	if client, err = NewStreamClient(addr, maxconns, flags); err != nil {
		t.Fatal(err)
	}
	if err := client.SendVbmap(vbmap); err != nil {
		t.Fatal(err)
	}
	verify(msgch, errch, func(mutn, err interface{}) {
		if msg, ok := (err).(RestartVbuckets); ok { // check
			if len([]uint64(msg)) != maxvbuckets {
				t.Fatal(fmt.Errorf("mismatch in restart vbuckets %v", msg))
			}
		} else {
			t.Fatal(fmt.Errorf("expected restart vbuckets"))
		}
	})
	client.Close()
	daemon.Close()
}

func TestStreamLoopback(t *testing.T) {
	var client *StreamClient
	var err error

	maxconns, maxvbuckets, mutChanSize := 2, 4, 100
	log.SetOutput(ioutil.Discard)

	// start server
	msgch := make(chan interface{}, mutChanSize)
	errch := make(chan interface{}, 1000)
	daemon := doServer(addr, t, msgch, errch, 100)
	flags := StreamTransportFlag(0).SetProtobuf()
	vbmap := makeVbmap(maxvbuckets) // vbmap

	// start client and test loopback
	if client, err = NewStreamClient(addr, maxconns, flags); err != nil {
		t.Fatal(err)
	}
	if err := client.SendVbmap(vbmap); err != nil {
		t.Fatal(err)
	}
	k := c.NewUpsert(1, 1, []byte("cities"), 10000000)
	k.Keys = [][]byte{[]byte("bangalore"), []byte("delhi"), []byte("jaipur")}
	k.Oldkeys = [][]byte{[]byte("varanasi"), []byte("pune"), []byte("mahe")}
	k.Uuids = []uint64{1, 2, 3}
	count := 10000
	for i := 1; i <= count; i += 2 {
		for j := 0; j < maxvbuckets; j++ {
			l := *k
			k.Vbucket = uint16(j)
			k.Vbuuid = uint64(j * 10)
			k.Seqno = uint64(i)
			l.Vbucket = uint16(j)
			l.Vbuuid = uint64(j * 10)
			l.Seqno = uint64(i + 1)
			err := client.SendKeyVersions([]*c.KeyVersions{k, &l})
			if err != nil {
				t.Fatal(err)
			}
			verify(msgch, errch, func(mutn, err interface{}) {
				if err != nil {
					t.Fatal(err)
				}
				if ks, ok := mutn.([]*protobuf.KeyVersions); !ok {
					err := fmt.Errorf("unexpected type in loopback %T:%v", ks, ks)
					t.Fatal(err)
				} else {
					if len(ks) != 2 {
						t.Fatal(fmt.Errorf("mismatch in number of key versions"))
					}
					if ks[0].GetSeqno() != uint64(i) || ks[1].GetSeqno() != uint64(i+1) {
						t.Fatal(fmt.Errorf("unexpected response"))
					}
					ksLb := protobuf2KeyVersions(ks)
					if reflect.DeepEqual(ksLb[0], k) == false {
						t.Fatal(fmt.Errorf("unexpected response"))
					}
				}
			})
		}
	}

	client.Close()
	daemon.Close()
}

func TestStreamDropData(t *testing.T) {
	var client *StreamClient
	var err error

	maxconns, maxvbuckets, mutChanSize := 1, 1, 100
	log.SetOutput(ioutil.Discard)

	// start server
	msgch := make(chan interface{}, mutChanSize)
	errch := make(chan interface{}, 1000)
	daemon := doServer(addr, t, msgch, errch, 100)
	flags := StreamTransportFlag(0).SetProtobuf()
	vbmap := makeVbmap(maxvbuckets) // vbmap

	// start client and test drop data
	if client, err = NewStreamClient(addr, maxconns, flags); err != nil {
		t.Fatal(err)
	}
	if err := client.SendVbmap(vbmap); err != nil {
		t.Fatal(err)
	}
	endch := make(chan bool)
	go func() {
		done := false
		for {
			verify(msgch, errch, func(mutn, err interface{}) {
				if val, ok := err.(DropVbucketData); ok {
					vbnos := []uint64(val)
					if len(vbnos) == 1 && vbnos[0] == 0 {
						close(endch)
						done = true
					} else {
						t.Fatal("mismatch in dropdata")
					}
				}
				time.Sleep(100 * time.Microsecond)
			})
			if done {
				return
			}
		}
	}()
	k := c.NewUpsert(1, 1, []byte("cities"), 10000000)
	k.Keys = [][]byte{[]byte("bangalore"), []byte("delhi"), []byte("jaipur")}
	k.Oldkeys = [][]byte{[]byte("varanasi"), []byte("pune"), []byte("mahe")}
	k.Uuids = []uint64{1, 2, 3}
	i := 0
loop:
	for {
		kk := *k
		kk.Vbucket = uint16(0)
		kk.Vbuuid = uint64(0)
		kk.Seqno = uint64(i)
		err := client.SendKeyVersions([]*c.KeyVersions{&kk})
		if err != nil {
			t.Fatal(err)
		}
		select {
		case <-endch:
			break loop
		default:
		}
		i++
	}
	client.Close()
	daemon.Close()
}

func BenchmarkLoopback(b *testing.B) {
	var client *StreamClient
	var err error

	maxconns, maxvbuckets, mutChanSize := 2, 4, 10000
	log.SetOutput(ioutil.Discard)

	// start server
	msgch := make(chan interface{}, mutChanSize)
	errch := make(chan interface{}, 1000)
	daemon := doServer(addr, b, msgch, errch, mutChanSize)
	flags := StreamTransportFlag(0).SetProtobuf()
	vbmap := makeVbmap(maxvbuckets) // vbmap

	// start client and test loopback
	if client, err = NewStreamClient(addr, maxconns, flags); err != nil {
		b.Fatal(err)
	}
	if err := client.SendVbmap(vbmap); err != nil {
		b.Fatal(err)
	}
	k := c.NewUpsert(1, 1, []byte("cities"), 10000000)
	k.Keys = [][]byte{[]byte("bangalore"), []byte("delhi"), []byte("jaipur")}
	k.Oldkeys = [][]byte{[]byte("varanasi"), []byte("pune"), []byte("mahe")}
	k.Uuids = []uint64{1, 2, 3}

	go func() {
		for {
			select {
			case <-msgch:
			case <-errch:
			}
		}
	}()

	b.ResetTimer()
	for i := 0; i <= b.N; i++ {
		j := i % maxvbuckets
		kk := *k
		kk.Vbucket = uint16(j)
		kk.Vbuuid = uint64(j * 10)
		kk.Seqno = uint64(i)
		client.SendKeyVersions([]*c.KeyVersions{&kk})
	}

	client.Close()
	daemon.Close()
}

func doServer(addr string, tb testing.TB, msgch, errch chan interface{}, mutChanSize int) *MutationStream {
	var mStream *MutationStream
	var err error

	mutch := make(chan []*protobuf.KeyVersions, mutChanSize)
	sbch := make(chan interface{}, 100)
	if mStream, err = NewMutationStream(addr, mutch, sbch); err != nil {
		tb.Fatal(err)
	}

	go func() {
		var mutn, err interface{}
		var ok bool
		for {
			select {
			case mutn, ok = <-mutch:
				msgch <- mutn
			case err, ok = <-sbch:
				errch <- err
			}
			if ok == false {
				return
			}
		}
	}()
	return mStream
}

func makeVbmap(maxvbuckets int) *c.VbConnectionMap {
	vbmap := &c.VbConnectionMap{
		Bucket:   "default",
		Vbuckets: make([]uint16, 0, maxvbuckets),
		Vbuuids:  make([]uint64, 0, maxvbuckets),
	}
	for i := 0; i < maxvbuckets; i++ {
		vbmap.Vbuckets = append(vbmap.Vbuckets, uint16(i))
		vbmap.Vbuuids = append(vbmap.Vbuuids, uint64(i*10))
	}
	return vbmap
}

func verify(msgch, errch chan interface{}, fn func(mutn, err interface{})) {
	select {
	case mutn := <-msgch:
		fn(mutn, nil)
	case err := <-errch:
		fn(nil, err)
	}
}
