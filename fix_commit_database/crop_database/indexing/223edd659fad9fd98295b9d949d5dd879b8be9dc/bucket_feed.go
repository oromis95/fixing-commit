// concurrency model:
//                                                           Engines
//                                                         E1  E2 ... En
//                             *---deleteKVFeed() ----*   ----------------
//                             |  (sbKVFeedClosed)    |     ^   ^      ^
//                             |                      |     |   |      |
//                             |  NewBucketFeed()     |    (MutationEvent)
//                             |       |  |           |     |   |      |
//                             |      (spawn)         |     |   |      |
//                             |       |  |           |     |   |      |
//                             |       |  *---------- mainGatherScatter()
//                             |       |                 ^     ^
//                             |       |                 |     |
//    RequestFeed() -----*-*-*-*---> genServer()-- sidebandCh  |
//              |        | | |                                 |
//  <--failTs,kvTs       | | |                                 | MutationEvent
//                       | | |                                 | sbKVFeedClosed
//     CloseFeed() ------* | |                             ---------------------
//                         | |                               upstream KVFeed
//   UpdateEngines() ------* |
//                           |
//   DeleteEngines() --------*
//
// Notes:
//
// - new bucket-feed spawns a gen-server routine for control path and
//   gather-scatter routine for data path.
// - RequestFeed can start, restart or shutdown one or more vbuckets across
//   kv-nodes.
// - for a successful RequestFeed,
//   - failover-timestamp, restart-timestamp from different kv-nodes
//     (containing an exlusive set of vbuckets) will be aggregated into a
//     single failover-timestamp and restart-timestamp and return back.
//   - if request is to shutdown vbuckets, failover-timetamp and
//     restart-timetamp will be empty.
// - when data path receives *sbKVFeedClosed, it will notify gen-server to
//   delete KVFeed.
// - when data path receives *MutationEvent, it will scatter the mutation-event
//   to all engines instantiated for that bucket.

package projector

import (
	c "github.com/couchbase/indexing/secondary/common"
	"log"
	"sort"
)

const ()

// BucketFeed is per bucket, multiple kv-node feeds, for a subset of vbuckets.
type BucketFeed struct {
	feed    *Feed
	bucketn string
	pooln   string
	kvfeeds map[string]*KVFeed // kvaddr -> *KVFeed
	eventch chan interface{}   // carries *MutationEvent and *sbKVFeedClosed
	// gen-server
	reqch chan []interface{}
	finch chan bool
}

type sbBfeedUpdateEngines map[uint64]*Engine
type sbBfeedDeleteEngines []uint64
type sbBfeedExit byte

// NewBucketFeed creates a new instance of feed for specified bucket. Spawns a
// routine for gen-server, another routine to gather MutationEvents from
// upstream KVFeed instances and pipe them to corresponding router-routines.
//
// if error, BucketFeed is not created.
// - error returned by couchbase client, via NewKVFeed()
func NewBucketFeed(
	feed *Feed,
	kvaddrs []string, // if co-located, len(kvaddrs) equals 1
	pooln, bucketn string) (bfeed *BucketFeed, err error) {

	bfeed = &BucketFeed{
		feed:    feed,
		bucketn: bucketn,
		pooln:   pooln,
		kvfeeds: make(map[string]*KVFeed),
		eventch: make(chan interface{}, c.MutationChannelSize),
		reqch:   make(chan []interface{}, c.GenserverChannelSize),
		finch:   make(chan bool),
	}

	// initialize KVFeeds
	for _, kvaddr := range kvaddrs {
		kvfeed, err := NewKVFeed(bfeed, kvaddr, pooln, bucketn, bfeed.eventch)
		if err != nil {
			bfeed.doClose(nil)
			return nil, err
		}
		bfeed.kvfeeds[kvaddr] = kvfeed
	}
	sidebandCh := make(chan []interface{}) // both side-band and kill command
	go bfeed.genServer(bfeed.reqch, sidebandCh)
	go bfeed.runGatherScatter(bfeed.eventch, sidebandCh)
	return bfeed, nil
}

func (bfeed *BucketFeed) getFeed() *Feed {
	return bfeed.feed
}

// gen-server API commands
const (
	bfCmdRequestFeed byte = iota + 1
	bfCmdCloseFeed
	bfCmdDeleteKVFeed
	bfCmdUpdateEngines
	bfCmdDeleteEngines
)

// RequestFeed will gather MutationEvent from one or more KVFeeds. Used by Feed
// to start a new mutation feed. `engines` can only be added when specified as
// part of RequestFeed.
//
// returns failover-timetamp and kv-timestamp (restart seqno. after honoring
// rollback)
// - ErrorInvalidRequest if request is malformed.
// - error returned by couchbase client.
// - error if BucketFeed is already closed.
func (bfeed *BucketFeed) RequestFeed(
	request RequestReader,
	engines map[uint64]*Engine) (*c.Timestamp, *c.Timestamp, error) {

	respch := make(chan []interface{}, 1)
	cmd := []interface{}{bfCmdRequestFeed, request, engines, respch}
	resp, err := c.FailsafeOp(bfeed.reqch, respch, cmd, bfeed.finch)
	if err != nil {
		return nil, nil, err
	}
	if resp[2] == nil {
		failoverTs, kvTs := resp[0].(*c.Timestamp), resp[1].(*c.Timestamp)
		return failoverTs, kvTs, nil
	}
	return nil, nil, resp[2].(error)
}

// UpdateFeed will start, restart and shutdown vbucket streams for this active
// feed. To atomically update downstream projection and routing for index,
// supply a list of indexes with updated routing tables.
//
// returns failover-timetamp and kv-timestamp (restart seqno. after honoring
// rollback)
// - ErrorInvalidRequest if request is malformed.
// - error returned by couchbase client.
// - error if BucketFeed is already closed.
func (bfeed *BucketFeed) UpdateFeed(
	request RequestReader,
	engines map[uint64]*Engine) (failoverTs, kvTs *c.Timestamp, err error) {

	respch := make(chan []interface{}, 1)
	cmd := []interface{}{bfCmdRequestFeed, request, engines, respch}
	resp, err := c.FailsafeOp(bfeed.reqch, respch, cmd, bfeed.finch)
	if err != nil {
		return nil, nil, err
	}
	if resp[2] == nil {
		failoverTs, kvTs = resp[0].(*c.Timestamp), resp[1].(*c.Timestamp)
		return failoverTs, kvTs, nil
	}
	return nil, nil, resp[2].(error)
}

// UpdateEngines will update downstream projection and routing this BucketFeed.
//
// - error if BucketFeed is already closed.
func (bfeed *BucketFeed) UpdateEngines(engines map[uint64]*Engine) error {
	respch := make(chan []interface{}, 1)
	cmd := []interface{}{bfCmdUpdateEngines, engines}
	_, err := c.FailsafeOp(bfeed.reqch, respch, cmd, bfeed.finch)
	return err
}

// DeleteEngines from active set of engines for this BucketFeed.
//
// - error if BucketFeed is already closed.
func (bfeed *BucketFeed) DeleteEngines(engines []uint64) error {
	respch := make(chan []interface{}, 1)
	cmd := []interface{}{bfCmdDeleteEngines, engines}
	_, err := c.FailsafeOp(bfeed.reqch, respch, cmd, bfeed.finch)
	return err
}

// deleteKVFeed for `kvaddr`. Downstream will had to timeout on vbucket
// inactivity and take appropriate action.
//
// - error if BucketFeed is already closed.
func (bfeed *BucketFeed) deleteKVFeed(kvaddr string) error {
	respch := make(chan []interface{}, 1)
	cmd := []interface{}{bfCmdDeleteKVFeed, kvaddr}
	_, err := c.FailsafeOp(bfeed.reqch, respch, cmd, bfeed.finch)
	return err
}

// CloseFeed will close all upstream KVFeed connected to this Bucket. Note that
// this feed can get closed automatically due to upstream error.
//
// - error if BucketFeed is already closed.
func (bfeed *BucketFeed) CloseFeed() error {
	respch := make(chan []interface{}, 1)
	cmd := []interface{}{bfCmdCloseFeed, respch}
	_, err := c.FailsafeOp(bfeed.reqch, respch, cmd, bfeed.finch)
	return err
}

// routine handles control path.
func (bfeed *BucketFeed) genServer(reqch chan []interface{}, sidebandCh chan []interface{}) {
	defer func() { // panic safe
		if r := recover(); r != nil {
			log.Printf("BucketFeed:genServer() crashed `%v`\n", r)
			bfeed.doClose(sidebandCh)
		}
	}()

	sendSideBand := func(info interface{}) {
		respch := make(chan bool)
		sidebandCh <- []interface{}{info, respch}
		<-respch
	}

loop:
	for {
		msg := <-reqch
		switch msg[0].(byte) {
		case bfCmdRequestFeed:
			req := msg[1].(RequestReader)
			engines := msg[2].(map[uint64]*Engine)
			respch := msg[3].(chan []interface{})
			sendSideBand(sbBfeedUpdateEngines(engines))
			failTs, kvTs, err := bfeed.requestFeed(req)
			respch <- []interface{}{failTs, kvTs, err}

		case bfCmdUpdateEngines:
			engines := msg[1].(map[uint64]*Engine)
			sendSideBand(sbBfeedUpdateEngines(engines))

		case bfCmdDeleteEngines:
			engines := msg[1].([]uint64)
			sendSideBand(sbBfeedDeleteEngines(engines))

		case bfCmdDeleteKVFeed:
			delete(bfeed.kvfeeds, msg[1].(string))

		case bfCmdCloseFeed:
			respch := msg[1].(chan error)
			respch <- bfeed.doClose(sidebandCh)
			break loop
		}
	}
}

// request a new feed or start, restart and shutdown upstream vbuckets and/or
// update downstream engines.
func (bfeed *BucketFeed) requestFeed(req RequestReader) (failTs, kvTs *c.Timestamp, err error) {
	var fTs, kTs *c.Timestamp
	failTs = c.NewTimestamp(bfeed.bucketn, c.MaxVbuckets)
	kvTs = c.NewTimestamp(bfeed.bucketn, c.MaxVbuckets)
	for _, kvfeed := range bfeed.kvfeeds {
		if fTs, kTs, err = kvfeed.RequestFeed(req); err != nil {
			return nil, nil, err
		}
		failTs = failTs.Union(fTs)
		kvTs = kvTs.Union(kTs)
	}
	sort.Sort(failTs)
	sort.Sort(kvTs)
	return failTs, kvTs, nil
}

// execute close
func (bfeed *BucketFeed) doClose(sidebandCh chan []interface{}) (err error) {
	// close and wait for woker routine runGatherScatter to exit.
	if sidebandCh != nil {
		respch := make(chan bool)
		sidebandCh <- []interface{}{sbBfeedExit(0), respch}
		<-respch
	}
	// proceed closing the upstream
	for _, kvfeed := range bfeed.kvfeeds {
		kvfeed.CloseFeed()
	}
	// close the gen-server
	close(bfeed.finch)
	bfeed.kvfeeds = nil
	bfeed.feed.CloseFeed()
	return
}

// routine handles data-path
func (bfeed *BucketFeed) runGatherScatter(eventch <-chan interface{}, sidebandCh chan []interface{}) {
	defer func() { // panic safe
		if r := recover(); r != nil {
			log.Printf("BucketFeed:runGatherScatter() crashed `%v`\n", r)
			go bfeed.CloseFeed()
		}
	}()

	engines := make(map[uint64]*Engine)
loop:
	for {
		select {
		case event := <-eventch:
			switch val := event.(type) {
			case *MutationEvent:
				for _, engine := range engines {
					engine.Event(val)
				}

			case *sbKVFeedClosed:
				log.Printf("KVFeed %q exited: `%v`\n", val.kvaddr, val.err)
				bfeed.deleteKVFeed(val.kvaddr)
			}

		case msg := <-sidebandCh:
			val, respch := msg[0].(interface{}), msg[1].(chan bool)
			switch info := val.(type) {
			case sbBfeedUpdateEngines:
				for uuid, engine := range info {
					if e, ok := engines[uuid]; ok {
						e.Close() // delete older version of engine
						delete(engines, uuid)
					}
					engines[uuid] = engine // add engine
				}

			case sbBfeedDeleteEngines:
				for _, uuid := range info {
					engines[uuid].Close() // delete engine
					delete(engines, uuid)
				}

			case sbBfeedExit:
				break loop
			}
			close(respch)
		}
	}
}
