package protobuf

import (
	"fmt"

	mc "github.com/couchbase/gomemcached/client"
	c "github.com/couchbase/indexing/secondary/common"
)

// IndexEvaluator implements `Evaluator` interface for protobuf
// definition of an index instance.
type IndexEvaluator struct {
	skExprs  []interface{} // compiled expression
	pkExpr   interface{}   // compiled expression
	instance *IndexInst
}

// NewIndexEvaluator returns a reference to a new instance
// of IndexEvaluator.
func NewIndexEvaluator(instance *IndexInst) (*IndexEvaluator, error) {
	var err error

	ie := &IndexEvaluator{instance: instance}
	// compile expressions once and reuse it many times.
	defn := ie.instance.GetDefinition()
	switch defn.GetExprType() {
	case ExprType_JavaScript:
	case ExprType_N1QL:
		ie.skExprs, err = c.CompileN1QLExpression(defn.GetSecExpressions())
		if err != nil {
			return nil, err
		}
		expr := defn.GetPartnExpression()
		cExprs, err := c.CompileN1QLExpression([]string{expr})
		if err != nil {
			return nil, err
		}
		ie.pkExpr = cExprs[0]
	}
	return ie, nil
}

// Bucket implements Evaluator{} interface.
func (ie *IndexEvaluator) Bucket() string {
	return ie.instance.GetDefinition().GetBucket()
}

// StreamBeginData implement Evaluator{} interface.
func (ie *IndexEvaluator) StreamBeginData(
	vbno uint16, vbuuid, seqno uint64) (data interface{}) {

	bucket := ie.Bucket()
	kv := c.NewKeyVersions(seqno, nil, 1)
	kv.AddStreamBegin()
	return &c.DataportKeyVersions{bucket, vbno, vbuuid, kv}
}

// SyncData implement Evaluator{} interface.
func (ie *IndexEvaluator) SyncData(
	vbno uint16, vbuuid, seqno uint64) (data interface{}) {

	bucket := ie.Bucket()
	kv := c.NewKeyVersions(seqno, nil, 1)
	kv.AddSync()
	return &c.DataportKeyVersions{bucket, vbno, vbuuid, kv}
}

// SnapshotData implement Evaluator{} interface.
func (ie *IndexEvaluator) SnapshotData(
	m *mc.UprEvent, vbno uint16, vbuuid, seqno uint64) (data interface{}) {

	bucket := ie.Bucket()
	kv := c.NewKeyVersions(seqno, nil, 1)
	kv.AddSnapshot(m.SnapshotType, m.SnapstartSeq, m.SnapendSeq)
	return &c.DataportKeyVersions{bucket, vbno, vbuuid, kv}
}

// StreamEndData implement Evaluator{} interface.
func (ie *IndexEvaluator) StreamEndData(
	vbno uint16, vbuuid, seqno uint64) (data interface{}) {

	bucket := ie.Bucket()
	kv := c.NewKeyVersions(seqno, nil, 1)
	kv.AddStreamEnd()
	return &c.DataportKeyVersions{bucket, vbno, vbuuid, kv}
}

// TransformRoute implement Evaluator{} interface.
func (ie *IndexEvaluator) TransformRoute(
	vbuuid uint64,
	m *mc.UprEvent) (endpoints map[string]interface{}, err error) {

	defer func() { // panic safe
		if r := recover(); r != nil {
			err = fmt.Errorf("%v", r)
		}
	}()

	var pkey, nkey, okey []byte

	if len(m.Value) > 0 { // project new secondary key
		if pkey, err = ie.partitionKey(m.Key, m.Value); err != nil {
			return nil, err
		}
		if nkey, err = ie.evaluate(m.Key, m.Value); err != nil {
			return nil, err
		}
	}
	if len(m.OldValue) > 0 { // project old secondary key
		if okey, err = ie.evaluate(m.Key, m.OldValue); err != nil {
			return nil, err
		}
	}

	vbno, seqno, docid := m.VBucket, m.Seqno, m.Key // Key is Docid
	uuid := ie.instance.GetInstId()

	var kv *c.KeyVersions

	endpoints = make(map[string]interface{})
	bucket := ie.Bucket()

	switch m.Opcode {
	case mc.UprMutation:
		// Upsert
		raddrs := ie.instance.UpsertEndpoints(
			vbno, seqno, docid, pkey, nkey, okey)
		for _, raddr := range raddrs {
			if _, ok := endpoints[raddr]; !ok {
				kv = c.NewKeyVersions(seqno, m.Key, 4)
			}
			kv.AddUpsert(uuid, nkey, okey)
			endpoints[raddr] = &c.DataportKeyVersions{bucket, vbno, vbuuid, kv}
		}
		// UpsertDeletion
		raddrs = ie.instance.UpsertDeletionEndpoints(
			vbno, seqno, docid, pkey, nkey, okey)
		for _, raddr := range raddrs {
			if _, ok := endpoints[raddr]; !ok {
				kv = c.NewKeyVersions(seqno, m.Key, 4)
			}
			kv.AddUpsertDeletion(uuid, okey)
			endpoints[raddr] = &c.DataportKeyVersions{bucket, vbno, vbuuid, kv}
		}

	case mc.UprDeletion:
		raddrs := ie.instance.DeletionEndpoints(vbno, seqno, docid, pkey, okey)
		for _, raddr := range raddrs {
			if _, ok := endpoints[raddr]; !ok {
				kv = c.NewKeyVersions(seqno, m.Key, 4)
			}
			kv.AddDeletion(uuid, okey)
			endpoints[raddr] = &c.DataportKeyVersions{bucket, vbno, vbuuid, kv}
		}
	}
	return endpoints, nil
}

func (ie *IndexEvaluator) evaluate(docid, doc []byte) (skey []byte, err error) {
	defn := ie.instance.GetDefinition()
	if defn.GetIsPrimary() { // primary index supported !!
		return docid, nil // by saying primary-key is secondary-key
	}

	exprType := defn.GetExprType()
	switch exprType {
	case ExprType_JavaScript:
	case ExprType_N1QL:
		skey, err = c.N1QLTransform(doc, ie.skExprs)
	}
	return skey, err
}

func (ie *IndexEvaluator) partitionKey(docid, doc []byte) (pKey []byte, err error) {
	defn := ie.instance.GetDefinition()
	if defn.GetIsPrimary() { // TODO: strategy for primary index ???
		return nil, nil
	}

	exprType := defn.GetExprType()
	switch exprType {
	case ExprType_JavaScript:
	case ExprType_N1QL:
		pKey, err = c.N1QLTransform(doc, []interface{}{ie.pkExpr})
	}
	return pKey, err
}

// Bucket implements Router{} interface.
func (instance *IndexInst) Bucket() string {
	return instance.GetDefinition().GetBucket()
}

// Endpoints implements Router{} interface.
func (instance *IndexInst) Endpoints() []string {
	if tp := instance.getTestPartitionScheme(); tp != nil {
		return tp.GetEndpoints()
	}
	return []string{}
}

// UpsertEndpoints implements Router{} interface.
func (instance *IndexInst) UpsertEndpoints(
	vbno uint16, seqno uint64, docid, partKey, key, oldKey []byte) []string {

	if tp := instance.getTestPartitionScheme(); tp != nil {
		return tp.GetEndpoints()
	}
	return []string{}
}

// UpsertDeletionEndpoints implements Router{} interface.
func (instance *IndexInst) UpsertDeletionEndpoints(
	vbno uint16, seqno uint64, docid, partKey, key, oldKey []byte) []string {

	if tp := instance.getTestPartitionScheme(); tp != nil {
		return tp.GetEndpoints()
	}
	return []string{}
}

// DeletionEndpoints implements Router{} interface.
func (instance *IndexInst) DeletionEndpoints(
	vbno uint16, seqno uint64, docid, partKey, oldKey []byte) []string {

	if tp := instance.getTestPartitionScheme(); tp != nil {
		return tp.GetEndpoints()
	}
	return []string{}
}

// -----

func (instance *IndexInst) getTestPartitionScheme() *TestPartition {
	defn := instance.GetDefinition()
	if defn.GetPartitionScheme() == PartitionScheme_TEST {
		return instance.GetTp()
	}
	return nil
}
