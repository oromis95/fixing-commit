// data-path concurrency model:
//
//               back-channel
//     feed <---------------------*   NewKVData()
//                StreamRequest   |     |            *---> vbucket
//                    StreamEnd   |   (spawn)        |
//                                |     |            *---> vbucket
//                                |     |            |
//        AddEngines() --*-----> runScatter ---------*---> vbucket
//                       |
//     DeleteEngines() --*
//                       |
//     GetStatistics() --*
//                       |
//             Close() --*
package projector

import "fmt"
import "strconv"

import mc "github.com/couchbase/gomemcached/client"
import c "github.com/couchbase/indexing/secondary/common"

// KVData captures an instance of data-path for single kv-node
// from upstream connection.
type KVData struct {
	feed   *Feed
	topic  string // immutable
	bucket string // immutable
	kvaddr string // immutable
	vrs    map[uint16]*VbucketRoutine
	// evaluators and subscribers
	engines   map[uint64]*Engine
	endpoints map[string]c.RouterEndpoint
	// server channels
	sbch  chan []interface{}
	finch chan bool
	// misc.
	logPrefix string
}

// NewKVData create a new data-path instance.
func NewKVData(
	feed *Feed, bucket, kvaddr string, mutch <-chan *mc.UprEvent) *KVData {

	kvdata := &KVData{
		feed:      feed,
		topic:     feed.topic,
		bucket:    bucket,
		kvaddr:    kvaddr,
		vrs:       make(map[uint16]*VbucketRoutine),
		engines:   make(map[uint64]*Engine),
		endpoints: make(map[string]c.RouterEndpoint),
		sbch:      make(chan []interface{}, 16), // TODO: avoid magic number
		finch:     make(chan bool),
		logPrefix: fmt.Sprintf("[%v->%v->%v]", feed.topic, bucket, kvaddr),
	}
	go kvdata.runScatter(mutch)
	c.Infof("%v started ...\n", kvdata.logPrefix)
	return kvdata
}

// commands to server
const (
	kvCmdAddEngines byte = iota + 1
	kvCmdDelEngines
	kvCmdGetStats
	kvCmdClose
)

// AddEngines and endpoints, synchronous call.
func (this *KVData) AddEngines(
	engines map[uint64]*Engine, endpoints map[string]c.RouterEndpoint) error {

	respch := make(chan []interface{}, 1)
	cmd := []interface{}{kvCmdAddEngines, engines, endpoints, respch}
	_, err := c.FailsafeOp(this.sbch, respch, cmd, this.finch)
	return err
}

// DeleteEngines, synchronous call.
func (this *KVData) DeleteEngines(engineKeys []uint64) error {
	respch := make(chan []interface{}, 1)
	cmd := []interface{}{kvCmdDelEngines, engineKeys, respch}
	_, err := c.FailsafeOp(this.sbch, respch, cmd, this.finch)
	return err
}

// GetStatistics from kv data path, synchronous call.
func (this *KVData) GetStatistics() map[string]interface{} {
	respch := make(chan []interface{}, 1)
	cmd := []interface{}{kvCmdGetStats, respch}
	resp, _ := c.FailsafeOp(this.sbch, respch, cmd, this.finch)
	return resp[0].(map[string]interface{})
}

// Close this kv data path, synchronous call.
func (this *KVData) Close() error {
	respch := make(chan []interface{}, 1)
	cmd := []interface{}{kvCmdClose, respch}
	_, err := c.FailsafeOp(this.sbch, respch, cmd, this.finch)
	return err
}

// go-routine handles data path.
func (this *KVData) runScatter(mutch <-chan *mc.UprEvent) {
	this.vrs = make(map[uint16]*VbucketRoutine)
	stats := this.newStats()

	defer func() {
		if r := recover(); r != nil {
			msg := "%v for %q runScatter() panic: %v\n"
			c.Errorf(msg, this.logPrefix, this.kvaddr, r)
			// TODO: print stack.
		}
		close(this.finch)
		c.Infof("%v for %q ... stopped\n", this.logPrefix, this.kvaddr)
	}()

	eventCount := 0

loop:
	for {
		select {
		case m, ok := <-mutch:
			if ok == false { // upstream has closed
				break loop
			}
			this.scatterMutation(m)
			eventCount++

		case msg := <-this.sbch:
			cmd := msg[0].(byte)
			switch cmd {
			case kvCmdAddEngines:
				respch := msg[3].(chan []interface{})
				if msg[1] != nil {
					for uuid, engine := range msg[1].(map[uint64]*Engine) {
						this.engines[uuid] = engine
					}
				}
				if msg[2] != nil {
					rv := msg[2].(map[string]c.RouterEndpoint)
					for raddr, endp := range rv {
						this.endpoints[raddr] = endp
					}
				}
				if this.engines != nil || this.endpoints != nil {
					for _, vr := range this.vrs {
						vr.AddEngines(this.engines, this.endpoints)
					}
				}
				respch <- []interface{}{nil}

			case kvCmdDelEngines:
				engineKeys := msg[1].([]uint64)
				respch := msg[2].(chan []interface{})
				for _, vr := range this.vrs {
					vr.DeleteEngines(engineKeys)
				}
				for _, engineKey := range engineKeys {
					delete(this.engines, engineKey)
				}
				respch <- []interface{}{nil}

			case kvCmdGetStats:
				respch := msg[1].(chan []interface{})
				statVbuckets := make(map[string]interface{})
				for i, vr := range this.vrs {
					statVbuckets[strconv.Itoa(int(i))] = vr.GetStatistics()
				}
				stats.Set("vbuckets", statVbuckets)
				respch <- []interface{}{map[string]interface{}(stats)}

			case kvCmdClose:
				respch := msg[1].(chan []interface{})
				respch <- []interface{}{nil}
				break loop
			}
		}
	}
}

func (this *KVData) scatterMutation(m *mc.UprEvent) (err error) {
	vbno := m.VBucket

	switch m.Opcode {
	case mc.UprStreamRequest:
		if _, ok := this.vrs[vbno]; !ok {
			if m.VBuuid, m.Seqno, err = m.FailoverLog.Latest(); err == nil {
				topic, bucket, kv := this.topic, this.bucket, this.kvaddr
				vr := NewVbucketRoutine(topic, bucket, kv, vbno, m.VBuuid)
				vr.AddEngines(this.engines, this.endpoints)
				vr.Event(m)
				this.vrs[vbno] = vr

			} else {
				c.Errorf("%v vbucket(%v) %v", this.logPrefix, vbno, err)
			}
			this.feed.PostStreamRequest(this.bucket, this.kvaddr, m)

		} else {
			msg := "%v duplicate OpStreamRequest for %v\n"
			c.Errorf(msg, this.logPrefix, vbno)
		}

	case mc.UprStreamEnd:
		if vr, ok := this.vrs[vbno]; ok {
			vr.Event(m)
			delete(this.vrs, vbno)
			this.feed.PostStreamEnd(this.bucket, this.kvaddr, m)

		} else {
			c.Errorf("%v duplicate OpStreamEnd for %v\n", this.logPrefix, vbno)
		}

	case mc.UprMutation, mc.UprDeletion, mc.UprSnapshot:
		if vr, ok := this.vrs[vbno]; ok {
			if vr.vbuuid == m.VBuuid {
				vr.Event(m)

			} else {
				msg := "%v vbuuid mismatch (%v:%v) for vbucket %v\n"
				c.Errorf(msg, this.logPrefix, vr.vbuuid, m.VBuuid, vbno)
				delete(this.vrs, vbno)
			}

		} else {
			c.Errorf("%v unknown vbucket %v\n", vbno)
		}
	}
	return
}

func (this *KVData) newStats() c.Statistics {
	statVbuckets := make(map[string]interface{})
	m := map[string]interface{}{
		"events":   float64(0),   // no. of mutations events received
		"vbuckets": statVbuckets, // per vbucket statistics
	}
	stats, _ := c.NewStatistics(m)
	return stats
}
