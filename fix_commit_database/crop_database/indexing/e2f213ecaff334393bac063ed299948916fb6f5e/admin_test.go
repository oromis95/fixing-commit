package adminport

import (
	"code.google.com/p/goprotobuf/proto"
	"fmt"
	"github.com/couchbase/indexing/secondary/protobuf"
	"io/ioutil"
	"log"
	"reflect"
	"testing"
)

var addr = "http://localhost:8888"

func TestLoopBack(t *testing.T) {
	log.SetOutput(ioutil.Discard)
	q := make(chan bool)

	doServer(addr, t, q)

	client := NewHttpClient(addr)
	req := &protobuf.Mutation{
		Version:  proto.Uint32(uint32(1)),
		Command:  proto.Uint32(uint32(1)),
		Vbucket:  proto.Uint32(uint32(512)),
		Vbuuid:   proto.Uint64(uint64(0x1234567812345678)),
		Docid:    []byte("cities"),
		Seqno:    proto.Uint64(uint64(10000000)),
		Keys:     [][]byte{[]byte("bangalore"), []byte("delhi"), []byte("jaipur")},
		Oldkeys:  [][]byte{[]byte("varanasi"), []byte("pune"), []byte("mahe")},
		Indexids: []uint32{1, 2, 3},
	}
	resp := &protobuf.Mutation{}
	if err := client.Request(req, resp); err != nil {
		t.Fatal(err)
	}
	if reflect.DeepEqual(req, resp) == false {
		t.Fatal(fmt.Errorf("unexpected response"))
	}
}

func BenchmarkClientRequest(b *testing.B) {
	log.SetOutput(ioutil.Discard)

	client := NewHttpClient(addr)
	req := &protobuf.Mutation{
		Version:  proto.Uint32(uint32(1)),
		Command:  proto.Uint32(uint32(1)),
		Vbucket:  proto.Uint32(uint32(512)),
		Vbuuid:   proto.Uint64(uint64(0x1234567812345678)),
		Docid:    []byte("cities"),
		Seqno:    proto.Uint64(uint64(10000000)),
		Keys:     [][]byte{[]byte("bangalore"), []byte("delhi"), []byte("jaipur")},
		Oldkeys:  [][]byte{[]byte("varanasi"), []byte("pune"), []byte("mahe")},
		Indexids: []uint32{1, 2, 3},
	}
	resp := &protobuf.Mutation{}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		if err := client.Request(req, resp); err != nil {
			b.Fatal(err)
		}
	}
}

func doServer(addr string, tb testing.TB, quit chan bool) AdminServer {
	reqch := make(chan AdminRequest, 10)
	server := NewHttpServer("localhost:8888", 0, 0, reqch)
	if err := server.Register(&protobuf.Mutation{}); err != nil {
		tb.Fatal(err)
	}

	if err := server.Start(); err != nil {
		tb.Fatal(err)
	}

	go func() {
	loop:
		for {
			select {
			case req, ok := <-reqch:
				if ok {
					msg := req.GetMessage().(*protobuf.Mutation)
					if err := req.Send(msg); err != nil {
						tb.Fatal(err)
					}
				} else {
					break loop
				}
			}
		}
		close(quit)
	}()

	return server
}
