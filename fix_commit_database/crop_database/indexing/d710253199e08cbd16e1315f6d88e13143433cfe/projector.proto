// system request and administration request supported by projector's
// admin-port.

package protobuf;

import "common.proto";
import "index.proto";

// Requested by Coordinator during system-start, re-connect, rollback
message FailoverLogRequest {
    required string pool   = 1;
    required string bucket = 2;
    repeated uint32 vbnos  = 3; // vbuckets for which failover log is request
}

message FailoverLogResponse {
    required Error       err  = 1;
    repeated FailoverLog logs = 2;
}

// Requested by Coordinator or indexer to start a new mutation stream.
// BranchTimestamp.Vbnos should be in sort order
message MutationStreamRequest {
    required string             topic             = 1; // topic name.
    required uint32             flag              = 2;
    repeated string             pools             = 3;
    repeated string             buckets           = 4;
    repeated BranchTimestamp    restartTimestamps = 5; // list of timestamps, one per bucket
    // list of index applicable for this stream, optional as well
    repeated Index              indexes           = 6;
}

message MutationStreamResponse {
    required Error              err                = 1;
    required string             topic              = 2;
    required uint32             flag               = 3;
    repeated string             pools              = 4;
    repeated string             buckets            = 5;
    // per bucket failover-timestamp, kv-timestamp for all active vbuckets,
    // for each bucket, after executing the request.
    repeated BranchTimestamp    failoverTimestamps = 6; // sort order
    repeated BranchTimestamp    kvTimestamps       = 7; // sort order
    repeated uint64             indexUuids         = 8;
}

// Requested by Coordinator or indexer to restart or shutdown vbuckets from an
// active mutation stream. Returns back MutationStreamResponse.
message UpdateMutationStreamRequest {
    required string             topic             = 1; // topic name.
    required uint32             flag              = 2;
    repeated string             pools             = 3;
    repeated string             buckets           = 4;
    repeated BranchTimestamp    restartTimestamps = 5; // list of timestamps, one per bucket
    repeated Index              indexes           = 6;
}

// Requested by third party component that wants to subscribe to a topic-name.
// Error message will be sent as response
message SubscribeStreamRequest {
    required string topic   = 1; // must be an already started stream.
    required uint32 flag    = 2;
    repeated Index  indexes = 3;
}

// Requested by indexer / coordinator to inform router to re-connect with
// downstream endpoint. Error message will be sent as response.
message RepairDownstreamEndpoints {
    required string topic     = 1; // must be an already started stream.
}

// Requested by coordinator to should down a mutation stream and all KV
// connections active for that stream. Error message will be sent as response.
message ShutdownStreamRequest {
    required string topic = 1;
}

// Requested by Coordinator during bootstrap handshake to get the current list
// of active streams from projector
message ActiveStreamRequest {
}

message ActiveStreamResponse {
    required Error                  err     = 1;
    repeated MutationStreamResponse streams = 2;
}


// Requested by Coordinator during initial index build, to calculate
// initial-build-timestamp for each bucket.
message CurrentTimestampRequest {
    repeated string buckets = 1;
}

message CurrentTimestampResponse {
    repeated BranchTimestamp currentTimestamps = 1;
}
