// Common set of definitions.

package protobuf;

message Error {
    required string error = 1; // Empty string means success
}

// logical clock
message Timestamp {
    required string bucket = 1; // bucket name
    repeated uint32 vbno   = 2; // list of vbucket numbers
    repeated uint64 seqno  = 3; // corresponding sequence number for each vbucket listed above
}

// logical clock that also associate a vbucket branch for the specified sequence number
message BranchTimestamp {
    required string bucket = 1; // bucket name
    repeated uint32 vbno   = 2; // list of vbucket numbers
    repeated uint64 seqno  = 3; // corresponding sequence number for each vbucket listed above
    repeated uint64 vbuuid = 4; // corresponding vbuuid for each vbucket listed above.
}

// An actor is a process in secondary index cluster and can be one of the following.
enum ActorRole {
    Projector        = 1;
    Indexer          = 2;
    IndexCoordinator = 3;
}

// Actors joining or leaving the topology. An actor is identified using its
// admin-port's connection-address <host:port>.
message Actor {
    required bool     active         = 1;  // whether actor is joining or leaving.
    required string   connectionAddr = 2;  // connectionAddr for actor.
    repeated ActorRole roles          = 3; // roles actor can perform.
}

// failover log for a vbucket.
message FailoverLog {
    required uint32 vbno   = 1;
    repeated uint64 vbuuid = 2; // list of vbuuid for each branch history
    repeated uint64 seqno  = 3; // corresponding high sequence no for each vbuuid listed above.
}

// Periodic Heartbeat from indexer to Coordinator, for each bucket.
message HeartBeat {
    required Timestamp hwTimestamp         = 1; // tip of the mutation queue
    required uint32    lastPersistenceHash = 2; // tip of the storage
    required uint32    lastStabilityHash   = 3; // tip of the snapshot
    required uint32    freeQueue           = 4; // Mutation Queue Size
}

// Version information for Coordinator's StateContext
message StateContextVersion {
    required uint64 cas      = 1;
    required uint32 checksum = 2;
}
