package projector

// SpawnProjectors for each kvaddrs, provided they are not already spawned and
// remembered in `projectors` parameter. return the new set of projectors that
// were spawned.
// adminport of the projector will be 500+kvport.
//func SpawnProjectors(
//    cluster string, kvaddrs []string,
//    projectors map[string]ap.Client) (map[string]ap.Client, error) {
//
//    // kvaddr -> adminport-client
//    newprojectors := make(map[string]ap.Client)
//    // create a projector instance for each kvnode
//    for _, kvaddr := range kvaddrs {
//        if _, ok := projectors[kvaddr]; ok {
//            continue
//        }
//        c := projector.NewClient(kvaddr2adminport(kvaddr, 500))
//        projectors[kvaddr] = c
//        newprojectors[kvaddr] = c
//    }
//    return newprojectors, nil
//}

//func ShutdownProjectors(
//    cluster, pooln string, buckets []string,
//    projectors map[string]ap.Client) (map[string]ap.Client, error) {
//
//    var b *couchbase.Bucket
//    var err error
//
//    for _, bucketn := range buckets {
//        if b, err = c.ConnectBucket(cluster, pooln, bucketn); err != nil {
//            return nil, err
//        } else {
//            break
//        }
//    }
//
//    defer b.Close()
//
//    b.Refresh()
//    m, err := b.GetVBmap(nil)
//    if err != nil {
//        return nil, err
//    }
//
//    newProjectors := make(map[string]ap.Client)
//    for kvaddr, c := range projectors {
//        if vbnos, ok := m[kvaddr]; !ok || (vbnos != nil && len(vbnos) == 0) {
//            ShutdownStream(c, "backfill")
//        } else if ok {
//            newProjectors[kvaddr] = c
//        }
//    }
//    projectors = newProjectors
//    return newProjectors, nil
//}
