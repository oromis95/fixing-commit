package transport

import "net"
import "bufio"

type Transporter interface {
	Read(b []byte) (n int, err error)
	Write(b []byte) (n int, err error)
	LocalAddr() net.Addr
	RemoteAddr() net.Addr
}

// Buffered reader and writer for net.Conn
type connTransporter struct {
	c net.Conn
	r *bufio.Reader
	w *bufio.Writer
}

func (t *connTransporter) Read(b []byte) (n int, err error) {
	return t.r.Read(b)
}

func (t *connTransporter) Write(b []byte) (n int, err error) {
	return t.w.Write(b)
}

func (t *connTransporter) LocalAddr() net.Addr {
	return t.c.LocalAddr()
}

func (t *connTransporter) RemoteAddr() net.Addr {
	return t.c.RemoteAddr()
}

func (t *connTransporter) Flush() error {
	return t.w.Flush()
}

func NewConnTransporter(conn net.Conn, rBufSize int, wBufSize int) *connTransporter {
	t := &connTransporter{
		c: conn,
		r: bufio.NewReaderSize(conn, rBufSize),
		w: bufio.NewWriterSize(conn, wBufSize),
	}

	return t
}
