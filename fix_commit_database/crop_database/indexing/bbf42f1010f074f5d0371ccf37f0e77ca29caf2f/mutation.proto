package protobuf;

// List of possible mutation commands.
enum Command {
    Upsert         = 1; // data command
    Deletion       = 2; // data command
    UpsertDeletion = 3; // data command
    Sync           = 4; // control command
    DropData       = 5; // control command
    StreamBegin    = 6; // control command
    StreamEnd      = 7; // control command
}

// A single mutation message that will framed and transported by router.
// For efficiency mutations from mutiple vbuckets (bounded to same connection)
// can be packed into the same message.
message Mutation {
    required uint32          version   = 1; // protocol version TBD

    // -- Following fields are mutually exclusive --
    repeated KeyVersions     keys      = 2;
    optional VbConnectionMap vbuckets  = 3;
}

// Mutation per vbucket, mutations are broadly divided into data and
// control messages. The division is based on the commands.
//
// Interpreting seq.no:
// 1. For Upsert, Deletion, UpsertDeletion messages, sequence number corresponds
//    to kv mutation.
// 2. For Sync message, it is the latest kv mutation sequence-no. received for
//   a vbucket.
// 3. For DropData message, it is the first kv mutation that was dropped due
//    to buffer overflow.
// 4. For StreamBegin, it is the first kv mutation received after opening a
//    vbucket stream with kv.
// 5. For StreamEnd, it is the last kv mutation received before ending a vbucket
//    stream with kv.
//
// fields `docid`, `indexids`, `keys`, `oldkeys` are valid only for
// Upsert, Deletion, UpsertDeletion messages.
message KeyVersions {
    required uint32 command  = 1;
    required uint32 vbucket  = 2; // 16 bit vbucket in which document is located
    required uint64 seqno    = 5; // sequence number corresponding to this mutation
    required uint64 vbuuid   = 3; // unique id to detect branch history
    optional bytes  docid    = 4; // primary document id
    repeated bytes  keys     = 7; // key-versions for each indexids listed above
    repeated uint32 indexids = 6; // indexids, hosting key-version
    repeated bytes  oldkeys  = 8; // key-versions from old copy of the document
}

// List of vbuckets that will be streamed via a newly opened connection.
message VbConnectionMap {
    repeated uint32 vbuckets = 1;
    repeated uint64 vbuuids  = 2;
}
