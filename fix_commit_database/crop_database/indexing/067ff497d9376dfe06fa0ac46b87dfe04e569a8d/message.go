package indexer

type MsgType int16

const (

	//General Messages
	SUCCESS = iota
	ERROR

	//Component specific messages
	//TODO
)

type MsgData interface{}

type Message interface {
	GetMsgType() MsgType
	GetMsgData() MsgData
}

//Error Message
type MsgError struct {
	mType MsgType
	err   error
}

func (m *MsgError) GetMsgTye() MsgType {
	return m.mType
}

func (m *MsgError) GetMsgData() MsgData {
	return m.err
}

//Success Message
type MsgSuccess struct {
	mType MsgType
}

func (m *MsgSuccess) GetMsgTye() MsgType {
	return m.mType
}

func (m *MsgSuccess) GetMsgData() MsgData {
	return nil
}
