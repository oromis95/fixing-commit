## Router

Router is responsible for identifying local-indexer endpoints that should
receive secondary keys from projector. On one side it is connected with
projector, to get KeyVersions for a vbucket, and on the other side it is
connected with local indexer nodes that will persist KeyVersions on durable
storage.

In the initial version, router is part of projector, hence it uses projector's
admin interface to get index-topology.

* manages per vbucket input queue which is populated with projector's
  KeyVersions.
* uses hash-partition or key-partition. Partitioning algorithm is pluggable
  and partitioning algorithms like range partition and adaptive partition will
  be added at later point.
* partition algorithm maps a secondary key to slice-no.
* IndexTopology is used to identify local-indexer-node hosting the partition
  that containing the slice.
* manages a streaming connection with local indexer nodes to push Mutation
  events to them.
* based on `topicid`, router will either publish it to all indexer-nodes in
  the topology, or only to catchup indexer-node.
