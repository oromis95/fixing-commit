A list of invariants in the system.

* gauranteed transmission of kv mutations across the system.
* kv mutations within a vbucket is always ordered.
* the tip of indexer node across the index-cluster is always at a stable
  timestamp.
* query is always based on a stable timestamp.
* Ensure that at any given time there is only one unique path for each
  mutations from kv-vbucket to index-slice.
* indexer/coordinator is responsible for recovery.

* all components will have a server thread listening on admin port, and strictly
  follow request and response protocol with its clients.
* indexer/coordinator can detect if a projector is disconnected and
  re-establish the connection.
* indexer/coordinator can call projector but it does not interact with KV
  directly.
* indexer/coordinator does not induce `{projector -> VB}` mapping by itself.
* projector can call the coordinator.
* projector can respond to indexer request.
* projector cannot call indexer directly since projector does not know about
  topology.
* router can send key-versions and control-messages to indexers through the
  endpoints provided in the topology.
* rollback happens when there is data loss in kv.
* rollback is stopping the whole system from functioning.

* an index can be split into N number of partition.
* same partition can be hosted by more than one indexer-node.
* an indexer node can host more than one partition of the index.
* replicas of a partition are identical.
* each partition can have different number of replicas.
