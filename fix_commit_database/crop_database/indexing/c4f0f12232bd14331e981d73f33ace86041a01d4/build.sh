#!/bin/bash

if [ -z "$1" ]
    then
    echo "Building Indexer..."
    cd indexer/main;
    go build -o indexer
    cp indexer ../../bin/
    cd ../..
    echo "Done"
    echo "Indexer binary under bin/"
    echo "Building Projector..."
    echo "Done"
    echo "Projector binary under bin/"
elif [ $1 == "indexer" ]
    then
    echo "Building Indexer..."
    cd indexer/main;
    go build -o indexer
    cp indexer ../../bin/
    cd ../..
    echo "Done"
    echo "Indexer binary under bin/"
elif [ $1 == "projector" ]
    then
    echo "Building Projector..."
    echo "Done"
    echo "Projector binary under bin/"
else
    echo "Unknown build option"
fi
