// concurrency model:
//
//                                                    Network socket
//                                                  ------------------
//                                                              ^
//                                      *common.VbConnectionMap |
//                                        *[]common.KeyVersions |
//                                                              |
//                      NewStreamClient()                       |
//                           |     |                            |
//                           (spawn)----------*--------- runTransmitter()
//                           |                |
//                           |                *--------- runTransmitter()
//        SendVbmap() --*-> genServer         |
//                      |         |           *--------- runTransmitter()
//  SendKeyVersions() --*         |                        ^   ^   ^
//                      |         |                        |   |   |
//            Close() --*         *------------------------*---*---*
//                                  *[]common.KeyVersions
//                                  *common.VbConnectionMap
//
// client behavior:
//
// - DropData is generated by the client when buffer overflows.
// - client API to push mutation messages to the other end. This is an all or
//   nothing client for each downstream host.
// - If caller receives an error value while calling an exported method on
//   StreamClient, it is adviced to stop the client, its connection pool, and
//   wait for a reconnect request.

package indexer

import (
	"github.com/couchbase/indexing/secondary/common"
	"log"
	"net"
)

// StreamClient is an active client for each remote host, and there can be
// multiple connections opened with remote host for the same endpoint.
type StreamClient struct {
	// immutable fields
	raddr     string
	conns     map[int]net.Conn
	connChans map[int]chan interface{}
	// gen-server
	reqch chan []interface{}
	finch chan bool
	// mutable fields
	vbChans  map[uint16]chan interface{}
	dropData map[uint16][]*common.KeyVersions
}

// NewStreamClient returns a pool of connection. Multiple connections, based
// on parameter `n`,  can be used to speed up mutation transport across network.
// A vbucket is always binded to a connection and ensure that mutations within
// a vbucket are serialized.
func NewStreamClient(raddr string, n int, flags StreamTransportFlag) (c *StreamClient, err error) {
	var conn net.Conn

	c = &StreamClient{
		raddr:     raddr,
		conns:     make(map[int]net.Conn),
		connChans: make(map[int]chan interface{}),
		reqch:     make(chan []interface{}, common.GenserverChannelSize),
		finch:     make(chan bool),
	}
	// open connections with remote
	size := common.KeyVersionsChannelSize
	pkts := make(map[int]*StreamTransportPacket)
	for i := 0; i < n; i++ {
		if conn, err = net.Dial("tcp", raddr); err != nil {
			break
		}
		c.conns[i] = conn
		c.connChans[i] = make(chan interface{}, size)
		pkts[i] = NewStreamTransportPacket(common.MaxStreamDataLen, flags)
	}
	// if err close connections
	if err != nil {
		c.doClose()
		return nil, err
	}
	// spawn routines per connection.
	for i, conn := range c.conns {
		go c.runTransmitter(conn, pkts[i], c.connChans[i])
	}
	go c.genServer(c.reqch)
	return c, nil
}

// gen-server commands
const (
	streamCmdSendVbmap byte = iota + 1
	streamCmdSendKeyVersions
	streamCmdClose
)

// SendVbmap will use `vbmap` for this endpoint to calculate vbmap for each
// connection to endpoint, and send the same to the otherside.
//
// on error, closes all connection to this endpoint.
func (c *StreamClient) SendVbmap(vbmap *common.VbConnectionMap) error {
	respch := make(chan []interface{}, 1)
	cmd := []interface{}{streamCmdSendVbmap, vbmap, respch}
	resp, err := common.FailsafeOp(c.reqch, respch, cmd, c.finch)
	if err != nil {
		return err
	} else if resp[0] == nil {
		return nil
	}
	return resp[0].(error)
}

// SendKeyVersions to downstream endpoint using one of the connection.
//
// Note:
//
// - a mutation message can carry more than on KeyVersions payload, in
//   which case, all of them should belong to the same vbucket.
// - in case of control messages like Sync, StreamBegin, StreamEnd and
//   DropData, len(keys) =:= 1
func (c *StreamClient) SendKeyVersions(keys []*common.KeyVersions) error {
	if len(keys) == 0 {
		return ErrorStreamcEmptyKeys
	}
	// TODO: sanity check, can be removed in production.
	if common.Debug {
		vbucket := keys[0].Vbucket
		for _, kv := range keys {
			if vbucket != kv.Vbucket {
				return ErrorStreamcUnexpectedKeys
			}
		}
	}

	respch := make(chan []interface{}, 1)
	cmd := []interface{}{streamCmdSendKeyVersions, keys, respch}
	resp, err := common.FailsafeOp(c.reqch, respch, cmd, c.finch)
	if err != nil {
		return err
	} else if resp[0] == nil {
		return nil
	}
	return resp[0].(error)
}

// Close the client and all its active connection with downstream host.
func (c *StreamClient) Close() (err error) {
	respch := make(chan []interface{}, 1)
	cmd := []interface{}{streamCmdClose, respch}
	resp, err := common.FailsafeOp(c.reqch, respch, cmd, c.finch)
	if err != nil {
		return err
	} else if resp[0] == nil {
		return nil
	}
	return resp[0].(error)
}

func (c *StreamClient) genServer(reqch chan []interface{}) {
	defer func() { c.doClose() }()

loop:
	for {
		msg := <-reqch
		switch msg[0].(byte) {
		case streamCmdSendKeyVersions:
			keys := msg[1].([]*common.KeyVersions)
			respch := msg[2].(chan []interface{})
			respch <- []interface{}{c.sendKeyVersions(keys)}

		case streamCmdSendVbmap:
			vbmap := msg[1].(*common.VbConnectionMap)
			respch := msg[2].(chan []interface{})
			respch <- []interface{}{c.sendVbmap(vbmap)}

		case streamCmdClose:
			respch := msg[1].(chan []interface{})
			respch <- []interface{}{nil}
			break loop
		}
	}
}

// send array of KeyVersions to other side. DropData if buffer gets full
// TODO: smart-throttling.
func (c *StreamClient) sendKeyVersions(keys []*common.KeyVersions) error {
	vbno, err := c.handleControlMsg(keys)
	if err != nil {
		return err
	}

	// pending DropData message
	if kvs, ok := c.dropData[vbno]; ok {
		keys = kvs // this is where keys are lost !!!
	}

	select {
	case c.vbChans[vbno] <- keys: // send it to streamer routine
		delete(c.dropData, vbno)

	default: // buffer full
		if _, ok := c.dropData[vbno]; !ok { // remember DropData
			log.Printf("dropping data for vbucket %v\n", vbno)
			kv := common.NewDropData(vbno, keys[0].Vbuuid, keys[1].Seqno)
			c.dropData[vbno] = []*common.KeyVersions{kv}
		}
	}
	return nil
}

func (c *StreamClient) sendVbmap(vbmap *common.VbConnectionMap) error {
	c.vbChans = make(map[uint16]chan interface{})

	n := len(c.conns)
	// instantiate VbConnectionMap per connection
	connVbs := make(map[int]*common.VbConnectionMap)
	for i := 0; i < n; i++ {
		connVbs[i] = &common.VbConnectionMap{
			Bucket:   vbmap.Bucket,
			Vbuckets: make([]uint16, 0, len(vbmap.Vbuckets)),
			Vbuuids:  make([]uint64, 0, len(vbmap.Vbuuids)),
		}
	}
	// bind vbuckets with connections
	for i, vbno := range vbmap.Vbuckets {
		j := i % n
		connVbs[j].Vbuckets = append(connVbs[j].Vbuckets, vbno)
		connVbs[j].Vbuuids = append(connVbs[j].Vbuuids, vbmap.Vbuuids[i])
		c.vbChans[vbno] = c.connChans[j]
	}
	// send it.
	for i := range c.conns {
		c.connChans[i] <- connVbs[i]
	}
	return nil
}

// close all connections with downstream host.
func (c *StreamClient) doClose() (err error) {
	recoverClose := func(conn net.Conn) {
		laddr := conn.LocalAddr().String()
		defer func() {
			if r := recover(); r != nil {
				log.Printf("%v\n", r)
				err = common.ErrorClosed
			}
		}()
		conn.Close()
		log.Printf("connection %q closed\n", laddr)
	}
	// close connections
	for _, conn := range c.conns {
		recoverClose(conn)
	}
	close(c.finch)
	return
}

// add/remove transmitter channel for vbucket based on StreamBegin and
// StreamEnd.
func (c *StreamClient) handleControlMsg(keys []*common.KeyVersions) (uint16, error) {
	vbno := keys[0].Vbucket
	switch keys[0].Command {
	case common.StreamBegin:
		if len(keys) > 1 {
			panic("can't handle more than one keys with control message")
		}
		index := int(vbno) % len(c.conns)
		c.vbChans[vbno] = c.connChans[index]

	case common.StreamEnd:
		if _, ok := c.vbChans[vbno]; ok == false {
			panic("duplicate StreamEnd: this can never ever happend!")
		} else if len(keys) > 1 {
			panic("cann't handle more than one keys with control message")
		}
		delete(c.vbChans, vbno)
	}
	return vbno, nil
}

// routine pushes per vbucket slice of KeyVersions to the other end.
func (c *StreamClient) runTransmitter(conn net.Conn, pkt *StreamTransportPacket, payloadch chan interface{}) {
	var err error
	laddr := conn.LocalAddr().String()

loop:
	for {
		if payload, ok := <-payloadch; ok {
			err = pkt.Send(conn, payload)
		} else {
			err = ErrorStreamcChannelClosed
		}
		if err != nil {
			log.Printf("error: stream-client %q exited, `%v`\n", laddr, err)
			break loop
		}
	}
	c.Close()
}
