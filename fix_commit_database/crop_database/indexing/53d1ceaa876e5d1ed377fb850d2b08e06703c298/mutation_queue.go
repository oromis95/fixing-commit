//  Copyright (c) 2014 Couchbase, Inc.
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
//  except in compliance with the License. You may obtain a copy of the License at
//    http://www.apache.org/licenses/LICENSE-2.0
//  Unless required by applicable law or agreed to in writing, software distributed under the
//  License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
//  either express or implied. See the License for the specific language governing permissions
//  and limitations under the License.

package indexer

import (
	"errors"
	"sync"
	"time"

	"github.com/couchbase/indexing/secondary/common"
)

//MutationQueue is a concurrent multi-queue with internal queue per vbucket
//for storing Mutations. It doesn't copy the mutation and its caller's responsiblity
//to allocate/deallocate Mutation struct. A mutation which is currently in queue
//shouldn't be freed.
//
//MutationQueue internally uses mutex locks per vbucket queue. Multiple
//Reader/Writer threads can access the queue concurrently.

type mutationQueue struct {
	head        []*node      //head pointer per vbucket queue
	tail        []*node      //tail pointer per vbucket queue
	lock        []sync.Mutex //mutex lock per vbucket queue
	numVbuckets uint16       //num vbuckets for the queue
	size        []int64      //size of queue per vbucket
}

//NewMutationQueue allocates a new Mutation Queue and initializes it
func NewMutationQueue(numVbuckets uint16) *mutationQueue {

	q := &mutationQueue{head: make([]*node, numVbuckets),
		tail:        make([]*node, numVbuckets),
		lock:        make([]sync.Mutex, numVbuckets),
		size:        make([]int64, numVbuckets),
		numVbuckets: numVbuckets,
	}

	var x uint16
	for x = 0; x < numVbuckets; x++ {
		n := &node{} //sentinel node for the queue
		q.head[x] = n
		q.tail[x] = n
	}

	return q

}

//Enqueue will enqueue the mutation reference for given vbucket.
//Caller should not free the mutation till it is dequeued.
//Mutation will not be copied internally by the queue.
func (q *mutationQueue) Enqueue(mutation *common.Mutation, vbucket uint16) error {

	if vbucket < 0 || vbucket > q.numVbuckets-1 {
		return errors.New("vbucket out of range")
	}

	//create a new node
	n := q.allocNode(vbucket)
	n.mutation = mutation
	n.next = nil

	q.lock[vbucket].Lock()
	defer q.lock[vbucket].Unlock()

	//point tail's next to new node
	q.tail[vbucket].next = n
	//update tail to new node
	q.tail[vbucket] = q.tail[vbucket].next

	q.size[vbucket] = q.size[vbucket] + 1

	return nil

}

//DequeueUptoSeqno returns a channel on which it will return mutation reference
//for specified vbucket upto the sequence number specified.
//This function will keep polling till mutations upto seqno are available
//to be sent. It terminates when it finds a mutation with seqno higher than
//the one specified as argument. This allow for multiple mutations with same
//seqno (e.g. in case of multiple indexes)
//It closes the mutation channel to indicate its done.
func (q *mutationQueue) DequeueUptoSeqno(vbucket uint16, seqno uint64) (
	<-chan *common.Mutation, error) {

	datach := make(chan *common.Mutation)

	go q.dequeueUptoSeqno(vbucket, seqno, datach)

	return datach, nil

}

func (q *mutationQueue) dequeueUptoSeqno(vbucket uint16, seqno uint64,
	datach chan *common.Mutation) {

	//every DEQUEUE_POLL_INTERVAL milliseconds, check for new mutations
	ticker := time.NewTicker(time.Millisecond * DEQUEUE_POLL_INTERVAL)

	for _ = range ticker.C {

		func() {
			q.lock[vbucket].Lock()
			defer q.lock[vbucket].Unlock()
			for q.head[vbucket] != q.tail[vbucket] { //if queue is nonempty

				head := q.head[vbucket]
				//copy the mutation pointer
				m := head.next.mutation
				if seqno >= m.Seqno {
					//move head to next
					q.head[vbucket] = head.next
					q.size[vbucket] = q.size[vbucket] - 1
					head = nil
					//send mutation to caller
					datach <- m
				} else {
					ticker.Stop()
					close(datach)
					return
				}
			}
		}()
	}
}

//Dequeue returns a channel on which it will return mutations for specified vbucket.
//This function will keep polling and send mutations as those become available.
//It returns a stop channel on which caller can signal it to stop.
func (q *mutationQueue) Dequeue(vbucket uint16) (<-chan *common.Mutation,
	chan<- bool, error) {

	datach := make(chan *common.Mutation)
	stopch := make(chan bool)

	//every DEQUEUE_POLL_INTERVAL milliseconds, check for new mutations
	ticker := time.NewTicker(time.Millisecond * DEQUEUE_POLL_INTERVAL)

	go func() {
		for {
			select {
			case <-ticker.C:
				q.dequeue(vbucket, datach)
			case <-stopch:
				ticker.Stop()
				close(datach)
				return
			}
		}
	}()

	return datach, stopch, nil

}

func (q *mutationQueue) dequeue(vbucket uint16, datach chan *common.Mutation) {

	for {
		m := q.DequeueSingleElement(vbucket)
		if m == nil {
			return
		}
		datach <- m
	}

}

//DequeueSingleElement dequeues a single element and returns.
//Returns nil in case of empty queue.
func (q *mutationQueue) DequeueSingleElement(vbucket uint16) *common.Mutation {

	q.lock[vbucket].Lock()
	defer q.lock[vbucket].Unlock()

	if q.head[vbucket] != q.tail[vbucket] { //if queue is nonempty

		head := q.head[vbucket]
		//copy the mutation pointer
		m := head.next.mutation
		//move head to next
		q.head[vbucket] = head.next
		q.size[vbucket] = q.size[vbucket] - 1
		head = nil
		return m
	}
	return nil
}

//PeekTail returns reference to a vbucket's mutation at tail of queue without dequeue
func (q *mutationQueue) PeekTail(vbucket uint16) *common.Mutation {

	q.lock[vbucket].Lock()
	defer q.lock[vbucket].Unlock()

	if q.head[vbucket] != q.tail[vbucket] { //if queue is nonempty
		return q.tail[vbucket].mutation
	}
	return nil
}

//PeekHead returns reference to a vbucket's mutation at head of queue without dequeue
func (q *mutationQueue) PeekHead(vbucket uint16) *common.Mutation {

	q.lock[vbucket].Lock()
	defer q.lock[vbucket].Unlock()

	if q.head[vbucket] != q.tail[vbucket] { //if queue is nonempty
		return q.head[vbucket].mutation
	}
	return nil
}

//GetSize returns the size of the vbucket queue
func (q *mutationQueue) GetSize(vbucket uint16) int64 {

	q.lock[vbucket].Lock()
	defer q.lock[vbucket].Unlock()

	return q.size[vbucket]
}

//GetNumVbuckets returns the numbers of vbuckets for the queue
func (q *mutationQueue) GetNumVbuckets() uint16 {
	return q.numVbuckets
}

//allocates a new node and returns
func (q *mutationQueue) allocNode(vbucket uint16) *node {
	return &node{}
}
