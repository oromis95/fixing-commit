package indexer

import (
    "fmt"
    "github.com/couchbase/indexing/secondary/common"
    "github.com/couchbase/indexing/secondary/protobuf"
    "io/ioutil"
    "log"
    "reflect"
    "testing"
    "time"
)

var addr  = "localhost:8888"
var count = 0
var msgch = make(chan interface{}, 1000)

func TestLoopback(t *testing.T) {
    var client *StreamClient
    var err    error

    log.SetOutput(ioutil.Discard)

    doServer(addr, t, msgch)

    // start client
    flags := StreamTransportFlag(0).SetProtobuf()
    if client, err = NewStreamClient(addr, 2, flags); err != nil {
        t.Fatal(err)
    }

    vbmap := &common.VbConnectionMap{
        Bucket: "default",
        Vbuckets: []uint16{1,2,3,4},
        Vbuuids:  []uint64{1,3,2,4},
    }
    if err := client.SendVbmap(vbmap); err != nil {
        t.Fatal(err)
    }

    // test timeouts
    if msg, ok := (<-msgch).(RestartVbuckets); ok {
        if len([]uint64(msg)) != 4 {
            t.Fatal(fmt.Errorf("mismatch in restart vbuckets %v", msg))
        }
    } else {
        t.Fatal(fmt.Errorf("expected restart vbuckets"))
    }

    // stop client and restart
    client.Close()
    if client, err = NewStreamClient(addr, 2, flags); err != nil {
        t.Fatal(err)
    }
    if err := client.SendVbmap(vbmap); err != nil {
        t.Fatal(err)
    }
    time.Sleep(100 * time.Millisecond)

    // test loop back of valid key-versions
    ks_ref := make([]*common.KeyVersions, 0)
    k := common.NewUpsert(1, 1, []byte("cities"), 10000000)
    k.Keys = [][]byte{[]byte("bangalore"), []byte("delhi"), []byte("jaipur")}
    k.Oldkeys = [][]byte{[]byte("varanasi"), []byte("pune"), []byte("mahe")}
    k.Uuids = []uint64{1, 2, 3}
    ks_ref = append(ks_ref, k)

    if err := client.SendKeyVersions(ks_ref); err != nil {
        t.Fatal(err)
    }

    val := <-msgch
    if ks, ok := (val).([]*protobuf.KeyVersions); ok {
        if len(ks) != len(ks_ref) {
            t.Fatal(fmt.Errorf("mismatch in number of key versions"))
        }
        ks_lb := protobuf2KeyVersions(ks)
        if reflect.DeepEqual(ks_lb[0], ks_ref[0]) == false {
            t.Fatal(fmt.Errorf("unexpected response"))
        }
    } else {
        t.Fatal(fmt.Errorf("unexpected type in loopback %T:%v", val, val))
    }
    client.Close()
}

func BenchmarkLoopback(b *testing.B) {
    var client *StreamClient
    var err error

    flags := StreamTransportFlag(0).SetProtobuf()
    if client, err = NewStreamClient(addr, 1, flags); err != nil {
        b.Fatal(err)
    }

    ks_ref := make([]*common.KeyVersions, 0)
    k := common.NewUpsert(1, 10, []byte("cities"), 10000000)
    k.Keys = [][]byte{[]byte("bangalore"), []byte("delhi"), []byte("jaipur")}
    k.Oldkeys = [][]byte{[]byte("varanasi"), []byte("pune"), []byte("mahe")}
    k.Uuids = []uint64{1, 2, 3}
    ks_ref = append(ks_ref, k)

    b.ResetTimer()
    for i := 0; i < b.N; i++ {
        client.SendKeyVersions(ks_ref)
        <-msgch
    }
    client.Close()
}

func doServer(addr string, tb testing.TB, msgch chan interface{}) *MutationStream {
    var mStream *MutationStream
    var err error

    mutch := make(chan []*protobuf.KeyVersions, 10000)
    sbch := make(chan interface{}, 100)
    if mStream, err = NewMutationStream(addr, mutch, sbch); err != nil {
        tb.Fatal(err)
    }

    go func() {
        var mutn, err interface{}
        var ok bool
        for {
            select {
            case mutn, ok = <-mutch:
                msgch <- mutn
            case err, ok = <-sbch:
                msgch <- err
            }
            if ok == false {
                return
            }
        }
    }()
    return mStream
}
