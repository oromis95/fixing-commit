// concurrency model:
//
//                  NewEndpoint()
//                       |
//                       |
//                    (spawn)
//                       |
//                       |  (timeout)
//   Send() -----*----> run ----------> stream-client ---> TCP
//               |       ^
//   Close() ----*       |
//                       V
//                     buffers

package projector

import (
    c "github.com/couchbase/indexing/secondary/common"
    "github.com/couchbase/indexing/secondary/indexer"
    "log"
    "time"
)

type Endpoint struct {
    raddr  string
    client *indexer.StreamClient
    reqch  chan []interface{} // carries *c.KeyVersions, endpCmdClose
    finch  chan bool
}

func NewEndpoint(raddr string, n int) (*Endpoint, error) {
    flags := indexer.StreamTransportFlag(0).SetProtobuf() // TODO: configurable
    client, err := indexer.NewStreamClient(raddr, n, flags)
    if err != nil {
        return nil, err
    }
    endpoint := &Endpoint{
        raddr:  raddr,
        client: client,
        reqch:  make(chan []interface{}, c.KeyVersionsChannelSize),
        finch:  make(chan bool),
    }
    go endpoint.run(endpoint.reqch)
    log.Printf("endpoint client started for %q with %v connections\n", raddr, n)
    return endpoint, nil
}

const (
    endpCmdPing byte = iota + 1
    endpCmdSendVbmap
    endpCmdSend
    endpCmdClose
)

func (endp *Endpoint) Ping() bool {
    respch := make(chan []interface{}, 1)
    cmd := []interface{}{endpCmdPing, respch}
    resp, err := c.FailsafeOp(endp.reqch, respch, cmd, endp.finch)
    if err != nil {
        return false
    }
    return resp[0].(bool)
}

func (endp *Endpoint) SendVbmap(vbmap *c.VbConnectionMap) error {
    respch := make(chan []interface{}, 1)
    cmd := []interface{}{endpCmdSendVbmap, vbmap, respch}
    resp, err := c.FailsafeOp(endp.reqch, respch, cmd, endp.finch)
    if err != nil {
        return err
    } else if resp[0] == nil {
        return nil
    }
    return resp[0].(error)
}

func (endp *Endpoint) Send(kv *c.KeyVersions) error {
    var respch chan []interface{}
    cmd := []interface{}{endpCmdSend, kv}
    _, err := c.FailsafeOp(endp.reqch, respch, cmd, endp.finch)
    return err
}

func (endpoint *Endpoint) Close() error {
    var respch chan []interface{}
    cmd := []interface{}{endpCmdClose}
    _, err := c.FailsafeOp(endpoint.reqch, respch, cmd, endpoint.finch)
    return err
}

func (endpoint *Endpoint) run(reqch chan []interface{}) {
    client := endpoint.client
    raddr := endpoint.raddr

    timeout := time.After(c.EndpointKVTimeout * time.Millisecond)
    harakiri := time.After(c.EndpointHarakiriTimeout * time.Millisecond)
    buffers := newEndpointBuffers(c.MaxEndpointBufferSize)
loop:
    for {
        select {
        case msg := <-reqch:
            switch msg[0].(byte) {
            case endpCmdPing:
                respch := msg[1].(chan []interface{})
                respch <- []interface{}{true}

            case endpCmdSendVbmap:
                vbmap := msg[1].(*c.VbConnectionMap)
                respch := msg[2].(chan []interface{})
                respch <- []interface{}{client.SendVbmap(vbmap)}

            case endpCmdSend:
                buffers.addKeyVersions(msg[1].(*c.KeyVersions))

            case endpCmdClose:
                buffers.flushBuffers(client)
                endpoint.doClose()
                log.Printf("closed endpoint with remote %q\n", raddr)
                break loop
            }

        case <-timeout:
            if err := buffers.flushBuffers(client); err != nil {
                log.Printf("error `%v` with endpoint %q\n", err, raddr)
                endpoint.doClose()
                break loop
            }
            timeout = time.After(c.EndpointKVTimeout * time.Millisecond)
            buffers = newEndpointBuffers(c.MaxEndpointBufferSize)

        case <-harakiri:
            buffers.flushBuffers(client)
            endpoint.doClose()
            log.Printf("endpoint with remote %q committed harakiri\n", raddr)
            break loop
        }
    }
}

func (endpoint *Endpoint) doClose() {
    endpoint.client.Close()
    close(endpoint.finch)
}
