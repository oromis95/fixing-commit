package common

// Router defines is per uuid router interface, where uuid might refer to an
// index or similar entities.
type Router interface {
    // Bucket will return the bucket name for which this router instance is
    // applicable.
    Bucket() string

    // UuidEndpoints return a list of endpoints <host:port> that are
    // associated with unique id `uuid`, and an endpoint for Coordinator. Uuid
    // can be an index-uuid.
    UuidEndpoints() []string

    // UpsertEndpoints return a list of endpoints <host:port> to which Upserted
    // KeyVersions, for this uuid, need to be published.
    UpsertEndpoints(keys *KeyVersions) []string

    // UpsertDeletionEndpoints return a list of endpoints <host:port> and an
    // endpoing to Coordinator to which UpsertDeletion for KeyVersions, for
    // this uuid, need to be published.
    UpsertDeletionEndpoints(keys *KeyVersions) []string

    // DeletionEndpoints return a list of endpoints <host:port> to which
    // Deletion for KeyVersions, for this uuid,  need to be published.
    DeletionEndpoints(keys *KeyVersions) []string
}
