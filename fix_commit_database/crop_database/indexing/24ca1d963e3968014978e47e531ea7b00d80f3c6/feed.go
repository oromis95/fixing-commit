// Feed is the central program around which adminport, bucket_feed, kvfeed and
// projector and router algorithms are organized.

package projector

import (
    "fmt"
    c "github.com/couchbase/indexing/secondary/common"
    "log"
    "sort"
)

var ErrorInvalidBucket = fmt.Errorf("ErrorInvalidBucket")

// Feed is mutation stream - for maintenance, initial-load, catchup etc...
type Feed struct {
    // manage upstream
    projector *Projector             // immutable
    topic     string                 // immutable
    bfeeds    map[string]*BucketFeed // immutable and indexed by bucket name
    // manage downstream
    engines   map[uint64]*Engine   // immutable, indexed by uuid
    endpoints map[string]*Endpoint // immutable, indexed by <host:port>
    // timestamp feedback
    failoverTimestamps map[string]*c.Timestamp // indexed by bucket name
    kvTimestamps       map[string]*c.Timestamp // indexed by bucket name
    // gen-server
    reqch chan []interface{}
    finch chan bool
}

// NewFeed creates a new instance of mutation stream for specified topic. Spawns
// a routine for gen-server
//
// if error, Feed is not created.
// - error returned by couchbase client
func NewFeed(p *Projector, topic string, request RequestReader) (*Feed, error) {
    var feed *Feed

    pools := request.GetPools()
    buckets := request.GetBuckets()

    feed = &Feed{
        projector:          p,
        topic:              topic,
        bfeeds:             make(map[string]*BucketFeed),
        failoverTimestamps: make(map[string]*c.Timestamp),
        kvTimestamps:       make(map[string]*c.Timestamp),
        reqch:              make(chan []interface{}, c.GenserverChannelSize),
        finch:              make(chan bool),
    }
    // fresh start of a mutation stream.
    for i, bucket := range buckets {
        bfeed, err := NewBucketFeed(feed, p.getKVNodes(), pools[i], bucket)
        if err != nil {
            feed.doClose()
            return nil, err
        }
        feed.bfeeds[bucket] = bfeed
    }
    go feed.genServer(feed.reqch)
    return feed, nil
}

func (feed *Feed) getProjector() *Projector {
    return feed.projector
}

// gen-server API commands
const (
    fCmdRequestFeed byte = iota + 1
    fCmdUpdateFeed
    fCmdUpdateEngines
    fCmdDeleteEngines
    fCmdRepairEndpoints
    fCmdCloseFeed
)

// RequestFeed to start a new mutation stream for a subset of vbuckets from one
// or more buckets on a mutation stream.
//
// if error is returned then upstream instances of BucketFeed and KVFeed are
// shutdown and application must retry the request or fall-back.
// - ErrorInvalidRequest if request is malformed.
// - error returned by couchbase client.
// - error if KVFeed is already closed.
func (feed *Feed) RequestFeed(request RequestReader) error {
    respch := make(chan []interface{}, 1)
    cmd := []interface{}{fCmdRequestFeed, request, respch}
    resp, err := c.FailsafeOp(feed.reqch, respch, cmd, feed.finch)
    if err != nil {
        return err
    } else if resp[0] == nil {
        return nil
    }
    return resp[0].(error)
}

// UpdateFeed will start, restart and shutdown vbucket streams for this active
// feed. To atomically update downstream projection and routing for index,
// supply a list of indexes with updated routing tables.
//
// returns failover-timetamp and kv-timestamp (restart seqno. after honoring
// rollback)
// - ErrorInvalidRequest if request is malformed.
// - error returned by couchbase client.
// - error if Feed is already closed.
func (feed *Feed) UpdateFeed(request RequestReader) error {
    respch := make(chan []interface{}, 1)
    cmd := []interface{}{fCmdUpdateFeed, request, respch}
    resp, err := c.FailsafeOp(feed.reqch, respch, cmd, feed.finch)
    if err != nil {
        return err
    } else if resp[0] == nil {
        return nil
    }
    return resp[0].(error)
}

// UpdateEngines will update active engines for this Feed. This happens when
// topology changes for one or more indexes
//
// - error if Feed is already closed.
func (feed *Feed) UpdateEngines(request Subscriber) error {
    respch := make(chan []interface{}, 1)
    cmd := []interface{}{fCmdUpdateEngines, request, respch}
    resp, err := c.FailsafeOp(feed.reqch, respch, cmd, feed.finch)
    if err != nil {
        return err
    } else if resp[0] == nil {
        return nil
    }
    return resp[0].(error)
}

// DeleteEngines from active set of engines for this Feed. This happens when
// one or more indexes are deleted.
//
// - error if Feed is already closed.
func (feed *Feed) DeleteEngines(request Subscriber) error {
    respch := make(chan []interface{}, 1)
    cmd := []interface{}{fCmdDeleteEngines, request, respch}
    resp, err := c.FailsafeOp(feed.reqch, respch, cmd, feed.finch)
    if err != nil {
        return err
    } else if resp[0] == nil {
        return nil
    }
    return resp[0].(error)
}

// RepairEndpoints will restart downstream endpoints if it is already dead.
func (feed *Feed) RepairEndpoints() error {
    respch := make(chan []interface{}, 1)
    cmd := []interface{}{fCmdRepairEndpoints, respch}
    resp, err := c.FailsafeOp(feed.reqch, respch, cmd, feed.finch)
    if err != nil {
        return err
    } else if resp[0] == nil {
        return nil
    }
    return resp[0].(error)
}

// CloseFeed will shutdown this feed and upstream and downstream instances.
func (feed *Feed) CloseFeed() error {
    respch := make(chan []interface{}, 1)
    cmd := []interface{}{fCmdCloseFeed}
    resp, err := c.FailsafeOp(feed.reqch, respch, cmd, feed.finch)
    if err != nil {
        return err
    } else if resp[0] == nil {
        return nil
    }
    return resp[0].(error)
}

func (feed *Feed) genServer(reqch chan []interface{}) {
loop:
    for {
        msg := <-reqch
        switch msg[0].(byte) {
        case fCmdRequestFeed:
            req, respch := msg[1].(RequestReader), msg[2].(chan []interface{})
            respch <- []interface{}{feed.requestFeed(req)}

        case fCmdUpdateFeed:
            req, respch := msg[1].(RequestReader), msg[2].(chan []interface{})
            respch <- []interface{}{feed.updateFeed(req)}

        case fCmdUpdateEngines:
            req, respch := msg[1].(Subscriber), msg[2].(chan []interface{})
            respch <- []interface{}{feed.updateEngines(req)}

        case fCmdDeleteEngines:
            req, respch := msg[1].(Subscriber), msg[2].(chan []interface{})
            respch <- []interface{}{feed.deleteEngines(req)}

        case fCmdRepairEndpoints:
            respch := msg[1].(chan []interface{})
            respch <- []interface{}{feed.repairEndpoints()}

        case fCmdCloseFeed:
            respch := msg[1].(chan []interface{})
            respch <- []interface{}{feed.doClose()}
            break loop
        }
    }
}

// start a new feed.
func (feed *Feed) requestFeed(req RequestReader) (err error) {
    engines, endpoints, err := buildEngines(
        req.(Subscriber), make(map[uint64]*Engine), make(map[string]*Endpoint))
    if err != nil {
        return err
    }

    // order of bucket, restartTimestamp, failoverTimestamp and kvTimestamp
    // are preserved
    for bucket, emap := range bucketWiseEngines(engines) {
        bfeed := feed.bfeeds[bucket]
        if bfeed == nil {
            return ErrorInvalidBucket
        }
        failTs, kvTs, err := bfeed.RequestFeed(req, emap)
        if err != nil {
            return err
        }
        // update failover-timestamps, kv-timestamps
        failTs = feed.failoverTimestamps[bucket].Union(failTs)
        kvTs = feed.kvTimestamps[bucket].Union(kvTs)
        feed.failoverTimestamps[bucket] = failTs
        feed.kvTimestamps[bucket] = kvTs
        sort.Sort(feed.failoverTimestamps[bucket])
        sort.Sort(feed.kvTimestamps[bucket])
        // Send vbmap across to the endpoints
        feed.sendVbmap(endpoints, kvTs)
    }
    feed.engines, feed.endpoints = engines, endpoints
    return nil
}

func (feed *Feed) sendVbmap(endpoints map[string]*Endpoint, kvTs *c.Timestamp) error {
    for raddr, endpoint := range endpoints {
        vbmap := &c.VbConnectionMap{
            Bucket:   kvTs.Bucket,
            Vbuckets: make([]uint16, 0),
            Vbuuids:  make([]uint64, 0),
        }
        for i, vbno := range kvTs.Vbnos {
            vbmap.Vbuckets = append(vbmap.Vbuckets, vbno)
            vbmap.Vbuuids = append(vbmap.Vbuuids, kvTs.Vbuuids[i])
        }
        if err := endpoint.SendVbmap(vbmap); err != nil {
            log.Printf("error sending vbmap to endpoint %q: %v", raddr, err)
        }
    }
    return nil
}

// start, restart, shutdown vbuckets in an active feed and/or update
// downstream engines and endpoints.
func (feed *Feed) updateFeed(req RequestReader) (err error) {
    engines, endpoints, err := buildEngines(
        req.(Subscriber), feed.engines, feed.endpoints)
    if err != nil {
        return err
    }

    // order of bucket, restartTimestamp, failoverTimestamp and kvTimestamp
    // are preserved
    for bucket, emap := range bucketWiseEngines(engines) {
        bfeed := feed.bfeeds[bucket]
        if bfeed == nil {
            return ErrorInvalidBucket
        }
        failTs, kvTs, err := bfeed.UpdateFeed(req, emap)
        if err != nil {
            return err
        }
        // update failover-timestamps, kv-timestamps
        if req.IsShutdown() {
            failTs = feed.failoverTimestamps[bucket].FilterByVbuckets(failTs.Vbnos)
            kvTs = feed.kvTimestamps[bucket].FilterByVbuckets(kvTs.Vbnos)
        } else {
            failTs = feed.failoverTimestamps[bucket].Union(failTs)
            kvTs = feed.kvTimestamps[bucket].Union(kvTs)
        }
        feed.failoverTimestamps[bucket] = failTs
        feed.kvTimestamps[bucket] = kvTs
        sort.Sort(feed.failoverTimestamps[bucket])
        sort.Sort(feed.kvTimestamps[bucket])
    }
    feed.engines, feed.endpoints = engines, endpoints
    return nil
}

// index topology has changed, update it.
func (feed *Feed) updateEngines(req Subscriber) (err error) {
    engines, _, err := buildEngines(req, feed.engines, feed.endpoints)
    if err != nil {
        return err
    }

    for bucket, emap := range bucketWiseEngines(engines) {
        if bfeed, ok := feed.bfeeds[bucket]; ok {
            if err = bfeed.UpdateEngines(emap); err != nil {
                return
            }
        } else {
            return c.ErrorInvalidRequest
        }
    }
    feed.engines = engines
    return
}

// index is deleted, delete all of its downstream
func (feed *Feed) deleteEngines(req Subscriber) (err error) {
    engines, _, err := buildEngines(req, feed.engines, feed.endpoints)
    if err != nil {
        return err
    }

    for bucket, emap := range bucketWiseEngines(engines) {
        if bfeed, ok := feed.bfeeds[bucket]; ok {
            uuids := make([]uint64, 0, len(emap))
            for uuid, _ := range emap {
                uuids = append(uuids, uuid)
            }
            if err = bfeed.DeleteEngines(uuids); err != nil {
                return
            }
        } else {
            return c.ErrorInvalidRequest
        }
    }
    feed.engines = engines
    return
}

// repair endpoints, restart engines and update bucket-feed
func (feed *Feed) repairEndpoints() error {
    var err error

    // repair endpoints
    endpoints := make(map[string]*Endpoint)
    for raddr, endpoint := range feed.endpoints {
        if !endpoint.Ping() {
            endpoint, err = NewEndpoint(raddr, c.ConnsPerEndpoint)
            if err != nil {
                return err
            }
        }
        endpoints[raddr] = endpoint
    }
    // restart engines
    engines := make(map[uint64]*Engine)
    for uuid, engine := range feed.engines {
        engine, err = NewEngine(uuid, engine.evaluator, engine.router, endpoints)
        if err != nil {
            return err
        }
        engines[uuid] = engine
    }
    // update Engines with BucketFeed
    for bucket, emap := range bucketWiseEngines(engines) {
        if bfeed, ok := feed.bfeeds[bucket]; ok {
            if err = bfeed.UpdateEngines(emap); err != nil {
                return err
            }
        } else {
            return c.ErrorInvalidRequest
        }
    }
    feed.engines, feed.endpoints = engines, endpoints
    return nil
}

func (feed *Feed) doClose() (err error) {
    for _, bfeed := range feed.bfeeds { // shutdown upstream
        bfeed.CloseFeed()
    }
    for _, engine := range feed.engines { // shutdown algorithms
        engine.Close()
    }
    for _, endpoint := range feed.endpoints { // shutdown downstream
        endpoint.Close()
    }
    // shutdown
    close(feed.finch)
    feed.bfeeds, feed.engines, feed.endpoints = nil, nil, nil
    return
}

// build per bucket uuid->engine map using Subscriber interface. `engines` and
// `endpoints` are updated inplace.
func buildEngines(
    subscriber Subscriber,
    engines map[uint64]*Engine,
    endpoints map[string]*Endpoint) (map[uint64]*Engine, map[string]*Endpoint, error) {

    evaluators, routers, err := validateSubscriber(subscriber)
    if err != nil {
        return engines, endpoints, err
    }
    endpoints = buildEndpoints(routers, endpoints)

    // gather all involved buckets
    buckets := make([]string, 0)
    for _, router := range routers {
        buckets = append(buckets, router.Bucket())
    }

    // Rebuild engines
    for uuid, evaluator := range evaluators {
        engine, err := NewEngine(uuid, evaluator, routers[uuid], endpoints)
        if err != nil {
            return engines, endpoints, err
        }
        engines[uuid] = engine
    }
    return engines, endpoints, nil
}

func bucketWiseEngines(engines map[uint64]*Engine) map[string]map[uint64]*Engine {
    // organize engines based on buckets, an engine is always associted with a
    // single bucket.
    bengines := make(map[string]map[uint64]*Engine)
    for uuid, engine := range engines {
        bucket := engine.evaluator.Bucket()
        emap, ok := bengines[bucket]
        if !ok {
            emap = make(map[uint64]*Engine)
        }
        emap[uuid] = engine
        bengines[bucket] = emap
    }
    return bengines
}

// start endpoints for listed topology, an endpoint is not started if it is
// already active.
func buildEndpoints(routers map[uint64]c.Router, endpoints map[string]*Endpoint) map[string]*Endpoint {
    for _, router := range routers {
        for _, raddr := range router.UuidEndpoints() {
            if endpoint, ok := endpoints[raddr]; (!ok) || (!endpoint.Ping()) {
                endpoint, err := NewEndpoint(raddr, c.ConnsPerEndpoint)
                if err == nil {
                    endpoints[raddr] = endpoint
                } else {
                    log.Printf("error starting endpoint %q: %v", raddr, err)
                }
            }
        }
    }
    return endpoints
}

func validateSubscriber(subscriber Subscriber) (map[uint64]c.Evaluator, map[uint64]c.Router, error) {
    evaluators, err := subscriber.GetEvaluators()
    if err != nil {
        return nil, nil, err
    }
    routers, err := subscriber.GetRouters()
    if err != nil {
        return nil, nil, err
    }
    if len(evaluators) != len(routers) {
        return nil, nil, ErrorInconsistentFeed
    }
    for uuid, _ := range evaluators {
        if _, ok := routers[uuid]; ok == false {
            return nil, nil, ErrorInconsistentFeed
        }
    }
    return evaluators, routers, nil
}
