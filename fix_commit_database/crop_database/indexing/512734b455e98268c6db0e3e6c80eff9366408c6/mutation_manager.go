// Copyright (c) 2014 Couchbase, Inc.
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
// except in compliance with the License. You may obtain a copy of the License at
//   http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software distributed under the
// License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
// either express or implied. See the License for the specific language governing permissions
// and limitations under the License.

package indexer

import (
	"log"
	"sync"
)

//MutationManager handles messages from Indexer to manage Mutation Streams
//and flush mutations from mutation queues.
type MutationManager interface {
	Shutdown()
}

//Map from bucket name to mutation queue
type BucketQueueMap map[string]IndexerMutationQueue

//Map from bucket name to flusher stop channel
type BucketStopChMap map[string]StopChannel

type mutationMgr struct {
	streamBucketQueueMap map[StreamId]BucketQueueMap
	streamIndexQueueMap  map[StreamId]IndexQueueMap

	streamReaderMap map[StreamId]MutationStreamReader

	streamReaderCmdChMap   map[StreamId]MsgChannel      //Command msg channel for StreamReader
	streamFlusherStopChMap map[StreamId]BucketStopChMap //stop channels for flusher
	mutMgrRecvCh           MsgChannel                   //Receive msg channel for Mutation Manager

	supvCmdch  MsgChannel //supervisor sends commands on this channel
	supvRespch MsgChannel //channel to send any message to supervisor

	sliceMap SliceMap

	numVbuckets uint16 //number of vbuckets

	flusher          Flusher //handle to flusher
	flusherWaitGroup sync.WaitGroup

	lock  sync.Mutex //lock to protect this structure
	flock sync.Mutex //fine-grain lock for streamFlusherStopChMap
}

const DEFAULT_NUM_STREAM_READER_WORKERS = 8
const DEFAULT_START_CHUNK_SIZE = 256
const DEFAULT_SLAB_SIZE = DEFAULT_START_CHUNK_SIZE * 1024
const DEFAULT_MAX_SLAB_MEMORY = DEFAULT_SLAB_SIZE * 1024

//NewMutationManager creates a new Mutation Manager which listens
//for commands from Indexer and process those.
//In case returned MutationManager is nil, Message will have the error msg.
func NewMutationManager(supvCmdch MsgChannel, supvRespch MsgChannel,
	numVbuckets uint16) (MutationManager, Message) {

	//Init the mutationMgr struct
	m := &mutationMgr{
		streamBucketQueueMap:   make(map[StreamId]BucketQueueMap),
		streamIndexQueueMap:    make(map[StreamId]IndexQueueMap),
		streamReaderMap:        make(map[StreamId]MutationStreamReader),
		streamReaderCmdChMap:   make(map[StreamId]MsgChannel),
		streamFlusherStopChMap: make(map[StreamId]BucketStopChMap),
		mutMgrRecvCh:           make(MsgChannel),
		supvCmdch:              supvCmdch,
		supvRespch:             supvRespch,
		numVbuckets:            numVbuckets,
		flusher:                NewFlusher(),
	}

	//start Mutation Manager loop which listens to commands from its supervisor
	go m.run()

	return m, nil

}

//Shutdown shuts down all stream readers and flushers
func (m *mutationMgr) Shutdown() {

	//shutdown stream readers
	donech := make(DoneChannel)
	go func(donech DoneChannel) {
		m.lock.Lock()
		defer m.lock.Unlock()
		for _, r := range m.streamReaderMap {
			r.Shutdown()
		}
		donech <- true
	}(donech)

	//wait for all readers to shutdown
	<-donech

	//stop flushers
	go func() {
		m.flock.Lock()
		defer m.flock.Unlock()

		//send stop signal to all flushers
		for _, bucketStopChMap := range m.streamFlusherStopChMap {
			for _, stopch := range bucketStopChMap {
				close(stopch)
			}
		}
	}()

	//wait for all flushers to finish before returning
	m.flusherWaitGroup.Wait()

}

//run starts the mutation manager loop which listens to messages
//from its workers(stream_reader and flusher) and
//supervisor(indexer)
func (m *mutationMgr) run() {

	//main Mutation Manager loop
	for {
		select {
		case cmd, ok := <-m.supvCmdch:
			if ok {
				m.handleSupervisorCommands(cmd)
			} else {
				log.Println("Supervisor Channel Closed." +
					"Mutation Manager Shutting Itself Down.")
				m.Shutdown()
				return
			}
		case msg, ok := <-m.mutMgrRecvCh:
			if ok {
				m.handleWorkerMessage(msg)
			} else {
				//Mutation Manager channel shouldn't get closed until shutdown
				m.supvRespch <- &MsgError{mType: ERROR,
					err: Error{code: ERROR_MUT_MGR_UNKNOWN_ERROR,
						severity: FATAL,
						category: MUTATION_MANAGER}}
			}
		}
	}

}

//handleSupervisorCommands handles the messages from Supervisor
func (m *mutationMgr) handleSupervisorCommands(cmd Message) {

	switch cmd.GetMsgType() {

	case MUT_MGR_OPEN_STREAM:
		m.handleOpenStream(cmd)

	case MUT_MGR_ADD_INDEX_LIST_TO_STREAM:
		m.handleAddIndexListToStream(cmd)

	case MUT_MGR_REMOVE_INDEX_LIST_FROM_STREAM:
		m.handleRemoveIndexListFromStream(cmd)

	case MUT_MGR_CLOSE_STREAM:
		m.handleCloseStream(cmd)

	case MUT_MGR_CLEANUP_STREAM:
		m.handleCleanupStream(cmd)

	case MUT_MGR_PERSIST_MUTATION_QUEUE:
		m.handlePersistMutationQueue(cmd)

	case MUT_MGR_DRAIN_MUTATION_QUEUE:
		m.handleDrainMutationQueue(cmd)

	case MUT_MGR_GET_MUTATION_QUEUE_HWT:
		m.handleGetMutationQueueHWT(cmd)

	case MUT_MGR_GET_MUTATION_QUEUE_LWT:
		m.handleGetMutationQueueLWT(cmd)

	case MUT_MGR_UPDATE_SLICE_MAP:
		m.handleUpdateSliceMap(cmd)

	default:
		m.supvCmdch <- &MsgError{mType: ERROR,
			err: Error{code: ERROR_MUT_MGR_UNKNOWN_COMMAND,
				severity: NORMAL,
				category: MUTATION_MANAGER}}
	}
}

//handleWorkerMessage handles messages from workers
func (m *mutationMgr) handleWorkerMessage(cmd Message) {

	switch cmd.GetMsgType() {

	case STREAM_READER_STREAM_DROP_DATA,
		STREAM_READER_STREAM_BEGIN,
		STREAM_READER_STREAM_END,
		STREAM_READER_ERROR:
		//send message to supervisor to take decision
		m.supvRespch <- cmd

	default:
		log.Println("Mutation Manager received unhandled message from worker", cmd)
	}

}

//handleOpenStream creates a new MutationStreamReader and
//initializes it with the mutation queue to store the
//mutations in.
func (m *mutationMgr) handleOpenStream(cmd Message) {

	streamId := cmd.(*MsgMutMgrStreamUpdate).GetStreamId()

	m.lock.Lock()
	defer m.lock.Lock()

	//return error if this stream is already open
	if _, ok := m.streamReaderMap[streamId]; ok {
		m.supvRespch <- &MsgError{mType: ERROR,
			err: Error{code: ERROR_MUT_MGR_STREAM_ALREADY_OPEN,
				severity: NORMAL,
				category: MUTATION_MANAGER}}
		return
	}

	indexList := cmd.(*MsgMutMgrStreamUpdate).GetIndexList()

	bucketQueueMap := make(BucketQueueMap)
	indexQueueMap := make(IndexQueueMap)
	//create a new mutation queue for the stream reader
	//for each bucket, a separate queue is required
	for _, i := range indexList {
		//if there is no mutation queue for this index, allocate a new one
		if _, ok := bucketQueueMap[i.Bucket]; !ok {
			//init mutation queue
			var queue MutationQueue
			if queue = NewAtomicMutationQueue(m.numVbuckets); queue == nil {
				m.supvCmdch <- &MsgError{mType: ERROR,
					err: Error{code: ERROR_MUTATION_QUEUE_INIT,
						severity: FATAL,
						category: MUTATION_QUEUE}}
				return
			}

			//init slab manager
			var slabMgr SlabManager
			var errMsg Message
			if slabMgr, errMsg = NewSlabManager(DEFAULT_START_CHUNK_SIZE,
				DEFAULT_SLAB_SIZE,
				DEFAULT_MAX_SLAB_MEMORY); slabMgr == nil {
				m.supvCmdch <- errMsg
				return
			}

			bucketQueueMap[i.Bucket] = IndexerMutationQueue{
				queue:   queue,
				slabMgr: slabMgr}
		}
		indexQueueMap[i.InstanceId] = bucketQueueMap[i.Bucket]
	}
	cmdCh := make(MsgChannel)

	reader, errMsg := CreateMutationStreamReader(streamId, indexQueueMap,
		cmdCh, m.mutMgrRecvCh, DEFAULT_NUM_STREAM_READER_WORKERS)

	if reader == nil {
		//send the error back on supv channel
		m.supvCmdch <- errMsg
	} else {
		//update internal structs
		m.streamReaderMap[streamId] = reader
		m.streamBucketQueueMap[streamId] = bucketQueueMap
		m.streamIndexQueueMap[streamId] = indexQueueMap
		m.streamReaderCmdChMap[streamId] = cmdCh
		m.streamFlusherStopChMap[streamId] = make(BucketStopChMap)

		//send success on supv channel
		m.supvCmdch <- &MsgSuccess{}
	}

}

//handleAddIndexListToStream adds a list of indexes to an
//already running MutationStreamReader. If the list has index
//for a bucket for which there is no mutation queue, it will
//be created.
func (m *mutationMgr) handleAddIndexListToStream(cmd Message) {

	streamId := cmd.(*MsgMutMgrStreamUpdate).GetStreamId()

	m.lock.Lock()
	defer m.lock.Lock()

	//return error if this stream is already closed
	if _, ok := m.streamReaderMap[streamId]; !ok {
		m.supvRespch <- &MsgError{mType: ERROR,
			err: Error{code: ERROR_MUT_MGR_STREAM_ALREADY_CLOSED,
				severity: NORMAL,
				category: MUTATION_MANAGER}}
		return
	}

	indexList := cmd.(*MsgMutMgrStreamUpdate).GetIndexList()

	bucketQueueMap := m.streamBucketQueueMap[streamId]
	indexQueueMap := m.streamIndexQueueMap[streamId]

	for _, i := range indexList {
		//if there is no mutation queue for this index, allocate a new one
		if _, ok := bucketQueueMap[i.Bucket]; !ok {
			//init mutation queue
			var queue MutationQueue
			if queue = NewAtomicMutationQueue(m.numVbuckets); queue == nil {
				m.supvCmdch <- &MsgError{mType: ERROR,
					err: Error{code: ERROR_MUTATION_QUEUE_INIT,
						severity: FATAL,
						category: MUTATION_QUEUE}}
				return
			}

			//init slab manager
			var slabMgr SlabManager
			var errMsg Message
			if slabMgr, errMsg = NewSlabManager(DEFAULT_START_CHUNK_SIZE,
				DEFAULT_SLAB_SIZE,
				DEFAULT_MAX_SLAB_MEMORY); slabMgr == nil {
				m.supvCmdch <- errMsg
				return
			}

			bucketQueueMap[i.Bucket] = IndexerMutationQueue{
				queue:   queue,
				slabMgr: slabMgr}
		}
		indexQueueMap[i.InstanceId] = bucketQueueMap[i.Bucket]
	}

	//send message to Stream Reader to update its map
	m.streamReaderCmdChMap[streamId] <- &MsgUpdateIndexQueue{indexQueueMap: indexQueueMap}
	//wait for response
	respMsg := <-m.streamReaderCmdChMap[streamId]

	if respMsg.GetMsgType() != SUCCESS {
		//send the error back on supv channel
		m.supvCmdch <- respMsg
	} else {
		//update internal structures
		m.streamBucketQueueMap[streamId] = bucketQueueMap
		m.streamIndexQueueMap[streamId] = indexQueueMap
		//send success on supv channel
		m.supvCmdch <- &MsgSuccess{}
	}
}

//handleRemoveIndexListFromStream removes a list of indexes from an
//already running MutationStreamReader. If all the indexes for a
//bucket get deleted, its mutation queue is dropped.
func (m *mutationMgr) handleRemoveIndexListFromStream(cmd Message) {

	streamId := cmd.(*MsgMutMgrStreamUpdate).GetStreamId()

	m.lock.Lock()
	defer m.lock.Lock()

	//return error if this stream is already closed
	if _, ok := m.streamReaderMap[streamId]; !ok {
		m.supvRespch <- &MsgError{mType: ERROR,
			err: Error{code: ERROR_MUT_MGR_STREAM_ALREADY_CLOSED,
				severity: NORMAL,
				category: MUTATION_MANAGER}}
		return
	}

	indexList := cmd.(*MsgMutMgrStreamUpdate).GetIndexList()

	bucketQueueMap := m.streamBucketQueueMap[streamId]
	indexQueueMap := m.streamIndexQueueMap[streamId]

	//delete the given indexes from map
	for _, i := range indexList {
		delete(indexQueueMap, i.InstanceId)
	}

	//send message to Stream Reader to update its map
	m.streamReaderCmdChMap[streamId] <- &MsgUpdateIndexQueue{indexQueueMap: indexQueueMap}
	//wait for response
	respMsg := <-m.streamReaderCmdChMap[streamId]

	if respMsg.GetMsgType() != SUCCESS {
		//send the error back on supv channel
		m.supvCmdch <- respMsg
	} else {
		//update internal structures
		//if all indexes for a bucket have been removed, drop the mutation queue
		for b, bq := range bucketQueueMap {
			dropBucket := true
			for i, iq := range indexQueueMap {
				//bad check: if the queues match, it is still being used
				//better way is to check with bucket
				if bq == iq {
					dropBucket = false
					break
				}
			}
			if dropBucket == true {
				delete(m.streamBucketQueueMap[streamId], b)
			}
		}
		m.streamIndexQueueMap[streamId] = indexQueueMap
		//send success on supv channel
		m.supvCmdch <- &MsgSuccess{}
	}
}

//handleCloseStream closes MutationStreamReader for the specified stream.
func (m *mutationMgr) handleCloseStream(cmd Message) {

	streamId := cmd.(*MsgMutMgrStreamUpdate).GetStreamId()

	m.lock.Lock()
	defer m.lock.Lock()

	//return error if this stream is already closed
	if reader, ok := m.streamReaderMap[streamId]; !ok {
		m.supvRespch <- &MsgError{mType: ERROR,
			err: Error{code: ERROR_MUT_MGR_STREAM_ALREADY_CLOSED,
				severity: NORMAL,
				category: MUTATION_MANAGER}}
		return
	} else {

		//shutdown the stream reader
		reader.Shutdown()

		//update internal data structures
		delete(m.streamReaderMap, streamId)
		delete(m.streamBucketQueueMap, streamId)
		delete(m.streamIndexQueueMap, streamId)
		delete(m.streamReaderCmdChMap, streamId)
		delete(m.streamFlusherStopChMap, streamId)

		//send success on supv channel
		m.supvCmdch <- &MsgSuccess{}

	}
}

//handleCleanupStream cleans up an already closed stream.
//This handles the case when a MutationStreamReader closes
//abruptly. This method can be used to clean up internal
//mutation manager structures.
func (m *mutationMgr) handleCleanupStream(cmd Message) {

	streamId := cmd.(*MsgMutMgrStreamUpdate).GetStreamId()

	m.lock.Lock()
	defer m.lock.Unlock()

	//clean up internal structs for this stream
	delete(m.streamReaderMap, streamId)
	delete(m.streamBucketQueueMap, streamId)
	delete(m.streamIndexQueueMap, streamId)
	delete(m.streamReaderCmdChMap, streamId)
	delete(m.streamFlusherStopChMap, streamId)
}

//handlePersistMutationQueue handles persist queue message from
//Indexer. Success is sent on the supervisor Cmd channel
//if the flush can be processed. Once the queue gets persisted,
//status is sent on the supervisor Response channel.
func (m *mutationMgr) handlePersistMutationQueue(cmd Message) {

	bucket := cmd.(*MsgMutMgrFlushMutationQueue).GetBucket()
	streamId := cmd.(*MsgMutMgrFlushMutationQueue).GetStreamId()
	ts := cmd.(*MsgMutMgrFlushMutationQueue).GetTimestamp()

	m.lock.Lock()
	defer m.lock.Unlock()

	q := m.streamBucketQueueMap[streamId][bucket]
	go m.persistMutationQueue(q, streamId, bucket, ts)
	m.supvCmdch <- &MsgSuccess{}

}

//persistMutationQueue implements the actual persist for the queue
func (m *mutationMgr) persistMutationQueue(q IndexerMutationQueue,
	streamId StreamId, bucket string, ts Timestamp) {

	m.flock.Lock()
	defer m.flock.Unlock()

	stopch := make(StopChannel)
	m.streamFlusherStopChMap[streamId][bucket] = stopch
	m.flusherWaitGroup.Add(1)

	go func() {
		defer m.flusherWaitGroup.Done()

		msgch := m.flusher.FlushQueueUptoTimestampWithoutPersistence(q.queue,
			streamId, m.sliceMap, ts, stopch)
		//wait for flusher to finish
		msg := <-msgch

		m.flock.Lock()
		defer m.flock.Unlock()

		//delete the stop channel from the map
		delete(m.streamFlusherStopChMap[streamId], bucket)

		//send the response to supervisor
		m.supvRespch <- msg
	}()

}

//handleDrainMutationQueue handles drain queue message from
//supervisor. Success is sent on the supervisor Cmd channel
//if the flush can be processed. Once the queue gets drained,
//status is sent on the supervisor Response channel.
func (m *mutationMgr) handleDrainMutationQueue(cmd Message) {

	bucket := cmd.(*MsgMutMgrFlushMutationQueue).GetBucket()
	streamId := cmd.(*MsgMutMgrFlushMutationQueue).GetStreamId()
	ts := cmd.(*MsgMutMgrFlushMutationQueue).GetTimestamp()

	m.lock.Lock()
	defer m.lock.Unlock()

	q := m.streamBucketQueueMap[streamId][bucket]
	go m.drainMutationQueue(q, streamId, bucket, ts)
	m.supvCmdch <- &MsgSuccess{}
}

//drainMutationQueue implements the actual drain for the queue
func (m *mutationMgr) drainMutationQueue(q IndexerMutationQueue,
	streamId StreamId, bucket string, ts Timestamp) {

	m.flock.Lock()
	defer m.flock.Unlock()

	stopch := make(StopChannel)
	m.streamFlusherStopChMap[streamId][bucket] = stopch
	m.flusherWaitGroup.Add(1)

	go func() {
		defer m.flusherWaitGroup.Done()

		msgch := m.flusher.FlushQueueUptoTimestampWithoutPersistence(q.queue,
			streamId, m.sliceMap, ts, stopch)
		//wait for flusher to finish
		msg := <-msgch

		m.flock.Lock()
		defer m.flock.Unlock()

		//delete the stop channel from the map
		delete(m.streamFlusherStopChMap[streamId], bucket)

		//send the response to supervisor
		m.supvRespch <- msg
	}()

}

//handleGetMutationQueueHWT calculates HWT for a mutation queue
//for a given stream and bucket
func (m *mutationMgr) handleGetMutationQueueHWT(cmd Message) {

	bucket := cmd.(*MsgMutMgrGetTimestamp).GetBucket()
	streamId := cmd.(*MsgMutMgrGetTimestamp).GetStreamId()

	m.lock.Lock()
	defer m.lock.Unlock()

	q := m.streamBucketQueueMap[streamId][bucket]

	go func() {
		ts := m.flusher.GetQueueHWT(q.queue)
		m.supvCmdch <- &MsgTimestamp{ts: ts}
	}()
}

//handleGetMutationQueueLWT calculates LWT for a mutation queue
//for a given stream and bucket
func (m *mutationMgr) handleGetMutationQueueLWT(cmd Message) {

	bucket := cmd.(*MsgMutMgrGetTimestamp).GetBucket()
	streamId := cmd.(*MsgMutMgrGetTimestamp).GetStreamId()

	m.lock.Lock()
	defer m.lock.Unlock()

	q := m.streamBucketQueueMap[streamId][bucket]

	go func() {
		ts := m.flusher.GetQueueLWT(q.queue)
		m.supvCmdch <- &MsgTimestamp{ts: ts}
	}()
}

//handleUpdateSliceMap updates the slice map
func (m *mutationMgr) handleUpdateSliceMap(cmd Message) {

	m.lock.Lock()
	defer m.lock.Unlock()

	m.sliceMap = cmd.(*MsgMutMgrUpdateSliceMap).GetSliceMap()

}

//instance issue
