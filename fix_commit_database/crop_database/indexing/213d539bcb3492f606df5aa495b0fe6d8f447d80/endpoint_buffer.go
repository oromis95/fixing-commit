package projector

import (
	c "github.com/couchbase/indexing/secondary/common"
	"github.com/couchbase/indexing/secondary/indexer"
	"log"
)

type endpointBuffers struct {
	endpoint *Endpoint
	raddr    string
	vbs      map[uint32]*c.VbKeyVersions
}

func newEndpointBuffers(endpoint *Endpoint, raddr string) *endpointBuffers {
	vbs := make(map[uint32]*c.VbKeyVersions)
	b := &endpointBuffers{endpoint, raddr, vbs}
	return b
}

// addKeyVersions, add a mutation to VbKeyVersions.
func (b *endpointBuffers) addKeyVersions(bucketcode byte, vbno uint16, vbuuid uint64, kv *c.KeyVersions) {
	id := c.ID(bucketcode, vbno)
	if _, ok := b.vbs[id]; !ok {
		nMuts := 10 // TODO: avoid magic numbers
		b.vbs[id] = c.NewVbKeyVersions(bucketcode, vbno, vbuuid, nMuts)
	}
	b.vbs[id].AddKeyVersions(kv)
}

// flush the buffers to the other end.
func (b *endpointBuffers) flushBuffers(client *indexer.StreamClient) error {
	vbs := make([]*c.VbKeyVersions, 0, len(b.vbs))
	for _, vb := range b.vbs {
		vbs = append(vbs, vb)
	}
	b.vbs = make(map[uint32]*c.VbKeyVersions) // re-initialize
	if err := client.SendKeyVersions(vbs); err != nil {
		endp := b.endpoint
		log.Printf("%v, error sending keyversions: %v\n", endp.logPrefix, err)
		return err
	}
	return nil
}
