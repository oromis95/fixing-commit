package indexer

import (
	"fmt"
	c "github.com/couchbase/indexing/secondary/common"
	"github.com/couchbase/indexing/secondary/protobuf"
	"io/ioutil"
	"log"
	"testing"
)

var addr = "localhost:8888"

func TestStreamClient(t *testing.T) {
	maxconns, maxvbuckets, mutChanSize := 2, 8, 100
	log.SetOutput(ioutil.Discard)

	// start server
	msgch := make(chan interface{}, mutChanSize)
	errch := make(chan interface{}, 1000)
	daemon := doServer(addr, t, msgch, errch, 100)
	flags := StreamTransportFlag(0).SetProtobuf()

	// start client and test number of connection.
	client, err := NewStreamClient(addr, maxconns, flags)
	if err != nil {
		t.Fatal(err)
	} else if len(client.conns) != maxconns {
		t.Fatal("failed stream client connections")
	} else if len(client.conns) != len(client.connChans) {
		t.Fatal("failed stream client connection channels")
	} else if len(client.conns) != len(client.conn2Vbs) {
		t.Fatal("failed stream client connection channels")
	} else {
		num := 2
		vbmaps := makeVbmaps(maxvbuckets, num) // vbmaps
		for i := 0; i < num; i++ {
			if err := client.SendVbmap(vbmaps[i]); err != nil {
				t.Fatal(err)
			}
		}
		validateClientInstance(client, maxvbuckets, maxconns, num, t)
	}
	client.Close()
	daemon.Close()
}

func TestStreamClientBegin(t *testing.T) {
	maxconns, maxvbuckets, mutChanSize := 2, 8, 100
	log.SetOutput(ioutil.Discard)

	// start server
	msgch := make(chan interface{}, mutChanSize)
	errch := make(chan interface{}, 1000)
	daemon := doServer(addr, t, msgch, errch, 100)
	flags := StreamTransportFlag(0).SetProtobuf()

	// start client
	client, _ := NewStreamClient(addr, maxconns, flags)
	num := 2
	vbmaps := makeVbmaps(maxvbuckets, num) // vbmaps
	for i := 0; i < num; i++ {
		if err := client.SendVbmap(vbmaps[i]); err != nil {
			t.Fatal(err)
		}
	}
	// test a live StreamBegin
	bcode, vbno, vbuuid := byte(1), uint16(maxvbuckets), uint64(1111)
	id := c.ID(bcode, vbno)
	vals, _ := client.Getcontext()
	vbChans := vals[1].(map[uint32]chan interface{})
	if _, ok := vbChans[id]; ok {
		t.Fatal("duplicate id")
	}
	vb := c.NewVbKeyVersions(bcode, vbno, vbuuid, 1)
	seqno, docid, maxCount := uint64(10), []byte("document-name"), 10
	kv := c.NewKeyVersions(seqno, docid, maxCount)
	kv.AddStreamBegin()
	vb.AddKeyVersions(kv)
	err := client.SendKeyVersions([]*c.VbKeyVersions{vb})
	client.Getcontext() // syncup
	if err != nil {
		t.Fatal(err)
	} else if _, ok := vbChans[id]; !ok {
		fmt.Printf("%v %v\n", len(vbChans), id)
		t.Fatal("failed StreamBegin")
	}
	client.Close()
	daemon.Close()
}

func TestStreamClientEnd(t *testing.T) {
	maxconns, maxvbuckets, mutChanSize := 2, 8, 100
	log.SetOutput(ioutil.Discard)

	// start server
	msgch := make(chan interface{}, mutChanSize)
	errch := make(chan interface{}, 1000)
	daemon := doServer(addr, t, msgch, errch, 100)
	flags := StreamTransportFlag(0).SetProtobuf()

	// start client
	client, _ := NewStreamClient(addr, maxconns, flags)
	num := 2
	vbmaps := makeVbmaps(maxvbuckets, num) // vbmaps
	for i := 0; i < num; i++ {
		if err := client.SendVbmap(vbmaps[i]); err != nil {
			t.Fatal(err)
		}
	}
	// test a live StreamEnd
	bcode := vbmaps[0].Bucketcode
	vbno := vbmaps[0].Vbuckets[0]
	vbuuid := uint64(vbmaps[0].Vbuuids[0])
	id := c.ID(bcode, vbno)
	vals, _ := client.Getcontext()
	vbChans := vals[1].(map[uint32]chan interface{})
	if _, ok := vbChans[id]; !ok {
		t.Fatal("expected id")
	}
	vb := c.NewVbKeyVersions(bcode, vbno, vbuuid, 1)
	seqno, docid, maxCount := uint64(10), []byte("document-name"), 10
	kv := c.NewKeyVersions(seqno, docid, maxCount)
	kv.AddStreamEnd()
	vb.AddKeyVersions(kv)
	err := client.SendKeyVersions([]*c.VbKeyVersions{vb})
	client.Getcontext() // syncup
	if err != nil {
		t.Fatal(err)
	} else if _, ok := vbChans[id]; ok {
		t.Fatal("failed StreamEnd")
	}
	client.Close()
	daemon.Close()
}

func doServer(addr string, tb testing.TB, msgch, errch chan interface{}, mutChanSize int) *MutationStream {
	var mStream *MutationStream
	var err error

	mutch := make(chan []*protobuf.VbKeyVersions, mutChanSize)
	sbch := make(chan interface{}, 100)
	if mStream, err = NewMutationStream(addr, mutch, sbch); err != nil {
		tb.Fatal(err)
	}

	go func() {
		var mutn, err interface{}
		var ok bool
		for {
			select {
			case mutn, ok = <-mutch:
				msgch <- mutn
			case err, ok = <-sbch:
				errch <- err
			}
			if ok == false {
				return
			}
		}
	}()
	return mStream
}

func makeVbmaps(maxvbuckets int, num int) []*c.VbConnectionMap {
	vbmaps := make([]*c.VbConnectionMap, 0, num)
	for i := 0; i < num; i++ {
		vbmap := &c.VbConnectionMap{
			Bucket:     fmt.Sprintf("default%v", i),
			Bucketcode: byte(i),
			Vbuckets:   make([]uint16, 0, maxvbuckets),
			Vbuuids:    make([]uint64, 0, maxvbuckets),
		}
		for i := 0; i < maxvbuckets; i++ {
			vbmap.Vbuckets = append(vbmap.Vbuckets, uint16(i))
			vbmap.Vbuuids = append(vbmap.Vbuuids, uint64(i*10))
		}
		vbmaps = append(vbmaps, vbmap)
	}
	return vbmaps
}

func verify(msgch, errch chan interface{}, fn func(mutn, err interface{})) {
	select {
	case mutn := <-msgch:
		fn(mutn, nil)
	case err := <-errch:
		fn(nil, err)
	}
}

func validateClientInstance(client *StreamClient, maxvbuckets, maxconns, num int, t *testing.T) {
	ref := ((maxvbuckets / maxconns) * num)
	// validate bucket-codes
	vals, _ := client.Getcontext()
	bcodes := vals[0].(map[byte]string)
	vbChans := vals[1].(map[uint32]chan interface{})
	for bucketcode, bucket := range bcodes {
		ref := fmt.Sprintf("default%v", bucketcode)
		if bucket != ref {
			t.Fatal("failed stream client, bcodes")
		}
	}
	// validate vbucket channels
	refChans := make(map[chan interface{}][]uint32)
	for id, ch := range vbChans {
		if ids, ok := refChans[ch]; !ok {
			refChans[ch] = make([]uint32, 0)
		} else {
			refChans[ch] = append(ids, id)
		}
	}
	// validate connection to vbuckets.
	conn2Vbs := vals[2].(map[int][]uint32)
	for _, ids := range conn2Vbs {
		if len(ids) != ref {
			t.Fatal("failed stream client, vbucket mapping")
		}
	}
}
