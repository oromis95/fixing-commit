package adminport

import (
	"code.google.com/p/goprotobuf/proto"
	"fmt"
	"github.com/couchbase/indexing/secondary/protobuf"
	"reflect"
	"testing"
)

func TestLoopBack(t *testing.T) {
	q := make(chan bool)
	server := doServer(t, q)

	client := NewHttpClient("http://localhost:8888")
	req := &protobuf.Mutation{
		Version:  proto.Uint32(uint32(1)),
		Command:  proto.Uint32(uint32(1)),
		Vbucket:  proto.Uint32(uint32(512)),
		Vbuuid:   proto.Uint64(uint64(0x1234567812345678)),
		Docid:    []byte("cities"),
		Seqno:    proto.Uint64(uint64(10000000)),
		Keys:     [][]byte{[]byte("bangalore"), []byte("delhi"), []byte("jaipur")},
		Oldkeys:  [][]byte{[]byte("varanasi"), []byte("pune"), []byte("mahe")},
		Indexids: []uint32{1, 2, 3},
	}
	resp := &protobuf.Mutation{}
	if err := client.Request(req, resp); err != nil {
		t.Fatal(err)
	}
	if reflect.DeepEqual(req, resp) == false {
		t.Fatal(fmt.Errorf("unexpected response"))
	}
	server.Stop()

	<-q
}

func doServer(t *testing.T, quit chan bool) AdminServer {
	reqch := make(chan AdminRequest, 10)
	server := NewHttpServer("localhost:8888", 0, 0, reqch)
	if err := server.Register(&protobuf.Mutation{}); err != nil {
		t.Fatal(err)
	}

	go func() {
		if err := server.Start(); err != nil {
			t.Fatal(err)
		}

	loop:
		for {
			select {
			case req, ok := <-reqch:
				if ok {
					msg := req.GetMessage().(*protobuf.Mutation)
					if err := req.Send(msg); err != nil {
						t.Fatal(err)
					}
				} else {
					break loop
				}
			}
		}
		close(quit)
	}()

	return server
}
