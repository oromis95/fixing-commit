package forestdb

type Lock struct {
	initialized bool
	ch          chan int
}

func (l *Lock) Lock() {
	if !initializied {
		l.ch = make(chan int, 1)
		l.initialized = true
		l.ch <- 1
	}

	printStack := true
loop:
	for {
		select {
		case <-l.ch:
			break loop
		default:
			if printStack {
				Log.Infof("Unable to acquire lock\n%s", string(debug.Stack()))
				printStack = false
			}
		}
	}
}

func (l *Lock) Unlock() {
	l.ch <- 1
}
