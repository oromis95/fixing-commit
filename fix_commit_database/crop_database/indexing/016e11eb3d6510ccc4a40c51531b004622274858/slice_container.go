// Copyright (c) 2014 Couchbase, Inc.
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
// except in compliance with the License. You may obtain a copy of the License at
//   http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software distributed under the
// License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
// either express or implied. See the License for the specific language governing permissions
// and limitations under the License.

package common

//SliceContainer contains all slices for an index partition
//and provides methods to determine how data
//is distributed in multiple slices for a single partition
type SliceContainer interface {

	//Return Slice for the given IndexKey
	GetSliceByIndexKey(IndexKey) Slice

	//Return SliceId for the given IndexKey
	GetSliceIdByIndexKey(IndexKey) SliceId

	//Return Slice for the given SliceId
	GetSliceById(SliceId) Slice
}

//hashedSliceContainer provides a hash based implementation
//for SliceContainer. Each IndexKey is hashed to determine
//which slice it belongs to.
type HashedSliceContainer struct {
	SliceList []Slice
	NumSlices int
}

//GetSliceByIndexKey returns Slice for the given IndexKey
//This is a convenience method which calls other interface methods
//to first determine the sliceId from IndexKey and then the slice from
//sliceId
func (s *hashedSliceContainer) GetSliceByIndexKey(key IndexKey) Slice {

	id := s.GetSliceIdByIndexKey(key)
	return s.GetSliceById(id)

}

//GetSliceIdByIndexKey returns SliceId for the given IndexKey
func (s *hashedSliceContainer) GetSliceIdByIndexKey(key IndexKey) SliceId {

	//run hash function on index key and return slice id
	hash := crc32.ChecksumIEEE([]byte(key[0]))
	sliceId := int(hash) % s.NumSlices
	return sliceId
}

//GetSliceById returns Slice for the given SliceId
func (s *hashedSliceContainer) GetSliceById(id SliceId) Slice {

	if id < len(s.SliceList) {
		return s.SliceList[id]
	} else {
		log.Println("HashedSliceContainer: Invalid SliceId %v", id)
		return nil
	}

}
