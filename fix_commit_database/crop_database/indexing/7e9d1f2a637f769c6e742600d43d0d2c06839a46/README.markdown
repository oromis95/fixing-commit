## Examples

# Usage:
    Usage: ./querycmd -type scanAll -index idx1 -bucket default
      -bucket="default": Bucket name
      -buffersz=0: Rows buffer size per internal message
      -equal="": Range: [key]
      -exprs="": Comma separated index exprs. Eg: '(`personal_details`.`age`),(`personal_detail`.`first_name`)'
      -high="": Range: [high]
      -incl=0: Range: 0|1|2|3
      -index="": Index name
      -instanceid="": Index instanceId
      -limit=10: Row limit
      -low="": Range: [low]
      -primary=false: Is primary index
      -server="localhost:7000": index server or scan server address
      -type="scanAll": Index command (scan|stats|scanAll|create|drop|list)

# Scan
    $ querycmd -type=scanAll -bucket default -server localhost:7000 -index abcd
    $ querycmd -type=scanAll -index abcd -limit 0
    $ querycmd -type=scan -index state -low='["Ar"]' -high='["Co"]' -buffersz=300
    $ querycmd -type=scan -index name_state_age -low='["Ar"]' -high='["Arlette", "N"]'
    $ querycmd -type scan -index '#primary' -equal='["Adena_54605074"]'

# Create
    $ querycmd -type create -bucket default -index first_name -server localhost:9101 -exprs='(`personal_detail`.`first_name`)'
    $ querycmd -type create -bucket default -server localhost:9101 -primary=true

# Drop
    $ querycmd -type drop -instanceid 1234 -server localhost:9101

# List
    $ querycmd -type list -server localhost:9101

