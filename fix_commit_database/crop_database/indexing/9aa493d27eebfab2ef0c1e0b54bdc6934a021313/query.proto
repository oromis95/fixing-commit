package protobuf;

// Error message can be sent back as response or
// encapsulated in response packets.
message Error {
    required string error = 1; // Empty string means success
}

// Request can be one of the optional field.
message QueryPayload {
    required uint32             version           = 1;
    optional StatisticsRequest  statisticsRequest = 2;
    optional ScanRequest        scanRequest       = 3;
    optional ScanAllRequest     scanAllRequest    = 4;
    optional EndStreamRequest   endStream         = 5;
    optional StatisticsResponse statistics        = 6;
    optional ResponseStream     stream            = 7;
    optional StreamEndResponse  streamEnd         = 8;
}

// Get Index statistics. StatisticsResponse is returned back from indexer.
message StatisticsRequest {
    required Span   span      = 1;
    required string indexName = 3;
    required string bucket    = 4;
}

message StatisticsResponse {
    required IndexStatistics stats = 1;
    optional Error           err   = 2;
}

// Scan request to indexer.
message ScanRequest {
    required Span   span      = 1;
    required bool   distinct  = 2;
    required int64  limit     = 3;
    required int64  pageSize  = 4;
    required string indexName = 5;
    required string bucket    = 6;
}

// Full table scan request from indexer.
message ScanAllRequest {
    required int64  pageSize  = 1;
    required int64  limit     = 2;
    required string indexName = 3;
    required string bucket    = 4;
}

// Request by client to stop streaming the query results.
message EndStreamRequest {
}

message ResponseStream {
    repeated IndexEntry entries = 1;
    optional Error      err     = 2;
}

// Last response packet sent by server to end query results.
message StreamEndResponse {
    optional Error err = 1;
}

// Query messages / arguments for indexer

message Span {
    required Range  range = 1;
    repeated bytes  equal = 2;
}

message Range {
    required bytes  low       = 1;
    required bytes  high      = 2;
    required uint32 inclusion = 3;
}

message IndexEntry {
    required bytes  entryKey   = 1;
    required bytes  primaryKey = 2;
}

// Statistics of a given index.
message IndexStatistics {
    required uint64 count      = 1;
    required uint64 uniqueKeys = 2;
    required bytes  min        = 3;
    required bytes  max        = 4;
}

