package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/couchbase/indexing/secondary/coordinator"
)

var config = flag.String("config", "config.json", "Config file")
var help = flag.Bool("help", false, "Help")

func main() {
	flag.Parse()

	if *help {
		fmt.Printf("Usage: %s -config config.json\n", os.Args[0])
		os.Exit(0)
	}

	cfg := &coordinator.Config{}
	err := cfg.Parse(*config)
	if err != nil {
		fmt.Printf("Invalid coordinator config file (%v)\n", err)
		os.Exit(1)
	}

	coord := coordinator.NewServer(cfg)
	coord.Start()

	if coord.Loop() != nil {
		os.Exit(1)
	}
}
