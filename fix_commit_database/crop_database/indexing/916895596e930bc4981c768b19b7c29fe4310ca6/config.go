// Configuration for Coordinator
package coordinator

import (
	"encoding/json"
	"io/ioutil"
)

type Config struct {
	AdminPortAddr    string
	MutationPortAddr string
}

func (c *Config) Parse(path string) error {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	err = json.Unmarshal(data, c)
	if err != nil {
		return err
	}

	return nil
}
