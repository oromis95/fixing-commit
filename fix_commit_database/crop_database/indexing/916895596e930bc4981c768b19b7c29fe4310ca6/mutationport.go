package coordinator

import (
	"github.com/couchbase/indexing/secondary/indexer"
	"github.com/couchbase/indexing/secondary/protobuf"
)

// Mutation port receives key versions and control messages related to mutations
// Subscribe* methods should be invoked before starting mutation port
type mutationPort struct {
	c     *Coordinator
	ms    *indexer.MutationStream
	mutch chan []*protobuf.VbKeyVersions
	sbch  chan interface{}

	// Subscribers list
	mutsubs []chan []*protobuf.VbKeyVersions
	sbsubs  []chan interface{}
}

func newMutationPort(c *Coordinator) *mutationPort {
	mutch := make(chan []*protobuf.VbKeyVersions)
	sbch := make(chan interface{})

	return &mutationPort{
		c:     c,
		mutch: mutch,
		sbch:  sbch,
	}
}

func (port *mutationPort) Start() error {
	var err error

	port.ms, err = indexer.NewMutationStream(
		port.c.config.MutationPortAddr,
		port.mutch,
		port.sbch)

	if err != nil {
		return err
	}

	go port.handleMutationMessages()
	go port.handleControlMessages()

	return nil
}

func (port *mutationPort) Stop() {
	port.ms.Close()
}

func (port *mutationPort) SubscribeMutationEvents(ch chan []*protobuf.VbKeyVersions) {
	port.mutsubs = append(port.mutsubs, ch)
}

func (port *mutationPort) SubscribeControlEvents(ch chan interface{}) {
	port.sbsubs = append(port.sbsubs, ch)
}

func (port *mutationPort) handleMutationMessages() {
	for mutMsg := range port.mutch {
		for _, ch := range port.mutsubs {
			ch <- mutMsg
		}
	}
}

func (port *mutationPort) handleControlMessages() {
	for ctrlMsg := range port.sbch {
		for _, ch := range port.sbsubs {
			ch <- ctrlMsg
		}
	}
}
