package protobuf

import (
	"code.google.com/p/goprotobuf/proto"
)

// VersionRequest implement MessageMarshaller interface
func (req *VersionRequest) Name() string {
	return "versionRequest"
}

func (req *VersionRequest) ContentType() string {
	return "application/protobuf"
}

func (req *VersionRequest) Encode() (data []byte, err error) {
	return proto.Marshal(req)
}

func (req *VersionRequest) Decode(data []byte) (err error) {
	return proto.Unmarshal(data, req)
}

// VersionResponse implement MessageMarshaller interface
func (req *VersionResponse) Name() string {
	return "versionResponse"
}

func (req *VersionResponse) ContentType() string {
	return "application/protobuf"
}

func (req *VersionResponse) Encode() (data []byte, err error) {
	return proto.Marshal(req)
}

func (req *VersionResponse) Decode(data []byte) (err error) {
	return proto.Unmarshal(data, req)
}
