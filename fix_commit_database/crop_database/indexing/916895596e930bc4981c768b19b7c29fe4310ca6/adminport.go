package coordinator

import (
	"github.com/couchbase/indexing/secondary/adminport"
	"github.com/couchbase/indexing/secondary/common"
	"github.com/couchbase/indexing/secondary/protobuf"
)

var reqVersion = &protobuf.VersionRequest{}

type adminPort struct {
	c      *Coordinator
	server adminport.Server
	reqch  chan adminport.Request
}

func newAdminPort(c *Coordinator) *adminPort {
	reqch := make(chan adminport.Request)

	server := adminport.NewHTTPServer(
		"coordinator",
		c.config.AdminPortAddr,
		reqch)

	server.Register(reqVersion)

	return &adminPort{
		c:      c,
		server: server,
		reqch:  reqch,
	}
}

func (ap *adminPort) Start() error {
	var err error

	err = ap.server.Start()
	if err != nil {
		return err
	}

	go func() {
		for {
			select {
			case req, ok := <-ap.reqch:
				if ok == false {
					break
				}

				msg := req.GetMessage()
				if reply, err := ap.handleRequest(msg); err == nil {
					req.Send(reply)
				} else {
					req.SendError(err)
				}
			}
		}

	}()

	return nil
}

func (ap *adminPort) Stop() {
	ap.server.Stop()
}

func (ap *adminPort) handleRequest(msg adminport.MessageMarshaller) (reply adminport.MessageMarshaller, err error) {
	switch req := msg.(type) {
	case *protobuf.VersionRequest:
		reply, err = ap.doVersionRequest(req)
	default:
		err = common.ErrorInvalidRequest
	}

	return
}

func (ap *adminPort) doVersionRequest(r *protobuf.VersionRequest) (adminport.MessageMarshaller, error) {
	version := "development"

	return &protobuf.VersionResponse{Version: &version}, nil
}
