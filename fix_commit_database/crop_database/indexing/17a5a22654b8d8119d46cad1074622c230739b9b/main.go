// Implements gometa watchdog that starts up gometa process with necessary configuration
// Gometa currently does not have capability to make dynamic membership changes. List of all gometa
// nodes should be known before gometa process startup.
// To workaround this problem, the gometa watchdog process will monitor for node membership changes
// and restart gometa process with new configuration whenever nodelist in the cluster changes.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/couchbase/gometa/server"
	"github.com/couchbase/indexing/secondary/common"
	"io"
	"io/ioutil"
	"net"
	"os"
	"os/exec"
	"reflect"
	"strconv"
	"time"
)

var (
	cluster string
)

const (
	DEFAULT_POOL             = "default"
	LOG_PREFIX               = "gometa-watchdog: "
	CONF_BASE_DIR            = ""
	GOMETA_PATH              = "gometa"
	CLUSTER_REFRESH_INTERVAL = 5 * time.Second
)

// Assuming that messageAddr and RequestAddr use consecutive port numbers
func newGometaNode(addr string) *server.Node {
	host, port, err := net.SplitHostPort(addr)
	if err != nil {
		panic(err)
	}
	portInt, err := strconv.Atoi(port)
	if err != nil {
		panic(err)
	}

	return &server.Node{
		ElectionAddr: addr,
		MessageAddr:  net.JoinHostPort(host, fmt.Sprint(portInt+1)),
		RequestAddr:  net.JoinHostPort(host, fmt.Sprint(portInt+2)),
	}
}

func newGometaConfig(cinfo *common.ClusterInfoCache,
	self common.NodeId, nodes []common.NodeId) *server.Config {

	conf := new(server.Config)
	for _, nid := range nodes {
		addr, err := cinfo.GetServiceAddress(nid, "gometa")
		addr = common.LocalListenAddr(addr)
		if err != nil {
			panic(err)
		}
		if nid == self {
			conf.Host = newGometaNode(addr)
		} else {
			conf.Peer = append(conf.Peer, newGometaNode(addr))
		}
	}

	return conf
}

func membershipWatcher(cluster string, notifyCh chan interface{}) {
	var nodes []common.NodeId
	var self common.NodeId

	url := fmt.Sprint("http://", cluster)
	cinfo := common.NewClusterInfoCache(url, DEFAULT_POOL)
	cinfo.SetLogPrefix(LOG_PREFIX)
	cinfo.SetMaxRetries(600)

	for {
		time.Sleep(CLUSTER_REFRESH_INTERVAL)
		err := cinfo.Fetch()
		if err != nil {
			panic(err)
		}
		currNodes := cinfo.GetNodesByServiceType("gometa")
		if !reflect.DeepEqual(currNodes, nodes) {
			common.Infof(LOG_PREFIX + "Node membership changed, attempting gometa restart")
			if self, err = cinfo.GetCurrentNode(); err != nil {
				// Current node is in inactive state
				continue
			}
			config := newGometaConfig(cinfo, self, currNodes)
			notifyCh <- config
		}
		nodes = currNodes
	}
}

func gometaSpawner(ch chan interface{}) {
	var process *exec.Cmd
	var conf *server.Config
	var stdinCloser io.Closer

	restartch := make(chan bool, 1)

loop:
	for {
		select {
		case msg := <-ch:
			conf = msg.(*server.Config)
			if process == nil {
				restartch <- true
			} else {
				stdinCloser.Close()
			}
			continue loop
		case <-restartch:
		}

		bytes, _ := json.Marshal(conf)
		file, err := ioutil.TempFile(CONF_BASE_DIR, "gometa-conf-")
		if err != nil {
			panic(err)
		}

		if _, err = file.Write(bytes); err != nil {
			panic(err)
		}

		filepath := file.Name()
		file.Close()

		common.Infof("Starting gometa with config: %v", filepath)
		process = exec.Command(GOMETA_PATH, "-config", filepath)
		if stdinCloser, err = process.StdinPipe(); err != nil {
			panic(err)
		}

		process.Stdout = os.Stderr
		process.Stderr = os.Stderr
		if err = process.Start(); err != nil {
			common.Errorf("Failed to start gometa %v", err)
			os.Exit(1)
		}

		go func() {
			common.Errorf("Gometa process died, attempting gometa restart")
			restartch <- true
		}()
	}
}

func main() {
	flag.StringVar(&cluster, "cluster", "localhost:9000", "Cluster address")
	flag.Parse()

	ch := make(chan interface{})
	common.SetLogLevel(common.LogLevelInfo)

	go membershipWatcher(cluster, ch)
	go gometaSpawner(ch)

	common.ExitOnStdinClose()
}
