##Documentation

- All documentation is done through text markup, preferably with markdown
  syntax.

- We encourage using diagrams where ever it is relevant or necessary. Few of
  us use yed to draw diagrams and save it as `graphml` files. You can add
  references to these images from within your markdown documents.

- To enable spell checking in vim use this option
    > :setlocal spell spelllang=en_us

  to learn more about spell-checker shortcuts.
    > :help spell

- It is convenient to use chrome plugins like `Markdown Preview` to visually
  check document formatting before committing your changes to git.
  After installing it from chrome-extensions, enable "Allow access to file URLs"
  in Extensions (menu > Tools > Extensions). Then open the file in chrome to
  see the visual effects of markdown.

- If the markdown file is stored with `.md` file extension, it could get
  interpreted a modula-2 file. To enable correct syntax highlighting refer to
  your editor documentation.

- **please try to use 80 column width for text documents**, although it is not a
  strict rule let there be a valid reason for lines exceeding 80 columns.
