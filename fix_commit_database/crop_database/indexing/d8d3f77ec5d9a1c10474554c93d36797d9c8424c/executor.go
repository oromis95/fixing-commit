package main

import (
	c "github.com/couchbase/indexing/secondary/common"
	qclient "github.com/couchbase/indexing/secondary/queryport/client"
	"time"
	"sync"
	"fmt"
)

func RunScan(client *qclient.GsiClient,
	spec *ScanConfig) {
        callb := func(res qclient.ResponseReader) bool {
                 return true
        }

	for i := 0; i < spec.Repeat+1; i++ {
			client.Range(spec.DefnId, spec.Low, spec.High,
				qclient.Inclusion(spec.Inclusion), false, spec.Limit, c.AnyConsistency, nil, callb)

	}

}

func RunCommands(cluster string, cfg *Config) (*Result, error) {
	var result Result
        var wg sync.WaitGroup

	config := c.SystemConfig.SectionConfig("queryport.client.", true)
	client, err := qclient.NewGsiClient(cluster, config)
	if err != nil {
		return nil, err
	}

	indexes, err := client.Refresh()
	if err != nil {
		return nil, err
	}

	for i, spec := range cfg.ScanSpecs {
		if spec.Id == 0 {
			spec.Id = uint64(i)
		}

		for _, index := range indexes {
			if index.Definition.Bucket == spec.Bucket &&
				index.Definition.Name == spec.Index {
				spec.DefnId = uint64(index.Definition.DefnId)
			}
		}

	        for j := 0; j< 20; j++ {
                    wg.Add(1)
                    go func(wg *sync.WaitGroup) {
         	       RunScan(client, spec)
                       wg.Done()
                    }(&wg)
                }
	        startTime := time.Now()
                wg.Wait()
	        dur := time.Now().Sub(startTime)
                fmt.Printf("total throughput duration in seconds: %f\n", dur.Seconds())
                fmt.Printf("throughput number of scans per sec: %f\n", (20*1000)/dur.Seconds())
        }

	client.Close()

	return &result, err
}
