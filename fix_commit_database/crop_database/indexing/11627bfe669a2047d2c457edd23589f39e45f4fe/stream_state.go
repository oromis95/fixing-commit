// Copyright (c) 2014 Couchbase, Inc.
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
// except in compliance with the License. You may obtain a copy of the License at
//   http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software distributed under the
// License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
// either express or implied. See the License for the specific language governing permissions
// and limitations under the License.

package indexer

import (
	"container/list"
	"github.com/couchbase/indexing/secondary/common"
)

type StreamState struct {
	config common.Config

	streamStatus            map[common.StreamId]StreamStatus
	streamBucketStatus      map[common.StreamId]BucketStatus
	streamBucketVbStatusMap map[common.StreamId]BucketVbStatusMap

	streamBucketHWTMap           map[common.StreamId]BucketHWTMap
	streamBucketSyncCountMap     map[common.StreamId]BucketSyncCountMap
	streamBucketNewTsReqdMap     map[common.StreamId]BucketNewTsReqdMap
	streamBucketTsListMap        map[common.StreamId]BucketTsListMap
	streamBucketLastFlushedTsMap map[common.StreamId]BucketLastFlushedTsMap
	streamBucketRestartTsMap     map[common.StreamId]BucketRestartTsMap

	streamBucketFlushInProgressTsMap map[common.StreamId]BucketFlushInProgressTsMap
	streamBucketAbortInProgressMap   map[common.StreamId]BucketAbortInProgressMap
	streamBucketFlushEnabledMap      map[common.StreamId]BucketFlushEnabledMap
	streamBucketDrainEnabledMap      map[common.StreamId]BucketDrainEnabledMap

	streamBucketIndexCountMap map[common.StreamId]BucketIndexCountMap
	streamBucketRepairStopCh  map[common.StreamId]BucketRepairStopCh
}

type BucketHWTMap map[string]*common.TsVbuuid
type BucketLastFlushedTsMap map[string]*common.TsVbuuid
type BucketRestartTsMap map[string]*common.TsVbuuid
type BucketSyncCountMap map[string]uint64
type BucketNewTsReqdMap map[string]bool

type BucketTsListMap map[string]*list.List
type BucketFlushInProgressTsMap map[string]*common.TsVbuuid
type BucketAbortInProgressMap map[string]bool
type BucketFlushEnabledMap map[string]bool
type BucketDrainEnabledMap map[string]bool

type BucketVbStatusMap map[string]Timestamp
type BucketRepairStopCh map[string]StopChannel

type BucketStatus map[string]StreamStatus

func InitStreamState(config common.Config) *StreamState {

	ss := &StreamState{
		config:                           config,
		streamBucketHWTMap:               make(map[common.StreamId]BucketHWTMap),
		streamBucketSyncCountMap:         make(map[common.StreamId]BucketSyncCountMap),
		streamBucketNewTsReqdMap:         make(map[common.StreamId]BucketNewTsReqdMap),
		streamBucketTsListMap:            make(map[common.StreamId]BucketTsListMap),
		streamBucketFlushInProgressTsMap: make(map[common.StreamId]BucketFlushInProgressTsMap),
		streamBucketAbortInProgressMap:   make(map[common.StreamId]BucketAbortInProgressMap),
		streamBucketLastFlushedTsMap:     make(map[common.StreamId]BucketLastFlushedTsMap),
		streamBucketRestartTsMap:         make(map[common.StreamId]BucketRestartTsMap),
		streamBucketFlushEnabledMap:      make(map[common.StreamId]BucketFlushEnabledMap),
		streamBucketDrainEnabledMap:      make(map[common.StreamId]BucketDrainEnabledMap),
		streamBucketVbStatusMap:          make(map[common.StreamId]BucketVbStatusMap),
		streamStatus:                     make(map[common.StreamId]StreamStatus),
		streamBucketStatus:               make(map[common.StreamId]BucketStatus),
		streamBucketIndexCountMap:        make(map[common.StreamId]BucketIndexCountMap),
	}

	return ss

}

func (ss *StreamState) initNewStream(streamId common.StreamId) {

	//init all internal maps for this stream
	bucketHWTMap := make(BucketHWTMap)
	ss.streamBucketHWTMap[streamId] = bucketHWTMap

	bucketSyncCountMap := make(BucketSyncCountMap)
	ss.streamBucketSyncCountMap[streamId] = bucketSyncCountMap

	bucketNewTsReqdMap := make(BucketNewTsReqdMap)
	ss.streamBucketNewTsReqdMap[streamId] = bucketNewTsReqdMap

	bucketTsListMap := make(BucketTsListMap)
	ss.streamBucketTsListMap[streamId] = bucketTsListMap

	bucketFlushInProgressTsMap := make(BucketFlushInProgressTsMap)
	ss.streamBucketFlushInProgressTsMap[streamId] = bucketFlushInProgressTsMap

	bucketAbortInProgressMap := make(BucketAbortInProgressMap)
	ss.streamBucketAbortInProgressMap[streamId] = bucketAbortInProgressMap

	bucketLastFlushedTsMap := make(BucketLastFlushedTsMap)
	ss.streamBucketLastFlushedTsMap[streamId] = bucketLastFlushedTsMap

	bucketFlushEnabledMap := make(BucketFlushEnabledMap)
	ss.streamBucketFlushEnabledMap[streamId] = bucketFlushEnabledMap

	bucketDrainEnabledMap := make(BucketDrainEnabledMap)
	ss.streamBucketDrainEnabledMap[streamId] = bucketDrainEnabledMap

	bucketVbStatusMap := make(BucketVbStatusMap)
	ss.streamBucketVbStatusMap[streamId] = bucketVbStatusMap

	bucketIndexCountMap := make(BucketIndexCountMap)
	ss.streamBucketIndexCountMap[streamId] = bucketIndexCountMap

	bucketStatus := make(BucketStatus)
	ss.streamBucketStatus[streamId] = bucketStatus

	ss.streamStatus[streamId] = STREAM_ACTIVE

}

func (ss *StreamState) initBucketInStream(streamId common.StreamId,
	bucket string) {

	numVbuckets := ss.config["numVbuckets"].Int()
	ss.streamBucketHWTMap[streamId][bucket] = common.NewTsVbuuid(bucket, numVbuckets)
	ss.streamBucketNewTsReqdMap[streamId][bucket] = false
	ss.streamBucketFlushInProgressTsMap[streamId][bucket] = nil
	ss.streamBucketTsListMap[streamId][bucket] = list.New()
	ss.streamBucketFlushEnabledMap[streamId][bucket] = true
	ss.streamBucketDrainEnabledMap[streamId][bucket] = true
	ss.streamBucketVbStatusMap[streamId][bucket] = NewTimestamp(numVbuckets)

	ss.streamBucketStatus[streamId][bucket] = STREAM_ACTIVE

	common.Debugf("StreamState::initBucketInStream \n\tNew Bucket %v Added for "+
		"Stream %v", bucket, streamId)
}

func (ss *StreamState) cleanupBucketFromStream(streamId common.StreamId,
	bucket string) {

	delete(ss.streamBucketHWTMap[streamId], bucket)
	delete(ss.streamBucketSyncCountMap[streamId], bucket)
	delete(ss.streamBucketNewTsReqdMap[streamId], bucket)
	delete(ss.streamBucketTsListMap[streamId], bucket)
	delete(ss.streamBucketFlushInProgressTsMap[streamId], bucket)
	delete(ss.streamBucketAbortInProgressMap[streamId], bucket)
	delete(ss.streamBucketLastFlushedTsMap[streamId], bucket)
	delete(ss.streamBucketFlushEnabledMap[streamId], bucket)
	delete(ss.streamBucketDrainEnabledMap[streamId], bucket)
	delete(ss.streamBucketVbStatusMap[streamId], bucket)
	delete(ss.streamBucketIndexCountMap[streamId], bucket)

	ss.streamBucketStatus[streamId][bucket] = STREAM_INACTIVE

	common.Debugf("StreamState::cleanupBucketFromStream \n\tBucket %v Deleted from "+
		"Stream %v", bucket, streamId)

}

func (ss *StreamState) resetStreamState(streamId common.StreamId) {

	//delete this stream from internal maps
	delete(ss.streamBucketHWTMap, streamId)
	delete(ss.streamBucketSyncCountMap, streamId)
	delete(ss.streamBucketNewTsReqdMap, streamId)
	delete(ss.streamBucketTsListMap, streamId)
	delete(ss.streamBucketFlushInProgressTsMap, streamId)
	delete(ss.streamBucketAbortInProgressMap, streamId)
	delete(ss.streamBucketLastFlushedTsMap, streamId)
	delete(ss.streamBucketFlushEnabledMap, streamId)
	delete(ss.streamBucketDrainEnabledMap, streamId)
	delete(ss.streamBucketVbStatusMap, streamId)
	delete(ss.streamBucketIndexCountMap, streamId)
	delete(ss.streamBucketStatus, streamId)

	ss.streamStatus[streamId] = STREAM_INACTIVE

	common.Debugf("StreamState::resetStreamState \n\tReset Stream %v State", streamId)
}

func (ss *StreamState) updateVbStatus(streamId common.StreamId, bucket string,
	vbList []Vbucket, status VbStatus) {

	for _, vb := range vbList {
		vbs := ss.streamBucketVbStatusMap[streamId][bucket]
		vbs[vb] = status
	}

}

//computes the restart Ts for all the buckets in a given stream
func (ss *StreamState) computeRestartTs(streamId common.StreamId,
	bucket string) *common.TsVbuuid {

	var restartTs *common.TsVbuuid

	if _, ok := ss.streamBucketLastFlushedTsMap[streamId][bucket]; ok {
		restartTs = ss.streamBucketLastFlushedTsMap[streamId][bucket].Copy()
	} else if ts, ok := ss.streamBucketRestartTsMap[streamId][bucket]; ok && ts != nil {
		//if no flush has been done yet, use restart TS
		restartTs = ss.streamBucketRestartTsMap[streamId][bucket].Copy()
	}
	return restartTs
}

func (ss *StreamState) setHWTFromRestartTs(streamId common.StreamId,
	bucket string) {

	common.Debugf("StreamState::setHWTFromRestartTs Stream %v "+
		"Bucket %v", streamId, bucket)

	if bucketRestartTs, ok := ss.streamBucketRestartTsMap[streamId]; ok {

		if restartTs, ok := bucketRestartTs[bucket]; ok {

			//update HWT
			ss.streamBucketHWTMap[streamId][bucket] = restartTs.Copy()

			//update Last Flushed Ts
			ss.streamBucketLastFlushedTsMap[streamId][bucket] = restartTs.Copy()
			common.Debugf("StreamState::setHWTFromRestartTs \n\tHWT Set For "+
				"Bucket %v StreamId %v. TS %v.", bucket, streamId, restartTs)

		} else {
			common.Warnf("StreamState::setHWTFromRestartTs \n\tRestartTs Not Found For "+
				"Bucket %v StreamId %v. No Value Set.", bucket, streamId)
		}
	}
}

func (ss *StreamState) getRepairTsForBucket(streamId common.StreamId,
	bucket string) (*common.TsVbuuid, bool) {

	anythingToRepair := false

	numVbuckets := ss.config["numVbuckets"].Int()
	repairTs := common.NewTsVbuuid(bucket, numVbuckets)

	hwtTs := ss.streamBucketHWTMap[streamId][bucket]
	for i, s := range ss.streamBucketVbStatusMap[streamId][bucket] {
		if s == VBS_STREAM_END || s == VBS_CONN_ERROR {
			repairTs.Seqno[i] = hwtTs.Seqno[i]
			repairTs.Vbuuids[i] = hwtTs.Vbuuids[i]
			repairTs.Snapshots[i][0] = hwtTs.Snapshots[i][0]
			repairTs.Snapshots[i][1] = hwtTs.Snapshots[i][1]
			//change the status of this vbucket to REPAIR
			ss.streamBucketVbStatusMap[streamId][bucket][i] = VBS_REPAIR
			anythingToRepair = true
		}
	}

	return repairTs, anythingToRepair

}

func (ss *StreamState) checkAllStreamBeginsReceived(streamId common.StreamId,
	bucket string) bool {

	if bucketVbStatusMap, ok := ss.streamBucketVbStatusMap[streamId]; ok {
		if vbs, ok := bucketVbStatusMap[bucket]; ok {
			for _, v := range vbs {
				if v != VBS_STREAM_BEGIN {
					return false
				}
			}
		} else {
			return false
		}
	} else {
		return false
	}
	return true
}

//checks if the given bucket in this stream has any flush in progress
func (ss *StreamState) checkAnyFlushPending(streamId common.StreamId,
	bucket string) bool {

	bucketFlushInProgressTsMap := ss.streamBucketFlushInProgressTsMap[streamId]

	//check if there is any flush in progress. No flush in progress implies
	//there is no flush pending as well bcoz every flush done triggers
	//the next flush.
	if ts := bucketFlushInProgressTsMap[bucket]; ts != nil {
		return true
	}
	return false
}

//checks if none of the buckets in this stream has any flush abort in progress
func (ss *StreamState) checkAnyAbortPending(streamId common.StreamId,
	bucket string) bool {

	bucketAbortInProgressMap := ss.streamBucketAbortInProgressMap[streamId]

	if running := bucketAbortInProgressMap[bucket]; running {
		return true
	}
	return false
}

func (ss *StreamState) incrSyncCount(streamId common.StreamId,
	bucket string) {

	bucketSyncCountMap := ss.streamBucketSyncCountMap[streamId]

	//update sync count for this bucket
	if syncCount, ok := bucketSyncCountMap[bucket]; ok {
		syncCount++

		common.Tracef("StreamState::incrSyncCount \n\tUpdating Sync Count for Bucket: %v "+
			"Stream: %v. SyncCount: %v.", bucket, streamId, syncCount)
		//update only if its less than trigger count, otherwise it makes no
		//difference. On long running systems, syncCount may overflow otherwise
		numVbuckets := ss.config["numVbuckets"].Int()
		if syncCount <= uint64(SYNC_COUNT_TS_TRIGGER)*uint64(numVbuckets) {
			bucketSyncCountMap[bucket] = syncCount
		}

	} else {
		//add a new counter for this bucket
		common.Debugf("StreamState::incrSyncCount \n\tAdding new Sync Count for Bucket: %v "+
			"Stream: %v. SyncCount: %v.", bucket, streamId, syncCount)
		bucketSyncCountMap[bucket] = 1
	}

}

//updateHWT will update the HW Timestamp for a bucket in the stream
//based on the Sync message received.
func (ss *StreamState) updateHWT(streamId common.StreamId,
	meta *MutationMeta) {

	//if seqno has incremented, update it
	ts := ss.streamBucketHWTMap[streamId][meta.bucket]
	if uint64(meta.seqno) > ts.Seqnos[meta.vbucket] {
		ts.Seqnos[meta.vbucket] = uint64(meta.seqno)
		ts.Vbuuids[meta.vbucket] = uint64(meta.vbuuid)
		common.Tracef("StreamState::updateHWT \n\tHWT Updated : %v", ts)
	}

}
