package common

import "github.com/couchbase/indexing/secondary/dcp"
import "errors"
import "fmt"
import "net"

var (
	ErrInvalidNodeId       = errors.New("Invalid NodeId")
	ErrInvalidService      = errors.New("Invalid service")
	ErrNodeNotBucketMember = errors.New("Node is not a member of bucket")
)

// Helper object for fetching cluster information
// Info cache can be updated by using Refresh() method.
type ClusterInfoCache struct {
	url      string
	poolName string

	client  couchbase.Client
	pool    couchbase.Pool
	nodesvs []couchbase.NodeServices
}

type NodeId int

func NewClusterInfoCache(cluster string, pool string) (*ClusterInfoCache, error) {
	var err error

	c := &ClusterInfoCache{
		url:      cluster,
		poolName: pool,
	}
	c.client, err = couchbase.Connect(c.url)
	if err != nil {
		return c, err
	}

	return c, c.Refresh()
}

func (c *ClusterInfoCache) Refresh() error {
	var err error
	var poolServs couchbase.PoolServices
	c.pool, err = c.client.GetPool(c.poolName)
	if err != nil {
		return err
	}

	poolServs, err = c.client.GetPoolServices(c.poolName)
	if err != nil {
		return err
	}

	c.nodesvs = poolServs.NodesExt

	return nil
}

func (c ClusterInfoCache) GetNodesByServiceType(srvc string) (nids []NodeId) {
	for i, _ := range c.pool.Nodes {
		if _, ok := c.nodesvs[i].Services[srvc]; ok {
			nids = append(nids, NodeId(i))
		}
	}

	return
}

func (c ClusterInfoCache) GetNodesByBucket(bucket string) (nids []NodeId, err error) {
	b, berr := c.pool.GetBucket(bucket)
	if berr != nil {
		err = berr
		return
	}
	defer b.Close()

	for i, _ := range c.pool.Nodes {
		nid := NodeId(i)
		if _, ok := c.findVBServerIndex(b, nid); ok {
			nids = append(nids, nid)
		}
	}

	return
}

func (c ClusterInfoCache) GetCurrentNode() NodeId {
	for i, node := range c.pool.Nodes {
		if node.ThisNode {
			return NodeId(i)
		}
	}

	panic("Invalid cluster info")
}

func (c ClusterInfoCache) GetServiceAddress(nid NodeId, srvc string) (addr string, err error) {
	var port int
	var ok bool

	if int(nid) >= len(c.nodesvs) {
		err = ErrInvalidNodeId
		return
	}

	node := c.nodesvs[nid]
	if port, ok = node.Services[srvc]; !ok {
		err = ErrInvalidService
		return
	}

	addr = net.JoinHostPort(node.Hostname, fmt.Sprint(port))
	return
}

func (c ClusterInfoCache) GetVBuckets(nid NodeId, bucket string) (vbs []uint16, err error) {
	b, berr := c.pool.GetBucket(bucket)
	if berr != nil {
		err = berr
		return
	}
	defer b.Close()

	idx, ok := c.findVBServerIndex(b, nid)
	if !ok {
		err = ErrNodeNotBucketMember
		return
	}

	vbmap := b.VBServerMap()

	for vb, idxs := range vbmap.VBucketMap {
		if idxs[0] == idx {
			vbs = append(vbs, uint16(vb))
		}
	}

	return
}

func (c ClusterInfoCache) findVBServerIndex(b *couchbase.Bucket, nid NodeId) (int, bool) {
	bnodes := b.Nodes()

	for idx, n := range bnodes {
		if c.sameNode(n, c.pool.Nodes[nid]) {
			return idx, true
		}
	}

	return 0, false
}

func (c ClusterInfoCache) sameNode(n1 couchbase.Node, n2 couchbase.Node) bool {
	if n1.Hostname == n2.Hostname {
		return true
	}

	return false
}
