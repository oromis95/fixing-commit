package protobuf;

import "common.proto";
import "index.proto";

// Request can be one of the optional field.
message QueryPayload {
    required uint32             version           = 1;
    optional StatisticsRequest  statisticsRequest = 2;
    optional ScanRequest        scanRequest       = 3;
    optional ScanAllRequest     scanAllRequest    = 4;
    optional EndStreamRequest   endStream         = 5;
    optional StatisticsResponse statistics        = 6;
    optional ResponseStream     stream            = 7;
    optional StreamEndResponse  streamEnd         = 8;
}

// Get Index statistics. StatisticsResponse is returned back from indexer.
message StatisticsRequest {
    required Span   span     = 1;
}

message StatisticsResponse {
    required IndexStatistics stats = 1;
    optional Error           err   = 2;
}

// Scan request to indexer.
message ScanRequest {
    required Span   span     = 1;
    required bool   distinct = 2;
    required int64  pageSize = 4;
    required int64  limit    = 3;
}

// Full table scan request from indexer.
message ScanAllRequest {
    required int64 pageSize = 1;
    required int64 limit    = 2;
}

// Request by client to stop streaming the query results.
message EndStreamRequest {
}

message ResponseStream {
    repeated IndexEntry entries = 1;
    optional Error      err     = 2;
}

// Last response packet sent by server to end query results.
message StreamEndResponse {
    optional Error err = 1;
}

// Query messages / arguments for indexer

message Span {
    required Range  range = 1;
    repeated string equal = 2;
}

message Range {
    required bytes  low       = 1;
    required bytes  high      = 2;
    required uint32 inclusion = 3;
}

message IndexEntry {
    required bytes  entryKey   = 1;
    required bytes  primaryKey = 2;
}
