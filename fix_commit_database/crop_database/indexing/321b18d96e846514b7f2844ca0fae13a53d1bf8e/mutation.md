## Mutation Execution Flow

This document describes how KV mutations are received and processed by the
Secondary Indexes.

### Insert/Update Mutation

![InsertWorkFlow][]

Insert/Update Mutation Workflow can be divided into 3 phases:

#### Key Distribution Phase

1. During this phase, [Projector][] receives mutations from [UPR][1],
2. Apply mapping functions and extracts secondary keys.
3. If older version of the document is present, projector will apply the mapping
   functions on the older document and generate the older-keys.
4. Finally projector will send `Upsert` to [Router][].
   A single `Upsert` message can contain secondary-key, aka key-versions,
   for more than one index.
5. Router will identify the indexer endpoints per index, based on partition
   algorithm, for which a key-version is present in the incoming `Upsert`
   message.
6. If older keys are not available, `UpsertDeletion` message will be
   generated, to delete the old entry, by the router and published to all indexer
   nodes hosting the index.

An important thing to note here is that UPR INSERT/UPDATE mutation can causes 2
messages to be published or broadcasted to the indexers.
* `Upsert` to add the new key-version, this will be published only to the
  indexer node hosting the new secondary-key.
* `UpsertDeletion` to delete the older key-version,
  * will be broadcasted, if older version of document is not available from UPR.
  * published only to indexer node hosting the older secondary-key, if older
    version of document is available from UPR.
* as an optimiziation to previous step, `UpsertDeletion` message won't be sent
  to the indexer node that received `Upsert` message.

#### Checkpoint Accumulation Phase

7. During this phase, all local Indexers accept messages from Router and store it
   in [Mutation Queue][1].
8. The [High-Watermark Timestamp][1] of local node is updated to reflect the
   current state of Mutation Queue.

#### Stable Persistence Phase

9. [Index Coordinator][] (based on the `Deletion` / SYNC messages received and
   internal algorithm) decides to promote [Stability Timestamp][1]
10. This decision along with the timestamp is broadcasted to all Indexers.

11. This triggers the processing of Mutation Queue at Indexer.
12. Message are applied/skipped as required.
13. Messages from Mutation Queue are processed in order and applied as batch to
    the persistent storage. Indexer will process the messages for each vbucket
    only till the Max Sequence Number in Stability Timestamp.

Stability timestamps are necessary to prevent problems such as [Tearing Reads][1]
while doing a distributed range scan.

### Delete Mutation

![DeleteWorkFlow][]

Delete Mutations follow similar workflow as Insert/Update Mutation:

* Key Distribution Phase
* Checkpoint Accumulation Phase
* Stable Persistence Phase

A notable point here is that UPR Delete Mutations from KV will result in
a broadcast message to be sent to all Indexers. This is due to the fact that
there is no way to determine the secondary key's location based on KV's DocId
(which is the only information available in delete).

*For more details, see [John's Execution Flow Document]*

### Control messages

Three types of control messages, differentiated by command-field, will be
communicated to indexers and coordinator along the connection stream.
* StreamBegin, that indicates that a new UPR-stream is started for specified
  vbucket.
* StreamEnd, that indicates that an existing UPR-stream is closed for specified
  vbucket.
* Sync, that broadcast the latest `{seqno, vbuuid}` for a live vbucket.
  UPR_SNAPSHOT_MARKER, `UpsertDeletion`, `Deletion`, periodic timeouts,
  mutation-count, can be used as triggers for SYNC message.

[InsertWorkflow]: ../images/InsertWorkflow.svg
[DeleteWorkflow]: ../images/DeleteWorkflow.svg
[Projector]: projector.md
[Router]: router.md
[Indexers]: indexer.md
[Indexer Coordinator]: indexer.md
[1]: terminology.md
[John's Execution Flow Document]: https://docs.google.com/document/d/11IojzquMYrOO0NNu7P52oQb6lai0ooXrugcEMEyXsUc/edit#
