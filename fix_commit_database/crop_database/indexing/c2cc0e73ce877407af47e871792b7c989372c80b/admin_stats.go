package adminport

import (
	c "github.com/couchbase/indexing/secondary/common"
)

func (s *httpServer) newStats() *c.ComponentStat {
	statsp, _ := c.NewComponentStat(nil)
	stats := *statsp
	stats["componentName"] = s.component + ".adminport"
	stats["urlPrefix"] = s.urlPrefix   // HTTP mount point
	stats["payload"] = []float64{0, 0} // [request,response] Bytes
	for _, msg := range s.messages {   // [req,resp,err] count
		stats["message."+msg.Name()] = []float64{0, 0, 0}
	}
	stats["message."+stats.Name()] = []float64{0, 0, 0} // This is vicious !!!
	stats["messages"] = float64(len(s.messages) + 1)    // registered messages
	return statsp
}
