package org.eclipse.egit.ui.internal.repository;

import org.eclipse.osgi.util.NLS;
/**
 * UI Texts for the Repositories View
 *
 */
public class RepositoryViewUITexts extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.egit.ui.internal.repository.repositoryviewuitexts"; //$NON-NLS-1$

	/** */
	public static String RepositoriesView_ActionCanceled_Message;

	/** */
	public static String RepositoriesView_Add_Button;

	/** */
	public static String RepositoriesView_AddRepository_MenuItem;

	/** */
	public static String RepositoriesView_AddRepository_Tooltip;

	/** */
	public static String RepositoriesView_Branches_Nodetext;

	/** */
	public static String RepositoriesView_Checking_Message;

	/** */
	public static String RepositoriesView_CheckOut_MenuItem;

	/** */
	public static String RepositoriesView_Clone_Tooltip;

	/** */
	public static String RepositoriesView_ConfirmProjectDeletion_Question;

	/** */
	public static String RepositoriesView_ConfirmProjectDeletion_WindowTitle;

	/** */
	public static String RepositoriesView_Error_WindowTitle;

	/** */
	public static String RepositoriesView_ExistingProjects_Nodetext;

	/** */
	public static String RepositoriesView_Import_Button;

	/** */
	public static String RepositoriesView_ImportExistingProjects_MenuItem;

	/** */
	public static String RepositoriesView_ImportProject_MenuItem;

	/** */
	public static String RepositoriesView_ImportRepository_MenuItem;

	/** */
	public static String RepositoriesView_Refresh_Button;

	/** */
	public static String RepositoriesView_Remove_MenuItem;

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, RepositoryViewUITexts.class);
	}

	private RepositoryViewUITexts() {
	}
}
