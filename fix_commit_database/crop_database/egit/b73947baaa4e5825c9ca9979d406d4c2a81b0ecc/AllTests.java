package org.eclipse.egit.ui.test;

import org.eclipse.egit.ui.wizards.clone.GitCloneWizardTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ GitCloneWizardTest.class })
public class AllTests {
	// empty class, don't need anything here
}
