/*******************************************************************************
 * Copyright (C) 2007, Dave Watson <dwatson@mimvista.com>
 * Copyright (C) 2008, Robin Rosenberg <robin.rosenberg@dewire.com>
 * Copyright (C) 2006, Shawn O. Pearce <spearce@spearce.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.egit.ui.internal.actions;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.egit.core.op.ResetOperation;
import org.eclipse.egit.core.op.ResetOperation.ResetType;
import org.eclipse.egit.ui.UIText;
import org.eclipse.egit.ui.internal.decorators.GitLightweightDecorator;
import org.eclipse.egit.ui.internal.dialogs.BranchSelectionDialog;
import org.eclipse.egit.ui.internal.trace.GitTraceLocation;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.osgi.util.NLS;

/**
 * An action to reset the current branch to a specific revision.
 *
 * @see ResetOperation
 */
public class ResetAction extends RepositoryAction {

	@Override
	public void execute(IAction action) {
		final Repository repository = getRepository(true);
		if (repository == null)
			return;

		if (!repository.getRepositoryState().canResetHead()) {
			MessageDialog.openError(getShell(), UIText.ResetAction_errorResettingHead,
					NLS.bind(UIText.ResetAction_repositoryState, repository.getRepositoryState().getDescription()));
			return;
		}

		BranchSelectionDialog branchSelectionDialog = new BranchSelectionDialog(getShell(), repository);
		if (branchSelectionDialog.open() == IDialogConstants.OK_ID) {
			final String refName = branchSelectionDialog.getRefName();
			final ResetType type = branchSelectionDialog.getResetType();

			try {
				getTargetPart().getSite().getWorkbenchWindow().run(true, false,
						new IRunnableWithProgress() {
					public void run(final IProgressMonitor monitor)
					throws InvocationTargetException {
						try {
							new ResetOperation(repository, refName, type).run(monitor);
							GitLightweightDecorator.refresh();
						} catch (CoreException e) {
							if (GitTraceLocation.UI.isActive())
								GitTraceLocation.getTrace().trace(GitTraceLocation.UI.getLocation(), e.getMessage(), e);
							throw new InvocationTargetException(e);
						}
					}
				});
			} catch (InvocationTargetException e) {
				MessageDialog.openError(getShell(),UIText.ResetAction_resetFailed, e.getMessage());
			} catch (InterruptedException e) {
				MessageDialog.openError(getShell(),UIText.ResetAction_resetFailed, e.getMessage());
			}
		}

	}

	@Override
	public boolean isEnabled() {
		return getRepository(false) != null;
	}
}
