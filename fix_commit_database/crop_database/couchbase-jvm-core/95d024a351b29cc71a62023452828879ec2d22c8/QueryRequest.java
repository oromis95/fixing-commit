package com.couchbase.client.core.message.query;

import com.couchbase.client.core.message.CouchbaseRequest;

public interface QueryRequest extends CouchbaseRequest {
}
