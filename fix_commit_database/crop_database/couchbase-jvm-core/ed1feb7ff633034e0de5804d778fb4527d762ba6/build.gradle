/**
 * Copyright (C) 2014 Couchbase, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALING
 * IN THE SOFTWARE.
 */

apply plugin: 'java'
apply plugin: 'eclipse'
apply plugin: 'idea'
apply plugin: 'maven'

group = 'com.couchbase.client'
description = 'Official Couchbase JVM Core Library'

sourceCompatibility = 1.6
targetCompatibility = 1.6

buildscript {
    repositories {
        maven {
            name 'Shadow'
            url 'http://dl.bintray.com/content/gvsmirnov/gradle-plugins'
        }
        jcenter()
    }
    dependencies {
        classpath 'com.github.jengelman.gradle.plugins:shadow:0.8.1'
    }
}

repositories {
    //mavenLocal()
    mavenCentral()
    jcenter()
    //maven { url 'https://oss.sonatype.org/content/repositories/snapshots' }
}

sourceSets {
    perf
    integration
}

configurations {
    perfCompile.extendsFrom compile, testCompile
    perfRuntime.extendsFrom runtime, testRuntime

    integrationCompile.extendsFrom compile, testCompile
    integrationRuntime.extendsFrom runtime, testRuntime

    markdownDoclet
}

dependencies {
    compile group: 'io.netty', name: 'netty-all', version: '4.0.19.Final'
    compile group: 'com.netflix.rxjava', name:'rxjava-core', version: '0.18.4'
    compile group: 'com.lmax', name: 'disruptor', version: '3.2.1'
    compile group: 'com.typesafe', name: 'config', version: '1.2.0'
    compile group: 'org.slf4j', name: 'slf4j-api', version: '1.7.7'
    compile group: 'com.fasterxml.jackson.core', name: 'jackson-databind', version: '2.3.3'

    testCompile group: 'junit', name: 'junit', version: '4.11'
    testCompile group: 'ch.qos.logback', name: 'logback-classic', version: '1.0.13'
    testCompile group: 'org.mockito', name: 'mockito-all', version: '1.9.5'

    integrationCompile sourceSets.main.output

    perfCompile group: 'org.openjdk.jmh', name: 'jmh-core', version: '0.5.6'
    perfCompile group: 'org.openjdk.jmh', name: 'jmh-generator-annprocess', version: '0.5.6'
    perfCompile project

    markdownDoclet 'ch.raffael.pegdown-doclet:pegdown-doclet:1.1.1'
}

idea {
    module {
        scopes.PROVIDED.plus  += configurations.perfCompile
        scopes.PROVIDED.minus += configurations.compile
    }
}

eclipse {
    classpath {
        plusConfigurations += configurations.perfCompile
    }
}

javadoc.options {
    docletpath = configurations.markdownDoclet.files.asType(List)
    doclet = "ch.raffael.doclets.pegdown.PegdownDoclet"
    addStringOption("parse-timeout", "10")
}

task integrationTest(type: Test) {
    testClassesDir = sourceSets.integration.output.classesDir
    classpath = sourceSets.integration.runtimeClasspath
}

task perfJar(type: Jar, dependsOn: perfClasses) {
    from sourceSets.perf.output + sourceSets.main.output
}

task benchmarks(dependsOn: perfJar) {
    apply plugin: 'shadow'
    shadow {
        classifier = "benchmarks"
        includeDependenciesFor = ["runtime", "perfRuntime"]

        transformer(com.github.jengelman.gradle.plugins.shadow.transformers.ManifestResourceTransformer) {
            mainClass = "org.openjdk.jmh.Main"
        }
    }
    doLast {
        shadowJar.execute()
    }
}

task wrapper(type: Wrapper) {
    gradleVersion = '1.11'
}

task sourcesJar(type: Jar) {
    classifier = 'sources'
    from sourceSets.main.allSource
}

task javadocJar(type: Jar) {
    classifier = 'javadoc'
    from javadoc
}

artifacts {
    archives sourcesJar
    archives javadocJar
}

install {
    repositories.mavenInstaller {
        customizePom(pom)
    }
}

def customizePom(def pom) {
    pom.whenConfigured { generatedPom ->
        // eliminate test-scoped dependencies (no need in maven central poms)
        generatedPom.dependencies.removeAll { dep ->
            dep.scope == "test"
        }

        // sort to make pom dependencies order consistent to ease comparison of older poms
        generatedPom.dependencies = generatedPom.dependencies.sort { dep ->
            "$dep.scope:$dep.groupId:$dep.artifactId"
        }

        // add all items necessary for maven central publication
        generatedPom.project {
            name = rootProject.name
            description = rootProject.description
            url = 'http://couchbase.com'
            organization {
                name = 'Couchbase, Inc.'
                url = 'http://couchbase.com'
            }
            licenses {
                license {
                    name 'The Apache Software License, Version 2.0'
                    url 'http://www.apache.org/licenses/LICENSE-2.0.txt'
                    distribution 'repo'
                }
            }
            scm {
                url = 'https://github.com/couchbase/couchbase-jvm-core'
                connection = 'scm:git:git://github.com/couchbase/couchbase-jvm-core'
                developerConnection = 'scm:git:git://github.com/couchbase/couchbase-jvm-core'
            }
            developers {
                developer {
                    id = 'daschl'
                    name = 'Michael Nitschinger'
                    email = 'michael.nitschinger@couchbase.com'
                }
            }
            issueManagement {
                system = "Couchbase JIRA"
                url = "http://www.couchbase.com/issues/browse/JCBC"
            }
        }
    }
}