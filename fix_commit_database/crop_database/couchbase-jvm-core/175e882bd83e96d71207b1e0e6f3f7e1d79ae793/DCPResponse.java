package com.couchbase.client.core.message.dcp;

import com.couchbase.client.core.message.CouchbaseResponse;

/**
 * @author Sergey Avseyev
 * @since 1.0.2
 */
public interface DCPResponse extends CouchbaseResponse {
}
