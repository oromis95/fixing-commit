@@ -43,6 +43,14 @@ public abstract class KcpClient implements Output, KcpListerner, Runnable
   private NioEventLoopGroup nioEventLoopGroup;
   private ByteOrder order;
 
+  /**
+   * client
+   */
+  public KcpClient()
+  {
+    this(0);
+  }
+
   /**
    * 客户端
    *
