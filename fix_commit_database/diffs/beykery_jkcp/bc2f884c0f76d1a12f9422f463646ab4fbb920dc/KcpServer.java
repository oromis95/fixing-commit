@@ -108,6 +108,7 @@ public abstract class KcpServer implements Output, KcpListerner
   {
     if (this.running)
     {
+      this.running = false;
       for (KcpThread kt : this.workers)
       {
         kt.close();
@@ -206,11 +207,7 @@ public abstract class KcpServer implements Output, KcpListerner
   {
     ku.send(bb);
   }
-  public void send(ByteBuf bb, InetSocketAddress addr)
-  {
-     DatagramPacket temp = new DatagramPacket(bb,  addr, this.addr);
-    this.channel.writeAndFlush(temp);
-  }
+
   /**
    * receive DatagramPacket
    *
@@ -226,7 +223,7 @@ public abstract class KcpServer implements Output, KcpListerner
   /**
    * handler
    */
-  class UdpHandler extends ChannelInboundHandlerAdapter
+  public class UdpHandler extends ChannelInboundHandlerAdapter
   {
 
     @Override
