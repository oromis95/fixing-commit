@@ -1,5 +1,5 @@
 /**
- * 测试
+ * 客户端
  */
 package test;
 
@@ -7,26 +7,26 @@ import io.netty.buffer.ByteBuf;
 import io.netty.buffer.PooledByteBufAllocator;
 import java.net.InetSocketAddress;
 import java.nio.charset.Charset;
+import org.beykery.jkcp.KcpClient;
 import org.beykery.jkcp.KcpOnUdp;
-import org.beykery.jkcp.KcpServer;
 
 /**
  *
  * @author beykery
  */
-public class TestClient extends KcpServer
+public class TestClient extends KcpClient
 {
 
-  public TestClient(int port, int workerSize)
+  public TestClient(int port)
   {
-    super(port, workerSize);
+    super(port);
   }
 
   @Override
   public void handleReceive(ByteBuf bb, KcpOnUdp kcp)
   {
     String content = bb.toString(Charset.forName("utf-8"));
-    System.out.println("msg:" + content);
+    System.out.println("msg:" + content + " from " + kcp);
     bb.release();
   }
 
@@ -39,26 +39,20 @@ public class TestClient extends KcpServer
   @Override
   public void handleClose(KcpOnUdp kcp)
   {
-    System.out.println("客户端离开:" + kcp);
+    System.out.println("服务器离开:" + kcp);
+    this.close();
   }
 
-  /**
-   * 测试
-   *
-   * @param args
-   */
   public static void main(String[] args)
   {
-    TestClient s = new TestClient(2223, 1);
-    s.noDelay(1, 10, 2, 1);
-    s.wndSize(64, 64);
-    s.setTimeout(10 * 1000);
-    InetSocketAddress addr = new InetSocketAddress("localhost",2222);
-    s.connect(addr);
-    s.start();
-    
-    ByteBuf bb = PooledByteBufAllocator.DEFAULT.buffer(100);
-    bb.writeBytes("aabc".getBytes());
-    s.send(bb, addr);
+    TestClient tc = new TestClient(2223);
+    tc.noDelay(1, 10, 2, 1);
+    tc.wndSize(64, 64);
+    tc.setTimeout(10 * 1000);
+    tc.connect(new InetSocketAddress("localhost", 2222));
+    tc.start();
+    ByteBuf bb = PooledByteBufAllocator.DEFAULT.buffer(255);
+    bb.writeBytes("abcd".getBytes());
+    tc.send(bb);
   }
 }
