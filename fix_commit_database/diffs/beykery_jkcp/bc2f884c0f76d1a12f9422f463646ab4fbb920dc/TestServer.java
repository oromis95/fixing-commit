@@ -24,8 +24,8 @@ public class TestServer extends KcpServer
   public void handleReceive(ByteBuf bb, KcpOnUdp kcp)
   {
     String content = bb.toString(Charset.forName("utf-8"));
-    System.out.println("msg:" + content);
-    bb.release();
+    System.out.println("msg:" + content+" from "+kcp);
+    kcp.send(bb);
   }
 
   @Override
