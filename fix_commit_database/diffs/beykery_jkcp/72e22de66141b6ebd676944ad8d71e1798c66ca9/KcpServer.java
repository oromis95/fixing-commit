@@ -34,6 +34,8 @@ public abstract class KcpServer implements Output, KcpListerner
   private int sndwnd = Kcp.IKCP_WND_SND;
   private int rcvwnd = Kcp.IKCP_WND_RCV;
   private int mtu = Kcp.IKCP_MTU_DEF;
+  private boolean stream;
+  private int minRto = Kcp.IKCP_RTO_MIN;
   private KcpThread[] workers;
   private volatile boolean running;
   private long timeout;
@@ -94,6 +96,8 @@ public abstract class KcpServer implements Output, KcpListerner
         workers[i].noDelay(nodelay, interval, resend, nc);
         workers[i].setMtu(mtu);
         workers[i].setTimeout(timeout);
+        workers[i].setMinRto(minRto);
+        workers[i].setStream(stream);
         workers[i].start();
       }
     }
@@ -187,6 +191,27 @@ public abstract class KcpServer implements Output, KcpListerner
     this.mtu = mtu;
   }
 
+  /**
+   * stream mode
+   *
+   *
+   * @param stream
+   */
+  public void setStream(boolean stream)
+  {
+    this.stream = stream;
+  }
+
+  public boolean isStream()
+  {
+    return stream;
+  }
+
+  public void setMinRto(int minRto)
+  {
+    this.minRto = minRto;
+  }
+
   public void setTimeout(long timeout)
   {
     this.timeout = timeout;
