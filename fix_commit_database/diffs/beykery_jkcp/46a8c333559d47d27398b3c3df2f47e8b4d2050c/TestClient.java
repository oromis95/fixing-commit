@@ -53,9 +53,9 @@ public class TestClient extends KcpClient
   }
 
   @Override
-  public void handleException(Throwable ex)
+  public void handleException(Throwable ex, KcpOnUdp kcp)
   {
-    System.out.println(ex.fillInStackTrace());
+    System.out.println(ex);
   }
 
   @Override
