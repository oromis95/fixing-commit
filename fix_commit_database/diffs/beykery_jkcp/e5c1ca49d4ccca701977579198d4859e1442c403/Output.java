@@ -9,8 +9,15 @@ import io.netty.buffer.ByteBuf;
  *
  * @author beykery
  */
-interface Output
+public interface Output
 {
 
+  /**
+   * kcp的底层输出
+   *
+   * @param msg
+   * @param kcp
+   * @param user
+   */
   void out(ByteBuf msg, Kcp kcp, Object user);
 }
