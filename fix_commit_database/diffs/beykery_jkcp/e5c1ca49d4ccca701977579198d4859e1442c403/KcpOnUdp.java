@@ -3,24 +3,11 @@
  */
 package org.beykery.jkcp;
 
-import io.netty.bootstrap.Bootstrap;
 import io.netty.buffer.ByteBuf;
 import io.netty.buffer.PooledByteBufAllocator;
-import io.netty.channel.ChannelFuture;
-import io.netty.channel.ChannelHandlerContext;
-import io.netty.channel.ChannelInboundHandlerAdapter;
-import io.netty.channel.ChannelInitializer;
-import io.netty.channel.ChannelPipeline;
-import io.netty.channel.nio.NioEventLoopGroup;
-import io.netty.channel.socket.DatagramPacket;
-import io.netty.channel.socket.nio.NioDatagramChannel;
-import java.net.InetSocketAddress;
-import java.util.HashMap;
 import java.util.LinkedList;
-import java.util.Map;
 import java.util.Queue;
-import java.util.concurrent.locks.Lock;
-import java.util.concurrent.locks.ReentrantLock;
+import java.util.concurrent.LinkedBlockingQueue;
 import org.slf4j.Logger;
 import org.slf4j.LoggerFactory;
 
@@ -28,142 +15,80 @@ import org.slf4j.LoggerFactory;
  *
  * @author beykery
  */
-public abstract class KcpOnUdp implements Output
+public class KcpOnUdp
 {
 
   private static final Logger LOG = LoggerFactory.getLogger(KcpOnUdp.class);
-
-  private final NioDatagramChannel channel;
-  private final InetSocketAddress addr;
-  private final Map<InetSocketAddress, Kcp> kcps;
-  private final Map<InetSocketAddress, Queue<DatagramPacket>> received;
-  private final Lock dataLock = new ReentrantLock();
-  private final Lock kcpLock = new ReentrantLock();
-  private int nodelay;
-  private int interval = Kcp.IKCP_INTERVAL;
-  private int resend;
-  private int nc;
-  private int sndwnd = Kcp.IKCP_WND_SND;
-  private int rcvwnd = Kcp.IKCP_WND_RCV;
-  private int mtu = Kcp.IKCP_MTU_DEF;
+  private final Kcp kcp;//kcp的状态
+  private final Queue<ByteBuf> received;//输入
+  private final Queue<ByteBuf> sendList;
+  private long timeout;//超时设定
+  private long lastTime;//上次超时检查时间
+  private final KcpListerner listerner;
+  private volatile boolean needUpdate;
+  private volatile boolean closed;
 
   /**
-   * kcp for udp
+   * fastest: ikcp_nodelay(kcp, 1, 20, 2, 1) nodelay: 0:disable(default),
+   * 1:enable interval: internal update timer interval in millisec, default is
+   * 100ms resend: 0:disable fast resend(default), 1:enable fast resend nc:
+   * 0:normal congestion control(default), 1:disable congestion control
    *
-   * @param port
+   * @param nodelay
+   * @param interval
+   * @param resend
+   * @param nc
    */
-  public KcpOnUdp(int port)
+  public void noDelay(int nodelay, int interval, int resend, int nc)
   {
-    this.kcps = new HashMap<>();
-    received = new HashMap<>();
-    final NioEventLoopGroup nioEventLoopGroup = new NioEventLoopGroup(5);
-    Bootstrap bootstrap = new Bootstrap();
-    bootstrap.channel(NioDatagramChannel.class);
-    bootstrap.group(nioEventLoopGroup);
-    bootstrap.handler(new ChannelInitializer<NioDatagramChannel>()
-    {
-
-      @Override
-      protected void initChannel(NioDatagramChannel ch) throws Exception
-      {
-        ChannelPipeline cp = ch.pipeline();
-        cp.addLast(new KcpOnUdp.UdpHandler());
-      }
-    });
-    ChannelFuture sync = bootstrap.bind(port).syncUninterruptibly();
-    channel = (NioDatagramChannel) sync.channel();
-    addr = channel.localAddress();
-    Runtime.getRuntime().addShutdownHook(new Thread(new Runnable()
-    {
-      @Override
-      public void run()
-      {
-        nioEventLoopGroup.shutdownGracefully();
-      }
-    }));
+    this.kcp.noDelay(nodelay, interval, resend, nc);
   }
 
   /**
-   * close
+   * set maximum window size: sndwnd=32, rcvwnd=32 by default
    *
-   * @return
+   * @param sndwnd
+   * @param rcvwnd
    */
-  public ChannelFuture close()
+  public void wndSize(int sndwnd, int rcvwnd)
   {
-    return this.channel.close();
+    this.kcp.wndSize(sndwnd, rcvwnd);
   }
 
   /**
-   * kcp call
+   * change MTU size, default is 1400
    *
-   * @param msg
-   * @param kcp
-   * @param user
+   * @param mtu
    */
-  @Override
-  public void out(ByteBuf msg, Kcp kcp, Object user)
+  public void setMtu(int mtu)
   {
-    DatagramPacket temp = new DatagramPacket(msg, (InetSocketAddress) user, this.addr);
-    this.channel.writeAndFlush(temp);
+    this.kcp.setMtu(mtu);
   }
 
   /**
-   * one kcp per addr
+   * kcp for udp
    *
-   * @param addr
-   * @return
+   * @param out
+   * @param user
+   * @param listerner
    */
-  private Kcp getKcp(InetSocketAddress addr,boolean recreate)
+  public KcpOnUdp(Output out, Object user, KcpListerner listerner)
   {
-    Kcp kcp = null;
-    kcpLock.lock();
-    try
-    {
-      kcp = kcps.get(addr);
-      if (kcp == null||recreate)
-      {
-        kcp = new Kcp(121106, KcpOnUdp.this, addr);
-        //mode setting
-        kcp.noDelay(nodelay, interval, resend, nc);
-        kcp.wndSize(sndwnd, rcvwnd);
-        kcp.setMtu(mtu);
-        kcps.put(addr, kcp);
-      }
-    } finally
-    {
-      kcpLock.unlock();
-    }
-    return kcp;
+    this.listerner = listerner;
+    kcp = new Kcp(121106, out, user);
+    received = new LinkedList<>();
+    sendList = new LinkedBlockingQueue<>();
   }
 
   /**
    * send data to addr
    *
    * @param bb
-   * @param addr
    */
-  public void send(ByteBuf bb, InetSocketAddress addr)
+  public void send(ByteBuf bb)
   {
-    Kcp kcp = this.getKcp(addr,false);
-    kcp.send(bb);
-  }
-
-  /**
-   * update every tick
-   */
-  public void update()
-  {
-    kcpLock.lock();
-    try
-    {
-      for (Map.Entry<InetSocketAddress, Kcp> en : this.kcps.entrySet())
-      {
-        update(en.getKey(), en.getValue());
-      }
-    } finally
-    {
-      kcpLock.unlock();
-    }
+    this.sendList.add(bb);
+    this.needUpdate = true;
   }
 
   /**
@@ -172,21 +97,19 @@ public abstract class KcpOnUdp implements Output
    * @param addr
    * @param kcp
    */
-  private void update(InetSocketAddress addr, Kcp kcp)
+  void update()
   {
-    //input
-    dataLock.lock();
-    try
+    //send
+    while (!this.sendList.isEmpty())
     {
-      Queue<DatagramPacket> q = this.received.get(addr);
-      while (q != null && q.size() > 0)
-      {
-        DatagramPacket dp = q.remove();
-        kcp.input(dp.content());
-      }
-    } finally
+      ByteBuf bb = sendList.remove();
+      this.kcp.send(bb);
+    }
+    //input
+    while (!this.received.isEmpty())
     {
-      dataLock.unlock();
+      ByteBuf dp = this.received.remove();
+      kcp.input(dp);
     }
     //receive
     int len;
@@ -196,7 +119,8 @@ public abstract class KcpOnUdp implements Output
       int n = kcp.receive(bb);
       if (n > 0)
       {
-        this.handleReceive(bb, addr);
+        this.lastTime = System.currentTimeMillis();
+        this.listerner.handleReceive(bb, this);
       } else
       {
         bb.release();
@@ -204,111 +128,49 @@ public abstract class KcpOnUdp implements Output
     }
     //update kcp status
     int cur = (int) System.currentTimeMillis();
-    if (kcp.isNeedUpdate() || cur >= kcp.getNextUpdate())
+    if (this.needUpdate || cur >= kcp.getNextUpdate())
     {
       kcp.update(cur);
       kcp.setNextUpdate(kcp.check(cur));
-      kcp.setNeedUpdate(false);
+      this.needUpdate = false;
+    }
+    //check timeout
+    if (this.timeout > 0 && System.currentTimeMillis() - this.lastTime > this.timeout)
+    {
+      this.closed = true;
+      this.listerner.handleClose(this);
     }
   }
 
   /**
-   * kcp message
+   * 输入 只会在worker线程调用,不会多线程调用
    *
-   * @param bb the data 
-   * @param addr the sender
+   * @param content
    */
-  protected abstract void handleReceive(ByteBuf bb, InetSocketAddress addr);
-
-  /**
-   * fastest: ikcp_nodelay(kcp, 1, 20, 2, 1) nodelay: 0:disable(default),
-   * 1:enable interval: internal update timer interval in millisec, default is
-   * 100ms resend: 0:disable fast resend(default), 1:enable fast resend nc:
-   * 0:normal congestion control(default), 1:disable congestion control
-   *
-   * @param nodelay
-   * @param interval
-   * @param resend
-   * @param nc
-   */
-  public void noDelay(int nodelay, int interval, int resend, int nc)
+  void input(ByteBuf content)
   {
-    this.nodelay = nodelay;
-    this.interval = interval;
-    this.resend = resend;
-    this.nc = nc;
+    this.received.add(content);
+    this.needUpdate = true;
   }
 
-  /**
-   * set maximum window size: sndwnd=32, rcvwnd=32 by default
-   *
-   * @param sndwnd
-   * @param rcvwnd
-   */
-  public void wndSize(int sndwnd, int rcvwnd)
+  public boolean isClosed()
   {
-    this.sndwnd = sndwnd;
-    this.rcvwnd = rcvwnd;
+    return closed;
   }
 
-  /**
-   * change MTU size, default is 1400
-   *
-   * @param mtu
-   */
-  public void setMtu(int mtu)
+  public Kcp getKcp()
   {
-    this.mtu = mtu;
+    return kcp;
   }
 
-  /**
-   * receive DatagramPacket
-   *
-   * @param dp
-   */
-  private void onReceive(DatagramPacket dp)
+  public void setTimeout(long timeout)
   {
-    this.dataLock.lock();
-    try
-    {
-      Queue<DatagramPacket> q = this.received.get(dp.sender());
-      if (q == null)
-      {
-        q = new LinkedList<>();
-        received.put(dp.sender(), q);
-      }
-      if(dp.content().readableBytes()>0)
-      {
-        q.add(dp);
-      }else//新链接
-      {
-        this.getKcp(dp.sender(), true);
-      }      
-    } finally
-    {
-      dataLock.unlock();
-    }
+    this.timeout = timeout;
   }
 
-  /**
-   * handler
-   */
-  class UdpHandler extends ChannelInboundHandlerAdapter
+  public long getTimeout()
   {
-
-    @Override
-    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception
-    {
-      DatagramPacket dp = (DatagramPacket) msg;
-      System.out.println("udp received:"+dp);
-      KcpOnUdp.this.onReceive(dp);
-    }
-
-    @Override
-    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception
-    {
-      LOG.error(cause.toString());
-    }
+    return timeout;
   }
 
 }
