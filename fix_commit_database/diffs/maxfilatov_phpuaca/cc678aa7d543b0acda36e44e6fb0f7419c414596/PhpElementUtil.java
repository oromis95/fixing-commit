@@ -100,6 +100,13 @@ public class PhpElementUtil {
         return phpClass instanceof PhpClass ? (PhpClass) phpClass : null;
     }
 
+    @Nullable
+    public static PhpClass resolvePhpClass(@Nullable ClassConstantReference classConstantReference)
+    {
+        ClassReference classReference = PsiTreeUtil.getChildOfType(classConstantReference, ClassReference.class);
+        return classReference == null ? null : resolvePhpClass(classReference);
+    }
+
     @Nullable
     private static SmartList<Statement> findMethodStatements(@Nullable Method method)
     {
