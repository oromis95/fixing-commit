@@ -5,7 +5,6 @@ import com.intellij.patterns.PlatformPatterns;
 import com.intellij.psi.PsiElement;
 import com.intellij.psi.util.PsiTreeUtil;
 import com.intellij.util.ProcessingContext;
-import com.intellij.util.ThreeState;
 import com.jetbrains.php.completion.PhpLookupElement;
 import com.jetbrains.php.lang.psi.elements.*;
 import com.phpuaca.completion.filter.Filter;
@@ -34,8 +33,7 @@ public class StringLiteralContributor extends CompletionContributor {
                 }
 
                 ClassConstantReference classConstantReference = filter.getClassConstantReference();
-                ClassReference classReference = PsiTreeUtil.getChildOfType(classConstantReference, ClassReference.class);
-                PhpClass phpClass = PhpElementUtil.resolvePhpClass(classReference);
+                PhpClass phpClass = PhpElementUtil.resolvePhpClass(classConstantReference);
                 if (phpClass == null) {
                     return;
                 }
