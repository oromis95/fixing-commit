@@ -30,17 +30,7 @@ public abstract class Try<T> {
 
     public abstract Try<Exception> failed();
 
-    public Optional<T> toOptional() {
-        Optional<T> result = Optional.empty();
-        // TODO: Avoid try catch if possible
-        try {
-            if (isSuccess()) {
-                result = Optional.of(get());
-            }
-        } catch (Exception exception) {
-        }
-        return result;
-    }
+    public abstract Optional<T> toOptional();
 
     /* TODO:
     withFilter
@@ -48,42 +38,12 @@ public abstract class Try<T> {
 
      */
 
-    public T getOrElse(T defaultValue) {
-        T result = defaultValue;
-        // TODO: Avoid try catch if possible
-        try {
-            if (isSuccess())
-                result = get();
-        } catch (Exception exception) {
-        }
-        return result;
-    }
+    public abstract T getOrElse(T defaultValue);
 
-    public Try<? super T> orElse(Try<? super T> defaultValue) {
-        if (isSuccess())
-            return this;
-        else
-            return defaultValue;
-    }
+    public abstract Try<T> orElse(Try<T> defaultValue);
 
-    public <U> Try<? extends U> transform(Function<? super T, Try<? extends U>> g,
-                                Function<Exception, Try<? extends  U>> f) {
-        Try<? extends U> result = null;
-        if (this instanceof Success) {
-            Success<T> success = (Success<T>) this;
-            result = g.apply(success.get());
-        } else if(this instanceof Failure) {
-            Failure<T> failure = (Failure<T>) this;
-            try {
-                failure.get();
-            } catch(Exception t) {
-                result = f.apply(t);
-            }
-        } else {
-            throw new RuntimeException("Algebraic data type (Try-Success-Failure) violated");
-        }
-        return result;
-    }
+    public abstract <U> Try<U> transform(Function<? super T, ? extends Try<U>> successFunc,
+                                                   Function<Exception, ? extends Try<U>> failureFunc);
 
     public static <T> Try<T> apply(FailableSupplier<T> supplier) {
         try {
