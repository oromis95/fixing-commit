@@ -2,18 +2,14 @@ package pl.touk.throwing;
 
 import static java.util.stream.Collectors.toList;
 import static org.assertj.core.api.Assertions.assertThat;
-import static org.junit.Assert.fail;
 import static pl.touk.throwing.TestCommons.givenThrowingFunction;
 import static pl.touk.throwing.ThrowingFunction.checked;
+import static pl.touk.throwing.ThrowingFunction.trying;
 import static pl.touk.throwing.ThrowingFunction.unchecked;
 
-import java.net.MalformedURLException;
 import java.net.URI;
 import java.net.URISyntaxException;
-import java.util.List;
-import java.util.Map;
 import java.util.Optional;
-import java.util.function.Supplier;
 import java.util.stream.Stream;
 
 import org.junit.Test;
@@ -108,9 +104,7 @@ public class ThrowingFunctionTest {
         // when
         checked(URISyntaxException.class,
                 () -> Stream.of(". .")
-                        .map(unchecked(URISyntaxException.class,
-                                URI::new)
-                        )
+                .map(unchecked(URI::new))
                         .collect(toList())
         );
 
