@@ -1,6 +1,9 @@
 package pl.touk.throwing;
 
+import org.junit.Rule;
 import org.junit.Test;
+import org.junit.rules.ExpectedException;
+
 import pl.touk.throwing.exception.WrappedException;
 
 import java.io.IOException;
@@ -11,36 +14,53 @@ import java.util.stream.Stream;
 import static java.util.stream.Collectors.joining;
 import static java.util.stream.Collectors.toList;
 import static org.assertj.core.api.Assertions.assertThat;
+import static org.assertj.core.api.Assertions.not;
+import static org.hamcrest.CoreMatchers.isA;
+import static org.hamcrest.core.IsNot.not;
+import static org.junit.Assert.fail;
 import static pl.touk.throwing.ThrowingFunction.unchecked;
 
 public class CheckerTest {
+    
+    @Rule
+    public ExpectedException thrown = ExpectedException.none();
 
-    @Test(expected = URISyntaxException.class)
+    @Test
     public void shouldUnwrapOriginalExceptionWithTryCatch() throws Throwable {
+        thrown.expect(URISyntaxException.class);
+        thrown.expectMessage("Illegal character in path at index 1: . .");
 
         // when
         Checker.checked(() -> Stream.of(". .").map(unchecked(URI::new)).collect(toList()));
 
         // then a checked exception is thrown
+        fail("exception expected");
     }
 
-    @Test(expected = URISyntaxException.class)
+    @Test
     public void shouldUnwrapSpecifiedExceptionWithTryCatch() throws Throwable {
+        thrown.expect(URISyntaxException.class);
+        thrown.expectMessage("Illegal character in path at index 1: . .");
 
         // when
         Checker.checked(URISyntaxException.class,
                 () -> Stream.of(". .").map(unchecked(URI::new)).collect(toList()));
 
         // then a checked exception is thrown
+        fail("exception expected");
     }
 
-    @Test(expected = WrappedException.class)
+    @Test
     public void shouldIgnoreUnspecifiedExceptionWithTryCatch() throws Throwable {
+        thrown.expect(WrappedException.class);
+        thrown.expectMessage("Illegal character in path at index 1: . .");
+        thrown.expectCause(not(isA(IOException.class)));
 
         // when
         Checker.checked(IOException.class, () -> Stream.of(". .").map(unchecked(URI::new)).collect(toList()));
 
         // then a checked exception is thrown
+        fail("exception expected");
     }
 
     @Test
@@ -69,8 +89,10 @@ public class CheckerTest {
         assertThat(result).isEqualTo("A");
     }
 
-    @Test(expected = URISyntaxException.class)
+    @Test
     public void shouldUnwrapOriginalExceptionWhenUsingStandardUtilsFunctions() throws URISyntaxException {
+        thrown.expect(URISyntaxException.class);
+        thrown.expectMessage("Illegal character in path at index 1: . .");
 
         // when
         Checker.checked(URISyntaxException.class,
@@ -80,5 +102,6 @@ public class CheckerTest {
         );
 
         // then a checked exception is thrown
+        fail("exception expected");
     }
 }
