@@ -1,14 +1,12 @@
-package cn.wanghaomiao.xpath.core;
-
-import org.jsoup.nodes.Element;
-
-import java.util.List;
-
-/**
- * @author github.com/zhegexiaohuozi seimimaster@gmail.com
- * @since 2018/2/28.
- */
-public interface Function {
-    String name();
-    XValue call(Element context, List<XValue> params);
-}
+package cn.wanghaomiao.xpath.core;
+
+import java.util.List;
+
+/**
+ * @author github.com/zhegexiaohuozi seimimaster@gmail.com
+ * @since 2018/2/28.
+ */
+public interface Function {
+    String name();
+    XValue call(Scope scope, List<XValue> params);
+}
