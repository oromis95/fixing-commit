@@ -37,7 +37,7 @@ public class MainConfigurable implements Configurable.Composite, Configurable.No
     @Override
     public JComponent createComponent() {
         JPanel panel = new JPanel(new BorderLayout());
-        JLabel label = new JLabel("<html>Here you can edit 'Android Code Generator Plugin' settings. In children pages you can edit template for each code generation method.</html>");
+        JLabel label = new JLabel("<html>Here you can edit 'Android Code Generator' settings. In children pages you can edit template for each code generation method.</html>");
         label.setVerticalAlignment(SwingConstants.TOP);
         panel.add(label, BorderLayout.PAGE_START);
         return panel;
