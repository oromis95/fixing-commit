@@ -2,6 +2,7 @@ package com.morcinek.android.codegenerator.plugin.codegenerator;
 
 import com.intellij.openapi.editor.Editor;
 import com.intellij.openapi.fileEditor.FileEditorManager;
+import com.intellij.openapi.fileEditor.TextEditor;
 import com.intellij.openapi.project.Project;
 import com.intellij.openapi.vfs.VirtualFile;
 import com.morcinek.android.codegenerator.CodeGenerator;
@@ -12,6 +13,7 @@ import javax.xml.parsers.ParserConfigurationException;
 import javax.xml.xpath.XPathExpressionException;
 import java.io.ByteArrayInputStream;
 import java.io.IOException;
+import java.io.InputStream;
 
 /**
  * Copyright 2014 Tomasz Morcinek. All rights reserved.
@@ -25,22 +27,33 @@ public class CodeGeneratorController {
     }
 
     public String generateCode(Project project, VirtualFile file, Editor editor) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
-        editor = getTextEditor(editor, project);
-        return codeGenerator.produceCode(getEditorContents(editor), file.getName());
+        return codeGenerator.produceCode(getContents(project, editor, file), file.getName());
     }
 
-    private Editor getTextEditor(Editor editor, Project project) {
+    private InputStream getContents(Project project, Editor editor, VirtualFile file) throws IOException {
+        editor = getEditor(project, editor, file);
+        if (editor != null) {
+            return new ByteArrayInputStream(getText(editor).getBytes());
+        } else {
+            return file.getInputStream();
+        }
+    }
+
+    private Editor getEditor(Project project, Editor editor, VirtualFile file) {
         if (editor == null) {
-            return FileEditorManager.getInstance(project).getSelectedTextEditor();
+            TextEditor textEditor = getTextEditor(project, file);
+            if (textEditor != null) {
+                return textEditor.getEditor();
+            }
         }
         return editor;
     }
 
-    private ByteArrayInputStream getEditorContents(Editor editor) {
-        return new ByteArrayInputStream(getEditorText(editor).getBytes());
+    private TextEditor getTextEditor(Project project, VirtualFile file) {
+        return (TextEditor) FileEditorManager.getInstance(project).getSelectedEditor(file);
     }
 
-    private String getEditorText(Editor editor) {
+    private String getText(Editor editor) {
         return editor.getDocument().getText();
     }
 }
