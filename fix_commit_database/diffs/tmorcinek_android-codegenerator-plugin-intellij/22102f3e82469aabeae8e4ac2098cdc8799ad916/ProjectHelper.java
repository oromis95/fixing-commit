@@ -7,6 +7,7 @@ import com.intellij.openapi.fileEditor.FileEditorManager;
 import com.intellij.openapi.project.Project;
 import com.intellij.openapi.roots.ModuleRootManager;
 import com.intellij.openapi.vfs.VirtualFile;
+import org.apache.velocity.util.StringUtils;
 
 import java.io.IOException;
 import java.util.List;
@@ -55,8 +56,9 @@ public class ProjectHelper {
 
     public List<String> getSourceRootPathList(Project project, AnActionEvent event) {
         List<String> sourceRoots = Lists.newArrayList();
+        String projectPath = StringUtils.normalizePath(project.getBasePath());
         for (VirtualFile virtualFile : getModuleRootManager(event).getSourceRoots(false)) {
-            sourceRoots.add(virtualFile.getPath().replace(project.getBasePath(), ""));
+            sourceRoots.add(StringUtils.normalizePath(virtualFile.getPath()).replace(projectPath, ""));
         }
         return sourceRoots;
     }
