@@ -17,6 +17,7 @@ import org.red5.server.api.so.ISharedObjectListener;
 import org.slf4j.Logger;
 import org.slf4j.LoggerFactory;
 
+@SuppressWarnings("deprecation")
 public class SharedObjectClient extends RTMPClient implements IPendingServiceCallback, ISharedObjectListener, ClientExceptionHandler {
 
     private static Logger log = LoggerFactory.getLogger(SharedObjectClient.class);
