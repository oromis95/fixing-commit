@@ -26,12 +26,10 @@ package org.red5.client;
  */
 public final class Red5Client {
 
-	//private static Logger log = LoggerFactory.getLogger(Red5Client.class);
-
 	/**
 	 * Current server version with revision
 	 */
-	public static final String VERSION = "Red5 Client 1.0.2 RC1 $Rev: 4445 $";
+	public static final String VERSION = "Red5 Client 1.0.5";
 
 	/**
 	 * Create a new Red5Client object using the connection local to the current thread
