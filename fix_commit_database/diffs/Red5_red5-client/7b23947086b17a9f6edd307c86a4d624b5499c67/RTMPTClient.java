@@ -22,11 +22,12 @@ import java.util.Map;
 
 import org.apache.mina.core.buffer.IoBuffer;
 import org.red5.client.net.rtmp.BaseRTMPClientHandler;
+import org.red5.client.net.rtmp.OutboundHandshake;
 import org.red5.server.net.rtmp.RTMPConnection;
-import org.red5.server.net.rtmp.RTMPHandshake;
 import org.red5.server.net.rtmp.codec.RTMP;
 import org.red5.server.net.rtmp.codec.RTMPProtocolDecoder;
 import org.red5.server.net.rtmp.codec.RTMPProtocolEncoder;
+import org.red5.server.net.rtmp.message.Constants;
 import org.red5.server.net.rtmp.message.Packet;
 import org.red5.server.net.rtmpt.codec.RTMPTCodecFactory;
 import org.slf4j.Logger;
@@ -82,18 +83,65 @@ public class RTMPTClient extends BaseRTMPClientHandler {
         } else {
             // raw buffer handling
             IoBuffer in = (IoBuffer) message;
+            // filter based on current connection state
+            RTMP rtmp = conn.getState();
+            final byte connectionState = conn.getStateCode();
+            log.trace("connectionState: {}", RTMP.states[connectionState]);
             // get the handshake
-            RTMPHandshake handshake = (RTMPHandshake) conn.getAttribute(RTMPConnection.RTMP_HANDSHAKE);
-            if (handshake != null) {
-                log.debug("Handshake - client phase 2 - size: {}", in.remaining());
-                in.position(2);
-                IoBuffer out = handshake.doHandshake(in);
-                if (out != null) {
-                    conn.writeRaw(out);
+            OutboundHandshake handshake = (OutboundHandshake) conn.getAttribute(RTMPConnection.RTMP_HANDSHAKE);
+            switch (connectionState) {
+                case RTMP.STATE_CONNECT:
+                    log.debug("Handshake - client phase 1 - size: {}", in.remaining());
+                    in.get(); // 0x01
+                    byte handshakeType = in.get(); // usually 0x03 (rtmp)
+                    log.debug("Handshake - byte type: {}", handshakeType);
+                    // copy out 1536 bytes
+                    byte[] s1 = new byte[Constants.HANDSHAKE_SIZE];
+                    in.get(s1);
+                    // decode s1
+                    IoBuffer out = handshake.decodeServerResponse1(IoBuffer.wrap(s1));
+                    if (out != null) {
+                        // set state to indicate we're waiting for S2
+                        rtmp.setState(RTMP.STATE_HANDSHAKE);
+                        conn.writeRaw(out);
+                        // if we got S0S1+S2 continue processing
+                        if (in.remaining() >= Constants.HANDSHAKE_SIZE) {
+                            log.debug("Handshake - client phase 2 - size: {}", in.remaining());
+                            if (handshake.decodeServerResponse2(in)) {
+//                                conn.removeAttribute(RTMPConnection.RTMP_HANDSHAKE);
+//                                conn.setStateCode(RTMP.STATE_CONNECTED);
+//                                connectionOpened(conn);
+                            } else {
+                                log.warn("Handshake failed on S2 processing");
+                                //conn.close();
+                            }
+                            // open regardless of server type
+                            conn.removeAttribute(RTMPConnection.RTMP_HANDSHAKE);
+                            conn.setStateCode(RTMP.STATE_CONNECTED);
+                            connectionOpened(conn);
+                        }
+                    } else {
+                        log.warn("Handshake failed on S0S1 processing");
+                        conn.close();
+                    }
+                    break;
+                case RTMP.STATE_HANDSHAKE:
+                    log.debug("Handshake - client phase 2 - size: {}", in.remaining());
+                    if (handshake.decodeServerResponse2(in)) {
+//                        conn.removeAttribute(RTMPConnection.RTMP_HANDSHAKE);
+//                        conn.setStateCode(RTMP.STATE_CONNECTED);
+//                        connectionOpened(conn);
+                    } else {
+                        log.warn("Handshake failed on S2 processing");
+                        //conn.close();
+                    }
+                    // open regardless of server type
                     conn.removeAttribute(RTMPConnection.RTMP_HANDSHAKE);
                     conn.setStateCode(RTMP.STATE_CONNECTED);
                     connectionOpened(conn);
-                }
+                    break;
+                default:
+                    throw new IllegalStateException("Invalid RTMP state: " + connectionState);
             }
         }
     }
