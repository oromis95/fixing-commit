@@ -819,6 +819,19 @@ public class GUI extends Application{
 
         vb.getChildren().add(reset);
 
+        Label trashLabel = new Label("Gmail trash folder name: ");
+
+        final TextField trashNameField = new TextField(trashName);
+
+        trashNameField.setPrefWidth(65);
+
+        HBox trash = new HBox();
+        trash.setAlignment(Pos.CENTER);
+        trash.getChildren().addAll(trashLabel,trashNameField);
+        trash.setSpacing(10);
+
+        vb.getChildren().add(trash);
+
 //        CheckBox outputFormat = new CheckBox("RocketMap output formatting");
 //        outputFormat.setSelected();
         ComboBox<OutputFormat> outputFormatComboBox = new ComboBox<>(FXCollections.observableArrayList(OutputFormat.values()));
@@ -837,6 +850,8 @@ public class GUI extends Application{
         usePrivateDomain.setSelected(privateDomain);
         vb.getChildren().addAll(usePrivateDomain);
 
+
+
         TextArea textArea = new TextArea();
         textArea.setEditable(false);
         textArea.setPrefHeight(500);
