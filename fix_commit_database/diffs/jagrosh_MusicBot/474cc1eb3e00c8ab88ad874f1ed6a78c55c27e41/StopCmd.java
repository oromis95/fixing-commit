@@ -37,8 +37,8 @@ public class StopCmd extends MusicCommand {
     @Override
     public void doCommand(CommandEvent event) {
         AudioHandler handler = (AudioHandler)event.getGuild().getAudioManager().getSendingHandler();
-        handler.getPlayer().stopTrack();
         handler.getQueue().clear();
+        handler.getPlayer().stopTrack();
         event.getGuild().getAudioManager().closeAudioConnection();
         event.reply(event.getClient().getSuccess()+" The player has stopped and the queue has been cleared.");
     }
