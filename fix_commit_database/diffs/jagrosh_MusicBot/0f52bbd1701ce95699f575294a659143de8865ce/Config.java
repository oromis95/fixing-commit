@@ -13,7 +13,7 @@
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
-package me.jagrosh.jmusicbot;
+package com.jagrosh.jmusicbot;
 
 import java.io.IOException;
 import java.nio.file.Files;
@@ -124,7 +124,8 @@ public class Config {
             try {
                 Files.write(Paths.get("config.txt"), builder.toString().trim().getBytes());
             } catch(IOException ex) {
-                alert("Failed to write new config options to config.txt: "+ex);
+                alert("Failed to write new config options to config.txt: "+ex
+                    + "\nPlease make sure that the files are not on your desktop or some other restricted area.");
             }
         }
     }
