@@ -13,11 +13,11 @@
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
-package me.jagrosh.jmusicbot.utils;
+package com.jagrosh.jmusicbot.utils;
 
 import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
 import java.util.List;
-import me.jagrosh.jmusicbot.audio.AudioHandler;
+import com.jagrosh.jmusicbot.audio.AudioHandler;
 import net.dv8tion.jda.core.JDA;
 import net.dv8tion.jda.core.entities.Role;
 import net.dv8tion.jda.core.entities.TextChannel;
