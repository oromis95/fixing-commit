@@ -13,12 +13,12 @@
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
-package me.jagrosh.jmusicbot.commands;
+package com.jagrosh.jmusicbot.commands;
 
-import me.jagrosh.jdautilities.commandclient.CommandEvent;
-import me.jagrosh.jmusicbot.Bot;
-import me.jagrosh.jmusicbot.audio.AudioHandler;
-import me.jagrosh.jmusicbot.audio.QueuedTrack;
+import com.jagrosh.jdautilities.commandclient.CommandEvent;
+import com.jagrosh.jmusicbot.Bot;
+import com.jagrosh.jmusicbot.audio.AudioHandler;
+import com.jagrosh.jmusicbot.audio.QueuedTrack;
 import net.dv8tion.jda.core.Permission;
 import net.dv8tion.jda.core.entities.User;
 import net.dv8tion.jda.core.utils.PermissionUtil;
