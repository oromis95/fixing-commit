@@ -13,16 +13,16 @@
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
-package me.jagrosh.jmusicbot;
+package com.jagrosh.jmusicbot;
 
 import java.awt.Color;
 import javax.security.auth.login.LoginException;
-import me.jagrosh.jdautilities.commandclient.CommandClient;
-import me.jagrosh.jdautilities.commandclient.CommandClientBuilder;
-import me.jagrosh.jdautilities.commandclient.examples.*;
-import me.jagrosh.jdautilities.waiter.EventWaiter;
-import me.jagrosh.jmusicbot.commands.*;
-import me.jagrosh.jmusicbot.gui.GUI;
+import com.jagrosh.jdautilities.commandclient.CommandClient;
+import com.jagrosh.jdautilities.commandclient.CommandClientBuilder;
+import com.jagrosh.jdautilities.commandclient.examples.*;
+import com.jagrosh.jdautilities.waiter.EventWaiter;
+import com.jagrosh.jmusicbot.commands.*;
+import com.jagrosh.jmusicbot.gui.GUI;
 import net.dv8tion.jda.core.AccountType;
 import net.dv8tion.jda.core.JDABuilder;
 import net.dv8tion.jda.core.OnlineStatus;
