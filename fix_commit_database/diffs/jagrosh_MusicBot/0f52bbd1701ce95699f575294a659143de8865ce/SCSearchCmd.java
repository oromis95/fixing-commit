@@ -13,7 +13,7 @@
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
-package me.jagrosh.jmusicbot.commands;
+package com.jagrosh.jmusicbot.commands;
 
 import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
 import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
@@ -21,10 +21,10 @@ import com.sedmelluq.discord.lavaplayer.tools.FriendlyException.Severity;
 import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
 import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
 import java.util.concurrent.TimeUnit;
-import me.jagrosh.jdautilities.commandclient.CommandEvent;
-import me.jagrosh.jdautilities.menu.orderedmenu.OrderedMenuBuilder;
-import me.jagrosh.jmusicbot.Bot;
-import me.jagrosh.jmusicbot.utils.FormatUtil;
+import com.jagrosh.jdautilities.commandclient.CommandEvent;
+import com.jagrosh.jdautilities.menu.orderedmenu.OrderedMenuBuilder;
+import com.jagrosh.jmusicbot.Bot;
+import com.jagrosh.jmusicbot.utils.FormatUtil;
 import net.dv8tion.jda.core.Permission;
 import net.dv8tion.jda.core.entities.Message;
 
@@ -100,7 +100,7 @@ public class SCSearchCmd extends MusicCommand {
             {
                 AudioTrack track = playlist.getTracks().get(i);
                 builder.addChoices("`["+FormatUtil.formatTime(track.getDuration())+"]` [**"
-                        +track.getInfo().title+"**](https://youtu.be/"+track.getIdentifier()+")");
+                        +track.getInfo().title+"**]("+track.getInfo().uri+")");
             }
             builder.build().display(m);
         }
