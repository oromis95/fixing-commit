@@ -13,7 +13,7 @@
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
-package me.jagrosh.jmusicbot;
+package com.jagrosh.jmusicbot;
 
 import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
 import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
@@ -27,12 +27,12 @@ import java.util.HashMap;
 import java.util.concurrent.Executors;
 import java.util.concurrent.ScheduledExecutorService;
 import java.util.concurrent.TimeUnit;
-import me.jagrosh.jdautilities.commandclient.Command.Category;
-import me.jagrosh.jdautilities.commandclient.CommandEvent;
-import me.jagrosh.jdautilities.waiter.EventWaiter;
-import me.jagrosh.jmusicbot.audio.AudioHandler;
-import me.jagrosh.jmusicbot.gui.GUI;
-import me.jagrosh.jmusicbot.utils.FormatUtil;
+import com.jagrosh.jdautilities.commandclient.Command.Category;
+import com.jagrosh.jdautilities.commandclient.CommandEvent;
+import com.jagrosh.jdautilities.waiter.EventWaiter;
+import com.jagrosh.jmusicbot.audio.AudioHandler;
+import com.jagrosh.jmusicbot.gui.GUI;
+import com.jagrosh.jmusicbot.utils.FormatUtil;
 import net.dv8tion.jda.core.JDA;
 import net.dv8tion.jda.core.Permission;
 import net.dv8tion.jda.core.entities.Guild;
@@ -152,7 +152,7 @@ public class Bot extends ListenerAdapter {
         {
             String otherText;
             if(tchan.getTopic()==null || tchan.getTopic().isEmpty())
-                otherText = "";
+                otherText = "\u200B";
             else if(tchan.getTopic().contains("\u200B"))
                 otherText = tchan.getTopic().substring(tchan.getTopic().indexOf("\u200B"));
             else
@@ -293,7 +293,7 @@ public class Bot extends ListenerAdapter {
     public void clearTextChannel(Guild guild)
     {
         Settings s = getSettings(guild);
-        if(s!=null)
+        if(s!=Settings.DEFAULT_SETTINGS)
         {
             if(s.getVoiceId()==null && s.getRoleId()==null)
                 settings.remove(guild.getId());
@@ -306,7 +306,7 @@ public class Bot extends ListenerAdapter {
     public void clearVoiceChannel(Guild guild)
     {
         Settings s = getSettings(guild);
-        if(s!=null)
+        if(s!=Settings.DEFAULT_SETTINGS)
         {
             if(s.getTextId()==null && s.getRoleId()==null)
                 settings.remove(guild.getId());
@@ -319,7 +319,7 @@ public class Bot extends ListenerAdapter {
     public void clearRole(Guild guild)
     {
         Settings s = getSettings(guild);
-        if(s!=null)
+        if(s!=Settings.DEFAULT_SETTINGS)
         {
             if(s.getVoiceId()==null && s.getTextId()==null)
                 settings.remove(guild.getId());
