@@ -13,14 +13,14 @@
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
-package me.jagrosh.jmusicbot.gui;
+package com.jagrosh.jmusicbot.gui;
 
 import java.awt.event.WindowEvent;
 import java.awt.event.WindowListener;
 import javax.swing.JFrame;
 import javax.swing.JTabbedPane;
 import javax.swing.WindowConstants;
-import me.jagrosh.jmusicbot.Bot;
+import com.jagrosh.jmusicbot.Bot;
 
 
 /**
