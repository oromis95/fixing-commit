@@ -13,12 +13,12 @@
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
-package me.jagrosh.jmusicbot.commands;
+package com.jagrosh.jmusicbot.commands;
 
 import java.util.List;
-import me.jagrosh.jdautilities.commandclient.CommandEvent;
-import me.jagrosh.jmusicbot.Bot;
-import me.jagrosh.jmusicbot.playlist.Playlist;
+import com.jagrosh.jdautilities.commandclient.CommandEvent;
+import com.jagrosh.jmusicbot.Bot;
+import com.jagrosh.jmusicbot.playlist.Playlist;
 
 /**
  *
@@ -55,6 +55,7 @@ public class PlaylistsCmd extends MusicCommand {
         {
             StringBuilder builder = new StringBuilder(event.getClient().getSuccess()+" Available playlists:\n");
             list.forEach(str -> builder.append("`").append(str).append("` "));
+            builder.append("\nType `").append(event.getClient().getTextualPrefix()).append("play playlist <name>` to play a playlist");
             event.reply(builder.toString());
         }
     }
