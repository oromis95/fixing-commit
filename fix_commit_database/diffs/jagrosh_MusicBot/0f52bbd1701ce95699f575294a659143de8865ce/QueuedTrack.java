@@ -13,11 +13,11 @@
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
-package me.jagrosh.jmusicbot.audio;
+package com.jagrosh.jmusicbot.audio;
 
 import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
-import me.jagrosh.jmusicbot.queue.Queueable;
-import me.jagrosh.jmusicbot.utils.FormatUtil;
+import com.jagrosh.jmusicbot.queue.Queueable;
+import com.jagrosh.jmusicbot.utils.FormatUtil;
 
 /**
  *
