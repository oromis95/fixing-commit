@@ -13,7 +13,7 @@
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
-package me.jagrosh.jmusicbot.gui;
+package com.jagrosh.jmusicbot.gui;
 
 import java.awt.Dimension;
 import java.awt.GridBagConstraints;
@@ -26,9 +26,9 @@ import javax.swing.JScrollPane;
 import javax.swing.JTextArea;
 import javax.swing.ListSelectionModel;
 import javax.swing.event.ListSelectionEvent;
-import me.jagrosh.jmusicbot.Bot;
-import me.jagrosh.jmusicbot.audio.AudioHandler;
-import me.jagrosh.jmusicbot.utils.FormatUtil;
+import com.jagrosh.jmusicbot.Bot;
+import com.jagrosh.jmusicbot.audio.AudioHandler;
+import com.jagrosh.jmusicbot.utils.FormatUtil;
 import net.dv8tion.jda.core.entities.Guild;
 
 /**
