@@ -13,17 +13,17 @@
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
-package me.jagrosh.jmusicbot.commands;
+package com.jagrosh.jmusicbot.commands;
 
 import java.io.File;
 import java.io.IOException;
 import java.nio.file.Files;
 import java.nio.file.Paths;
 import java.util.List;
-import me.jagrosh.jdautilities.commandclient.Command;
-import me.jagrosh.jdautilities.commandclient.CommandEvent;
-import me.jagrosh.jmusicbot.Bot;
-import me.jagrosh.jmusicbot.playlist.Playlist;
+import com.jagrosh.jdautilities.commandclient.Command;
+import com.jagrosh.jdautilities.commandclient.CommandEvent;
+import com.jagrosh.jmusicbot.Bot;
+import com.jagrosh.jmusicbot.playlist.Playlist;
 
 /**
  *
@@ -55,7 +55,7 @@ public class PlaylistCmd extends Command {
         StringBuilder builder = new StringBuilder(event.getClient().getWarning()+" Playlist Management Commands:\n");
         for(Command cmd: this.children)
             builder.append("\n`").append(event.getClient().getPrefix()).append(name).append(" ").append(cmd.getName())
-                    .append(" ").append(cmd.getArguments()).append("` - ").append(cmd.getHelp());
+                    .append(" ").append(cmd.getArguments()==null ? "" : cmd.getArguments()).append("` - ").append(cmd.getHelp());
         event.reply(builder.toString());
     }
     
@@ -68,6 +68,7 @@ public class PlaylistCmd extends Command {
             this.help = "makes a new playlist";
             this.arguments = "<name>";
             this.category = bot.OWNER;
+            this.ownerCommand = true;
             this.guildOnly = false;
         }
 
@@ -100,6 +101,7 @@ public class PlaylistCmd extends Command {
             this.help = "deletes an existing playlist";
             this.arguments = "<name>";
             this.guildOnly = false;
+            this.ownerCommand = true;
             this.category = bot.OWNER;
         }
 
@@ -132,6 +134,7 @@ public class PlaylistCmd extends Command {
             this.help = "appends songs to an existing playlist";
             this.arguments = "<name> <URL> | <URL> | ...";
             this.guildOnly = false;
+            this.ownerCommand = true;
             this.category = bot.OWNER;
         }
 
@@ -181,6 +184,7 @@ public class PlaylistCmd extends Command {
             this.help = "sets the default playlist for the server";
             this.arguments = "<playlistname|NONE>";
             this.guildOnly = true;
+            this.ownerCommand = true;
             this.category = bot.OWNER;
         }
 
@@ -217,6 +221,7 @@ public class PlaylistCmd extends Command {
             this.aliases = new String[]{"available","list"};
             this.help = "lists all available playlists";
             this.guildOnly = true;
+            this.ownerCommand = true;
             this.category = bot.OWNER;
         }
 
