@@ -13,14 +13,14 @@
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
-package me.jagrosh.jmusicbot.commands;
+package com.jagrosh.jmusicbot.commands;
 
 import java.util.List;
-import me.jagrosh.jdautilities.commandclient.Command;
-import me.jagrosh.jdautilities.commandclient.CommandEvent;
-import me.jagrosh.jmusicbot.Bot;
-import me.jagrosh.jmusicbot.utils.FinderUtil;
-import me.jagrosh.jmusicbot.utils.FormatUtil;
+import com.jagrosh.jdautilities.commandclient.Command;
+import com.jagrosh.jdautilities.commandclient.CommandEvent;
+import com.jagrosh.jmusicbot.Bot;
+import com.jagrosh.jmusicbot.utils.FinderUtil;
+import com.jagrosh.jmusicbot.utils.FormatUtil;
 import net.dv8tion.jda.core.entities.VoiceChannel;
 
 /**
