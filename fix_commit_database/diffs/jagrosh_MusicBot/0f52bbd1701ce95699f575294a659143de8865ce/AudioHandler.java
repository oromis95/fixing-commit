@@ -13,7 +13,7 @@
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
-package me.jagrosh.jmusicbot.audio;
+package com.jagrosh.jmusicbot.audio;
 
 import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
 import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
@@ -24,9 +24,9 @@ import java.util.HashSet;
 import java.util.LinkedList;
 import java.util.List;
 import java.util.Set;
-import me.jagrosh.jmusicbot.Bot;
-import me.jagrosh.jmusicbot.playlist.Playlist;
-import me.jagrosh.jmusicbot.queue.FairQueue;
+import com.jagrosh.jmusicbot.Bot;
+import com.jagrosh.jmusicbot.playlist.Playlist;
+import com.jagrosh.jmusicbot.queue.FairQueue;
 import net.dv8tion.jda.core.audio.AudioSendHandler;
 import net.dv8tion.jda.core.entities.Guild;
 import net.dv8tion.jda.core.entities.User;
