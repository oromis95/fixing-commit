@@ -18,6 +18,8 @@ import spectramusic.Command;
 import spectramusic.Sender;
 import spectramusic.SpConst;
 import spectramusic.entities.ClumpedMusicPlayer;
+import spectramusic.web.YoutubeSearcher;
+import spectramusic.web.YoutubeSearcher.YoutubeInfo;
 
 /**
  *
@@ -25,9 +27,11 @@ import spectramusic.entities.ClumpedMusicPlayer;
  */
 public class SearchCmd extends Command {
     private final Bot bot;
-    public SearchCmd(Bot bot)
+    private final YoutubeSearcher searcher;
+    public SearchCmd(Bot bot, YoutubeSearcher searcher)
     {
         this.bot = bot;
+        this.searcher = searcher;
         this.command = "search";
         this.help = "searches YouTube and offers results to be played";
         this.arguments = "<query>";
@@ -36,37 +40,61 @@ public class SearchCmd extends Command {
     
     @Override
     protected void execute(String args, GuildMessageReceivedEvent event, PermLevel caller, ClumpedMusicPlayer player) {
-        try 
+        StringBuilder builder = new StringBuilder(SpConst.SUCCESS+"<@"+event.getAuthor().getId()+"> Results for `"+args+"`:");
+        if(searcher==null)
         {
-            String query = "ytsearch4:"+URLEncoder.encode(args, "UTF-8");
-            Playlist playlist;
-            event.getChannel().sendTyping();
-            playlist = Playlist.getPlaylist(query);
-            List<AudioSource> list = new ArrayList<>();
-            if(playlist.getSources().isEmpty())
+            try 
+            {
+                String query = "ytsearch4:"+URLEncoder.encode(args, "UTF-8");
+                Playlist playlist;
+                event.getChannel().sendTyping();
+                playlist = Playlist.getPlaylist(query);
+                List<AudioSource> list = new ArrayList<>();
+                if(playlist.getSources().isEmpty())
+                {
+                    Sender.sendReply(SpConst.WARNING+"No results found for `"+args+"`", event);
+                    return;
+                }
+                for(int i=0; i<playlist.getSources().size() && i<3; i++)
+                {
+                    AudioInfo info = playlist.getSources().get(i).getInfo();
+                    if(info.getError()==null)
+                    {
+                        list.add(playlist.getSources().get(i));
+                        builder.append("\n**").append(i+1).append(".** `[")
+                            .append(info.getDuration().getTimestamp())
+                            .append("]` **").append(info.getTitle()).append("**");
+                    }
+                }
+                builder.append("\nType the number of your choice to play, or any invalid choice to cancel");
+                Sender.sendReplyNoDelete(builder.toString(), event, m -> bot.addSearch(event, list, null, m) );
+            } 
+            catch(NullPointerException | UnsupportedEncodingException e)
+            {
+                Sender.sendReply(SpConst.ERROR+"The given query or result was invalid", event);
+            }
+        }
+        else
+        {
+            List<YoutubeInfo> list = searcher.getResults(args, 3);
+            if(list==null)
+            {
+                Sender.sendReply(SpConst.ERROR+"The search was unable to be completed", event);
+                return;
+            }
+            if(list.isEmpty())
             {
                 Sender.sendReply(SpConst.WARNING+"No results found for `"+args+"`", event);
                 return;
             }
-            StringBuilder builder = new StringBuilder(SpConst.SUCCESS+"<@"+event.getAuthor().getId()+"> Results for `"+args+"`:");
-            for(int i=0; i<playlist.getSources().size() && i<3; i++)
+            for(int i=0; i<list.size(); i++)
             {
-                AudioInfo info = playlist.getSources().get(i).getInfo();
-                if(info.getError()==null)
-                {
-                    list.add(playlist.getSources().get(i));
-                    builder.append("\n**").append(i+1).append(".** `[")
-                        .append(info.getDuration().getTimestamp())
-                        .append("]` **").append(info.getTitle()).append("**");
-                }
+                builder.append("\n**").append(i+1).append(".** `[")
+                        .append(list.get(i).duration)
+                        .append("]` **").append(list.get(i).title).append("**");
             }
             builder.append("\nType the number of your choice to play, or any invalid choice to cancel");
-            Sender.sendReplyNoDelete(builder.toString(), event, m -> bot.addSearch(event, list, m) );
-        } 
-        catch(NullPointerException | UnsupportedEncodingException e)
-        {
-            Sender.sendReply(SpConst.ERROR+"The given query or result was invalid", event);
-            e.printStackTrace();
+            Sender.sendReplyNoDelete(builder.toString(), event, m -> bot.addSearch(event, null, list, m) );
         }
     }
     
