@@ -14,6 +14,7 @@ import java.io.File;
 import java.io.IOException;
 import java.nio.file.Files;
 import java.nio.file.Paths;
+import java.util.ArrayList;
 import java.util.Arrays;
 import java.util.LinkedList;
 import java.util.List;
@@ -29,11 +30,13 @@ public class Playlist {
     private final List<String> items;
     private List<AudioTrack> tracks;
     private List<PlaylistLoadError> errors;
+    private final boolean shuffle;
     
-    private Playlist(String name, List<String> items)
+    private Playlist(String name, List<String> items, boolean shuffle)
     {
         this.name = name;
         this.items = items;
+        this.shuffle = shuffle;
     }
     
     public void loadTracks(AudioPlayerManager manager, Runnable callback)
@@ -50,8 +53,13 @@ public class Playlist {
                     @Override
                     public void trackLoaded(AudioTrack at) {
                         tracks.add(at);
-                        if(last && callback!=null)
-                            callback.run();
+                        if(last)
+                        {
+                            if(shuffle)
+                                shuffleTracks();
+                            if(callback!=null)
+                                callback.run();
+                        }
                     }
                     @Override
                     public void playlistLoaded(AudioPlaylist ap) {
@@ -61,22 +69,37 @@ public class Playlist {
                             tracks.add(ap.getSelectedTrack());
                         else
                             tracks.addAll(ap.getTracks());
-                        if(last && callback!=null)
-                            callback.run();
+                        if(last)
+                        {
+                            if(shuffle)
+                                shuffleTracks();
+                            if(callback!=null)
+                                callback.run();
+                        }
                     }
 
                     @Override
                     public void noMatches() {
                         errors.add(new PlaylistLoadError(index, items.get(index), "No matches found."));
-                        if(last && callback!=null)
-                            callback.run();
+                        if(last)
+                        {
+                            if(shuffle)
+                                shuffleTracks();
+                            if(callback!=null)
+                                callback.run();
+                        }
                     }
 
                     @Override
                     public void loadFailed(FriendlyException fe) {
                         errors.add(new PlaylistLoadError(index, items.get(index), "Failed to load track: "+fe.getLocalizedMessage()));
-                        if(last && callback!=null)
-                            callback.run();
+                        if(last)
+                        {
+                            if(shuffle)
+                                shuffleTracks();
+                            if(callback!=null)
+                                callback.run();
+                        }
                     }
                 });
             }
@@ -98,6 +121,20 @@ public class Playlist {
         return tracks;
     }
     
+    public void shuffleTracks()
+    {
+        if(tracks!=null)
+        {
+            for(int first =0; first<tracks.size(); first++)
+            {
+                int second = (int)(Math.random()*tracks.size());
+                AudioTrack tmp = tracks.get(first);
+                tracks.set(first, tracks.get(second));
+                tracks.set(second, tmp);
+            }
+        }
+    }
+    
     public List<PlaylistLoadError> getErrors()
     {
         return errors;
@@ -138,11 +175,22 @@ public class Playlist {
         {
             if(folderExists())
             {
-                return new Playlist(name, Files.readAllLines(Paths.get("Playlists"+File.separator+name+".txt"))
-                        .stream()
-                        .map((str) -> str.trim())
-                        .filter((s) -> (!s.isEmpty() && !s.startsWith("#") && !s.startsWith("//")))
-                        .collect(Collectors.toList()));
+                boolean[] shuffle = {false};
+                List<String> list = new ArrayList<>();
+                Files.readAllLines(Paths.get("Playlists"+File.separator+name+".txt")).forEach(str -> {
+                    String s = str.trim();
+                    if(s.isEmpty())
+                        return;
+                    if(s.startsWith("#") || s.startsWith("//"))
+                    {
+                        s = s.replaceAll("\\s+", "");
+                        if(s.equalsIgnoreCase("#shuffle") || s.equalsIgnoreCase("//shuffle"))
+                            shuffle[0]=true;
+                    }
+                    else
+                        list.add(s);
+                });
+                return new Playlist(name, list, shuffle[0]);
             }
             else
             {
