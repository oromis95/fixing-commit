@@ -23,7 +23,7 @@ public interface IRedisManager {
      * @param value
      * @return
      */
-    public byte[] set(byte[] key, byte[] value);
+    public byte[] set(byte[] key, byte[] value, int expire);
 
     /**
      * del
@@ -43,10 +43,4 @@ public interface IRedisManager {
      */
     public Set<byte[]> keys(byte[] pattern);
 
-    /**
-     * expire time
-     * @return
-     */
-    public int getExpire();
-
 }
