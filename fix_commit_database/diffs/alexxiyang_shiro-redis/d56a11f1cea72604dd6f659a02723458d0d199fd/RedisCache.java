@@ -1,58 +1,34 @@
 package org.crazycake.shiro;
 
-import java.util.ArrayList;
-import java.util.Collection;
-import java.util.Collections;
-import java.util.HashSet;
-import java.util.List;
-import java.util.Set;
-
 import org.apache.shiro.cache.Cache;
 import org.apache.shiro.cache.CacheException;
 import org.apache.shiro.util.CollectionUtils;
 import org.slf4j.Logger;
 import org.slf4j.LoggerFactory;
 
+import java.util.*;
+
 public class RedisCache<K, V> implements Cache<K, V> {
 	
-	private Logger logger = LoggerFactory.getLogger(this.getClass());
-		
-	/**
-     * The wrapped Jedis instance.
-     */
-	private RedisManager cache;
-	
-	/**
-	 * The Redis key prefix for the sessions 
-	 */
-	private String keyPrefix = "shiro_redis_session:";
-	
-	/**
-	 * Returns the Redis session keys
-	 * prefix.
-	 * @return The prefix
-	 */
-	public String getKeyPrefix() {
-		return keyPrefix;
-	}
+	private static Logger logger = LoggerFactory.getLogger(RedisCache.class);
+	private static final String DEFAULT_CACHE_KEY_PREFIX = "shiro:cache:";
+
+	private RedisSerializer keySerializer = new StringSerializer();
+	private RedisSerializer valueSerializer = new ObjectSerializer();
+	private RedisManager redisManager;
+	private String keyPrefix = DEFAULT_CACHE_KEY_PREFIX;
 
 	/**
-	 * Sets the Redis sessions key 
-	 * prefix.
-	 * @param keyPrefix The prefix
-	 */
-	public void setKeyPrefix(String keyPrefix) {
-		this.keyPrefix = keyPrefix;
-	}
-	
-	/**
-	 * 通过一个JedisManager实例构造RedisCache
+	 * Construction
+	 * @param redisManager
 	 */
-	public RedisCache(RedisManager cache){
-		 if (cache == null) {
+	public RedisCache(RedisManager redisManager, RedisSerializer keySerializer, RedisSerializer valueSerializer){
+		 if (redisManager == null) {
 	         throw new IllegalArgumentException("Cache argument cannot be null.");
 	     }
-	     this.cache = cache;
+	     this.redisManager = redisManager;
+		 this.keySerializer = keySerializer;
+		 this.valueSerializer = valueSerializer;
 	}
 	
 	/**
@@ -61,129 +37,141 @@ public class RedisCache<K, V> implements Cache<K, V> {
 	 * @param cache The cache manager instance
 	 * @param prefix The Redis key prefix
 	 */
-	public RedisCache(RedisManager cache, 
+	public RedisCache(RedisManager cache, RedisSerializer keySerializer, RedisSerializer valueSerializer,
 				String prefix){
-		 
-		this( cache );
-		
-		// set the prefix
+		this( cache, keySerializer, valueSerializer );
 		this.keyPrefix = prefix;
 	}
-	
-	/**
-	 * 获得byte[]型的key
-	 * @param key
-	 * @return
-	 */
-	private byte[] getByteKey(K key){
-		if(key instanceof String){
-			String preKey = this.keyPrefix + key;
-    		return preKey.getBytes();
-    	}else{
-    		return SerializeUtils.serialize(key);
-    	}
-	}
  	
 	@Override
 	public V get(K key) throws CacheException {
-		logger.debug("根据key从Redis中获取对象 key [" + key + "]");
-		try {
-			if (key == null) {
-	            return null;
-	        }else{
-	        	byte[] rawValue = cache.get(getByteKey(key));
-	        	@SuppressWarnings("unchecked")
-				V value = (V)SerializeUtils.deserialize(rawValue);
-	        	return value;
-	        }
-		} catch (Throwable t) {
-			throw new CacheException(t);
+		logger.debug("get key [" + key + "]");
+
+		if (key == null) {
+			return null;
 		}
 
+		try {
+			Object redisCacheKey = getRedisCacheKey(key);
+			byte[] rawValue = redisManager.get(keySerializer.serialize(redisCacheKey));
+			V value = (V) valueSerializer.deserialize(rawValue);
+			return value;
+		} catch (SerializationException e) {
+			throw new CacheException(e);
+		}
 	}
 
 	@Override
 	public V put(K key, V value) throws CacheException {
-		logger.debug("根据key从存储 key [" + key + "]");
-		 try {
-			 	cache.set(getByteKey(key), SerializeUtils.serialize(value));
-	            return value;
-	        } catch (Throwable t) {
-	            throw new CacheException(t);
-	        }
+		logger.debug("put key [" + key + "]");
+		try {
+			Object redisCacheKey = getRedisCacheKey(key);
+			redisManager.set(keySerializer.serialize(redisCacheKey), valueSerializer.serialize(value));
+			return value;
+		} catch (SerializationException e) {
+			throw new CacheException(e);
+		}
 	}
 
 	@Override
 	public V remove(K key) throws CacheException {
-		logger.debug("从redis中删除 key [" + key + "]");
+		logger.debug("remove key [" + key + "]");
 		try {
             V previous = get(key);
-            cache.del(getByteKey(key));
+			Object redisCacheKey = getRedisCacheKey(key);
+            redisManager.del(keySerializer.serialize(redisCacheKey));
             return previous;
-        } catch (Throwable t) {
-            throw new CacheException(t);
+        } catch (SerializationException e) {
+            throw new CacheException(e);
         }
 	}
 
+	private Object getRedisCacheKey(K key) {
+		if (key == null) {
+			return null;
+		}
+		Object redisKey = key;
+		if (keySerializer instanceof StringSerializer) {
+            redisKey = key.toString();
+        }
+        if (redisKey instanceof String) {
+		    return this.keyPrefix + (String)redisKey;
+        }
+		return redisKey;
+	}
+
 	@Override
 	public void clear() throws CacheException {
-		logger.debug("从redis中删除所有元素");
-		try {
-            cache.flushDB();
-        } catch (Throwable t) {
-            throw new CacheException(t);
-        }
+		logger.debug("clear cache");
+        redisManager.flushDB();
 	}
 
 	@Override
 	public int size() {
-		try {
-			Long longSize = new Long(cache.dbSize());
-            return longSize.intValue();
-        } catch (Throwable t) {
-            throw new CacheException(t);
-        }
+		Long longSize = new Long(redisManager.dbSize());
+		return longSize.intValue();
 	}
 
 	@SuppressWarnings("unchecked")
 	@Override
 	public Set<K> keys() {
+		Set<byte[]> keys = null;
 		try {
-            Set<byte[]> keys = cache.keys(this.keyPrefix + "*");
-            if (CollectionUtils.isEmpty(keys)) {
-            	return Collections.emptySet();
-            }else{
-            	Set<K> newKeys = new HashSet<K>();
-            	for(byte[] key:keys){
-            		newKeys.add((K)key);
-            	}
-            	return newKeys;
-            }
-        } catch (Throwable t) {
-            throw new CacheException(t);
-        }
+			keys = redisManager.keys(keySerializer.serialize(this.keyPrefix + "*"));
+		} catch (SerializationException e) {
+			logger.error("get keys error", e);
+			return Collections.emptySet();
+		}
+
+		if (CollectionUtils.isEmpty(keys)) {
+			return Collections.emptySet();
+		}
+
+		Set<K> convertedKeys = new HashSet<K>();
+		for(byte[] key:keys){
+			try {
+				convertedKeys.add((K)keySerializer.deserialize(key));
+			} catch (SerializationException e) {
+				logger.error("deserialize keys error", e);
+			}
+		}
+		return convertedKeys;
 	}
 
 	@Override
 	public Collection<V> values() {
+		Set<byte[]> keys = null;
 		try {
-            Set<byte[]> keys = cache.keys(this.keyPrefix + "*");
-            if (!CollectionUtils.isEmpty(keys)) {
-                List<V> values = new ArrayList<V>(keys.size());
-                for (byte[] key : keys) {
-                    @SuppressWarnings("unchecked")
-					V value = get((K)key);
-                    if (value != null) {
-                        values.add(value);
-                    }
-                }
-                return Collections.unmodifiableList(values);
-            } else {
-                return Collections.emptyList();
-            }
-        } catch (Throwable t) {
-            throw new CacheException(t);
-        }
+			keys = redisManager.keys(keySerializer.serialize(this.keyPrefix + "*"));
+		} catch (SerializationException e) {
+			logger.error("get values error", e);
+			return Collections.emptySet();
+		}
+
+		if(CollectionUtils.isEmpty(keys)) {
+			return Collections.emptySet();
+		}
+
+		List<V> values = new ArrayList<V>(keys.size());
+		for (byte[] key : keys) {
+			V value = null;
+			try {
+				value = (V)valueSerializer.deserialize(redisManager.get(key));
+			} catch (SerializationException e) {
+				logger.error("deserialize values= error", e);
+			}
+			if (value != null) {
+				values.add(value);
+			}
+		}
+		return Collections.unmodifiableList(values);
 	}
 
+	public String getKeyPrefix() {
+		return keyPrefix;
+	}
+
+	public void setKeyPrefix(String keyPrefix) {
+		this.keyPrefix = keyPrefix;
+	}
 }
