@@ -1,28 +1,25 @@
 package org.crazycake.shiro;
 
-import java.io.Serializable;
-import java.util.Collection;
-import java.util.HashSet;
-import java.util.Set;
-
 import org.apache.shiro.session.Session;
 import org.apache.shiro.session.UnknownSessionException;
 import org.apache.shiro.session.mgt.eis.AbstractSessionDAO;
 import org.slf4j.Logger;
 import org.slf4j.LoggerFactory;
 
+import java.io.Serializable;
+import java.util.Collection;
+import java.util.HashSet;
+import java.util.Set;
+
 public class RedisSessionDAO extends AbstractSessionDAO {
 
 	private static Logger logger = LoggerFactory.getLogger(RedisSessionDAO.class);
-	/**
-	 * shiro-redis的session对象前缀
-	 */
+	private static final String DEFAULT_SESSION_KEY_PREFIX = "shiro:session:";
+
 	private RedisManager redisManager;
-	
-	/**
-	 * The Redis key prefix for the sessions 
-	 */
-	private String keyPrefix = "shiro_redis_session:";
+	private String keyPrefix = DEFAULT_SESSION_KEY_PREFIX;
+	private RedisSerializer keySerializer = new StringSerializer();
+	private RedisSerializer valueSerializer = new ObjectSerializer();
 	
 	@Override
 	public void update(Session session) throws UnknownSessionException {
@@ -37,11 +34,18 @@ public class RedisSessionDAO extends AbstractSessionDAO {
 	private void saveSession(Session session) throws UnknownSessionException{
 		if(session == null || session.getId() == null){
 			logger.error("session or session id is null");
-			return;
+			throw new UnknownSessionException("session or session id is null");
+		}
+		byte[] key;
+		byte[] value;
+		try {
+			key = keySerializer.serialize(getRedisSessionKey(session.getId()));
+			value = valueSerializer.serialize(session);
+		} catch (SerializationException e) {
+			logger.error("serialize session error. session id=" + session.getId());
+			throw new UnknownSessionException(e);
 		}
-		
-		byte[] key = getByteKey(session.getId());
-		byte[] value = SerializeUtils.serialize(session);
+
 		session.setTimeout(redisManager.getExpire()*1000);		
 		this.redisManager.set(key, value, redisManager.getExpire());
 	}
@@ -52,27 +56,36 @@ public class RedisSessionDAO extends AbstractSessionDAO {
 			logger.error("session or session id is null");
 			return;
 		}
-		redisManager.del(this.getByteKey(session.getId()));
-
+		try {
+			redisManager.del(keySerializer.serialize(getRedisSessionKey(session.getId())));
+		} catch (SerializationException e) {
+			logger.error("delete session error. session id=" + session.getId());
+		}
 	}
 
 	@Override
 	public Collection<Session> getActiveSessions() {
 		Set<Session> sessions = new HashSet<Session>();
-		
-		Set<byte[]> keys = redisManager.keys(this.keyPrefix + "*");
-		if(keys != null && keys.size()>0){
-			for(byte[] key:keys){
-				Session s = (Session)SerializeUtils.deserialize(redisManager.get(key));
-				sessions.add(s);
+		try {
+			Set<byte[]> keys = redisManager.keys(this.keySerializer.serialize(this.keyPrefix + "*"));
+			if(keys != null && keys.size()>0){
+				for(byte[] key:keys){
+					Session s = (Session)valueSerializer.deserialize(redisManager.get(key));
+					sessions.add(s);
+				}
 			}
+		} catch (SerializationException e) {
+			logger.error("get active sessions error.");
 		}
-		
 		return sessions;
 	}
 
 	@Override
 	protected Serializable doCreate(Session session) {
+		if(session == null){
+			logger.error("session is null");
+			throw new UnknownSessionException("session is null");
+		}
 		Serializable sessionId = this.generateSessionId(session);  
         this.assignSessionId(session, sessionId);
         this.saveSession(session);
@@ -82,22 +95,20 @@ public class RedisSessionDAO extends AbstractSessionDAO {
 	@Override
 	protected Session doReadSession(Serializable sessionId) {
 		if(sessionId == null){
-			logger.error("session id is null");
+			logger.warn("session id is null");
 			return null;
 		}
-		
-		Session s = (Session)SerializeUtils.deserialize(redisManager.get(this.getByteKey(sessionId)));
+		Session s = null;
+		try {
+			s = (Session)valueSerializer.deserialize(redisManager.get(keySerializer.serialize(getRedisSessionKey(sessionId))));
+		} catch (SerializationException e) {
+			logger.error("read session error. settionId=" + sessionId);
+		}
 		return s;
 	}
-	
-	/**
-	 * 获得byte[]型的key
-	 * @param key
-	 * @return
-	 */
-	private byte[] getByteKey(Serializable sessionId){
-		String preKey = this.keyPrefix + sessionId;
-		return preKey.getBytes();
+
+	private String getRedisSessionKey(Serializable sessionId) {
+		return this.keyPrefix + sessionId;
 	}
 
 	public RedisManager getRedisManager() {
@@ -106,30 +117,29 @@ public class RedisSessionDAO extends AbstractSessionDAO {
 
 	public void setRedisManager(RedisManager redisManager) {
 		this.redisManager = redisManager;
-		
-		/**
-		 * 初始化redisManager
-		 */
-		this.redisManager.init();
 	}
 
-	/**
-	 * Returns the Redis session keys
-	 * prefix.
-	 * @return The prefix
-	 */
 	public String getKeyPrefix() {
 		return keyPrefix;
 	}
 
-	/**
-	 * Sets the Redis sessions key 
-	 * prefix.
-	 * @param keyPrefix The prefix
-	 */
 	public void setKeyPrefix(String keyPrefix) {
 		this.keyPrefix = keyPrefix;
 	}
-	
-	
+
+	public RedisSerializer getKeySerializer() {
+		return keySerializer;
+	}
+
+	public void setKeySerializer(RedisSerializer keySerializer) {
+		this.keySerializer = keySerializer;
+	}
+
+	public RedisSerializer getValueSerializer() {
+		return valueSerializer;
+	}
+
+	public void setValueSerializer(RedisSerializer valueSerializer) {
+		this.valueSerializer = valueSerializer;
+	}
 }
