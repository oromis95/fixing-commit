@@ -6,7 +6,7 @@ public class PrincipalInstanceException extends RuntimeException  {
             + "So you need to defined an id field which you can get unique id to identify this principal. "
             + "For example, if you use UserInfo as Principal class, the id field maybe userId, userName, email, etc. "
             + "For example, getUserId(), getUserName(), getEmail(), etc.\n"
-            + "Default value is authCacheKey or id, that means your principal object has a method called \"getAuthCacheKey()\" or \"getId()\"";
+            + "Default value is \"id\", that means your principal object has a method called \"getId()\"";
 
     public PrincipalInstanceException(Class clazz, String idMethodName) {
         super(clazz + " must has getter for field: " +  idMethodName + "\n" + MESSAGE);
