@@ -32,7 +32,7 @@ public class RedisCacheManager implements CacheManager {
 		Cache cache = caches.get(name);
 		
 		if (cache == null) {
-			cache = new RedisCache<K, V>(redisManager, keySerializer, valueSerializer, keyPrefix);
+			cache = new RedisCache<K, V>(redisManager, keySerializer, valueSerializer, keyPrefix + name + ":");
 			caches.put(name, cache);
 		}
 		return cache;
