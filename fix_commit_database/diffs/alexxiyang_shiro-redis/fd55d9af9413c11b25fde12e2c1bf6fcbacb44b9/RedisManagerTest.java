@@ -73,11 +73,6 @@ public class RedisManagerTest {
         verify(jedis, times(1)).del(keySerializer.serialize("1"));
     }
 
-    @Test
-    public void testFlushDB() {
-        redisManager.flushDB();
-    }
-
     @Test
     public void testDbSize() {
         Long actualDbSize = redisManager.dbSize();
