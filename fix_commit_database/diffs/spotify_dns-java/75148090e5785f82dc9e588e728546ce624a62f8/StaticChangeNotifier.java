@@ -21,29 +21,28 @@ import com.google.common.collect.ImmutableSet;
 import java.util.Set;
 
 /**
- * An endpoint provider that provides a static set of endpoints.
+ * A {@link ChangeNotifier} that provides a static set of records.
  */
 class StaticChangeNotifier<T> extends AbstractChangeNotifier<T> {
 
-  private final Set<T> endpoints;
+  private final Set<T> records;
 
   /**
-   * Create a static endpoint provider.
+   * Create a static {@link ChangeNotifier}.
    *
-   * @param endpoints The endpoints to provide.
+   * @param records The records to provide.
    */
-  public StaticChangeNotifier(final Set<T> endpoints) {
-    this.endpoints = ImmutableSet.copyOf(endpoints);
+  public StaticChangeNotifier(final Set<T> records) {
+    this.records = ImmutableSet.copyOf(records);
   }
 
   @Override
   public Set<T> current() {
-    return endpoints;
+    return records;
   }
 
   @Override
   protected void closeImplementation() {
-    // empty implementation
   }
 }
 
