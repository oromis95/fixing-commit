@@ -47,6 +47,7 @@ class ServiceResolvingChangeNotifier<T> extends AbstractChangeNotifier<T>
   private final ErrorHandler errorHandler;
 
   private volatile Set<T> records = ImmutableSet.of();
+  private volatile boolean waitingForFirstEvent = true;
 
   private volatile boolean run = true;
 
@@ -100,9 +101,11 @@ class ServiceResolvingChangeNotifier<T> extends AbstractChangeNotifier<T>
         errorHandler.handle(fqdn, e);
       }
       log.error(e.getMessage(), e);
+      fireEmpty();
       return;
     } catch (Exception e) {
       log.error(e.getMessage(), e);
+      fireEmpty();
       return;
     }
 
@@ -116,10 +119,12 @@ class ServiceResolvingChangeNotifier<T> extends AbstractChangeNotifier<T>
       current = builder.build();
     } catch (Exception e) {
       log.error(e.getMessage(), e);
+      fireEmpty();
       return;
     }
 
-    if (!current.equals(records)) {
+    if (waitingForFirstEvent || !current.equals(records)) {
+      waitingForFirstEvent = false;
       final ChangeNotification<T> changeNotification =
           newChangeNotification(current, records);
       records = current;
@@ -127,5 +132,12 @@ class ServiceResolvingChangeNotifier<T> extends AbstractChangeNotifier<T>
       fireRecordsUpdated(changeNotification);
     }
   }
+
+  private void fireEmpty() {
+    if (waitingForFirstEvent) {
+      waitingForFirstEvent = false;
+      fireRecordsUpdated(newChangeNotification(current(), current()));
+    }
+  }
 }
 
