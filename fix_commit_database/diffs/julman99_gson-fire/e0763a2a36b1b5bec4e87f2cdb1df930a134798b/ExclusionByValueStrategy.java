@@ -2,6 +2,6 @@ package io.gsonfire.gson;
 
 public interface ExclusionByValueStrategy<T> {
 
-    boolean	shouldSkipField(T fieldValue);
+    boolean shouldSkipField(T fieldValue);
 
 }
