@@ -4,6 +4,7 @@ import com.google.gson.Gson;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 import com.google.gson.TypeAdapter;
+import com.google.gson.internal.bind.JsonTreeReader;
 import com.google.gson.stream.JsonReader;
 import com.google.gson.stream.JsonWriter;
 import io.gsonfire.ClassConfig;
@@ -49,7 +50,7 @@ public final class FireTypeAdapter<T> extends TypeAdapter<T> {
         JsonElement json = new JsonParser().parse(in);
 
         runPreDeserialize(json);
-        T result = deserialize(json);
+        T result = deserialize(json, in.isLenient());
 
         //Run all the post deserializers
         if (classConfig.isHooksEnabled()) {
@@ -78,9 +79,11 @@ public final class FireTypeAdapter<T> extends TypeAdapter<T> {
         }
     }
 
-    private T deserialize(JsonElement json){
-        T result = originalTypeAdapter.fromJsonTree(json);
-        return result;
+    private T deserialize(JsonElement json, boolean lenient) throws IOException{
+        JsonReader jsonReader = new JsonTreeReader(json);
+        jsonReader.setLenient(lenient);
+        T deserialized = originalTypeAdapter.read(jsonReader);
+        return deserialized;
     }
 
 }
