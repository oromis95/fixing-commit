@@ -107,8 +107,8 @@ public class TimeoutService
 	/**
 	 * It is assumed that the passed handler will not execute for a long time.
 	 * 
-	 * @param runTime
-	 * @param handler
+	 * @param runTime runTime
+	 * @param handler handler
 	 * @return a TimeoutToken that can be used to cancel the timeout.
 	 */
 	public static final TimeoutToken addTimeoutHandler(long runTime, Runnable handler)
