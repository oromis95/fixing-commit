@@ -386,7 +386,7 @@ public class SCPClient
 	 *            Remote target directory. Use an empty string to specify the
 	 *            default directory.
 	 * 
-	 * @throws IOException
+	 * @throws IOException the io exception
 	 */
 	public void put(String localFile, String remoteTargetDirectory) throws IOException
 	{
@@ -403,7 +403,7 @@ public class SCPClient
 	 *            Remote target directory. Use an empty string to specify the
 	 *            default directory.
 	 * 
-	 * @throws IOException
+	 * @throws IOException the io exception
 	 */
 
 	public void put(String[] localFiles, String remoteTargetDirectory) throws IOException
@@ -422,7 +422,7 @@ public class SCPClient
 	 *            default directory.
 	 * @param mode
 	 *            a four digit string (e.g., 0644, see "man chmod", "man open")
-	 * @throws IOException
+	 * @throws IOException the io exception
 	 */
 	public void put(String localFile, String remoteTargetDirectory, String mode) throws IOException
 	{
@@ -443,7 +443,7 @@ public class SCPClient
 	 *            default directory.
 	 * @param mode
 	 *            a four digit string (e.g., 0644, see "man chmod", "man open")
-	 * @throws IOException
+	 * @throws IOException the io exception
 	 */
 	public void put(String localFile, String remoteFileName, String remoteTargetDirectory, String mode)
 			throws IOException
@@ -463,7 +463,7 @@ public class SCPClient
 	 * @param remoteTargetDirectory
 	 *            Remote target directory. Use an empty string to specify the
 	 *            default directory.
-	 * @throws IOException
+	 * @throws IOException the io exception
 	 */
 
 	public void put(byte[] data, String remoteFileName, String remoteTargetDirectory) throws IOException
@@ -486,7 +486,7 @@ public class SCPClient
 	 *            default directory.
 	 * @param mode
 	 *            a four digit string (e.g., 0644, see "man chmod", "man open")
-	 * @throws IOException
+	 * @throws IOException the io exception
 	 */
 	public void put(byte[] data, String remoteFileName, String remoteTargetDirectory, String mode) throws IOException
 	{
@@ -535,7 +535,7 @@ public class SCPClient
 	 *            default directory.
 	 * @param mode
 	 *            a four digit string (e.g., 0644, see "man chmod", "man open")
-	 * @throws IOException
+	 * @throws IOException the io exception
 	 */
 	public void put(String[] localFiles, String remoteTargetDirectory, String mode) throws IOException
 	{
@@ -601,7 +601,7 @@ public class SCPClient
 	 * @param localTargetDirectory
 	 *            Local directory to put the downloaded file.
 	 * 
-	 * @throws IOException
+	 * @throws IOException the io exception
 	 */
 	public void get(String remoteFile, String localTargetDirectory) throws IOException
 	{
@@ -618,7 +618,7 @@ public class SCPClient
 	 *            Path and name of the remote file.
 	 * @param target
 	 *            OutputStream where the contents of the file will be sent to.
-	 * @throws IOException
+	 * @throws IOException the io exception
 	 */
 	public void get(String remoteFile, OutputStream target) throws IOException
 	{
@@ -678,7 +678,7 @@ public class SCPClient
 	 * @param localTargetDirectory
 	 *            Local directory to put the downloaded files.
 	 * 
-	 * @throws IOException
+	 * @throws IOException the io exception
 	 */
 	public void get(String remoteFiles[], String localTargetDirectory) throws IOException
 	{
