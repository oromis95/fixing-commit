@@ -21,7 +21,7 @@ import java.util.concurrent.TimeUnit;
 
 /**
  * AuthenticationManager.
- * 
+ *
  * @author Christian Plattner, plattner@trilead.com
  * @version $Id: AuthenticationManager.java,v 1.1 2007/10/15 12:49:57 cplattne Exp $
  */
