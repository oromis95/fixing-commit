@@ -52,6 +52,7 @@ public class KnownHosts
 	public static final int HOSTKEY_IS_OK = 0;
 	public static final int HOSTKEY_IS_NEW = 1;
 	public static final int HOSTKEY_HAS_CHANGED = 2;
+    private static final SecureRandom SECURE_RANDOM = RandomFactory.create();
 
 	private class KnownHostsEntry
 	{
@@ -151,7 +152,7 @@ public class KnownHosts
 
 		byte[] salt = new byte[sha1.getDigestLength()];
 
-		new SecureRandom().nextBytes(salt);
+		SECURE_RANDOM.nextBytes(salt);
 
 		byte[] hash = hmacSha1Hash(salt, hostname);
 
