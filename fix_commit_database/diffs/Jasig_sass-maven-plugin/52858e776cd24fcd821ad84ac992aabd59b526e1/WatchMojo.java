@@ -23,6 +23,7 @@ import javax.script.ScriptEngine;
 import javax.script.ScriptEngineManager;
 import javax.script.ScriptException;
 
+import org.apache.commons.io.FilenameUtils;
 import org.apache.maven.plugin.MojoExecutionException;
 import org.apache.maven.plugin.MojoFailureException;
 import org.apache.maven.plugin.logging.Log;
@@ -62,8 +63,8 @@ public class WatchMojo extends AbstractSassMojo {
 
     
     protected String buildSassScript() throws MojoExecutionException {
-        this.sassOptions.put("template_location", "'" + resources.get(0).source.getDirectory() + "'");
-        this.sassOptions.put("css_location", "'" + resources.get(0).destination + "'");
+        this.sassOptions.put("template_location", "'" + FilenameUtils.separatorsToUnix(resources.get(0).source.getDirectory()) + "'");
+        this.sassOptions.put("css_location", "'" + FilenameUtils.separatorsToUnix(resources.get(0).destination.toString()) + "'");
         
         final StringBuilder sassScript = new StringBuilder();
         buildBasicSASSScript(sassScript);		
