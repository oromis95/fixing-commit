@@ -40,6 +40,23 @@ public abstract class AbstractSassMojo extends AbstractMojo {
 
     /**
      * Sources for compilation with their destination directory containing SASS files.
+     * Example configuration
+     * <pre>
+     * &lt;resources>
+     *   &lt;resource>
+     *     &lt;source>
+     *       &lt;directory>${basedir}/src/main/resources/css&lt;/directory>
+     *       &lt;includes>
+     *         &lt;include>&#42;&#42;&lt;/include>
+     *       &lt;/includes>
+     *       &lt;excludes>
+     *         &lt;exclude>&#42;&#42;/watch-settings&lt;/exclude>
+     *       &lt;/excludes>
+     *     &lt;/source>
+     *     &lt;destination>${project.build.directory}/css&lt;/destination>
+     *   &lt;/resource>
+     * &lt;/resources>
+     * </pre>
      *
      * @parameter
      * @required
