@@ -19,19 +19,15 @@
 package org.jasig.maven.plugin.sass;
 
 import java.io.File;
-import java.io.IOException;
-import java.util.Arrays;
 import java.util.HashMap;
 import java.util.Iterator;
-import java.util.LinkedHashSet;
+import java.util.List;
 import java.util.Map;
 import java.util.Map.Entry;
-import java.util.Set;
 
 import org.apache.maven.plugin.AbstractMojo;
 import org.apache.maven.plugin.MojoExecutionException;
 import org.apache.maven.plugin.logging.Log;
-import org.codehaus.plexus.util.DirectoryScanner;
 
 import com.google.common.collect.ImmutableMap;
 
@@ -40,43 +36,27 @@ import com.google.common.collect.ImmutableMap;
  * 
  */
 public abstract class AbstractSassMojo extends AbstractMojo {
+
     /**
-     * Directory containing SASS files, defaults to the Maven Web application sources directory (src/main/webapp)
-     *
-     * @parameter default-value="${basedir}/src/main/webapp" 
-     * @required
-     */
-    protected File sassSourceDirectory;
-    
-    /**
-     * Defines output directory relative to 
-     *
-     * @parameter default-value=".."
-     */
-    protected String relativeOutputDirectory;
-    
-    /**
-     * Defines files in the source directories to include (none by default), recommended to be
-     * set in favor of skinConfigurationFile
-     * 
-     * Defaults to: "**&#47;scss"
-     *
-     * @parameter
-     */
-    protected String[] includes = new String[] { "**/scss" };
- 
-    /**
-     * Defines which of the included files in the source directories to exclude (none by default).
+     * Sources for compilation with their destination directory containing SASS files
      *
      * @parameter
+     * @required
      */
-    protected String[] excludes;
+    protected List<Resource> resources;
     
     /**
      * @parameter expression="${encoding}" default-value="${project.build.directory}
      * @required
      */
     protected File buildDirectory;
+
+    /**
+     * Fail the build if errors occur during compilation of sass/scss templates.
+     *
+     * @parameter default-value="true"
+     */
+    protected boolean failOnError;
     
     /**
      * Defines options for Sass::Plugin.options. See http://sass-lang.com/docs/yardoc/file.SASS_REFERENCE.html#options
@@ -99,14 +79,18 @@ public abstract class AbstractSassMojo extends AbstractMojo {
             "style", ":expanded"));
 
     
-    protected void buildSassOptions(final StringBuilder sassScript) throws MojoExecutionException {
+    protected void buildBasicSASSScript(final StringBuilder sassScript) throws MojoExecutionException {
+        final Log log = this.getLog();
+        
         sassScript.append("require 'rubygems'\n");
         sassScript.append("require 'sass/plugin'\n");
+        sassScript.append("require 'java'\n");
+        
         sassScript.append("Sass::Plugin.options.merge!(\n");
         
         //If not explicitly set place the cache location in the target dir
         if (!this.sassOptions.containsKey("cache_location")) {
-            final File sassCacheDir = newCanonicalFile(this.buildDirectory, "sass_cache");
+            final File sassCacheDir = new File(this.buildDirectory, "sass_cache");
             final String sassCacheDirStr = sassCacheDir.toString();
             this.sassOptions.put("cache_location", "'" + escapePath(sassCacheDirStr) + "'");
         }
@@ -123,34 +107,26 @@ public abstract class AbstractSassMojo extends AbstractMojo {
             sassScript.append("\n");
         }
         sassScript.append(")\n");
-    }
-
-    protected File newCanonicalFile(final File parent, final String child) throws MojoExecutionException {
-        final File f = new File(parent, child);
-        try {
-            return f.getCanonicalFile();
-        }
-        catch (final IOException e) {
-            throw new MojoExecutionException("Failed to create canonical File for: " + f, e);
-        }
-    }
+        
+        // set up compilation error reporting
+        sassScript.append("java_import ");
+        sassScript.append(CompilationErrors.class.getName());
+        sassScript.append("\n");
+        sassScript.append("$compilation_errors = CompilationErrors.new\n");
+        sassScript.append("Sass::Plugin.on_compilation_error {|error, template, css| $compilation_errors.add(template, error.message) }\n");
 
-    protected Set<String> findSassDirs() {
-        final Log log = this.getLog();
-        log.debug("Looking in " + this.sassSourceDirectory + " for dirs that match " + Arrays.toString(this.includes) + " but not " + Arrays.toString(this.excludes));
+        //Add the SASS template locations
+        for (Resource source : resources) {
+        	for (Entry<String, String> entry : source.getDirectoriesAndDestinations().entrySet()) {
+        		log.info("Queing SASS Template for compile: " + entry.getKey() + " => " + entry.getValue());
 
-        final DirectoryScanner directoryScanner = new DirectoryScanner();
-        directoryScanner.setIncludes(this.includes);
-        directoryScanner.setExcludes(this.excludes);
-        directoryScanner.setBasedir(this.sassSourceDirectory);
-        directoryScanner.scan();
-        
-        final Set<String> sassDirectories = new LinkedHashSet<String>();
-        for (final String dirName : directoryScanner.getIncludedDirectories()) {
-            sassDirectories.add(dirName);
-        }  
-        
-        return sassDirectories;
+    		    sassScript.append("Sass::Plugin.add_template_location('")
+    		    	.append(entry.getKey())
+    		    	.append("', '")
+    		    	.append(entry.getValue())
+    		    	.append("')\n");
+        	}
+		}
     }
 
     /**
