@@ -1,12 +1,18 @@
 package com.csforge.sstable;
 
 import com.google.common.base.Strings;
+import org.apache.cassandra.config.Config;
 import org.apache.cassandra.hints.HintsTool;
 
 import java.io.File;
 import java.util.Arrays;
 
 public class Driver {
+
+    static {
+        Config.setClientMode(true);
+    }
+
     public static void main(String ... args) {
         if (args.length == 0) {
             printCommands();
