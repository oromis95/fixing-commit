@@ -4,6 +4,9 @@ import java.io.IOException;
 import java.io.PrintWriter;
 import java.nio.file.Files;
 import java.nio.file.Paths;
+import java.util.Arrays;
+import java.util.HashSet;
+import java.util.List;
 
 import com.csforge.reader.CassandraReader;
 import org.apache.cassandra.config.Config;
@@ -30,7 +33,7 @@ public class SSTable2Json {
         if (DatabaseDescriptor.getPartitioner() == null)
             DatabaseDescriptor.setPartitionerUnsafe(Murmur3Partitioner.instance);
 
-        Option partitionKey = new Option(PARTITION_KEY_OPTION, true, "Partition Key");
+        Option partitionKey = new Option(PARTITION_KEY_OPTION, true, "Partition Keys to be included");
         partitionKey.setArgs(Option.UNLIMITED_VALUES);
 
         Option excludeKey = new Option(EXCLUDE_KEY_OPTION, true, "Excluded Partition Key");
@@ -38,7 +41,7 @@ public class SSTable2Json {
 
         Option enumerateKeys = new Option(ENUMERATE_KEYS_OPTION, false, "Enumerate keys only");
 
-        Option cqlCreate = new Option(CREATE_OPTION, false, "file containing \"CREATE TABLE...\" for the sstables schema");
+        Option cqlCreate = new Option(CREATE_OPTION, true, "file containing \"CREATE TABLE...\" for the sstables schema");
 
         options.addOption(partitionKey);
         options.addOption(excludeKey);
@@ -68,7 +71,8 @@ public class SSTable2Json {
 
         String sstablePath = cmd.getArgs()[0];
         String[] keys = cmd.getOptionValues(PARTITION_KEY_OPTION);
-        String[] excludes = cmd.getOptionValues(EXCLUDE_KEY_OPTION);
+        List<String> excludes = Arrays.asList(cmd.getOptionValues(EXCLUDE_KEY_OPTION) == null?
+                new String[0] : cmd.getOptionValues(EXCLUDE_KEY_OPTION));
         boolean enumerateKeysOnly = cmd.hasOption(ENUMERATE_KEYS_OPTION);
         String create = cmd.getOptionValue(CREATE_OPTION);
 
@@ -76,7 +80,7 @@ public class SSTable2Json {
             String cql = new String(Files.readAllBytes(Paths.get(create)));
             CassandraReader reader = new CassandraReader(cql);
 
-            reader.readSSTable(sstablePath, null);
+            reader.readSSTable(sstablePath, new HashSet<String>(excludes));
         } catch (IOException e) {
             e.printStackTrace();
         }
