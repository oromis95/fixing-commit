@@ -158,10 +158,11 @@ public class Query {
         int now = FBUtilities.nowInSeconds();
 
         UnfilteredPartitionIterator ret = sstable.getScanner(queriedColumns, range, false);
-        ret = restrictions.getRowFilter(null, OPTIONS).filter(ret, now);
         if (statement.limit != null && !selection.isAggregate()) {
             int limit = Integer.parseInt(statement.limit.getText());
-            ret = DataLimits.cqlLimits(limit).filter(sstable.getScanner(), now);
+            ret = DataLimits.cqlLimits(limit).filter(ret, now);
+        } else {
+            ret = restrictions.getRowFilter(null, OPTIONS).filter(ret, now);
         }
         return ret;
     }
