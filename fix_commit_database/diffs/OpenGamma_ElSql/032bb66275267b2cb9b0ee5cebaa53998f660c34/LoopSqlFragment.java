@@ -5,8 +5,6 @@
  */
 package com.opengamma.elsql;
 
-import org.springframework.jdbc.core.namedparam.SqlParameterSource;
-
 /**
  * Representation of WHERE.
  * <p>
@@ -30,9 +28,9 @@ final class LoopSqlFragment extends ContainerSqlFragment {
 
   //-------------------------------------------------------------------------
   @Override
-  protected void toSQL(StringBuilder buf, ElSqlBundle bundle, SqlParameterSource paramSource, int loopIndex) {
+  protected void toSQL(StringBuilder buf, ElSqlBundle bundle, SqlParams params, int loopIndex) {
     // find loop size
-    Object sizeObj = paramSource.getValue(_sizeVariable);
+    Object sizeObj = params.get(_sizeVariable);
     int size;
     if (sizeObj instanceof Number) {
       size = ((Number) sizeObj).intValue();
@@ -45,7 +43,7 @@ final class LoopSqlFragment extends ContainerSqlFragment {
     for (int i = 0; i < size; i++) {
       // index
       StringBuilder part = new StringBuilder();
-      super.toSQL(part, bundle, paramSource, i);
+      super.toSQL(part, bundle, params, i);
       int joinIndex = part.indexOf("@LOOPJOIN ");
       if (joinIndex >= 0) {
         if (i >= (size - 1)) {
