@@ -18,7 +18,7 @@ abstract class SqlFragment {
    * @param params  the SQL parameters, not null
    * @param loopIndex  the current loopIndex
    */
-  abstract void toSQL(StringBuilder buf, SqlFragments fragments, SqlParams params, int loopIndex);
+  abstract void toSQL(StringBuilder buf, SqlFragments fragments, SqlParams params, int[] loopIndex);
 
   /**
    * Applies the loop index to the string.
@@ -27,8 +27,20 @@ abstract class SqlFragment {
    * @param loopIndex  the loop index
    * @return the applied text, not null
    */
-  String applyLoopIndex(String text, int loopIndex) {
-    return text.replace("@LOOPINDEX", Integer.toString(loopIndex));
+  String applyLoopIndex(String text, int[] loopIndex) {
+    String result = text;
+    switch (loopIndex.length) {
+      case 4:
+        result = result.replace("@LOOPINDEX3", Integer.toString(loopIndex[3]));  // fall through
+      case 3:
+        result = result.replace("@LOOPINDEX2", Integer.toString(loopIndex[2]));  // fall through
+      case 2:
+        result = result.replace("@LOOPINDEX1", Integer.toString(loopIndex[1]));  // fall through
+      case 1:
+      default:
+        result = result.replace("@LOOPINDEX", Integer.toString(loopIndex[loopIndex.length - 1]));
+    }
+    return result;
   }
 
 }
