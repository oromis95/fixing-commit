@@ -778,6 +778,17 @@ public class ElSqlBundleTest {
 
   //-------------------------------------------------------------------------
   public void test_value() {
+    List<String> lines = Arrays.asList(
+        "@NAME(Test1)",
+        "  SELECT * FROM @VALUE(:var) WHERE true"
+    );
+    ElSqlBundle bundle = ElSqlBundle.parse(lines);
+    MapSqlParameterSource source = new MapSqlParameterSource("var", "mytable");
+    String sql1 = bundle.getSql("Test1", source);
+    assertEquals("SELECT * FROM mytable WHERE true ", sql1);
+  }
+
+  public void test_value_followedByComma() {
     List<String> lines = Arrays.asList(
         "@NAME(Test1)",
         "  SELECT * FROM @VALUE(:var), vax"
@@ -788,6 +799,17 @@ public class ElSqlBundleTest {
     assertEquals("SELECT * FROM mytable, vax ", sql1);
   }
 
+  public void test_value_insertContainsComma() {
+    List<String> lines = Arrays.asList(
+        "@NAME(Test1)",
+        "  SELECT * FROM base @VALUE(:additionaltables)"
+    );
+    ElSqlBundle bundle = ElSqlBundle.parse(lines);
+    MapSqlParameterSource source = new MapSqlParameterSource("additionaltables", ", mytable");
+    String sql1 = bundle.getSql("Test1", source);
+    assertEquals("SELECT * FROM base , mytable ", sql1);
+  }
+
   public void test_value_null() {
     List<String> lines = Arrays.asList(
         "@NAME(Test1)",
