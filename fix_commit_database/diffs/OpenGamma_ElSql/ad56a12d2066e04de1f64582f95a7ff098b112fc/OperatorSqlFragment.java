@@ -21,13 +21,7 @@ abstract class OperatorSqlFragment extends ContainerSqlFragment {
    * @param variable  the variable to base the Operator on, not null
    */
   OperatorSqlFragment(String variable) {
-    if (variable == null) {
-      throw new IllegalArgumentException("Variable must be specified");
-    }
-    if (variable.startsWith(":") == false || variable.length() < 2) {
-      throw new IllegalArgumentException("Argument is not a variable (starting with a colon)");
-    }
-    _variable = variable.substring(1);
+    _variable = extractVariableName(variable);
   }
   
   //-------------------------------------------------------------------------
