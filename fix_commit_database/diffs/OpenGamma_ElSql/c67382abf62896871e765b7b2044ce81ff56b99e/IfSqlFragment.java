@@ -24,9 +24,9 @@ final class IfSqlFragment extends ConditionalSqlFragment {
 
   //-------------------------------------------------------------------------
   @Override
-  protected void toSQL(StringBuilder buf, ElSqlBundle bundle, SqlParams params, int loopIndex) {
+  void toSQL(StringBuilder buf, SqlFragments fragments, SqlParams params, int loopIndex) {
     if (isMatch(params, loopIndex)) {
-      super.toSQL(buf, bundle, params, loopIndex);
+      super.toSQL(buf, fragments, params, loopIndex);
     }
   }
 
