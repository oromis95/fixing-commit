@@ -23,19 +23,19 @@ final class LikeSqlFragment extends OperatorSqlFragment {
 
   //-------------------------------------------------------------------------
   @Override
-  protected void toSQL(StringBuilder buf, ElSqlBundle bundle, SqlParams params, int loopIndex) {
+  void toSQL(StringBuilder buf, SqlFragments fragments, SqlParams params, int loopIndex) {
     String var = applyLoopIndex(_variable, loopIndex);
     Object val = params.get(var);
     if (val == null) {
       buf.append("IS NULL ");
     } else {
-      if (bundle.getConfig().isLikeWildcard(val.toString())) {
+      if (fragments.getConfig().isLikeWildcard(val.toString())) {
         buf.append("LIKE ");
-        super.toSQL(buf, bundle, params, loopIndex);
-        buf.append(bundle.getConfig().getLikeSuffix());
+        super.toSQL(buf, fragments, params, loopIndex);
+        buf.append(fragments.getConfig().getLikeSuffix());
       } else {
         buf.append("= ");
-        super.toSQL(buf, bundle, params, loopIndex);
+        super.toSQL(buf, fragments, params, loopIndex);
       }
     }
   }
