@@ -56,7 +56,7 @@ abstract class ConditionalSqlFragment extends ContainerSqlFragment {
   }
 
   //-------------------------------------------------------------------------
-  protected boolean isMatch(SqlParams params, int loopIndex) {
+  boolean isMatch(SqlParams params, int loopIndex) {
     String var = applyLoopIndex(_variable, loopIndex);
     if (params.contains(var) == false) {
       return false;
@@ -74,7 +74,7 @@ abstract class ConditionalSqlFragment extends ContainerSqlFragment {
     return true;
   }
 
-  protected boolean endsWith(StringBuilder buf, String match) {
+  boolean endsWith(StringBuilder buf, String match) {
     String str = (buf.length() >= match.length() ? buf.substring(buf.length() - match.length()) : "");
     return str.equals(match);
   }
