@@ -18,7 +18,19 @@ abstract class SqlFragment {
    * @param buf  the buffer to append to, not null
    * @param bundle  the elsql bundle for context, not null
    * @param paramSource  the SQL parameters, not null
+   * @param loopIndex  the current loopIndex
    */
-  protected abstract void toSQL(StringBuilder buf, ElSqlBundle bundle, SqlParameterSource paramSource);
+  protected abstract void toSQL(StringBuilder buf, ElSqlBundle bundle, SqlParameterSource paramSource, int loopIndex);
+
+  /**
+   * Applies the loop index to the string.
+   * 
+   * @param text  the text to apply to, not null
+   * @param loopIndex  the loop index
+   * @return the applied text, not null
+   */
+  protected String applyLoopIndex(String text, int loopIndex) {
+    return text.replace("@LOOPINDEX", Integer.toString(loopIndex));
+  }
 
 }
