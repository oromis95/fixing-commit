@@ -34,28 +34,19 @@ final class LikeSqlFragment extends ContainerSqlFragment {
     _variable = variable.substring(1);
   }
 
-  //-------------------------------------------------------------------------
-  /**
-   * Gets the variable.
-   * 
-   * @return the variable, not null
-   */
-  String getVariable() {
-    return _variable;
-  }
-
   //-------------------------------------------------------------------------
   @Override
-  protected void toSQL(StringBuilder buf, ElSqlBundle bundle, SqlParameterSource paramSource) {
-    Object val = paramSource.getValue(_variable);
+  protected void toSQL(StringBuilder buf, ElSqlBundle bundle, SqlParameterSource paramSource, int loopIndex) {
+    String var = applyLoopIndex(_variable, loopIndex);
+    Object val = paramSource.getValue(var);
     String value = (val == null ? "" : val.toString());
     if (bundle.getConfig().isLikeWildcard(value)) {
       buf.append("LIKE ");
-      super.toSQL(buf, bundle, paramSource);
+      super.toSQL(buf, bundle, paramSource, loopIndex);
       buf.append(bundle.getConfig().getLikeSuffix());
     } else {
       buf.append("= ");
-      super.toSQL(buf, bundle, paramSource);
+      super.toSQL(buf, bundle, paramSource, loopIndex);
     }
   }
 
