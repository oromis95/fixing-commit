@@ -190,8 +190,8 @@ public class ElSqlBundleTest {
     String sql1 = bundle.getSql("Test1", new MapSqlParameterSource("var", "va_"));
     assertEquals("SELECT * FROM foo WHERE var LIKE :var ", sql1);
   }
-  
-  public void test_like_isnull() {
+
+  public void test_like_isNull() {
     List<String> lines = Arrays.asList(
         "@NAME(Test1)",
         "  SELECT * FROM foo",
@@ -200,7 +200,7 @@ public class ElSqlBundleTest {
     ElSqlBundle bundle = ElSqlBundle.parse(lines);
     String sql1 = bundle.getSql("Test1", new MapSqlParameterSource("var", null));
     assertEquals("SELECT * FROM foo WHERE var IS NULL ", sql1);
-  }  
+  }
 
   public void test_likeEndLike_equals() {
     List<String> lines = Arrays.asList(
@@ -235,7 +235,7 @@ public class ElSqlBundleTest {
     String sql1 = bundle.getSql("Test1", new MapSqlParameterSource("var", "va%l"));
     assertEquals("SELECT * FROM foo WHERE (var LIKE :var ESCAPE '\\' ) ", sql1);
   }
-  
+
   //-------------------------------------------------------------------------
   public void test_equals_equals()  {
     List<String> lines = Arrays.asList(
@@ -247,8 +247,8 @@ public class ElSqlBundleTest {
     String sql1 = bundle.getSql("Test1", new MapSqlParameterSource("var", "val"));
     assertEquals("SELECT * FROM foo WHERE var = :var ", sql1);   
   }
-  
-  public void test_equals_isnull() {
+
+  public void test_equals_isNull() {
     List<String> lines = Arrays.asList(
         "@NAME(Test1)",
         "  SELECT * FROM foo",
@@ -258,7 +258,7 @@ public class ElSqlBundleTest {
     String sql1 = bundle.getSql("Test1", new MapSqlParameterSource("var", null));
     assertEquals("SELECT * FROM foo WHERE var IS NULL ", sql1);       
   }
-  
+
   public void test_equalsEndEquals_equals() {
     List<String> lines = Arrays.asList(
         "@NAME(Test1)",
@@ -269,8 +269,8 @@ public class ElSqlBundleTest {
     String sql1 = bundle.getSql("Test1", new MapSqlParameterSource("var", "val"));
     assertEquals("SELECT * FROM foo WHERE (var = :var ) ", sql1);
   }
-  
-  public void test_equalsEndEquals_isnull() {
+
+  public void test_equalsEndEquals_isNull() {
     List<String> lines = Arrays.asList(
         "@NAME(Test1)",
         "  SELECT * FROM foo",
