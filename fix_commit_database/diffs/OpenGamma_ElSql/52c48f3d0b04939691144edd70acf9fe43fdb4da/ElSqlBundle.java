@@ -5,10 +5,8 @@
  */
 package com.opengamma.elsql;
 
-import java.io.BufferedReader;
 import java.io.IOException;
-import java.io.InputStream;
-import java.io.InputStreamReader;
+import java.net.URL;
 import java.util.ArrayList;
 import java.util.List;
 
@@ -17,11 +15,15 @@ import org.springframework.core.io.Resource;
 import org.springframework.jdbc.core.namedparam.SqlParameterSource;
 
 /**
- * Provides access to a bundle of elsql formatted SQL, integrated with the Spring framework.
+ * Entry point integrated with the Spring framework, providing access to a bundle of elsql formatted SQL.
  * <p>
  * The bundle encapsulates the SQL needed for a particular feature.
  * This will typically correspond to a data access object, or set of related tables.
  * <p>
+ * This class uses features from the Spring framework.
+ * The same functionality is available from the {@link ElSql} class, which does
+ * not depend on Spring.
+ * <p>
  * This class is immutable and thread-safe.
  */
 public final class ElSqlBundle {
@@ -92,44 +94,20 @@ public final class ElSqlBundle {
     List<List<String>> files = new ArrayList<List<String>>();
     for (Resource resource : resources) {
       if (resource.exists()) {
-        List<String> lines = loadResource(resource);
+        URL url;
+        try {
+          url = resource.getURL();
+        } catch (IOException ex) {
+          throw new RuntimeException(ex);
+        }
+        List<String> lines = SqlFragments.loadResource(url);
         files.add(lines);
       }
     }
     return new ElSqlBundle(SqlFragments.parse(files, config));
   }
 
-  // package scoped for testing
-  static ElSqlBundle parse(List<String> lines) {
-    ArrayList<List<String>> files = new ArrayList<List<String>>();
-    files.add(lines);
-    return new ElSqlBundle(SqlFragments.parse(files, ElSqlConfig.DEFAULT));
-  }
-
-  private static List<String> loadResource(Resource resource) {
-    InputStream in = null;
-    try {
-      in = resource.getInputStream();
-      BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
-      List<String> list = new ArrayList<String>();
-      String line = reader.readLine();
-      while (line != null) {
-          list.add(line);
-          line = reader.readLine();
-      }
-      return list;
-    } catch (IOException ex) {
-      throw new RuntimeException(ex);
-    } finally {
-      try {
-        if (in != null) {
-          in.close();
-        }
-      } catch (IOException ignored) {
-      }
-    }
-  }
-
+  //-------------------------------------------------------------------------
   /**
    * Creates an instance.
    * 
