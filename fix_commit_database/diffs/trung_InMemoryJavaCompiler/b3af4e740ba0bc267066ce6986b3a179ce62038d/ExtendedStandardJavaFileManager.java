@@ -2,6 +2,9 @@ package org.mdkt.compiler;
 
 import java.io.FileNotFoundException;
 import java.io.IOException;
+import java.util.ArrayList;
+import java.util.Arrays;
+import java.util.List;
 
 import javax.tools.FileObject;
 import javax.tools.ForwardingJavaFileManager;
@@ -9,13 +12,13 @@ import javax.tools.JavaFileManager;
 import javax.tools.JavaFileObject;
 
 /**
- * Created by trung on 5/3/15.
- * Edited by turpid-monkey on 9/25/15, completed support for multiple compile units.
+ * Created by trung on 5/3/15. Edited by turpid-monkey on 9/25/15, completed
+ * support for multiple compile units.
  */
 public class ExtendedStandardJavaFileManager extends
 		ForwardingJavaFileManager<JavaFileManager> {
 
-	private CompiledCode[] compiledCode;
+	private List<CompiledCode> compiledCode = new ArrayList<CompiledCode>();
 	private DynamicClassLoader cl;
 
 	/**
@@ -26,23 +29,27 @@ public class ExtendedStandardJavaFileManager extends
 	 * @param cl
 	 */
 	protected ExtendedStandardJavaFileManager(JavaFileManager fileManager,
-			DynamicClassLoader cl, CompiledCode... compiledCode) {
+			DynamicClassLoader cl) {
 		super(fileManager);
-		this.compiledCode = compiledCode;
 		this.cl = cl;
-		for (CompiledCode code : compiledCode) {
-			this.cl.addCode(code);
-		}
 	}
 
 	@Override
-    public JavaFileObject getJavaFileForOutput(JavaFileManager.Location location, String className, JavaFileObject.Kind kind, FileObject sibling) throws IOException {
-    	for (CompiledCode code : compiledCode)
-    	{
-    		if (code.getClassName().equals(className)) return code;
-    	}
-    	throw new FileNotFoundException("Missing source code for class " + className );
-    }
+	public JavaFileObject getJavaFileForOutput(
+			JavaFileManager.Location location, String className,
+			JavaFileObject.Kind kind, FileObject sibling) throws IOException {
+
+		try {
+			CompiledCode innerClass = new CompiledCode(className);
+			compiledCode.add(innerClass);
+			cl.addCode(innerClass);
+			return innerClass;
+		} catch (Exception e) {
+			throw new RuntimeException(
+					"Error while creating in-memory output file for "
+							+ className, e);
+		}
+	}
 
 	@Override
 	public ClassLoader getClassLoader(JavaFileManager.Location location) {
