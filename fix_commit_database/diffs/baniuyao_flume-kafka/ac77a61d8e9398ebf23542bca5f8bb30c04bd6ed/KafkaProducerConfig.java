@@ -3,46 +3,49 @@ package com.vipshop.flume;
 import org.apache.flume.Context;
 
 public class KafkaProducerConfig {
-	public static String getGroupId(Context context) {
+	public static String getProducerType(Context context) {
 		return context.getString(KafkaProducerConstants.CONFIG_ZK_CONNECT);
 	}
-	public static String getSocketTimeoutMs(Context context) {
+	public static String getBrokerList(Context context) {
+		return context.getString(KafkaProducerConstants.CONFIG_ZK_CONNECT);
+	}
+	public static String getZkConnect(Context context) {
 		return context.getString(KafkaProducerConstants.CONFIG_ZK_CONNECT);
 	}
-	public static String getSocketBufferSize(Context context) {
+	public static String getBufferSize(Context context) {
 		return context.getString(KafkaProducerConstants.CONFIG_ZK_CONNECT);
 	}
-	public static String getFetchSize(Context context) {
+	public static String getConnectTimeoutMs(Context context) {
 		return context.getString(KafkaProducerConstants.CONFIG_ZK_CONNECT);
 	}
-	public static String getBackOffIncrementMs(Context context) {
+	public static String getSocketTimeoutMs(Context context) {
 		return context.getString(KafkaProducerConstants.CONFIG_ZK_CONNECT);
 	}
-	public static String getQueuedChunksMax(Context context) {
+	public static String getReconnectInterval(Context context) {
 		return context.getString(KafkaProducerConstants.CONFIG_ZK_CONNECT);
 	}
-	public static String getAutoCommitEnable(Context context) {
+	public static String getReconnectTimeIntervalMs(Context context) {
 		return context.getString(KafkaProducerConstants.CONFIG_ZK_CONNECT);
 	}
-	public static String getAutoCommitIntervalMs(Context context) {
+	public static String getMaxMessageSize(Context context) {
 		return context.getString(KafkaProducerConstants.CONFIG_ZK_CONNECT);
 	}
-	public static String getAutoOffsetReset(Context context) {
+	public static String getCompressionCodec(Context context) {
 		return context.getString(KafkaProducerConstants.CONFIG_ZK_CONNECT);
 	}
-	public static String getConsumerTimeoutMs(Context context) {
+	public static String getCompressedTopics(Context context) {
 		return context.getString(KafkaProducerConstants.CONFIG_ZK_CONNECT);
 	}
-	public static String getRebalanceRetriesMax(Context context) {
+	public static String getZkReadNumRetries(Context context) {
 		return context.getString(KafkaProducerConstants.CONFIG_ZK_CONNECT);
 	}
-	public static String getMirrorTopicsWhiteList(Context context) {
+	public static String getQueueTime(Context context) {
 		return context.getString(KafkaProducerConstants.CONFIG_ZK_CONNECT);
 	}
-	public static String getMirrorTopicsBlackList(Context context) {
+	public static String getQueueSize(Context context) {
 		return context.getString(KafkaProducerConstants.CONFIG_ZK_CONNECT);
 	}
-	public static String getMirrorConsumerNumThreads(Context context) {
+	public static String getBatchSize(Context context) {
 		return context.getString(KafkaProducerConstants.CONFIG_ZK_CONNECT);
 	}
 
