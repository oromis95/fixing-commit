@@ -19,7 +19,7 @@ public class Vehicle {
     // Max vehicle speed. Anything faster is considered noise.
     public static final double MAX_SPEED = 31.0;
     // Max time between two successive GPS fixes from a single vehicle. Anything longer is considered noise.
-    public static final int MAX_GPS_PAIR_DURATION = 20;
+    public static final int MAX_GPS_PAIR_DURATION = 200;
 
     public Fun.Tuple2<Integer, Integer> tile;
 
