@@ -162,18 +162,18 @@ class SettingsLight // light settings
 	private int nexAlg = 0;
 	private int maxAlg = ImmersiveProcess.algorithms;
 	
-	public void checkSettings(String uniqueid) throws Exception // setup default light settings if it doesn't have
+	public void checkSettings(HLight light) throws Exception // setup default light settings if it doesn't have
 	{
-		Preferences lprefs = Preferences.userRoot().node(prefs.absolutePath() + "/" + uniqueid);
-		if (prefs.get("active", null) == null)
+		Preferences lprefs = Preferences.userRoot().node(prefs.absolutePath() + "/" + light.uniqueid);
+		if (lprefs.get("active", null) == null)
 		{
 			lprefs.putBoolean("active", true);
 		}
-		if (prefs.get("bri", null) == null)
+		if (lprefs.get("bri", null) == null)
 		{
 			lprefs.putInt("bri", 100);
 		}
-		if (prefs.get("alg", null) == null)
+		if (lprefs.get("alg", null) == null)
 		{
 			lprefs.putInt("alg", nexAlg);
 			nexAlg++;
@@ -200,35 +200,35 @@ class SettingsLight // light settings
 		Debug.info("settings lights", settings);
 	}
 	
-	public void setBrightness(String uniqueid, int bri)
+	public void setBrightness(HLight light, int bri)
 	{
-		Preferences lprefs = Preferences.userRoot().node(prefs.absolutePath() + "/" + uniqueid);
+		Preferences lprefs = Preferences.userRoot().node(prefs.absolutePath() + "/" + light.uniqueid);
 		lprefs.putInt("bri", bri);
 	}
-	public void setActive(String uniqueid, boolean active)
+	public void setActive(HLight light, boolean active)
 	{
-		Preferences lprefs = Preferences.userRoot().node(prefs.absolutePath() + "/" + uniqueid);
+		Preferences lprefs = Preferences.userRoot().node(prefs.absolutePath() + "/" + light.uniqueid);
 		lprefs.putBoolean("active", active);
 	}
-	public void setAlgorithm(String uniqueid, int alg)
+	public void setAlgorithm(HLight light, int alg)
 	{
-		Preferences lprefs = Preferences.userRoot().node(prefs.absolutePath() + "/" + uniqueid);
+		Preferences lprefs = Preferences.userRoot().node(prefs.absolutePath() + "/" + light.uniqueid);
 		lprefs.putInt("alg", alg);
 	}
 	
-	public boolean getActive(String uniqueid)
+	public boolean getActive(HLight light)
 	{
-		Preferences lprefs = Preferences.userRoot().node(prefs.absolutePath() + "/" + uniqueid);
+		Preferences lprefs = Preferences.userRoot().node(prefs.absolutePath() + "/" + light.uniqueid);
 		return lprefs.getBoolean("active", true);
 	}
-	public int getAlgorithm(String uniqueid)
+	public int getAlgorithm(HLight light)
 	{
-		Preferences lprefs = Preferences.userRoot().node(prefs.absolutePath() + "/" + uniqueid);
+		Preferences lprefs = Preferences.userRoot().node(prefs.absolutePath() + "/" + light.uniqueid);
 		return lprefs.getInt("alg", -1);
 	}
-	public int getBrightness(String uniqueid)
+	public int getBrightness(HLight light)
 	{
-		Preferences lprefs = Preferences.userRoot().node(prefs.absolutePath() + "/" + uniqueid);
+		Preferences lprefs = Preferences.userRoot().node(prefs.absolutePath() + "/" + light.uniqueid);
 		return lprefs.getInt("bri", -1);
 	}
 }
