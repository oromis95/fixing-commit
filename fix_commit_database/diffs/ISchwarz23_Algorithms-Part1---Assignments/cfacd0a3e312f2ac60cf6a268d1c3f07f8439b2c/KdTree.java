@@ -35,20 +35,15 @@ public class KdTree {
         root = put(root, p, 0);
     }
 
-    private Node put(Node node, Point2D value, int level) {
-        if (node == null) return new Node(level, value, 1);
-
-        double key;
-        if (level % 2 == 0) {
-            key = value.x();
-        } else {
-            key = value.y();
+    private Node put(final Node node, final Point2D pointToInsert, final int level) {
+        if (node == null) {
+            return new Node(level, pointToInsert, 1);
         }
 
-        double cmp = key - node.key;
-        if (cmp < 0) node.left = put(node.left, value, ++level);
-        else if (cmp > 0) node.right = put(node.right, value, ++level);
-        else node.value = value;
+        double cmp = node.compare(pointToInsert);
+        if (cmp < 0) node.left = put(node.left, pointToInsert, level + 1);
+        else if (cmp > 0) node.right = put(node.right, pointToInsert, level + 1);
+        else if (!pointToInsert.equals(node.value)) node.right = put(node.right, pointToInsert, level + 1);
 
         node.n = 1 + size(node.left) + size(node.right);
         return node;
@@ -59,19 +54,13 @@ public class KdTree {
         return get(root, p, 0) != null;
     }
 
-    private Point2D get(Node node, Point2D value, int level) {
+    private Point2D get(Node node, Point2D searchedPoint, int level) {
         if (node == null) return null;
 
-        double key;
-        if (level % 2 == 0) {
-            key = value.x();
-        } else {
-            key = value.y();
-        }
-
-        double cmp = key - node.key;
-        if (cmp < 0) return get(node.left, value, ++level);
-        else if (cmp > 0) return get(node.right, value, ++level);
+        double cmp = node.compare(searchedPoint);
+        if (cmp < 0) return get(node.left, searchedPoint, ++level);
+        else if (cmp > 0) return get(node.right, searchedPoint, ++level);
+        else if (!searchedPoint.equals(node.value)) return get(node.right, searchedPoint, ++level);
         else return node.value;
     }
 
@@ -80,10 +69,8 @@ public class KdTree {
     }
 
     private void draw(Node node) {
-        if(node == null) return;
-
-        Point2D currentPoint = node.value;
-        StdDraw.point(currentPoint.x(), currentPoint.y());
+        if (node == null) return;
+        StdDraw.point(node.value.x(), node.value.y());
         draw(node.left);
         draw(node.right);
     }
@@ -104,14 +91,7 @@ public class KdTree {
             points.addAll(range(rect, node.right));
             return points;
         } else {
-            double key;
-            if (node.level % 2 == 0) {
-                key = currentPoint.x();
-            } else {
-                key = currentPoint.y();
-            }
-
-            double cmp = key - node.key;
+            double cmp = node.compare(currentPoint);
             if (cmp < 0) return range(rect, node.left);
             else if (cmp > 0) return range(rect, node.right);
             else {
@@ -133,18 +113,10 @@ public class KdTree {
         if (node == null) return null;
 
         Point2D currentPoint = node.value;
-        if (currentPoint.equals(queryPoint)) return currentPoint;
+        double currentDistance = currentPoint.distanceTo(queryPoint);
 
         Point2D nearestPoint;
-        double key;
-        if (node.level % 2 == 0) {
-            key = currentPoint.x();
-        } else {
-            key = currentPoint.y();
-        }
-
-        double currentDistance = currentPoint.distanceTo(queryPoint);
-        double cmp = key - node.key;
+        double cmp = node.compare(currentPoint);
         if (cmp < 0) {
             nearestPoint = nearest(queryPoint, node.left);
         } else {
@@ -157,19 +129,25 @@ public class KdTree {
         }
 
         if (nearestDistance > currentDistance) {
+            nearestDistance = currentDistance;
+            nearestPoint = currentPoint;
+        }
+
+        if (nearestDistance > cmp) {
+            Point2D foundPoint;
             if (cmp < 0) {
-                nearestPoint = nearest(queryPoint, node.right);
+                foundPoint = nearest(queryPoint, node.right);
             } else {
-                nearestPoint = nearest(queryPoint, node.left);
+                foundPoint = nearest(queryPoint, node.left);
             }
 
-            nearestDistance = 2;
-            if (nearestPoint != null) {
-                nearestDistance = nearestPoint.distanceTo(queryPoint);
+            double foundDistance = 2;
+            if (foundPoint != null) {
+                foundDistance = foundPoint.distanceTo(queryPoint);
             }
 
-            if (nearestDistance > currentDistance) {
-                nearestPoint = currentPoint;
+            if (nearestDistance > foundDistance) {
+                nearestPoint = foundPoint;
             }
         }
 
@@ -182,22 +160,25 @@ public class KdTree {
 
     private static class Node {
 
-        private double key;
         private Point2D value;
         private Node left, right;  // left and right subtrees
         private int n;             // number of nodes in subtree
         private int level;
 
         public Node(int level, Point2D value, int n) {
-            if (level % 2 == 0) {
-                key = value.x();
-            } else {
-                key = value.y();
-            }
             this.level = level;
             this.value = value;
             this.n = n;
         }
+
+        public double compare(Point2D point) {
+            if (level % 2 == 0) {
+                return point.x() - value.x();
+            } else {
+                return point.y() - value.y();
+            }
+        }
+
     }
 
 }
