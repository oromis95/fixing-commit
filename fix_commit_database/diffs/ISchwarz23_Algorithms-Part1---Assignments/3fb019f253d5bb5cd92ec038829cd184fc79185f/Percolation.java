@@ -9,6 +9,7 @@ import edu.princeton.cs.algs4.WeightedQuickUnionUF;
 public class Percolation {
 
     private final WeightedQuickUnionUF quickUnionStructure;
+    private final WeightedQuickUnionUF quickUnionStructureForIsFull;
 
     private int gridSize;
     private final boolean[][] grid;
@@ -25,6 +26,7 @@ public class Percolation {
         grid = new boolean[n][n];
 
         quickUnionStructure = new WeightedQuickUnionUF(n * n + 2);
+        quickUnionStructureForIsFull = new WeightedQuickUnionUF(n * n + 1);
 
         virtualTopSite = 0;
         virtualBottomSite = n * n + 1;
@@ -36,6 +38,7 @@ public class Percolation {
 
             if (i == 1) {
                 quickUnionStructure.union(virtualTopSite, fieldIndex);
+                quickUnionStructureForIsFull.union(virtualTopSite, fieldIndex);
             }
             if (i == gridSize) {
                 quickUnionStructure.union(virtualBottomSite, fieldIndex);
@@ -57,7 +60,7 @@ public class Percolation {
     public boolean isFull(int i, int j) {   // is site (row i, column j) connected to top?
         if (isOpen(i, j)) {
             int fieldIndex = getFieldIndexInQuickUnionStructure(i, j);
-            return quickUnionStructure.connected(virtualTopSite, fieldIndex);
+            return quickUnionStructureForIsFull.connected(virtualTopSite, fieldIndex);
         }
         return false;
     }
@@ -71,6 +74,7 @@ public class Percolation {
             if (isOpen(i, j)) {
                 int neighbourFieldIndex = getFieldIndexInQuickUnionStructure(i, j);
                 quickUnionStructure.union(neighbourFieldIndex, fieldIndex);
+                quickUnionStructureForIsFull.union(neighbourFieldIndex, fieldIndex);
             }
         } catch (IndexOutOfBoundsException e) {
             // don't connect field with field outside grid
