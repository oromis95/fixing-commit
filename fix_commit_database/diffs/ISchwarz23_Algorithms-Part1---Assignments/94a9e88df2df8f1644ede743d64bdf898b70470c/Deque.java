@@ -98,6 +98,13 @@ public class Deque<Item> implements Iterable<Item> {
         return oldLastItem.value;
     }
 
+
+    private static class InternalItem<Item> {
+        private Item value;
+        private InternalItem<Item> nextItem;
+        private InternalItem<Item> previousItem;
+    }
+
     @Override
     public Iterator<Item> iterator() {
         return new ForwardIterator();
@@ -129,10 +136,4 @@ public class Deque<Item> implements Iterable<Item> {
         }
     }
 
-    private static class InternalItem<I> {
-        I value;
-        InternalItem<I> nextItem;
-        InternalItem<I> previousItem;
-    }
-
 }
