@@ -4,10 +4,13 @@ import net.jodah.concurrentunit.Waiter;
 import org.junit.Test;
 import org.quartz.CronTrigger;
 import org.quartz.JobDetail;
+import org.quartz.SimpleTrigger;
+import org.quartz.TriggerBuilder;
 import org.quartz.impl.matchers.NameMatcher;
 
 import static net.joelinn.quartz.TestUtils.createCronTrigger;
 import static net.joelinn.quartz.TestUtils.createJob;
+import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
 
 /**
  * @author Joe Linn
@@ -35,4 +38,20 @@ public class SingleThreadedIntegrationTest extends BaseIntegrationTest {
         // wait for MisfireListener.triggerMisfired() to be called
         waiter.await(2500);
     }
+
+
+    @Test
+    public void testSingleExecution() throws Exception {
+        final String jobName = "oneJob";
+        JobDetail jobDetail = createJob(TestJob.class, jobName, "oneGroup");
+
+        SimpleTrigger trigger = TriggerBuilder.newTrigger().withSchedule(simpleSchedule().withRepeatCount(0).withIntervalInMilliseconds(200)).build();
+
+        Waiter waiter = new Waiter();
+        scheduler.getListenerManager().addTriggerListener(new CompleteListener(waiter), NameMatcher.triggerNameEquals(trigger.getKey().getName()));
+
+        scheduler.scheduleJob(jobDetail, trigger);
+
+        waiter.await(2000);
+    }
 }
