@@ -5,11 +5,8 @@ import net.jodah.concurrentunit.Waiter;
 import net.joelinn.quartz.jobstore.RedisJobStore;
 import org.junit.After;
 import org.junit.Before;
-import org.junit.Test;
 import org.quartz.*;
 import org.quartz.impl.StdSchedulerFactory;
-import org.quartz.impl.matchers.NameMatcher;
-import org.quartz.simpl.PropertySettingJobFactory;
 import org.slf4j.Logger;
 import org.slf4j.LoggerFactory;
 import redis.clients.jedis.Jedis;
@@ -18,21 +15,21 @@ import redis.clients.util.Pool;
 import redis.embedded.RedisServer;
 
 import java.util.Properties;
+import java.util.concurrent.atomic.AtomicInteger;
 
-import static net.joelinn.quartz.TestUtils.*;
-import static org.hamcrest.CoreMatchers.equalTo;
-import static org.hamcrest.MatcherAssert.assertThat;
+import static net.joelinn.quartz.TestUtils.getPort;
 
 /**
  * @author Joe Linn
- *         10/4/2016
+ *         12/4/2016
  */
-public class RedisJobStoreIntegrationTest {
-    private static final Logger log = LoggerFactory.getLogger(RedisJobStoreIntegrationTest.class);
+public abstract class BaseIntegrationTest {
+    private static final Logger log = LoggerFactory.getLogger(BaseIntegrationTest.class);
+
+    protected RedisServer redisServer;
+    protected Scheduler scheduler;
+    protected Pool<Jedis> jedisPool;
 
-    private RedisServer redisServer;
-    private Scheduler scheduler;
-    private Pool<Jedis> jedisPool;
 
     @Before
     public void setUp() throws Exception {
@@ -45,14 +42,20 @@ public class RedisJobStoreIntegrationTest {
 
         jedisPool = new JedisPool(host, port);
 
+
+        scheduler = new StdSchedulerFactory(schedulerConfig(host, port)).getScheduler();
+        scheduler.start();
+    }
+
+
+    protected Properties schedulerConfig(String host, int port) {
         Properties config = new Properties();
         config.setProperty("org.quartz.jobStore.class", RedisJobStore.class.getName());
         config.setProperty("org.quartz.jobStore.host", host);
         config.setProperty("org.quartz.jobStore.port", String.valueOf(port));
         config.setProperty("org.quartz.threadPool.threadCount", "1");
         config.setProperty("org.quartz.jobStore.misfireThreshold", "500");
-        scheduler = new StdSchedulerFactory(config).getScheduler();
-        scheduler.start();
+        return config;
     }
 
 
@@ -66,80 +69,6 @@ public class RedisJobStoreIntegrationTest {
     }
 
 
-    @Test
-    public void testCompleteListener() throws Exception {
-        final String jobName = "oneJob";
-        JobDetail jobDetail = createJob(TestJob.class, jobName, "oneGroup");
-
-        final String triggerName = "trigger1";
-        CronTrigger trigger = createCronTrigger(triggerName, "oneGroup", "* * * * * ?");
-
-        Waiter waiter = new Waiter();
-        scheduler.scheduleJob(jobDetail, trigger);
-        scheduler.getListenerManager().addTriggerListener(new CompleteListener(waiter), NameMatcher.triggerNameEquals(triggerName));
-
-        // wait for CompleteListener.triggerComplete() to be called
-        waiter.await(1500);
-    }
-
-
-    @Test
-    public void testMisfireListener() throws Exception {
-        final String jobName = "oneJob";
-        JobDetail jobDetail = createJob(TestJob.class, jobName, "oneGroup");
-
-        final String triggerName = "trigger1";
-        final String everySecond = "* * * * * ?";
-        CronTrigger trigger = createCronTrigger(triggerName, "oneGroup", everySecond);
-
-
-        JobDetail sleepJob = createJob(SleepJob.class, "sleepJob", "twoGroup");
-        CronTrigger sleepTrigger = createCronTrigger("sleepTrigger", "twoGroup", everySecond);
-        Waiter waiter = new Waiter();
-        scheduler.scheduleJob(sleepJob, sleepTrigger);
-        scheduler.scheduleJob(jobDetail, trigger);
-
-        scheduler.getListenerManager().addTriggerListener(new MisfireListener(waiter), NameMatcher.triggerNameEquals(triggerName));
-
-        // wait for MisfireListener.triggerMisfired() to be called
-        waiter.await(2500);
-    }
-
-
-    @Test
-    public void testTriggerData() throws Exception {
-        final String jobName = "good";
-        JobDetail jobDetail = createJob(DataJob.class, jobName, "goodGroup");
-
-        final String triggerName = "trigger1";
-        final String everySecond = "* * * * * ?";
-        CronTrigger trigger = createCronTrigger(triggerName, "oneGroup", everySecond);
-        trigger = trigger.getTriggerBuilder()
-                .usingJobData("foo", "bar")
-                .build();
-        scheduler.setJobFactory(new RedisJobFactory());
-        scheduler.scheduleJob(jobDetail, trigger);
-        Waiter waiter = new Waiter();
-        scheduler.getListenerManager().addTriggerListener(new CompleteListener(waiter), NameMatcher.triggerNameEquals(triggerName));
-
-        // wait for CompleteListener.triggerComplete() to be called
-        waiter.await(1500);
-
-        try (Jedis jedis = jedisPool.getResource()) {
-            assertThat(jedis.get("foo"), equalTo("bar"));
-        }
-    }
-
-
-    private class RedisJobFactory extends PropertySettingJobFactory {
-        @Override
-        protected void setBeanProps(Object obj, JobDataMap data) throws SchedulerException {
-            data.put("jedisPool", jedisPool);
-            super.setBeanProps(obj, data);
-        }
-    }
-
-
     public static class DataJob implements Job {
         private Pool<Jedis> jedisPool;
 
@@ -173,10 +102,34 @@ public class RedisJobStoreIntegrationTest {
     }
 
 
-    private class CompleteListener implements TriggerListener {
+    @DisallowConcurrentExecution
+    public static class SingletonSleepJob extends SleepJob {
+        public static final AtomicInteger currentlyExecuting = new AtomicInteger(0);
+        public static final AtomicInteger concurrentExecutions = new AtomicInteger(0);
+
+        @Override
+        public void execute(JobExecutionContext context) throws JobExecutionException {
+            log.info("Starting job: " + context.getJobDetail().getKey() + " due to trigger " + context.getTrigger().getKey());
+            if (currentlyExecuting.incrementAndGet() > 1) {
+                log.error("Concurrent execution detected!!");
+                concurrentExecutions.incrementAndGet();
+                throw new JobExecutionException("Concurrent execution not allowed!");
+            }
+            try {
+                Thread.sleep(1000);     // add some extra sleep time to ensure that concurrent execution will be attempted
+            } catch (InterruptedException e) {
+                throw new JobExecutionException("Interrupted while sleeping.", e);
+            }
+            super.execute(context);
+            currentlyExecuting.decrementAndGet();
+        }
+    }
+
+
+    protected class CompleteListener implements TriggerListener {
         private final Waiter waiter;
 
-        private CompleteListener(Waiter waiter) {
+        protected CompleteListener(Waiter waiter) {
             this.waiter = waiter;
         }
 
@@ -207,10 +160,10 @@ public class RedisJobStoreIntegrationTest {
     }
 
 
-    private class MisfireListener implements TriggerListener {
+    protected class MisfireListener implements TriggerListener {
         private final Waiter waiter;
 
-        private MisfireListener(Waiter waiter) {
+        protected MisfireListener(Waiter waiter) {
             this.waiter = waiter;
         }
 
