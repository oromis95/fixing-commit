@@ -21,5 +21,5 @@ import java.lang.annotation.Target;
 @Target({ElementType.TYPE})
 @Retention(RetentionPolicy.RUNTIME)
 public @interface MergingAnnotation {
-    RepeatableAnnotation[] value();
+	RepeatableAnnotation[] value();
 }
