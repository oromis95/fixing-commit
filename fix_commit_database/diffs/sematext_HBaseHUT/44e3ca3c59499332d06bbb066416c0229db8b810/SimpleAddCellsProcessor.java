@@ -1,17 +1,24 @@
-/*
- * Copyright (c) Sematext International
- * All Rights Reserved
- * <p/>
- * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF Sematext International
- * The copyright notice above does not evidence any
- * actual or intended publication of such source code.
+/**
+ * Copyright 2010 Sematext International
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *     http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
  */
 package com.sematext.hbase.hut;
 
 import org.apache.hadoop.hbase.client.Result;
 
 /**
- * Performs simple processing of records: just adds all columns from records to the final one.
+ * Performs simple processing of records: just adds all columns from records to the result one.
  */
 public class SimpleAddCellsProcessor implements UpdateProcessor {
   @Override
