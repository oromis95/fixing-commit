@@ -10,6 +10,11 @@ import java.io.PrintWriter;
 import java.io.Reader;
 import java.io.Serializable;
 import java.io.StringReader;
+import java.io.InputStream;
+import java.io.InputStreamReader;
+import java.io.BufferedReader;
+import java.nio.charset.Charset;
+import java.nio.charset.CharsetDecoder;
 import java.net.URL;
 import java.util.ArrayList;
 import java.util.Arrays;
@@ -54,14 +59,29 @@ public class ChineseDocumentToSentenceProcessor implements Serializable {
   private void initNormalizationTable() {
     if (normalizationTable == null && normalizationTableFile != null) {
       normalizationTable = new ArrayList<Pair<String,String>>();
-      for (String line : ObjectBank.getLineIterator(new File(normalizationTableFile), encoding)) {
-        Pattern pairPattern = Pattern.compile("([^\\s]+)\\s+([^\\s]+)");
-        Matcher pairMatcher = pairPattern.matcher(line);
-        if (pairMatcher.find()) {
-          normalizationTable.add(new Pair<String,String>(pairMatcher.group(1),pairMatcher.group(2)));
-        } else {
-          System.err.println("Didn't match: "+line);
-        }
+
+      Charset charset = Charset.forName(encoding);
+      CharsetDecoder decoder = charset.newDecoder();
+      try {
+          InputStream ins = IOUtils.getInputStreamFromURLOrClasspathOrFileSystem(normalizationTableFile);
+          InputStreamReader isr = new InputStreamReader(ins, decoder);
+          BufferedReader br = new BufferedReader(isr);
+
+          for (String line : ObjectBank.getLineIterator(br)) {
+            Pattern pairPattern = Pattern.compile("([^\\s]+)\\s+([^\\s]+)");
+            Matcher pairMatcher = pairPattern.matcher(line);
+            if (pairMatcher.find()) {
+              normalizationTable.add(new Pair<String,String>(pairMatcher.group(1),pairMatcher.group(2)));
+            } else {
+              System.err.println("Didn't match: "+line);
+            }
+          }
+
+          br.close();
+          isr.close();
+          ins.close();
+      } catch (IOException ioe) {
+          System.err.println(ioe);
       }
     }
   }
