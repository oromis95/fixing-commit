@@ -1,41 +1,39 @@
 package com.guokr.nlp;
 
-import com.beust.jcommander.JCommander;
-import com.beust.jcommander.Parameter;
+import java.io.PrintStream;
 
 import com.guokr.nlp.SegWrapper;
 import com.guokr.nlp.NerWrapper;
 import com.guokr.nlp.TagWrapper;
 
-import com.guokr.nlp.commands.Seg;
-import com.guokr.nlp.commands.Ner;
-import com.guokr.nlp.commands.Tag;
-
 public class GkNlpCli {
 
     public static void main(String[] args) {
-        JCommander jc = new JCommander();
-        Seg seg = new Seg();
-        jc.addCommand("seg", seg);
-
-        Ner ner = new Ner();
-        jc.addCommand("ner", ner);
-
-        Tag tag = new Tag();
-        jc.addCommand("tag", tag);
-
-        jc.parse(args);
-        String subcmd = jc.getParsedCommand();
-        if(subcmd == null) {
-            jc.usage();
-        } else if(subcmd.equals("seg")) {
-            System.out.println(SegWrapper.segment(seg.text.get(0)));
-        } else if(subcmd.equals("ner")) {
-            System.out.println(NerWrapper.recognize(ner.text.get(0)));
-        } else if(subcmd.equals("tag")) {
-            System.out.println(TagWrapper.tag(tag.text.get(0)));
+
+        if(args.length < 2) {
+            usage();
+        } else {
+            String subcmd = args[0];
+            String text = args[1];
+            if(subcmd.equals("seg")) {
+                System.out.println(SegWrapper.segment(text));
+            } else if(subcmd.equals("ner")) {
+                System.out.println(NerWrapper.recognize(text));
+            } else if(subcmd.equals("tag")) {
+                System.out.println(TagWrapper.tag(text));
+            }
         }
 
     }
 
+    private static void usage() {
+        PrintStream out = System.out;
+        out.println("ant [command] [text]");
+        out.println("\tcommands:");
+        out.println("\t\tseg: segment the text into words");
+        out.println("\t\tner: recognize the named entity in the text");
+        out.println("\t\ttag: tag the text into words with syntax information");
+        out.println("");
+    }
+
 }
