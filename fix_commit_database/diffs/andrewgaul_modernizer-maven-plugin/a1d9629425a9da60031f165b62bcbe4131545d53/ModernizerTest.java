@@ -58,7 +58,6 @@ import com.google.common.primitives.Shorts;
 import com.google.common.primitives.UnsignedInts;
 import com.google.common.primitives.UnsignedLongs;
 import com.google.common.util.concurrent.Atomics;
-import com.google.inject.Inject;
 import com.google.inject.Provider;
 
 import org.apache.commons.codec.binary.Base64;
@@ -341,8 +340,6 @@ public final class ModernizerTest {
                 new ClassReader(VoidPredicate.class.getName())));
         occurrences.addAll(modernizer.check(
                 new ClassReader(VoidSupplier.class.getName())));
-        occurrences.addAll(modernizer.check(
-                new ClassReader(InjectMethod.class.getName())));
         occurrences.addAll(modernizer.check(
                 new ClassReader(AutowiredMethod.class.getName())));
         occurrences.addAll(modernizer.check(
@@ -474,13 +471,6 @@ public final class ModernizerTest {
         }
     }
 
-    private static class InjectMethod {
-        @Inject
-        InjectMethod() {
-            // Nothing
-        }
-    }
-
     private static class AutowiredMethod {
         @Autowired
         AutowiredMethod() {
