@@ -35,7 +35,7 @@ import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorUtils;
  */
 @Description(name = "least", value = "_FUNC_(value1, value2, value3, ....) " +
 		"- Returns the least value in the list.", 
-		extended = "Example:\n" + " > SELECT _FUNC_(2, 5, 12, 3) FROM src;\n 12")
+		extended = "Example:\n" + " > SELECT _FUNC_(2, 5, 12, 3) FROM src;\n 2")
 
 public class GenericUDFLeast extends GenericUDF {
 
