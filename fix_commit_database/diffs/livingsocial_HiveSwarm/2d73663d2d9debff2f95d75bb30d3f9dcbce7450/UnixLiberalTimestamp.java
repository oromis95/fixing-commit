@@ -1,9 +1,14 @@
 package com.livingsocial.hive.udf;
 
-import org.apache.hadoop.hive.ql.udf.UDFUnixTimeStamp;
 
+import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
+import org.apache.hadoop.hive.ql.exec.UDFArgumentLengthException;
 import org.apache.hadoop.hive.ql.exec.Description;
-import org.apache.hadoop.io.LongWritable;
+import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
+import org.apache.hadoop.hive.serde2.objectinspector.primitive.StringObjectInspector;
+import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
+import org.apache.hadoop.hive.ql.udf.generic.GenericUDFUnixTimeStamp;
+import org.apache.hadoop.hive.ql.metadata.HiveException;
 import org.apache.hadoop.io.Text;
 
 @Description(
@@ -12,10 +17,27 @@ import org.apache.hadoop.io.Text;
 	     extended = "Example:\n" +
 	     "  > SELECT a.* FROM srcpart a WHERE _FUNC_ (a.hr) < unix_timestamp() LIMIT 1;\n"
 	     )
-public class UnixLiberalTimestamp extends UDFUnixTimeStamp {
-    public LongWritable evaluate(Text datestring) {
-	if(datestring != null && datestring.find(" ") == -1)
-	    datestring = new Text(datestring.toString() + " 00:00:00");
-	return super.evaluate(datestring);
+public class UnixLiberalTimestamp extends GenericUDFUnixTimeStamp {
+
+    private transient StringObjectInspector stringOI;
+
+    public ObjectInspector initialize(ObjectInspector[] arguments) throws UDFArgumentException {
+        if (arguments.length != 1) {
+            throw new UDFArgumentLengthException(getName().toUpperCase() + " only takes 1 arguments: String");
+        }
+        super.initialize(new ObjectInspector[]{arguments[0], arguments[0]});
+        stringOI = (StringObjectInspector) arguments[0];
+        return PrimitiveObjectInspectorFactory.writableLongObjectInspector;
+    }
+
+    public Object evaluate(DeferredObject[] arguments) throws HiveException {
+        String datestring = stringOI.getPrimitiveJavaObject(arguments[0].get());
+        if (datestring == null) return null;
+        if (datestring.length() != 10) // timestamp
+            return super.evaluate(new DeferredObject[]{arguments[0],
+                                                       new DeferredJavaObject(new Text("yyyy-MM-dd HH:mm:ss"))});
+        else                           // date
+            return super.evaluate(new DeferredObject[]{arguments[0],
+                                                       new DeferredJavaObject(new Text("yyyy-MM-dd"))});
     }
 }
