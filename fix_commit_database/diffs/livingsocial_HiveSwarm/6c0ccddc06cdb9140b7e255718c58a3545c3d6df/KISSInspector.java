@@ -1,4 +1,4 @@
-package com.livingsocial.hive.udtf;
+package com.livingsocial.hive.utils;
 
 import java.util.ArrayList;
 import java.util.List;
@@ -61,6 +61,18 @@ public class KISSInspector {
 	return result;
     }
 
+    public boolean isNull() {
+	return getCategory() == PrimitiveCategory.VOID;
+    }
+
+    public boolean isString() {
+	return getCategory() == PrimitiveCategory.STRING;
+    }
+
+    public static boolean isPrimitive(ObjectInspector oi) {
+	return oi.getCategory() == ObjectInspector.Category.PRIMITIVE;
+    }
+
     public boolean equalPrimitive(Object first, Object second) {
 	if((first == null && second != null) || (first != null && second == null))
 	    return false;
