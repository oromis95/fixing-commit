@@ -8,13 +8,13 @@ import org.apache.hadoop.io.Text;
 
 @Description(
 	     name = "unix_liberal_timestamp",
-	     value = "_FUNC_(str) - gets unix timestamp in either yyyy-MM-dd HH:mm:ss or yyyy-MM-dd format",
+	     value = "_FUNC_(str) - gets unix timestamp in either yyyy-MM-dd HH:mm:ss or yyyy-MM-dd format - returns null if input is null",
 	     extended = "Example:\n" +
 	     "  > SELECT a.* FROM srcpart a WHERE _FUNC_ (a.hr) < unix_timestamp() LIMIT 1;\n"
 	     )
 public class UnixLiberalTimestamp extends UDFUnixTimeStamp {
     public LongWritable evaluate(Text datestring) {
-	if(datestring.find(" ") == -1)
+	if(datestring != null && datestring.find(" ") == -1)
 	    datestring = new Text(datestring.toString() + " 00:00:00");
 	return super.evaluate(datestring);
     }
