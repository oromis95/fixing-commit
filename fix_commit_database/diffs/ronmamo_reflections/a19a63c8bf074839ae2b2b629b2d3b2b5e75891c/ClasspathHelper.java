@@ -8,10 +8,15 @@ import java.io.File;
 import java.io.IOException;
 import java.io.UnsupportedEncodingException;
 import java.net.MalformedURLException;
+import java.net.URI;
 import java.net.URL;
 import java.net.URLClassLoader;
 import java.net.URLDecoder;
+import java.util.ArrayList;
+import java.util.Collection;
 import java.util.Enumeration;
+import java.util.HashSet;
+import java.util.List;
 import java.util.Set;
 import java.util.jar.Attributes;
 import java.util.jar.JarFile;
@@ -64,41 +69,39 @@ public abstract class ClasspathHelper {
     }
 
     /**
-     * Returns a set of URLs based on a package name.
+     * Returns a distinct collection of URLs based on a package name.
      * <p>
      * This searches for the package name as a resource, using {@link ClassLoader#getResources(String)}.
-     * For example, {@code forPackage("org.reflections")} effectively returns URLs from the
+     * For example, {@code forPackage(org.reflections)} effectively returns URLs from the
      * classpath containing packages starting with {@code org.reflections}.
      * <p>
      * If the optional {@link ClassLoader}s are not specified, then both {@link #contextClassLoader()}
      * and {@link #staticClassLoader()} are used for {@link ClassLoader#getResources(String)}.
      * <p>
-     * The returned set retains order using {@code LinkedHashSet}, however that order
-     * may not be the same as the classpath order.
+     * The returned URLs retainsthe order of the given {@code classLoaders}.
      * 
-     * @return the set of URLs, not null
+     * @return the collection of URLs, not null
      */
-    public static Set<URL> forPackage(String name, ClassLoader... classLoaders) {
+    public static Collection<URL> forPackage(String name, ClassLoader... classLoaders) {
         return forResource(resourceName(name), classLoaders);
     }
 
     /**
-     * Returns a set of URLs based on a resource.
+     * Returns a distinct collection of URLs based on a resource.
      * <p>
      * This searches for the resource name, using {@link ClassLoader#getResources(String)}.
-     * For example, {@code forResource("test.properties")} effectively returns URLs from the
+     * For example, {@code forResource(test.properties)} effectively returns URLs from the
      * classpath containing files of that name.
      * <p>
      * If the optional {@link ClassLoader}s are not specified, then both {@link #contextClassLoader()}
      * and {@link #staticClassLoader()} are used for {@link ClassLoader#getResources(String)}.
      * <p>
-     * The returned set retains order using {@code LinkedHashSet}, however that order
-     * may not be the same as the classpath order.
-     * 
-     * @return the set of URLs, not null
+     * The returned URLs retains the order of the given {@code classLoaders}.
+     *
+     * @return the collection of URLs, not null
      */
-    public static Set<URL> forResource(String resourceName, ClassLoader... classLoaders) {
-        final Set<URL> result = Sets.newLinkedHashSet();
+    public static Collection<URL> forResource(String resourceName, ClassLoader... classLoaders) {
+        final List<URL> result = new ArrayList<URL>();
         final ClassLoader[] loaders = classLoaders(classLoaders);
         for (ClassLoader classLoader : loaders) {
             try {
@@ -118,7 +121,7 @@ public abstract class ClasspathHelper {
                 }
             }
         }
-        return result;
+        return distinctUrls(result);
     }
 
     /**
@@ -149,22 +152,21 @@ public abstract class ClasspathHelper {
     }
     
     /**
-     * Returns a set of URLs based on URLs derived from class loaders.
+     * Returns a distinct collection of URLs based on URLs derived from class loaders.
      * <p>
      * This finds the URLs using {@link URLClassLoader#getURLs()} using both
      * {@link #contextClassLoader()} and {@link #staticClassLoader()}.
      * <p>
-     * The returned set retains order using {@code LinkedHashSet}, however that order
-     * may not be the same as the classpath order.
+     * The returned URLs retains the order of the given {@code classLoaders}.
      * 
-     * @return the set of URLs, not null
+     * @return the collection of URLs, not null
      */
-    public static Set<URL> forClassLoader() {
+    public static Collection<URL> forClassLoader() {
         return forClassLoader(classLoaders());
     }
 
     /**
-     * Returns a set of URLs based on URLs derived from class loaders.
+     * Returns a distinct collection of URLs based on URLs derived from class loaders.
      * <p>
      * This finds the URLs using {@link URLClassLoader#getURLs()} using the specified
      * class loader, searching up the parent hierarchy.
@@ -172,13 +174,12 @@ public abstract class ClasspathHelper {
      * If the optional {@link ClassLoader}s are not specified, then both {@link #contextClassLoader()}
      * and {@link #staticClassLoader()} are used for {@link ClassLoader#getResources(String)}.
      * <p>
-     * The returned set retains order using {@code LinkedHashSet}, however that order
-     * may not be the same as the classpath order.
+     * The returned URLs retains the order of the given {@code classLoaders}.
      * 
-     * @return the set of URLs, not null
+     * @return the collection of URLs, not null
      */
-    public static Set<URL> forClassLoader(ClassLoader... classLoaders) {
-        final Set<URL> result = Sets.newLinkedHashSet();
+    public static Collection<URL> forClassLoader(ClassLoader... classLoaders) {
+        final Collection<URL> result = new ArrayList<URL>();
         final ClassLoader[] loaders = classLoaders(classLoaders);
         for (ClassLoader classLoader : loaders) {
             while (classLoader != null) {
@@ -191,20 +192,20 @@ public abstract class ClasspathHelper {
                 classLoader = classLoader.getParent();
             }
         }
-        return result;
+        return distinctUrls(result);
     }
 
     /**
-     * Returns a set of URLs based on the {@code java.class.path} system property.
+     * Returns a distinct collection of URLs based on the {@code java.class.path} system property.
      * <p>
      * This finds the URLs using the {@code java.class.path} system property.
      * <p>
-     * The returned set retains the classpath order using {@code LinkedHashSet}.
+     * The returned collection of URLs retains the classpath order.
      * 
-     * @return the set of URLs, not null
+     * @return the collection of URLs, not null
      */
-    public static Set<URL> forJavaClassPath() {
-        Set<URL> urls = Sets.newLinkedHashSet();
+    public static Collection<URL> forJavaClassPath() {
+        Collection<URL> urls = new ArrayList<URL>();
         String javaClassPath = System.getProperty("java.class.path");
         if (javaClassPath != null) {
             for (String path : javaClassPath.split(File.pathSeparator)) {
@@ -215,27 +216,26 @@ public abstract class ClasspathHelper {
                 }
             }
         }
-        return urls;
+        return distinctUrls(urls);
     }
 
     /**
-     * Returns a set of URLs based on the {@code WEB-INF/lib} folder.
+     * Returns a distinct collection of URLs based on the {@code WEB-INF/lib} folder.
      * <p>
      * This finds the URLs using the {@link ServletContext}.
      * <p>
-     * The returned set retains order using {@code LinkedHashSet}, however that order
-     * may not be the same as the classpath order.
+     * The returned URLs retains the order of the given {@code classLoaders}.
      * 
-     * @return the set of URLs, not null
+     * @return the collection of URLs, not null
      */
-    public static Set<URL> forWebInfLib(final ServletContext servletContext) {
-        final Set<URL> urls = Sets.newLinkedHashSet();
+    public static Collection<URL> forWebInfLib(final ServletContext servletContext) {
+        final Collection<URL> urls = new ArrayList<URL>();
         for (Object urlString : servletContext.getResourcePaths("/WEB-INF/lib")) {
             try {
                 urls.add(servletContext.getResource((String) urlString));
             } catch (MalformedURLException e) { /*fuck off*/ }
         }
-        return urls;
+        return distinctUrls(urls);
     }
 
     /**
@@ -243,7 +243,7 @@ public abstract class ClasspathHelper {
      * <p>
      * This finds the URLs using the {@link ServletContext}.
      * 
-     * @return the set of URLs, not null
+     * @return the collection of URLs, not null
      */
     public static URL forWebInfClasses(final ServletContext servletContext) {
         try {
@@ -260,31 +260,31 @@ public abstract class ClasspathHelper {
     }
 
     /**
-     * Returns a set of URLs based on URLs derived from class loaders expanded with Manifest information.
+     * Returns a distinct collection of URLs based on URLs derived from class loaders expanded with Manifest information.
      * <p>
      * The {@code MANIFEST.MF} file can contain a {@code Class-Path} entry that defines
      * additional jar files to be included on the classpath. This method finds the jar files
      * using the {@link #contextClassLoader()} and {@link #staticClassLoader()}, before
      * searching for any additional manifest classpaths.
      * 
-     * @return the set of URLs, not null
+     * @return the collection of URLs, not null
      */
-    public static Set<URL> forManifest() {
+    public static Collection<URL> forManifest() {
         return forManifest(forClassLoader());
     }
 
     /**
-     * Returns a set of URLs from a single URL based on the Manifest information.
+     * Returns a distinct collection of URLs from a single URL based on the Manifest information.
      * <p>
      * The {@code MANIFEST.MF} file can contain a {@code Class-Path} entry that defines additional
      * jar files to be included on the classpath. This method takes a single URL, tries to
      * resolve it as a jar file, and if so, adds any additional manifest classpaths.
-     * The returned set will always contain the input URL.
+     * The returned collection of URLs will always contain the input URL.
      * 
-     * @return the set of URLs, not null
+     * @return the collection of URLs, not null
      */
-    public static Set<URL> forManifest(final URL url) {
-        final Set<URL> result = Sets.newLinkedHashSet();
+    public static Collection<URL> forManifest(final URL url) {
+        final Collection<URL> result = new ArrayList<URL>();
         result.add(url);
         try {
             final String part = cleanPath(url);
@@ -305,28 +305,28 @@ public abstract class ClasspathHelper {
         } catch (IOException e) {
             // don't do anything, we're going on the assumption it is a jar, which could be wrong
         }
-        return result;
+        return distinctUrls(result);
     }
 
     /**
-     * Returns a set of URLs by expanding the specified URLs with Manifest information.
+     * Returns a distinct collection of URLs by expanding the specified URLs with Manifest information.
      * <p>
      * The {@code MANIFEST.MF} file can contain a {@code Class-Path} entry that defines additional
      * jar files to be included on the classpath. This method takes each URL in turn, tries to
      * resolve it as a jar file, and if so, adds any additional manifest classpaths.
-     * The returned set will always contain all the input URLs.
+     * The returned collection of URLs will always contain all the input URLs.
      * <p>
-     * The returned set retains the input order using {@code LinkedHashSet}.
+     * The returned URLs retains the input order.
      * 
-     * @return the set of URLs, not null
+     * @return the collection of URLs, not null
      */
-    public static Set<URL> forManifest(final Iterable<URL> urls) {
-        Set<URL> result = Sets.newLinkedHashSet();
+    public static Collection<URL> forManifest(final Iterable<URL> urls) {
+        Collection<URL> result = new ArrayList<URL>();
         // determine if any of the URLs are JARs, and get any dependencies
         for (URL url : urls) {
             result.addAll(forManifest(url));
         }
-        return result;
+        return distinctUrls(result);
     }
 
     //a little bit cryptic...
@@ -377,8 +377,25 @@ public abstract class ClasspathHelper {
                 resourceName = resourceName.substring(1);
             }
             return resourceName;
-        } else {
-            return name;
+        }
+        return null;
+    }
+
+    //http://michaelscharf.blogspot.co.il/2006/11/javaneturlequals-and-hashcode-make.html
+    private static Collection<URL> distinctUrls(Collection<URL> urls) {
+        try {
+            Set<URI> uris = new HashSet<URI>(urls.size());
+            for (URL url : urls) {
+                uris.add(url.toURI());
+            }
+            List<URL> result = new ArrayList<URL>(uris.size());
+            for (URI uri : uris) {
+                result.add(uri.toURL());
+            }
+            return result;
+        } catch (Exception e) {
+            //return original urls as set, prefer backward comp over potential performance issue
+            return Sets.newHashSet(urls);
         }
     }
 }
