@@ -224,9 +224,7 @@ public class ReflectionsTest {
         assertThat(resolved, are("META-INF/reflections/resource1-reflections.xml"));
 
         Set<String> resources = reflections.getStore().get(ResourcesScanner.class.getSimpleName()).keySet();
-        assertThat(resources, are(
-                "resource1-reflections.xml", "resource2-reflections.xml", "testModel-reflections.xml"
-        ));
+        assertThat(resources, are("resource1-reflections.xml", "resource2-reflections.xml"));
     }
 
     @Test
