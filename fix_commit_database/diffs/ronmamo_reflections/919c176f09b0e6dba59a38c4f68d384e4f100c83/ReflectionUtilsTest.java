@@ -8,7 +8,6 @@ import org.junit.Test;
 import org.reflections.scanners.FieldAnnotationsScanner;
 
 import javax.annotation.Nullable;
-import java.io.Serializable;
 import java.lang.annotation.Annotation;
 import java.lang.reflect.Field;
 import java.lang.reflect.Member;
@@ -17,39 +16,17 @@ import java.lang.reflect.Modifier;
 import java.util.Arrays;
 import java.util.Collection;
 import java.util.Collections;
-import java.util.List;
-import java.util.Random;
 import java.util.Set;
 
 import static com.google.common.collect.Collections2.transform;
-import static org.junit.Assert.assertEquals;
-import static org.junit.Assert.assertFalse;
-import static org.junit.Assert.assertThat;
-import static org.junit.Assert.assertTrue;
-import static org.reflections.ReflectionUtils.getAll;
-import static org.reflections.ReflectionUtils.getAllAnnotations;
-import static org.reflections.ReflectionUtils.getAllConstructors;
-import static org.reflections.ReflectionUtils.getAllFields;
-import static org.reflections.ReflectionUtils.getAllMethods;
-import static org.reflections.ReflectionUtils.getAllSuperTypes;
-import static org.reflections.ReflectionUtils.getAnnotations;
-import static org.reflections.ReflectionUtils.getMethods;
-import static org.reflections.ReflectionUtils.withAnnotation;
-import static org.reflections.ReflectionUtils.withAnyParameterAnnotation;
-import static org.reflections.ReflectionUtils.withModifier;
-import static org.reflections.ReflectionUtils.withName;
-import static org.reflections.ReflectionUtils.withParameters;
-import static org.reflections.ReflectionUtils.withParametersAssignableTo;
-import static org.reflections.ReflectionUtils.withParametersCount;
-import static org.reflections.ReflectionUtils.withPattern;
-import static org.reflections.ReflectionUtils.withReturnType;
-import static org.reflections.ReflectionUtils.withReturnTypeAssignableTo;
-import static org.reflections.ReflectionUtils.withTypeAssignableTo;
+import static org.junit.Assert.*;
+import static org.reflections.ReflectionUtils.*;
 import static org.reflections.ReflectionsTest.are;
 
 /**
  * @author mamo
  */
+@SuppressWarnings("unchecked")
 public class ReflectionUtilsTest {
 
     @Test
@@ -82,13 +59,26 @@ public class ReflectionUtilsTest {
         assertTrue(getAnnotations(m4).isEmpty());
     }
 
-    @Test public void withAssignable() {
-        Set<Method> allMethods = getAllMethods(Collections.class, withParameters(List.class, Random.class));
-        assertThat(allMethods, names("shuffle"));
+    @Test public void withParameter() throws Exception {
+        Class target = Collections.class;
+        Object arg1 = Arrays.asList(1, 2, 3);
 
-        Set<Method> allMethods1 = getAllMethods(Collections.class, withParametersAssignableTo(Iterable.class, Serializable.class));
-        assertTrue(allMethods1.contains(allMethods.iterator().next()));
+        Set<Method> allMethods = Sets.newHashSet();
+        for (Class<?> type : getAllSuperTypes(arg1.getClass())) {
+            allMethods.addAll(getAllMethods(target, withModifier(Modifier.STATIC), withParameters(type)));
+        }
 
+        Set<Method> allMethods1 = getAllMethods(target, withModifier(Modifier.STATIC), withParametersAssignableTo(arg1.getClass()));
+
+        assertEquals(allMethods, allMethods1);
+
+        for (Method method : allMethods) { //effectively invokable
+            //noinspection UnusedDeclaration
+            Object invoke = method.invoke(null, arg1);
+        }
+    }
+
+    @Test public void withReturn() throws Exception {
         Set<Method> returnMember = getAllMethods(Class.class, withReturnTypeAssignableTo(Member.class));
         Set<Method> returnsAssignableToMember = getAllMethods(Class.class, withReturnType(Method.class));
 
@@ -105,7 +95,7 @@ public class ReflectionUtilsTest {
         Reflections reflections = new Reflections(TestModel.class, new FieldAnnotationsScanner());
 
         Set<Field> af1 = reflections.getFieldsAnnotatedWith(TestModel.AF1.class);
-        Set<? extends Field> allFields = getAll(af1, withModifier(Modifier.PROTECTED));
+        Set<? extends Field> allFields = Sets.filter(af1, withModifier(Modifier.PROTECTED));
         assertTrue(allFields.size() == 1);
         assertThat(allFields, names("f2"));
     }
