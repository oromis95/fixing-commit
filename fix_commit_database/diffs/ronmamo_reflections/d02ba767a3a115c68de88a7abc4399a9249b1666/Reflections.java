@@ -249,7 +249,7 @@ public class Reflections {
                     Object classObject = null;
                     for (Scanner scanner : configuration.getScanners()) {
                         try {
-                            if (scanner.acceptsInput(path) || scanner.acceptResult(fqn)) {
+                            if (scanner.acceptsInput(path) || scanner.acceptsInput(fqn)) {
                                 classObject = scanner.scan(file, classObject);
                             }
                         } catch (Exception e) {
