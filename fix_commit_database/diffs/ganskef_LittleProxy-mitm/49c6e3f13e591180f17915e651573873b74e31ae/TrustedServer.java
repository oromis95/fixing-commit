@@ -1,17 +1,22 @@
 package de.ganskef.test;
 
-import io.netty.handler.ssl.SslContext;
-
 import java.io.File;
 
 import org.littleshoot.proxy.mitm.Authority;
 import org.littleshoot.proxy.mitm.BouncyCastleSslEngineSource;
 import org.littleshoot.proxy.mitm.SubjectAlternativeNameHolder;
 
-public class TrustedServer extends Server {
+public class TrustedServer extends SecureServer {
 
-    public TrustedServer(int port) {
+    private String commonName;
+
+    public TrustedServer(int port, String commonName) {
         super(port);
+        this.commonName = commonName;
+    }
+
+    public TrustedServer(int port) {
+        this(port, "localhost");
     }
 
     public Server start() throws Exception {
@@ -19,20 +24,11 @@ public class TrustedServer extends Server {
                 new Authority(), true, true);
         SubjectAlternativeNameHolder san = new SubjectAlternativeNameHolder();
         // san.addDomainName("localhost");
-        es.initializeServerCertificates("localhost", san);
-        File certChainFile = new File("littleproxy-mitm-localhost-cert.pem");
-        File keyFile = new File("littleproxy-mitm-localhost-key.pem");
-        SslContext sslCtx = SslContext.newServerContext(certChainFile, keyFile);
-        return super.start(sslCtx);
-    }
-
-    @Override
-    public String getBaseUrl() {
-        if (getPort() == 443) {
-            return ("https://localhost");
-        } else {
-            return ("https://localhost:" + getPort());
-        }
+        es.initializeServerCertificates(commonName, san);
+        File certChainFile = new File("littleproxy-mitm-" + commonName
+                + "-cert.pem");
+        File keyFile = new File("littleproxy-mitm-" + commonName + "-key.pem");
+        return initServerContext(certChainFile, keyFile);
     }
 
     public static void main(String[] args) throws Exception {
