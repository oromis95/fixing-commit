@@ -12,7 +12,7 @@ import io.netty.channel.SimpleChannelInboundHandler;
 import io.netty.channel.nio.NioEventLoopGroup;
 import io.netty.channel.socket.SocketChannel;
 import io.netty.channel.socket.nio.NioSocketChannel;
-import io.netty.handler.codec.http.DefaultHttpRequest;
+import io.netty.handler.codec.http.DefaultFullHttpRequest;
 import io.netty.handler.codec.http.HttpClientCodec;
 import io.netty.handler.codec.http.HttpContent;
 import io.netty.handler.codec.http.HttpContentDecompressor;
@@ -21,8 +21,11 @@ import io.netty.handler.codec.http.HttpMethod;
 import io.netty.handler.codec.http.HttpObject;
 import io.netty.handler.codec.http.HttpRequest;
 import io.netty.handler.codec.http.HttpVersion;
+import io.netty.handler.codec.http.LastHttpContent;
 import io.netty.handler.logging.LogLevel;
 import io.netty.handler.logging.LoggingHandler;
+import io.netty.handler.ssl.SslContext;
+import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
 
 import java.io.File;
 import java.io.RandomAccessFile;
@@ -50,28 +53,52 @@ public class NettyClient_NoHttps {
         URI uri = new URI(url);
         String host = uri.getHost();
         int port = uri.getPort();
+        if (port == -1) {
+            if (isSecured(uri)) {
+                port = 443;
+            } else {
+                port = 80;
+            }
+        }
         return get(uri, uri.getRawPath(), host, port, target);
     }
 
+    private boolean isSecured(URI uri) {
+
+        // XXX https via offline proxy won't work with this client. I mean, this
+        // was my experience while debugging it. I had no success with Apache
+        // HC, too. Only URLConnection works like expected for me.
+        //
+        // It seems to me we have to wait for a proper solution - see:
+        // https://github.com/netty/netty/issues/1133#event-299614098
+        // normanmaurer modified the milestone: 4.1.0.Beta5, 4.1.0.Beta6
+        //
+        // return uri.getScheme().equalsIgnoreCase("https");
+
+        return false;
+    }
+
     private File get(URI uri, String url, String proxyHost, int proxyPort,
             final String target) throws Exception {
-        if (url.toLowerCase().startsWith("https")) {
-            System.out.println("HTTPS is not supported, try HTTP for " + url);
-        }
-        if (proxyPort == -1) {
-            proxyPort = 80;
+        final SslContext sslCtx;
+        if (isSecured(uri)) {
+            sslCtx = SslContext
+                    .newClientContext(InsecureTrustManagerFactory.INSTANCE);
+        } else {
+            sslCtx = null;
         }
         final NettyClientHandler handler = new NettyClientHandler(target);
         EventLoopGroup group = new NioEventLoopGroup();
         try {
             Bootstrap b = new Bootstrap();
             b.group(group).channel(NioSocketChannel.class)
-                    .handler(new NettyClientInitializer(handler));
+                    .handler(new NettyClientInitializer(sslCtx, handler));
+            // .handler(new HttpSnoopClientInitializer(sslCtx));
 
             Channel ch = b.connect(proxyHost, proxyPort).sync().channel();
 
-            HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1,
-                    HttpMethod.GET, url);
+            HttpRequest request = new DefaultFullHttpRequest(
+                    HttpVersion.HTTP_1_1, HttpMethod.GET, url);
             request.headers().set(HttpHeaders.Names.HOST, uri.getHost());
             request.headers().set(HttpHeaders.Names.CONNECTION,
                     HttpHeaders.Values.CLOSE);
@@ -119,6 +146,9 @@ class NettyClientHandler extends SimpleChannelInboundHandler<HttpObject> {
                 IOUtils.closeQuietly(oc);
                 IOUtils.closeQuietly(output);
             }
+            if (content instanceof LastHttpContent) {
+                ctx.close();
+            }
         }
     }
 
@@ -131,13 +161,19 @@ class NettyClientInitializer extends ChannelInitializer<SocketChannel> {
 
     private ChannelHandler handler;
 
-    public NettyClientInitializer(ChannelHandler handler) {
+    private SslContext sslCtx;
+
+    public NettyClientInitializer(SslContext sslCtx, ChannelHandler handler) {
+        this.sslCtx = sslCtx;
         this.handler = handler;
     }
 
     @Override
     protected void initChannel(SocketChannel ch) throws Exception {
         ChannelPipeline p = ch.pipeline();
+        if (sslCtx != null) {
+            p.addLast(sslCtx.newHandler(ch.alloc()));
+        }
         p.addLast("log", new LoggingHandler(LogLevel.TRACE));
         p.addLast("codec", new HttpClientCodec());
         p.addLast("inflater", new HttpContentDecompressor());
