@@ -3,9 +3,15 @@ package osmedile.intellij.stringmanip.styles;
 import org.apache.commons.lang.WordUtils;
 
 public class WordsCapitalizeAction extends AbstractCaseConvertingAction {
+	public WordsCapitalizeAction() {
+	}
+
+	public WordsCapitalizeAction(boolean setupHandler) {
+		super(setupHandler);
+	}
 
 	@Override
 	public String transformByLine(String s) {
-		return WordUtils.capitalize(s);
+		return WordUtils.capitalizeFully(s);
 	}
 }
