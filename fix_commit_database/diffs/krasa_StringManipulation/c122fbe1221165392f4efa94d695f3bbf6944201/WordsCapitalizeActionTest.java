@@ -0,0 +1,27 @@
+package osmedile.intellij.stringmanip.styles;
+
+import org.junit.Test;
+
+import static org.junit.Assert.assertEquals;
+
+public class WordsCapitalizeActionTest {
+	WordsCapitalizeAction action;
+
+
+	@Test
+	public void transformByLine() {
+		action = new WordsCapitalizeAction(false);
+		assertEquals("Foo", action.transformByLine("foo"));
+		assertEquals("Foo", action.transformByLine("FOO"));
+
+		assertEquals("Foo Bar", action.transformByLine("foo bar"));
+		assertEquals("Foo Bar", action.transformByLine("FOO BAR"));
+
+		assertEquals("Foo-bar", action.transformByLine("foo-bar"));
+		assertEquals("Foo-bar", action.transformByLine("FOO-BAR"));
+
+		assertEquals("Foo.bar", action.transformByLine("foo.bar"));
+		assertEquals("Foo.bar", action.transformByLine("FOO.BAR"));
+
+	}
+}
\ No newline at end of file
