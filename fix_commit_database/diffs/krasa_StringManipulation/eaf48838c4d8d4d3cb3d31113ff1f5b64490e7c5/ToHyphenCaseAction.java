@@ -12,10 +12,10 @@ public class ToHyphenCaseAction extends AbstractCaseConvertingAction {
 	@Override
 	public String transformByLine(String s) {
 		Style from = Style.from(s);
-		if (from == Style.HYPHEN_LOWERCASE) {
+		if (from == Style.KEBAB_LOWERCASE) {
 			return Style.SNAKE_CASE.transform(from, s);
 		}
-		return Style.HYPHEN_LOWERCASE.transform(from, s);
+		return Style.KEBAB_LOWERCASE.transform(from, s);
 	}
 
 }
