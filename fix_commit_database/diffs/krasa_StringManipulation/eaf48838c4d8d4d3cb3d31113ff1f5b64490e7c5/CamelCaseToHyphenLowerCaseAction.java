@@ -11,10 +11,10 @@ public class CamelCaseToHyphenLowerCaseAction extends AbstractCaseConvertingActi
 	@Override
 	public String transformByLine(String s) {
 		Style from = Style.from(s);
-		if (from == Style.HYPHEN_LOWERCASE) {
+		if (from == Style.KEBAB_LOWERCASE) {
 			return Style.CAMEL_CASE.transform(from, s);
 		} else {
-			return Style.HYPHEN_LOWERCASE.transform(from, s);
+			return Style.KEBAB_LOWERCASE.transform(from, s);
 		}
 	}
 }
