@@ -2,7 +2,7 @@ package osmedile.intellij.stringmanip.align;
 
 import com.intellij.openapi.diagnostic.Logger;
 import com.jgoodies.common.collect.ArrayListModel;
-import osmedile.intellij.stringmanip.PluginPersistentStateComponent;
+import osmedile.intellij.stringmanip.config.PluginPersistentStateComponent;
 
 import javax.swing.*;
 import javax.swing.event.ListSelectionEvent;
