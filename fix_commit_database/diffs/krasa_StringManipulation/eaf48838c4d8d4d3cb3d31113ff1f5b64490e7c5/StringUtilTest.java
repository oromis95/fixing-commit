@@ -1,9 +1,10 @@
 package osmedile.intellij.stringmanip.utils;
 
-import junit.framework.Test;
-import junit.framework.TestCase;
-import junit.framework.TestSuite;
-import osmedile.intellij.stringmanip.PluginPersistentStateComponent;
+import org.junit.Test;
+import osmedile.intellij.stringmanip.config.PluginPersistentStateComponent;
+import osmedile.intellij.stringmanip.styles.CaseSwitchingTest;
+
+import static org.junit.Assert.assertEquals;
 
 /**
  * StringUtil Tester.
@@ -12,26 +13,8 @@ import osmedile.intellij.stringmanip.PluginPersistentStateComponent;
  * @version $Id$
  * @since <pre>03/21/2008</pre>
  */
-public class StringUtilTest extends TestCase {
-
-    public StringUtilTest(String name) {
-        super(name);
-    }
-
-	@Override
-    public void setUp() throws Exception {
-        super.setUp();
-    }
-
-	@Override
-    public void tearDown() throws Exception {
-        super.tearDown();
-    }
-
-    public static Test suite() {
-        return new TestSuite(StringUtilTest.class);
-    }
-
+public class StringUtilTest extends CaseSwitchingTest {
+    @Test
     public void testToCamelCase() {
         assertEquals("thisIsAText", StringUtil.toCamelCase("This is a text"));
         //this is ugly but nothing can be done about that.
@@ -44,6 +27,7 @@ public class StringUtilTest extends TestCase {
 //        assertEquals("whoahATest", StringUtil.toCamelCase("WhOAh a_TeSt"));
     }
 
+    @Test
     public void testWordsAndCamelToConstantCase() {
         PluginPersistentStateComponent.getInstance().getCaseSwitchingSettings().setSeparatorAfterDigit(true);
         PluginPersistentStateComponent.getInstance().getCaseSwitchingSettings().setSeparatorBeforeDigit(true);
@@ -56,16 +40,17 @@ public class StringUtilTest extends TestCase {
         assertEquals("V2_COUNTER", StringUtil.wordsAndHyphenAndCamelToConstantCase("v2Counter"));
         assertEquals("V22_COUNTER", StringUtil.wordsAndHyphenAndCamelToConstantCase("v22Counter"));
         assertEquals("V22_COUNTER22", StringUtil.wordsAndHyphenAndCamelToConstantCase("v22Counter22"));
-		assertEquals("THIS_IS_ATEXT", StringUtil.wordsAndHyphenAndCamelToConstantCase("ThisIsAText"));
-		assertEquals("WHOAH_ATEST", StringUtil.wordsAndHyphenAndCamelToConstantCase("WhoahATest"));
-		assertEquals("WHOAH_ATEST", StringUtil.wordsAndHyphenAndCamelToConstantCase("Whoah ATest"));
-		assertEquals("WHOAH_A_TEST,_AGAIN", StringUtil.wordsAndHyphenAndCamelToConstantCase("Whoah  A   Test, again"));
+        assertEquals("THIS_IS_ATEXT", StringUtil.wordsAndHyphenAndCamelToConstantCase("ThisIsAText"));
+        assertEquals("WHOAH_ATEST", StringUtil.wordsAndHyphenAndCamelToConstantCase("WhoahATest"));
+        assertEquals("WHOAH_ATEST", StringUtil.wordsAndHyphenAndCamelToConstantCase("Whoah ATest"));
+        assertEquals("WHOAH_A_TEST,_AGAIN", StringUtil.wordsAndHyphenAndCamelToConstantCase("Whoah  A   Test, again"));
         assertEquals("ANOTHER_T_EST", StringUtil.wordsAndHyphenAndCamelToConstantCase("Another      t_Est"));
         assertEquals("TEST_AGAIN_TEST",
-                StringUtil.wordsAndHyphenAndCamelToConstantCase("test again     _    _    test"));
+            StringUtil.wordsAndHyphenAndCamelToConstantCase("test again     _    _    test"));
         assertEquals("TEST_AGAIN_TEST", StringUtil.wordsAndHyphenAndCamelToConstantCase("TestAgain_   _    Test"));
     }
 
+    @Test
     public void testEscapedUnicodeToString() throws Exception {
         assertEquals("Información del diseño", StringUtil.escapedUnicodeToString("Información del diseño"));
         assertEquals("Čás", StringUtil.escapedUnicodeToString("\\u010c\\u00e1s"));
@@ -74,15 +59,16 @@ public class StringUtilTest extends TestCase {
         assertEquals("ěščřžýáíéĚŠČŘŽÝÁÍÉ", StringUtil.escapedUnicodeToString("\\u011B\\u0161\\u010D\\u0159\\u017E\\u00FD\\u00E1\\u00ED\\u00E9\\u011A\\u0160\\u010C\\u0158\\u017D\\u00DD\\u00C1\\u00CD\\u00C9"));
     }
 
+    @Test
     public void testWordsToConstantCase() {
         assertEquals("THISISATEXT", StringUtil.wordsToConstantCase("ThisIsAText"));
         assertEquals("WHOAH_A_TEST", StringUtil.wordsToConstantCase("Whoah A Test"));
         assertEquals("WHOAH_A_TEST", StringUtil.wordsToConstantCase("Whoah    a tESt"));
         assertEquals("_ANOTHER_TEXT_", StringUtil.wordsToConstantCase("_ANOTHER     TExT_"));
         assertEquals("TEST_AGAIN_____TEST",
-                StringUtil.wordsToConstantCase("test agaIN     _    _    _    _    _    test"));
+            StringUtil.wordsToConstantCase("test agaIN     _    _    _    _    _    test"));
         assertEquals("TEST_AGAIN_____TEST",
-                StringUtil.wordsToConstantCase("test agaIN_    _    _    _    _    Test"));
+            StringUtil.wordsToConstantCase("test agaIN_    _    _    _    _    Test"));
         assertEquals("TEST_AGAIN", StringUtil.wordsToConstantCase(" test agaIN"));
         assertEquals("_TEST_AGAIN", StringUtil.wordsToConstantCase("_  test agaIN"));
         assertEquals("_TEST_AGAIN", StringUtil.wordsToConstantCase("   _  test agaIN"));
