@@ -2,6 +2,7 @@ package osmedile.intellij.stringmanip.styles.custom;
 
 import com.intellij.openapi.diagnostic.Logger;
 import org.jetbrains.annotations.NotNull;
+import osmedile.intellij.stringmanip.MyEditorWriteActionHandler;
 import osmedile.intellij.stringmanip.styles.AbstractCaseConvertingAction;
 import osmedile.intellij.stringmanip.styles.Style;
 
@@ -21,6 +22,8 @@ public class CustomAction extends AbstractCaseConvertingAction {
 		getTemplatePresentation().setText(customActionModel.getName());
 		String description = makeDescription();
 		getTemplatePresentation().setDescription(description);
+		MyEditorWriteActionHandler handler = (MyEditorWriteActionHandler) getHandler();
+		handler.setCustomActionModel(model);
 	}
 
 	@NotNull
