@@ -8,6 +8,7 @@ import java.util.List;
 public enum Sort {
 
 	SHUFFLE(new Comparator<SortLine>() {
+
 		@Override
 		public int compare(SortLine o1, SortLine o2) {
 			throw new RuntimeException();
@@ -19,28 +20,44 @@ public enum Sort {
 			throw new RuntimeException();
 		}
 	}),
-	CASE_SENSITIVE_A_Z(new Comparator<SortLine>() {
-		@Override
-		public int compare(SortLine o1, SortLine o2) {
-			return COMPARATOR.compare(o1.getTextForComparison(), o2.getTextForComparison());
+	CASE_SENSITIVE_A_Z(new ComparatorFactory() {
+		public Comparator<SortLine> prototype(Comparator comparator) {
+			return new Comparator<SortLine>() {
+				@Override
+				public int compare(SortLine o1, SortLine o2) {
+					return comparator.compare(o1.getTextForComparison(), o2.getTextForComparison());
+				}
+			};
 		}
 	}),
-	CASE_SENSITIVE_Z_A(new Comparator<SortLine>() {
-		@Override
-		public int compare(SortLine o1, SortLine o2) {
-			return COMPARATOR.compare(o2.getTextForComparison(), o1.getTextForComparison());
+	CASE_SENSITIVE_Z_A(new ComparatorFactory() {
+		public Comparator<SortLine> prototype(Comparator comparator) {
+			return new Comparator<SortLine>() {
+				@Override
+				public int compare(SortLine o1, SortLine o2) {
+					return comparator.compare(o2.getTextForComparison(), o1.getTextForComparison());
+				}
+			};
 		}
 	}),
-	CASE_INSENSITIVE_A_Z(new Comparator<SortLine>() {
-		@Override
-		public int compare(SortLine o1, SortLine o2) {
-			return COMPARATOR.compare(o1.getTextForComparison().toLowerCase(), o2.getTextForComparison().toLowerCase());
+	CASE_INSENSITIVE_A_Z(new ComparatorFactory() {
+		public Comparator<SortLine> prototype(Comparator comparator) {
+			return new Comparator<SortLine>() {
+				@Override
+				public int compare(SortLine o1, SortLine o2) {
+					return comparator.compare(o1.getTextForComparison().toLowerCase(), o2.getTextForComparison().toLowerCase());
+				}
+			};
 		}
 	}),
-	CASE_INSENSITIVE_Z_A(new Comparator<SortLine>() {
-		@Override
-		public int compare(SortLine o1, SortLine o2) {
-			return COMPARATOR.compare(o2.getTextForComparison().toLowerCase(), o1.getTextForComparison().toLowerCase());
+	CASE_INSENSITIVE_Z_A(new ComparatorFactory() {
+		public Comparator<SortLine> prototype(Comparator comparator) {
+			return new Comparator<SortLine>() {
+				@Override
+				public int compare(SortLine o1, SortLine o2) {
+					return comparator.compare(o2.getTextForComparison().toLowerCase(), o1.getTextForComparison().toLowerCase());
+				}
+			};
 		}
 	}),
 	LINE_LENGTH_SHORT_LONG(new Comparator<SortLine>() {
@@ -73,12 +90,17 @@ public enum Sort {
 	});
 
 	private Comparator<SortLine> comparator;
+	private ComparatorFactory factory;
 
 	Sort(Comparator<SortLine> comparator) {
 		this.comparator = comparator;
 	}
 
-	public <T extends SortLine> List<T> sortLines(List<T> lines) {
+	Sort(ComparatorFactory factory) {
+		this.factory = factory;
+	}
+
+	public <T extends SortLine> List<T> sortLines(List<T> lines, SortSettings.ComparatorEnum comparatorEnum) {
 		switch (this) {
 			case SHUFFLE:
 				Collections.shuffle(lines);
@@ -87,18 +109,25 @@ public enum Sort {
 				Collections.reverse(lines);
 				break;
 			default:
-				Collections.sort(lines, comparator);
+				Collections.sort(lines, getComparator(comparatorEnum));
 		}
 		return lines;
 	}
 
+	protected Comparator getComparator(SortSettings.ComparatorEnum comparatorEnum) {
+		if (factory != null) {
+			return factory.prototype(comparatorEnum.comparator);
+		} else {
+			return comparator;
+		}
+	}
+
 
 	public Comparator<SortLine> getComparator() {
 		return comparator;
 	}
 
-	final static Comparator<String> COMPARATOR = new NaturalOrderComparator();
-
-	// final static Comparator alphanumComparator = Ordering.natural();
-
+	static abstract class ComparatorFactory {
+		abstract Comparator<SortLine> prototype(Comparator comparator);
+	}
 }
