@@ -8,8 +8,16 @@ public class SortSettingsTest {
 	public void test() throws Exception {
 		SortSettings sortSettings = new SortSettings(Sort.LINE_LENGTH_SHORT_LONG).ignoreLeadingSpaces(true).preserveLeadingSpaces(true).preserveTrailingSpecialCharacters(true).trailingChars(",;'|;[p;/|");
 
-		Assert.assertEquals("LINE_LENGTH_SHORT_LONG|REMOVE|true|true|true|,;'|;[p;/|", sortSettings.asString());
+		Assert.assertEquals("NATURAL|LINE_LENGTH_SHORT_LONG|REMOVE|true|true|true|,;'|;[p;/|", sortSettings.asString());
 		Assert.assertEquals(sortSettings, sortSettings.fromString(sortSettings.asString()));
 	}
 
+	@Test
+	public void test2() throws Exception {
+		SortSettings sortSettings = new SortSettings(Sort.LINE_LENGTH_SHORT_LONG).ignoreLeadingSpaces(true).preserveLeadingSpaces(true).preserveTrailingSpecialCharacters(true).trailingChars(",;'|;[p;/|");
+
+		Assert.assertEquals(sortSettings, sortSettings.fromString("LINE_LENGTH_SHORT_LONG|REMOVE|true|true|true|,;'|;[p;/|"));
+		Assert.assertEquals(sortSettings, sortSettings.fromString("NATURAL|LINE_LENGTH_SHORT_LONG|REMOVE|true|true|true|,;'|;[p;/|"));
+	}
+
 }
\ No newline at end of file
