@@ -0,0 +1,13 @@
+package osmedile.intellij.stringmanip.styles;
+
+import static org.junit.Assert.*;
+
+import org.junit.Test;
+
+public class ToCamelCaseActionTest {
+
+	@Test
+	public void testTransform() throws Exception {
+		assertEquals("foo", new ToCamelCaseAction(false).transform("foo"));
+	}
+}
\ No newline at end of file
