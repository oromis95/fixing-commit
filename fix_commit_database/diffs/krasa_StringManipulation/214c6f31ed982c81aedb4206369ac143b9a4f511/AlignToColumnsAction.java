@@ -9,6 +9,7 @@ import com.intellij.openapi.editor.actionSystem.EditorAction;
 import com.intellij.openapi.ui.Messages;
 import com.intellij.openapi.util.Pair;
 import com.intellij.openapi.util.TextRange;
+import org.jetbrains.annotations.NotNull;
 import org.jetbrains.annotations.Nullable;
 import osmedile.intellij.stringmanip.MyEditorWriteActionHandler;
 
@@ -27,6 +28,7 @@ public class AlignToColumnsAction extends EditorAction {
 		super(null);
 		if (setupHandler) {
 			this.setupHandler(new MyEditorWriteActionHandler<String>() {
+				@NotNull
 				@Override
 				public Pair<Boolean, String> beforeWriteAction(Editor editor, DataContext dataContext) {
 					String separator = chooseSeparator();
