@@ -11,7 +11,7 @@ public class ToPascalCaseAction extends AbstractCaseConvertingAction {
 	@Override
 	public String transformByLine(String s) {
         Style from = Style.from(s);
-        if (from == Style.PASCAL_CASE) {
+        if (from == Style.PASCAL_CASE || from == Style._SINGLE_WORD_WORD_CAPITALIZED) {
             return Style.CAMEL_CASE.transform(from, s);
         } else {
             return Style.PASCAL_CASE.transform(from, s);
