@@ -2,7 +2,6 @@ package osmedile.intellij.stringmanip.styles.custom;
 
 import org.jetbrains.annotations.NotNull;
 import osmedile.intellij.stringmanip.styles.Style;
-import osmedile.intellij.stringmanip.utils.Cloner;
 
 import java.util.*;
 
@@ -11,8 +10,7 @@ import static osmedile.intellij.stringmanip.styles.Style.*;
 public class DefaultActions {
 
 	public static final String SWITCH_STYLE_ACTION = "StringManipulation.SwitchStyleAction";
-	public static final List<CustomActionModel.Step> DEFAULT = Helper.getStyleSteps();
-	public static final Map<Style, Boolean> DEFAULT_AS_MAP = Helper.getStyleBooleanHashMap(DEFAULT);
+	public static final Map<Style, Boolean> DEFAULT_AS_MAP = Helper.getStyleBooleanHashMap(Helper.getDefaultSteps());
 
 	@NotNull
 	public static List<CustomActionModel> defaultActions() {
@@ -21,12 +19,29 @@ public class DefaultActions {
 		return customActionModels;
 	}
 
+	public static List<CustomActionModel.Step> getDefaultSteps() {
+		return Helper.getDefaultSteps();
+	}
+
+	@NotNull
+	public static CustomActionModel defaultSwitchCase() {
+		CustomActionModel e = CustomActionModel.create(DefaultActions.SWITCH_STYLE_ACTION);
+		resetDefaultSwitchCase(e);
+		return e;
+	}
+
+	@NotNull
+	public static void resetDefaultSwitchCase(CustomActionModel model) {
+		model.setName("Switch case");
+		model.setId(SWITCH_STYLE_ACTION);
+		model.setSteps(Helper.getDefaultSteps());
+	}
 
 	private static class Helper {
 
 		@NotNull
-		private static List<CustomActionModel.Step> getStyleSteps() {
-			return Arrays.asList(
+		private static List<CustomActionModel.Step> getDefaultSteps() {
+			return new ArrayList<>(Arrays.asList(
 				new CustomActionModel.Step(false, _UNKNOWN),
 				new CustomActionModel.Step(true, CAMEL_CASE),
 				new CustomActionModel.Step(true, KEBAB_LOWERCASE),
@@ -40,33 +55,19 @@ public class DefaultActions {
 				new CustomActionModel.Step(true, SENTENCE_CASE),
 				new CustomActionModel.Step(true, WORD_CAPITALIZED),
 				new CustomActionModel.Step(true, PASCAL_CASE)
-			);
+			));
 		}
 
 
 		@NotNull
-		private static HashMap<Style, Boolean> getStyleBooleanHashMap(List<CustomActionModel.Step> aDefault) {
+		private static Map<Style, Boolean> getStyleBooleanHashMap(List<CustomActionModel.Step> aDefault) {
 			HashMap<Style, Boolean> styleBooleanHashMap = new HashMap<>();
 			for (CustomActionModel.Step step : aDefault) {
 				styleBooleanHashMap.put(step.getStyleAsEnum(), step.isEnabled());
 			}
-			return styleBooleanHashMap;
+			return Collections.unmodifiableMap(styleBooleanHashMap);
 		}
 	}
-
-	@NotNull
-	public static CustomActionModel defaultSwitchCase() {
-		CustomActionModel e = CustomActionModel.create(DefaultActions.SWITCH_STYLE_ACTION);
-		resetDefaultSwitchCase(e);
-		return e;
-	}
-
-	@NotNull
-	public static void resetDefaultSwitchCase(CustomActionModel model) {
-		model.setName("Switch case");
-		model.setId(SWITCH_STYLE_ACTION);
-		model.setSteps(Cloner.deepClone(DEFAULT));
-	}
-
+	          
 
 }
