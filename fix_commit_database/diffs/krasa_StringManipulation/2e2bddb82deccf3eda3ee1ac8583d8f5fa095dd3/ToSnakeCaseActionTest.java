@@ -1,15 +1,24 @@
 package osmedile.intellij.stringmanip.styles;
 
+import org.junit.Before;
 import org.junit.Test;
 
 import static org.junit.Assert.assertEquals;
 
-public class ToSnakeCaseActionTest {
+public class ToSnakeCaseActionTest extends CaseSwitchingTest {
     protected ToSnakeCaseAction action;
 
-    @Test
-    public void testTransform() throws Exception {
+	@Override
+	@Before
+	public void setUp() throws Exception {
+		super.setUp();
 		action = new ToSnakeCaseAction(false);
+	}
+
+	@Test
+    public void testTransform() throws Exception {
+		caseSwitchingSettings.setSeparatorBeforeDigit(false);
+		
 		assertEquals("11_foo22_foo_bar33_bar44_foo55_x6_y7_z",
 				action.transformByLine("11foo22fooBAR33BAR44foo55x6Y7Z"));
 		assertEquals("transform_db", action.transformByLine("transformDB"));
@@ -21,4 +30,12 @@ public class ToSnakeCaseActionTest {
 		assertEquals("2_v2_counter_3", action.transformByLine("2_V2_COUNTER_3"));
 		assertEquals("\\my\\app_bundle\\app\\twig\\google_tag_manager_data_layer", action.transformByLine("\\My\\AppBundle\\App\\Twig\\GoogleTagManagerDataLayer"));
 	}
+
+	@Test
+	public void testTransform2() throws Exception {
+		caseSwitchingSettings.setSeparatorBeforeDigit(true);
+
+		assertEquals("11_foo_22_foo_bar_33_bar_44_foo_55_x_6_y_7_z",
+			action.transformByLine("11foo22fooBAR33BAR44foo55x6Y7Z"));
+	}
 }
\ No newline at end of file
