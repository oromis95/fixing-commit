@@ -11,10 +11,10 @@ public class ToSnakeCaseAction extends AbstractCaseConvertingAction {
 	@Override
 	public String transformByLine(String s) {
 		Style from = Style.from(s);
-		if (from == Style.UNDERSCORE_LOWERCASE) {
+		if (from == Style.SNAKE_CASE) {
 			return Style.CAMEL_CASE.transform(from, s);
 		} else {
-			return Style.UNDERSCORE_LOWERCASE.transform(from, s);
+			return Style.SNAKE_CASE.transform(from, s);
 		}
 	}
 
