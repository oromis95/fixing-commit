@@ -4,11 +4,12 @@ public class SwitchStyleAction extends AbstractCaseConvertingAction {
 
 	public static Style[][] transformation = new Style[][]{
 		{Style.HYPHEN_LOWERCASE, Style.HYPHEN_UPPERCASE},
-		{Style.HYPHEN_UPPERCASE, Style.UNDERSCORE_LOWERCASE},
-		{Style.UNDERSCORE_LOWERCASE, Style.SCREAMING_SNAKE_CASE},
+		{Style.HYPHEN_UPPERCASE, Style.SNAKE_CASE},
+		{Style.SNAKE_CASE, Style.SCREAMING_SNAKE_CASE},
 		{Style.SCREAMING_SNAKE_CASE, Style.DOT},
 		{Style.DOT, Style.WORD_LOWERCASE},
-		{Style.WORD_LOWERCASE, Style.WORD_CAPITALIZED},
+		{Style.WORD_LOWERCASE, Style.SENTENCE_CASE},
+		{Style.SENTENCE_CASE, Style.WORD_CAPITALIZED},
 		{Style.WORD_CAPITALIZED, Style.PASCAL_CASE},
 		{Style.PASCAL_CASE, Style.CAMEL_CASE},
 		{Style.CAMEL_CASE, Style.HYPHEN_LOWERCASE},
