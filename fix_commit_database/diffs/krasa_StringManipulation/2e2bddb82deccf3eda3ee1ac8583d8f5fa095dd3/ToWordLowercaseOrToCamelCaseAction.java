@@ -1,17 +1,17 @@
 package osmedile.intellij.stringmanip.styles;
 
-public class ToCamelCaseOrToWordLowercaseAction extends AbstractCaseConvertingAction {
-    public ToCamelCaseOrToWordLowercaseAction() {
+public class ToWordLowercaseOrToCamelCaseAction extends AbstractCaseConvertingAction {
+    public ToWordLowercaseOrToCamelCaseAction() {
     }
 
-    public ToCamelCaseOrToWordLowercaseAction(boolean setupHandler) {
+    public ToWordLowercaseOrToCamelCaseAction(boolean setupHandler) {
         super(setupHandler);
     }
 
     @Override
     public String transformByLine(String s) {
         Style from = Style.from(s);
-        if (from == Style.CAMEL_CASE) {
+        if (from != Style.WORD_LOWERCASE) {
             return Style.WORD_LOWERCASE.transform(from, s);
         } else {
             return Style.CAMEL_CASE.transform(from, s);
