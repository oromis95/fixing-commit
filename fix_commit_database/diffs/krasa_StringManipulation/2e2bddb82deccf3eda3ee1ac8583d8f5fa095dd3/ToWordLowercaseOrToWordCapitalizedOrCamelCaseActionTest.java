@@ -3,8 +3,8 @@ package osmedile.intellij.stringmanip.styles;
 import org.junit.Assert;
 import org.junit.Test;
 
-public class ToCamelCaseOrToWordLowercaseActionTest {
-    private static final ToCamelCaseOrToWordLowercaseAction ACTION = new ToCamelCaseOrToWordLowercaseAction(false);
+public class ToWordLowercaseOrToWordCapitalizedOrCamelCaseActionTest extends CaseSwitchingTest {
+    private static final ToWordLowercaseOrToCamelCaseAction ACTION = new ToWordLowercaseOrToCamelCaseAction(false);
 
     @Test
     public void transformByLine() throws Exception {
