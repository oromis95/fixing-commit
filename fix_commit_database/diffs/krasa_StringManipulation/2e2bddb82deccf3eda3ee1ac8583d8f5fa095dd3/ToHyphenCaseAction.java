@@ -13,7 +13,7 @@ public class ToHyphenCaseAction extends AbstractCaseConvertingAction {
 	public String transformByLine(String s) {
 		Style from = Style.from(s);
 		if (from == Style.HYPHEN_LOWERCASE) {
-			return Style.UNDERSCORE_LOWERCASE.transform(from, s);
+			return Style.SNAKE_CASE.transform(from, s);
 		}
 		return Style.HYPHEN_LOWERCASE.transform(from, s);
 	}
