@@ -1,8 +1,8 @@
 package osmedile.intellij.stringmanip.styles;
 
-import org.junit.Test;
+import static org.junit.Assert.assertEquals;
 
-import static org.junit.Assert.*;
+import org.junit.Test;
 
 public class ToSnakeCaseActionTest {
     protected ToSnakeCaseAction action;
@@ -10,11 +10,11 @@ public class ToSnakeCaseActionTest {
     @Test
     public void testTransform() throws Exception {
         action = new ToSnakeCaseAction(false);
-        assertEquals("2_c_2", action.transform("2_C_2"));
-        assertEquals("organ_vizepraesident_1", action.transform("organ-vizepraesident-1"));
-        assertEquals("foo_bar", action.transform("FOO-BAR"));
-        assertEquals("2_v2_counter_3", action.transform("2-v2-counter-3"));
-        assertEquals("2_v2_counter_3", action.transform("2_v2_Counter_3"));
-        assertEquals("2_v2_counter_3", action.transform("2_V2_COUNTER_3"));
+		assertEquals("2_c_2", action.transformByLine("2_C_2"));
+		assertEquals("organ_vizepraesident_1", action.transformByLine("organ-vizepraesident-1"));
+		assertEquals("foo_bar", action.transformByLine("FOO-BAR"));
+		assertEquals("2_v2_counter_3", action.transformByLine("2-v2-counter-3"));
+		assertEquals("2_v2_counter_3", action.transformByLine("2_v2_Counter_3"));
+		assertEquals("2_v2_counter_3", action.transformByLine("2_V2_COUNTER_3"));
     }
 }
\ No newline at end of file
