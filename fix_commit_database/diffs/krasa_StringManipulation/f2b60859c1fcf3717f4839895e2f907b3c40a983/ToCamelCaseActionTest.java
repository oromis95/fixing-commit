@@ -11,21 +11,21 @@ public class ToCamelCaseActionTest {
     @Test
     public void testTransform() throws Exception {
         action = new ToCamelCaseAction(false);
-        assertEquals("foo", action.transform("foo"));
-		assertEquals("!@#$%^&*)(*&|+!!!!!foo!!!!", action.transform("!@#$%^&*)(*&|+!!!!!FOO!!!!"));
-		assertEquals("public", action.transform("PUBLIC"));
+		assertEquals("foo", action.transformByLine("foo"));
+		assertEquals("!@#$%^&*)(*&|+!!!!!foo!!!!", action.transformByLine("!@#$%^&*)(*&|+!!!!!FOO!!!!"));
+		assertEquals("public", action.transformByLine("PUBLIC"));
 
-        assertEquals("testFlexibleQuery", action.transform("testFLEXIBLE_QUERY"));
+		assertEquals("testFlexibleQuery", action.transformByLine("testFLEXIBLE_QUERY"));
         assertEquals("testFlexibleQueryProductsForWorkflowAttachment",
-                action.transform("testFlexibleQuery_PRODUCTS_FOR_WORKFLOW_ATTACHMENT"));
+				action.transformByLine("testFlexibleQuery_PRODUCTS_FOR_WORKFLOW_ATTACHMENT"));
 
-        assertEquals("thisIsAText", action.transform("This is a text"));
+		assertEquals("thisIsAText", action.transformByLine("This is a text"));
 
         //this is ugly but nothing can be done about that.
-        assertEquals("whOAhATeSt", action.transform("WhOAh a TeSt"));
-        assertEquals("whOAhATeSt", action.transform("WhOAh_a_TeSt"));
-        assertEquals("whOAhATeSt", action.transform("WhOAh a_TeSt"));
-		assertEquals("'closeBsAlert'", action.transform("'Close Bs Alert'"));
-		assertEquals("\"closeBsAlert\"", action.transform("\"Close Bs Alert\""));
+		assertEquals("whOAhATeSt", action.transformByLine("WhOAh a TeSt"));
+		assertEquals("whOAhATeSt", action.transformByLine("WhOAh_a_TeSt"));
+		assertEquals("whOAhATeSt", action.transformByLine("WhOAh a_TeSt"));
+		assertEquals("'closeBsAlert'", action.transformByLine("'Close Bs Alert'"));
+		assertEquals("\"closeBsAlert\"", action.transformByLine("\"Close Bs Alert\""));
     }
 }
\ No newline at end of file
