@@ -2,6 +2,7 @@ package osmedile.intellij.stringmanip;
 
 import org.junit.Assert;
 import org.junit.Test;
+
 import osmedile.intellij.stringmanip.unused.WordsCamelCaseToWordsAction;
 
 /**
@@ -11,6 +12,6 @@ public class WordsCamelCaseToWordsActionTest {
     @Test
     public void testTransform() throws Exception {
         WordsCamelCaseToWordsAction wordsCamelCaseToWordsAction = new WordsCamelCaseToWordsAction(false);
-        Assert.assertEquals("Foo bar Wee All", wordsCamelCaseToWordsAction.transform("FooBar Wee All"));
+		Assert.assertEquals("Foo bar Wee All", wordsCamelCaseToWordsAction.transformByLine("FooBar Wee All"));
     }
 }
