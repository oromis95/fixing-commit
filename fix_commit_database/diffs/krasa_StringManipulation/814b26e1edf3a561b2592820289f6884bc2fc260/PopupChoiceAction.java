@@ -4,6 +4,8 @@ import com.intellij.codeInsight.lookup.LookupManager;
 import com.intellij.openapi.actionSystem.ActionGroup;
 import com.intellij.openapi.actionSystem.ActionManager;
 import com.intellij.openapi.actionSystem.AnActionEvent;
+import com.intellij.openapi.actionSystem.CommonDataKeys;
+import com.intellij.openapi.editor.Editor;
 import com.intellij.openapi.project.DumbAwareAction;
 import com.intellij.openapi.project.Project;
 import com.intellij.openapi.ui.popup.JBPopupFactory;
@@ -32,9 +34,15 @@ public class PopupChoiceAction extends DumbAwareAction {
     @Override
     public void update(AnActionEvent e) {
         super.update(e);
-        Project project = getEventProject(e);
+		Editor editor = CommonDataKeys.EDITOR.getData(e.getDataContext());
+		if (editor == null) {
+			e.getPresentation().setEnabled(false);
+			return;
+		}
+		Project project = getEventProject(e);
         if (project != null) {
             e.getPresentation().setEnabled(LookupManager.getInstance(project).getActiveLookup() == null);
-        }
+			return;
+		}
     }
 }
