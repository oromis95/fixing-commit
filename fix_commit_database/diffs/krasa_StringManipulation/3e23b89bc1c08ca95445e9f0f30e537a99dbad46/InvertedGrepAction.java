@@ -0,0 +1,12 @@
+package osmedile.intellij.stringmanip.filter;
+
+public class InvertedGrepAction extends GrepAction {
+
+	public InvertedGrepAction() {
+		super(new GrepFilter() {
+			public boolean execute(String textPart, String grepos) {
+				return !textPart.contains(grepos);
+			}
+		});
+	}
+}
\ No newline at end of file
