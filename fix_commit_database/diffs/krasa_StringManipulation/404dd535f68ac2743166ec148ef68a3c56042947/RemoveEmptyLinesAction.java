@@ -1,27 +1,29 @@
 package osmedile.intellij.stringmanip.filter;
 
-import java.util.ArrayList;
-import java.util.Collection;
-
 import com.intellij.openapi.actionSystem.DataContext;
 import com.intellij.openapi.editor.Editor;
 import com.intellij.openapi.editor.SelectionModel;
-import com.intellij.openapi.editor.actionSystem.EditorAction;
 import com.intellij.openapi.editor.actionSystem.EditorWriteActionHandler;
-
+import osmedile.intellij.stringmanip.MyApplicationComponent;
+import osmedile.intellij.stringmanip.MyEditorAction;
 import osmedile.intellij.stringmanip.utils.StringUtils;
 
+import java.util.ArrayList;
+import java.util.Collection;
+
 /**
  * @author Olivier Smedile
  * @version $Id: RemoveEmptyLinesAction.java 60 2008-04-18 06:51:03Z osmedile $
  */
-public class RemoveEmptyLinesAction extends EditorAction {
+public class RemoveEmptyLinesAction extends MyEditorAction {
 
 	public RemoveEmptyLinesAction() {
-		super(new EditorWriteActionHandler(true) {
+		super(null);
+		setupHandler(new EditorWriteActionHandler(true) {
 
 			@Override
 			public void executeWriteAction(Editor editor, DataContext dataContext) {
+				MyApplicationComponent.setAction(getActionClass());
 
 				// Column mode not supported
 				if (editor.isColumnMode()) {
@@ -46,7 +48,7 @@ public class RemoveEmptyLinesAction extends EditorAction {
 
 					final String s = StringUtils.join(res, '\n');
 					editor.getDocument().replaceString(selectionModel.getSelectionStart(),
-							selectionModel.getSelectionEnd(), s);
+						selectionModel.getSelectionEnd(), s);
 				}
 
 			}
