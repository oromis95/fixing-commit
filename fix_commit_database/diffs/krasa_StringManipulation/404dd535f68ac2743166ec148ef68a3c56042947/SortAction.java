@@ -2,12 +2,12 @@ package osmedile.intellij.stringmanip.sort;
 
 import com.intellij.openapi.actionSystem.DataContext;
 import com.intellij.openapi.editor.Editor;
-import com.intellij.openapi.editor.actionSystem.EditorAction;
 import com.intellij.openapi.ui.DialogWrapper;
 import com.intellij.openapi.util.Pair;
 import org.jetbrains.annotations.NotNull;
 import org.jetbrains.annotations.Nullable;
 import osmedile.intellij.stringmanip.MultiCaretHandlerHandler;
+import osmedile.intellij.stringmanip.MyEditorAction;
 import osmedile.intellij.stringmanip.sort.support.SortLines;
 import osmedile.intellij.stringmanip.sort.support.SortSettings;
 import osmedile.intellij.stringmanip.sort.support.SortTypeDialog;
@@ -15,7 +15,7 @@ import osmedile.intellij.stringmanip.sort.support.SortTypeDialog;
 import javax.swing.*;
 import java.util.List;
 
-public class SortAction extends EditorAction {
+public class SortAction extends MyEditorAction {
 	public static final String STORE_KEY = "StringManipulation.SortAction.SortSettings";
 	private String storeKey = STORE_KEY;
 
@@ -30,7 +30,7 @@ public class SortAction extends EditorAction {
 
 	protected SortAction(boolean setupHandler) {
 		super(null);
-		this.setupHandler(new MultiCaretHandlerHandler<SortSettings>() {
+		this.setupHandler(new MultiCaretHandlerHandler<SortSettings>(getActionClass()) {
 			@NotNull
 			@Override
 			protected Pair beforeWriteAction(Editor editor, DataContext dataContext) {
