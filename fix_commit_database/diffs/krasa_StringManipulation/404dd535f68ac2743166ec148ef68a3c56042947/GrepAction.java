@@ -4,12 +4,12 @@ import com.intellij.openapi.actionSystem.DataContext;
 import com.intellij.openapi.editor.Caret;
 import com.intellij.openapi.editor.Editor;
 import com.intellij.openapi.editor.SelectionModel;
-import com.intellij.openapi.editor.actionSystem.EditorAction;
 import com.intellij.openapi.ui.Messages;
 import com.intellij.openapi.util.Pair;
 import com.intellij.openapi.util.text.StringUtil;
 import org.jetbrains.annotations.NotNull;
 import org.jetbrains.annotations.Nullable;
+import osmedile.intellij.stringmanip.MyEditorAction;
 import osmedile.intellij.stringmanip.MyEditorWriteActionHandler;
 import osmedile.intellij.stringmanip.utils.StringUtils;
 
@@ -20,7 +20,7 @@ import java.util.Collection;
  * @author Olivier Smedile
  * @version $Id: GrepAction.java 60 2008-04-18 06:51:03Z osmedile $
  */
-public class GrepAction extends EditorAction {
+public class GrepAction extends MyEditorAction {
 
 	public GrepAction() {
 		this(new GrepFilter() {
@@ -36,7 +36,8 @@ public class GrepAction extends EditorAction {
 	}
 
 	public GrepAction(final GrepFilter shouldAdd) {
-		super(new MyEditorWriteActionHandler<String>() {
+		super(null);
+		setupHandler(new MyEditorWriteActionHandler<String>(getActionClass()) {
 			@NotNull
 			@Override
 			protected Pair<Boolean, String> beforeWriteAction(Editor editor, DataContext dataContext) {
