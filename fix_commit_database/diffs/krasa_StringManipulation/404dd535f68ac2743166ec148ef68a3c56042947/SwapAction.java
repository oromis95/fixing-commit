@@ -3,19 +3,19 @@ package osmedile.intellij.stringmanip.swap;
 import com.intellij.openapi.actionSystem.DataContext;
 import com.intellij.openapi.editor.Caret;
 import com.intellij.openapi.editor.Editor;
-import com.intellij.openapi.editor.actionSystem.EditorAction;
 import com.intellij.openapi.util.Pair;
 import org.jetbrains.annotations.NotNull;
 import org.jetbrains.annotations.Nullable;
+import osmedile.intellij.stringmanip.MyEditorAction;
 import osmedile.intellij.stringmanip.MyEditorWriteActionHandler;
 
-public class SwapAction extends EditorAction {
+public class SwapAction extends MyEditorAction {
 
 	String lastSeparator = ",";
 
 	protected SwapAction() {
 		super(null);
-		this.setupHandler(new MyEditorWriteActionHandler<SwapActionExecutor>() {
+		this.setupHandler(new MyEditorWriteActionHandler<SwapActionExecutor>(getActionClass()) {
 
 			@NotNull
 			protected Pair<Boolean, SwapActionExecutor> beforeWriteAction(Editor editor, DataContext dataContext) {
