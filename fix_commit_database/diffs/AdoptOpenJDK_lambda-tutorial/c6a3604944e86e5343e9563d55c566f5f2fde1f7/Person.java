@@ -2,11 +2,19 @@ package org.adoptopenjdk.lambda.tutorial.exercise2;
 
 public final class Person {
 
-    public final String name;
-    public final int age;
+    private final String name;
+    private final int age;
 
     public Person(String name, int age) {
         this.name = name;
         this.age = age;
     }
+
+    public int getAge() {
+        return age;
+    }
+    
+    public String getName() {
+        return name;
+    }
 }
