@@ -35,12 +35,14 @@ import static org.hamcrest.Matchers.not;
 
 /**
  * Exercise 2 - Filtering and Collecting
- *
+ * <p>
  * Filtering is one of the most popular operations performed on collections. When coupled with mapping/transforming,
  * which we'll learn about shortly, it provides a lot of power.
- *
+ * </p>
+ * <p>
  * Consider how often you have written code like this:
- *
+ * <pre>
+ * {@code
  *     List<String> things = new ArrayList<>();
  *     for (String s: otherThings) {
  *         if (satisfiesSomeCondition(s)) {
@@ -48,52 +50,72 @@ import static org.hamcrest.Matchers.not;
  *         }
  *     }
  *     return things;
- *
+ * }
+ * </pre>
+ * </p>
+ * <p>
  * This is a common pattern, and it's one that is made more concise with lambdas. The common parts of the pattern are:
- *  - creating an empty, destination collection
- *  - iterating over the entire source collection
- *  - deciding whether to include each item, according to some test which evaluates to a boolean
- *      - items which pass the test are copied to the destination collection
- *      - items which fail are ignored
- *
+ * <ul>
+ *     <li>creating an empty, destination collection</li>
+ *     <li>iterating over the entire source collection</li>
+ *     <li>deciding whether to include each item, according to some test which evaluates to a boolean
+ *         <ul>
+ *             <li>items which pass the test are copied to the destination collection</li>
+ *             <li>items which fail are ignored</li>
+ *         </ul>
+ *     </li>
+ * </ul>
+ * </p>
+ * <p>
  * These steps will rarely change, but think of how we represent that in pre JDK 8 code. We use a for (or while) loop,
  * and an if statement. Those parts are syntax that until now we couldn't escape, and they're almost always identical.
- *
+ * </p>
+ * <p>
  * The remaining steps (creating a collection, adding them) do have some variation: which destination collection to
  * create? how should an instance be constructed? which method is used to transfer elements to the new list? These
  * decisions generally lead to code which differs slightly between examples.
- *
+ * </p>
+ * <p>
  * In post-JDK 8 code, one potential solution which matches the above problem is:
  *
+ * <pre>
+ * {@code
  *     return otherThings.stream()
  *         .filter(s -> satisfiesSomeCondition(s))
  *         .collect(Collectors.toList());
- *
+ * }
+ * </pre>
+ * </p>
+ * <p>
  * In this case, unlike the previous example where we used forEach directly from List, in this case we have to "open up"
- * the Streams API by calling the stream() method defined on Collection. This provides many operations which take
+ * the Streams API by calling the <code>stream()</code> method defined on Collection. This provides many operations which take
  * advantage of the lambda syntax.
- *
- * One such method available on Stream is filter(). In the first code snippet, we perform our filtering using
- * if (satisfiesSomeCondition(s)). The code inside the if condition can be referred to as a "predicate". That is, a
- * function which takes some input, and returns a boolean. This is how the filter() method is defined: it takes a
+ * </p>
+ * <p>
+ * One such method available on Stream is <code>filter()</code>. In the first code snippet, we perform our filtering using
+ * <code>if (satisfiesSomeCondition(s))</code>. The code inside the if condition can be referred to as a "predicate". That is, a
+ * function which takes some input, and returns a boolean. This is how the <code>filter()</code> method is defined: it takes a
  * lambda, which, when given an element from the Stream, returns a boolean. Internally, the filter method has the
  * equivalent semantics of the for loop, and the if statement. It will return a new Stream, containing
  * all the elements which passed the test (or "satisfied the predicate") from the source Stream.
- *
- * The last piece in this snippet of code (".collect(Collectors.toList())") is what takes the place of constructing
- * the initial empty list, and invoking the add() method. The code used in these examples depends on what type of
+ * </p>
+ * <p>
+ * The last piece in this snippet of code (<code>.collect(Collectors.toList())</code>) is what takes the place of constructing
+ * the initial empty list, and invoking the <code>add()</code> method. The code used in these examples depends on what type of
  * list is best suited for the resultant list, and also, how best to transfer elements from the result Stream into
- * the new list. The very common case, of creating a new ArrayList, and simply calling add() for each element, is
+ * the new list. The very common case, of creating a new ArrayList, and simply calling <code>add()</code> for each element, is
  * fulfilled by Collectors.toList.
- *
+ * </p>
+ * <p>
  * If you want your result to be a different type of collection, or potentially, something completely outwith the
  * collections library, and you wanted to control over how those elements were transferred, you would write your own
  * collector. In most cases, and in these examples, we'll use only simple, pre-existing collectors, available in the
  * JDK.
- *
- * The below tests can be made to pass using Stream's filter and collect methods. Try to make them pass without using
+ * </p>
+ * <p>
+ * The tests below can be made to pass using Stream's filter and collect methods. Try to make them pass without using
  * a loop, or an if statement.
- *
+ * </p>
  *
  * @see Collection#stream()
  * @see Stream#filter(Predicate)
@@ -109,9 +131,10 @@ import static org.hamcrest.Matchers.not;
 public class Exercise_2_Test {
 
     /**
-     * Use Stream.filter() to produce a list containing only those Persons eligible to vote.
+     * Use <code>Stream.filter()</code> to produce a list containing only those 
+     * Persons eligible to vote.
      *
-     * @see Person#age
+     * @see Person#getAge()
      */
     @Test
     public void getAllPersonsEligibleToVote() {
@@ -130,16 +153,19 @@ public class Exercise_2_Test {
     }
 
     /**
+     * <p>
      * Use Stream.filter() to find all the voters residing in a given district.
-     *
+     * </p>
+     * <p>
      * The resulting collection is to be used for quick lookups to find if a given
      * voter resides in a district. Performance measurements indicate we should
      * prefer the result to be a java.util.Set. Use Stream.collect() to produce a
      * Set containing the result, rather than a List.
-     *
-     * HINT: sometimes type inference is "not there yet", in either the IDE or javac, help out the compiler
-     * with explicit generic arguments if you have to.
-     *
+     * </p>
+     * <p>
+     * HINT: sometimes type inference is "not there yet", in either the IDE or javac, 
+     * help out the compiler with explicit generic arguments if you have to.
+     * </p>
      * @see Stream#collect(java.util.stream.Collector)
      * @see java.util.stream.Collectors#toSet()
      *
@@ -189,12 +215,14 @@ public class Exercise_2_Test {
     }
 
     /**
+     * <p>
      * Ensure that the Set returned cannot be modified by callers by wrapping the result
      * in Collections.unmodifiableSet().
-     *
+     * </p>
+     * <p>
      * The Streams API does not provide a way to wrap the final result, as one of its operations. So just wrap the
      * result in an unmodifiableSet yourself. Sometimes it's just as important to know what an API _doesn't_ do.
-     *
+     * </p>
      * @see Stream#collect(java.util.stream.Collector)
      * @see java.util.stream.Collectors#toSet()
      * @see Collections#unmodifiableSet(java.util.Set)
@@ -218,15 +246,15 @@ public class Exercise_2_Test {
     // Test helpers
 
     private static Matcher<Person> aPersonNamed(String name) {
-        return FeatureMatchers.from(is(name), "a person", "name", p -> p.name);
+        return FeatureMatchers.from(is(name), "a person", "name", person -> person.getName());
     }
 
     private static Matcher<RegisteredVoter> aVoterWithId(String name) {
-        return FeatureMatchers.from(is(name), "a voter", "electorId", v -> v.electorId);
+        return FeatureMatchers.from(is(name), "a voter", "electorId", voter -> voter.getElectorId());
     }
 
     private static Matcher<Ballot> spoiled() {
-        return FeatureMatchers.from(equalTo(Boolean.TRUE), "a spoiled ballot", "isSpoiled", b -> b.isSpoiled);
+        return FeatureMatchers.from(equalTo(Boolean.TRUE), "a spoiled ballot", "isSpoiled", ballot -> ballot.isSpoiled());
     }
 
 }
\ No newline at end of file
