@@ -44,7 +44,7 @@ public class DateTimeZoneConverter implements JsonSerializer<DateTimeZone>, Json
   /**
    * Gson invokes this call-back method during serialization when it encounters a field of the
    * specified type. <p>
-   * <p>
+   *
    * In the implementation of this call-back method, you should consider invoking
    * {@link JsonSerializationContext#serialize(Object, Type)} method to create JsonElements for any
    * non-trivial field of the {@code src} object. However, you should never invoke it on the
@@ -63,7 +63,7 @@ public class DateTimeZoneConverter implements JsonSerializer<DateTimeZone>, Json
   /**
    * Gson invokes this call-back method during deserialization when it encounters a field of the
    * specified type. <p>
-   * <p>
+   *
    * In the implementation of this call-back method, you should consider invoking
    * {@link JsonDeserializationContext#deserialize(JsonElement, Type)} method to create objects
    * for any non-trivial field of the returned object. However, you should never invoke it on the
