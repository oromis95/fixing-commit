@@ -86,6 +86,11 @@ public class LocalDateConverter implements JsonSerializer<LocalDate>, JsonDeseri
   public LocalDate deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
       throws JsonParseException
   {
+      /* Do not try to deserialize null or empty values */
+      if(json.getAsString() == null || json.getAsString().isEmpty()) {
+          return null;
+      }
+      
     final DateTimeFormatter fmt = DateTimeFormat.forPattern(PATTERN);
     return fmt.parseLocalDate(json.getAsString());
   }
