@@ -83,6 +83,11 @@ public class DateMidnightConverter implements JsonSerializer<DateMidnight>, Json
   public DateMidnight deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
       throws JsonParseException
   {
+      /* Do not try to deserialize null or empty values */
+      if(json.getAsString() == null || json.getAsString().isEmpty()) {
+          return null;
+      }
+      
     final DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
     return new DateMidnight(fmt.parseDateTime(json.getAsString()));
   }
