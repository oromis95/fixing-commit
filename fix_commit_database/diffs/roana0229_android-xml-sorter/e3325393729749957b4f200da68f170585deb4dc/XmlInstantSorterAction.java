@@ -2,6 +2,8 @@ package org.roana0229.android_xml_sorter;
 
 import com.intellij.ide.util.PropertiesComponent;
 import com.intellij.openapi.actionSystem.AnActionEvent;
+import com.intellij.openapi.actionSystem.PlatformDataKeys;
+import com.intellij.openapi.editor.Editor;
 import com.intellij.openapi.project.Project;
 
 import static org.roana0229.android_xml_sorter.XmlSorterDialog.*;
@@ -10,11 +12,14 @@ public class XmlInstantSorterAction extends XmlSorterAction {
 
     @Override
     public void actionPerformed(AnActionEvent event) {
+
         final Project project = getEventProject(event);
+        final Editor editor = event.getData(PlatformDataKeys.EDITOR);
         XmlSorterDialog dialog = new XmlSorterDialog(project);
 
         PropertiesComponent pc = PropertiesComponent.getInstance();
-        execute(event,
+        execute(project,
+                editor,
                 pc.getInt(PC_KEY_INPUT_CASE, 0) == 0,
                 dialog.getPrefixSpacePositionValueAt(pc.getInt(PC_KEY_PREFIX_SPACE_POS, 0)),
                 pc.getBoolean(PC_KEY_SPACE_BETWEEN_PREFIX, true),
