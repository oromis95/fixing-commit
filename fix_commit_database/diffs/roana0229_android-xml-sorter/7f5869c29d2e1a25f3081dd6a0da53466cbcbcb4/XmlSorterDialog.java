@@ -3,6 +3,8 @@ import com.intellij.openapi.ui.DialogWrapper;
 import org.jetbrains.annotations.Nullable;
 
 import javax.swing.*;
+import java.awt.event.ActionEvent;
+import java.awt.event.ActionListener;
 
 public class XmlSorterDialog extends DialogWrapper {
 
@@ -12,9 +14,11 @@ public class XmlSorterDialog extends DialogWrapper {
     private JPanel mMainPanel;
     private JCheckBox mInsertSpaceCheckBox;
     private JComboBox mInputCaseBox;
+    private JComboBox mPrefixSpacePositionBox;
     private JCheckBox mInsertXmlInfoCheckBox;
     private JCheckBox mDeleteCommentCheckBox;
     private JComboBox mCodeIndentBox;
+    private JLabel mPrefixSpacePositionLabel;
 
     protected XmlSorterDialog(@Nullable Project project) {
         super(project, true);
@@ -28,20 +32,24 @@ public class XmlSorterDialog extends DialogWrapper {
         mInsertSpaceCheckBox.setSelected(true);
         // Snake Case
         mInputCaseBox.setSelectedIndex(0);
+        mPrefixSpacePositionBox.setSelectedIndex(0);
         mInsertXmlInfoCheckBox.setSelected(true);
         mDeleteCommentCheckBox.setSelected(false);
         // indent 4
         mCodeIndentBox.setSelectedIndex(1);
 
         // TODO: CamelCase対応
-        mInputCaseBox.setVisible(false);
-//        mInputCaseBox.setEnabled(true);
-//        mInsertSpaceCheckBox.addActionListener(new ActionListener() {
-//            @Override
-//            public void actionPerformed(ActionEvent actionEvent) {
+        mInputCaseBox.setEnabled(false);
+
+        mInsertSpaceCheckBox.addActionListener(new ActionListener() {
+            @Override
+            public void actionPerformed(ActionEvent actionEvent) {
+                // TODO: CamelCase対応
 //                mInputCaseBox.setEnabled(mInsertSpaceCheckBox.isSelected());
-//            }
-//        });
+                mPrefixSpacePositionLabel.setEnabled(mInsertSpaceCheckBox.isSelected());
+                mPrefixSpacePositionBox.setEnabled(mInsertSpaceCheckBox.isSelected());
+            }
+        });
     }
 
     @Nullable
@@ -58,6 +66,10 @@ public class XmlSorterDialog extends DialogWrapper {
         return mInputCaseBox.getSelectedIndex() == 0;
     }
 
+    public int getPrefixSpacePosition() {
+        return Integer.parseInt(mPrefixSpacePositionBox.getSelectedItem().toString());
+    }
+
     public boolean enableInsertXmlInfo() {
         return mInsertXmlInfoCheckBox.isSelected();
     }
