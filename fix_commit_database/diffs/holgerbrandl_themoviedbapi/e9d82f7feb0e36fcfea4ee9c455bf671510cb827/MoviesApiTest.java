@@ -101,7 +101,7 @@ public class MoviesApiTest extends AbstractTmdbApiTest {
         MovieDb movie = tmdb.getMovies().getMovie(10191, "fr", TmdbMovies.MovieMethod.keywords);
         List<Keyword> result = movie.getKeywords();
         assertFalse("No keywords found", result.isEmpty());
-        assertEquals(7, result.size());
+        assertEquals(8, result.size());
     }
 
 
