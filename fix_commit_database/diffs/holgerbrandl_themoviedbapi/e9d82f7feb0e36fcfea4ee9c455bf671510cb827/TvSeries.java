@@ -71,6 +71,9 @@ public class TvSeries extends AbstractTvElement implements Multi {
     @JsonProperty("seasons")
     private List<TvSeason> seasons;
 
+    @JsonProperty("rating")
+    private float userRating;
+
     @JsonProperty("vote_average")
     private float voteAverage;
 
@@ -166,6 +169,11 @@ public class TvSeries extends AbstractTvElement implements Multi {
     }
 
 
+    public float getUserRating() {
+        return userRating;
+    }
+
+
     public float getVoteAverage() {
         return voteAverage;
     }
