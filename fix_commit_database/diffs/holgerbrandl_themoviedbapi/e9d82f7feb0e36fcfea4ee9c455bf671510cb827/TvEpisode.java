@@ -14,12 +14,21 @@ public class TvEpisode extends AbstractTvElement {
     private String airDate;
 
 
+    @JsonProperty("show_id")
+    private int seriesId;
+
+    @JsonProperty("season_number")
+    private Integer seasonNumber;
+
     @JsonProperty("episode_number")
     private int episodeNumber;
 
     @JsonProperty("still_path")
     private String stillPath;
 
+    @JsonProperty("rating")
+    private float userRating;
+
 
     @JsonProperty("vote_average")
     private float voteAverage;
@@ -38,6 +47,19 @@ public class TvEpisode extends AbstractTvElement {
     }
 
 
+    /**
+     * Just set when fetching rated tv-episodes.
+     */
+    public Integer getSeriesId() {
+        return seriesId;
+    }
+
+
+    public int getSeasonNumber() {
+        return seasonNumber;
+    }
+
+
     public int getEpisodeNumber() {
         return episodeNumber;
     }
@@ -48,6 +70,10 @@ public class TvEpisode extends AbstractTvElement {
     }
 
 
+    public float getUserRating() {
+        return userRating;
+    }
+
     public float getVoteAverage() {
         return voteAverage;
     }
@@ -58,6 +84,7 @@ public class TvEpisode extends AbstractTvElement {
     }
 
 
+
     public void setOverview(String overview) {
         this.overview = overview;
     }
