@@ -1,6 +1,7 @@
 package info.movito.themoviedbapi;
 
 import info.movito.themoviedbapi.tools.MovieDbException;
+import info.movito.themoviedbapi.tools.MovieDbExceptionType;
 
 
 public class TmdbChanges extends AbstractApiElement {
@@ -11,11 +12,11 @@ public class TmdbChanges extends AbstractApiElement {
 
 
     public void getMovieChangesList(int page, String startDate, String endDate) {
-        throw new MovieDbException(MovieDbException.MovieDbExceptionType.UNKNOWN_CAUSE, "Not implemented yet");
+        throw new MovieDbException(MovieDbExceptionType.UNKNOWN_CAUSE, "Not implemented yet");
     }
 
 
     public void getPersonChangesList(int page, String startDate, String endDate) {
-        throw new MovieDbException(MovieDbException.MovieDbExceptionType.UNKNOWN_CAUSE, "Not implemented yet");
+        throw new MovieDbException(MovieDbExceptionType.UNKNOWN_CAUSE, "Not implemented yet");
     }
 }
