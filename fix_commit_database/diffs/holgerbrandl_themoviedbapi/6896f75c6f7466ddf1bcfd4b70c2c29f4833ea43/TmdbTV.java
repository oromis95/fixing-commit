@@ -10,7 +10,7 @@ public class TmdbTV extends AbstractApiElement {
     public static final String TMDB_METHOD_TV = "tv";
 
 
-    public static enum EpisodeMethod {credits, external_ids, images}
+    public static enum TvMethod {credits, external_ids, images}
 
 
     TmdbTV(TmdbApi tmdbApi) {
@@ -24,14 +24,14 @@ public class TmdbTV extends AbstractApiElement {
      * @param seriesId
      * @param language
      */
-    public TvSeries getSeries(int seriesId, String language, String... appendToResponse) {
+    public TvSeries getSeries(int seriesId, String language, TvMethod... appendToResponse) {
         ApiUrl apiUrl = new ApiUrl(TMDB_METHOD_TV, seriesId);
 
         if (StringUtils.isNotBlank(language)) {
             apiUrl.addParam(PARAM_LANGUAGE, language);
         }
 
-        apiUrl.appendToResponse(appendToResponse);
+        apiUrl.appendToResponse(Utils.asStringArray(appendToResponse));
 
         return mapJsonResult(apiUrl, TvSeries.class);
     }
