@@ -2,7 +2,7 @@ package info.movito.themoviedbapi.model;
 
 import com.fasterxml.jackson.annotation.JsonProperty;
 import info.movito.themoviedbapi.model.core.AbstractJsonMapping;
-import info.movito.themoviedbapi.model.core.NamedIdElement;
+import info.movito.themoviedbapi.model.tv.TvSeries;
 import info.movito.themoviedbapi.model.people.Person;
 
 import java.util.List;
@@ -17,7 +17,7 @@ public class FindResults extends AbstractJsonMapping {
     private List<Person> personResults;
 
     @JsonProperty("tv_results")
-    private List<NamedIdElement> tvResults;
+    private List<TvSeries> tvResults;
 
 
     public List<MovieDb> getMovieResults() {
@@ -30,7 +30,7 @@ public class FindResults extends AbstractJsonMapping {
     }
 
 
-    public List<NamedIdElement> getTvResults() {
+    public List<TvSeries> getTvResults() {
         return tvResults;
     }
 }
