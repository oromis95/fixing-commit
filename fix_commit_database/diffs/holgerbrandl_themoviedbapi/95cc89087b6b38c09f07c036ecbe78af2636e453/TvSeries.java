@@ -1,15 +1,16 @@
 package info.movito.themoviedbapi.model.tv;
 
+import java.util.List;
+
 import com.fasterxml.jackson.annotation.JsonProperty;
 import com.fasterxml.jackson.annotation.JsonTypeInfo;
+
 import info.movito.themoviedbapi.model.ContentRating;
 import info.movito.themoviedbapi.model.Genre;
 import info.movito.themoviedbapi.model.Multi;
 import info.movito.themoviedbapi.model.core.ResultsPage;
 import info.movito.themoviedbapi.model.people.Person;
 
-import java.util.List;
-
 
 /**
  * @author Holger Brandl
