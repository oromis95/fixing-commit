@@ -1,14 +1,15 @@
 package info.movito.themoviedbapi;
 
+import static info.movito.themoviedbapi.Utils.asStringArray;
+
+import info.movito.themoviedbapi.model.ContentRating;
 import info.movito.themoviedbapi.model.Credits;
 import info.movito.themoviedbapi.model.MovieImages;
 import info.movito.themoviedbapi.model.config.Timezone;
-import info.movito.themoviedbapi.model.core.MovieKeywords;
+import info.movito.themoviedbapi.model.core.TvKeywords;
 import info.movito.themoviedbapi.model.tv.TvSeries;
 import info.movito.themoviedbapi.tools.ApiUrl;
 
-import static info.movito.themoviedbapi.Utils.asStringArray;
-
 
 public class TmdbTV extends AbstractTmdbApi {
 
@@ -23,9 +24,11 @@ public class TmdbTV extends AbstractTmdbApi {
     public static final String TMDB_METHOD_KEYWORDS = "keywords";
 
 
+
     public static enum TvMethod {credits, external_ids, images, videos, recommendations, keywords, content_ratings}
 
 
+
     TmdbTV(TmdbApi tmdbApi) {
         super(tmdbApi);
     }
@@ -111,11 +114,19 @@ public class TmdbTV extends AbstractTmdbApi {
         return mapJsonResult(apiUrl, MovieImages.class);
     }
 
-    public MovieKeywords getKeywords(int seriesId, String language) {
+    public TvKeywords getKeywords(int seriesId, String language) {
         ApiUrl apiUrl = new ApiUrl(TMDB_METHOD_TV, seriesId, TMDB_METHOD_KEYWORDS);
 
         apiUrl.addLanguage(language);
 
-        return mapJsonResult(apiUrl, MovieKeywords.class);
+        return mapJsonResult(apiUrl, TvKeywords.class);
+    }
+    
+    public ContentRating.Results getContentRating(int seriesId, String language) {
+    	ApiUrl apiUrl = new ApiUrl(TMDB_METHOD_TV, seriesId, TMDB_METHOD_CONTENT_RATING);
+
+        apiUrl.addLanguage(language);
+        
+        return mapJsonResult(apiUrl, ContentRating.Results.class);
     }
 }
