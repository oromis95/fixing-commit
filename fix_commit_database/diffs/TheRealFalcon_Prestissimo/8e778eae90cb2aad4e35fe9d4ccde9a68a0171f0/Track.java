@@ -46,7 +46,6 @@ public class Track {
 	private AudioTrack mTrack;
 	private Sonic mSonic;
 	private MediaExtractor mExtractor;
-	private MediaFormat mFormat;
 	private MediaCodec mCodec;
 	private Thread mDecoderThread;
 	private String mPath;
@@ -86,7 +85,7 @@ public class Track {
 
 	// Don't know how to persist this other than pass it in and 'hold' it
 	private final IDeathCallback_0_8 mDeath;
-	final ReentrantLock lock;// = new ReentrantLock();
+	final ReentrantLock lock;
 
 	public Track(Context context, IDeathCallback_0_8 cb) {
 		mCurrentState = STATE_IDLE;
@@ -115,7 +114,6 @@ public class Track {
 			return (int) (mExtractor.getSampleTime() / 1000);
 		}
 		return 0;
-
 	}
 
 	public float getCurrentSpeed() {
@@ -157,7 +155,6 @@ public class Track {
 		default:
 			error();
 		}
-
 	}
 
 	public void prepare() {
@@ -171,8 +168,7 @@ public class Track {
 				preparedCallback.onPrepared();
 			} catch (RemoteException e) {
 				// Binder should handle our death
-				Log.e(TAG_TRACK,
-						"RemoteException calling onPrepared after prepare", e);
+				Log.e(TAG_TRACK, "RemoteException calling onPrepared after prepare", e);
 			}
 			break;
 		default:
@@ -230,11 +226,9 @@ public class Track {
 			mTrack.pause();
 			mTrack.flush();
 			break;
-
 		default:
 			error();
 		}
-
 	}
 
 	public void start() {
@@ -245,7 +239,6 @@ public class Track {
 			Log.d(SoundService.TAG_API, "State changed to STATE_STARTED");
 			mContinue = true;
 			decode();
-			mTrack.play();
 		case STATE_STARTED:
 			break;
 		case STATE_PAUSED:
@@ -264,7 +257,6 @@ public class Track {
 						"Attempting to start while in idle after construction.  Not allowed by no callbacks called");
 			}
 		}
-
 	}
 
 	public void release() {
@@ -278,24 +270,20 @@ public class Track {
 		seekCompleteCallback = null;
 		speedAdjustmentAvailableChangedCallback = null;
 		mCurrentState = STATE_END;
-
 	}
 
 	public void reset() {
 		mContinue = false;
 		try {
 			if (mDecoderThread != null
-					&& mCurrentState != STATE_PLAYBACK_COMPLETED) {
+				&& mCurrentState != STATE_PLAYBACK_COMPLETED) {
 				mDecoderThread.interrupt();
-				while (mIsDecoding) {
+				while (mIsDecoding)
 					Thread.sleep(50);
-				}
 			}
 		} catch (InterruptedException e) {
 			// WTF is happening?
-			Log.e(TAG_TRACK,
-					"Interrupted in reset while waiting for decoder thread to stop.",
-					e);
+			Log.e(TAG_TRACK, "Interrupted in reset while waiting for decoder thread to stop.", e);
 		}
 		if (mCodec != null) {
 			mCodec.release();
@@ -309,15 +297,11 @@ public class Track {
 			mTrack.release();
 			mTrack = null;
 		}
-		mFormat = null;
-
 		mCurrentState = STATE_IDLE;
 		Log.d(TAG_TRACK, "State changed to STATE_IDLE");
-
 	}
 
 	public void seekTo(final int msec) {
-
 		switch (mCurrentState) {
 		case STATE_PREPARED:
 		case STATE_STARTED:
@@ -388,11 +372,9 @@ public class Track {
 		Log.e(TAG_TRACK, "Moved to error state!");
 		mCurrentState = STATE_ERROR;
 		try {
-			boolean handled = errorCallback.onError(
-					MediaPlayer.MEDIA_ERROR_UNKNOWN, 0);
-			if (!handled) {
+			boolean handled = errorCallback.onError(MediaPlayer.MEDIA_ERROR_UNKNOWN, 0);
+			if (!handled)
 				completionCallback.onCompletion();
-			}
 		} catch (RemoteException e) {
 			// Binder should handle our death
 			Log.e(TAG_TRACK,
@@ -403,22 +385,17 @@ public class Track {
 
 	private int findFormatFromChannels(int numChannels) {
 		switch (numChannels) {
-		case 1:
-			return AudioFormat.CHANNEL_OUT_MONO;
-		case 2:
-			return AudioFormat.CHANNEL_OUT_STEREO;
-		default:
-			return -1; // Error
+		case 1:  return AudioFormat.CHANNEL_OUT_MONO;
+		case 2:  return AudioFormat.CHANNEL_OUT_STEREO;
+		default: return -1; // Error
 		}
 	}
 
 	public void initStream() {
 		mExtractor = new MediaExtractor();
-		if (mPath != null) {
-			// Map<String, String> hm = new HashMap<String, String>();
-			// hm.put("Content-Type", "audio/mpeg");
+		if (mPath != null)
 			mExtractor.setDataSource(mPath);
-		} else if (mUri != null) {
+		else if (mUri != null) {
 			try {
 				mExtractor.setDataSource(mContext, mUri, null);
 			} catch (IOException e) {
@@ -426,26 +403,26 @@ public class Track {
 				error();
 			}
 		}
-		mFormat = mExtractor.getTrackFormat(TRACK_NUM);
-
-		int sampleRate = mFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE);
-		int channelCount = mFormat.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
-		mDuration = mFormat.getLong(MediaFormat.KEY_DURATION);
-
-		Log.v(TAG_TRACK, "Sample rate: " + sampleRate);
-		initDevice(sampleRate, channelCount);
 
 		mExtractor.selectTrack(TRACK_NUM);
-		String mime = mFormat.getString(MediaFormat.KEY_MIME);
+		final MediaFormat oFormat = mExtractor.getTrackFormat(TRACK_NUM);
+		final String mime = oFormat.getString(MediaFormat.KEY_MIME);
+		mDuration         = oFormat.getLong(  MediaFormat.KEY_DURATION);
+
 		Log.v(TAG_TRACK, "Mime type: " + mime);
 		mCodec = MediaCodec.createDecoderByType(mime);
+		mCodec.configure(oFormat, null, null, 0);
 
-		mCodec.configure(mFormat, null, null, 0);
+		/* Initialise mTrack with dummy data, otherwise all uses of mTrack
+		 * must be protected agains null reference */
+		mTrack = new AudioTrack(AudioManager.STREAM_MUSIC, 44100, 2,
+				AudioFormat.ENCODING_PCM_16BIT, 1024,
+				AudioTrack.MODE_STREAM);
 	}
 
 	private void initDevice(int sampleRate, int numChannels) {
-		int format = findFormatFromChannels(numChannels);
-		int minSize = AudioTrack.getMinBufferSize(sampleRate, format,
+		final int format  = findFormatFromChannels(numChannels);
+		final int minSize = AudioTrack.getMinBufferSize(sampleRate, format,
 				AudioFormat.ENCODING_PCM_16BIT);
 		mTrack = new AudioTrack(AudioManager.STREAM_MUSIC, sampleRate, format,
 				AudioFormat.ENCODING_PCM_16BIT, minSize * 4,
@@ -460,10 +437,10 @@ public class Track {
 				mIsDecoding = true;
 				mCodec.start();
 
-				ByteBuffer[] inputBuffers = mCodec.getInputBuffers();
+				ByteBuffer[] inputBuffers  = mCodec.getInputBuffers();
 				ByteBuffer[] outputBuffers = mCodec.getOutputBuffers();
 
-				boolean sawInputEOS = false;
+				boolean sawInputEOS  = false;
 				boolean sawOutputEOS = false;
 
 				while (!sawInputEOS && !sawOutputEOS && mContinue) {
@@ -475,8 +452,13 @@ public class Track {
 						}
 						continue;
 					}
-					mSonic.setSpeed(mCurrentSpeed);
-					mSonic.setPitch(mCurrentPitch);
+
+					if (null != mSonic)
+					{
+						mSonic.setSpeed(mCurrentSpeed);
+						mSonic.setPitch(mCurrentPitch);
+					}
+
 					int inputBufIndex = mCodec.dequeueInputBuffer(200);
 					if (inputBufIndex >= 0) {
 						ByteBuffer dstBuf = inputBuffers[inputBufIndex];
@@ -485,9 +467,9 @@ public class Track {
 						if (sampleSize < 0) {
 							sawInputEOS = true;
 							sampleSize = 0;
-						} else {
+						} else
 							presentationTimeUs = mExtractor.getSampleTime();
-						}
+
 						mCodec.queueInputBuffer(
 								inputBufIndex,
 								0,
@@ -495,72 +477,61 @@ public class Track {
 								presentationTimeUs,
 								sawInputEOS ? MediaCodec.BUFFER_FLAG_END_OF_STREAM
 										: 0);
-						if (!sawInputEOS) {
+						if (!sawInputEOS)
 							mExtractor.advance();
-						}
 					}
 
-					MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
-					int res;
+					final MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
+					byte[] modifiedSamples = new byte[info.size];
 
+					int res;
 					do {
 						res = mCodec.dequeueOutputBuffer(info, 200);
 						if (res >= 0) {
 							int outputBufIndex = res;
-							ByteBuffer buf = outputBuffers[outputBufIndex];
-
 							final byte[] chunk = new byte[info.size];
-							buf.get(chunk);
-							buf.clear();
+							outputBuffers[res].get(chunk);
+							outputBuffers[res].clear();
 
 							if (chunk.length > 0) {
 								mSonic.putBytes(chunk, chunk.length);
 								int available = mSonic.availableBytes();
 								if (available > 0) {
-									final byte[] modifiedSamples = new byte[available];
-									mSonic.receiveBytes(modifiedSamples,
-											available);
+									if (modifiedSamples.length < available)
+										modifiedSamples = new byte[available];
+									mSonic.receiveBytes(modifiedSamples, available);
 									mTrack.write(modifiedSamples, 0, available);
 								}
-							} else {
+							} else
 								mSonic.flush();
-							}
+
 							mCodec.releaseOutputBuffer(outputBufIndex, false);
 
-							if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
+							if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0)
 								sawOutputEOS = true;
-							}
+
 						} else if (res == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
 							outputBuffers = mCodec.getOutputBuffers();
 							Log.d("PCM", "Output buffers changed");
 						} else if (res == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
-							final MediaFormat oformat = mCodec
-									.getOutputFormat();
+							mTrack.stop();
+							final MediaFormat oformat = mCodec.getOutputFormat();
+							Log.d("PCM", "Output format has changed to" + oformat);
 							initDevice(
 									oformat.getInteger(MediaFormat.KEY_SAMPLE_RATE),
 									oformat.getInteger(MediaFormat.KEY_CHANNEL_COUNT));
-							mTrack.stop();
-							Log.d("PCM", "Output format has changed to"
-									+ oformat);
 							outputBuffers = mCodec.getOutputBuffers();
 							mTrack.play();
-
 						}
-					} while (res == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED
+					} while (  res == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED
 							|| res == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED);
 				}
-				Log.d(TAG_TRACK,
-						"Decoding loop exited. Stopping codec and track");
-				Log.d(TAG_TRACK, "Duration: " + (int) (mDuration / 1000));
-				Log.d(TAG_TRACK,
-						"Current position: "
-								+ (int) (mExtractor.getSampleTime() / 1000));
 				mCodec.stop();
 				mTrack.stop();
-				Log.d(TAG_TRACK, "Stopped codec and track");
-				Log.d(TAG_TRACK,
-						"Current position: "
-								+ (int) (mExtractor.getSampleTime() / 1000));
+				Log.d(TAG_TRACK, "Decoding loop exited. Stopped codec and track");
+				Log.d(TAG_TRACK, "Duration: "         + (int) (mDuration                  / 1000));
+				Log.d(TAG_TRACK, "Current position: " + (int) (mExtractor.getSampleTime() / 1000));
+
 				if (mContinue && (sawInputEOS || sawOutputEOS)) {
 					mCurrentState = STATE_PLAYBACK_COMPLETED;
 					try {
@@ -572,9 +543,8 @@ public class Track {
 								e);
 					}
 				} else {
-					Log.d(TAG_TRACK,
-							"Loop ended before saw input eos or output eos");
-					Log.d(TAG_TRACK, "sawInputEOS: " + sawInputEOS);
+					Log.d(TAG_TRACK, "Loop ended before saw input eos or output eos");
+					Log.d(TAG_TRACK, "sawInputEOS: "  + sawInputEOS);
 					Log.d(TAG_TRACK, "sawOutputEOS: " + sawOutputEOS);
 				}
 				mIsDecoding = false;
