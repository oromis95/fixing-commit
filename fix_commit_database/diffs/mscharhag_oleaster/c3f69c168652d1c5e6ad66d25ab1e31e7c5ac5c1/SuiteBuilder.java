@@ -28,9 +28,9 @@ import com.mscharhag.oleaster.runner.PendingInvokable;
 public class SuiteBuilder {
 
 	private Map<String, Invokable> suiteDefinitions;
-	private Map<String, Invokable> focussedSuiteDefinitions;
+	private Map<String, Invokable> focusedSuiteDefinitions;
 	private Map<String, Optional<Invokable>> specDefinitions;
-	private Map<String, Optional<Invokable>> focussedSpecDefinitions;
+	private Map<String, Optional<Invokable>> focusedSpecDefinitions;
 	private List<Invokable> beforeEachHandlers;
 	private List<Invokable> beforeHandlers;
 	private List<Invokable> afterEachHandlers;
@@ -42,9 +42,9 @@ public class SuiteBuilder {
 
 	private void prepare() {
 		this.suiteDefinitions = new LinkedHashMap<>();
-		this.focussedSuiteDefinitions = new LinkedHashMap<>();
+		this.focusedSuiteDefinitions = new LinkedHashMap<>();
 		this.specDefinitions = new LinkedHashMap<>();
-		this.focussedSpecDefinitions = new LinkedHashMap<>();
+		this.focusedSpecDefinitions = new LinkedHashMap<>();
 		this.beforeHandlers = new ArrayList<>();
 		this.afterHandlers = new ArrayList<>();
 		this.beforeEachHandlers = new ArrayList<>();
@@ -66,7 +66,7 @@ public class SuiteBuilder {
 
 	public void fdescribe(String description, Invokable definition) {
 		throwExceptionWhenSuiteDescriptionExists(description);
-		this.focussedSuiteDefinitions.put(description, definition);
+		this.focusedSuiteDefinitions.put(description, definition);
 	}
 
 	public void xdescribe(String description, PendingInvokable definition) {
@@ -75,7 +75,7 @@ public class SuiteBuilder {
 	}
 
 	private void throwExceptionWhenSuiteDescriptionExists(final String description) {
-		if (this.suiteDefinitions.containsKey(description) || this.focussedSuiteDefinitions.containsKey(description)) {
+		if (this.suiteDefinitions.containsKey(description) || this.focusedSuiteDefinitions.containsKey(description)) {
 			throw new IllegalArgumentException(String.format("Suite with description '%s' does already exist", description));
 		}
 	}
@@ -87,7 +87,7 @@ public class SuiteBuilder {
 
 	public void fit(String description, Invokable definition) {
 		throwExceptionWhenSpecDescriptionExists(description);
-		this.focussedSpecDefinitions.put(description, Optional.of(definition));
+		this.focusedSpecDefinitions.put(description, Optional.of(definition));
 	}
 
 	public void xit(String description) {
@@ -96,7 +96,7 @@ public class SuiteBuilder {
 	}
 
 	private void throwExceptionWhenSpecDescriptionExists(final String description) {
-		if (this.specDefinitions.containsKey(description) || this.focussedSpecDefinitions.containsKey(description)) {
+		if (this.specDefinitions.containsKey(description) || this.focusedSpecDefinitions.containsKey(description)) {
 			throw new IllegalArgumentException(String.format("Spec with description '%s' does already exist", description));
 		}
 	}
@@ -121,16 +121,16 @@ public class SuiteBuilder {
 		return suiteDefinitions;
 	}
 
-	public Map<String, Invokable> getFocussedSuiteDefinitions() {
-		return focussedSuiteDefinitions;
+	public Map<String, Invokable> getFocusedSuiteDefinitions() {
+		return focusedSuiteDefinitions;
 	}
 
 	public Map<String, Optional<Invokable>> getSpecDefinitions() {
 		return specDefinitions;
 	}
 
-	public Map<String, Optional<Invokable>> getFocussedSpecDefinitions() {
-		return focussedSpecDefinitions;
+	public Map<String, Optional<Invokable>> getFocusedSpecDefinitions() {
+		return focusedSpecDefinitions;
 	}
 
 	public List<Invokable> getBeforeEachHandlers() {
