@@ -50,15 +50,15 @@ public class SuiteDefinitionEvaluator {
 		suite.addAfterEachHandlers(suiteBuilder.getAfterEachHandlers());
 		suite.addAfterHandlers(suiteBuilder.getAfterHandlers());
 
-		Map<String, Optional<Invokable>> specDefinitions = suiteBuilder.getFocussedSpecDefinitions().size() > 0
-				? suiteBuilder.getFocussedSpecDefinitions()
+		Map<String, Optional<Invokable>> specDefinitions = suiteBuilder.getFocusedSpecDefinitions().size() > 0
+				? suiteBuilder.getFocusedSpecDefinitions()
 				: suiteBuilder.getSpecDefinitions();
 
 		specDefinitions.forEach((description, block) ->
 				suite.addSpec(new Spec(suite, description, suiteDefinition.isPending() ? Optional.empty() : block)));
 
-		if (suiteBuilder.getFocussedSuiteDefinitions().size() > 0) {
-			suiteBuilder.getFocussedSuiteDefinitions().forEach((description, block) -> {
+		if (suiteBuilder.getFocusedSuiteDefinitions().size() > 0) {
+			suiteBuilder.getFocusedSuiteDefinitions().forEach((description, block) -> {
 				SuiteDefinition childSuiteDefinition = new SuiteDefinition(suite, description, block, suiteDefinition.isPending());
 				suite.addChildSuite(this.evaluate(childSuiteDefinition, suiteBuilder));
 			});
