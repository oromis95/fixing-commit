@@ -62,7 +62,36 @@ public class ObjectMatcherTest {{
 
 			it("is ok if the value is instance of the expected class", () -> {
 				new ObjectMatcher<>("foo").toBeInstanceOf(String.class);
+
+			});
+
+			it("is ok if the value is of the same class", () -> {
+				final Animal john = new Animal("John");
+				new ObjectMatcher<>(john).toBeInstanceOf(Animal.class);
+			});
+
+			it("is ok if the value is a child of the parent class", () -> {
+				final Dog marie = new Dog("Marie");
+				new ObjectMatcher<>(marie).toBeInstanceOf(Animal.class);
+			});
+
+			it("is not ok if the value is a parent of the child class", () -> {
+				final Animal bo = new Animal("Bo");
+				expectAssertionError(() -> new ObjectMatcher<>(bo).toBeInstanceOf(Dog.class), "Expected '" + bo + "' to be instance of '" + Dog.class.getName() + "'");
 			});
+
 		});
 	});
-}}
+}
+	public static class Animal {
+		public final String name;
+		public Animal(final String name) {
+			this.name = name;
+		}
+	}
+	public static class Dog extends Animal {
+		public Dog(final String name) {
+			super(name);
+		}
+	}
+}
