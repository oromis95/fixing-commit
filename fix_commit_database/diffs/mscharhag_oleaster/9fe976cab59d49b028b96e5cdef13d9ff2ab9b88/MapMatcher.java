@@ -49,6 +49,7 @@ public class MapMatcher<K, V> extends ObjectMatcher<Map<K, V>> {
      *     <li>the stored {@code Map} does not contain the given {@code key}</li>
      *     <li>the stored {@code Map} is {@code null}</li>
      * </ul>
+	 * @param key the expected key
      */
 	public void toContainKey(final K key) {
         expectNotNull(this.getValue(), "Expected null to contain the key '%s'", key);
@@ -62,6 +63,7 @@ public class MapMatcher<K, V> extends ObjectMatcher<Map<K, V>> {
      *     <li>the stored {@code Map} contains the given {@code key}</li>
      *     <li>the stored {@code Map} is {@code null}</li>
      * </ul>
+	 * @param key the expected key
      */
 	public void toNotContainKey(final K key) {
         expectNotNull(this.getValue(), "Expected null to not contain the key '%s'", key);
@@ -75,6 +77,7 @@ public class MapMatcher<K, V> extends ObjectMatcher<Map<K, V>> {
      *     <li>the stored {@code Map} does not contain the given {@code value}</li>
      *     <li>the stored {@code Map} is {@code null}</li>
      * </ul>
+	 * @param value the expected value
      */
 	public void toContainValue(final V value) {
         expectNotNull(this.getValue(), "Expected null to contain the value '%s'", value);
@@ -88,6 +91,7 @@ public class MapMatcher<K, V> extends ObjectMatcher<Map<K, V>> {
      *     <li>the stored {@code Map} does not contain the given {@code value}</li>
      *     <li>the stored {@code Map} is {@code null}</li>
      * </ul>
+	 * @param value the expected value
      */
 	public void toNotContainValue(final V value) {
         expectNotNull(this.getValue(), "Expected null to not contain the value '%s'", value);
@@ -101,6 +105,7 @@ public class MapMatcher<K, V> extends ObjectMatcher<Map<K, V>> {
      *     <li>the stored {@code Map} does not have the given {@code size}</li>
      *     <li>the stored {@code Map} is {@code null}</li>
      * </ul>
+	 * @param size the expected size
      */
 	public void toHaveSize(final int size) {
         expectNotNull(this.getValue(), "Expected null to have size '%s'", size);
