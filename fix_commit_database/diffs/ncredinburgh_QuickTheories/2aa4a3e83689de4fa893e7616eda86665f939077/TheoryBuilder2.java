@@ -105,8 +105,10 @@ public final class TheoryBuilder2<A, B> {
 
   /**
    * Converts theory to one about a different type using the given function
-   * retaining all precursor values 
-   *   
+   * retaining all precursor values
+   *  
+   * @param <T>
+   *          type to convert to
    * @param mapping
    * Function from types A and B to type T
    * @return a Subject3 relating to the state of a theory involving three values
