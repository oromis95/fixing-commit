@@ -6,6 +6,8 @@ final class Doubles {
 
   private static final long POSITIVE_INFINITY_CORRESPONDING_LONG = 0x7ff0000000000000L;
   private static final long NEGATIVE_INFINITY_CORRESPONDING_LONG = 0xfff0000000000000L;
+  private static final long ONE_CORRESPONDING_LONG = 4607182418800017408l;
+  private static final long NEGATIVE_ZERO_CORRESPONDING_LONG = Long.MIN_VALUE;
 
   static Source<Double> fromNegativeInfinityToPositiveInfinity() {
     return Compositions.interleave(fromNegativeInfinityToNegativeZero(),
@@ -18,30 +20,37 @@ final class Doubles {
   }
 
   static Source<Double> fromNegativeInfinityToNegativeZero() {
-    return Longs.range(Long.MIN_VALUE, NEGATIVE_INFINITY_CORRESPONDING_LONG)
-        .withShrinker(Longs.shrinkTowardsTarget(Long.MIN_VALUE))
-        .as(i -> Double.longBitsToDouble(i), j -> Double.doubleToLongBits(j));
+    return range(NEGATIVE_ZERO_CORRESPONDING_LONG,
+        NEGATIVE_INFINITY_CORRESPONDING_LONG, NEGATIVE_ZERO_CORRESPONDING_LONG);
   }
 
   static Source<Double> fromNegativeDoubleMaxToNegativeZero() {
-    return Longs.range(Long.MIN_VALUE, NEGATIVE_INFINITY_CORRESPONDING_LONG - 1)
-        .withShrinker(Longs.shrinkTowardsTarget(Long.MIN_VALUE))
-        .as(i -> Double.longBitsToDouble(i), j -> Double.doubleToLongBits(j));
+    return range(NEGATIVE_ZERO_CORRESPONDING_LONG,
+        NEGATIVE_INFINITY_CORRESPONDING_LONG - 1,
+        NEGATIVE_ZERO_CORRESPONDING_LONG);
   }
 
   static Source<Double> fromZeroToPositiveInfinity() {
-    return Longs.range(0, POSITIVE_INFINITY_CORRESPONDING_LONG)
-        .as(i -> Double.longBitsToDouble(i), j -> Double.doubleToLongBits(j));
+    return range(0, POSITIVE_INFINITY_CORRESPONDING_LONG);
   }
 
   static Source<Double> fromZeroToDoubleMax() {
-    return Longs.range(0, POSITIVE_INFINITY_CORRESPONDING_LONG - 1)
-        .as(i -> Double.longBitsToDouble(i), j -> Double.doubleToLongBits(j));
+    return range(0, POSITIVE_INFINITY_CORRESPONDING_LONG - 1);
   }
 
   static Source<Double> fromZeroToOne() {
-    return Longs.range(0, 1).as(i -> Double.longBitsToDouble(i),
-        j -> Double.doubleToLongBits(j));
+    return range(0, ONE_CORRESPONDING_LONG);
+  }
+
+  static Source<Double> range(long startInclusive, long endInclusive) {
+    return range(startInclusive, endInclusive, 0);
+  }
+
+  static Source<Double> range(long startInclusive, long endInclusive,
+      long target) {
+    return Longs.range(startInclusive, endInclusive)
+        .withShrinker(Longs.shrinkTowardsTarget(target))
+        .as(i -> Double.longBitsToDouble(i), j -> Double.doubleToLongBits(j));
   }
 
 }
