@@ -8,6 +8,7 @@ import java.util.Objects;
 import java.util.function.BiPredicate;
 import java.util.function.Function;
 import java.util.function.Predicate;
+import java.util.stream.Collectors;
 import java.util.stream.Stream;
 
 import org.assertj.core.api.AbstractAssert;
@@ -50,6 +51,12 @@ public class SourceAssert<T>
     org.assertj.core.api.Assertions.assertThat(generated).contains(ts);
     return this;
   }
+  
+  public SourceAssert<T> generatesAtLeastNDistinctValues(int count) {
+    List<T> generated = generateValues(1000).stream().distinct().collect(Collectors.toList());
+    org.assertj.core.api.Assertions.assertThat(generated.size()>=count).isTrue();
+    return this;
+  }
 
   private List<T> generateValues(int count) {
     isNotNull();
