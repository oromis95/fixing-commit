@@ -17,7 +17,7 @@ public class ArraysTest {
   @Test
   public void shouldGenerateAllPossibleArraysWithinDomain() {
     Source<Integer[]> testee = Arrays.arraysOf(Integers.range(1, 2),
-        Integer.class, 2);
+        Integer.class, 2, 2);
     assertThatSource(testee).generatesAllOf(
         new Integer[] { 1, 1 }, new Integer[] { 1, 2 }, new Integer[] { 2, 1 },
         new Integer[] { 2, 2 });
@@ -28,7 +28,7 @@ public class ArraysTest {
     Source<Character[]> testee = Arrays
         .arraysOf(
             Characters.ofCharacters(FIRST_CODEPOINT, ASCII_LAST_CODEPOINT),
-            Character.class, 0);
+            Character.class, 0, 0);
     assertThatSource(testee)
         .cannotShrink(new Character[] {});
   }
@@ -36,7 +36,7 @@ public class ArraysTest {
   @Test
   public void shouldShrinkNegativeIntegersByOneInAFixedLengthArrayWhereAllValuesAreWithinRemainingCyclesOfTarget() {
     Source<Integer[]> testee = Arrays
-        .arraysOf(Integers.range(-9, 0), Integer.class, 3);
+        .arraysOf(Integers.range(-9, 0), Integer.class, 3, 3);
     assertThatSource(testee).shrinksArrayValueTo(
         new Integer[] { -6, -3, -1 }, new Integer[] { -5, -2, 0 },
         new ShrinkContext(0, 100, Configuration.defaultPRNG(2)));
@@ -62,7 +62,7 @@ public class ArraysTest {
   @Test
   public void shouldShrinkAFixedLengthArrayToAnArrayOfSameLength() {
     Source<Integer[]> testee = Arrays
-        .arraysOf(Integers.range(1, Integer.MAX_VALUE), Integer.class, 4);
+        .arraysOf(Integers.range(1, Integer.MAX_VALUE), Integer.class, 4, 4);
     Integer[] input = new Integer[] { -7, -2, 0, -4 };
     Integer[] shrunk = testee
         .shrink(input, new ShrinkContext(0, 100, Configuration.defaultPRNG(-2)))
@@ -76,7 +76,7 @@ public class ArraysTest {
   public void shouldShrinkElementsOfFixedArrayByOneIfAllWithinRemainingCyclesOfTarget() {
     Source<String[]> testee = Arrays
         .arraysOf(Strings.ofBoundedLengthStrings(Character.MIN_CODE_POINT,
-            Character.MAX_CODE_POINT, 1, 8), String.class, 2);
+            Character.MAX_CODE_POINT, 1, 8), String.class, 2, 2);
     String[] input = new String[] { "\ud81b\udf33", "b" };
     String[] expected = new String[] { "\ufffd", "a" };
     assertThatSource(testee).shrinksArrayValueTo(input, expected,
@@ -104,12 +104,12 @@ public class ArraysTest {
         new Character[] { 'a', 'a', 'a', 'a' },
         new ShrinkContext(0, 100, Configuration.defaultPRNG(2)));
   }
-  
+
   @Test
   public void shouldDescribeArrayContents() {
     Source<Integer[]> testee = Arrays.arraysOf(Integers.range(0, 1),
-        Integer.class, 2);
-    Integer[] anArray = {1,2,3};
+        Integer.class, 2, 2);
+    Integer[] anArray = { 1, 2, 3 };
     assertThat(testee.asString(anArray)).isEqualTo("[1, 2, 3]");
   }
 
