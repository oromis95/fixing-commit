@@ -33,31 +33,31 @@ public class LSHMinHash extends LSH {
 
     /**
      * Instantiates a LSH instance that internally uses MinHash,
-     * with s stages (or bands) and b buckets (per stage), for sets out of a 
+     * with s stages (or bands) and b buckets (per stage), for sets out of a
      * dictionary of n elements.
-     * 
+     *
      * Attention: the number of buckets should be chosen such that we have at
      * least 100 items per bucket.
-     * 
+     *
      * @param s stages
      * @param b buckets (per stage)
      * @param n dictionary size
      */
     public LSHMinHash(int s, int b, int n) {
         super(s, b, n);
-        
+
         /**
          * "Mining of Massive Datasets", p.88.
-         * It can be shown that, using MinHash, the probability that the 
-         * signatures of 2 sets with Jaccard similarity s agree in all the 
-         * rows of at least one stage (band), and therefore become a candidate 
+         * It can be shown that, using MinHash, the probability that the
+         * signatures of 2 sets with Jaccard similarity s agree in all the
+         * rows of at least one stage (band), and therefore become a candidate
          * pair, is 1−(1−s^R)^b
          * where R = signature_size / b (number of rows in a stage/band)
-         * Thus, the curve that shows the probability that 2 items fall in the 
-         * same bucket for at least one of the stages, as a function of their 
+         * Thus, the curve that shows the probability that 2 items fall in the
+         * same bucket for at least one of the stages, as a function of their
          * Jaccard index similarity, has a S shape.
-         * The threshold (the value of similarity at which the probability of 
-         * becoming a candidate is 1/2) is a function of the number of stages 
+         * The threshold (the value of similarity at which the probability of
+         * becoming a candidate is 1/2) is a function of the number of stages
          * (s, or bands b in the book) and the signature size:
          * threshold ≃ (1/s)^(1/R)
          * Hence the signature size can be computed as:
@@ -69,12 +69,12 @@ public class LSHMinHash extends LSH {
         int signature_size = R * s;
         this.mh = new MinHash(signature_size, n);
     }
-    
+
     public int[] hash(boolean[] vector) {
         return hashSignature(this.mh.signature(vector));
     }
-    
-    public int[][] getCoefficients() {
+
+    public long[][] getCoefficients() {
         return mh.getCoefficients();
     }
 }
