@@ -4,22 +4,33 @@ import org.bukkit.event.EventHandler;
 import org.bukkit.event.EventPriority;
 import org.bukkit.event.Listener;
 import org.bukkit.event.player.PlayerTeleportEvent;
+import org.bukkit.event.player.PlayerPortalEvent;
 import org.bukkit.event.world.ChunkLoadEvent;
 import org.bukkit.Location;
 
 
 public class WBListener implements Listener
 {
-	@EventHandler(priority = EventPriority.LOWEST)
+	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
 	public void onPlayerTeleport(PlayerTeleportEvent event)
 	{
-		if (event.isCancelled()) return;
+		// if knockback is set to 0, simply return
+		if (Config.KnockBack() == 0.0)
+			return;
+
+		Location newLoc = BorderCheckTask.checkPlayer(event.getPlayer(), event.getTo(), true, true);
+		if (newLoc != null)
+			event.setTo(newLoc);
+	}
 
+	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
+	public void onPlayerPortal(PlayerPortalEvent event)
+	{
 		// if knockback is set to 0, simply return
 		if (Config.KnockBack() == 0.0)
 			return;
 
-		Location newLoc = BorderCheckTask.checkPlayer(event.getPlayer(), event.getTo(), true);
+		Location newLoc = BorderCheckTask.checkPlayer(event.getPlayer(), event.getTo(), true, false);
 		if (newLoc != null)
 			event.setTo(newLoc);
 	}
