@@ -16,6 +16,16 @@ public class CmdDenypearl extends WBCmd
 		minParams = maxParams = 1;
 
 		addCmdExample(nameEmphasized() + "<on|off> - stop ender pearls past the border.");
+		helpText = "Default value: on. When enabled, this setting will directly cancel attempts to use an ender pearl to " +
+			"get past the border rather than just knocking the player back. This should prevent usage of ender " +
+			"pearls to glitch into areas otherwise inaccessible at the border edge.";
+	}
+
+	@Override
+	public void cmdStatus(CommandSender sender)
+	{
+		sender.sendMessage(C_HEAD + "Direct cancellation of ender pearls thrown past the border is " +
+						   enabledColored(Config.getDenyEnderpearl()) + C_HEAD + ".");
 	}
 
 	@Override
@@ -26,7 +36,7 @@ public class CmdDenypearl extends WBCmd
 		if (player != null)
 		{
 			Config.log((Config.getDenyEnderpearl() ? "Enabled" : "Disabled") + " direct cancellation of ender pearls thrown past the border at the command of player \"" + player.getName() + "\".");
-			sender.sendMessage("Direct cancellation of ender pearls thrown past the border " + enabledColored(Config.getDenyEnderpearl()) + ".");
+			cmdStatus(sender);
 		}
 	}
 }
