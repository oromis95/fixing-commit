@@ -16,6 +16,14 @@ public class CmdDynmap extends WBCmd
 		minParams = maxParams = 1;
 
 		addCmdExample(nameEmphasized() + "<on|off> - turn DynMap border display on or off.");
+		helpText = "Default value: on. If you are running the DynMap plugin and this setting is enabled, all borders will " +
+			"be visually shown in DynMap.";
+	}
+
+	@Override
+	public void cmdStatus(CommandSender sender)
+	{
+		sender.sendMessage(C_HEAD + "DynMap border display is " + enabledColored(Config.DynmapBorderEnabled()) + C_HEAD + ".");
 	}
 
 	@Override
@@ -25,7 +33,7 @@ public class CmdDynmap extends WBCmd
 
 		if (player != null)
 		{
-			sender.sendMessage("DynMap border display " + (Config.DynmapBorderEnabled() ? "enabled" : "disabled") + ".");
+			cmdStatus(sender);
 			Config.log((Config.DynmapBorderEnabled() ? "Enabled" : "Disabled") + " DynMap border display at the command of player \"" + player.getName() + "\".");
 		}
 	}
