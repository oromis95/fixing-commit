@@ -16,6 +16,15 @@ public class CmdDelay extends WBCmd
 		minParams = maxParams = 1;
 
 		addCmdExample(nameEmphasized() + "<amount> - time between border checks.");
+		helpText = "Default value: 5. The <amount> is in server ticks, of which there are roughly 20 every second, each " +
+			"tick taking ~50ms. The default value therefore has border checks run about 4 times per second.";
+	}
+
+	@Override
+	public void cmdStatus(CommandSender sender)
+	{
+		int delay = Config.TimerTicks();
+		sender.sendMessage(C_HEAD + "Timer delay is set to " + delay + " tick(s). That is roughly " + (delay * 50) + "ms.");
 	}
 
 	@Override
@@ -37,6 +46,6 @@ public class CmdDelay extends WBCmd
 		Config.setTimerTicks(delay);
 
 		if (player != null)
-			sender.sendMessage("Timer delay set to " + delay + " tick(s). That is roughly " + (delay * 50) + "ms.");
+			cmdStatus(sender);
 	}
 }
