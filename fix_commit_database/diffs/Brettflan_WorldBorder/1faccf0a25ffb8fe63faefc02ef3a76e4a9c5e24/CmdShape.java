@@ -17,6 +17,16 @@ public class CmdShape extends WBCmd
 
 		addCmdExample(nameEmphasized() + "<round|square> - set the default border shape.");
 		addCmdExample(nameEmphasized() + "<elliptic|rectangular> - same as above.");
+		helpText = "Default value: round/elliptic. The default border shape will be used on all worlds which don't " +
+			"have an individual shape set using the " + commandEmphasized("wshape") + C_DESC + "command. Elliptic " +
+			"and round work the same, as rectangular and square do. The difference is down to whether the X and Z " +
+			"radius are the same.";
+	}
+
+	@Override
+	public void cmdStatus(CommandSender sender)
+	{
+		sender.sendMessage(C_HEAD + "The default border shape for all worlds is currently set to \"" + Config.ShapeName() + "\".");
 	}
 
 	@Override
@@ -29,11 +39,11 @@ public class CmdShape extends WBCmd
 			Config.setShape(true);
 		else
 		{
-			sendErrorAndHelp(sender, "You must specify one of the 4 valid shape names as indicated below.");
+			sendErrorAndHelp(sender, "You must specify one of the 4 valid shape names below.");
 			return;
 		}
 
 		if (player != null)
-			sender.sendMessage("Default border shape for all worlds is now set to \"" + Config.ShapeName() + "\".");
+			cmdStatus(sender);
 	}
 }
