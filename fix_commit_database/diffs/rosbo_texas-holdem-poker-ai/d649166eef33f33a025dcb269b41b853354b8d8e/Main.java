@@ -1,12 +1,12 @@
-package main.java;
+package edu.ntnu.texasai;
 
-import main.java.controller.PokerManager;
+import edu.ntnu.texasai.controller.PokerManager;
 
 public class Main {
     public static void main(String[] args) {
         PokerManager pokerManager = new PokerManager(2);
 
-        for(int i = 0; i < 1000; i++){
+        for (int i = 0; i < 1000; i++) {
             pokerManager.playHand();
         }
     }
