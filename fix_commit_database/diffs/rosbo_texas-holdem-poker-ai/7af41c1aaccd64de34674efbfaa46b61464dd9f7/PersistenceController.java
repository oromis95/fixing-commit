@@ -34,7 +34,7 @@ public class PersistenceController {
 		}
 		try {
 			Statement st = conn.createStatement();
-			String table = "CREATE TABLE IF NOT EXISTS Equivalence(id integer, players integer,number1 varchar(10),number2 varchar(10), type varchar(10),wins integer)";
+			String table = "CREATE TABLE IF NOT EXISTS Equivalences(id integer, players integer,number1 varchar(10),number2 varchar(10), type varchar(10),wins integer)";
 			st.executeUpdate(table);
 			System.out.println("Table creation process successfully!");
 		} catch (SQLException s) {
@@ -49,6 +49,42 @@ public class PersistenceController {
 		}
 	}
 	
+	public Double retrievePercentageOfWinsByPlayerAndEquivalenceClass(Integer numberOfPlayers, EquivalenceClass equivalenceClass){
+		Connection connection = null;
+		String number1 = equivalenceClass.getNumber1().toString();
+		String number2 = equivalenceClass.getNumber2().toString();
+		String type = equivalenceClass.getType();
+		String query = "SELECT wins FROM Equivalences WHERE players = " + numberOfPlayers + " ,number1 = " + number1 + " ,number2 = " + number2 +  " , type = " + type;
+		Double percentageOfWins = null;
+		try {
+			Class.forName("org.h2.Driver");
+			connection = DriverManager.getConnection("jdbc:h2:equivalenceTable/equivalence_table", "sa", "");
+		} catch (ClassNotFoundException e) {
+			// TODO Auto-generated catch block
+			e.printStackTrace();
+		} catch (SQLException e) {			
+			// TODO Auto-generated catch block
+			e.printStackTrace();
+		}
+		try {
+			PreparedStatement statement = connection.prepareStatement(query);
+			
+			ResultSet result = statement.executeQuery();
+			while (result.next()) {
+				percentageOfWins = new Double(result.getDouble("wins"));
+			}
+			result.close();
+		} catch (SQLException e) {
+			e.printStackTrace();
+		}try {
+			connection.close();
+		} catch (SQLException e) {
+			// TODO Auto-generated catch block
+			e.printStackTrace();
+		}		
+		return percentageOfWins;
+	}
+	
 	public void persistResult(Integer id, Integer numberOfPlayers, EquivalenceClass equivalenceClass, Double percentage){
 		Connection conn = null;
 		try {
@@ -62,7 +98,7 @@ public class PersistenceController {
 			e.printStackTrace();
 		}
 		try {
-			String insert = "INSERT INTO Equivalence VALUES(?,?,?,?,?,?)";	
+			String insert = "INSERT INTO Equivalences VALUES(?,?,?,?,?,?)";	
 			PreparedStatement statement = conn.prepareStatement(insert);
 			statement.setInt(1, id);
 			statement.setInt(2, numberOfPlayers);
@@ -98,7 +134,7 @@ public class PersistenceController {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
-		String query = "SELECT * FROM Equivalence";
+		String query = "SELECT * FROM Equivalences";
 		try {
 			PreparedStatement statement = connection.prepareStatement(query);
 			
@@ -108,6 +144,11 @@ public class PersistenceController {
 			}
 		} catch (SQLException e) {
 			e.printStackTrace();
+		}try {
+			connection.close();
+		} catch (SQLException e) {
+			// TODO Auto-generated catch block
+			e.printStackTrace();
 		}
 		
 	}
