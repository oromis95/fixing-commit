@@ -24,6 +24,11 @@ public class EquivalenceClassUnsuited extends EquivalenceClass{
 		return cards;
 	}
 	
+	@Override
+	public String getType(){
+		return "UNSUITED";
+	}
+	
 	@Override
 	public String toString() {
 		return "EquivalenceClassUnsuited [number1=" + getNumber1() + ", number2="
