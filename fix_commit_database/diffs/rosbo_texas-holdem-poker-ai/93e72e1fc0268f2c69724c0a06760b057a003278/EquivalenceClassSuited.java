@@ -7,7 +7,6 @@ public class EquivalenceClassSuited extends EquivalenceClass {
 
     public EquivalenceClassSuited(CardNumber number1, CardNumber number2) {
         super(number1, number2);
-
     }
 
     @Override
@@ -17,10 +16,8 @@ public class EquivalenceClassSuited extends EquivalenceClass {
 
     @Override
     public List<Card> equivalence2cards() {
-        // TODO Auto-generated method stub
         List<Card> cards = new ArrayList<Card>();
         Card card1, card2;
-
         card1 = new Card(CardSuit.SPADE, this.getNumber1());
         card2 = new Card(CardSuit.SPADE, this.getNumber2());
         cards.add(card1);
@@ -30,8 +27,8 @@ public class EquivalenceClassSuited extends EquivalenceClass {
 
     @Override
     public String toString() {
-        return "EquivalenceClassSuited [number1=" + getNumber1() + ", number2="
-                + getNumber2() + "]";
+        return "Equivalence Class Suited (" + getNumber1() + ","
+                + getNumber2() + ")";
     }
 
 }
