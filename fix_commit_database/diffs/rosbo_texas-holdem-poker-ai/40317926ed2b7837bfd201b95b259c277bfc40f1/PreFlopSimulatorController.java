@@ -10,11 +10,13 @@ import edu.ntnu.texasai.utils.GameProperties;
 import edu.ntnu.texasai.utils.Logger;
 
 import javax.inject.Inject;
-import java.util.ArrayList;
 import java.util.Collection;
 
 public class PreFlopSimulatorController {
-    private final Game game;
+    // TODO: Must be at least 100
+    public static final int ROLLOUTS_PER_EQUIV_CLASS = 10;
+
+    private final Game game = new Game();
     private final Logger logger;
     private final GameProperties gameProperties;
     private final PlayerControllerPreFlopRoll playerControllerPreFlopRoll;
@@ -37,45 +39,37 @@ public class PreFlopSimulatorController {
         this.gameHandControllerPreFlopRoll = gameHandControllerPreFlopRoll;
         this.statisticsController = statisticsController;
         this.persistenceController = persistenceController;
-        game = new Game(new ArrayList<Player>());
     }
 
     public void play() {
+        this.equivalenceClassController.generateAllEquivalenceClass();
 
-        int idDatabase = 0;
+        game.addPlayer(new Player(0, gameProperties.getInitialMoney(), playerControllerPreFlopRoll));
+        Collection<EquivalenceClass> equivalenceClasses = equivalenceClassController.getEquivalenceClasses();
 
-        this.equivalenceClassController.generateAllEquivalenceClass();
+        // TODO: Generate 2 to 10 players
+        for (int numberOfPlayers = 2; numberOfPlayers <= 6; numberOfPlayers++) {
+            game.addPlayer(new Player(numberOfPlayers - 1, 0, playerControllerPreFlopRoll));
 
-        game.addPlayer(new Player(0, gameProperties.getInitialMoney(),
-                playerControllerPreFlopRoll));
-        Collection<EquivalenceClass> equivalenceClasses = equivalenceClassController
-                .getEquivalenceClasses();
-        for (int numberOfPlayers = 2; numberOfPlayers <= 4; numberOfPlayers++) {
-            // gameProperties.getPlayers().size()
-            game.addPlayer(new Player(numberOfPlayers, gameProperties
-                    .getInitialMoney(), playerControllerPreFlopRoll));
             for (EquivalenceClass equivalenceClass : equivalenceClasses) {
-                this.statisticsController.initializeStatistics();
-                for (int i = 0; i < gameProperties.getNumberOfHands(); i++) {
-                    // this.statisticsController.setCurrentNumberOfPlayers(new
-                    // Integer(numberOfPlayers));
+                statisticsController.initializeStatistics();
+
+                for (int i = 0; i < ROLLOUTS_PER_EQUIV_CLASS; i++) {
                     gameHandControllerPreFlopRoll.play(game, equivalenceClass);
                     game.setNextDealer();
                 }
 
-                Double percentageOfWinsPlayer0 = this.statisticsController
-                        .getPercentageOfWinsPlayer0(gameProperties
-                                .getNumberOfHands());
+                Double percentageOfWinsPlayer0 = (double) statisticsController.getPlayer0Wins() /
+                        ROLLOUTS_PER_EQUIV_CLASS;
+                persistenceController.persistResult(numberOfPlayers, equivalenceClass,
+                        percentageOfWinsPlayer0);
+
                 logger.log("=================");
                 logger.log("STATISTICS FOR EQUIVALENCE CLASS "
                         + equivalenceClass.toString());
-                logger.log("Number of hands played: "
-                        + gameProperties.getNumberOfHands());
+                logger.log("Number of hands played: "+ ROLLOUTS_PER_EQUIV_CLASS);
                 logger.log("Number players: " + numberOfPlayers);
                 logger.log("Percentage of wins is " + percentageOfWinsPlayer0);
-                idDatabase++;
-                persistenceController.persistResult(idDatabase, numberOfPlayers, equivalenceClass,
-                        percentageOfWinsPlayer0);
             }
         }
     }
