@@ -1,9 +1,5 @@
 package edu.ntnu.texasai.controller;
 
-import java.util.List;
-
-import javax.inject.Inject;
-
 import edu.ntnu.texasai.model.BettingDecision;
 import edu.ntnu.texasai.model.GameHand;
 import edu.ntnu.texasai.model.Player;
@@ -12,6 +8,9 @@ import edu.ntnu.texasai.model.cards.EquivalenceClass;
 import edu.ntnu.texasai.persistence.PersistenceManager;
 import edu.ntnu.texasai.utils.Logger;
 
+import javax.inject.Inject;
+import java.util.List;
+
 public class PlayerControllerPhaseII extends PlayerController {
     private final PlayerControllerPhaseI playerControllerPhaseI;
     private final PersistenceManager persistanceController;
@@ -21,9 +20,10 @@ public class PlayerControllerPhaseII extends PlayerController {
 
     @Inject
     public PlayerControllerPhaseII(final PlayerControllerPhaseI playerControllerPhaseI,
-                    final EquivalenceClassController equivalenceClassController,
-                    final PersistenceManager persistanceController, final HandStrengthEvaluator handStrengthEvaluator,
-                    final Logger logger) {
+                                   final EquivalenceClassController equivalenceClassController,
+                                   final PersistenceManager persistanceController,
+                                   final HandStrengthEvaluator handStrengthEvaluator,
+                                   final Logger logger) {
         this.playerControllerPhaseI = playerControllerPhaseI;
         this.persistanceController = persistanceController;
         this.equivalenceClassController = equivalenceClassController;
@@ -37,11 +37,11 @@ public class PlayerControllerPhaseII extends PlayerController {
         Card card2 = cards.get(1);
         EquivalenceClass equivalenceClass = this.equivalenceClassController.cards2Equivalence(card1, card2);
         Double percentageOfWins = this.persistanceController.retrievePercentageOfWinsByPlayerAndEquivalenceClass(
-                        gameHand.getPlayers().size(), equivalenceClass);
+                gameHand.getPlayers().size(), equivalenceClass);
 
-        if (percentageOfWins.doubleValue() > 0.8)
+        if (percentageOfWins > 0.8)
             return BettingDecision.RAISE;
-        else if (percentageOfWins.doubleValue() < 0.33)
+        else if (percentageOfWins < 0.5)
             return BettingDecision.FOLD;
         return BettingDecision.CALL;
     }
@@ -49,10 +49,10 @@ public class PlayerControllerPhaseII extends PlayerController {
     @Override
     public BettingDecision decideAfterFlop(Player player, GameHand gameHand, List<Card> cards) {
         Double handStrength = this.handStrengthEvaluator.evaluate(player.getHoleCards(), gameHand.getSharedCards(),
-                        gameHand.getPlayers().size());
+                gameHand.getPlayers().size());
         if (handStrength > 0.8) {
             return BettingDecision.RAISE;
-        } else if (handStrength > 0.33 || canCheck(gameHand, player)) {
+        } else if (handStrength > 0.4 || canCheck(gameHand, player)) {
             return BettingDecision.CALL;
         }
         return BettingDecision.FOLD;
