@@ -3,6 +3,7 @@ package edu.ntnu.texasai.controller;
 import edu.ntnu.texasai.model.Game;
 import edu.ntnu.texasai.model.Player;
 import edu.ntnu.texasai.utils.GameProperties;
+import edu.ntnu.texasai.utils.Logger;
 
 import javax.inject.Inject;
 import java.util.ArrayList;
@@ -10,12 +11,15 @@ import java.util.List;
 
 public class PokerController {
     private final Game game;
+    private final Logger logger;
     private final GameProperties gameProperties;
     private final GameHandController gameHandController;
 
     @Inject
-    public PokerController(final GameHandController gameHandController, final GameProperties gameProperties) {
+    public PokerController(final GameHandController gameHandController, final Logger logger,
+                           final GameProperties gameProperties) {
         this.gameHandController = gameHandController;
+        this.logger = logger;
         this.gameProperties = gameProperties;
 
         game = createGame();
@@ -26,6 +30,8 @@ public class PokerController {
             gameHandController.play(game);
             game.setNextDealer();
         }
+
+        printStats();
     }
 
     private Game createGame() {
@@ -41,4 +47,14 @@ public class PokerController {
         }
         return players;
     }
+
+    private void printStats() {
+        logger.log("-----------------------------------------");
+        logger.log("Statistics");
+        logger.log("-----------------------------------------");
+        logger.log("Number of hands played: " + game.gameHandsCount());
+        for (Player player : game.getPlayers()) {
+            logger.log(player.toString() + ": " + player.getMoney() + "$");
+        }
+    }
 }
