@@ -1,15 +1,19 @@
 package edu.ntnu.texasai.persistence;
 
+import java.sql.Connection;
+import java.sql.DriverManager;
+import java.sql.PreparedStatement;
+import java.sql.ResultSet;
+import java.sql.SQLException;
+import java.sql.Statement;
+
 import com.google.inject.Inject;
+
 import edu.ntnu.texasai.model.cards.EquivalenceClass;
 import edu.ntnu.texasai.utils.Logger;
 
-import java.sql.*;
-
 public class PersistenceManager {
 
-    public static final String EQUIVALENCE_TABLE_PATH = "equivalenceTable/equivalence_table";
-
     private final Logger logger;
 
     @Inject
@@ -21,8 +25,7 @@ public class PersistenceManager {
         Connection conn = null;
         try {
             Class.forName("org.h2.Driver");
-            conn = DriverManager.getConnection(
-                    "jdbc:h2:" + EQUIVALENCE_TABLE_PATH, "sa", "");
+            conn = DriverManager.getConnection("jdbc:h2:equivalenceTable/equivalence_table", "sa", "");
         } catch (ClassNotFoundException e) {
             e.printStackTrace();
         } catch (SQLException e) {
@@ -30,8 +33,7 @@ public class PersistenceManager {
         }
         try {
             Statement st = conn.createStatement();
-            String table = "CREATE TABLE IF NOT EXISTS Equivalences(id integer, players integer,number1 varchar(10)," +
-                    "number2 varchar(10), type varchar(10),wins integer)";
+            String table = "CREATE TABLE IF NOT EXISTS equivalences(id integer, players integer,number1 VARCHAR_IGNORECASE,number2 VARCHAR_IGNORECASE, type VARCHAR_IGNORECASE, wins double)";
             st.executeUpdate(table);
             System.out.println("Table creation process successfully!");
         } catch (SQLException s) {
@@ -45,20 +47,17 @@ public class PersistenceManager {
         }
     }
 
-    public Double retrievePercentageOfWinsByPlayerAndEquivalenceClass(
-            Integer numberOfPlayers, EquivalenceClass equivalenceClass) {
+    public Double retrievePercentageOfWinsByPlayerAndEquivalenceClass(Integer numberOfPlayers,
+                    EquivalenceClass equivalenceClass) {
         Connection connection = null;
         String number1 = equivalenceClass.getNumber1().toString();
         String number2 = equivalenceClass.getNumber2().toString();
         String type = equivalenceClass.getType();
-        String query = "SELECT wins FROM Equivalences WHERE players = "
-                + numberOfPlayers + " ,number1 = " + number1 + " ,number2 = "
-                + number2 + " , type = " + type;
-        Double percentageOfWins = null;
+        String query = "SELECT * FROM equivalences WHERE players = ? and number1 = ? and number2 = ? and type = ? ";
+        Double percentageOfWins = 0.0;
         try {
             Class.forName("org.h2.Driver");
-            connection = DriverManager.getConnection(
-                    "jdbc:h2:" + EQUIVALENCE_TABLE_PATH + "/equivalence_table", "sa", "");
+            connection = DriverManager.getConnection("jdbc:h2:equivalenceTable/equivalence_table", "sa", "");
         } catch (ClassNotFoundException e) {
             e.printStackTrace();
         } catch (SQLException e) {
@@ -66,10 +65,13 @@ public class PersistenceManager {
         }
         try {
             PreparedStatement statement = connection.prepareStatement(query);
-
+            statement.setInt(1, numberOfPlayers.intValue());
+            statement.setString(2, number1);
+            statement.setString(3, number2);
+            statement.setString(4, type);
             ResultSet result = statement.executeQuery();
             while (result.next()) {
-                percentageOfWins = result.getDouble("wins");
+                percentageOfWins = new Double(result.getDouble("wins"));
             }
             result.close();
         } catch (SQLException e) {
@@ -83,13 +85,11 @@ public class PersistenceManager {
         return percentageOfWins;
     }
 
-    public void persistResult(Integer id, Integer numberOfPlayers,
-                              EquivalenceClass equivalenceClass, Double percentage) {
+    public void persistResult(Integer id, Integer numberOfPlayers, EquivalenceClass equivalenceClass, Double percentage) {
         Connection conn = null;
         try {
             Class.forName("org.h2.Driver");
-            conn = DriverManager.getConnection(
-                    "jdbc:h2:" + EQUIVALENCE_TABLE_PATH, "sa", "");
+            conn = DriverManager.getConnection("jdbc:h2:equivalenceTable/equivalence_table", "sa", "");
         } catch (ClassNotFoundException e) {
             e.printStackTrace();
         } catch (SQLException e) {
@@ -109,7 +109,6 @@ public class PersistenceManager {
         } catch (SQLException e) {
             e.printStackTrace();
         }
-
         try {
             conn.close();
 
@@ -122,8 +121,7 @@ public class PersistenceManager {
         Connection connection = null;
         try {
             Class.forName("org.h2.Driver");
-            connection = DriverManager.getConnection(
-                    "jdbc:h2:" + EQUIVALENCE_TABLE_PATH, "sa", "");
+            connection = DriverManager.getConnection("jdbc:h2:equivalenceTable/equivalence_table", "sa", "");
         } catch (ClassNotFoundException e) {
             e.printStackTrace();
         } catch (SQLException e) {
@@ -132,10 +130,11 @@ public class PersistenceManager {
         String query = "SELECT * FROM Equivalences";
         try {
             PreparedStatement statement = connection.prepareStatement(query);
-
             ResultSet result = statement.executeQuery();
             while (result.next()) {
-                logger.log(result.getInt("id") + " " + result.getInt("players"));
+                logger.log(result.getInt("id") + " " + result.getInt("players") + " " + result.getString("number1")
+                                + " " + result.getString("number2") + " " + result.getString("type") + " "
+                                + result.getDouble("wins"));
             }
         } catch (SQLException e) {
             e.printStackTrace();
@@ -148,4 +147,4 @@ public class PersistenceManager {
 
     }
 
-}
+}
\ No newline at end of file
