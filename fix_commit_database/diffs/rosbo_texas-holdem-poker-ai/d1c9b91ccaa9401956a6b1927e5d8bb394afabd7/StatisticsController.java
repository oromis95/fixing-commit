@@ -10,32 +10,49 @@ import edu.ntnu.texasai.model.Player;
 
 public class StatisticsController {
 	
-	private Map<Integer,Integer> players2wins = new HashMap<Integer,Integer>();
+	private Map<Integer,Integer> players2wins;
 //	private Map<Integer,Integer> players2loses = new HashMap<Integer,Integer>();
 	private Map<Integer,Integer> players2ties = new HashMap<Integer,Integer>();
+	private Integer player0winnings;
 	
 	@Inject
 	public StatisticsController(){
 		this.players2wins = new HashMap<Integer,Integer>();
+		player0winnings = new Integer(0);
+		
 	}
 	
 	public void initializeStatistics(){
 		for(int i = 0; i<=10; i++){
-			this.players2wins.put(i, new Integer(0));
+			this.players2wins.put(i, 0);
 //			this.players2loses.put(i, new Integer(0));
-			this.players2ties.put(i, new Integer(0));
+			this.players2ties.put(i, 0);
 		}
+		player0winnings = new Integer(0);
+		
 	}
 	
 	public void storeWinners(List<Player> winners){
-		if(winners.size()==1){
-			Player winner = winners.get(0);
-			Integer numberOfWins = players2wins.get(winner.getNumber());
-			players2wins.put(winner.getNumber(), new Integer(numberOfWins++));			
-		}else{
-			for(Player tiePlayer : winners){
-				Integer numberOfWins = players2ties.get(tiePlayer.getNumber());
-				players2wins.put(tiePlayer.getNumber(), new Integer(numberOfWins++));	
+//		if(winners.size()==1){
+//			System.out.println("WINNER "+winners.get(0).getNumber());
+//			Player winner = winners.get(0);
+//			System.out.println(winner.toString());
+//			int numberOfWins = players2wins.get(winner.getNumber());
+//			this.players2wins.put(winner.getNumber(), new Integer(numberOfWins++));	
+//			System.out.println("NUmber of wins " + numberOfWins);
+//		}else{
+//			for(Player tiePlayer : winners){
+				//TODO: tiePlayers if really needed
+//				int numberOfTies = players2ties.get(tiePlayer.getNumber());
+//				players2ties.put(tiePlayer.getNumber(), numberOfTies++);	
+//			}
+//		}
+		//I don't know why storing the wins of all players was giving me problems
+		//At the moment we need only player0 wins in order to store it into the table
+		//I'll fix it later if we need it
+		for(Player winner : winners){
+			if (winner.getNumber() == 0){
+				player0winnings ++;
 			}
 		}
 	}
@@ -59,10 +76,20 @@ public class StatisticsController {
 	public void clearStatistics(){
 		this.players2ties.clear();
 		this.players2wins.clear();
+		
 	}
 	
-	public Integer getPercentageOfWins(Integer playerNumber, Integer numberOfHands){
-		return this.players2wins.get(playerNumber)/numberOfHands;
+	public Double getPercentageOfWinsPlayer0(Integer numberOfHands){
+//		return this.players2wins.get(0)/numberOfHands;
+		Double d = new Double(10);
+	
+		return new Double(player0winnings/new Double(numberOfHands));
+		
+	}
+
+	public Integer getPlayer0Wins() {
+		// TODO Auto-generated method stub
+		return player0winnings;
 	}
 	
 	
