@@ -20,8 +20,6 @@ import static org.junit.Assert.assertEquals;
 import static org.junit.Assert.assertFalse;
 
 import org.junit.Test;
-import org.trendafilov.confucius.InjectableConfiguration;
-import org.trendafilov.confucius.core.ConfigurationException;
 
 public class InjectableConfigurationTest {
 	private final static String TEST_CONTEXT = "Test2";
@@ -33,7 +31,7 @@ public class InjectableConfigurationTest {
 	}
 
 	@Test
-	public void testTwoArgsConstructor() throws ConfigurationException {
+	public void testTwoArgsConstructor() {
 		InjectableConfiguration config = new InjectableConfiguration("some-file.cfg", TEST_CONTEXT);
 		assertFalse(config.keySet().isEmpty());
 		assertEquals(TEST_CONTEXT, config.getStringValue("conf.context"));
