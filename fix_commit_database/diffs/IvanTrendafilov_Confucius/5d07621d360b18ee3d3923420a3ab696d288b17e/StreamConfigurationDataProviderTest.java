@@ -1,19 +1,36 @@
+/* 
+ * Copyright 2013-2014 Ivan Trendafilov and contributors
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *     http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
 package org.trendafilov.confucius.core;
 
-import junit.framework.TestCase;
-import org.junit.Test;
+import static org.junit.Assert.assertEquals;
+import static org.junit.Assert.assertNotNull;
 
 import java.io.ByteArrayInputStream;
 import java.io.IOException;
 import java.io.InputStream;
 import java.util.List;
 
-public class StreamConfigurationDataProviderTest extends TestCase {
+import org.junit.Test;
+
+public class StreamConfigurationDataProviderTest {
 
 	@Test
 	public void testReturnInputStream() throws IOException {
 		InputStream inputStream = new ByteArrayInputStream("contents".getBytes("UTF-8"));
-
 		StreamConfigurationDataProvider provider = new StreamConfigurationDataProvider(inputStream);
 		assertEquals("contents", Utils.streamToString(provider.getInputStream()));
 	}
@@ -21,7 +38,6 @@ public class StreamConfigurationDataProviderTest extends TestCase {
 	@Test
 	public void testReturnEmptyListWhenInputStreamIsNull() throws IOException {
 		StreamConfigurationDataProvider provider = new StreamConfigurationDataProvider(null);
-
 		List<String> lines = provider.getAllLines();
 		assertNotNull(lines);
 		assertEquals(0, lines.size());
@@ -30,7 +46,6 @@ public class StreamConfigurationDataProviderTest extends TestCase {
 	@Test
 	public void testReturnLines() throws IOException {
 		InputStream inputStream = new ByteArrayInputStream("a\nb\r\nc\n".getBytes("UTF-8"));
-
 		StreamConfigurationDataProvider provider = new StreamConfigurationDataProvider(inputStream);
 		List<String> lines = provider.getAllLines();
 		assertEquals(3, lines.size());
@@ -46,5 +61,4 @@ public class StreamConfigurationDataProviderTest extends TestCase {
 		provider.getAllLines();
 		assertEquals("contents", Utils.streamToString(provider.getInputStream()));
 	}
-
 }
