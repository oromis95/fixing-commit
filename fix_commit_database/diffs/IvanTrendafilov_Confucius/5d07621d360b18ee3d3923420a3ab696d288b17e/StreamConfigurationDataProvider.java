@@ -1,3 +1,19 @@
+/* 
+ * Copyright 2013-2014 Ivan Trendafilov and contributors
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *     http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
 package org.trendafilov.confucius.core;
 
 import java.io.ByteArrayInputStream;
@@ -7,30 +23,23 @@ import java.util.ArrayList;
 import java.util.Arrays;
 import java.util.List;
 
-public class StreamConfigurationDataProvider implements ConfigurationDataProvider {
+class StreamConfigurationDataProvider implements ConfigurationDataProvider {
 
-	InputStream inputStream;
+	private InputStream inputStream;
 
 	public StreamConfigurationDataProvider(InputStream inputStream) {
 		this.inputStream = inputStream;
 	}
 
-	@Override
 	public List<String> getAllLines() throws IOException {
-		if (inputStream == null) {
+		if (inputStream == null)
 			return new ArrayList<>();
-		}
-
 		String configurationString = Utils.streamToString(inputStream);
 		this.inputStream = new ByteArrayInputStream(configurationString.getBytes("UTF-8"));
 		return new ArrayList<>(Arrays.asList(configurationString.split("\\r?\\n")));
 	}
 
-
-	@Override
 	public InputStream getInputStream() throws IOException {
 		return inputStream;
 	}
-
-
 }
