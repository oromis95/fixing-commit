@@ -1,5 +1,5 @@
 /* 
- * Copyright 2013 Ivan Trendafilov
+ * Copyright 2013-2014 Ivan Trendafilov and contributors
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -13,6 +13,7 @@
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
+
 package org.trendafilov.confucius;
 
 import java.util.List;
