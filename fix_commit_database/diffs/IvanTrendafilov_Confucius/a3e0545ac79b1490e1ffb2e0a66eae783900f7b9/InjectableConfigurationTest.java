@@ -19,23 +19,59 @@ package org.trendafilov.confucius;
 import static org.junit.Assert.assertEquals;
 import static org.junit.Assert.assertFalse;
 
+import java.io.BufferedWriter;
+import java.io.File;
+import java.io.FileWriter;
+import java.io.IOException;
+
 import org.junit.Test;
 
 public class InjectableConfigurationTest {
 	private final static String TEST_CONTEXT = "Test2";
-	private final static String TEST_CFG_FILE = "some-file.cfg";
-
+	
 	@Test
 	public void sanityCheck() {
 		InjectableConfiguration config = new InjectableConfiguration();
 		assertFalse(config.keySet().isEmpty());
 	}
-
+	
 	@Test
-	public void testTwoArgsConstructor() {
-		InjectableConfiguration config = new InjectableConfiguration(TEST_CFG_FILE, TEST_CONTEXT);
+	public void testTwoArgsConstructor() throws IOException {
+		File temp = writeFile(true);
+		InjectableConfiguration config = new InjectableConfiguration(temp.getAbsolutePath(), TEST_CONTEXT);
 		assertFalse(config.keySet().isEmpty());
 		assertEquals(TEST_CONTEXT, config.getStringValue("conf.context"));
-		assertEquals(TEST_CFG_FILE, config.getStringValue("conf.properties"));
+		assertEquals(temp.getAbsolutePath(), config.getStringValue("conf.properties"));
+		assertEquals("value123", config.getStringValue("key123"));
+		assertEquals("value456", config.getStringValue("key456"));
+		temp.delete();
+	}
+	
+	@Test
+	public void testTwoArgsConstructorWithoutContext() throws IOException {
+		File temp = writeFile(false);
+		InjectableConfiguration config = new InjectableConfiguration(temp.getAbsolutePath(), null);
+		assertFalse(config.keySet().isEmpty());
+		assertEquals(temp.getAbsolutePath(), config.getStringValue("conf.properties"));
+		assertEquals("value123", config.getStringValue("key123"));
+		assertEquals("value456", config.getStringValue("key456"));
+		temp.delete();
+	}
+	
+	private File writeFile(boolean hasContext) throws IOException {
+		File temp = File.createTempFile("confuciusTest", ".tmp");
+		BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
+		if (hasContext)
+			bw.write("[Default]");
+		bw.newLine();
+		bw.write("key123=value123");
+		bw.newLine();
+		bw.newLine();
+		if (hasContext)
+			bw.write("[" + TEST_CONTEXT + "]");
+		bw.newLine();
+		bw.write("key456=value456");
+		bw.close();
+		return temp;
 	}
 }
