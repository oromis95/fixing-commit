@@ -1,7 +1,8 @@
-package com.voodoodyne.gwizard.logging;
+package com.voodoodyne.gwizard.logging.example;
 
 import com.google.inject.Guice;
 import com.google.inject.Injector;
+import com.voodoodyne.gwizard.logging.LoggingModule;
 import lombok.extern.slf4j.Slf4j;
 
 /**
