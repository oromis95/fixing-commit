@@ -1,9 +1,11 @@
-package com.voodoodyne.gwizard.logging;
+package com.voodoodyne.gwizard.logging.example;
 
 import com.google.inject.AbstractModule;
 import com.google.inject.Guice;
 import com.google.inject.Injector;
 import com.google.inject.Provides;
+import com.voodoodyne.gwizard.logging.LoggingConfig;
+import com.voodoodyne.gwizard.logging.LoggingModule;
 import lombok.extern.slf4j.Slf4j;
 import javax.inject.Singleton;
 
