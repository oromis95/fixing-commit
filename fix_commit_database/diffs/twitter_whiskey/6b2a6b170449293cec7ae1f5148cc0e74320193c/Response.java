@@ -1,17 +1,21 @@
 package com.twitter.internal.network.whiskey;
 
 
+import java.nio.ByteBuffer;
+
 /**
  * @author Michael Schore
  */
 public class Response {
     private int statusCode;
     private Headers headers;
+    private ByteBuffer body;
     private RequestStats stats;
 
-    Response(int statusCode, Headers headers, RequestStats stats) {
+    Response(int statusCode, Headers headers, ByteBuffer body, RequestStats stats) {
         this.statusCode = statusCode;
         this.headers = headers;
+        this.body = body;
         this.stats = stats;
     }
 
@@ -23,7 +27,9 @@ public class Response {
         return headers;
     }
 
-    public RequestStats getStatus() {
+    public ByteBuffer getBody() { return body; }
+
+    public RequestStats getStats() {
         return stats;
     }
 }
