@@ -3,7 +3,6 @@ package com.twitter.internal.network.whiskey;
 import android.support.annotation.NonNull;
 
 import java.util.concurrent.Executor;
-import java.util.concurrent.atomic.AtomicBoolean;
 
 /**
  * @author Michael Schore
