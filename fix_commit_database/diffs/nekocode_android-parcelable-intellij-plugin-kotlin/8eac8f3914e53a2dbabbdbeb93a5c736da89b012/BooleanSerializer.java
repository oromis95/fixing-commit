@@ -27,10 +27,10 @@ public class BooleanSerializer extends TypeSerializer {
     }
 
     public String generateReadValue() {
-        return "1.equals(source.readInt())";
+        return "1 == source.readInt()";
     }
 
     public String generateWriteValue() {
-        return "dest?.writeInt((if(" + getFieldName() + ") 1 else 0))";
+        return "dest.writeInt((if(" + getFieldName() + ") 1 else 0))";
     }
 }
