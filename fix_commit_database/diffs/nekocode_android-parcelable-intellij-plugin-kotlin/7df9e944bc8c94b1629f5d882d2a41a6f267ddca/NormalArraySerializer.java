@@ -34,7 +34,7 @@ public class NormalArraySerializer extends TypeSerializer {
             typeProjection = field.getType().toString();
             typeProjection = typeProjection.substring(0, typeProjection.length()-5);
         }
-        return "source.create" + typeProjection + "Array()";
+        return "source.create" + typeProjection + "Array().toTypedArray()";
     }
 
     public String writeValue() {
@@ -45,6 +45,6 @@ public class NormalArraySerializer extends TypeSerializer {
             typeProjection = field.getType().toString();
             typeProjection = typeProjection.substring(0, typeProjection.length()-5);
         }
-        return "dest?.write" + typeProjection + "Array(" + field.getName() + ")";
+        return "dest?.write" + typeProjection + "Array(" + field.getName() + ".to" + typeProjection + "Array())";
     }
 }
