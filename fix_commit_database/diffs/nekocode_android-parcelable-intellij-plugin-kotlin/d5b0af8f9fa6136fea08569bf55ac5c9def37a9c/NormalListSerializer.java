@@ -31,6 +31,6 @@ public class NormalListSerializer extends TypeSerializer {
     }
 
     public String generateWriteValue() {
-        return "dest.writeList(" + getFieldName() + ")";
+        return "writeList(" + getFieldName() + ")";
     }
 }
