@@ -1,5 +1,5 @@
 /*
- * Copyright 2006-2014 the original author or authors.
+ * Copyright 2006-2015 the original author or authors.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -18,7 +18,7 @@ package org.springframework.batch.item.excel.support.rowset;
 import org.springframework.batch.item.excel.Sheet;
 
 /**
- * Contract for extracting column names for a given {@Sheet sheet}.
+ * Contract for extracting column names for a given {@code sheet}.
  *
  * @author Marten Deinum
  * @since 0.5.0
