@@ -1,5 +1,5 @@
 /*
- * Copyright 2006-2014 the original author or authors.
+ * Copyright 2006-2015 the original author or authors.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -20,7 +20,7 @@ import org.springframework.batch.item.excel.support.rowset.RowSet;
 /**
  * Map rows from an excel sheet to an object.
  *
- * @param <T>
+ * @param <T> the type
  * @author Marten Deinum
  * @since 0.5.0
  */
