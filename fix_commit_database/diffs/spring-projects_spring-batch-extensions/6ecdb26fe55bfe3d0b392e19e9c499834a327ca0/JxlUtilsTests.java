@@ -50,10 +50,10 @@ public class JxlUtilsTests {
      */
     @Test
     public void checkIfCellsAreEmpty() {
-        Assert.assertFalse("Cell1 should not be empty", JxlUtils.isEmpty(this.cell1));
-        Assert.assertTrue("Cell2 should be empty", JxlUtils.isEmpty(this.cell2));
-        Assert.assertTrue("Cell3 should be empty", JxlUtils.isEmpty(this.cell3));
-        Assert.assertTrue("Cell4 should be empty", JxlUtils.isEmpty(this.cell4));
+        Assert.assertFalse("Cell[1] should not be empty", JxlUtils.isEmpty(this.cell1));
+        Assert.assertTrue("Cell[2] should be empty", JxlUtils.isEmpty(this.cell2));
+        Assert.assertTrue("Cell[3] should be empty", JxlUtils.isEmpty(this.cell3));
+        Assert.assertTrue("Cell[4] should be empty", JxlUtils.isEmpty(this.cell4));
         Assert.assertTrue("[null] should be empty", JxlUtils.isEmpty((Cell) null));
     }
 
@@ -64,9 +64,9 @@ public class JxlUtilsTests {
     public void checkIfRowIsEmpty() {
         Assert.assertTrue("[null] should be empty", JxlUtils.isEmpty((Cell[]) null));
         Assert.assertTrue("[null] should be empty", JxlUtils.isEmpty(new Cell[0]));
-        Assert.assertFalse("Cell[]1 should not be empty",
+        Assert.assertFalse("Cell[1] should not be empty",
                 JxlUtils.isEmpty(new Cell[]{this.cell1, this.cell2, this.cell3}));
-        Assert.assertTrue("Cell[]2 should be empty", JxlUtils.isEmpty(new Cell[]{this.cell2, this.cell3, null}));
+        Assert.assertTrue("Cell[2] should be empty", JxlUtils.isEmpty(new Cell[]{this.cell2, this.cell3, null}));
     }
 
     /**
