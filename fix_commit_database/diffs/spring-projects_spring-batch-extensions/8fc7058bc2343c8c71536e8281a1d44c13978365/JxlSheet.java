@@ -25,7 +25,9 @@ import org.springframework.batch.item.excel.Sheet;
  *
  * @author Marten Deinum
  * @since 0.5.0
+ * @deprecated since JExcelAPI is an abandoned project (no release since 2009, with serious bugs remaining)
  */
+@Deprecated
 public class JxlSheet implements Sheet {
 
     private final jxl.Sheet delegate;
