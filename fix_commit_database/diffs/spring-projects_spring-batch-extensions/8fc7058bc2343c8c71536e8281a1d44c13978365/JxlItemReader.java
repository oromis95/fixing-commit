@@ -32,7 +32,10 @@ import org.springframework.util.ClassUtils;
  * @param <T> the type
  * @author Marten Deinum
  * @since 0.5.0
+ *
+ * @deprecated since JExcelAPI is an abandoned project (no release since 2009, with serious bugs remaining)
  */
+@Deprecated
 public class JxlItemReader<T> extends AbstractExcelItemReader<T> {
 
     private Workbook workbook;
