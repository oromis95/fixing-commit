@@ -93,10 +93,10 @@ public class RedisServerTest {
     @Test
     public void shouldOverrideDefaultExecutable() throws Exception {
         RedisExecProvider customProvider = RedisExecProvider.defaultProvider()
-                .override(OS.UNIX, Resources.getResource("redis-server").getFile())
-                .override(OS.WINDOWS, Architecture.x86, Resources.getResource("redis-server.exe").getFile())
-                .override(OS.WINDOWS, Architecture.x86_64, Resources.getResource("redis-server-64.exe").getFile())
-                .override(OS.MAC_OS_X, Resources.getResource("redis-server").getFile());
+                .override(OS.UNIX, Resources.getResource("redis-server-2.8.19").getFile())
+                .override(OS.WINDOWS, Architecture.x86, Resources.getResource("redis-server-2.8.19.exe").getFile())
+                .override(OS.WINDOWS, Architecture.x86_64, Resources.getResource("redis-server-2.8.19.exe").getFile())
+                .override(OS.MAC_OS_X, Resources.getResource("redis-server-2.8.19").getFile());
         
         redisServer = new RedisServerBuilder()
                 .redisExecProvider(customProvider)
