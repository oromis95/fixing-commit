@@ -41,7 +41,7 @@ public class RedisServer extends AbstractRedisInstance {
 
     RedisServer(List<String> args, int port) {
         super(port);
-        this.args = new ArrayList<>(args);
+        this.args = new ArrayList<String>(args);
     }
 
     public static RedisServerBuilder builder() {
