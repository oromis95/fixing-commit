@@ -8,7 +8,7 @@ public class RedisSentinel extends AbstractRedisInstance {
 
     public RedisSentinel(List<String> args, int port) {
         super(port);
-        this.args = new ArrayList<>(args);
+        this.args = new ArrayList<String>(args);
     }
 
     public static RedisSentinelBuilder builder() { return new RedisSentinelBuilder(); }
