@@ -0,0 +1,6 @@
+package redis.embedded.util;
+
+public enum Architecture {
+    x86,
+    x86_64
+}
