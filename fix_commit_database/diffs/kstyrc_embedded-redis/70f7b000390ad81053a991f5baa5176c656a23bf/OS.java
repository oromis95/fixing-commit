@@ -0,0 +1,7 @@
+package redis.embedded.util;
+
+public enum OS {
+    WINDOWS,
+    UNIX,
+    MAC_OS_X
+}
