@@ -98,6 +98,19 @@ public class FilteringAllMaxSpliteratorTest {
         assertThat(count).isEqualTo(1L);
     }
 
+    @Test
+    public void should_correctly_count_the_elements_of_a_sized_stream() {
+        // Given
+        Stream<String> strings = Arrays.asList("one", "two", "three").stream();
+        Stream<String> stream = StreamsUtils.filteringAllMax(strings);
+
+        // When
+        long count = stream.count();
+
+        // Then
+        assertThat(count).isEqualTo(1L);
+    }
+
     @Test(expectedExceptions = NullPointerException.class)
     public void should_not_build_a_filtering_spliterator_on_a_null_stream() {
 
