@@ -142,6 +142,21 @@ public class GroupingOnSplittingSpliteratorTest {
         assertThat(count).isEqualTo(11L);
     }
 
+    @Test
+    public void should_correctly_count_the_elements_of_a_sized_stream() {
+        // Given
+        Stream<String> strings = Stream.of("o", "1", "2", "3", "4", "5", "6", "7", "8", "9", "c");
+        Predicate<String> splitter = s -> s.startsWith("o");
+
+        Stream<Stream<String>> stream = StreamsUtils.group(strings, splitter);
+
+        // When
+        long count = stream.count();
+
+        // Then
+        assertThat(count).isEqualTo(1L);
+    }
+
     @Test(expectedExceptions = NullPointerException.class)
     public void should_not_build_a_grouping_spliterator_on_a_null_spliterator() {
 
