@@ -165,6 +165,22 @@ public class GroupingOnGatingSpliteratorTest {
         assertThat(count).isEqualTo(11L);
     }
 
+    @Test
+    public void should_correctly_count_the_elements_of_a_sized_stream() {
+        // Given
+        Stream<String> strings = Stream.of("o", "1", "2", "3", "4", "5", "6", "7", "8", "9", "c");
+        Predicate<String> open = s -> s.startsWith("o");
+        Predicate<String> close = s -> s.startsWith("c");
+
+        Stream<Stream<String>> stream = StreamsUtils.group(strings, open, close);
+
+        // When
+        long count = stream.count();
+
+        // Then
+        assertThat(count).isEqualTo(1L);
+    }
+
     @Test(expectedExceptions = NullPointerException.class)
     public void should_not_build_a_grouping_spliterator_on_a_null_spliterator() {
 
