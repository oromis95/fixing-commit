@@ -117,6 +117,6 @@ public class GroupingOnSplittingSpliterator<E> implements Spliterator<Stream<E>>
     @Override
     public int characteristics() {
         // this spliterator is already ordered
-        return spliterator.characteristics() & ~Spliterator.SORTED;
+        return spliterator.characteristics() & ~Spliterator.SORTED & ~Spliterator.SIZED & ~Spliterator.SUBSIZED;
     }
 }
\ No newline at end of file
