@@ -115,16 +115,31 @@ public class GroupingOnSplittingNonIncludedSpliteratorTest {
     @Test
     public void should_group_a_sorted_stream_correctly_and_in_an_unsorted_stream() {
         // Given
-        SortedSet<String> sortedSet = new TreeSet<>(Arrays.asList("o", "1", "2", "3", "4", "5", "6", "7", "8", "9", "c"));
+        List<String> strings = Arrays.asList("o", "1", "2", "3", "4", "5", "6", "7", "8", "9", "c");
         Predicate<String> splitter = s -> s.startsWith("o");
 
         // When
-        Stream<Stream<String>> groupingStream = StreamsUtils.group(sortedSet.stream(), splitter, false);
+        Stream<Stream<String>> groupingStream = StreamsUtils.group(strings.stream(), splitter, false);
 
         // Then
         assertThat(groupingStream.spliterator().characteristics() & Spliterator.SORTED).isEqualTo(0);
     }
 
+    @Test
+    public void should_correctly_count_the_elements_of_a_sized_stream() {
+        // Given
+        Stream<String> strings = Stream.of("o", "1", "2", "3", "4", "5", "6", "7", "8", "9", "c");
+        Predicate<String> splitter = s -> s.startsWith("o");
+
+        Stream<Stream<String>> stream = StreamsUtils.group(strings, splitter, false);
+
+        // When
+        long count = stream.count();
+
+        // Then
+        assertThat(count).isEqualTo(1L);
+    }
+
     @Test
     public void should_conform_to_specified_trySplit_behavior() {
         // Given
