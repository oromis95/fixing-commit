@@ -17,13 +17,16 @@
 package org.paumard.spliterators;
 
 
+import org.paumard.spliterators.util.TryAdvanceCheckingSpliterator;
 import org.paumard.streams.StreamsUtils;
 import org.testng.annotations.Test;
 
 import java.util.*;
+import java.util.function.Function;
 import java.util.function.Predicate;
 import java.util.stream.Collectors;
 import java.util.stream.Stream;
+import java.util.stream.StreamSupport;
 
 import static org.assertj.core.api.Assertions.assertThat;
 
@@ -91,6 +94,23 @@ public class GroupingSpliteratorTest {
         assertThat(groupingStream.spliterator().characteristics() & Spliterator.SORTED).isEqualTo(0);
     }
 
+    @Test
+    public void should_conform_to_specified_trySplit_behavior() {
+        // Given
+        Stream<String> strings = Stream.of("1", "2", "3", "4", "5", "6", "7");
+        int groupingFactor = 3;
+
+        Stream<Stream<String>> testedStream = StreamsUtils.group(strings, groupingFactor);
+        TryAdvanceCheckingSpliterator<Stream<String>> spliterator = new TryAdvanceCheckingSpliterator<>(testedStream.spliterator());
+        Stream<String> monitoredStream = StreamSupport.stream(spliterator, false).flatMap(Function.identity());
+
+        // When
+        long count = monitoredStream.count();
+
+        // Then
+        assertThat(count).isEqualTo(11L);
+    }
+
     @Test(expectedExceptions = NullPointerException.class)
     public void should_not_build_a_grouping_spliterator_on_a_null_spliterator() {
 
