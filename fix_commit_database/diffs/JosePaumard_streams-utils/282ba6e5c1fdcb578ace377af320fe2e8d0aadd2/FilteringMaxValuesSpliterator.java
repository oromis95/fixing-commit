@@ -145,7 +145,7 @@ public class FilteringMaxValuesSpliterator<E> implements Spliterator<E> {
             map.computeIfAbsent(t, key -> Stream.builder()).add(t);
         }
 
-        public Stream<T> getResult() {
+        private Stream<T> getResult() {
             return map.entrySet().stream().flatMap(entry -> entry.getValue().build());
         }
     }
