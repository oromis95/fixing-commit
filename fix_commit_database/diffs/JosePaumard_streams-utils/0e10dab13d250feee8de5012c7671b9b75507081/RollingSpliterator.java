@@ -18,7 +18,6 @@ package org.paumard.spliterators;
 
 import org.paumard.streams.StreamsUtils;
 
-import java.util.Comparator;
 import java.util.Objects;
 import java.util.Spliterator;
 import java.util.concurrent.atomic.AtomicInteger;
@@ -32,84 +31,94 @@ import java.util.stream.Stream;
  */
 public class RollingSpliterator<E> implements Spliterator<Stream<E>> {
 
-	private final int grouping ;
-	private final Spliterator<E> spliterator ;
-	private final Object [] buffer ; // we cant create arrays of E
-	private final AtomicInteger bufferWriteIndex = new AtomicInteger(0) ;
-	private final AtomicInteger bufferReadIndex = new AtomicInteger(0) ;
+    private final int grouping;
+    private final Spliterator<E> spliterator;
+    private final Object[] buffer; // we cant create arrays of E
+    private final AtomicInteger bufferWriteIndex = new AtomicInteger(0);
+    private final AtomicInteger bufferReadIndex = new AtomicInteger(0);
+    private boolean lastBufferSent = false;
 
-	public static <E> RollingSpliterator<E> of(Spliterator<E> spliterator, int grouping) {
-		Objects.requireNonNull(spliterator);
-		if (grouping < 2) {
-            throw new IllegalArgumentException ("Why would you be creating a rolling spliterator with a grouping factor of less than 2?");
+    public static <E> RollingSpliterator<E> of(Spliterator<E> spliterator, int grouping) {
+        Objects.requireNonNull(spliterator);
+        if (grouping < 2) {
+            throw new IllegalArgumentException("Why would you be creating a rolling spliterator with a grouping factor of less than 2?");
         }
         if ((spliterator.characteristics() & Spliterator.ORDERED) == 0) {
-            throw new IllegalArgumentException ("Why would you create a rolling spliterator on a non-ordered spliterator?");
+            throw new IllegalArgumentException("Why would you create a rolling spliterator on a non-ordered spliterator?");
         }
 
-		return new RollingSpliterator<>(spliterator, grouping);
-	}
-
-	private RollingSpliterator(Spliterator<E> spliterator, int grouping) {
-		this.spliterator = spliterator ;
-		this.grouping = grouping ;
-		this.buffer = new Object[grouping + 1] ;
-	}
-
-	@Override
-	public boolean tryAdvance(Consumer<? super Stream<E>> action) {
-		boolean finished = false ;
-
-		if (bufferWriteIndex.get() == bufferReadIndex.get()) {
-			for (int i = 0 ; i < grouping ; i++) {
-				if (!advanceSpliterator()) {
-					finished = true ;
-				}
-			}
-		}
-		if (!advanceSpliterator()) {
-			finished = true ;
-		}
-		
-		Stream<E> subStream = buildSubstream() ;
-		action.accept(subStream) ;
-		return !finished ;
-	}
-
-	private boolean advanceSpliterator() {
-		return spliterator.tryAdvance(
-					element -> { 
-						buffer[bufferWriteIndex.get() % buffer.length] = element ; 
-						bufferWriteIndex.incrementAndGet() ;
-				});
-	}
-
-	@Override
-	public Spliterator<Stream<E>> trySplit() {
-		Spliterator<E> splitSpliterator = spliterator.trySplit();
-		return splitSpliterator == null ? null : new RollingSpliterator<>(splitSpliterator, grouping) ;
-	}
-	
-	@SuppressWarnings("unchecked")
-	private Stream<E> buildSubstream() {
-		
-		Stream.Builder<E> subBuilder = Stream.builder() ;
-		for (int i = 0 ; i < grouping ; i++) {			
-			subBuilder.add((E)buffer[(i + bufferReadIndex.get()) % buffer.length]) ;
-		}
-		bufferReadIndex.incrementAndGet() ;
-		return subBuilder.build() ;
-	}
-
-	@Override
-	public long estimateSize() {
-		long estimateSize = spliterator.estimateSize();
-		return estimateSize == Long.MAX_VALUE ? Long.MAX_VALUE : estimateSize - grouping;
-	}
-
-	@Override
-	public int characteristics() {
+        return new RollingSpliterator<>(spliterator, grouping);
+    }
+
+    private RollingSpliterator(Spliterator<E> spliterator, int grouping) {
+        this.spliterator = spliterator;
+        this.grouping = grouping;
+        this.buffer = new Object[grouping + 1];
+    }
+
+    @Override
+    public boolean tryAdvance(Consumer<? super Stream<E>> action) {
+        boolean hasMore = true;
+
+        if ((bufferWriteIndex.get() == bufferReadIndex.get()) && hasMore) {
+            for (int i = 0; i < grouping; i++) {
+                if (!advance(this.spliterator)) {
+                    hasMore = false;
+                }
+            }
+        }
+        if (!advance(spliterator)) {
+            hasMore = false;
+        }
+
+        if (hasMore) {
+            Stream<E> subStream = buildSubstream();
+            action.accept(subStream);
+            return true;
+        } else if (!lastBufferSent) {
+            Stream<E> subStream = buildSubstream();
+            action.accept(subStream);
+            lastBufferSent = true;
+            return true;
+        } else {
+            return false;
+        }
+    }
+
+    private boolean advance(Spliterator<E> spliterator) {
+        return spliterator.tryAdvance(
+                element -> {
+                    buffer[bufferWriteIndex.get() % buffer.length] = element;
+                    bufferWriteIndex.incrementAndGet();
+                });
+    }
+
+    @Override
+    public Spliterator<Stream<E>> trySplit() {
+        Spliterator<E> splitSpliterator = spliterator.trySplit();
+        return splitSpliterator == null ? null : new RollingSpliterator<>(splitSpliterator, grouping);
+    }
+
+    @SuppressWarnings("unchecked")
+    private Stream<E> buildSubstream() {
+
+        Stream.Builder<E> subBuilder = Stream.builder();
+        for (int i = 0; i < grouping; i++) {
+            subBuilder.add((E) buffer[(i + bufferReadIndex.get()) % buffer.length]);
+        }
+        bufferReadIndex.incrementAndGet();
+        return subBuilder.build();
+    }
+
+    @Override
+    public long estimateSize() {
+        long estimateSize = spliterator.estimateSize();
+        return estimateSize == Long.MAX_VALUE ? Long.MAX_VALUE : estimateSize - grouping;
+    }
+
+    @Override
+    public int characteristics() {
         // this spliterator is already ordered
-		return spliterator.characteristics() & ~Spliterator.SORTED;
-	}
+        return spliterator.characteristics() & ~Spliterator.SORTED;
+    }
 }
\ No newline at end of file
