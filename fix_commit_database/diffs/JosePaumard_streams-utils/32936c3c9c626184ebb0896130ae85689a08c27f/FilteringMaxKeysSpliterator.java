@@ -89,6 +89,11 @@ public class FilteringMaxKeysSpliterator<E> implements Spliterator<E> {
         return spliterator.characteristics();
     }
 
+    @Override
+    public Comparator<? super E> getComparator() {
+        return this.spliterator.getComparator();
+    }
+
     private static class InsertionTab<T> implements Consumer<T> {
 
         private final T[] tab;
