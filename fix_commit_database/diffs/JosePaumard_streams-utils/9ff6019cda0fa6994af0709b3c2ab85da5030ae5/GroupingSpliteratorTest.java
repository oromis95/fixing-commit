@@ -20,7 +20,8 @@ package org.paumard.spliterators;
 import org.paumard.streams.StreamsUtils;
 import org.testng.annotations.Test;
 
-import java.util.List;
+import java.util.*;
+import java.util.function.Predicate;
 import java.util.stream.Collectors;
 import java.util.stream.Stream;
 
@@ -77,6 +78,19 @@ public class GroupingSpliteratorTest {
         assertThat(collect.get(2)).containsExactly("7");
     }
 
+    @Test
+    public void should_group_a_sorted_stream_correctly_and_in_an_unsorted_stream() {
+        // Given
+        SortedSet<String> sortedSet = new TreeSet<>(Arrays.asList("o", "1", "2", "3", "4", "5", "6", "7", "8", "9", "c"));
+        int groupingFactor = 3;
+
+        // When
+        Stream<Stream<String>> groupingStream = StreamsUtils.group(sortedSet.stream(), groupingFactor);
+
+        // Then
+        assertThat(groupingStream.spliterator().characteristics() & Spliterator.SORTED).isEqualTo(0);
+    }
+
     @Test(expectedExceptions = NullPointerException.class)
     public void should_not_build_a_grouping_spliterator_on_a_null_spliterator() {
 
