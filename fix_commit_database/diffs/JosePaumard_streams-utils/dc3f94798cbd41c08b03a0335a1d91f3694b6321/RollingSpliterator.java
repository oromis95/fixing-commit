@@ -113,7 +113,7 @@ public class RollingSpliterator<E> implements Spliterator<Stream<E>> {
     @Override
     public long estimateSize() {
         long estimateSize = spliterator.estimateSize();
-        return estimateSize == Long.MAX_VALUE ? Long.MAX_VALUE : estimateSize - grouping;
+        return estimateSize == Long.MAX_VALUE ? Long.MAX_VALUE : estimateSize - grouping + 1;
     }
 
     @Override
