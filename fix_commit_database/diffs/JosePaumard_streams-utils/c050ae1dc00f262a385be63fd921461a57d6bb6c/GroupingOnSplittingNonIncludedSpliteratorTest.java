@@ -17,13 +17,16 @@
 package org.paumard.spliterators;
 
 
+import org.paumard.spliterators.util.TryAdvanceCheckingSpliterator;
 import org.paumard.streams.StreamsUtils;
 import org.testng.annotations.Test;
 
 import java.util.*;
+import java.util.function.Function;
 import java.util.function.Predicate;
 import java.util.stream.Collectors;
 import java.util.stream.Stream;
+import java.util.stream.StreamSupport;
 
 import static org.assertj.core.api.Assertions.assertThat;
 
@@ -121,4 +124,21 @@ public class GroupingOnSplittingNonIncludedSpliteratorTest {
         // Then
         assertThat(groupingStream.spliterator().characteristics() & Spliterator.SORTED).isEqualTo(0);
     }
+
+    @Test
+    public void should_conform_to_specified_trySplit_behavior() {
+        // Given
+        Stream<String> strings = Stream.of("o", "1", "2", "3", "4", "5", "6", "7", "8", "9", "c");
+        Predicate<String> splitter = s -> s.startsWith("o");
+
+        Stream<Stream<String>> testedStream = StreamsUtils.group(strings, splitter, false);
+        TryAdvanceCheckingSpliterator<Stream<String>> spliterator = new TryAdvanceCheckingSpliterator<>(testedStream.spliterator());
+        Stream<String> monitoredStream = StreamSupport.stream(spliterator, false).flatMap(Function.identity());
+
+        // When
+        long count = monitoredStream.count();
+
+        // Then
+        assertThat(count).isEqualTo(10L);
+    }
 }
\ No newline at end of file
