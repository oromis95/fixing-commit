@@ -121,6 +121,6 @@ public class GroupingOnGatingSpliterator<E> implements Spliterator<Stream<E>> {
     @Override
     public int characteristics() {
         // this spliterator is already ordered
-        return spliterator.characteristics();
+        return spliterator.characteristics() & ~Spliterator.SORTED;
     }
 }
\ No newline at end of file
