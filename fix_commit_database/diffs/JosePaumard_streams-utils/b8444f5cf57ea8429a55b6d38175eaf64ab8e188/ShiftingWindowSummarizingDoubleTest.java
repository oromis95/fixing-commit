@@ -19,8 +19,8 @@ package org.paumard.spliterators;
 import org.paumard.streams.StreamsUtils;
 import org.testng.annotations.Test;
 
-import java.util.DoubleSummaryStatistics;
-import java.util.List;
+import java.util.*;
+import java.util.stream.DoubleStream;
 import java.util.stream.Stream;
 
 import static java.util.stream.Collectors.toList;
@@ -69,6 +69,19 @@ public class ShiftingWindowSummarizingDoubleTest {
         assertThat(result.get(5).toString()).isEqualTo(stats.toString());
     }
 
+    @Test
+    public void should_process_a_sorted_stream_correctly_and_in_an_unsorted_stream() {
+        // Given
+        SortedSet<String> sortedSet = new TreeSet<>(Arrays.asList("2", "4", "2", "4", "2", "4", "2"));
+        int groupingFactor = 2;
+
+        // When
+        Stream<DoubleSummaryStatistics> stream = StreamsUtils.shiftingWindowSummarizingDouble(sortedSet.stream(), groupingFactor, Double::parseDouble);
+
+        // Then
+        assertThat(stream.spliterator().characteristics() & Spliterator.SORTED).isEqualTo(0);
+    }
+
     @Test(expectedExceptions = NullPointerException.class)
     public void should_not_build_a_shifting_stream_on_a_null_stream() {
 
