@@ -92,7 +92,7 @@ public class FilteringMaxKeysSpliterator<E> implements Spliterator<E> {
 
     @Override
     public int characteristics() {
-        return spliterator.characteristics();
+        return spliterator.characteristics() & ~Spliterator.SIZED & ~Spliterator.SUBSIZED;
     }
 
     @Override
