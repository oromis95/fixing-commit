@@ -16,7 +16,6 @@
 
 package org.paumard.spliterators;
 
-import com.sun.xml.internal.ws.util.StreamUtils;
 import org.paumard.streams.StreamsUtils;
 import org.testng.annotations.Test;
 
@@ -24,7 +23,6 @@ import java.util.Arrays;
 import java.util.List;
 import java.util.stream.Collectors;
 import java.util.stream.Stream;
-import java.util.stream.StreamSupport;
 
 import static org.assertj.core.api.StrictAssertions.assertThat;
 
