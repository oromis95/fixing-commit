@@ -114,6 +114,18 @@ public class CrossProductNoDoublesSpliteratorTest {
         assertThat(entry.getValue()).isEqualTo("c");
     }
 
+    @Test
+    public void should_be_able_to_cross_product_no_doubles_a_sorted_stream_in_an_non_sorted_cross_product_stream() {
+        // Given
+        SortedSet<String> sortedSet = new TreeSet(Arrays.asList("one", "two", "three"));
+
+        // When
+        Stream<Map.Entry<String, String>> stream = StreamsUtils.crossProductNoDoubles(sortedSet.stream());
+
+        // Then
+        assertThat(stream.spliterator().characteristics() & Spliterator.SORTED).isEqualTo(0);
+    }
+
     @Test(expectedExceptions = NullPointerException.class)
     public void should_not_build_a_crossing_spliterator_on_a_null_spliterator() {
 
