@@ -120,7 +120,7 @@ public class ShiftingWindowSummarizingIntTest {
         StreamsUtils.<String>shiftingWindowSummarizingInt(null, 3, Integer::parseInt);
     }
 
-    @Test(expectedExceptions = IllegalArgumentException .class)
+    @Test(expectedExceptions = IllegalArgumentException.class)
     public void should_not_build_a_shifting_stream_with_a_grouping_factor_of_1() {
         // Given
         Stream<String> strings = Stream.of("1", "2", "3", "4", "5", "6", "7");
