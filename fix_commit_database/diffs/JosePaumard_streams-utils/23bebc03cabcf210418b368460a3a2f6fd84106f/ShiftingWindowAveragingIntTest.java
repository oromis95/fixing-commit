@@ -22,7 +22,6 @@ import org.testng.annotations.Test;
 
 import java.util.*;
 import java.util.stream.DoubleStream;
-import java.util.stream.IntStream;
 import java.util.stream.Stream;
 import java.util.stream.StreamSupport;
 
@@ -119,7 +118,7 @@ public class ShiftingWindowAveragingIntTest {
         StreamsUtils.<String>shiftingWindowAveragingInt(null, 3, Integer::parseInt);
     }
 
-    @Test(expectedExceptions = IllegalArgumentException .class)
+    @Test(expectedExceptions = IllegalArgumentException.class)
     public void should_not_build_a_shifting_stream_with_a_grouping_factor_of_1() {
         // Given
         Stream<String> strings = Stream.of("1", "2", "3", "4", "5", "6", "7");
