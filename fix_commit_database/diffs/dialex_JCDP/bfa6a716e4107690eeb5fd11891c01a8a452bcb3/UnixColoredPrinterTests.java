@@ -284,7 +284,7 @@ public class UnixColoredPrinterTests {
     }
 
     @Test
-    public void Print_DebugMessage_IgnoreDebugIsDisabled() {
+    public void Print_DebugMessage_IgnoreDebugIfDisabled() {
         // ARRANGE
         UnixColoredPrinter printer = new UnixColoredPrinter.Builder(2, false).build();
         String msg = DataGenerator.createMsg();
