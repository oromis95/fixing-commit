@@ -153,6 +153,11 @@ public class Printer implements IPrinter {
         getImpl().printTimestamp();
     }
 
+    @Override
+    public void printErrorTimestamp() {
+        getImpl().printErrorTimestamp();
+    }
+
     @Override
     public void print(Object msg) {
         getImpl().print(msg);
