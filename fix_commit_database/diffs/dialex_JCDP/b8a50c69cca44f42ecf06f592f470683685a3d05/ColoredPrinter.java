@@ -180,6 +180,11 @@ public class ColoredPrinter implements IColoredPrinter {
         getImpl().printTimestamp();
     }
 
+    @Override
+    public void printErrorTimestamp() {
+        getImpl().printTimestamp();
+    }
+
     @Override
     public void print(Object msg) {
         getImpl().print(msg);
