@@ -1,6 +1,7 @@
 package com.diogonunes.jcdp.tests.unit;
 
 
+import com.diogonunes.jcdp.color.ColoredPrinter;
 import com.diogonunes.jcdp.color.api.Ansi;
 import com.diogonunes.jcdp.color.impl.UnixColoredPrinter;
 import helpers.DataGenerator;
@@ -517,4 +518,23 @@ public class TestColoredPrinter {
         assertThat("First message is formatted with ansi code", messages[0], containsString(ansiCode1));
         assertThat("Second message has different ansi code", messages[1], containsString(ansiCode2));
     }
+
+    @Test
+    public void ColoredPrint_Message_FontIsColoredEvenWithoutBColor() {
+        // ARRANGE
+        String msg = DataGenerator.createMsg();
+        Ansi.FColor fColor = Ansi.FColor.GREEN;
+        Ansi.BColor bColor = Ansi.BColor.NONE;
+        Ansi.Attribute attr = Ansi.Attribute.NONE;
+
+        UnixColoredPrinter printer = new UnixColoredPrinter.Builder(1, false).build();
+        printer.setForegroundColor(fColor);
+
+        // ACT
+        printer.println(msg);
+
+        // ASSERT
+        String expectedAnsiCode = printer.generateCode(attr, fColor, bColor);
+        assertThat("Message is colored, even without a bkg color", outContent.toString(), containsString(expectedAnsiCode));
+    }
 }
\ No newline at end of file
