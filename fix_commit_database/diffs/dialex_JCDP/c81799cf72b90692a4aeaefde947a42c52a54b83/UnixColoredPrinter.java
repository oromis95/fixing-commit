@@ -30,6 +30,8 @@ public class UnixColoredPrinter extends AbstractColoredPrinter {
 
     /**
      * Constructor using builder.
+     *
+     * @param builder Builder with the desired configurations.
      */
     public UnixColoredPrinter(Builder builder) {
         setLevel(builder._level);
@@ -121,22 +123,23 @@ public class UnixColoredPrinter extends AbstractColoredPrinter {
     // =================================
 
     /**
-     * Prints current Time and Date to terminal.
+     * {@inheritDoc}
      */
     @Override
     public void printTimestamp() {
         System.out.print(generateCode() + getDateFormatted() + " ");
     }
 
+    /**
+     * {@inheritDoc}
+     */
     @Override
     public void printErrorTimestamp() {
         System.err.print(generateCode() + getDateFormatted() + " ");
     }
 
     /**
-     * Prints a message to terminal.
-     *
-     * @param msg The message to print.
+     * {@inheritDoc}
      */
     @Override
     public void print(Object msg) {
@@ -148,6 +151,9 @@ public class UnixColoredPrinter extends AbstractColoredPrinter {
         System.out.print(msg);
     }
 
+    /**
+     * {@inheritDoc}
+     */
     @Override
     public void print(Object msg, Attribute attr, FColor fg, BColor bg) {
         if (isLoggingTimestamps()) {
@@ -159,9 +165,7 @@ public class UnixColoredPrinter extends AbstractColoredPrinter {
     }
 
     /**
-     * Prints a message to terminal with a new line at the end.
-     *
-     * @param msg The message to print.
+     * {@inheritDoc}
      */
     @Override
     public void println(Object msg) {
@@ -173,6 +177,9 @@ public class UnixColoredPrinter extends AbstractColoredPrinter {
         System.out.println(msg);
     }
 
+    /**
+     * {@inheritDoc}
+     */
     @Override
     public void println(Object msg, Attribute attr, FColor fg, BColor bg) {
         if (isLoggingTimestamps()) {
@@ -184,9 +191,7 @@ public class UnixColoredPrinter extends AbstractColoredPrinter {
     }
 
     /**
-     * Prints an error message to terminal.
-     *
-     * @param msg The error message to print.
+     * {@inheritDoc}
      */
     @Override
     public void errorPrint(Object msg) {
@@ -198,6 +203,9 @@ public class UnixColoredPrinter extends AbstractColoredPrinter {
         System.err.print(msg);
     }
 
+    /**
+     * {@inheritDoc}
+     */
     @Override
     public void errorPrint(Object msg, Attribute attr, FColor fg, BColor bg) {
         if (isLoggingTimestamps()) {
@@ -209,9 +217,7 @@ public class UnixColoredPrinter extends AbstractColoredPrinter {
     }
 
     /**
-     * Prints an error message to terminal with a new line at the end.
-     *
-     * @param msg The error message to print.
+     * {@inheritDoc}
      */
     @Override
     public void errorPrintln(Object msg) {
@@ -223,6 +229,9 @@ public class UnixColoredPrinter extends AbstractColoredPrinter {
         System.err.println(msg);
     }
 
+    /**
+     * {@inheritDoc}
+     */
     @Override
     public void errorPrintln(Object msg, Attribute attr, FColor fg, BColor bg) {
         if (isLoggingTimestamps()) {
@@ -234,9 +243,7 @@ public class UnixColoredPrinter extends AbstractColoredPrinter {
     }
 
     /**
-     * Prints a debug message to terminal.
-     *
-     * @param msg Debug message to print
+     * {@inheritDoc}
      */
     @Override
     public void debugPrint(Object msg) {
@@ -244,6 +251,9 @@ public class UnixColoredPrinter extends AbstractColoredPrinter {
             print(msg);
     }
 
+    /**
+     * {@inheritDoc}
+     */
     @Override
     public void debugPrint(Object msg, Attribute attr, FColor fg, BColor bg) {
         if (isLoggingDebug())
@@ -251,11 +261,7 @@ public class UnixColoredPrinter extends AbstractColoredPrinter {
     }
 
     /**
-     * Prints a debug message to terminal if the printer has enough level of
-     * debug to print that message.
-     *
-     * @param msg   Debug message to print
-     * @param level Level of debug needed to print msg
+     * {@inheritDoc}
      */
     @Override
     public void debugPrint(Object msg, int level) {
@@ -263,6 +269,9 @@ public class UnixColoredPrinter extends AbstractColoredPrinter {
             print(msg);
     }
 
+    /**
+     * {@inheritDoc}
+     */
     @Override
     public void debugPrint(Object msg, int level, Attribute attr, FColor fg, BColor bg) {
         if (canPrint(level))
@@ -270,26 +279,23 @@ public class UnixColoredPrinter extends AbstractColoredPrinter {
     }
 
     /**
-     * Prints a debug message (with a newline at the end) to terminal.
-     *
-     * @param msg Debug message to print
+     * {@inheritDoc}
      */
     @Override
     public void debugPrintln(Object msg) {
         println(msg);
     }
 
+    /**
+     * {@inheritDoc}
+     */
     @Override
     public void debugPrintln(Object msg, Attribute attr, FColor fg, BColor bg) {
         println(msg, attr, fg, bg);
     }
 
     /**
-     * Prints a debug message (with a newline at the end) to terminal if the
-     * printer has enough level of debug to print that message.
-     *
-     * @param msg   Debug message to print
-     * @param level Level of debug needed to print msg
+     * {@inheritDoc}
      */
     @Override
     public void debugPrintln(Object msg, int level) {
@@ -297,6 +303,9 @@ public class UnixColoredPrinter extends AbstractColoredPrinter {
             println(msg);
     }
 
+    /**
+     * {@inheritDoc}
+     */
     @Override
     public void debugPrintln(Object msg, int level, Attribute attr, FColor fg, BColor bg) {
         if (canPrint(level))
