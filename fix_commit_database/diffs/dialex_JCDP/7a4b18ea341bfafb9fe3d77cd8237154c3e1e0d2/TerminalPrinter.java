@@ -8,7 +8,7 @@ import com.diogonunes.jcdp.api.AbstractPrinter;
 /**
  * This class is a Terminal implementation of the Printer interface, hence all
  * output is sent to standard output. It implements all abstract methods
- * inherited from the {@link print.PrinterTemplate} class.
+ * inherited from the {@link com.diogonunes.jcdp.api.AbstractPrinter} class.
  * 
  * @version 2.0
  * @author Diogo Nunes
@@ -115,7 +115,7 @@ public class TerminalPrinter extends AbstractPrinter {
 	 */
 	@Override
 	public void printTimestamp() {
-		System.out.print(getDateTime() + " "); /* time + whitespace */
+		System.out.print(getDateTime() + " ");
 	}
 
 	/**
