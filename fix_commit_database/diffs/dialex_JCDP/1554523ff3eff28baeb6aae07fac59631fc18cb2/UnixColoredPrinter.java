@@ -143,12 +143,7 @@ public class UnixColoredPrinter extends AbstractColoredPrinter {
      */
     @Override
     public void print(Object msg) {
-        if (isLoggingTimestamps()) {
-            printTimestamp();
-        } else {
-            System.out.print(generateCode());
-        }
-        System.out.print(msg);
+        printWithColor(msg, generateCode(), false);
     }
 
     /**
@@ -156,12 +151,7 @@ public class UnixColoredPrinter extends AbstractColoredPrinter {
      */
     @Override
     public void print(Object msg, Attribute attr, FColor fg, BColor bg) {
-        if (isLoggingTimestamps()) {
-            printTimestamp();
-        } else {
-            System.out.print(generateCode(attr, fg, bg));
-        }
-        System.out.print(msg);
+        printWithColor(msg, generateCode(attr, fg, bg), false);
     }
 
     /**
@@ -169,12 +159,7 @@ public class UnixColoredPrinter extends AbstractColoredPrinter {
      */
     @Override
     public void println(Object msg) {
-        if (isLoggingTimestamps()) {
-            printTimestamp();
-        } else {
-            System.out.print(generateCode());
-        }
-        System.out.println(msg);
+        printWithColor(msg, generateCode(), true);
     }
 
     /**
@@ -182,12 +167,7 @@ public class UnixColoredPrinter extends AbstractColoredPrinter {
      */
     @Override
     public void println(Object msg, Attribute attr, FColor fg, BColor bg) {
-        if (isLoggingTimestamps()) {
-            printTimestamp();
-        } else {
-            System.out.print(generateCode(attr, fg, bg));
-        }
-        System.out.println(msg);
+        printWithColor(msg, generateCode(attr, fg, bg), true);
     }
 
     /**
@@ -322,4 +302,14 @@ public class UnixColoredPrinter extends AbstractColoredPrinter {
                 + " | Foreground color: " + getForegroundColor().name() + " | Background color: "
                 + getBackgroundColor().name();
     }
+
+    private void printWithColor(Object msg, String ansiFormatCode, boolean appendNewline) {
+        StringBuilder output = new StringBuilder();
+        output.append(ansiFormatCode);
+        output.append(isLoggingTimestamps() ? getDateFormatted() + " " : "");
+        output.append(msg);
+        output.append(appendNewline ?  '\n' : "");
+
+        System.out.print(output.toString());
+    }
 }
\ No newline at end of file
