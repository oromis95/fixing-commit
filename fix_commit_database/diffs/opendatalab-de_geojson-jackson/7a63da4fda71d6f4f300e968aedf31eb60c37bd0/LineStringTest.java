@@ -18,7 +18,7 @@ public class LineStringTest {
 	@Test
 	public void itShouldSerializeMultiPoint() throws Exception {
 		MultiPoint lineString = new LineString(new LngLatAlt(100, 0), new LngLatAlt(101, 1));
-		assertEquals("{\"type\":\"LineString\",\"properties\":{},\"coordinates\":[[100.0,0.0],[101.0,1.0]]}",
+		assertEquals("{\"type\":\"LineString\",\"coordinates\":[[100.0,0.0],[101.0,1.0]]}",
 				mapper.writeValueAsString(lineString));
 	}
 
