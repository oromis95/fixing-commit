@@ -18,9 +18,9 @@ public class GeometryCollectionTest {
 		GeometryCollection gc = new GeometryCollection();
 		gc.add(new Point(100, 0));
 		gc.add(new LineString(new LngLatAlt(101, 0), new LngLatAlt(102, 1)));
-		assertEquals("{\"type\":\"GeometryCollection\",\"properties\":{},"
-						+ "\"geometries\":[{\"type\":\"Point\",\"properties\":{},\"coordinates\":[100.0,0.0]},"
-						+ "{\"type\":\"LineString\",\"properties\":{},\"coordinates\":[[101.0,0.0],[102.0,1.0]]}]}",
+		assertEquals("{\"type\":\"GeometryCollection\","
+						+ "\"geometries\":[{\"type\":\"Point\",\"coordinates\":[100.0,0.0]},"
+						+ "{\"type\":\"LineString\",\"coordinates\":[[101.0,0.0],[102.0,1.0]]}]}",
 				mapper.writeValueAsString(gc));
 	}
 }
