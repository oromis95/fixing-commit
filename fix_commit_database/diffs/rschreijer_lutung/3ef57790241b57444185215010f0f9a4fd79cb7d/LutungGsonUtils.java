@@ -1,5 +1,5 @@
 /**
- * 
+ *
  */
 package com.microtripit.mandrillapp.lutung.model;
 
@@ -28,72 +28,71 @@ import java.util.TimeZone;
  */
 public final class LutungGsonUtils {
 	private static final String dateFormatStr = "yyyy-MM-dd HH:mm:ss";
-	
+
 	private static Gson gson = createGson();
-	
+
 	public static final Gson getGson() {
 		return gson;
 	}
-	
+
 	public static final Gson createGson() {
 		return createGsonBuilder().create();
 	}
-	
+
 	public static final GsonBuilder createGsonBuilder() {
 		return new GsonBuilder()
 				.setDateFormat(dateFormatStr)
 				.registerTypeAdapter(Date.class, new DateDeserializer())
 				.registerTypeAdapter(Map.class, new MapSerializer())
-                .registerTypeAdapter(MandrillMessage.Recipient.Type.class, 
+                .registerTypeAdapter(MandrillMessage.Recipient.Type.class,
                 		new RecipientTypeSerializer());
 	}
-	
-	public static final class DateDeserializer 
+
+	public static final class DateDeserializer
 			implements JsonDeserializer<Date>, JsonSerializer<Date> {
-		
-		private final SimpleDateFormat formatter;
-		
-		protected DateDeserializer() {
-			formatter = new SimpleDateFormat(dateFormatStr);
+
+		private SimpleDateFormat getNewDefaultDateTimeFormat() {
+			final SimpleDateFormat formatter = new SimpleDateFormat(dateFormatStr);
 			formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
+			return formatter;
 		}
-		
-		public final Date deserialize(final JsonElement json, 
-				final Type typeOfT, 
-				final JsonDeserializationContext context) 
+
+		public final Date deserialize(final JsonElement json,
+				final Type typeOfT,
+				final JsonDeserializationContext context)
 						throws JsonParseException {
-			
+
 			if(!json.isJsonPrimitive()) {
 				throw new JsonParseException(
 						"Unexpected type for date: " +json.toString());
-				
+
 			}
 			try {
-				return formatter.parse(json.getAsString());
-				
+				return getNewDefaultDateTimeFormat().parse(json.getAsString());
+
 			} catch(final ParseException e) {
-				throw new JsonParseException("Failed to parse date '" 
+				throw new JsonParseException("Failed to parse date '"
 						+json.getAsString()+ "'", e);
-				
+
 			}
 		}
-		
+
 		public JsonElement serialize(
 				final Date src,
 				final Type typeOfSrc,
 				final JsonSerializationContext context) {
-			
-			return new JsonPrimitive(formatter.format(src));
+
+			return new JsonPrimitive(getNewDefaultDateTimeFormat().format(src));
 		}
 	}
-	
+
 	public static class MapSerializer implements JsonSerializer<Map<? extends Object,? extends Object>> {
-		
+
 		public final JsonElement serialize(
-				final Map<?, ?> src, 
+				final Map<?, ?> src,
 				final Type typeOfSrc,
 				final JsonSerializationContext context) {
-			
+
 			Object value;
 			final JsonObject json = new JsonObject();
 			for(Object key : src.keySet()) {
@@ -102,9 +101,9 @@ public final class LutungGsonUtils {
 						value, value.getClass()) );
 			}
 			return json;
-			
+
 		}
-		
+
 	}
 
 	public static final class RecipientTypeSerializer
@@ -112,11 +111,11 @@ public final class LutungGsonUtils {
 				JsonSerializer<MandrillMessage.Recipient.Type> {
 
 		public final MandrillMessage.Recipient.Type deserialize(
-				final JsonElement json, 
+				final JsonElement json,
 				final Type typeOfT,
 				final JsonDeserializationContext context)
 						throws JsonParseException {
-			
+
 			if(!json.isJsonPrimitive()) {
 				throw new JsonParseException(
 						"Unexpected type for recipient type: " +json.toString());
@@ -129,7 +128,7 @@ public final class LutungGsonUtils {
 
 		public JsonPrimitive serialize(
 				final MandrillMessage.Recipient.Type src,
-				final Type typeOfSrc, 
+				final Type typeOfSrc,
 				final JsonSerializationContext context) {
 
 			return new JsonPrimitive(src.name().toLowerCase());
