@@ -32,8 +32,8 @@ public class MandrillWebhooksApi {
 	/**
 	 * <p>Get the list of all webhooks defined for this account.</p>
 	 * @return An array of {@link MandrillWebhook} objects.
-	 * @throws MandrillApiError
-	 * @throws IOException
+	 * @throws MandrillApiError Mandrill API Error
+	 * @throws IOException IO Error
 	 */
 	public MandrillWebhook[] list() throws MandrillApiError, 
 			IOException {
@@ -56,13 +56,8 @@ public class MandrillWebhooksApi {
 	 * {@link MandrillWebhook#UNSUB} and {@link MandrillWebhook#REJECT} 
 	 * as valid events.
 	 * @return A {@link MandrillWebhook} object with info about the new webhook.
-	 * @throws MandrillApiError
-	 * @throws IOException
-	 * @see {@link MandrillWebhook#SEND}, {@link MandrillWebhook#HARD_BOUNCE}, 
-	 * {@link MandrillWebhook#SOFT_BOUNCE}, {@link MandrillWebhook#OPEN}, 
-	 * {@link MandrillWebhook#CLICK}, {@link MandrillWebhook#SPAM}, 
-	 * {@link MandrillWebhook#UNSUB} and {@link MandrillWebhook#REJECT} 
-	 * for valid events.
+	 * @throws MandrillApiError Mandrill API Error
+	 * @throws IOException IO Error
 	 */
 	public MandrillWebhook add(final String url, final String event) 
 			throws MandrillApiError, IOException {
@@ -84,13 +79,8 @@ public class MandrillWebhooksApi {
 	 * {@link MandrillWebhook#UNSUB} and {@link MandrillWebhook#REJECT} 
 	 * as valid events.
 	 * @return A {@link MandrillWebhook} object with info about the new webhook.
-	 * @throws MandrillApiError
-	 * @throws IOException
-	 * @see {@link MandrillWebhook#SEND}, {@link MandrillWebhook#HARD_BOUNCE}, 
-	 * {@link MandrillWebhook#SOFT_BOUNCE}, {@link MandrillWebhook#OPEN}, 
-	 * {@link MandrillWebhook#CLICK}, {@link MandrillWebhook#SPAM}, 
-	 * {@link MandrillWebhook#UNSUB} and {@link MandrillWebhook#REJECT} 
-	 * for valid events.
+	 * @throws MandrillApiError Mandrill API Error
+	 * @throws IOException IO Error
 	 */
 	public MandrillWebhook add(final String url, 
 			final Collection<String> events) throws MandrillApiError, 
@@ -112,13 +102,8 @@ public class MandrillWebhooksApi {
 	 * {@link MandrillWebhook#UNSUB} and {@link MandrillWebhook#REJECT} 
 	 * as valid events.
 	 * @return A {@link MandrillWebhook} object with info about the new webhook.
-	 * @throws MandrillApiError
-	 * @throws IOException
-	 * @see {@link MandrillWebhook#SEND}, {@link MandrillWebhook#HARD_BOUNCE}, 
-	 * {@link MandrillWebhook#SOFT_BOUNCE}, {@link MandrillWebhook#OPEN}, 
-	 * {@link MandrillWebhook#CLICK}, {@link MandrillWebhook#SPAM}, 
-	 * {@link MandrillWebhook#UNSUB} and {@link MandrillWebhook#REJECT} 
-	 * for valid events.
+	 * @throws MandrillApiError Mandrill API Error
+	 * @throws IOException IO Error
 	 */
 	public MandrillWebhook add(final String url, final String description, 
 			final Collection<String> events) throws MandrillApiError, IOException {
@@ -134,11 +119,10 @@ public class MandrillWebhooksApi {
 	
 	/**
 	 * <p>Get the data about an existing webhook.</p>
-	 * @param id The unique identifier of a webhook 
-	 * belonging to this account.
+	 * @param id The unique identifier of a webhook belonging to this account.
 	 * @return A {@link MandrillWebhook} object with info about the webhook.
-	 * @throws MandrillApiError
-	 * @throws IOException
+	 * @throws MandrillApiError Mandrill API Error
+	 * @throws IOException IO Error
 	 */
 	public MandrillWebhook info(final Integer id) 
 			throws MandrillApiError, IOException {
@@ -152,24 +136,17 @@ public class MandrillWebhooksApi {
 	
 	/**
 	 * <p>Update an existing webhook.</p>
-	 * @param id The unique identifier of a webhook 
-	 * belonging to this account.
+	 * @param id The unique identifier of a webhook belonging to this account.
 	 * @param url The URL to POST batches of events.
-	 * @param event An optional events that will be posted 
-	 * to the webhook. You can use
+	 * @param event An optional events that will be posted to the webhook. You can use
 	 * {@link MandrillWebhook#SEND}, {@link MandrillWebhook#HARD_BOUNCE}, 
 	 * {@link MandrillWebhook#SOFT_BOUNCE}, {@link MandrillWebhook#OPEN}, 
 	 * {@link MandrillWebhook#CLICK}, {@link MandrillWebhook#SPAM}, 
 	 * {@link MandrillWebhook#UNSUB} and {@link MandrillWebhook#REJECT} 
 	 * as valid events.
 	 * @return A {@link MandrillWebhook} object with info about the webhook.
-	 * @throws MandrillApiError
-	 * @throws IOException
-	 * @see {@link MandrillWebhook#SEND}, {@link MandrillWebhook#HARD_BOUNCE}, 
-	 * {@link MandrillWebhook#SOFT_BOUNCE}, {@link MandrillWebhook#OPEN}, 
-	 * {@link MandrillWebhook#CLICK}, {@link MandrillWebhook#SPAM}, 
-	 * {@link MandrillWebhook#UNSUB} and {@link MandrillWebhook#REJECT} 
-	 * for valid events.
+	 * @throws MandrillApiError Mandrill API Error
+	 * @throws IOException IO Error
 	 */
 	public MandrillWebhook update(final Integer id, 
 			final String url, final String event) 
@@ -186,21 +163,10 @@ public class MandrillWebhooksApi {
 	 * @param id The unique identifier of a webhook 
 	 * belonging to this account.
 	 * @param url The URL to POST batches of events.
-	 * @param events An optional collection of events 
-	 * that will be posted to the webhook. You can use
-	 * {@link MandrillWebhook#SEND}, {@link MandrillWebhook#HARD_BOUNCE}, 
-	 * {@link MandrillWebhook#SOFT_BOUNCE}, {@link MandrillWebhook#OPEN}, 
-	 * {@link MandrillWebhook#CLICK}, {@link MandrillWebhook#SPAM}, 
-	 * {@link MandrillWebhook#UNSUB} and {@link MandrillWebhook#REJECT} 
-	 * as valid events.
+	 * @param events An optional collection of events that will be posted to the webhook.
 	 * @return A {@link MandrillWebhook} object with info about the webhook.
-	 * @throws MandrillApiError
-	 * @throws IOException
-	 * @see {@link MandrillWebhook#SEND}, {@link MandrillWebhook#HARD_BOUNCE}, 
-	 * {@link MandrillWebhook#SOFT_BOUNCE}, {@link MandrillWebhook#OPEN}, 
-	 * {@link MandrillWebhook#CLICK}, {@link MandrillWebhook#SPAM}, 
-	 * {@link MandrillWebhook#UNSUB} and {@link MandrillWebhook#REJECT} 
-	 * for valid events.
+	 * @throws MandrillApiError Mandrill API Error
+	 * @throws IOException IO Error
 	 */
 	public MandrillWebhook update(final Integer id, 
 			final String url, final Collection<String> events) 
@@ -212,25 +178,13 @@ public class MandrillWebhooksApi {
 	
 	/**
 	 * <p>Update an existing webhook.</p>
-	 * @param id The unique identifier of a webhook 
-	 * belonging to this account.
+	 * @param id The unique identifier of a webhook belonging to this account.
 	 * @param url The URL to POST batches of events.
-	 * @param An optional description for the webhook.
-	 * @param events An optional collection of events 
-	 * that will be posted to the webhook. You can use
-	 * {@link MandrillWebhook#SEND}, {@link MandrillWebhook#HARD_BOUNCE}, 
-	 * {@link MandrillWebhook#SOFT_BOUNCE}, {@link MandrillWebhook#OPEN}, 
-	 * {@link MandrillWebhook#CLICK}, {@link MandrillWebhook#SPAM}, 
-	 * {@link MandrillWebhook#UNSUB} and {@link MandrillWebhook#REJECT} 
-	 * as valid events.
+	 * @param description An optional description for the webhook.
+	 * @param events An optional collection of events that will be posted to the webhook.
 	 * @return A {@link MandrillWebhook} object with info about the webhook.
-	 * @throws MandrillApiError
-	 * @throws IOException
-	 * @see {@link MandrillWebhook#SEND}, {@link MandrillWebhook#HARD_BOUNCE}, 
-	 * {@link MandrillWebhook#SOFT_BOUNCE}, {@link MandrillWebhook#OPEN}, 
-	 * {@link MandrillWebhook#CLICK}, {@link MandrillWebhook#SPAM}, 
-	 * {@link MandrillWebhook#UNSUB} and {@link MandrillWebhook#REJECT} 
-	 * for valid events.
+	 * @throws MandrillApiError Mandrill API Error
+	 * @throws IOException IO Error
 	 */
 	public MandrillWebhook update(final Integer id, final String url, 
 			final String description, final Collection<String> events) 
@@ -248,12 +202,10 @@ public class MandrillWebhooksApi {
 	
 	/**
 	 * <p>Delete an existing webhook.</p>
-	 * @param id The unique identifier of a webhook 
-	 * belonging to this account.
-	 * @return A {@link MandrillWebhook} object with info 
-	 * about the just deleted webhook.
-	 * @throws MandrillApiError
-	 * @throws IOException
+	 * @param id The unique identifier of a webhook belonging to this account.
+	 * @return A {@link MandrillWebhook} object with info about the just deleted webhook.
+	 * @throws MandrillApiError Mandrill API Error
+	 * @throws IOException IO Error
 	 */
 	public MandrillWebhook delete(final Integer id) 
 			throws MandrillApiError, IOException {
