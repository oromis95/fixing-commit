@@ -26,8 +26,8 @@ public class MandrillSubaccountsApi {
 	 * @param q An optional prefix to filter the 
 	 * subaccounts' ids and names.
 	 * @return The subaccounts for the account, up to a maximum of 1000.
-	 * @throws MandrillApiError
-	 * @throws IOException
+	 * @throws MandrillApiError Mandrill API Error
+	 * @throws IOException IO Error
 	 */
 	public MandrillSubaccountInfo[] list(final String q) 
 			throws MandrillApiError, IOException {
@@ -51,8 +51,8 @@ public class MandrillSubaccountsApi {
 	 * for the subaccount. If not specified, Mandrill will 
 	 * manage this based on reputation.
 	 * @return The information saved about the new subaccount.
-	 * @throws MandrillApiError
-	 * @throws IOException
+	 * @throws MandrillApiError Mandrill API Error
+	 * @throws IOException IO Error
 	 */
 	public MandrillSubaccountInfo add(final String id, final String name, 
 			final String notes, final Integer customQuota)  
@@ -72,8 +72,8 @@ public class MandrillSubaccountsApi {
 	 * <p>Given the ID of an existing subaccount, return the data about it.</p>
 	 * @param id The unique identifier of the subaccount to query.
 	 * @return The information about the subaccount.
-	 * @throws MandrillApiError
-	 * @throws IOException
+	 * @throws MandrillApiError Mandrill API Error
+	 * @throws IOException IO Error
 	 */
 	public MandrillSubaccountInfo info(final String id) 
 			throws MandrillApiError, IOException {
@@ -97,8 +97,8 @@ public class MandrillSubaccountsApi {
 	 * for the subaccount. If not specified, Mandrill will 
 	 * manage this based on reputation.
 	 * @return The information for the updated subaccount.
-	 * @throws MandrillApiError
-	 * @throws IOException
+	 * @throws MandrillApiError Mandrill API Error
+	 * @throws IOException IO Error
 	 */
 	public MandrillSubaccountInfo update(final String id, 
 			final String name, final String notes, final Integer customQuota)  
@@ -120,8 +120,8 @@ public class MandrillSubaccountsApi {
 	 * future sending calls to this subaccount will fail.</p>
 	 * @param id The unique identifier of the subaccount.
 	 * @return The information about the subaccount.
-	 * @throws MandrillApiError
-	 * @throws IOException
+	 * @throws MandrillApiError Mandrill API Error
+	 * @throws IOException IO Error
 	 */
 	public MandrillSubaccountInfo delete(final String id) 
 			throws MandrillApiError, IOException {
@@ -139,8 +139,8 @@ public class MandrillSubaccountsApi {
 	 * until the subaccount is resumed.</p>
 	 * @param id The unique identifier of the subaccount to pause.
 	 * @return The information for the paused subaccount.
-	 * @throws MandrillApiError
-	 * @throws IOException
+	 * @throws MandrillApiError Mandrill API Error
+	 * @throws IOException IO Error
 	 */
 	public MandrillSubaccountInfo pause(final String id) 
 			throws MandrillApiError, IOException {
@@ -156,8 +156,8 @@ public class MandrillSubaccountsApi {
 	 * <p>Resume a paused subaccount's sending.</p>
 	 * @param id The unique identifier of the subaccount to resume.
 	 * @return The information for the resumed subaccount.
-	 * @throws MandrillApiError
-	 * @throws IOException
+	 * @throws MandrillApiError Mandrill API Error
+	 * @throws IOException IO Error
 	 */
 	public MandrillSubaccountInfo resume(final String id) 
 			throws MandrillApiError, IOException {
