@@ -58,7 +58,7 @@ public final class IrcConnection {
 	/** Whether this library should call garbage collection. */
 	protected static final boolean GARBAGE_COLLECTION = true;
 	/** sIRC Library version. */
-	public static final String VERSION = "1.1.2";
+	public static final String VERSION = "1.1.3b";
 	/** Advanced listener. */
 	private AdvancedListener advancedListener = null;
 	/** Connection InputStream thread. */
