@@ -85,7 +85,7 @@ final class IrcInput extends Thread {
 	 */
 	private void handleLine(final String line) {
 		// transform the raw line into an easier format
-		final IrcDecoder parser = new IrcDecoder(line, this.irc);
+		final IrcPacket parser = new IrcPacket(line, this.irc);
 		// Handle numeric server replies.
 		if (parser.isNumeric()) {
 			IrcParser.parseNumeric(this.irc, parser);
@@ -107,7 +107,7 @@ final class IrcInput extends Thread {
 				IrcDebug.log("<<< " + line);
 				// always respond to PING
 				if (line.startsWith("PING ")) {
-					this.irc.out.sendNowEx("PONG " + line.substring(5));
+					this.irc.out.pong(line.substring(5));
 				} else {
 					this.handleLine(line);
 				}
