@@ -58,7 +58,7 @@ public final class ClientState {
 	 * @param channel The channel to add.
 	 */
 	protected void addChannel(final Channel channel) {
-		if (!this.channels.containsKey(channel.getName())) {
+		if (!this.channels.containsKey(channel.getName().toLowerCase())) {
 			this.channels.put(channel.getName().toLowerCase(), channel);
 		}
 	}
@@ -83,7 +83,7 @@ public final class ClientState {
 	 *         (The local user is not in that channel)
 	 */
 	protected Channel getChannel(final String channel) {
- 		if (this.channels.containsKey(channel.toLowerCase())) {
+ 		if (channel != null && this.channels.containsKey(channel.toLowerCase())) {
 			return this.channels.get(channel.toLowerCase());
 		}
 		return null;
@@ -114,7 +114,7 @@ public final class ClientState {
 	 * @return True if the channel is in the list, false otherwise.
 	 */
 	protected boolean hasChannel(final String name) {
-		return this.channels.containsKey(name.toLowerCase());
+		return name != null && this.channels.containsKey(name.toLowerCase());
 	}
 	
 	/**
@@ -130,7 +130,7 @@ public final class ClientState {
 	 * @param channel The channel name.
 	 */
 	protected void removeChannel(final String channel) {
-		if (this.channels.containsKey(channel.toLowerCase())) {
+		if (channel != null && this.channels.containsKey(channel.toLowerCase())) {
 			this.channels.remove(channel.toLowerCase());
 		}
 	}
