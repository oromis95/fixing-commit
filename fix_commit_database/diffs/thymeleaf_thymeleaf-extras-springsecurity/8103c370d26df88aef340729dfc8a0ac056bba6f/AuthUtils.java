@@ -44,6 +44,7 @@
 import org.springframework.security.core.Authentication;
 import org.springframework.security.core.context.SecurityContextHolder;
 import org.springframework.security.web.FilterInvocation;
+import org.springframework.security.web.WebAttributes;
 import org.springframework.security.web.access.WebInvocationPrivilegeEvaluator;
 import org.thymeleaf.TemplateEngine;
 import org.thymeleaf.context.IExpressionContext;
@@ -267,7 +268,7 @@ public static boolean authorizeUsingUrlCheck(
         }
         
         final boolean result =
-                getPrivilegeEvaluator(servletContext).isAllowed(
+                getPrivilegeEvaluator(servletContext, request).isAllowed(
                     request.getContextPath(), url, method, authentication) ? 
                             true : false;
 
@@ -286,7 +287,15 @@ public static boolean authorizeUsingUrlCheck(
 
 
     
-    private static WebInvocationPrivilegeEvaluator getPrivilegeEvaluator(final ServletContext servletContext) {
+    private static WebInvocationPrivilegeEvaluator getPrivilegeEvaluator(
+            final ServletContext servletContext, final HttpServletRequest request) {
+
+        final WebInvocationPrivilegeEvaluator privEvaluatorFromRequest =
+                (WebInvocationPrivilegeEvaluator) request.getAttribute(WebAttributes.WEB_INVOCATION_PRIVILEGE_EVALUATOR_ATTRIBUTE);
+        if (privEvaluatorFromRequest != null) {
+            return privEvaluatorFromRequest;
+        }
+
         final ApplicationContext ctx = getContext(servletContext);
 
         final Map<String, WebInvocationPrivilegeEvaluator> privilegeEvaluators = 
