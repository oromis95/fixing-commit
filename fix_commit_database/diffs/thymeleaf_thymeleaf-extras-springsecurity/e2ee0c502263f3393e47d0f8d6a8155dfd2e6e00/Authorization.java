@@ -19,14 +19,10 @@
  */
 package org.thymeleaf.extras.springsecurity3.auth;
 
-import java.util.List;
-
 import javax.servlet.ServletContext;
 import javax.servlet.http.HttpServletRequest;
 import javax.servlet.http.HttpServletResponse;
 
-import org.springframework.context.ApplicationContext;
-import org.springframework.security.acls.model.Permission;
 import org.springframework.security.core.Authentication;
 import org.thymeleaf.context.IProcessingContext;
 import org.thymeleaf.util.Validate;
@@ -126,22 +122,6 @@ public boolean url(final String method, final String url) {
                 url, method, this.authentication, this.request, this.servletContext);
         
     }
-    
-
-    
-    public boolean acl(final Object domainObject, final String permissions) {
-        
-        Validate.notEmpty(permissions, "permissions cannot be null or empty");
-
-        final ApplicationContext applicationContext = AuthUtils.getContext(this.servletContext);
-        
-        final List<Permission> permissionsList =
-                AuthUtils.parsePermissionsString(applicationContext, permissions);        
-        
-        return AuthUtils.authorizeUsingAccessControlList(
-                domainObject, permissionsList, this.authentication, this.servletContext);
-        
-    }
 
     
 }
