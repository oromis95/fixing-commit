@@ -72,7 +72,7 @@ protected boolean isVisible(
             return false;
         }
 
-        final Authentication authentication = AuthUtils.getAuthenticationObject();
+        final Authentication authentication = AuthUtils.getAuthenticationObject(context);
         if (authentication == null) {
             return false;
         }
