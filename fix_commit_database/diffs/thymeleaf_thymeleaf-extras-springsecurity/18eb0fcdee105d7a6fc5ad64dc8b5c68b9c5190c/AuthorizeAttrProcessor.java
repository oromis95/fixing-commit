@@ -64,7 +64,7 @@ protected boolean isVisible(
             return false;
         }
 
-        final Authentication authentication = AuthUtils.getAuthenticationObject();
+        final Authentication authentication = AuthUtils.getAuthenticationObject(context);
 
         if (authentication == null) {
             return false;
