@@ -76,7 +76,7 @@
 
     static {
 
-        if (!SpringVersionUtils.isSpringWebReactivePresent()) {
+        if (!SpringVersionUtils.isSpringWebFluxPresent()) {
 
             EXECUTION_ATTRIBUTES = null;
 
