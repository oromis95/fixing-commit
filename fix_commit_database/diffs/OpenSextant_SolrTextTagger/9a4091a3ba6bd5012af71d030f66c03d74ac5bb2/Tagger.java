@@ -123,7 +123,7 @@ public abstract class Tagger {
         // TODO cache hashcodes of valid first terms (directly from char[]?) to skip lookups?
         termsEnum = terms.iterator(termsEnum);
         if (cursor == null)//re-usable
-          cursor = new TermPrefixCursor();
+          cursor = new TermPrefixCursor(liveDocs);
         if (cursor.advanceFirst(term, termsEnum)) {
           TagLL newTail = new TagLL(head, cursor, offsetAtt.startOffset(), offsetAtt.endOffset(), null);
           termsEnum = null;//because the cursor now "owns" this instance
@@ -188,82 +188,5 @@ public abstract class Tagger {
     return (DocsEnum) docIdsKey;
   }
 
-  class TermPrefixCursor {
-
-    static final byte SEPARATOR_CHAR = ' ';
-    BytesRef prefixBuf;
-    TermsEnum termsEnum;
-    DocsEnum docsEnum;
-
-    boolean advanceFirst(BytesRef word, TermsEnum termsEnum) throws IOException {
-      this.termsEnum = termsEnum;
-      prefixBuf = word;//don't copy it unless we have to
-      if (seekPrefix()) {//... and we have to
-        prefixBuf = new BytesRef(64);
-        prefixBuf.copyBytes(word);
-        return true;
-      } else {
-        prefixBuf = null;//just to be darned sure 'word' isn't referenced here
-        return false;
-      }
-    }
-
-    boolean advanceNext(BytesRef word) throws IOException {
-      //append to existing
-      prefixBuf.grow(1 + word.length);
-      prefixBuf.bytes[prefixBuf.length++] = SEPARATOR_CHAR;
-      prefixBuf.append(word);
-      return seekPrefix();
-    }
-
-    /** Seeks to prefixBuf or the next prefix of it. Sets docsEnum. **/
-    private boolean seekPrefix() throws IOException {
-      TermsEnum.SeekStatus seekStatus = termsEnum.seekCeil(prefixBuf);
-
-      docsEnum = null;//can't re-use :-(
-      switch (seekStatus) {
-        case END:
-          return false;
-
-        case FOUND:
-          docsEnum = termsEnum.docs(liveDocs, docsEnum, DocsEnum.FLAG_NONE);
-          if (liveDocs == null)//then docsEnum is guaranteed to match docs
-            return true;
-
-          //need to verify there are indeed docs, which might not be so when there is a filter
-          if (docsEnum.nextDoc() != DocsEnum.NO_MORE_DOCS) {
-            docsEnum = termsEnum.docs(liveDocs, docsEnum, DocsEnum.FLAG_NONE);//reset
-            return true;
-          }
-          //Pretend we didn't find it; go to next term
-          docsEnum = null;
-          if (termsEnum.next() == null) { // case END
-            return false;
-          }
-          //fall through to NOT_FOUND
-
-        case NOT_FOUND:
-          //termsEnum must start with prefixBuf to continue
-          BytesRef teTerm = termsEnum.term();
-
-          if (teTerm.length > prefixBuf.length) {
-            for (int i = 0; i < prefixBuf.length; i++) {
-              if (prefixBuf.bytes[prefixBuf.offset + i] != teTerm.bytes[teTerm.offset + i])
-                return false;
-            }
-            if (teTerm.bytes[teTerm.offset + prefixBuf.length] != SEPARATOR_CHAR)
-              return false;
-            return true;
-          }
-          return false;
-      }
-      throw new IllegalStateException(seekStatus.toString());
-    }
-
-    /** should only be called after advance* returns true */
-    DocsEnum getDocsEnum() {
-      return docsEnum;
-    }
-  }
 }
 
