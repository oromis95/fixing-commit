@@ -30,6 +30,7 @@ public class HtmlInterpolationTest extends XmlInterpolationTest {
   public void setUp() throws Exception {
     super.setUp();
     baseParams.set("htmlOffsetAdjust", "true");
+    baseParams.set("matchText", "true");
   }
 
   @Override
@@ -58,6 +59,7 @@ public class HtmlInterpolationTest extends XmlInterpolationTest {
     assertXmlTag("start end <em>other text</em>", true);
     assertXmlTag("start end<em> other text</em>", true);
     assertXmlTag("<em>other text</em> start end", true);
+    assertXmlTag("start <td/> end", true);    
   }
 
   @Test
