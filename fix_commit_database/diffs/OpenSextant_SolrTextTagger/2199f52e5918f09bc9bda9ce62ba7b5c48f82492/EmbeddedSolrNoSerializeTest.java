@@ -37,12 +37,14 @@ import org.apache.solr.common.util.ContentStream;
 import org.apache.solr.common.util.ContentStreamBase;
 import org.junit.Before;
 import org.junit.BeforeClass;
+import org.junit.Ignore;
 import org.junit.Test;
 
 import java.io.IOException;
 import java.util.Collection;
 import java.util.Collections;
 import java.util.concurrent.atomic.AtomicReference;
+import java.util.function.BiFunction;
 
 /**
  * Tests that we can skip serialization of the documents when embedding
@@ -112,10 +114,23 @@ public class EmbeddedSolrNoSerializeTest extends SolrTestCaseJ4 {
   }
 
   @Test
-  public void testTagStreaming() throws IOException, SolrServerException {
+  public void testAssertTagStreamingWithSolrTaggerRequest() throws Exception {
+    doTestAssertTagStreaming(SolrTaggerRequest::new);
+  }
+
+  @Test @Ignore("As of Solr 7, stream.body is disabled by default for security ") // DWS: dubious, IMO
+  // and it can't be enabled with EmbeddedSolrServer until SOLR-12126
+  public void testAssertTagStreamingWithStreamBodyParam() throws Exception {
+    doTestAssertTagStreaming((params, input) -> {
+      params.set("stream.body", input);
+      return new QueryRequest(params);
+    });
+  }
+
+  public void doTestAssertTagStreaming(BiFunction<ModifiableSolrParams,String,QueryRequest> newQueryRequest) throws IOException, SolrServerException {
     ModifiableSolrParams params = params();
     String input = "foo boston bar";//just one tag;
-    QueryRequest req = new SolrTaggerRequest(params, input);
+    QueryRequest req = newQueryRequest.apply(params, input);
     req.setPath("/tag");
 
     final AtomicReference<SolrDocument> refDoc = new AtomicReference<>();
