@@ -74,7 +74,8 @@ public class XmlInterpolationTest extends AbstractTaggerTest {
   }
 
   protected void assertXmlTag(String docText, boolean expected) throws Exception {
-    try (SolrQueryRequest req = reqDoc(docText)) {
+    final SolrQueryRequest req = reqDoc(docText);
+    try { // 5.4 and beyond we can use try-with-resources
       final SolrQueryResponse rsp = h.queryAndResponse(req.getParams().get("qt"), req);
       final TestTag[] testTags = pullTagsFromResponse(req, rsp);
       if (!expected) {
@@ -84,6 +85,8 @@ public class XmlInterpolationTest extends AbstractTaggerTest {
         final TestTag tag = testTags[0];
         validateXml(insertAnchorAtOffsets(docText, tag.startOffset, tag.endOffset, tag.docName));
       }
+    } finally {
+      req.close();
     }
   }
 
