@@ -13,16 +13,20 @@ public class Join implements SubCommand{
 
 	public boolean onCommand(Player player, String[] args) {
 		if(args.length == 1){
-
-                    try {
-                        int a = Integer.parseInt(args[0]);
-                        GameManager.getInstance().addPlayer(player, a);
-                    } catch (NumberFormatException e) {
-                        MessageManager.getInstance().sendFMessage(PrefixType.ERROR, "error.notanumber", player, "input-" + args[0]);
-                    }
+			if(player.hasPermission(permission())){
+				try {
+					int a = Integer.parseInt(args[0]);
+					GameManager.getInstance().addPlayer(player, a);
+				} catch (NumberFormatException e) {
+					MessageManager.getInstance().sendFMessage(PrefixType.ERROR, "error.notanumber", player, "input-" + args[0]);
+				}
+			}
+			else{
+				MessageManager.getInstance().sendFMessage(PrefixType.WARNING, "error.nopermission", player);
+			}
 		}
 		else{
-			if(player.hasPermission(permission())){
+			if(player.hasPermission("sg.lobby.join")){
 				if(GameManager.getInstance().getPlayerGameId(player)!=-1){
 					MessageManager.getInstance().sendMessage(PrefixType.ERROR, "error.alreadyingame", player);
 					return true;
@@ -39,7 +43,7 @@ public class Join implements SubCommand{
 
 	@Override
 	public String help(Player p) {
-                return "/sg join - " + SettingsManager.getInstance().getMessageConfig().getString("messages.help.join", "Join the lobby");
+		return "/sg join - " + SettingsManager.getInstance().getMessageConfig().getString("messages.help.join", "Join the lobby");
 	}
 
 	@Override
