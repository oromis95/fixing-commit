@@ -50,7 +50,7 @@ public class MessageManager {
 	public void sendFMessage(PrefixType type, String input, Player player, String ... args) {
 		String msg = SettingsManager.getInstance().getMessageConfig().getString("messages."+input);
 		boolean enabled = SettingsManager.getInstance().getMessageConfig().getBoolean("messages."+input+"_enabled", true);
-		if(msg == null)player.sendMessage(ChatColor.RED+"Failed to load message for messages."+input);
+		if(msg == null){player.sendMessage(ChatColor.RED+"Failed to load message for messages."+input); return;}
 		if(!enabled)return;
 		if(args != null && args.length != 0){msg = MessageUtil.replaceVars(msg, args);}
 		msg = MessageUtil.replaceColors(msg);
