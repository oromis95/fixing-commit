@@ -25,7 +25,7 @@ public class LobbyWall {
 
     public boolean loadSign(World w, int x1, int x2, int z1, int z2, int y1) {
         boolean usingx = (x1 == x2) ? false : true;
-        System.out.println(w + " " + x1 + " " + x2 + " " + z1 + " " + z2 + " " + y1 + " " + usingx);
+        SurvivalGames.debug(w + " " + x1 + " " + x2 + " " + z1 + " " + z2 + " " + y1 + " " + usingx);
         int dir = new Location(w, x1, y1, z1).getBlock().getData();
         if (usingx) {
             for (int a = Math.max(x1, x2); a >= Math.min(x1, x2); a--) {
@@ -34,28 +34,28 @@ public class LobbyWall {
                 if (b instanceof Sign) {
                     signs.add((Sign) b);
                     LobbyManager.lobbychunks.add(b.getChunk());
-                    System.out.println("usingx - " + b.getLocation().toString());
+                    SurvivalGames.debug("usingx - " + b.getLocation().toString());
                 } else {
-                    System.out.println("Not a sign" + b.getType().toString());
+                    SurvivalGames.debug("Not a sign" + b.getType().toString());
                     return false;
                 }
             }
         } else {
             for (int a = Math.min(z1, z2); a <= Math.max(z1, z2); a++) {
-                System.out.println(a);
+            	SurvivalGames.debug(a);
                 Location l = new Location(w, x1, y1, a);
                 BlockState b = l.getBlock().getState();
                 if (b instanceof Sign) {
                     signs.add((Sign) b);
                     LobbyManager.lobbychunks.add(b.getChunk());
-                    System.out.println("notx - " + b.getLocation().toString());
+                    SurvivalGames.debug("notx - " + b.getLocation().toString());
                 } else {
-                    System.out.println("Not a sign" + b.getType().toString());
+                	SurvivalGames.debug("Not a sign" + b.getType().toString());
                     return false;
                 }
             }
         }
-        System.out.println("dir: " + dir);
+        SurvivalGames.debug("dir: " + dir);
         if (dir == 3 || dir == 5) {
             Collections.reverse(signs);
         }
