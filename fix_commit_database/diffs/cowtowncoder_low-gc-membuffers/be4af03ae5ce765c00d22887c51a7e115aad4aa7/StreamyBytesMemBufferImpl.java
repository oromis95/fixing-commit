@@ -3,6 +3,27 @@ package com.fasterxml.util.membuf.impl;
 import com.fasterxml.util.membuf.*;
 import com.fasterxml.util.membuf.base.BytesSegment;
 
+/**
+ * {@link StreamyBytesMemBuffer} implementation used for storing a sequence
+ * of byte values in a single byte sequence which does not retain
+ * boundaries implied by appends (as per {@link StreamyMemBuffer}).
+ * This means that ordering of bytes appended is retained on byte-by-byte
+ * basis, but there are no discrete grouping of sub-sequences (entries);
+ * all content is seen as sort of stream.
+ *<p>
+ * Access to queue is fully synchronized -- meaning that all methods are
+ * synchronized by implementations as necessary, and caller should not need
+ * to use external synchronization -- since parts will have to be anyway
+ * (updating of stats, pointers), and since all real-world use cases will
+ * need some level of synchronization anyway, even with just single producer
+ * and consumer. If it turns out that there are bottlenecks that could be
+ * avoided with more granular (or external) locking, this design can be
+ * revisited.
+ *<p>
+ * Note that if instances are discarded, they <b>MUST</b> be closed:
+ * finalize() method is not implemented since it is both somewhat unreliable
+ * (i.e. should not be counted on) and can add overhead for GC processing.
+ */
 public class StreamyBytesMemBufferImpl extends StreamyBytesMemBuffer
 {
     /*
