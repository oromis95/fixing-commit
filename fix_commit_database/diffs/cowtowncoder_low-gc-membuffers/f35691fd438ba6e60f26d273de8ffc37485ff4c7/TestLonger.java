@@ -17,30 +17,55 @@ public class TestLonger extends MembufTestBase
     
     public void testShakespeareLineByLine() throws Exception
     {
-        /* Input file is about 280k; assume +40% overhead (since
-         * entries are lines, relatively short). So, 400k should
-         * be enough; use 11 buffers of 40k each (one more than
+        // First, read the data
+        BufferedReader br = new BufferedReader(new InputStreamReader(
+                getClass().getResourceAsStream("/hamlet.xml"), ENCODING));
+        List<byte[]> rows = readRows(br);
+        br.close();
+
+        /* Input file is about 280k; assume modest overhead (even if
+         * entries are lines, relatively short). So, 330k should
+         * be enough; use 11 buffers of 30k each (one more than
          * absolutely needed)
          */
-        MemBuffers bufs = new MemBuffers(40 * 1024, 2, 11);
+        MemBuffers bufs = new MemBuffers(30 * 1024, 2, 11);
         MemBuffer buffer = bufs.createBuffer(2, 11);
 
-        BufferedReader br = new BufferedReader(new InputStreamReader(
-                getClass().getResourceAsStream("/hamlet.xml"), ENCODING));
-        ArrayList<String> lines = new ArrayList<String>(1000);
+        // then append/remove three times
+        appendAndRemove(rows, buffer);
+    }
+
+    /*
+    /**********************************************************************
+    /* Helper methods
+    /**********************************************************************
+     */
+
+    protected List<byte[]> readRows(BufferedReader br) throws IOException
+    {
+        ArrayList<byte[]> lines = new ArrayList<byte[]>(1000);
         String line;
-        int totalPayload = 0;
-        
         while ((line = br.readLine()) != null) {
-            lines.add(line);
-            byte[] b = line.getBytes(ENCODING);
+            lines.add(line.getBytes(ENCODING));
+        }
+        return lines;
+    }
+
+    protected void appendAndRemove(List<byte[]> rows, MemBuffer buffer)
+        throws InterruptedException
+    {
+        long totalPayload = 0L;
+        for (byte[] b : rows) {
             totalPayload += b.length;
             buffer.appendEntry(b);
+            assertEquals(totalPayload, buffer.getTotalPayloadLength());
         }
 
         // ok: should have added enough; verify book-keeping
-        assertEquals(lines.size(), buffer.getEntryCount());
+        assertEquals(rows.size(), buffer.getEntryCount());
         assertEquals(totalPayload, buffer.getTotalPayloadLength());
+        // we measured that it will take 10 segments for this data
+        assertEquals(10, buffer.getSegmentCount());
 
         /*
         System.err.println("DEBUG: read "+lines.size()+"; space left = "+buffer.getMaximumAvailableSpace()
@@ -50,11 +75,11 @@ public class TestLonger extends MembufTestBase
                 */
 
         // then read, verify ordering, invariants:
-        Iterator<String> it = lines.iterator();
-        int left = lines.size();
+        Iterator<byte[]> it = rows.iterator();
+        int left = rows.size();
         while (it.hasNext()) {
-            line = it.next();
-            byte[] b = line.getBytes(ENCODING);
+            byte[] b = it.next();
+            // pass timeout only to prevent infinite wait in case of a bug
             byte[] actual = buffer.getNextEntry(100L);
             Assert.assertArrayEquals(b, actual);
             totalPayload -= b.length;
@@ -65,7 +90,8 @@ public class TestLonger extends MembufTestBase
         // All done, should be empty...
         assertEquals(0, buffer.getEntryCount());
         assertEquals(0L, buffer.getTotalPayloadLength());
-        assertEquals(2, buffer.getSegmentCount());
+        // always have at least one segment
+        assertEquals(1, buffer.getSegmentCount());
     }
 
 }
