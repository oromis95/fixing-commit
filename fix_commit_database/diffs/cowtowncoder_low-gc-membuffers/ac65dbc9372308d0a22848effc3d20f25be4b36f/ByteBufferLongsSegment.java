@@ -58,7 +58,7 @@ public class ByteBufferLongsSegment extends LongsSegment
     }
 
     /**
-     * Factory method for construction {@link LongsSegmentAllocator} that
+     * Factory method for constructing an {@link Allocator} that
      * constructs instances of this segment type
      */
     public static Allocator allocator(int segmentSize, int minSegmentsToRetain, int maxSegments,
@@ -232,7 +232,8 @@ public class ByteBufferLongsSegment extends LongsSegment
             super(segmentSize, minSegmentsToRetain, maxSegments);
             _cfgAllocateNative = allocateNativeBuffers;
         }
-        
+
+        @Override
         protected LongsSegment _allocateSegment()
         {
             // can reuse a segment returned earlier?
