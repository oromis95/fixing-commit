@@ -138,7 +138,7 @@ public class ArrayLongsSegment extends LongsSegment
     @Override
     public int readLength()
     {
-        if (availableForReading() > 1) {
+        if (availableForReading() < 1) {
             return -1;
         }
         return (int) _buffer[_readPtr++];
