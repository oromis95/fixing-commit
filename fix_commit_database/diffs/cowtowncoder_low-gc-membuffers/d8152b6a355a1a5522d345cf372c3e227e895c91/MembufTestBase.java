@@ -1,5 +1,7 @@
 package com.fasterxml.util.membuf;
 
+import java.util.Arrays;
+
 import junit.framework.TestCase;
 
 public abstract class MembufTestBase extends TestCase
@@ -32,4 +34,17 @@ public abstract class MembufTestBase extends TestCase
             }
         }
     }
+
+    protected void verifyException(Throwable e, String... matches)
+    {
+        String msg = e.getMessage();
+        String lmsg = (msg == null) ? "" : msg.toLowerCase();
+        for (String match : matches) {
+            String lmatch = match.toLowerCase();
+            if (lmsg.indexOf(lmatch) >= 0) {
+                return;
+            }
+        }
+        fail("Expected an exception with one of substrings ("+Arrays.asList(matches)+"): got one with message \""+msg+"\"");
+    }
 }
\ No newline at end of file
