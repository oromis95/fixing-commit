@@ -21,7 +21,7 @@ public class ItemCleaner {
 
 	private static ConfigManager cm = ConfigManager.getInstance();
 	private int preMessageTime = 0;
-	//private int holoTime = 0;
+	private int holoTime = 0;
 
 	public ItemCleaner() {
 		NeverLag.getInstance().getServer().getScheduler().scheduleSyncRepeatingTask(NeverLag.getInstance(), new Runnable() {
@@ -33,13 +33,12 @@ public class ItemCleaner {
 			NeverLag.getInstance().getServer().getScheduler().scheduleSyncRepeatingTask(NeverLag.getInstance(), new Runnable() {
 				public void run() {
 					doPreMessage();
-					//holoDisplay();
+					holoDisplay();
 				}
 			}, 20L, 20L);
 		}
 	}
 	
-	/*
 	// 悬浮提醒
 	private void holoDisplay() {
 		if (!cm.isClearDropItem() || !cm.isClearItem() || !cm.isClearItemPreHoloMessage()) {
@@ -78,7 +77,6 @@ public class ItemCleaner {
 			}
 		}
 	}
-	*/
 	
 	// 提前通知
 	private void doPreMessage() {
