@@ -1,22 +1,22 @@
 package com.morcinek.android.codegenerator.codegeneration.builders;
 
-import com.morcinek.android.codegenerator.codegeneration.builders.file.LayoutBuilder;
+import com.morcinek.android.codegenerator.codegeneration.builders.file.ResourceNameBuilder;
 import org.fest.assertions.Assertions;
 import org.junit.Test;
 
-public class LayoutBuilderTest {
+public class ResourceNameBuilderTest {
 
-    private LayoutBuilder layoutBuilder;
+    private ResourceNameBuilder resourceNameBuilder;
 
     @Test
     public void builtStringTest() throws Exception {
         // given
-        layoutBuilder = new LayoutBuilder("main");
+        resourceNameBuilder = new ResourceNameBuilder("main");
 
         // when
-        String layout = layoutBuilder.builtString();
+        String layout = resourceNameBuilder.builtString();
 
         // then
-        Assertions.assertThat(layout).isNotNull().isEqualTo("R.layout.main");
+        Assertions.assertThat(layout).isNotNull().isEqualTo("main");
     }
 }
\ No newline at end of file
