@@ -25,6 +25,20 @@ public class InterfaceBuilderTest {
         Assertions.assertThat(value).isNotNull().isEqualTo("implements OnClickListener");
     }
 
+    @Test
+    public void builtNoStringTest() throws Exception {
+        // given
+        ResourceProvider resourceProvider = Mockito.mock(ResourceProvider.class);
+        when(resourceProvider.provideInterface()).thenReturn(null);
+        interfaceBuilder = new InterfaceBuilder(Lists.newArrayList(resourceProvider));
+
+        // when
+        String value = interfaceBuilder.builtString();
+
+        // then
+        Assertions.assertThat(value).isNotNull().isEqualTo("");
+    }
+
     @Test
     public void builtAdvancedStringTest() throws Exception {
         // given
