@@ -8,9 +8,9 @@ import java.util.Set;
 /**
  * Copyright 2014 Tomasz Morcinek. All rights reserved.
  */
-public class BNDefaultProvider extends AbstractResourceProvider {
+public class BDefaultProvider extends AbstractResourceProvider {
 
-    public BNDefaultProvider(Resource resource) {
+    public BDefaultProvider(Resource resource) {
         super(resource);
     }
 
@@ -26,7 +26,7 @@ public class BNDefaultProvider extends AbstractResourceProvider {
 
     @Override
     public Set<String> provideField() {
-        return Sets.newHashSet("BN");
+        return Sets.newHashSet("B");
     }
 
     @Override
