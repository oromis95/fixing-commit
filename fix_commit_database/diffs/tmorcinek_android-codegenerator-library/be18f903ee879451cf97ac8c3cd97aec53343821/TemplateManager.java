@@ -1,5 +1,7 @@
 package com.morcinek.android.codegenerator.writer.templates;
 
+import java.util.Map;
+
 /**
  * Copyright 2014 Tomasz Morcinek. All rights reserved.
  */
@@ -15,6 +17,12 @@ public class TemplateManager {
         template = template.replace(getKeyWrapper(key), value);
     }
 
+    public void addTemplateValues(Map<String, String> templateValueMap) {
+        for (String key : templateValueMap.keySet()) {
+            addTemplateValue(key, templateValueMap.get(key));
+        }
+    }
+
     public String getResult() {
         return template.replaceAll("\\$\\{\\w+\\}", "");
     }
