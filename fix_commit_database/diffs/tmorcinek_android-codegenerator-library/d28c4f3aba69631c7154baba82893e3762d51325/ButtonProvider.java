@@ -25,6 +25,11 @@ public class ButtonProvider extends AbstractResourceProvider {
         return Sets.newHashSet("Button");
     }
 
+    @Override
+    public Set<String> provideField() {
+        return null;
+    }
+
     @Override
     public Set<String> provideMethod() {
         return Sets.newHashSet("OnClickListener");
