@@ -3,6 +3,7 @@ package com.morcinek.android.codegenerator.codegeneration.providers.factories;
 import com.google.common.collect.Lists;
 import com.morcinek.android.codegenerator.codegeneration.providers.ResourceProvider;
 import com.morcinek.android.codegenerator.codegeneration.providers.ResourceProvidersFactory;
+import com.morcinek.android.codegenerator.codegeneration.providers.resources.AbstractResourceProvider;
 import com.morcinek.android.codegenerator.codegeneration.providers.resources.ButtonProvider;
 import com.morcinek.android.codegenerator.codegeneration.providers.resources.DefaultProvider;
 import com.morcinek.android.codegenerator.codegeneration.providers.resources.GetterProvider;
@@ -16,11 +17,16 @@ public class FragmentResourceProvidersFactory implements ResourceProvidersFactor
     @Override
     public ResourceProvider createResourceProvider(Resource resource) {
         if (isApplicable(resource, "Button", "ImageButton")) {
-            return new ButtonProvider(resource);
+            return getResourceProviderWithContainerPrefix(new ButtonProvider(resource), "view.");
         } else if (isApplicable(resource, "CheckBox", "EditText", "View")) {
-            return new GetterProvider(resource);
+            return getResourceProviderWithContainerPrefix(new GetterProvider(resource), "getView().");
         }
-        return new DefaultProvider(resource);
+        return getResourceProviderWithContainerPrefix(new DefaultProvider(resource), "view.");
+    }
+
+    private AbstractResourceProvider getResourceProviderWithContainerPrefix(AbstractResourceProvider resourceProvider, String value) {
+        resourceProvider.putExtra("CONTAINER_PREFIX", value);
+        return resourceProvider;
     }
 
     private boolean isApplicable(Resource resource, String... resourcesNames) {
