@@ -18,7 +18,7 @@ public class ResourceTemplatesProviderTest {
 
     @Test
     public void provideAssignmentTemplateTest() throws Exception {
-        String template = templatesProvider.provideTemplateForName("Assignment_template");
+        String template = templatesProvider.provideTemplateForName("Assignment__template");
         Assertions.assertThat(template).isNotNull().isNotEmpty().isEqualTo(
                 "        ${RESOURCE_NAME} = (${RESOURCE_TYPE}) findViewById(${RESOURCE_ID});\n" +
                         "        ${RESOURCE_ASSIGNMENT}");
