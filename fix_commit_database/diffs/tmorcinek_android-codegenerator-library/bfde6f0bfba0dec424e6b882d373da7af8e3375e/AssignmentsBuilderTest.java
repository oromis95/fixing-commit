@@ -2,6 +2,7 @@ package com.morcinek.android.codegenerator.writer.builders.resources;
 
 import com.google.common.collect.Lists;
 import com.google.common.collect.Maps;
+import com.google.common.collect.Sets;
 import com.morcinek.android.codegenerator.writer.providers.generic.ResourceProvider;
 import com.morcinek.android.codegenerator.writer.templates.ResourceTemplatesProvider;
 import org.apache.commons.lang3.StringUtils;
@@ -37,44 +38,52 @@ public class AssignmentsBuilderTest {
     }
 
     @Test
-    public void builtOneFieldString() throws Exception {
+    public void builtButtonFieldString() throws Exception {
         // given
-        interfaceBuilder = provideAssignmentBuilder(Lists.newArrayList(getMockResourceProvider("button")));
+        interfaceBuilder = provideAssignmentBuilder(Lists.newArrayList(getMockResourceProvider("button", "Button")));
 
         // when
         String value = interfaceBuilder.builtString();
 
         // then
-        Assertions.assertThat(value).isNotNull().isEqualTo(
-                "        button = (Button) findViewById(R.id.button);\n" +
-                        "        button.setOnClickListener(this);\n");
+        Assertions.assertThat(value).isNotNull().isEqualTo("        findViewById(R.id.button).setOnClickListener(this);\n");
+    }
+
+    @Test
+    public void builtViewFieldString() throws Exception {
+        // given
+        interfaceBuilder = provideAssignmentBuilder(Lists.newArrayList(getMockResourceProvider("view", "")));
+
+        // when
+        String value = interfaceBuilder.builtString();
+
+        // then
+        Assertions.assertThat(value).isNotNull().isEqualTo("        view = (View) findViewById(R.id.view);\n");
     }
 
     @Test
     public void builtTwoFieldString() throws Exception {
         // given
-        interfaceBuilder = provideAssignmentBuilder(Lists.newArrayList(getMockResourceProvider("button"), getMockResourceProvider("view")));
+        interfaceBuilder = provideAssignmentBuilder(Lists.newArrayList(getMockResourceProvider("button", "Button"), getMockResourceProvider("view", "")));
 
         // when
         String value = interfaceBuilder.builtString();
 
         // then
         Assertions.assertThat(value).isNotNull().isEqualTo(
-                "        button = (Button) findViewById(R.id.button);\n" +
-                        "        button.setOnClickListener(this);\n" +
-                        "        view = (View) findViewById(R.id.view);\n" +
-                        "        view.setOnClickListener(this);\n"
+                "        findViewById(R.id.button).setOnClickListener(this);\n" +
+                        "        view = (View) findViewById(R.id.view);\n"
         );
     }
 
-    private ResourceProvider getMockResourceProvider(String name) {
+    private ResourceProvider getMockResourceProvider(String name, String assignment) {
         ResourceProvider resourceProvider = Mockito.mock(ResourceProvider.class);
         Map<String, String> treeMap = Maps.newHashMap();
         treeMap.put("RESOURCE_NAME", name);
         treeMap.put("RESOURCE_TYPE", StringUtils.capitalize(name));
         treeMap.put("RESOURCE_ID", "R.id." + name);
-        treeMap.put("RESOURCE_ASSIGNMENT", name + ".setOnClickListener(this);\n");
         when(resourceProvider.provideValues()).thenReturn(treeMap);
+        when(resourceProvider.provideAssignment()).thenReturn(Sets.newHashSet(assignment));
         return resourceProvider;
     }
 }
\ No newline at end of file
