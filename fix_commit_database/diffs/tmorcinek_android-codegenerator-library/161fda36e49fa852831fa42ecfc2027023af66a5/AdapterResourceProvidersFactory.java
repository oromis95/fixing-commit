@@ -1,6 +1,7 @@
 package com.morcinek.android.codegenerator.codegeneration.providers.factories;
 
 import com.morcinek.android.codegenerator.codegeneration.providers.generic.ResourceProvider;
+import com.morcinek.android.codegenerator.codegeneration.providers.resources.AdapterProvider;
 import com.morcinek.android.codegenerator.codegeneration.providers.resources.DefaultProvider;
 import com.morcinek.android.codegenerator.extractor.model.Resource;
 
@@ -11,6 +12,6 @@ public class AdapterResourceProvidersFactory implements ResourceProvidersFactory
 
     @Override
     public ResourceProvider createResourceProvider(Resource resource) {
-        return new DefaultProvider(resource);
+        return new AdapterProvider(resource);
     }
 }
