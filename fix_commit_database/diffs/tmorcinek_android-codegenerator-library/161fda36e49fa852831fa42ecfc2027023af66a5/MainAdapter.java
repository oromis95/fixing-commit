@@ -36,7 +36,7 @@ public class MainAdapter extends BaseAdapter {
         if (convertView == null) {
             convertView = layoutInflater.inflate(R.layout.main, null);
             ViewHolder viewHolder = new ViewHolder();
-        button = (Button) findViewById(R.id.button);
+            viewHolder.button = (Button) convertView.findViewById(R.id.button);
 
             convertView.setTag(viewHolder);
         }
