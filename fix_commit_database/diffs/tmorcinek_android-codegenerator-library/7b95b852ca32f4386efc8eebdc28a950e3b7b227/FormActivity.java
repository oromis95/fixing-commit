@@ -1,11 +1,11 @@
 import android.os.Bundle;
 import android.app.Activity;
 import android.widget.ImageButton;
+import android.widget.Button;
 import android.widget.EditText;
 import android.widget.TextView;
 import android.widget.List;
 
-
 public class FormActivity extends Activity implements OnClickListener {
 
     private TextView headerText;
@@ -16,15 +16,18 @@ public class FormActivity extends Activity implements OnClickListener {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.form);
 
+        findViewById(R.id.imageButton).setOnClickListener(this);
         findViewById(R.id.button).setOnClickListener(this);
         headerText = (TextView) findViewById(R.id.header_text);
         list = (List) findViewById(android.R.id.list);
-
     }
 
     @Override
     public void onClick(View view) {
         switch (view.getId()) {
+            case R.id.imageButton:
+                //TODO implement
+                break;
             case R.id.button:
                 //TODO implement
                 break;
@@ -39,6 +42,4 @@ public class FormActivity extends Activity implements OnClickListener {
     private EditText getEditTextCity(){
         return (EditText) findViewById(R.id.edit_text_city);
     }
-
-
 }
