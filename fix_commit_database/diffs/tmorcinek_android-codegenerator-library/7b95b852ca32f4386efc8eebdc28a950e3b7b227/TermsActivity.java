@@ -3,7 +3,6 @@ import android.app.Activity;
 import android.widget.Button;
 import android.widget.CheckBox;
 
-
 public class TermsActivity extends Activity implements OnClickListener {
 
 
@@ -13,7 +12,6 @@ public class TermsActivity extends Activity implements OnClickListener {
         setContentView(R.layout.terms);
 
         findViewById(R.id.button).setOnClickListener(this);
-
     }
 
     @Override
@@ -29,6 +27,4 @@ public class TermsActivity extends Activity implements OnClickListener {
     private CheckBox getAcceptsTerms(){
         return (CheckBox) findViewById(R.id.accepts_terms);
     }
-
-
 }
