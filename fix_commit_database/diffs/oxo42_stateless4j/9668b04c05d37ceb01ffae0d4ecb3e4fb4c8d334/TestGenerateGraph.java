@@ -6,6 +6,7 @@ import com.github.oxo42.stateless4j.Trigger;
 import com.github.oxo42.stateless4j.helpers.InputStreamHelper;
 
 import java.io.ByteArrayOutputStream;
+import java.io.IOException;
 import java.io.InputStream;
 import java.io.UnsupportedEncodingException;
 
@@ -16,7 +17,7 @@ public class TestGenerateGraph {
     // This isn't going to work because the StateMachine uses a HashMap which does not maintain a consistent output
     // Changing it to LinkedHashMap will make this test work all the time but will incur a runtime performance penalty
     // @Test
-    public void testGenerateSimpleGraph() throws UnsupportedEncodingException {
+    public void testGenerateSimpleGraph() throws UnsupportedEncodingException, IOException {
         StateMachine<State, Trigger> sm = new StateMachine<State, Trigger>(State.A);
         sm.configure(State.A)
                 .permit(Trigger.X, State.B)
