@@ -1,6 +1,5 @@
 package com.github.oxo42.stateless4j;
 
-
 import org.junit.Test;
 
 import com.github.oxo42.stateless4j.triggers.TriggerWithParameters1;
@@ -8,8 +7,8 @@ import com.github.oxo42.stateless4j.triggers.TriggerWithParameters2;
 
 import static org.junit.Assert.*;
 
-
 public class TriggerWithParametersTests {
+
     @Test
     public void DescribesUnderlyingTrigger() {
         TriggerWithParameters1<String, State, Trigger> twp = new TriggerWithParameters1<String, State, Trigger>(Trigger.X, String.class);
@@ -28,33 +27,21 @@ public class TriggerWithParametersTests {
         twp.validateParameters(new Object[]{"arg"});
     }
 
-    @Test
+    @Test(expected = IllegalStateException.class)
     public void IncompatibleParametersAreNotValid() {
-        try {
-            TriggerWithParameters1<String, State, Trigger> twp = new TriggerWithParameters1<String, State, Trigger>(Trigger.X, String.class);
-            twp.validateParameters(new Object[]{123});
-            fail();
-        } catch (Exception e) {
-        }
+        TriggerWithParameters1<String, State, Trigger> twp = new TriggerWithParameters1<String, State, Trigger>(Trigger.X, String.class);
+        twp.validateParameters(new Object[]{123});
     }
 
-    @Test
+    @Test(expected = IllegalStateException.class)
     public void TooFewParametersDetected() {
-        try {
-            TriggerWithParameters2<String, String, State, Trigger> twp = new TriggerWithParameters2<String, String, State, Trigger>(Trigger.X, String.class, String.class);
-            twp.validateParameters(new Object[]{"a"});
-            fail();
-        } catch (Exception e) {
-        }
+        TriggerWithParameters2<String, String, State, Trigger> twp = new TriggerWithParameters2<String, String, State, Trigger>(Trigger.X, String.class, String.class);
+        twp.validateParameters(new Object[]{"a"});
     }
 
-    @Test
+    @Test(expected = IllegalStateException.class)
     public void TooManyParametersDetected() {
-        try {
-            TriggerWithParameters2<String, String, State, Trigger> twp = new TriggerWithParameters2<String, String, State, Trigger>(Trigger.X, String.class, String.class);
-            twp.validateParameters(new Object[]{"a", "b", "c"});
-            fail();
-        } catch (Exception e) {
-        }
+        TriggerWithParameters2<String, String, State, Trigger> twp = new TriggerWithParameters2<String, String, State, Trigger>(Trigger.X, String.class, String.class);
+        twp.validateParameters(new Object[]{"a", "b", "c"});
     }
 }
