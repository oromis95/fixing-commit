@@ -1,22 +1,22 @@
 package com.googlecode.stateless4j.triggers;
+
 import com.googlecode.stateless4j.delegates.Func;
 
 
-public abstract class TriggerBehaviour<TState, TTrigger>
-{
+public abstract class TriggerBehaviour<TState, TTrigger> {
     final TTrigger _trigger;
     final Func<Boolean> _guard;
 
-    protected TriggerBehaviour(TTrigger trigger, Func<Boolean> guard)
-    {
+    protected TriggerBehaviour(TTrigger trigger, Func<Boolean> guard) {
         _trigger = trigger;
         _guard = guard;
     }
 
-    public TTrigger getTrigger() { return _trigger; } 
+    public TTrigger getTrigger() {
+        return _trigger;
+    }
 
-    public Boolean isGuardConditionMet()
-    {
+    public Boolean isGuardConditionMet() throws Exception {
         return _guard.call();
     }
 
