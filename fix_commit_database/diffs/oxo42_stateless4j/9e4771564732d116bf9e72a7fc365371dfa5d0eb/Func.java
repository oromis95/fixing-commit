@@ -1,5 +1,15 @@
 package com.googlecode.stateless4j.delegates;
 
-public interface Func<T> {
-    public T call();
+/**
+ * Represents a function that accepts no input and produces a result
+ *
+ * @param <R> Result type
+ */
+public interface Func<R> {
+    /**
+     * Applies this function
+     *
+     * @return Result
+     */
+    R call() throws Exception;
 }
