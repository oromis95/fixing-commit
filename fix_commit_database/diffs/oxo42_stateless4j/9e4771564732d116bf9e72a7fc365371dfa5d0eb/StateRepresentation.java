@@ -1,25 +1,23 @@
 package com.googlecode.stateless4j;
 
-import java.util.ArrayList;
-import java.util.HashMap;
-import java.util.List;
-import java.util.Map;
-
-import org.apache.commons.collections.ListUtils;
-
 import com.googlecode.stateless4j.delegates.Action1;
 import com.googlecode.stateless4j.delegates.Action2;
 import com.googlecode.stateless4j.resources.StateRepresentationResources;
 import com.googlecode.stateless4j.transitions.Transition;
 import com.googlecode.stateless4j.triggers.TriggerBehaviour;
 import com.googlecode.stateless4j.validation.Enforce;
+import org.apache.commons.collections.ListUtils;
+
+import java.util.ArrayList;
+import java.util.HashMap;
+import java.util.List;
+import java.util.Map;
 
-public class StateRepresentation<TState, TTrigger>
-{
+public class StateRepresentation<TState, TTrigger> {
     final TState _state;
 
     final Map<TTrigger, List<TriggerBehaviour<TState, TTrigger>>> _triggerBehaviours =
-        new HashMap<TTrigger, List<TriggerBehaviour<TState, TTrigger>>>();
+            new HashMap<TTrigger, List<TriggerBehaviour<TState, TTrigger>>>();
 
     final List<Action2<Transition<TState, TTrigger>, Object[]>> _entryActions = new ArrayList<Action2<Transition<TState, TTrigger>, Object[]>>();
     final List<Action1<Transition<TState, TTrigger>>> _exitActions = new ArrayList<Action1<Transition<TState, TTrigger>>>();
@@ -28,39 +26,35 @@ public class StateRepresentation<TState, TTrigger>
 
     final List<StateRepresentation<TState, TTrigger>> _substates = new ArrayList<StateRepresentation<TState, TTrigger>>();
 
-    public StateRepresentation(TState state)
-    {
+    public StateRepresentation(TState state) {
         _state = state;
     }
 
-    public Boolean CanHandle(TTrigger trigger)
-    {
+    public Boolean CanHandle(TTrigger trigger) {
         try {
             TryFindHandler(trigger);
             return true;
-        } catch(Exception e) {
+        } catch (Exception e) {
         }
         return false;
     }
 
-    public TriggerBehaviour<TState, TTrigger> TryFindHandler(TTrigger trigger)
-    {
+    public TriggerBehaviour<TState, TTrigger> TryFindHandler(TTrigger trigger) {
         try {
             return TryFindLocalHandler(trigger);
         } catch (Exception e) {
             return getSuperstate().TryFindHandler(trigger);
         }
     }
-    
-    TriggerBehaviour<TState, TTrigger> TryFindLocalHandler(TTrigger trigger/*, out TriggerBehaviour handler*/) throws Exception
-    {
+
+    TriggerBehaviour<TState, TTrigger> TryFindLocalHandler(TTrigger trigger/*, out TriggerBehaviour handler*/) throws Exception {
         List<TriggerBehaviour<TState, TTrigger>> possible;
         if (!_triggerBehaviours.containsKey(trigger)) {
             throw new Exception();
         }
         possible = _triggerBehaviours.get(trigger);
-        
-        List<TriggerBehaviour<TState, TTrigger>> actual = new ArrayList<TriggerBehaviour<TState,TTrigger>>();
+
+        List<TriggerBehaviour<TState, TTrigger>> actual = new ArrayList<TriggerBehaviour<TState, TTrigger>>();
         for (TriggerBehaviour<TState, TTrigger> triggerBehaviour : possible) {
             if (triggerBehaviour.isGuardConditionMet()) {
                 actual.add(triggerBehaviour);
@@ -69,49 +63,43 @@ public class StateRepresentation<TState, TTrigger>
 
         if (actual.size() > 1)
             throw new Exception(
-                String.format(StateRepresentationResources.MultipleTransitionsPermitted,
-                trigger, _state));
+                    String.format(StateRepresentationResources.MultipleTransitionsPermitted,
+                            trigger, _state)
+            );
 
         TriggerBehaviour<TState, TTrigger> handler = actual.get(0);
         if (handler == null) {
-        	throw new Exception();
+            throw new Exception();
         }
-		return handler;
+        return handler;
     }
 
-    public void AddEntryAction(final TTrigger trigger, final Action2<Transition<TState, TTrigger>, Object[]> action) throws Exception
-    {
+    public void AddEntryAction(final TTrigger trigger, final Action2<Transition<TState, TTrigger>, Object[]> action) throws Exception {
         Enforce.ArgumentNotNull(action, "action");
-        
-        
-        _entryActions.add(new Action2<Transition<TState,TTrigger>, Object[]>() {
-            public void doIt(Transition<TState,TTrigger> t, Object[] args) throws Exception {
+
+
+        _entryActions.add(new Action2<Transition<TState, TTrigger>, Object[]>() {
+            public void doIt(Transition<TState, TTrigger> t, Object[] args) throws Exception {
                 if (t.getTrigger().equals(trigger))
                     action.doIt(t, args);
             }
         });
     }
 
-    public void AddEntryAction(Action2<Transition<TState, TTrigger>, Object[]> action) throws Exception
-    {
+    public void AddEntryAction(Action2<Transition<TState, TTrigger>, Object[]> action) throws Exception {
         _entryActions.add(Enforce.ArgumentNotNull(action, "action"));
     }
 
-    public void AddExitAction(Action1<Transition<TState, TTrigger>> action) throws Exception
-    {
+    public void AddExitAction(Action1<Transition<TState, TTrigger>> action) throws Exception {
         _exitActions.add(Enforce.ArgumentNotNull(action, "action"));
     }
 
-    public void Enter(Transition<TState, TTrigger> transition, Object... entryArgs) throws Exception
-    {
+    public void Enter(Transition<TState, TTrigger> transition, Object... entryArgs) throws Exception {
         Enforce.ArgumentNotNull(transition, "transtion");
 
-        if (transition.isReentry())
-        {
+        if (transition.isReentry()) {
             ExecuteEntryActions(transition, entryArgs);
-        }
-        else if (!Includes(transition.getSource()))
-        {
+        } else if (!Includes(transition.getSource())) {
             if (_superstate != null)
                 _superstate.Enter(transition, entryArgs);
 
@@ -119,42 +107,34 @@ public class StateRepresentation<TState, TTrigger>
         }
     }
 
-    public void Exit(Transition<TState, TTrigger> transition) throws Exception
-    {
+    public void Exit(Transition<TState, TTrigger> transition) throws Exception {
         Enforce.ArgumentNotNull(transition, "transtion");
 
-        if (transition.isReentry())
-        {
+        if (transition.isReentry()) {
             ExecuteExitActions(transition);
-        }
-        else if (!Includes(transition.getDestination()))
-        {
+        } else if (!Includes(transition.getDestination())) {
             ExecuteExitActions(transition);
             if (_superstate != null)
                 _superstate.Exit(transition);
         }
     }
 
-    void ExecuteEntryActions(Transition<TState, TTrigger> transition, Object[] entryArgs) throws Exception
-    {
+    void ExecuteEntryActions(Transition<TState, TTrigger> transition, Object[] entryArgs) throws Exception {
         Enforce.ArgumentNotNull(transition, "transtion");
         Enforce.ArgumentNotNull(entryArgs, "entryArgs");
         for (Action2<Transition<TState, TTrigger>, Object[]> action : _entryActions)
             action.doIt(transition, entryArgs);
     }
 
-    void ExecuteExitActions(Transition<TState, TTrigger> transition) throws Exception
-    {
+    void ExecuteExitActions(Transition<TState, TTrigger> transition) throws Exception {
         Enforce.ArgumentNotNull(transition, "transtion");
         for (Action1<Transition<TState, TTrigger>> action : _exitActions)
             action.doIt(transition);
     }
 
-    public void AddTriggerBehaviour(TriggerBehaviour<TState, TTrigger> triggerBehaviour)
-    {
+    public void AddTriggerBehaviour(TriggerBehaviour<TState, TTrigger> triggerBehaviour) {
         List<TriggerBehaviour<TState, TTrigger>> allowed;
-        if (!_triggerBehaviours.containsKey(triggerBehaviour.getTrigger()))
-        {
+        if (!_triggerBehaviours.containsKey(triggerBehaviour.getTrigger())) {
             allowed = new ArrayList<TriggerBehaviour<TState, TTrigger>>();
             _triggerBehaviours.put(triggerBehaviour.getTrigger(), allowed);
         }
@@ -162,29 +142,24 @@ public class StateRepresentation<TState, TTrigger>
         allowed.add(triggerBehaviour);
     }
 
-    public StateRepresentation<TState, TTrigger> getSuperstate()
-    {
+    public StateRepresentation<TState, TTrigger> getSuperstate() {
         return _superstate;
     }
-    
-    public void setSuperstate(StateRepresentation<TState, TTrigger> value)
-    {
+
+    public void setSuperstate(StateRepresentation<TState, TTrigger> value) {
         _superstate = value;
     }
 
-    public TState getUnderlyingState()
-    {
+    public TState getUnderlyingState() {
         return _state;
     }
 
-    public void AddSubstate(StateRepresentation<TState, TTrigger> substate) throws Exception
-    {
+    public void AddSubstate(StateRepresentation<TState, TTrigger> substate) throws Exception {
         Enforce.ArgumentNotNull(substate, "substate");
         _substates.add(substate);
     }
 
-    public Boolean Includes(TState state)
-    {
+    public Boolean Includes(TState state) {
         Boolean isIncluded = false;
         for (StateRepresentation<TState, TTrigger> s : _substates) {
             if (s.Includes(state)) {
@@ -194,18 +169,16 @@ public class StateRepresentation<TState, TTrigger>
         return _state.equals(state) || isIncluded;
     }
 
-    public Boolean IsIncludedIn(TState state)
-    {
+    public Boolean IsIncludedIn(TState state) {
         return
-            _state.equals(state) ||
-            (_superstate != null && _superstate.IsIncludedIn(state));
+                _state.equals(state) ||
+                        (_superstate != null && _superstate.IsIncludedIn(state));
     }
 
     @SuppressWarnings("unchecked")
-    public List<TTrigger> getPermittedTriggers()
-    {
+    public List<TTrigger> getPermittedTriggers() throws Exception {
         List<TTrigger> result = new ArrayList<TTrigger>();
-        
+
         for (TTrigger t : _triggerBehaviours.keySet()) {
             Boolean isOk = false;
             for (TriggerBehaviour<TState, TTrigger> v : _triggerBehaviours.get(t)) {
@@ -217,7 +190,7 @@ public class StateRepresentation<TState, TTrigger>
                 result.add(t);
             }
         }
-        
+
         if (getSuperstate() != null)
             result = ListUtils.sum(result, getSuperstate().getPermittedTriggers());
 
