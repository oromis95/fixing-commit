@@ -1,4 +1,11 @@
 package com.googlecode.stateless4j.delegates;
+
+/**
+ * Represents an operation that accepts no input arguments and returns no result.
+ */
 public interface Action {
-    public void doIt();
+    /**
+     * Performs this operation
+     */
+    void doIt() throws Exception;
 }
\ No newline at end of file
