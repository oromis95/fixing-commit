@@ -1,5 +1,15 @@
 package com.googlecode.stateless4j.delegates;
 
+/**
+ * Represents an operation that accepts an input and returns no result
+ *
+ * @param <T> The type of the input to the operation
+ */
 public interface Action1<T> {
-    public void doIt(T arg1);
+    /**
+     * Performs this operation on the given input
+     *
+     * @param arg1 Input argument
+     */
+    void doIt(T arg1) throws Exception;
 }
