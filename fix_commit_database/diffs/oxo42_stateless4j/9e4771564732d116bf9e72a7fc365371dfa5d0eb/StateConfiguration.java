@@ -19,20 +19,18 @@ import com.googlecode.stateless4j.triggers.TriggerWithParameters2;
 import com.googlecode.stateless4j.triggers.TriggerWithParameters3;
 import com.googlecode.stateless4j.validation.Enforce;
 
-public class StateConfiguration<TState, TTrigger>
-{
+public class StateConfiguration<TState, TTrigger> {
     final StateRepresentation<TState, TTrigger> _representation;
     final Func2<TState, StateRepresentation<TState, TTrigger>> _lookup;
     final Func<Boolean> NoGuard = new Func<Boolean>() {
-        
-        
+
+
         public Boolean call() {
             return true;
         }
     };
 
-    public StateConfiguration(StateRepresentation<TState, TTrigger> representation, Func2<TState, StateRepresentation<TState, TTrigger>> lookup) throws Exception
-    {
+    public StateConfiguration(StateRepresentation<TState, TTrigger> representation, Func2<TState, StateRepresentation<TState, TTrigger>> lookup) throws Exception {
         _representation = Enforce.ArgumentNotNull(representation, "representation");
         _lookup = Enforce.ArgumentNotNull(lookup, "lookup");
     }
@@ -44,8 +42,7 @@ public class StateConfiguration<TState, TTrigger>
     /// <param name="destinationState">The state that the trigger will cause a
     /// transition to.</param>
     /// <returns>The reciever.</returns>
-    public StateConfiguration<TState, TTrigger> Permit(TTrigger trigger, TState destinationState) throws Exception
-    {
+    public StateConfiguration<TState, TTrigger> Permit(TTrigger trigger, TState destinationState) throws Exception {
         enforceNotIdentityTransition(destinationState);
         return publicPermit(trigger, destinationState);
     }
@@ -59,8 +56,7 @@ public class StateConfiguration<TState, TTrigger>
     /// <param name="guard">Function that must return true in order for the
     /// trigger to be accepted.</param>
     /// <returns>The reciever.</returns>
-    public StateConfiguration<TState, TTrigger> PermitIf(TTrigger trigger, TState destinationState, Func<Boolean> guard) throws Exception
-    {
+    public StateConfiguration<TState, TTrigger> PermitIf(TTrigger trigger, TState destinationState, Func<Boolean> guard) throws Exception {
         enforceNotIdentityTransition(destinationState);
         return publicPermitIf(trigger, destinationState, guard);
     }
@@ -75,8 +71,7 @@ public class StateConfiguration<TState, TTrigger>
     /// Applies to the current state only. Will not re-execute superstate actions, or
     /// cause actions to execute transitioning between super- and sub-states.
     /// </remarks>
-    public StateConfiguration<TState, TTrigger> PermitReentry(TTrigger trigger) throws Exception
-    {
+    public StateConfiguration<TState, TTrigger> PermitReentry(TTrigger trigger) throws Exception {
         return publicPermit(trigger, _representation.getUnderlyingState());
     }
 
@@ -92,17 +87,16 @@ public class StateConfiguration<TState, TTrigger>
     /// Applies to the current state only. Will not re-execute superstate actions, or
     /// cause actions to execute transitioning between super- and sub-states.
     /// </remarks>
-    public StateConfiguration<TState, TTrigger> PermitReentryIf(TTrigger trigger, Func<Boolean> guard) throws Exception
-    {
+    public StateConfiguration<TState, TTrigger> PermitReentryIf(TTrigger trigger, Func<Boolean> guard) throws Exception {
         return publicPermitIf(trigger, _representation.getUnderlyingState(), guard);
     }
+
     /// <summary>
     /// Ignore the specified trigger when in the configured state.
     /// </summary>
     /// <param name="trigger">The trigger to ignore.</param>
     /// <returns>The receiver.</returns>
-    public StateConfiguration<TState, TTrigger> Ignore(TTrigger trigger) throws Exception
-    {
+    public StateConfiguration<TState, TTrigger> Ignore(TTrigger trigger) throws Exception {
         return IgnoreIf(trigger, NoGuard);
     }
 
@@ -114,8 +108,7 @@ public class StateConfiguration<TState, TTrigger>
     /// <param name="guard">Function that must return true in order for the
     /// trigger to be ignored.</param>
     /// <returns>The receiver.</returns>
-    public StateConfiguration<TState, TTrigger> IgnoreIf(TTrigger trigger, Func<Boolean> guard) throws Exception
-    {
+    public StateConfiguration<TState, TTrigger> IgnoreIf(TTrigger trigger, Func<Boolean> guard) throws Exception {
         Enforce.ArgumentNotNull(guard, "guard");
         _representation.AddTriggerBehaviour(new IgnoredTriggerBehaviour<TState, TTrigger>(trigger, guard));
         return this;
@@ -127,13 +120,12 @@ public class StateConfiguration<TState, TTrigger>
     /// </summary>
     /// <param name="entryAction">Action to execute.</param>
     /// <returns>The receiver.</returns>
-    public StateConfiguration<TState, TTrigger> OnEntry(final Action entryAction) throws Exception
-    {
+    public StateConfiguration<TState, TTrigger> OnEntry(final Action entryAction) throws Exception {
         Enforce.ArgumentNotNull(entryAction, "entryAction");
-        return OnEntry( new Action1<Transition<TState,TTrigger>>() {
-            public void doIt(Transition<TState,TTrigger> t)  {
+        return OnEntry(new Action1<Transition<TState, TTrigger>>() {
+            public void doIt(Transition<TState, TTrigger> t) throws Exception {
                 entryAction.doIt();
-            };
+            }
         });
     }
 
@@ -143,16 +135,16 @@ public class StateConfiguration<TState, TTrigger>
     /// </summary>
     /// <param name="entryAction">Action to execute, providing details of the transition.</param>
     /// <returns>The receiver.</returns>
-    public StateConfiguration<TState, TTrigger> OnEntry(final Action1<Transition<TState, TTrigger>> entryAction) throws Exception
-    {
+    public StateConfiguration<TState, TTrigger> OnEntry(final Action1<Transition<TState, TTrigger>> entryAction) throws Exception {
         Enforce.ArgumentNotNull(entryAction, "entryAction");
         _representation.AddEntryAction(new Action2<Transition<TState, TTrigger>, Object[]>() {
-            public void doIt(Transition<TState, TTrigger> arg1, Object[] arg2) {
+            public void doIt(Transition<TState, TTrigger> arg1, Object[] arg2) throws Exception {
                 entryAction.doIt(arg1);
-            };
-        }); 
+            }
+        });
         return this;
     }
+
     /// <summary>
     /// Specify an action that will execute when transitioning into
     /// the configured state.
@@ -160,13 +152,12 @@ public class StateConfiguration<TState, TTrigger>
     /// <param name="entryAction">Action to execute.</param>
     /// <param name="trigger">The trigger by which the state must be entered in order for the action to execute.</param>
     /// <returns>The receiver.</returns>
-    public StateConfiguration<TState, TTrigger> OnEntryFrom(TTrigger trigger, final Action entryAction) throws Exception
-    {
+    public StateConfiguration<TState, TTrigger> OnEntryFrom(TTrigger trigger, final Action entryAction) throws Exception {
         Enforce.ArgumentNotNull(entryAction, "entryAction");
-        return OnEntryFrom(trigger, new Action1<Transition<TState,TTrigger>>() {
-                public void doIt(Transition<TState,TTrigger> arg1) {
-                    entryAction.doIt();
-                };
+        return OnEntryFrom(trigger, new Action1<Transition<TState, TTrigger>>() {
+            public void doIt(Transition<TState, TTrigger> arg1) throws Exception {
+                entryAction.doIt();
+            }
         });
     }
 
@@ -177,13 +168,12 @@ public class StateConfiguration<TState, TTrigger>
     /// <param name="entryAction">Action to execute, providing details of the transition.</param>
     /// <param name="trigger">The trigger by which the state must be entered in order for the action to execute.</param>
     /// <returns>The receiver.</returns>
-    public StateConfiguration<TState, TTrigger> OnEntryFrom(TTrigger trigger, final Action1<Transition<TState, TTrigger>> entryAction) throws Exception
-    {
+    public StateConfiguration<TState, TTrigger> OnEntryFrom(TTrigger trigger, final Action1<Transition<TState, TTrigger>> entryAction) throws Exception {
         Enforce.ArgumentNotNull(entryAction, "entryAction");
         _representation.AddEntryAction(trigger, new Action2<Transition<TState, TTrigger>, Object[]>() {
-                public void doIt(Transition<TState, TTrigger> arg1, Object[] arg2) {
-                    entryAction.doIt(arg1);
-                };
+            public void doIt(Transition<TState, TTrigger> arg1, Object[] arg2) throws Exception {
+                entryAction.doIt(arg1);
+            }
         });
         return this;
     }
@@ -196,13 +186,12 @@ public class StateConfiguration<TState, TTrigger>
     /// <param name="entryAction">Action to execute, providing details of the transition.</param>
     /// <param name="trigger">The trigger by which the state must be entered in order for the action to execute.</param>
     /// <returns>The receiver.</returns>
-    public <TArg0> StateConfiguration<TState, TTrigger> OnEntryFrom(TriggerWithParameters1<TArg0, TState, TTrigger> trigger, final Action1<TArg0> entryAction, final Class<TArg0> classe0) throws Exception
-    {
+    public <TArg0> StateConfiguration<TState, TTrigger> OnEntryFrom(TriggerWithParameters1<TArg0, TState, TTrigger> trigger, final Action1<TArg0> entryAction, final Class<TArg0> classe0) throws Exception {
         Enforce.ArgumentNotNull(entryAction, "entryAction");
         return OnEntryFrom(trigger, new Action2<TArg0, Transition<TState, TTrigger>>() {
-            public void doIt(TArg0 arg1, Transition<TState,TTrigger> arg2) {
+            public void doIt(TArg0 arg1, Transition<TState, TTrigger> arg2) throws Exception {
                 entryAction.doIt(arg1);
-            };
+            }
         }, classe0);
     }
 
@@ -214,15 +203,14 @@ public class StateConfiguration<TState, TTrigger>
     /// <param name="entryAction">Action to execute, providing details of the transition.</param>
     /// <param name="trigger">The trigger by which the state must be entered in order for the action to execute.</param>
     /// <returns>The receiver.</returns>
-    public <TArg0> StateConfiguration<TState, TTrigger> OnEntryFrom(TriggerWithParameters1<TArg0, TState, TTrigger> trigger, final Action2<TArg0, Transition<TState, TTrigger>> entryAction, final Class<TArg0> classe0) throws Exception
-    {
+    public <TArg0> StateConfiguration<TState, TTrigger> OnEntryFrom(TriggerWithParameters1<TArg0, TState, TTrigger> trigger, final Action2<TArg0, Transition<TState, TTrigger>> entryAction, final Class<TArg0> classe0) throws Exception {
         Enforce.ArgumentNotNull(entryAction, "entryAction");
         Enforce.ArgumentNotNull(trigger, "trigger");
         _representation.AddEntryAction(trigger.getTrigger(), new Action2<Transition<TState, TTrigger>, Object[]>() {
             @SuppressWarnings("unchecked")
-			public void doIt(Transition<TState, TTrigger> t, Object[] arg2) throws Exception {
-                entryAction.doIt((TArg0)arg2[0], t);
-            };
+            public void doIt(Transition<TState, TTrigger> t, Object[] arg2) throws Exception {
+                entryAction.doIt((TArg0) arg2[0], t);
+            }
         });
         return this;
     }
@@ -236,13 +224,12 @@ public class StateConfiguration<TState, TTrigger>
     /// <param name="entryAction">Action to execute, providing details of the transition.</param>
     /// <param name="trigger">The trigger by which the state must be entered in order for the action to execute.</param>
     /// <returns>The receiver.</returns>
-    public <TArg0, TArg1> StateConfiguration<TState, TTrigger> OnEntryFrom(TriggerWithParameters2<TArg0, TArg1, TState, TTrigger> trigger, final Action2<TArg0, TArg1> entryAction, final Class<TArg0> classe0, final Class<TArg1> classe1) throws Exception
-    {
+    public <TArg0, TArg1> StateConfiguration<TState, TTrigger> OnEntryFrom(TriggerWithParameters2<TArg0, TArg1, TState, TTrigger> trigger, final Action2<TArg0, TArg1> entryAction, final Class<TArg0> classe0, final Class<TArg1> classe1) throws Exception {
         Enforce.ArgumentNotNull(entryAction, "entryAction");
         return OnEntryFrom(trigger, new Action3<TArg0, TArg1, Transition<TState, TTrigger>>() {
             public void doIt(TArg0 a0, TArg1 a1, Transition<TState, TTrigger> t) throws Exception {
                 entryAction.doIt(a0, a1);
-            };
+            }
         }, classe0, classe1);
     }
 
@@ -255,17 +242,16 @@ public class StateConfiguration<TState, TTrigger>
     /// <param name="entryAction">Action to execute, providing details of the transition.</param>
     /// <param name="trigger">The trigger by which the state must be entered in order for the action to execute.</param>
     /// <returns>The receiver.</returns>
-    public <TArg0, TArg1> StateConfiguration<TState, TTrigger> OnEntryFrom(TriggerWithParameters2<TArg0, TArg1, TState, TTrigger> trigger, final Action3<TArg0, TArg1, Transition<TState, TTrigger>> entryAction, final Class<TArg0> classe0, final Class<TArg1> classe1) throws Exception
-    {
+    public <TArg0, TArg1> StateConfiguration<TState, TTrigger> OnEntryFrom(TriggerWithParameters2<TArg0, TArg1, TState, TTrigger> trigger, final Action3<TArg0, TArg1, Transition<TState, TTrigger>> entryAction, final Class<TArg0> classe0, final Class<TArg1> classe1) throws Exception {
         Enforce.ArgumentNotNull(entryAction, "entryAction");
         Enforce.ArgumentNotNull(trigger, "trigger");
         _representation.AddEntryAction(trigger.getTrigger(), new Action2<Transition<TState, TTrigger>, Object[]>() {
             @SuppressWarnings("unchecked")
-			public void doIt(Transition<TState,TTrigger> t, Object[] args) throws Exception {
+            public void doIt(Transition<TState, TTrigger> t, Object[] args) throws Exception {
                 entryAction.doIt(
-                		(TArg0)args[0],
-                		(TArg1)args[1], t);
-            };
+                        (TArg0) args[0],
+                        (TArg1) args[1], t);
+            }
         });
         return this;
     }
@@ -280,13 +266,12 @@ public class StateConfiguration<TState, TTrigger>
     /// <param name="entryAction">Action to execute, providing details of the transition.</param>
     /// <param name="trigger">The trigger by which the state must be entered in order for the action to execute.</param>
     /// <returns>The receiver.</returns>
-    public <TArg0, TArg1, TArg2> StateConfiguration<TState, TTrigger> OnEntryFrom(TriggerWithParameters3<TArg0, TArg1, TArg2, TState, TTrigger> trigger, final Action3<TArg0, TArg1, TArg2> entryAction, final Class<TArg0> classe0, final Class<TArg1> classe1, final Class<TArg2> classe2) throws Exception
-    {
+    public <TArg0, TArg1, TArg2> StateConfiguration<TState, TTrigger> OnEntryFrom(TriggerWithParameters3<TArg0, TArg1, TArg2, TState, TTrigger> trigger, final Action3<TArg0, TArg1, TArg2> entryAction, final Class<TArg0> classe0, final Class<TArg1> classe1, final Class<TArg2> classe2) throws Exception {
         Enforce.ArgumentNotNull(entryAction, "entryAction");
         return OnEntryFrom(trigger, new Action4<TArg0, TArg1, TArg2, Transition<TState, TTrigger>>() {
             public void doIt(TArg0 a0, TArg1 a1, TArg2 a2, Transition<TState, TTrigger> t) throws Exception {
                 entryAction.doIt(a0, a1, a2);
-            };
+            }
         }, classe0, classe1, classe2);
     }
 
@@ -300,18 +285,17 @@ public class StateConfiguration<TState, TTrigger>
     /// <param name="entryAction">Action to execute, providing details of the transition.</param>
     /// <param name="trigger">The trigger by which the state must be entered in order for the action to execute.</param>
     /// <returns>The receiver.</returns>
-    public <TArg0, TArg1, TArg2> StateConfiguration<TState, TTrigger> OnEntryFrom(TriggerWithParameters3<TArg0, TArg1, TArg2, TState, TTrigger> trigger, final Action4<TArg0, TArg1, TArg2, Transition<TState, TTrigger>> entryAction, final Class<TArg0> classe0, final Class<TArg1> classe1, final Class<TArg2> classe2) throws Exception
-    {
+    public <TArg0, TArg1, TArg2> StateConfiguration<TState, TTrigger> OnEntryFrom(TriggerWithParameters3<TArg0, TArg1, TArg2, TState, TTrigger> trigger, final Action4<TArg0, TArg1, TArg2, Transition<TState, TTrigger>> entryAction, final Class<TArg0> classe0, final Class<TArg1> classe1, final Class<TArg2> classe2) throws Exception {
         Enforce.ArgumentNotNull(entryAction, "entryAction");
         Enforce.ArgumentNotNull(trigger, "trigger");
         _representation.AddEntryAction(trigger.getTrigger(), new Action2<Transition<TState, TTrigger>, Object[]>() {
             @SuppressWarnings("unchecked")
-			public void doIt(Transition<TState, TTrigger> t, Object[] args) throws Exception {
+            public void doIt(Transition<TState, TTrigger> t, Object[] args) throws Exception {
                 entryAction.doIt(
-                		(TArg0)args[0],
-                		(TArg1)args[1],
-                		(TArg2)args[2], t);
-            };
+                        (TArg0) args[0],
+                        (TArg1) args[1],
+                        (TArg2) args[2], t);
+            }
         });
         return this;
     }
@@ -322,12 +306,10 @@ public class StateConfiguration<TState, TTrigger>
     /// </summary>
     /// <param name="exitAction">Action to execute.</param>
     /// <returns>The receiver.</returns>
-    public StateConfiguration<TState, TTrigger> OnExit(final Action exitAction) throws Exception
-    {
+    public StateConfiguration<TState, TTrigger> OnExit(final Action exitAction) throws Exception {
         Enforce.ArgumentNotNull(exitAction, "exitAction");
         return OnExit(new Action1<Transition<TState, TTrigger>>() {
-            
-            public void doIt(Transition<TState, TTrigger> arg1) {
+            public void doIt(Transition<TState, TTrigger> arg1) throws Exception {
                 exitAction.doIt();
             }
         });
@@ -339,8 +321,7 @@ public class StateConfiguration<TState, TTrigger>
     /// </summary>
     /// <param name="exitAction">Action to execute, providing details of the transition.</param>
     /// <returns>The receiver.</returns>
-    public StateConfiguration<TState, TTrigger> OnExit(Action1<Transition<TState, TTrigger>> exitAction) throws Exception
-    {
+    public StateConfiguration<TState, TTrigger> OnExit(Action1<Transition<TState, TTrigger>> exitAction) throws Exception {
         Enforce.ArgumentNotNull(exitAction, "exitAction");
         _representation.AddExitAction(exitAction);
         return this;
@@ -358,8 +339,7 @@ public class StateConfiguration<TState, TTrigger>
     /// </remarks>
     /// <param name="superstate">The superstate.</param>
     /// <returns>The receiver.</returns>
-    public StateConfiguration<TState, TTrigger> SubstateOf(TState superstate) throws Exception
-    {
+    public StateConfiguration<TState, TTrigger> SubstateOf(TState superstate) throws Exception {
         StateRepresentation<TState, TTrigger> superRepresentation = _lookup.call(superstate);
         _representation.setSuperstate(superRepresentation);
         superRepresentation.AddSubstate(_representation);
@@ -374,8 +354,7 @@ public class StateConfiguration<TState, TTrigger>
     /// <param name="destinationStateSelector">Function to calculate the state 
     /// that the trigger will cause a transition to.</param>
     /// <returns>The reciever.</returns>
-    public StateConfiguration<TState, TTrigger> PermitDynamic(TTrigger trigger, final Func<TState> destinationStateSelector) throws Exception
-    {
+    public StateConfiguration<TState, TTrigger> PermitDynamic(TTrigger trigger, final Func<TState> destinationStateSelector) throws Exception {
         return PermitDynamicIf(trigger, destinationStateSelector, NoGuard);
     }
 
@@ -388,8 +367,7 @@ public class StateConfiguration<TState, TTrigger>
     /// that the trigger will cause a transition to.</param>
     /// <returns>The reciever.</returns>
     /// <typeparam name="TArg0">Type of the first trigger argument.</typeparam>
-    public <TArg0> StateConfiguration<TState, TTrigger> PermitDynamic(TriggerWithParameters1<TArg0, TState, TTrigger> trigger, Func2<TArg0, TState> destinationStateSelector) throws Exception
-    {
+    public <TArg0> StateConfiguration<TState, TTrigger> PermitDynamic(TriggerWithParameters1<TArg0, TState, TTrigger> trigger, Func2<TArg0, TState> destinationStateSelector) throws Exception {
         return permitDynamicIf(trigger, destinationStateSelector, NoGuard);
     }
 
@@ -403,8 +381,7 @@ public class StateConfiguration<TState, TTrigger>
     /// <returns>The reciever.</returns>
     /// <typeparam name="TArg0">Type of the first trigger argument.</typeparam>
     /// <typeparam name="TArg1">Type of the second trigger argument.</typeparam>
-    public <TArg0, TArg1> StateConfiguration<TState, TTrigger> PermitDynamic(TriggerWithParameters2<TArg0, TArg1, TState, TTrigger> trigger, Func3<TArg0, TArg1, TState> destinationStateSelector) throws Exception
-    {
+    public <TArg0, TArg1> StateConfiguration<TState, TTrigger> PermitDynamic(TriggerWithParameters2<TArg0, TArg1, TState, TTrigger> trigger, Func3<TArg0, TArg1, TState> destinationStateSelector) throws Exception {
         return permitDynamicIf(trigger, destinationStateSelector, NoGuard);
     }
 
@@ -419,8 +396,7 @@ public class StateConfiguration<TState, TTrigger>
     /// <typeparam name="TArg0">Type of the first trigger argument.</typeparam>
     /// <typeparam name="TArg1">Type of the second trigger argument.</typeparam>
     /// <typeparam name="TArg2">Type of the third trigger argument.</typeparam>
-    public <TArg0, TArg1, TArg2> StateConfiguration<TState, TTrigger> PermitDynamic(TriggerWithParameters3<TArg0, TArg1, TArg2, TState, TTrigger> trigger, final Func4<TArg0, TArg1, TArg2, TState> destinationStateSelector) throws Exception
-    {
+    public <TArg0, TArg1, TArg2> StateConfiguration<TState, TTrigger> PermitDynamic(TriggerWithParameters3<TArg0, TArg1, TArg2, TState, TTrigger> trigger, final Func4<TArg0, TArg1, TArg2, TState> destinationStateSelector) throws Exception {
         return permitDynamicIf(trigger, destinationStateSelector, NoGuard);
     }
 
@@ -435,12 +411,10 @@ public class StateConfiguration<TState, TTrigger>
     /// <param name="guard">Function that must return true in order for the
     /// trigger to be accepted.</param>
     /// <returns>The reciever.</returns>
-    public StateConfiguration<TState, TTrigger> PermitDynamicIf(TTrigger trigger, final Func<TState> destinationStateSelector, Func<Boolean> guard) throws Exception
-    {
+    public StateConfiguration<TState, TTrigger> PermitDynamicIf(TTrigger trigger, final Func<TState> destinationStateSelector, Func<Boolean> guard) throws Exception {
         Enforce.ArgumentNotNull(destinationStateSelector, "destinationStateSelector");
         return publicPermitDynamicIf(trigger, new Func2<Object[], TState>() {
-            
-            public TState call(Object[] arg0) {
+            public TState call(Object[] arg0) throws Exception {
                 return destinationStateSelector.call();
             }
         }, guard);
@@ -457,20 +431,19 @@ public class StateConfiguration<TState, TTrigger>
     /// trigger to be accepted.</param>
     /// <returns>The reciever.</returns>
     /// <typeparam name="TArg0">Type of the first trigger argument.</typeparam>
-    public <TArg0> StateConfiguration<TState, TTrigger> permitDynamicIf(TriggerWithParameters1<TArg0, TState, TTrigger> trigger, final Func2<TArg0, TState> destinationStateSelector, Func<Boolean> guard) throws Exception
-    {
+    public <TArg0> StateConfiguration<TState, TTrigger> permitDynamicIf(TriggerWithParameters1<TArg0, TState, TTrigger> trigger, final Func2<TArg0, TState> destinationStateSelector, Func<Boolean> guard) throws Exception {
         Enforce.ArgumentNotNull(trigger, "trigger");
         Enforce.ArgumentNotNull(destinationStateSelector, "destinationStateSelector");
         return publicPermitDynamicIf(
-            trigger.getTrigger(), new Func2<Object[], TState>() {
-                @SuppressWarnings("unchecked")
-				
-                public TState call(Object[] args) throws Exception {
-                    return destinationStateSelector.call((TArg0) args[0]);
-                        
-                }
-            },
-            guard);
+                trigger.getTrigger(), new Func2<Object[], TState>() {
+                    @SuppressWarnings("unchecked")
+                    public TState call(Object[] args) throws Exception {
+                        return destinationStateSelector.call((TArg0) args[0]);
+
+                    }
+                },
+                guard
+        );
     }
 
     /// <summary>
@@ -485,21 +458,21 @@ public class StateConfiguration<TState, TTrigger>
     /// <returns>The reciever.</returns>
     /// <typeparam name="TArg0">Type of the first trigger argument.</typeparam>
     /// <typeparam name="TArg1">Type of the second trigger argument.</typeparam>
-    public <TArg0, TArg1> StateConfiguration<TState, TTrigger> permitDynamicIf(TriggerWithParameters2<TArg0, TArg1, TState, TTrigger> trigger, final Func3<TArg0, TArg1, TState> destinationStateSelector, Func<Boolean> guard) throws Exception
-    {
+    public <TArg0, TArg1> StateConfiguration<TState, TTrigger> permitDynamicIf(TriggerWithParameters2<TArg0, TArg1, TState, TTrigger> trigger, final Func3<TArg0, TArg1, TState> destinationStateSelector, Func<Boolean> guard) throws Exception {
         Enforce.ArgumentNotNull(trigger, "trigger");
         Enforce.ArgumentNotNull(destinationStateSelector, "destinationStateSelector");
         return publicPermitDynamicIf(
-            trigger.getTrigger(), new Func2<Object[], TState>() {
-                @SuppressWarnings("unchecked")
-				
-                public TState call(Object[] args) throws Exception {
-                    return destinationStateSelector.call(
-                    		(TArg0) args[0],
-                    		(TArg1) args[1]);
-                }
-            },
-            guard);
+                trigger.getTrigger(), new Func2<Object[], TState>() {
+                    @SuppressWarnings("unchecked")
+
+                    public TState call(Object[] args) throws Exception {
+                        return destinationStateSelector.call(
+                                (TArg0) args[0],
+                                (TArg1) args[1]);
+                    }
+                },
+                guard
+        );
     }
 
     /// <summary>
@@ -515,57 +488,51 @@ public class StateConfiguration<TState, TTrigger>
     /// <typeparam name="TArg0">Type of the first trigger argument.</typeparam>
     /// <typeparam name="TArg1">Type of the second trigger argument.</typeparam>
     /// <typeparam name="TArg2">Type of the third trigger argument.</typeparam>
-    public <TArg0, TArg1, TArg2> StateConfiguration<TState, TTrigger> permitDynamicIf(TriggerWithParameters3<TArg0, TArg1, TArg2, TState, TTrigger> trigger, final Func4<TArg0, TArg1, TArg2, TState> destinationStateSelector, Func<Boolean> guard) throws Exception
-    {
+    public <TArg0, TArg1, TArg2> StateConfiguration<TState, TTrigger> permitDynamicIf(TriggerWithParameters3<TArg0, TArg1, TArg2, TState, TTrigger> trigger, final Func4<TArg0, TArg1, TArg2, TState> destinationStateSelector, Func<Boolean> guard) throws Exception {
         Enforce.ArgumentNotNull(trigger, "trigger");
         Enforce.ArgumentNotNull(destinationStateSelector, "destinationStateSelector");
         return publicPermitDynamicIf(
-            trigger.getTrigger(), new Func2<Object[], TState>() {
-                @SuppressWarnings("unchecked")
-				
-                public TState call(Object[] args) throws Exception {
-                    return destinationStateSelector.call(
-                    		(TArg0) args[0],
-                    		(TArg1) args[1],
-                    		(TArg2) args[2]
+                trigger.getTrigger(), new Func2<Object[], TState>() {
+                    @SuppressWarnings("unchecked")
+
+                    public TState call(Object[] args) throws Exception {
+                        return destinationStateSelector.call(
+                                (TArg0) args[0],
+                                (TArg1) args[1],
+                                (TArg2) args[2]
                         );
-                }
-            }, guard);
+                    }
+                }, guard
+        );
     }
 
-    void enforceNotIdentityTransition(TState destination) throws Exception
-    {
-        if (destination.equals(_representation.getUnderlyingState()))
-        {
+    void enforceNotIdentityTransition(TState destination) throws Exception {
+        if (destination.equals(_representation.getUnderlyingState())) {
             throw new Exception(StateConfigurationResources.SelfTransitionsEitherIgnoredOrReentrant);
         }
     }
 
-    StateConfiguration<TState, TTrigger> publicPermit(TTrigger trigger, TState destinationState) throws Exception
-    {
+    StateConfiguration<TState, TTrigger> publicPermit(TTrigger trigger, TState destinationState) throws Exception {
         return publicPermitIf(trigger, destinationState, new Func<Boolean>() {
-            
-            
+
+
             public Boolean call() {
                 return true;
             }
         });
     }
 
-    StateConfiguration<TState, TTrigger> publicPermitIf(TTrigger trigger, TState destinationState, Func<Boolean> guard) throws Exception
-    {
+    StateConfiguration<TState, TTrigger> publicPermitIf(TTrigger trigger, TState destinationState, Func<Boolean> guard) throws Exception {
         Enforce.ArgumentNotNull(guard, "guard");
         _representation.AddTriggerBehaviour(new TransitioningTriggerBehaviour<TState, TTrigger>(trigger, destinationState, guard));
-        return this;                
+        return this;
     }
 
-    StateConfiguration<TState, TTrigger> publicPermitDynamic(TTrigger trigger, Func2<Object[], TState> destinationStateSelector) throws Exception
-    {
+    StateConfiguration<TState, TTrigger> publicPermitDynamic(TTrigger trigger, Func2<Object[], TState> destinationStateSelector) throws Exception {
         return publicPermitDynamicIf(trigger, destinationStateSelector, NoGuard);
     }
 
-    StateConfiguration<TState, TTrigger> publicPermitDynamicIf(TTrigger trigger, Func2<Object[], TState> destinationStateSelector, Func<Boolean> guard) throws Exception
-    {
+    StateConfiguration<TState, TTrigger> publicPermitDynamicIf(TTrigger trigger, Func2<Object[], TState> destinationStateSelector, Func<Boolean> guard) throws Exception {
         Enforce.ArgumentNotNull(destinationStateSelector, "destinationStateSelector");
         Enforce.ArgumentNotNull(guard, "guard");
         _representation.AddTriggerBehaviour(new DynamicTriggerBehaviour<TState, TTrigger>(trigger, destinationStateSelector, guard));
