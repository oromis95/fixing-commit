@@ -25,13 +25,13 @@ public class DynamicTransitionActionTests {
             TriggerX = Trigger.X, TriggerY = Trigger.Y;
     
     final private TriggerWithParameters1<Integer, Trigger> TriggerX1 =
-    		new TriggerWithParameters1<Integer, Trigger>(Trigger.X, Integer.class);
+            new TriggerWithParameters1<>(Trigger.X, Integer.class);
     
     final private TriggerWithParameters2<Integer, Integer, Trigger> TriggerY2 =
-    		new TriggerWithParameters2<Integer, Integer, Trigger>(Trigger.Y, Integer.class, Integer.class);
+            new TriggerWithParameters2<>(Trigger.Y, Integer.class, Integer.class);
     
     final private TriggerWithParameters3<Integer, Integer, Integer, Trigger> TriggerY3 =
-    		new TriggerWithParameters3<Integer, Integer, Integer, Trigger>(Trigger.Y, Integer.class, Integer.class, Integer.class);
+            new TriggerWithParameters3<>(Trigger.Y, Integer.class, Integer.class, Integer.class);
 
     
     private class DynamicallyGotoState<T> implements Func<State>, Func2<T, State>, Func3<T, T, State>, Func4<T, T, T, State> {
@@ -61,10 +61,10 @@ public class DynamicTransitionActionTests {
         public State call(T val1, T val2, T val3) {
             return this.targetState;
         }
-    };
+    }
     
-    private DynamicallyGotoState<Integer> gotoA = new DynamicallyGotoState<Integer>(State.A);
-    private DynamicallyGotoState<Integer> gotoB = new DynamicallyGotoState<Integer>(State.B);
+    private DynamicallyGotoState<Integer> gotoA = new DynamicallyGotoState<>(State.A);
+    private DynamicallyGotoState<Integer> gotoB = new DynamicallyGotoState<>(State.B);
 
     private class AccumulatingAction<T> implements Action1<T>, Action2<T,T>, Action3<T,T,T>, Action4<T,T,T,T> {
         private List<T> accumulator;
@@ -118,11 +118,11 @@ public class DynamicTransitionActionTests {
     public void UnguardedDynamicTransitionActionsArePerformed() {
         StateMachineConfig<State, Trigger> config = new StateMachineConfig<>();
 
-        List<Integer> list = new ArrayList<Integer>();
-        FixedAccumulator<Integer>   actionZero = new FixedAccumulator<Integer>(list, new Integer(0));
-        AccumulatingAction<Integer> actionOne = new AccumulatingAction<Integer>(list);
-        AccumulatingAction<Integer> actionTwo = new AccumulatingAction<Integer>(list);
-        AccumulatingAction<Integer> actionThree = new AccumulatingAction<Integer>(list);
+        List<Integer> list = new ArrayList<>();
+        FixedAccumulator<Integer>   actionZero = new FixedAccumulator<>(list, 0);
+        AccumulatingAction<Integer> actionOne = new AccumulatingAction<>(list);
+        AccumulatingAction<Integer> actionTwo = new AccumulatingAction<>(list);
+        AccumulatingAction<Integer> actionThree = new AccumulatingAction<>(list);
 
         config.configure(State.A)
                 .permitDynamic(Trigger.X, gotoB, actionZero)
@@ -133,30 +133,30 @@ public class DynamicTransitionActionTests {
 
         StateMachine<State, Trigger> sm = new StateMachine<>(State.A, config);
         sm.fire(Trigger.X);
-        sm.fire(TriggerX1, new Integer(2));
-        sm.fire(TriggerY2, new Integer(4), new Integer(6));
-        sm.fire(TriggerY3, new Integer(8), new Integer(10), new Integer(12));
+        sm.fire(TriggerX1, 2);
+        sm.fire(TriggerY2, 4, 6);
+        sm.fire(TriggerY3, 8, 10, 12);
 
         assertEquals(State.A, sm.getState());
         assertEquals(7, list.size());
-        assertEquals(new Integer(0), list.get(0));
-        assertEquals(new Integer(2), list.get(1));
-        assertEquals(new Integer(4), list.get(2));
-        assertEquals(new Integer(6), list.get(3));
-        assertEquals(new Integer(8), list.get(4));
-        assertEquals(new Integer(10), list.get(5));
-        assertEquals(new Integer(12), list.get(6));
+        assertEquals(Integer.valueOf(0), list.get(0));
+        assertEquals(Integer.valueOf(2), list.get(1));
+        assertEquals(Integer.valueOf(4), list.get(2));
+        assertEquals(Integer.valueOf(6), list.get(3));
+        assertEquals(Integer.valueOf(8), list.get(4));
+        assertEquals(Integer.valueOf(10), list.get(5));
+        assertEquals(Integer.valueOf(12), list.get(6));
     }
 
     @Test
     public void GuardedDynamicTransitionActionsArePerformed() {
         StateMachineConfig<State, Trigger> config = new StateMachineConfig<>();
 
-        List<Integer> list = new ArrayList<Integer>();
-        FixedAccumulator<Integer>   actionZero = new FixedAccumulator<Integer>(list, new Integer(0));
-        AccumulatingAction<Integer> actionOne = new AccumulatingAction<Integer>(list);
-        AccumulatingAction<Integer> actionTwo = new AccumulatingAction<Integer>(list);
-        AccumulatingAction<Integer> actionThree = new AccumulatingAction<Integer>(list);
+        List<Integer> list = new ArrayList<>();
+        FixedAccumulator<Integer>   actionZero = new FixedAccumulator<>(list, 0);
+        AccumulatingAction<Integer> actionOne = new AccumulatingAction<>(list);
+        AccumulatingAction<Integer> actionTwo = new AccumulatingAction<>(list);
+        AccumulatingAction<Integer> actionThree = new AccumulatingAction<>(list);
 
         config.configure(State.A)
                 .permitDynamicIf(Trigger.X, gotoB, InternalTriggerBehaviourTests.returnTrue, actionZero)
@@ -167,18 +167,18 @@ public class DynamicTransitionActionTests {
 
         StateMachine<State, Trigger> sm = new StateMachine<>(State.A, config);
         sm.fire(Trigger.X);
-        sm.fire(TriggerX1, new Integer(3));
-        sm.fire(TriggerY2, new Integer(6), new Integer(9));
-        sm.fire(TriggerY3, new Integer(12), new Integer(15), new Integer(18));
+        sm.fire(TriggerX1, 3);
+        sm.fire(TriggerY2, 6, 9);
+        sm.fire(TriggerY3, 12, 15, 18);
 
         assertEquals(State.A, sm.getState());
         assertEquals(7, list.size());
-        assertEquals(new Integer(0), list.get(0));
-        assertEquals(new Integer(3), list.get(1));
-        assertEquals(new Integer(6), list.get(2));
-        assertEquals(new Integer(9), list.get(3));
-        assertEquals(new Integer(12), list.get(4));
-        assertEquals(new Integer(15), list.get(5));
-        assertEquals(new Integer(18), list.get(6));
+        assertEquals(Integer.valueOf(0), list.get(0));
+        assertEquals(Integer.valueOf(3), list.get(1));
+        assertEquals(Integer.valueOf(6), list.get(2));
+        assertEquals(Integer.valueOf(9), list.get(3));
+        assertEquals(Integer.valueOf(12), list.get(4));
+        assertEquals(Integer.valueOf(15), list.get(5));
+        assertEquals(Integer.valueOf(18), list.get(6));
     }
 }
