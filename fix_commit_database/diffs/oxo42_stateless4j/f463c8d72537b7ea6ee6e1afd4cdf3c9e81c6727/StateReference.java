@@ -1,7 +1,7 @@
 package com.googlecode.stateless4j;
 
 public class StateReference<TState, TTrigger> {
-    TState state;
+    private TState state;
 
     public TState getState() {
         return state;
@@ -10,4 +10,4 @@ public class StateReference<TState, TTrigger> {
     public void setState(TState value) {
         state = value;
     }
-}
\ No newline at end of file
+}
