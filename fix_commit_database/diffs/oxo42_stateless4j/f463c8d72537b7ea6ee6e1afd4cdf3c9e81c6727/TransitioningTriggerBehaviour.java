@@ -3,18 +3,15 @@ package com.googlecode.stateless4j.transitions;
 import com.googlecode.stateless4j.delegates.Func;
 import com.googlecode.stateless4j.triggers.TriggerBehaviour;
 
-public class TransitioningTriggerBehaviour<TState, TTrigger> extends TriggerBehaviour<TState, TTrigger>
-{
-    final TState _destination;
+public class TransitioningTriggerBehaviour<TState, TTrigger> extends TriggerBehaviour<TState, TTrigger> {
+    private final TState destination;
 
-    public TransitioningTriggerBehaviour(TTrigger trigger, TState destination, Func<Boolean> guard)
-    {
+    public TransitioningTriggerBehaviour(TTrigger trigger, TState destination, Func<Boolean> guard) {
         super(trigger, guard);
-        _destination = destination;
+        this.destination = destination;
     }
 
-    public TState ResultsInTransitionFrom(TState source, Object... args)
-    {
-        return _destination;
+    public TState resultsInTransitionFrom(TState source, Object... args) {
+        return destination;
     }
-}
\ No newline at end of file
+}
