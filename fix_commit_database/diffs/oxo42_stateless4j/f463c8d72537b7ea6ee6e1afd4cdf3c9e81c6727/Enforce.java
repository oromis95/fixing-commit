@@ -1,12 +1,16 @@
 package com.googlecode.stateless4j.validation;
 
-public class Enforce
-{
-    public static <T extends Object> T ArgumentNotNull(T argument, String description) throws Exception
-    {
-        if (argument == null)
+public final class Enforce {
+    private Enforce() {
+
+    }
+
+    public static <T> T argumentNotNull(T argument, String description) throws Exception {
+        if (argument == null) {
             throw new Exception(description);
+        }
 
         return argument;
     }
 }
+
