@@ -2,7 +2,6 @@ package com.googlecode.stateless4j;
 
 import com.googlecode.stateless4j.delegates.Action1;
 import com.googlecode.stateless4j.delegates.Action2;
-import com.googlecode.stateless4j.resources.StateRepresentationResources;
 import com.googlecode.stateless4j.transitions.Transition;
 import com.googlecode.stateless4j.triggers.TriggerBehaviour;
 import com.googlecode.stateless4j.validation.Enforce;
@@ -13,56 +12,59 @@ import java.util.List;
 import java.util.Map;
 
 public class StateRepresentation<TState, TTrigger> {
-    final TState _state;
+    private final TState state;
 
-    final Map<TTrigger, List<TriggerBehaviour<TState, TTrigger>>> _triggerBehaviours =
-            new HashMap<TTrigger, List<TriggerBehaviour<TState, TTrigger>>>();
-
-    final List<Action2<Transition<TState, TTrigger>, Object[]>> _entryActions = new ArrayList<Action2<Transition<TState, TTrigger>, Object[]>>();
-    final List<Action1<Transition<TState, TTrigger>>> _exitActions = new ArrayList<Action1<Transition<TState, TTrigger>>>();
-    final List<StateRepresentation<TState, TTrigger>> _substates = new ArrayList<StateRepresentation<TState, TTrigger>>();
-    StateRepresentation<TState, TTrigger> _superstate; // null
+    private final Map<TTrigger, List<TriggerBehaviour<TState, TTrigger>>> triggerBehaviours = new HashMap<>();
+    private final List<Action2<Transition<TState, TTrigger>, Object[]>> entryActions = new ArrayList<>();
+    private final List<Action1<Transition<TState, TTrigger>>> exitActions = new ArrayList<>();
+    private final List<StateRepresentation<TState, TTrigger>> substates = new ArrayList<>();
+    private StateRepresentation<TState, TTrigger> superstate; // null
 
     public StateRepresentation(TState state) {
-        _state = state;
+        this.state = state;
+    }
+
+    protected Map<TTrigger, List<TriggerBehaviour<TState, TTrigger>>> getTriggerBehaviours() {
+        return triggerBehaviours;
     }
 
-    public Boolean CanHandle(TTrigger trigger) {
+    public Boolean canHandle(TTrigger trigger) {
         try {
-            TryFindHandler(trigger);
+            tryFindHandler(trigger);
             return true;
         } catch (Exception e) {
+            return false;
         }
-        return false;
     }
 
-    public TriggerBehaviour<TState, TTrigger> TryFindHandler(TTrigger trigger) {
+    public TriggerBehaviour<TState, TTrigger> tryFindHandler(TTrigger trigger) {
         try {
-            return TryFindLocalHandler(trigger);
+            return tryFindLocalHandler(trigger);
         } catch (Exception e) {
-            return getSuperstate().TryFindHandler(trigger);
+            return getSuperstate().tryFindHandler(trigger);
         }
     }
 
-    TriggerBehaviour<TState, TTrigger> TryFindLocalHandler(TTrigger trigger/*, out TriggerBehaviour handler*/) throws Exception {
+    TriggerBehaviour<TState, TTrigger> tryFindLocalHandler(TTrigger trigger/*, out TriggerBehaviour handler*/) throws Exception {
         List<TriggerBehaviour<TState, TTrigger>> possible;
-        if (!_triggerBehaviours.containsKey(trigger)) {
+        if (!triggerBehaviours.containsKey(trigger)) {
             throw new Exception();
         }
-        possible = _triggerBehaviours.get(trigger);
+        possible = triggerBehaviours.get(trigger);
 
-        List<TriggerBehaviour<TState, TTrigger>> actual = new ArrayList<TriggerBehaviour<TState, TTrigger>>();
+        List<TriggerBehaviour<TState, TTrigger>> actual = new ArrayList<>();
         for (TriggerBehaviour<TState, TTrigger> triggerBehaviour : possible) {
             if (triggerBehaviour.isGuardConditionMet()) {
                 actual.add(triggerBehaviour);
             }
         }
 
-        if (actual.size() > 1)
+        if (actual.size() > 1) {
             throw new Exception(
-                    String.format(StateRepresentationResources.MultipleTransitionsPermitted,
-                            trigger, _state)
+                    String.format("Multiple permitted exit transitions are configured from state '%s' for trigger '%s'. Guard clauses must be mutually exclusive.",
+                            trigger, state)
             );
+        }
 
         TriggerBehaviour<TState, TTrigger> handler = actual.get(0);
         if (handler == null) {
@@ -71,114 +73,117 @@ public class StateRepresentation<TState, TTrigger> {
         return handler;
     }
 
-    public void AddEntryAction(final TTrigger trigger, final Action2<Transition<TState, TTrigger>, Object[]> action) throws Exception {
-        Enforce.ArgumentNotNull(action, "action");
+    public void addEntryAction(final TTrigger trigger, final Action2<Transition<TState, TTrigger>, Object[]> action) throws Exception {
+        Enforce.argumentNotNull(action, "action");
 
 
-        _entryActions.add(new Action2<Transition<TState, TTrigger>, Object[]>() {
+        entryActions.add(new Action2<Transition<TState, TTrigger>, Object[]>() {
             public void doIt(Transition<TState, TTrigger> t, Object[] args) throws Exception {
-                if (t.getTrigger().equals(trigger))
+                if (t.getTrigger().equals(trigger)) {
                     action.doIt(t, args);
+                }
             }
         });
     }
 
-    public void AddEntryAction(Action2<Transition<TState, TTrigger>, Object[]> action) throws Exception {
-        _entryActions.add(Enforce.ArgumentNotNull(action, "action"));
+    public void addEntryAction(Action2<Transition<TState, TTrigger>, Object[]> action) throws Exception {
+        entryActions.add(Enforce.argumentNotNull(action, "action"));
     }
 
-    public void AddExitAction(Action1<Transition<TState, TTrigger>> action) throws Exception {
-        _exitActions.add(Enforce.ArgumentNotNull(action, "action"));
+    public void addExitAction(Action1<Transition<TState, TTrigger>> action) throws Exception {
+        exitActions.add(Enforce.argumentNotNull(action, "action"));
     }
 
-    public void Enter(Transition<TState, TTrigger> transition, Object... entryArgs) throws Exception {
-        Enforce.ArgumentNotNull(transition, "transtion");
+    public void enter(Transition<TState, TTrigger> transition, Object... entryArgs) throws Exception {
+        Enforce.argumentNotNull(transition, "transtion");
 
         if (transition.isReentry()) {
-            ExecuteEntryActions(transition, entryArgs);
-        } else if (!Includes(transition.getSource())) {
-            if (_superstate != null)
-                _superstate.Enter(transition, entryArgs);
+            executeEntryActions(transition, entryArgs);
+        } else if (!includes(transition.getSource())) {
+            if (superstate != null) {
+                superstate.enter(transition, entryArgs);
+            }
 
-            ExecuteEntryActions(transition, entryArgs);
+            executeEntryActions(transition, entryArgs);
         }
     }
 
-    public void Exit(Transition<TState, TTrigger> transition) throws Exception {
-        Enforce.ArgumentNotNull(transition, "transtion");
+    public void exit(Transition<TState, TTrigger> transition) throws Exception {
+        Enforce.argumentNotNull(transition, "transtion");
 
         if (transition.isReentry()) {
-            ExecuteExitActions(transition);
-        } else if (!Includes(transition.getDestination())) {
-            ExecuteExitActions(transition);
-            if (_superstate != null)
-                _superstate.Exit(transition);
+            executeExitActions(transition);
+        } else if (!includes(transition.getDestination())) {
+            executeExitActions(transition);
+            if (superstate != null) {
+                superstate.exit(transition);
+            }
         }
     }
 
-    void ExecuteEntryActions(Transition<TState, TTrigger> transition, Object[] entryArgs) throws Exception {
-        Enforce.ArgumentNotNull(transition, "transtion");
-        Enforce.ArgumentNotNull(entryArgs, "entryArgs");
-        for (Action2<Transition<TState, TTrigger>, Object[]> action : _entryActions)
+    void executeEntryActions(Transition<TState, TTrigger> transition, Object[] entryArgs) throws Exception {
+        Enforce.argumentNotNull(transition, "transtion");
+        Enforce.argumentNotNull(entryArgs, "entryArgs");
+        for (Action2<Transition<TState, TTrigger>, Object[]> action : entryActions) {
             action.doIt(transition, entryArgs);
+        }
     }
 
-    void ExecuteExitActions(Transition<TState, TTrigger> transition) throws Exception {
-        Enforce.ArgumentNotNull(transition, "transtion");
-        for (Action1<Transition<TState, TTrigger>> action : _exitActions)
+    void executeExitActions(Transition<TState, TTrigger> transition) throws Exception {
+        Enforce.argumentNotNull(transition, "transtion");
+        for (Action1<Transition<TState, TTrigger>> action : exitActions) {
             action.doIt(transition);
+        }
     }
 
-    public void AddTriggerBehaviour(TriggerBehaviour<TState, TTrigger> triggerBehaviour) {
+    public void addTriggerBehaviour(TriggerBehaviour<TState, TTrigger> triggerBehaviour) {
         List<TriggerBehaviour<TState, TTrigger>> allowed;
-        if (!_triggerBehaviours.containsKey(triggerBehaviour.getTrigger())) {
-            allowed = new ArrayList<TriggerBehaviour<TState, TTrigger>>();
-            _triggerBehaviours.put(triggerBehaviour.getTrigger(), allowed);
+        if (!triggerBehaviours.containsKey(triggerBehaviour.getTrigger())) {
+            allowed = new ArrayList<>();
+            triggerBehaviours.put(triggerBehaviour.getTrigger(), allowed);
         }
-        allowed = _triggerBehaviours.get(triggerBehaviour.getTrigger());
+        allowed = triggerBehaviours.get(triggerBehaviour.getTrigger());
         allowed.add(triggerBehaviour);
     }
 
     public StateRepresentation<TState, TTrigger> getSuperstate() {
-        return _superstate;
+        return superstate;
     }
 
     public void setSuperstate(StateRepresentation<TState, TTrigger> value) {
-        _superstate = value;
+        superstate = value;
     }
 
     public TState getUnderlyingState() {
-        return _state;
+        return state;
     }
 
-    public void AddSubstate(StateRepresentation<TState, TTrigger> substate) throws Exception {
-        Enforce.ArgumentNotNull(substate, "substate");
-        _substates.add(substate);
+    public void addSubstate(StateRepresentation<TState, TTrigger> substate) throws Exception {
+        Enforce.argumentNotNull(substate, "substate");
+        substates.add(substate);
     }
 
-    public Boolean Includes(TState state) {
+    public Boolean includes(TState stateToCheck) {
         Boolean isIncluded = false;
-        for (StateRepresentation<TState, TTrigger> s : _substates) {
-            if (s.Includes(state)) {
+        for (StateRepresentation<TState, TTrigger> s : substates) {
+            if (s.includes(stateToCheck)) {
                 isIncluded = true;
             }
         }
-        return _state.equals(state) || isIncluded;
+        return this.state.equals(stateToCheck) || isIncluded;
     }
 
-    public Boolean IsIncludedIn(TState state) {
-        return
-                _state.equals(state) ||
-                        (_superstate != null && _superstate.IsIncludedIn(state));
+    public Boolean isIncludedIn(TState stateToCheck) {
+        return this.state.equals(stateToCheck) || (superstate != null && superstate.isIncludedIn(stateToCheck));
     }
 
     @SuppressWarnings("unchecked")
     public List<TTrigger> getPermittedTriggers() throws Exception {
-        List<TTrigger> result = new ArrayList<TTrigger>();
+        List<TTrigger> result = new ArrayList<>();
 
-        for (TTrigger t : _triggerBehaviours.keySet()) {
+        for (TTrigger t : triggerBehaviours.keySet()) {
             Boolean isOk = false;
-            for (TriggerBehaviour<TState, TTrigger> v : _triggerBehaviours.get(t)) {
+            for (TriggerBehaviour<TState, TTrigger> v : triggerBehaviours.get(t)) {
                 if (v.isGuardConditionMet()) {
                     isOk = true;
                 }
@@ -194,4 +199,4 @@ public class StateRepresentation<TState, TTrigger> {
 
         return result;
     }
-}
\ No newline at end of file
+}
