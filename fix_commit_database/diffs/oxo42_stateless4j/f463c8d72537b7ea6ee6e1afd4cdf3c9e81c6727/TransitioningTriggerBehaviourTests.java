@@ -10,7 +10,7 @@ public class TransitioningTriggerBehaviourTests {
     @Test
     public void TransitionsToDestinationState() {
         TransitioningTriggerBehaviour<State, Trigger> transtioning = new TransitioningTriggerBehaviour<State, Trigger>(Trigger.X, State.C, IgnoredTriggerBehaviourTests.returnTrue);
-        State destination = transtioning.ResultsInTransitionFrom(State.B, new Object[0]);
+        State destination = transtioning.resultsInTransitionFrom(State.B, new Object[0]);
         Assert.assertEquals(State.C, destination);
     }
 }
