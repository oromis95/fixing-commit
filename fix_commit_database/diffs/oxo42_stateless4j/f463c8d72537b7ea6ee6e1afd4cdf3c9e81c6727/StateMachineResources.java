@@ -1,9 +0,0 @@
-package com.googlecode.stateless4j.resources;
-
-
-public class StateMachineResources {
-
-    public static final String CannotReconfigureParameters = "CannotReconfigureParameters";
-    public static final String NoTransitionsPermitted = "NoTransitionsPermitted";
-
-}
