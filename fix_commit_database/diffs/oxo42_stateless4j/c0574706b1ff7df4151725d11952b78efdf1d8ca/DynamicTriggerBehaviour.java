@@ -3,7 +3,6 @@ package com.github.oxo42.stateless4j.triggers;
 import com.github.oxo42.stateless4j.OutVar;
 import com.github.oxo42.stateless4j.delegates.Func2;
 import com.github.oxo42.stateless4j.delegates.FuncBoolean;
-import com.github.oxo42.stateless4j.validation.Enforce;
 
 public class DynamicTriggerBehaviour<TState, TTrigger> extends TriggerBehaviour<TState, TTrigger> {
 
