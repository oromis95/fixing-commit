@@ -6,11 +6,11 @@ import org.junit.Test;
 import com.googlecode.stateless4j.transitions.TransitioningTriggerBehaviour;
 
 
-public class TriggerBehaviourFixture {
+public class TriggerBehaviourTests {
     @Test
     public void ExposesCorrectUnderlyingTrigger() {
         TransitioningTriggerBehaviour<State, Trigger> transtioning = new TransitioningTriggerBehaviour<State, Trigger>(
-                Trigger.X, State.C, IgnoredTriggerBehaviourFixture.returnTrue);
+                Trigger.X, State.C, IgnoredTriggerBehaviourTests.returnTrue);
 
         Assert.assertEquals(Trigger.X, transtioning.getTrigger());
     }
@@ -18,7 +18,7 @@ public class TriggerBehaviourFixture {
     @Test
     public void WhenGuardConditionFalse_IsGuardConditionMetIsFalse() throws Exception {
         TransitioningTriggerBehaviour<State, Trigger> transtioning = new TransitioningTriggerBehaviour<State, Trigger>(
-                Trigger.X, State.C, IgnoredTriggerBehaviourFixture.returnFalse);
+                Trigger.X, State.C, IgnoredTriggerBehaviourTests.returnFalse);
 
         Assert.assertFalse(transtioning.isGuardConditionMet());
     }
@@ -26,7 +26,7 @@ public class TriggerBehaviourFixture {
     @Test
     public void WhenGuardConditionTrue_IsGuardConditionMetIsTrue() throws Exception {
         TransitioningTriggerBehaviour<State, Trigger> transtioning = new TransitioningTriggerBehaviour<State, Trigger>(
-                Trigger.X, State.C, IgnoredTriggerBehaviourFixture.returnTrue);
+                Trigger.X, State.C, IgnoredTriggerBehaviourTests.returnTrue);
 
         Assert.assertTrue(transtioning.isGuardConditionMet());
     }
