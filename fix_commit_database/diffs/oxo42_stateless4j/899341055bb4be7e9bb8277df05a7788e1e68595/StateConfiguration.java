@@ -53,7 +53,7 @@ public class StateConfiguration<TState , TTrigger > {
     /**
      * Accept the specified trigger, execute exit actions and re-execute entry actions. Reentry behaves as though the
      * configured state transitions to an identical sibling state
-     * <p/>
+     * <p>
      * Applies to the current state only. Will not re-execute superstate actions, or  cause actions to execute
      * transitioning between super- and sub-states
      *
@@ -68,7 +68,7 @@ public class StateConfiguration<TState , TTrigger > {
     /**
      * Accept the specified trigger, execute exit actions and re-execute entry actions. Reentry behaves as though the
      * configured state transitions to an identical sibling state
-     * <p/>
+     * <p>
      * Applies to the current state only. Will not re-execute superstate actions, or  cause actions to execute
      * transitioning between super- and sub-states
      *
@@ -345,7 +345,7 @@ public class StateConfiguration<TState , TTrigger > {
 
     /**
      * Sets the superstate that the configured state is a substate of
-     * <p/>
+     * <p>
      * Substates inherit the allowed transitions of their superstate.
      * When entering directly into a substate from outside of the superstate,
      * entry actions for the superstate are executed.
