@@ -70,6 +70,9 @@ class JSONParserString extends JSONParserMemory {
 		return in.indexOf(c, pos);
 	}
 
+	/**
+	 * Read next char or END OF INPUT
+	 */
 	protected void read() {
 		if (++pos >= len)
 			this.c = EOI;
@@ -78,7 +81,7 @@ class JSONParserString extends JSONParserMemory {
 	}
 
 	/**
-	 * Same as read() in memory parssing
+	 * Same as read() in memory parsing
 	 */
 	protected void readS() {
 		if (++pos >= len)
