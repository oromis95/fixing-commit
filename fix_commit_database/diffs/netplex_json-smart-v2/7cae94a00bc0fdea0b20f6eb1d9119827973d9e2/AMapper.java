@@ -45,7 +45,7 @@ public abstract class AMapper<T> {
 	}
 
 	/**
-	 * called when json-smart done parssing a value
+	 * called when json-smart done parsing a value
 	 */
 	public void setValue(Object current, String key, Object value) throws ParseException, IOException {
 		throw new RuntimeException(ERR_MSG);
