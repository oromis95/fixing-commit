@@ -19,10 +19,8 @@ import net.minidev.json.actions.path.PathDelimiter;
  * @author adoneitan@gmail.com
  *
  */
-public class JSONTraverser extends TreeTraverser<JSONObject, JSONArray>
-{
-	public JSONTraverser(JSONTraverseAction action)
-	{
+public class JSONTraverser extends TreeTraverser<JSONObject, JSONArray> {
+	public JSONTraverser(JSONTraverseAction action) {
 		super(action, new DotDelimiter());
 	}
 
