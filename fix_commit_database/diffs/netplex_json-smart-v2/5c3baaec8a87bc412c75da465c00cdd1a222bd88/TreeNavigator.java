@@ -28,14 +28,12 @@ import java.util.Map;
  * @since 15 June 2016.
  *
  */
-public class TreeNavigator<M extends Map<String, Object>, L extends List<Object>>
-{
+public class TreeNavigator<M extends Map<String, Object>, L extends List<Object>> {
 	protected List<String> pathsToNavigate;
 	protected NavigateAction action;
 	protected String pathPrefix = "";
 
-	public TreeNavigator(NavigateAction action, List<String> pathsToNavigate)
-	{
+	public TreeNavigator(NavigateAction action, List<String> pathsToNavigate) {
 		if (action == null) {
 			throw new IllegalArgumentException("NavigateAction cannot be null");
 		}
@@ -48,32 +46,23 @@ public class TreeNavigator<M extends Map<String, Object>, L extends List<Object>
 		return this;
 	}
 
-	public TreeNavigator(NavigateAction action, String... pathsToNavigate)
-	{
+	public TreeNavigator(NavigateAction action, String... pathsToNavigate) {
 		this(action, Arrays.asList(pathsToNavigate));
 	}
 
-	public void nav(M object) throws Exception
-	{
-		if (action.start(object, pathsToNavigate))
-		{
-			for (String path: pathsToNavigate)
-			{
-				try
-				{
-					if (path != null && !path.equals("") && action.pathStart(path))
-					{
+	public void nav(M object) throws Exception {
+		if (action.start(object, pathsToNavigate)) {
+			for (String path : pathsToNavigate) {
+				try {
+					if (path != null && !path.equals("") && action.pathStart(path)) {
 						TreePath jp = new TreePath(path, new DotDelimiter().withAcceptDelimiterInNodeName(true));
 						nav(jp, object);
 						action.pathEnd(path);
 					}
-				}
-				catch (Exception e)
-				{
-					if (action.failSilently(path ,e)) {
+				} catch (Exception e) {
+					if (action.failSilently(path, e)) {
 						break;
-					}
-					else if (action.failFast(path, e)) {
+					} else if (action.failFast(path, e)) {
 						throw e;
 					}
 				}
@@ -82,75 +71,53 @@ public class TreeNavigator<M extends Map<String, Object>, L extends List<Object>
 		action.end();
 	}
 
-	public void nav(TreePath jp, M map)
-	{
-		if (map == null || !action.recurInto(jp, map))
-		{
+	public void nav(TreePath jp, M map) {
+		if (map == null || !action.recurInto(jp, map)) {
 			//source is null - navigation impossible
 			return;
 		}
 
-		if (jp.hasNext())
-		{
+		if (jp.hasNext()) {
 			String key = jp.next();
-			if (!map.containsKey(key))
-			{
+			if (!map.containsKey(key)) {
 				// cannot find next element of path in the source -
 				// the specified path does not exist in the source
 				action.pathTailNotFound(jp, map);
-			}
-			else if (map.get(key) instanceof Map)
-			{
+			} else if (map.get(key) instanceof Map) {
 				//reached Map type node - handle it and recur into it
 				nav(jp, (M) map.get(key));
-			}
-			else if (map.get(key) instanceof List)
-			{
+			} else if (map.get(key) instanceof List) {
 				//reached List type node - handle it and recur into it
 				nav(jp, (L) map.get(key));
-			}
-			else if (jp.hasNext())
-			{
+			} else if (jp.hasNext()) {
 				// reached leaf node (not a container) in source but specified path expects children -
 				// the specified path is illegal because it does not exist in the source.
 				action.foundLeafBeforePathEnd(jp, map.get(key));
-			}
-			else if (!jp.hasNext())
-			{
+			} else if (!jp.hasNext()) {
 				//reached leaf in source and specified path is also at leaf -> handle it
 				action.handleLeaf(jp, map.get(key));
-			}
-			else
-			{
+			} else {
 				throw new IllegalStateException("fatal: unreachable code reached at '" + jp.origin() + "'");
 			}
 		}
 		action.recurEnd(jp, (M) map);
 	}
 
-	public void nav(TreePath jp, L list)
-	{
-		if (list == null || !action.recurInto(jp, (L) list))
-		{
+	public void nav(TreePath jp, L list) {
+		if (list == null || !action.recurInto(jp, (L) list)) {
 			//list is null - navigation impossible
 			return;
 		}
 		int arrIndex = 0;
-		for (Object arrItem : list.toArray())
-		{
-			if (arrItem instanceof Map)
-			{
+		for (Object arrItem : list.toArray()) {
+			if (arrItem instanceof Map) {
 				// clone the path so that for each object in the array,
 				// the iterator continues from the same position in the path
 				TreePath jpClone = getClone(jp);
 				nav(jpClone, (M) arrItem);
-			}
-			else if (arrItem instanceof List)
-			{
+			} else if (arrItem instanceof List) {
 				nav(jp, (L) arrItem);
-			}
-			else if (!jp.hasNext())
-			{
+			} else if (!jp.hasNext()) {
 				//reached leaf - handle it
 				action.handleLeaf(jp, arrIndex, arrItem);
 			}
@@ -159,13 +126,10 @@ public class TreeNavigator<M extends Map<String, Object>, L extends List<Object>
 		action.recurEnd(jp, (L) list);
 	}
 
-	private TreePath getClone(TreePath jp)
-	{
-		try
-		{
+	private TreePath getClone(TreePath jp) {
+		try {
 			return jp.clone();
-		}
-		catch (CloneNotSupportedException e) {
+		} catch (CloneNotSupportedException e) {
 			throw new RuntimeException("failed to clone path", e);
 		}
 	}
