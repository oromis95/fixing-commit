@@ -25,8 +25,8 @@ import net.minidev.json.reader.JsonWriter;
  * A JSON object. Key value pairs are unordered. JSONObject supports
  * java.util.Map interface.
  * 
- * @author FangYidong<fangyidong@yahoo.com.cn>
- * @author Uriel Chemouni <uchemouni@gmail.com>
+ * @author FangYidong &lt;fangyidong@yahoo.com.cn&gt;
+ * @author Uriel Chemouni &lt;uchemouni@gmail.com&gt;
  */
 public class JSONObject extends HashMap<String, Object> implements JSONAware, JSONAwareEx, JSONStreamAwareEx {
 	private static final long serialVersionUID = -503443796854799292L;
@@ -113,7 +113,6 @@ public class JSONObject extends HashMap<String, Object> implements JSONAware, JS
 	/**
 	 * A Simple Helper cast an Object to an Number
 	 * 
-	 * @see net.minidev.json.parser.JSONParserBase#parseNumber(String s)
 	 * @return a Number or null
 	 */
 	public Number getAsNumber(String key) {
