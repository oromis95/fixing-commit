@@ -21,7 +21,7 @@ import java.io.IOException;
  * Beans that support customized output of JSON text to a writer shall implement
  * this interface.
  * 
- * @author FangYidong<fangyidong@yahoo.com.cn>
+ * @author FangYidong &lt;fangyidong@yahoo.com.cn&gt;
  */
 public interface JSONStreamAware {
 	/**
