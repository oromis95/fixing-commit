@@ -85,13 +85,15 @@ class JStylerObj {
 			if (s == null)
 				return false;
 			int len = s.length();
+			// protect empty String
 			if (len == 0)
 				return true;
-
+			
+			// protect trimable String
 			if (s.trim() != s)
 				return true;
 
-			// Digit like text must be protect
+			// json special char
 			char ch = s.charAt(0);
 			if (isSpecial(ch) || isUnicode(ch))
 				return true;
@@ -101,13 +103,15 @@ class JStylerObj {
 				if (isSpecialClose(ch) || isUnicode(ch))
 					return true;
 			}
-
+			// keyWord must be protect
 			if (isKeyword(s))
 				return true;
-
+			// Digit like text must be protect
 			ch = s.charAt(0);
+			// only test String if First Ch is a digit
 			if (ch >= '0' && ch <= '9' || ch == '-') {
 				int p = 1;
+				// skip first digits
 				for (; p < len; p++) {
 					ch = s.charAt(p);
 					if (ch < '0' || ch > '9')
@@ -116,41 +120,38 @@ class JStylerObj {
 				// int/long
 				if (p == len)
 					return true;
+				// Floating point
 				if (ch == '.') {
 					p++;
-					for (; p < len; p++) {
-						ch = s.charAt(p);
-						if (ch < '0' || ch > '9')
-							break;
-					}
 				}
-				if (p == len)
-					return true; // can be read as an number
-
-				if (ch != 'E' || ch != 'e')
-					return false;
-				p++;
-				if (p == len)
-					return false;
-				ch = s.charAt(p);
-				if (ch == '+' || ch == '-') {
-					ch++;
-					if (p == len)
-						return false;
+				// Skip digits
+				for (; p < len; p++) {
 					ch = s.charAt(p);
+					if (ch < '0' || ch > '9')
+						break;
 				}
-
-				if (ch == '+' || ch == '-') {
-					ch++;
-					if (p == len)
+				if (p == len)
+					return true; // can be read as an floating number
+				// Double
+				if (ch == 'E' || ch == 'e') {
+					p++;
+					if (p == len) // no power data not a digits
 						return false;
+					ch = s.charAt(p);
+					if (ch == '+' || ch == '-')	{
+						p++;
+						ch = s.charAt(p);
+					}
 				}
-
+				if (p == len) // no power data => not a digit
+					return false;
+				
 				for (; p < len; p++) {
 					ch = s.charAt(p);
 					if (ch < '0' || ch > '9')
 						break;
 				}
+				// floating point With power of data.
 				if (p == len)
 					return true;
 				return false;
