@@ -19,8 +19,10 @@ public class TestDateConvert extends TestCase {
 		tests.add("23 janvier 2012 13:42:12");
 		tests.add("lundi 23 janvier 2012 13:42:12");
 		tests.add("2012-01-23 13:42:12");
-		tests.add("Thu Jan 23 13:42:12 PST 2012");
+		// need to use the same time Zone
+		// tests.add("Thu Jan 23 13:42:12 PST 2012");
 		//
+		// tests.add("Thu Jan 23 13:42:12 CET 2012");
 		ConvertDate.convertToDate(null);
 		for (String testDate : tests) {
 			String jobName = "Parsing FR Date:" + testDate;
