@@ -195,7 +195,7 @@ public abstract class XmlWriter
     /**
      * @param data Contents of the CDATA section to write out
 
-     * @return offset of the (first) illegal content segment ("]]>") in 
+     * @return offset of the (first) illegal content segment ("]]&gt;") in 
      *   passed content, if not in repairing mode; or -1 if none
      */
     public abstract int writeCData(String data)
