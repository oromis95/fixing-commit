@@ -243,8 +243,8 @@ public class AsyncByteBufferScanner
     /**
      * @return EVENT_INCOMPLETE, if there's not enough input to
      *   handle pending char, COMMENT, if we handled complete
-     *   "-->" end marker, or 0 to indicate something else
-     *   was succesfully handled.
+     *   "--&gt;" end marker, or 0 to indicate something else
+     *   was successfully handled.
      */
     protected int handleCommentPending() throws XMLStreamException
     {
@@ -423,7 +423,7 @@ public class AsyncByteBufferScanner
     /**
      * @return EVENT_INCOMPLETE, if there's not enough input to
      *   handle pending char, PROCESSING_INSTRUCTION, if we handled complete
-     *   "?>" end marker, or 0 to indicate something else
+     *   "?&gt;" end marker, or 0 to indicate something else
      *   was succesfully handled.
      */
     protected int handlePIPending() throws XMLStreamException
@@ -773,8 +773,8 @@ public class AsyncByteBufferScanner
     /**
      * @return EVENT_INCOMPLETE, if there's not enough input to
      *   handle pending char, CDATA, if we handled complete
-     *   "]]>" end marker, or 0 to indicate something else
-     *   was succesfully handled.
+     *   "]]&gt;" end marker, or 0 to indicate something else
+     *   was successfully handled.
      */
     protected final int handleCDataPending() throws XMLStreamException
     {
@@ -1596,7 +1596,7 @@ public class AsyncByteBufferScanner
     }
 
     /**
-     * Method called when '<' and (what appears to be) a name
+     * Method called when {@code '<'} and (what appears to be) a name
      * start character have been seen.
      */
     @Override
@@ -4070,7 +4070,7 @@ public class AsyncByteBufferScanner
     }
 
     /**
-     * @return Character value <b>minus 0x10000</c>; this so that caller
+     * @return Character value <b>minus 0x10000</b>; this so that caller
      *    can readily expand it to actual surrogates
      */
     protected final int decodeUtf8_4(int c1, int c2, int c3, int c4) throws XMLStreamException
