@@ -32,7 +32,7 @@ public class TestPIParsing extends AsyncTestBase
     
     private void _testPI(String spaces, int chunkSize) throws Exception
     {
-        String XML = spaces+"<?p    i ?><root><?pi \nwith\r\ndata??><?x \nfoo>bar? ?></root><?proc    \r?>";
+        String XML = spaces+"<?p    i ?><root><?pi \nwith\r\ndata??><?x \nfoo> "+UNICODE_SEGMENT+" bar? ?></root><?proc    \r?>";
         AsyncXMLInputFactory f = new InputFactoryImpl();
         AsyncXMLStreamReader sr = f.createAsyncXMLStreamReader();
         AsyncReaderWrapper reader = new AsyncReaderWrapper(sr, chunkSize, XML);
@@ -47,7 +47,7 @@ public class TestPIParsing extends AsyncTestBase
         assertEquals("with\ndata?", sr.getPIData());
         assertTokenType(PROCESSING_INSTRUCTION, reader.nextToken());
         assertEquals("x", sr.getPITarget());
-        assertEquals("foo>bar? ", sr.getPIData());
+        assertEquals("foo> "+UNICODE_SEGMENT+" bar? ", sr.getPIData());
         assertTokenType(END_ELEMENT, reader.nextToken());
         assertEquals("root", sr.getLocalName());
         assertTokenType(PROCESSING_INSTRUCTION, reader.nextToken());
