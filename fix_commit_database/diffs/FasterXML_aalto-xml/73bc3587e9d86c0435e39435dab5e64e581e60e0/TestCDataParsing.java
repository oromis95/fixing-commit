@@ -32,7 +32,7 @@ public class TestCDataParsing extends AsyncTestBase
         AsyncXMLInputFactory f = new InputFactoryImpl();
         AsyncXMLStreamReader sr = f.createAsyncXMLStreamReader();
         AsyncReaderWrapper reader = new AsyncReaderWrapper(sr, chunkSize, 
-            SPC+"<root><![CDATA[cdata\r\n&] ]] stuff]]>...<![CDATA[this\r\r and that!</root>");
+            SPC+"<root><![CDATA[cdata\r\n&] ]] stuff]]>...<![CDATA[this\r\r and that!]]></root>");
 
         int t = _verifyStart(reader);
         assertTokenType(START_ELEMENT, t);
@@ -47,9 +47,11 @@ public class TestCDataParsing extends AsyncTestBase
 
         assertTokenType(XMLStreamConstants.CHARACTERS, reader.currentToken());
         str = collectAsyncText(reader, CHARACTERS);
+        assertEquals("...", str);
 
         assertTokenType(XMLStreamConstants.CDATA, reader.currentToken());
         str = collectAsyncText(reader, CDATA);
+        assertEquals("this\n\n and that!", str);
         
         assertTokenType(XMLStreamConstants.END_ELEMENT, reader.currentToken());
         assertEquals("root", sr.getLocalName());
