@@ -1830,7 +1830,7 @@ public class AsyncByteBufferScanner
                 }
                 {
                     char[] buf = _textBuilder.resetWithEmpty();
-                    if (_inputPtr >= _inputEnd || !parseDtdId(buf, 0)) {
+                    if (_inputPtr >= _inputEnd || !parseDtdId(buf, 0, false)) {
                         _state = STATE_DTD_PUBLIC_ID;
                         break;
                     }
@@ -1840,7 +1840,7 @@ public class AsyncByteBufferScanner
                 continue main_loop;
     
             case STATE_DTD_PUBLIC_ID: 
-                if (!parseDtdId(_textBuilder.getBufferWithoutReset(), _textBuilder.getCurrentLength())) {
+                if (!parseDtdId(_textBuilder.getBufferWithoutReset(), _textBuilder.getCurrentLength(), false)) {
                     break;
                 }
                 verifyAndSetPublicId();
@@ -1870,7 +1870,7 @@ public class AsyncByteBufferScanner
                 }
                 {
                     char[] buf = _textBuilder.resetWithEmpty();
-                    if (_inputPtr >= _inputEnd || !parseDtdId(buf, 0)) {
+                    if (_inputPtr >= _inputEnd || !parseDtdId(buf, 0, true)) {
                         _state = STATE_DTD_SYSTEM_ID;
                         break;
                     }
@@ -1880,7 +1880,7 @@ public class AsyncByteBufferScanner
                 continue main_loop;
 
             case STATE_DTD_SYSTEM_ID: 
-                if (!parseDtdId(_textBuilder.getBufferWithoutReset(), _textBuilder.getCurrentLength())) {
+                if (!parseDtdId(_textBuilder.getBufferWithoutReset(), _textBuilder.getCurrentLength(), true)) {
                     break;
                 }
                 verifyAndSetSystemId();
@@ -1933,7 +1933,7 @@ public class AsyncByteBufferScanner
         return _currToken;
     }
 
-    private final boolean parseDtdId(char[] outputBuffer, int outputPtr) throws XMLStreamException
+    private final boolean parseDtdId(char[] outputBuffer, int outputPtr, boolean system) throws XMLStreamException
     {
         final int quote = (int) _elemAttrQuote;
         while (_inputPtr < _inputEnd) {
@@ -1942,9 +1942,8 @@ public class AsyncByteBufferScanner
                 _textBuilder.setCurrentLength(outputPtr);
                 return true;
             }
-            // this is not exact check; but does work for all legal (valid) characters:
-            if (ch <= INT_SPACE || ch > 0x7E) {
-                reportPrologUnexpChar(true, decodeCharForError((byte) ch), " (not valid in PUBLIC or SYSTEM ID)");
+            if (!system && !validPublicIdChar(ch)) {
+                reportPrologUnexpChar(true, decodeCharForError((byte) ch), " (not valid in " + (system ? "SYSTEM" : "PUBLIC") + " ID)");
             }
             if (outputPtr >= outputBuffer.length) {
                 outputBuffer = _textBuilder.finishCurrentSegment();
