@@ -1,4 +1,4 @@
-package failing;
+package wstream;
 
 import java.io.ByteArrayOutputStream;
 
@@ -10,6 +10,9 @@ public class TestUTF8Surrogates extends BaseWriterTest
 {
     public void testWithKappas() throws Exception
     {
+        // !!! TODO: mark using Unicode escape, for better source compatibility
+        final String K = new StringBuilder().append("𝜅").toString();
+        
         //loop to find exactly at which point entity encoding kicks in.
 //        for (int j = 0; j < 1000; j++)
         final int j = 985;
@@ -19,7 +22,7 @@ public class TestUTF8Surrogates extends BaseWriterTest
             final String namespace = "http://example.org";
             StringBuilder kappas = new StringBuilder();
             for (int i = 0; i < (2000 + j); i++) {
-                kappas.append("𝜅");
+                kappas.append(K);
             }
             w.writeStartElement("", "ex", namespace);
             w.writeCharacters(kappas.toString());
@@ -31,7 +34,6 @@ public class TestUTF8Surrogates extends BaseWriterTest
             if (!exp.equals(act)) {
                 fail("Iteration "+j+" failed; exp length "+exp.length()+" vs actual "+act.length());
             }
-            else System.err.println("#"+j+" length: "+exp.length());
         }
     }
 }
