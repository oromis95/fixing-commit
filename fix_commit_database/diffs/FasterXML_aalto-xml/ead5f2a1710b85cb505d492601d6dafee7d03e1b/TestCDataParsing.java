@@ -51,28 +51,16 @@ public class TestCDataParsing extends AsyncTestBase
         final AsyncXMLInputFactory f = new InputFactoryImpl();
 
         //test for byte array
-        AsyncXMLStreamReader<AsyncByteArrayFeeder> sr_array = null;
-        try {
-            sr_array = f.createAsyncForByteArray();
-            final AsyncReaderWrapperForByteArray reader_array = new AsyncReaderWrapperForByteArray(sr_array, chunkSize, SPC + XML);
-            _testCData(sr_array, reader_array);
-        } finally {
-            if(sr_array != null) {
-                sr_array.close();
-            }
-        }
+        AsyncXMLStreamReader<AsyncByteArrayFeeder> sr_array = f.createAsyncForByteArray();
+        final AsyncReaderWrapperForByteArray reader_array = new AsyncReaderWrapperForByteArray(sr_array, chunkSize, SPC + XML);
+        _testCData(sr_array, reader_array);
+        sr_array.close();
 
         //test for byte buffer
-        AsyncXMLStreamReader<AsyncByteBufferFeeder> sr_buffer = null;
-        try {
-            sr_buffer = f.createAsyncForByteBuffer();
-            final AsyncReaderWrapperForByteBuffer reader_buffer = new AsyncReaderWrapperForByteBuffer(sr_buffer, chunkSize, SPC + XML);
-            _testCData(sr_buffer, reader_buffer);
-        } finally {
-            if(sr_buffer != null) {
-                sr_buffer.close();
-            }
-        }
+        AsyncXMLStreamReader<AsyncByteBufferFeeder> sr_buffer = f.createAsyncForByteBuffer();
+        final AsyncReaderWrapperForByteBuffer reader_buffer = new AsyncReaderWrapperForByteBuffer(sr_buffer, chunkSize, SPC + XML);
+        _testCData(sr_buffer, reader_buffer);
+        sr_buffer.close();
     }
 
     private void _testCData(final AsyncXMLStreamReader<?> sr, final AsyncReaderWrapper reader) throws Exception
