@@ -9,7 +9,7 @@ import com.fasterxml.aalto.stax.InputFactoryImpl;
 
 public class TestCDataParsing extends AsyncTestBase
 {
-    public void testCData() throws Exception
+    public void testCDataParse() throws Exception
     {
         // let's try with different chunking, addition (or not) of space
         for (int spaces = 0; spaces < 3; ++spaces) {
@@ -18,6 +18,22 @@ public class TestCDataParsing extends AsyncTestBase
             _testCData(2, SPC);
             _testCData(3, SPC);
             _testCData(5, SPC);
+            _testCData(11, SPC);
+            _testCData(999, SPC);
+        }
+    }
+
+    public void testCDataSkip() throws Exception
+    {
+        // let's try with different chunking, addition (or not) of space
+        for (int spaces = 0; spaces < 3; ++spaces) {
+            String SPC = "  ".substring(0, spaces);
+            _testCDataSkip(1, SPC);
+            _testCDataSkip(2, SPC);
+            _testCDataSkip(3, SPC);
+            _testCDataSkip(5, SPC);
+            _testCDataSkip(11, SPC);
+            _testCDataSkip(999, SPC);
         }
     }
     
@@ -27,12 +43,13 @@ public class TestCDataParsing extends AsyncTestBase
     /**********************************************************************
      */
 
+    private final static String XML = "<root><![CDATA[cdata\r\n&] ]] stuff]]>...<![CDATA[this\r\r and Unicode: "+UNICODE_SEGMENT+"!]]></root>";
+    
     private void _testCData(int chunkSize, String SPC) throws Exception
     {
         AsyncXMLInputFactory f = new InputFactoryImpl();
         AsyncXMLStreamReader sr = f.createAsyncXMLStreamReader();
-        AsyncReaderWrapper reader = new AsyncReaderWrapper(sr, chunkSize, 
-            SPC+"<root><![CDATA[cdata\r\n&] ]] stuff]]>...<![CDATA[this\r\r and Unicode: "+UNICODE_SEGMENT+"!]]></root>");
+        AsyncReaderWrapper reader = new AsyncReaderWrapper(sr, chunkSize, SPC + XML);
 
         int t = verifyStart(reader);
         assertTokenType(START_ELEMENT, t);
@@ -61,6 +78,30 @@ public class TestCDataParsing extends AsyncTestBase
         
         assertFalse(sr.hasNext());
     }
+
+    private void _testCDataSkip(int chunkSize, String SPC) throws Exception
+    {
+        AsyncXMLInputFactory f = new InputFactoryImpl();
+        AsyncXMLStreamReader sr = f.createAsyncXMLStreamReader();
+        AsyncReaderWrapper reader = new AsyncReaderWrapper(sr, chunkSize, SPC + XML);
+
+        int t = verifyStart(reader);
+        assertTokenType(START_ELEMENT, t);
+        assertEquals("root", sr.getLocalName());
+        assertEquals("", sr.getNamespaceURI());
+        assertEquals(0, sr.getAttributeCount());
+
+        // note: moved to next element by now, so:
+        assertTokenType(CDATA, reader.nextToken());
+        assertTokenType(XMLStreamConstants.CHARACTERS, reader.nextToken());
+        assertTokenType(XMLStreamConstants.CDATA, reader.nextToken());
+        assertTokenType(XMLStreamConstants.END_ELEMENT, reader.nextToken());
+        assertEquals("root", sr.getLocalName());
+        assertEquals("", sr.getNamespaceURI());
+        assertTokenType(XMLStreamConstants.END_DOCUMENT, reader.nextToken());
+        
+        assertFalse(sr.hasNext());
+    }
     
     /*
     /**********************************************************************
