@@ -9,49 +9,58 @@ import com.fasterxml.aalto.stax.InputFactoryImpl;
 
 public class BasicParsingTest extends AsyncTestBase
 {
-    public void testEmptyRoot() throws Exception
+    public void testRootElement() throws Exception
     {
         // let's try with different chunking, addition (or not) of space
-        _testEmptyRoot(1, false);
-        _testEmptyRoot(2, false);
-        _testEmptyRoot(3, false);
-        _testEmptyRoot(5, false);
-
-        _testEmptyRoot(1, true);
-        _testEmptyRoot(2, true);
-        _testEmptyRoot(3, true);
-        _testEmptyRoot(5, true);
+        for (int spaces = 0; spaces < 3; ++spaces) {
+            String SPC = "  ".substring(0, spaces);
+            _testEmptyRoot(1, SPC+"<root />");
+            _testEmptyRoot(1, SPC+"<root/>");
+            _testEmptyRoot(1, SPC+"<root></root>");
+            _testEmptyRoot(2, SPC+"<root />");
+            _testEmptyRoot(2, SPC+"<root/>");
+            _testEmptyRoot(2, SPC+"<root></root>");
+            _testEmptyRoot(3, SPC+"<root />");
+            _testEmptyRoot(3, SPC+"<root/>");
+            _testEmptyRoot(3, SPC+"<root></root>");
+            _testEmptyRoot(5, SPC+"<root />");
+            _testEmptyRoot(5, SPC+"<root/>");
+            _testEmptyRoot(5, SPC+"<root></root>");
+        }
     }
 
-    public void testRootNoContent() throws Exception
+    public void testComments() throws Exception
     {
-        // let's try with different chunking, addition (or not) of space
-        
-        _testRootNoContent(1);
-        _testRootNoContent(2);
-        _testRootNoContent(3);
-        _testRootNoContent(5);
+        for (int spaces = 0; spaces < 3; ++spaces) {
+            String SPC = "  ".substring(0, spaces);
+            _testComments(SPC, 1);
+            _testComments(SPC, 2);
+            _testComments(SPC, 3);
+            _testComments(SPC, 5);
+        }
     }
 
-    public void testSimple() throws Exception
+    public void testProcInstr() throws Exception
     {
-        _testSimple(1);
-        _testSimple(2);
-        _testSimple(3);
-        _testSimple(5);
+        for (int spaces = 0; spaces < 3; ++spaces) {
+            String SPC = "  ".substring(0, spaces);
+            _testPI(SPC, 1);
+            _testPI(SPC, 2);
+            _testPI(SPC, 3);
+            _testPI(SPC, 5);
+        }
     }
-
+    
     /*
     /**********************************************************************
     /* Secondary test methods
     /**********************************************************************
      */
 
-    private void _testEmptyRoot(int chunkSize, boolean addSpace) throws Exception
+    private void _testEmptyRoot(int chunkSize, String XML) throws Exception
     {
         AsyncXMLInputFactory f = new InputFactoryImpl();
         AsyncXMLStreamReader sr = f.createAsyncXMLStreamReader();
-        String XML = addSpace ? "<root />" : "<root/>";
         AsyncReaderWrapper reader = new AsyncReaderWrapper(sr, chunkSize, XML);
 
         // should start with START_DOCUMENT, but for now skip
@@ -67,26 +76,49 @@ public class BasicParsingTest extends AsyncTestBase
         assertFalse(sr.hasNext());
     }
 
-    private void _testRootNoContent(int chunkSize) throws Exception
+    private void _testComments(String spaces, int chunkSize) throws Exception
     {
+        String XML = spaces+"<!--comments&s\r\ntuf-fy>--><root><!----></root><!--\nHi - ho!->-->";
         AsyncXMLInputFactory f = new InputFactoryImpl();
         AsyncXMLStreamReader sr = f.createAsyncXMLStreamReader();
-        AsyncReaderWrapper reader = new AsyncReaderWrapper(sr, chunkSize, "<root  ></root>");
-
-        // should start with START_DOCUMENT, but for now skip
-        int t = reader.nextToken();
-        assertTokenType(START_ELEMENT, t);
+        AsyncReaderWrapper reader = new AsyncReaderWrapper(sr, chunkSize, XML);
+        int t = _verifyStart(reader);
+        assertTokenType(COMMENT, t);
+        assertEquals("comments&s\ntuf-fy>", sr.getText());
+        assertTokenType(START_ELEMENT, reader.nextToken());
         assertEquals("root", sr.getLocalName());
-        assertEquals("", sr.getNamespaceURI());
-        assertEquals(0, sr.getAttributeCount());
+        assertTokenType(COMMENT, reader.nextToken());
+        assertEquals("", sr.getText());
         assertTokenType(END_ELEMENT, reader.nextToken());
+        assertEquals("root", sr.getLocalName());
+        assertTokenType(COMMENT, reader.nextToken());
+        assertEquals("\nHi - ho!", sr.getText());
+        assertTokenType(END_DOCUMENT, reader.nextToken());
+    }
 
+    private void _testPI(String spaces, int chunkSize) throws Exception
+    {
+        String XML = spaces+"<?p    i ?><root><?pi \nwith\r\ndata? or not????></root><?proc    \r?>";
+        AsyncXMLInputFactory f = new InputFactoryImpl();
+        AsyncXMLStreamReader sr = f.createAsyncXMLStreamReader();
+        AsyncReaderWrapper reader = new AsyncReaderWrapper(sr, chunkSize, XML);
+        int t = _verifyStart(reader);
+        assertTokenType(PROCESSING_INSTRUCTION, t);
+        assertEquals("p", sr.getPITarget());
+        assertEquals("i ", sr.getPIData());
+        assertTokenType(START_ELEMENT, reader.nextToken());
         assertEquals("root", sr.getLocalName());
-        assertEquals("", sr.getNamespaceURI());
-        assertTokenType(XMLStreamConstants.END_DOCUMENT, reader.nextToken());
-        assertFalse(sr.hasNext());
+        assertTokenType(PROCESSING_INSTRUCTION, reader.nextToken());
+        assertEquals("pi", sr.getPITarget());
+        assertEquals("with\ndata? or not???", sr.getPIData());
+        assertTokenType(END_ELEMENT, reader.nextToken());
+        assertEquals("root", sr.getLocalName());
+        assertTokenType(PROCESSING_INSTRUCTION, reader.nextToken());
+        assertEquals("proc", sr.getPITarget());
+        assertEquals("", sr.getPIData());
+        assertTokenType(END_DOCUMENT, reader.nextToken());
     }
-    
+
     private void _testSimple(int chunkSize) throws Exception
     {
         AsyncXMLInputFactory f = new InputFactoryImpl();
@@ -130,6 +162,13 @@ public class BasicParsingTest extends AsyncTestBase
     /**********************************************************************
      */
 
+    private int _verifyStart(AsyncReaderWrapper reader) throws Exception
+    {
+        // !!! TODO: should not start with START_DOCUMENT; but should get it right away
+        int t = reader.nextToken();
+        return t;
+    }
+        
     protected String collectAsyncText(AsyncReaderWrapper reader, int tt) throws XMLStreamException
     {
         StringBuilder sb = new StringBuilder();
