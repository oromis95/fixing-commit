@@ -83,7 +83,7 @@ public abstract class SingleByteXmlWriter
             if (ch <= SURR2_LAST) { // yes, outside of BMP
                 // Do we have second part?
                 if (inputOffset >= inputLen) { // nope... have to note down
-                    mSurrogate = ch;
+                    _surrogate = ch;
                 } else {
                     int ch2 = cbuf[inputOffset++];
                     outputSurrogates(ch, ch2);
