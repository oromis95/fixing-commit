@@ -3,7 +3,7 @@ package com.github.jhonnymertz.wkhtmltopdf.wrapper.page;
 public class Page {
 
     private String source;
-
+    private String filePath;
     private PageType type;
 
     public Page(String source, PageType type) {
@@ -26,4 +26,12 @@ public class Page {
     public void setType(PageType type) {
         this.type = type;
     }
+
+    public void setFilePath(String filePath) {
+        this.filePath = filePath;
+    }
+
+    public String getFilePath() {
+        return filePath;
+    }
 }
