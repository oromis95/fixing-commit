@@ -34,35 +34,35 @@ import static org.junit.Assert.*;
 public class ExpressionParserTest {
 
     @Test
-    public void shouldParseEqualComparisonExpression() {
+    public void shouldParseEqualComparisonRange() {
         ExpressionParser parser = new ExpressionParser(new Lexer());
         Expression eq = parser.parse("=1.0.0");
         assertTrue(eq.interpret(Version.valueOf("1.0.0")));
     }
 
     @Test
-    public void shouldParseEqualComparisonExpressionIfOnlyVersionGiven() {
+    public void shouldParseEqualComparisonRangeIfOnlyFullVersionGiven() {
         ExpressionParser parser = new ExpressionParser(new Lexer());
         Expression eq = parser.parse("1.0.0");
         assertTrue(eq.interpret(Version.valueOf("1.0.0")));
     }
 
     @Test
-    public void shouldParseNotEqualComparisonExpression() {
+    public void shouldParseNotEqualComparisonRange() {
         ExpressionParser parser = new ExpressionParser(new Lexer());
         Expression ne = parser.parse("!=1.0.0");
         assertTrue(ne.interpret(Version.valueOf("1.2.3")));
     }
 
     @Test
-    public void shouldParseGreaterComparisonExpression() {
+    public void shouldParseGreaterComparisonRange() {
         ExpressionParser parser = new ExpressionParser(new Lexer());
         Expression gt = parser.parse(">1.0.0");
         assertTrue(gt.interpret(Version.valueOf("1.2.3")));
     }
 
     @Test
-    public void shouldParseGreaterOrEqualComparisonExpression() {
+    public void shouldParseGreaterOrEqualComparisonRange() {
         ExpressionParser parser = new ExpressionParser(new Lexer());
         Expression ge = parser.parse(">=1.0.0");
         assertTrue(ge.interpret(Version.valueOf("1.0.0")));
@@ -70,14 +70,14 @@ public class ExpressionParserTest {
     }
 
     @Test
-    public void shouldParseLessComparisonExpression() {
+    public void shouldParseLessComparisonRange() {
         ExpressionParser parser = new ExpressionParser(new Lexer());
         Expression lt = parser.parse("<1.2.3");
         assertTrue(lt.interpret(Version.valueOf("1.0.0")));
     }
 
     @Test
-    public void shouldParseLessOrEqualComparisonExpression() {
+    public void shouldParseLessOrEqualComparisonRange() {
         ExpressionParser parser = new ExpressionParser(new Lexer());
         Expression le = parser.parse("<=1.2.3");
         assertTrue(le.interpret(Version.valueOf("1.0.0")));
@@ -85,7 +85,7 @@ public class ExpressionParserTest {
     }
 
     @Test
-    public void shouldParseTildeExpression() {
+    public void shouldParseTildeRange() {
         ExpressionParser parser = new ExpressionParser(new Lexer());
         Expression expr1 = parser.parse("~1");
         assertTrue(expr1.interpret(Version.valueOf("1.2.3")));
@@ -99,27 +99,43 @@ public class ExpressionParserTest {
     }
 
     @Test
-    public void shouldParseShortFormOfVersion() {
+    public void shouldParseCaretRange() {
+        ExpressionParser parser = new ExpressionParser(new Lexer());
+        Expression expr1 = parser.parse("^1");
+        assertTrue(expr1.interpret(Version.valueOf("1.2.3")));
+        assertFalse(expr1.interpret(Version.valueOf("3.2.1")));
+        Expression expr2 = parser.parse("^0.2");
+        assertTrue(expr2.interpret(Version.valueOf("0.2.3")));
+        assertFalse(expr2.interpret(Version.valueOf("0.3.0")));
+        Expression expr3 = parser.parse("^0.0.3");
+        assertTrue(expr3.interpret(Version.valueOf("0.0.3")));
+        assertFalse(expr3.interpret(Version.valueOf("0.0.4")));
+    }
+
+    @Test
+    public void shouldParsePartialVersionRange() {
         ExpressionParser parser = new ExpressionParser(new Lexer());
         Expression expr1 = parser.parse("1");
-        assertTrue(expr1.interpret(Version.valueOf("1.0.0")));
+        assertTrue(expr1.interpret(Version.valueOf("1.2.3")));
         Expression expr2 = parser.parse("2.0");
-        assertTrue(expr2.interpret(Version.valueOf("2.0.0")));
+        assertTrue(expr2.interpret(Version.valueOf("2.0.9")));
     }
 
     @Test
-    public void shouldParseVersionExpression() {
+    public void shouldParseWildcardRange() {
         ExpressionParser parser = new ExpressionParser(new Lexer());
         Expression expr1 = parser.parse("1.*");
         assertTrue(expr1.interpret(Version.valueOf("1.2.3")));
         assertFalse(expr1.interpret(Version.valueOf("3.2.1")));
-        Expression expr2 = parser.parse("1.2.*");
+        Expression expr2 = parser.parse("1.2.x");
         assertTrue(expr2.interpret(Version.valueOf("1.2.3")));
         assertFalse(expr2.interpret(Version.valueOf("1.3.2")));
+        Expression expr3 = parser.parse("X");
+        assertTrue(expr3.interpret(Version.valueOf("1.2.3")));
     }
 
     @Test
-    public void shouldParseRangeExpression() {
+    public void shouldParseHyphenRange() {
         ExpressionParser parser = new ExpressionParser(new Lexer());
         Expression range = parser.parse("1.0.0 - 2.0.0");
         assertTrue(range.interpret(Version.valueOf("1.2.3")));
@@ -127,7 +143,7 @@ public class ExpressionParserTest {
     }
 
     @Test
-    public void shouldParseAndBooleanExpression() {
+    public void shouldParseMultipleRangesJoinedWithAnd() {
         ExpressionParser parser = new ExpressionParser(new Lexer());
         Expression and = parser.parse(">=1.0.0 & <2.0.0");
         assertTrue(and.interpret(Version.valueOf("1.2.3")));
@@ -135,7 +151,7 @@ public class ExpressionParserTest {
     }
 
     @Test
-    public void shouldParseOrBooleanExpression() {
+    public void shouldParseMultipleRangesJoinedWithOr() {
         ExpressionParser parser = new ExpressionParser(new Lexer());
         Expression or = parser.parse("1.* | =2.0.0");
         assertTrue(or.interpret(Version.valueOf("1.2.3")));
@@ -146,7 +162,7 @@ public class ExpressionParserTest {
     public void shouldParseParenthesizedExpression() {
         ExpressionParser parser = new ExpressionParser(new Lexer());
         Expression expr = parser.parse("(1)");
-        assertTrue(expr.interpret(Version.valueOf("1.0.0")));
+        assertTrue(expr.interpret(Version.valueOf("1.2.3")));
         assertFalse(expr.interpret(Version.valueOf("2.0.0")));
     }
 
@@ -154,7 +170,7 @@ public class ExpressionParserTest {
     public void shouldParseExpressionWithMultipleParentheses() {
         ExpressionParser parser = new ExpressionParser(new Lexer());
         Expression expr = parser.parse("((1))");
-        assertTrue(expr.interpret(Version.valueOf("1.0.0")));
+        assertTrue(expr.interpret(Version.valueOf("1.2.3")));
         assertFalse(expr.interpret(Version.valueOf("2.0.0")));
     }
 
@@ -163,7 +179,7 @@ public class ExpressionParserTest {
         ExpressionParser parser = new ExpressionParser(new Lexer());
         Expression not1 = parser.parse("!(1)");
         assertTrue(not1.interpret(Version.valueOf("2.0.0")));
-        assertFalse(not1.interpret(Version.valueOf("1.0.0")));
+        assertFalse(not1.interpret(Version.valueOf("1.2.3")));
         Expression not2 = parser.parse("0.* & !(>=1 & <2)");
         assertTrue(not2.interpret(Version.valueOf("0.5.0")));
         assertFalse(not2.interpret(Version.valueOf("1.0.1")));
@@ -184,98 +200,8 @@ public class ExpressionParserTest {
     @Test
     public void shouldParseComplexExpressions() {
         ExpressionParser parser = new ExpressionParser(new Lexer());
-        Expression expr = parser.parse(
-            "((>=1.0.1 & <2) | (>=3.0 & <4)) & ((1-1.5) & (~1.5))"
-        );
+        Expression expr = parser.parse("((>=1.0.1 & <2) | (>=3.0 & <4)) & ((1-1.5) & (~1.5))");
         assertTrue(expr.interpret(Version.valueOf("1.5.0")));
         assertFalse(expr.interpret(Version.valueOf("2.5.0")));
     }
-
-    @Test
-    public void shouldRaiseErrorIfClosingParenthesisIsMissing() {
-        ExpressionParser parser = new ExpressionParser(new Lexer());
-        try {
-            parser.parse("((>=1.0.1 & < 2)");
-        } catch (UnexpectedTokenException e) {
-            return;
-        }
-        fail("Should raise error if closing parenthesis is missing");
-    }
-
-    @Test
-    public void shouldCheckIfPrereleaseVersionSatisfiesExpression() {
-        Version v = Version.valueOf("2.1.0-beta");
-        assertTrue(v.satisfies("*")); // >=0.0.0
-        assertTrue(v.satisfies("x")); // >=0.0.0
-        assertTrue(v.satisfies("X")); // >=0.0.0
-        assertTrue(v.satisfies("2.*")); // >=2.0.0 <3.0.0
-        assertTrue(v.satisfies("2.x")); // >=2.0.0 <3.0.0
-        assertTrue(v.satisfies("2.X")); // >=2.0.0 <3.0.0
-        assertTrue(v.satisfies("2.0.*")); // >=2.0.0 <2.1.0
-        assertTrue(v.satisfies("2.0.x")); // >=2.0.0 <2.1.0
-        assertTrue(v.satisfies("2.0.X")); // >=2.0.0 <2.1.0
-        assertTrue(v.satisfies("2")); // >=2.0.0 <3.0.0
-        assertTrue(v.satisfies("2.0")); // >=2.0.0 <2.1.0
-        assertTrue(v.satisfies("2.0.0")); // >=2.0.0
-        assertTrue(v.satisfies("~2")); // >=2.0.0 <3.0.0
-        assertTrue(v.satisfies("~2.0")); // >=2.0.0 <2.1.0
-        assertTrue(v.satisfies("~2.0.0")); // >=2.0.0 <2.1.0
-        assertTrue(v.satisfies(">=2.0 & <3.0")); // >=2.0.0 & <3.0.0
-        assertTrue(v.satisfies("2.0.0 - 2.1.0")); // >=2.0.0 & <=2.1.0
-        assertTrue(v.satisfies("2.0 - 2.1")); // >=2.0.0 & <=2.1.0
-        assertTrue(v.satisfies("^2")); // >=2.0.0 <3.0.0
-        assertTrue(v.satisfies("^2.0.3")); // >=2.0.3 <3.0.0
-        assertFalse(v.satisfies("2.1.*")); // >=2.1.0 <2.2.0
-        assertFalse(v.satisfies("2.1.x")); // >=2.1.0 <2.2.0
-        assertFalse(v.satisfies("2.1.X")); // >=2.1.0 <2.2.0
-        assertFalse(v.satisfies("~1")); // >=1.0.0 <2.0.0
-        assertFalse(v.satisfies("~1.2.3")); // >=1.2.3 <1.3.0
-        assertFalse(v.satisfies("~1.2")); // >=1.2.0 <1.3.0
-        assertFalse(v.satisfies("~2.2.3")); // >=2.2.3 <2.3.0
-        assertFalse(v.satisfies("^0.2.3")); // >=0.2.3 <0.3.0
-        assertFalse(v.satisfies("^0.0.3")); // >=0.0.3 <0.0.4
-        assertFalse(v.satisfies("^0")); // >=0.0.0 <0.1.0
-        assertFalse(v.satisfies("^0.0")); // >=0.0.0 <0.1.0
-        assertFalse(v.satisfies("^0.0.0")); // >=0.0.0 <0.0.0
-        assertFalse(v.satisfies(">=1.0 & <2.0")); // >=1.0.0 & <2.0.0
-        assertFalse(v.satisfies("1 - 2")); // >=1.0.0 & <2.0.0
-    }
-
-    @Test
-    public void shouldCheckIfStableVersionSatisfiesExpression() {
-        Version v = Version.valueOf("2.1.0");
-        assertTrue(v.satisfies("*")); // >=0.0.0
-        assertTrue(v.satisfies("x")); // >=0.0.0
-        assertTrue(v.satisfies("X")); // >=0.0.0
-        assertTrue(v.satisfies("2.*")); // >=2.0.0 <3.0.0
-        assertTrue(v.satisfies("2.x")); // >=2.0.0 <3.0.0
-        assertTrue(v.satisfies("2.X")); // >=2.0.0 <3.0.0
-        assertTrue(v.satisfies("2.1.*")); // >=2.1.0 <2.2.0
-        assertTrue(v.satisfies("2.1.x")); // >=2.1.0 <2.2.0
-        assertTrue(v.satisfies("2.1.X")); // >=2.1.0 <2.2.0
-        assertTrue(v.satisfies("2")); // >=2.0.0 <3.0.0
-        assertTrue(v.satisfies("2.0.0")); // >=2.0.0
-        assertTrue(v.satisfies("~2")); // >=2.0.0 <3.0.0
-        assertTrue(v.satisfies(">=2.0 & <3.0")); // >=2.0.0 & <3.0.0
-        assertTrue(v.satisfies("2.0.0 - 2.1.0")); // >=2.0.0 & <=2.1.0
-        assertTrue(v.satisfies("2.0 - 2.1")); // >=2.0.0 & <=2.1.0
-        assertTrue(v.satisfies("^2")); // >=2.0.0 <3.0.0
-        assertTrue(v.satisfies("^2.0")); // >=2.0.0 <3.0.0
-        assertTrue(v.satisfies("^2.0.3")); // >=2.0.3 <3.0.0
-        assertFalse(v.satisfies("2.0")); // >=2.0.0 <2.1.0
-        assertFalse(v.satisfies("2.0.*")); // >=2.0.0 <2.1.0
-        assertFalse(v.satisfies("~2.0.0")); // >=2.0.0 <2.1.0
-        assertFalse(v.satisfies("~2.0")); // >=2.0.0 <2.1.0
-        assertFalse(v.satisfies("~1")); // >=1.0.0 <2.0.0
-        assertFalse(v.satisfies("~1.2.3")); // >=1.2.3 <1.3.0
-        assertFalse(v.satisfies("~1.2")); // >=1.2.0 <1.3.0
-        assertFalse(v.satisfies("~2.2.3")); // >=2.2.3 <2.3.0
-        assertFalse(v.satisfies("^0.2.3")); // >=0.2.3 <0.3.0
-        assertFalse(v.satisfies("^0.0.3")); // >=0.0.3 <0.0.4
-        assertFalse(v.satisfies("^0")); // >=0.0.0 <0.1.0
-        assertFalse(v.satisfies("^0.0")); // >=0.0.0 <0.1.0
-        assertFalse(v.satisfies("^0.0.0")); // >=0.0.0 <0.0.0
-        assertFalse(v.satisfies(">=1.0 & <2.0")); // >=1.0.0 & <2.0.0
-        assertFalse(v.satisfies("1 - 2")); // >=1.0.0 & <2.0.0
-    }
 }
