@@ -138,6 +138,11 @@ public abstract class Access<T> {
         return (Access<T>) CharSequenceAccess.charSequenceAccess(backingOrder);
     }
 
+    /**
+     * Constructor for use in subclasses.
+     */
+    protected Access() {}
+
     /**
      * Reads {@code [offset, offset + 7]} bytes of the byte sequence represented by the given
      * {@code input} as a single {@code long} value.
