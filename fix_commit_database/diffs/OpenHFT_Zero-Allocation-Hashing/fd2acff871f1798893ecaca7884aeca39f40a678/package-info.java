@@ -27,8 +27,20 @@
  *         {@linkplain net.openhft.hashing.LongHashFunction#xx_r39(long) with a seed}.
  *         </li>
  *         <li>
+ *         {@linkplain net.openhft.hashing.LongHashFunction#farmUo() FarmHash 1.1 (farmhashuo)
+ *         without seed}, {@linkplain net.openhft.hashing.LongHashFunction#farmUo(long) with one
+ *         seed} and {@linkplain net.openhft.hashing.LongHashFunction#farmUo(long, long) with
+ *         two seeds}.
+ *         </li>
+ *         <li>
+ *         {@linkplain net.openhft.hashing.LongHashFunction#farmNa() FarmHash 1.0 (farmhashna)
+ *         without seed}, {@linkplain net.openhft.hashing.LongHashFunction#farmNa(long) with one
+ *         seed} and {@linkplain net.openhft.hashing.LongHashFunction#farmNa(long, long) with
+ *         two seeds}.
+ *         </li>
+ *         <li>
  *         {@linkplain net.openhft.hashing.LongHashFunction#city_1_1() CityHash 1.1 without seeds},
- *         {@linkplain net.openhft.hashing.LongHashFunction#city_1_1(long) with one seed},
+ *         {@linkplain net.openhft.hashing.LongHashFunction#city_1_1(long) with one seed} and
  *         {@linkplain net.openhft.hashing.LongHashFunction#city_1_1(long, long) with two seeds}.
  *         </li>
  *         <li>
