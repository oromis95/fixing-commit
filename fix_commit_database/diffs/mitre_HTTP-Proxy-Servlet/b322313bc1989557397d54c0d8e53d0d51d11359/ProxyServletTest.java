@@ -121,7 +121,7 @@ public class ProxyServletTest
   //  user agents aren't supposed to send them. HttpComponents has behaved
   //  differently on sending them vs not sending them.
   private static String[] testUrlSuffixes = new String[]{
-          "","/pathInfo","?q=v","/p?q=v",
+          "","/pathInfo","/pathInfo/%23%25abc","?q=v","/p?q=v",
           "/p?query=note:Leitbild",//colon  Issue#4
           "/p?id=p%20i", "/p%20i" // encoded space in param then in path
   };
