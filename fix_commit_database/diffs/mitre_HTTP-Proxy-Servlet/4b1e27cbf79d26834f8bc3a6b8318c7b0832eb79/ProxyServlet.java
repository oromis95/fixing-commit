@@ -400,10 +400,8 @@ public class ProxyServlet extends HttpServlet {
     return theUrl;
   }
   
-  
-  public URI getTargetUri() {
-    return targetUri;
-  }
+  /** The target URI as configured. Not null. */
+  public String getTargetUri() { return targetUri; }
 
   /**
    * Encodes characters in the query or fragment part of the URI.
