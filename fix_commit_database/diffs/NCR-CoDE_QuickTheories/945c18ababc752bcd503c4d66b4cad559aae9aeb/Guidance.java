@@ -4,6 +4,9 @@ import java.util.Collection;
 
 import org.quicktheories.impl.Precursor;
 
+/**
+ * Provides hints for values to visit during search.
+ */
 public interface Guidance {
 
   void newExample(Precursor precursor);
