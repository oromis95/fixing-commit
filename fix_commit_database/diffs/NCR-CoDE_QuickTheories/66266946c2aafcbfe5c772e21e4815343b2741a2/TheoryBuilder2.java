@@ -112,7 +112,10 @@ public final class TheoryBuilder2<A, B> implements Subject2<A, B> {
   /**
    * Converts theory to one about a different type using the given function
    * retaining all precursor values
-   * 
+   *
+   * @param <T>
+   *          type to create theory about
+   *
    * @param mapping
    *          Function from types A and B to type T
    * @return a Subject3 relating to the state of a theory involving three values
@@ -124,7 +127,10 @@ public final class TheoryBuilder2<A, B> implements Subject2<A, B> {
   /**
    * Converts theory to one about a different type using the given function
    * retaining all precursor values
-   * 
+   *
+   * @param <T>
+   *          type to create theory about
+   *
    * @param mapping
    *          Function from types A and B to type T
    * @param typeToString
