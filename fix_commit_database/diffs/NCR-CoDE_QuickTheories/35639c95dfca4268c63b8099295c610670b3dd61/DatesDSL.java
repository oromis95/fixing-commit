@@ -13,7 +13,7 @@ public class DatesDSL {
   /**
    * Generates Dates inclusively bounded between January 1, 1970, 00:00:00 GMT
    * and new Date(milliSecondsFromEpoch). The Source restricts Date generation,
-   * so that no Dates before 1970 can be created. . The Source is weighted so it
+   * so that no Dates before 1970 can be created. The Source is weighted so it
    * is likely to produce new Date(millisecondsFromEpoch) one or more times.
    * 
    * @param millisecondsFromEpoch
@@ -72,8 +72,7 @@ public class DatesDSL {
   static class Dates {
 
     static Source<Date> withMilliSeconds(long milliSecondsFromEpoch) {
-      return Longs.range(0, milliSecondsFromEpoch).as(l -> new Date(l),
-          d -> d.getTime());
+      return withMilliSecondsBetween(0, milliSecondsFromEpoch);
     }
 
     static Source<Date> withMilliSecondsBetween(
