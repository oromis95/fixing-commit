@@ -21,6 +21,6 @@ package com.criteo.kafka;
 import kafka.metrics.KafkaMetricsReporterMBean;
 
 public interface KafkaGraphiteMetricsReporterMBean extends
-		KafkaMetricsReporterMBean {
+        KafkaMetricsReporterMBean {
 
 }
