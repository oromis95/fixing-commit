@@ -9,6 +9,7 @@ import org.slf4j.LoggerFactory;
 
 import java.time.Duration;
 import java.time.temporal.ChronoUnit;
+import java.util.Collection;
 import java.util.HashSet;
 import java.util.Set;
 import java.util.UUID;
@@ -133,8 +134,8 @@ public class CallExecutor<T> implements RetryExecutor<T, Status<T>> {
             T callResult = callable.call();
 
             boolean shouldRetryOnThisResult = config.shouldRetryOnValue() && (
-                            ( config.getValuesToExpect() != null && !isOneOfValuesToExpect( callResult ) )
-                                            || isOneOfValuesToRetryOn( callResult ) );
+                    (config.getValuesToExpect() != null && !isOneOfValuesToExpect(callResult))
+                            || isOneOfValuesToRetryOn(callResult));
             if (shouldRetryOnThisResult) {
                 attemptStatus.setSuccessful(false);
             } else {
@@ -154,11 +155,11 @@ public class CallExecutor<T> implements RetryExecutor<T, Status<T>> {
         return attemptStatus;
     }
 
-    private boolean isOneOfValuesToExpect( T callResult ) {
-        Object[] valuesToExpect = config.getValuesToExpect();
-        if ( valuesToExpect != null ) {
-            for ( Object o : valuesToExpect ) {
-                if ( o.equals( callResult ) ) {
+    private boolean isOneOfValuesToExpect(T callResult) {
+        Collection<Object> valuesToExpect = config.getValuesToExpect();
+        if (valuesToExpect != null) {
+            for (Object o : valuesToExpect) {
+                if (o.equals(callResult)) {
                     return true;
                 }
             }
@@ -166,13 +167,11 @@ public class CallExecutor<T> implements RetryExecutor<T, Status<T>> {
         return false;
     }
 
-    private boolean isOneOfValuesToRetryOn( T callResult )
-    {
-        if ( config.getValuesToRetryOn() != null ) {
-            for ( Object o : config.getValuesToRetryOn() )
-            {
-                if ( o.equals( callResult ) )
-                {
+    private boolean isOneOfValuesToRetryOn(T callResult) {
+        Collection<Object> valuesToRetryOn = config.getValuesToRetryOn();
+        if (valuesToRetryOn != null) {
+            for (Object o : valuesToRetryOn) {
+                if (o.equals(callResult)) {
                     return true;
                 }
             }
