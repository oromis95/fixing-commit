@@ -2,15 +2,13 @@ package com.evanlennick.retry4j;
 
 import java.time.Duration;
 
-public class CallResults<T> {
+public class CallResults<T> extends AttemptResults<T> {
 
     private long startTime;
     private long endTime;
     private String callName;
-    private boolean wasSuccessful;
     private int totalTries;
     private Duration totalElapsedDuration;
-    private T result;
     private Exception lastExceptionThatCausedRetry;
 
     public long getStartTime() {
@@ -37,14 +35,6 @@ public class CallResults<T> {
         this.callName = callName;
     }
 
-    public boolean wasSuccessful() {
-        return wasSuccessful;
-    }
-
-    public void setSuccessful(boolean wasSuccessful) {
-        this.wasSuccessful = wasSuccessful;
-    }
-
     public int getTotalTries() {
         return totalTries;
     }
@@ -53,14 +43,6 @@ public class CallResults<T> {
         this.totalTries = totalTries;
     }
 
-    public boolean isWasSuccessful() {
-        return wasSuccessful;
-    }
-
-    public void setWasSuccessful(boolean wasSuccessful) {
-        this.wasSuccessful = wasSuccessful;
-    }
-
     public Duration getTotalElapsedDuration() {
         return totalElapsedDuration;
     }
@@ -69,14 +51,6 @@ public class CallResults<T> {
         this.totalElapsedDuration = totalElapsedDuration;
     }
 
-    public T getResult() {
-        return result;
-    }
-
-    public void setResult(T result) {
-        this.result = result;
-    }
-
     public Exception getLastExceptionThatCausedRetry() {
         return lastExceptionThatCausedRetry;
     }
@@ -91,10 +65,10 @@ public class CallResults<T> {
         sb.append("startTime=").append(startTime);
         sb.append(", endTime=").append(endTime);
         sb.append(", callName='").append(callName).append('\'');
-        sb.append(", wasSuccessful=").append(wasSuccessful);
+        sb.append(", wasSuccessful=").append(wasSuccessful());
         sb.append(", totalTries=").append(totalTries);
         sb.append(", totalElapsedDuration=").append(totalElapsedDuration);
-        sb.append(", result=").append(result);
+        sb.append(", result=").append(getResult());
         sb.append(", lastExceptionThatCausedRetry=").append(lastExceptionThatCausedRetry);
         sb.append('}');
         return sb.toString();
