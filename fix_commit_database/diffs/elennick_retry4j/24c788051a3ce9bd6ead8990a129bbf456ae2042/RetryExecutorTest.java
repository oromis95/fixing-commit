@@ -187,7 +187,7 @@ public class RetryExecutorTest {
             new RetryExecutor(retryConfig).execute(callable);
         } catch (CallFailureException e) {
             RetryResults results = e.getRetryResults();
-            assertThat(results.isSucceeded());
+            assertThat(results.isSucceeded()).isFalse();
             assertThat(results.getCallName()).isNotEmpty();
             assertThat(results.getTotalDurationElapsed().toMillis()).isCloseTo(0, within(25L));
             assertThat(results.getTotalTries()).isEqualTo(5);
