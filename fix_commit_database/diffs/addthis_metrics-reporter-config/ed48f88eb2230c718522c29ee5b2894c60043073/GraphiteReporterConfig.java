@@ -14,19 +14,52 @@
 
 package com.addthis.metrics.reporter.config;
 
-import com.yammer.metrics.Metrics;
-
+import java.net.InetAddress;
+import java.net.UnknownHostException;
+import java.util.HashMap;
 import java.util.List;
+import java.util.Map;
 
 import javax.validation.constraints.NotNull;
 
+import org.apache.commons.lang3.StringUtils;
+import org.apache.commons.lang3.text.StrSubstitutor;
 import org.slf4j.Logger;
 import org.slf4j.LoggerFactory;
 
+import com.yammer.metrics.Metrics;
+
 public class GraphiteReporterConfig extends AbstractHostPortReporterConfig
 {
     private static final Logger log = LoggerFactory.getLogger(GraphiteReporterConfig.class);
 
+    public static final String MACRO_HOST_NAME = "host.name";
+    public static final String MACRO_HOST_ADDRESS = "host.address";
+    public static final String MACRO_HOST_FQDN = "host.fqdn";
+    public static final String MACRO_HOST_NAME_SHORT = "host.name.short";
+
+    private InetAddress localhost;
+    private String resolvedPrefix;
+
+    /**
+     * Test constructor
+     * 
+     * @param localhost
+     *            localhost
+     */
+    GraphiteReporterConfig(InetAddress localhost) {
+        this.localhost = localhost;
+    }
+
+    public GraphiteReporterConfig() {
+        try {
+            this.localhost = InetAddress.getLocalHost();
+        } catch (UnknownHostException e) {
+            // not expected to happen with properly configured system
+            log.error("Unable to get localhost", e);
+        }
+    }
+    
     @NotNull
     private String prefix = "";
 
@@ -35,9 +68,62 @@ public class GraphiteReporterConfig extends AbstractHostPortReporterConfig
         return prefix;
     }
 
-    public void setPrefix(String prefix)
-    {
+    /**
+     * <p>
+     * Sets the prefix to be prepended to all metric names. The prefix may
+     * contain the variable references in the following format: ${macro_name}.
+     * <p>
+     * The following macros are supported:
+     * <dl>
+     * <dt>{@link #MACRO_HOST_ADDRESS}</dt>
+     * <dd>The value returned for local host by
+     * {@link InetAddress#getHostAddress()}</dd>
+     * <dt>{@link #MACRO_HOST_NAME}</dt>
+     * <dd>The value returned for local host by
+     * {@link InetAddress#getHostName()}</dd>
+     * <dt>{@link #MACRO_HOST_NAME_SHORT}</dt>
+     * <dd>The value returned for local host by
+     * {@link InetAddress#getHostName()} up to first dot</dd>
+     * <dt>{@link #MACRO_HOST_FQDN}</dt>
+     * <dd>The value returned for local host by
+     * {@link InetAddress#getCanonicalHostName()}</dd>
+     * </dl>
+     * 
+     * <p>
+     * All substituted values are made metric-safe
+     * 
+     * @param prefix
+     *            prefix value
+     */
+    public void setPrefix(String prefix) {
         this.prefix = prefix;
+        this.resolvedPrefix = resolvePrefix(prefix);
+    }
+
+    private String resolvePrefix(String prefixTemplate) {
+        Map<String, String> valueMap = new HashMap<String, String>();
+        if (localhost != null) {
+            String hostname = localhost.getHostName();
+            valueMap.put(MACRO_HOST_NAME, sanitizeName(hostname));
+            if (!StringUtils.isEmpty(hostname)) {
+                valueMap.put(MACRO_HOST_NAME_SHORT,
+                        sanitizeName(StringUtils.split(hostname, '.')[0]));
+            }
+            valueMap.put(MACRO_HOST_ADDRESS,
+                    sanitizeName(localhost.getHostAddress()));
+            valueMap.put(MACRO_HOST_FQDN,
+                    sanitizeName(localhost.getCanonicalHostName()));
+        }
+
+        return StrSubstitutor.replace(prefixTemplate, valueMap);
+    }
+	
+    private String sanitizeName(String name) {
+        return name.replaceAll("[^a-zA-Z0-9_-]", "_");
+    }
+
+    String getResolvedPrefix() {
+        return this.resolvedPrefix;
     }
 
     @Override
@@ -67,7 +153,7 @@ public class GraphiteReporterConfig extends AbstractHostPortReporterConfig
             {
                 log.info("Enabling GraphiteReporter to {}:{}", new Object[] {hostPort.getHost(), hostPort.getPort()});
                 com.yammer.metrics.reporting.GraphiteReporter.enable(Metrics.defaultRegistry(), getPeriod(), getRealTimeunit(),
-                                                                     hostPort.getHost(), hostPort.getPort(), prefix, getMetricPredicate());
+                                                                     hostPort.getHost(), hostPort.getPort(), resolvedPrefix, getMetricPredicate());
 
             }
             catch (Exception e)
