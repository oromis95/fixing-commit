@@ -14,6 +14,7 @@ import java.io.IOException;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
+import java.util.concurrent.TimeUnit;
 
 /**
  * Defines permutations for a host connection.
@@ -26,14 +27,14 @@ public class Connection implements NotificationListener {
 	String password;
 	String connectionInstancePrefix;
 	List<String> beanAliases;
+	private int connectBackoff;
 
 	private NotificationListener notificationListener;
 	private JMXConnector serverConnector;
 	private MBeanServerConnection serverConnection;
 
 	public Connection(final NotificationListener notificationListener, final String rawUrl, final String hostname, final JMXServiceURL serviceURL, final String username,
-	                  final String password, final String connectionInstancePrefix, final List<String> beanAliases)
-	{
+	                  final String password, final String connectionInstancePrefix, final List<String> beanAliases) {
 		this.notificationListener = notificationListener;
 		this.rawUrl = rawUrl;
 		this.hostname = hostname;
@@ -43,6 +44,7 @@ public class Connection implements NotificationListener {
 		this.connectionInstancePrefix = connectionInstancePrefix;
 		this.beanAliases = beanAliases;
 
+		this.connectBackoff = 0;
 		this.serverConnector = null;
 		this.serverConnection = null;
 	}
@@ -53,11 +55,23 @@ public class Connection implements NotificationListener {
 
 	public void connect() {
 		if (!isConnected()) {
+			if (connectBackoff == 0) {
+				connectBackoff = 5;
+			} else {
+				Collectd.logNotice("FastJMX plugin: Attempting reconnect to: " + rawUrl + " in: " + connectBackoff + " seconds.");
+				try {
+					TimeUnit.SECONDS.sleep(connectBackoff);
+					connectBackoff *= connectBackoff / (connectBackoff / 2);
+				} catch (InterruptedException ie) {
+					Collectd.logNotice("FastJMX plugin: Reconnect to: " + rawUrl + " INTERRUPTED.");
+					return;
+				}
+			}
 
 			Map environment = new HashMap();
 
 			if (password != null && username != null) {
-				environment.put(JMXConnector.CREDENTIALS, new String[] {username, password});
+				environment.put(JMXConnector.CREDENTIALS, new String[]{username, password});
 
 			}
 			environment.put(JMXConnectorFactory.PROTOCOL_PROVIDER_CLASS_LOADER, this.getClass().getClassLoader());
@@ -72,7 +86,7 @@ public class Connection implements NotificationListener {
 				} catch (IOException ioe) {
 					serverConnector = null;
 					serverConnection = null;
-					Collectd.logWarning("FastJMX: Could not connect to : " + rawUrl + " exception message: " + ioe.getMessage());
+					Collectd.logWarning("FastJMX plugin: Could not connect to : " + rawUrl + " exception message: " + ioe.getMessage());
 				}
 			}
 
@@ -80,11 +94,12 @@ public class Connection implements NotificationListener {
 				try {
 					serverConnection = serverConnector.getMBeanServerConnection();
 					serverConnection.addNotificationListener(MBeanServerDelegate.DELEGATE_NAME, notificationListener, null, this);
+					connectBackoff = 0;
 				} catch (IOException ioe) {
-					Collectd.logWarning("FastJMX: Could not get mbeanServerConnection to: " + rawUrl + " exception message: " + ioe.getMessage());
+					Collectd.logWarning("FastJMX plugin: Could not get mbeanServerConnection to: " + rawUrl + " exception message: " + ioe.getMessage());
 					close();
 				} catch (InstanceNotFoundException infe) {
-					Collectd.logNotice("FastJMX: Could not register MBeanServerDelegate. I will not be able to detect newly deployed or undeployed beans at: " + rawUrl);
+					Collectd.logNotice("FastJMX plugin: Could not register MBeanServerDelegate. I will not be able to detect newly deployed or undeployed beans at: " + rawUrl);
 				}
 			}
 		}
@@ -95,20 +110,20 @@ public class Connection implements NotificationListener {
 	 * Removes all NofiticationListeners and closes the connections.
 	 */
 	public void close() {
-		Collectd.logInfo("FastJMX: Closing " + rawUrl);
+		Collectd.logInfo("FastJMX plugin: Closing " + rawUrl);
 		if (serverConnector != null) {
-			Collectd.logInfo("FastJMX: Removing connection listeners for " + rawUrl);
+			Collectd.logInfo("FastJMX plugin: Removing connection listeners for " + rawUrl);
 			try {
 				serverConnector.removeConnectionNotificationListener(notificationListener);
 				serverConnector.removeConnectionNotificationListener(this);
 			} catch (ListenerNotFoundException lnfe) {
-				Collectd.logDebug("FastJMX: Couldn't unregister ourselves from our JMXConnector.");
+				Collectd.logDebug("FastJMX plugin: Couldn't unregister ourselves from our JMXConnector.");
 			}
 
 			try {
 				serverConnector.close();
 			} catch (IOException ioe) {
-				Collectd.logWarning("FastJMX: Exception closing JMXConnection: " + ioe.getMessage());
+				Collectd.logWarning("FastJMX plugin: Exception closing JMXConnection: " + ioe.getMessage());
 			}
 		}
 
@@ -147,12 +162,12 @@ public class Connection implements NotificationListener {
 		if (this == obj) {
 			return true;
 		} else if (obj instanceof Connection) {
-			Connection that = (Connection)obj;
+			Connection that = (Connection) obj;
 			return that.rawUrl.equals(this.rawUrl) &&
-			       this.username != null ? this.username.equals(that.username) : this.username == that.username &&
-				   this.password != null ? this.password.equals(that.password) : this.password == that.password &&
-				   this.hostname != null ? this.hostname.equals(that.hostname) : this.hostname == that.hostname &&
-				   this.connectionInstancePrefix != null ? this.connectionInstancePrefix.equals(that.connectionInstancePrefix) : this.connectionInstancePrefix == this.connectionInstancePrefix;
+					       this.username != null ? this.username.equals(that.username) : this.username == that.username &&
+							                                                                     this.password != null ? this.password.equals(that.password) : this.password == that.password &&
+									                                                                                                                                   this.hostname != null ? this.hostname.equals(that.hostname) : this.hostname == that.hostname &&
+											                                                                                                                                                                                                 this.connectionInstancePrefix != null ? this.connectionInstancePrefix.equals(that.connectionInstancePrefix) : this.connectionInstancePrefix == this.connectionInstancePrefix;
 		}
 		return false;
 	}
