@@ -15,6 +15,7 @@ import io.airlift.airline.Suggester;
 
 import javax.annotation.Nullable;
 import javax.inject.Inject;
+
 import java.lang.reflect.Field;
 import java.util.Collection;
 import java.util.List;
@@ -96,7 +97,6 @@ public class MetadataLoader
                 commandType);
 
         return commandMetadata;
-
     }
 
     public static SuggesterMetadata loadSuggester(Class<? extends Suggester> suggesterClass)
@@ -126,7 +126,8 @@ public class MetadataLoader
                             field.getType().equals(CommandGroupMetadata.class) ||
                             field.getType().equals(CommandMetadata.class)) {
                         injectionMetadata.metadataInjections.add(new Accessor(path));
-                    } else {
+                    }
+                    else {
                         loadInjectionMetadata(field.getType(), injectionMetadata, path);
                     }
                 }
