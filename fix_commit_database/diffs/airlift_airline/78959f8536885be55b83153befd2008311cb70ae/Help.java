@@ -5,13 +5,15 @@ import io.airlift.airline.model.CommandMetadata;
 import io.airlift.airline.model.GlobalMetadata;
 
 import javax.inject.Inject;
+
 import java.util.List;
 import java.util.concurrent.Callable;
 
 import static com.google.common.collect.Lists.newArrayList;
 
 @Command(name = "help", description = "Display help information")
-public class Help implements Runnable, Callable<Void>
+public class Help
+        implements Runnable, Callable<Void>
 {
     @Inject
     public GlobalMetadata global;
