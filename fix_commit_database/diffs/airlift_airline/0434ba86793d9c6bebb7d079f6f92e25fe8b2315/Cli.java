@@ -39,17 +39,22 @@ import static io.airlift.command.ParserUtil.createInstance;
 
 public class Cli<C>
 {
+    public static <T> CliBuilder<T> builder(String name)
+    {
+        Preconditions.checkNotNull(name, "name is null");
+        return new CliBuilder<T>(name);
+    }
 
+    @Deprecated
     public static CliBuilder<Object> buildCli(String name)
     {
-        Preconditions.checkNotNull(name, "name is null");
-        return new CliBuilder<Object>(name);
+        return builder(name);
     }
 
+    @Deprecated
     public static <T> CliBuilder<T> buildCli(String name, Class<T> commandTypes)
     {
-        Preconditions.checkNotNull(name, "name is null");
-        return new CliBuilder<T>(name);
+        return builder(name);
     }
 
     private final GlobalMetadata metadata;
