@@ -4,7 +4,7 @@ public class TestUtil
 {
     public static <T> Cli<T> singleCommandParser(Class<T> commandClass)
     {
-         return Cli.buildCli("parser", commandClass)
+         return Cli.<T>builder("parser")
                 .withCommand(commandClass)
                 .build();
     }
