@@ -227,6 +227,11 @@ public class ModbusTcpSlave {
             return request;
         }
 
+        @Override
+        public Channel getChannel() {
+            return channel;
+        }
+
         @Override
         public void sendResponse(Response response) {
             channel.writeAndFlush(new ModbusTcpPayload(transactionId, unitId, response));
