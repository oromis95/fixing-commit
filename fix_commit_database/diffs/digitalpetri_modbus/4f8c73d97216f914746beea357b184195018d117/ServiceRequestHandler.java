@@ -37,6 +37,7 @@ import com.digitalpetri.modbus.responses.WriteMultipleCoilsResponse;
 import com.digitalpetri.modbus.responses.WriteMultipleRegistersResponse;
 import com.digitalpetri.modbus.responses.WriteSingleCoilResponse;
 import com.digitalpetri.modbus.responses.WriteSingleRegisterResponse;
+import io.netty.channel.Channel;
 import io.netty.util.ReferenceCountUtil;
 
 public interface ServiceRequestHandler {
@@ -103,6 +104,11 @@ public interface ServiceRequestHandler {
          */
         Request getRequest();
 
+        /**
+         * @return the {@link Channel} this request was received on.
+         */
+        Channel getChannel();
+
         /**
          * Send a normal response.
          *
