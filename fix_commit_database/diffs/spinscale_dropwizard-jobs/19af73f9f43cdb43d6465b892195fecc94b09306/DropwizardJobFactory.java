@@ -1,11 +1,13 @@
 package de.spinscale.dropwizard.jobs;
 
-import org.quartz.Job;
+import java.util.Objects;
 import org.quartz.JobDetail;
+import org.quartz.JobKey;
 import org.quartz.Scheduler;
 import org.quartz.SchedulerException;
 import org.quartz.spi.JobFactory;
 import org.quartz.spi.TriggerFiredBundle;
+import org.quartz.utils.Key;
 
 class DropwizardJobFactory implements JobFactory {
 
@@ -18,11 +20,18 @@ class DropwizardJobFactory implements JobFactory {
     @Override
     public Job newJob(TriggerFiredBundle bundle, Scheduler scheduler) throws SchedulerException {
         JobDetail jobDetail = bundle.getJobDetail();
+        JobKey jobKey = jobDetail.getKey();
+
         for (Job job : jobs) {
-            if (job.getClass().equals(jobDetail.getJobClass())) {
+            if (job.getClass().equals(jobDetail.getJobClass()) && equalGroupName(job, jobKey)) {
                 return job;
             }
         }
         return null;
     }
+
+    private boolean equalGroupName(final Job job, final JobKey quartzJobKey) {
+        return Key.DEFAULT_GROUP.equals(quartzJobKey.getGroup()) && job.getGroupName() == null
+            || Objects.equals(job.getGroupName(), quartzJobKey.getGroup());
+    }
 }
