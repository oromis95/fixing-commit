@@ -26,6 +26,7 @@ public @interface Every {
     /**
      * The name of this job. If not specified, the name of the job will default to the canonical name of the annotated
      * class
+     * @return the name of the job
      */
     String jobName() default "";
 
