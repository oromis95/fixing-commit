@@ -18,6 +18,7 @@ import java.util.concurrent.TimeUnit;
 import org.hamcrest.Matchers;
 import org.hamcrest.core.IsEqual;
 import org.junit.Test;
+import org.quartz.CronTrigger;
 import org.quartz.JobDetail;
 import org.quartz.JobKey;
 import org.quartz.SchedulerConfigException;
@@ -155,6 +156,20 @@ public class JobManagerTest {
         jobManager.stop();
     }
 
+    @Test
+    public void testJobsWithTimeZoneInOnAnnotation() throws Exception {
+        jobManager = new JobManager(new TestConfig(), new OnTestJobWithTimeZoneConfiguration(),
+            new OnTestJobWithDefaultConfiguration());
+        jobManager.start();
+
+        String jobName = OnTestJobWithTimeZoneConfiguration.class.getCanonicalName();
+        CronTrigger trigger = (CronTrigger) jobManager.scheduler.getTriggersOfJob(JobKey.jobKey(jobName)).get(0);
+
+        assertEquals("Europe/Stockholm", trigger.getTimeZone().getID());
+
+        jobManager.stop();
+    }
+
     @Test
     public void testJobsWithNonDefaultConfiguration() throws Exception {
         jobManager = new JobManager(new TestConfig(), new EveryTestJobWithNonDefaultConfiguration(),
@@ -252,6 +267,13 @@ public class JobManagerTest {
         }
     }
 
+    @On(value = "0 0 13 ? * MON", timeZone = "Europe/Stockholm")
+    class OnTestJobWithTimeZoneConfiguration extends AbstractJob {
+        public OnTestJobWithTimeZoneConfiguration() {
+            super(1);
+        }
+    }
+
     @Every(value = "10ms", requestRecovery = true, storeDurably = true, priority = 20, misfirePolicy = Every.MisfirePolicy.IGNORE_MISFIRES)
     class EveryTestJobWithNonDefaultConfiguration extends AbstractJob {
         public EveryTestJobWithNonDefaultConfiguration() {
