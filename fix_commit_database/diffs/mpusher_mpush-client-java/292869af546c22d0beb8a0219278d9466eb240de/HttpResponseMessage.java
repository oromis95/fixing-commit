@@ -3,7 +3,7 @@ package com.shinemo.mpush.message;
 import com.shinemo.mpush.api.connection.Connection;
 import com.shinemo.mpush.api.protocol.Packet;
 import com.shinemo.mpush.util.MPUtils;
-import com.shinemo.mpush.util.WrappedByteBuffer;
+import com.shinemo.mpush.util.ScalableBuffer;
 
 import java.nio.ByteBuffer;
 import java.util.HashMap;
@@ -31,7 +31,7 @@ public final class HttpResponseMessage extends ByteBufMessage {
     }
 
     @Override
-    public void encode(WrappedByteBuffer body) {
+    public void encode(ScalableBuffer body) {
         encodeInt(body, statusCode);
         encodeString(body, reasonPhrase);
         encodeString(body, MPUtils.headerToString(headers));
