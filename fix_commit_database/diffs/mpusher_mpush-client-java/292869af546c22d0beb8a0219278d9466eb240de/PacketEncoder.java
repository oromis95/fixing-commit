@@ -2,16 +2,14 @@ package com.shinemo.mpush.codec;
 
 import com.shinemo.mpush.api.protocol.Command;
 import com.shinemo.mpush.api.protocol.Packet;
-import com.shinemo.mpush.util.WrappedByteBuffer;
-
-import java.nio.ByteBuffer;
+import com.shinemo.mpush.util.ScalableBuffer;
 
 /**
  * Created by ohun on 2016/1/17.
  */
 public final class PacketEncoder {
 
-    public static void encode(Packet packet, WrappedByteBuffer out) {
+    public static void encode(Packet packet, ScalableBuffer out) {
 
         if (packet.cmd == Command.HEARTBEAT.cmd) {
             out.put(Packet.HB_PACKET_BYTE);
