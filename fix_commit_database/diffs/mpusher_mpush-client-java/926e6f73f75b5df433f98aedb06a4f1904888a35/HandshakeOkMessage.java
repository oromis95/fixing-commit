@@ -13,8 +13,6 @@ import java.util.Arrays;
  */
 public final class HandshakeOkMessage extends ByteBufMessage {
     public byte[] serverKey;
-    public String serverHost;
-    public long serverTime;
     public int heartbeat;
     public String sessionId;
     public long expireTime;
@@ -26,8 +24,6 @@ public final class HandshakeOkMessage extends ByteBufMessage {
     @Override
     public void decode(ByteBuffer body) {
         serverKey = decodeBytes(body);
-        serverHost = decodeString(body);
-        serverTime = decodeLong(body);
         heartbeat = decodeInt(body);
         sessionId = decodeString(body);
         expireTime = decodeLong(body);
@@ -36,8 +32,6 @@ public final class HandshakeOkMessage extends ByteBufMessage {
     @Override
     public void encode(ScalableBuffer body) {
         encodeBytes(body, serverKey);
-        encodeString(body, serverHost);
-        encodeLong(body, serverTime);
         encodeInt(body, heartbeat);
         encodeString(body, sessionId);
         encodeLong(body, expireTime);
@@ -52,16 +46,6 @@ public final class HandshakeOkMessage extends ByteBufMessage {
         return this;
     }
 
-    public HandshakeOkMessage setServerHost(String serverHost) {
-        this.serverHost = serverHost;
-        return this;
-    }
-
-    public HandshakeOkMessage setServerTime(long serverTime) {
-        this.serverTime = serverTime;
-        return this;
-    }
-
     public HandshakeOkMessage setHeartbeat(int heartbeat) {
         this.heartbeat = heartbeat;
         return this;
@@ -81,8 +65,6 @@ public final class HandshakeOkMessage extends ByteBufMessage {
     public String toString() {
         return "HandshakeOkMessage{" +
                 "serverKey=" + Arrays.toString(serverKey) +
-                ", serverHost='" + serverHost + '\'' +
-                ", serverTime=" + serverTime +
                 ", heartbeat=" + heartbeat +
                 ", sessionId='" + sessionId + '\'' +
                 ", expireTime=" + expireTime +
