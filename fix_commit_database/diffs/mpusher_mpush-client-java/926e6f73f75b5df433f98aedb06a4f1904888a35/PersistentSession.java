@@ -8,7 +8,6 @@ import com.shinemo.mpush.util.Strings;
  * Created by ohun on 2016/1/25.
  */
 public final class PersistentSession {
-    public String serverHost;
     public String sessionId;
     public long expireTime;
     public Cipher cipher;
@@ -18,21 +17,19 @@ public final class PersistentSession {
     }
 
     public static String encode(PersistentSession session) {
-        return session.serverHost
-                + "," + session.sessionId
+        return session.sessionId
                 + "," + session.expireTime
                 + "," + session.cipher.toString();
     }
 
     public static PersistentSession decode(String value) {
         String[] array = value.split(",");
-        if (array.length != 5) return null;
+        if (array.length != 4) return null;
         PersistentSession session = new PersistentSession();
-        session.serverHost = array[0];
-        session.sessionId = array[1];
-        session.expireTime = Strings.toLong(array[2], 0);
-        byte[] key = AesCipher.toArray(array[3]);
-        byte[] iv = AesCipher.toArray(array[4]);
+        session.sessionId = array[0];
+        session.expireTime = Strings.toLong(array[1], 0);
+        byte[] key = AesCipher.toArray(array[2]);
+        byte[] iv = AesCipher.toArray(array[3]);
         if (key == null || iv == null) return null;
         session.cipher = new AesCipher(key, iv);
         return session;
@@ -41,8 +38,7 @@ public final class PersistentSession {
     @Override
     public String toString() {
         return "PersistentSession{" +
-                "serverHost='" + serverHost + '\'' +
-                ", sessionId='" + sessionId + '\'' +
+                "sessionId='" + sessionId + '\'' +
                 ", expireTime=" + expireTime +
                 ", cipher=" + cipher +
                 '}';
