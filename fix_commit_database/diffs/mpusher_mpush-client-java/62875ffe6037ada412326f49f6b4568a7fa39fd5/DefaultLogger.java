@@ -24,28 +24,28 @@ public final class DefaultLogger implements Logger {
     @Override
     public void d(String s, Object... args) {
         if (enable) {
-            System.out.printf(format.format(new Date()) + " [D] " + TAG + s + "%n", args);
+            System.out.printf(format.format(new Date()) + " [D] " + TAG + s + '\n', args);
         }
     }
 
     @Override
     public void i(String s, Object... args) {
         if (enable) {
-            System.out.printf(format.format(new Date()) + " [I] " + TAG + s + "%n", args);
+            System.out.printf(format.format(new Date()) + " [I] " + TAG + s + '\n', args);
         }
     }
 
     @Override
     public void w(String s, Object... args) {
         if (enable) {
-            System.err.printf(format.format(new Date()) + " [W] " + TAG + s + "%n", args);
+            System.err.printf(format.format(new Date()) + " [W] " + TAG + s + '\n', args);
         }
     }
 
     @Override
     public void e(Throwable e, String s, Object... args) {
         if (enable) {
-            System.err.printf(format.format(new Date()) + " [E] " + TAG + s + "%n", args);
+            System.err.printf(format.format(new Date()) + " [E] " + TAG + s + '\n', args);
             e.printStackTrace();
         }
     }
