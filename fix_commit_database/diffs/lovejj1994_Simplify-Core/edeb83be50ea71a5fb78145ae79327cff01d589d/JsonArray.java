@@ -3,11 +3,12 @@ package cn.xxywithpq.json.parse;
 import cn.xxywithpq.Json;
 
 import java.util.ArrayList;
+import java.util.Objects;
 
 /**
  * Created by panqian on 2017/6/20.
  */
-public class JsonArray extends ArrayList<JsonObject> {
+public class JsonArray<T extends Object> extends ArrayList<T> {
 
     @Override
     public String toString() {
