@@ -12,28 +12,28 @@ import java.util.List;
  *
  * A test of a class A can be defined by declaring a public method. Example:
  *
- * <code>
+ * <pre>
  *   public class A {
- *     @Before public void setUp() {...}
- *     @After public void tearDown() {...}
+ *     &#064;Before public void setUp() {...}
+ *     &#064;After public void tearDown() {...}
  *
- *     @Test public void test1() {...}
- *     @Test public void test2() {...}
- *     @Test public void test3() {...}
+ *     &#064;Test public void test1() {...}
+ *     &#064;Test public void test2() {...}
+ *     &#064;Test public void test3() {...}
  *
  *     public class B {
- *       @Test public void test4() {...}
+ *       &#064;Test public void test4() {...}
  *
  *       public class C {
- *         @Test public void test5() {...}
+ *         &#064;Test public void test5() {...}
  *       }
  *     }
  *
  *     public class D {
- *       @Test public void test6() {...}
+ *       &#064;Test public void test6() {...}
  *     }
  *   }
- * </code>
+ * </pre>
  *
  * If the {@link #getChildren(TestClass)} method is called on the {@link TestClass} object of class A, the it will
  * return a {@link List} containing methods test1, test2 and test3. A call on the {@link TestClass} object of class B,
