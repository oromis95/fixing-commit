@@ -67,11 +67,19 @@ public class BankTest {
             }
 
             public class AfterInterestRateChangeContext {
+                private double oldInterestRate;
+
                 @Before
                 public void changeInterestRate() {
+                    oldInterestRate = Bank.currentInterestRate;
                     Bank.currentInterestRate = 3.25;
                 }
 
+                @After
+                public void resetInterestRate() {
+                    Bank.currentInterestRate = oldInterestRate;
+                }
+
                 @Test
                 public void shouldHaveOldInterestRate() throws Exception {
                     assertMoneyEquals("Interest rate is not fixed", 2.75, oldAccount.getInterestRate());
@@ -79,4 +87,4 @@ public class BankTest {
             }
         }
     }
-}
+}
\ No newline at end of file
