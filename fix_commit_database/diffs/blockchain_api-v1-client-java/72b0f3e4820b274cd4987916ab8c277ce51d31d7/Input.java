@@ -70,7 +70,7 @@ public class Input {
 
     @Override
     public int hashCode () {
-        int result = previousOutput.hashCode();
+        int result = previousOutput == null ? 1 : previousOutput.hashCode();
         result = 31 * result + (int) (sequence ^ (sequence >>> 32));
         result = 31 * result + scriptSignature.hashCode();
         return result;
