@@ -1,71 +1,50 @@
 package info.blockchain.api.blockexplorer;
 
-import java.util.ArrayList;
-import java.util.List;
-
-import com.google.gson.JsonElement;
 import com.google.gson.JsonObject;
 
 /**
- * TODO 
+ * This class contains data related to inventory messages that Blockchain.info received
+ * for an object.
  *
  */
 public class InventoryData
 {
 	private String hash;
+	private String type;
 	private long initialTime;
 	private long lastTime;
 	private String initialIP;
 	private int nConnected;
 	private int relayedCount;
 	private int relayedPercent;
-	private List<Owner> probableOwners;
-	private List<MiningNode> miningNodes;
 	
-	public InventoryData(String hash, long initialTime, long lastTime, String initialIP,
-			int nConnected, int relayedCount, int relayedPercent,
-			List<Owner> probableOwners, List<MiningNode> miningNodes)
+	public InventoryData(String hash, String type, long initialTime, long lastTime, String initialIP,
+			int nConnected, int relayedCount, int relayedPercent)
 	{
 		this.hash = hash;
+		this.type = type;
 		this.initialTime = initialTime;
 		this.lastTime = lastTime;
 		this.initialIP = initialIP;
 		this.nConnected = nConnected;
 		this.relayedCount = relayedCount;
 		this.relayedPercent = relayedPercent;
-		this.probableOwners = probableOwners;
-		this.miningNodes = miningNodes;
 	}
 
 	public InventoryData(JsonObject i)
 	{
 		this (	i.get("hash").getAsString(),
+				i.get("type").getAsString(),
 				i.get("initial_time").getAsLong(),
 				i.get("last_time").getAsLong(),
 				i.get("initial_ip").getAsString(),
 				i.get("nconnected").getAsInt(),
 				i.get("relayed_count").getAsInt(),
-				i.get("relayed_percent").getAsInt(),
-				new ArrayList<Owner>(),
-				new ArrayList<MiningNode>());
-		
-		for (JsonElement jsonOwner : i.get("probable_owners").getAsJsonArray())
-		{
-			JsonObject ownerObj = jsonOwner.getAsJsonObject();
-			probableOwners.add(new Owner(ownerObj.get("ip").getAsString(),
-					ownerObj.get("confidence").getAsInt()));
-		}
-		
-		for (JsonElement jsonNode : i.get("mining_nodes").getAsJsonArray())
-		{
-			JsonObject nodeObj = jsonNode.getAsJsonObject();
-			miningNodes.add(new MiningNode(nodeObj.get("link").getAsString(),
-					nodeObj.get("name").getAsString()));
-		}
+				i.get("relayed_percent").getAsInt());
 	}
 	
 	/**
-	 * @return Transaction hash
+	 * @return Object hash
 	 */
 	public String getHash()
 	{
@@ -73,7 +52,16 @@ public class InventoryData
 	}
 
 	/**
-	 * @return the initialTime
+	 * @return Object type
+	 */
+	public String getType()
+	{
+		return type;
+	}
+
+	/**
+	 * @return The time Blockchain.info first received an inventory message 
+	 * containing a hash for this transaction.
 	 */
 	public long getInitialTime()
 	{
@@ -81,7 +69,8 @@ public class InventoryData
 	}
 
 	/**
-	 * @return
+	 * @return The last time Blockchain.info received an inventory message 
+	 * containing a hash for this transaction.
 	 */
 	public long getLastTime()
 	{
@@ -89,7 +78,8 @@ public class InventoryData
 	}
 
 	/**
-	 * @return the initialIP
+	 * @return IP of the peer from which Blockchain.info first received an inventory message 
+	 * containing a hash for this transaction.
 	 */
 	public String getInitialIP()
 	{
@@ -97,7 +87,7 @@ public class InventoryData
 	}
 
 	/**
-	 * @return the nConnected
+	 * @return Number of nodes that Blockchain.info is currently connected to.
 	 */
 	public int getnConnected()
 	{
@@ -105,7 +95,8 @@ public class InventoryData
 	}
 
 	/**
-	 * @return the relayedCount
+	 * @return Number of nodes Blockchain.info received an inventory message containing 
+	 * a hash for this transaction from.
 	 */
 	public int getRelayedCount()
 	{
@@ -113,81 +104,11 @@ public class InventoryData
 	}
 
 	/**
-	 * @return the relayedPercent
+	 * @return Ratio of nodes that Blockchain.info received an inventory message 
+	 * containing a hash for this transaction from and the number of connected nodes.
 	 */
 	public int getRelayedPercent()
 	{
 		return relayedPercent;
 	}
-
-	/**
-	 * @return the probableOwners
-	 */
-	public List<Owner> getProbableOwners()
-	{
-		return probableOwners;
-	}
-
-	/**
-	 * @return the miningNodes
-	 */
-	public List<MiningNode> getMiningNodes()
-	{
-		return miningNodes;
-	}
-
-	public class Owner
-	{
-		private String ip;
-		private int confidence;
-		
-		public Owner(String ip, int confidence)
-		{
-			this.ip = ip;
-			this.confidence = confidence;
-		}
-		
-		/**
-		 * @return IP address of the potential owner
-		 */
-		public String getIp()
-		{
-			return ip;
-		}
-		/**
-		 * @return Confidence that the transaction belongs to this owner
-		 */
-		public int getConfidence()
-		{
-			return confidence;
-		}
-	}
-	
-	public class MiningNode
-	{
-		private String link;
-		private String name;
-
-		public MiningNode(String link, String name)
-		{
-			this.link = link;
-			this.name = name;
-		}
-
-		/**
-		 * @return Link to the mining pool
-		 */
-		public String getLink()
-		{
-			return link;
-		}
-
-		/**
-		 * @return Name of the mining pool
-		 */
-		public String getName()
-		{
-			return name;
-		}
-	}
 }
