@@ -1,12 +1,10 @@
 package info.blockchain.api.statistics;
 
-import info.blockchain.api.Util;
-
 import com.google.gson.JsonObject;
 import com.google.gson.JsonParser;
 
 /**
- * This class is used as a response object to the 'getStats' method in the 'Statistics' class
+ * This class is used as a response object to the 'get' method in the 'Statistics' class
  *
  */
 public class StatisticsResponse
@@ -17,14 +15,14 @@ public class StatisticsResponse
 	private double minersRevenueUSD;
 	private double marketPriceUSD;
 	private double estimatedTransactionVolumeUSD;
-	private double totalFeesBTC;
+	private long totalFeesBTC;
 	private long totalBTCSent;
-	private double estimatedBTCSent;
-	private double btcMined;
+	private long estimatedBTCSent;
+	private long btcMined;
 	private double difficulty;
 	private double minutesBetweenBlocks;
 	private double daysDestroyed;
-	private long nTx;
+	private long numberOfTransactions;
 	private double hashRate;
 	private long timestamp;
 	private long minedBlocks;
@@ -43,15 +41,14 @@ public class StatisticsResponse
 		this.minersRevenueUSD = s.get("miners_revenue_usd").getAsDouble();
 		this.marketPriceUSD = s.get("market_price_usd").getAsDouble();
 		this.estimatedTransactionVolumeUSD = s.get("estimated_transaction_volume_usd").getAsDouble();
-		this.totalFeesBTC = s.get("total_fees_btc").getAsDouble();
-		this.totalBTCSent = s.get("total_btc_sent").getAsLong() / Util.SATOSHI_IN_BTC;
-		this.estimatedBTCSent = s.get("estimated_btc_sent").getAsDouble();
-		this.btcMined = s.get("n_btc_mined").getAsDouble();
-		this.tradeVolumeUSD = s.get("trade_volume_usd").getAsDouble();
+		this.totalFeesBTC = s.get("total_fees_btc").getAsLong();
+		this.totalBTCSent = s.get("total_btc_sent").getAsLong();
+		this.estimatedBTCSent = s.get("estimated_btc_sent").getAsLong();
+		this.btcMined = s.get("n_btc_mined").getAsLong();
 		this.difficulty = s.get("difficulty").getAsDouble();
 		this.minutesBetweenBlocks = s.get("minutes_between_blocks").getAsDouble();
 		this.daysDestroyed = s.get("days_destroyed").getAsDouble();
-		this.nTx = s.get("n_tx").getAsLong();
+		this.numberOfTransactions = s.get("n_tx").getAsLong();
 		this.hashRate = s.get("hash_rate").getAsDouble();
 		this.timestamp = s.get("timestamp").getAsLong();
 		this.minedBlocks = s.get("n_blocks_mined").getAsLong();
@@ -104,30 +101,30 @@ public class StatisticsResponse
 		return estimatedTransactionVolumeUSD;
 	}
 	/**
-	 * @return Total fees in the past 24 hours
+	 * @return Total fees in the past 24 hours (in satoshi)
 	 */
-	public double getTotalFeesBTC()
+	public long getTotalFeesBTC()
 	{
 		return totalFeesBTC;
 	}
 	/**
-	 * @return Total BTC sent in the past 24 hours
+	 * @return Total BTC sent in the past 24 hours (in satoshi)
 	 */
 	public long getTotalBTCSent()
 	{
 		return totalBTCSent;
 	}
 	/**
-	 * @return Estimated BTC sent in the past 24 hours
+	 * @return Estimated BTC sent in the past 24 hours (in satoshi)
 	 */
-	public double getEstimatedBTCSent()
+	public long getEstimatedBTCSent()
 	{
 		return estimatedBTCSent;
 	}
 	/**
-	 * @return Number of BTC mined in the past 24 hours
+	 * @return Number of BTC mined in the past 24 hours (in satoshi)
 	 */
-	public double getBTCMined()
+	public long getBTCMined()
 	{
 		return btcMined;
 	}
@@ -155,9 +152,9 @@ public class StatisticsResponse
 	/**
 	 * @return Number of transactions in the past 24 hours
 	 */
-	public long getNTx()
+	public long getNumberOfTransactions()
 	{
-		return nTx;
+		return numberOfTransactions;
 	}
 	/**
 	 * @return Current hashrate in GH/s
@@ -167,7 +164,7 @@ public class StatisticsResponse
 		return hashRate;
 	}
 	/**
-	 * @return Timestamp of when the report was compiled (in ms)
+	 * @return Timestamp of when this report was compiled (in ms)
 	 */
 	public long getTimestamp()
 	{
@@ -195,14 +192,14 @@ public class StatisticsResponse
 		return totalBTC;
 	}
 	/**
-	 * @return Total number of blocks in existence
+	 * @return Total number of blocks in existence (in satoshi)
 	 */
 	public long getTotalBlocks()
 	{
 		return totalBlocks;
 	}
 	/**
-	 * @return The next block where the difficulty retarget will occur
+	 * @return The next block height where the difficulty retarget will occur
 	 */
 	public long getNextRetarget()
 	{
