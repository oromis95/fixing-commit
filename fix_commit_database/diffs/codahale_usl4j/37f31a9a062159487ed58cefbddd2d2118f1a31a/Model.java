@@ -270,7 +270,7 @@ public class Model {
     if (this == o) {
       return true;
     }
-    if (o == null || getClass() != o.getClass()) {
+    if (!(o instanceof Model)) {
       return false;
     }
     final Model model = (Model) o;
