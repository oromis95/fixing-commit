@@ -79,7 +79,7 @@ public abstract class Model {
       throw new IllegalArgumentException("Needs at least 6 measurements");
     }
     // use Levenberg-Marquardt least-squares to determine best fitting coefficients for the model
-    final UnconstrainedLeastSquares lm = FactoryOptimization.leastSquaresLM(1e-3, true);
+    final UnconstrainedLeastSquares<?> lm = FactoryOptimization.leastSquaresLM(1e-3, true);
     lm.setFunction(
         new FunctionNtoM() {
 
