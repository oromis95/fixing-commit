@@ -71,6 +71,11 @@ public class EspressoTestGenerator implements TestGenerator {
         sb.append("onView(withId(").append(subject.getId()).append(")).");
     }
 
+    @Override
+    public void generateSubject(StringBuilder sb, DisplayedView subject) {
+        sb.append("onView(allOf(isDisplayed(),withId(").append(subject.getId()).append("))).");
+    }
+
     @Override
     public void generateSubject(StringBuilder sb, OptionsMenu subject) {
         sb.append("openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext())");
