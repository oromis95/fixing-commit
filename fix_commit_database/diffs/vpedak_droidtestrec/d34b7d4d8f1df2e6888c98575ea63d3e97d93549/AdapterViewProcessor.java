@@ -26,18 +26,7 @@ public class AdapterViewProcessor {
             adapterView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                 @Override
                 public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
-                    Object tmp = parent.getItemAtPosition(position);
-
-                    Data data = null;
-                    String descr = "Click at item with value '"+tmp.toString()+"' in " + activityProcessor.getWidgetName(adapterView);
-                    if (tmp instanceof String) {
-                        data = new Data(String.class.getName(), tmp.toString());
-                    } else if (tmp instanceof Cursor) { // CursorAdapter support
-                        data = new Data(Cursor.class.getName());
-                    } else {
-                        data = new Data(tmp.getClass().getName());
-                    }
-                    activityProcessor.getEventWriter().writeEvent(new RecordingEvent(data, new ClickAction(), descr));
+                    generateClickEvent(activityProcessor, position, adapterView);
 
                     if (listener != null) {
                         listener.onItemClick(parent, view, position, id);
@@ -50,18 +39,7 @@ public class AdapterViewProcessor {
             adapterView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                 @Override
                 public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
-                    Object tmp = parent.getItemAtPosition(position);
-
-                    String descr = "Long click at item with value '" + tmp.toString() + "' in " + activityProcessor.getWidgetName(adapterView);
-                    Data data;
-                    if (tmp instanceof String) {
-                        data = new Data(String.class.getName(), tmp.toString());
-                    } else if (tmp instanceof Cursor) { // CursorAdapter support
-                        data = new Data(Cursor.class.getName());
-                    } else {
-                        data = new Data(tmp.getClass().getName());
-                    }
-                    activityProcessor.getEventWriter().writeEvent(new RecordingEvent(data, new LongClickAction(), descr));
+                    generateLongClickEvent(activityProcessor, position, adapterView);
 
                     if (longListener != null) {
                         return longListener.onItemLongClick(parent, view, position, id);
@@ -73,4 +51,34 @@ public class AdapterViewProcessor {
         }
     }
 
+    public static void generateClickEvent(ActivityProcessor activityProcessor, int position, AdapterView adapterView) {
+        Object tmp = adapterView.getItemAtPosition(position);
+
+        Data data = null;
+        String descr = "Click at item with value '"+tmp.toString()+"' in " + activityProcessor.getWidgetName(adapterView);
+        if (tmp instanceof String) {
+            data = new Data(String.class.getName(), tmp.toString());
+        } else if (tmp instanceof Cursor) { // CursorAdapter support
+            data = new Data(Cursor.class.getName());
+        } else {
+            data = new Data(tmp.getClass().getName());
+        }
+        activityProcessor.getEventWriter().writeEvent(new RecordingEvent(data, new ClickAction(), descr));
+    }
+
+    public static void generateLongClickEvent(ActivityProcessor activityProcessor, int position, AdapterView adapterView) {
+        Object tmp = adapterView.getItemAtPosition(position);
+
+        String descr = "Long click at item with value '" + tmp.toString() + "' in " + activityProcessor.getWidgetName(adapterView);
+        Data data;
+        if (tmp instanceof String) {
+            data = new Data(String.class.getName(), tmp.toString());
+        } else if (tmp instanceof Cursor) { // CursorAdapter support
+            data = new Data(Cursor.class.getName());
+        } else {
+            data = new Data(tmp.getClass().getName());
+        }
+        activityProcessor.getEventWriter().writeEvent(new RecordingEvent(data, new LongClickAction(), descr));
+    }
+
 }
