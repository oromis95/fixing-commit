@@ -4,23 +4,25 @@ import android.util.Log;
 
 import com.vpedak.testsrecorder.core.events.RecordingEvent;
 
+import java.util.ArrayList;
 import java.util.HashMap;
+import java.util.List;
 import java.util.Map;
 
 public class EventWriter {
     private long uniqueId;
     public static final String ANDRIOD_TEST_RECORDER = "AndriodTestRecorder";
     public String tag;
-    private Map<String, RecordingEvent> delayedEvents = new HashMap<String, RecordingEvent>();
+    private List<RecordingEvent> delayedEvents = new ArrayList<RecordingEvent>();
 
     public EventWriter(long uniqueId) {
         this.uniqueId = uniqueId;
         tag = ANDRIOD_TEST_RECORDER+uniqueId;
     }
 
-    public void writeEvent(RecordingEvent event) {
+    public synchronized void writeEvent(RecordingEvent event) {
         if (delayedEvents.size() > 0) {
-            for(RecordingEvent delayedEvent : delayedEvents.values()) {
+            for(RecordingEvent delayedEvent : delayedEvents) {
                 Log.d(tag, delayedEvent.toString());
             }
             delayedEvents.clear();
@@ -28,7 +30,7 @@ public class EventWriter {
         Log.d(tag, event.toString());
     }
 
-    public void addDelayedEvent(String id, RecordingEvent delayedEvent) {
-        delayedEvents.put(id, delayedEvent);
+    public synchronized void addDelayedEvent(RecordingEvent delayedEvent) {
+        delayedEvents.add(delayedEvent);
     }
 }
