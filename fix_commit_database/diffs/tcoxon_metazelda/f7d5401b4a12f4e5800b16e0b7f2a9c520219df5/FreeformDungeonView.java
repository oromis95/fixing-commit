@@ -17,13 +17,14 @@ public class FreeformDungeonView extends GridDungeonView {
     
     @Override
     public void draw(Graphics2D g, Dimension dim, IDungeon dungeon) {
-        drawColors(g, getScale(dim,dungeon));
+        drawColors(g, getScale(dim,dungeon), getRoomSize(dim,dungeon));
         super.draw(g,dim,dungeon);
     }
     
-    protected void drawColors(Graphics2D g, double scale) {
+    protected void drawColors(Graphics2D g, double scale, double roomSize) {
         Graphics2D g2 = (Graphics2D)g.create();
         g2.scale(scale, scale);
+        g2.translate(scale*roomSize/4, scale*roomSize/4);
 
         for (int x = colorMap.getLeft(); x <= colorMap.getRight(); ++x)
             for (int y = colorMap.getTop(); y <= colorMap.getBottom(); ++y) {
