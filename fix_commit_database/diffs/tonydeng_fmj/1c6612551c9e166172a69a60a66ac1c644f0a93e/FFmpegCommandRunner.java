@@ -23,8 +23,6 @@ import java.util.concurrent.TimeUnit;
  */
 public class FFmpegCommandRunner {
     private static final Logger log = LoggerFactory.getLogger(FFmpegCommandRunner.class);
-    private static ProcessBuilder pb = null;
-    private static Process process = null;
 
     /**
      * 获取视频信息
@@ -156,6 +154,9 @@ public class FFmpegCommandRunner {
      * @throws Exception
      */
     public static String runProcess(List<String> commands, ProcessCallbackHandler handler) {
+        ProcessBuilder pb = null;
+        Process process = null;
+
         if (log.isDebugEnabled())
             log.debug("start to run ffmpeg process... cmd : '{}'", FFmpegUtils.ffmpegCmdLine(commands));
         Stopwatch stopwatch = Stopwatch.createStarted();
@@ -163,7 +164,6 @@ public class FFmpegCommandRunner {
 
         pb.redirectErrorStream(true);
 
-
         if (null == handler) {
             handler = new DefaultCallbackHandler();
         }
@@ -208,4 +208,4 @@ public class FFmpegCommandRunner {
         }
         return result;
     }
-}
+}
\ No newline at end of file
