@@ -6,6 +6,7 @@ import com.intellij.openapi.project.Project;
 import com.intellij.psi.JavaPsiFacade;
 import com.intellij.psi.PsiClass;
 import com.intellij.psi.PsiPackage;
+import com.intellij.psi.search.GlobalSearchScope;
 
 import org.jetbrains.annotations.NotNull;
 
@@ -31,14 +32,24 @@ public abstract class PackageAliasResolver extends AliasResolver{
       }
       PsiPackage pkg = javaPsiFacade.findPackage(pkgName);
       if (null != pkg) {
-        for (PsiClass clzz : pkg.getClasses()) {
-          addAliasDesc(result, clzz, clzz.getName());
+        addAliasDesc(result, pkg);
+        PsiPackage[] packages = pkg.getSubPackages();
+        for (PsiPackage tmp : packages) {
+          if (null != tmp) {
+            addAliasDesc(result, tmp);
+          }
         }
       }
     }
     return result;
   }
 
+  private void addAliasDesc(Set<AliasDesc> result, PsiPackage pkg) {
+    for (PsiClass clzz : pkg.getClasses()) {
+      addAliasDesc(result, clzz, clzz.getName());
+    }
+  }
+
   @NotNull
   public abstract Collection<String> getPackages();
 }
