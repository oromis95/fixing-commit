@@ -12,7 +12,7 @@ public interface Icons {
 
   Icon MYBATIS_LOGO = IconLoader.getIcon("/javaee/persistenceId.png");
 
-  Icon PARAM_COMPLECTION_ICON = PlatformIcons.PARAMETER_ICON;
+  Icon PARAM_COMPLETION_ICON = PlatformIcons.PARAMETER_ICON;
 
   Icon MAPPER_LINE_MARKER_ICON = IconLoader.getIcon("/images/mapper_method.png");
 
