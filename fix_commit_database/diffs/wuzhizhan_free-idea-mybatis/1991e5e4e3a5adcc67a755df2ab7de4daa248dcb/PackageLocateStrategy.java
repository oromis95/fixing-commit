@@ -7,21 +7,18 @@ import com.intellij.psi.PsiPackage;
 
 import org.jetbrains.annotations.NotNull;
 
-import java.util.Set;
-
 /**
  * @author yanglin
  */
 public class PackageLocateStrategy extends LocateStrategy{
 
-  private PackageProvider provider = new MapperXmlPakcageProvider();
+  private PackageProvider provider = new MapperXmlPackageProvider();
 
   @Override
-  public boolean apply(@NotNull PsiClass clzz) {
-    Set<PsiPackage> pakcages = provider.getPackages(clzz.getProject());
-    String packageName = ((PsiJavaFile) clzz.getContainingFile()).getPackageName();
-    PsiPackage pkg = JavaPsiFacade.getInstance(clzz.getProject()).findPackage(packageName);
-    for (PsiPackage tmp : pakcages) {
+  public boolean apply(@NotNull PsiClass clazz) {
+    String packageName = ((PsiJavaFile) clazz.getContainingFile()).getPackageName();
+    PsiPackage pkg = JavaPsiFacade.getInstance(clazz.getProject()).findPackage(packageName);
+    for (PsiPackage tmp : provider.getPackages(clazz.getProject())) {
       if (tmp.equals(pkg)) {
         return true;
       }
