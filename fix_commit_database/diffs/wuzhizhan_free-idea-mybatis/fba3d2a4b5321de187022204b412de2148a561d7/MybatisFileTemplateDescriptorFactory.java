@@ -14,7 +14,7 @@ public class MybatisFileTemplateDescriptorFactory implements FileTemplateGroupDe
 
   @Override
   public FileTemplateGroupDescriptor getFileTemplatesDescriptor() {
-    FileTemplateGroupDescriptor group = new FileTemplateGroupDescriptor("Mybaits", Icons.MYBATIS_LOGO);
+    FileTemplateGroupDescriptor group = new FileTemplateGroupDescriptor("Mybatis", Icons.MYBATIS_LOGO);
     group.addTemplate(new FileTemplateDescriptor(MYBATIS_MAPPER_XML_TEMPLATE, Icons.MYBATIS_LOGO));
     return group;
   }
