@@ -1,7 +1,10 @@
 package com.seventh7.mybatis.alias;
 
+import com.google.common.base.Optional;
+
 import com.intellij.openapi.project.Project;
 import com.intellij.psi.PsiClass;
+import com.seventh7.mybatis.util.JavaUtils;
 
 import org.jetbrains.annotations.NotNull;
 import org.jetbrains.annotations.Nullable;
@@ -19,16 +22,16 @@ public abstract class AliasResolver {
     this.project = project;
   }
 
-  @Nullable
-  protected AliasDesc addAliasDesc(@NotNull Set<AliasDesc> descs, @Nullable PsiClass clzz, @Nullable String alias) {
-    if (null == clzz || null == alias || clzz.isEnum() || clzz.isInterface() || !clzz.isValid()) {
-      return null;
+  @NotNull
+  protected Optional<AliasDesc> addAliasDesc(@NotNull Set<AliasDesc> descs, @Nullable PsiClass clzz, @Nullable String alias) {
+    if (null == alias || !JavaUtils.isModelClzz(clzz)) {
+      return Optional.absent();
     }
     AliasDesc desc = new AliasDesc();
     descs.add(desc);
     desc.setClzz(clzz);
     desc.setAlias(alias);
-    return desc;
+    return Optional.of(desc);
   }
 
   @NotNull
