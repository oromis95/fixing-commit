@@ -26,7 +26,7 @@ import static com.wuzhizhan.mybatis.generate.StatementGenerator.UPDATE_GENERATOR
  */
 @State(
         name = "MybatisSettings",
-        storages = @Storage(id = "other", file = "$APP_CONFIG$/mybatis.xml"))
+        storages = @Storage(file = "$APP_CONFIG$/mybatis.xml"))
 public class MybatisSetting implements PersistentStateComponent<Element> {
 
     private GenerateModel statementGenerateModel;
