@@ -76,20 +76,4 @@ public final class JavaUtils {
     return false;
   }
 
-  public static void importClzz(Project project, PsiJavaFile file, String clzzName) {
-    if (hasImportClzz(file, clzzName)) {
-      return;
-    }
-    Optional<PsiClass> clzz = findClzz(project, clzzName);
-    if (!clzz.isPresent()) {
-      return;
-    }
-    PsiElementFactory elementFactory = JavaPsiFacade.getInstance(project).getElementFactory();
-    PsiImportStatement statement = elementFactory.createImportStatement(clzz.get());
-    file.getImportList().add(statement);
-
-    CodeFormatterFacade formatter = new CodeFormatterFacade(new CodeStyleSettings());
-    formatter.processText(file, new FormatTextRanges(statement.getTextRange(), true), true);
-  }
-
 }
