@@ -24,7 +24,7 @@ import java.util.Set;
 public class MapperXmlPakcageProvider extends PackageProvider{
 
   @NotNull @Override
-  public Set<PsiPackage> getPakcages(@NotNull Project project) {
+  public Set<PsiPackage> getPackages(@NotNull Project project) {
     HashSet<PsiPackage> res = Sets.newHashSet();
     Collection<Mapper> mappers = MapperUtils.findMappers(project);
     JavaPsiFacade javaPsiFacade = JavaPsiFacade.getInstance(project);
