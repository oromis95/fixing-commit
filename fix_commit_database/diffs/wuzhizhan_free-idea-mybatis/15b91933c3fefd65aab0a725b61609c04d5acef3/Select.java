@@ -6,11 +6,14 @@ import com.intellij.util.xml.Convert;
 import com.intellij.util.xml.GenericAttributeValue;
 import com.seventh7.mybatis.dom.converter.AliasConverter;
 
+import org.jetbrains.annotations.NotNull;
+
 /**
  * @author yanglin
  */
 public interface Select extends GroupTwo, ResultMapGroup{
 
+  @NotNull
   @Attribute("resultType")
   @Convert(AliasConverter.class)
   public GenericAttributeValue<PsiClass> getResultType();
