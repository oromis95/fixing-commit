@@ -5,7 +5,9 @@ import com.google.common.collect.Lists;
 
 import com.intellij.openapi.components.ServiceManager;
 import com.intellij.openapi.project.Project;
+import com.intellij.psi.JavaPsiFacade;
 import com.intellij.psi.PsiClass;
+import com.intellij.psi.search.GlobalSearchScope;
 
 import org.jetbrains.annotations.NotNull;
 import org.jetbrains.annotations.Nullable;
@@ -42,6 +44,10 @@ public class AliasFacade {
 
   @NotNull
   public Optional<PsiClass> findPsiClass(@NotNull String shortName) {
+    PsiClass clzz = JavaPsiFacade.getInstance(project).findClass(shortName, GlobalSearchScope.allScope(project));
+    if (null != clzz) {
+      return Optional.of(clzz);
+    }
     for (AliasResolver resolver : resolvers) {
       for (AliasDesc desc : resolver.getClssAliasDescriptions()) {
         if (desc.getAlias().equals(shortName)) {
