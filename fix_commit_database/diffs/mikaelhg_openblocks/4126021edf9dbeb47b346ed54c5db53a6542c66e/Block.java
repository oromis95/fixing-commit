@@ -23,7 +23,7 @@ import codeblocks.BlockConnector.PositionType;
 /**
  * Block holds the mutable prop (data) of a particular block.  These mutable prop include socket, 
  * before, after and blocks, "bad"-ness. In addition, Block maintains information to describe a particular 
- * blocks relationship with other blocks. 
+ * block's relationship with other blocks. 
  * 
  */
 public class Block implements ISupportMemento {
