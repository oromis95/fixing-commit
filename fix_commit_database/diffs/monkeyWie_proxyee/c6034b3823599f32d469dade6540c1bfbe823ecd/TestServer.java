@@ -4,6 +4,7 @@ import io.netty.bootstrap.ServerBootstrap;
 import io.netty.channel.*;
 import io.netty.channel.nio.NioEventLoopGroup;
 import io.netty.channel.socket.nio.NioServerSocketChannel;
+import io.netty.handler.codec.http.HttpServerCodec;
 
 public class TestServer {
 
@@ -21,13 +22,28 @@ public class TestServer {
 
             @Override
             protected void initChannel(Channel ch) throws Exception {
+              ch.pipeline().addLast(new HttpServerCodec());
               ch.pipeline().addLast(new ChannelInboundHandlerAdapter() {
                 @Override
                 public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                   ctx.channel().writeAndFlush(msg);
                 }
+
               });
             }
+
+            @Override
+            public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
+                throws Exception {
+              System.out.println("exceptionCaught:33333333");
+              super.exceptionCaught(ctx, cause);
+            }
+
+            @Override
+            public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
+              System.out.println("channelUnregistered:33333333");
+              super.channelUnregistered(ctx);
+            }
           });
       ChannelFuture f = b
           .bind(80)
