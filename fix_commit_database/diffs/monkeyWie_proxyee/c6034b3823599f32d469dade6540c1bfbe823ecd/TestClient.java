@@ -9,7 +9,9 @@ import io.netty.channel.ChannelInitializer;
 import io.netty.channel.nio.NioEventLoopGroup;
 import io.netty.channel.socket.nio.NioSocketChannel;
 import io.netty.handler.codec.http.DefaultFullHttpRequest;
+import io.netty.handler.codec.http.DefaultLastHttpContent;
 import io.netty.handler.codec.http.HttpClientCodec;
+import io.netty.handler.codec.http.HttpContent;
 import io.netty.handler.codec.http.HttpHeaderNames;
 import io.netty.handler.codec.http.HttpMethod;
 import io.netty.handler.codec.http.HttpObjectAggregator;
@@ -31,35 +33,53 @@ public class TestClient {
 
           @Override
           protected void initChannel(Channel ch) throws Exception {
-            ch.pipeline().addLast(
-                SslContextBuilder
-                    .forClient()
-//                    .protocols("TLSv1")
-                    .trustManager(
-                InsecureTrustManagerFactory.INSTANCE)
-                .build()
-                    .newHandler(
-                        ch.alloc(),"cdn.mdn.mozilla.net",443
-                    )
-            );
             ch.pipeline().addLast(new HttpClientCodec());
             ch.pipeline().addLast(new HttpObjectAggregator(81920000));
             ch.pipeline().addLast(new ChannelInboundHandlerAdapter() {
+              @Override
+              public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
+                System.out.println("channelUnregistered:222222222");
+                super.channelUnregistered(ctx);
+              }
+
+              @Override
+              public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
+                  throws Exception {
+                System.out.println("exceptionCaught:222222222");
+                super.exceptionCaught(ctx, cause);
+              }
+
               @Override
               public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                 System.out.println(msg.toString());
               }
             });
           }
+
+          @Override
+          public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
+            System.out.println("channelUnregistered:11111111");
+            super.channelUnregistered(ctx);
+          }
+
+          @Override
+          public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
+            System.out.println("exceptionCaught:1111111");
+            super.exceptionCaught(ctx, cause);
+          }
         });
     //https://cdn.mdn.mozilla.net/static/img/favicon32.7f3da72dcea1.png
-    ChannelFuture cf = bootstrap.connect("cdn.mdn.mozilla.net", 443).sync();
+    ChannelFuture cf = bootstrap.connect("127.0.0.1", 80).sync();
     HttpRequest httpRequest = new DefaultFullHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET,
-        "/static/img/favicon32.7f3da72dcea1.png");
+        "/");
     httpRequest.headers().add(HttpHeaderNames.HOST, "cdn.mdn.mozilla.net");
     httpRequest.headers().add(HttpHeaderNames.USER_AGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.75 Safari/537.36");
-    httpRequest.headers().add(HttpHeaderNames.CONTENT_LENGTH, 0);
+    byte[] bts = new byte[512];
+    httpRequest.headers().add(HttpHeaderNames.CONTENT_LENGTH, bts.length+100);
     System.out.println(httpRequest.toString());
+    HttpContent httpContent = new DefaultLastHttpContent();
+    httpContent.content().writeBytes(bts);
     cf.channel().writeAndFlush(httpRequest);
+    cf.channel().writeAndFlush(httpContent);
   }
 }
