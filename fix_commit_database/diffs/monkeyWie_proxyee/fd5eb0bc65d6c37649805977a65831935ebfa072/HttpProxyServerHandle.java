@@ -94,12 +94,14 @@ public class HttpProxyServerHandle extends ChannelInboundHandlerAdapter {
               HttpProxyServer.SUCCESS);
           ctx.writeAndFlush(response);
           ctx.channel().pipeline().remove("httpCodec");
+          //fix issue #42
+          ReferenceCountUtil.release(msg);
           return;
         }
       }
       interceptPipeline = buildPipeline();
       interceptPipeline.setRequestProto(new RequestProto(host, port, isSsl));
-      //fix issues #27
+      //fix issue #27
       if (request.uri().indexOf("/") != 0) {
         URL url = new URL(request.uri());
         request.setUri(url.getFile());
