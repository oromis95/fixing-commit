@@ -6,6 +6,7 @@ public class BlockFurnace extends BlockContainer {
 
     private Random a = new Random();
     private final boolean b;
+    private static boolean c = false;
 
     protected BlockFurnace(int i, boolean flag) {
         super(i, Material.STONE);
@@ -69,12 +70,14 @@ public class BlockFurnace extends BlockContainer {
         int l = world.getData(i, j, k);
         TileEntity tileentity = world.getTileEntity(i, j, k);
 
+        c = true;
         if (flag) {
             world.setTypeId(i, j, k, Block.BURNING_FURNACE.id);
         } else {
             world.setTypeId(i, j, k, Block.FURNACE.id);
         }
 
+        c = false;
         world.setData(i, j, k, l);
         world.setTileEntity(i, j, k, tileentity);
     }
@@ -104,31 +107,33 @@ public class BlockFurnace extends BlockContainer {
     }
 
     public void remove(World world, int i, int j, int k) {
-        TileEntityFurnace tileentityfurnace = (TileEntityFurnace) world.getTileEntity(i, j, k);
+        if (!c) {
+            TileEntityFurnace tileentityfurnace = (TileEntityFurnace) world.getTileEntity(i, j, k);
 
-        for (int l = 0; l < tileentityfurnace.getSize(); ++l) {
-            ItemStack itemstack = tileentityfurnace.getItem(l);
+            for (int l = 0; l < tileentityfurnace.getSize(); ++l) {
+                ItemStack itemstack = tileentityfurnace.getItem(l);
 
-            if (itemstack != null) {
-                float f = this.a.nextFloat() * 0.8F + 0.1F;
-                float f1 = this.a.nextFloat() * 0.8F + 0.1F;
-                float f2 = this.a.nextFloat() * 0.8F + 0.1F;
+                if (itemstack != null) {
+                    float f = this.a.nextFloat() * 0.8F + 0.1F;
+                    float f1 = this.a.nextFloat() * 0.8F + 0.1F;
+                    float f2 = this.a.nextFloat() * 0.8F + 0.1F;
 
-                while (itemstack.count > 0) {
-                    int i1 = this.a.nextInt(21) + 10;
+                    while (itemstack.count > 0) {
+                        int i1 = this.a.nextInt(21) + 10;
 
-                    if (i1 > itemstack.count) {
-                        i1 = itemstack.count;
-                    }
+                        if (i1 > itemstack.count) {
+                            i1 = itemstack.count;
+                        }
 
-                    itemstack.count -= i1;
-                    EntityItem entityitem = new EntityItem(world, (double) ((float) i + f), (double) ((float) j + f1), (double) ((float) k + f2), new ItemStack(itemstack.id, i1, itemstack.getData()));
-                    float f3 = 0.05F;
+                        itemstack.count -= i1;
+                        EntityItem entityitem = new EntityItem(world, (double) ((float) i + f), (double) ((float) j + f1), (double) ((float) k + f2), new ItemStack(itemstack.id, i1, itemstack.getData()));
+                        float f3 = 0.05F;
 
-                    entityitem.motX = (double) ((float) this.a.nextGaussian() * f3);
-                    entityitem.motY = (double) ((float) this.a.nextGaussian() * f3 + 0.2F);
-                    entityitem.motZ = (double) ((float) this.a.nextGaussian() * f3);
-                    world.addEntity(entityitem);
+                        entityitem.motX = (double) ((float) this.a.nextGaussian() * f3);
+                        entityitem.motY = (double) ((float) this.a.nextGaussian() * f3 + 0.2F);
+                        entityitem.motZ = (double) ((float) this.a.nextGaussian() * f3);
+                        world.addEntity(entityitem);
+                    }
                 }
             }
         }
