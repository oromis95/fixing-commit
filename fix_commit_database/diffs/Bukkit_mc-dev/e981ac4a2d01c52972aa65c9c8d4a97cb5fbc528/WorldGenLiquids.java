@@ -11,28 +11,28 @@ public class WorldGenLiquids extends WorldGenerator {
     }
 
     public boolean a(World world, Random random, int i, int j, int k) {
-        if (world.a(i, j + 1, k) != Block.STONE.bh) {
+        if (world.a(i, j + 1, k) != Block.STONE.bi) {
             return false;
-        } else if (world.a(i, j - 1, k) != Block.STONE.bh) {
+        } else if (world.a(i, j - 1, k) != Block.STONE.bi) {
             return false;
-        } else if (world.a(i, j, k) != 0 && world.a(i, j, k) != Block.STONE.bh) {
+        } else if (world.a(i, j, k) != 0 && world.a(i, j, k) != Block.STONE.bi) {
             return false;
         } else {
             int l = 0;
 
-            if (world.a(i - 1, j, k) == Block.STONE.bh) {
+            if (world.a(i - 1, j, k) == Block.STONE.bi) {
                 ++l;
             }
 
-            if (world.a(i + 1, j, k) == Block.STONE.bh) {
+            if (world.a(i + 1, j, k) == Block.STONE.bi) {
                 ++l;
             }
 
-            if (world.a(i, j, k - 1) == Block.STONE.bh) {
+            if (world.a(i, j, k - 1) == Block.STONE.bi) {
                 ++l;
             }
 
-            if (world.a(i, j, k + 1) == Block.STONE.bh) {
+            if (world.a(i, j, k + 1) == Block.STONE.bi) {
                 ++l;
             }
 
@@ -55,7 +55,7 @@ public class WorldGenLiquids extends WorldGenerator {
             }
 
             if (l == 3 && i1 == 1) {
-                world.d(i, j, k, this.a);
+                world.e(i, j, k, this.a);
                 world.a = true;
                 Block.m[this.a].a(world, i, j, k, random);
                 world.a = false;
