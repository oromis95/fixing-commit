@@ -3,13 +3,13 @@ package net.minecraft.server;
 import java.io.DataInputStream;
 import java.io.DataOutputStream;
 
-public class Packet101 extends Packet {
+public class Packet101CloseWindow extends Packet {
 
     public int a;
 
-    public Packet101() {}
+    public Packet101CloseWindow() {}
 
-    public Packet101(int i) {
+    public Packet101CloseWindow(int i) {
         this.a = i;
     }
 
