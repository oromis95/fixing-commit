@@ -2,28 +2,28 @@ package net.minecraft.server;
 
 public class ItemHoe extends Item {
 
-    public ItemHoe(int i, int j) {
+    public ItemHoe(int i, EnumToolMaterial enumtoolmaterial) {
         super(i);
-        this.aX = 1;
-        this.aY = 32 << j;
+        this.bb = 1;
+        this.bc = enumtoolmaterial.a();
     }
 
     public boolean a(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
         int i1 = world.a(i, j, k);
         Material material = world.c(i, j + 1, k);
 
-        if ((material.a() || i1 != Block.GRASS.bh) && i1 != Block.DIRT.bh) {
+        if ((material.a() || i1 != Block.GRASS.bi) && i1 != Block.DIRT.bi) {
             return false;
         } else {
             Block block = Block.SOIL;
 
-            world.a((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), block.bq.c(), (block.bq.a() + 1.0F) / 2.0F, block.bq.b() * 0.8F);
+            world.a((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), block.br.c(), (block.br.a() + 1.0F) / 2.0F, block.br.b() * 0.8F);
             if (world.z) {
                 return true;
             } else {
-                world.d(i, j, k, block.bh);
+                world.e(i, j, k, block.bi);
                 itemstack.b(1);
-                if (world.l.nextInt(8) == 0 && i1 == Block.GRASS.bh) {
+                if (world.l.nextInt(8) == 0 && i1 == Block.GRASS.bi) {
                     byte b0 = 1;
 
                     for (int j1 = 0; j1 < b0; ++j1) {
