@@ -76,11 +76,11 @@ public class ChunkProviderHell implements IChunkProvider {
                                 int l2 = 0;
 
                                 if (k1 * 8 + l1 < b1) {
-                                    l2 = Block.STATIONARY_LAVA.bh;
+                                    l2 = Block.STATIONARY_LAVA.bi;
                                 }
 
                                 if (d15 > 0.0D) {
-                                    l2 = Block.NETHERRACK.bh;
+                                    l2 = Block.NETHERRACK.bi;
                                 }
 
                                 abyte[j2] = (byte) l2;
@@ -116,48 +116,48 @@ public class ChunkProviderHell implements IChunkProvider {
                 boolean flag1 = this.q[k + l * 16] + this.h.nextDouble() * 0.2D > 0.0D;
                 int i1 = (int) (this.r[k + l * 16] / 3.0D + 3.0D + this.h.nextDouble() * 0.25D);
                 int j1 = -1;
-                byte b1 = (byte) Block.NETHERRACK.bh;
-                byte b2 = (byte) Block.NETHERRACK.bh;
+                byte b1 = (byte) Block.NETHERRACK.bi;
+                byte b2 = (byte) Block.NETHERRACK.bi;
 
                 for (int k1 = 127; k1 >= 0; --k1) {
                     int l1 = (k * 16 + l) * 128 + k1;
 
                     if (k1 >= 127 - this.h.nextInt(5)) {
-                        abyte[l1] = (byte) Block.BEDROCK.bh;
+                        abyte[l1] = (byte) Block.BEDROCK.bi;
                     } else if (k1 <= 0 + this.h.nextInt(5)) {
-                        abyte[l1] = (byte) Block.BEDROCK.bh;
+                        abyte[l1] = (byte) Block.BEDROCK.bi;
                     } else {
                         byte b3 = abyte[l1];
 
                         if (b3 == 0) {
                             j1 = -1;
-                        } else if (b3 == Block.NETHERRACK.bh) {
+                        } else if (b3 == Block.NETHERRACK.bi) {
                             if (j1 == -1) {
                                 if (i1 <= 0) {
                                     b1 = 0;
-                                    b2 = (byte) Block.NETHERRACK.bh;
+                                    b2 = (byte) Block.NETHERRACK.bi;
                                 } else if (k1 >= b0 - 4 && k1 <= b0 + 1) {
-                                    b1 = (byte) Block.NETHERRACK.bh;
-                                    b2 = (byte) Block.NETHERRACK.bh;
+                                    b1 = (byte) Block.NETHERRACK.bi;
+                                    b2 = (byte) Block.NETHERRACK.bi;
                                     if (flag1) {
-                                        b1 = (byte) Block.GRAVEL.bh;
+                                        b1 = (byte) Block.GRAVEL.bi;
                                     }
 
                                     if (flag1) {
-                                        b2 = (byte) Block.NETHERRACK.bh;
+                                        b2 = (byte) Block.NETHERRACK.bi;
                                     }
 
                                     if (flag) {
-                                        b1 = (byte) Block.SOUL_SAND.bh;
+                                        b1 = (byte) Block.SOUL_SAND.bi;
                                     }
 
                                     if (flag) {
-                                        b2 = (byte) Block.SOUL_SAND.bh;
+                                        b2 = (byte) Block.SOUL_SAND.bi;
                                     }
                                 }
 
                                 if (k1 < b0 && b1 == 0) {
-                                    b1 = (byte) Block.STATIONARY_LAVA.bh;
+                                    b1 = (byte) Block.STATIONARY_LAVA.bi;
                                 }
 
                                 j1 = i1;
@@ -186,8 +186,6 @@ public class ChunkProviderHell implements IChunkProvider {
         this.s.a(this, this.n, i, j, abyte);
         Chunk chunk = new Chunk(this.n, abyte, i, j);
 
-        chunk.b();
-        chunk.c();
         return chunk;
     }
 
@@ -324,7 +322,7 @@ public class ChunkProviderHell implements IChunkProvider {
             j1 = k + this.h.nextInt(16) + 8;
             k1 = this.h.nextInt(120) + 4;
             l1 = l + this.h.nextInt(16) + 8;
-            (new WorldGenHellLava(Block.LAVA.bh)).a(this.n, this.h, j1, k1, l1);
+            (new WorldGenHellLava(Block.LAVA.bi)).a(this.n, this.h, j1, k1, l1);
         }
 
         i1 = this.h.nextInt(this.h.nextInt(10) + 1) + 1;
@@ -358,14 +356,14 @@ public class ChunkProviderHell implements IChunkProvider {
             j1 = k + this.h.nextInt(16) + 8;
             k1 = this.h.nextInt(128);
             l1 = l + this.h.nextInt(16) + 8;
-            (new WorldGenFlowers(Block.BROWN_MUSHROOM.bh)).a(this.n, this.h, j1, k1, l1);
+            (new WorldGenFlowers(Block.BROWN_MUSHROOM.bi)).a(this.n, this.h, j1, k1, l1);
         }
 
         if (this.h.nextInt(1) == 0) {
             j1 = k + this.h.nextInt(16) + 8;
             k1 = this.h.nextInt(128);
             l1 = l + this.h.nextInt(16) + 8;
-            (new WorldGenFlowers(Block.RED_MUSHROOM.bh)).a(this.n, this.h, j1, k1, l1);
+            (new WorldGenFlowers(Block.RED_MUSHROOM.bi)).a(this.n, this.h, j1, k1, l1);
         }
 
         BlockSand.a = false;
