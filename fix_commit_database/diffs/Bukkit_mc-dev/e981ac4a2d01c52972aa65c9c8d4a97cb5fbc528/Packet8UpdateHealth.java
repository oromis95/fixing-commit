@@ -3,13 +3,13 @@ package net.minecraft.server;
 import java.io.DataInputStream;
 import java.io.DataOutputStream;
 
-public class Packet8 extends Packet {
+public class Packet8UpdateHealth extends Packet {
 
     public int a;
 
-    public Packet8() {}
+    public Packet8UpdateHealth() {}
 
-    public Packet8(int i) {
+    public Packet8UpdateHealth(int i) {
         this.a = i;
     }
 
