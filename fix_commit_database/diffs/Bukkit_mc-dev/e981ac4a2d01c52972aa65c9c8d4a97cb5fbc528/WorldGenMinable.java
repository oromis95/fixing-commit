@@ -28,16 +28,28 @@ public class WorldGenMinable extends WorldGenerator {
             double d9 = random.nextDouble() * (double) this.b / 16.0D;
             double d10 = (double) (MathHelper.a((float) l * 3.1415927F / (float) this.b) + 1.0F) * d9 + 1.0D;
             double d11 = (double) (MathHelper.a((float) l * 3.1415927F / (float) this.b) + 1.0F) * d9 + 1.0D;
-
-            for (int i1 = (int) (d6 - d10 / 2.0D); i1 <= (int) (d6 + d10 / 2.0D); ++i1) {
-                for (int j1 = (int) (d7 - d11 / 2.0D); j1 <= (int) (d7 + d11 / 2.0D); ++j1) {
-                    for (int k1 = (int) (d8 - d10 / 2.0D); k1 <= (int) (d8 + d10 / 2.0D); ++k1) {
-                        double d12 = ((double) i1 + 0.5D - d6) / (d10 / 2.0D);
-                        double d13 = ((double) j1 + 0.5D - d7) / (d11 / 2.0D);
-                        double d14 = ((double) k1 + 0.5D - d8) / (d10 / 2.0D);
-
-                        if (d12 * d12 + d13 * d13 + d14 * d14 < 1.0D && world.a(i1, j1, k1) == Block.STONE.bh) {
-                            world.a(i1, j1, k1, this.a);
+            int i1 = (int) (d6 - d10 / 2.0D);
+            int j1 = (int) (d7 - d11 / 2.0D);
+            int k1 = (int) (d8 - d10 / 2.0D);
+            int l1 = (int) (d6 + d10 / 2.0D);
+            int i2 = (int) (d7 + d11 / 2.0D);
+            int j2 = (int) (d8 + d10 / 2.0D);
+
+            for (int k2 = i1; k2 <= l1; ++k2) {
+                double d12 = ((double) k2 + 0.5D - d6) / (d10 / 2.0D);
+
+                if (d12 * d12 < 1.0D) {
+                    for (int l2 = j1; l2 <= i2; ++l2) {
+                        double d13 = ((double) l2 + 0.5D - d7) / (d11 / 2.0D);
+
+                        if (d12 * d12 + d13 * d13 < 1.0D) {
+                            for (int i3 = k1; i3 <= j2; ++i3) {
+                                double d14 = ((double) i3 + 0.5D - d8) / (d10 / 2.0D);
+
+                                if (d12 * d12 + d13 * d13 + d14 * d14 < 1.0D && world.a(k2, l2, i3) == Block.STONE.bi) {
+                                    world.b(k2, l2, i3, this.a);
+                                }
+                            }
                         }
                     }
                 }
