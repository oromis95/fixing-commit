@@ -2,10 +2,10 @@ package net.minecraft.server;
 
 public class ItemSpade extends ItemTool {
 
-    private static Block[] bb = new Block[] { Block.GRASS, Block.DIRT, Block.SAND, Block.GRAVEL, Block.SNOW, Block.SNOW_BLOCK, Block.CLAY};
+    private static Block[] bg = new Block[] { Block.GRASS, Block.DIRT, Block.SAND, Block.GRAVEL, Block.SNOW, Block.SNOW_BLOCK, Block.CLAY};
 
-    public ItemSpade(int i, int j) {
-        super(i, 1, j, bb);
+    public ItemSpade(int i, EnumToolMaterial enumtoolmaterial) {
+        super(i, 1, enumtoolmaterial, bg);
     }
 
     public boolean a(Block block) {
