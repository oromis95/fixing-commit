@@ -120,7 +120,7 @@ public class ChunkLoader implements IChunkLoader {
         nbttagcompound.a("BlockLight", chunk.g.a);
         nbttagcompound.a("HeightMap", chunk.h);
         nbttagcompound.a("TerrainPopulated", chunk.n);
-        chunk.r = false;
+        chunk.q = false;
         NBTTagList nbttaglist = new NBTTagList();
 
         Iterator iterator;
@@ -132,7 +132,7 @@ public class ChunkLoader implements IChunkLoader {
             while (iterator.hasNext()) {
                 Entity entity = (Entity) iterator.next();
 
-                chunk.r = true;
+                chunk.q = true;
                 nbttagcompound1 = new NBTTagCompound();
                 if (entity.c(nbttagcompound1)) {
                     nbttaglist.a((NBTBase) nbttagcompound1);
@@ -189,7 +189,7 @@ public class ChunkLoader implements IChunkLoader {
                 NBTTagCompound nbttagcompound1 = (NBTTagCompound) nbttaglist.a(k);
                 Entity entity = EntityTypes.a(nbttagcompound1, world);
 
-                chunk.r = true;
+                chunk.q = true;
                 if (entity != null) {
                     chunk.a(entity);
                 }
