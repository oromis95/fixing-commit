@@ -111,7 +111,7 @@ public class MapGenCaves extends MapGenBase {
                             for (int l3 = j2 + 1; !flag2 && l3 >= i2 - 1; --l3) {
                                 i3 = (j3 * 16 + k3) * 128 + l3;
                                 if (l3 >= 0 && l3 < 128) {
-                                    if (abyte[i3] == Block.WATER.bh || abyte[i3] == Block.STATIONARY_WATER.bh) {
+                                    if (abyte[i3] == Block.WATER.bi || abyte[i3] == Block.STATIONARY_WATER.bi) {
                                         flag2 = true;
                                     }
 
@@ -132,29 +132,31 @@ public class MapGenCaves extends MapGenBase {
                                 int i4 = (j3 * 16 + i3) * 128 + j2;
                                 boolean flag3 = false;
 
-                                for (int j4 = j2 - 1; j4 >= i2; --j4) {
-                                    double d14 = ((double) j4 + 0.5D - d1) / d7;
+                                if (d12 * d12 + d13 * d13 < 1.0D) {
+                                    for (int j4 = j2 - 1; j4 >= i2; --j4) {
+                                        double d14 = ((double) j4 + 0.5D - d1) / d7;
 
-                                    if (d14 > -0.7D && d12 * d12 + d14 * d14 + d13 * d13 < 1.0D) {
-                                        byte b0 = abyte[i4];
+                                        if (d14 > -0.7D && d12 * d12 + d14 * d14 + d13 * d13 < 1.0D) {
+                                            byte b0 = abyte[i4];
 
-                                        if (b0 == Block.GRASS.bh) {
-                                            flag3 = true;
-                                        }
+                                            if (b0 == Block.GRASS.bi) {
+                                                flag3 = true;
+                                            }
 
-                                        if (b0 == Block.STONE.bh || b0 == Block.DIRT.bh || b0 == Block.GRASS.bh) {
-                                            if (j4 < 10) {
-                                                abyte[i4] = (byte) Block.LAVA.bh;
-                                            } else {
-                                                abyte[i4] = 0;
-                                                if (flag3 && abyte[i4 - 1] == Block.DIRT.bh) {
-                                                    abyte[i4 - 1] = (byte) Block.GRASS.bh;
+                                            if (b0 == Block.STONE.bi || b0 == Block.DIRT.bi || b0 == Block.GRASS.bi) {
+                                                if (j4 < 10) {
+                                                    abyte[i4] = (byte) Block.LAVA.bi;
+                                                } else {
+                                                    abyte[i4] = 0;
+                                                    if (flag3 && abyte[i4 - 1] == Block.DIRT.bi) {
+                                                        abyte[i4 - 1] = (byte) Block.GRASS.bi;
+                                                    }
                                                 }
                                             }
                                         }
-                                    }
 
-                                    --i4;
+                                        --i4;
+                                    }
                                 }
                             }
                         }
