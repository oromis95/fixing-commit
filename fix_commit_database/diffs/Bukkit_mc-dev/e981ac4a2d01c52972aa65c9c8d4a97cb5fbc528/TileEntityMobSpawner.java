@@ -3,20 +3,24 @@ package net.minecraft.server;
 public class TileEntityMobSpawner extends TileEntity {
 
     public int e = -1;
-    public String f = "Pig";
-    public double g;
-    public double h = 0.0D;
+    private String h = "Pig";
+    public double f;
+    public double g = 0.0D;
 
     public TileEntityMobSpawner() {
         this.e = 20;
     }
 
+    public void a(String s) {
+        this.h = s;
+    }
+
     public boolean a() {
         return this.a.a((double) this.b + 0.5D, (double) this.c + 0.5D, (double) this.d + 0.5D, 16.0D) != null;
     }
 
-    public void e() {
-        this.h = this.g;
+    public void f() {
+        this.g = this.f;
         if (this.a()) {
             double d0 = (double) ((float) this.b + this.a.l.nextFloat());
             double d1 = (double) ((float) this.c + this.a.l.nextFloat());
@@ -25,8 +29,8 @@ public class TileEntityMobSpawner extends TileEntity {
             this.a.a("smoke", d0, d1, d2, 0.0D, 0.0D, 0.0D);
             this.a.a("flame", d0, d1, d2, 0.0D, 0.0D, 0.0D);
 
-            for (this.g += (double) (1000.0F / ((float) this.e + 200.0F)); this.g > 360.0D; this.h -= 360.0D) {
-                this.g -= 360.0D;
+            for (this.f += (double) (1000.0F / ((float) this.e + 200.0F)); this.f > 360.0D; this.g -= 360.0D) {
+                this.f -= 360.0D;
             }
 
             if (this.e == -1) {
@@ -39,7 +43,7 @@ public class TileEntityMobSpawner extends TileEntity {
                 byte b0 = 4;
 
                 for (int i = 0; i < b0; ++i) {
-                    EntityLiving entityliving = (EntityLiving) ((EntityLiving) EntityTypes.a(this.f, this.a));
+                    EntityLiving entityliving = (EntityLiving) ((EntityLiving) EntityTypes.a(this.h, this.a));
 
                     if (entityliving == null) {
                         return;
@@ -58,7 +62,7 @@ public class TileEntityMobSpawner extends TileEntity {
                         double d5 = (double) this.d + (this.a.l.nextDouble() - this.a.l.nextDouble()) * 4.0D;
 
                         entityliving.c(d3, d4, d5, this.a.l.nextFloat() * 360.0F, 0.0F);
-                        if (entityliving.a()) {
+                        if (entityliving.b()) {
                             this.a.a((Entity) entityliving);
 
                             for (int k = 0; k < 20; ++k) {
@@ -69,13 +73,13 @@ public class TileEntityMobSpawner extends TileEntity {
                                 this.a.a("flame", d0, d1, d2, 0.0D, 0.0D, 0.0D);
                             }
 
-                            entityliving.O();
+                            entityliving.R();
                             this.b();
                         }
                     }
                 }
 
-                super.e();
+                super.f();
             }
         }
     }
@@ -86,13 +90,13 @@ public class TileEntityMobSpawner extends TileEntity {
 
     public void a(NBTTagCompound nbttagcompound) {
         super.a(nbttagcompound);
-        this.f = nbttagcompound.h("EntityId");
+        this.h = nbttagcompound.h("EntityId");
         this.e = nbttagcompound.c("Delay");
     }
 
     public void b(NBTTagCompound nbttagcompound) {
         super.b(nbttagcompound);
-        nbttagcompound.a("EntityId", this.f);
+        nbttagcompound.a("EntityId", this.h);
         nbttagcompound.a("Delay", (short) this.e);
     }
 }
