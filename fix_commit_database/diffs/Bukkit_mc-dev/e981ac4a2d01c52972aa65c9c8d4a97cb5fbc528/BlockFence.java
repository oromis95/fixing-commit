@@ -13,7 +13,7 @@ public class BlockFence extends Block {
     }
 
     public boolean a(World world, int i, int j, int k) {
-        return world.a(i, j - 1, k) == this.bh ? false : (!world.c(i, j - 1, k).a() ? false : super.a(world, i, j, k));
+        return world.a(i, j - 1, k) == this.bi ? false : (!world.c(i, j - 1, k).a() ? false : super.a(world, i, j, k));
     }
 
     public boolean a() {
