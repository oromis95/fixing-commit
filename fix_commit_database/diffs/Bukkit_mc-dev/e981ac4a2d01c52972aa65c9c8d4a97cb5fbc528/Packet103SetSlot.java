@@ -3,18 +3,18 @@ package net.minecraft.server;
 import java.io.DataInputStream;
 import java.io.DataOutputStream;
 
-public class Packet103 extends Packet {
+public class Packet103SetSlot extends Packet {
 
     public int a;
     public int b;
     public ItemStack c;
 
-    public Packet103() {}
+    public Packet103SetSlot() {}
 
-    public Packet103(int i, int j, ItemStack itemstack) {
+    public Packet103SetSlot(int i, int j, ItemStack itemstack) {
         this.a = i;
         this.b = j;
-        this.c = itemstack == null ? itemstack : itemstack.d();
+        this.c = itemstack == null ? itemstack : itemstack.j();
     }
 
     public void a(NetHandler nethandler) {
@@ -28,9 +28,9 @@ public class Packet103 extends Packet {
 
         if (short1 >= 0) {
             byte b0 = datainputstream.readByte();
-            byte b1 = datainputstream.readByte();
+            short short2 = datainputstream.readShort();
 
-            this.c = new ItemStack(short1, b0, b1);
+            this.c = new ItemStack(short1, b0, short2);
         } else {
             this.c = null;
         }
@@ -44,11 +44,11 @@ public class Packet103 extends Packet {
         } else {
             dataoutputstream.writeShort(this.c.c);
             dataoutputstream.writeByte(this.c.a);
-            dataoutputstream.writeByte(this.c.d);
+            dataoutputstream.writeShort(this.c.h());
         }
     }
 
     public int a() {
-        return 7;
+        return 8;
     }
 }
