@@ -3,14 +3,14 @@ package net.minecraft.server;
 import java.io.DataInputStream;
 import java.io.DataOutputStream;
 
-public class Packet38 extends Packet {
+public class Packet38EntityStatus extends Packet {
 
     public int a;
     public byte b;
 
-    public Packet38() {}
+    public Packet38EntityStatus() {}
 
-    public Packet38(int i, byte b0) {
+    public Packet38EntityStatus(int i, byte b0) {
         this.a = i;
         this.b = b0;
     }
