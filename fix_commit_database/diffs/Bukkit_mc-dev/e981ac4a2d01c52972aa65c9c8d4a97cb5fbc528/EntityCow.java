@@ -2,11 +2,9 @@ package net.minecraft.server;
 
 public class EntityCow extends EntityAnimal {
 
-    public boolean a = false;
-
     public EntityCow(World world) {
         super(world);
-        this.aQ = "/mob/cow.png";
+        this.aP = "/mob/cow.png";
         this.a(0.9F, 1.3F);
     }
 
@@ -18,30 +16,30 @@ public class EntityCow extends EntityAnimal {
         super.b(nbttagcompound);
     }
 
-    protected String d() {
+    protected String e() {
         return "mob.cow";
     }
 
-    protected String e() {
+    protected String f() {
         return "mob.cowhurt";
     }
 
-    protected String f() {
+    protected String g() {
         return "mob.cowhurt";
     }
 
-    protected float h() {
+    protected float i() {
         return 0.4F;
     }
 
-    protected int g() {
-        return Item.LEATHER.aW;
+    protected int h() {
+        return Item.LEATHER.ba;
     }
 
     public boolean a(EntityHuman entityhuman) {
         ItemStack itemstack = entityhuman.an.e();
 
-        if (itemstack != null && itemstack.c == Item.BUCKET.aW) {
+        if (itemstack != null && itemstack.c == Item.BUCKET.ba) {
             entityhuman.an.a(entityhuman.an.c, new ItemStack(Item.MILK_BUCKET));
             return true;
         } else {
