@@ -3,7 +3,7 @@ package net.minecraft.server;
 import java.io.DataInputStream;
 import java.io.DataOutputStream;
 
-public class Packet102 extends Packet {
+public class Packet102WindowClick extends Packet {
 
     public int a;
     public int b;
@@ -11,7 +11,7 @@ public class Packet102 extends Packet {
     public short d;
     public ItemStack e;
 
-    public Packet102() {}
+    public Packet102WindowClick() {}
 
     public void a(NetHandler nethandler) {
         nethandler.a(this);
@@ -26,9 +26,9 @@ public class Packet102 extends Packet {
 
         if (short1 >= 0) {
             byte b0 = datainputstream.readByte();
-            byte b1 = datainputstream.readByte();
+            short short2 = datainputstream.readShort();
 
-            this.e = new ItemStack(short1, b0, b1);
+            this.e = new ItemStack(short1, b0, short2);
         } else {
             this.e = null;
         }
@@ -44,11 +44,11 @@ public class Packet102 extends Packet {
         } else {
             dataoutputstream.writeShort(this.e.c);
             dataoutputstream.writeByte(this.e.a);
-            dataoutputstream.writeByte(this.e.d);
+            dataoutputstream.writeShort(this.e.h());
         }
     }
 
     public int a() {
-        return 10;
+        return 11;
     }
 }
