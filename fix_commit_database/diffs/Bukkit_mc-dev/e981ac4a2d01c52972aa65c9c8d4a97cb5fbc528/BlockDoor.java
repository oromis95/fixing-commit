@@ -6,9 +6,9 @@ public class BlockDoor extends Block {
 
     protected BlockDoor(int i, Material material) {
         super(i, material);
-        this.bg = 97;
+        this.bh = 97;
         if (material == Material.e) {
-            ++this.bg;
+            ++this.bh;
         }
 
         float f = 0.5F;
@@ -27,10 +27,10 @@ public class BlockDoor extends Block {
     }
 
     public void a(IBlockAccess iblockaccess, int i, int j, int k) {
-        this.b(this.d(iblockaccess.b(i, j, k)));
+        this.c(this.d(iblockaccess.b(i, j, k)));
     }
 
-    public void b(int i) {
+    public void c(int i) {
         float f = 0.1875F;
 
         this.a(0.0F, 0.0F, 0.0F, 1.0F, 2.0F, 1.0F);
@@ -56,23 +56,23 @@ public class BlockDoor extends Block {
     }
 
     public boolean a(World world, int i, int j, int k, EntityHuman entityhuman) {
-        if (this.bs == Material.e) {
+        if (this.bt == Material.e) {
             return true;
         } else {
             int l = world.b(i, j, k);
 
             if ((l & 8) != 0) {
-                if (world.a(i, j - 1, k) == this.bh) {
+                if (world.a(i, j - 1, k) == this.bi) {
                     this.a(world, i, j - 1, k, entityhuman);
                 }
 
                 return true;
             } else {
-                if (world.a(i, j + 1, k) == this.bh) {
-                    world.b(i, j + 1, k, (l ^ 4) + 8);
+                if (world.a(i, j + 1, k) == this.bi) {
+                    world.c(i, j + 1, k, (l ^ 4) + 8);
                 }
 
-                world.b(i, j, k, l ^ 4);
+                world.c(i, j, k, l ^ 4);
                 world.b(i, j - 1, k, i, j, k);
                 if (Math.random() < 0.5D) {
                     world.a((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.door_open", 1.0F, world.l.nextFloat() * 0.1F + 0.9F);
@@ -89,18 +89,18 @@ public class BlockDoor extends Block {
         int l = world.b(i, j, k);
 
         if ((l & 8) != 0) {
-            if (world.a(i, j - 1, k) == this.bh) {
+            if (world.a(i, j - 1, k) == this.bi) {
                 this.a(world, i, j - 1, k, flag);
             }
         } else {
             boolean flag1 = (world.b(i, j, k) & 4) > 0;
 
             if (flag1 != flag) {
-                if (world.a(i, j + 1, k) == this.bh) {
-                    world.b(i, j + 1, k, (l ^ 4) + 8);
+                if (world.a(i, j + 1, k) == this.bi) {
+                    world.c(i, j + 1, k, (l ^ 4) + 8);
                 }
 
-                world.b(i, j, k, l ^ 4);
+                world.c(i, j, k, l ^ 4);
                 world.b(i, j - 1, k, i, j, k);
                 if (Math.random() < 0.5D) {
                     world.a((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.door_open", 1.0F, world.l.nextFloat() * 0.1F + 0.9F);
@@ -115,8 +115,8 @@ public class BlockDoor extends Block {
         int i1 = world.b(i, j, k);
 
         if ((i1 & 8) != 0) {
-            if (world.a(i, j - 1, k) != this.bh) {
-                world.d(i, j, k, 0);
+            if (world.a(i, j - 1, k) != this.bi) {
+                world.e(i, j, k, 0);
             }
 
             if (l > 0 && Block.m[l].c()) {
@@ -125,23 +125,23 @@ public class BlockDoor extends Block {
         } else {
             boolean flag = false;
 
-            if (world.a(i, j + 1, k) != this.bh) {
-                world.d(i, j, k, 0);
+            if (world.a(i, j + 1, k) != this.bi) {
+                world.e(i, j, k, 0);
                 flag = true;
             }
 
             if (!world.d(i, j - 1, k)) {
-                world.d(i, j, k, 0);
+                world.e(i, j, k, 0);
                 flag = true;
-                if (world.a(i, j + 1, k) == this.bh) {
-                    world.d(i, j + 1, k, 0);
+                if (world.a(i, j + 1, k) == this.bi) {
+                    world.e(i, j + 1, k, 0);
                 }
             }
 
             if (flag) {
                 this.a_(world, i, j, k, i1);
             } else if (l > 0 && Block.m[l].c()) {
-                boolean flag1 = world.o(i, j, k) || world.o(i, j + 1, k);
+                boolean flag1 = world.p(i, j, k) || world.p(i, j + 1, k);
 
                 this.a(world, i, j, k, flag1);
             }
@@ -149,7 +149,7 @@ public class BlockDoor extends Block {
     }
 
     public int a(int i, Random random) {
-        return (i & 8) != 0 ? 0 : (this.bs == Material.e ? Item.IRON_DOOR.aW : Item.WOOD_DOOR.aW);
+        return (i & 8) != 0 ? 0 : (this.bt == Material.e ? Item.IRON_DOOR.ba : Item.WOOD_DOOR.ba);
     }
 
     public MovingObjectPosition a(World world, int i, int j, int k, Vec3D vec3d, Vec3D vec3d1) {
