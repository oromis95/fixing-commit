@@ -9,13 +9,13 @@ public class EntityFlying extends EntityLiving {
     protected void a(float f) {}
 
     public void c(float f, float f1) {
-        if (this.r()) {
+        if (this.v()) {
             this.a(f, f1, 0.02F);
             this.c(this.s, this.t, this.u);
             this.s *= 0.800000011920929D;
             this.t *= 0.800000011920929D;
             this.u *= 0.800000011920929D;
-        } else if (this.t()) {
+        } else if (this.x()) {
             this.a(f, f1, 0.02F);
             this.c(this.s, this.t, this.u);
             this.s *= 0.5D;
@@ -29,7 +29,7 @@ public class EntityFlying extends EntityLiving {
                 int i = this.l.a(MathHelper.b(this.p), MathHelper.b(this.z.b) - 1, MathHelper.b(this.r));
 
                 if (i > 0) {
-                    f2 = Block.m[i].bt * 0.91F;
+                    f2 = Block.m[i].bu * 0.91F;
                 }
             }
 
@@ -42,7 +42,7 @@ public class EntityFlying extends EntityLiving {
                 int j = this.l.a(MathHelper.b(this.p), MathHelper.b(this.z.b) - 1, MathHelper.b(this.r));
 
                 if (j > 0) {
-                    f2 = Block.m[j].bt * 0.91F;
+                    f2 = Block.m[j].bu * 0.91F;
                 }
             }
 
@@ -52,7 +52,7 @@ public class EntityFlying extends EntityLiving {
             this.u *= (double) f2;
         }
 
-        this.bm = this.bn;
+        this.bl = this.bm;
         double d0 = this.p - this.m;
         double d1 = this.r - this.o;
         float f4 = MathHelper.a(d0 * d0 + d1 * d1) * 4.0F;
@@ -61,11 +61,11 @@ public class EntityFlying extends EntityLiving {
             f4 = 1.0F;
         }
 
-        this.bn += (f4 - this.bn) * 0.4F;
-        this.bo += this.bn;
+        this.bm += (f4 - this.bm) * 0.4F;
+        this.bn += this.bm;
     }
 
-    public boolean d_() {
+    public boolean m() {
         return false;
     }
 }
