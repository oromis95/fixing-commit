@@ -6,26 +6,26 @@ public class EntityPigZombie extends EntityZombie {
 
     private int a = 0;
     private int b = 0;
-    private static final ItemStack c = new ItemStack(Item.GOLD_SWORD, 1);
+    private static final ItemStack f = new ItemStack(Item.GOLD_SWORD, 1);
 
     public EntityPigZombie(World world) {
         super(world);
-        this.aQ = "/mob/pigzombie.png";
-        this.bD = 0.5F;
-        this.f = 5;
+        this.aP = "/mob/pigzombie.png";
+        this.bC = 0.5F;
+        this.c = 5;
         this.ae = true;
     }
 
     public void b_() {
-        this.bD = this.aj != null ? 0.95F : 0.5F;
+        this.bC = this.d != null ? 0.95F : 0.5F;
         if (this.b > 0 && --this.b == 0) {
-            this.l.a(this, "mob.zombiepig.zpigangry", this.h() * 2.0F, ((this.W.nextFloat() - this.W.nextFloat()) * 0.2F + 1.0F) * 1.8F);
+            this.l.a(this, "mob.zombiepig.zpigangry", this.i() * 2.0F, ((this.W.nextFloat() - this.W.nextFloat()) * 0.2F + 1.0F) * 1.8F);
         }
 
         super.b_();
     }
 
-    public boolean a() {
+    public boolean b() {
         return this.l.k > 0 && this.l.a(this.z) && this.l.a((Entity) this, this.z).size() == 0 && !this.l.b(this.z);
     }
 
@@ -39,12 +39,12 @@ public class EntityPigZombie extends EntityZombie {
         this.a = nbttagcompound.c("Anger");
     }
 
-    protected Entity k() {
-        return this.a == 0 ? null : super.k();
+    protected Entity l() {
+        return this.a == 0 ? null : super.l();
     }
 
-    public void G() {
-        super.G();
+    public void o() {
+        super.o();
     }
 
     public boolean a(Entity entity, int i) {
@@ -68,24 +68,24 @@ public class EntityPigZombie extends EntityZombie {
     }
 
     private void g(Entity entity) {
-        this.aj = entity;
+        this.d = entity;
         this.a = 400 + this.W.nextInt(400);
         this.b = this.W.nextInt(40);
     }
 
-    protected String d() {
+    protected String e() {
         return "mob.zombiepig.zpig";
     }
 
-    protected String e() {
+    protected String f() {
         return "mob.zombiepig.zpighurt";
     }
 
-    protected String f() {
+    protected String g() {
         return "mob.zombiepig.zpigdeath";
     }
 
-    protected int g() {
-        return Item.GRILLED_PORK.aW;
+    protected int h() {
+        return Item.GRILLED_PORK.ba;
     }
 }
