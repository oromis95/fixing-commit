@@ -9,11 +9,11 @@ public class BlockFurnace extends BlockContainer {
     protected BlockFurnace(int i, boolean flag) {
         super(i, Material.d);
         this.a = flag;
-        this.bg = 45;
+        this.bh = 45;
     }
 
     public int a(int i, Random random) {
-        return Block.FURNACE.bh;
+        return Block.FURNACE.bi;
     }
 
     public void e(World world, int i, int j, int k) {
@@ -44,18 +44,18 @@ public class BlockFurnace extends BlockContainer {
             b0 = 4;
         }
 
-        world.b(i, j, k, b0);
+        world.c(i, j, k, b0);
     }
 
     public int a(int i) {
-        return i == 1 ? Block.STONE.bh : (i == 0 ? Block.STONE.bh : (i == 3 ? this.bg - 1 : this.bg));
+        return i == 1 ? this.bh + 17 : (i == 0 ? this.bh + 17 : (i == 3 ? this.bh - 1 : this.bh));
     }
 
     public boolean a(World world, int i, int j, int k, EntityHuman entityhuman) {
         if (world.z) {
             return true;
         } else {
-            TileEntityFurnace tileentityfurnace = (TileEntityFurnace) world.l(i, j, k);
+            TileEntityFurnace tileentityfurnace = (TileEntityFurnace) world.m(i, j, k);
 
             entityhuman.a(tileentityfurnace);
             return true;
@@ -64,15 +64,15 @@ public class BlockFurnace extends BlockContainer {
 
     public static void a(boolean flag, World world, int i, int j, int k) {
         int l = world.b(i, j, k);
-        TileEntity tileentity = world.l(i, j, k);
+        TileEntity tileentity = world.m(i, j, k);
 
         if (flag) {
-            world.d(i, j, k, Block.BURNING_FURNACE.bh);
+            world.e(i, j, k, Block.BURNING_FURNACE.bi);
         } else {
-            world.d(i, j, k, Block.FURNACE.bh);
+            world.e(i, j, k, Block.FURNACE.bi);
         }
 
-        world.b(i, j, k, l);
+        world.c(i, j, k, l);
         world.a(i, j, k, tileentity);
     }
 
@@ -84,19 +84,19 @@ public class BlockFurnace extends BlockContainer {
         int l = MathHelper.b((double) (entityliving.v * 4.0F / 360.0F) + 0.5D) & 3;
 
         if (l == 0) {
-            world.b(i, j, k, 2);
+            world.c(i, j, k, 2);
         }
 
         if (l == 1) {
-            world.b(i, j, k, 5);
+            world.c(i, j, k, 5);
         }
 
         if (l == 2) {
-            world.b(i, j, k, 3);
+            world.c(i, j, k, 3);
         }
 
         if (l == 3) {
-            world.b(i, j, k, 4);
+            world.c(i, j, k, 4);
         }
     }
 }
