@@ -46,18 +46,18 @@ public class BlockButton extends Block {
             i1 = 1;
         }
 
-        world.b(i, j, k, i1 + j1);
+        world.c(i, j, k, i1 + j1);
     }
 
     public void e(World world, int i, int j, int k) {
         if (world.d(i - 1, j, k)) {
-            world.b(i, j, k, 1);
+            world.c(i, j, k, 1);
         } else if (world.d(i + 1, j, k)) {
-            world.b(i, j, k, 2);
+            world.c(i, j, k, 2);
         } else if (world.d(i, j, k - 1)) {
-            world.b(i, j, k, 3);
+            world.c(i, j, k, 3);
         } else if (world.d(i, j, k + 1)) {
-            world.b(i, j, k, 4);
+            world.c(i, j, k, 4);
         }
 
         this.g(world, i, j, k);
@@ -86,7 +86,7 @@ public class BlockButton extends Block {
 
             if (flag) {
                 this.a_(world, i, j, k, world.b(i, j, k));
-                world.d(i, j, k, 0);
+                world.e(i, j, k, 0);
             }
         }
     }
@@ -94,7 +94,7 @@ public class BlockButton extends Block {
     private boolean g(World world, int i, int j, int k) {
         if (!this.a(world, i, j, k)) {
             this.a_(world, i, j, k, world.b(i, j, k));
-            world.d(i, j, k, 0);
+            world.e(i, j, k, 0);
             return false;
         } else {
             return true;
@@ -140,23 +140,23 @@ public class BlockButton extends Block {
             if (j1 == 0) {
                 return true;
             } else {
-                world.b(i, j, k, i1 + j1);
+                world.c(i, j, k, i1 + j1);
                 world.b(i, j, k, i, j, k);
                 world.a((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.click", 0.3F, 0.6F);
-                world.g(i, j, k, this.bh);
+                world.h(i, j, k, this.bi);
                 if (i1 == 1) {
-                    world.g(i - 1, j, k, this.bh);
+                    world.h(i - 1, j, k, this.bi);
                 } else if (i1 == 2) {
-                    world.g(i + 1, j, k, this.bh);
+                    world.h(i + 1, j, k, this.bi);
                 } else if (i1 == 3) {
-                    world.g(i, j, k - 1, this.bh);
+                    world.h(i, j, k - 1, this.bi);
                 } else if (i1 == 4) {
-                    world.g(i, j, k + 1, this.bh);
+                    world.h(i, j, k + 1, this.bi);
                 } else {
-                    world.g(i, j - 1, k, this.bh);
+                    world.h(i, j - 1, k, this.bi);
                 }
 
-                world.h(i, j, k, this.bh);
+                world.i(i, j, k, this.bi);
                 return true;
             }
         }
@@ -166,19 +166,19 @@ public class BlockButton extends Block {
         int l = world.b(i, j, k);
 
         if ((l & 8) > 0) {
-            world.g(i, j, k, this.bh);
+            world.h(i, j, k, this.bi);
             int i1 = l & 7;
 
             if (i1 == 1) {
-                world.g(i - 1, j, k, this.bh);
+                world.h(i - 1, j, k, this.bi);
             } else if (i1 == 2) {
-                world.g(i + 1, j, k, this.bh);
+                world.h(i + 1, j, k, this.bi);
             } else if (i1 == 3) {
-                world.g(i, j, k - 1, this.bh);
+                world.h(i, j, k - 1, this.bi);
             } else if (i1 == 4) {
-                world.g(i, j, k + 1, this.bh);
+                world.h(i, j, k + 1, this.bi);
             } else {
-                world.g(i, j - 1, k, this.bh);
+                world.h(i, j - 1, k, this.bi);
             }
         }
 
@@ -210,20 +210,20 @@ public class BlockButton extends Block {
             int l = world.b(i, j, k);
 
             if ((l & 8) != 0) {
-                world.b(i, j, k, l & 7);
-                world.g(i, j, k, this.bh);
+                world.c(i, j, k, l & 7);
+                world.h(i, j, k, this.bi);
                 int i1 = l & 7;
 
                 if (i1 == 1) {
-                    world.g(i - 1, j, k, this.bh);
+                    world.h(i - 1, j, k, this.bi);
                 } else if (i1 == 2) {
-                    world.g(i + 1, j, k, this.bh);
+                    world.h(i + 1, j, k, this.bi);
                 } else if (i1 == 3) {
-                    world.g(i, j, k - 1, this.bh);
+                    world.h(i, j, k - 1, this.bi);
                 } else if (i1 == 4) {
-                    world.g(i, j, k + 1, this.bh);
+                    world.h(i, j, k + 1, this.bi);
                 } else {
-                    world.g(i, j - 1, k, this.bh);
+                    world.h(i, j - 1, k, this.bi);
                 }
 
                 world.a((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.click", 0.3F, 0.5F);
