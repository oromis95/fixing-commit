@@ -3,14 +3,14 @@ package net.minecraft.server;
 import java.io.DataInputStream;
 import java.io.DataOutputStream;
 
-public class Packet39 extends Packet {
+public class Packet39AttachEntity extends Packet {
 
     public int a;
     public int b;
 
-    public Packet39() {}
+    public Packet39AttachEntity() {}
 
-    public Packet39(Entity entity, Entity entity1) {
+    public Packet39AttachEntity(Entity entity, Entity entity1) {
         this.a = entity.g;
         this.b = entity1 != null ? entity1.g : -1;
     }
