@@ -4,7 +4,7 @@ public class BlockSponge extends Block {
 
     protected BlockSponge(int i) {
         super(i, Material.j);
-        this.bg = 48;
+        this.bh = 48;
     }
 
     public void e(World world, int i, int j, int k) {
@@ -27,7 +27,7 @@ public class BlockSponge extends Block {
         for (int l = i - b0; l <= i + b0; ++l) {
             for (int i1 = j - b0; i1 <= j + b0; ++i1) {
                 for (int j1 = k - b0; j1 <= k + b0; ++j1) {
-                    world.g(l, i1, j1, world.a(l, i1, j1));
+                    world.h(l, i1, j1, world.a(l, i1, j1));
                 }
             }
         }
