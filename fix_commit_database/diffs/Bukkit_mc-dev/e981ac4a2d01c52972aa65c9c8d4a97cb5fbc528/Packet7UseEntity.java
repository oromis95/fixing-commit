@@ -3,13 +3,13 @@ package net.minecraft.server;
 import java.io.DataInputStream;
 import java.io.DataOutputStream;
 
-public class Packet7 extends Packet {
+public class Packet7UseEntity extends Packet {
 
     public int a;
     public int b;
     public int c;
 
-    public Packet7() {}
+    public Packet7UseEntity() {}
 
     public void a(DataInputStream datainputstream) {
         this.a = datainputstream.readInt();
