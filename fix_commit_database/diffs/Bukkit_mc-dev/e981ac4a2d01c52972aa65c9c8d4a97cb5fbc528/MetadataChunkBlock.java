@@ -26,96 +26,130 @@ public class MetadataChunkBlock {
         int k = this.g - this.d + 1;
         int l = i * j * k;
 
-        if (l <= '\u8000') {
-            for (int i1 = this.b; i1 <= this.e; ++i1) {
-                for (int j1 = this.d; j1 <= this.g; ++j1) {
-                    if (world.f(i1, 0, j1)) {
-                        for (int k1 = this.c; k1 <= this.f; ++k1) {
-                            if (k1 >= 0 && k1 < 128) {
-                                int l1 = world.a(this.a, i1, k1, j1);
-                                boolean flag = false;
-                                int i2 = world.a(i1, k1, j1);
-                                int j2 = Block.q[i2];
-
-                                if (j2 == 0) {
-                                    j2 = 1;
+        if (l > '\u8000') {
+            System.out.println("Light too large, skipping!");
+        } else {
+            int i1 = 0;
+            int j1 = 0;
+            boolean flag = false;
+            boolean flag1 = false;
+
+            for (int k1 = this.b; k1 <= this.e; ++k1) {
+                for (int l1 = this.d; l1 <= this.g; ++l1) {
+                    int i2 = k1 >> 4;
+                    int j2 = l1 >> 4;
+                    boolean flag2 = false;
+
+                    if (flag && i2 == i1 && j2 == j1) {
+                        flag2 = flag1;
+                    } else {
+                        flag2 = world.a(k1, 0, l1, 1);
+                        if (flag2) {
+                            Chunk chunk = world.c(k1 >> 4, l1 >> 4);
+
+                            if (chunk.g()) {
+                                flag2 = false;
+                            }
+                        }
+
+                        flag1 = flag2;
+                        i1 = i2;
+                        j1 = j2;
+                    }
+
+                    if (flag2) {
+                        if (this.c < 0) {
+                            this.c = 0;
+                        }
+
+                        if (this.f >= 128) {
+                            this.f = 127;
+                        }
+
+                        for (int k2 = this.c; k2 <= this.f; ++k2) {
+                            int l2 = world.a(this.a, k1, k2, l1);
+                            boolean flag3 = false;
+                            int i3 = world.a(k1, k2, l1);
+                            int j3 = Block.q[i3];
+
+                            if (j3 == 0) {
+                                j3 = 1;
+                            }
+
+                            int k3 = 0;
+
+                            if (this.a == EnumSkyBlock.SKY) {
+                                if (world.k(k1, k2, l1)) {
+                                    k3 = 15;
                                 }
+                            } else if (this.a == EnumSkyBlock.BLOCK) {
+                                k3 = Block.s[i3];
+                            }
 
-                                int k2 = 0;
+                            int l3;
+                            int i4;
+
+                            if (j3 >= 15 && k3 == 0) {
+                                i4 = 0;
+                            } else {
+                                l3 = world.a(this.a, k1 - 1, k2, l1);
+                                int j4 = world.a(this.a, k1 + 1, k2, l1);
+                                int k4 = world.a(this.a, k1, k2 - 1, l1);
+                                int l4 = world.a(this.a, k1, k2 + 1, l1);
+                                int i5 = world.a(this.a, k1, k2, l1 - 1);
+                                int j5 = world.a(this.a, k1, k2, l1 + 1);
+
+                                i4 = l3;
+                                if (j4 > l3) {
+                                    i4 = j4;
+                                }
+
+                                if (k4 > i4) {
+                                    i4 = k4;
+                                }
+
+                                if (l4 > i4) {
+                                    i4 = l4;
+                                }
+
+                                if (i5 > i4) {
+                                    i4 = i5;
+                                }
+
+                                if (j5 > i4) {
+                                    i4 = j5;
+                                }
+
+                                i4 -= j3;
+                                if (i4 < 0) {
+                                    i4 = 0;
+                                }
+
+                                if (k3 > i4) {
+                                    i4 = k3;
+                                }
+                            }
+
+                            if (l2 != i4) {
+                                world.b(this.a, k1, k2, l1, i4);
+                                l3 = i4 - 1;
+                                if (l3 < 0) {
+                                    l3 = 0;
+                                }
 
-                                if (this.a == EnumSkyBlock.SKY) {
-                                    if (world.j(i1, k1, j1)) {
-                                        k2 = 15;
-                                    }
-                                } else if (this.a == EnumSkyBlock.BLOCK) {
-                                    k2 = Block.s[i2];
+                                world.a(this.a, k1 - 1, k2, l1, l3);
+                                world.a(this.a, k1, k2 - 1, l1, l3);
+                                world.a(this.a, k1, k2, l1 - 1, l3);
+                                if (k1 + 1 >= this.e) {
+                                    world.a(this.a, k1 + 1, k2, l1, l3);
                                 }
 
-                                int l2;
-                                int i3;
-
-                                if (j2 >= 15 && k2 == 0) {
-                                    i3 = 0;
-                                } else {
-                                    l2 = world.a(this.a, i1 - 1, k1, j1);
-                                    int j3 = world.a(this.a, i1 + 1, k1, j1);
-                                    int k3 = world.a(this.a, i1, k1 - 1, j1);
-                                    int l3 = world.a(this.a, i1, k1 + 1, j1);
-                                    int i4 = world.a(this.a, i1, k1, j1 - 1);
-                                    int j4 = world.a(this.a, i1, k1, j1 + 1);
-
-                                    i3 = l2;
-                                    if (j3 > l2) {
-                                        i3 = j3;
-                                    }
-
-                                    if (k3 > i3) {
-                                        i3 = k3;
-                                    }
-
-                                    if (l3 > i3) {
-                                        i3 = l3;
-                                    }
-
-                                    if (i4 > i3) {
-                                        i3 = i4;
-                                    }
-
-                                    if (j4 > i3) {
-                                        i3 = j4;
-                                    }
-
-                                    i3 -= j2;
-                                    if (i3 < 0) {
-                                        i3 = 0;
-                                    }
-
-                                    if (k2 > i3) {
-                                        i3 = k2;
-                                    }
+                                if (k2 + 1 >= this.f) {
+                                    world.a(this.a, k1, k2 + 1, l1, l3);
                                 }
 
-                                if (l1 != i3) {
-                                    world.b(this.a, i1, k1, j1, i3);
-                                    l2 = i3 - 1;
-                                    if (l2 < 0) {
-                                        l2 = 0;
-                                    }
-
-                                    world.a(this.a, i1 - 1, k1, j1, l2);
-                                    world.a(this.a, i1, k1 - 1, j1, l2);
-                                    world.a(this.a, i1, k1, j1 - 1, l2);
-                                    if (i1 + 1 >= this.e) {
-                                        world.a(this.a, i1 + 1, k1, j1, l2);
-                                    }
-
-                                    if (k1 + 1 >= this.f) {
-                                        world.a(this.a, i1, k1 + 1, j1, l2);
-                                    }
-
-                                    if (j1 + 1 >= this.g) {
-                                        world.a(this.a, i1, k1, j1 + 1, l2);
-                                    }
+                                if (l1 + 1 >= this.g) {
+                                    world.a(this.a, k1, k2, l1 + 1, l3);
                                 }
                             }
                         }
