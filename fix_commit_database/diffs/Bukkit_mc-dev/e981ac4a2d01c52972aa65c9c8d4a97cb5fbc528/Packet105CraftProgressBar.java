@@ -3,15 +3,15 @@ package net.minecraft.server;
 import java.io.DataInputStream;
 import java.io.DataOutputStream;
 
-public class Packet105 extends Packet {
+public class Packet105CraftProgressBar extends Packet {
 
     public int a;
     public int b;
     public int c;
 
-    public Packet105() {}
+    public Packet105CraftProgressBar() {}
 
-    public Packet105(int i, int j, int k) {
+    public Packet105CraftProgressBar(int i, int j, int k) {
         this.a = i;
         this.b = j;
         this.c = k;
