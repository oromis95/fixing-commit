@@ -43,7 +43,7 @@ public class BlockPressurePlate extends Block {
 
         if (flag) {
             this.a_(world, i, j, k, world.b(i, j, k));
-            world.d(i, j, k, 0);
+            world.e(i, j, k, 0);
         }
     }
 
@@ -86,23 +86,23 @@ public class BlockPressurePlate extends Block {
         }
 
         if (flag1 && !flag) {
-            world.b(i, j, k, 1);
-            world.g(i, j, k, this.bh);
-            world.g(i, j - 1, k, this.bh);
+            world.c(i, j, k, 1);
+            world.h(i, j, k, this.bi);
+            world.h(i, j - 1, k, this.bi);
             world.b(i, j, k, i, j, k);
             world.a((double) i + 0.5D, (double) j + 0.1D, (double) k + 0.5D, "random.click", 0.3F, 0.6F);
         }
 
         if (!flag1 && flag) {
-            world.b(i, j, k, 0);
-            world.g(i, j, k, this.bh);
-            world.g(i, j - 1, k, this.bh);
+            world.c(i, j, k, 0);
+            world.h(i, j, k, this.bi);
+            world.h(i, j - 1, k, this.bi);
             world.b(i, j, k, i, j, k);
             world.a((double) i + 0.5D, (double) j + 0.1D, (double) k + 0.5D, "random.click", 0.3F, 0.5F);
         }
 
         if (flag1) {
-            world.h(i, j, k, this.bh);
+            world.i(i, j, k, this.bi);
         }
     }
 
@@ -110,8 +110,8 @@ public class BlockPressurePlate extends Block {
         int l = world.b(i, j, k);
 
         if (l > 0) {
-            world.g(i, j, k, this.bh);
-            world.g(i, j - 1, k, this.bh);
+            world.h(i, j, k, this.bi);
+            world.h(i, j - 1, k, this.bi);
         }
 
         super.b(world, i, j, k);
