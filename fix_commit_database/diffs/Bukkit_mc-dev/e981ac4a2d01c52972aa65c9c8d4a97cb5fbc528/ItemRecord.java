@@ -7,12 +7,12 @@ public class ItemRecord extends Item {
     protected ItemRecord(int i, String s) {
         super(i);
         this.a = s;
-        this.aX = 1;
+        this.bb = 1;
     }
 
     public boolean a(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
-        if (world.a(i, j, k) == Block.JUKEBOX.bh && world.b(i, j, k) == 0) {
-            world.b(i, j, k, this.aW - Item.GOLD_RECORD.aW + 1);
+        if (world.a(i, j, k) == Block.JUKEBOX.bi && world.b(i, j, k) == 0) {
+            world.c(i, j, k, this.ba - Item.GOLD_RECORD.ba + 1);
             world.a(this.a, i, j, k);
             --itemstack.a;
             return true;
