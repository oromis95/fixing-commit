@@ -6,7 +6,7 @@ public class BlockReed extends Block {
 
     protected BlockReed(int i, int j) {
         super(i, Material.i);
-        this.bg = j;
+        this.bh = j;
         float f = 0.375F;
 
         this.a(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, 1.0F, 0.5F + f);
@@ -17,7 +17,7 @@ public class BlockReed extends Block {
         if (world.e(i, j + 1, k)) {
             int l;
 
-            for (l = 1; world.a(i, j - l, k) == this.bh; ++l) {
+            for (l = 1; world.a(i, j - l, k) == this.bi; ++l) {
                 ;
             }
 
@@ -25,10 +25,10 @@ public class BlockReed extends Block {
                 int i1 = world.b(i, j, k);
 
                 if (i1 == 15) {
-                    world.d(i, j + 1, k, this.bh);
-                    world.b(i, j, k, 0);
+                    world.e(i, j + 1, k, this.bi);
+                    world.c(i, j, k, 0);
                 } else {
-                    world.b(i, j, k, i1 + 1);
+                    world.c(i, j, k, i1 + 1);
                 }
             }
         }
@@ -37,7 +37,7 @@ public class BlockReed extends Block {
     public boolean a(World world, int i, int j, int k) {
         int l = world.a(i, j - 1, k);
 
-        return l == this.bh ? true : (l != Block.GRASS.bh && l != Block.DIRT.bh ? false : (world.c(i - 1, j - 1, k) == Material.f ? true : (world.c(i + 1, j - 1, k) == Material.f ? true : (world.c(i, j - 1, k - 1) == Material.f ? true : world.c(i, j - 1, k + 1) == Material.f))));
+        return l == this.bi ? true : (l != Block.GRASS.bi && l != Block.DIRT.bi ? false : (world.c(i - 1, j - 1, k) == Material.f ? true : (world.c(i + 1, j - 1, k) == Material.f ? true : (world.c(i, j - 1, k - 1) == Material.f ? true : world.c(i, j - 1, k + 1) == Material.f))));
     }
 
     public void b(World world, int i, int j, int k, int l) {
@@ -47,7 +47,7 @@ public class BlockReed extends Block {
     protected final void g(World world, int i, int j, int k) {
         if (!this.f(world, i, j, k)) {
             this.a_(world, i, j, k, world.b(i, j, k));
-            world.d(i, j, k, 0);
+            world.e(i, j, k, 0);
         }
     }
 
@@ -60,7 +60,7 @@ public class BlockReed extends Block {
     }
 
     public int a(int i, Random random) {
-        return Item.SUGAR_CANE.aW;
+        return Item.SUGAR_CANE.ba;
     }
 
     public boolean a() {
