@@ -3,19 +3,19 @@ package net.minecraft.server;
 import java.io.DataInputStream;
 import java.io.DataOutputStream;
 
-public class Packet130 extends Packet {
+public class Packet130UpdateSign extends Packet {
 
     public int a;
     public int b;
     public int c;
     public String[] d;
 
-    public Packet130() {
-        this.k = true;
+    public Packet130UpdateSign() {
+        this.l = true;
     }
 
-    public Packet130(int i, int j, int k, String[] astring) {
-        this.k = true;
+    public Packet130UpdateSign(int i, int j, int k, String[] astring) {
+        this.l = true;
         this.a = i;
         this.b = j;
         this.c = k;
