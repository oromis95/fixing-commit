@@ -11,30 +11,30 @@ public class WorldGenHellLava extends WorldGenerator {
     }
 
     public boolean a(World world, Random random, int i, int j, int k) {
-        if (world.a(i, j + 1, k) != Block.NETHERRACK.bh) {
+        if (world.a(i, j + 1, k) != Block.NETHERRACK.bi) {
             return false;
-        } else if (world.a(i, j, k) != 0 && world.a(i, j, k) != Block.NETHERRACK.bh) {
+        } else if (world.a(i, j, k) != 0 && world.a(i, j, k) != Block.NETHERRACK.bi) {
             return false;
         } else {
             int l = 0;
 
-            if (world.a(i - 1, j, k) == Block.NETHERRACK.bh) {
+            if (world.a(i - 1, j, k) == Block.NETHERRACK.bi) {
                 ++l;
             }
 
-            if (world.a(i + 1, j, k) == Block.NETHERRACK.bh) {
+            if (world.a(i + 1, j, k) == Block.NETHERRACK.bi) {
                 ++l;
             }
 
-            if (world.a(i, j, k - 1) == Block.NETHERRACK.bh) {
+            if (world.a(i, j, k - 1) == Block.NETHERRACK.bi) {
                 ++l;
             }
 
-            if (world.a(i, j, k + 1) == Block.NETHERRACK.bh) {
+            if (world.a(i, j, k + 1) == Block.NETHERRACK.bi) {
                 ++l;
             }
 
-            if (world.a(i, j - 1, k) == Block.NETHERRACK.bh) {
+            if (world.a(i, j - 1, k) == Block.NETHERRACK.bi) {
                 ++l;
             }
 
@@ -61,7 +61,7 @@ public class WorldGenHellLava extends WorldGenerator {
             }
 
             if (l == 4 && i1 == 1) {
-                world.d(i, j, k, this.a);
+                world.e(i, j, k, this.a);
                 world.a = true;
                 Block.m[this.a].a(world, i, j, k, random);
                 world.a = false;
