@@ -4,7 +4,7 @@ public class ItemBoat extends Item {
 
     public ItemBoat(int i) {
         super(i);
-        this.aX = 1;
+        this.bb = 1;
     }
 
     public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
@@ -28,7 +28,7 @@ public class ItemBoat extends Item {
         if (movingobjectposition == null) {
             return itemstack;
         } else {
-            if (movingobjectposition.a == 0) {
+            if (movingobjectposition.a == EnumMovingObjectType.TILE) {
                 int i = movingobjectposition.b;
                 int j = movingobjectposition.c;
                 int k = movingobjectposition.d;
