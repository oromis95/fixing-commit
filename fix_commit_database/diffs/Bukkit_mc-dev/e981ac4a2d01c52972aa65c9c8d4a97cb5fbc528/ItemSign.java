@@ -4,8 +4,8 @@ public class ItemSign extends Item {
 
     public ItemSign(int i) {
         super(i);
-        this.aY = 64;
-        this.aX = 1;
+        this.bc = 64;
+        this.bb = 1;
     }
 
     public boolean a(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
@@ -38,13 +38,13 @@ public class ItemSign extends Item {
                 return false;
             } else {
                 if (l == 1) {
-                    world.b(i, j, k, Block.SIGN_POST.bh, MathHelper.b((double) ((entityhuman.v + 180.0F) * 16.0F / 360.0F) + 0.5D) & 15);
+                    world.b(i, j, k, Block.SIGN_POST.bi, MathHelper.b((double) ((entityhuman.v + 180.0F) * 16.0F / 360.0F) + 0.5D) & 15);
                 } else {
-                    world.b(i, j, k, Block.WALL_SIGN.bh, l);
+                    world.b(i, j, k, Block.WALL_SIGN.bi, l);
                 }
 
                 --itemstack.a;
-                TileEntitySign tileentitysign = (TileEntitySign) world.l(i, j, k);
+                TileEntitySign tileentitysign = (TileEntitySign) world.m(i, j, k);
 
                 if (tileentitysign != null) {
                     entityhuman.a(tileentitysign);
