@@ -3,15 +3,15 @@ package net.minecraft.server;
 import java.io.DataInputStream;
 import java.io.DataOutputStream;
 
-public class Packet106 extends Packet {
+public class Packet106Transaction extends Packet {
 
     public int a;
     public short b;
     public boolean c;
 
-    public Packet106() {}
+    public Packet106Transaction() {}
 
-    public Packet106(int i, short short1, boolean flag) {
+    public Packet106Transaction(int i, short short1, boolean flag) {
         this.a = i;
         this.b = short1;
         this.c = flag;
