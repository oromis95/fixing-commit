@@ -4,36 +4,36 @@ public class EntityZombie extends EntityMonster {
 
     public EntityZombie(World world) {
         super(world);
-        this.aQ = "/mob/zombie.png";
-        this.bD = 0.5F;
-        this.f = 5;
+        this.aP = "/mob/zombie.png";
+        this.bC = 0.5F;
+        this.c = 5;
     }
 
-    public void G() {
+    public void o() {
         if (this.l.b()) {
             float f = this.b(1.0F);
 
-            if (f > 0.5F && this.l.h(MathHelper.b(this.p), MathHelper.b(this.q), MathHelper.b(this.r)) && this.W.nextFloat() * 30.0F < (f - 0.4F) * 2.0F) {
+            if (f > 0.5F && this.l.i(MathHelper.b(this.p), MathHelper.b(this.q), MathHelper.b(this.r)) && this.W.nextFloat() * 30.0F < (f - 0.4F) * 2.0F) {
                 this.Z = 300;
             }
         }
 
-        super.G();
+        super.o();
     }
 
-    protected String d() {
+    protected String e() {
         return "mob.zombie";
     }
 
-    protected String e() {
+    protected String f() {
         return "mob.zombiehurt";
     }
 
-    protected String f() {
+    protected String g() {
         return "mob.zombiedeath";
     }
 
-    protected int g() {
-        return Item.FEATHER.aW;
+    protected int h() {
+        return Item.FEATHER.ba;
     }
 }
