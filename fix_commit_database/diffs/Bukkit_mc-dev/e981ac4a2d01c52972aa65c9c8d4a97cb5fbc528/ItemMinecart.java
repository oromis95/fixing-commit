@@ -6,14 +6,14 @@ public class ItemMinecart extends Item {
 
     public ItemMinecart(int i, int j) {
         super(i);
-        this.aX = 1;
+        this.bb = 1;
         this.a = j;
     }
 
     public boolean a(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
         int i1 = world.a(i, j, k);
 
-        if (i1 == Block.RAILS.bh) {
+        if (i1 == Block.RAILS.bi) {
             if (!world.z) {
                 world.a((Entity) (new EntityMinecart(world, (double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), this.a)));
             }
