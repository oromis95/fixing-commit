@@ -6,7 +6,7 @@ public class BlockFlower extends Block {
 
     protected BlockFlower(int i, int j) {
         super(i, Material.i);
-        this.bg = j;
+        this.bh = j;
         this.a(true);
         float f = 0.2F;
 
@@ -14,11 +14,11 @@ public class BlockFlower extends Block {
     }
 
     public boolean a(World world, int i, int j, int k) {
-        return this.b(world.a(i, j - 1, k));
+        return this.c(world.a(i, j - 1, k));
     }
 
-    protected boolean b(int i) {
-        return i == Block.GRASS.bh || i == Block.DIRT.bh || i == Block.SOIL.bh;
+    protected boolean c(int i) {
+        return i == Block.GRASS.bi || i == Block.DIRT.bi || i == Block.SOIL.bi;
     }
 
     public void b(World world, int i, int j, int k, int l) {
@@ -33,12 +33,12 @@ public class BlockFlower extends Block {
     protected final void g(World world, int i, int j, int k) {
         if (!this.f(world, i, j, k)) {
             this.a_(world, i, j, k, world.b(i, j, k));
-            world.d(i, j, k, 0);
+            world.e(i, j, k, 0);
         }
     }
 
     public boolean f(World world, int i, int j, int k) {
-        return (world.i(i, j, k) >= 8 || world.h(i, j, k)) && this.b(world.a(i, j - 1, k));
+        return (world.j(i, j, k) >= 8 || world.i(i, j, k)) && this.c(world.a(i, j - 1, k));
     }
 
     public AxisAlignedBB d(World world, int i, int j, int k) {
