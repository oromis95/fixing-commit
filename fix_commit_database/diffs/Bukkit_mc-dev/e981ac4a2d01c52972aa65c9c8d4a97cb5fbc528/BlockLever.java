@@ -43,20 +43,20 @@ public class BlockLever extends Block {
             i1 = 1;
         }
 
-        world.b(i, j, k, i1 + j1);
+        world.c(i, j, k, i1 + j1);
     }
 
     public void e(World world, int i, int j, int k) {
         if (world.d(i - 1, j, k)) {
-            world.b(i, j, k, 1);
+            world.c(i, j, k, 1);
         } else if (world.d(i + 1, j, k)) {
-            world.b(i, j, k, 2);
+            world.c(i, j, k, 2);
         } else if (world.d(i, j, k - 1)) {
-            world.b(i, j, k, 3);
+            world.c(i, j, k, 3);
         } else if (world.d(i, j, k + 1)) {
-            world.b(i, j, k, 4);
+            world.c(i, j, k, 4);
         } else if (world.d(i, j - 1, k)) {
-            world.b(i, j, k, 5 + world.l.nextInt(2));
+            world.c(i, j, k, 5 + world.l.nextInt(2));
         }
 
         this.g(world, i, j, k);
@@ -89,7 +89,7 @@ public class BlockLever extends Block {
 
             if (flag) {
                 this.a_(world, i, j, k, world.b(i, j, k));
-                world.d(i, j, k, 0);
+                world.e(i, j, k, 0);
             }
         }
     }
@@ -97,7 +97,7 @@ public class BlockLever extends Block {
     private boolean g(World world, int i, int j, int k) {
         if (!this.a(world, i, j, k)) {
             this.a_(world, i, j, k, world.b(i, j, k));
-            world.d(i, j, k, 0);
+            world.e(i, j, k, 0);
             return false;
         } else {
             return true;
@@ -134,20 +134,20 @@ public class BlockLever extends Block {
             int i1 = l & 7;
             int j1 = 8 - (l & 8);
 
-            world.b(i, j, k, i1 + j1);
+            world.c(i, j, k, i1 + j1);
             world.b(i, j, k, i, j, k);
             world.a((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.click", 0.3F, j1 > 0 ? 0.6F : 0.5F);
-            world.g(i, j, k, this.bh);
+            world.h(i, j, k, this.bi);
             if (i1 == 1) {
-                world.g(i - 1, j, k, this.bh);
+                world.h(i - 1, j, k, this.bi);
             } else if (i1 == 2) {
-                world.g(i + 1, j, k, this.bh);
+                world.h(i + 1, j, k, this.bi);
             } else if (i1 == 3) {
-                world.g(i, j, k - 1, this.bh);
+                world.h(i, j, k - 1, this.bi);
             } else if (i1 == 4) {
-                world.g(i, j, k + 1, this.bh);
+                world.h(i, j, k + 1, this.bi);
             } else {
-                world.g(i, j - 1, k, this.bh);
+                world.h(i, j - 1, k, this.bi);
             }
 
             return true;
@@ -158,19 +158,19 @@ public class BlockLever extends Block {
         int l = world.b(i, j, k);
 
         if ((l & 8) > 0) {
-            world.g(i, j, k, this.bh);
+            world.h(i, j, k, this.bi);
             int i1 = l & 7;
 
             if (i1 == 1) {
-                world.g(i - 1, j, k, this.bh);
+                world.h(i - 1, j, k, this.bi);
             } else if (i1 == 2) {
-                world.g(i + 1, j, k, this.bh);
+                world.h(i + 1, j, k, this.bi);
             } else if (i1 == 3) {
-                world.g(i, j, k - 1, this.bh);
+                world.h(i, j, k - 1, this.bi);
             } else if (i1 == 4) {
-                world.g(i, j, k + 1, this.bh);
+                world.h(i, j, k + 1, this.bi);
             } else {
-                world.g(i, j - 1, k, this.bh);
+                world.h(i, j - 1, k, this.bi);
             }
         }
 
