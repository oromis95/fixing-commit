@@ -4,16 +4,16 @@ public class ItemSaddle extends Item {
 
     public ItemSaddle(int i) {
         super(i);
-        this.aX = 1;
-        this.aY = 64;
+        this.bb = 1;
+        this.bc = 64;
     }
 
     public void b(ItemStack itemstack, EntityLiving entityliving) {
         if (entityliving instanceof EntityPig) {
             EntityPig entitypig = (EntityPig) entityliving;
 
-            if (!entitypig.a) {
-                entitypig.a = true;
+            if (!entitypig.K()) {
+                entitypig.a(true);
                 --itemstack.a;
             }
         }
