@@ -2,9 +2,9 @@ package net.minecraft.server;
 
 public class ItemAxe extends ItemTool {
 
-    private static Block[] bb = new Block[] { Block.WOOD, Block.BOOKSHELF, Block.LOG, Block.CHEST};
+    private static Block[] bg = new Block[] { Block.WOOD, Block.BOOKSHELF, Block.LOG, Block.CHEST};
 
-    public ItemAxe(int i, int j) {
-        super(i, 3, j, bb);
+    protected ItemAxe(int i, EnumToolMaterial enumtoolmaterial) {
+        super(i, 3, enumtoolmaterial, bg);
     }
 }
