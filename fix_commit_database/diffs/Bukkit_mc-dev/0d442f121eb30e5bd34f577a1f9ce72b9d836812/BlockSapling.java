@@ -12,14 +12,16 @@ public class BlockSapling extends BlockFlower {
     }
 
     public void a(World world, int i, int j, int k, Random random) {
-        super.a(world, i, j, k, random);
-        if (world.getLightLevel(i, j + 1, k) >= 9 && random.nextInt(30) == 0) {
-            int l = world.getData(i, j, k);
-
-            if ((l & 8) == 0) {
-                world.setData(i, j, k, l | 8);
-            } else {
-                this.b(world, i, j, k, random);
+        if (!world.isStatic) {
+            super.a(world, i, j, k, random);
+            if (world.getLightLevel(i, j + 1, k) >= 9 && random.nextInt(30) == 0) {
+                int l = world.getData(i, j, k);
+
+                if ((l & 8) == 0) {
+                    world.setData(i, j, k, l | 8);
+                } else {
+                    this.b(world, i, j, k, random);
+                }
             }
         }
     }
