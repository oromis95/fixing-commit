@@ -46,14 +46,14 @@ public class ConsoleCommandHandler {
 
                     for (i = 0; i < this.server.worldServer.length; ++i) {
                         worldserver = this.server.worldServer[i];
-                        worldserver.z = true;
+                        worldserver.E = true;
                     }
                 } else if (s.toLowerCase().startsWith("save-on")) {
                     this.print(s1, "Enabling level saving..");
 
                     for (i = 0; i < this.server.worldServer.length; ++i) {
                         worldserver = this.server.worldServer[i];
-                        worldserver.z = false;
+                        worldserver.E = false;
                     }
                 } else {
                     String s2;
@@ -125,6 +125,8 @@ public class ConsoleCommandHandler {
                                             icommandlistener.sendMessage("Can\'t find user " + astring[1] + ". No tp.");
                                         } else if (entityplayer2 == null) {
                                             icommandlistener.sendMessage("Can\'t find user " + astring[2] + ". No tp.");
+                                        } else if (entityplayer.dimension != entityplayer2.dimension) {
+                                            icommandlistener.sendMessage("User " + astring[1] + " and " + astring[2] + " are in different dimensions. No tp.");
                                         } else {
                                             entityplayer.netServerHandler.a(entityplayer2.locX, entityplayer2.locY, entityplayer2.locZ, entityplayer2.yaw, entityplayer2.pitch);
                                             this.print(s1, "Teleporting " + astring[1] + " to " + astring[2] + ".");
