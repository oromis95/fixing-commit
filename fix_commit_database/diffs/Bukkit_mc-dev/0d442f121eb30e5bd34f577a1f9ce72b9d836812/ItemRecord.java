@@ -2,7 +2,7 @@ package net.minecraft.server;
 
 public class ItemRecord extends Item {
 
-    private String a;
+    public final String a;
 
     protected ItemRecord(int i, String s) {
         super(i);
@@ -12,10 +12,14 @@ public class ItemRecord extends Item {
 
     public boolean a(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
         if (world.getTypeId(i, j, k) == Block.JUKEBOX.id && world.getData(i, j, k) == 0) {
-            world.setData(i, j, k, this.id - Item.GOLD_RECORD.id + 1);
-            world.a(this.a, i, j, k);
-            --itemstack.count;
-            return true;
+            if (world.isStatic) {
+                return true;
+            } else {
+                ((BlockJukeBox) Block.JUKEBOX).a_(world, i, j, k, this.id);
+                world.a((EntityHuman) null, 1005, i, j, k, this.id);
+                --itemstack.count;
+                return true;
+            }
         } else {
             return false;
         }
