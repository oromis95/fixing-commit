@@ -11,6 +11,10 @@ public class BlockLongGrass extends BlockFlower {
         this.a(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, 0.8F, 0.5F + f);
     }
 
+    public int a(int i, int j) {
+        return j == 1 ? this.textureId : (j == 2 ? this.textureId + 16 + 1 : (j == 0 ? this.textureId + 16 : this.textureId));
+    }
+
     public int a(int i, Random random) {
         return random.nextInt(8) == 0 ? Item.SEEDS.id : -1;
     }
