@@ -5,23 +5,27 @@ import java.util.Random;
 public class WorldGenGrass extends WorldGenerator {
 
     private int a;
+    private int b;
 
-    public WorldGenGrass(int i) {
+    public WorldGenGrass(int i, int j) {
         this.a = i;
+        this.b = j;
     }
 
     public boolean a(World world, Random random, int i, int j, int k) {
-        while (world.getTypeId(i, j, k) == 0 && j > 0) {
-            --j;
+        int l;
+
+        for (boolean flag = false; ((l = world.getTypeId(i, j, k)) == 0 || l == Block.LEAVES.id) && j > 0; --j) {
+            ;
         }
 
-        for (int l = 0; l < 256; ++l) {
-            int i1 = i + random.nextInt(8) - random.nextInt(8);
-            int j1 = j + random.nextInt(4) - random.nextInt(4);
-            int k1 = k + random.nextInt(8) - random.nextInt(8);
+        for (int i1 = 0; i1 < 128; ++i1) {
+            int j1 = i + random.nextInt(8) - random.nextInt(8);
+            int k1 = j + random.nextInt(4) - random.nextInt(4);
+            int l1 = k + random.nextInt(8) - random.nextInt(8);
 
-            if (world.isEmpty(i1, j1, k1) && ((BlockFlower) Block.byId[this.a]).f(world, i1, j1, k1)) {
-                world.setRawTypeId(i1, j1, k1, this.a);
+            if (world.isEmpty(j1, k1, l1) && ((BlockFlower) Block.byId[this.a]).f(world, j1, k1, l1)) {
+                world.setRawTypeIdAndData(j1, k1, l1, this.a, this.b);
             }
         }
 
