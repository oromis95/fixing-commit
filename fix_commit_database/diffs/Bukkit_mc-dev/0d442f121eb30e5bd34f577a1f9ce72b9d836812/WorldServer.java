@@ -7,9 +7,9 @@ public class WorldServer extends World {
 
     public ChunkProviderServer chunkProviderServer;
     public boolean weirdIsOpCache = false;
-    public boolean z;
+    public boolean E;
     private MinecraftServer server;
-    private EntityList B = new EntityList();
+    private EntityList G = new EntityList();
 
     public WorldServer(MinecraftServer minecraftserver, IDataManager idatamanager, String s, int i, long j) {
         super(idatamanager, s, j, WorldProvider.a(i));
@@ -31,7 +31,7 @@ public class WorldServer extends World {
     }
 
     protected IChunkProvider b() {
-        IChunkLoader ichunkloader = this.r.a(this.worldProvider);
+        IChunkLoader ichunkloader = this.w.a(this.worldProvider);
 
         this.chunkProviderServer = new ChunkProviderServer(this, ichunkloader, this.worldProvider.b());
         return this.chunkProviderServer;
@@ -64,21 +64,21 @@ public class WorldServer extends World {
 
     protected void c(Entity entity) {
         super.c(entity);
-        this.B.a(entity.id, entity);
+        this.G.a(entity.id, entity);
     }
 
     protected void d(Entity entity) {
         super.d(entity);
-        this.B.d(entity.id);
+        this.G.d(entity.id);
     }
 
     public Entity getEntity(int i) {
-        return (Entity) this.B.a(i);
+        return (Entity) this.G.a(i);
     }
 
     public boolean a(Entity entity) {
         if (super.a(entity)) {
-            this.server.serverConfigurationManager.a(entity.locX, entity.locY, entity.locZ, 512.0D, new Packet71Weather(entity));
+            this.server.serverConfigurationManager.a(entity.locX, entity.locY, entity.locZ, 512.0D, this.worldProvider.dimension, new Packet71Weather(entity));
             return true;
         } else {
             return false;
@@ -97,17 +97,17 @@ public class WorldServer extends World {
         explosion.a = flag;
         explosion.a();
         explosion.a(false);
-        this.server.serverConfigurationManager.a(d0, d1, d2, 64.0D, new Packet60Explosion(d0, d1, d2, f, explosion.g));
+        this.server.serverConfigurationManager.a(d0, d1, d2, 64.0D, this.worldProvider.dimension, new Packet60Explosion(d0, d1, d2, f, explosion.g));
         return explosion;
     }
 
     public void d(int i, int j, int k, int l, int i1) {
         super.d(i, j, k, l, i1);
-        this.server.serverConfigurationManager.a((double) i, (double) j, (double) k, 64.0D, new Packet54PlayNoteBlock(i, j, k, l, i1));
+        this.server.serverConfigurationManager.a((double) i, (double) j, (double) k, 64.0D, this.worldProvider.dimension, new Packet54PlayNoteBlock(i, j, k, l, i1));
     }
 
     public void saveLevel() {
-        this.r.e();
+        this.w.e();
     }
 
     protected void i() {
