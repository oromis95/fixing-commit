@@ -4,18 +4,18 @@ import java.util.List;
 
 public class EntityFireball extends Entity {
 
-    private int e = -1;
     private int f = -1;
     private int g = -1;
-    private int h = 0;
-    private boolean i = false;
+    private int h = -1;
+    private int i = 0;
+    private boolean j = false;
     public int a = 0;
-    private EntityLiving shooter;
+    public EntityLiving shooter;
     private int k;
     private int l = 0;
-    public double b;
     public double c;
     public double d;
+    public double e;
 
     public EntityFireball(World world) {
         super(world);
@@ -37,9 +37,9 @@ public class EntityFireball extends Entity {
         d2 += this.random.nextGaussian() * 0.4D;
         double d3 = (double) MathHelper.a(d0 * d0 + d1 * d1 + d2 * d2);
 
-        this.b = d0 / d3 * 0.1D;
-        this.c = d1 / d3 * 0.1D;
-        this.d = d2 / d3 * 0.1D;
+        this.c = d0 / d3 * 0.1D;
+        this.d = d1 / d3 * 0.1D;
+        this.e = d2 / d3 * 0.1D;
     }
 
     public void p_() {
@@ -49,10 +49,10 @@ public class EntityFireball extends Entity {
             --this.a;
         }
 
-        if (this.i) {
-            int i = this.world.getTypeId(this.e, this.f, this.g);
+        if (this.j) {
+            int i = this.world.getTypeId(this.f, this.g, this.h);
 
-            if (i == this.h) {
+            if (i == this.i) {
                 ++this.k;
                 if (this.k == 1200) {
                     this.die();
@@ -61,7 +61,7 @@ public class EntityFireball extends Entity {
                 return;
             }
 
-            this.i = false;
+            this.j = false;
             this.motX *= (double) (this.random.nextFloat() * 0.2F);
             this.motY *= (double) (this.random.nextFloat() * 0.2F);
             this.motZ *= (double) (this.random.nextFloat() * 0.2F);
@@ -109,11 +109,14 @@ public class EntityFireball extends Entity {
         }
 
         if (movingobjectposition != null) {
-            if (movingobjectposition.entity != null && movingobjectposition.entity.damageEntity(this.shooter, 0)) {
-                ;
+            if (!this.world.isStatic) {
+                if (movingobjectposition.entity != null && movingobjectposition.entity.damageEntity(this.shooter, 0)) {
+                    ;
+                }
+
+                this.world.createExplosion((Entity) null, this.locX, this.locY, this.locZ, 1.0F, true);
             }
 
-            this.world.createExplosion((Entity) null, this.locX, this.locY, this.locZ, 1.0F, true);
             this.die();
         }
 
@@ -154,9 +157,9 @@ public class EntityFireball extends Entity {
             f2 = 0.8F;
         }
 
-        this.motX += this.b;
-        this.motY += this.c;
-        this.motZ += this.d;
+        this.motX += this.c;
+        this.motY += this.d;
+        this.motZ += this.e;
         this.motX *= (double) f2;
         this.motY *= (double) f2;
         this.motZ *= (double) f2;
@@ -165,21 +168,21 @@ public class EntityFireball extends Entity {
     }
 
     public void b(NBTTagCompound nbttagcompound) {
-        nbttagcompound.a("xTile", (short) this.e);
-        nbttagcompound.a("yTile", (short) this.f);
-        nbttagcompound.a("zTile", (short) this.g);
-        nbttagcompound.a("inTile", (byte) this.h);
+        nbttagcompound.a("xTile", (short) this.f);
+        nbttagcompound.a("yTile", (short) this.g);
+        nbttagcompound.a("zTile", (short) this.h);
+        nbttagcompound.a("inTile", (byte) this.i);
         nbttagcompound.a("shake", (byte) this.a);
-        nbttagcompound.a("inGround", (byte) (this.i ? 1 : 0));
+        nbttagcompound.a("inGround", (byte) (this.j ? 1 : 0));
     }
 
     public void a(NBTTagCompound nbttagcompound) {
-        this.e = nbttagcompound.d("xTile");
-        this.f = nbttagcompound.d("yTile");
-        this.g = nbttagcompound.d("zTile");
-        this.h = nbttagcompound.c("inTile") & 255;
+        this.f = nbttagcompound.d("xTile");
+        this.g = nbttagcompound.d("yTile");
+        this.h = nbttagcompound.d("zTile");
+        this.i = nbttagcompound.c("inTile") & 255;
         this.a = nbttagcompound.c("shake") & 255;
-        this.i = nbttagcompound.c("inGround") == 1;
+        this.j = nbttagcompound.c("inGround") == 1;
     }
 
     public boolean o_() {
@@ -195,9 +198,9 @@ public class EntityFireball extends Entity {
                 this.motX = vec3d.a;
                 this.motY = vec3d.b;
                 this.motZ = vec3d.c;
-                this.b = this.motX * 0.1D;
-                this.c = this.motY * 0.1D;
-                this.d = this.motZ * 0.1D;
+                this.c = this.motX * 0.1D;
+                this.d = this.motY * 0.1D;
+                this.e = this.motZ * 0.1D;
             }
 
             return true;
