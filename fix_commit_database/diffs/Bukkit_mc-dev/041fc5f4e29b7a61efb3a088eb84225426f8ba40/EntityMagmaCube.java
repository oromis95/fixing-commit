@@ -13,7 +13,7 @@ public class EntityMagmaCube extends EntitySlime {
         return this.world.difficulty > 0 && this.world.containsEntity(this.boundingBox) && this.world.getCubes(this, this.boundingBox).size() == 0 && !this.world.containsLiquid(this.boundingBox);
     }
 
-    public int S() {
+    public int T() {
         return this.getSize() * 3;
     }
 
@@ -25,7 +25,7 @@ public class EntityMagmaCube extends EntitySlime {
         return "flame";
     }
 
-    protected EntitySlime D() {
+    protected EntitySlime C() {
         return new EntityMagmaCube(this.world);
     }
 
@@ -61,7 +61,7 @@ public class EntityMagmaCube extends EntitySlime {
         this.a *= 0.9F;
     }
 
-    protected void ab() {
+    protected void ac() {
         this.motY = (double) (0.42F + (float) this.getSize() * 0.1F);
         this.ce = true;
     }
@@ -84,11 +84,11 @@ public class EntityMagmaCube extends EntitySlime {
         return "mob.slime";
     }
 
-    protected String J() {
+    protected String I() {
         return this.getSize() > 1 ? "mob.magmacube.big" : "mob.magmacube.small";
     }
 
-    public boolean aU() {
+    public boolean aV() {
         return false;
     }
 
