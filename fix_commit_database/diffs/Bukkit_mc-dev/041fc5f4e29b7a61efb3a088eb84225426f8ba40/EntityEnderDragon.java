@@ -265,14 +265,14 @@ public class EntityEnderDragon extends EntityComplex {
             float f11 = MathHelper.sin(f10);
             float f12 = MathHelper.cos(f10);
 
-            this.h.G_();
+            this.h.F_();
             this.h.setPositionRotation(this.locX + (double) (f11 * 0.5F), this.locY, this.locZ - (double) (f12 * 0.5F), 0.0F, 0.0F);
-            this.l.G_();
+            this.l.F_();
             this.l.setPositionRotation(this.locX + (double) (f12 * 4.5F), this.locY + 2.0D, this.locZ + (double) (f11 * 4.5F), 0.0F, 0.0F);
-            this.m.G_();
+            this.m.F_();
             this.m.setPositionRotation(this.locX - (double) (f12 * 4.5F), this.locY + 2.0D, this.locZ - (double) (f11 * 4.5F), 0.0F, 0.0F);
             if (!this.world.isStatic) {
-                this.D();
+                this.C();
             }
 
             if (!this.world.isStatic && this.at == 0) {
@@ -287,7 +287,7 @@ public class EntityEnderDragon extends EntityComplex {
             f3 = MathHelper.sin(this.yaw * 3.1415927F / 180.0F - this.aY * 0.01F);
             float f13 = MathHelper.cos(this.yaw * 3.1415927F / 180.0F - this.aY * 0.01F);
 
-            this.g.G_();
+            this.g.F_();
             this.g.setPositionRotation(this.locX + (double) (f3 * 5.5F * f1), this.locY + (adouble1[1] - adouble[1]) * 1.0D + (double) (f9 * 5.5F), this.locZ - (double) (f13 * 5.5F * f1), 0.0F, 0.0F);
 
             for (int j = 0; j < 3; ++j) {
@@ -312,7 +312,7 @@ public class EntityEnderDragon extends EntityComplex {
                 float f17 = 1.5F;
                 float f18 = (float) (j + 1) * 2.0F;
 
-                entitycomplexpart.G_();
+                entitycomplexpart.F_();
                 entitycomplexpart.setPositionRotation(this.locX - (double) ((f11 * f17 + f15 * f18) * f1), this.locY + (adouble2[1] - adouble[1]) * 1.0D - (double) ((f18 + f17) * f9) + 1.5D, this.locZ + (double) ((f12 * f17 + f16 * f18) * f1), 0.0F, 0.0F);
             }
 
@@ -356,7 +356,7 @@ public class EntityEnderDragon extends EntityComplex {
         }
     }
 
-    private void D() {}
+    private void C() {}
 
     private void a(List list) {
         double d0 = (this.h.boundingBox.a + this.h.boundingBox.d) / 2.0D;
@@ -480,7 +480,7 @@ public class EntityEnderDragon extends EntityComplex {
         return true;
     }
 
-    protected void aA() {
+    protected void aB() {
         ++this.r;
         if (this.r >= 180 && this.r <= 200) {
             float f = (this.random.nextFloat() - 0.5F) * 8.0F;
@@ -515,7 +515,7 @@ public class EntityEnderDragon extends EntityComplex {
             }
 
             this.a(MathHelper.floor(this.locX), MathHelper.floor(this.locZ));
-            this.aG();
+            this.aH();
             this.die();
         }
     }
@@ -562,9 +562,9 @@ public class EntityEnderDragon extends EntityComplex {
         BlockEnderPortal.a = false;
     }
 
-    protected void aF() {}
+    protected void aG() {}
 
-    public Entity[] ba() {
+    public Entity[] bb() {
         return this.children;
     }
 
