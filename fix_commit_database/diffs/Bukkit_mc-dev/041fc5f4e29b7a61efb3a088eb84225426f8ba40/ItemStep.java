@@ -34,15 +34,56 @@ public class ItemStep extends ItemBlock {
             boolean flag = (j1 & 8) != 0;
 
             if ((l == 1 && !flag || l == 0 && flag) && i1 == Block.STEP.id && k1 == itemstack.getData()) {
-                if (world.setTypeIdAndData(i, j, k, Block.DOUBLE_STEP.id, k1)) {
+                if (world.containsEntity(Block.DOUBLE_STEP.e(world, i, j, k)) && world.setTypeIdAndData(i, j, k, Block.DOUBLE_STEP.id, k1)) {
                     world.makeSound((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), Block.DOUBLE_STEP.stepSound.getName(), (Block.DOUBLE_STEP.stepSound.getVolume1() + 1.0F) / 2.0F, Block.DOUBLE_STEP.stepSound.getVolume2() * 0.8F);
                     --itemstack.count;
                 }
 
                 return true;
             } else {
-                return super.interactWith(itemstack, entityhuman, world, i, j, k, l);
+                return b(itemstack, entityhuman, world, i, j, k, l) ? true : super.interactWith(itemstack, entityhuman, world, i, j, k, l);
             }
         }
     }
+
+    private static boolean b(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
+        if (l == 0) {
+            --j;
+        }
+
+        if (l == 1) {
+            ++j;
+        }
+
+        if (l == 2) {
+            --k;
+        }
+
+        if (l == 3) {
+            ++k;
+        }
+
+        if (l == 4) {
+            --i;
+        }
+
+        if (l == 5) {
+            ++i;
+        }
+
+        int i1 = world.getTypeId(i, j, k);
+        int j1 = world.getData(i, j, k);
+        int k1 = j1 & 7;
+
+        if (i1 == Block.STEP.id && k1 == itemstack.getData()) {
+            if (world.containsEntity(Block.DOUBLE_STEP.e(world, i, j, k)) && world.setTypeIdAndData(i, j, k, Block.DOUBLE_STEP.id, k1)) {
+                world.makeSound((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), Block.DOUBLE_STEP.stepSound.getName(), (Block.DOUBLE_STEP.stepSound.getVolume1() + 1.0F) / 2.0F, Block.DOUBLE_STEP.stepSound.getVolume2() * 0.8F);
+                --itemstack.count;
+            }
+
+            return true;
+        } else {
+            return false;
+        }
+    }
 }
