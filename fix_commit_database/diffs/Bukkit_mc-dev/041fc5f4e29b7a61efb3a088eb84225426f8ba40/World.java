@@ -201,7 +201,7 @@ public class World implements IBlockAccess {
     public boolean isTileEntity(int i, int j, int k) {
         int l = this.getTypeId(i, j, k);
 
-        return Block.byId[l] != null && Block.byId[l].n();
+        return Block.byId[l] != null && Block.byId[l].o();
     }
 
     public boolean isLoaded(int i, int j, int k) {
@@ -964,7 +964,7 @@ public class World implements IBlockAccess {
 
         for (i = 0; i < this.e.size(); ++i) {
             entity = (Entity) this.e.get(i);
-            entity.G_();
+            entity.F_();
             if (entity.dead) {
                 this.e.remove(i--);
             }
@@ -1107,9 +1107,9 @@ public class World implements IBlockAccess {
             entity.lastPitch = entity.pitch;
             if (flag && entity.bZ) {
                 if (entity.vehicle != null) {
-                    entity.Q();
+                    entity.R();
                 } else {
-                    entity.G_();
+                    entity.F_();
                 }
             }
 
@@ -1832,7 +1832,7 @@ public class World implements IBlockAccess {
                         ++j;
                         Block block = Block.byId[j3];
 
-                        if (block != null && block.m()) {
+                        if (block != null && block.n()) {
                             ++i;
                             block.a(this, k2 + k, i3 + chunksection.c(), l2 + l, this.random);
                         }
@@ -1854,7 +1854,7 @@ public class World implements IBlockAccess {
 
     public boolean c(int i, int j, int k, boolean flag) {
         BiomeBase biomebase = this.getBiome(i, k);
-        float f = biomebase.h();
+        float f = biomebase.i();
 
         if (f > 0.15F) {
             return false;
@@ -1897,7 +1897,7 @@ public class World implements IBlockAccess {
 
     public boolean u(int i, int j, int k) {
         BiomeBase biomebase = this.getBiome(i, k);
-        float f = biomebase.h();
+        float f = biomebase.i();
 
         if (f > 0.15F) {
             return false;
@@ -2615,14 +2615,14 @@ public class World implements IBlockAccess {
         } else {
             BiomeBase biomebase = this.getBiome(i, k);
 
-            return biomebase.b() ? false : biomebase.c();
+            return biomebase.c() ? false : biomebase.d();
         }
     }
 
     public boolean z(int i, int j, int k) {
         BiomeBase biomebase = this.getBiome(i, k);
 
-        return biomebase.d();
+        return biomebase.e();
     }
 
     public void a(String s, WorldMapBase worldmapbase) {
