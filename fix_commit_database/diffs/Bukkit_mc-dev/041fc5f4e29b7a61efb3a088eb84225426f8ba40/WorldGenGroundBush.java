@@ -23,7 +23,7 @@ public class WorldGenGroundBush extends WorldGenerator {
 
         if (i1 == Block.DIRT.id || i1 == Block.GRASS.id) {
             ++j;
-            world.setRawTypeIdAndData(i, j, k, Block.LOG.id, this.b);
+            this.setTypeAndData(world, i, j, k, Block.LOG.id, this.b);
 
             for (int j1 = j; j1 <= j + 2; ++j1) {
                 int k1 = j1 - j;
