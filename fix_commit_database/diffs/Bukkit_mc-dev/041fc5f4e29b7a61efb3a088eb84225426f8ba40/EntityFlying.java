@@ -9,13 +9,13 @@ public abstract class EntityFlying extends EntityLiving {
     protected void a(float f) {}
 
     public void a(float f, float f1) {
-        if (this.aT()) {
+        if (this.aU()) {
             this.a(f, f1, 0.02F);
             this.move(this.motX, this.motY, this.motZ);
             this.motX *= 0.800000011920929D;
             this.motY *= 0.800000011920929D;
             this.motZ *= 0.800000011920929D;
-        } else if (this.aU()) {
+        } else if (this.aV()) {
             this.a(f, f1, 0.02F);
             this.move(this.motX, this.motY, this.motZ);
             this.motX *= 0.5D;
