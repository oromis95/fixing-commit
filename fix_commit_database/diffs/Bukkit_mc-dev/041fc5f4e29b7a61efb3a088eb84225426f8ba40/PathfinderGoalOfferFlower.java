@@ -14,7 +14,7 @@ public class PathfinderGoalOfferFlower extends PathfinderGoal {
     public boolean a() {
         if (!this.a.world.e()) {
             return false;
-        } else if (this.a.am().nextInt(8000) != 0) {
+        } else if (this.a.an().nextInt(8000) != 0) {
             return false;
         } else {
             this.b = (EntityVillager) this.a.world.a(EntityVillager.class, this.a.boundingBox.grow(6.0D, 2.0D, 6.0D), this.a);
