@@ -84,7 +84,7 @@ public class EntityPlayer extends EntityHuman implements ICrafting {
         return 1.62F;
     }
 
-    public void G_() {
+    public void F_() {
         this.itemInWorldManager.c();
         --this.invulnerableTicks;
         this.activeContainer.a();
@@ -132,7 +132,7 @@ public class EntityPlayer extends EntityHuman implements ICrafting {
         }
     }
 
-    protected boolean C_() {
+    protected boolean C() {
         return this.server.pvpMode;
     }
 
@@ -141,7 +141,7 @@ public class EntityPlayer extends EntityHuman implements ICrafting {
     }
 
     public void a(boolean flag) {
-        super.G_();
+        super.F_();
 
         for (int i = 0; i < this.inventory.getSize(); ++i) {
             ItemStack itemstack = this.inventory.getItem(i);
@@ -307,7 +307,7 @@ public class EntityPlayer extends EntityHuman implements ICrafting {
         this.activeContainer.a();
     }
 
-    public void D() {
+    public void C_() {
         if (!this.t) {
             this.u = -1;
             this.t = true;
@@ -477,7 +477,7 @@ public class EntityPlayer extends EntityHuman implements ICrafting {
         }
     }
 
-    public void J() {
+    public void D_() {
         this.cf = -99999999;
     }
 
@@ -532,4 +532,10 @@ public class EntityPlayer extends EntityHuman implements ICrafting {
 
         entitytracker.sendPacketToEntity(this, new Packet18ArmAnimation(entity, 7));
     }
+
+    public void updateAbilities() {
+        if (this.netServerHandler != null) {
+            this.netServerHandler.sendPacket(new Packet202Abilities(this.abilities));
+        }
+    }
 }
