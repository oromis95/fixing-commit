@@ -45,4 +45,8 @@ public class NBTTagShort extends NBTBase {
             return false;
         }
     }
+
+    public int hashCode() {
+        return super.hashCode() ^ this.data;
+    }
 }
