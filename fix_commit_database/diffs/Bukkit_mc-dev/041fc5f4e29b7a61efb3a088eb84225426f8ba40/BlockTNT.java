@@ -32,10 +32,12 @@ public class BlockTNT extends Block {
     }
 
     public void wasExploded(World world, int i, int j, int k) {
-        EntityTNTPrimed entitytntprimed = new EntityTNTPrimed(world, (double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F));
+        if (!world.isStatic) {
+            EntityTNTPrimed entitytntprimed = new EntityTNTPrimed(world, (double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F));
 
-        entitytntprimed.fuseTicks = world.random.nextInt(entitytntprimed.fuseTicks / 4) + entitytntprimed.fuseTicks / 8;
-        world.addEntity(entitytntprimed);
+            entitytntprimed.fuseTicks = world.random.nextInt(entitytntprimed.fuseTicks / 4) + entitytntprimed.fuseTicks / 8;
+            world.addEntity(entitytntprimed);
+        }
     }
 
     public void postBreak(World world, int i, int j, int k, int l) {
@@ -52,7 +54,7 @@ public class BlockTNT extends Block {
     }
 
     public void attack(World world, int i, int j, int k, EntityHuman entityhuman) {
-        if (entityhuman.T() != null && entityhuman.T().id == Item.FLINT_AND_STEEL.id) {
+        if (entityhuman.U() != null && entityhuman.U().id == Item.FLINT_AND_STEEL.id) {
             world.setRawData(i, j, k, 1);
         }
 
