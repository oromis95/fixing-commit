@@ -45,4 +45,8 @@ public class NBTTagFloat extends NBTBase {
             return false;
         }
     }
+
+    public int hashCode() {
+        return super.hashCode() ^ Float.floatToIntBits(this.data);
+    }
 }
