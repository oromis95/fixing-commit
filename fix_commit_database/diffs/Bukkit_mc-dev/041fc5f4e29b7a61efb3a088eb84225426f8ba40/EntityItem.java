@@ -32,8 +32,8 @@ public class EntityItem extends Entity {
 
     protected void b() {}
 
-    public void G_() {
-        super.G_();
+    public void F_() {
+        super.F_();
         if (this.pickupDelay > 0) {
             --this.pickupDelay;
         }
@@ -88,7 +88,7 @@ public class EntityItem extends Entity {
     }
 
     public boolean damageEntity(DamageSource damagesource, int i) {
-        this.aV();
+        this.aW();
         this.e -= i;
         if (this.e <= 0) {
             this.die();
