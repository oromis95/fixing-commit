@@ -55,7 +55,7 @@ public class WorldGenSwampTree extends WorldGenerator {
             } else {
                 i1 = world.getTypeId(i, j - 1, k);
                 if ((i1 == Block.GRASS.id || i1 == Block.DIRT.id) && j < 128 - l - 1) {
-                    world.setRawTypeId(i, j - 1, k, Block.DIRT.id);
+                    this.setType(world, i, j - 1, k, Block.DIRT.id);
 
                     int i2;
                     int j2;
@@ -71,7 +71,7 @@ public class WorldGenSwampTree extends WorldGenerator {
                                 int l2 = k2 - k;
 
                                 if ((Math.abs(i2) != k1 || Math.abs(l2) != k1 || random.nextInt(2) != 0 && j1 != 0) && !Block.n[world.getTypeId(l1, j2, k2)]) {
-                                    world.setRawTypeId(l1, j2, k2, Block.LEAVES.id);
+                                    this.setType(world, l1, j2, k2, Block.LEAVES.id);
                                 }
                             }
                         }
@@ -80,7 +80,7 @@ public class WorldGenSwampTree extends WorldGenerator {
                     for (j2 = 0; j2 < l; ++j2) {
                         j1 = world.getTypeId(i, j + j2, k);
                         if (j1 == 0 || j1 == Block.LEAVES.id || j1 == Block.WATER.id || j1 == Block.STATIONARY_WATER.id) {
-                            world.setRawTypeId(i, j + j2, k, Block.LOG.id);
+                            this.setType(world, i, j + j2, k, Block.LOG.id);
                         }
                     }
 
@@ -92,19 +92,19 @@ public class WorldGenSwampTree extends WorldGenerator {
                             for (i2 = k - k1; i2 <= k + k1; ++i2) {
                                 if (world.getTypeId(l1, j2, i2) == Block.LEAVES.id) {
                                     if (random.nextInt(4) == 0 && world.getTypeId(l1 - 1, j2, i2) == 0) {
-                                        this.a(world, l1 - 1, j2, i2, 8);
+                                        this.b(world, l1 - 1, j2, i2, 8);
                                     }
 
                                     if (random.nextInt(4) == 0 && world.getTypeId(l1 + 1, j2, i2) == 0) {
-                                        this.a(world, l1 + 1, j2, i2, 2);
+                                        this.b(world, l1 + 1, j2, i2, 2);
                                     }
 
                                     if (random.nextInt(4) == 0 && world.getTypeId(l1, j2, i2 - 1) == 0) {
-                                        this.a(world, l1, j2, i2 - 1, 1);
+                                        this.b(world, l1, j2, i2 - 1, 1);
                                     }
 
                                     if (random.nextInt(4) == 0 && world.getTypeId(l1, j2, i2 + 1) == 0) {
-                                        this.a(world, l1, j2, i2 + 1, 4);
+                                        this.b(world, l1, j2, i2 + 1, 4);
                                     }
                                 }
                             }
@@ -121,8 +121,8 @@ public class WorldGenSwampTree extends WorldGenerator {
         }
     }
 
-    private void a(World world, int i, int j, int k, int l) {
-        world.setTypeIdAndData(i, j, k, Block.VINE.id, l);
+    private void b(World world, int i, int j, int k, int l) {
+        this.setTypeAndData(world, i, j, k, Block.VINE.id, l);
         int i1 = 4;
 
         while (true) {
@@ -131,7 +131,7 @@ public class WorldGenSwampTree extends WorldGenerator {
                 return;
             }
 
-            world.setTypeIdAndData(i, j, k, Block.VINE.id, l);
+            this.setTypeAndData(world, i, j, k, Block.VINE.id, l);
             --i1;
         }
     }
