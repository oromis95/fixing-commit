@@ -10,6 +10,8 @@ public class ItemEnderPearl extends Item {
     public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
         if (entityhuman.abilities.canInstantlyBuild) {
             return itemstack;
+        } else if (entityhuman.vehicle != null) {
+            return itemstack;
         } else {
             --itemstack.count;
             world.makeSound(entityhuman, "random.bow", 0.5F, 0.4F / (c.nextFloat() * 0.4F + 0.8F));
