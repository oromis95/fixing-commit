@@ -45,4 +45,8 @@ public class NBTTagLong extends NBTBase {
             return false;
         }
     }
+
+    public int hashCode() {
+        return super.hashCode() ^ (int) (this.data ^ this.data >>> 32);
+    }
 }
