@@ -60,7 +60,7 @@ public class WorldGenTrees extends WorldGenerator {
             } else {
                 i1 = world.getTypeId(i, j - 1, k);
                 if ((i1 == Block.GRASS.id || i1 == Block.DIRT.id) && j < 256 - l - 1) {
-                    world.setRawTypeId(i, j - 1, k, Block.DIRT.id);
+                    this.setType(world, i, j - 1, k, Block.DIRT.id);
                     b0 = 3;
                     byte b1 = 0;
 
@@ -118,19 +118,19 @@ public class WorldGenTrees extends WorldGenerator {
                                 for (k2 = k - i2; k2 <= k + i2; ++k2) {
                                     if (world.getTypeId(j2, j1, k2) == Block.LEAVES.id) {
                                         if (random.nextInt(4) == 0 && world.getTypeId(j2 - 1, j1, k2) == 0) {
-                                            this.a(world, j2 - 1, j1, k2, 8);
+                                            this.b(world, j2 - 1, j1, k2, 8);
                                         }
 
                                         if (random.nextInt(4) == 0 && world.getTypeId(j2 + 1, j1, k2) == 0) {
-                                            this.a(world, j2 + 1, j1, k2, 2);
+                                            this.b(world, j2 + 1, j1, k2, 2);
                                         }
 
                                         if (random.nextInt(4) == 0 && world.getTypeId(j2, j1, k2 - 1) == 0) {
-                                            this.a(world, j2, j1, k2 - 1, 1);
+                                            this.b(world, j2, j1, k2 - 1, 1);
                                         }
 
                                         if (random.nextInt(4) == 0 && world.getTypeId(j2, j1, k2 + 1) == 0) {
-                                            this.a(world, j2, j1, k2 + 1, 4);
+                                            this.b(world, j2, j1, k2 + 1, 4);
                                         }
                                     }
                                 }
@@ -148,8 +148,8 @@ public class WorldGenTrees extends WorldGenerator {
         }
     }
 
-    private void a(World world, int i, int j, int k, int l) {
-        world.setTypeIdAndData(i, j, k, Block.VINE.id, l);
+    private void b(World world, int i, int j, int k, int l) {
+        this.setTypeAndData(world, i, j, k, Block.VINE.id, l);
         int i1 = 4;
 
         while (true) {
@@ -158,7 +158,7 @@ public class WorldGenTrees extends WorldGenerator {
                 return;
             }
 
-            world.setTypeIdAndData(i, j, k, Block.VINE.id, l);
+            this.setTypeAndData(world, i, j, k, Block.VINE.id, l);
             --i1;
         }
     }
