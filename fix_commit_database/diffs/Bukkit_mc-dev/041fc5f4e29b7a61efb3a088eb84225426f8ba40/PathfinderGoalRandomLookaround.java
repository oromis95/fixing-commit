@@ -13,7 +13,7 @@ public class PathfinderGoalRandomLookaround extends PathfinderGoal {
     }
 
     public boolean a() {
-        return this.a.am().nextFloat() < 0.02F;
+        return this.a.an().nextFloat() < 0.02F;
     }
 
     public boolean b() {
@@ -21,15 +21,15 @@ public class PathfinderGoalRandomLookaround extends PathfinderGoal {
     }
 
     public void c() {
-        double d0 = 6.283185307179586D * this.a.am().nextDouble();
+        double d0 = 6.283185307179586D * this.a.an().nextDouble();
 
         this.b = Math.cos(d0);
         this.c = Math.sin(d0);
-        this.d = 20 + this.a.am().nextInt(20);
+        this.d = 20 + this.a.an().nextInt(20);
     }
 
     public void e() {
         --this.d;
-        this.a.getControllerLook().a(this.a.locX + this.b, this.a.locY + (double) this.a.getHeadHeight(), this.a.locZ + this.c, 10.0F, (float) this.a.C());
+        this.a.getControllerLook().a(this.a.locX + this.b, this.a.locY + (double) this.a.getHeadHeight(), this.a.locZ + this.c, 10.0F, (float) this.a.D());
     }
 }
