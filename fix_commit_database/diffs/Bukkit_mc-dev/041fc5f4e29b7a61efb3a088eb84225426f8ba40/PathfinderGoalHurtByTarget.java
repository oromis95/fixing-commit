@@ -14,11 +14,11 @@ public class PathfinderGoalHurtByTarget extends PathfinderGoalTarget {
     }
 
     public boolean a() {
-        return this.a(this.c.an(), true);
+        return this.a(this.c.ao(), true);
     }
 
     public void c() {
-        this.c.b(this.c.an());
+        this.c.b(this.c.ao());
         if (this.a) {
             List list = this.c.world.a(this.c.getClass(), AxisAlignedBB.b(this.c.locX, this.c.locY, this.c.locZ, this.c.locX + 1.0D, this.c.locY + 1.0D, this.c.locZ + 1.0D).grow((double) this.d, 4.0D, (double) this.d));
             Iterator iterator = list.iterator();
@@ -27,8 +27,8 @@ public class PathfinderGoalHurtByTarget extends PathfinderGoalTarget {
                 Entity entity = (Entity) iterator.next();
                 EntityLiving entityliving = (EntityLiving) entity;
 
-                if (this.c != entityliving && entityliving.as() == null) {
-                    entityliving.b(this.c.an());
+                if (this.c != entityliving && entityliving.at() == null) {
+                    entityliving.b(this.c.ao());
                 }
             }
         }
