@@ -69,19 +69,19 @@ public abstract class EntityHuman extends EntityLiving {
         this.datawatcher.a(17, Byte.valueOf((byte) 0));
     }
 
-    public boolean L() {
+    public boolean M() {
         return this.d != null;
     }
 
-    public void M() {
+    public void N() {
         if (this.d != null) {
             this.d.b(this.world, this, this.e);
         }
 
-        this.N();
+        this.O();
     }
 
-    public void N() {
+    public void O() {
         this.d = null;
         this.e = 0;
         if (!this.world.isStatic) {
@@ -89,16 +89,16 @@ public abstract class EntityHuman extends EntityLiving {
         }
     }
 
-    public boolean O() {
-        return this.L() && Item.byId[this.d.id].d(this.d) == EnumAnimation.d;
+    public boolean P() {
+        return this.M() && Item.byId[this.d.id].d(this.d) == EnumAnimation.d;
     }
 
-    public void G_() {
+    public void F_() {
         if (this.d != null) {
             ItemStack itemstack = this.inventory.getItemInHand();
 
             if (itemstack != this.d) {
-                this.N();
+                this.O();
             } else {
                 if (this.e <= 25 && this.e % 4 == 0) {
                     this.b(itemstack, 5);
@@ -134,7 +134,7 @@ public abstract class EntityHuman extends EntityLiving {
             }
         }
 
-        super.G_();
+        super.F_();
         if (!this.world.isStatic && this.activeContainer != null && !this.activeContainer.b(this)) {
             this.closeInventory();
             this.activeContainer = this.defaultContainer;
@@ -231,11 +231,11 @@ public abstract class EntityHuman extends EntityLiving {
                 }
             }
 
-            this.N();
+            this.O();
         }
     }
 
-    protected boolean P() {
+    protected boolean Q() {
         return this.getHealth() <= 0 || this.isSleeping();
     }
 
@@ -243,12 +243,12 @@ public abstract class EntityHuman extends EntityLiving {
         this.activeContainer = this.defaultContainer;
     }
 
-    public void Q() {
+    public void R() {
         double d0 = this.locX;
         double d1 = this.locY;
         double d2 = this.locZ;
 
-        super.Q();
+        super.R();
         this.r = this.s;
         this.s = 0.0F;
         this.h(this.locX - d0, this.locY - d1, this.locZ - d2);
@@ -365,7 +365,7 @@ public abstract class EntityHuman extends EntityLiving {
         return j > 0 && this.random.nextInt(j + 1) > 0 ? i : super.b_(i);
     }
 
-    public EntityItem R() {
+    public EntityItem S() {
         return this.a(this.inventory.splitStack(this.inventory.itemInHandIndex, 1), false);
     }
 
@@ -576,7 +576,7 @@ public abstract class EntityHuman extends EntityLiving {
         }
     }
 
-    protected boolean C_() {
+    protected boolean C() {
         return false;
     }
 
@@ -590,7 +590,7 @@ public abstract class EntityHuman extends EntityLiving {
                 }
             }
 
-            if (!(entityliving instanceof EntityHuman) || this.C_()) {
+            if (!(entityliving instanceof EntityHuman) || this.C()) {
                 List list = this.world.a(EntityWolf.class, AxisAlignedBB.b(this.locX, this.locY, this.locZ, this.locX + 1.0D, this.locY + 1.0D, this.locZ + 1.0D).grow(16.0D, 4.0D, 16.0D));
                 Iterator iterator = list.iterator();
 
@@ -598,7 +598,7 @@ public abstract class EntityHuman extends EntityLiving {
                     Entity entity = (Entity) iterator.next();
                     EntityWolf entitywolf1 = (EntityWolf) entity;
 
-                    if (entitywolf1.isTamed() && entitywolf1.H() == null && this.name.equals(entitywolf1.getOwnerName()) && (!flag || !entitywolf1.isSitting())) {
+                    if (entitywolf1.isTamed() && entitywolf1.I() == null && this.name.equals(entitywolf1.getOwnerName()) && (!flag || !entitywolf1.isSitting())) {
                         entitywolf1.setSitting(false);
                         entitywolf1.setTarget(entityliving);
                     }
@@ -611,12 +611,12 @@ public abstract class EntityHuman extends EntityLiving {
         this.inventory.e(i);
     }
 
-    public int S() {
+    public int T() {
         return this.inventory.j();
     }
 
     protected void c(DamageSource damagesource, int i) {
-        if (!damagesource.ignoresArmor() && this.O()) {
+        if (!damagesource.ignoresArmor() && this.P()) {
             i = 1 + i >> 1;
         }
 
@@ -636,31 +636,31 @@ public abstract class EntityHuman extends EntityLiving {
 
     public void e(Entity entity) {
         if (!entity.b(this)) {
-            ItemStack itemstack = this.T();
+            ItemStack itemstack = this.U();
 
             if (itemstack != null && entity instanceof EntityLiving) {
                 itemstack.a((EntityLiving) entity);
                 if (itemstack.count <= 0) {
                     itemstack.a(this);
-                    this.U();
+                    this.V();
                 }
             }
         }
     }
 
-    public ItemStack T() {
+    public ItemStack U() {
         return this.inventory.getItemInHand();
     }
 
-    public void U() {
+    public void V() {
         this.inventory.setItem(this.inventory.itemInHandIndex, (ItemStack) null);
     }
 
-    public double V() {
+    public double W() {
         return (double) (this.height - 0.5F);
     }
 
-    public void D() {
+    public void C_() {
         if (!this.t || this.u >= this.E() / 2 || this.u < 0) {
             this.u = -1;
             this.t = true;
@@ -692,7 +692,7 @@ public abstract class EntityHuman extends EntityLiving {
             }
 
             if (i > 0 || k > 0) {
-                boolean flag = this.fallDistance > 0.0F && !this.onGround && !this.t() && !this.aT() && !this.hasEffect(MobEffectList.BLINDNESS) && this.vehicle == null && entity instanceof EntityLiving;
+                boolean flag = this.fallDistance > 0.0F && !this.onGround && !this.t() && !this.aU() && !this.hasEffect(MobEffectList.BLINDNESS) && this.vehicle == null && entity instanceof EntityLiving;
 
                 if (flag) {
                     i += this.random.nextInt(i / 2 + 2);
@@ -724,13 +724,13 @@ public abstract class EntityHuman extends EntityLiving {
                     this.g(entity);
                 }
 
-                ItemStack itemstack = this.T();
+                ItemStack itemstack = this.U();
 
                 if (itemstack != null && entity instanceof EntityLiving) {
                     itemstack.a((EntityLiving) entity, this);
                     if (itemstack.count <= 0) {
                         itemstack.a(this);
-                        this.U();
+                        this.V();
                     }
                 }
 
@@ -940,8 +940,8 @@ public abstract class EntityHuman extends EntityLiving {
 
     public void a(Statistic statistic, int i) {}
 
-    protected void ab() {
-        super.ab();
+    protected void ac() {
+        super.ac();
         this.a(StatisticList.u, 1);
         if (this.isSprinting()) {
             this.c(0.8F);
@@ -980,7 +980,7 @@ public abstract class EntityHuman extends EntityLiving {
                     this.a(StatisticList.q, i);
                     this.c(0.015F * (float) i * 0.01F);
                 }
-            } else if (this.aT()) {
+            } else if (this.aU()) {
                 i = Math.round(MathHelper.sqrt(d0 * d0 + d2 * d2) * 100.0F);
                 if (i > 0) {
                     this.a(StatisticList.m, i);
@@ -1046,7 +1046,7 @@ public abstract class EntityHuman extends EntityLiving {
         }
     }
 
-    public void ac() {
+    public void ad() {
         if (this.I > 0) {
             this.I = 10;
         } else {
@@ -1101,7 +1101,7 @@ public abstract class EntityHuman extends EntityLiving {
         return (flag || this.foodData.b()) && !this.abilities.isInvulnerable;
     }
 
-    public boolean af() {
+    public boolean ag() {
         return this.getHealth() > 0 && this.getHealth() < this.getMaxHealth();
     }
 
@@ -1148,4 +1148,6 @@ public abstract class EntityHuman extends EntityLiving {
     protected boolean g_() {
         return !this.abilities.isFlying;
     }
+
+    public void updateAbilities() {}
 }
