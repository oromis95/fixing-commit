@@ -77,7 +77,7 @@ public class EntityEnderman extends EntityMonster {
     }
 
     public void e() {
-        if (this.aS()) {
+        if (this.aT()) {
             this.damageEntity(DamageSource.DROWN, 1);
         }
 
@@ -129,7 +129,7 @@ public class EntityEnderman extends EntityMonster {
             }
         }
 
-        if (this.aS()) {
+        if (this.aT()) {
             this.target = null;
             this.x();
         }
