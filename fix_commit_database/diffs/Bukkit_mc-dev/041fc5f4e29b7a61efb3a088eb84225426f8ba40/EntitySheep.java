@@ -14,7 +14,7 @@ public class EntitySheep extends EntityAnimal {
         this.b(0.9F, 1.3F);
         float f = 0.23F;
 
-        this.ak().a(true);
+        this.al().a(true);
         this.goalSelector.a(0, new PathfinderGoalFloat(this));
         this.goalSelector.a(1, new PathfinderGoalPanic(this, 0.38F));
         this.goalSelector.a(2, new PathfinderGoalBreed(this, f));
