@@ -193,6 +193,7 @@ public abstract class Packet {
         a(105, true, false, Packet105CraftProgressBar.class);
         a(106, true, true, Packet106Transaction.class);
         a(130, true, true, Packet130UpdateSign.class);
+        a(131, true, false, Packet131.class);
         a(200, true, false, Packet200Statistic.class);
         a(255, true, true, Packet255KickDisconnect.class);
         e = new HashMap();
