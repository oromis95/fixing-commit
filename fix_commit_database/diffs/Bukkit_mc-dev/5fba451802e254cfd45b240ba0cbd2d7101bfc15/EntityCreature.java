@@ -23,13 +23,15 @@ public class EntityCreature extends EntityLiving {
             if (this.target != null) {
                 this.pathEntity = this.world.findPath(this, this.target, f);
             }
-        } else if (!this.target.P()) {
+        } else if (!this.target.Q()) {
             this.target = null;
         } else {
             float f1 = this.target.f(this);
 
             if (this.e(this.target)) {
                 this.a(this.target, f1);
+            } else {
+                this.b(this.target, f1);
             }
         }
 
@@ -63,8 +65,8 @@ public class EntityCreature extends EntityLiving {
         }
 
         int l1 = MathHelper.floor(this.boundingBox.b + 0.5D);
-        boolean flag1 = this.Z();
-        boolean flag2 = this.aa();
+        boolean flag1 = this.aa();
+        boolean flag2 = this.ab();
 
         this.pitch = 0.0F;
         if (this.pathEntity != null && this.random.nextInt(100) != 0) {
@@ -81,7 +83,7 @@ public class EntityCreature extends EntityLiving {
                 }
             }
 
-            this.ay = false;
+            this.aB = false;
             if (vec3d != null) {
                 double d1 = vec3d.a - this.locX;
                 double d2 = vec3d.c - this.locZ;
@@ -89,7 +91,7 @@ public class EntityCreature extends EntityLiving {
                 float f4 = (float) (Math.atan2(d2, d1) * 180.0D / 3.1415927410125732D) - 90.0F;
                 float f5 = f4 - this.yaw;
 
-                for (this.aw = this.aA; f5 < -180.0F; f5 += 360.0F) {
+                for (this.az = this.aD; f5 < -180.0F; f5 += 360.0F) {
                     ;
                 }
 
@@ -113,12 +115,12 @@ public class EntityCreature extends EntityLiving {
 
                     this.yaw = (float) (Math.atan2(d5, d4) * 180.0D / 3.1415927410125732D) - 90.0F;
                     f5 = (f6 - this.yaw + 90.0F) * 3.1415927F / 180.0F;
-                    this.av = -MathHelper.sin(f5) * this.aw * 1.0F;
-                    this.aw = MathHelper.cos(f5) * this.aw * 1.0F;
+                    this.ay = -MathHelper.sin(f5) * this.az * 1.0F;
+                    this.az = MathHelper.cos(f5) * this.az * 1.0F;
                 }
 
                 if (d3 > 0.0D) {
-                    this.ay = true;
+                    this.aB = true;
                 }
             }
 
@@ -127,11 +129,11 @@ public class EntityCreature extends EntityLiving {
             }
 
             if (this.positionChanged && !this.C()) {
-                this.ay = true;
+                this.aB = true;
             }
 
             if (this.random.nextFloat() < 0.8F && (flag1 || flag2)) {
-                this.ay = true;
+                this.aB = true;
             }
         } else {
             super.c_();
@@ -141,6 +143,8 @@ public class EntityCreature extends EntityLiving {
 
     protected void a(Entity entity, float f) {}
 
+    protected void b(Entity entity, float f) {}
+
     protected float a(int i, int j, int k) {
         return 0.0F;
     }
