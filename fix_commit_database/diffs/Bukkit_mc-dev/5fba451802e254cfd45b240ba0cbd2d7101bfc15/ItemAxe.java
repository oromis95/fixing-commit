@@ -2,9 +2,9 @@ package net.minecraft.server;
 
 public class ItemAxe extends ItemTool {
 
-    private static Block[] bi = new Block[] { Block.WOOD, Block.BOOKSHELF, Block.LOG, Block.CHEST};
+    private static Block[] bj = new Block[] { Block.WOOD, Block.BOOKSHELF, Block.LOG, Block.CHEST};
 
     protected ItemAxe(int i, EnumToolMaterial enumtoolmaterial) {
-        super(i, 3, enumtoolmaterial, bi);
+        super(i, 3, enumtoolmaterial, bj);
     }
 }
