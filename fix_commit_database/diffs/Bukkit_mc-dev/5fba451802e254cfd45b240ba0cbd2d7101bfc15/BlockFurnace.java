@@ -4,11 +4,12 @@ import java.util.Random;
 
 public class BlockFurnace extends BlockContainer {
 
-    private final boolean a;
+    private Random a = new Random();
+    private final boolean b;
 
     protected BlockFurnace(int i, boolean flag) {
         super(i, Material.STONE);
-        this.a = flag;
+        this.b = flag;
         this.textureId = 45;
     }
 
@@ -101,4 +102,37 @@ public class BlockFurnace extends BlockContainer {
             world.setData(i, j, k, 4);
         }
     }
+
+    public void remove(World world, int i, int j, int k) {
+        TileEntityFurnace tileentityfurnace = (TileEntityFurnace) world.getTileEntity(i, j, k);
+
+        for (int l = 0; l < tileentityfurnace.getSize(); ++l) {
+            ItemStack itemstack = tileentityfurnace.getItem(l);
+
+            if (itemstack != null) {
+                float f = this.a.nextFloat() * 0.8F + 0.1F;
+                float f1 = this.a.nextFloat() * 0.8F + 0.1F;
+                float f2 = this.a.nextFloat() * 0.8F + 0.1F;
+
+                while (itemstack.count > 0) {
+                    int i1 = this.a.nextInt(21) + 10;
+
+                    if (i1 > itemstack.count) {
+                        i1 = itemstack.count;
+                    }
+
+                    itemstack.count -= i1;
+                    EntityItem entityitem = new EntityItem(world, (double) ((float) i + f), (double) ((float) j + f1), (double) ((float) k + f2), new ItemStack(itemstack.id, i1, itemstack.getData()));
+                    float f3 = 0.05F;
+
+                    entityitem.motX = (double) ((float) this.a.nextGaussian() * f3);
+                    entityitem.motY = (double) ((float) this.a.nextGaussian() * f3 + 0.2F);
+                    entityitem.motZ = (double) ((float) this.a.nextGaussian() * f3);
+                    world.addEntity(entityitem);
+                }
+            }
+        }
+
+        super.remove(world, i, j, k);
+    }
 }
