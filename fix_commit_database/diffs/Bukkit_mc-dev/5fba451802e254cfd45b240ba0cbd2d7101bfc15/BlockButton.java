@@ -13,7 +13,7 @@ public class BlockButton extends Block {
         return null;
     }
 
-    public int b() {
+    public int c() {
         return 20;
     }
 
@@ -21,6 +21,14 @@ public class BlockButton extends Block {
         return false;
     }
 
+    public boolean b() {
+        return false;
+    }
+
+    public boolean canPlace(World world, int i, int j, int k, int l) {
+        return l == 2 && world.d(i, j, k + 1) ? true : (l == 3 && world.d(i, j, k - 1) ? true : (l == 4 && world.d(i + 1, j, k) ? true : l == 5 && world.d(i - 1, j, k)));
+    }
+
     public boolean canPlace(World world, int i, int j, int k) {
         return world.d(i - 1, j, k) ? true : (world.d(i + 1, j, k) ? true : (world.d(i, j, k - 1) ? true : world.d(i, j, k + 1)));
     }
@@ -71,7 +79,7 @@ public class BlockButton extends Block {
             }
 
             if (flag) {
-                this.a_(world, i, j, k, world.getData(i, j, k));
+                this.b_(world, i, j, k, world.getData(i, j, k));
                 world.setTypeId(i, j, k, 0);
             }
         }
@@ -79,7 +87,7 @@ public class BlockButton extends Block {
 
     private boolean h(World world, int i, int j, int k) {
         if (!this.canPlace(world, i, j, k)) {
-            this.a_(world, i, j, k, world.getData(i, j, k));
+            this.b_(world, i, j, k, world.getData(i, j, k));
             world.setTypeId(i, j, k, 0);
             return false;
         } else {
@@ -139,7 +147,7 @@ public class BlockButton extends Block {
                 world.applyPhysics(i, j - 1, k, this.id);
             }
 
-            world.c(i, j, k, this.id, this.b());
+            world.c(i, j, k, this.id, this.c());
             return true;
         }
     }
@@ -167,7 +175,7 @@ public class BlockButton extends Block {
         super.remove(world, i, j, k);
     }
 
-    public boolean b(IBlockAccess iblockaccess, int i, int j, int k, int l) {
+    public boolean a(IBlockAccess iblockaccess, int i, int j, int k, int l) {
         return (iblockaccess.getData(i, j, k) & 8) > 0;
     }
 
