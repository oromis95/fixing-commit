@@ -2,6 +2,7 @@ package net.minecraft.server;
 
 import java.io.DataInput;
 import java.io.DataOutput;
+import java.util.Collection;
 import java.util.HashMap;
 import java.util.Iterator;
 import java.util.Map;
@@ -34,6 +35,10 @@ public class NBTTagCompound extends NBTBase {
         }
     }
 
+    public Collection c() {
+        return this.a.values();
+    }
+
     public byte a() {
         return (byte) 10;
     }
