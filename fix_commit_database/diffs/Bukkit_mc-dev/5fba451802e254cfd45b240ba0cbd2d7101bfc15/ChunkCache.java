@@ -64,6 +64,6 @@ public class ChunkCache implements IBlockAccess {
     public boolean d(int i, int j, int k) {
         Block block = Block.byId[this.getTypeId(i, j, k)];
 
-        return block == null ? false : block.a();
+        return block == null ? false : block.material.isSolid() && block.b();
     }
 }
