@@ -5,14 +5,14 @@ import java.util.Random;
 public class BlockGrass extends Block {
 
     protected BlockGrass(int i) {
-        super(i, Material.EARTH);
+        super(i, Material.GRASS);
         this.textureId = 3;
         this.a(true);
     }
 
     public void a(World world, int i, int j, int k, Random random) {
         if (!world.isStatic) {
-            if (world.getLightLevel(i, j + 1, k) < 4 && world.getMaterial(i, j + 1, k).blocksLight()) {
+            if (world.getLightLevel(i, j + 1, k) < 4 && Block.q[world.getTypeId(i, j + 1, k)] > 2) {
                 if (random.nextInt(4) != 0) {
                     return;
                 }
@@ -22,8 +22,9 @@ public class BlockGrass extends Block {
                 int l = i + random.nextInt(3) - 1;
                 int i1 = j + random.nextInt(5) - 3;
                 int j1 = k + random.nextInt(3) - 1;
+                int k1 = world.getTypeId(l, i1 + 1, j1);
 
-                if (world.getTypeId(l, i1, j1) == Block.DIRT.id && world.getLightLevel(l, i1 + 1, j1) >= 4 && !world.getMaterial(l, i1 + 1, j1).blocksLight()) {
+                if (world.getTypeId(l, i1, j1) == Block.DIRT.id && world.getLightLevel(l, i1 + 1, j1) >= 4 && Block.q[k1] <= 2) {
                     world.setTypeId(l, i1, j1, Block.GRASS.id);
                 }
             }
