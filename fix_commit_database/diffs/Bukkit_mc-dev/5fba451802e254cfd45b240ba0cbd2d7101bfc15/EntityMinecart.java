@@ -28,7 +28,7 @@ public class EntityMinecart extends Entity implements IInventory {
         this.b = 0;
         this.c = 1;
         this.i = false;
-        this.aE = true;
+        this.aH = true;
         this.b(0.98F, 0.7F);
         this.height = this.width / 2.0F;
     }
@@ -71,17 +71,49 @@ public class EntityMinecart extends Entity implements IInventory {
         if (!this.world.isStatic && !this.dead) {
             this.c = -this.c;
             this.b = 10;
-            this.ab();
+            this.ac();
             this.damage += i * 10;
             if (this.damage > 40) {
+                if (this.passenger != null) {
+                    this.passenger.mount(this);
+                }
+
+                this.die();
                 this.a(Item.MINECART.id, 1, 0.0F);
                 if (this.type == 1) {
+                    EntityMinecart entityminecart = this;
+
+                    for (int j = 0; j < entityminecart.getSize(); ++j) {
+                        ItemStack itemstack = entityminecart.getItem(j);
+
+                        if (itemstack != null) {
+                            float f = this.random.nextFloat() * 0.8F + 0.1F;
+                            float f1 = this.random.nextFloat() * 0.8F + 0.1F;
+                            float f2 = this.random.nextFloat() * 0.8F + 0.1F;
+
+                            while (itemstack.count > 0) {
+                                int k = this.random.nextInt(21) + 10;
+
+                                if (k > itemstack.count) {
+                                    k = itemstack.count;
+                                }
+
+                                itemstack.count -= k;
+                                EntityItem entityitem = new EntityItem(this.world, this.locX + (double) f, this.locY + (double) f1, this.locZ + (double) f2, new ItemStack(itemstack.id, k, itemstack.getData()));
+                                float f3 = 0.05F;
+
+                                entityitem.motX = (double) ((float) this.random.nextGaussian() * f3);
+                                entityitem.motY = (double) ((float) this.random.nextGaussian() * f3 + 0.2F);
+                                entityitem.motZ = (double) ((float) this.random.nextGaussian() * f3);
+                                this.world.addEntity(entityitem);
+                            }
+                        }
+                    }
+
                     this.a(Block.CHEST.id, 1, 0.0F);
                 } else if (this.type == 2) {
                     this.a(Block.FURNACE.id, 1, 0.0F);
                 }
-
-                this.die();
             }
 
             return true;
@@ -179,7 +211,7 @@ public class EntityMinecart extends Entity implements IInventory {
             int l = this.world.getTypeId(i, j, k);
 
             if (BlockMinecartTrack.c(l)) {
-                Vec3D vec3d = this.g(this.locX, this.locY, this.locZ);
+                Vec3D vec3d = this.h(this.locX, this.locY, this.locZ);
                 int i1 = this.world.getData(i, j, k);
 
                 this.locY = (double) j;
@@ -191,7 +223,7 @@ public class EntityMinecart extends Entity implements IInventory {
                     flag2 = !flag1;
                 }
 
-                if (((BlockMinecartTrack) Block.byId[l]).d()) {
+                if (((BlockMinecartTrack) Block.byId[l]).e()) {
                     i1 &= 7;
                 }
 
@@ -333,7 +365,7 @@ public class EntityMinecart extends Entity implements IInventory {
                     this.motZ *= 0.9599999785423279D;
                 }
 
-                Vec3D vec3d1 = this.g(this.locX, this.locY, this.locZ);
+                Vec3D vec3d1 = this.h(this.locX, this.locY, this.locZ);
 
                 if (vec3d1 != null && vec3d != null) {
                     double d19 = (vec3d.b - vec3d1.b) * 0.05D;
@@ -376,7 +408,7 @@ public class EntityMinecart extends Entity implements IInventory {
                 if (flag1) {
                     d20 = Math.sqrt(this.motX * this.motX + this.motZ * this.motZ);
                     if (d20 > 0.01D) {
-                        double d21 = 0.04D;
+                        double d21 = 0.06D;
 
                         this.motX += this.motX / d20 * d21;
                         this.motZ += this.motZ / d20 * d21;
@@ -479,7 +511,7 @@ public class EntityMinecart extends Entity implements IInventory {
         }
     }
 
-    public Vec3D g(double d0, double d1, double d2) {
+    public Vec3D h(double d0, double d1, double d2) {
         int i = MathHelper.floor(d0);
         int j = MathHelper.floor(d1);
         int k = MathHelper.floor(d2);
@@ -494,7 +526,7 @@ public class EntityMinecart extends Entity implements IInventory {
             int i1 = this.world.getData(i, j, k);
 
             d1 = (double) j;
-            if (((BlockMinecartTrack) Block.byId[l]).d()) {
+            if (((BlockMinecartTrack) Block.byId[l]).e()) {
                 i1 &= 7;
             }
 
@@ -615,13 +647,22 @@ public class EntityMinecart extends Entity implements IInventory {
                     d1 *= d3;
                     d0 *= 0.10000000149011612D;
                     d1 *= 0.10000000149011612D;
-                    d0 *= (double) (1.0F - this.bq);
-                    d1 *= (double) (1.0F - this.bq);
+                    d0 *= (double) (1.0F - this.bt);
+                    d1 *= (double) (1.0F - this.bt);
                     d0 *= 0.5D;
                     d1 *= 0.5D;
                     if (entity instanceof EntityMinecart) {
-                        double d4 = entity.motX + this.motX;
-                        double d5 = entity.motZ + this.motZ;
+                        double d4 = entity.locX - this.locX;
+                        double d5 = entity.locZ - this.locZ;
+                        double d6 = d4 * entity.motZ + d5 * entity.lastX;
+
+                        d6 *= d6;
+                        if (d6 > 5.0D) {
+                            return;
+                        }
+
+                        double d7 = entity.motX + this.motX;
+                        double d8 = entity.motZ + this.motZ;
 
                         if (((EntityMinecart) entity).type == 2 && this.type != 2) {
                             this.motX *= 0.20000000298023224D;
@@ -636,14 +677,14 @@ public class EntityMinecart extends Entity implements IInventory {
                             this.motX *= 0.699999988079071D;
                             this.motZ *= 0.699999988079071D;
                         } else {
-                            d4 /= 2.0D;
-                            d5 /= 2.0D;
+                            d7 /= 2.0D;
+                            d8 /= 2.0D;
                             this.motX *= 0.20000000298023224D;
                             this.motZ *= 0.20000000298023224D;
-                            this.f(d4 - d0, 0.0D, d5 - d1);
+                            this.f(d7 - d0, 0.0D, d8 - d1);
                             entity.motX *= 0.20000000298023224D;
                             entity.motZ *= 0.20000000298023224D;
-                            entity.f(d4 + d0, 0.0D, d5 + d1);
+                            entity.f(d7 + d0, 0.0D, d8 + d1);
                         }
                     } else {
                         this.f(-d0, 0.0D, -d1);
