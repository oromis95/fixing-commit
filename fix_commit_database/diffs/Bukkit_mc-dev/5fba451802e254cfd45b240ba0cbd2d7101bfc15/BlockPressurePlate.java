@@ -7,8 +7,8 @@ public class BlockPressurePlate extends Block {
 
     private EnumMobType a;
 
-    protected BlockPressurePlate(int i, int j, EnumMobType enummobtype) {
-        super(i, j, Material.STONE);
+    protected BlockPressurePlate(int i, int j, EnumMobType enummobtype, Material material) {
+        super(i, j, material);
         this.a = enummobtype;
         this.a(true);
         float f = 0.0625F;
@@ -16,7 +16,7 @@ public class BlockPressurePlate extends Block {
         this.a(f, 0.0F, f, 1.0F - f, 0.03125F, 1.0F - f);
     }
 
-    public int b() {
+    public int c() {
         return 20;
     }
 
@@ -28,6 +28,10 @@ public class BlockPressurePlate extends Block {
         return false;
     }
 
+    public boolean b() {
+        return false;
+    }
+
     public boolean canPlace(World world, int i, int j, int k) {
         return world.d(i, j - 1, k);
     }
@@ -42,7 +46,7 @@ public class BlockPressurePlate extends Block {
         }
 
         if (flag) {
-            this.a_(world, i, j, k, world.getData(i, j, k));
+            this.b_(world, i, j, k, world.getData(i, j, k));
             world.setTypeId(i, j, k, 0);
         }
     }
@@ -102,7 +106,7 @@ public class BlockPressurePlate extends Block {
         }
 
         if (flag1) {
-            world.c(i, j, k, this.id, this.b());
+            world.c(i, j, k, this.id, this.c());
         }
     }
 
@@ -128,7 +132,7 @@ public class BlockPressurePlate extends Block {
         }
     }
 
-    public boolean b(IBlockAccess iblockaccess, int i, int j, int k, int l) {
+    public boolean a(IBlockAccess iblockaccess, int i, int j, int k, int l) {
         return iblockaccess.getData(i, j, k) > 0;
     }
 
