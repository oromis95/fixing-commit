@@ -35,11 +35,11 @@ public class BlockFlowing extends BlockFluids {
             byte b1 = -100;
 
             this.a = 0;
-            int j1 = this.e(world, i - 1, j, k, b1);
+            int j1 = this.f(world, i - 1, j, k, b1);
 
-            j1 = this.e(world, i + 1, j, k, j1);
-            j1 = this.e(world, i, j, k - 1, j1);
-            j1 = this.e(world, i, j, k + 1, j1);
+            j1 = this.f(world, i + 1, j, k, j1);
+            j1 = this.f(world, i, j, k - 1, j1);
+            j1 = this.f(world, i, j, k + 1, j1);
             i1 = j1 + b0;
             if (i1 >= 8 || j1 < 0) {
                 i1 = -1;
@@ -56,7 +56,7 @@ public class BlockFlowing extends BlockFluids {
             }
 
             if (this.a >= 2 && this.material == Material.WATER) {
-                if (world.d(i, j - 1, k)) {
+                if (world.getMaterial(i, j - 1, k).isBuildable()) {
                     i1 = 0;
                 } else if (world.getMaterial(i, j - 1, k) == this.material && world.getData(i, j, k) == 0) {
                     i1 = 0;
@@ -74,7 +74,7 @@ public class BlockFlowing extends BlockFluids {
                     world.setTypeId(i, j, k, 0);
                 } else {
                     world.setData(i, j, k, i1);
-                    world.c(i, j, k, this.id, this.b());
+                    world.c(i, j, k, this.id, this.c());
                     world.applyPhysics(i, j, k, this.id);
                 }
             } else if (flag) {
@@ -128,7 +128,7 @@ public class BlockFlowing extends BlockFluids {
                 if (this.material == Material.LAVA) {
                     this.h(world, i, j, k);
                 } else {
-                    Block.byId[i1].a_(world, i, j, k, world.getData(i, j, k));
+                    Block.byId[i1].b_(world, i, j, k, world.getData(i, j, k));
                 }
             }
 
@@ -237,14 +237,14 @@ public class BlockFlowing extends BlockFluids {
             } else {
                 Material material = Block.byId[l].material;
 
-                return material.isBuildable();
+                return material.isSolid();
             }
         } else {
             return true;
         }
     }
 
-    protected int e(World world, int i, int j, int k, int l) {
+    protected int f(World world, int i, int j, int k, int l) {
         int i1 = this.g(world, i, j, k);
 
         if (i1 < 0) {
@@ -271,7 +271,7 @@ public class BlockFlowing extends BlockFluids {
     public void e(World world, int i, int j, int k) {
         super.e(world, i, j, k);
         if (world.getTypeId(i, j, k) == this.id) {
-            world.c(i, j, k, this.id, this.b());
+            world.c(i, j, k, this.id, this.c());
         }
     }
 }
