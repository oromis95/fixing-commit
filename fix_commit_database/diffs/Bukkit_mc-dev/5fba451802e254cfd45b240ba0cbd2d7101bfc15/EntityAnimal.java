@@ -7,7 +7,7 @@ public abstract class EntityAnimal extends EntityCreature implements IAnimal {
     }
 
     protected float a(int i, int j, int k) {
-        return this.world.getTypeId(i, j - 1, k) == Block.GRASS.id ? 10.0F : this.world.l(i, j, k) - 0.5F;
+        return this.world.getTypeId(i, j - 1, k) == Block.GRASS.id ? 10.0F : this.world.m(i, j, k) - 0.5F;
     }
 
     public void b(NBTTagCompound nbttagcompound) {
@@ -23,7 +23,7 @@ public abstract class EntityAnimal extends EntityCreature implements IAnimal {
         int j = MathHelper.floor(this.boundingBox.b);
         int k = MathHelper.floor(this.locZ);
 
-        return this.world.getTypeId(i, j - 1, k) == Block.GRASS.id && this.world.getLightLevel(i, j, k) > 8 && super.d();
+        return this.world.getTypeId(i, j - 1, k) == Block.GRASS.id && this.world.j(i, j, k) > 8 && super.d();
     }
 
     public int e() {
