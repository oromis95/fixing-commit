@@ -46,7 +46,7 @@ public class BlockReed extends Block {
 
     protected final void g(World world, int i, int j, int k) {
         if (!this.f(world, i, j, k)) {
-            this.a_(world, i, j, k, world.getData(i, j, k));
+            this.b_(world, i, j, k, world.getData(i, j, k));
             world.setTypeId(i, j, k, 0);
         }
     }
@@ -66,4 +66,8 @@ public class BlockReed extends Block {
     public boolean a() {
         return false;
     }
+
+    public boolean b() {
+        return false;
+    }
 }
