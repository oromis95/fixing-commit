@@ -9,13 +9,19 @@ public class BlockFire extends Block {
 
     protected BlockFire(int i, int j) {
         super(i, j, Material.FIRE);
+        this.a(true);
+    }
+
+    public void f() {
         this.a(Block.WOOD.id, 5, 20);
+        this.a(Block.FENCE.id, 5, 20);
+        this.a(Block.WOOD_STAIRS.id, 5, 20);
         this.a(Block.LOG.id, 5, 5);
         this.a(Block.LEAVES.id, 30, 60);
         this.a(Block.BOOKSHELF.id, 30, 20);
         this.a(Block.TNT.id, 15, 100);
+        this.a(Block.LONG_GRASS.id, 60, 100);
         this.a(Block.WOOL.id, 30, 60);
-        this.a(true);
     }
 
     private void a(int i, int j, int k) {
@@ -31,27 +37,35 @@ public class BlockFire extends Block {
         return false;
     }
 
+    public boolean b() {
+        return false;
+    }
+
     public int a(Random random) {
         return 0;
     }
 
-    public int b() {
-        return 10;
+    public int c() {
+        return 40;
     }
 
     public void a(World world, int i, int j, int k, Random random) {
         boolean flag = world.getTypeId(i, j - 1, k) == Block.NETHERRACK.id;
 
-        if (!flag && world.v() && (world.q(i, j, k) || world.q(i - 1, j, k) || world.q(i + 1, j, k) || world.q(i, j, k - 1) || world.q(i, j, k + 1))) {
+        if (!this.canPlace(world, i, j, k)) {
+            world.setTypeId(i, j, k, 0);
+        }
+
+        if (!flag && world.v() && (world.s(i, j, k) || world.s(i - 1, j, k) || world.s(i + 1, j, k) || world.s(i, j, k - 1) || world.s(i, j, k + 1))) {
             world.setTypeId(i, j, k, 0);
         } else {
             int l = world.getData(i, j, k);
 
             if (l < 15) {
-                world.setData(i, j, k, l + 1);
-                world.c(i, j, k, this.id, this.b());
+                world.setRawData(i, j, k, l + random.nextInt(3) / 2);
             }
 
+            world.c(i, j, k, this.id, this.c());
             if (!flag && !this.g(world, i, j, k)) {
                 if (!world.d(i, j - 1, k) || l > 3) {
                     world.setTypeId(i, j, k, 0);
@@ -59,55 +73,60 @@ public class BlockFire extends Block {
             } else if (!flag && !this.b(world, i, j - 1, k) && l == 15 && random.nextInt(4) == 0) {
                 world.setTypeId(i, j, k, 0);
             } else {
-                if (l % 2 == 0 && l > 2) {
-                    this.a(world, i + 1, j, k, 300, random);
-                    this.a(world, i - 1, j, k, 300, random);
-                    this.a(world, i, j - 1, k, 250, random);
-                    this.a(world, i, j + 1, k, 250, random);
-                    this.a(world, i, j, k - 1, 300, random);
-                    this.a(world, i, j, k + 1, 300, random);
-
-                    for (int i1 = i - 1; i1 <= i + 1; ++i1) {
-                        for (int j1 = k - 1; j1 <= k + 1; ++j1) {
-                            for (int k1 = j - 1; k1 <= j + 4; ++k1) {
-                                if (i1 != i || k1 != j || j1 != k) {
-                                    int l1 = 100;
-
-                                    if (k1 > j + 1) {
-                                        l1 += (k1 - (j + 1)) * 100;
-                                    }
+                this.a(world, i + 1, j, k, 300, random, l);
+                this.a(world, i - 1, j, k, 300, random, l);
+                this.a(world, i, j - 1, k, 250, random, l);
+                this.a(world, i, j + 1, k, 250, random, l);
+                this.a(world, i, j, k - 1, 300, random, l);
+                this.a(world, i, j, k + 1, 300, random, l);
+
+                for (int i1 = i - 1; i1 <= i + 1; ++i1) {
+                    for (int j1 = k - 1; j1 <= k + 1; ++j1) {
+                        for (int k1 = j - 1; k1 <= j + 4; ++k1) {
+                            if (i1 != i || k1 != j || j1 != k) {
+                                int l1 = 100;
+
+                                if (k1 > j + 1) {
+                                    l1 += (k1 - (j + 1)) * 100;
+                                }
+
+                                int i2 = this.h(world, i1, k1, j1);
+
+                                if (i2 > 0) {
+                                    int j2 = (i2 + 40) / (l + 30);
+
+                                    if (j2 > 0 && random.nextInt(l1) <= j2 && (!world.v() || !world.s(i1, k1, j1)) && !world.s(i1 - 1, k1, k) && !world.s(i1 + 1, k1, j1) && !world.s(i1, k1, j1 - 1) && !world.s(i1, k1, j1 + 1)) {
+                                        int k2 = l + random.nextInt(5) / 4;
 
-                                    int i2 = this.h(world, i1, k1, j1);
+                                        if (k2 > 15) {
+                                            k2 = 15;
+                                        }
 
-                                    if (i2 > 0 && random.nextInt(l1) <= i2 && (!world.v() || !world.q(i1, k1, j1)) && !world.q(i1 - 1, k1, k) && !world.q(i1 + 1, k1, j1) && !world.q(i1, k1, j1 - 1) && !world.q(i1, k1, j1 + 1)) {
-                                        world.setTypeId(i1, k1, j1, this.id);
+                                        world.setTypeIdAndData(i1, k1, j1, this.id, k2);
                                     }
                                 }
                             }
                         }
                     }
                 }
-
-                if (l == 15) {
-                    this.a(world, i + 1, j, k, 1, random);
-                    this.a(world, i - 1, j, k, 1, random);
-                    this.a(world, i, j - 1, k, 1, random);
-                    this.a(world, i, j + 1, k, 1, random);
-                    this.a(world, i, j, k - 1, 1, random);
-                    this.a(world, i, j, k + 1, 1, random);
-                }
             }
         }
     }
 
-    private void a(World world, int i, int j, int k, int l, Random random) {
-        int i1 = this.b[world.getTypeId(i, j, k)];
+    private void a(World world, int i, int j, int k, int l, Random random, int i1) {
+        int j1 = this.b[world.getTypeId(i, j, k)];
 
-        if (random.nextInt(l) < i1) {
+        if (random.nextInt(l) < j1) {
             boolean flag = world.getTypeId(i, j, k) == Block.TNT.id;
 
-            if (random.nextInt(2) == 0 && !world.q(i, j, k)) {
-                world.setTypeId(i, j, k, this.id);
+            if (random.nextInt(i1 + 10) < 5 && !world.s(i, j, k)) {
+                int k1 = i1 + random.nextInt(5) / 4;
+
+                if (k1 > 15) {
+                    k1 = 15;
+                }
+
+                world.setTypeIdAndData(i, j, k, this.id, k1);
             } else {
                 world.setTypeId(i, j, k, 0);
             }
@@ -128,13 +147,13 @@ public class BlockFire extends Block {
         if (!world.isEmpty(i, j, k)) {
             return 0;
         } else {
-            int l = this.f(world, i + 1, j, k, b0);
+            int l = this.g(world, i + 1, j, k, b0);
 
-            l = this.f(world, i - 1, j, k, l);
-            l = this.f(world, i, j - 1, k, l);
-            l = this.f(world, i, j + 1, k, l);
-            l = this.f(world, i, j, k - 1, l);
-            l = this.f(world, i, j, k + 1, l);
+            l = this.g(world, i - 1, j, k, l);
+            l = this.g(world, i, j - 1, k, l);
+            l = this.g(world, i, j + 1, k, l);
+            l = this.g(world, i, j, k - 1, l);
+            l = this.g(world, i, j, k + 1, l);
             return l;
         }
     }
@@ -147,7 +166,7 @@ public class BlockFire extends Block {
         return this.a[iblockaccess.getTypeId(i, j, k)] > 0;
     }
 
-    public int f(World world, int i, int j, int k, int l) {
+    public int g(World world, int i, int j, int k, int l) {
         int i1 = this.a[world.getTypeId(i, j, k)];
 
         return i1 > l ? i1 : l;
@@ -168,7 +187,7 @@ public class BlockFire extends Block {
             if (!world.d(i, j - 1, k) && !this.g(world, i, j, k)) {
                 world.setTypeId(i, j, k, 0);
             } else {
-                world.c(i, j, k, this.id, this.b());
+                world.c(i, j, k, this.id, this.c());
             }
         }
     }
