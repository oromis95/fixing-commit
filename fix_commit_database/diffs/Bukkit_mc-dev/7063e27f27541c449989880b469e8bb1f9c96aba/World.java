@@ -786,10 +786,26 @@ public class World implements IBlockAccess {
         entity.l();
         if (entity instanceof EntityHuman) {
             this.d.remove((EntityHuman) entity);
-            System.out.println("Player count: " + this.d.size());
         }
     }
 
+    public void e(Entity entity) {
+        entity.l();
+        if (entity instanceof EntityHuman) {
+            this.d.remove((EntityHuman) entity);
+        }
+
+        int i = entity.ag;
+        int j = entity.ai;
+
+        if (entity.af && this.f(i, j)) {
+            this.c(i, j).b(entity);
+        }
+
+        this.b.remove(entity);
+        this.c(entity);
+    }
+
     public void a(IWorldAccess iworldaccess) {
         this.r.add(iworldaccess);
     }
@@ -938,7 +954,7 @@ public class World implements IBlockAccess {
             }
 
             if (!entity.G) {
-                this.e(entity);
+                this.f(entity);
             }
 
             if (entity.G) {
@@ -960,7 +976,7 @@ public class World implements IBlockAccess {
         }
     }
 
-    public void e(Entity entity) {
+    public void f(Entity entity) {
         this.a(entity, true);
     }
 
@@ -1002,7 +1018,7 @@ public class World implements IBlockAccess {
 
             if (flag && entity.af && entity.j != null) {
                 if (!entity.j.G && entity.j.k == entity) {
-                    this.e(entity.j);
+                    this.f(entity.j);
                 } else {
                     entity.j.k = null;
                     entity.j = null;
@@ -1261,11 +1277,11 @@ public class World implements IBlockAccess {
         } else {
             ++this.J;
 
+            boolean flag;
+
             try {
                 int i = 5000;
 
-                boolean flag;
-
                 while (this.A.size() > 0) {
                     --i;
                     if (i <= 0) {
@@ -1277,10 +1293,11 @@ public class World implements IBlockAccess {
                 }
 
                 flag = false;
-                return flag;
             } finally {
                 --this.J;
             }
+
+            return flag;
         }
     }
 
