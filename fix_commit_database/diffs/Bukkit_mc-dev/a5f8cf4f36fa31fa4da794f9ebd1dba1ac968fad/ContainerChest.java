@@ -44,9 +44,11 @@ public class ContainerChest extends Container {
 
             itemstack = itemstack1.cloneItemStack();
             if (i < this.b * 9) {
-                this.a(itemstack1, this.b * 9, this.e.size(), true);
-            } else {
-                this.a(itemstack1, 0, this.b * 9, false);
+                if (!this.a(itemstack1, this.b * 9, this.e.size(), true)) {
+                    return null;
+                }
+            } else if (!this.a(itemstack1, 0, this.b * 9, false)) {
+                return null;
             }
 
             if (itemstack1.count == 0) {
