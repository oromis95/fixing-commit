@@ -14,9 +14,13 @@ public class DistanceComparator implements Comparator {
     }
 
     public int a(Entity entity, Entity entity1) {
-        double d0 = this.b.j(entity);
-        double d1 = this.b.j(entity1);
+        double d0 = this.b.e(entity);
+        double d1 = this.b.e(entity1);
 
         return d0 < d1 ? -1 : (d0 > d1 ? 1 : 0);
     }
+
+    public int compare(Object object, Object object1) {
+        return this.a((Entity) object, (Entity) object1);
+    }
 }
