@@ -5,34 +5,34 @@ public class Material {
     public static final Material AIR = new MaterialGas(MaterialMapColor.b);
     public static final Material GRASS = new Material(MaterialMapColor.c);
     public static final Material EARTH = new Material(MaterialMapColor.l);
-    public static final Material WOOD = (new Material(MaterialMapColor.o)).f();
-    public static final Material STONE = (new Material(MaterialMapColor.m)).e();
-    public static final Material ORE = (new Material(MaterialMapColor.h)).e();
-    public static final Material WATER = (new MaterialLiquid(MaterialMapColor.n)).m();
-    public static final Material LAVA = (new MaterialLiquid(MaterialMapColor.f)).m();
-    public static final Material LEAVES = (new Material(MaterialMapColor.i)).f().o().m();
-    public static final Material PLANT = (new MaterialDecoration(MaterialMapColor.i)).m();
-    public static final Material REPLACEABLE_PLANT = (new MaterialDecoration(MaterialMapColor.i)).f().m().h();
+    public static final Material WOOD = (new Material(MaterialMapColor.o)).g();
+    public static final Material STONE = (new Material(MaterialMapColor.m)).f();
+    public static final Material ORE = (new Material(MaterialMapColor.h)).f();
+    public static final Material WATER = (new MaterialLiquid(MaterialMapColor.n)).n();
+    public static final Material LAVA = (new MaterialLiquid(MaterialMapColor.f)).n();
+    public static final Material LEAVES = (new Material(MaterialMapColor.i)).g().p().n();
+    public static final Material PLANT = (new MaterialDecoration(MaterialMapColor.i)).n();
+    public static final Material REPLACEABLE_PLANT = (new MaterialDecoration(MaterialMapColor.i)).g().n().i();
     public static final Material SPONGE = new Material(MaterialMapColor.e);
-    public static final Material CLOTH = (new Material(MaterialMapColor.e)).f();
-    public static final Material FIRE = (new MaterialGas(MaterialMapColor.b)).m();
+    public static final Material CLOTH = (new Material(MaterialMapColor.e)).g();
+    public static final Material FIRE = (new MaterialGas(MaterialMapColor.b)).n();
     public static final Material SAND = new Material(MaterialMapColor.d);
-    public static final Material ORIENTABLE = (new MaterialDecoration(MaterialMapColor.b)).m();
-    public static final Material SHATTERABLE = (new Material(MaterialMapColor.b)).o();
+    public static final Material ORIENTABLE = (new MaterialDecoration(MaterialMapColor.b)).n();
+    public static final Material SHATTERABLE = (new Material(MaterialMapColor.b)).p();
     public static final Material BUILDABLE_GLASS = new Material(MaterialMapColor.b);
-    public static final Material TNT = (new Material(MaterialMapColor.f)).f().o();
-    public static final Material CORAL = (new Material(MaterialMapColor.i)).m();
-    public static final Material ICE = (new Material(MaterialMapColor.g)).o();
-    public static final Material SNOW_LAYER = (new MaterialDecoration(MaterialMapColor.j)).h().o().e().m();
-    public static final Material SNOW_BLOCK = (new Material(MaterialMapColor.j)).e();
-    public static final Material CACTUS = (new Material(MaterialMapColor.i)).o().m();
+    public static final Material TNT = (new Material(MaterialMapColor.f)).g().p();
+    public static final Material CORAL = (new Material(MaterialMapColor.i)).n();
+    public static final Material ICE = (new Material(MaterialMapColor.g)).p();
+    public static final Material SNOW_LAYER = (new MaterialDecoration(MaterialMapColor.j)).i().p().f().n();
+    public static final Material SNOW_BLOCK = (new Material(MaterialMapColor.j)).f();
+    public static final Material CACTUS = (new Material(MaterialMapColor.i)).p().n();
     public static final Material CLAY = new Material(MaterialMapColor.k);
-    public static final Material PUMPKIN = (new Material(MaterialMapColor.i)).m();
-    public static final Material DRAGON_EGG = (new Material(MaterialMapColor.i)).m();
-    public static final Material PORTAL = (new MaterialPortal(MaterialMapColor.b)).n();
-    public static final Material CAKE = (new Material(MaterialMapColor.b)).m();
-    public static final Material WEB = (new MaterialWeb(MaterialMapColor.e)).e().m();
-    public static final Material PISTON = (new Material(MaterialMapColor.m)).n();
+    public static final Material PUMPKIN = (new Material(MaterialMapColor.i)).n();
+    public static final Material DRAGON_EGG = (new Material(MaterialMapColor.i)).n();
+    public static final Material PORTAL = (new MaterialPortal(MaterialMapColor.b)).o();
+    public static final Material CAKE = (new Material(MaterialMapColor.b)).n();
+    public static final Material WEB = (new MaterialWeb(MaterialMapColor.e)).f().n();
+    public static final Material PISTON = (new Material(MaterialMapColor.m)).o();
     private boolean canBurn;
     private boolean H;
     private boolean I;
@@ -60,17 +60,17 @@ public class Material {
         return true;
     }
 
-    private Material o() {
+    private Material p() {
         this.I = true;
         return this;
     }
 
-    protected Material e() {
+    protected Material f() {
         this.J = false;
         return this;
     }
 
-    protected Material f() {
+    protected Material g() {
         this.canBurn = true;
         return this;
     }
@@ -79,16 +79,16 @@ public class Material {
         return this.canBurn;
     }
 
-    public Material h() {
+    public Material i() {
         this.H = true;
         return this;
     }
 
-    public boolean isReplacable() {
+    public boolean isReplaceable() {
         return this.H;
     }
 
-    public boolean j() {
+    public boolean k() {
         return this.I ? false : this.isSolid();
     }
 
@@ -100,12 +100,12 @@ public class Material {
         return this.K;
     }
 
-    protected Material m() {
+    protected Material n() {
         this.K = 1;
         return this;
     }
 
-    protected Material n() {
+    protected Material o() {
         this.K = 2;
         return this;
     }
