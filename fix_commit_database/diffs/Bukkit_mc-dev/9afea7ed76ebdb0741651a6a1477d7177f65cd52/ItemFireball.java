@@ -4,9 +4,10 @@ public class ItemFireball extends Item {
 
     public ItemFireball(int i) {
         super(i);
+        this.a(CreativeModeTab.f);
     }
 
-    public boolean interactWith(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
+    public boolean interactWith(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l, float f, float f1, float f2) {
         if (world.isStatic) {
             return true;
         } else {
@@ -34,13 +35,13 @@ public class ItemFireball extends Item {
                 ++i;
             }
 
-            if (!entityhuman.d(i, j, k)) {
+            if (!entityhuman.e(i, j, k)) {
                 return false;
             } else {
                 int i1 = world.getTypeId(i, j, k);
 
                 if (i1 == 0) {
-                    world.makeSound((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "fire.ignite", 1.0F, c.nextFloat() * 0.4F + 0.8F);
+                    world.makeSound((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "fire.ignite", 1.0F, d.nextFloat() * 0.4F + 0.8F);
                     world.setTypeId(i, j, k, Block.FIRE.id);
                 }
 
