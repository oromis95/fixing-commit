@@ -6,8 +6,8 @@ public abstract class EntityAgeable extends EntityCreature {
         super(world);
     }
 
-    protected void b() {
-        super.b();
+    protected void a() {
+        super.a();
         this.datawatcher.a(12, new Integer(0));
     }
 
@@ -29,8 +29,8 @@ public abstract class EntityAgeable extends EntityCreature {
         this.setAge(nbttagcompound.getInt("Age"));
     }
 
-    public void e() {
-        super.e();
+    public void d() {
+        super.d();
         int i = this.getAge();
 
         if (i < 0) {
