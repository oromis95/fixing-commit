@@ -5,21 +5,26 @@ import java.util.Random;
 
 public class WorldGenVillageStartPiece extends WorldGenVillageWell {
 
-    public WorldChunkManager a;
-    public int b;
-    public WorldGenVillagePieceWeight c;
-    public ArrayList d;
-    public ArrayList e = new ArrayList();
-    public ArrayList f = new ArrayList();
+    public final WorldChunkManager a;
+    public final boolean b;
+    public final int c;
+    public WorldGenVillagePieceWeight d;
+    public ArrayList h;
+    public ArrayList i = new ArrayList();
+    public ArrayList j = new ArrayList();
 
     public WorldGenVillageStartPiece(WorldChunkManager worldchunkmanager, int i, Random random, int j, int k, ArrayList arraylist, int l) {
-        super(0, random, j, k);
+        super((WorldGenVillageStartPiece) null, 0, random, j, k);
         this.a = worldchunkmanager;
-        this.d = arraylist;
-        this.b = l;
+        this.h = arraylist;
+        this.c = l;
+        BiomeBase biomebase = worldchunkmanager.getBiome(j, k);
+
+        this.b = biomebase == BiomeBase.DESERT || biomebase == BiomeBase.DESERT_HILLS;
+        this.k = this;
     }
 
-    public WorldChunkManager a() {
+    public WorldChunkManager d() {
         return this.a;
     }
 }
