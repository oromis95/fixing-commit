@@ -12,20 +12,19 @@ public class BlockFlowing extends BlockFluids {
         super(i, material);
     }
 
-    private void i(World world, int i, int j, int k) {
+    private void l(World world, int i, int j, int k) {
         int l = world.getData(i, j, k);
 
         world.setRawTypeIdAndData(i, j, k, this.id + 1, l);
-        world.b(i, j, k, i, j, k);
-        world.notify(i, j, k);
+        world.d(i, j, k, i, j, k);
     }
 
-    public boolean b(IBlockAccess iblockaccess, int i, int j, int k) {
+    public boolean c(IBlockAccess iblockaccess, int i, int j, int k) {
         return this.material != Material.LAVA;
     }
 
-    public void a(World world, int i, int j, int k, Random random) {
-        int l = this.g(world, i, j, k);
+    public void b(World world, int i, int j, int k, Random random) {
+        int l = this.f_(world, i, j, k);
         byte b0 = 1;
 
         if (this.material == Material.LAVA && !world.worldProvider.d) {
@@ -39,18 +38,18 @@ public class BlockFlowing extends BlockFluids {
             byte b1 = -100;
 
             this.a = 0;
-            int j1 = this.f(world, i - 1, j, k, b1);
+            int j1 = this.e(world, i - 1, j, k, b1);
 
-            j1 = this.f(world, i + 1, j, k, j1);
-            j1 = this.f(world, i, j, k - 1, j1);
-            j1 = this.f(world, i, j, k + 1, j1);
+            j1 = this.e(world, i + 1, j, k, j1);
+            j1 = this.e(world, i, j, k - 1, j1);
+            j1 = this.e(world, i, j, k + 1, j1);
             i1 = j1 + b0;
             if (i1 >= 8 || j1 < 0) {
                 i1 = -1;
             }
 
-            if (this.g(world, i, j + 1, k) >= 0) {
-                int k1 = this.g(world, i, j + 1, k);
+            if (this.f_(world, i, j + 1, k) >= 0) {
+                int k1 = this.f_(world, i, j + 1, k);
 
                 if (k1 >= 8) {
                     i1 = k1;
@@ -72,23 +71,25 @@ public class BlockFlowing extends BlockFluids {
                 flag = false;
             }
 
-            if (i1 != l) {
+            if (i1 == l) {
+                if (flag) {
+                    this.l(world, i, j, k);
+                }
+            } else {
                 l = i1;
                 if (i1 < 0) {
                     world.setTypeId(i, j, k, 0);
                 } else {
                     world.setData(i, j, k, i1);
-                    world.c(i, j, k, this.id, this.d());
+                    world.a(i, j, k, this.id, this.p_());
                     world.applyPhysics(i, j, k, this.id);
                 }
-            } else if (flag) {
-                this.i(world, i, j, k);
             }
         } else {
-            this.i(world, i, j, k);
+            this.l(world, i, j, k);
         }
 
-        if (this.l(world, i, j - 1, k)) {
+        if (this.p(world, i, j - 1, k)) {
             if (this.material == Material.LAVA && world.getMaterial(i, j - 1, k) == Material.WATER) {
                 world.setTypeId(i, j - 1, k, Block.STONE.id);
                 this.fizz(world, i, j - 1, k);
@@ -96,12 +97,12 @@ public class BlockFlowing extends BlockFluids {
             }
 
             if (l >= 8) {
-                world.setTypeIdAndData(i, j - 1, k, this.id, l);
+                this.flow(world, i, j - 1, k, l);
             } else {
-                world.setTypeIdAndData(i, j - 1, k, this.id, l + 8);
+                this.flow(world, i, j - 1, k, l + 8);
             }
-        } else if (l >= 0 && (l == 0 || this.k(world, i, j - 1, k))) {
-            boolean[] aboolean = this.j(world, i, j, k);
+        } else if (l >= 0 && (l == 0 || this.o(world, i, j - 1, k))) {
+            boolean[] aboolean = this.n(world, i, j, k);
 
             i1 = l + b0;
             if (l >= 8) {
@@ -131,14 +132,14 @@ public class BlockFlowing extends BlockFluids {
     }
 
     private void flow(World world, int i, int j, int k, int l) {
-        if (this.l(world, i, j, k)) {
+        if (this.p(world, i, j, k)) {
             int i1 = world.getTypeId(i, j, k);
 
             if (i1 > 0) {
                 if (this.material == Material.LAVA) {
                     this.fizz(world, i, j, k);
                 } else {
-                    Block.byId[i1].b(world, i, j, k, world.getData(i, j, k), 0);
+                    Block.byId[i1].c(world, i, j, k, world.getData(i, j, k), 0);
                 }
             }
 
@@ -146,7 +147,7 @@ public class BlockFlowing extends BlockFluids {
         }
     }
 
-    private int c(World world, int i, int j, int k, int l, int i1) {
+    private int d(World world, int i, int j, int k, int l, int i1) {
         int j1 = 1000;
 
         for (int k1 = 0; k1 < 4; ++k1) {
@@ -170,13 +171,13 @@ public class BlockFlowing extends BlockFluids {
                     ++i2;
                 }
 
-                if (!this.k(world, l1, j, i2) && (world.getMaterial(l1, j, i2) != this.material || world.getData(l1, j, i2) != 0)) {
-                    if (!this.k(world, l1, j - 1, i2)) {
+                if (!this.o(world, l1, j, i2) && (world.getMaterial(l1, j, i2) != this.material || world.getData(l1, j, i2) != 0)) {
+                    if (!this.o(world, l1, j - 1, i2)) {
                         return l;
                     }
 
                     if (l < 4) {
-                        int j2 = this.c(world, l1, j, i2, l + 1, k1);
+                        int j2 = this.d(world, l1, j, i2, l + 1, k1);
 
                         if (j2 < j1) {
                             j1 = j2;
@@ -189,7 +190,7 @@ public class BlockFlowing extends BlockFluids {
         return j1;
     }
 
-    private boolean[] j(World world, int i, int j, int k) {
+    private boolean[] n(World world, int i, int j, int k) {
         int l;
         int i1;
 
@@ -214,11 +215,11 @@ public class BlockFlowing extends BlockFluids {
                 ++j1;
             }
 
-            if (!this.k(world, i1, j, j1) && (world.getMaterial(i1, j, j1) != this.material || world.getData(i1, j, j1) != 0)) {
-                if (!this.k(world, i1, j - 1, j1)) {
-                    this.c[l] = 0;
+            if (!this.o(world, i1, j, j1) && (world.getMaterial(i1, j, j1) != this.material || world.getData(i1, j, j1) != 0)) {
+                if (this.o(world, i1, j - 1, j1)) {
+                    this.c[l] = this.d(world, i1, j, j1, 1, l);
                 } else {
-                    this.c[l] = this.c(world, i1, j, j1, 1, l);
+                    this.c[l] = 0;
                 }
             }
         }
@@ -238,7 +239,7 @@ public class BlockFlowing extends BlockFluids {
         return this.b;
     }
 
-    private boolean k(World world, int i, int j, int k) {
+    private boolean o(World world, int i, int j, int k) {
         int l = world.getTypeId(i, j, k);
 
         if (l != Block.WOODEN_DOOR.id && l != Block.IRON_DOOR_BLOCK.id && l != Block.SIGN_POST.id && l != Block.LADDER.id && l != Block.SUGAR_CANE_BLOCK.id) {
@@ -254,8 +255,8 @@ public class BlockFlowing extends BlockFluids {
         }
     }
 
-    protected int f(World world, int i, int j, int k, int l) {
-        int i1 = this.g(world, i, j, k);
+    protected int e(World world, int i, int j, int k, int l) {
+        int i1 = this.f_(world, i, j, k);
 
         if (i1 < 0) {
             return l;
@@ -272,16 +273,16 @@ public class BlockFlowing extends BlockFluids {
         }
     }
 
-    private boolean l(World world, int i, int j, int k) {
+    private boolean p(World world, int i, int j, int k) {
         Material material = world.getMaterial(i, j, k);
 
-        return material == this.material ? false : (material == Material.LAVA ? false : !this.k(world, i, j, k));
+        return material == this.material ? false : (material == Material.LAVA ? false : !this.o(world, i, j, k));
     }
 
     public void onPlace(World world, int i, int j, int k) {
         super.onPlace(world, i, j, k);
         if (world.getTypeId(i, j, k) == this.id) {
-            world.c(i, j, k, this.id, this.d());
+            world.a(i, j, k, this.id, this.p_());
         }
     }
 }
