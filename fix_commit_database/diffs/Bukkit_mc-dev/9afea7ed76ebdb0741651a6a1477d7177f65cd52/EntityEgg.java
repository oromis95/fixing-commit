@@ -15,8 +15,8 @@ public class EntityEgg extends EntityProjectile {
     }
 
     protected void a(MovingObjectPosition movingobjectposition) {
-        if (movingobjectposition.entity != null && movingobjectposition.entity.damageEntity(DamageSource.projectile(this, this.shooter), 0)) {
-            ;
+        if (movingobjectposition.entity != null) {
+            movingobjectposition.entity.damageEntity(DamageSource.projectile(this, this.shooter), 0);
         }
 
         if (!this.world.isStatic && this.random.nextInt(8) == 0) {
