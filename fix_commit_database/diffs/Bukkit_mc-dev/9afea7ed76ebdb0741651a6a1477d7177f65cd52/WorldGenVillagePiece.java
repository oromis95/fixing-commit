@@ -6,24 +6,26 @@ import java.util.Random;
 abstract class WorldGenVillagePiece extends StructurePiece {
 
     private int a;
+    protected WorldGenVillageStartPiece k;
 
-    protected WorldGenVillagePiece(int i) {
+    protected WorldGenVillagePiece(WorldGenVillageStartPiece worldgenvillagestartpiece, int i) {
         super(i);
+        this.k = worldgenvillagestartpiece;
     }
 
     protected StructurePiece a(WorldGenVillageStartPiece worldgenvillagestartpiece, List list, Random random, int i, int j) {
-        switch (this.h) {
+        switch (this.f) {
         case 0:
-            return WorldGenVillagePieces.a(worldgenvillagestartpiece, list, random, this.g.a - 1, this.g.b + i, this.g.c + j, 1, this.c());
+            return WorldGenVillagePieces.a(worldgenvillagestartpiece, list, random, this.e.a - 1, this.e.b + i, this.e.c + j, 1, this.c());
 
         case 1:
-            return WorldGenVillagePieces.a(worldgenvillagestartpiece, list, random, this.g.a + j, this.g.b + i, this.g.c - 1, 2, this.c());
+            return WorldGenVillagePieces.a(worldgenvillagestartpiece, list, random, this.e.a + j, this.e.b + i, this.e.c - 1, 2, this.c());
 
         case 2:
-            return WorldGenVillagePieces.a(worldgenvillagestartpiece, list, random, this.g.a - 1, this.g.b + i, this.g.c + j, 1, this.c());
+            return WorldGenVillagePieces.a(worldgenvillagestartpiece, list, random, this.e.a - 1, this.e.b + i, this.e.c + j, 1, this.c());
 
         case 3:
-            return WorldGenVillagePieces.a(worldgenvillagestartpiece, list, random, this.g.a + j, this.g.b + i, this.g.c - 1, 2, this.c());
+            return WorldGenVillagePieces.a(worldgenvillagestartpiece, list, random, this.e.a + j, this.e.b + i, this.e.c - 1, 2, this.c());
 
         default:
             return null;
@@ -31,18 +33,18 @@ abstract class WorldGenVillagePiece extends StructurePiece {
     }
 
     protected StructurePiece b(WorldGenVillageStartPiece worldgenvillagestartpiece, List list, Random random, int i, int j) {
-        switch (this.h) {
+        switch (this.f) {
         case 0:
-            return WorldGenVillagePieces.a(worldgenvillagestartpiece, list, random, this.g.d + 1, this.g.b + i, this.g.c + j, 3, this.c());
+            return WorldGenVillagePieces.a(worldgenvillagestartpiece, list, random, this.e.d + 1, this.e.b + i, this.e.c + j, 3, this.c());
 
         case 1:
-            return WorldGenVillagePieces.a(worldgenvillagestartpiece, list, random, this.g.a + j, this.g.b + i, this.g.f + 1, 0, this.c());
+            return WorldGenVillagePieces.a(worldgenvillagestartpiece, list, random, this.e.a + j, this.e.b + i, this.e.f + 1, 0, this.c());
 
         case 2:
-            return WorldGenVillagePieces.a(worldgenvillagestartpiece, list, random, this.g.d + 1, this.g.b + i, this.g.c + j, 3, this.c());
+            return WorldGenVillagePieces.a(worldgenvillagestartpiece, list, random, this.e.d + 1, this.e.b + i, this.e.c + j, 3, this.c());
 
         case 3:
-            return WorldGenVillagePieces.a(worldgenvillagestartpiece, list, random, this.g.a + j, this.g.b + i, this.g.f + 1, 0, this.c());
+            return WorldGenVillagePieces.a(worldgenvillagestartpiece, list, random, this.e.a + j, this.e.b + i, this.e.f + 1, 0, this.c());
 
         default:
             return null;
@@ -53,10 +55,10 @@ abstract class WorldGenVillagePiece extends StructurePiece {
         int i = 0;
         int j = 0;
 
-        for (int k = this.g.c; k <= this.g.f; ++k) {
-            for (int l = this.g.a; l <= this.g.d; ++l) {
+        for (int k = this.e.c; k <= this.e.f; ++k) {
+            for (int l = this.e.a; l <= this.e.d; ++l) {
                 if (structureboundingbox.b(l, 64, k)) {
-                    i += Math.max(world.g(l, k), world.worldProvider.getSeaLevel());
+                    i += Math.max(world.h(l, k), world.worldProvider.getSeaLevel());
                     ++j;
                 }
             }
@@ -77,7 +79,7 @@ abstract class WorldGenVillagePiece extends StructurePiece {
         if (this.a < l) {
             for (int i1 = this.a; i1 < l; ++i1) {
                 int j1 = this.a(i + i1, k);
-                int k1 = this.b(j);
+                int k1 = this.a(j);
                 int l1 = this.b(i + i1, k);
 
                 if (!structureboundingbox.b(j1, k1, l1)) {
@@ -85,7 +87,7 @@ abstract class WorldGenVillagePiece extends StructurePiece {
                 }
 
                 ++this.a;
-                EntityVillager entityvillager = new EntityVillager(world, this.a(i1));
+                EntityVillager entityvillager = new EntityVillager(world, this.b(i1));
 
                 entityvillager.setPositionRotation((double) j1 + 0.5D, (double) k1, (double) l1 + 0.5D, 0.0F, 0.0F);
                 world.addEntity(entityvillager);
@@ -93,7 +95,78 @@ abstract class WorldGenVillagePiece extends StructurePiece {
         }
     }
 
-    protected int a(int i) {
+    protected int b(int i) {
         return 0;
     }
+
+    protected int d(int i, int j) {
+        if (this.k.b) {
+            if (i == Block.LOG.id) {
+                return Block.SANDSTONE.id;
+            }
+
+            if (i == Block.COBBLESTONE.id) {
+                return Block.SANDSTONE.id;
+            }
+
+            if (i == Block.WOOD.id) {
+                return Block.SANDSTONE.id;
+            }
+
+            if (i == Block.WOOD_STAIRS.id) {
+                return Block.SANDSTONE_STAIRS.id;
+            }
+
+            if (i == Block.COBBLESTONE_STAIRS.id) {
+                return Block.SANDSTONE_STAIRS.id;
+            }
+
+            if (i == Block.GRAVEL.id) {
+                return Block.SANDSTONE.id;
+            }
+        }
+
+        return i;
+    }
+
+    protected int e(int i, int j) {
+        if (this.k.b) {
+            if (i == Block.LOG.id) {
+                return 0;
+            }
+
+            if (i == Block.COBBLESTONE.id) {
+                return 0;
+            }
+
+            if (i == Block.WOOD.id) {
+                return 2;
+            }
+        }
+
+        return j;
+    }
+
+    protected void a(World world, int i, int j, int k, int l, int i1, StructureBoundingBox structureboundingbox) {
+        int j1 = this.d(i, j);
+        int k1 = this.e(i, j);
+
+        super.a(world, j1, k1, k, l, i1, structureboundingbox);
+    }
+
+    protected void a(World world, StructureBoundingBox structureboundingbox, int i, int j, int k, int l, int i1, int j1, int k1, int l1, boolean flag) {
+        int i2 = this.d(k1, 0);
+        int j2 = this.e(k1, 0);
+        int k2 = this.d(l1, 0);
+        int l2 = this.e(l1, 0);
+
+        super.a(world, structureboundingbox, i, j, k, l, i1, j1, i2, j2, k2, l2, flag);
+    }
+
+    protected void b(World world, int i, int j, int k, int l, int i1, StructureBoundingBox structureboundingbox) {
+        int j1 = this.d(i, j);
+        int k1 = this.e(i, j);
+
+        super.b(world, j1, k1, k, l, i1, structureboundingbox);
+    }
 }
