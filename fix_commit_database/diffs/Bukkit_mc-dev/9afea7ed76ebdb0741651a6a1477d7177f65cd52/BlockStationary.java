@@ -6,34 +6,34 @@ public class BlockStationary extends BlockFluids {
 
     protected BlockStationary(int i, Material material) {
         super(i, material);
-        this.a(false);
+        this.b(false);
         if (material == Material.LAVA) {
-            this.a(true);
+            this.b(true);
         }
     }
 
-    public boolean b(IBlockAccess iblockaccess, int i, int j, int k) {
+    public boolean c(IBlockAccess iblockaccess, int i, int j, int k) {
         return this.material != Material.LAVA;
     }
 
     public void doPhysics(World world, int i, int j, int k, int l) {
         super.doPhysics(world, i, j, k, l);
         if (world.getTypeId(i, j, k) == this.id) {
-            this.i(world, i, j, k);
+            this.l(world, i, j, k);
         }
     }
 
-    private void i(World world, int i, int j, int k) {
+    private void l(World world, int i, int j, int k) {
         int l = world.getData(i, j, k);
 
         world.suppressPhysics = true;
         world.setRawTypeIdAndData(i, j, k, this.id - 1, l);
-        world.b(i, j, k, i, j, k);
-        world.c(i, j, k, this.id - 1, this.d());
+        world.d(i, j, k, i, j, k);
+        world.a(i, j, k, this.id - 1, this.p_());
         world.suppressPhysics = false;
     }
 
-    public void a(World world, int i, int j, int k, Random random) {
+    public void b(World world, int i, int j, int k, Random random) {
         if (this.material == Material.LAVA) {
             int l = random.nextInt(3);
 
@@ -46,7 +46,7 @@ public class BlockStationary extends BlockFluids {
                 k += random.nextInt(3) - 1;
                 j1 = world.getTypeId(i, j, k);
                 if (j1 == 0) {
-                    if (this.j(world, i - 1, j, k) || this.j(world, i + 1, j, k) || this.j(world, i, j, k - 1) || this.j(world, i, j, k + 1) || this.j(world, i, j - 1, k) || this.j(world, i, j + 1, k)) {
+                    if (this.n(world, i - 1, j, k) || this.n(world, i + 1, j, k) || this.n(world, i, j, k - 1) || this.n(world, i, j, k + 1) || this.n(world, i, j - 1, k) || this.n(world, i, j + 1, k)) {
                         world.setTypeId(i, j, k, Block.FIRE.id);
                         return;
                     }
@@ -62,7 +62,7 @@ public class BlockStationary extends BlockFluids {
                 for (int k1 = 0; k1 < 3; ++k1) {
                     i = i1 + random.nextInt(3) - 1;
                     k = j1 + random.nextInt(3) - 1;
-                    if (world.isEmpty(i, j + 1, k) && this.j(world, i, j, k)) {
+                    if (world.isEmpty(i, j + 1, k) && this.n(world, i, j, k)) {
                         world.setTypeId(i, j + 1, k, Block.FIRE.id);
                     }
                 }
@@ -70,7 +70,7 @@ public class BlockStationary extends BlockFluids {
         }
     }
 
-    private boolean j(World world, int i, int j, int k) {
+    private boolean n(World world, int i, int j, int k) {
         return world.getMaterial(i, j, k).isBurnable();
     }
 }
