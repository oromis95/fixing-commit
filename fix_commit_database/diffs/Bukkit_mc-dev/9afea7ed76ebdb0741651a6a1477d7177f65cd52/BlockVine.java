@@ -6,22 +6,23 @@ public class BlockVine extends Block {
 
     public BlockVine(int i) {
         super(i, 143, Material.REPLACEABLE_PLANT);
-        this.a(true);
+        this.b(true);
+        this.a(CreativeModeTab.c);
     }
 
     public void f() {
         this.a(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
     }
 
-    public int c() {
+    public int b() {
         return 20;
     }
 
-    public boolean a() {
+    public boolean d() {
         return false;
     }
 
-    public boolean b() {
+    public boolean c() {
         return false;
     }
 
@@ -75,7 +76,7 @@ public class BlockVine extends Block {
             flag = true;
         }
 
-        if (!flag && this.d(iblockaccess.getTypeId(i, j + 1, k))) {
+        if (!flag && this.e(iblockaccess.getTypeId(i, j + 1, k))) {
             f1 = Math.min(f1, 0.9375F);
             f4 = 1.0F;
             f = 0.0F;
@@ -94,36 +95,36 @@ public class BlockVine extends Block {
     public boolean canPlace(World world, int i, int j, int k, int l) {
         switch (l) {
         case 1:
-            return this.d(world.getTypeId(i, j + 1, k));
+            return this.e(world.getTypeId(i, j + 1, k));
 
         case 2:
-            return this.d(world.getTypeId(i, j, k + 1));
+            return this.e(world.getTypeId(i, j, k + 1));
 
         case 3:
-            return this.d(world.getTypeId(i, j, k - 1));
+            return this.e(world.getTypeId(i, j, k - 1));
 
         case 4:
-            return this.d(world.getTypeId(i + 1, j, k));
+            return this.e(world.getTypeId(i + 1, j, k));
 
         case 5:
-            return this.d(world.getTypeId(i - 1, j, k));
+            return this.e(world.getTypeId(i - 1, j, k));
 
         default:
             return false;
         }
     }
 
-    private boolean d(int i) {
+    private boolean e(int i) {
         if (i == 0) {
             return false;
         } else {
             Block block = Block.byId[i];
 
-            return block.b() && block.material.isSolid();
+            return block.c() && block.material.isSolid();
         }
     }
 
-    private boolean g(World world, int i, int j, int k) {
+    private boolean l(World world, int i, int j, int k) {
         int l = world.getData(i, j, k);
         int i1 = l;
 
@@ -131,13 +132,13 @@ public class BlockVine extends Block {
             for (int j1 = 0; j1 <= 3; ++j1) {
                 int k1 = 1 << j1;
 
-                if ((l & k1) != 0 && !this.d(world.getTypeId(i + Direction.a[j1], j, k + Direction.b[j1])) && (world.getTypeId(i, j + 1, k) != this.id || (world.getData(i, j + 1, k) & k1) == 0)) {
+                if ((l & k1) != 0 && !this.e(world.getTypeId(i + Direction.a[j1], j, k + Direction.b[j1])) && (world.getTypeId(i, j + 1, k) != this.id || (world.getData(i, j + 1, k) & k1) == 0)) {
                     i1 &= ~k1;
                 }
             }
         }
 
-        if (i1 == 0 && !this.d(world.getTypeId(i, j + 1, k))) {
+        if (i1 == 0 && !this.e(world.getTypeId(i, j + 1, k))) {
             return false;
         } else {
             if (i1 != l) {
@@ -149,13 +150,13 @@ public class BlockVine extends Block {
     }
 
     public void doPhysics(World world, int i, int j, int k, int l) {
-        if (!world.isStatic && !this.g(world, i, j, k)) {
-            this.b(world, i, j, k, world.getData(i, j, k), 0);
+        if (!world.isStatic && !this.l(world, i, j, k)) {
+            this.c(world, i, j, k, world.getData(i, j, k), 0);
             world.setTypeId(i, j, k, 0);
         }
     }
 
-    public void a(World world, int i, int j, int k, Random random) {
+    public void b(World world, int i, int j, int k, Random random) {
         if (!world.isStatic && world.random.nextInt(4) == 0) {
             byte b0 = 4;
             int l = 5;
@@ -194,7 +195,7 @@ public class BlockVine extends Block {
                 l1 = world.random.nextInt(16) & i1;
                 if (l1 > 0) {
                     for (i2 = 0; i2 <= 3; ++i2) {
-                        if (!this.d(world.getTypeId(i + Direction.a[i2], j + 1, k + Direction.b[i2]))) {
+                        if (!this.e(world.getTypeId(i + Direction.a[i2], j + 1, k + Direction.b[i2]))) {
                             l1 &= ~(1 << i2);
                         }
                     }
@@ -213,21 +214,21 @@ public class BlockVine extends Block {
 
                     l1 = world.getTypeId(i + Direction.a[k1], j, k + Direction.b[k1]);
                     if (l1 != 0 && Block.byId[l1] != null) {
-                        if (Block.byId[l1].material.j() && Block.byId[l1].b()) {
+                        if (Block.byId[l1].material.k() && Block.byId[l1].c()) {
                             world.setData(i, j, k, i1 | 1 << k1);
                         }
                     } else {
                         i2 = k1 + 1 & 3;
                         j2 = k1 + 3 & 3;
-                        if ((i1 & 1 << i2) != 0 && this.d(world.getTypeId(i + Direction.a[k1] + Direction.a[i2], j, k + Direction.b[k1] + Direction.b[i2]))) {
+                        if ((i1 & 1 << i2) != 0 && this.e(world.getTypeId(i + Direction.a[k1] + Direction.a[i2], j, k + Direction.b[k1] + Direction.b[i2]))) {
                             world.setTypeIdAndData(i + Direction.a[k1], j, k + Direction.b[k1], this.id, 1 << i2);
-                        } else if ((i1 & 1 << j2) != 0 && this.d(world.getTypeId(i + Direction.a[k1] + Direction.a[j2], j, k + Direction.b[k1] + Direction.b[j2]))) {
+                        } else if ((i1 & 1 << j2) != 0 && this.e(world.getTypeId(i + Direction.a[k1] + Direction.a[j2], j, k + Direction.b[k1] + Direction.b[j2]))) {
                             world.setTypeIdAndData(i + Direction.a[k1], j, k + Direction.b[k1], this.id, 1 << j2);
-                        } else if ((i1 & 1 << i2) != 0 && world.isEmpty(i + Direction.a[k1] + Direction.a[i2], j, k + Direction.b[k1] + Direction.b[i2]) && this.d(world.getTypeId(i + Direction.a[i2], j, k + Direction.b[i2]))) {
+                        } else if ((i1 & 1 << i2) != 0 && world.isEmpty(i + Direction.a[k1] + Direction.a[i2], j, k + Direction.b[k1] + Direction.b[i2]) && this.e(world.getTypeId(i + Direction.a[i2], j, k + Direction.b[i2]))) {
                             world.setTypeIdAndData(i + Direction.a[k1] + Direction.a[i2], j, k + Direction.b[k1] + Direction.b[i2], this.id, 1 << (k1 + 2 & 3));
-                        } else if ((i1 & 1 << j2) != 0 && world.isEmpty(i + Direction.a[k1] + Direction.a[j2], j, k + Direction.b[k1] + Direction.b[j2]) && this.d(world.getTypeId(i + Direction.a[j2], j, k + Direction.b[j2]))) {
+                        } else if ((i1 & 1 << j2) != 0 && world.isEmpty(i + Direction.a[k1] + Direction.a[j2], j, k + Direction.b[k1] + Direction.b[j2]) && this.e(world.getTypeId(i + Direction.a[j2], j, k + Direction.b[j2]))) {
                             world.setTypeIdAndData(i + Direction.a[k1] + Direction.a[j2], j, k + Direction.b[k1] + Direction.b[j2], this.id, 1 << (k1 + 2 & 3));
-                        } else if (this.d(world.getTypeId(i + Direction.a[k1], j + 1, k + Direction.b[k1]))) {
+                        } else if (this.e(world.getTypeId(i + Direction.a[k1], j + 1, k + Direction.b[k1]))) {
                             world.setTypeIdAndData(i + Direction.a[k1], j, k + Direction.b[k1], this.id, 0);
                         }
                     }
@@ -250,7 +251,7 @@ public class BlockVine extends Block {
         }
     }
 
-    public void postPlace(World world, int i, int j, int k, int l) {
+    public void postPlace(World world, int i, int j, int k, int l, float f, float f1, float f2) {
         byte b0 = 0;
 
         switch (l) {
@@ -284,7 +285,7 @@ public class BlockVine extends Block {
     }
 
     public void a(World world, EntityHuman entityhuman, int i, int j, int k, int l) {
-        if (!world.isStatic && entityhuman.U() != null && entityhuman.U().id == Item.SHEARS.id) {
+        if (!world.isStatic && entityhuman.bC() != null && entityhuman.bC().id == Item.SHEARS.id) {
             entityhuman.a(StatisticList.C[this.id], 1);
             this.a(world, i, j, k, new ItemStack(Block.VINE, 1, 0));
         } else {
