@@ -7,7 +7,7 @@ public class TileEntity {
 
     private static Map a = new HashMap();
     private static Map b = new HashMap();
-    public World world;
+    protected World world;
     public int x;
     public int y;
     public int z;
@@ -18,7 +18,7 @@ public class TileEntity {
     public TileEntity() {}
 
     private static void a(Class oclass, String s) {
-        if (b.containsKey(s)) {
+        if (a.containsKey(s)) {
             throw new IllegalArgumentException("Duplicate id: " + s);
         } else {
             a.put(s, oclass);
@@ -26,6 +26,14 @@ public class TileEntity {
         }
     }
 
+    public void a(World world) {
+        this.world = world;
+    }
+
+    public boolean m() {
+        return this.world != null;
+    }
+
     public void a(NBTTagCompound nbttagcompound) {
         this.x = nbttagcompound.getInt("x");
         this.y = nbttagcompound.getInt("y");
@@ -45,7 +53,7 @@ public class TileEntity {
         }
     }
 
-    public void q_() {}
+    public void g() {}
 
     public static TileEntity c(NBTTagCompound nbttagcompound) {
         TileEntity tileentity = null;
@@ -69,7 +77,7 @@ public class TileEntity {
         return tileentity;
     }
 
-    public int k() {
+    public int n() {
         if (this.p == -1) {
             this.p = this.world.getData(this.x, this.y, this.z);
         }
@@ -84,11 +92,11 @@ public class TileEntity {
         }
     }
 
-    public Packet d() {
+    public Packet e() {
         return null;
     }
 
-    public boolean l() {
+    public boolean p() {
         return this.o;
     }
 
@@ -96,7 +104,7 @@ public class TileEntity {
         this.o = true;
     }
 
-    public void m() {
+    public void q() {
         this.o = false;
     }
 
@@ -110,6 +118,7 @@ public class TileEntity {
     static {
         a(TileEntityFurnace.class, "Furnace");
         a(TileEntityChest.class, "Chest");
+        a(TileEntityEnderChest.class, "EnderChest");
         a(TileEntityRecordPlayer.class, "RecordPlayer");
         a(TileEntityDispenser.class, "Trap");
         a(TileEntitySign.class, "Sign");
