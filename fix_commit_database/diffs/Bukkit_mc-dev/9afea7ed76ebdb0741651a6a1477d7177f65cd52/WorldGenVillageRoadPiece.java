@@ -2,7 +2,7 @@ package net.minecraft.server;
 
 public abstract class WorldGenVillageRoadPiece extends WorldGenVillagePiece {
 
-    protected WorldGenVillageRoadPiece(int i) {
-        super(i);
+    protected WorldGenVillageRoadPiece(WorldGenVillageStartPiece worldgenvillagestartpiece, int i) {
+        super(worldgenvillagestartpiece, i);
     }
 }
