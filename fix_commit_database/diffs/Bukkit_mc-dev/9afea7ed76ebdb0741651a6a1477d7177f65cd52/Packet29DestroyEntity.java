@@ -5,20 +5,28 @@ import java.io.DataOutputStream;
 
 public class Packet29DestroyEntity extends Packet {
 
-    public int a;
+    public int[] a;
 
     public Packet29DestroyEntity() {}
 
-    public Packet29DestroyEntity(int i) {
-        this.a = i;
+    public Packet29DestroyEntity(int... aint) {
+        this.a = aint;
     }
 
     public void a(DataInputStream datainputstream) {
-        this.a = datainputstream.readInt();
+        this.a = new int[datainputstream.readByte()];
+
+        for (int i = 0; i < this.a.length; ++i) {
+            this.a[i] = datainputstream.readInt();
+        }
     }
 
     public void a(DataOutputStream dataoutputstream) {
-        dataoutputstream.writeInt(this.a);
+        dataoutputstream.writeByte(this.a.length);
+
+        for (int i = 0; i < this.a.length; ++i) {
+            dataoutputstream.writeInt(this.a[i]);
+        }
     }
 
     public void handle(NetHandler nethandler) {
@@ -26,6 +34,6 @@ public class Packet29DestroyEntity extends Packet {
     }
 
     public int a() {
-        return 4;
+        return 1 + this.a.length * 4;
     }
 }
