@@ -4,13 +4,12 @@ import java.util.Random;
 
 public class BlockMonsterEggs extends Block {
 
+    public static final String[] a = new String[] { "stone", "cobble", "brick"};
+
     public BlockMonsterEggs(int i) {
         super(i, 1, Material.CLAY);
         this.c(0.0F);
-    }
-
-    public void a(World world, EntityHuman entityhuman, int i, int j, int k, int l) {
-        super.a(world, entityhuman, i, j, k, l);
+        this.a(CreativeModeTab.c);
     }
 
     public int a(int i, int j) {
@@ -23,7 +22,7 @@ public class BlockMonsterEggs extends Block {
 
             entitysilverfish.setPositionRotation((double) i + 0.5D, (double) j, (double) k + 0.5D, 0.0F, 0.0F);
             world.addEntity(entitysilverfish);
-            entitysilverfish.aC();
+            entitysilverfish.aK();
         }
 
         super.postBreak(world, i, j, k, l);
@@ -33,15 +32,15 @@ public class BlockMonsterEggs extends Block {
         return 0;
     }
 
-    public static boolean d(int i) {
+    public static boolean e(int i) {
         return i == Block.STONE.id || i == Block.COBBLESTONE.id || i == Block.SMOOTH_BRICK.id;
     }
 
-    public static int e(int i) {
+    public static int f(int i) {
         return i == Block.COBBLESTONE.id ? 1 : (i == Block.SMOOTH_BRICK.id ? 2 : 0);
     }
 
-    protected ItemStack a_(int i) {
+    protected ItemStack c_(int i) {
         Block block = Block.STONE;
 
         if (i == 1) {
