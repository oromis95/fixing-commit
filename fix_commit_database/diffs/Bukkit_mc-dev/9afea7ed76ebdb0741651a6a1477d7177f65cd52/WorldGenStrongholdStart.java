@@ -1,27 +1,19 @@
 package net.minecraft.server;
 
 import java.util.ArrayList;
-import java.util.List;
 import java.util.Random;
 
-class WorldGenStrongholdStart extends StructureStart {
+public class WorldGenStrongholdStart extends WorldGenStrongholdStairs2 {
 
-    public WorldGenStrongholdStart(World world, Random random, int i, int j) {
-        WorldGenStrongholdPieces.a();
-        WorldGenStrongholdStairs2 worldgenstrongholdstairs2 = new WorldGenStrongholdStairs2(0, random, (i << 4) + 2, (j << 4) + 2);
+    public WorldGenStrongholdPieceWeight a;
+    public WorldGenStrongholdPortalRoom b;
+    public ArrayList c = new ArrayList();
 
-        this.a.add(worldgenstrongholdstairs2);
-        worldgenstrongholdstairs2.a(worldgenstrongholdstairs2, this.a, random);
-        ArrayList arraylist = worldgenstrongholdstairs2.c;
-
-        while (!arraylist.isEmpty()) {
-            int k = random.nextInt(arraylist.size());
-            StructurePiece structurepiece = (StructurePiece) arraylist.remove(k);
-
-            structurepiece.a((StructurePiece) worldgenstrongholdstairs2, (List) this.a, random);
-        }
+    public WorldGenStrongholdStart(int i, Random random, int j, int k) {
+        super(0, random, j, k);
+    }
 
-        this.d();
-        this.a(world, random, 10);
+    public ChunkPosition a() {
+        return this.b != null ? this.b.a() : super.a();
     }
 }
