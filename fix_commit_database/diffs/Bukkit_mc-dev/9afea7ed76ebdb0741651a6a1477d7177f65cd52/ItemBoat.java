@@ -1,5 +1,6 @@
 package net.minecraft.server;
 
+import java.util.Iterator;
 import java.util.List;
 
 public class ItemBoat extends Item {
@@ -7,6 +8,7 @@ public class ItemBoat extends Item {
     public ItemBoat(int i) {
         super(i);
         this.maxStackSize = 1;
+        this.a(CreativeModeTab.e);
     }
 
     public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
@@ -16,7 +18,7 @@ public class ItemBoat extends Item {
         double d0 = entityhuman.lastX + (entityhuman.locX - entityhuman.lastX) * (double) f;
         double d1 = entityhuman.lastY + (entityhuman.locY - entityhuman.lastY) * (double) f + 1.62D - (double) entityhuman.height;
         double d2 = entityhuman.lastZ + (entityhuman.locZ - entityhuman.lastZ) * (double) f;
-        Vec3D vec3d = Vec3D.create(d0, d1, d2);
+        Vec3D vec3d = Vec3D.a().create(d0, d1, d2);
         float f3 = MathHelper.cos(-f2 * 0.017453292F - 3.1415927F);
         float f4 = MathHelper.sin(-f2 * 0.017453292F - 3.1415927F);
         float f5 = -MathHelper.cos(-f1 * 0.017453292F);
@@ -30,16 +32,17 @@ public class ItemBoat extends Item {
         if (movingobjectposition == null) {
             return itemstack;
         } else {
-            Vec3D vec3d2 = entityhuman.f(f);
+            Vec3D vec3d2 = entityhuman.i(f);
             boolean flag = false;
             float f9 = 1.0F;
             List list = world.getEntities(entityhuman, entityhuman.boundingBox.a(vec3d2.a * d3, vec3d2.b * d3, vec3d2.c * d3).grow((double) f9, (double) f9, (double) f9));
+            Iterator iterator = list.iterator();
 
-            for (int i = 0; i < list.size(); ++i) {
-                Entity entity = (Entity) list.get(i);
+            while (iterator.hasNext()) {
+                Entity entity = (Entity) iterator.next();
 
-                if (entity.o_()) {
-                    float f10 = entity.j_();
+                if (entity.L()) {
+                    float f10 = entity.Y();
                     AxisAlignedBB axisalignedbb = entity.boundingBox.grow((double) f10, (double) f10, (double) f10);
 
                     if (axisalignedbb.a(vec3d)) {
@@ -52,16 +55,16 @@ public class ItemBoat extends Item {
                 return itemstack;
             } else {
                 if (movingobjectposition.type == EnumMovingObjectType.TILE) {
-                    int j = movingobjectposition.b;
-                    int k = movingobjectposition.c;
-                    int l = movingobjectposition.d;
+                    int i = movingobjectposition.b;
+                    int j = movingobjectposition.c;
+                    int k = movingobjectposition.d;
 
                     if (!world.isStatic) {
-                        if (world.getTypeId(j, k, l) == Block.SNOW.id) {
-                            --k;
+                        if (world.getTypeId(i, j, k) == Block.SNOW.id) {
+                            --j;
                         }
 
-                        world.addEntity(new EntityBoat(world, (double) ((float) j + 0.5F), (double) ((float) k + 1.0F), (double) ((float) l + 0.5F)));
+                        world.addEntity(new EntityBoat(world, (double) ((float) i + 0.5F), (double) ((float) j + 1.0F), (double) ((float) k + 0.5F)));
                     }
 
                     if (!entityhuman.abilities.canInstantlyBuild) {
