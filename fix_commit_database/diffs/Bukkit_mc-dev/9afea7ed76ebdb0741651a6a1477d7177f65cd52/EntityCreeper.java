@@ -3,7 +3,7 @@ package net.minecraft.server;
 public class EntityCreeper extends EntityMonster {
 
     int fuseTicks;
-    int b;
+    int e;
 
     public EntityCreeper(World world) {
         super(world);
@@ -19,7 +19,7 @@ public class EntityCreeper extends EntityMonster {
         this.targetSelector.a(2, new PathfinderGoalHurtByTarget(this, false));
     }
 
-    public boolean c_() {
+    public boolean aV() {
         return true;
     }
 
@@ -27,8 +27,8 @@ public class EntityCreeper extends EntityMonster {
         return 20;
     }
 
-    protected void b() {
-        super.b();
+    protected void a() {
+        super.a();
         this.datawatcher.a(16, Byte.valueOf((byte) -1));
         this.datawatcher.a(17, Byte.valueOf((byte) 0));
     }
@@ -45,10 +45,10 @@ public class EntityCreeper extends EntityMonster {
         this.datawatcher.watch(17, Byte.valueOf((byte) (nbttagcompound.getBoolean("powered") ? 1 : 0)));
     }
 
-    public void F_() {
+    public void h_() {
         if (this.isAlive()) {
-            this.b = this.fuseTicks;
-            int i = this.A();
+            this.e = this.fuseTicks;
+            int i = this.p();
 
             if (i > 0 && this.fuseTicks == 0) {
                 this.world.makeSound(this, "random.fuse", 1.0F, 0.5F);
@@ -73,14 +73,14 @@ public class EntityCreeper extends EntityMonster {
             }
         }
 
-        super.F_();
+        super.h_();
     }
 
-    protected String j() {
+    protected String aR() {
         return "mob.creeper";
     }
 
-    protected String k() {
+    protected String aS() {
         return "mob.creeperdeath";
     }
 
@@ -91,7 +91,7 @@ public class EntityCreeper extends EntityMonster {
         }
     }
 
-    public boolean a(Entity entity) {
+    public boolean k(Entity entity) {
         return true;
     }
 
@@ -103,16 +103,16 @@ public class EntityCreeper extends EntityMonster {
         return Item.SULPHUR.id;
     }
 
-    public int A() {
+    public int p() {
         return this.datawatcher.getByte(16);
     }
 
-    public void c(int i) {
+    public void a(int i) {
         this.datawatcher.watch(16, Byte.valueOf((byte) i));
     }
 
-    public void a(EntityWeatherLighting entityweatherlighting) {
-        super.a(entityweatherlighting);
+    public void a(EntityLightning entitylightning) {
+        super.a(entitylightning);
         this.datawatcher.watch(17, Byte.valueOf((byte) 1));
     }
 }
