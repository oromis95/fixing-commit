@@ -32,7 +32,7 @@ public class FoodMetaData {
             }
         }
 
-        if (this.foodLevel >= 18 && entityhuman.ag()) {
+        if (this.foodLevel >= 18 && entityhuman.bM()) {
             ++this.foodTickTimer;
             if (this.foodTickTimer >= 80) {
                 entityhuman.heal(1);
@@ -72,7 +72,7 @@ public class FoodMetaData {
         return this.foodLevel;
     }
 
-    public boolean b() {
+    public boolean c() {
         return this.foodLevel < 20;
     }
 
@@ -80,7 +80,7 @@ public class FoodMetaData {
         this.exhaustionLevel = Math.min(this.exhaustionLevel + f, 40.0F);
     }
 
-    public float c() {
+    public float e() {
         return this.saturationLevel;
     }
 }
