@@ -51,7 +51,7 @@ public class TileEntityDispenser extends TileEntity implements IInventory {
         }
     }
 
-    public ItemStack p_() {
+    public int i() {
         int i = -1;
         int j = 1;
 
@@ -61,11 +61,7 @@ public class TileEntityDispenser extends TileEntity implements IInventory {
             }
         }
 
-        if (i >= 0) {
-            return this.splitStack(i, 1);
-        } else {
-            return null;
-        }
+        return i;
     }
 
     public void setItem(int i, ItemStack itemstack) {
@@ -77,6 +73,17 @@ public class TileEntityDispenser extends TileEntity implements IInventory {
         this.update();
     }
 
+    public int a(ItemStack itemstack) {
+        for (int i = 0; i < this.items.length; ++i) {
+            if (this.items[i] == null || this.items[i].id == 0) {
+                this.items[i] = itemstack;
+                return i;
+            }
+        }
+
+        return -1;
+    }
+
     public String getName() {
         return "container.dispenser";
     }
@@ -122,7 +129,7 @@ public class TileEntityDispenser extends TileEntity implements IInventory {
         return this.world.getTileEntity(this.x, this.y, this.z) != this ? false : entityhuman.e((double) this.x + 0.5D, (double) this.y + 0.5D, (double) this.z + 0.5D) <= 64.0D;
     }
 
-    public void f() {}
+    public void startOpen() {}
 
-    public void g() {}
+    public void f() {}
 }
