@@ -13,23 +13,23 @@ public class PathfinderGoalSit extends PathfinderGoal {
     public boolean a() {
         if (!this.a.isTamed()) {
             return false;
-        } else if (this.a.aU()) {
+        } else if (this.a.H()) {
             return false;
         } else if (!this.a.onGround) {
             return false;
         } else {
             EntityLiving entityliving = this.a.getOwner();
 
-            return entityliving == null ? true : (this.a.j(entityliving) < 144.0D && entityliving.ao() != null ? false : this.b);
+            return entityliving == null ? true : (this.a.e(entityliving) < 144.0D && entityliving.av() != null ? false : this.b);
         }
     }
 
-    public void c() {
-        this.a.al().f();
+    public void e() {
+        this.a.getNavigation().g();
         this.a.setSitting(true);
     }
 
-    public void d() {
+    public void c() {
         this.a.setSitting(false);
     }
 
