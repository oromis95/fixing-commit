@@ -16,7 +16,7 @@ public class PathfinderGoalFollowOwner extends PathfinderGoal {
         this.d = entitytameableanimal;
         this.a = entitytameableanimal.world;
         this.f = f;
-        this.g = entitytameableanimal.al();
+        this.g = entitytameableanimal.getNavigation();
         this.c = f1;
         this.b = f2;
         this.a(3);
@@ -29,7 +29,7 @@ public class PathfinderGoalFollowOwner extends PathfinderGoal {
             return false;
         } else if (this.d.isSitting()) {
             return false;
-        } else if (this.d.j(entityliving) < (double) (this.c * this.c)) {
+        } else if (this.d.e(entityliving) < (double) (this.c * this.c)) {
             return false;
         } else {
             this.e = entityliving;
@@ -38,37 +38,37 @@ public class PathfinderGoalFollowOwner extends PathfinderGoal {
     }
 
     public boolean b() {
-        return !this.g.e() && this.d.j(this.e) > (double) (this.b * this.b) && !this.d.isSitting();
+        return !this.g.f() && this.d.e(this.e) > (double) (this.b * this.b) && !this.d.isSitting();
     }
 
-    public void c() {
+    public void e() {
         this.h = 0;
-        this.i = this.d.al().a();
-        this.d.al().a(false);
+        this.i = this.d.getNavigation().a();
+        this.d.getNavigation().a(false);
     }
 
-    public void d() {
+    public void c() {
         this.e = null;
-        this.g.f();
-        this.d.al().a(this.i);
+        this.g.g();
+        this.d.getNavigation().a(this.i);
     }
 
-    public void e() {
-        this.d.getControllerLook().a(this.e, 10.0F, (float) this.d.D());
+    public void d() {
+        this.d.getControllerLook().a(this.e, 10.0F, (float) this.d.bf());
         if (!this.d.isSitting()) {
             if (--this.h <= 0) {
                 this.h = 10;
                 if (!this.g.a(this.e, this.f)) {
-                    if (this.d.j(this.e) >= 144.0D) {
+                    if (this.d.e(this.e) >= 144.0D) {
                         int i = MathHelper.floor(this.e.locX) - 2;
                         int j = MathHelper.floor(this.e.locZ) - 2;
                         int k = MathHelper.floor(this.e.boundingBox.b);
 
                         for (int l = 0; l <= 4; ++l) {
                             for (int i1 = 0; i1 <= 4; ++i1) {
-                                if ((l < 1 || i1 < 1 || l > 3 || i1 > 3) && this.a.e(i + l, k - 1, j + i1) && !this.a.e(i + l, k, j + i1) && !this.a.e(i + l, k + 1, j + i1)) {
+                                if ((l < 1 || i1 < 1 || l > 3 || i1 > 3) && this.a.t(i + l, k - 1, j + i1) && !this.a.s(i + l, k, j + i1) && !this.a.s(i + l, k + 1, j + i1)) {
                                     this.d.setPositionRotation((double) ((float) (i + l) + 0.5F), (double) k, (double) ((float) (j + i1) + 0.5F), this.d.yaw, this.d.pitch);
-                                    this.g.f();
+                                    this.g.g();
                                     return;
                                 }
                             }
