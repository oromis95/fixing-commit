@@ -0,0 +1,8 @@
+package net.minecraft.server;
+
+public class ExceptionWorldConflict extends Exception {
+
+    public ExceptionWorldConflict(String s) {
+        super(s);
+    }
+}
