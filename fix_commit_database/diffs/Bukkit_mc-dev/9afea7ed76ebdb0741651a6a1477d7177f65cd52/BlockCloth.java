@@ -4,6 +4,7 @@ public class BlockCloth extends Block {
 
     public BlockCloth() {
         super(35, 64, Material.CLOTH);
+        this.a(CreativeModeTab.b);
     }
 
     public int a(int i, int j) {
@@ -19,11 +20,11 @@ public class BlockCloth extends Block {
         return i;
     }
 
-    public static int d(int i) {
+    public static int e_(int i) {
         return ~i & 15;
     }
 
-    public static int e(int i) {
+    public static int d(int i) {
         return ~i & 15;
     }
 }
