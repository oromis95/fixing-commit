@@ -1,42 +1,47 @@
 package net.minecraft.server;
 
+import java.util.Iterator;
 import java.util.List;
 
 public class EntityBoat extends Entity {
 
-    private int a;
+    private boolean a;
     private double b;
-    private double c;
+    private int c;
     private double d;
     private double e;
     private double f;
+    private double g;
+    private double h;
 
     public EntityBoat(World world) {
         super(world);
-        this.bf = true;
-        this.b(1.5F, 0.6F);
+        this.a = true;
+        this.b = 0.07D;
+        this.m = true;
+        this.a(1.5F, 0.6F);
         this.height = this.length / 2.0F;
     }
 
-    protected boolean g_() {
+    protected boolean e_() {
         return false;
     }
 
-    protected void b() {
+    protected void a() {
         this.datawatcher.a(17, new Integer(0));
         this.datawatcher.a(18, new Integer(1));
         this.datawatcher.a(19, new Integer(0));
     }
 
-    public AxisAlignedBB b_(Entity entity) {
+    public AxisAlignedBB g(Entity entity) {
         return entity.boundingBox;
     }
 
-    public AxisAlignedBB h() {
+    public AxisAlignedBB E() {
         return this.boundingBox;
     }
 
-    public boolean e_() {
+    public boolean M() {
         return true;
     }
 
@@ -51,31 +56,26 @@ public class EntityBoat extends Entity {
         this.lastZ = d2;
     }
 
-    public double x_() {
+    public double X() {
         return (double) this.length * 0.0D - 0.30000001192092896D;
     }
 
     public boolean damageEntity(DamageSource damagesource, int i) {
         if (!this.world.isStatic && !this.dead) {
-            this.d(-this.m());
-            this.c(10);
+            this.c(-this.i());
+            this.b(10);
             this.setDamage(this.getDamage() + i * 10);
-            this.aW();
+            this.K();
+            if (damagesource.getEntity() instanceof EntityHuman && ((EntityHuman) damagesource.getEntity()).abilities.canInstantlyBuild) {
+                this.setDamage(100);
+            }
+
             if (this.getDamage() > 40) {
                 if (this.passenger != null) {
                     this.passenger.mount(this);
                 }
 
-                int j;
-
-                for (j = 0; j < 3; ++j) {
-                    this.a(Block.WOOD.id, 1, 0.0F);
-                }
-
-                for (j = 0; j < 2; ++j) {
-                    this.a(Item.STICK.id, 1, 0.0F);
-                }
-
+                this.a(Item.BOAT.id, 1, 0.0F);
                 this.die();
             }
 
@@ -85,14 +85,14 @@ public class EntityBoat extends Entity {
         }
     }
 
-    public boolean o_() {
+    public boolean L() {
         return !this.dead;
     }
 
-    public void F_() {
-        super.F_();
-        if (this.l() > 0) {
-            this.c(this.l() - 1);
+    public void h_() {
+        super.h_();
+        if (this.h() > 0) {
+            this.b(this.h() - 1);
         }
 
         if (this.getDamage() > 0) {
@@ -108,7 +108,7 @@ public class EntityBoat extends Entity {
         for (int i = 0; i < b0; ++i) {
             double d1 = this.boundingBox.b + (this.boundingBox.e - this.boundingBox.b) * (double) (i + 0) / (double) b0 - 0.125D;
             double d2 = this.boundingBox.b + (this.boundingBox.e - this.boundingBox.b) * (double) (i + 1) / (double) b0 - 0.125D;
-            AxisAlignedBB axisalignedbb = AxisAlignedBB.b(this.boundingBox.a, d1, this.boundingBox.c, this.boundingBox.d, d2, this.boundingBox.f);
+            AxisAlignedBB axisalignedbb = AxisAlignedBB.a().a(this.boundingBox.a, d1, this.boundingBox.c, this.boundingBox.d, d2, this.boundingBox.f);
 
             if (this.world.b(axisalignedbb, Material.WATER)) {
                 d0 += 1.0D / (double) b0;
@@ -119,7 +119,7 @@ public class EntityBoat extends Entity {
         double d4;
         double d5;
 
-        if (d3 > 0.15D) {
+        if (d3 > 0.26249999999999996D) {
             d4 = Math.cos((double) this.yaw * 3.141592653589793D / 180.0D);
             d5 = Math.sin((double) this.yaw * 3.141592653589793D / 180.0D);
 
@@ -144,25 +144,17 @@ public class EntityBoat extends Entity {
         double d10;
         double d11;
 
-        if (this.world.isStatic) {
-            if (this.a > 0) {
-                d4 = this.locX + (this.b - this.locX) / (double) this.a;
-                d5 = this.locY + (this.c - this.locY) / (double) this.a;
-                d10 = this.locZ + (this.d - this.locZ) / (double) this.a;
-
-                for (d11 = this.e - (double) this.yaw; d11 < -180.0D; d11 += 360.0D) {
-                    ;
-                }
-
-                while (d11 >= 180.0D) {
-                    d11 -= 360.0D;
-                }
-
-                this.yaw = (float) ((double) this.yaw + d11 / (double) this.a);
-                this.pitch = (float) ((double) this.pitch + (this.f - (double) this.pitch) / (double) this.a);
-                --this.a;
+        if (this.world.isStatic && this.a) {
+            if (this.c > 0) {
+                d4 = this.locX + (this.d - this.locX) / (double) this.c;
+                d5 = this.locY + (this.e - this.locY) / (double) this.c;
+                d10 = this.locZ + (this.f - this.locZ) / (double) this.c;
+                d11 = MathHelper.g(this.g - (double) this.yaw);
+                this.yaw = (float) ((double) this.yaw + d11 / (double) this.c);
+                this.pitch = (float) ((double) this.pitch + (this.h - (double) this.pitch) / (double) this.c);
+                --this.c;
                 this.setPosition(d4, d5, d10);
-                this.c(this.yaw, this.pitch);
+                this.b(this.yaw, this.pitch);
             } else {
                 d4 = this.locX + this.motX;
                 d5 = this.locY + this.motY;
@@ -191,25 +183,28 @@ public class EntityBoat extends Entity {
             }
 
             if (this.passenger != null) {
-                this.motX += this.passenger.motX * 0.2D;
-                this.motZ += this.passenger.motZ * 0.2D;
-            }
-
-            d4 = 0.4D;
-            if (this.motX < -d4) {
-                this.motX = -d4;
-            }
-
-            if (this.motX > d4) {
-                this.motX = d4;
+                this.motX += this.passenger.motX * this.b;
+                this.motZ += this.passenger.motZ * this.b;
             }
 
-            if (this.motZ < -d4) {
-                this.motZ = -d4;
+            d4 = Math.sqrt(this.motX * this.motX + this.motZ * this.motZ);
+            if (d4 > 0.35D) {
+                d5 = 0.35D / d4;
+                this.motX *= d5;
+                this.motZ *= d5;
+                d4 = 0.35D;
             }
 
-            if (this.motZ > d4) {
-                this.motZ = d4;
+            if (d4 > d3 && this.b < 0.35D) {
+                this.b += (0.35D - this.b) / 35.0D;
+                if (this.b > 0.35D) {
+                    this.b = 0.35D;
+                }
+            } else {
+                this.b -= (this.b - 0.07D) / 35.0D;
+                if (this.b < 0.07D) {
+                    this.b = 0.07D;
+                }
             }
 
             if (this.onGround) {
@@ -243,19 +238,11 @@ public class EntityBoat extends Entity {
             d5 = (double) this.yaw;
             d10 = this.lastX - this.locX;
             d11 = this.lastZ - this.locZ;
-            if (d10 * d10 + d11 * d11 > 0.0010D) {
+            if (d10 * d10 + d11 * d11 > 0.001D) {
                 d5 = (double) ((float) (Math.atan2(d11, d10) * 180.0D / 3.141592653589793D));
             }
 
-            double d12;
-
-            for (d12 = d5 - (double) this.yaw; d12 >= 180.0D; d12 -= 360.0D) {
-                ;
-            }
-
-            while (d12 < -180.0D) {
-                d12 += 360.0D;
-            }
+            double d12 = MathHelper.g(d5 - (double) this.yaw);
 
             if (d12 > 20.0D) {
                 d12 = 20.0D;
@@ -266,42 +253,53 @@ public class EntityBoat extends Entity {
             }
 
             this.yaw = (float) ((double) this.yaw + d12);
-            this.c(this.yaw, this.pitch);
-            List list = this.world.getEntities(this, this.boundingBox.grow(0.20000000298023224D, 0.0D, 0.20000000298023224D));
-            int l;
+            this.b(this.yaw, this.pitch);
+            if (!this.world.isStatic) {
+                List list = this.world.getEntities(this, this.boundingBox.grow(0.20000000298023224D, 0.0D, 0.20000000298023224D));
 
-            if (list != null && list.size() > 0) {
-                for (l = 0; l < list.size(); ++l) {
-                    Entity entity = (Entity) list.get(l);
+                if (list != null && !list.isEmpty()) {
+                    Iterator iterator = list.iterator();
 
-                    if (entity != this.passenger && entity.e_() && entity instanceof EntityBoat) {
-                        entity.collide(this);
+                    while (iterator.hasNext()) {
+                        Entity entity = (Entity) iterator.next();
+
+                        if (entity != this.passenger && entity.M() && entity instanceof EntityBoat) {
+                            entity.collide(this);
+                        }
                     }
                 }
-            }
 
-            for (l = 0; l < 4; ++l) {
-                int i1 = MathHelper.floor(this.locX + ((double) (l % 2) - 0.5D) * 0.8D);
-                int j1 = MathHelper.floor(this.locY);
-                int k1 = MathHelper.floor(this.locZ + ((double) (l / 2) - 0.5D) * 0.8D);
-
-                if (this.world.getTypeId(i1, j1, k1) == Block.SNOW.id) {
-                    this.world.setTypeId(i1, j1, k1, 0);
+                for (int l = 0; l < 4; ++l) {
+                    int i1 = MathHelper.floor(this.locX + ((double) (l % 2) - 0.5D) * 0.8D);
+                    int j1 = MathHelper.floor(this.locZ + ((double) (l / 2) - 0.5D) * 0.8D);
+
+                    for (int k1 = 0; k1 < 2; ++k1) {
+                        int l1 = MathHelper.floor(this.locY) + k1;
+                        int i2 = this.world.getTypeId(i1, l1, j1);
+                        int j2 = this.world.getData(i1, l1, j1);
+
+                        if (i2 == Block.SNOW.id) {
+                            this.world.setTypeId(i1, l1, j1, 0);
+                        } else if (i2 == Block.WATER_LILY.id) {
+                            Block.WATER_LILY.dropNaturally(this.world, i1, l1, j1, j2, 0.3F, 0);
+                            this.world.setTypeId(i1, l1, j1, 0);
+                        }
+                    }
                 }
-            }
 
-            if (this.passenger != null && this.passenger.dead) {
-                this.passenger = null;
+                if (this.passenger != null && this.passenger.dead) {
+                    this.passenger = null;
+                }
             }
         }
     }
 
-    public void i_() {
+    public void V() {
         if (this.passenger != null) {
             double d0 = Math.cos((double) this.yaw * 3.141592653589793D / 180.0D) * 0.4D;
             double d1 = Math.sin((double) this.yaw * 3.141592653589793D / 180.0D) * 0.4D;
 
-            this.passenger.setPosition(this.locX + d0, this.locY + this.x_() + this.passenger.W(), this.locZ + d1);
+            this.passenger.setPosition(this.locX + d0, this.locY + this.X() + this.passenger.W(), this.locZ + d1);
         }
     }
 
@@ -309,7 +307,7 @@ public class EntityBoat extends Entity {
 
     protected void a(NBTTagCompound nbttagcompound) {}
 
-    public boolean b(EntityHuman entityhuman) {
+    public boolean c(EntityHuman entityhuman) {
         if (this.passenger != null && this.passenger instanceof EntityHuman && this.passenger != entityhuman) {
             return true;
         } else {
@@ -329,19 +327,19 @@ public class EntityBoat extends Entity {
         return this.datawatcher.getInt(19);
     }
 
-    public void c(int i) {
+    public void b(int i) {
         this.datawatcher.watch(17, Integer.valueOf(i));
     }
 
-    public int l() {
+    public int h() {
         return this.datawatcher.getInt(17);
     }
 
-    public void d(int i) {
+    public void c(int i) {
         this.datawatcher.watch(18, Integer.valueOf(i));
     }
 
-    public int m() {
+    public int i() {
         return this.datawatcher.getInt(18);
     }
 }
