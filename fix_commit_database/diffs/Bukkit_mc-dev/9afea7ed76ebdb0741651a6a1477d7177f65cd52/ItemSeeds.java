@@ -9,12 +9,13 @@ public class ItemSeeds extends Item {
         super(i);
         this.id = j;
         this.b = k;
+        this.a(CreativeModeTab.l);
     }
 
-    public boolean interactWith(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
+    public boolean interactWith(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l, float f, float f1, float f2) {
         if (l != 1) {
             return false;
-        } else if (entityhuman.d(i, j, k) && entityhuman.d(i, j + 1, k)) {
+        } else if (entityhuman.e(i, j, k) && entityhuman.e(i, j + 1, k)) {
             int i1 = world.getTypeId(i, j, k);
 
             if (i1 == this.b && world.isEmpty(i, j + 1, k)) {
