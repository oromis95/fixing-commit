@@ -1,5 +1,7 @@
 package net.minecraft.server;
 
+import java.util.Iterator;
+
 public class WorldManager implements IWorldAccess {
 
     private MinecraftServer server;
@@ -13,30 +15,46 @@ public class WorldManager implements IWorldAccess {
     public void a(String s, double d0, double d1, double d2, double d3, double d4, double d5) {}
 
     public void a(Entity entity) {
-        this.server.getTracker(this.world.worldProvider.dimension).track(entity);
+        this.world.getTracker().track(entity);
     }
 
     public void b(Entity entity) {
-        this.server.getTracker(this.world.worldProvider.dimension).untrackEntity(entity);
+        this.world.getTracker().untrackEntity(entity);
     }
 
-    public void a(String s, double d0, double d1, double d2, float f, float f1) {}
+    public void a(String s, double d0, double d1, double d2, float f, float f1) {
+        this.server.getServerConfigurationManager().sendPacketNearby(d0, d1, d2, f > 1.0F ? (double) (16.0F * f) : 16.0D, this.world.worldProvider.dimension, new Packet62NamedSoundEffect(s, d0, d1, d2, f, f1));
+    }
 
     public void a(int i, int j, int k, int l, int i1, int j1) {}
 
     public void a(int i, int j, int k) {
-        this.server.serverConfigurationManager.flagDirty(i, j, k, this.world.worldProvider.dimension);
+        this.world.getPlayerManager().flagDirty(i, j, k);
     }
 
     public void b(int i, int j, int k) {}
 
     public void a(String s, int i, int j, int k) {}
 
-    public void a(int i, int j, int k, TileEntity tileentity) {
-        this.server.serverConfigurationManager.a(i, j, k, tileentity);
+    public void a(EntityHuman entityhuman, int i, int j, int k, int l, int i1) {
+        this.server.getServerConfigurationManager().sendPacketNearby(entityhuman, (double) j, (double) k, (double) l, 64.0D, this.world.worldProvider.dimension, new Packet61WorldEvent(i, j, k, l, i1));
     }
 
-    public void a(EntityHuman entityhuman, int i, int j, int k, int l, int i1) {
-        this.server.serverConfigurationManager.sendPacketNearby(entityhuman, (double) j, (double) k, (double) l, 64.0D, this.world.worldProvider.dimension, new Packet61WorldEvent(i, j, k, l, i1));
+    public void a(int i, int j, int k, int l, int i1) {
+        Iterator iterator = this.server.getServerConfigurationManager().players.iterator();
+
+        while (iterator.hasNext()) {
+            EntityPlayer entityplayer = (EntityPlayer) iterator.next();
+
+            if (entityplayer != null && entityplayer.world == this.world && entityplayer.id != i) {
+                double d0 = (double) j - entityplayer.locX;
+                double d1 = (double) k - entityplayer.locY;
+                double d2 = (double) l - entityplayer.locZ;
+
+                if (d0 * d0 + d1 * d1 + d2 * d2 < 1024.0D) {
+                    entityplayer.netServerHandler.sendPacket(new Packet55BlockBreakAnimation(i, j, k, l, i1));
+                }
+            }
+        }
     }
 }
