@@ -5,20 +5,25 @@ import java.io.DataOutputStream;
 
 public class Packet2Handshake extends Packet {
 
-    public String a;
+    private int a;
+    private String b;
+    private String c;
+    private int d;
 
     public Packet2Handshake() {}
 
-    public Packet2Handshake(String s) {
-        this.a = s;
-    }
-
     public void a(DataInputStream datainputstream) {
-        this.a = a(datainputstream, 64);
+        this.a = datainputstream.readByte();
+        this.b = a(datainputstream, 16);
+        this.c = a(datainputstream, 255);
+        this.d = datainputstream.readInt();
     }
 
     public void a(DataOutputStream dataoutputstream) {
-        a(this.a, dataoutputstream);
+        dataoutputstream.writeByte(this.a);
+        a(this.b, dataoutputstream);
+        a(this.c, dataoutputstream);
+        dataoutputstream.writeInt(this.d);
     }
 
     public void handle(NetHandler nethandler) {
@@ -26,6 +31,14 @@ public class Packet2Handshake extends Packet {
     }
 
     public int a() {
-        return 4 + this.a.length() + 4;
+        return 3 + 2 * this.b.length();
+    }
+
+    public int d() {
+        return this.a;
+    }
+
+    public String f() {
+        return this.b;
     }
 }
