@@ -1,13 +1,13 @@
 package net.minecraft.server;
 
-class VillageAgressor {
+class VillageAggressor {
 
     public EntityLiving a;
     public int b;
 
     final Village c;
 
-    VillageAgressor(Village village, EntityLiving entityliving, int i) {
+    VillageAggressor(Village village, EntityLiving entityliving, int i) {
         this.c = village;
         this.a = entityliving;
         this.b = i;
