@@ -7,15 +7,13 @@ public class WorldGenVillageLight extends WorldGenVillagePiece {
 
     private int a = -1;
 
-    public WorldGenVillageLight(int i, Random random, StructureBoundingBox structureboundingbox, int j) {
-        super(i);
-        this.h = j;
-        this.g = structureboundingbox;
+    public WorldGenVillageLight(WorldGenVillageStartPiece worldgenvillagestartpiece, int i, Random random, StructureBoundingBox structureboundingbox, int j) {
+        super(worldgenvillagestartpiece, i);
+        this.f = j;
+        this.e = structureboundingbox;
     }
 
-    public void a(StructurePiece structurepiece, List list, Random random) {}
-
-    public static StructureBoundingBox a(List list, Random random, int i, int j, int k, int l) {
+    public static StructureBoundingBox a(WorldGenVillageStartPiece worldgenvillagestartpiece, List list, Random random, int i, int j, int k, int l) {
         StructureBoundingBox structureboundingbox = StructureBoundingBox.a(i, j, k, 0, 0, 0, 3, 4, 2, l);
 
         return StructurePiece.a(list, structureboundingbox) != null ? null : structureboundingbox;
@@ -28,7 +26,7 @@ public class WorldGenVillageLight extends WorldGenVillagePiece {
                 return true;
             }
 
-            this.g.a(0, this.a - this.g.e + 4 - 1, 0);
+            this.e.a(0, this.a - this.e.e + 4 - 1, 0);
         }
 
         this.a(world, structureboundingbox, 0, 0, 0, 2, 3, 1, 0, 0, false);
