@@ -5,43 +5,43 @@ import java.util.List;
 
 public class EntityEnderDragon extends EntityComplex {
 
-    public double a;
     public double b;
     public double c;
-    public double[][] d = new double[64][3];
-    public int e = -1;
+    public double d;
+    public double[][] e = new double[64][3];
+    public int f = -1;
     public EntityComplexPart[] children;
-    public EntityComplexPart g;
     public EntityComplexPart h;
     public EntityComplexPart i;
     public EntityComplexPart j;
-    public EntityComplexPart k;
-    public EntityComplexPart l;
-    public EntityComplexPart m;
-    public float n = 0.0F;
-    public float o = 0.0F;
-    public boolean p = false;
-    public boolean q = false;
-    private Entity u;
-    public int r = 0;
-    public EntityEnderCrystal s = null;
+    public EntityComplexPart by;
+    public EntityComplexPart bz;
+    public EntityComplexPart bA;
+    public EntityComplexPart bB;
+    public float bC = 0.0F;
+    public float bD = 0.0F;
+    public boolean bE = false;
+    public boolean bF = false;
+    private Entity bI;
+    public int bG = 0;
+    public EntityEnderCrystal bH = null;
 
     public EntityEnderDragon(World world) {
         super(world);
-        this.children = new EntityComplexPart[] { this.g = new EntityComplexPart(this, "head", 6.0F, 6.0F), this.h = new EntityComplexPart(this, "body", 8.0F, 8.0F), this.i = new EntityComplexPart(this, "tail", 4.0F, 4.0F), this.j = new EntityComplexPart(this, "tail", 4.0F, 4.0F), this.k = new EntityComplexPart(this, "tail", 4.0F, 4.0F), this.l = new EntityComplexPart(this, "wing", 4.0F, 4.0F), this.m = new EntityComplexPart(this, "wing", 4.0F, 4.0F)};
-        this.t = 200;
-        this.setHealth(this.t);
+        this.children = new EntityComplexPart[] { this.h = new EntityComplexPart(this, "head", 6.0F, 6.0F), this.i = new EntityComplexPart(this, "body", 8.0F, 8.0F), this.j = new EntityComplexPart(this, "tail", 4.0F, 4.0F), this.by = new EntityComplexPart(this, "tail", 4.0F, 4.0F), this.bz = new EntityComplexPart(this, "tail", 4.0F, 4.0F), this.bA = new EntityComplexPart(this, "wing", 4.0F, 4.0F), this.bB = new EntityComplexPart(this, "wing", 4.0F, 4.0F)};
+        this.a = 200;
+        this.setHealth(this.a);
         this.texture = "/mob/enderdragon/ender.png";
-        this.b(16.0F, 8.0F);
-        this.bQ = true;
+        this.a(16.0F, 8.0F);
+        this.X = true;
         this.fireProof = true;
-        this.b = 100.0D;
-        this.cd = true;
+        this.c = 100.0D;
+        this.ak = true;
     }
 
-    protected void b() {
-        super.b();
-        this.datawatcher.a(16, new Integer(this.t));
+    protected void a() {
+        super.a();
+        this.datawatcher.a(16, new Integer(this.a));
     }
 
     public double[] a(int i, float f) {
@@ -50,31 +50,22 @@ public class EntityEnderDragon extends EntityComplex {
         }
 
         f = 1.0F - f;
-        int j = this.e - i * 1 & 63;
-        int k = this.e - i * 1 - 1 & 63;
+        int j = this.f - i * 1 & 63;
+        int k = this.f - i * 1 - 1 & 63;
         double[] adouble = new double[3];
-        double d0 = this.d[j][0];
-
-        double d1;
-
-        for (d1 = this.d[k][0] - d0; d1 < -180.0D; d1 += 360.0D) {
-            ;
-        }
-
-        while (d1 >= 180.0D) {
-            d1 -= 360.0D;
-        }
+        double d0 = this.e[j][0];
+        double d1 = MathHelper.g(this.e[k][0] - d0);
 
         adouble[0] = d0 + d1 * (double) f;
-        d0 = this.d[j][1];
-        d1 = this.d[k][1] - d0;
+        d0 = this.e[j][1];
+        d1 = this.e[k][1] - d0;
         adouble[1] = d0 + d1 * (double) f;
-        adouble[2] = this.d[j][2] + (this.d[k][2] - this.d[j][2]) * (double) f;
+        adouble[2] = this.e[j][2] + (this.e[k][2] - this.e[j][2]) * (double) f;
         return adouble;
     }
 
-    public void e() {
-        this.n = this.o;
+    public void d() {
+        this.bC = this.bD;
         if (!this.world.isStatic) {
             this.datawatcher.watch(16, Integer.valueOf(this.health));
         }
@@ -89,36 +80,29 @@ public class EntityEnderDragon extends EntityComplex {
             f1 = (this.random.nextFloat() - 0.5F) * 8.0F;
             this.world.a("largeexplode", this.locX + (double) f, this.locY + 2.0D + (double) d05, this.locZ + (double) f1, 0.0D, 0.0D, 0.0D);
         } else {
-            this.A();
+            this.j();
             f = 0.2F / (MathHelper.sqrt(this.motX * this.motX + this.motZ * this.motZ) * 10.0F + 1.0F);
             f *= (float) Math.pow(2.0D, this.motY);
-            if (this.q) {
-                this.o += f * 0.5F;
+            if (this.bF) {
+                this.bD += f * 0.5F;
             } else {
-                this.o += f;
+                this.bD += f;
             }
 
-            while (this.yaw >= 180.0F) {
-                this.yaw -= 360.0F;
-            }
-
-            while (this.yaw < -180.0F) {
-                this.yaw += 360.0F;
-            }
-
-            if (this.e < 0) {
-                for (int i = 0; i < this.d.length; ++i) {
-                    this.d[i][0] = (double) this.yaw;
-                    this.d[i][1] = this.locY;
+            this.yaw = MathHelper.g(this.yaw);
+            if (this.f < 0) {
+                for (int i = 0; i < this.e.length; ++i) {
+                    this.e[i][0] = (double) this.yaw;
+                    this.e[i][1] = this.locY;
                 }
             }
 
-            if (++this.e == this.d.length) {
-                this.e = 0;
+            if (++this.f == this.e.length) {
+                this.f = 0;
             }
 
-            this.d[this.e][0] = (double) this.yaw;
-            this.d[this.e][1] = this.locY;
+            this.e[this.f][0] = (double) this.yaw;
+            this.e[this.f][1] = this.locY;
             double d0;
             double d1;
             double d2;
@@ -126,35 +110,27 @@ public class EntityEnderDragon extends EntityComplex {
             float f3;
 
             if (this.world.isStatic) {
-                if (this.aN > 0) {
-                    d0 = this.locX + (this.aO - this.locX) / (double) this.aN;
-                    d1 = this.locY + (this.aP - this.locY) / (double) this.aN;
-                    d2 = this.locZ + (this.aQ - this.locZ) / (double) this.aN;
-
-                    for (d3 = this.aR - (double) this.yaw; d3 < -180.0D; d3 += 360.0D) {
-                        ;
-                    }
-
-                    while (d3 >= 180.0D) {
-                        d3 -= 360.0D;
-                    }
-
-                    this.yaw = (float) ((double) this.yaw + d3 / (double) this.aN);
-                    this.pitch = (float) ((double) this.pitch + (this.aS - (double) this.pitch) / (double) this.aN);
-                    --this.aN;
+                if (this.bi > 0) {
+                    d0 = this.locX + (this.bj - this.locX) / (double) this.bi;
+                    d1 = this.locY + (this.bk - this.locY) / (double) this.bi;
+                    d2 = this.locZ + (this.bl - this.locZ) / (double) this.bi;
+                    d3 = MathHelper.g(this.bm - (double) this.yaw);
+                    this.yaw = (float) ((double) this.yaw + d3 / (double) this.bi);
+                    this.pitch = (float) ((double) this.pitch + (this.bn - (double) this.pitch) / (double) this.bi);
+                    --this.bi;
                     this.setPosition(d0, d1, d2);
-                    this.c(this.yaw, this.pitch);
+                    this.b(this.yaw, this.pitch);
                 }
             } else {
-                d0 = this.a - this.locX;
-                d1 = this.b - this.locY;
-                d2 = this.c - this.locZ;
+                d0 = this.b - this.locX;
+                d1 = this.c - this.locY;
+                d2 = this.d - this.locZ;
                 d3 = d0 * d0 + d1 * d1 + d2 * d2;
-                if (this.u != null) {
-                    this.a = this.u.locX;
-                    this.c = this.u.locZ;
-                    double d4 = this.a - this.locX;
-                    double d5 = this.c - this.locZ;
+                if (this.bI != null) {
+                    this.b = this.bI.locX;
+                    this.d = this.bI.locZ;
+                    double d4 = this.b - this.locX;
+                    double d5 = this.d - this.locZ;
                     double d6 = Math.sqrt(d4 * d4 + d5 * d5);
                     double d7 = 0.4000000059604645D + d6 / 80.0D - 1.0D;
 
@@ -162,14 +138,14 @@ public class EntityEnderDragon extends EntityComplex {
                         d7 = 10.0D;
                     }
 
-                    this.b = this.u.boundingBox.b + d7;
+                    this.c = this.bI.boundingBox.b + d7;
                 } else {
-                    this.a += this.random.nextGaussian() * 2.0D;
-                    this.c += this.random.nextGaussian() * 2.0D;
+                    this.b += this.random.nextGaussian() * 2.0D;
+                    this.d += this.random.nextGaussian() * 2.0D;
                 }
 
-                if (this.p || d3 < 100.0D || d3 > 22500.0D || this.positionChanged || this.bz) {
-                    this.E();
+                if (this.bE || d3 < 100.0D || d3 > 22500.0D || this.positionChanged || this.G) {
+                    this.k();
                 }
 
                 d1 /= (double) MathHelper.sqrt(d0 * d0 + d2 * d2);
@@ -182,25 +158,10 @@ public class EntityEnderDragon extends EntityComplex {
                     d1 = (double) f3;
                 }
 
-                for (this.motY += d1 * 0.10000000149011612D; this.yaw < -180.0F; this.yaw += 360.0F) {
-                    ;
-                }
-
-                while (this.yaw >= 180.0F) {
-                    this.yaw -= 360.0F;
-                }
-
+                this.motY += d1 * 0.10000000149011612D;
+                this.yaw = MathHelper.g(this.yaw);
                 double d8 = 180.0D - Math.atan2(d0, d2) * 180.0D / 3.1415927410125732D;
-
-                double d9;
-
-                for (d9 = d8 - (double) this.yaw; d9 < -180.0D; d9 += 360.0D) {
-                    ;
-                }
-
-                while (d9 >= 180.0D) {
-                    d9 -= 360.0D;
-                }
+                double d9 = MathHelper.g(d8 - (double) this.yaw);
 
                 if (d9 > 50.0D) {
                     d9 = 50.0D;
@@ -210,15 +171,15 @@ public class EntityEnderDragon extends EntityComplex {
                     d9 = -50.0D;
                 }
 
-                Vec3D vec3d = Vec3D.create(this.a - this.locX, this.b - this.locY, this.c - this.locZ).b();
-                Vec3D vec3d1 = Vec3D.create((double) MathHelper.sin(this.yaw * 3.1415927F / 180.0F), this.motY, (double) (-MathHelper.cos(this.yaw * 3.1415927F / 180.0F))).b();
-                float f4 = (float) (vec3d1.a(vec3d) + 0.5D) / 1.5F;
+                Vec3D vec3d = Vec3D.a().create(this.b - this.locX, this.c - this.locY, this.d - this.locZ).b();
+                Vec3D vec3d1 = Vec3D.a().create((double) MathHelper.sin(this.yaw * 3.1415927F / 180.0F), this.motY, (double) (-MathHelper.cos(this.yaw * 3.1415927F / 180.0F))).b();
+                float f4 = (float) (vec3d1.b(vec3d) + 0.5D) / 1.5F;
 
                 if (f4 < 0.0F) {
                     f4 = 0.0F;
                 }
 
-                this.aY *= 0.8F;
+                this.bt *= 0.8F;
                 float f5 = MathHelper.sqrt(this.motX * this.motX + this.motZ * this.motZ) * 1.0F + 1.0F;
                 double d10 = Math.sqrt(this.motX * this.motX + this.motZ * this.motZ) * 1.0D + 1.0D;
 
@@ -226,20 +187,20 @@ public class EntityEnderDragon extends EntityComplex {
                     d10 = 40.0D;
                 }
 
-                this.aY = (float) ((double) this.aY + d9 * (0.699999988079071D / d10 / (double) f5));
-                this.yaw += this.aY * 0.1F;
+                this.bt = (float) ((double) this.bt + d9 * (0.699999988079071D / d10 / (double) f5));
+                this.yaw += this.bt * 0.1F;
                 float f6 = (float) (2.0D / (d10 + 1.0D));
                 float f7 = 0.06F;
 
                 this.a(0.0F, -1.0F, f7 * (f4 * f6 + (1.0F - f6)));
-                if (this.q) {
+                if (this.bF) {
                     this.move(this.motX * 0.800000011920929D, this.motY * 0.800000011920929D, this.motZ * 0.800000011920929D);
                 } else {
                     this.move(this.motX, this.motY, this.motZ);
                 }
 
-                Vec3D vec3d2 = Vec3D.create(this.motX, this.motY, this.motZ).b();
-                float f8 = (float) (vec3d2.a(vec3d1) + 1.0D) / 2.0F;
+                Vec3D vec3d2 = Vec3D.a().create(this.motX, this.motY, this.motZ).b();
+                float f8 = (float) (vec3d2.b(vec3d1) + 1.0D) / 2.0F;
 
                 f8 = 0.8F + 0.15F * f8;
                 this.motX *= (double) f8;
@@ -247,17 +208,17 @@ public class EntityEnderDragon extends EntityComplex {
                 this.motY *= 0.9100000262260437D;
             }
 
-            this.V = this.yaw;
-            this.g.width = this.g.length = 3.0F;
-            this.i.width = this.i.length = 2.0F;
+            this.aq = this.yaw;
+            this.h.width = this.h.length = 3.0F;
             this.j.width = this.j.length = 2.0F;
-            this.k.width = this.k.length = 2.0F;
-            this.h.length = 3.0F;
-            this.h.width = 5.0F;
-            this.l.length = 2.0F;
-            this.l.width = 4.0F;
-            this.m.length = 3.0F;
-            this.m.width = 4.0F;
+            this.by.width = this.by.length = 2.0F;
+            this.bz.width = this.bz.length = 2.0F;
+            this.i.length = 3.0F;
+            this.i.width = 5.0F;
+            this.bA.length = 2.0F;
+            this.bA.width = 4.0F;
+            this.bB.length = 3.0F;
+            this.bB.width = 4.0F;
             d05 = (float) (this.a(5, 1.0F)[1] - this.a(10, 1.0F)[1]) * 10.0F / 180.0F * 3.1415927F;
             f1 = MathHelper.cos(d05);
             float f9 = -MathHelper.sin(d05);
@@ -265,72 +226,68 @@ public class EntityEnderDragon extends EntityComplex {
             float f11 = MathHelper.sin(f10);
             float f12 = MathHelper.cos(f10);
 
-            this.h.F_();
-            this.h.setPositionRotation(this.locX + (double) (f11 * 0.5F), this.locY, this.locZ - (double) (f12 * 0.5F), 0.0F, 0.0F);
-            this.l.F_();
-            this.l.setPositionRotation(this.locX + (double) (f12 * 4.5F), this.locY + 2.0D, this.locZ + (double) (f11 * 4.5F), 0.0F, 0.0F);
-            this.m.F_();
-            this.m.setPositionRotation(this.locX - (double) (f12 * 4.5F), this.locY + 2.0D, this.locZ - (double) (f11 * 4.5F), 0.0F, 0.0F);
-            if (!this.world.isStatic) {
-                this.C();
-            }
-
-            if (!this.world.isStatic && this.at == 0) {
-                this.a(this.world.getEntities(this, this.l.boundingBox.grow(4.0D, 2.0D, 4.0D).d(0.0D, -2.0D, 0.0D)));
-                this.a(this.world.getEntities(this, this.m.boundingBox.grow(4.0D, 2.0D, 4.0D).d(0.0D, -2.0D, 0.0D)));
-                this.b(this.world.getEntities(this, this.g.boundingBox.grow(1.0D, 1.0D, 1.0D)));
+            this.i.h_();
+            this.i.setPositionRotation(this.locX + (double) (f11 * 0.5F), this.locY, this.locZ - (double) (f12 * 0.5F), 0.0F, 0.0F);
+            this.bA.h_();
+            this.bA.setPositionRotation(this.locX + (double) (f12 * 4.5F), this.locY + 2.0D, this.locZ + (double) (f11 * 4.5F), 0.0F, 0.0F);
+            this.bB.h_();
+            this.bB.setPositionRotation(this.locX - (double) (f12 * 4.5F), this.locY + 2.0D, this.locZ - (double) (f11 * 4.5F), 0.0F, 0.0F);
+            if (!this.world.isStatic && this.hurtTicks == 0) {
+                this.a(this.world.getEntities(this, this.bA.boundingBox.grow(4.0D, 2.0D, 4.0D).d(0.0D, -2.0D, 0.0D)));
+                this.a(this.world.getEntities(this, this.bB.boundingBox.grow(4.0D, 2.0D, 4.0D).d(0.0D, -2.0D, 0.0D)));
+                this.b(this.world.getEntities(this, this.h.boundingBox.grow(1.0D, 1.0D, 1.0D)));
             }
 
             double[] adouble = this.a(5, 1.0F);
             double[] adouble1 = this.a(0, 1.0F);
 
-            f3 = MathHelper.sin(this.yaw * 3.1415927F / 180.0F - this.aY * 0.01F);
-            float f13 = MathHelper.cos(this.yaw * 3.1415927F / 180.0F - this.aY * 0.01F);
+            f3 = MathHelper.sin(this.yaw * 3.1415927F / 180.0F - this.bt * 0.01F);
+            float f13 = MathHelper.cos(this.yaw * 3.1415927F / 180.0F - this.bt * 0.01F);
 
-            this.g.F_();
-            this.g.setPositionRotation(this.locX + (double) (f3 * 5.5F * f1), this.locY + (adouble1[1] - adouble[1]) * 1.0D + (double) (f9 * 5.5F), this.locZ - (double) (f13 * 5.5F * f1), 0.0F, 0.0F);
+            this.h.h_();
+            this.h.setPositionRotation(this.locX + (double) (f3 * 5.5F * f1), this.locY + (adouble1[1] - adouble[1]) * 1.0D + (double) (f9 * 5.5F), this.locZ - (double) (f13 * 5.5F * f1), 0.0F, 0.0F);
 
             for (int j = 0; j < 3; ++j) {
                 EntityComplexPart entitycomplexpart = null;
 
                 if (j == 0) {
-                    entitycomplexpart = this.i;
+                    entitycomplexpart = this.j;
                 }
 
                 if (j == 1) {
-                    entitycomplexpart = this.j;
+                    entitycomplexpart = this.by;
                 }
 
                 if (j == 2) {
-                    entitycomplexpart = this.k;
+                    entitycomplexpart = this.bz;
                 }
 
                 double[] adouble2 = this.a(12 + j * 2, 1.0F);
-                float f14 = this.yaw * 3.1415927F / 180.0F + this.a(adouble2[0] - adouble[0]) * 3.1415927F / 180.0F * 1.0F;
+                float f14 = this.yaw * 3.1415927F / 180.0F + this.b(adouble2[0] - adouble[0]) * 3.1415927F / 180.0F * 1.0F;
                 float f15 = MathHelper.sin(f14);
                 float f16 = MathHelper.cos(f14);
                 float f17 = 1.5F;
                 float f18 = (float) (j + 1) * 2.0F;
 
-                entitycomplexpart.F_();
+                entitycomplexpart.h_();
                 entitycomplexpart.setPositionRotation(this.locX - (double) ((f11 * f17 + f15 * f18) * f1), this.locY + (adouble2[1] - adouble[1]) * 1.0D - (double) ((f18 + f17) * f9) + 1.5D, this.locZ + (double) ((f12 * f17 + f16 * f18) * f1), 0.0F, 0.0F);
             }
 
             if (!this.world.isStatic) {
-                this.q = this.a(this.g.boundingBox) | this.a(this.h.boundingBox);
+                this.bF = this.a(this.h.boundingBox) | this.a(this.i.boundingBox);
             }
         }
     }
 
-    private void A() {
-        if (this.s != null) {
-            if (this.s.dead) {
+    private void j() {
+        if (this.bH != null) {
+            if (this.bH.dead) {
                 if (!this.world.isStatic) {
-                    this.a(this.g, DamageSource.EXPLOSION, 10);
+                    this.a(this.h, DamageSource.EXPLOSION, 10);
                 }
 
-                this.s = null;
-            } else if (this.ticksLived % 10 == 0 && this.health < this.t) {
+                this.bH = null;
+            } else if (this.ticksLived % 10 == 0 && this.health < this.a) {
                 ++this.health;
             }
         }
@@ -343,24 +300,22 @@ public class EntityEnderDragon extends EntityComplex {
             Iterator iterator = list.iterator();
 
             while (iterator.hasNext()) {
-                Entity entity = (Entity) iterator.next();
-                double d1 = entity.j(this);
+                EntityEnderCrystal entityendercrystal1 = (EntityEnderCrystal) iterator.next();
+                double d1 = entityendercrystal1.e(this);
 
                 if (d1 < d0) {
                     d0 = d1;
-                    entityendercrystal = (EntityEnderCrystal) entity;
+                    entityendercrystal = entityendercrystal1;
                 }
             }
 
-            this.s = entityendercrystal;
+            this.bH = entityendercrystal;
         }
     }
 
-    private void C() {}
-
     private void a(List list) {
-        double d0 = (this.h.boundingBox.a + this.h.boundingBox.d) / 2.0D;
-        double d1 = (this.h.boundingBox.c + this.h.boundingBox.f) / 2.0D;
+        double d0 = (this.i.boundingBox.a + this.i.boundingBox.d) / 2.0D;
+        double d1 = (this.i.boundingBox.c + this.i.boundingBox.f) / 2.0D;
         Iterator iterator = list.iterator();
 
         while (iterator.hasNext()) {
@@ -371,14 +326,16 @@ public class EntityEnderDragon extends EntityComplex {
                 double d3 = entity.locZ - d1;
                 double d4 = d2 * d2 + d3 * d3;
 
-                entity.b_(d2 / d4 * 4.0D, 0.20000000298023224D, d3 / d4 * 4.0D);
+                entity.g(d2 / d4 * 4.0D, 0.20000000298023224D, d3 / d4 * 4.0D);
             }
         }
     }
 
     private void b(List list) {
-        for (int i = 0; i < list.size(); ++i) {
-            Entity entity = (Entity) list.get(i);
+        Iterator iterator = list.iterator();
+
+        while (iterator.hasNext()) {
+            Entity entity = (Entity) iterator.next();
 
             if (entity instanceof EntityLiving) {
                 entity.damageEntity(DamageSource.mobAttack(this), 10);
@@ -386,40 +343,32 @@ public class EntityEnderDragon extends EntityComplex {
         }
     }
 
-    private void E() {
-        this.p = false;
-        if (this.random.nextInt(2) == 0 && this.world.players.size() > 0) {
-            this.u = (Entity) this.world.players.get(this.random.nextInt(this.world.players.size()));
+    private void k() {
+        this.bE = false;
+        if (this.random.nextInt(2) == 0 && !this.world.players.isEmpty()) {
+            this.bI = (Entity) this.world.players.get(this.random.nextInt(this.world.players.size()));
         } else {
             boolean flag = false;
 
             do {
-                this.a = 0.0D;
-                this.b = (double) (70.0F + this.random.nextFloat() * 50.0F);
-                this.c = 0.0D;
-                this.a += (double) (this.random.nextFloat() * 120.0F - 60.0F);
-                this.c += (double) (this.random.nextFloat() * 120.0F - 60.0F);
-                double d0 = this.locX - this.a;
-                double d1 = this.locY - this.b;
-                double d2 = this.locZ - this.c;
+                this.b = 0.0D;
+                this.c = (double) (70.0F + this.random.nextFloat() * 50.0F);
+                this.d = 0.0D;
+                this.b += (double) (this.random.nextFloat() * 120.0F - 60.0F);
+                this.d += (double) (this.random.nextFloat() * 120.0F - 60.0F);
+                double d0 = this.locX - this.b;
+                double d1 = this.locY - this.c;
+                double d2 = this.locZ - this.d;
 
                 flag = d0 * d0 + d1 * d1 + d2 * d2 > 100.0D;
             } while (!flag);
 
-            this.u = null;
+            this.bI = null;
         }
     }
 
-    private float a(double d0) {
-        while (d0 >= 180.0D) {
-            d0 -= 360.0D;
-        }
-
-        while (d0 < -180.0D) {
-            d0 += 360.0D;
-        }
-
-        return (float) d0;
+    private float b(double d0) {
+        return (float) MathHelper.g(d0);
     }
 
     private boolean a(AxisAlignedBB axisalignedbb) {
@@ -461,7 +410,7 @@ public class EntityEnderDragon extends EntityComplex {
     }
 
     public boolean a(EntityComplexPart entitycomplexpart, DamageSource damagesource, int i) {
-        if (entitycomplexpart != this.g) {
+        if (entitycomplexpart != this.h) {
             i = i / 4 + 1;
         }
 
@@ -469,10 +418,10 @@ public class EntityEnderDragon extends EntityComplex {
         float f1 = MathHelper.sin(f);
         float f2 = MathHelper.cos(f);
 
-        this.a = this.locX + (double) (f1 * 5.0F) + (double) ((this.random.nextFloat() - 0.5F) * 2.0F);
-        this.b = this.locY + (double) (this.random.nextFloat() * 3.0F) + 1.0D;
-        this.c = this.locZ - (double) (f2 * 5.0F) + (double) ((this.random.nextFloat() - 0.5F) * 2.0F);
-        this.u = null;
+        this.b = this.locX + (double) (f1 * 5.0F) + (double) ((this.random.nextFloat() - 0.5F) * 2.0F);
+        this.c = this.locY + (double) (this.random.nextFloat() * 3.0F) + 1.0D;
+        this.d = this.locZ - (double) (f2 * 5.0F) + (double) ((this.random.nextFloat() - 0.5F) * 2.0F);
+        this.bI = null;
         if (damagesource.getEntity() instanceof EntityHuman || damagesource == DamageSource.EXPLOSION) {
             this.dealDamage(damagesource, i);
         }
@@ -480,9 +429,9 @@ public class EntityEnderDragon extends EntityComplex {
         return true;
     }
 
-    protected void aB() {
-        ++this.r;
-        if (this.r >= 180 && this.r <= 200) {
+    protected void aI() {
+        ++this.bG;
+        if (this.bG >= 180 && this.bG <= 200) {
             float f = (this.random.nextFloat() - 0.5F) * 8.0F;
             float f1 = (this.random.nextFloat() - 0.5F) * 4.0F;
             float f2 = (this.random.nextFloat() - 0.5F) * 8.0F;
@@ -493,7 +442,7 @@ public class EntityEnderDragon extends EntityComplex {
         int i;
         int j;
 
-        if (!this.world.isStatic && this.r > 150 && this.r % 5 == 0) {
+        if (!this.world.isStatic && this.bG > 150 && this.bG % 5 == 0) {
             i = 1000;
 
             while (i > 0) {
@@ -504,9 +453,9 @@ public class EntityEnderDragon extends EntityComplex {
         }
 
         this.move(0.0D, 0.10000000149011612D, 0.0D);
-        this.V = this.yaw += 20.0F;
-        if (this.r == 200) {
-            i = 10000;
+        this.aq = this.yaw += 20.0F;
+        if (this.bG == 200 && !this.world.isStatic) {
+            i = 2000;
 
             while (i > 0) {
                 j = EntityExperienceOrb.getOrbValue(i);
@@ -515,7 +464,6 @@ public class EntityEnderDragon extends EntityComplex {
             }
 
             this.a(MathHelper.floor(this.locX), MathHelper.floor(this.locZ));
-            this.aH();
             this.die();
         }
     }
@@ -531,16 +479,16 @@ public class EntityEnderDragon extends EntityComplex {
                 for (int i1 = j - b1; i1 <= j + b1; ++i1) {
                     double d0 = (double) (l - i);
                     double d1 = (double) (i1 - j);
-                    double d2 = (double) MathHelper.sqrt(d0 * d0 + d1 * d1);
+                    double d2 = d0 * d0 + d1 * d1;
 
-                    if (d2 <= (double) b1 - 0.5D) {
+                    if (d2 <= ((double) b1 - 0.5D) * ((double) b1 - 0.5D)) {
                         if (k < b0) {
-                            if (d2 <= (double) (b1 - 1) - 0.5D) {
+                            if (d2 <= ((double) (b1 - 1) - 0.5D) * ((double) (b1 - 1) - 0.5D)) {
                                 this.world.setTypeId(l, k, i1, Block.BEDROCK.id);
                             }
                         } else if (k > b0) {
                             this.world.setTypeId(l, k, i1, 0);
-                        } else if (d2 > (double) (b1 - 1) - 0.5D) {
+                        } else if (d2 > ((double) (b1 - 1) - 0.5D) * ((double) (b1 - 1) - 0.5D)) {
                             this.world.setTypeId(l, k, i1, Block.BEDROCK.id);
                         } else {
                             this.world.setTypeId(l, k, i1, Block.ENDER_PORTAL.id);
@@ -562,13 +510,13 @@ public class EntityEnderDragon extends EntityComplex {
         BlockEnderPortal.a = false;
     }
 
-    protected void aG() {}
+    protected void bb() {}
 
-    public Entity[] bb() {
+    public Entity[] al() {
         return this.children;
     }
 
-    public boolean o_() {
+    public boolean L() {
         return false;
     }
 }
