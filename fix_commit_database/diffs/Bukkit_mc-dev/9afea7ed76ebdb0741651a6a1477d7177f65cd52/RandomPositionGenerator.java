@@ -6,8 +6,6 @@ public class RandomPositionGenerator {
 
     private static Vec3D a = Vec3D.a(0.0D, 0.0D, 0.0D);
 
-    public RandomPositionGenerator() {}
-
     public static Vec3D a(EntityCreature entitycreature, int i, int j) {
         return c(entitycreature, i, j, (Vec3D) null);
     }
@@ -27,7 +25,7 @@ public class RandomPositionGenerator {
     }
 
     private static Vec3D c(EntityCreature entitycreature, int i, int j, Vec3D vec3d) {
-        Random random = entitycreature.an();
+        Random random = entitycreature.au();
         boolean flag = false;
         int k = 0;
         int l = 0;
@@ -35,10 +33,11 @@ public class RandomPositionGenerator {
         float f = -99999.0F;
         boolean flag1;
 
-        if (entitycreature.ay()) {
-            double d0 = entitycreature.av().b(MathHelper.floor(entitycreature.locX), MathHelper.floor(entitycreature.locY), MathHelper.floor(entitycreature.locZ)) + 4.0D;
+        if (entitycreature.aF()) {
+            double d0 = (double) (entitycreature.aC().e(MathHelper.floor(entitycreature.locX), MathHelper.floor(entitycreature.locY), MathHelper.floor(entitycreature.locZ)) + 4.0F);
+            double d1 = (double) (entitycreature.aD() + (float) i);
 
-            flag1 = d0 < (double) (entitycreature.aw() + (float) i);
+            flag1 = d0 < d1 * d1;
         } else {
             flag1 = false;
         }
@@ -52,7 +51,7 @@ public class RandomPositionGenerator {
                 k1 += MathHelper.floor(entitycreature.locX);
                 l1 += MathHelper.floor(entitycreature.locY);
                 i2 += MathHelper.floor(entitycreature.locZ);
-                if (!flag1 || entitycreature.e(k1, l1, i2)) {
+                if (!flag1 || entitycreature.d(k1, l1, i2)) {
                     float f1 = entitycreature.a(k1, l1, i2);
 
                     if (f1 > f) {
@@ -67,7 +66,7 @@ public class RandomPositionGenerator {
         }
 
         if (flag) {
-            return Vec3D.create((double) k, (double) l, (double) i1);
+            return Vec3D.a().create((double) k, (double) l, (double) i1);
         } else {
             return null;
         }
