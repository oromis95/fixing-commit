@@ -1,7 +1,5 @@
 package net.minecraft.server;
 
-import java.util.Random;
-
 public class PathfinderGoalMakeLove extends PathfinderGoal {
 
     private EntityVillager b;
@@ -19,7 +17,7 @@ public class PathfinderGoalMakeLove extends PathfinderGoal {
     public boolean a() {
         if (this.b.getAge() != 0) {
             return false;
-        } else if (this.b.an().nextInt(500) != 0) {
+        } else if (this.b.au().nextInt(500) != 0) {
             return false;
         } else {
             this.a = this.d.villages.getClosestVillage(MathHelper.floor(this.b.locX), MathHelper.floor(this.b.locY), MathHelper.floor(this.b.locZ), 0);
@@ -40,32 +38,32 @@ public class PathfinderGoalMakeLove extends PathfinderGoal {
         }
     }
 
-    public void c() {
+    public void e() {
         this.e = 300;
-        this.b.a(true);
+        this.b.e(true);
     }
 
-    public void d() {
+    public void c() {
         this.a = null;
         this.c = null;
-        this.b.a(false);
+        this.b.e(false);
     }
 
     public boolean b() {
         return this.e >= 0 && this.f() && this.b.getAge() == 0;
     }
 
-    public void e() {
+    public void d() {
         --this.e;
         this.b.getControllerLook().a(this.c, 10.0F, 30.0F);
-        if (this.b.j(this.c) > 2.25D) {
-            this.b.al().a((EntityLiving) this.c, 0.25F);
-        } else if (this.e == 0 && this.c.A()) {
+        if (this.b.e(this.c) > 2.25D) {
+            this.b.getNavigation().a((EntityLiving) this.c, 0.25F);
+        } else if (this.e == 0 && this.c.o()) {
             this.i();
         }
 
-        if (this.b.an().nextInt(35) == 0) {
-            this.a(this.b);
+        if (this.b.au().nextInt(35) == 0) {
+            this.d.broadcastEntityEffect(this.b, (byte) 12);
         }
     }
 
@@ -81,21 +79,9 @@ public class PathfinderGoalMakeLove extends PathfinderGoal {
         this.c.setAge(6000);
         this.b.setAge(6000);
         entityvillager.setAge(-24000);
-        entityvillager.setProfession(this.b.an().nextInt(5));
+        entityvillager.setProfession(this.b.au().nextInt(5));
         entityvillager.setPositionRotation(this.b.locX, this.b.locY, this.b.locZ, 0.0F, 0.0F);
         this.d.addEntity(entityvillager);
-        this.a(entityvillager);
-    }
-
-    private void a(EntityLiving entityliving) {
-        Random random = entityliving.an();
-
-        for (int i = 0; i < 5; ++i) {
-            double d0 = random.nextGaussian() * 0.02D;
-            double d1 = random.nextGaussian() * 0.02D;
-            double d2 = random.nextGaussian() * 0.02D;
-
-            this.d.a("heart", entityliving.locX + (double) (random.nextFloat() * entityliving.width * 2.0F) - (double) entityliving.width, entityliving.locY + 1.0D + (double) (random.nextFloat() * entityliving.length), entityliving.locZ + (double) (random.nextFloat() * entityliving.width * 2.0F) - (double) entityliving.width, d0, d1, d2);
-        }
+        this.d.broadcastEntityEffect(entityvillager, (byte) 12);
     }
 }
