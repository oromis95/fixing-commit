@@ -1,11 +1,17 @@
 package net.minecraft.server;
 
+import java.util.Iterator;
+
 public class TileEntityMobSpawner extends TileEntity {
 
     public int spawnDelay = -1;
     private String mobName = "Pig";
+    private NBTTagCompound spawnData = null;
     public double b;
     public double c = 0.0D;
+    private int minSpawnDelay = 200;
+    private int maxSpawnDelay = 800;
+    private int spawnCount = 4;
 
     public TileEntityMobSpawner() {
         this.spawnDelay = 20;
@@ -15,27 +21,24 @@ public class TileEntityMobSpawner extends TileEntity {
         this.mobName = s;
     }
 
-    public boolean c() {
+    public boolean b() {
         return this.world.findNearbyPlayer((double) this.x + 0.5D, (double) this.y + 0.5D, (double) this.z + 0.5D, 16.0D) != null;
     }
 
-    public void q_() {
-        this.c = this.b;
-        if (this.c()) {
-            double d0 = (double) ((float) this.x + this.world.random.nextFloat());
-            double d1 = (double) ((float) this.y + this.world.random.nextFloat());
-            double d2 = (double) ((float) this.z + this.world.random.nextFloat());
-
-            this.world.a("smoke", d0, d1, d2, 0.0D, 0.0D, 0.0D);
-            this.world.a("flame", d0, d1, d2, 0.0D, 0.0D, 0.0D);
-
-            for (this.b += (double) (1000.0F / ((float) this.spawnDelay + 200.0F)); this.b > 360.0D; this.c -= 360.0D) {
-                this.b -= 360.0D;
-            }
-
-            if (!this.world.isStatic) {
+    public void g() {
+        if (this.b()) {
+            if (this.world.isStatic) {
+                double d0 = (double) ((float) this.x + this.world.random.nextFloat());
+                double d1 = (double) ((float) this.y + this.world.random.nextFloat());
+                double d2 = (double) ((float) this.z + this.world.random.nextFloat());
+
+                this.world.a("smoke", d0, d1, d2, 0.0D, 0.0D, 0.0D);
+                this.world.a("flame", d0, d1, d2, 0.0D, 0.0D, 0.0D);
+                this.c = this.b % 360.0D;
+                this.b += 4.545454502105713D;
+            } else {
                 if (this.spawnDelay == -1) {
-                    this.e();
+                    this.f();
                 }
 
                 if (this.spawnDelay > 0) {
@@ -43,61 +46,99 @@ public class TileEntityMobSpawner extends TileEntity {
                     return;
                 }
 
-                byte b0 = 4;
+                for (int i = 0; i < this.spawnCount; ++i) {
+                    Entity entity = EntityTypes.createEntityByName(this.mobName, this.world);
 
-                for (int i = 0; i < b0; ++i) {
-                    EntityLiving entityliving = (EntityLiving) ((EntityLiving) EntityTypes.createEntityByName(this.mobName, this.world));
-
-                    if (entityliving == null) {
+                    if (entity == null) {
                         return;
                     }
 
-                    int j = this.world.a(entityliving.getClass(), AxisAlignedBB.b((double) this.x, (double) this.y, (double) this.z, (double) (this.x + 1), (double) (this.y + 1), (double) (this.z + 1)).grow(8.0D, 4.0D, 8.0D)).size();
+                    int j = this.world.a(entity.getClass(), AxisAlignedBB.a().a((double) this.x, (double) this.y, (double) this.z, (double) (this.x + 1), (double) (this.y + 1), (double) (this.z + 1)).grow(8.0D, 4.0D, 8.0D)).size();
 
                     if (j >= 6) {
-                        this.e();
+                        this.f();
                         return;
                     }
 
-                    if (entityliving != null) {
+                    if (entity != null) {
                         double d3 = (double) this.x + (this.world.random.nextDouble() - this.world.random.nextDouble()) * 4.0D;
                         double d4 = (double) (this.y + this.world.random.nextInt(3) - 1);
                         double d5 = (double) this.z + (this.world.random.nextDouble() - this.world.random.nextDouble()) * 4.0D;
+                        EntityLiving entityliving = entity instanceof EntityLiving ? (EntityLiving) entity : null;
 
-                        entityliving.setPositionRotation(d3, d4, d5, this.world.random.nextFloat() * 360.0F, 0.0F);
-                        if (entityliving.canSpawn()) {
-                            this.world.addEntity(entityliving);
+                        entity.setPositionRotation(d3, d4, d5, this.world.random.nextFloat() * 360.0F, 0.0F);
+                        if (entityliving == null || entityliving.canSpawn()) {
+                            this.a(entity);
+                            this.world.addEntity(entity);
                             this.world.triggerEffect(2004, this.x, this.y, this.z, 0);
-                            entityliving.aC();
-                            this.e();
+                            if (entityliving != null) {
+                                entityliving.aK();
+                            }
+
+                            this.f();
                         }
                     }
                 }
             }
 
-            super.q_();
+            super.g();
         }
     }
 
-    private void e() {
-        this.spawnDelay = 200 + this.world.random.nextInt(600);
+    public void a(Entity entity) {
+        if (this.spawnData != null) {
+            NBTTagCompound nbttagcompound = new NBTTagCompound();
+
+            entity.c(nbttagcompound);
+            Iterator iterator = this.spawnData.c().iterator();
+
+            while (iterator.hasNext()) {
+                NBTBase nbtbase = (NBTBase) iterator.next();
+
+                nbttagcompound.set(nbtbase.getName(), nbtbase.clone());
+            }
+
+            entity.e(nbttagcompound);
+        }
+    }
+
+    private void f() {
+        this.spawnDelay = this.minSpawnDelay + this.world.random.nextInt(this.maxSpawnDelay - this.minSpawnDelay);
     }
 
     public void a(NBTTagCompound nbttagcompound) {
         super.a(nbttagcompound);
         this.mobName = nbttagcompound.getString("EntityId");
         this.spawnDelay = nbttagcompound.getShort("Delay");
+        if (nbttagcompound.hasKey("SpawnData")) {
+            this.spawnData = nbttagcompound.getCompound("SpawnData");
+        } else {
+            this.spawnData = null;
+        }
+
+        if (nbttagcompound.hasKey("MinSpawnDelay")) {
+            this.minSpawnDelay = nbttagcompound.getShort("MinSpawnDelay");
+            this.maxSpawnDelay = nbttagcompound.getShort("MaxSpawnDelay");
+            this.spawnCount = nbttagcompound.getShort("SpawnCount");
+        }
     }
 
     public void b(NBTTagCompound nbttagcompound) {
         super.b(nbttagcompound);
         nbttagcompound.setString("EntityId", this.mobName);
         nbttagcompound.setShort("Delay", (short) this.spawnDelay);
+        nbttagcompound.setShort("MinSpawnDelay", (short) this.minSpawnDelay);
+        nbttagcompound.setShort("MaxSpawnDelay", (short) this.maxSpawnDelay);
+        nbttagcompound.setShort("SpawnCount", (short) this.spawnCount);
+        if (this.spawnData != null) {
+            nbttagcompound.setCompound("SpawnData", this.spawnData);
+        }
     }
 
-    public Packet d() {
-        int i = EntityTypes.a(this.mobName);
+    public Packet e() {
+        NBTTagCompound nbttagcompound = new NBTTagCompound();
 
-        return new Packet132TileEntityData(this.x, this.y, this.z, 1, i);
+        this.b(nbttagcompound);
+        return new Packet132TileEntityData(this.x, this.y, this.z, 1, nbttagcompound);
     }
 }
