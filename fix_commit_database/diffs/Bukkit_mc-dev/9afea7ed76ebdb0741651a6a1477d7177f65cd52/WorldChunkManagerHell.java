@@ -6,18 +6,18 @@ import java.util.Random;
 
 public class WorldChunkManagerHell extends WorldChunkManager {
 
-    private BiomeBase a;
-    private float b;
-    private float c;
+    private BiomeBase d;
+    private float e;
+    private float f;
 
     public WorldChunkManagerHell(BiomeBase biomebase, float f, float f1) {
-        this.a = biomebase;
-        this.b = f;
-        this.c = f1;
+        this.d = biomebase;
+        this.e = f;
+        this.f = f1;
     }
 
     public BiomeBase getBiome(int i, int j) {
-        return this.a;
+        return this.d;
     }
 
     public BiomeBase[] getBiomes(BiomeBase[] abiomebase, int i, int j, int k, int l) {
@@ -25,25 +25,25 @@ public class WorldChunkManagerHell extends WorldChunkManager {
             abiomebase = new BiomeBase[k * l];
         }
 
-        Arrays.fill(abiomebase, 0, k * l, this.a);
+        Arrays.fill(abiomebase, 0, k * l, this.d);
         return abiomebase;
     }
 
-    public float[] getTemperatures(float[] afloat, int i, int j, int k, int l) {
+    public float[] getWetness(float[] afloat, int i, int j, int k, int l) {
         if (afloat == null || afloat.length < k * l) {
             afloat = new float[k * l];
         }
 
-        Arrays.fill(afloat, 0, k * l, this.b);
+        Arrays.fill(afloat, 0, k * l, this.e);
         return afloat;
     }
 
-    public float[] getWetness(float[] afloat, int i, int j, int k, int l) {
+    public float[] getTemperatures(float[] afloat, int i, int j, int k, int l) {
         if (afloat == null || afloat.length < k * l) {
             afloat = new float[k * l];
         }
 
-        Arrays.fill(afloat, 0, k * l, this.c);
+        Arrays.fill(afloat, 0, k * l, this.f);
         return afloat;
     }
 
@@ -52,7 +52,7 @@ public class WorldChunkManagerHell extends WorldChunkManager {
             abiomebase = new BiomeBase[k * l];
         }
 
-        Arrays.fill(abiomebase, 0, k * l, this.a);
+        Arrays.fill(abiomebase, 0, k * l, this.d);
         return abiomebase;
     }
 
@@ -61,10 +61,10 @@ public class WorldChunkManagerHell extends WorldChunkManager {
     }
 
     public ChunkPosition a(int i, int j, int k, List list, Random random) {
-        return list.contains(this.a) ? new ChunkPosition(i - k + random.nextInt(k * 2 + 1), 0, j - k + random.nextInt(k * 2 + 1)) : null;
+        return list.contains(this.d) ? new ChunkPosition(i - k + random.nextInt(k * 2 + 1), 0, j - k + random.nextInt(k * 2 + 1)) : null;
     }
 
     public boolean a(int i, int j, int k, List list) {
-        return list.contains(this.a);
+        return list.contains(this.d);
     }
 }
