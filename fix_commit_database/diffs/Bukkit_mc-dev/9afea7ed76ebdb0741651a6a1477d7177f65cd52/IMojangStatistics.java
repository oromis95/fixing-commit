@@ -0,0 +1,10 @@
+package net.minecraft.server;
+
+public interface IMojangStatistics {
+
+    void a(MojangStatisticsGenerator mojangstatisticsgenerator);
+
+    void b(MojangStatisticsGenerator mojangstatisticsgenerator);
+
+    boolean getSnooperEnabled();
+}
