@@ -48,7 +48,7 @@ public class PathEntity {
         double d1 = (double) this.a[i].b;
         double d2 = (double) this.a[i].c + (double) ((int) (entity.width + 1.0F)) * 0.5D;
 
-        return Vec3D.create(d0, d1, d2);
+        return Vec3D.a().create(d0, d1, d2);
     }
 
     public Vec3D a(Entity entity) {
@@ -71,7 +71,7 @@ public class PathEntity {
         }
     }
 
-    public boolean a(Vec3D vec3d) {
+    public boolean b(Vec3D vec3d) {
         PathPoint pathpoint = this.c();
 
         return pathpoint == null ? false : pathpoint.a == (int) vec3d.a && pathpoint.c == (int) vec3d.c;
