@@ -2,24 +2,26 @@ package net.minecraft.server;
 
 import java.util.ArrayList;
 import java.util.HashSet;
+import java.util.Iterator;
 import java.util.List;
 import java.util.Set;
 
 public abstract class Container {
 
-    public List d = new ArrayList();
-    public List e = new ArrayList();
+    public List a = new ArrayList();
+    public List b = new ArrayList();
     public int windowId = 0;
-    private short a = 0;
+    private short e = 0;
     protected List listeners = new ArrayList();
-    private Set b = new HashSet();
+    private Set f = new HashSet();
 
     public Container() {}
 
-    protected void a(Slot slot) {
-        slot.c = this.e.size();
-        this.e.add(slot);
-        this.d.add(null);
+    protected Slot a(Slot slot) {
+        slot.d = this.b.size();
+        this.b.add(slot);
+        this.a.add(null);
+        return slot;
     }
 
     public void addSlotListener(ICrafting icrafting) {
@@ -27,32 +29,38 @@ public abstract class Container {
             throw new IllegalArgumentException("Listener already listening");
         } else {
             this.listeners.add(icrafting);
-            icrafting.a(this, this.b());
-            this.a();
+            icrafting.a(this, this.a());
+            this.b();
         }
     }
 
-    public List b() {
+    public List a() {
         ArrayList arraylist = new ArrayList();
+        Iterator iterator = this.b.iterator();
 
-        for (int i = 0; i < this.e.size(); ++i) {
-            arraylist.add(((Slot) this.e.get(i)).getItem());
+        while (iterator.hasNext()) {
+            Slot slot = (Slot) iterator.next();
+
+            arraylist.add(slot.getItem());
         }
 
         return arraylist;
     }
 
-    public void a() {
-        for (int i = 0; i < this.e.size(); ++i) {
-            ItemStack itemstack = ((Slot) this.e.get(i)).getItem();
-            ItemStack itemstack1 = (ItemStack) this.d.get(i);
+    public void b() {
+        for (int i = 0; i < this.b.size(); ++i) {
+            ItemStack itemstack = ((Slot) this.b.get(i)).getItem();
+            ItemStack itemstack1 = (ItemStack) this.a.get(i);
 
             if (!ItemStack.matches(itemstack1, itemstack)) {
                 itemstack1 = itemstack == null ? null : itemstack.cloneItemStack();
-                this.d.set(i, itemstack1);
+                this.a.set(i, itemstack1);
+                Iterator iterator = this.listeners.iterator();
+
+                while (iterator.hasNext()) {
+                    ICrafting icrafting = (ICrafting) iterator.next();
 
-                for (int j = 0; j < this.listeners.size(); ++j) {
-                    ((ICrafting) this.listeners.get(j)).a(this, i, itemstack1);
+                    icrafting.a(this, i, itemstack1);
                 }
             }
         }
@@ -63,23 +71,27 @@ public abstract class Container {
     }
 
     public Slot a(IInventory iinventory, int i) {
-        for (int j = 0; j < this.e.size(); ++j) {
-            Slot slot = (Slot) this.e.get(j);
+        Iterator iterator = this.b.iterator();
+
+        Slot slot;
 
-            if (slot.a(iinventory, i)) {
-                return slot;
+        do {
+            if (!iterator.hasNext()) {
+                return null;
             }
-        }
 
-        return null;
+            slot = (Slot) iterator.next();
+        } while (!slot.a(iinventory, i));
+
+        return slot;
     }
 
     public Slot getSlot(int i) {
-        return (Slot) this.e.get(i);
+        return (Slot) this.b.get(i);
     }
 
-    public ItemStack a(int i) {
-        Slot slot = (Slot) this.e.get(i);
+    public ItemStack b(int i) {
+        Slot slot = (Slot) this.b.get(i);
 
         return slot != null ? slot.getItem() : null;
     }
@@ -108,13 +120,13 @@ public abstract class Container {
                         }
                     }
                 } else if (flag) {
-                    ItemStack itemstack1 = this.a(i);
+                    ItemStack itemstack1 = this.b(i);
 
                     if (itemstack1 != null) {
                         int k = itemstack1.id;
 
                         itemstack = itemstack1.cloneItemStack();
-                        Slot slot = (Slot) this.e.get(i);
+                        Slot slot = (Slot) this.b.get(i);
 
                         if (slot != null && slot.getItem() != null && slot.getItem().id == k) {
                             this.b(i, j, flag, entityhuman);
@@ -125,10 +137,9 @@ public abstract class Container {
                         return null;
                     }
 
-                    Slot slot1 = (Slot) this.e.get(i);
+                    Slot slot1 = (Slot) this.b.get(i);
 
                     if (slot1 != null) {
-                        slot1.d();
                         ItemStack itemstack2 = slot1.getItem();
                         ItemStack itemstack3 = playerinventory.getCarried();
 
@@ -159,7 +170,7 @@ public abstract class Container {
                                 slot1.set((ItemStack) null);
                             }
 
-                            slot1.c(playerinventory.getCarried());
+                            slot1.b(playerinventory.getCarried());
                         } else if (slot1.isAllowed(itemstack3)) {
                             if (itemstack2.id == itemstack3.id && (!itemstack2.usesData() || itemstack2.getData() == itemstack3.getData()) && ItemStack.equals(itemstack2, itemstack3)) {
                                 l = j == 0 ? itemstack3.count : 1;
@@ -190,9 +201,11 @@ public abstract class Container {
                                     slot1.set((ItemStack) null);
                                 }
 
-                                slot1.c(playerinventory.getCarried());
+                                slot1.b(playerinventory.getCarried());
                             }
                         }
+
+                        slot1.e();
                     }
                 }
             }
@@ -215,26 +228,26 @@ public abstract class Container {
     }
 
     public void a(IInventory iinventory) {
-        this.a();
+        this.b();
     }
 
     public void setItem(int i, ItemStack itemstack) {
         this.getSlot(i).set(itemstack);
     }
 
-    public boolean c(EntityHuman entityhuman) {
-        return !this.b.contains(entityhuman);
+    public boolean b(EntityHuman entityhuman) {
+        return !this.f.contains(entityhuman);
     }
 
     public void a(EntityHuman entityhuman, boolean flag) {
         if (flag) {
-            this.b.remove(entityhuman);
+            this.f.remove(entityhuman);
         } else {
-            this.b.add(entityhuman);
+            this.f.add(entityhuman);
         }
     }
 
-    public abstract boolean b(EntityHuman entityhuman);
+    public abstract boolean c(EntityHuman entityhuman);
 
     protected boolean a(ItemStack itemstack, int i, int j, boolean flag) {
         boolean flag1 = false;
@@ -249,7 +262,7 @@ public abstract class Container {
 
         if (itemstack.isStackable()) {
             while (itemstack.count > 0 && (!flag && k < j || flag && k >= i)) {
-                slot = (Slot) this.e.get(k);
+                slot = (Slot) this.b.get(k);
                 itemstack1 = slot.getItem();
                 if (itemstack1 != null && itemstack1.id == itemstack.id && (!itemstack.usesData() || itemstack.getData() == itemstack1.getData()) && ItemStack.equals(itemstack, itemstack1)) {
                     int l = itemstack1.count + itemstack.count;
@@ -257,12 +270,12 @@ public abstract class Container {
                     if (l <= itemstack.getMaxStackSize()) {
                         itemstack.count = 0;
                         itemstack1.count = l;
-                        slot.d();
+                        slot.e();
                         flag1 = true;
                     } else if (itemstack1.count < itemstack.getMaxStackSize()) {
                         itemstack.count -= itemstack.getMaxStackSize() - itemstack1.count;
                         itemstack1.count = itemstack.getMaxStackSize();
-                        slot.d();
+                        slot.e();
                         flag1 = true;
                     }
                 }
@@ -283,11 +296,11 @@ public abstract class Container {
             }
 
             while (!flag && k < j || flag && k >= i) {
-                slot = (Slot) this.e.get(k);
+                slot = (Slot) this.b.get(k);
                 itemstack1 = slot.getItem();
                 if (itemstack1 == null) {
                     slot.set(itemstack.cloneItemStack());
-                    slot.d();
+                    slot.e();
                     itemstack.count = 0;
                     flag1 = true;
                     break;
