@@ -7,10 +7,10 @@ public class BlockMinecartDetector extends BlockMinecartTrack {
 
     public BlockMinecartDetector(int i, int j) {
         super(i, j, true);
-        this.a(true);
+        this.b(true);
     }
 
-    public int d() {
+    public int p_() {
         return 20;
     }
 
@@ -23,17 +23,17 @@ public class BlockMinecartDetector extends BlockMinecartTrack {
             int l = world.getData(i, j, k);
 
             if ((l & 8) == 0) {
-                this.f(world, i, j, k, l);
+                this.e(world, i, j, k, l);
             }
         }
     }
 
-    public void a(World world, int i, int j, int k, Random random) {
+    public void b(World world, int i, int j, int k, Random random) {
         if (!world.isStatic) {
             int l = world.getData(i, j, k);
 
             if ((l & 8) != 0) {
-                this.f(world, i, j, k, l);
+                this.e(world, i, j, k, l);
             }
         }
     }
@@ -42,17 +42,17 @@ public class BlockMinecartDetector extends BlockMinecartTrack {
         return (iblockaccess.getData(i, j, k) & 8) != 0;
     }
 
-    public boolean d(World world, int i, int j, int k, int l) {
+    public boolean c(World world, int i, int j, int k, int l) {
         return (world.getData(i, j, k) & 8) == 0 ? false : l == 1;
     }
 
-    private void f(World world, int i, int j, int k, int l) {
+    private void e(World world, int i, int j, int k, int l) {
         boolean flag = (l & 8) != 0;
         boolean flag1 = false;
         float f = 0.125F;
-        List list = world.a(EntityMinecart.class, AxisAlignedBB.b((double) ((float) i + f), (double) j, (double) ((float) k + f), (double) ((float) (i + 1) - f), (double) ((float) (j + 1) - f), (double) ((float) (k + 1) - f)));
+        List list = world.a(EntityMinecart.class, AxisAlignedBB.a().a((double) ((float) i + f), (double) j, (double) ((float) k + f), (double) ((float) (i + 1) - f), (double) ((float) (j + 1) - f), (double) ((float) (k + 1) - f)));
 
-        if (list.size() > 0) {
+        if (!list.isEmpty()) {
             flag1 = true;
         }
 
@@ -60,18 +60,18 @@ public class BlockMinecartDetector extends BlockMinecartTrack {
             world.setData(i, j, k, l | 8);
             world.applyPhysics(i, j, k, this.id);
             world.applyPhysics(i, j - 1, k, this.id);
-            world.b(i, j, k, i, j, k);
+            world.d(i, j, k, i, j, k);
         }
 
         if (!flag1 && flag) {
             world.setData(i, j, k, l & 7);
             world.applyPhysics(i, j, k, this.id);
             world.applyPhysics(i, j - 1, k, this.id);
-            world.b(i, j, k, i, j, k);
+            world.d(i, j, k, i, j, k);
         }
 
         if (flag1) {
-            world.c(i, j, k, this.id, this.d());
+            world.a(i, j, k, this.id, this.p_());
         }
     }
 }
