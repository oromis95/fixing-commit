@@ -8,6 +8,7 @@ public class ItemBucket extends Item {
         super(i);
         this.maxStackSize = 1;
         this.a = j;
+        this.a(CreativeModeTab.f);
     }
 
     public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
@@ -31,7 +32,7 @@ public class ItemBucket extends Item {
                 }
 
                 if (this.a == 0) {
-                    if (!entityhuman.d(i, j, k)) {
+                    if (!entityhuman.e(i, j, k)) {
                         return itemstack;
                     }
 
@@ -41,7 +42,15 @@ public class ItemBucket extends Item {
                             return itemstack;
                         }
 
-                        return new ItemStack(Item.WATER_BUCKET);
+                        if (--itemstack.count <= 0) {
+                            return new ItemStack(Item.WATER_BUCKET);
+                        }
+
+                        if (!entityhuman.inventory.pickup(new ItemStack(Item.WATER_BUCKET))) {
+                            entityhuman.drop(new ItemStack(Item.WATER_BUCKET.id, 1, 0));
+                        }
+
+                        return itemstack;
                     }
 
                     if (world.getMaterial(i, j, k) == Material.LAVA && world.getData(i, j, k) == 0) {
@@ -50,7 +59,15 @@ public class ItemBucket extends Item {
                             return itemstack;
                         }
 
-                        return new ItemStack(Item.LAVA_BUCKET);
+                        if (--itemstack.count <= 0) {
+                            return new ItemStack(Item.LAVA_BUCKET);
+                        }
+
+                        if (!entityhuman.inventory.pickup(new ItemStack(Item.LAVA_BUCKET))) {
+                            entityhuman.drop(new ItemStack(Item.LAVA_BUCKET.id, 1, 0));
+                        }
+
+                        return itemstack;
                     }
                 } else {
                     if (this.a < 0) {
@@ -81,25 +98,11 @@ public class ItemBucket extends Item {
                         ++i;
                     }
 
-                    if (!entityhuman.d(i, j, k)) {
+                    if (!entityhuman.e(i, j, k)) {
                         return itemstack;
                     }
 
-                    if (world.isEmpty(i, j, k) || !world.getMaterial(i, j, k).isBuildable()) {
-                        if (world.worldProvider.d && this.a == Block.WATER.id) {
-                            world.makeSound(d0 + 0.5D, d1 + 0.5D, d2 + 0.5D, "random.fizz", 0.5F, 2.6F + (world.random.nextFloat() - world.random.nextFloat()) * 0.8F);
-
-                            for (int l = 0; l < 8; ++l) {
-                                world.a("largesmoke", (double) i + Math.random(), (double) j + Math.random(), (double) k + Math.random(), 0.0D, 0.0D, 0.0D);
-                            }
-                        } else {
-                            world.setTypeIdAndData(i, j, k, this.a, 0);
-                        }
-
-                        if (entityhuman.abilities.canInstantlyBuild) {
-                            return itemstack;
-                        }
-
+                    if (this.a(world, d0, d1, d2, i, j, k) && !entityhuman.abilities.canInstantlyBuild) {
                         return new ItemStack(Item.BUCKET);
                     }
                 }
@@ -110,4 +113,24 @@ public class ItemBucket extends Item {
             return itemstack;
         }
     }
+
+    public boolean a(World world, double d0, double d1, double d2, int i, int j, int k) {
+        if (this.a <= 0) {
+            return false;
+        } else if (!world.isEmpty(i, j, k) && world.getMaterial(i, j, k).isBuildable()) {
+            return false;
+        } else {
+            if (world.worldProvider.d && this.a == Block.WATER.id) {
+                world.makeSound(d0 + 0.5D, d1 + 0.5D, d2 + 0.5D, "random.fizz", 0.5F, 2.6F + (world.random.nextFloat() - world.random.nextFloat()) * 0.8F);
+
+                for (int l = 0; l < 8; ++l) {
+                    world.a("largesmoke", (double) i + Math.random(), (double) j + Math.random(), (double) k + Math.random(), 0.0D, 0.0D, 0.0D);
+                }
+            } else {
+                world.setTypeIdAndData(i, j, k, this.a, 0);
+            }
+
+            return true;
+        }
+    }
 }
