@@ -18,21 +18,20 @@ public class PathfinderGoalTakeFlower extends PathfinderGoal {
     public boolean a() {
         if (this.a.getAge() >= 0) {
             return false;
-        } else if (!this.a.world.e()) {
+        } else if (!this.a.world.r()) {
             return false;
         } else {
             List list = this.a.world.a(EntityIronGolem.class, this.a.boundingBox.grow(6.0D, 2.0D, 6.0D));
 
-            if (list.size() == 0) {
+            if (list.isEmpty()) {
                 return false;
             } else {
                 Iterator iterator = list.iterator();
 
                 while (iterator.hasNext()) {
-                    Entity entity = (Entity) iterator.next();
-                    EntityIronGolem entityirongolem = (EntityIronGolem) entity;
+                    EntityIronGolem entityirongolem = (EntityIronGolem) iterator.next();
 
-                    if (entityirongolem.m_() > 0) {
+                    if (entityirongolem.p() > 0) {
                         this.b = entityirongolem;
                         break;
                     }
@@ -44,30 +43,30 @@ public class PathfinderGoalTakeFlower extends PathfinderGoal {
     }
 
     public boolean b() {
-        return this.b.m_() > 0;
+        return this.b.p() > 0;
     }
 
-    public void c() {
-        this.c = this.a.an().nextInt(320);
+    public void e() {
+        this.c = this.a.au().nextInt(320);
         this.d = false;
-        this.b.al().f();
+        this.b.getNavigation().g();
     }
 
-    public void d() {
+    public void c() {
         this.b = null;
-        this.a.al().f();
+        this.a.getNavigation().g();
     }
 
-    public void e() {
+    public void d() {
         this.a.getControllerLook().a(this.b, 30.0F, 30.0F);
-        if (this.b.m_() == this.c) {
-            this.a.al().a((EntityLiving) this.b, 0.15F);
+        if (this.b.p() == this.c) {
+            this.a.getNavigation().a((EntityLiving) this.b, 0.15F);
             this.d = true;
         }
 
-        if (this.d && this.a.j(this.b) < 4.0D) {
-            this.b.a(false);
-            this.a.al().f();
+        if (this.d && this.a.e(this.b) < 4.0D) {
+            this.b.e(false);
+            this.a.getNavigation().g();
         }
     }
 }
