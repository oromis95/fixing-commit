@@ -8,16 +8,16 @@ public class Packet9Respawn extends Packet {
     public int a;
     public int b;
     public int c;
-    public int d;
+    public EnumGamemode d;
     public WorldType e;
 
     public Packet9Respawn() {}
 
-    public Packet9Respawn(int i, byte b0, WorldType worldtype, int j, int k) {
+    public Packet9Respawn(int i, byte b0, WorldType worldtype, int j, EnumGamemode enumgamemode) {
         this.a = i;
         this.b = b0;
         this.c = j;
-        this.d = k;
+        this.d = enumgamemode;
         this.e = worldtype;
     }
 
@@ -28,7 +28,7 @@ public class Packet9Respawn extends Packet {
     public void a(DataInputStream datainputstream) {
         this.a = datainputstream.readInt();
         this.b = datainputstream.readByte();
-        this.d = datainputstream.readByte();
+        this.d = EnumGamemode.a(datainputstream.readByte());
         this.c = datainputstream.readShort();
         String s = a(datainputstream, 16);
 
@@ -41,12 +41,12 @@ public class Packet9Respawn extends Packet {
     public void a(DataOutputStream dataoutputstream) {
         dataoutputstream.writeInt(this.a);
         dataoutputstream.writeByte(this.b);
-        dataoutputstream.writeByte(this.d);
+        dataoutputstream.writeByte(this.d.a());
         dataoutputstream.writeShort(this.c);
         a(this.e.name(), dataoutputstream);
     }
 
     public int a() {
-        return 8 + this.e.name().length();
+        return 8 + (this.e == null ? 0 : this.e.name().length());
     }
 }
