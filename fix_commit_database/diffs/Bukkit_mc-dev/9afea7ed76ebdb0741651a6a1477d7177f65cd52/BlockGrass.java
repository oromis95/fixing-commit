@@ -7,14 +7,15 @@ public class BlockGrass extends Block {
     protected BlockGrass(int i) {
         super(i, Material.GRASS);
         this.textureId = 3;
-        this.a(true);
+        this.b(true);
+        this.a(CreativeModeTab.b);
     }
 
     public int a(int i, int j) {
         return i == 1 ? 0 : (i == 0 ? 2 : 3);
     }
 
-    public void a(World world, int i, int j, int k, Random random) {
+    public void b(World world, int i, int j, int k, Random random) {
         if (!world.isStatic) {
             if (world.getLightLevel(i, j + 1, k) < 4 && Block.lightBlock[world.getTypeId(i, j + 1, k)] > 2) {
                 world.setTypeId(i, j, k, Block.DIRT.id);
