@@ -2,8 +2,8 @@ package net.minecraft.server;
 
 public class EntityBlaze extends EntityMonster {
 
-    private float a = 0.5F;
-    private int b;
+    private float d = 0.5F;
+    private int e;
     private int g;
 
     public EntityBlaze(World world) {
@@ -11,55 +11,47 @@ public class EntityBlaze extends EntityMonster {
         this.texture = "/mob/fire.png";
         this.fireProof = true;
         this.damage = 6;
-        this.aA = 10;
+        this.aV = 10;
     }
 
     public int getMaxHealth() {
         return 20;
     }
 
-    protected void b() {
-        super.b();
+    protected void a() {
+        super.a();
         this.datawatcher.a(16, new Byte((byte) 0));
     }
 
-    protected String i() {
+    protected String aQ() {
         return "mob.blaze.breathe";
     }
 
-    protected String j() {
+    protected String aR() {
         return "mob.blaze.hit";
     }
 
-    protected String k() {
+    protected String aS() {
         return "mob.blaze.death";
     }
 
-    public boolean damageEntity(DamageSource damagesource, int i) {
-        return super.damageEntity(damagesource, i);
-    }
-
-    public void die(DamageSource damagesource) {
-        super.die(damagesource);
-    }
-
-    public float b(float f) {
+    public float c(float f) {
         return 1.0F;
     }
 
-    public void e() {
+    public void d() {
         if (!this.world.isStatic) {
-            if (this.aT()) {
+            if (this.G()) {
                 this.damageEntity(DamageSource.DROWN, 1);
             }
 
-            --this.b;
-            if (this.b <= 0) {
-                this.b = 100;
-                this.a = 0.5F + (float) this.random.nextGaussian() * 3.0F;
+            --this.e;
+            if (this.e <= 0) {
+                this.e = 100;
+                this.d = 0.5F + (float) this.random.nextGaussian() * 3.0F;
             }
 
-            if (this.I() != null && this.I().locY + (double) this.I().getHeadHeight() > this.locY + (double) this.getHeadHeight() + (double) this.a) {
+            if (this.m() != null && this.m().locY + (double) this.m().getHeadHeight() > this.locY + (double) this.getHeadHeight() + (double) this.d) {
                 this.motY += (0.30000001192092896D - this.motY) * 0.30000001192092896D;
             }
         }
@@ -76,13 +68,13 @@ public class EntityBlaze extends EntityMonster {
             this.world.a("largesmoke", this.locX + (this.random.nextDouble() - 0.5D) * (double) this.width, this.locY + this.random.nextDouble() * (double) this.length, this.locZ + (this.random.nextDouble() - 0.5D) * (double) this.width, 0.0D, 0.0D, 0.0D);
         }
 
-        super.e();
+        super.d();
     }
 
     protected void a(Entity entity, float f) {
         if (this.attackTicks <= 0 && f < 2.0F && entity.boundingBox.e > this.boundingBox.b && entity.boundingBox.b < this.boundingBox.e) {
             this.attackTicks = 20;
-            this.a(entity);
+            this.k(entity);
         } else if (f < 30.0F) {
             double d0 = entity.locX - this.locX;
             double d1 = entity.boundingBox.b + (double) (entity.length / 2.0F) - (this.locY + (double) (this.length / 2.0F));
@@ -92,13 +84,13 @@ public class EntityBlaze extends EntityMonster {
                 ++this.g;
                 if (this.g == 1) {
                     this.attackTicks = 60;
-                    this.a(true);
+                    this.e(true);
                 } else if (this.g <= 4) {
                     this.attackTicks = 6;
                 } else {
                     this.attackTicks = 100;
                     this.g = 0;
-                    this.a(false);
+                    this.e(false);
                 }
 
                 if (this.g > 1) {
@@ -116,26 +108,18 @@ public class EntityBlaze extends EntityMonster {
             }
 
             this.yaw = (float) (Math.atan2(d2, d0) * 180.0D / 3.1415927410125732D) - 90.0F;
-            this.e = true;
+            this.b = true;
         }
     }
 
     protected void a(float f) {}
 
-    public void b(NBTTagCompound nbttagcompound) {
-        super.b(nbttagcompound);
-    }
-
-    public void a(NBTTagCompound nbttagcompound) {
-        super.a(nbttagcompound);
-    }
-
     protected int getLootId() {
         return Item.BLAZE_ROD.id;
     }
 
     public boolean isBurning() {
-        return this.A();
+        return this.n();
     }
 
     protected void dropDeathLoot(boolean flag, int i) {
@@ -148,11 +132,11 @@ public class EntityBlaze extends EntityMonster {
         }
     }
 
-    public boolean A() {
+    public boolean n() {
         return (this.datawatcher.getByte(16) & 1) != 0;
     }
 
-    public void a(boolean flag) {
+    public void e(boolean flag) {
         byte b0 = this.datawatcher.getByte(16);
 
         if (flag) {
@@ -164,7 +148,7 @@ public class EntityBlaze extends EntityMonster {
         this.datawatcher.watch(16, Byte.valueOf(b0));
     }
 
-    protected boolean C() {
+    protected boolean o() {
         return true;
     }
 }
