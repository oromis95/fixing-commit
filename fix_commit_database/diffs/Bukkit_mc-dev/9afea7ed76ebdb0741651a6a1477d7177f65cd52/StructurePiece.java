@@ -6,13 +6,13 @@ import java.util.Random;
 
 public abstract class StructurePiece {
 
-    protected StructureBoundingBox g;
-    protected int h;
-    protected int i;
+    protected StructureBoundingBox e;
+    protected int f;
+    protected int g;
 
     protected StructurePiece(int i) {
-        this.i = i;
-        this.h = -1;
+        this.g = i;
+        this.f = -1;
     }
 
     public void a(StructurePiece structurepiece, List list, Random random) {}
@@ -20,11 +20,11 @@ public abstract class StructurePiece {
     public abstract boolean a(World world, Random random, StructureBoundingBox structureboundingbox);
 
     public StructureBoundingBox b() {
-        return this.g;
+        return this.e;
     }
 
     public int c() {
-        return this.i;
+        return this.g;
     }
 
     public static StructurePiece a(List list, StructureBoundingBox structureboundingbox) {
@@ -43,17 +43,17 @@ public abstract class StructurePiece {
         return structurepiece;
     }
 
-    public ChunkPosition b_() {
-        return new ChunkPosition(this.g.e(), this.g.f(), this.g.g());
+    public ChunkPosition a() {
+        return new ChunkPosition(this.e.e(), this.e.f(), this.e.g());
     }
 
     protected boolean a(World world, StructureBoundingBox structureboundingbox) {
-        int i = Math.max(this.g.a - 1, structureboundingbox.a);
-        int j = Math.max(this.g.b - 1, structureboundingbox.b);
-        int k = Math.max(this.g.c - 1, structureboundingbox.c);
-        int l = Math.min(this.g.d + 1, structureboundingbox.d);
-        int i1 = Math.min(this.g.e + 1, structureboundingbox.e);
-        int j1 = Math.min(this.g.f + 1, structureboundingbox.f);
+        int i = Math.max(this.e.a - 1, structureboundingbox.a);
+        int j = Math.max(this.e.b - 1, structureboundingbox.b);
+        int k = Math.max(this.e.c - 1, structureboundingbox.c);
+        int l = Math.min(this.e.d + 1, structureboundingbox.d);
+        int i1 = Math.min(this.e.e + 1, structureboundingbox.e);
+        int j1 = Math.min(this.e.f + 1, structureboundingbox.f);
 
         int k1;
         int l1;
@@ -105,37 +105,37 @@ public abstract class StructurePiece {
     }
 
     protected int a(int i, int j) {
-        switch (this.h) {
+        switch (this.f) {
         case 0:
         case 2:
-            return this.g.a + i;
+            return this.e.a + i;
 
         case 1:
-            return this.g.d - j;
+            return this.e.d - j;
 
         case 3:
-            return this.g.a + j;
+            return this.e.a + j;
 
         default:
             return i;
         }
     }
 
-    protected int b(int i) {
-        return this.h == -1 ? i : i + this.g.b;
+    protected int a(int i) {
+        return this.f == -1 ? i : i + this.e.b;
     }
 
     protected int b(int i, int j) {
-        switch (this.h) {
+        switch (this.f) {
         case 0:
-            return this.g.c + j;
+            return this.e.c + j;
 
         case 1:
         case 3:
-            return this.g.c + i;
+            return this.e.c + i;
 
         case 2:
-            return this.g.f - j;
+            return this.e.f - j;
 
         default:
             return j;
@@ -144,7 +144,7 @@ public abstract class StructurePiece {
 
     protected int c(int i, int j) {
         if (i == Block.RAILS.id) {
-            if (this.h == 1 || this.h == 3) {
+            if (this.f == 1 || this.f == 3) {
                 if (j == 1) {
                     return 0;
                 }
@@ -152,9 +152,9 @@ public abstract class StructurePiece {
                 return 1;
             }
         } else if (i != Block.WOODEN_DOOR.id && i != Block.IRON_DOOR_BLOCK.id) {
-            if (i != Block.COBBLESTONE_STAIRS.id && i != Block.WOOD_STAIRS.id && i != Block.NETHER_BRICK_STAIRS.id && i != Block.STONE_STAIRS.id) {
+            if (i != Block.COBBLESTONE_STAIRS.id && i != Block.WOOD_STAIRS.id && i != Block.NETHER_BRICK_STAIRS.id && i != Block.STONE_STAIRS.id && i != Block.SANDSTONE_STAIRS.id) {
                 if (i == Block.LADDER.id) {
-                    if (this.h == 0) {
+                    if (this.f == 0) {
                         if (j == 2) {
                             return 3;
                         }
@@ -162,7 +162,7 @@ public abstract class StructurePiece {
                         if (j == 3) {
                             return 2;
                         }
-                    } else if (this.h == 1) {
+                    } else if (this.f == 1) {
                         if (j == 2) {
                             return 4;
                         }
@@ -178,7 +178,7 @@ public abstract class StructurePiece {
                         if (j == 5) {
                             return 3;
                         }
-                    } else if (this.h == 3) {
+                    } else if (this.f == 3) {
                         if (j == 2) {
                             return 5;
                         }
@@ -196,7 +196,7 @@ public abstract class StructurePiece {
                         }
                     }
                 } else if (i == Block.STONE_BUTTON.id) {
-                    if (this.h == 0) {
+                    if (this.f == 0) {
                         if (j == 3) {
                             return 4;
                         }
@@ -204,7 +204,7 @@ public abstract class StructurePiece {
                         if (j == 4) {
                             return 3;
                         }
-                    } else if (this.h == 1) {
+                    } else if (this.f == 1) {
                         if (j == 3) {
                             return 1;
                         }
@@ -220,7 +220,7 @@ public abstract class StructurePiece {
                         if (j == 1) {
                             return 4;
                         }
-                    } else if (this.h == 3) {
+                    } else if (this.f == 3) {
                         if (j == 3) {
                             return 2;
                         }
@@ -237,8 +237,84 @@ public abstract class StructurePiece {
                             return 4;
                         }
                     }
+                } else if (i != Block.TRIPWIRE_SOURCE.id && (Block.byId[i] == null || !(Block.byId[i] instanceof BlockDirectional))) {
+                    if (i == Block.PISTON.id || i == Block.PISTON_STICKY.id || i == Block.LEVER.id || i == Block.DISPENSER.id) {
+                        if (this.f == 0) {
+                            if (j == 2 || j == 3) {
+                                return Facing.OPPOSITE_FACING[j];
+                            }
+                        } else if (this.f == 1) {
+                            if (j == 2) {
+                                return 4;
+                            }
+
+                            if (j == 3) {
+                                return 5;
+                            }
+
+                            if (j == 4) {
+                                return 2;
+                            }
+
+                            if (j == 5) {
+                                return 3;
+                            }
+                        } else if (this.f == 3) {
+                            if (j == 2) {
+                                return 5;
+                            }
+
+                            if (j == 3) {
+                                return 4;
+                            }
+
+                            if (j == 4) {
+                                return 2;
+                            }
+
+                            if (j == 5) {
+                                return 3;
+                            }
+                        }
+                    }
+                } else if (this.f == 0) {
+                    if (j == 0 || j == 2) {
+                        return Direction.e[j];
+                    }
+                } else if (this.f == 1) {
+                    if (j == 2) {
+                        return 1;
+                    }
+
+                    if (j == 0) {
+                        return 3;
+                    }
+
+                    if (j == 1) {
+                        return 2;
+                    }
+
+                    if (j == 3) {
+                        return 0;
+                    }
+                } else if (this.f == 3) {
+                    if (j == 2) {
+                        return 3;
+                    }
+
+                    if (j == 0) {
+                        return 1;
+                    }
+
+                    if (j == 1) {
+                        return 2;
+                    }
+
+                    if (j == 3) {
+                        return 0;
+                    }
                 }
-            } else if (this.h == 0) {
+            } else if (this.f == 0) {
                 if (j == 2) {
                     return 3;
                 }
@@ -246,7 +322,7 @@ public abstract class StructurePiece {
                 if (j == 3) {
                     return 2;
                 }
-            } else if (this.h == 1) {
+            } else if (this.f == 1) {
                 if (j == 0) {
                     return 2;
                 }
@@ -262,7 +338,7 @@ public abstract class StructurePiece {
                 if (j == 3) {
                     return 1;
                 }
-            } else if (this.h == 3) {
+            } else if (this.f == 3) {
                 if (j == 0) {
                     return 2;
                 }
@@ -279,7 +355,7 @@ public abstract class StructurePiece {
                     return 0;
                 }
             }
-        } else if (this.h == 0) {
+        } else if (this.f == 0) {
             if (j == 0) {
                 return 2;
             }
@@ -288,11 +364,11 @@ public abstract class StructurePiece {
                 return 0;
             }
         } else {
-            if (this.h == 1) {
+            if (this.f == 1) {
                 return j + 1 & 3;
             }
 
-            if (this.h == 3) {
+            if (this.f == 3) {
                 return j + 3 & 3;
             }
         }
@@ -302,7 +378,7 @@ public abstract class StructurePiece {
 
     protected void a(World world, int i, int j, int k, int l, int i1, StructureBoundingBox structureboundingbox) {
         int j1 = this.a(k, i1);
-        int k1 = this.b(l);
+        int k1 = this.a(l);
         int l1 = this.b(k, i1);
 
         if (structureboundingbox.b(j1, k1, l1)) {
@@ -312,12 +388,22 @@ public abstract class StructurePiece {
 
     protected int a(World world, int i, int j, int k, StructureBoundingBox structureboundingbox) {
         int l = this.a(i, k);
-        int i1 = this.b(j);
+        int i1 = this.a(j);
         int j1 = this.b(i, k);
 
         return !structureboundingbox.b(l, i1, j1) ? 0 : world.getTypeId(l, i1, j1);
     }
 
+    protected void a(World world, StructureBoundingBox structureboundingbox, int i, int j, int k, int l, int i1, int j1) {
+        for (int k1 = j; k1 <= i1; ++k1) {
+            for (int l1 = i; l1 <= l; ++l1) {
+                for (int i2 = k; i2 <= j1; ++i2) {
+                    this.a(world, 0, 0, l1, k1, i2, structureboundingbox);
+                }
+            }
+        }
+    }
+
     protected void a(World world, StructureBoundingBox structureboundingbox, int i, int j, int k, int l, int i1, int j1, int k1, int l1, boolean flag) {
         for (int i2 = j; i2 <= i1; ++i2) {
             for (int j2 = i; j2 <= l; ++j2) {
@@ -334,6 +420,22 @@ public abstract class StructurePiece {
         }
     }
 
+    protected void a(World world, StructureBoundingBox structureboundingbox, int i, int j, int k, int l, int i1, int j1, int k1, int l1, int i2, int j2, boolean flag) {
+        for (int k2 = j; k2 <= i1; ++k2) {
+            for (int l2 = i; l2 <= l; ++l2) {
+                for (int i3 = k; i3 <= j1; ++i3) {
+                    if (!flag || this.a(world, l2, k2, i3, structureboundingbox) != 0) {
+                        if (k2 != j && k2 != i1 && l2 != i && l2 != l && i3 != k && i3 != j1) {
+                            this.a(world, i2, j2, l2, k2, i3, structureboundingbox);
+                        } else {
+                            this.a(world, k1, l1, l2, k2, i3, structureboundingbox);
+                        }
+                    }
+                }
+            }
+        }
+    }
+
     protected void a(World world, StructureBoundingBox structureboundingbox, int i, int j, int k, int l, int i1, int j1, boolean flag, Random random, StructurePieceBlockSelector structurepieceblockselector) {
         for (int k1 = j; k1 <= i1; ++k1) {
             for (int l1 = i; l1 <= l; ++l1) {
@@ -399,7 +501,7 @@ public abstract class StructurePiece {
 
     protected void b(World world, int i, int j, int k, StructureBoundingBox structureboundingbox) {
         int l = this.a(i, k);
-        int i1 = this.b(j);
+        int i1 = this.a(j);
         int j1 = this.b(i, k);
 
         if (structureboundingbox.b(l, i1, j1)) {
@@ -412,7 +514,7 @@ public abstract class StructurePiece {
 
     protected void b(World world, int i, int j, int k, int l, int i1, StructureBoundingBox structureboundingbox) {
         int j1 = this.a(k, i1);
-        int k1 = this.b(l);
+        int k1 = this.a(l);
         int l1 = this.b(k, i1);
 
         if (structureboundingbox.b(j1, k1, l1)) {
@@ -423,9 +525,9 @@ public abstract class StructurePiece {
         }
     }
 
-    protected void a(World world, StructureBoundingBox structureboundingbox, Random random, int i, int j, int k, StructurePieceTreasure[] astructurepiecetreasure, int l) {
+    protected boolean a(World world, StructureBoundingBox structureboundingbox, Random random, int i, int j, int k, StructurePieceTreasure[] astructurepiecetreasure, int l) {
         int i1 = this.a(i, k);
-        int j1 = this.b(j);
+        int j1 = this.a(j);
         int k1 = this.b(i, k);
 
         if (structureboundingbox.b(i1, j1, k1) && world.getTypeId(i1, j1, k1) != Block.CHEST.id) {
@@ -433,29 +535,37 @@ public abstract class StructurePiece {
             TileEntityChest tileentitychest = (TileEntityChest) world.getTileEntity(i1, j1, k1);
 
             if (tileentitychest != null) {
-                a(random, astructurepiecetreasure, tileentitychest, l);
+                StructurePieceTreasure.a(random, astructurepiecetreasure, tileentitychest, l);
             }
+
+            return true;
+        } else {
+            return false;
         }
     }
 
-    private static void a(Random random, StructurePieceTreasure[] astructurepiecetreasure, TileEntityChest tileentitychest, int i) {
-        for (int j = 0; j < i; ++j) {
-            StructurePieceTreasure structurepiecetreasure = (StructurePieceTreasure) WeightedRandom.a(random, (WeightedRandomChoice[]) astructurepiecetreasure);
-            int k = structurepiecetreasure.c + random.nextInt(structurepiecetreasure.e - structurepiecetreasure.c + 1);
+    protected boolean a(World world, StructureBoundingBox structureboundingbox, Random random, int i, int j, int k, int l, StructurePieceTreasure[] astructurepiecetreasure, int i1) {
+        int j1 = this.a(i, k);
+        int k1 = this.a(j);
+        int l1 = this.b(i, k);
 
-            if (Item.byId[structurepiecetreasure.a].getMaxStackSize() >= k) {
-                tileentitychest.setItem(random.nextInt(tileentitychest.getSize()), new ItemStack(structurepiecetreasure.a, k, structurepiecetreasure.b));
-            } else {
-                for (int l = 0; l < k; ++l) {
-                    tileentitychest.setItem(random.nextInt(tileentitychest.getSize()), new ItemStack(structurepiecetreasure.a, 1, structurepiecetreasure.b));
-                }
+        if (structureboundingbox.b(j1, k1, l1) && world.getTypeId(j1, k1, l1) != Block.DISPENSER.id) {
+            world.setTypeIdAndData(j1, k1, l1, Block.DISPENSER.id, this.c(Block.DISPENSER.id, l));
+            TileEntityDispenser tileentitydispenser = (TileEntityDispenser) world.getTileEntity(j1, k1, l1);
+
+            if (tileentitydispenser != null) {
+                StructurePieceTreasure.a(random, astructurepiecetreasure, tileentitydispenser, i1);
             }
+
+            return true;
+        } else {
+            return false;
         }
     }
 
     protected void a(World world, StructureBoundingBox structureboundingbox, Random random, int i, int j, int k, int l) {
         int i1 = this.a(i, k);
-        int j1 = this.b(j);
+        int j1 = this.a(j);
         int k1 = this.b(i, k);
 
         if (structureboundingbox.b(i1, j1, k1)) {
