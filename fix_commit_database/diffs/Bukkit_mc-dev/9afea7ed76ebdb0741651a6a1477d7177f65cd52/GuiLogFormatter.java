@@ -31,7 +31,7 @@ class GuiLogFormatter extends Formatter {
         } else if (level == Level.SEVERE) {
             stringbuilder.append("[SEVERE] ");
         } else if (level == Level.SEVERE) {
-            stringbuilder.append("[" + level.getLocalizedName() + "] ");
+            stringbuilder.append("[").append(level.getLocalizedName()).append("] ");
         }
 
         stringbuilder.append(logrecord.getMessage());
