@@ -8,9 +8,10 @@ public class ItemMinecart extends Item {
         super(i);
         this.maxStackSize = 1;
         this.a = j;
+        this.a(CreativeModeTab.e);
     }
 
-    public boolean interactWith(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
+    public boolean interactWith(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l, float f, float f1, float f2) {
         int i1 = world.getTypeId(i, j, k);
 
         if (BlockMinecartTrack.d(i1)) {
