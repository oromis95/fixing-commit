@@ -1,15 +1,16 @@
 package net.minecraft.server;
 
+import java.util.Iterator;
 import java.util.List;
 
-public class ContainerEnchantTableSubcontainer implements IInventory {
+public class InventorySubcontainer implements IInventory {
 
     private String a;
     private int b;
     private ItemStack[] items;
     private List d;
 
-    public ContainerEnchantTableSubcontainer(String s, int i) {
+    public InventorySubcontainer(String s, int i) {
         this.a = s;
         this.b = i;
         this.items = new ItemStack[i];
@@ -76,8 +77,12 @@ public class ContainerEnchantTableSubcontainer implements IInventory {
 
     public void update() {
         if (this.d != null) {
-            for (int i = 0; i < this.d.size(); ++i) {
-                ((IInventoryListener) this.d.get(i)).a(this);
+            Iterator iterator = this.d.iterator();
+
+            while (iterator.hasNext()) {
+                IInventoryListener iinventorylistener = (IInventoryListener) iterator.next();
+
+                iinventorylistener.a(this);
             }
         }
     }
@@ -86,7 +91,7 @@ public class ContainerEnchantTableSubcontainer implements IInventory {
         return true;
     }
 
-    public void f() {}
+    public void startOpen() {}
 
-    public void g() {}
+    public void f() {}
 }
