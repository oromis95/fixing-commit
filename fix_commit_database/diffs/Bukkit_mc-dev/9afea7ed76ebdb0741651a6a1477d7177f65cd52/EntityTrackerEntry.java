@@ -26,6 +26,7 @@ public class EntityTrackerEntry {
     private boolean s = false;
     private boolean isMoving;
     private int u = 0;
+    private Entity v;
     public boolean n = false;
     public Set trackedPlayers = new HashSet();
 
@@ -39,7 +40,7 @@ public class EntityTrackerEntry {
         this.zLoc = MathHelper.floor(entity.locZ * 32.0D);
         this.yRot = MathHelper.d(entity.yaw * 256.0F / 360.0F);
         this.xRot = MathHelper.d(entity.pitch * 256.0F / 360.0F);
-        this.i = MathHelper.d(entity.ar() * 256.0F / 360.0F);
+        this.i = MathHelper.d(entity.am() * 256.0F / 360.0F);
     }
 
     public boolean equals(Object object) {
@@ -61,81 +62,86 @@ public class EntityTrackerEntry {
             this.scanPlayers(list);
         }
 
-        ++this.u;
-        if (this.m++ % this.c == 0 || this.tracker.ce) {
-            int i = MathHelper.floor(this.tracker.locX * 32.0D);
-            int j = MathHelper.floor(this.tracker.locY * 32.0D);
-            int k = MathHelper.floor(this.tracker.locZ * 32.0D);
-            int l = MathHelper.d(this.tracker.yaw * 256.0F / 360.0F);
-            int i1 = MathHelper.d(this.tracker.pitch * 256.0F / 360.0F);
-            int j1 = i - this.xLoc;
-            int k1 = j - this.yLoc;
-            int l1 = k - this.zLoc;
-            Object object = null;
-            boolean flag = Math.abs(j1) >= 4 || Math.abs(k1) >= 4 || Math.abs(l1) >= 4;
-            boolean flag1 = Math.abs(l - this.yRot) >= 4 || Math.abs(i1 - this.xRot) >= 4;
-
-            if (j1 >= -128 && j1 < 128 && k1 >= -128 && k1 < 128 && l1 >= -128 && l1 < 128 && this.u <= 400) {
-                if (flag && flag1) {
-                    object = new Packet33RelEntityMoveLook(this.tracker.id, (byte) j1, (byte) k1, (byte) l1, (byte) l, (byte) i1);
-                } else if (flag) {
-                    object = new Packet31RelEntityMove(this.tracker.id, (byte) j1, (byte) k1, (byte) l1);
-                } else if (flag1) {
-                    object = new Packet32EntityLook(this.tracker.id, (byte) l, (byte) i1);
+        if (this.v != this.tracker.vehicle) {
+            this.v = this.tracker.vehicle;
+            this.broadcast(new Packet39AttachEntity(this.tracker, this.tracker.vehicle));
+        }
+
+        if (this.tracker.vehicle == null) {
+            ++this.u;
+            if (this.m++ % this.c == 0 || this.tracker.al) {
+                int i = this.tracker.am.a(this.tracker.locX);
+                int j = MathHelper.floor(this.tracker.locY * 32.0D);
+                int k = this.tracker.am.a(this.tracker.locZ);
+                int l = MathHelper.d(this.tracker.yaw * 256.0F / 360.0F);
+                int i1 = MathHelper.d(this.tracker.pitch * 256.0F / 360.0F);
+                int j1 = i - this.xLoc;
+                int k1 = j - this.yLoc;
+                int l1 = k - this.zLoc;
+                Object object = null;
+                boolean flag = Math.abs(j1) >= 4 || Math.abs(k1) >= 4 || Math.abs(l1) >= 4;
+                boolean flag1 = Math.abs(l - this.yRot) >= 4 || Math.abs(i1 - this.xRot) >= 4;
+
+                if (j1 >= -128 && j1 < 128 && k1 >= -128 && k1 < 128 && l1 >= -128 && l1 < 128 && this.u <= 400) {
+                    if (flag && flag1) {
+                        object = new Packet33RelEntityMoveLook(this.tracker.id, (byte) j1, (byte) k1, (byte) l1, (byte) l, (byte) i1);
+                    } else if (flag) {
+                        object = new Packet31RelEntityMove(this.tracker.id, (byte) j1, (byte) k1, (byte) l1);
+                    } else if (flag1) {
+                        object = new Packet32EntityLook(this.tracker.id, (byte) l, (byte) i1);
+                    }
+                } else {
+                    this.u = 0;
+                    object = new Packet34EntityTeleport(this.tracker.id, i, j, k, (byte) l, (byte) i1);
                 }
-            } else {
-                this.u = 0;
-                this.tracker.locX = (double) i / 32.0D;
-                this.tracker.locY = (double) j / 32.0D;
-                this.tracker.locZ = (double) k / 32.0D;
-                object = new Packet34EntityTeleport(this.tracker.id, i, j, k, (byte) l, (byte) i1);
-            }
 
-            if (this.isMoving) {
-                double d0 = this.tracker.motX - this.j;
-                double d1 = this.tracker.motY - this.k;
-                double d2 = this.tracker.motZ - this.l;
-                double d3 = 0.02D;
-                double d4 = d0 * d0 + d1 * d1 + d2 * d2;
+                if (this.isMoving) {
+                    double d0 = this.tracker.motX - this.j;
+                    double d1 = this.tracker.motY - this.k;
+                    double d2 = this.tracker.motZ - this.l;
+                    double d3 = 0.02D;
+                    double d4 = d0 * d0 + d1 * d1 + d2 * d2;
+
+                    if (d4 > d3 * d3 || d4 > 0.0D && this.tracker.motX == 0.0D && this.tracker.motY == 0.0D && this.tracker.motZ == 0.0D) {
+                        this.j = this.tracker.motX;
+                        this.k = this.tracker.motY;
+                        this.l = this.tracker.motZ;
+                        this.broadcast(new Packet28EntityVelocity(this.tracker.id, this.j, this.k, this.l));
+                    }
+                }
 
-                if (d4 > d3 * d3 || d4 > 0.0D && this.tracker.motX == 0.0D && this.tracker.motY == 0.0D && this.tracker.motZ == 0.0D) {
-                    this.j = this.tracker.motX;
-                    this.k = this.tracker.motY;
-                    this.l = this.tracker.motZ;
-                    this.broadcast(new Packet28EntityVelocity(this.tracker.id, this.j, this.k, this.l));
+                if (object != null) {
+                    this.broadcast((Packet) object);
                 }
-            }
 
-            if (object != null) {
-                this.broadcast((Packet) object);
-            }
+                DataWatcher datawatcher = this.tracker.getDataWatcher();
 
-            DataWatcher datawatcher = this.tracker.getDataWatcher();
+                if (datawatcher.a()) {
+                    this.broadcastIncludingSelf(new Packet40EntityMetadata(this.tracker.id, datawatcher));
+                }
 
-            if (datawatcher.a()) {
-                this.broadcastIncludingSelf(new Packet40EntityMetadata(this.tracker.id, datawatcher));
-            }
+                int i2 = MathHelper.d(this.tracker.am() * 256.0F / 360.0F);
 
-            int i2 = MathHelper.d(this.tracker.ar() * 256.0F / 360.0F);
+                if (Math.abs(i2 - this.i) >= 4) {
+                    this.broadcast(new Packet35EntityHeadRotation(this.tracker.id, (byte) i2));
+                    this.i = i2;
+                }
 
-            if (Math.abs(i2 - this.i) >= 4) {
-                this.broadcast(new Packet35EntityHeadRotation(this.tracker.id, (byte) i2));
-                this.i = i2;
-            }
+                if (flag) {
+                    this.xLoc = i;
+                    this.yLoc = j;
+                    this.zLoc = k;
+                }
 
-            if (flag) {
-                this.xLoc = i;
-                this.yLoc = j;
-                this.zLoc = k;
+                if (flag1) {
+                    this.yRot = l;
+                    this.xRot = i1;
+                }
             }
 
-            if (flag1) {
-                this.yRot = l;
-                this.xRot = i1;
-            }
+            this.tracker.al = false;
         }
 
-        this.tracker.ce = false;
         if (this.tracker.velocityChanged) {
             this.broadcastIncludingSelf(new Packet28EntityVelocity(this.tracker));
             this.tracker.velocityChanged = false;
@@ -160,7 +166,13 @@ public class EntityTrackerEntry {
     }
 
     public void a() {
-        this.broadcast(new Packet29DestroyEntity(this.tracker.id));
+        Iterator iterator = this.trackedPlayers.iterator();
+
+        while (iterator.hasNext()) {
+            EntityPlayer entityplayer = (EntityPlayer) iterator.next();
+
+            entityplayer.g.add(Integer.valueOf(this.tracker.id));
+        }
     }
 
     public void a(EntityPlayer entityplayer) {
@@ -175,13 +187,22 @@ public class EntityTrackerEntry {
             double d1 = entityplayer.locZ - (double) (this.zLoc / 32);
 
             if (d0 >= (double) (-this.b) && d0 <= (double) this.b && d1 >= (double) (-this.b) && d1 <= (double) this.b) {
-                if (!this.trackedPlayers.contains(entityplayer)) {
+                if (!this.trackedPlayers.contains(entityplayer) && this.d(entityplayer)) {
                     this.trackedPlayers.add(entityplayer);
-                    entityplayer.netServerHandler.sendPacket(this.b());
-                    if (this.isMoving) {
+                    Packet packet = this.b();
+
+                    entityplayer.netServerHandler.sendPacket(packet);
+                    this.j = this.tracker.motX;
+                    this.k = this.tracker.motY;
+                    this.l = this.tracker.motZ;
+                    if (this.isMoving && !(packet instanceof Packet24MobSpawn)) {
                         entityplayer.netServerHandler.sendPacket(new Packet28EntityVelocity(this.tracker.id, this.tracker.motX, this.tracker.motY, this.tracker.motZ));
                     }
 
+                    if (this.tracker.vehicle != null) {
+                        entityplayer.netServerHandler.sendPacket(new Packet39AttachEntity(this.tracker, this.tracker.vehicle));
+                    }
+
                     ItemStack[] aitemstack = this.tracker.getEquipment();
 
                     if (aitemstack != null) {
@@ -211,14 +232,22 @@ public class EntityTrackerEntry {
                 }
             } else if (this.trackedPlayers.contains(entityplayer)) {
                 this.trackedPlayers.remove(entityplayer);
-                entityplayer.netServerHandler.sendPacket(new Packet29DestroyEntity(this.tracker.id));
+                entityplayer.g.add(Integer.valueOf(this.tracker.id));
             }
         }
     }
 
+    private boolean d(EntityPlayer entityplayer) {
+        return entityplayer.q().getPlayerManager().a(entityplayer, this.tracker.ah, this.tracker.aj);
+    }
+
     public void scanPlayers(List list) {
-        for (int i = 0; i < list.size(); ++i) {
-            this.updatePlayer((EntityPlayer) list.get(i));
+        Iterator iterator = list.iterator();
+
+        while (iterator.hasNext()) {
+            EntityHuman entityhuman = (EntityHuman) iterator.next();
+
+            this.updatePlayer((EntityPlayer) entityhuman);
         }
     }
 
@@ -256,81 +285,67 @@ public class EntityTrackerEntry {
 
             if (this.tracker instanceof EntityBoat) {
                 return new Packet23VehicleSpawn(this.tracker, 1);
-            } else if (this.tracker instanceof IAnimal) {
-                return new Packet24MobSpawn((EntityLiving) this.tracker);
-            } else if (this.tracker instanceof EntityEnderDragon) {
-                return new Packet24MobSpawn((EntityLiving) this.tracker);
-            } else if (this.tracker instanceof EntityFishingHook) {
-                return new Packet23VehicleSpawn(this.tracker, 90);
-            } else if (this.tracker instanceof EntityArrow) {
-                Entity entity = ((EntityArrow) this.tracker).shooter;
-
-                return new Packet23VehicleSpawn(this.tracker, 60, entity != null ? entity.id : this.tracker.id);
-            } else if (this.tracker instanceof EntitySnowball) {
-                return new Packet23VehicleSpawn(this.tracker, 61);
-            } else if (this.tracker instanceof EntityPotion) {
-                return new Packet23VehicleSpawn(this.tracker, 73, ((EntityPotion) this.tracker).getPotionValue());
-            } else if (this.tracker instanceof EntityThrownExpBottle) {
-                return new Packet23VehicleSpawn(this.tracker, 75);
-            } else if (this.tracker instanceof EntityEnderPearl) {
-                return new Packet23VehicleSpawn(this.tracker, 65);
-            } else if (this.tracker instanceof EntityEnderSignal) {
-                return new Packet23VehicleSpawn(this.tracker, 72);
-            } else {
-                Packet23VehicleSpawn packet23vehiclespawn;
-
-                if (this.tracker instanceof EntitySmallFireball) {
-                    EntitySmallFireball entitysmallfireball = (EntitySmallFireball) this.tracker;
-
-                    packet23vehiclespawn = null;
-                    if (entitysmallfireball.shooter != null) {
-                        packet23vehiclespawn = new Packet23VehicleSpawn(this.tracker, 64, entitysmallfireball.shooter.id);
-                    } else {
-                        packet23vehiclespawn = new Packet23VehicleSpawn(this.tracker, 64, 0);
-                    }
-
-                    packet23vehiclespawn.e = (int) (entitysmallfireball.dirX * 8000.0D);
-                    packet23vehiclespawn.f = (int) (entitysmallfireball.dirY * 8000.0D);
-                    packet23vehiclespawn.g = (int) (entitysmallfireball.dirZ * 8000.0D);
-                    return packet23vehiclespawn;
-                } else if (this.tracker instanceof EntityFireball) {
-                    EntityFireball entityfireball = (EntityFireball) this.tracker;
-
-                    packet23vehiclespawn = null;
-                    if (entityfireball.shooter != null) {
-                        packet23vehiclespawn = new Packet23VehicleSpawn(this.tracker, 63, ((EntityFireball) this.tracker).shooter.id);
-                    } else {
-                        packet23vehiclespawn = new Packet23VehicleSpawn(this.tracker, 63, 0);
-                    }
-
-                    packet23vehiclespawn.e = (int) (entityfireball.dirX * 8000.0D);
-                    packet23vehiclespawn.f = (int) (entityfireball.dirY * 8000.0D);
-                    packet23vehiclespawn.g = (int) (entityfireball.dirZ * 8000.0D);
-                    return packet23vehiclespawn;
-                } else if (this.tracker instanceof EntityEgg) {
-                    return new Packet23VehicleSpawn(this.tracker, 62);
-                } else if (this.tracker instanceof EntityTNTPrimed) {
-                    return new Packet23VehicleSpawn(this.tracker, 50);
-                } else if (this.tracker instanceof EntityEnderCrystal) {
-                    return new Packet23VehicleSpawn(this.tracker, 51);
+            } else if (!(this.tracker instanceof IAnimal) && !(this.tracker instanceof EntityEnderDragon)) {
+                if (this.tracker instanceof EntityFishingHook) {
+                    EntityHuman entityhuman = ((EntityFishingHook) this.tracker).owner;
+
+                    return new Packet23VehicleSpawn(this.tracker, 90, entityhuman != null ? entityhuman.id : this.tracker.id);
+                } else if (this.tracker instanceof EntityArrow) {
+                    Entity entity = ((EntityArrow) this.tracker).shooter;
+
+                    return new Packet23VehicleSpawn(this.tracker, 60, entity != null ? entity.id : this.tracker.id);
+                } else if (this.tracker instanceof EntitySnowball) {
+                    return new Packet23VehicleSpawn(this.tracker, 61);
+                } else if (this.tracker instanceof EntityPotion) {
+                    return new Packet23VehicleSpawn(this.tracker, 73, ((EntityPotion) this.tracker).getPotionValue());
+                } else if (this.tracker instanceof EntityThrownExpBottle) {
+                    return new Packet23VehicleSpawn(this.tracker, 75);
+                } else if (this.tracker instanceof EntityEnderPearl) {
+                    return new Packet23VehicleSpawn(this.tracker, 65);
+                } else if (this.tracker instanceof EntityEnderSignal) {
+                    return new Packet23VehicleSpawn(this.tracker, 72);
                 } else {
-                    if (this.tracker instanceof EntityFallingBlock) {
-                        EntityFallingBlock entityfallingblock = (EntityFallingBlock) this.tracker;
+                    Packet23VehicleSpawn packet23vehiclespawn;
 
-                        if (entityfallingblock.id == Block.SAND.id) {
-                            return new Packet23VehicleSpawn(this.tracker, 70);
-                        }
+                    if (this.tracker instanceof EntitySmallFireball) {
+                        EntitySmallFireball entitysmallfireball = (EntitySmallFireball) this.tracker;
 
-                        if (entityfallingblock.id == Block.GRAVEL.id) {
-                            return new Packet23VehicleSpawn(this.tracker, 71);
+                        packet23vehiclespawn = null;
+                        if (entitysmallfireball.shooter != null) {
+                            packet23vehiclespawn = new Packet23VehicleSpawn(this.tracker, 64, entitysmallfireball.shooter.id);
+                        } else {
+                            packet23vehiclespawn = new Packet23VehicleSpawn(this.tracker, 64, 0);
                         }
 
-                        if (entityfallingblock.id == Block.DRAGON_EGG.id) {
-                            return new Packet23VehicleSpawn(this.tracker, 74);
+                        packet23vehiclespawn.e = (int) (entitysmallfireball.dirX * 8000.0D);
+                        packet23vehiclespawn.f = (int) (entitysmallfireball.dirY * 8000.0D);
+                        packet23vehiclespawn.g = (int) (entitysmallfireball.dirZ * 8000.0D);
+                        return packet23vehiclespawn;
+                    } else if (this.tracker instanceof EntityFireball) {
+                        EntityFireball entityfireball = (EntityFireball) this.tracker;
+
+                        packet23vehiclespawn = null;
+                        if (entityfireball.shooter != null) {
+                            packet23vehiclespawn = new Packet23VehicleSpawn(this.tracker, 63, ((EntityFireball) this.tracker).shooter.id);
+                        } else {
+                            packet23vehiclespawn = new Packet23VehicleSpawn(this.tracker, 63, 0);
                         }
-                    }
 
-                    if (this.tracker instanceof EntityPainting) {
+                        packet23vehiclespawn.e = (int) (entityfireball.dirX * 8000.0D);
+                        packet23vehiclespawn.f = (int) (entityfireball.dirY * 8000.0D);
+                        packet23vehiclespawn.g = (int) (entityfireball.dirZ * 8000.0D);
+                        return packet23vehiclespawn;
+                    } else if (this.tracker instanceof EntityEgg) {
+                        return new Packet23VehicleSpawn(this.tracker, 62);
+                    } else if (this.tracker instanceof EntityTNTPrimed) {
+                        return new Packet23VehicleSpawn(this.tracker, 50);
+                    } else if (this.tracker instanceof EntityEnderCrystal) {
+                        return new Packet23VehicleSpawn(this.tracker, 51);
+                    } else if (this.tracker instanceof EntityFallingBlock) {
+                        EntityFallingBlock entityfallingblock = (EntityFallingBlock) this.tracker;
+
+                        return new Packet23VehicleSpawn(this.tracker, 70, entityfallingblock.id | entityfallingblock.data << 16);
+                    } else if (this.tracker instanceof EntityPainting) {
                         return new Packet25EntityPainting((EntityPainting) this.tracker);
                     } else if (this.tracker instanceof EntityExperienceOrb) {
                         return new Packet26AddExpOrb((EntityExperienceOrb) this.tracker);
@@ -338,6 +353,9 @@ public class EntityTrackerEntry {
                         throw new IllegalArgumentException("Don\'t know how to add " + this.tracker.getClass() + "!");
                     }
                 }
+            } else {
+                this.i = MathHelper.d(this.tracker.am() * 256.0F / 360.0F);
+                return new Packet24MobSpawn((EntityLiving) this.tracker);
             }
         }
     }
@@ -345,7 +363,7 @@ public class EntityTrackerEntry {
     public void clear(EntityPlayer entityplayer) {
         if (this.trackedPlayers.contains(entityplayer)) {
             this.trackedPlayers.remove(entityplayer);
-            entityplayer.netServerHandler.sendPacket(new Packet29DestroyEntity(this.tracker.id));
+            entityplayer.g.add(Integer.valueOf(this.tracker.id));
         }
     }
 }
