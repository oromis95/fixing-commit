@@ -4,24 +4,24 @@ public abstract class EntityCreature extends EntityLiving {
 
     private PathEntity pathEntity;
     protected Entity target;
-    protected boolean e = false;
-    protected int f = 0;
+    protected boolean b = false;
+    protected int c = 0;
 
     public EntityCreature(World world) {
         super(world);
     }
 
-    protected boolean F() {
+    protected boolean i() {
         return false;
     }
 
-    protected void d_() {
-        MethodProfiler.a("ai");
-        if (this.f > 0) {
-            --this.f;
+    protected void be() {
+        this.world.methodProfiler.a("ai");
+        if (this.c > 0) {
+            --this.c;
         }
 
-        this.e = this.F();
+        this.b = this.i();
         float f = 16.0F;
 
         if (this.target == null) {
@@ -29,32 +29,30 @@ public abstract class EntityCreature extends EntityLiving {
             if (this.target != null) {
                 this.pathEntity = this.world.findPath(this, this.target, f, true, false, false, true);
             }
-        } else if (!this.target.isAlive()) {
-            this.target = null;
-        } else {
-            float f1 = this.target.i(this);
+        } else if (this.target.isAlive()) {
+            float f1 = this.target.d((Entity) this);
 
-            if (this.h(this.target)) {
+            if (this.l(this.target)) {
                 this.a(this.target, f1);
-            } else {
-                this.b(this.target, f1);
             }
+        } else {
+            this.target = null;
         }
 
-        MethodProfiler.a();
-        if (!this.e && this.target != null && (this.pathEntity == null || this.random.nextInt(20) == 0)) {
+        this.world.methodProfiler.b();
+        if (!this.b && this.target != null && (this.pathEntity == null || this.random.nextInt(20) == 0)) {
             this.pathEntity = this.world.findPath(this, this.target, f, true, false, false, true);
-        } else if (!this.e && (this.pathEntity == null && this.random.nextInt(180) == 0 || this.random.nextInt(120) == 0 || this.f > 0) && this.aV < 100) {
-            this.G();
+        } else if (!this.b && (this.pathEntity == null && this.random.nextInt(180) == 0 || this.random.nextInt(120) == 0 || this.c > 0) && this.bq < 100) {
+            this.j();
         }
 
         int i = MathHelper.floor(this.boundingBox.b + 0.5D);
-        boolean flag = this.aU();
-        boolean flag1 = this.aV();
+        boolean flag = this.H();
+        boolean flag1 = this.J();
 
         this.pitch = 0.0F;
         if (this.pathEntity != null && this.random.nextInt(100) != 0) {
-            MethodProfiler.a("followpath");
+            this.world.methodProfiler.a("followpath");
             Vec3D vec3d = this.pathEntity.a((Entity) this);
             double d0 = (double) (this.width * 2.0F);
 
@@ -68,22 +66,15 @@ public abstract class EntityCreature extends EntityLiving {
                 }
             }
 
-            this.aZ = false;
+            this.bu = false;
             if (vec3d != null) {
                 double d1 = vec3d.a - this.locX;
                 double d2 = vec3d.c - this.locZ;
                 double d3 = vec3d.b - (double) i;
                 float f2 = (float) (Math.atan2(d2, d1) * 180.0D / 3.1415927410125732D) - 90.0F;
-                float f3 = f2 - this.yaw;
-
-                for (this.aX = this.bb; f3 < -180.0F; f3 += 360.0F) {
-                    ;
-                }
-
-                while (f3 >= 180.0F) {
-                    f3 -= 360.0F;
-                }
+                float f3 = MathHelper.g(f2 - this.yaw);
 
+                this.bs = this.bw;
                 if (f3 > 30.0F) {
                     f3 = 30.0F;
                 }
@@ -93,19 +84,19 @@ public abstract class EntityCreature extends EntityLiving {
                 }
 
                 this.yaw += f3;
-                if (this.e && this.target != null) {
+                if (this.b && this.target != null) {
                     double d4 = this.target.locX - this.locX;
                     double d5 = this.target.locZ - this.locZ;
                     float f4 = this.yaw;
 
                     this.yaw = (float) (Math.atan2(d5, d4) * 180.0D / 3.1415927410125732D) - 90.0F;
                     f3 = (f4 - this.yaw + 90.0F) * 3.1415927F / 180.0F;
-                    this.aW = -MathHelper.sin(f3) * this.aX * 1.0F;
-                    this.aX = MathHelper.cos(f3) * this.aX * 1.0F;
+                    this.br = -MathHelper.sin(f3) * this.bs * 1.0F;
+                    this.bs = MathHelper.cos(f3) * this.bs * 1.0F;
                 }
 
                 if (d3 > 0.0D) {
-                    this.aZ = true;
+                    this.bu = true;
                 }
             }
 
@@ -113,23 +104,23 @@ public abstract class EntityCreature extends EntityLiving {
                 this.a(this.target, 30.0F, 30.0F);
             }
 
-            if (this.positionChanged && !this.H()) {
-                this.aZ = true;
+            if (this.positionChanged && !this.l()) {
+                this.bu = true;
             }
 
             if (this.random.nextFloat() < 0.8F && (flag || flag1)) {
-                this.aZ = true;
+                this.bu = true;
             }
 
-            MethodProfiler.a();
+            this.world.methodProfiler.b();
         } else {
-            super.d_();
+            super.be();
             this.pathEntity = null;
         }
     }
 
-    protected void G() {
-        MethodProfiler.a("stroll");
+    protected void j() {
+        this.world.methodProfiler.a("stroll");
         boolean flag = false;
         int i = -1;
         int j = -1;
@@ -155,13 +146,11 @@ public abstract class EntityCreature extends EntityLiving {
             this.pathEntity = this.world.a(this, i, j, k, 10.0F, true, false, false, true);
         }
 
-        MethodProfiler.a();
+        this.world.methodProfiler.b();
     }
 
     protected void a(Entity entity, float f) {}
 
-    protected void b(Entity entity, float f) {}
-
     public float a(int i, int j, int k) {
         return 0.0F;
     }
@@ -178,7 +167,7 @@ public abstract class EntityCreature extends EntityLiving {
         return super.canSpawn() && this.a(i, j, k) >= 0.0F;
     }
 
-    public boolean H() {
+    public boolean l() {
         return this.pathEntity != null;
     }
 
@@ -186,7 +175,7 @@ public abstract class EntityCreature extends EntityLiving {
         this.pathEntity = pathentity;
     }
 
-    public Entity I() {
+    public Entity m() {
         return this.target;
     }
 
@@ -194,13 +183,13 @@ public abstract class EntityCreature extends EntityLiving {
         this.target = entity;
     }
 
-    protected float J() {
-        if (this.c_()) {
+    protected float bs() {
+        if (this.aV()) {
             return 1.0F;
         } else {
-            float f = super.J();
+            float f = super.bs();
 
-            if (this.f > 0) {
+            if (this.c > 0) {
                 f *= 2.0F;
             }
 
