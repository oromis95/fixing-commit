@@ -8,7 +8,7 @@ public class PathfinderGoalNearestAttackableTarget extends PathfinderGoalTarget
 
     EntityLiving a;
     Class b;
-    int f;
+    int c;
     private DistanceComparator g;
 
     public PathfinderGoalNearestAttackableTarget(EntityLiving entityliving, Class oclass, float f, int i, boolean flag) {
@@ -18,25 +18,25 @@ public class PathfinderGoalNearestAttackableTarget extends PathfinderGoalTarget
     public PathfinderGoalNearestAttackableTarget(EntityLiving entityliving, Class oclass, float f, int i, boolean flag, boolean flag1) {
         super(entityliving, f, flag, flag1);
         this.b = oclass;
-        this.d = f;
-        this.f = i;
+        this.e = f;
+        this.c = i;
         this.g = new DistanceComparator(this, entityliving);
         this.a(1);
     }
 
     public boolean a() {
-        if (this.f > 0 && this.c.an().nextInt(this.f) != 0) {
+        if (this.c > 0 && this.d.au().nextInt(this.c) != 0) {
             return false;
         } else {
             if (this.b == EntityHuman.class) {
-                EntityHuman entityhuman = this.c.world.findNearbyVulnerablePlayer(this.c, (double) this.d);
+                EntityHuman entityhuman = this.d.world.findNearbyVulnerablePlayer(this.d, (double) this.e);
 
                 if (this.a(entityhuman, false)) {
                     this.a = entityhuman;
                     return true;
                 }
             } else {
-                List list = this.c.world.a(this.b, this.c.boundingBox.grow((double) this.d, 4.0D, (double) this.d));
+                List list = this.d.world.a(this.b, this.d.boundingBox.grow((double) this.e, 4.0D, (double) this.e));
 
                 Collections.sort(list, this.g);
                 Iterator iterator = list.iterator();
@@ -56,8 +56,8 @@ public class PathfinderGoalNearestAttackableTarget extends PathfinderGoalTarget
         }
     }
 
-    public void c() {
-        this.c.b(this.a);
-        super.c();
+    public void e() {
+        this.d.b(this.a);
+        super.e();
     }
 }
