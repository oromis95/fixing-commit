@@ -1,5 +1,7 @@
 package net.minecraft.server;
 
+import java.util.Iterator;
+
 public class EntityItem extends Entity {
 
     public ItemStack itemStack;
@@ -10,7 +12,7 @@ public class EntityItem extends Entity {
 
     public EntityItem(World world, double d0, double d1, double d2, ItemStack itemstack) {
         super(world);
-        this.b(0.25F, 0.25F);
+        this.a(0.25F, 0.25F);
         this.height = this.length / 2.0F;
         this.setPosition(d0, d1, d2);
         this.itemStack = itemstack;
@@ -20,20 +22,20 @@ public class EntityItem extends Entity {
         this.motZ = (double) ((float) (Math.random() * 0.20000000298023224D - 0.10000000149011612D));
     }
 
-    protected boolean g_() {
+    protected boolean e_() {
         return false;
     }
 
     public EntityItem(World world) {
         super(world);
-        this.b(0.25F, 0.25F);
+        this.a(0.25F, 0.25F);
         this.height = this.length / 2.0F;
     }
 
-    protected void b() {}
+    protected void a() {}
 
-    public void F_() {
-        super.F_();
+    public void h_() {
+        super.h_();
         if (this.pickupDelay > 0) {
             --this.pickupDelay;
         }
@@ -42,15 +44,29 @@ public class EntityItem extends Entity {
         this.lastY = this.locY;
         this.lastZ = this.locZ;
         this.motY -= 0.03999999910593033D;
-        if (this.world.getMaterial(MathHelper.floor(this.locX), MathHelper.floor(this.locY), MathHelper.floor(this.locZ)) == Material.LAVA) {
-            this.motY = 0.20000000298023224D;
-            this.motX = (double) ((this.random.nextFloat() - this.random.nextFloat()) * 0.2F);
-            this.motZ = (double) ((this.random.nextFloat() - this.random.nextFloat()) * 0.2F);
-            this.world.makeSound(this, "random.fizz", 0.4F, 2.0F + this.random.nextFloat() * 0.4F);
+        this.i(this.locX, (this.boundingBox.b + this.boundingBox.e) / 2.0D, this.locZ);
+        this.move(this.motX, this.motY, this.motZ);
+        boolean flag = (int) this.lastX != (int) this.locX || (int) this.lastY != (int) this.locY || (int) this.lastZ != (int) this.locZ;
+
+        if (flag) {
+            if (this.world.getMaterial(MathHelper.floor(this.locX), MathHelper.floor(this.locY), MathHelper.floor(this.locZ)) == Material.LAVA) {
+                this.motY = 0.20000000298023224D;
+                this.motX = (double) ((this.random.nextFloat() - this.random.nextFloat()) * 0.2F);
+                this.motZ = (double) ((this.random.nextFloat() - this.random.nextFloat()) * 0.2F);
+                this.world.makeSound(this, "random.fizz", 0.4F, 2.0F + this.random.nextFloat() * 0.4F);
+            }
+
+            if (!this.world.isStatic) {
+                Iterator iterator = this.world.a(EntityItem.class, this.boundingBox.grow(0.5D, 0.0D, 0.5D)).iterator();
+
+                while (iterator.hasNext()) {
+                    EntityItem entityitem = (EntityItem) iterator.next();
+
+                    this.a(entityitem);
+                }
+            }
         }
 
-        this.g(this.locX, (this.boundingBox.b + this.boundingBox.e) / 2.0D, this.locZ);
-        this.move(this.motX, this.motY, this.motZ);
         float f = 0.98F;
 
         if (this.onGround) {
@@ -75,11 +91,35 @@ public class EntityItem extends Entity {
         }
     }
 
-    public void k() {
+    public boolean a(EntityItem entityitem) {
+        if (entityitem == this) {
+            return false;
+        } else if (entityitem.isAlive() && this.isAlive()) {
+            if (entityitem.itemStack.getItem() != this.itemStack.getItem()) {
+                return false;
+            } else if (entityitem.itemStack.getItem().k() && entityitem.itemStack.getData() != this.itemStack.getData()) {
+                return false;
+            } else if (entityitem.itemStack.count < this.itemStack.count) {
+                return entityitem.a(this);
+            } else if (entityitem.itemStack.count + this.itemStack.count > entityitem.itemStack.getMaxStackSize()) {
+                return false;
+            } else {
+                entityitem.itemStack.count += this.itemStack.count;
+                entityitem.pickupDelay = Math.max(entityitem.pickupDelay, this.pickupDelay);
+                entityitem.age = Math.min(entityitem.age, this.age);
+                this.die();
+                return true;
+            }
+        } else {
+            return false;
+        }
+    }
+
+    public void d() {
         this.age = 4800;
     }
 
-    public boolean h_() {
+    public boolean I() {
         return this.world.a(this.boundingBox, Material.WATER, this);
     }
 
@@ -88,7 +128,7 @@ public class EntityItem extends Entity {
     }
 
     public boolean damageEntity(DamageSource damagesource, int i) {
-        this.aW();
+        this.K();
         this.e -= i;
         if (this.e <= 0) {
             this.die();
@@ -100,7 +140,9 @@ public class EntityItem extends Entity {
     public void b(NBTTagCompound nbttagcompound) {
         nbttagcompound.setShort("Health", (short) ((byte) this.e));
         nbttagcompound.setShort("Age", (short) this.age);
-        nbttagcompound.setCompound("Item", this.itemStack.save(new NBTTagCompound()));
+        if (this.itemStack != null) {
+            nbttagcompound.setCompound("Item", this.itemStack.save(new NBTTagCompound()));
+        }
     }
 
     public void a(NBTTagCompound nbttagcompound) {
@@ -114,7 +156,7 @@ public class EntityItem extends Entity {
         }
     }
 
-    public void a_(EntityHuman entityhuman) {
+    public void b_(EntityHuman entityhuman) {
         if (!this.world.isStatic) {
             int i = this.itemStack.count;
 
@@ -145,10 +187,10 @@ public class EntityItem extends Entity {
     }
 
     public String getLocalizedName() {
-        return LocaleI18n.get("item." + this.itemStack.k());
+        return LocaleI18n.get("item." + this.itemStack.a());
     }
 
-    public boolean k_() {
+    public boolean an() {
         return false;
     }
 }
