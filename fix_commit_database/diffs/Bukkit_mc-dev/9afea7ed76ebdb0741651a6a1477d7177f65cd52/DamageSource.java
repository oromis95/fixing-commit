@@ -12,13 +12,15 @@ public class DamageSource {
     public static DamageSource FALL = (new DamageSource("fall")).h();
     public static DamageSource OUT_OF_WORLD = (new DamageSource("outOfWorld")).h().i();
     public static DamageSource GENERIC = (new DamageSource("generic")).h();
-    public static DamageSource EXPLOSION = new DamageSource("explosion");
+    public static DamageSource EXPLOSION = (new DamageSource("explosion")).m();
+    public static DamageSource EXPLOSION2 = new DamageSource("explosion");
     public static DamageSource MAGIC = (new DamageSource("magic")).h();
-    private boolean a = false;
     private boolean o = false;
-    private float p = 0.3F;
-    private boolean q;
+    private boolean p = false;
+    private float q = 0.3F;
     private boolean r;
+    private boolean s;
+    private boolean t;
     public String translationIndex;
 
     public static DamageSource mobAttack(EntityLiving entityliving) {
@@ -30,47 +32,47 @@ public class DamageSource {
     }
 
     public static DamageSource arrow(EntityArrow entityarrow, Entity entity) {
-        return (new EntityDamageSourceIndirect("arrow", entityarrow, entity)).d();
+        return (new EntityDamageSourceIndirect("arrow", entityarrow, entity)).b();
     }
 
     public static DamageSource fireball(EntityFireball entityfireball, Entity entity) {
-        return (new EntityDamageSourceIndirect("fireball", entityfireball, entity)).j().d();
+        return entity == null ? (new EntityDamageSourceIndirect("onFire", entityfireball, entityfireball)).j().b() : (new EntityDamageSourceIndirect("fireball", entityfireball, entity)).j().b();
     }
 
     public static DamageSource projectile(Entity entity, Entity entity1) {
-        return (new EntityDamageSourceIndirect("thrown", entity, entity1)).d();
+        return (new EntityDamageSourceIndirect("thrown", entity, entity1)).b();
     }
 
     public static DamageSource b(Entity entity, Entity entity1) {
         return (new EntityDamageSourceIndirect("indirectMagic", entity, entity1)).h();
     }
 
-    public boolean c() {
-        return this.r;
+    public boolean a() {
+        return this.s;
     }
 
-    public DamageSource d() {
-        this.r = true;
+    public DamageSource b() {
+        this.s = true;
         return this;
     }
 
     public boolean ignoresArmor() {
-        return this.a;
+        return this.o;
     }
 
-    public float f() {
-        return this.p;
+    public float d() {
+        return this.q;
     }
 
     public boolean ignoresInvulnerability() {
-        return this.o;
+        return this.p;
     }
 
     protected DamageSource(String s) {
         this.translationIndex = s;
     }
 
-    public Entity b() {
+    public Entity f() {
         return this.getEntity();
     }
 
@@ -79,18 +81,18 @@ public class DamageSource {
     }
 
     protected DamageSource h() {
-        this.a = true;
-        this.p = 0.0F;
+        this.o = true;
+        this.q = 0.0F;
         return this;
     }
 
     protected DamageSource i() {
-        this.o = true;
+        this.p = true;
         return this;
     }
 
     protected DamageSource j() {
-        this.q = true;
+        this.r = true;
         return this;
     }
 
@@ -99,10 +101,19 @@ public class DamageSource {
     }
 
     public boolean k() {
-        return this.q;
+        return this.r;
     }
 
     public String l() {
         return this.translationIndex;
     }
+
+    public DamageSource m() {
+        this.t = true;
+        return this;
+    }
+
+    public boolean n() {
+        return this.t;
+    }
 }
