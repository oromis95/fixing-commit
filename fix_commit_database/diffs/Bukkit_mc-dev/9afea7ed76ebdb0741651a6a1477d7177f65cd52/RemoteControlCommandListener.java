@@ -7,19 +7,27 @@ public class RemoteControlCommandListener implements ICommandListener {
 
     public RemoteControlCommandListener() {}
 
-    public void a() {
+    public void b() {
         this.b.setLength(0);
     }
 
-    public String b() {
+    public String c() {
         return this.b.toString();
     }
 
+    public String getName() {
+        return "Rcon";
+    }
+
     public void sendMessage(String s) {
         this.b.append(s);
     }
 
-    public String getName() {
-        return "Rcon";
+    public boolean b(String s) {
+        return true;
+    }
+
+    public String a(String s, Object... aobject) {
+        return LocaleLanguage.a().a(s, aobject);
     }
 }
