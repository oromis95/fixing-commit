@@ -136,6 +136,17 @@ public class WorldGenTrees extends WorldGenerator {
                                 }
                             }
                         }
+
+                        if (random.nextInt(5) == 0 && l > 5) {
+                            for (j1 = 0; j1 < 2; ++j1) {
+                                for (k1 = 0; k1 < 4; ++k1) {
+                                    if (random.nextInt(4 - j1) == 0) {
+                                        i2 = random.nextInt(3);
+                                        this.setTypeAndData(world, i + Direction.a[Direction.e[k1]], j + l - 5 + j1, k + Direction.b[Direction.e[k1]], Block.COCOA.id, i2 << 2 | k1);
+                                    }
+                                }
+                            }
+                        }
                     }
 
                     return true;
