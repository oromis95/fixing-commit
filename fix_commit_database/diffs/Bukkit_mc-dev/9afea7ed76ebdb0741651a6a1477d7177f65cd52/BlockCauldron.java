@@ -1,6 +1,6 @@
 package net.minecraft.server;
 
-import java.util.ArrayList;
+import java.util.List;
 import java.util.Random;
 
 public class BlockCauldron extends Block {
@@ -14,19 +14,19 @@ public class BlockCauldron extends Block {
         return i == 1 ? 138 : (i == 0 ? 155 : 154);
     }
 
-    public void a(World world, int i, int j, int k, AxisAlignedBB axisalignedbb, ArrayList arraylist) {
+    public void a(World world, int i, int j, int k, AxisAlignedBB axisalignedbb, List list, Entity entity) {
         this.a(0.0F, 0.0F, 0.0F, 1.0F, 0.3125F, 1.0F);
-        super.a(world, i, j, k, axisalignedbb, arraylist);
+        super.a(world, i, j, k, axisalignedbb, list, entity);
         float f = 0.125F;
 
         this.a(0.0F, 0.0F, 0.0F, f, 1.0F, 1.0F);
-        super.a(world, i, j, k, axisalignedbb, arraylist);
+        super.a(world, i, j, k, axisalignedbb, list, entity);
         this.a(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, f);
-        super.a(world, i, j, k, axisalignedbb, arraylist);
+        super.a(world, i, j, k, axisalignedbb, list, entity);
         this.a(1.0F - f, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
-        super.a(world, i, j, k, axisalignedbb, arraylist);
+        super.a(world, i, j, k, axisalignedbb, list, entity);
         this.a(0.0F, 0.0F, 1.0F - f, 1.0F, 1.0F, 1.0F);
-        super.a(world, i, j, k, axisalignedbb, arraylist);
+        super.a(world, i, j, k, axisalignedbb, list, entity);
         this.f();
     }
 
@@ -34,19 +34,19 @@ public class BlockCauldron extends Block {
         this.a(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
     }
 
-    public boolean a() {
+    public boolean d() {
         return false;
     }
 
-    public int c() {
+    public int b() {
         return 24;
     }
 
-    public boolean b() {
+    public boolean c() {
         return false;
     }
 
-    public boolean interact(World world, int i, int j, int k, EntityHuman entityhuman) {
+    public boolean interact(World world, int i, int j, int k, EntityHuman entityhuman, int l, float f, float f1, float f2) {
         if (world.isStatic) {
             return true;
         } else {
@@ -55,10 +55,10 @@ public class BlockCauldron extends Block {
             if (itemstack == null) {
                 return true;
             } else {
-                int l = world.getData(i, j, k);
+                int i1 = world.getData(i, j, k);
 
                 if (itemstack.id == Item.WATER_BUCKET.id) {
-                    if (l < 3) {
+                    if (i1 < 3) {
                         if (!entityhuman.abilities.canInstantlyBuild) {
                             entityhuman.inventory.setItem(entityhuman.inventory.itemInHandIndex, new ItemStack(Item.BUCKET));
                         }
@@ -68,11 +68,13 @@ public class BlockCauldron extends Block {
 
                     return true;
                 } else {
-                    if (itemstack.id == Item.GLASS_BOTTLE.id && l > 0) {
+                    if (itemstack.id == Item.GLASS_BOTTLE.id && i1 > 0) {
                         ItemStack itemstack1 = new ItemStack(Item.POTION, 1, 0);
 
                         if (!entityhuman.inventory.pickup(itemstack1)) {
                             world.addEntity(new EntityItem(world, (double) i + 0.5D, (double) j + 1.5D, (double) k + 0.5D, itemstack1));
+                        } else if (entityhuman instanceof EntityPlayer) {
+                            ((EntityPlayer) entityhuman).updateInventory(entityhuman.defaultContainer);
                         }
 
                         --itemstack.count;
@@ -80,7 +82,7 @@ public class BlockCauldron extends Block {
                             entityhuman.inventory.setItem(entityhuman.inventory.itemInHandIndex, (ItemStack) null);
                         }
 
-                        world.setData(i, j, k, l - 1);
+                        world.setData(i, j, k, i1 - 1);
                     }
 
                     return true;
@@ -89,6 +91,16 @@ public class BlockCauldron extends Block {
         }
     }
 
+    public void f(World world, int i, int j, int k) {
+        if (world.random.nextInt(20) == 1) {
+            int l = world.getData(i, j, k);
+
+            if (l < 3) {
+                world.setData(i, j, k, l + 1);
+            }
+        }
+    }
+
     public int getDropType(int i, Random random, int j) {
         return Item.CAULDRON.id;
     }
