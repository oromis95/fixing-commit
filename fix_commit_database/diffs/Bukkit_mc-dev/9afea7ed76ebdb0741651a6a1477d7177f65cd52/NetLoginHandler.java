@@ -1,35 +1,40 @@
 package net.minecraft.server;
 
+import java.net.InetAddress;
 import java.net.Socket;
-import java.util.Iterator;
+import java.security.PrivateKey;
+import java.security.PublicKey;
+import java.util.Arrays;
 import java.util.Random;
 import java.util.logging.Logger;
+import javax.crypto.SecretKey;
 
 public class NetLoginHandler extends NetHandler {
 
+    private byte[] d;
     public static Logger logger = Logger.getLogger("Minecraft");
     private static Random random = new Random();
     public NetworkManager networkManager;
     public boolean c = false;
     private MinecraftServer server;
-    private int f = 0;
-    private String g = null;
-    private Packet1Login h = null;
+    private int g = 0;
+    private String h = null;
+    private volatile boolean i = false;
     private String loginKey = "";
+    private SecretKey k = null;
 
     public NetLoginHandler(MinecraftServer minecraftserver, Socket socket, String s) {
         this.server = minecraftserver;
-        this.networkManager = new NetworkManager(socket, s, this);
-        this.networkManager.f = 0;
+        this.networkManager = new NetworkManager(socket, s, this, minecraftserver.E().getPrivate());
+        this.networkManager.e = 0;
     }
 
-    public void a() {
-        if (this.h != null) {
-            this.b(this.h);
-            this.h = null;
+    public void c() {
+        if (this.i) {
+            this.d();
         }
 
-        if (this.f++ == 600) {
+        if (this.g++ == 600) {
             this.disconnect("Took too long to log in");
         } else {
             this.networkManager.b();
@@ -48,63 +53,61 @@ public class NetLoginHandler extends NetHandler {
     }
 
     public void a(Packet2Handshake packet2handshake) {
-        if (this.server.onlineMode) {
-            this.loginKey = Long.toString(random.nextLong(), 16);
-            this.networkManager.queue(new Packet2Handshake(this.loginKey));
+        this.h = packet2handshake.f();
+        if (!this.h.equals(StripColor.a(this.h))) {
+            this.disconnect("Invalid username!");
         } else {
-            this.networkManager.queue(new Packet2Handshake("-"));
+            PublicKey publickey = this.server.E().getPublic();
+
+            if (packet2handshake.d() != 39) {
+                if (packet2handshake.d() > 39) {
+                    this.disconnect("Outdated server!");
+                } else {
+                    this.disconnect("Outdated client!");
+                }
+            } else {
+                this.loginKey = this.server.getOnlineMode() ? Long.toString(random.nextLong(), 16) : "-";
+                this.d = new byte[4];
+                random.nextBytes(this.d);
+                this.networkManager.queue(new Packet253KeyRequest(this.loginKey, publickey, this.d));
+            }
         }
     }
 
-    public void a(Packet1Login packet1login) {
-        this.g = packet1login.name;
-        if (packet1login.a != 29) {
-            if (packet1login.a > 29) {
-                this.disconnect("Outdated server!");
-            } else {
-                this.disconnect("Outdated client!");
-            }
-        } else {
-            if (!this.server.onlineMode) {
-                this.b(packet1login);
+    public void a(Packet252KeyResponse packet252keyresponse) {
+        PrivateKey privatekey = this.server.E().getPrivate();
+
+        this.k = packet252keyresponse.a(privatekey);
+        if (!Arrays.equals(this.d, packet252keyresponse.b(privatekey))) {
+            this.disconnect("Invalid client reply");
+        }
+
+        this.networkManager.queue(new Packet252KeyResponse());
+    }
+
+    public void a(Packet205ClientCommand packet205clientcommand) {
+        if (packet205clientcommand.a == 0) {
+            if (this.server.getOnlineMode()) {
+                (new ThreadLoginVerifier(this)).start();
             } else {
-                (new ThreadLoginVerifier(this, packet1login)).start();
+                this.i = true;
             }
         }
     }
 
-    public void b(Packet1Login packet1login) {
-        EntityPlayer entityplayer = this.server.serverConfigurationManager.attemptLogin(this, packet1login.name);
-
-        if (entityplayer != null) {
-            this.server.serverConfigurationManager.b(entityplayer);
-            entityplayer.spawnIn(this.server.getWorldServer(entityplayer.dimension));
-            entityplayer.itemInWorldManager.a((WorldServer) entityplayer.world);
-            logger.info(this.getName() + " logged in with entity id " + entityplayer.id + " at (" + entityplayer.locX + ", " + entityplayer.locY + ", " + entityplayer.locZ + ")");
-            WorldServer worldserver = this.server.getWorldServer(entityplayer.dimension);
-            ChunkCoordinates chunkcoordinates = worldserver.getSpawn();
-
-            entityplayer.itemInWorldManager.b(worldserver.getWorldData().getGameType());
-            NetServerHandler netserverhandler = new NetServerHandler(this.server, this.networkManager, entityplayer);
+    public void a(Packet1Login packet1login) {}
 
-            netserverhandler.sendPacket(new Packet1Login("", entityplayer.id, worldserver.getWorldData().getType(), entityplayer.itemInWorldManager.getGameMode(), worldserver.worldProvider.dimension, (byte) worldserver.difficulty, (byte) worldserver.getHeight(), (byte) this.server.serverConfigurationManager.getMaxPlayers()));
-            netserverhandler.sendPacket(new Packet6SpawnPosition(chunkcoordinates.x, chunkcoordinates.y, chunkcoordinates.z));
-            netserverhandler.sendPacket(new Packet202Abilities(entityplayer.abilities));
-            this.server.serverConfigurationManager.a(entityplayer, worldserver);
-            this.server.serverConfigurationManager.sendAll(new Packet3Chat("\u00A7e" + entityplayer.name + " joined the game."));
-            this.server.serverConfigurationManager.c(entityplayer);
-            netserverhandler.a(entityplayer.locX, entityplayer.locY, entityplayer.locZ, entityplayer.yaw, entityplayer.pitch);
-            this.server.networkListenThread.a(netserverhandler);
-            netserverhandler.sendPacket(new Packet4UpdateTime(worldserver.getTime()));
-            Iterator iterator = entityplayer.getEffects().iterator();
+    public void d() {
+        String s = this.server.getServerConfigurationManager().attemptLogin(this.networkManager.getSocketAddress(), this.h);
 
-            while (iterator.hasNext()) {
-                MobEffect mobeffect = (MobEffect) iterator.next();
+        if (s != null) {
+            this.disconnect(s);
+        } else {
+            EntityPlayer entityplayer = this.server.getServerConfigurationManager().processLogin(this.h);
 
-                netserverhandler.sendPacket(new Packet41MobEffect(entityplayer.id, mobeffect));
+            if (entityplayer != null) {
+                this.server.getServerConfigurationManager().a((INetworkManager) this.networkManager, entityplayer);
             }
-
-            entityplayer.syncInventory();
         }
 
         this.c = true;
@@ -117,11 +120,19 @@ public class NetLoginHandler extends NetHandler {
 
     public void a(Packet254GetInfo packet254getinfo) {
         try {
-            String s = this.server.motd + "\u00A7" + this.server.serverConfigurationManager.getPlayerCount() + "\u00A7" + this.server.serverConfigurationManager.getMaxPlayers();
+            String s = this.server.getMotd() + "\u00A7" + this.server.getServerConfigurationManager().getPlayerCount() + "\u00A7" + this.server.getServerConfigurationManager().getMaxPlayers();
+            InetAddress inetaddress = null;
+
+            if (this.networkManager.getSocket() != null) {
+                inetaddress = this.networkManager.getSocket().getInetAddress();
+            }
 
             this.networkManager.queue(new Packet255KickDisconnect(s));
             this.networkManager.d();
-            this.server.networkListenThread.a(this.networkManager.getSocket());
+            if (inetaddress != null && this.server.ac() instanceof DedicatedServerConnection) {
+                ((DedicatedServerConnection) this.server.ac()).a(inetaddress);
+            }
+
             this.c = true;
         } catch (Exception exception) {
             exception.printStackTrace();
@@ -133,10 +144,10 @@ public class NetLoginHandler extends NetHandler {
     }
 
     public String getName() {
-        return this.g != null ? this.g + " [" + this.networkManager.getSocketAddress().toString() + "]" : this.networkManager.getSocketAddress().toString();
+        return this.h != null ? this.h + " [" + this.networkManager.getSocketAddress().toString() + "]" : this.networkManager.getSocketAddress().toString();
     }
 
-    public boolean c() {
+    public boolean a() {
         return true;
     }
 
@@ -144,7 +155,19 @@ public class NetLoginHandler extends NetHandler {
         return netloginhandler.loginKey;
     }
 
-    static Packet1Login a(NetLoginHandler netloginhandler, Packet1Login packet1login) {
-        return netloginhandler.h = packet1login;
+    static MinecraftServer b(NetLoginHandler netloginhandler) {
+        return netloginhandler.server;
+    }
+
+    static SecretKey c(NetLoginHandler netloginhandler) {
+        return netloginhandler.k;
+    }
+
+    static String d(NetLoginHandler netloginhandler) {
+        return netloginhandler.h;
+    }
+
+    static boolean a(NetLoginHandler netloginhandler, boolean flag) {
+        return netloginhandler.i = flag;
     }
 }
