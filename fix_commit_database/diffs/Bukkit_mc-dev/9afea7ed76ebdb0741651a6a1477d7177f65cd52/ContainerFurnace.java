@@ -1,17 +1,19 @@
 package net.minecraft.server;
 
+import java.util.Iterator;
+
 public class ContainerFurnace extends Container {
 
     private TileEntityFurnace furnace;
-    private int b = 0;
-    private int c = 0;
+    private int f = 0;
+    private int g = 0;
     private int h = 0;
 
     public ContainerFurnace(PlayerInventory playerinventory, TileEntityFurnace tileentityfurnace) {
         this.furnace = tileentityfurnace;
         this.a(new Slot(tileentityfurnace, 0, 56, 17));
         this.a(new Slot(tileentityfurnace, 1, 56, 53));
-        this.a(new SlotResult2(playerinventory.player, tileentityfurnace, 2, 116, 35));
+        this.a(new SlotFurnaceResult(playerinventory.player, tileentityfurnace, 2, 116, 35));
 
         int i;
 
@@ -33,17 +35,18 @@ public class ContainerFurnace extends Container {
         icrafting.setContainerData(this, 2, this.furnace.ticksForCurrentFuel);
     }
 
-    public void a() {
-        super.a();
+    public void b() {
+        super.b();
+        Iterator iterator = this.listeners.iterator();
 
-        for (int i = 0; i < this.listeners.size(); ++i) {
-            ICrafting icrafting = (ICrafting) this.listeners.get(i);
+        while (iterator.hasNext()) {
+            ICrafting icrafting = (ICrafting) iterator.next();
 
-            if (this.b != this.furnace.cookTime) {
+            if (this.f != this.furnace.cookTime) {
                 icrafting.setContainerData(this, 0, this.furnace.cookTime);
             }
 
-            if (this.c != this.furnace.burnTime) {
+            if (this.g != this.furnace.burnTime) {
                 icrafting.setContainerData(this, 1, this.furnace.burnTime);
             }
 
@@ -52,20 +55,20 @@ public class ContainerFurnace extends Container {
             }
         }
 
-        this.b = this.furnace.cookTime;
-        this.c = this.furnace.burnTime;
+        this.f = this.furnace.cookTime;
+        this.g = this.furnace.burnTime;
         this.h = this.furnace.ticksForCurrentFuel;
     }
 
-    public boolean b(EntityHuman entityhuman) {
+    public boolean c(EntityHuman entityhuman) {
         return this.furnace.a(entityhuman);
     }
 
-    public ItemStack a(int i) {
+    public ItemStack b(int i) {
         ItemStack itemstack = null;
-        Slot slot = (Slot) this.e.get(i);
+        Slot slot = (Slot) this.b.get(i);
 
-        if (slot != null && slot.c()) {
+        if (slot != null && slot.d()) {
             ItemStack itemstack1 = slot.getItem();
 
             itemstack = itemstack1.cloneItemStack();
@@ -76,7 +79,7 @@ public class ContainerFurnace extends Container {
 
                 slot.a(itemstack1, itemstack);
             } else if (i != 1 && i != 0) {
-                if (FurnaceRecipes.getInstance().getResult(itemstack1.getItem().id) != null) {
+                if (RecipesFurnace.getInstance().getResult(itemstack1.getItem().id) != null) {
                     if (!this.a(itemstack1, 0, 1, false)) {
                         return null;
                     }
@@ -98,14 +101,14 @@ public class ContainerFurnace extends Container {
             if (itemstack1.count == 0) {
                 slot.set((ItemStack) null);
             } else {
-                slot.d();
+                slot.e();
             }
 
             if (itemstack1.count == itemstack.count) {
                 return null;
             }
 
-            slot.c(itemstack1);
+            slot.b(itemstack1);
         }
 
         return itemstack;
