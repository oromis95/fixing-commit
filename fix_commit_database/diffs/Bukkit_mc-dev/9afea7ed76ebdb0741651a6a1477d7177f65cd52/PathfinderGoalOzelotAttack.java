@@ -14,7 +14,7 @@ public class PathfinderGoalOzelotAttack extends PathfinderGoal {
     }
 
     public boolean a() {
-        EntityLiving entityliving = this.b.at();
+        EntityLiving entityliving = this.b.az();
 
         if (entityliving == null) {
             return false;
@@ -25,15 +25,15 @@ public class PathfinderGoalOzelotAttack extends PathfinderGoal {
     }
 
     public boolean b() {
-        return !this.c.isAlive() ? false : (this.b.j(this.c) > 225.0D ? false : !this.b.al().e() || this.a());
+        return !this.c.isAlive() ? false : (this.b.e(this.c) > 225.0D ? false : !this.b.getNavigation().f() || this.a());
     }
 
-    public void d() {
+    public void c() {
         this.c = null;
-        this.b.al().f();
+        this.b.getNavigation().g();
     }
 
-    public void e() {
+    public void d() {
         this.b.getControllerLook().a(this.c, 30.0F, 30.0F);
         double d0 = (double) (this.b.width * 2.0F * this.b.width * 2.0F);
         double d1 = this.b.e(this.c.locX, this.c.boundingBox.b, this.c.locZ);
@@ -45,12 +45,12 @@ public class PathfinderGoalOzelotAttack extends PathfinderGoal {
             f = 0.18F;
         }
 
-        this.b.al().a(this.c, f);
+        this.b.getNavigation().a(this.c, f);
         this.d = Math.max(this.d - 1, 0);
         if (d1 <= d0) {
             if (this.d <= 0) {
                 this.d = 20;
-                this.b.a((Entity) this.c);
+                this.b.k(this.c);
             }
         }
     }
