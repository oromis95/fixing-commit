@@ -11,4 +11,14 @@ public class ItemLeaves extends ItemBlock {
     public int filterData(int i) {
         return i | 4;
     }
+
+    public String c(ItemStack itemstack) {
+        int i = itemstack.getData();
+
+        if (i < 0 || i >= BlockLeaves.a.length) {
+            i = 0;
+        }
+
+        return super.getName() + "." + BlockLeaves.a[i];
+    }
 }
