@@ -2,24 +2,24 @@ package net.minecraft.server;
 
 public class EntitySkeleton extends EntityMonster {
 
-    private static final ItemStack a = new ItemStack(Item.BOW, 1);
+    private static final ItemStack d = new ItemStack(Item.BOW, 1);
 
     public EntitySkeleton(World world) {
         super(world);
         this.texture = "/mob/skeleton.png";
-        this.bb = 0.25F;
+        this.bw = 0.25F;
         this.goalSelector.a(1, new PathfinderGoalFloat(this));
         this.goalSelector.a(2, new PathfinderGoalRestrictSun(this));
-        this.goalSelector.a(3, new PathfinderGoalFleeSun(this, this.bb));
-        this.goalSelector.a(4, new PathfinderGoalArrowAttack(this, this.bb, 1, 60));
-        this.goalSelector.a(5, new PathfinderGoalRandomStroll(this, this.bb));
+        this.goalSelector.a(3, new PathfinderGoalFleeSun(this, this.bw));
+        this.goalSelector.a(4, new PathfinderGoalArrowAttack(this, this.bw, 1, 60));
+        this.goalSelector.a(5, new PathfinderGoalRandomStroll(this, this.bw));
         this.goalSelector.a(6, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 8.0F));
         this.goalSelector.a(6, new PathfinderGoalRandomLookaround(this));
         this.targetSelector.a(1, new PathfinderGoalHurtByTarget(this, false));
         this.targetSelector.a(2, new PathfinderGoalNearestAttackableTarget(this, EntityHuman.class, 16.0F, 0, true));
     }
 
-    public boolean c_() {
+    public boolean aV() {
         return true;
     }
 
@@ -27,37 +27,37 @@ public class EntitySkeleton extends EntityMonster {
         return 20;
     }
 
-    protected String i() {
+    protected String aQ() {
         return "mob.skeleton";
     }
 
-    protected String j() {
+    protected String aR() {
         return "mob.skeletonhurt";
     }
 
-    protected String k() {
+    protected String aS() {
         return "mob.skeletonhurt";
     }
 
-    public MonsterType getMonsterType() {
-        return MonsterType.UNDEAD;
+    public EnumMonsterType getMonsterType() {
+        return EnumMonsterType.UNDEAD;
     }
 
-    public void e() {
-        if (this.world.e() && !this.world.isStatic) {
-            float f = this.b(1.0F);
+    public void d() {
+        if (this.world.r() && !this.world.isStatic) {
+            float f = this.c(1.0F);
 
-            if (f > 0.5F && this.world.isChunkLoaded(MathHelper.floor(this.locX), MathHelper.floor(this.locY), MathHelper.floor(this.locZ)) && this.random.nextFloat() * 30.0F < (f - 0.4F) * 2.0F) {
+            if (f > 0.5F && this.world.j(MathHelper.floor(this.locX), MathHelper.floor(this.locY), MathHelper.floor(this.locZ)) && this.random.nextFloat() * 30.0F < (f - 0.4F) * 2.0F) {
                 this.setOnFire(8);
             }
         }
 
-        super.e();
+        super.d();
     }
 
     public void die(DamageSource damagesource) {
         super.die(damagesource);
-        if (damagesource.b() instanceof EntityArrow && damagesource.getEntity() instanceof EntityHuman) {
+        if (damagesource.f() instanceof EntityArrow && damagesource.getEntity() instanceof EntityHuman) {
             EntityHuman entityhuman = (EntityHuman) damagesource.getEntity();
             double d0 = entityhuman.locX - this.locX;
             double d1 = entityhuman.locZ - this.locZ;
@@ -88,7 +88,7 @@ public class EntitySkeleton extends EntityMonster {
         }
     }
 
-    protected void b(int i) {
+    protected void l(int i) {
         if (i > 0) {
             ItemStack itemstack = new ItemStack(Item.BOW);
 
