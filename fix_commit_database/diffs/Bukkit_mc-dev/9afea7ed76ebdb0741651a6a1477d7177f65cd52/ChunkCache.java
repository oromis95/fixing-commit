@@ -77,9 +77,9 @@ public class ChunkCache implements IBlockAccess {
         return l == 0 ? Material.AIR : Block.byId[l].material;
     }
 
-    public boolean e(int i, int j, int k) {
+    public boolean s(int i, int j, int k) {
         Block block = Block.byId[this.getTypeId(i, j, k)];
 
-        return block == null ? false : block.material.isSolid() && block.b();
+        return block == null ? false : block.material.isSolid() && block.c();
     }
 }
