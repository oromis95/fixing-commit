@@ -4,11 +4,11 @@ public class EnchantmentDigging extends Enchantment {
 
     protected EnchantmentDigging(int i, int j) {
         super(i, j, EnchantmentSlotType.DIGGER);
-        this.a("digging");
+        this.b("digging");
     }
 
     public int a(int i) {
-        return 1 + 15 * (i - 1);
+        return 1 + 10 * (i - 1);
     }
 
     public int b(int i) {
