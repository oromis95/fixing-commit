@@ -1,5 +1,7 @@
 package net.minecraft.server;
 
+import java.util.List;
+
 public class BlockWaterLily extends BlockFlower {
 
     protected BlockWaterLily(int i, int j) {
@@ -8,25 +10,28 @@ public class BlockWaterLily extends BlockFlower {
         float f1 = 0.015625F;
 
         this.a(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, f1, 0.5F + f);
+        this.a(CreativeModeTab.c);
     }
 
-    public int c() {
+    public int b() {
         return 23;
     }
 
-    public AxisAlignedBB e(World world, int i, int j, int k) {
-        return AxisAlignedBB.b((double) i + this.minX, (double) j + this.minY, (double) k + this.minZ, (double) i + this.maxX, (double) j + this.maxY, (double) k + this.maxZ);
+    public void a(World world, int i, int j, int k, AxisAlignedBB axisalignedbb, List list, Entity entity) {
+        if (entity == null || !(entity instanceof EntityBoat)) {
+            super.a(world, i, j, k, axisalignedbb, list, entity);
+        }
     }
 
-    public boolean canPlace(World world, int i, int j, int k) {
-        return super.canPlace(world, i, j, k);
+    public AxisAlignedBB e(World world, int i, int j, int k) {
+        return AxisAlignedBB.a().a((double) i + this.minX, (double) j + this.minY, (double) k + this.minZ, (double) i + this.maxX, (double) j + this.maxY, (double) k + this.maxZ);
     }
 
-    protected boolean d(int i) {
+    protected boolean d_(int i) {
         return i == Block.STATIONARY_WATER.id;
     }
 
-    public boolean f(World world, int i, int j, int k) {
+    public boolean d(World world, int i, int j, int k) {
         return j >= 0 && j < 256 ? world.getMaterial(i, j - 1, k) == Material.WATER && world.getData(i, j - 1, k) == 0 : false;
     }
 }
