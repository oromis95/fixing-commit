@@ -8,25 +8,28 @@ public class BlockSand extends Block {
 
     public BlockSand(int i, int j) {
         super(i, j, Material.SAND);
+        this.a(CreativeModeTab.b);
     }
 
     public void onPlace(World world, int i, int j, int k) {
-        world.c(i, j, k, this.id, this.d());
+        world.a(i, j, k, this.id, this.p_());
     }
 
     public void doPhysics(World world, int i, int j, int k, int l) {
-        world.c(i, j, k, this.id, this.d());
+        world.a(i, j, k, this.id, this.p_());
     }
 
-    public void a(World world, int i, int j, int k, Random random) {
-        this.h(world, i, j, k);
+    public void b(World world, int i, int j, int k, Random random) {
+        if (!world.isStatic) {
+            this.l(world, i, j, k);
+        }
     }
 
-    private void h(World world, int i, int j, int k) {
+    private void l(World world, int i, int j, int k) {
         if (canFall(world, i, j - 1, k) && j >= 0) {
             byte b0 = 32;
 
-            if (!instaFall && world.a(i - b0, j - b0, k - b0, i + b0, j + b0, k + b0)) {
+            if (!instaFall && world.c(i - b0, j - b0, k - b0, i + b0, j + b0, k + b0)) {
                 if (!world.isStatic) {
                     EntityFallingBlock entityfallingblock = new EntityFallingBlock(world, (double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), this.id);
 
@@ -46,7 +49,7 @@ public class BlockSand extends Block {
         }
     }
 
-    public int d() {
+    public int p_() {
         return 3;
     }
 
