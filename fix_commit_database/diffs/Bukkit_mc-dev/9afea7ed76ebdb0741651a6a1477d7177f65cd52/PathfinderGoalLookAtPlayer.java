@@ -2,15 +2,15 @@ package net.minecraft.server;
 
 public class PathfinderGoalLookAtPlayer extends PathfinderGoal {
 
-    private EntityLiving a;
-    private Entity b;
+    private EntityLiving b;
+    protected Entity a;
     private float c;
     private int d;
     private float e;
     private Class f;
 
     public PathfinderGoalLookAtPlayer(EntityLiving entityliving, Class oclass, float f) {
-        this.a = entityliving;
+        this.b = entityliving;
         this.f = oclass;
         this.c = f;
         this.e = 0.02F;
@@ -18,7 +18,7 @@ public class PathfinderGoalLookAtPlayer extends PathfinderGoal {
     }
 
     public PathfinderGoalLookAtPlayer(EntityLiving entityliving, Class oclass, float f, float f1) {
-        this.a = entityliving;
+        this.b = entityliving;
         this.f = oclass;
         this.c = f;
         this.e = f1;
@@ -26,33 +26,33 @@ public class PathfinderGoalLookAtPlayer extends PathfinderGoal {
     }
 
     public boolean a() {
-        if (this.a.an().nextFloat() >= this.e) {
+        if (this.b.au().nextFloat() >= this.e) {
             return false;
         } else {
             if (this.f == EntityHuman.class) {
-                this.b = this.a.world.findNearbyPlayer(this.a, (double) this.c);
+                this.a = this.b.world.findNearbyPlayer(this.b, (double) this.c);
             } else {
-                this.b = this.a.world.a(this.f, this.a.boundingBox.grow((double) this.c, 3.0D, (double) this.c), this.a);
+                this.a = this.b.world.a(this.f, this.b.boundingBox.grow((double) this.c, 3.0D, (double) this.c), this.b);
             }
 
-            return this.b != null;
+            return this.a != null;
         }
     }
 
     public boolean b() {
-        return !this.b.isAlive() ? false : (this.a.j(this.b) > (double) (this.c * this.c) ? false : this.d > 0);
+        return !this.a.isAlive() ? false : (this.b.e(this.a) > (double) (this.c * this.c) ? false : this.d > 0);
     }
 
-    public void c() {
-        this.d = 40 + this.a.an().nextInt(40);
+    public void e() {
+        this.d = 40 + this.b.au().nextInt(40);
     }
 
-    public void d() {
-        this.b = null;
+    public void c() {
+        this.a = null;
     }
 
-    public void e() {
-        this.a.getControllerLook().a(this.b.locX, this.b.locY + (double) this.b.getHeadHeight(), this.b.locZ, 10.0F, (float) this.a.D());
+    public void d() {
+        this.b.getControllerLook().a(this.a.locX, this.a.locY + (double) this.a.getHeadHeight(), this.a.locZ, 10.0F, (float) this.b.bf());
         --this.d;
     }
 }
