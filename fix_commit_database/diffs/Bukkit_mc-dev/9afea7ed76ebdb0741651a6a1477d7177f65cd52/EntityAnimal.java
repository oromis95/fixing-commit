@@ -1,26 +1,27 @@
 package net.minecraft.server;
 
+import java.util.Iterator;
 import java.util.List;
 
 public abstract class EntityAnimal extends EntityAgeable implements IAnimal {
 
     private int love;
-    private int b = 0;
+    private int e = 0;
 
     public EntityAnimal(World world) {
         super(world);
     }
 
-    protected void g() {
+    protected void bd() {
         if (this.getAge() != 0) {
             this.love = 0;
         }
 
-        super.g();
+        super.bd();
     }
 
-    public void e() {
-        super.e();
+    public void d() {
+        super.d();
         if (this.getAge() != 0) {
             this.love = 0;
         }
@@ -37,7 +38,7 @@ public abstract class EntityAnimal extends EntityAgeable implements IAnimal {
                 this.world.a(s, this.locX + (double) (this.random.nextFloat() * this.width * 2.0F) - (double) this.width, this.locY + 0.5D + (double) (this.random.nextFloat() * this.length), this.locZ + (double) (this.random.nextFloat() * this.width * 2.0F) - (double) this.width, d0, d1, d2);
             }
         } else {
-            this.b = 0;
+            this.e = 0;
         }
     }
 
@@ -48,12 +49,12 @@ public abstract class EntityAnimal extends EntityAgeable implements IAnimal {
                 double d1 = entity.locZ - this.locZ;
 
                 this.yaw = (float) (Math.atan2(d1, d0) * 180.0D / 3.1415927410125732D) - 90.0F;
-                this.e = true;
+                this.b = true;
             }
 
             EntityHuman entityhuman = (EntityHuman) entity;
 
-            if (entityhuman.U() == null || !this.a(entityhuman.U())) {
+            if (entityhuman.bC() == null || !this.b(entityhuman.bC())) {
                 this.target = null;
             }
         } else if (entity instanceof EntityAnimal) {
@@ -61,7 +62,7 @@ public abstract class EntityAnimal extends EntityAgeable implements IAnimal {
 
             if (this.getAge() > 0 && entityanimal.getAge() < 0) {
                 if ((double) f < 2.5D) {
-                    this.e = true;
+                    this.b = true;
                 }
             } else if (this.love > 0 && entityanimal.love > 0) {
                 if (entityanimal.target == null) {
@@ -71,19 +72,19 @@ public abstract class EntityAnimal extends EntityAgeable implements IAnimal {
                 if (entityanimal.target == this && (double) f < 3.5D) {
                     ++entityanimal.love;
                     ++this.love;
-                    ++this.b;
-                    if (this.b % 4 == 0) {
+                    ++this.e;
+                    if (this.e % 4 == 0) {
                         this.world.a("heart", this.locX + (double) (this.random.nextFloat() * this.width * 2.0F) - (double) this.width, this.locY + 0.5D + (double) (this.random.nextFloat() * this.length), this.locZ + (double) (this.random.nextFloat() * this.width * 2.0F) - (double) this.width, 0.0D, 0.0D, 0.0D);
                     }
 
-                    if (this.b == 60) {
+                    if (this.e == 60) {
                         this.c((EntityAnimal) entity);
                     }
                 } else {
-                    this.b = 0;
+                    this.e = 0;
                 }
             } else {
-                this.b = 0;
+                this.e = 0;
                 this.target = null;
             }
         }
@@ -96,10 +97,10 @@ public abstract class EntityAnimal extends EntityAgeable implements IAnimal {
             this.setAge(6000);
             entityanimal.setAge(6000);
             this.love = 0;
-            this.b = 0;
+            this.e = 0;
             this.target = null;
             entityanimal.target = null;
-            entityanimal.b = 0;
+            entityanimal.e = 0;
             entityanimal.love = 0;
             entityanimal1.setAge(-24000);
             entityanimal1.setPositionRotation(this.locX, this.locY, this.locZ, this.yaw, this.pitch);
@@ -118,17 +119,15 @@ public abstract class EntityAnimal extends EntityAgeable implements IAnimal {
 
     public abstract EntityAnimal createChild(EntityAnimal entityanimal);
 
-    protected void b(Entity entity, float f) {}
-
     public boolean damageEntity(DamageSource damagesource, int i) {
-        this.f = 60;
+        this.c = 60;
         this.target = null;
         this.love = 0;
         return super.damageEntity(damagesource, i);
     }
 
     public float a(int i, int j, int k) {
-        return this.world.getTypeId(i, j - 1, k) == Block.GRASS.id ? 10.0F : this.world.p(i, j, k) - 0.5F;
+        return this.world.getTypeId(i, j - 1, k) == Block.GRASS.id ? 10.0F : this.world.o(i, j, k) - 0.5F;
     }
 
     public void b(NBTTagCompound nbttagcompound) {
@@ -142,38 +141,41 @@ public abstract class EntityAnimal extends EntityAgeable implements IAnimal {
     }
 
     protected Entity findTarget() {
-        if (this.f > 0) {
+        if (this.c > 0) {
             return null;
         } else {
             float f = 8.0F;
             List list;
-            int i;
+            Iterator iterator;
             EntityAnimal entityanimal;
 
             if (this.love > 0) {
                 list = this.world.a(this.getClass(), this.boundingBox.grow((double) f, (double) f, (double) f));
+                iterator = list.iterator();
 
-                for (i = 0; i < list.size(); ++i) {
-                    entityanimal = (EntityAnimal) list.get(i);
+                while (iterator.hasNext()) {
+                    entityanimal = (EntityAnimal) iterator.next();
                     if (entityanimal != this && entityanimal.love > 0) {
                         return entityanimal;
                     }
                 }
             } else if (this.getAge() == 0) {
                 list = this.world.a(EntityHuman.class, this.boundingBox.grow((double) f, (double) f, (double) f));
+                iterator = list.iterator();
 
-                for (i = 0; i < list.size(); ++i) {
-                    EntityHuman entityhuman = (EntityHuman) list.get(i);
+                while (iterator.hasNext()) {
+                    EntityHuman entityhuman = (EntityHuman) iterator.next();
 
-                    if (entityhuman.U() != null && this.a(entityhuman.U())) {
+                    if (entityhuman.bC() != null && this.b(entityhuman.bC())) {
                         return entityhuman;
                     }
                 }
             } else if (this.getAge() > 0) {
                 list = this.world.a(this.getClass(), this.boundingBox.grow((double) f, (double) f, (double) f));
+                iterator = list.iterator();
 
-                for (i = 0; i < list.size(); ++i) {
-                    entityanimal = (EntityAnimal) list.get(i);
+                while (iterator.hasNext()) {
+                    entityanimal = (EntityAnimal) iterator.next();
                     if (entityanimal != this && entityanimal.getAge() < 0) {
                         return entityanimal;
                     }
@@ -189,14 +191,14 @@ public abstract class EntityAnimal extends EntityAgeable implements IAnimal {
         int j = MathHelper.floor(this.boundingBox.b);
         int k = MathHelper.floor(this.locZ);
 
-        return this.world.getTypeId(i, j - 1, k) == Block.GRASS.id && this.world.m(i, j, k) > 8 && super.canSpawn();
+        return this.world.getTypeId(i, j - 1, k) == Block.GRASS.id && this.world.k(i, j, k) > 8 && super.canSpawn();
     }
 
-    public int m() {
+    public int aG() {
         return 120;
     }
 
-    protected boolean n() {
+    protected boolean ba() {
         return false;
     }
 
@@ -204,14 +206,14 @@ public abstract class EntityAnimal extends EntityAgeable implements IAnimal {
         return 1 + this.world.random.nextInt(3);
     }
 
-    public boolean a(ItemStack itemstack) {
+    public boolean b(ItemStack itemstack) {
         return itemstack.id == Item.WHEAT.id;
     }
 
-    public boolean b(EntityHuman entityhuman) {
+    public boolean c(EntityHuman entityhuman) {
         ItemStack itemstack = entityhuman.inventory.getItemInHand();
 
-        if (itemstack != null && this.a(itemstack) && this.getAge() == 0) {
+        if (itemstack != null && this.b(itemstack) && this.getAge() == 0) {
             if (!entityhuman.abilities.canInstantlyBuild) {
                 --itemstack.count;
                 if (itemstack.count <= 0) {
@@ -232,19 +234,19 @@ public abstract class EntityAnimal extends EntityAgeable implements IAnimal {
 
             return true;
         } else {
-            return super.b(entityhuman);
+            return super.c(entityhuman);
         }
     }
 
-    public boolean r_() {
+    public boolean s() {
         return this.love > 0;
     }
 
-    public void s_() {
+    public void t() {
         this.love = 0;
     }
 
     public boolean mate(EntityAnimal entityanimal) {
-        return entityanimal == this ? false : (entityanimal.getClass() != this.getClass() ? false : this.r_() && entityanimal.r_());
+        return entityanimal == this ? false : (entityanimal.getClass() != this.getClass() ? false : this.s() && entityanimal.s());
     }
 }
