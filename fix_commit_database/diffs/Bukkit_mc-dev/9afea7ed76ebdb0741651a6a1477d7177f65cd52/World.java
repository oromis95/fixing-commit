@@ -7,54 +7,49 @@ import java.util.Iterator;
 import java.util.List;
 import java.util.Random;
 import java.util.Set;
-import java.util.TreeSet;
+import java.util.concurrent.Callable;
 
-public class World implements IBlockAccess {
+public abstract class World implements IBlockAccess {
 
-    public boolean a = false;
+    public boolean e = false;
     public List entityList = new ArrayList();
-    private List G = new ArrayList();
-    private TreeSet H = new TreeSet();
-    private Set I = new HashSet();
+    protected List g = new ArrayList();
     public List tileEntityList = new ArrayList();
-    private List J = new ArrayList();
-    private List K = new ArrayList();
+    private List a = new ArrayList();
+    private List b = new ArrayList();
     public List players = new ArrayList();
-    public List e = new ArrayList();
-    private long L = 16777215L;
-    public int f = 0;
-    protected int g = (new Random()).nextInt();
-    protected final int h = 1013904223;
-    protected float i;
-    protected float j;
-    protected float k;
-    protected float l;
-    protected int m = 0;
-    public int n = 0;
+    public List j = new ArrayList();
+    private long c = 16777215L;
+    public int k = 0;
+    protected int l = (new Random()).nextInt();
+    protected final int m = 1013904223;
+    protected float n;
+    protected float o;
+    protected float p;
+    protected float q;
+    protected int r = 0;
+    public int s = 0;
     public boolean suppressPhysics = false;
-    private long M = System.currentTimeMillis();
-    protected int p = 40;
     public int difficulty;
     public Random random = new Random();
-    public boolean s = false;
     public final WorldProvider worldProvider;
-    protected List u = new ArrayList();
+    protected List x = new ArrayList();
     protected IChunkProvider chunkProvider;
     protected final IDataManager dataManager;
     protected WorldData worldData;
     public boolean isLoading;
-    private boolean N;
     public WorldMapCollection worldMaps;
     public final VillageCollection villages = new VillageCollection(this);
-    private final VillageSiege O = new VillageSiege(this);
-    private ArrayList P = new ArrayList();
-    private boolean Q;
+    protected final VillageSiege siegeManager = new VillageSiege(this);
+    public final MethodProfiler methodProfiler;
+    private ArrayList d = new ArrayList();
+    private boolean L;
     protected boolean allowMonsters = true;
     protected boolean allowAnimals = true;
     protected Set chunkTickList = new HashSet();
-    private int R;
-    int[] E;
-    private List S;
+    private int M;
+    int[] J;
+    private List N;
     public boolean isStatic;
 
     public BiomeBase getBiome(int i, int j) {
@@ -73,86 +68,44 @@ public class World implements IBlockAccess {
         return this.worldProvider.c;
     }
 
-    public World(IDataManager idatamanager, String s, WorldSettings worldsettings, WorldProvider worldprovider) {
-        this.R = this.random.nextInt(12000);
-        this.E = new int['\u8000'];
-        this.S = new ArrayList();
+    public World(IDataManager idatamanager, String s, WorldSettings worldsettings, WorldProvider worldprovider, MethodProfiler methodprofiler) {
+        this.M = this.random.nextInt(12000);
+        this.J = new int['\u8000'];
+        this.N = new ArrayList();
         this.isStatic = false;
         this.dataManager = idatamanager;
+        this.methodProfiler = methodprofiler;
         this.worldMaps = new WorldMapCollection(idatamanager);
         this.worldData = idatamanager.getWorldData();
-        this.s = this.worldData == null;
         if (worldprovider != null) {
             this.worldProvider = worldprovider;
-        } else if (this.worldData != null && this.worldData.g() != 0) {
-            this.worldProvider = WorldProvider.byDimension(this.worldData.g());
+        } else if (this.worldData != null && this.worldData.i() != 0) {
+            this.worldProvider = WorldProvider.byDimension(this.worldData.i());
         } else {
             this.worldProvider = WorldProvider.byDimension(0);
         }
 
-        boolean flag = false;
-
         if (this.worldData == null) {
             this.worldData = new WorldData(worldsettings, s);
-            flag = true;
         } else {
-            this.worldData.a(s);
+            this.worldData.setName(s);
         }
 
         this.worldProvider.a(this);
-        this.chunkProvider = this.b();
-        if (flag) {
-            this.c();
+        this.chunkProvider = this.h();
+        if (!this.worldData.isInitialized()) {
+            this.a(worldsettings);
+            this.worldData.d(true);
         }
 
-        this.g();
-        this.B();
+        this.v();
+        this.a();
     }
 
-    protected IChunkProvider b() {
-        IChunkLoader ichunkloader = this.dataManager.createChunkLoader(this.worldProvider);
-
-        return new ChunkProviderLoadOrGenerate(this, ichunkloader, this.worldProvider.getChunkProvider());
-    }
-
-    protected void c() {
-        if (!this.worldProvider.c()) {
-            this.worldData.setSpawn(0, this.worldProvider.getSeaLevel(), 0);
-        } else {
-            this.isLoading = true;
-            WorldChunkManager worldchunkmanager = this.worldProvider.c;
-            List list = worldchunkmanager.a();
-            Random random = new Random(this.getSeed());
-            ChunkPosition chunkposition = worldchunkmanager.a(0, 0, 256, list, random);
-            int i = 0;
-            int j = this.worldProvider.getSeaLevel();
-            int k = 0;
-
-            if (chunkposition != null) {
-                i = chunkposition.x;
-                k = chunkposition.z;
-            } else {
-                System.out.println("Unable to find spawn biome");
-            }
-
-            int l = 0;
-
-            while (!this.worldProvider.canSpawn(i, k)) {
-                i += random.nextInt(64) - random.nextInt(64);
-                k += random.nextInt(64) - random.nextInt(64);
-                ++l;
-                if (l == 1000) {
-                    break;
-                }
-            }
-
-            this.worldData.setSpawn(i, j, k);
-            this.isLoading = false;
-        }
-    }
+    protected abstract IChunkProvider h();
 
-    public ChunkCoordinates getDimensionSpawn() {
-        return this.worldProvider.e();
+    protected void a(WorldSettings worldsettings) {
+        this.worldData.d(true);
     }
 
     public int b(int i, int j) {
@@ -165,32 +118,11 @@ public class World implements IBlockAccess {
         return this.getTypeId(i, k, j);
     }
 
-    public void save(boolean flag, IProgressUpdate iprogressupdate) {
-        if (this.chunkProvider.canSave()) {
-            if (iprogressupdate != null) {
-                iprogressupdate.a("Saving level");
-            }
-
-            this.A();
-            if (iprogressupdate != null) {
-                iprogressupdate.b("Saving chunks");
-            }
-
-            this.chunkProvider.saveChunks(flag, iprogressupdate);
-        }
-    }
-
-    private void A() {
-        this.m();
-        this.dataManager.saveWorldData(this.worldData, this.players);
-        this.worldMaps.a();
-    }
-
     public int getTypeId(int i, int j, int k) {
         return i >= -30000000 && k >= -30000000 && i < 30000000 && k < 30000000 ? (j < 0 ? 0 : (j >= 256 ? 0 : this.getChunkAt(i >> 4, k >> 4).getTypeId(i & 15, j, k & 15))) : 0;
     }
 
-    public int f(int i, int j, int k) {
+    public int b(int i, int j, int k) {
         return i >= -30000000 && k >= -30000000 && i < 30000000 && k < 30000000 ? (j < 0 ? 0 : (j >= 256 ? 0 : this.getChunkAt(i >> 4, k >> 4).b(i & 15, j, k & 15))) : 0;
     }
 
@@ -201,7 +133,7 @@ public class World implements IBlockAccess {
     public boolean isTileEntity(int i, int j, int k) {
         int l = this.getTypeId(i, j, k);
 
-        return Block.byId[l] != null && Block.byId[l].o();
+        return Block.byId[l] != null && Block.byId[l].s();
     }
 
     public boolean isLoaded(int i, int j, int k) {
@@ -209,10 +141,10 @@ public class World implements IBlockAccess {
     }
 
     public boolean areChunksLoaded(int i, int j, int k, int l) {
-        return this.a(i - l, j - l, k - l, i + l, j + l, k + l);
+        return this.c(i - l, j - l, k - l, i + l, j + l, k + l);
     }
 
-    public boolean a(int i, int j, int k, int l, int i1, int j1) {
+    public boolean c(int i, int j, int k, int l, int i1, int j1) {
         if (i1 >= 0 && j < 256) {
             i >>= 4;
             k >>= 4;
@@ -233,7 +165,7 @@ public class World implements IBlockAccess {
         }
     }
 
-    private boolean isChunkLoaded(int i, int j) {
+    protected boolean isChunkLoaded(int i, int j) {
         return this.chunkProvider.isChunkLoaded(i, j);
     }
 
@@ -246,6 +178,10 @@ public class World implements IBlockAccess {
     }
 
     public boolean setRawTypeIdAndData(int i, int j, int k, int l, int i1) {
+        return this.setRawTypeIdAndData(i, j, k, l, i1, true);
+    }
+
+    public boolean setRawTypeIdAndData(int i, int j, int k, int l, int i1, boolean flag) {
         if (i >= -30000000 && k >= -30000000 && i < 30000000 && k < 30000000) {
             if (j < 0) {
                 return false;
@@ -253,12 +189,16 @@ public class World implements IBlockAccess {
                 return false;
             } else {
                 Chunk chunk = this.getChunkAt(i >> 4, k >> 4);
-                boolean flag = chunk.a(i & 15, j, k & 15, l, i1);
+                boolean flag1 = chunk.a(i & 15, j, k & 15, l, i1);
 
-                MethodProfiler.a("checkLight");
-                this.v(i, j, k);
-                MethodProfiler.a();
-                return flag;
+                this.methodProfiler.a("checkLight");
+                this.x(i, j, k);
+                this.methodProfiler.b();
+                if (flag && flag1 && (this.isStatic || chunk.seenByPlayer)) {
+                    this.notify(i, j, k);
+                }
+
+                return flag1;
             }
         } else {
             return false;
@@ -275,9 +215,13 @@ public class World implements IBlockAccess {
                 Chunk chunk = this.getChunkAt(i >> 4, k >> 4);
                 boolean flag = chunk.a(i & 15, j, k & 15, l);
 
-                MethodProfiler.a("checkLight");
-                this.v(i, j, k);
-                MethodProfiler.a();
+                this.methodProfiler.a("checkLight");
+                this.x(i, j, k);
+                this.methodProfiler.b();
+                if (flag && (this.isStatic || chunk.seenByPlayer)) {
+                    this.notify(i, j, k);
+                }
+
                 return flag;
             }
         } else {
@@ -311,13 +255,7 @@ public class World implements IBlockAccess {
 
     public void setData(int i, int j, int k, int l) {
         if (this.setRawData(i, j, k, l)) {
-            int i1 = this.getTypeId(i, j, k);
-
-            if (Block.r[i1 & 4095]) {
-                this.update(i, j, k, i1);
-            } else {
-                this.applyPhysics(i, j, k, i1);
-            }
+            this.update(i, j, k, this.getTypeId(i, j, k));
         }
     }
 
@@ -329,10 +267,15 @@ public class World implements IBlockAccess {
                 return false;
             } else {
                 Chunk chunk = this.getChunkAt(i >> 4, k >> 4);
+                int i1 = i & 15;
+                int j1 = k & 15;
+                boolean flag = chunk.b(i1, j, j1, l);
 
-                i &= 15;
-                k &= 15;
-                return chunk.b(i, j, k, l);
+                if (flag && (this.isStatic || chunk.seenByPlayer && Block.r[chunk.getTypeId(i1, j, j1) & 4095])) {
+                    this.notify(i, j, k);
+                }
+
+                return flag;
             }
         } else {
             return false;
@@ -358,13 +301,16 @@ public class World implements IBlockAccess {
     }
 
     public void notify(int i, int j, int k) {
-        for (int l = 0; l < this.u.size(); ++l) {
-            ((IWorldAccess) this.u.get(l)).a(i, j, k);
+        Iterator iterator = this.x.iterator();
+
+        while (iterator.hasNext()) {
+            IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
+
+            iworldaccess.a(i, j, k);
         }
     }
 
     public void update(int i, int j, int k, int l) {
-        this.notify(i, j, k);
         this.applyPhysics(i, j, k, l);
     }
 
@@ -379,35 +325,43 @@ public class World implements IBlockAccess {
 
         if (!this.worldProvider.e) {
             for (i1 = k; i1 <= l; ++i1) {
-                this.b(EnumSkyBlock.SKY, i, i1, j);
+                this.c(EnumSkyBlock.SKY, i, i1, j);
             }
         }
 
-        this.b(i, k, j, i, l, j);
+        this.d(i, k, j, i, l, j);
     }
 
-    public void k(int i, int j, int k) {
-        for (int l = 0; l < this.u.size(); ++l) {
-            ((IWorldAccess) this.u.get(l)).a(i, j, k, i, j, k);
+    public void i(int i, int j, int k) {
+        Iterator iterator = this.x.iterator();
+
+        while (iterator.hasNext()) {
+            IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
+
+            iworldaccess.a(i, j, k, i, j, k);
         }
     }
 
-    public void b(int i, int j, int k, int l, int i1, int j1) {
-        for (int k1 = 0; k1 < this.u.size(); ++k1) {
-            ((IWorldAccess) this.u.get(k1)).a(i, j, k, l, i1, j1);
+    public void d(int i, int j, int k, int l, int i1, int j1) {
+        Iterator iterator = this.x.iterator();
+
+        while (iterator.hasNext()) {
+            IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
+
+            iworldaccess.a(i, j, k, l, i1, j1);
         }
     }
 
     public void applyPhysics(int i, int j, int k, int l) {
-        this.k(i - 1, j, k, l);
-        this.k(i + 1, j, k, l);
-        this.k(i, j - 1, k, l);
-        this.k(i, j + 1, k, l);
-        this.k(i, j, k - 1, l);
-        this.k(i, j, k + 1, l);
+        this.m(i - 1, j, k, l);
+        this.m(i + 1, j, k, l);
+        this.m(i, j - 1, k, l);
+        this.m(i, j + 1, k, l);
+        this.m(i, j, k - 1, l);
+        this.m(i, j, k + 1, l);
     }
 
-    private void k(int i, int j, int k, int l) {
+    private void m(int i, int j, int k, int l) {
         if (!this.suppressPhysics && !this.isStatic) {
             Block block = Block.byId[this.getTypeId(i, j, k)];
 
@@ -417,11 +371,11 @@ public class World implements IBlockAccess {
         }
     }
 
-    public boolean isChunkLoaded(int i, int j, int k) {
+    public boolean j(int i, int j, int k) {
         return this.getChunkAt(i >> 4, k >> 4).d(i & 15, j, k & 15);
     }
 
-    public int m(int i, int j, int k) {
+    public int k(int i, int j, int k) {
         if (j < 0) {
             return 0;
         } else {
@@ -442,7 +396,7 @@ public class World implements IBlockAccess {
             if (flag) {
                 int l = this.getTypeId(i, j, k);
 
-                if (l == Block.STEP.id || l == Block.SOIL.id || l == Block.COBBLESTONE_STAIRS.id || l == Block.WOOD_STAIRS.id) {
+                if (l == Block.STEP.id || l == Block.WOOD_STEP.id || l == Block.SOIL.id || l == Block.COBBLESTONE_STAIRS.id || l == Block.WOOD_STAIRS.id) {
                     int i1 = this.a(i, j + 1, k, false);
                     int j1 = this.a(i + 1, j, k, false);
                     int k1 = this.a(i - 1, j, k, false);
@@ -480,7 +434,7 @@ public class World implements IBlockAccess {
 
                 i &= 15;
                 k &= 15;
-                return chunk.c(i, j, k, this.f);
+                return chunk.c(i, j, k, this.k);
             }
         } else {
             return 15;
@@ -501,7 +455,7 @@ public class World implements IBlockAccess {
         }
     }
 
-    public int a(EnumSkyBlock enumskyblock, int i, int j, int k) {
+    public int b(EnumSkyBlock enumskyblock, int i, int j, int k) {
         if (j < 0) {
             j = 0;
         }
@@ -526,7 +480,7 @@ public class World implements IBlockAccess {
         }
     }
 
-    public void a(EnumSkyBlock enumskyblock, int i, int j, int k, int l) {
+    public void b(EnumSkyBlock enumskyblock, int i, int j, int k, int l) {
         if (i >= -30000000 && k >= -30000000 && i < 30000000 && k < 30000000) {
             if (j >= 0) {
                 if (j < 256) {
@@ -534,9 +488,12 @@ public class World implements IBlockAccess {
                         Chunk chunk = this.getChunkAt(i >> 4, k >> 4);
 
                         chunk.a(enumskyblock, i & 15, j, k & 15, l);
+                        Iterator iterator = this.x.iterator();
 
-                        for (int i1 = 0; i1 < this.u.size(); ++i1) {
-                            ((IWorldAccess) this.u.get(i1)).b(i, j, k);
+                        while (iterator.hasNext()) {
+                            IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
+
+                            iworldaccess.b(i, j, k);
                         }
                     }
                 }
@@ -544,18 +501,22 @@ public class World implements IBlockAccess {
         }
     }
 
-    public void o(int i, int j, int k) {
-        for (int l = 0; l < this.u.size(); ++l) {
-            ((IWorldAccess) this.u.get(l)).b(i, j, k);
+    public void n(int i, int j, int k) {
+        Iterator iterator = this.x.iterator();
+
+        while (iterator.hasNext()) {
+            IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
+
+            iworldaccess.b(i, j, k);
         }
     }
 
-    public float p(int i, int j, int k) {
+    public float o(int i, int j, int k) {
         return this.worldProvider.f[this.getLightLevel(i, j, k)];
     }
 
-    public boolean e() {
-        return this.f < 4;
+    public boolean r() {
+        return this.k < 4;
     }
 
     public MovingObjectPosition a(Vec3D vec3d, Vec3D vec3d1) {
@@ -683,7 +644,7 @@ public class World implements IBlockAccess {
                         vec3d.c = d2;
                     }
 
-                    Vec3D vec3d2 = Vec3D.create(vec3d.a, vec3d.b, vec3d.c);
+                    Vec3D vec3d2 = Vec3D.a().create(vec3d.a, vec3d.b, vec3d.c);
 
                     l = (int) (vec3d2.a = (double) MathHelper.floor(vec3d.a));
                     if (b0 == 5) {
@@ -726,31 +687,51 @@ public class World implements IBlockAccess {
     }
 
     public void makeSound(Entity entity, String s, float f, float f1) {
-        for (int i = 0; i < this.u.size(); ++i) {
-            ((IWorldAccess) this.u.get(i)).a(s, entity.locX, entity.locY - (double) entity.height, entity.locZ, f, f1);
+        if (entity != null && s != null) {
+            Iterator iterator = this.x.iterator();
+
+            while (iterator.hasNext()) {
+                IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
+
+                iworldaccess.a(s, entity.locX, entity.locY - (double) entity.height, entity.locZ, f, f1);
+            }
         }
     }
 
     public void makeSound(double d0, double d1, double d2, String s, float f, float f1) {
-        for (int i = 0; i < this.u.size(); ++i) {
-            ((IWorldAccess) this.u.get(i)).a(s, d0, d1, d2, f, f1);
+        if (s != null) {
+            Iterator iterator = this.x.iterator();
+
+            while (iterator.hasNext()) {
+                IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
+
+                iworldaccess.a(s, d0, d1, d2, f, f1);
+            }
         }
     }
 
     public void a(String s, int i, int j, int k) {
-        for (int l = 0; l < this.u.size(); ++l) {
-            ((IWorldAccess) this.u.get(l)).a(s, i, j, k);
+        Iterator iterator = this.x.iterator();
+
+        while (iterator.hasNext()) {
+            IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
+
+            iworldaccess.a(s, i, j, k);
         }
     }
 
     public void a(String s, double d0, double d1, double d2, double d3, double d4, double d5) {
-        for (int i = 0; i < this.u.size(); ++i) {
-            ((IWorldAccess) this.u.get(i)).a(s, d0, d1, d2, d3, d4, d5);
+        Iterator iterator = this.x.iterator();
+
+        while (iterator.hasNext()) {
+            IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
+
+            iworldaccess.a(s, d0, d1, d2, d3, d4, d5);
         }
     }
 
     public boolean strikeLightning(Entity entity) {
-        this.e.add(entity);
+        this.j.add(entity);
         return true;
     }
 
@@ -775,20 +756,28 @@ public class World implements IBlockAccess {
 
             this.getChunkAt(i, j).a(entity);
             this.entityList.add(entity);
-            this.c(entity);
+            this.a(entity);
             return true;
         }
     }
 
-    protected void c(Entity entity) {
-        for (int i = 0; i < this.u.size(); ++i) {
-            ((IWorldAccess) this.u.get(i)).a(entity);
+    protected void a(Entity entity) {
+        Iterator iterator = this.x.iterator();
+
+        while (iterator.hasNext()) {
+            IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
+
+            iworldaccess.a(entity);
         }
     }
 
-    protected void d(Entity entity) {
-        for (int i = 0; i < this.u.size(); ++i) {
-            ((IWorldAccess) this.u.get(i)).b(entity);
+    protected void b(Entity entity) {
+        Iterator iterator = this.x.iterator();
+
+        while (iterator.hasNext()) {
+            IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
+
+            iworldaccess.b(entity);
         }
     }
 
@@ -803,7 +792,7 @@ public class World implements IBlockAccess {
 
         entity.die();
         if (entity instanceof EntityHuman) {
-            this.players.remove((EntityHuman) entity);
+            this.players.remove(entity);
             this.everyoneSleeping();
         }
     }
@@ -811,27 +800,27 @@ public class World implements IBlockAccess {
     public void removeEntity(Entity entity) {
         entity.die();
         if (entity instanceof EntityHuman) {
-            this.players.remove((EntityHuman) entity);
+            this.players.remove(entity);
             this.everyoneSleeping();
         }
 
-        int i = entity.ca;
-        int j = entity.cc;
+        int i = entity.ah;
+        int j = entity.aj;
 
-        if (entity.bZ && this.isChunkLoaded(i, j)) {
+        if (entity.ag && this.isChunkLoaded(i, j)) {
             this.getChunkAt(i, j).b(entity);
         }
 
         this.entityList.remove(entity);
-        this.d(entity);
+        this.b(entity);
     }
 
     public void addIWorldAccess(IWorldAccess iworldaccess) {
-        this.u.add(iworldaccess);
+        this.x.add(iworldaccess);
     }
 
     public List getCubes(Entity entity, AxisAlignedBB axisalignedbb) {
-        this.P.clear();
+        this.d.clear();
         int i = MathHelper.floor(axisalignedbb.a);
         int j = MathHelper.floor(axisalignedbb.d + 1.0D);
         int k = MathHelper.floor(axisalignedbb.b);
@@ -846,7 +835,7 @@ public class World implements IBlockAccess {
                         Block block = Block.byId[this.getTypeId(k1, i2, l1)];
 
                         if (block != null) {
-                            block.a(this, k1, i2, l1, axisalignedbb, this.P);
+                            block.a(this, k1, i2, l1, axisalignedbb, this.d, entity);
                         }
                     }
                 }
@@ -855,25 +844,53 @@ public class World implements IBlockAccess {
 
         double d0 = 0.25D;
         List list = this.getEntities(entity, axisalignedbb.grow(d0, d0, d0));
+        Iterator iterator = list.iterator();
 
-        for (int j2 = 0; j2 < list.size(); ++j2) {
-            AxisAlignedBB axisalignedbb1 = ((Entity) list.get(j2)).h();
+        while (iterator.hasNext()) {
+            Entity entity1 = (Entity) iterator.next();
+            AxisAlignedBB axisalignedbb1 = entity1.E();
 
             if (axisalignedbb1 != null && axisalignedbb1.a(axisalignedbb)) {
-                this.P.add(axisalignedbb1);
+                this.d.add(axisalignedbb1);
             }
 
-            axisalignedbb1 = entity.b_((Entity) list.get(j2));
+            axisalignedbb1 = entity.g(entity1);
             if (axisalignedbb1 != null && axisalignedbb1.a(axisalignedbb)) {
-                this.P.add(axisalignedbb1);
+                this.d.add(axisalignedbb1);
+            }
+        }
+
+        return this.d;
+    }
+
+    public List a(AxisAlignedBB axisalignedbb) {
+        this.d.clear();
+        int i = MathHelper.floor(axisalignedbb.a);
+        int j = MathHelper.floor(axisalignedbb.d + 1.0D);
+        int k = MathHelper.floor(axisalignedbb.b);
+        int l = MathHelper.floor(axisalignedbb.e + 1.0D);
+        int i1 = MathHelper.floor(axisalignedbb.c);
+        int j1 = MathHelper.floor(axisalignedbb.f + 1.0D);
+
+        for (int k1 = i; k1 < j; ++k1) {
+            for (int l1 = i1; l1 < j1; ++l1) {
+                if (this.isLoaded(k1, 64, l1)) {
+                    for (int i2 = k - 1; i2 < l; ++i2) {
+                        Block block = Block.byId[this.getTypeId(k1, i2, l1)];
+
+                        if (block != null) {
+                            block.a(this, k1, i2, l1, axisalignedbb, this.d, (Entity) null);
+                        }
+                    }
+                }
             }
         }
 
-        return this.P;
+        return this.d;
     }
 
     public int a(float f) {
-        float f1 = this.b(f);
+        float f1 = this.c(f);
         float f2 = 1.0F - (MathHelper.cos(f1 * 3.1415927F * 2.0F) * 2.0F + 0.5F);
 
         if (f2 < 0.0F) {
@@ -885,23 +902,23 @@ public class World implements IBlockAccess {
         }
 
         f2 = 1.0F - f2;
-        f2 = (float) ((double) f2 * (1.0D - (double) (this.d(f) * 5.0F) / 16.0D));
-        f2 = (float) ((double) f2 * (1.0D - (double) (this.c(f) * 5.0F) / 16.0D));
+        f2 = (float) ((double) f2 * (1.0D - (double) (this.j(f) * 5.0F) / 16.0D));
+        f2 = (float) ((double) f2 * (1.0D - (double) (this.i(f) * 5.0F) / 16.0D));
         f2 = 1.0F - f2;
         return (int) (f2 * 11.0F);
     }
 
-    public float b(float f) {
+    public float c(float f) {
         return this.worldProvider.a(this.worldData.getTime(), f);
     }
 
-    public int f(int i, int j) {
+    public int g(int i, int j) {
         return this.getChunkAtWorldCoords(i, j).d(i & 15, j & 15);
     }
 
-    public int g(int i, int j) {
+    public int h(int i, int j) {
         Chunk chunk = this.getChunkAtWorldCoords(i, j);
-        int k = chunk.g() + 16;
+        int k = chunk.h() + 15;
 
         i &= 15;
 
@@ -916,81 +933,50 @@ public class World implements IBlockAccess {
         return -1;
     }
 
-    public void c(int i, int j, int k, int l, int i1) {
-        NextTickListEntry nextticklistentry = new NextTickListEntry(i, j, k, l);
-        byte b0 = 8;
-
-        if (this.a) {
-            if (this.a(nextticklistentry.a - b0, nextticklistentry.b - b0, nextticklistentry.c - b0, nextticklistentry.a + b0, nextticklistentry.b + b0, nextticklistentry.c + b0)) {
-                int j1 = this.getTypeId(nextticklistentry.a, nextticklistentry.b, nextticklistentry.c);
-
-                if (j1 == nextticklistentry.d && j1 > 0) {
-                    Block.byId[j1].a(this, nextticklistentry.a, nextticklistentry.b, nextticklistentry.c, this.random);
-                }
-            }
-        } else {
-            if (this.a(i - b0, j - b0, k - b0, i + b0, j + b0, k + b0)) {
-                if (l > 0) {
-                    nextticklistentry.a((long) i1 + this.worldData.getTime());
-                }
-
-                if (!this.I.contains(nextticklistentry)) {
-                    this.I.add(nextticklistentry);
-                    this.H.add(nextticklistentry);
-                }
-            }
-        }
-    }
-
-    public void d(int i, int j, int k, int l, int i1) {
-        NextTickListEntry nextticklistentry = new NextTickListEntry(i, j, k, l);
-
-        if (l > 0) {
-            nextticklistentry.a((long) i1 + this.worldData.getTime());
-        }
+    public void a(int i, int j, int k, int l, int i1) {}
 
-        if (!this.I.contains(nextticklistentry)) {
-            this.I.add(nextticklistentry);
-            this.H.add(nextticklistentry);
-        }
-    }
+    public void b(int i, int j, int k, int l, int i1) {}
 
     public void tickEntities() {
-        MethodProfiler.a("entities");
-        MethodProfiler.a("global");
+        this.methodProfiler.a("entities");
+        this.methodProfiler.a("global");
 
         int i;
         Entity entity;
 
-        for (i = 0; i < this.e.size(); ++i) {
-            entity = (Entity) this.e.get(i);
-            entity.F_();
+        for (i = 0; i < this.j.size(); ++i) {
+            entity = (Entity) this.j.get(i);
+            entity.h_();
             if (entity.dead) {
-                this.e.remove(i--);
+                this.j.remove(i--);
             }
         }
 
-        MethodProfiler.b("remove");
-        this.entityList.removeAll(this.G);
+        this.methodProfiler.c("remove");
+        this.entityList.removeAll(this.g);
+        Iterator iterator = this.g.iterator();
 
         int j;
         int k;
 
-        for (i = 0; i < this.G.size(); ++i) {
-            entity = (Entity) this.G.get(i);
-            j = entity.ca;
-            k = entity.cc;
-            if (entity.bZ && this.isChunkLoaded(j, k)) {
+        while (iterator.hasNext()) {
+            entity = (Entity) iterator.next();
+            j = entity.ah;
+            k = entity.aj;
+            if (entity.ag && this.isChunkLoaded(j, k)) {
                 this.getChunkAt(j, k).b(entity);
             }
         }
 
-        for (i = 0; i < this.G.size(); ++i) {
-            this.d((Entity) this.G.get(i));
+        iterator = this.g.iterator();
+
+        while (iterator.hasNext()) {
+            entity = (Entity) iterator.next();
+            this.b(entity);
         }
 
-        this.G.clear();
-        MethodProfiler.b("regular");
+        this.g.clear();
+        this.methodProfiler.c("regular");
 
         for (i = 0; i < this.entityList.size(); ++i) {
             entity = (Entity) this.entityList.get(i);
@@ -1003,37 +989,39 @@ public class World implements IBlockAccess {
                 entity.vehicle = null;
             }
 
+            this.methodProfiler.a("tick");
             if (!entity.dead) {
                 this.playerJoinedWorld(entity);
             }
 
-            MethodProfiler.a("remove");
+            this.methodProfiler.b();
+            this.methodProfiler.a("remove");
             if (entity.dead) {
-                j = entity.ca;
-                k = entity.cc;
-                if (entity.bZ && this.isChunkLoaded(j, k)) {
+                j = entity.ah;
+                k = entity.aj;
+                if (entity.ag && this.isChunkLoaded(j, k)) {
                     this.getChunkAt(j, k).b(entity);
                 }
 
                 this.entityList.remove(i--);
-                this.d(entity);
+                this.b(entity);
             }
 
-            MethodProfiler.a();
+            this.methodProfiler.b();
         }
 
-        MethodProfiler.b("tileEntities");
-        this.Q = true;
-        Iterator iterator = this.tileEntityList.iterator();
+        this.methodProfiler.c("tileEntities");
+        this.L = true;
+        iterator = this.tileEntityList.iterator();
 
         while (iterator.hasNext()) {
             TileEntity tileentity = (TileEntity) iterator.next();
 
-            if (!tileentity.l() && tileentity.world != null && this.isLoaded(tileentity.x, tileentity.y, tileentity.z)) {
-                tileentity.q_();
+            if (!tileentity.p() && tileentity.m() && this.isLoaded(tileentity.x, tileentity.y, tileentity.z)) {
+                tileentity.g();
             }
 
-            if (tileentity.l()) {
+            if (tileentity.p()) {
                 iterator.remove();
                 if (this.isChunkLoaded(tileentity.x >> 4, tileentity.z >> 4)) {
                     Chunk chunk = this.getChunkAt(tileentity.x >> 4, tileentity.z >> 4);
@@ -1045,20 +1033,20 @@ public class World implements IBlockAccess {
             }
         }
 
-        this.Q = false;
-        if (!this.K.isEmpty()) {
-            this.tileEntityList.removeAll(this.K);
-            this.K.clear();
+        this.L = false;
+        if (!this.b.isEmpty()) {
+            this.tileEntityList.removeAll(this.b);
+            this.b.clear();
         }
 
-        MethodProfiler.b("pendingTileEntities");
-        if (!this.J.isEmpty()) {
-            Iterator iterator1 = this.J.iterator();
+        this.methodProfiler.c("pendingTileEntities");
+        if (!this.a.isEmpty()) {
+            Iterator iterator1 = this.a.iterator();
 
             while (iterator1.hasNext()) {
                 TileEntity tileentity1 = (TileEntity) iterator1.next();
 
-                if (!tileentity1.l()) {
+                if (!tileentity1.p()) {
                     if (!this.tileEntityList.contains(tileentity1)) {
                         this.tileEntityList.add(tileentity1);
                     }
@@ -1075,16 +1063,16 @@ public class World implements IBlockAccess {
                 }
             }
 
-            this.J.clear();
+            this.a.clear();
         }
 
-        MethodProfiler.a();
-        MethodProfiler.a();
+        this.methodProfiler.b();
+        this.methodProfiler.b();
     }
 
     public void a(Collection collection) {
-        if (this.Q) {
-            this.J.addAll(collection);
+        if (this.L) {
+            this.a.addAll(collection);
         } else {
             this.tileEntityList.addAll(collection);
         }
@@ -1099,31 +1087,31 @@ public class World implements IBlockAccess {
         int j = MathHelper.floor(entity.locZ);
         byte b0 = 32;
 
-        if (!flag || this.a(i - b0, 0, j - b0, i + b0, 0, j + b0)) {
-            entity.bL = entity.locX;
-            entity.bM = entity.locY;
-            entity.bN = entity.locZ;
+        if (!flag || this.c(i - b0, 0, j - b0, i + b0, 0, j + b0)) {
+            entity.S = entity.locX;
+            entity.T = entity.locY;
+            entity.U = entity.locZ;
             entity.lastYaw = entity.yaw;
             entity.lastPitch = entity.pitch;
-            if (flag && entity.bZ) {
+            if (flag && entity.ag) {
                 if (entity.vehicle != null) {
-                    entity.R();
+                    entity.U();
                 } else {
-                    entity.F_();
+                    entity.h_();
                 }
             }
 
-            MethodProfiler.a("chunkCheck");
+            this.methodProfiler.a("chunkCheck");
             if (Double.isNaN(entity.locX) || Double.isInfinite(entity.locX)) {
-                entity.locX = entity.bL;
+                entity.locX = entity.S;
             }
 
             if (Double.isNaN(entity.locY) || Double.isInfinite(entity.locY)) {
-                entity.locY = entity.bM;
+                entity.locY = entity.T;
             }
 
             if (Double.isNaN(entity.locZ) || Double.isInfinite(entity.locZ)) {
-                entity.locZ = entity.bN;
+                entity.locZ = entity.U;
             }
 
             if (Double.isNaN((double) entity.pitch) || Double.isInfinite((double) entity.pitch)) {
@@ -1138,21 +1126,21 @@ public class World implements IBlockAccess {
             int l = MathHelper.floor(entity.locY / 16.0D);
             int i1 = MathHelper.floor(entity.locZ / 16.0D);
 
-            if (!entity.bZ || entity.ca != k || entity.cb != l || entity.cc != i1) {
-                if (entity.bZ && this.isChunkLoaded(entity.ca, entity.cc)) {
-                    this.getChunkAt(entity.ca, entity.cc).a(entity, entity.cb);
+            if (!entity.ag || entity.ah != k || entity.ai != l || entity.aj != i1) {
+                if (entity.ag && this.isChunkLoaded(entity.ah, entity.aj)) {
+                    this.getChunkAt(entity.ah, entity.aj).a(entity, entity.ai);
                 }
 
                 if (this.isChunkLoaded(k, i1)) {
-                    entity.bZ = true;
+                    entity.ag = true;
                     this.getChunkAt(k, i1).a(entity);
                 } else {
-                    entity.bZ = false;
+                    entity.ag = false;
                 }
             }
 
-            MethodProfiler.a();
-            if (flag && entity.bZ && entity.passenger != null) {
+            this.methodProfiler.b();
+            if (flag && entity.ag && entity.passenger != null) {
                 if (!entity.passenger.dead && entity.passenger.vehicle == entity) {
                     this.playerJoinedWorld(entity.passenger);
                 } else {
@@ -1163,21 +1151,28 @@ public class World implements IBlockAccess {
         }
     }
 
-    public boolean containsEntity(AxisAlignedBB axisalignedbb) {
+    public boolean b(AxisAlignedBB axisalignedbb) {
+        return this.a(axisalignedbb, (Entity) null);
+    }
+
+    public boolean a(AxisAlignedBB axisalignedbb, Entity entity) {
         List list = this.getEntities((Entity) null, axisalignedbb);
+        Iterator iterator = list.iterator();
 
-        for (int i = 0; i < list.size(); ++i) {
-            Entity entity = (Entity) list.get(i);
+        Entity entity1;
 
-            if (!entity.dead && entity.bf) {
-                return false;
+        do {
+            if (!iterator.hasNext()) {
+                return true;
             }
-        }
 
-        return true;
+            entity1 = (Entity) iterator.next();
+        } while (entity1.dead || !entity1.m || entity1 == entity);
+
+        return false;
     }
 
-    public boolean b(AxisAlignedBB axisalignedbb) {
+    public boolean c(AxisAlignedBB axisalignedbb) {
         int i = MathHelper.floor(axisalignedbb.a);
         int j = MathHelper.floor(axisalignedbb.d + 1.0D);
         int k = MathHelper.floor(axisalignedbb.b);
@@ -1247,7 +1242,7 @@ public class World implements IBlockAccess {
         return false;
     }
 
-    public boolean d(AxisAlignedBB axisalignedbb) {
+    public boolean e(AxisAlignedBB axisalignedbb) {
         int i = MathHelper.floor(axisalignedbb.a);
         int j = MathHelper.floor(axisalignedbb.d + 1.0D);
         int k = MathHelper.floor(axisalignedbb.b);
@@ -1255,7 +1250,7 @@ public class World implements IBlockAccess {
         int i1 = MathHelper.floor(axisalignedbb.c);
         int j1 = MathHelper.floor(axisalignedbb.f + 1.0D);
 
-        if (this.a(i, k, i1, j, l, j1)) {
+        if (this.c(i, k, i1, j, l, j1)) {
             for (int k1 = i; k1 < j; ++k1) {
                 for (int l1 = k; l1 < l; ++l1) {
                     for (int i2 = i1; i2 < j1; ++i2) {
@@ -1280,11 +1275,11 @@ public class World implements IBlockAccess {
         int i1 = MathHelper.floor(axisalignedbb.c);
         int j1 = MathHelper.floor(axisalignedbb.f + 1.0D);
 
-        if (!this.a(i, k, i1, j, l, j1)) {
+        if (!this.c(i, k, i1, j, l, j1)) {
             return false;
         } else {
             boolean flag = false;
-            Vec3D vec3d = Vec3D.create(0.0D, 0.0D, 0.0D);
+            Vec3D vec3d = Vec3D.a().create(0.0D, 0.0D, 0.0D);
 
             for (int k1 = i; k1 < j; ++k1) {
                 for (int l1 = k; l1 < l; ++l1) {
@@ -1398,7 +1393,7 @@ public class World implements IBlockAccess {
                     double d4 = axisalignedbb.b + (axisalignedbb.e - axisalignedbb.b) * (double) f1;
                     double d5 = axisalignedbb.c + (axisalignedbb.f - axisalignedbb.c) * (double) f2;
 
-                    if (this.a(Vec3D.create(d3, d4, d5), vec3d) == null) {
+                    if (this.a(Vec3D.a().create(d3, d4, d5), vec3d) == null) {
                         ++i;
                     }
 
@@ -1456,12 +1451,12 @@ public class World implements IBlockAccess {
                 TileEntity tileentity = chunk.e(i & 15, j, k & 15);
 
                 if (tileentity == null) {
-                    Iterator iterator = this.J.iterator();
+                    Iterator iterator = this.a.iterator();
 
                     while (iterator.hasNext()) {
                         TileEntity tileentity1 = (TileEntity) iterator.next();
 
-                        if (!tileentity1.l() && tileentity1.x == i && tileentity1.y == j && tileentity1.z == k) {
+                        if (!tileentity1.p() && tileentity1.x == i && tileentity1.y == j && tileentity1.z == k) {
                             tileentity = tileentity1;
                             break;
                         }
@@ -1474,12 +1469,12 @@ public class World implements IBlockAccess {
     }
 
     public void setTileEntity(int i, int j, int k, TileEntity tileentity) {
-        if (tileentity != null && !tileentity.l()) {
-            if (this.Q) {
+        if (tileentity != null && !tileentity.p()) {
+            if (this.L) {
                 tileentity.x = i;
                 tileentity.y = j;
                 tileentity.z = k;
-                this.J.add(tileentity);
+                this.a.add(tileentity);
             } else {
                 this.tileEntityList.add(tileentity);
                 Chunk chunk = this.getChunkAt(i >> 4, k >> 4);
@@ -1494,12 +1489,12 @@ public class World implements IBlockAccess {
     public void q(int i, int j, int k) {
         TileEntity tileentity = this.getTileEntity(i, j, k);
 
-        if (tileentity != null && this.Q) {
+        if (tileentity != null && this.L) {
             tileentity.j();
-            this.J.remove(tileentity);
+            this.a.remove(tileentity);
         } else {
             if (tileentity != null) {
-                this.J.remove(tileentity);
+                this.a.remove(tileentity);
                 this.tileEntityList.remove(tileentity);
             }
 
@@ -1512,17 +1507,23 @@ public class World implements IBlockAccess {
     }
 
     public void a(TileEntity tileentity) {
-        this.K.add(tileentity);
+        this.b.add(tileentity);
     }
 
     public boolean r(int i, int j, int k) {
         Block block = Block.byId[this.getTypeId(i, j, k)];
 
-        return block == null ? false : block.a();
+        return block == null ? false : block.d();
     }
 
-    public boolean e(int i, int j, int k) {
-        return Block.g(this.getTypeId(i, j, k));
+    public boolean s(int i, int j, int k) {
+        return Block.i(this.getTypeId(i, j, k));
+    }
+
+    public boolean t(int i, int j, int k) {
+        Block block = Block.byId[this.getTypeId(i, j, k)];
+
+        return block == null ? false : (block.material.k() && block.c() ? true : (block instanceof BlockStairs ? (this.getData(i, j, k) & 4) == 4 : (block instanceof BlockStepAbstract ? (this.getData(i, j, k) & 8) == 8 : false)));
     }
 
     public boolean b(int i, int j, int k, boolean flag) {
@@ -1532,7 +1533,7 @@ public class World implements IBlockAccess {
             if (chunk != null && !chunk.isEmpty()) {
                 Block block = Block.byId[this.getTypeId(i, j, k)];
 
-                return block == null ? false : block.material.j() && block.b();
+                return block == null ? false : block.material.k() && block.c();
             } else {
                 return flag;
             }
@@ -1541,11 +1542,11 @@ public class World implements IBlockAccess {
         }
     }
 
-    public void g() {
+    public void v() {
         int i = this.a(1.0F);
 
-        if (i != this.f) {
-            this.f = i;
+        if (i != this.k) {
+            this.k = i;
         }
     }
 
@@ -1555,68 +1556,22 @@ public class World implements IBlockAccess {
     }
 
     public void doTick() {
-        if (this.getWorldData().isHardcore() && this.difficulty < 3) {
-            this.difficulty = 3;
-        }
-
-        this.worldProvider.c.b();
-        this.i();
-        long i;
-
-        if (this.everyoneDeeplySleeping()) {
-            boolean flag = false;
-
-            if (this.allowMonsters && this.difficulty >= 1) {
-                ;
-            }
-
-            if (!flag) {
-                i = this.worldData.getTime() + 24000L;
-                this.worldData.a(i - i % 24000L);
-                this.u();
-            }
-        }
-
-        MethodProfiler.a("mobSpawner");
-        SpawnerCreature.spawnEntities(this, this.allowMonsters, this.allowAnimals && this.worldData.getTime() % 400L == 0L);
-        MethodProfiler.b("chunkSource");
-        this.chunkProvider.unloadChunks();
-        int j = this.a(1.0F);
-
-        if (j != this.f) {
-            this.f = j;
-        }
-
-        i = this.worldData.getTime() + 1L;
-        if (i % (long) this.p == 0L) {
-            MethodProfiler.b("save");
-            this.save(false, (IProgressUpdate) null);
-        }
-
-        this.worldData.a(i);
-        MethodProfiler.b("tickPending");
-        this.a(false);
-        MethodProfiler.b("tickTiles");
         this.l();
-        MethodProfiler.b("village");
-        this.villages.tick();
-        this.O.a();
-        MethodProfiler.a();
     }
 
-    private void B() {
+    private void a() {
         if (this.worldData.hasStorm()) {
-            this.j = 1.0F;
+            this.o = 1.0F;
             if (this.worldData.isThundering()) {
-                this.l = 1.0F;
+                this.q = 1.0F;
             }
         }
     }
 
-    protected void i() {
+    protected void l() {
         if (!this.worldProvider.e) {
-            if (this.m > 0) {
-                --this.m;
+            if (this.r > 0) {
+                --this.r;
             }
 
             int i = this.worldData.getThunderDuration();
@@ -1651,52 +1606,45 @@ public class World implements IBlockAccess {
                 }
             }
 
-            this.i = this.j;
+            this.n = this.o;
             if (this.worldData.hasStorm()) {
-                this.j = (float) ((double) this.j + 0.01D);
+                this.o = (float) ((double) this.o + 0.01D);
             } else {
-                this.j = (float) ((double) this.j - 0.01D);
+                this.o = (float) ((double) this.o - 0.01D);
             }
 
-            if (this.j < 0.0F) {
-                this.j = 0.0F;
+            if (this.o < 0.0F) {
+                this.o = 0.0F;
             }
 
-            if (this.j > 1.0F) {
-                this.j = 1.0F;
+            if (this.o > 1.0F) {
+                this.o = 1.0F;
             }
 
-            this.k = this.l;
+            this.p = this.q;
             if (this.worldData.isThundering()) {
-                this.l = (float) ((double) this.l + 0.01D);
+                this.q = (float) ((double) this.q + 0.01D);
             } else {
-                this.l = (float) ((double) this.l - 0.01D);
+                this.q = (float) ((double) this.q - 0.01D);
             }
 
-            if (this.l < 0.0F) {
-                this.l = 0.0F;
+            if (this.q < 0.0F) {
+                this.q = 0.0F;
             }
 
-            if (this.l > 1.0F) {
-                this.l = 1.0F;
+            if (this.q > 1.0F) {
+                this.q = 1.0F;
             }
         }
     }
 
-    private void C() {
-        this.worldData.setWeatherDuration(0);
-        this.worldData.setStorm(false);
-        this.worldData.setThunderDuration(0);
-        this.worldData.setThundering(false);
-    }
-
-    public void j() {
+    public void w() {
         this.worldData.setWeatherDuration(1);
     }
 
-    protected void k() {
+    protected void x() {
         this.chunkTickList.clear();
-        MethodProfiler.a("buildList");
+        this.methodProfiler.a("buildList");
 
         int i;
         EntityHuman entityhuman;
@@ -1716,12 +1664,12 @@ public class World implements IBlockAccess {
             }
         }
 
-        MethodProfiler.a();
-        if (this.R > 0) {
-            --this.R;
+        this.methodProfiler.b();
+        if (this.M > 0) {
+            --this.M;
         }
 
-        MethodProfiler.a("playerCheckLight");
+        this.methodProfiler.a("playerCheckLight");
         if (!this.players.isEmpty()) {
             i = this.random.nextInt(this.players.size());
             entityhuman = (EntityHuman) this.players.get(i);
@@ -1729,19 +1677,17 @@ public class World implements IBlockAccess {
             k = MathHelper.floor(entityhuman.locY) + this.random.nextInt(11) - 5;
             int j1 = MathHelper.floor(entityhuman.locZ) + this.random.nextInt(11) - 5;
 
-            this.v(j, k, j1);
+            this.x(j, k, j1);
         }
 
-        MethodProfiler.a();
+        this.methodProfiler.b();
     }
 
     protected void a(int i, int j, Chunk chunk) {
-        MethodProfiler.b("tickChunk");
-        chunk.j();
-        MethodProfiler.b("moodSound");
-        if (this.R == 0) {
-            this.g = this.g * 3 + 1013904223;
-            int k = this.g >> 2;
+        this.methodProfiler.c("moodSound");
+        if (this.M == 0) {
+            this.l = this.l * 3 + 1013904223;
+            int k = this.l >> 2;
             int l = k & 15;
             int i1 = k >> 8 & 15;
             int j1 = k >> 16 & 127;
@@ -1749,117 +1695,40 @@ public class World implements IBlockAccess {
 
             l += i;
             i1 += j;
-            if (k1 == 0 && this.m(l, j1, i1) <= this.random.nextInt(8) && this.a(EnumSkyBlock.SKY, l, j1, i1) <= 0) {
+            if (k1 == 0 && this.k(l, j1, i1) <= this.random.nextInt(8) && this.b(EnumSkyBlock.SKY, l, j1, i1) <= 0) {
                 EntityHuman entityhuman = this.findNearbyPlayer((double) l + 0.5D, (double) j1 + 0.5D, (double) i1 + 0.5D, 8.0D);
 
                 if (entityhuman != null && entityhuman.e((double) l + 0.5D, (double) j1 + 0.5D, (double) i1 + 0.5D) > 4.0D) {
                     this.makeSound((double) l + 0.5D, (double) j1 + 0.5D, (double) i1 + 0.5D, "ambient.cave.cave", 0.7F, 0.8F + this.random.nextFloat() * 0.2F);
-                    this.R = this.random.nextInt(12000) + 6000;
+                    this.M = this.random.nextInt(12000) + 6000;
                 }
             }
         }
 
-        MethodProfiler.b("checkLight");
-        chunk.n();
+        this.methodProfiler.c("checkLight");
+        chunk.o();
     }
 
-    protected void l() {
-        this.k();
-        int i = 0;
-        int j = 0;
-        Iterator iterator = this.chunkTickList.iterator();
-
-        while (iterator.hasNext()) {
-            ChunkCoordIntPair chunkcoordintpair = (ChunkCoordIntPair) iterator.next();
-            int k = chunkcoordintpair.x * 16;
-            int l = chunkcoordintpair.z * 16;
-
-            MethodProfiler.a("getChunk");
-            Chunk chunk = this.getChunkAt(chunkcoordintpair.x, chunkcoordintpair.z);
-
-            this.a(k, l, chunk);
-            MethodProfiler.b("thunder");
-            int i1;
-            int j1;
-            int k1;
-            int l1;
-
-            if (this.random.nextInt(100000) == 0 && this.x() && this.w()) {
-                this.g = this.g * 3 + 1013904223;
-                i1 = this.g >> 2;
-                j1 = k + (i1 & 15);
-                k1 = l + (i1 >> 8 & 15);
-                l1 = this.f(j1, k1);
-                if (this.y(j1, l1, k1)) {
-                    this.strikeLightning(new EntityWeatherLighting(this, (double) j1, (double) l1, (double) k1));
-                    this.m = 2;
-                }
-            }
-
-            MethodProfiler.b("iceandsnow");
-            if (this.random.nextInt(16) == 0) {
-                this.g = this.g * 3 + 1013904223;
-                i1 = this.g >> 2;
-                j1 = i1 & 15;
-                k1 = i1 >> 8 & 15;
-                l1 = this.f(j1 + k, k1 + l);
-                if (this.t(j1 + k, l1 - 1, k1 + l)) {
-                    this.setTypeId(j1 + k, l1 - 1, k1 + l, Block.ICE.id);
-                }
-
-                if (this.x() && this.u(j1 + k, l1, k1 + l)) {
-                    this.setTypeId(j1 + k, l1, k1 + l, Block.SNOW.id);
-                }
-            }
-
-            MethodProfiler.b("tickTiles");
-            ChunkSection[] achunksection = chunk.h();
-
-            j1 = achunksection.length;
-
-            for (k1 = 0; k1 < j1; ++k1) {
-                ChunkSection chunksection = achunksection[k1];
-
-                if (chunksection != null && chunksection.b()) {
-                    for (int i2 = 0; i2 < 3; ++i2) {
-                        this.g = this.g * 3 + 1013904223;
-                        int j2 = this.g >> 2;
-                        int k2 = j2 & 15;
-                        int l2 = j2 >> 8 & 15;
-                        int i3 = j2 >> 16 & 15;
-                        int j3 = chunksection.a(k2, i3, l2);
-
-                        ++j;
-                        Block block = Block.byId[j3];
-
-                        if (block != null && block.n()) {
-                            ++i;
-                            block.a(this, k2 + k, i3 + chunksection.c(), l2 + l, this.random);
-                        }
-                    }
-                }
-            }
-
-            MethodProfiler.a();
-        }
+    protected void g() {
+        this.x();
     }
 
-    public boolean s(int i, int j, int k) {
+    public boolean u(int i, int j, int k) {
         return this.c(i, j, k, false);
     }
 
-    public boolean t(int i, int j, int k) {
+    public boolean v(int i, int j, int k) {
         return this.c(i, j, k, true);
     }
 
     public boolean c(int i, int j, int k, boolean flag) {
         BiomeBase biomebase = this.getBiome(i, k);
-        float f = biomebase.i();
+        float f = biomebase.j();
 
         if (f > 0.15F) {
             return false;
         } else {
-            if (j >= 0 && j < 256 && this.a(EnumSkyBlock.BLOCK, i, j, k) < 10) {
+            if (j >= 0 && j < 256 && this.b(EnumSkyBlock.BLOCK, i, j, k) < 10) {
                 int l = this.getTypeId(i, j, k);
 
                 if ((l == Block.STATIONARY_WATER.id || l == Block.WATER.id) && this.getData(i, j, k) == 0) {
@@ -1895,14 +1764,14 @@ public class World implements IBlockAccess {
         }
     }
 
-    public boolean u(int i, int j, int k) {
+    public boolean w(int i, int j, int k) {
         BiomeBase biomebase = this.getBiome(i, k);
-        float f = biomebase.i();
+        float f = biomebase.j();
 
         if (f > 0.15F) {
             return false;
         } else {
-            if (j >= 0 && j < 256 && this.a(EnumSkyBlock.BLOCK, i, j, k) < 10) {
+            if (j >= 0 && j < 256 && this.b(EnumSkyBlock.BLOCK, i, j, k) < 10) {
                 int l = this.getTypeId(i, j - 1, k);
                 int i1 = this.getTypeId(i, j, k);
 
@@ -1915,30 +1784,30 @@ public class World implements IBlockAccess {
         }
     }
 
-    public void v(int i, int j, int k) {
+    public void x(int i, int j, int k) {
         if (!this.worldProvider.e) {
-            this.b(EnumSkyBlock.SKY, i, j, k);
+            this.c(EnumSkyBlock.SKY, i, j, k);
         }
 
-        this.b(EnumSkyBlock.BLOCK, i, j, k);
+        this.c(EnumSkyBlock.BLOCK, i, j, k);
     }
 
-    private int c(int i, int j, int k, int l, int i1, int j1) {
+    private int a(int i, int j, int k, int l, int i1, int j1) {
         int k1 = 0;
 
-        if (this.isChunkLoaded(j, k, l)) {
+        if (this.j(j, k, l)) {
             k1 = 15;
         } else {
             if (j1 == 0) {
                 j1 = 1;
             }
 
-            int l1 = this.a(EnumSkyBlock.SKY, j - 1, k, l) - j1;
-            int i2 = this.a(EnumSkyBlock.SKY, j + 1, k, l) - j1;
-            int j2 = this.a(EnumSkyBlock.SKY, j, k - 1, l) - j1;
-            int k2 = this.a(EnumSkyBlock.SKY, j, k + 1, l) - j1;
-            int l2 = this.a(EnumSkyBlock.SKY, j, k, l - 1) - j1;
-            int i3 = this.a(EnumSkyBlock.SKY, j, k, l + 1) - j1;
+            int l1 = this.b(EnumSkyBlock.SKY, j - 1, k, l) - j1;
+            int i2 = this.b(EnumSkyBlock.SKY, j + 1, k, l) - j1;
+            int j2 = this.b(EnumSkyBlock.SKY, j, k - 1, l) - j1;
+            int k2 = this.b(EnumSkyBlock.SKY, j, k + 1, l) - j1;
+            int l2 = this.b(EnumSkyBlock.SKY, j, k, l - 1) - j1;
+            int i3 = this.b(EnumSkyBlock.SKY, j, k, l + 1) - j1;
 
             if (l1 > k1) {
                 k1 = l1;
@@ -1968,14 +1837,14 @@ public class World implements IBlockAccess {
         return k1;
     }
 
-    private int d(int i, int j, int k, int l, int i1, int j1) {
+    private int f(int i, int j, int k, int l, int i1, int j1) {
         int k1 = Block.lightEmission[i1];
-        int l1 = this.a(EnumSkyBlock.BLOCK, j - 1, k, l) - j1;
-        int i2 = this.a(EnumSkyBlock.BLOCK, j + 1, k, l) - j1;
-        int j2 = this.a(EnumSkyBlock.BLOCK, j, k - 1, l) - j1;
-        int k2 = this.a(EnumSkyBlock.BLOCK, j, k + 1, l) - j1;
-        int l2 = this.a(EnumSkyBlock.BLOCK, j, k, l - 1) - j1;
-        int i3 = this.a(EnumSkyBlock.BLOCK, j, k, l + 1) - j1;
+        int l1 = this.b(EnumSkyBlock.BLOCK, j - 1, k, l) - j1;
+        int i2 = this.b(EnumSkyBlock.BLOCK, j + 1, k, l) - j1;
+        int j2 = this.b(EnumSkyBlock.BLOCK, j, k - 1, l) - j1;
+        int k2 = this.b(EnumSkyBlock.BLOCK, j, k + 1, l) - j1;
+        int l2 = this.b(EnumSkyBlock.BLOCK, j, k, l - 1) - j1;
+        int i3 = this.b(EnumSkyBlock.BLOCK, j, k, l + 1) - j1;
 
         if (l1 > k1) {
             k1 = l1;
@@ -2004,16 +1873,16 @@ public class World implements IBlockAccess {
         return k1;
     }
 
-    public void b(EnumSkyBlock enumskyblock, int i, int j, int k) {
+    public void c(EnumSkyBlock enumskyblock, int i, int j, int k) {
         if (this.areChunksLoaded(i, j, k, 17)) {
             int l = 0;
             int i1 = 0;
 
-            MethodProfiler.a("getBrightness");
-            int j1 = this.a(enumskyblock, i, j, k);
+            this.methodProfiler.a("getBrightness");
+            int j1 = this.b(enumskyblock, i, j, k);
             boolean flag = false;
             int k1 = this.getTypeId(i, j, k);
-            int l1 = this.f(i, j, k);
+            int l1 = this.b(i, j, k);
 
             if (l1 == 0) {
                 l1 = 1;
@@ -2023,9 +1892,9 @@ public class World implements IBlockAccess {
             int i2;
 
             if (enumskyblock == EnumSkyBlock.SKY) {
-                i2 = this.c(j1, i, j, k, k1, l1);
+                i2 = this.a(j1, i, j, k, k1, l1);
             } else {
-                i2 = this.d(j1, i, j, k, k1, l1);
+                i2 = this.f(j1, i, j, k, k1, l1);
             }
 
             int j2;
@@ -2034,29 +1903,31 @@ public class World implements IBlockAccess {
             int i3;
             int j3;
             int k3;
+            int l3;
+            int i4;
 
             if (i2 > j1) {
-                this.E[i1++] = 133152;
+                this.J[i1++] = 133152;
             } else if (i2 < j1) {
                 if (enumskyblock != EnumSkyBlock.BLOCK) {
                     ;
                 }
 
-                this.E[i1++] = 133152 + (j1 << 18);
+                this.J[i1++] = 133152 + (j1 << 18);
 
                 while (l < i1) {
-                    j2 = this.E[l++];
-                    k1 = (j2 & 63) - 32 + i;
-                    l1 = (j2 >> 6 & 63) - 32 + j;
-                    i2 = (j2 >> 12 & 63) - 32 + k;
-                    k2 = j2 >> 18 & 15;
-                    l2 = this.a(enumskyblock, k1, l1, i2);
+                    k1 = this.J[l++];
+                    l1 = (k1 & 63) - 32 + i;
+                    i2 = (k1 >> 6 & 63) - 32 + j;
+                    j2 = (k1 >> 12 & 63) - 32 + k;
+                    k2 = k1 >> 18 & 15;
+                    l2 = this.b(enumskyblock, l1, i2, j2);
                     if (l2 == k2) {
-                        this.a(enumskyblock, k1, l1, i2, 0);
+                        this.b(enumskyblock, l1, i2, j2, 0);
                         if (k2 > 0) {
-                            i3 = k1 - i;
-                            k3 = l1 - j;
-                            j3 = i2 - k;
+                            i3 = l1 - i;
+                            k3 = i2 - j;
+                            j3 = j2 - k;
                             if (i3 < 0) {
                                 i3 = -i3;
                             }
@@ -2070,21 +1941,21 @@ public class World implements IBlockAccess {
                             }
 
                             if (i3 + k3 + j3 < 17) {
-                                for (int l3 = 0; l3 < 6; ++l3) {
-                                    int i4 = l3 % 2 * 2 - 1;
-                                    int j4 = k1 + l3 / 2 % 3 / 2 * i4;
-                                    int k4 = l1 + (l3 / 2 + 1) % 3 / 2 * i4;
-                                    int l4 = i2 + (l3 / 2 + 2) % 3 / 2 * i4;
+                                for (i4 = 0; i4 < 6; ++i4) {
+                                    l3 = i4 % 2 * 2 - 1;
+                                    int j4 = l1 + i4 / 2 % 3 / 2 * l3;
+                                    int k4 = i2 + (i4 / 2 + 1) % 3 / 2 * l3;
+                                    int l4 = j2 + (i4 / 2 + 2) % 3 / 2 * l3;
 
-                                    l2 = this.a(enumskyblock, j4, k4, l4);
+                                    l2 = this.b(enumskyblock, j4, k4, l4);
                                     int i5 = Block.lightBlock[this.getTypeId(j4, k4, l4)];
 
                                     if (i5 == 0) {
                                         i5 = 1;
                                     }
 
-                                    if (l2 == k2 - i5 && i1 < this.E.length) {
-                                        this.E[i1++] = j4 - i + 32 + (k4 - j + 32 << 6) + (l4 - k + 32 << 12) + (k2 - i5 << 18);
+                                    if (l2 == k2 - i5 && i1 < this.J.length) {
+                                        this.J[i1++] = j4 - i + 32 + (k4 - j + 32 << 6) + (l4 - k + 32 << 12) + (k2 - i5 << 18);
                                     }
                                 }
                             }
@@ -2095,146 +1966,90 @@ public class World implements IBlockAccess {
                 l = 0;
             }
 
-            MethodProfiler.a();
-            MethodProfiler.a("tcp < tcc");
+            this.methodProfiler.b();
+            this.methodProfiler.a("tcp < tcc");
 
             while (l < i1) {
-                j1 = this.E[l++];
-                int j5 = (j1 & 63) - 32 + i;
-
-                j2 = (j1 >> 6 & 63) - 32 + j;
-                k1 = (j1 >> 12 & 63) - 32 + k;
-                l1 = this.a(enumskyblock, j5, j2, k1);
-                i2 = this.getTypeId(j5, j2, k1);
-                k2 = Block.lightBlock[i2];
-                if (k2 == 0) {
-                    k2 = 1;
+                k1 = this.J[l++];
+                l1 = (k1 & 63) - 32 + i;
+                i2 = (k1 >> 6 & 63) - 32 + j;
+                j2 = (k1 >> 12 & 63) - 32 + k;
+                k2 = this.b(enumskyblock, l1, i2, j2);
+                l2 = this.getTypeId(l1, i2, j2);
+                i3 = Block.lightBlock[l2];
+                if (i3 == 0) {
+                    i3 = 1;
                 }
 
                 boolean flag2 = false;
 
                 if (enumskyblock == EnumSkyBlock.SKY) {
-                    l2 = this.c(l1, j5, j2, k1, i2, k2);
+                    k3 = this.a(k2, l1, i2, j2, l2, i3);
                 } else {
-                    l2 = this.d(l1, j5, j2, k1, i2, k2);
+                    k3 = this.f(k2, l1, i2, j2, l2, i3);
                 }
 
-                if (l2 != l1) {
-                    this.a(enumskyblock, j5, j2, k1, l2);
-                    if (l2 > l1) {
-                        i3 = j5 - i;
-                        k3 = j2 - j;
-                        j3 = k1 - k;
-                        if (i3 < 0) {
-                            i3 = -i3;
+                if (k3 != k2) {
+                    this.b(enumskyblock, l1, i2, j2, k3);
+                    if (k3 > k2) {
+                        j3 = l1 - i;
+                        i4 = i2 - j;
+                        l3 = j2 - k;
+                        if (j3 < 0) {
+                            j3 = -j3;
                         }
 
-                        if (k3 < 0) {
-                            k3 = -k3;
+                        if (i4 < 0) {
+                            i4 = -i4;
                         }
 
-                        if (j3 < 0) {
-                            j3 = -j3;
+                        if (l3 < 0) {
+                            l3 = -l3;
                         }
 
-                        if (i3 + k3 + j3 < 17 && i1 < this.E.length - 6) {
-                            if (this.a(enumskyblock, j5 - 1, j2, k1) < l2) {
-                                this.E[i1++] = j5 - 1 - i + 32 + (j2 - j + 32 << 6) + (k1 - k + 32 << 12);
+                        if (j3 + i4 + l3 < 17 && i1 < this.J.length - 6) {
+                            if (this.b(enumskyblock, l1 - 1, i2, j2) < k3) {
+                                this.J[i1++] = l1 - 1 - i + 32 + (i2 - j + 32 << 6) + (j2 - k + 32 << 12);
                             }
 
-                            if (this.a(enumskyblock, j5 + 1, j2, k1) < l2) {
-                                this.E[i1++] = j5 + 1 - i + 32 + (j2 - j + 32 << 6) + (k1 - k + 32 << 12);
+                            if (this.b(enumskyblock, l1 + 1, i2, j2) < k3) {
+                                this.J[i1++] = l1 + 1 - i + 32 + (i2 - j + 32 << 6) + (j2 - k + 32 << 12);
                             }
 
-                            if (this.a(enumskyblock, j5, j2 - 1, k1) < l2) {
-                                this.E[i1++] = j5 - i + 32 + (j2 - 1 - j + 32 << 6) + (k1 - k + 32 << 12);
+                            if (this.b(enumskyblock, l1, i2 - 1, j2) < k3) {
+                                this.J[i1++] = l1 - i + 32 + (i2 - 1 - j + 32 << 6) + (j2 - k + 32 << 12);
                             }
 
-                            if (this.a(enumskyblock, j5, j2 + 1, k1) < l2) {
-                                this.E[i1++] = j5 - i + 32 + (j2 + 1 - j + 32 << 6) + (k1 - k + 32 << 12);
+                            if (this.b(enumskyblock, l1, i2 + 1, j2) < k3) {
+                                this.J[i1++] = l1 - i + 32 + (i2 + 1 - j + 32 << 6) + (j2 - k + 32 << 12);
                             }
 
-                            if (this.a(enumskyblock, j5, j2, k1 - 1) < l2) {
-                                this.E[i1++] = j5 - i + 32 + (j2 - j + 32 << 6) + (k1 - 1 - k + 32 << 12);
+                            if (this.b(enumskyblock, l1, i2, j2 - 1) < k3) {
+                                this.J[i1++] = l1 - i + 32 + (i2 - j + 32 << 6) + (j2 - 1 - k + 32 << 12);
                             }
 
-                            if (this.a(enumskyblock, j5, j2, k1 + 1) < l2) {
-                                this.E[i1++] = j5 - i + 32 + (j2 - j + 32 << 6) + (k1 + 1 - k + 32 << 12);
+                            if (this.b(enumskyblock, l1, i2, j2 + 1) < k3) {
+                                this.J[i1++] = l1 - i + 32 + (i2 - j + 32 << 6) + (j2 + 1 - k + 32 << 12);
                             }
                         }
                     }
                 }
             }
 
-            MethodProfiler.a();
+            this.methodProfiler.b();
         }
     }
 
     public boolean a(boolean flag) {
-        int i = this.H.size();
-
-        if (i != this.I.size()) {
-            throw new IllegalStateException("TickNextTick list out of synch");
-        } else {
-            if (i > 1000) {
-                i = 1000;
-            }
-
-            for (int j = 0; j < i; ++j) {
-                NextTickListEntry nextticklistentry = (NextTickListEntry) this.H.first();
-
-                if (!flag && nextticklistentry.e > this.worldData.getTime()) {
-                    break;
-                }
-
-                this.H.remove(nextticklistentry);
-                this.I.remove(nextticklistentry);
-                byte b0 = 8;
-
-                if (this.a(nextticklistentry.a - b0, nextticklistentry.b - b0, nextticklistentry.c - b0, nextticklistentry.a + b0, nextticklistentry.b + b0, nextticklistentry.c + b0)) {
-                    int k = this.getTypeId(nextticklistentry.a, nextticklistentry.b, nextticklistentry.c);
-
-                    if (k == nextticklistentry.d && k > 0) {
-                        Block.byId[k].a(this, nextticklistentry.a, nextticklistentry.b, nextticklistentry.c, this.random);
-                    }
-                }
-            }
-
-            return this.H.size() != 0;
-        }
+        return false;
     }
 
     public List a(Chunk chunk, boolean flag) {
-        ArrayList arraylist = null;
-        ChunkCoordIntPair chunkcoordintpair = chunk.k();
-        int i = chunkcoordintpair.x << 4;
-        int j = i + 16;
-        int k = chunkcoordintpair.z << 4;
-        int l = k + 16;
-        Iterator iterator = this.I.iterator();
-
-        while (iterator.hasNext()) {
-            NextTickListEntry nextticklistentry = (NextTickListEntry) iterator.next();
-
-            if (nextticklistentry.a >= i && nextticklistentry.a < j && nextticklistentry.c >= k && nextticklistentry.c < l) {
-                if (flag) {
-                    this.H.remove(nextticklistentry);
-                    iterator.remove();
-                }
-
-                if (arraylist == null) {
-                    arraylist = new ArrayList();
-                }
-
-                arraylist.add(nextticklistentry);
-            }
-        }
-
-        return arraylist;
+        return null;
     }
 
     public List getEntities(Entity entity, AxisAlignedBB axisalignedbb) {
-        this.S.clear();
+        this.N.clear();
         int i = MathHelper.floor((axisalignedbb.a - 2.0D) / 16.0D);
         int j = MathHelper.floor((axisalignedbb.d + 2.0D) / 16.0D);
         int k = MathHelper.floor((axisalignedbb.c - 2.0D) / 16.0D);
@@ -2243,12 +2058,12 @@ public class World implements IBlockAccess {
         for (int i1 = i; i1 <= j; ++i1) {
             for (int j1 = k; j1 <= l; ++j1) {
                 if (this.isChunkLoaded(i1, j1)) {
-                    this.getChunkAt(i1, j1).a(entity, axisalignedbb, this.S);
+                    this.getChunkAt(i1, j1).a(entity, axisalignedbb, this.N);
                 }
             }
         }
 
-        return this.S;
+        return this.N;
     }
 
     public List a(Class oclass, AxisAlignedBB axisalignedbb) {
@@ -2279,7 +2094,7 @@ public class World implements IBlockAccess {
             Entity entity2 = (Entity) iterator.next();
 
             if (entity2 != entity) {
-                double d1 = entity.j(entity2);
+                double d1 = entity.e(entity2);
 
                 if (d1 <= d0) {
                     entity1 = entity2;
@@ -2295,10 +2110,6 @@ public class World implements IBlockAccess {
         if (this.isLoaded(i, j, k)) {
             this.getChunkAtWorldCoords(i, k).e();
         }
-
-        for (int l = 0; l < this.u.size(); ++l) {
-            ((IWorldAccess) this.u.get(l)).a(i, j, k, tileentity);
-        }
     }
 
     public int a(Class oclass) {
@@ -2319,15 +2130,15 @@ public class World implements IBlockAccess {
         this.entityList.addAll(list);
 
         for (int i = 0; i < list.size(); ++i) {
-            this.c((Entity) list.get(i));
+            this.a((Entity) list.get(i));
         }
     }
 
     public void b(List list) {
-        this.G.addAll(list);
+        this.g.addAll(list);
     }
 
-    public boolean mayPlace(int i, int j, int k, int l, boolean flag, int i1) {
+    public boolean mayPlace(int i, int j, int k, int l, boolean flag, int i1, Entity entity) {
         int j1 = this.getTypeId(j, k, l);
         Block block = Block.byId[j1];
         Block block1 = Block.byId[i];
@@ -2337,10 +2148,10 @@ public class World implements IBlockAccess {
             axisalignedbb = null;
         }
 
-        if (axisalignedbb != null && !this.containsEntity(axisalignedbb)) {
+        if (axisalignedbb != null && !this.a(axisalignedbb, entity)) {
             return false;
         } else {
-            if (block != null && (block == Block.WATER || block == Block.STATIONARY_WATER || block == Block.LAVA || block == Block.STATIONARY_LAVA || block == Block.FIRE || block.material.isReplacable())) {
+            if (block != null && (block == Block.WATER || block == Block.STATIONARY_WATER || block == Block.LAVA || block == Block.STATIONARY_LAVA || block == Block.FIRE || block.material.isReplaceable())) {
                 block = null;
             }
 
@@ -2349,7 +2160,7 @@ public class World implements IBlockAccess {
     }
 
     public PathEntity findPath(Entity entity, Entity entity1, float f, boolean flag, boolean flag1, boolean flag2, boolean flag3) {
-        MethodProfiler.a("pathfind");
+        this.methodProfiler.a("pathfind");
         int i = MathHelper.floor(entity.locX);
         int j = MathHelper.floor(entity.locY + 1.0D);
         int k = MathHelper.floor(entity.locZ);
@@ -2363,12 +2174,12 @@ public class World implements IBlockAccess {
         ChunkCache chunkcache = new ChunkCache(this, i1, j1, k1, l1, i2, j2);
         PathEntity pathentity = (new Pathfinder(chunkcache, flag, flag1, flag2, flag3)).a(entity, entity1, f);
 
-        MethodProfiler.a();
+        this.methodProfiler.b();
         return pathentity;
     }
 
     public PathEntity a(Entity entity, int i, int j, int k, float f, boolean flag, boolean flag1, boolean flag2, boolean flag3) {
-        MethodProfiler.a("pathfind");
+        this.methodProfiler.a("pathfind");
         int l = MathHelper.floor(entity.locX);
         int i1 = MathHelper.floor(entity.locY);
         int j1 = MathHelper.floor(entity.locZ);
@@ -2382,14 +2193,14 @@ public class World implements IBlockAccess {
         ChunkCache chunkcache = new ChunkCache(this, l1, i2, j2, k2, l2, i3);
         PathEntity pathentity = (new Pathfinder(chunkcache, flag, flag1, flag2, flag3)).a(entity, i, j, k, f);
 
-        MethodProfiler.a();
+        this.methodProfiler.b();
         return pathentity;
     }
 
     public boolean isBlockFacePowered(int i, int j, int k, int l) {
         int i1 = this.getTypeId(i, j, k);
 
-        return i1 == 0 ? false : Block.byId[i1].d(this, i, j, k, l);
+        return i1 == 0 ? false : Block.byId[i1].c(this, i, j, k, l);
     }
 
     public boolean isBlockPowered(int i, int j, int k) {
@@ -2397,7 +2208,7 @@ public class World implements IBlockAccess {
     }
 
     public boolean isBlockFaceIndirectlyPowered(int i, int j, int k, int l) {
-        if (this.e(i, j, k)) {
+        if (this.s(i, j, k)) {
             return this.isBlockPowered(i, j, k);
         } else {
             int i1 = this.getTypeId(i, j, k);
@@ -2431,23 +2242,6 @@ public class World implements IBlockAccess {
         return entityhuman;
     }
 
-    public EntityHuman a(double d0, double d1, double d2) {
-        double d3 = -1.0D;
-        EntityHuman entityhuman = null;
-
-        for (int i = 0; i < this.players.size(); ++i) {
-            EntityHuman entityhuman1 = (EntityHuman) this.players.get(i);
-            double d4 = entityhuman1.e(d0, entityhuman1.locY, d1);
-
-            if ((d2 < 0.0D || d4 < d2 * d2) && (d3 == -1.0D || d4 < d3)) {
-                d3 = d4;
-                entityhuman = entityhuman1;
-            }
-        }
-
-        return entityhuman;
-    }
-
     public EntityHuman findNearbyVulnerablePlayer(Entity entity, double d0) {
         return this.findNearbyVulnerablePlayer(entity.locX, entity.locY, entity.locZ, d0);
     }
@@ -2482,24 +2276,12 @@ public class World implements IBlockAccess {
         return null;
     }
 
-    public void m() {
+    public void B() {
         this.dataManager.checkSession();
     }
 
     public void setTime(long i) {
-        this.worldData.a(i);
-    }
-
-    public void setTimeAndFixTicklists(long i) {
-        long j = i - this.worldData.getTime();
-
-        NextTickListEntry nextticklistentry;
-
-        for (Iterator iterator = this.I.iterator(); iterator.hasNext(); nextticklistentry.e += j) {
-            nextticklistentry = (NextTickListEntry) iterator.next();
-        }
-
-        this.setTime(i);
+        this.worldData.b(i);
     }
 
     public long getSeed() {
@@ -2520,15 +2302,13 @@ public class World implements IBlockAccess {
 
     public void broadcastEntityEffect(Entity entity, byte b0) {}
 
-    public IChunkProvider q() {
+    public IChunkProvider F() {
         return this.chunkProvider;
     }
 
-    public void playNote(int i, int j, int k, int l, int i1) {
-        int j1 = this.getTypeId(i, j, k);
-
-        if (j1 > 0) {
-            Block.byId[j1].a(this, i, j, k, l, i1);
+    public void playNote(int i, int j, int k, int l, int i1, int j1) {
+        if (l > 0) {
+            Block.byId[l].b(this, i, j, k, i1, j1);
         }
     }
 
@@ -2540,77 +2320,30 @@ public class World implements IBlockAccess {
         return this.worldData;
     }
 
-    public void everyoneSleeping() {
-        this.N = !this.players.isEmpty();
-        Iterator iterator = this.players.iterator();
-
-        while (iterator.hasNext()) {
-            EntityHuman entityhuman = (EntityHuman) iterator.next();
-
-            if (!entityhuman.isSleeping()) {
-                this.N = false;
-                break;
-            }
-        }
-    }
-
-    protected void u() {
-        this.N = false;
-        Iterator iterator = this.players.iterator();
-
-        while (iterator.hasNext()) {
-            EntityHuman entityhuman = (EntityHuman) iterator.next();
-
-            if (entityhuman.isSleeping()) {
-                entityhuman.a(false, false, true);
-            }
-        }
-
-        this.C();
-    }
-
-    public boolean everyoneDeeplySleeping() {
-        if (this.N && !this.isStatic) {
-            Iterator iterator = this.players.iterator();
-
-            EntityHuman entityhuman;
-
-            do {
-                if (!iterator.hasNext()) {
-                    return true;
-                }
-
-                entityhuman = (EntityHuman) iterator.next();
-            } while (entityhuman.isDeeplySleeping());
+    public void everyoneSleeping() {}
 
-            return false;
-        } else {
-            return false;
-        }
+    public float i(float f) {
+        return (this.p + (this.q - this.p) * f) * this.j(f);
     }
 
-    public float c(float f) {
-        return (this.k + (this.l - this.k) * f) * this.d(f);
-    }
-
-    public float d(float f) {
-        return this.i + (this.j - this.i) * f;
+    public float j(float f) {
+        return this.n + (this.o - this.n) * f;
     }
 
-    public boolean w() {
-        return (double) this.c(1.0F) > 0.9D;
+    public boolean I() {
+        return (double) this.i(1.0F) > 0.9D;
     }
 
-    public boolean x() {
-        return (double) this.d(1.0F) > 0.2D;
+    public boolean J() {
+        return (double) this.j(1.0F) > 0.2D;
     }
 
-    public boolean y(int i, int j, int k) {
-        if (!this.x()) {
+    public boolean B(int i, int j, int k) {
+        if (!this.J()) {
             return false;
-        } else if (!this.isChunkLoaded(i, j, k)) {
+        } else if (!this.j(i, j, k)) {
             return false;
-        } else if (this.f(i, k) > j) {
+        } else if (this.g(i, k) > j) {
             return false;
         } else {
             BiomeBase biomebase = this.getBiome(i, k);
@@ -2619,7 +2352,7 @@ public class World implements IBlockAccess {
         }
     }
 
-    public boolean z(int i, int j, int k) {
+    public boolean C(int i, int j, int k) {
         BiomeBase biomebase = this.getBiome(i, k);
 
         return biomebase.e();
@@ -2642,8 +2375,8 @@ public class World implements IBlockAccess {
     }
 
     public void a(EntityHuman entityhuman, int i, int j, int k, int l, int i1) {
-        for (int j1 = 0; j1 < this.u.size(); ++j1) {
-            ((IWorldAccess) this.u.get(j1)).a(entityhuman, i, j, k, l, i1);
+        for (int j1 = 0; j1 < this.x.size(); ++j1) {
+            ((IWorldAccess) this.x.get(j1)).a(entityhuman, i, j, k, l, i1);
         }
     }
 
@@ -2651,7 +2384,11 @@ public class World implements IBlockAccess {
         return 256;
     }
 
-    public Random A(int i, int j, int k) {
+    public int L() {
+        return this.worldProvider.e ? 128 : 256;
+    }
+
+    public Random D(int i, int j, int k) {
         long l = (long) i * 341873128712L + (long) j * 132897987541L + this.getWorldData().getSeed() + (long) k;
 
         this.random.setSeed(l);
@@ -2662,13 +2399,24 @@ public class World implements IBlockAccess {
         return false;
     }
 
-    public BiomeMeta a(EnumCreatureType enumcreaturetype, int i, int j, int k) {
-        List list = this.q().getMobsFor(enumcreaturetype, i, j, k);
+    public ChunkPosition b(String s, int i, int j, int k) {
+        return this.F().findNearestMapFeature(this, s, i, j, k);
+    }
 
-        return list != null && !list.isEmpty() ? (BiomeMeta) WeightedRandom.a(this.random, (Collection) list) : null;
+    public CrashReport a(CrashReport crashreport) {
+        crashreport.a("World " + this.worldData.getName() + " Entities", (Callable) (new CrashReportEntities(this)));
+        crashreport.a("World " + this.worldData.getName() + " Players", (Callable) (new CrashReportPlayers(this)));
+        crashreport.a("World " + this.worldData.getName() + " Chunk Stats", (Callable) (new CrashReportChunkStats(this)));
+        return crashreport;
     }
 
-    public ChunkPosition b(String s, int i, int j, int k) {
-        return this.q().findNearestMapFeature(this, s, i, j, k);
+    public void f(int i, int j, int k, int l, int i1) {
+        Iterator iterator = this.x.iterator();
+
+        while (iterator.hasNext()) {
+            IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
+
+            iworldaccess.a(i, j, k, l, i1);
+        }
     }
 }
