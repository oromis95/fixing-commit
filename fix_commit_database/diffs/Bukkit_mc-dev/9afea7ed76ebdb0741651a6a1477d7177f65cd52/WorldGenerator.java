@@ -25,10 +25,6 @@ public abstract class WorldGenerator {
     protected void setTypeAndData(World world, int i, int j, int k, int l, int i1) {
         if (this.a) {
             world.setTypeIdAndData(i, j, k, l, i1);
-        } else if (world.isLoaded(i, j, k) && world.getChunkAtWorldCoords(i, k).seenByPlayer) {
-            if (world.setRawTypeIdAndData(i, j, k, l, i1)) {
-                world.notify(i, j, k);
-            }
         } else {
             world.setRawTypeIdAndData(i, j, k, l, i1);
         }
