@@ -3,13 +3,14 @@ package net.minecraft.server;
 public class WorldType {
 
     public static final WorldType[] types = new WorldType[16];
-    public static final WorldType NORMAL = (new WorldType(0, "default", 1)).d();
+    public static final WorldType NORMAL = (new WorldType(0, "default", 1)).f();
     public static final WorldType FLAT = new WorldType(1, "flat");
-    public static final WorldType VERSION_1_1f = (new WorldType(8, "default_1_1", 0)).a(false);
+    public static final WorldType LARGE_BIOMES = new WorldType(2, "largeBiomes");
+    public static final WorldType NORMAL_1_1 = (new WorldType(8, "default_1_1", 0)).a(false);
     private final String name;
     private final int version;
-    private boolean g;
     private boolean h;
+    private boolean i;
 
     private WorldType(int i, String s) {
         this(i, s, 0);
@@ -18,7 +19,7 @@ public class WorldType {
     private WorldType(int i, String s, int j) {
         this.name = s;
         this.version = j;
-        this.g = true;
+        this.h = true;
         types[i] = this;
     }
 
@@ -31,27 +32,32 @@ public class WorldType {
     }
 
     public WorldType a(int i) {
-        return this == NORMAL && i == 0 ? VERSION_1_1f : this;
+        return this == NORMAL && i == 0 ? NORMAL_1_1 : this;
     }
 
     private WorldType a(boolean flag) {
-        this.g = flag;
+        this.h = flag;
         return this;
     }
 
-    private WorldType d() {
-        this.h = true;
+    private WorldType f() {
+        this.i = true;
         return this;
     }
 
-    public boolean c() {
-        return this.h;
+    public boolean e() {
+        return this.i;
     }
 
     public static WorldType getType(String s) {
-        for (int i = 0; i < types.length; ++i) {
-            if (types[i] != null && types[i].name.equalsIgnoreCase(s)) {
-                return types[i];
+        WorldType[] aworldtype = types;
+        int i = aworldtype.length;
+
+        for (int j = 0; j < i; ++j) {
+            WorldType worldtype = aworldtype[j];
+
+            if (worldtype != null && worldtype.name.equalsIgnoreCase(s)) {
+                return worldtype;
             }
         }
 
