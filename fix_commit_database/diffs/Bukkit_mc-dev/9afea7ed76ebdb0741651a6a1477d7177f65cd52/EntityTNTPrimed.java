@@ -7,8 +7,8 @@ public class EntityTNTPrimed extends Entity {
     public EntityTNTPrimed(World world) {
         super(world);
         this.fuseTicks = 0;
-        this.bf = true;
-        this.b(0.98F, 0.98F);
+        this.m = true;
+        this.a(0.98F, 0.98F);
         this.height = this.length / 2.0F;
     }
 
@@ -26,17 +26,17 @@ public class EntityTNTPrimed extends Entity {
         this.lastZ = d2;
     }
 
-    protected void b() {}
+    protected void a() {}
 
-    protected boolean g_() {
+    protected boolean e_() {
         return false;
     }
 
-    public boolean o_() {
+    public boolean L() {
         return !this.dead;
     }
 
-    public void F_() {
+    public void h_() {
         this.lastX = this.locX;
         this.lastY = this.locY;
         this.lastZ = this.locZ;
@@ -52,11 +52,9 @@ public class EntityTNTPrimed extends Entity {
         }
 
         if (this.fuseTicks-- <= 0) {
+            this.die();
             if (!this.world.isStatic) {
-                this.die();
                 this.explode();
-            } else {
-                this.die();
             }
         } else {
             this.world.a("smoke", this.locX, this.locY + 0.5D, this.locZ, 0.0D, 0.0D, 0.0D);
