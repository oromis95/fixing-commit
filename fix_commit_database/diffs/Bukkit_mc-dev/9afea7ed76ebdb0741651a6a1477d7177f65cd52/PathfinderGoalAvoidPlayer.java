@@ -19,7 +19,7 @@ public class PathfinderGoalAvoidPlayer extends PathfinderGoal {
         this.e = f;
         this.b = f1;
         this.c = f2;
-        this.g = entitycreature.al();
+        this.g = entitycreature.getNavigation();
         this.a(1);
     }
 
@@ -36,46 +36,46 @@ public class PathfinderGoalAvoidPlayer extends PathfinderGoal {
         } else {
             List list = this.a.world.a(this.h, this.a.boundingBox.grow((double) this.e, 3.0D, (double) this.e));
 
-            if (list.size() == 0) {
+            if (list.isEmpty()) {
                 return false;
             }
 
             this.d = (Entity) list.get(0);
         }
 
-        if (!this.a.am().canSee(this.d)) {
+        if (!this.a.at().canSee(this.d)) {
             return false;
         } else {
-            Vec3D vec3d = RandomPositionGenerator.b(this.a, 16, 7, Vec3D.create(this.d.locX, this.d.locY, this.d.locZ));
+            Vec3D vec3d = RandomPositionGenerator.b(this.a, 16, 7, Vec3D.a().create(this.d.locX, this.d.locY, this.d.locZ));
 
             if (vec3d == null) {
                 return false;
-            } else if (this.d.e(vec3d.a, vec3d.b, vec3d.c) < this.d.j(this.a)) {
+            } else if (this.d.e(vec3d.a, vec3d.b, vec3d.c) < this.d.e((Entity) this.a)) {
                 return false;
             } else {
                 this.f = this.g.a(vec3d.a, vec3d.b, vec3d.c);
-                return this.f == null ? false : this.f.a(vec3d);
+                return this.f == null ? false : this.f.b(vec3d);
             }
         }
     }
 
     public boolean b() {
-        return !this.g.e();
+        return !this.g.f();
     }
 
-    public void c() {
+    public void e() {
         this.g.a(this.f, this.b);
     }
 
-    public void d() {
+    public void c() {
         this.d = null;
     }
 
-    public void e() {
-        if (this.a.j(this.d) < 49.0D) {
-            this.a.al().a(this.c);
+    public void d() {
+        if (this.a.e(this.d) < 49.0D) {
+            this.a.getNavigation().a(this.c);
         } else {
-            this.a.al().a(this.b);
+            this.a.getNavigation().a(this.b);
         }
     }
 }
