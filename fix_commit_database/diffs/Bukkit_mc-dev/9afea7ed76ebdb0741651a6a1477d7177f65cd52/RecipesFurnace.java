@@ -3,42 +3,49 @@ package net.minecraft.server;
 import java.util.HashMap;
 import java.util.Map;
 
-public class FurnaceRecipes {
+public class RecipesFurnace {
 
-    private static final FurnaceRecipes a = new FurnaceRecipes();
-    private Map recipies = new HashMap();
+    private static final RecipesFurnace a = new RecipesFurnace();
+    private Map recipes = new HashMap();
+    private Map c = new HashMap();
 
-    public static final FurnaceRecipes getInstance() {
+    public static final RecipesFurnace getInstance() {
         return a;
     }
 
-    private FurnaceRecipes() {
-        this.registerRecipe(Block.IRON_ORE.id, new ItemStack(Item.IRON_INGOT));
-        this.registerRecipe(Block.GOLD_ORE.id, new ItemStack(Item.GOLD_INGOT));
-        this.registerRecipe(Block.DIAMOND_ORE.id, new ItemStack(Item.DIAMOND));
-        this.registerRecipe(Block.SAND.id, new ItemStack(Block.GLASS));
-        this.registerRecipe(Item.PORK.id, new ItemStack(Item.GRILLED_PORK));
-        this.registerRecipe(Item.RAW_BEEF.id, new ItemStack(Item.COOKED_BEEF));
-        this.registerRecipe(Item.RAW_CHICKEN.id, new ItemStack(Item.COOKED_CHICKEN));
-        this.registerRecipe(Item.RAW_FISH.id, new ItemStack(Item.COOKED_FISH));
-        this.registerRecipe(Block.COBBLESTONE.id, new ItemStack(Block.STONE));
-        this.registerRecipe(Item.CLAY_BALL.id, new ItemStack(Item.CLAY_BRICK));
-        this.registerRecipe(Block.CACTUS.id, new ItemStack(Item.INK_SACK, 1, 2));
-        this.registerRecipe(Block.LOG.id, new ItemStack(Item.COAL, 1, 1));
-        this.registerRecipe(Block.COAL_ORE.id, new ItemStack(Item.COAL));
-        this.registerRecipe(Block.REDSTONE_ORE.id, new ItemStack(Item.REDSTONE));
-        this.registerRecipe(Block.LAPIS_ORE.id, new ItemStack(Item.INK_SACK, 1, 4));
+    private RecipesFurnace() {
+        this.registerRecipe(Block.IRON_ORE.id, new ItemStack(Item.IRON_INGOT), 0.7F);
+        this.registerRecipe(Block.GOLD_ORE.id, new ItemStack(Item.GOLD_INGOT), 1.0F);
+        this.registerRecipe(Block.DIAMOND_ORE.id, new ItemStack(Item.DIAMOND), 1.0F);
+        this.registerRecipe(Block.SAND.id, new ItemStack(Block.GLASS), 0.1F);
+        this.registerRecipe(Item.PORK.id, new ItemStack(Item.GRILLED_PORK), 0.3F);
+        this.registerRecipe(Item.RAW_BEEF.id, new ItemStack(Item.COOKED_BEEF), 0.3F);
+        this.registerRecipe(Item.RAW_CHICKEN.id, new ItemStack(Item.COOKED_CHICKEN), 0.3F);
+        this.registerRecipe(Item.RAW_FISH.id, new ItemStack(Item.COOKED_FISH), 0.3F);
+        this.registerRecipe(Block.COBBLESTONE.id, new ItemStack(Block.STONE), 0.1F);
+        this.registerRecipe(Item.CLAY_BALL.id, new ItemStack(Item.CLAY_BRICK), 0.2F);
+        this.registerRecipe(Block.CACTUS.id, new ItemStack(Item.INK_SACK, 1, 2), 0.2F);
+        this.registerRecipe(Block.LOG.id, new ItemStack(Item.COAL, 1, 1), 0.1F);
+        this.registerRecipe(Block.EMERALD_ORE.id, new ItemStack(Item.EMERALD), 1.0F);
+        this.registerRecipe(Block.COAL_ORE.id, new ItemStack(Item.COAL), 0.1F);
+        this.registerRecipe(Block.REDSTONE_ORE.id, new ItemStack(Item.REDSTONE), 0.7F);
+        this.registerRecipe(Block.LAPIS_ORE.id, new ItemStack(Item.INK_SACK, 1, 4), 0.2F);
     }
 
-    public void registerRecipe(int i, ItemStack itemstack) {
-        this.recipies.put(Integer.valueOf(i), itemstack);
+    public void registerRecipe(int i, ItemStack itemstack, float f) {
+        this.recipes.put(Integer.valueOf(i), itemstack);
+        this.c.put(Integer.valueOf(itemstack.id), Float.valueOf(f));
     }
 
     public ItemStack getResult(int i) {
-        return (ItemStack) this.recipies.get(Integer.valueOf(i));
+        return (ItemStack) this.recipes.get(Integer.valueOf(i));
     }
 
-    public Map getRecipies() {
-        return this.recipies;
+    public Map getRecipes() {
+        return this.recipes;
+    }
+
+    public float c(int i) {
+        return this.c.containsKey(Integer.valueOf(i)) ? ((Float) this.c.get(Integer.valueOf(i))).floatValue() : 0.0F;
     }
 }
