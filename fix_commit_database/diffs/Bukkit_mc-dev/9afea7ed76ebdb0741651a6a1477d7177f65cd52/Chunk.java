@@ -90,22 +90,22 @@ public class Chunk {
         return this.heightMap[j << 4 | i];
     }
 
-    public int g() {
+    public int h() {
         for (int i = this.sections.length - 1; i >= 0; --i) {
             if (this.sections[i] != null) {
-                return this.sections[i].c();
+                return this.sections[i].d();
             }
         }
 
         return 0;
     }
 
-    public ChunkSection[] h() {
+    public ChunkSection[] i() {
         return this.sections;
     }
 
     public void initLighting() {
-        int i = this.g();
+        int i = this.h();
 
         int j;
         int k;
@@ -138,7 +138,7 @@ public class Chunk {
 
                                 if (chunksection != null) {
                                     chunksection.c(j, i1 & 15, k, l);
-                                    this.world.o((this.x << 4) + j, i1, (this.z << 4) + k);
+                                    this.world.n((this.x << 4) + j, i1, (this.z << 4) + k);
                                 }
                             }
 
@@ -161,15 +161,13 @@ public class Chunk {
         }
     }
 
-    public void loadNOP() {}
-
     private void e(int i, int j) {
         this.c[i + j * 16] = true;
         this.s = true;
     }
 
-    private void o() {
-        MethodProfiler.a("recheckGaps");
+    private void q() {
+        this.world.methodProfiler.a("recheckGaps");
         if (this.world.areChunksLoaded(this.x * 16 + 8, 0, this.z * 16 + 8, 16)) {
             for (int i = 0; i < 16; ++i) {
                 for (int j = 0; j < 16; ++j) {
@@ -207,7 +205,7 @@ public class Chunk {
             this.s = false;
         }
 
-        MethodProfiler.a();
+        this.world.methodProfiler.b();
     }
 
     private void g(int i, int j, int k) {
@@ -223,7 +221,7 @@ public class Chunk {
     private void d(int i, int j, int k, int l) {
         if (l > k && this.world.areChunksLoaded(i, 0, j, 16)) {
             for (int i1 = k; i1 < l; ++i1) {
-                this.world.b(EnumSkyBlock.SKY, i, i1, j);
+                this.world.c(EnumSkyBlock.SKY, i, i1, j);
             }
 
             this.l = true;
@@ -258,7 +256,7 @@ public class Chunk {
                         chunksection = this.sections[l1 >> 4];
                         if (chunksection != null) {
                             chunksection.c(i, l1 & 15, k, 15);
-                            this.world.o((this.x << 4) + i, l1, (this.z << 4) + k);
+                            this.world.n((this.x << 4) + i, l1, (this.z << 4) + k);
                         }
                     }
                 } else {
@@ -266,7 +264,7 @@ public class Chunk {
                         chunksection = this.sections[l1 >> 4];
                         if (chunksection != null) {
                             chunksection.c(i, l1 & 15, k, 0);
-                            this.world.o((this.x << 4) + i, l1, (this.z << 4) + k);
+                            this.world.n((this.x << 4) + i, l1, (this.z << 4) + k);
                         }
                     }
                 }
@@ -351,8 +349,9 @@ public class Chunk {
 
         int k1 = this.heightMap[j1];
         int l1 = this.getTypeId(i, j, k);
+        int i2 = this.getData(i, j, k);
 
-        if (l1 == l && this.getData(i, j, k) == i1) {
+        if (l1 == l && i2 == i1) {
             return false;
         } else {
             ChunkSection chunksection = this.sections[j >> 4];
@@ -367,15 +366,19 @@ public class Chunk {
                 flag = j >= k1;
             }
 
-            chunksection.a(i, j & 15, k, l);
-            int i2 = this.x * 16 + i;
-            int j2 = this.z * 16 + k;
+            int j2 = this.x * 16 + i;
+            int k2 = this.z * 16 + k;
+
+            if (l1 != 0 && !this.world.isStatic) {
+                Block.byId[l1].h(this.world, j2, j, k2, i2);
+            }
 
+            chunksection.a(i, j & 15, k, l);
             if (l1 != 0) {
                 if (!this.world.isStatic) {
-                    Block.byId[l1].remove(this.world, i2, j, j2);
+                    Block.byId[l1].remove(this.world, j2, j, k2, l1, i2);
                 } else if (Block.byId[l1] instanceof BlockContainer && l1 != l) {
-                    this.world.q(i2, j, j2);
+                    this.world.q(j2, j, k2);
                 }
             }
 
@@ -401,14 +404,14 @@ public class Chunk {
 
                 if (l != 0) {
                     if (!this.world.isStatic) {
-                        Block.byId[l].onPlace(this.world, i2, j, j2);
+                        Block.byId[l].onPlace(this.world, j2, j, k2);
                     }
 
                     if (Block.byId[l] instanceof BlockContainer) {
                         tileentity = this.e(i, j, k);
                         if (tileentity == null) {
-                            tileentity = ((BlockContainer) Block.byId[l]).a_();
-                            this.world.setTileEntity(i2, j, j2, tileentity);
+                            tileentity = ((BlockContainer) Block.byId[l]).a(this.world);
+                            this.world.setTileEntity(j2, j, k2, tileentity);
                         }
 
                         if (tileentity != null) {
@@ -460,7 +463,7 @@ public class Chunk {
     public int getBrightness(EnumSkyBlock enumskyblock, int i, int j, int k) {
         ChunkSection chunksection = this.sections[j >> 4];
 
-        return chunksection == null ? enumskyblock.c : (enumskyblock == EnumSkyBlock.SKY ? chunksection.c(i, j & 15, k) : (enumskyblock == EnumSkyBlock.BLOCK ? chunksection.d(i, j & 15, k) : enumskyblock.c));
+        return chunksection == null ? (this.d(i, j, k) ? enumskyblock.c : 0) : (enumskyblock == EnumSkyBlock.SKY ? chunksection.c(i, j & 15, k) : (enumskyblock == EnumSkyBlock.BLOCK ? chunksection.d(i, j & 15, k) : enumskyblock.c));
     }
 
     public void a(EnumSkyBlock enumskyblock, int i, int j, int k, int l) {
@@ -476,11 +479,7 @@ public class Chunk {
             if (!this.world.worldProvider.e) {
                 chunksection.c(i, j & 15, k, l);
             }
-        } else {
-            if (enumskyblock != EnumSkyBlock.BLOCK) {
-                return;
-            }
-
+        } else if (enumskyblock == EnumSkyBlock.BLOCK) {
             chunksection.d(i, j & 15, k, l);
         }
     }
@@ -528,15 +527,15 @@ public class Chunk {
             k = this.entitySlices.length - 1;
         }
 
-        entity.bZ = true;
-        entity.ca = this.x;
-        entity.cb = k;
-        entity.cc = this.z;
+        entity.ag = true;
+        entity.ah = this.x;
+        entity.ai = k;
+        entity.aj = this.z;
         this.entitySlices[k].add(entity);
     }
 
     public void b(Entity entity) {
-        this.a(entity, entity.cb);
+        this.a(entity, entity.ai);
     }
 
     public void a(Entity entity, int i) {
@@ -562,19 +561,19 @@ public class Chunk {
         if (tileentity == null) {
             int l = this.getTypeId(i, j, k);
 
-            if (l <= 0 || !Block.byId[l].o()) {
+            if (l <= 0 || !Block.byId[l].s()) {
                 return null;
             }
 
             if (tileentity == null) {
-                tileentity = ((BlockContainer) Block.byId[l]).a_();
+                tileentity = ((BlockContainer) Block.byId[l]).a(this.world);
                 this.world.setTileEntity(this.x * 16 + i, j, this.z * 16 + k, tileentity);
             }
 
             tileentity = (TileEntity) this.tileEntities.get(chunkposition);
         }
 
-        if (tileentity != null && tileentity.l()) {
+        if (tileentity != null && tileentity.p()) {
             this.tileEntities.remove(chunkposition);
             return null;
         } else {
@@ -596,12 +595,12 @@ public class Chunk {
     public void a(int i, int j, int k, TileEntity tileentity) {
         ChunkPosition chunkposition = new ChunkPosition(i, j, k);
 
-        tileentity.world = this.world;
+        tileentity.a(this.world);
         tileentity.x = this.x * 16 + i;
         tileentity.y = j;
         tileentity.z = this.z * 16 + k;
         if (this.getTypeId(i, j, k) != 0 && Block.byId[this.getTypeId(i, j, k)] instanceof BlockContainer) {
-            tileentity.m();
+            tileentity.q();
             this.tileEntities.put(chunkposition, tileentity);
         }
     }
@@ -621,9 +620,13 @@ public class Chunk {
     public void addEntities() {
         this.d = true;
         this.world.a(this.tileEntities.values());
+        List[] alist = this.entitySlices;
+        int i = alist.length;
 
-        for (int i = 0; i < this.entitySlices.length; ++i) {
-            this.world.a(this.entitySlices[i]);
+        for (int j = 0; j < i; ++j) {
+            List list = alist[j];
+
+            this.world.a(list);
         }
     }
 
@@ -637,8 +640,13 @@ public class Chunk {
             this.world.a(tileentity);
         }
 
-        for (int i = 0; i < this.entitySlices.length; ++i) {
-            this.world.b(this.entitySlices[i]);
+        List[] alist = this.entitySlices;
+        int i = alist.length;
+
+        for (int j = 0; j < i; ++j) {
+            List list = alist[j];
+
+            this.world.b(list);
         }
     }
 
@@ -660,17 +668,18 @@ public class Chunk {
 
         for (int k = i; k <= j; ++k) {
             List list1 = this.entitySlices[k];
+            Iterator iterator = list1.iterator();
 
-            for (int l = 0; l < list1.size(); ++l) {
-                Entity entity1 = (Entity) list1.get(l);
+            while (iterator.hasNext()) {
+                Entity entity1 = (Entity) iterator.next();
 
                 if (entity1 != entity && entity1.boundingBox.a(axisalignedbb)) {
                     list.add(entity1);
-                    Entity[] aentity = entity1.bb();
+                    Entity[] aentity = entity1.al();
 
                     if (aentity != null) {
-                        for (int i1 = 0; i1 < aentity.length; ++i1) {
-                            entity1 = aentity[i1];
+                        for (int l = 0; l < aentity.length; ++l) {
+                            entity1 = aentity[l];
                             if (entity1 != entity && entity1.boundingBox.a(axisalignedbb)) {
                                 list.add(entity1);
                             }
@@ -699,9 +708,10 @@ public class Chunk {
 
         for (int k = i; k <= j; ++k) {
             List list1 = this.entitySlices[k];
+            Iterator iterator = list1.iterator();
 
-            for (int l = 0; l < list1.size(); ++l) {
-                Entity entity = (Entity) list1.get(l);
+            while (iterator.hasNext()) {
+                Entity entity = (Entity) iterator.next();
 
                 if (oclass.isAssignableFrom(entity.getClass()) && entity.boundingBox.a(axisalignedbb)) {
                     list.add(entity);
@@ -730,19 +740,6 @@ public class Chunk {
         return false;
     }
 
-    public void i() {
-        ChunkSection[] achunksection = this.sections;
-        int i = achunksection.length;
-
-        for (int j = 0; j < i; ++j) {
-            ChunkSection chunksection = achunksection[j];
-
-            if (chunksection != null) {
-                chunksection.e();
-            }
-        }
-    }
-
     public void a(IChunkProvider ichunkprovider, IChunkProvider ichunkprovider1, int i, int j) {
         if (!this.done && ichunkprovider.isChunkLoaded(i + 1, j + 1) && ichunkprovider.isChunkLoaded(i, j + 1) && ichunkprovider.isChunkLoaded(i + 1, j)) {
             ichunkprovider.getChunkAt(ichunkprovider1, i, j);
@@ -766,7 +763,7 @@ public class Chunk {
         int l = this.b[k];
 
         if (l == -999) {
-            int i1 = this.g() + 15;
+            int i1 = this.h() + 15;
 
             l = -1;
 
@@ -787,13 +784,13 @@ public class Chunk {
         return l;
     }
 
-    public void j() {
+    public void k() {
         if (this.s && !this.world.worldProvider.e) {
-            this.o();
+            this.q();
         }
     }
 
-    public ChunkCoordIntPair k() {
+    public ChunkCoordIntPair l() {
         return new ChunkCoordIntPair(this.x, this.z);
     }
 
@@ -834,7 +831,7 @@ public class Chunk {
         return BiomeBase.biomes[k] == null ? BiomeBase.PLAINS : BiomeBase.biomes[k];
     }
 
-    public byte[] l() {
+    public byte[] m() {
         return this.r;
     }
 
@@ -842,11 +839,11 @@ public class Chunk {
         this.r = abyte;
     }
 
-    public void m() {
+    public void n() {
         this.t = 0;
     }
 
-    public void n() {
+    public void o() {
         for (int i = 0; i < 8; ++i) {
             if (this.t >= 4096) {
                 return;
@@ -865,30 +862,30 @@ public class Chunk {
 
                 if (this.sections[j] == null && (k1 == 0 || k1 == 15 || k == 0 || k == 15 || l == 0 || l == 15) || this.sections[j] != null && this.sections[j].a(k, k1, l) == 0) {
                     if (Block.lightEmission[this.world.getTypeId(i1, l1 - 1, j1)] > 0) {
-                        this.world.v(i1, l1 - 1, j1);
+                        this.world.x(i1, l1 - 1, j1);
                     }
 
                     if (Block.lightEmission[this.world.getTypeId(i1, l1 + 1, j1)] > 0) {
-                        this.world.v(i1, l1 + 1, j1);
+                        this.world.x(i1, l1 + 1, j1);
                     }
 
                     if (Block.lightEmission[this.world.getTypeId(i1 - 1, l1, j1)] > 0) {
-                        this.world.v(i1 - 1, l1, j1);
+                        this.world.x(i1 - 1, l1, j1);
                     }
 
                     if (Block.lightEmission[this.world.getTypeId(i1 + 1, l1, j1)] > 0) {
-                        this.world.v(i1 + 1, l1, j1);
+                        this.world.x(i1 + 1, l1, j1);
                     }
 
                     if (Block.lightEmission[this.world.getTypeId(i1, l1, j1 - 1)] > 0) {
-                        this.world.v(i1, l1, j1 - 1);
+                        this.world.x(i1, l1, j1 - 1);
                     }
 
                     if (Block.lightEmission[this.world.getTypeId(i1, l1, j1 + 1)] > 0) {
-                        this.world.v(i1, l1, j1 + 1);
+                        this.world.x(i1, l1, j1 + 1);
                     }
 
-                    this.world.v(i1, l1, j1);
+                    this.world.x(i1, l1, j1);
                 }
             }
         }
