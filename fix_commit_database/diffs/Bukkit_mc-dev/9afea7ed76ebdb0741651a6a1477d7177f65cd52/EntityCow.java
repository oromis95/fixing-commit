@@ -5,8 +5,8 @@ public class EntityCow extends EntityAnimal {
     public EntityCow(World world) {
         super(world);
         this.texture = "/mob/cow.png";
-        this.b(0.9F, 1.3F);
-        this.al().a(true);
+        this.a(0.9F, 1.3F);
+        this.getNavigation().a(true);
         this.goalSelector.a(0, new PathfinderGoalFloat(this));
         this.goalSelector.a(1, new PathfinderGoalPanic(this, 0.38F));
         this.goalSelector.a(2, new PathfinderGoalBreed(this, 0.2F));
@@ -17,7 +17,7 @@ public class EntityCow extends EntityAnimal {
         this.goalSelector.a(7, new PathfinderGoalRandomLookaround(this));
     }
 
-    public boolean c_() {
+    public boolean aV() {
         return true;
     }
 
@@ -25,27 +25,19 @@ public class EntityCow extends EntityAnimal {
         return 10;
     }
 
-    public void b(NBTTagCompound nbttagcompound) {
-        super.b(nbttagcompound);
-    }
-
-    public void a(NBTTagCompound nbttagcompound) {
-        super.a(nbttagcompound);
-    }
-
-    protected String i() {
+    protected String aQ() {
         return "mob.cow";
     }
 
-    protected String j() {
+    protected String aR() {
         return "mob.cowhurt";
     }
 
-    protected String k() {
+    protected String aS() {
         return "mob.cowhurt";
     }
 
-    protected float p() {
+    protected float aP() {
         return 0.4F;
     }
 
@@ -73,14 +65,19 @@ public class EntityCow extends EntityAnimal {
         }
     }
 
-    public boolean b(EntityHuman entityhuman) {
+    public boolean c(EntityHuman entityhuman) {
         ItemStack itemstack = entityhuman.inventory.getItemInHand();
 
         if (itemstack != null && itemstack.id == Item.BUCKET.id) {
-            entityhuman.inventory.setItem(entityhuman.inventory.itemInHandIndex, new ItemStack(Item.MILK_BUCKET));
+            if (--itemstack.count <= 0) {
+                entityhuman.inventory.setItem(entityhuman.inventory.itemInHandIndex, new ItemStack(Item.MILK_BUCKET));
+            } else if (!entityhuman.inventory.pickup(new ItemStack(Item.MILK_BUCKET))) {
+                entityhuman.drop(new ItemStack(Item.MILK_BUCKET.id, 1, 0));
+            }
+
             return true;
         } else {
-            return super.b(entityhuman);
+            return super.c(entityhuman);
         }
     }
 
