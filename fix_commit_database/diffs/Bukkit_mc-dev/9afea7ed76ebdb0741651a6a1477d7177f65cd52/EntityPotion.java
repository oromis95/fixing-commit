@@ -21,15 +21,15 @@ public class EntityPotion extends EntityProjectile {
         this.d = i;
     }
 
-    protected float e() {
+    protected float h() {
         return 0.05F;
     }
 
-    protected float c() {
+    protected float d() {
         return 0.5F;
     }
 
-    protected float d() {
+    protected float g() {
         return -20.0F;
     }
 
@@ -39,7 +39,7 @@ public class EntityPotion extends EntityProjectile {
 
     protected void a(MovingObjectPosition movingobjectposition) {
         if (!this.world.isStatic) {
-            List list = Item.POTION.b(this.d);
+            List list = Item.POTION.f(this.d);
 
             if (list != null && !list.isEmpty()) {
                 AxisAlignedBB axisalignedbb = this.boundingBox.grow(4.0D, 2.0D, 4.0D);
@@ -49,13 +49,13 @@ public class EntityPotion extends EntityProjectile {
                     Iterator iterator = list1.iterator();
 
                     while (iterator.hasNext()) {
-                        Entity entity = (Entity) iterator.next();
-                        double d0 = this.j(entity);
+                        EntityLiving entityliving = (EntityLiving) iterator.next();
+                        double d0 = this.e(entityliving);
 
                         if (d0 < 16.0D) {
                             double d1 = 1.0D - Math.sqrt(d0) / 4.0D;
 
-                            if (entity == movingobjectposition.entity) {
+                            if (entityliving == movingobjectposition.entity) {
                                 d1 = 1.0D;
                             }
 
@@ -66,12 +66,12 @@ public class EntityPotion extends EntityProjectile {
                                 int i = mobeffect.getEffectId();
 
                                 if (MobEffectList.byId[i].isInstant()) {
-                                    MobEffectList.byId[i].applyInstantEffect(this.shooter, (EntityLiving) entity, mobeffect.getAmplifier(), d1);
+                                    MobEffectList.byId[i].applyInstantEffect(this.shooter, entityliving, mobeffect.getAmplifier(), d1);
                                 } else {
                                     int j = (int) (d1 * (double) mobeffect.getDuration() + 0.5D);
 
                                     if (j > 20) {
-                                        ((EntityLiving) entity).addEffect(new MobEffect(i, j, mobeffect.getAmplifier()));
+                                        entityliving.addEffect(new MobEffect(i, j, mobeffect.getAmplifier()));
                                     }
                                 }
                             }
@@ -84,4 +84,14 @@ public class EntityPotion extends EntityProjectile {
             this.die();
         }
     }
+
+    public void a(NBTTagCompound nbttagcompound) {
+        super.a(nbttagcompound);
+        this.d = nbttagcompound.getInt("potionValue");
+    }
+
+    public void b(NBTTagCompound nbttagcompound) {
+        super.b(nbttagcompound);
+        nbttagcompound.setInt("potionValue", this.d);
+    }
 }
