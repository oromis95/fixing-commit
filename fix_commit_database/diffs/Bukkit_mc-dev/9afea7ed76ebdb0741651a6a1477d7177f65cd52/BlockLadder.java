@@ -6,6 +6,7 @@ public class BlockLadder extends Block {
 
     protected BlockLadder(int i, int j) {
         super(i, j, Material.ORIENTABLE);
+        this.a(CreativeModeTab.c);
     }
 
     public AxisAlignedBB e(World world, int i, int j, int k) {
@@ -31,38 +32,38 @@ public class BlockLadder extends Block {
         return super.e(world, i, j, k);
     }
 
-    public boolean a() {
+    public boolean d() {
         return false;
     }
 
-    public boolean b() {
+    public boolean c() {
         return false;
     }
 
-    public int c() {
+    public int b() {
         return 8;
     }
 
     public boolean canPlace(World world, int i, int j, int k) {
-        return world.e(i - 1, j, k) ? true : (world.e(i + 1, j, k) ? true : (world.e(i, j, k - 1) ? true : world.e(i, j, k + 1)));
+        return world.s(i - 1, j, k) ? true : (world.s(i + 1, j, k) ? true : (world.s(i, j, k - 1) ? true : world.s(i, j, k + 1)));
     }
 
-    public void postPlace(World world, int i, int j, int k, int l) {
+    public void postPlace(World world, int i, int j, int k, int l, float f, float f1, float f2) {
         int i1 = world.getData(i, j, k);
 
-        if ((i1 == 0 || l == 2) && world.e(i, j, k + 1)) {
+        if ((i1 == 0 || l == 2) && world.s(i, j, k + 1)) {
             i1 = 2;
         }
 
-        if ((i1 == 0 || l == 3) && world.e(i, j, k - 1)) {
+        if ((i1 == 0 || l == 3) && world.s(i, j, k - 1)) {
             i1 = 3;
         }
 
-        if ((i1 == 0 || l == 4) && world.e(i + 1, j, k)) {
+        if ((i1 == 0 || l == 4) && world.s(i + 1, j, k)) {
             i1 = 4;
         }
 
-        if ((i1 == 0 || l == 5) && world.e(i - 1, j, k)) {
+        if ((i1 == 0 || l == 5) && world.s(i - 1, j, k)) {
             i1 = 5;
         }
 
@@ -73,24 +74,24 @@ public class BlockLadder extends Block {
         int i1 = world.getData(i, j, k);
         boolean flag = false;
 
-        if (i1 == 2 && world.e(i, j, k + 1)) {
+        if (i1 == 2 && world.s(i, j, k + 1)) {
             flag = true;
         }
 
-        if (i1 == 3 && world.e(i, j, k - 1)) {
+        if (i1 == 3 && world.s(i, j, k - 1)) {
             flag = true;
         }
 
-        if (i1 == 4 && world.e(i + 1, j, k)) {
+        if (i1 == 4 && world.s(i + 1, j, k)) {
             flag = true;
         }
 
-        if (i1 == 5 && world.e(i - 1, j, k)) {
+        if (i1 == 5 && world.s(i - 1, j, k)) {
             flag = true;
         }
 
         if (!flag) {
-            this.b(world, i, j, k, i1, 0);
+            this.c(world, i, j, k, i1, 0);
             world.setTypeId(i, j, k, 0);
         }
 
