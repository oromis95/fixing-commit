@@ -7,20 +7,21 @@ public class BlockSnow extends Block {
     protected BlockSnow(int i, int j) {
         super(i, j, Material.SNOW_LAYER);
         this.a(0.0F, 0.0F, 0.0F, 1.0F, 0.125F, 1.0F);
-        this.a(true);
+        this.b(true);
+        this.a(CreativeModeTab.c);
     }
 
     public AxisAlignedBB e(World world, int i, int j, int k) {
         int l = world.getData(i, j, k) & 7;
 
-        return l >= 3 ? AxisAlignedBB.b((double) i + this.minX, (double) j + this.minY, (double) k + this.minZ, (double) i + this.maxX, (double) ((float) j + 0.5F), (double) k + this.maxZ) : null;
+        return l >= 3 ? AxisAlignedBB.a().a((double) i + this.minX, (double) j + this.minY, (double) k + this.minZ, (double) i + this.maxX, (double) ((float) j + 0.5F), (double) k + this.maxZ) : null;
     }
 
-    public boolean a() {
+    public boolean d() {
         return false;
     }
 
-    public boolean b() {
+    public boolean c() {
         return false;
     }
 
@@ -34,16 +35,16 @@ public class BlockSnow extends Block {
     public boolean canPlace(World world, int i, int j, int k) {
         int l = world.getTypeId(i, j - 1, k);
 
-        return l != 0 && (l == Block.LEAVES.id || Block.byId[l].a()) ? world.getMaterial(i, j - 1, k).isSolid() : false;
+        return l != 0 && (l == Block.LEAVES.id || Block.byId[l].d()) ? world.getMaterial(i, j - 1, k).isSolid() : false;
     }
 
     public void doPhysics(World world, int i, int j, int k, int l) {
-        this.g(world, i, j, k);
+        this.n(world, i, j, k);
     }
 
-    private boolean g(World world, int i, int j, int k) {
+    private boolean n(World world, int i, int j, int k) {
         if (!this.canPlace(world, i, j, k)) {
-            this.b(world, i, j, k, world.getData(i, j, k), 0);
+            this.c(world, i, j, k, world.getData(i, j, k), 0);
             world.setTypeId(i, j, k, 0);
             return false;
         } else {
@@ -73,9 +74,9 @@ public class BlockSnow extends Block {
         return 0;
     }
 
-    public void a(World world, int i, int j, int k, Random random) {
-        if (world.a(EnumSkyBlock.BLOCK, i, j, k) > 11) {
-            this.b(world, i, j, k, world.getData(i, j, k), 0);
+    public void b(World world, int i, int j, int k, Random random) {
+        if (world.b(EnumSkyBlock.BLOCK, i, j, k) > 11) {
+            this.c(world, i, j, k, world.getData(i, j, k), 0);
             world.setTypeId(i, j, k, 0);
         }
     }
