@@ -1,7 +1,6 @@
 package net.minecraft.server;
 
 import java.io.File;
-import java.util.List;
 
 public interface IDataManager {
 
@@ -11,13 +10,15 @@ public interface IDataManager {
 
     IChunkLoader createChunkLoader(WorldProvider worldprovider);
 
-    void saveWorldData(WorldData worlddata, List list);
+    void saveWorldData(WorldData worlddata, NBTTagCompound nbttagcompound);
 
     void saveWorldData(WorldData worlddata);
 
     PlayerFileData getPlayerFileData();
 
-    void e();
+    void a();
 
     File getDataFile(String s);
+
+    String g();
 }
