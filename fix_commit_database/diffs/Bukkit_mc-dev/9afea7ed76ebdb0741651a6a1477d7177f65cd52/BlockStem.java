@@ -9,20 +9,21 @@ public class BlockStem extends BlockFlower {
     protected BlockStem(int i, Block block) {
         super(i, 111);
         this.blockFruit = block;
-        this.a(true);
+        this.b(true);
         float f = 0.125F;
 
         this.a(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, 0.25F, 0.5F + f);
+        this.a((CreativeModeTab) null);
     }
 
-    protected boolean d(int i) {
+    protected boolean d_(int i) {
         return i == Block.SOIL.id;
     }
 
-    public void a(World world, int i, int j, int k, Random random) {
-        super.a(world, i, j, k, random);
+    public void b(World world, int i, int j, int k, Random random) {
+        super.b(world, i, j, k, random);
         if (world.getLightLevel(i, j + 1, k) >= 9) {
-            float f = this.i(world, i, j, k);
+            float f = this.n(world, i, j, k);
 
             if (random.nextInt((int) (25.0F / f) + 1) == 0) {
                 int l = world.getData(i, j, k);
@@ -77,11 +78,11 @@ public class BlockStem extends BlockFlower {
         }
     }
 
-    public void g(World world, int i, int j, int k) {
+    public void l(World world, int i, int j, int k) {
         world.setData(i, j, k, 7);
     }
 
-    private float i(World world, int i, int j, int k) {
+    private float n(World world, int i, int j, int k) {
         float f = 1.0F;
         int l = world.getTypeId(i, j, k - 1);
         int i1 = world.getTypeId(i, j, k + 1);
@@ -139,7 +140,7 @@ public class BlockStem extends BlockFlower {
         this.a(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, (float) this.maxY, 0.5F + f);
     }
 
-    public int c() {
+    public int b() {
         return 19;
     }
 
@@ -172,10 +173,6 @@ public class BlockStem extends BlockFlower {
     }
 
     public int getDropType(int i, Random random, int j) {
-        if (i == 7) {
-            ;
-        }
-
         return -1;
     }
 
