@@ -6,9 +6,10 @@ public class ItemFlintAndSteel extends Item {
         super(i);
         this.maxStackSize = 1;
         this.setMaxDurability(64);
+        this.a(CreativeModeTab.i);
     }
 
-    public boolean interactWith(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
+    public boolean interactWith(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l, float f, float f1, float f2) {
         if (l == 0) {
             --j;
         }
@@ -33,13 +34,13 @@ public class ItemFlintAndSteel extends Item {
             ++i;
         }
 
-        if (!entityhuman.d(i, j, k)) {
+        if (!entityhuman.e(i, j, k)) {
             return false;
         } else {
             int i1 = world.getTypeId(i, j, k);
 
             if (i1 == 0) {
-                world.makeSound((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "fire.ignite", 1.0F, c.nextFloat() * 0.4F + 0.8F);
+                world.makeSound((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "fire.ignite", 1.0F, d.nextFloat() * 0.4F + 0.8F);
                 world.setTypeId(i, j, k, Block.FIRE.id);
             }
 
