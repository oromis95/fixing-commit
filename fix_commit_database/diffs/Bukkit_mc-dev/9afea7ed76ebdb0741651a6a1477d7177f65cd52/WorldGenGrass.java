@@ -24,7 +24,7 @@ public class WorldGenGrass extends WorldGenerator {
             int k1 = j + random.nextInt(4) - random.nextInt(4);
             int l1 = k + random.nextInt(8) - random.nextInt(8);
 
-            if (world.isEmpty(j1, k1, l1) && ((BlockFlower) Block.byId[this.a]).f(world, j1, k1, l1)) {
+            if (world.isEmpty(j1, k1, l1) && Block.byId[this.a].d(world, j1, k1, l1)) {
                 world.setRawTypeIdAndData(j1, k1, l1, this.a, this.b);
             }
         }
