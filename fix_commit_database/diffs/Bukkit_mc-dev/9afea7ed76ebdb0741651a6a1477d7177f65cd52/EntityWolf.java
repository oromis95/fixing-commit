@@ -2,27 +2,26 @@ package net.minecraft.server;
 
 public class EntityWolf extends EntityTameableAnimal {
 
-    private boolean b = false;
-    private float c;
-    private float g;
+    private float e;
+    private float f;
+    private boolean g;
     private boolean h;
-    private boolean i;
+    private float i;
     private float j;
-    private float k;
 
     public EntityWolf(World world) {
         super(world);
         this.texture = "/mob/wolf.png";
-        this.b(0.6F, 0.8F);
-        this.bb = 0.3F;
-        this.al().a(true);
+        this.a(0.6F, 0.8F);
+        this.bw = 0.3F;
+        this.getNavigation().a(true);
         this.goalSelector.a(1, new PathfinderGoalFloat(this));
-        this.goalSelector.a(2, this.a);
+        this.goalSelector.a(2, this.d);
         this.goalSelector.a(3, new PathfinderGoalLeapAtTarget(this, 0.4F));
-        this.goalSelector.a(4, new PathfinderGoalMeleeAttack(this, this.bb, true));
-        this.goalSelector.a(5, new PathfinderGoalFollowOwner(this, this.bb, 10.0F, 2.0F));
-        this.goalSelector.a(6, new PathfinderGoalBreed(this, this.bb));
-        this.goalSelector.a(7, new PathfinderGoalRandomStroll(this, this.bb));
+        this.goalSelector.a(4, new PathfinderGoalMeleeAttack(this, this.bw, true));
+        this.goalSelector.a(5, new PathfinderGoalFollowOwner(this, this.bw, 10.0F, 2.0F));
+        this.goalSelector.a(6, new PathfinderGoalBreed(this, this.bw));
+        this.goalSelector.a(7, new PathfinderGoalRandomStroll(this, this.bw));
         this.goalSelector.a(8, new PathfinderGoalBeg(this, 8.0F));
         this.goalSelector.a(9, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 8.0F));
         this.goalSelector.a(9, new PathfinderGoalRandomLookaround(this));
@@ -32,7 +31,7 @@ public class EntityWolf extends EntityTameableAnimal {
         this.targetSelector.a(4, new PathfinderGoalRandomTargetNonTamed(this, EntitySheep.class, 16.0F, 200, false));
     }
 
-    public boolean c_() {
+    public boolean aV() {
         return true;
     }
 
@@ -43,7 +42,7 @@ public class EntityWolf extends EntityTameableAnimal {
         }
     }
 
-    protected void g() {
+    protected void bd() {
         this.datawatcher.watch(18, Integer.valueOf(this.getHealth()));
     }
 
@@ -51,12 +50,13 @@ public class EntityWolf extends EntityTameableAnimal {
         return this.isTamed() ? 20 : 8;
     }
 
-    protected void b() {
-        super.b();
+    protected void a() {
+        super.a();
         this.datawatcher.a(18, new Integer(this.getHealth()));
+        this.datawatcher.a(19, new Byte((byte) 0));
     }
 
-    protected boolean g_() {
+    protected boolean e_() {
         return false;
     }
 
@@ -70,23 +70,23 @@ public class EntityWolf extends EntityTameableAnimal {
         this.setAngry(nbttagcompound.getBoolean("Angry"));
     }
 
-    protected boolean n() {
+    protected boolean ba() {
         return this.isAngry();
     }
 
-    protected String i() {
+    protected String aQ() {
         return this.isAngry() ? "mob.wolf.growl" : (this.random.nextInt(3) == 0 ? (this.isTamed() && this.datawatcher.getInt(18) < 10 ? "mob.wolf.whine" : "mob.wolf.panting") : "mob.wolf.bark");
     }
 
-    protected String j() {
+    protected String aR() {
         return "mob.wolf.hurt";
     }
 
-    protected String k() {
+    protected String aS() {
         return "mob.wolf.death";
     }
 
-    protected float p() {
+    protected float aP() {
         return 0.4F;
     }
 
@@ -94,51 +94,51 @@ public class EntityWolf extends EntityTameableAnimal {
         return -1;
     }
 
-    public void e() {
-        super.e();
-        if (!this.world.isStatic && this.h && !this.i && !this.H() && this.onGround) {
-            this.i = true;
+    public void d() {
+        super.d();
+        if (!this.world.isStatic && this.g && !this.h && !this.l() && this.onGround) {
+            this.h = true;
+            this.i = 0.0F;
             this.j = 0.0F;
-            this.k = 0.0F;
             this.world.broadcastEntityEffect(this, (byte) 8);
         }
     }
 
-    public void F_() {
-        super.F_();
-        this.g = this.c;
-        if (this.b) {
-            this.c += (1.0F - this.c) * 0.4F;
+    public void h_() {
+        super.h_();
+        this.f = this.e;
+        if (this.bv()) {
+            this.e += (1.0F - this.e) * 0.4F;
         } else {
-            this.c += (0.0F - this.c) * 0.4F;
+            this.e += (0.0F - this.e) * 0.4F;
         }
 
-        if (this.b) {
-            this.bc = 10;
+        if (this.bv()) {
+            this.bx = 10;
         }
 
-        if (this.aT()) {
-            this.h = true;
-            this.i = false;
+        if (this.G()) {
+            this.g = true;
+            this.h = false;
+            this.i = 0.0F;
             this.j = 0.0F;
-            this.k = 0.0F;
-        } else if ((this.h || this.i) && this.i) {
-            if (this.j == 0.0F) {
-                this.world.makeSound(this, "mob.wolf.shake", this.p(), (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
+        } else if ((this.g || this.h) && this.h) {
+            if (this.i == 0.0F) {
+                this.world.makeSound(this, "mob.wolf.shake", this.aP(), (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
             }
 
-            this.k = this.j;
-            this.j += 0.05F;
-            if (this.k >= 2.0F) {
+            this.j = this.i;
+            this.i += 0.05F;
+            if (this.j >= 2.0F) {
+                this.g = false;
                 this.h = false;
-                this.i = false;
-                this.k = 0.0F;
                 this.j = 0.0F;
+                this.i = 0.0F;
             }
 
-            if (this.j > 0.4F) {
+            if (this.i > 0.4F) {
                 float f = (float) this.boundingBox.b;
-                int i = (int) (MathHelper.sin((this.j - 0.4F) * 3.1415927F) * 7.0F);
+                int i = (int) (MathHelper.sin((this.i - 0.4F) * 3.1415927F) * 7.0F);
 
                 for (int j = 0; j < i; ++j) {
                     float f1 = (this.random.nextFloat() * 2.0F - 1.0F) * this.width * 0.5F;
@@ -154,14 +154,14 @@ public class EntityWolf extends EntityTameableAnimal {
         return this.length * 0.8F;
     }
 
-    public int D() {
-        return this.isSitting() ? 20 : super.D();
+    public int bf() {
+        return this.isSitting() ? 20 : super.bf();
     }
 
     public boolean damageEntity(DamageSource damagesource, int i) {
         Entity entity = damagesource.getEntity();
 
-        this.a.a(false);
+        this.d.a(false);
         if (entity != null && !(entity instanceof EntityHuman) && !(entity instanceof EntityArrow)) {
             i = (i + 1) / 2;
         }
@@ -169,48 +169,20 @@ public class EntityWolf extends EntityTameableAnimal {
         return super.damageEntity(damagesource, i);
     }
 
-    public boolean a(Entity entity) {
+    public boolean k(Entity entity) {
         int i = this.isTamed() ? 4 : 2;
 
         return entity.damageEntity(DamageSource.mobAttack(this), i);
     }
 
-    public boolean b(EntityHuman entityhuman) {
+    public boolean c(EntityHuman entityhuman) {
         ItemStack itemstack = entityhuman.inventory.getItemInHand();
 
-        if (!this.isTamed()) {
-            if (itemstack != null && itemstack.id == Item.BONE.id && !this.isAngry()) {
-                if (!entityhuman.abilities.canInstantlyBuild) {
-                    --itemstack.count;
-                }
-
-                if (itemstack.count <= 0) {
-                    entityhuman.inventory.setItem(entityhuman.inventory.itemInHandIndex, (ItemStack) null);
-                }
-
-                if (!this.world.isStatic) {
-                    if (this.random.nextInt(3) == 0) {
-                        this.setTamed(true);
-                        this.setPathEntity((PathEntity) null);
-                        this.b((EntityLiving) null);
-                        this.a.a(true);
-                        this.setHealth(20);
-                        this.setOwnerName(entityhuman.name);
-                        this.a(true);
-                        this.world.broadcastEntityEffect(this, (byte) 7);
-                    } else {
-                        this.a(false);
-                        this.world.broadcastEntityEffect(this, (byte) 6);
-                    }
-                }
-
-                return true;
-            }
-        } else {
+        if (this.isTamed()) {
             if (itemstack != null && Item.byId[itemstack.id] instanceof ItemFood) {
                 ItemFood itemfood = (ItemFood) Item.byId[itemstack.id];
 
-                if (itemfood.q() && this.datawatcher.getInt(18) < 20) {
+                if (itemfood.h() && this.datawatcher.getInt(18) < 20) {
                     if (!entityhuman.abilities.canInstantlyBuild) {
                         --itemstack.count;
                     }
@@ -224,21 +196,47 @@ public class EntityWolf extends EntityTameableAnimal {
                 }
             }
 
-            if (entityhuman.name.equalsIgnoreCase(this.getOwnerName()) && !this.world.isStatic && !this.a(itemstack)) {
-                this.a.a(!this.isSitting());
-                this.aZ = false;
+            if (entityhuman.name.equalsIgnoreCase(this.getOwnerName()) && !this.world.isStatic && !this.b(itemstack)) {
+                this.d.a(!this.isSitting());
+                this.bu = false;
                 this.setPathEntity((PathEntity) null);
             }
+        } else if (itemstack != null && itemstack.id == Item.BONE.id && !this.isAngry()) {
+            if (!entityhuman.abilities.canInstantlyBuild) {
+                --itemstack.count;
+            }
+
+            if (itemstack.count <= 0) {
+                entityhuman.inventory.setItem(entityhuman.inventory.itemInHandIndex, (ItemStack) null);
+            }
+
+            if (!this.world.isStatic) {
+                if (this.random.nextInt(3) == 0) {
+                    this.setTamed(true);
+                    this.setPathEntity((PathEntity) null);
+                    this.b((EntityLiving) null);
+                    this.d.a(true);
+                    this.setHealth(20);
+                    this.setOwnerName(entityhuman.name);
+                    this.e(true);
+                    this.world.broadcastEntityEffect(this, (byte) 7);
+                } else {
+                    this.e(false);
+                    this.world.broadcastEntityEffect(this, (byte) 6);
+                }
+            }
+
+            return true;
         }
 
-        return super.b(entityhuman);
+        return super.c(entityhuman);
     }
 
-    public boolean a(ItemStack itemstack) {
-        return itemstack == null ? false : (!(Item.byId[itemstack.id] instanceof ItemFood) ? false : ((ItemFood) Item.byId[itemstack.id]).q());
+    public boolean b(ItemStack itemstack) {
+        return itemstack == null ? false : (!(Item.byId[itemstack.id] instanceof ItemFood) ? false : ((ItemFood) Item.byId[itemstack.id]).h());
     }
 
-    public int q() {
+    public int bl() {
         return 8;
     }
 
@@ -264,8 +262,14 @@ public class EntityWolf extends EntityTameableAnimal {
         return entitywolf;
     }
 
-    public void e(boolean flag) {
-        this.b = flag;
+    public void i(boolean flag) {
+        byte b0 = this.datawatcher.getByte(19);
+
+        if (flag) {
+            this.datawatcher.watch(19, Byte.valueOf((byte) 1));
+        } else {
+            this.datawatcher.watch(19, Byte.valueOf((byte) 0));
+        }
     }
 
     public boolean mate(EntityAnimal entityanimal) {
@@ -278,7 +282,11 @@ public class EntityWolf extends EntityTameableAnimal {
         } else {
             EntityWolf entitywolf = (EntityWolf) entityanimal;
 
-            return !entitywolf.isTamed() ? false : (entitywolf.isSitting() ? false : this.r_() && entitywolf.r_());
+            return !entitywolf.isTamed() ? false : (entitywolf.isSitting() ? false : this.s() && entitywolf.s());
         }
     }
+
+    public boolean bv() {
+        return this.datawatcher.getByte(19) == 1;
+    }
 }
