@@ -4,12 +4,13 @@ public class BlockSlowSand extends Block {
 
     public BlockSlowSand(int i, int j) {
         super(i, j, Material.SAND);
+        this.a(CreativeModeTab.b);
     }
 
     public AxisAlignedBB e(World world, int i, int j, int k) {
         float f = 0.125F;
 
-        return AxisAlignedBB.b((double) i, (double) j, (double) k, (double) (i + 1), (double) ((float) (j + 1) - f), (double) (k + 1));
+        return AxisAlignedBB.a().a((double) i, (double) j, (double) k, (double) (i + 1), (double) ((float) (j + 1) - f), (double) (k + 1));
     }
 
     public void a(World world, int i, int j, int k, Entity entity) {
