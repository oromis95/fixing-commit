@@ -15,16 +15,16 @@ public class EntityAIBodyControl {
         double d1 = this.entity.locZ - this.entity.lastZ;
 
         if (d0 * d0 + d1 * d1 > 2.500000277905201E-7D) {
-            this.entity.V = this.entity.yaw;
-            this.entity.X = this.a(this.entity.V, this.entity.X, 75.0F);
-            this.c = this.entity.X;
+            this.entity.aq = this.entity.yaw;
+            this.entity.as = this.a(this.entity.aq, this.entity.as, 75.0F);
+            this.c = this.entity.as;
             this.b = 0;
         } else {
             float f = 75.0F;
 
-            if (Math.abs(this.entity.X - this.c) > 15.0F) {
+            if (Math.abs(this.entity.as - this.c) > 15.0F) {
                 this.b = 0;
-                this.c = this.entity.X;
+                this.c = this.entity.as;
             } else {
                 ++this.b;
                 if (this.b > 10) {
@@ -32,20 +32,12 @@ public class EntityAIBodyControl {
                 }
             }
 
-            this.entity.V = this.a(this.entity.X, this.entity.V, f);
+            this.entity.aq = this.a(this.entity.as, this.entity.aq, f);
         }
     }
 
     private float a(float f, float f1, float f2) {
-        float f3;
-
-        for (f3 = f - f1; f3 < -180.0F; f3 += 360.0F) {
-            ;
-        }
-
-        while (f3 >= 180.0F) {
-            f3 -= 360.0F;
-        }
+        float f3 = MathHelper.g(f - f1);
 
         if (f3 < -f2) {
             f3 = -f2;
