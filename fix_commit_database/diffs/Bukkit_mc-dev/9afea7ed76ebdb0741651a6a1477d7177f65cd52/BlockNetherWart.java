@@ -6,40 +6,37 @@ public class BlockNetherWart extends BlockFlower {
 
     protected BlockNetherWart(int i) {
         super(i, 226);
-        this.a(true);
+        this.b(true);
         float f = 0.5F;
 
         this.a(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, 0.25F, 0.5F + f);
+        this.a((CreativeModeTab) null);
     }
 
-    protected boolean d(int i) {
+    protected boolean d_(int i) {
         return i == Block.SOUL_SAND.id;
     }
 
-    public boolean f(World world, int i, int j, int k) {
-        return this.d(world.getTypeId(i, j - 1, k));
+    public boolean d(World world, int i, int j, int k) {
+        return this.d_(world.getTypeId(i, j - 1, k));
     }
 
-    public void a(World world, int i, int j, int k, Random random) {
+    public void b(World world, int i, int j, int k, Random random) {
         int l = world.getData(i, j, k);
 
-        if (l < 3) {
-            BiomeBase biomebase = world.getBiome(i, k);
-
-            if (biomebase instanceof BiomeHell && random.nextInt(10) == 0) {
-                ++l;
-                world.setData(i, j, k, l);
-            }
+        if (l < 3 && random.nextInt(10) == 0) {
+            ++l;
+            world.setData(i, j, k, l);
         }
 
-        super.a(world, i, j, k, random);
+        super.b(world, i, j, k, random);
     }
 
     public int a(int i, int j) {
         return j >= 3 ? this.textureId + 2 : (j > 0 ? this.textureId + 1 : this.textureId);
     }
 
-    public int c() {
+    public int b() {
         return 6;
     }
 
