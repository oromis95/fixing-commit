@@ -1,8 +0,0 @@
-package net.minecraft.server;
-
-public class WorldConlictException extends RuntimeException {
-
-    public WorldConlictException(String s) {
-        super(s);
-    }
-}
