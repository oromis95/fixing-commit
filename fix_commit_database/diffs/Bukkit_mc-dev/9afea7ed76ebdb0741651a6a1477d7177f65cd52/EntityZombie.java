@@ -5,16 +5,16 @@ public class EntityZombie extends EntityMonster {
     public EntityZombie(World world) {
         super(world);
         this.texture = "/mob/zombie.png";
-        this.bb = 0.23F;
+        this.bw = 0.23F;
         this.damage = 4;
-        this.al().b(true);
+        this.getNavigation().b(true);
         this.goalSelector.a(0, new PathfinderGoalFloat(this));
         this.goalSelector.a(1, new PathfinderGoalBreakDoor(this));
-        this.goalSelector.a(2, new PathfinderGoalMeleeAttack(this, EntityHuman.class, this.bb, false));
-        this.goalSelector.a(3, new PathfinderGoalMeleeAttack(this, EntityVillager.class, this.bb, true));
-        this.goalSelector.a(4, new PathfinderGoalMoveTowardsRestriction(this, this.bb));
-        this.goalSelector.a(5, new PathfinderGoalMoveThroughVillage(this, this.bb, false));
-        this.goalSelector.a(6, new PathfinderGoalRandomStroll(this, this.bb));
+        this.goalSelector.a(2, new PathfinderGoalMeleeAttack(this, EntityHuman.class, this.bw, false));
+        this.goalSelector.a(3, new PathfinderGoalMeleeAttack(this, EntityVillager.class, this.bw, true));
+        this.goalSelector.a(4, new PathfinderGoalMoveTowardsRestriction(this, this.bw));
+        this.goalSelector.a(5, new PathfinderGoalMoveThroughVillage(this, this.bw, false));
+        this.goalSelector.a(6, new PathfinderGoalRandomStroll(this, this.bw));
         this.goalSelector.a(7, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 8.0F));
         this.goalSelector.a(7, new PathfinderGoalRandomLookaround(this));
         this.targetSelector.a(1, new PathfinderGoalHurtByTarget(this, false));
@@ -26,35 +26,35 @@ public class EntityZombie extends EntityMonster {
         return 20;
     }
 
-    public int T() {
+    public int aO() {
         return 2;
     }
 
-    protected boolean c_() {
+    protected boolean aV() {
         return true;
     }
 
-    public void e() {
-        if (this.world.e() && !this.world.isStatic) {
-            float f = this.b(1.0F);
+    public void d() {
+        if (this.world.r() && !this.world.isStatic) {
+            float f = this.c(1.0F);
 
-            if (f > 0.5F && this.world.isChunkLoaded(MathHelper.floor(this.locX), MathHelper.floor(this.locY), MathHelper.floor(this.locZ)) && this.random.nextFloat() * 30.0F < (f - 0.4F) * 2.0F) {
+            if (f > 0.5F && this.world.j(MathHelper.floor(this.locX), MathHelper.floor(this.locY), MathHelper.floor(this.locZ)) && this.random.nextFloat() * 30.0F < (f - 0.4F) * 2.0F) {
                 this.setOnFire(8);
             }
         }
 
-        super.e();
+        super.d();
     }
 
-    protected String i() {
+    protected String aQ() {
         return "mob.zombie";
     }
 
-    protected String j() {
+    protected String aR() {
         return "mob.zombiehurt";
     }
 
-    protected String k() {
+    protected String aS() {
         return "mob.zombiedeath";
     }
 
@@ -62,11 +62,11 @@ public class EntityZombie extends EntityMonster {
         return Item.ROTTEN_FLESH.id;
     }
 
-    public MonsterType getMonsterType() {
-        return MonsterType.UNDEAD;
+    public EnumMonsterType getMonsterType() {
+        return EnumMonsterType.UNDEAD;
     }
 
-    protected void b(int i) {
+    protected void l(int i) {
         switch (this.random.nextInt(4)) {
         case 0:
             this.b(Item.IRON_SWORD.id, 1);
