@@ -6,21 +6,22 @@ public class BlockGlass extends BlockHalfTransparant {
 
     public BlockGlass(int i, int j, Material material, boolean flag) {
         super(i, j, material, flag);
+        this.a(CreativeModeTab.b);
     }
 
     public int a(Random random) {
         return 0;
     }
 
-    public boolean a() {
+    public boolean d() {
         return false;
     }
 
-    public boolean b() {
+    public boolean c() {
         return false;
     }
 
-    protected boolean h() {
+    protected boolean q_() {
         return true;
     }
 }
