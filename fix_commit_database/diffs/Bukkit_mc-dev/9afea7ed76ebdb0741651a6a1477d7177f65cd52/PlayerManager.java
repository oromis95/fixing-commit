@@ -1,57 +1,59 @@
 package net.minecraft.server;
 
 import java.util.ArrayList;
+import java.util.Iterator;
 import java.util.List;
 
 public class PlayerManager {
 
-    public List managedPlayers = new ArrayList();
-    private LongHashMap b = new LongHashMap();
-    private List c = new ArrayList();
-    private MinecraftServer server;
-    private int e;
-    private int f;
-    private final int[][] g = new int[][] { { 1, 0}, { 0, 1}, { -1, 0}, { 0, -1}};
+    private final WorldServer world;
+    private final List managedPlayers = new ArrayList();
+    private final LongHashMap c = new LongHashMap();
+    private final List d = new ArrayList();
+    private final int e;
+    private final int[][] f = new int[][] { { 1, 0}, { 0, 1}, { -1, 0}, { 0, -1}};
 
-    public PlayerManager(MinecraftServer minecraftserver, int i, int j) {
-        if (j > 15) {
+    public PlayerManager(WorldServer worldserver, int i) {
+        if (i > 15) {
             throw new IllegalArgumentException("Too big view radius!");
-        } else if (j < 3) {
+        } else if (i < 3) {
             throw new IllegalArgumentException("Too small view radius!");
         } else {
-            this.f = j;
-            this.server = minecraftserver;
             this.e = i;
+            this.world = worldserver;
         }
     }
 
     public WorldServer a() {
-        return this.server.getWorldServer(this.e);
+        return this.world;
     }
 
     public void flush() {
-        for (int i = 0; i < this.c.size(); ++i) {
-            ((PlayerInstance) this.c.get(i)).a();
+        Iterator iterator = this.d.iterator();
+
+        while (iterator.hasNext()) {
+            PlayerInstance playerinstance = (PlayerInstance) iterator.next();
+
+            playerinstance.a();
         }
 
-        this.c.clear();
+        this.d.clear();
         if (this.managedPlayers.isEmpty()) {
-            WorldServer worldserver = this.server.getWorldServer(this.e);
-            WorldProvider worldprovider = worldserver.worldProvider;
+            WorldProvider worldprovider = this.world.worldProvider;
 
-            if (!worldprovider.c()) {
-                worldserver.chunkProviderServer.c();
+            if (!worldprovider.e()) {
+                this.world.chunkProviderServer.a();
             }
         }
     }
 
     private PlayerInstance a(int i, int j, boolean flag) {
         long k = (long) i + 2147483647L | (long) j + 2147483647L << 32;
-        PlayerInstance playerinstance = (PlayerInstance) this.b.getEntry(k);
+        PlayerInstance playerinstance = (PlayerInstance) this.c.getEntry(k);
 
         if (playerinstance == null && flag) {
             playerinstance = new PlayerInstance(this, i, j);
-            this.b.put(k, playerinstance);
+            this.c.put(k, playerinstance);
         }
 
         return playerinstance;
@@ -73,44 +75,67 @@ public class PlayerManager {
 
         entityplayer.d = entityplayer.locX;
         entityplayer.e = entityplayer.locZ;
-        int k = 0;
-        int l = this.f;
+
+        for (int k = i - this.e; k <= i + this.e; ++k) {
+            for (int l = j - this.e; l <= j + this.e; ++l) {
+                this.a(k, l, true).a(entityplayer);
+            }
+        }
+
+        this.managedPlayers.add(entityplayer);
+        this.b(entityplayer);
+    }
+
+    public void b(EntityPlayer entityplayer) {
+        ArrayList arraylist = new ArrayList(entityplayer.chunkCoordIntPairQueue);
+        int i = 0;
+        int j = this.e;
+        int k = (int) entityplayer.locX >> 4;
+        int l = (int) entityplayer.locZ >> 4;
         int i1 = 0;
         int j1 = 0;
+        ChunkCoordIntPair chunkcoordintpair = PlayerInstance.a(this.a(k, l, true));
 
-        this.a(i, j, true).a(entityplayer);
+        entityplayer.chunkCoordIntPairQueue.clear();
+        if (arraylist.contains(chunkcoordintpair)) {
+            entityplayer.chunkCoordIntPairQueue.add(chunkcoordintpair);
+        }
 
         int k1;
 
-        for (k1 = 1; k1 <= l * 2; ++k1) {
+        for (k1 = 1; k1 <= j * 2; ++k1) {
             for (int l1 = 0; l1 < 2; ++l1) {
-                int[] aint = this.g[k++ % 4];
+                int[] aint = this.f[i++ % 4];
 
                 for (int i2 = 0; i2 < k1; ++i2) {
                     i1 += aint[0];
                     j1 += aint[1];
-                    this.a(i + i1, j + j1, true).a(entityplayer);
+                    chunkcoordintpair = PlayerInstance.a(this.a(k + i1, l + j1, true));
+                    if (arraylist.contains(chunkcoordintpair)) {
+                        entityplayer.chunkCoordIntPairQueue.add(chunkcoordintpair);
+                    }
                 }
             }
         }
 
-        k %= 4;
+        i %= 4;
 
-        for (k1 = 0; k1 < l * 2; ++k1) {
-            i1 += this.g[k][0];
-            j1 += this.g[k][1];
-            this.a(i + i1, j + j1, true).a(entityplayer);
+        for (k1 = 0; k1 < j * 2; ++k1) {
+            i1 += this.f[i][0];
+            j1 += this.f[i][1];
+            chunkcoordintpair = PlayerInstance.a(this.a(k + i1, l + j1, true));
+            if (arraylist.contains(chunkcoordintpair)) {
+                entityplayer.chunkCoordIntPairQueue.add(chunkcoordintpair);
+            }
         }
-
-        this.managedPlayers.add(entityplayer);
     }
 
     public void removePlayer(EntityPlayer entityplayer) {
         int i = (int) entityplayer.d >> 4;
         int j = (int) entityplayer.e >> 4;
 
-        for (int k = i - this.f; k <= i + this.f; ++k) {
-            for (int l = j - this.f; l <= j + this.f; ++l) {
+        for (int k = i - this.e; k <= i + this.e; ++k) {
+            for (int l = j - this.e; l <= j + this.e; ++l) {
                 PlayerInstance playerinstance = this.a(k, l, false);
 
                 if (playerinstance != null) {
@@ -122,11 +147,11 @@ public class PlayerManager {
         this.managedPlayers.remove(entityplayer);
     }
 
-    private boolean a(int i, int j, int k, int l) {
-        int i1 = i - k;
-        int j1 = j - l;
+    private boolean a(int i, int j, int k, int l, int i1) {
+        int j1 = i - k;
+        int k1 = j - l;
 
-        return i1 >= -this.f && i1 <= this.f ? j1 >= -this.f && j1 <= this.f : false;
+        return j1 >= -i1 && j1 <= i1 ? k1 >= -i1 && k1 <= i1 : false;
     }
 
     public void movePlayer(EntityPlayer entityplayer) {
@@ -139,18 +164,19 @@ public class PlayerManager {
         if (d2 >= 64.0D) {
             int k = (int) entityplayer.d >> 4;
             int l = (int) entityplayer.e >> 4;
-            int i1 = i - k;
-            int j1 = j - l;
-
-            if (i1 != 0 || j1 != 0) {
-                for (int k1 = i - this.f; k1 <= i + this.f; ++k1) {
-                    for (int l1 = j - this.f; l1 <= j + this.f; ++l1) {
-                        if (!this.a(k1, l1, k, l)) {
-                            this.a(k1, l1, true).a(entityplayer);
+            int i1 = this.e;
+            int j1 = i - k;
+            int k1 = j - l;
+
+            if (j1 != 0 || k1 != 0) {
+                for (int l1 = i - i1; l1 <= i + i1; ++l1) {
+                    for (int i2 = j - i1; i2 <= j + i1; ++i2) {
+                        if (!this.a(l1, i2, k, l, i1)) {
+                            this.a(l1, i2, true).a(entityplayer);
                         }
 
-                        if (!this.a(k1 - i1, l1 - j1, i, j)) {
-                            PlayerInstance playerinstance = this.a(k1 - i1, l1 - j1, false);
+                        if (!this.a(l1 - j1, i2 - k1, i, j, i1)) {
+                            PlayerInstance playerinstance = this.a(l1 - j1, i2 - k1, false);
 
                             if (playerinstance != null) {
                                 playerinstance.b(entityplayer);
@@ -159,21 +185,32 @@ public class PlayerManager {
                     }
                 }
 
+                this.b(entityplayer);
                 entityplayer.d = entityplayer.locX;
                 entityplayer.e = entityplayer.locZ;
             }
         }
     }
 
-    public int getFurthestViewableBlock() {
-        return this.f * 16 - 16;
+    public boolean a(EntityPlayer entityplayer, int i, int j) {
+        PlayerInstance playerinstance = this.a(i, j, false);
+
+        return playerinstance == null ? false : PlayerInstance.b(playerinstance).contains(entityplayer);
     }
 
-    static LongHashMap a(PlayerManager playermanager) {
-        return playermanager.b;
+    public static int getFurthestViewableBlock(int i) {
+        return i * 16 - 16;
     }
 
-    static List b(PlayerManager playermanager) {
+    static WorldServer a(PlayerManager playermanager) {
+        return playermanager.world;
+    }
+
+    static LongHashMap b(PlayerManager playermanager) {
         return playermanager.c;
     }
+
+    static List c(PlayerManager playermanager) {
+        return playermanager.d;
+    }
 }
