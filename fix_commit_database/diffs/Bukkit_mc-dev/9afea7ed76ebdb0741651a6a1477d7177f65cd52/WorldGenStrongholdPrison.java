@@ -9,13 +9,13 @@ public class WorldGenStrongholdPrison extends WorldGenStrongholdPiece {
 
     public WorldGenStrongholdPrison(int i, Random random, StructureBoundingBox structureboundingbox, int j) {
         super(i);
-        this.h = j;
+        this.f = j;
         this.a = this.a(random);
-        this.g = structureboundingbox;
+        this.e = structureboundingbox;
     }
 
     public void a(StructurePiece structurepiece, List list, Random random) {
-        this.a((WorldGenStrongholdStairs2) structurepiece, list, random, 1, 1);
+        this.a((WorldGenStrongholdStart) structurepiece, list, random, 1, 1);
     }
 
     public static WorldGenStrongholdPrison a(List list, Random random, int i, int j, int k, int l, int i1) {
