@@ -13,11 +13,11 @@ public class Packet51MapChunk extends Packet {
     public int b;
     public int c;
     public int d;
-    public byte[] buffer;
-    public boolean f;
+    private byte[] buffer;
+    private byte[] inflatedBuffer;
+    public boolean e;
     private int size;
-    private int h;
-    private static byte[] rawData = new byte[0];
+    private static byte[] buildBuffer = new byte[196864];
 
     public Packet51MapChunk() {
         this.lowPriority = true;
@@ -27,101 +27,18 @@ public class Packet51MapChunk extends Packet {
         this.lowPriority = true;
         this.a = chunk.x;
         this.b = chunk.z;
-        this.f = flag;
-        if (flag) {
-            i = '\uffff';
-            chunk.seenByPlayer = true;
-        }
-
-        ChunkSection[] achunksection = chunk.h();
-        int j = 0;
-        int k = 0;
-
-        int l;
-
-        for (l = 0; l < achunksection.length; ++l) {
-            if (achunksection[l] != null && (!flag || !achunksection[l].a()) && (i & 1 << l) != 0) {
-                this.c |= 1 << l;
-                ++j;
-                if (achunksection[l].h() != null) {
-                    this.d |= 1 << l;
-                    ++k;
-                }
-            }
-        }
-
-        l = 2048 * (5 * j + k);
-        if (flag) {
-            l += 256;
-        }
-
-        if (rawData.length < l) {
-            rawData = new byte[l];
-        }
-
-        byte[] abyte = rawData;
-        int i1 = 0;
-
-        int j1;
-
-        for (j1 = 0; j1 < achunksection.length; ++j1) {
-            if (achunksection[j1] != null && (!flag || !achunksection[j1].a()) && (i & 1 << j1) != 0) {
-                byte[] abyte1 = achunksection[j1].g();
-
-                System.arraycopy(abyte1, 0, abyte, i1, abyte1.length);
-                i1 += abyte1.length;
-            }
-        }
-
-        NibbleArray nibblearray;
-
-        for (j1 = 0; j1 < achunksection.length; ++j1) {
-            if (achunksection[j1] != null && (!flag || !achunksection[j1].a()) && (i & 1 << j1) != 0) {
-                nibblearray = achunksection[j1].i();
-                System.arraycopy(nibblearray.a, 0, abyte, i1, nibblearray.a.length);
-                i1 += nibblearray.a.length;
-            }
-        }
-
-        for (j1 = 0; j1 < achunksection.length; ++j1) {
-            if (achunksection[j1] != null && (!flag || !achunksection[j1].a()) && (i & 1 << j1) != 0) {
-                nibblearray = achunksection[j1].j();
-                System.arraycopy(nibblearray.a, 0, abyte, i1, nibblearray.a.length);
-                i1 += nibblearray.a.length;
-            }
-        }
-
-        for (j1 = 0; j1 < achunksection.length; ++j1) {
-            if (achunksection[j1] != null && (!flag || !achunksection[j1].a()) && (i & 1 << j1) != 0) {
-                nibblearray = achunksection[j1].k();
-                System.arraycopy(nibblearray.a, 0, abyte, i1, nibblearray.a.length);
-                i1 += nibblearray.a.length;
-            }
-        }
-
-        if (k > 0) {
-            for (j1 = 0; j1 < achunksection.length; ++j1) {
-                if (achunksection[j1] != null && (!flag || !achunksection[j1].a()) && achunksection[j1].h() != null && (i & 1 << j1) != 0) {
-                    nibblearray = achunksection[j1].h();
-                    System.arraycopy(nibblearray.a, 0, abyte, i1, nibblearray.a.length);
-                    i1 += nibblearray.a.length;
-                }
-            }
-        }
-
-        if (flag) {
-            byte[] abyte2 = chunk.l();
-
-            System.arraycopy(abyte2, 0, abyte, i1, abyte2.length);
-            i1 += abyte2.length;
-        }
-
+        this.e = flag;
+        ChunkMap chunkmap = a(chunk, flag, i);
         Deflater deflater = new Deflater(-1);
 
+        this.d = chunkmap.c;
+        this.c = chunkmap.b;
+
         try {
-            deflater.setInput(abyte, 0, i1);
+            this.inflatedBuffer = chunkmap.a;
+            deflater.setInput(chunkmap.a, 0, chunkmap.a.length);
             deflater.finish();
-            this.buffer = new byte[i1];
+            this.buffer = new byte[chunkmap.a.length];
             this.size = deflater.deflate(this.buffer);
         } finally {
             deflater.end();
@@ -131,16 +48,15 @@ public class Packet51MapChunk extends Packet {
     public void a(DataInputStream datainputstream) {
         this.a = datainputstream.readInt();
         this.b = datainputstream.readInt();
-        this.f = datainputstream.readBoolean();
+        this.e = datainputstream.readBoolean();
         this.c = datainputstream.readShort();
         this.d = datainputstream.readShort();
         this.size = datainputstream.readInt();
-        this.h = datainputstream.readInt();
-        if (rawData.length < this.size) {
-            rawData = new byte[this.size];
+        if (buildBuffer.length < this.size) {
+            buildBuffer = new byte[this.size];
         }
 
-        datainputstream.readFully(rawData, 0, this.size);
+        datainputstream.readFully(buildBuffer, 0, this.size);
         int i = 0;
 
         int j;
@@ -150,17 +66,17 @@ public class Packet51MapChunk extends Packet {
         }
 
         j = 12288 * i;
-        if (this.f) {
+        if (this.e) {
             j += 256;
         }
 
-        this.buffer = new byte[j];
+        this.inflatedBuffer = new byte[j];
         Inflater inflater = new Inflater();
 
-        inflater.setInput(rawData, 0, this.size);
+        inflater.setInput(buildBuffer, 0, this.size);
 
         try {
-            inflater.inflate(this.buffer);
+            inflater.inflate(this.inflatedBuffer);
         } catch (DataFormatException dataformatexception) {
             throw new IOException("Bad compressed data format");
         } finally {
@@ -171,11 +87,10 @@ public class Packet51MapChunk extends Packet {
     public void a(DataOutputStream dataoutputstream) {
         dataoutputstream.writeInt(this.a);
         dataoutputstream.writeInt(this.b);
-        dataoutputstream.writeBoolean(this.f);
+        dataoutputstream.writeBoolean(this.e);
         dataoutputstream.writeShort((short) (this.c & '\uffff'));
         dataoutputstream.writeShort((short) (this.d & '\uffff'));
         dataoutputstream.writeInt(this.size);
-        dataoutputstream.writeInt(this.h);
         dataoutputstream.write(this.buffer, 0, this.size);
     }
 
@@ -186,4 +101,84 @@ public class Packet51MapChunk extends Packet {
     public int a() {
         return 17 + this.size;
     }
+
+    public static ChunkMap a(Chunk chunk, boolean flag, int i) {
+        int j = 0;
+        ChunkSection[] achunksection = chunk.i();
+        int k = 0;
+        ChunkMap chunkmap = new ChunkMap();
+        byte[] abyte = buildBuffer;
+
+        if (flag) {
+            chunk.seenByPlayer = true;
+        }
+
+        int l;
+
+        for (l = 0; l < achunksection.length; ++l) {
+            if (achunksection[l] != null && (!flag || !achunksection[l].a()) && (i & 1 << l) != 0) {
+                chunkmap.b |= 1 << l;
+                if (achunksection[l].i() != null) {
+                    chunkmap.c |= 1 << l;
+                    ++k;
+                }
+            }
+        }
+
+        for (l = 0; l < achunksection.length; ++l) {
+            if (achunksection[l] != null && (!flag || !achunksection[l].a()) && (i & 1 << l) != 0) {
+                byte[] abyte1 = achunksection[l].g();
+
+                System.arraycopy(abyte1, 0, abyte, j, abyte1.length);
+                j += abyte1.length;
+            }
+        }
+
+        NibbleArray nibblearray;
+
+        for (l = 0; l < achunksection.length; ++l) {
+            if (achunksection[l] != null && (!flag || !achunksection[l].a()) && (i & 1 << l) != 0) {
+                nibblearray = achunksection[l].j();
+                System.arraycopy(nibblearray.a, 0, abyte, j, nibblearray.a.length);
+                j += nibblearray.a.length;
+            }
+        }
+
+        for (l = 0; l < achunksection.length; ++l) {
+            if (achunksection[l] != null && (!flag || !achunksection[l].a()) && (i & 1 << l) != 0) {
+                nibblearray = achunksection[l].k();
+                System.arraycopy(nibblearray.a, 0, abyte, j, nibblearray.a.length);
+                j += nibblearray.a.length;
+            }
+        }
+
+        for (l = 0; l < achunksection.length; ++l) {
+            if (achunksection[l] != null && (!flag || !achunksection[l].a()) && (i & 1 << l) != 0) {
+                nibblearray = achunksection[l].l();
+                System.arraycopy(nibblearray.a, 0, abyte, j, nibblearray.a.length);
+                j += nibblearray.a.length;
+            }
+        }
+
+        if (k > 0) {
+            for (l = 0; l < achunksection.length; ++l) {
+                if (achunksection[l] != null && (!flag || !achunksection[l].a()) && achunksection[l].i() != null && (i & 1 << l) != 0) {
+                    nibblearray = achunksection[l].i();
+                    System.arraycopy(nibblearray.a, 0, abyte, j, nibblearray.a.length);
+                    j += nibblearray.a.length;
+                }
+            }
+        }
+
+        if (flag) {
+            byte[] abyte2 = chunk.m();
+
+            System.arraycopy(abyte2, 0, abyte, j, abyte2.length);
+            j += abyte2.length;
+        }
+
+        chunkmap.a = new byte[j];
+        System.arraycopy(abyte, 0, chunkmap.a, 0, j);
+        return chunkmap;
+    }
 }
