@@ -6,6 +6,7 @@ public class RecipesCrafting {
 
     public void a(CraftingManager craftingmanager) {
         craftingmanager.registerShapedRecipe(new ItemStack(Block.CHEST), new Object[] { "###", "# #", "###", Character.valueOf('#'), Block.WOOD});
+        craftingmanager.registerShapedRecipe(new ItemStack(Block.ENDER_CHEST), new Object[] { "###", "#E#", "###", Character.valueOf('#'), Block.OBSIDIAN, Character.valueOf('E'), Item.EYE_OF_ENDER});
         craftingmanager.registerShapedRecipe(new ItemStack(Block.FURNACE), new Object[] { "###", "# #", "###", Character.valueOf('#'), Block.COBBLESTONE});
         craftingmanager.registerShapedRecipe(new ItemStack(Block.WORKBENCH), new Object[] { "##", "##", Character.valueOf('#'), Block.WOOD});
         craftingmanager.registerShapedRecipe(new ItemStack(Block.SANDSTONE), new Object[] { "##", "##", Character.valueOf('#'), Block.SAND});
