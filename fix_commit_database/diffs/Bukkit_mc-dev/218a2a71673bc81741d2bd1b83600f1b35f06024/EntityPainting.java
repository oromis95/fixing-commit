@@ -34,7 +34,7 @@ public class EntityPainting extends Entity {
 
             this.e = enumart;
             this.b(l);
-            if (this.h()) {
+            if (this.i()) {
                 arraylist.add(enumart);
             }
         }
@@ -112,17 +112,17 @@ public class EntityPainting extends Entity {
         return i == 32 ? 0.5F : (i == 64 ? 0.5F : 0.0F);
     }
 
-    public void m_() {
+    public void s_() {
         if (this.f++ == 100 && !this.world.isStatic) {
             this.f = 0;
-            if (!this.h()) {
+            if (!this.i()) {
                 this.die();
                 this.world.addEntity(new EntityItem(this.world, this.locX, this.locY, this.locZ, new ItemStack(Item.PAINTING)));
             }
         }
     }
 
-    public boolean h() {
+    public boolean i() {
         if (this.world.getEntities(this, this.boundingBox).size() > 0) {
             return false;
         } else {
@@ -180,14 +180,14 @@ public class EntityPainting extends Entity {
         }
     }
 
-    public boolean l_() {
+    public boolean r_() {
         return true;
     }
 
-    public boolean damageEntity(Entity entity, int i) {
+    public boolean damageEntity(DamageSource damagesource, int i) {
         if (!this.dead && !this.world.isStatic) {
             this.die();
-            this.af();
+            this.aq();
             this.world.addEntity(new EntityItem(this.world, this.locX, this.locY, this.locZ, new ItemStack(Item.PAINTING)));
         }
 
@@ -226,7 +226,7 @@ public class EntityPainting extends Entity {
         this.b(this.a);
     }
 
-    public void move(double d0, double d1, double d2) {
+    public void a_(double d0, double d1, double d2) {
         if (!this.world.isStatic && d0 * d0 + d1 * d1 + d2 * d2 > 0.0D) {
             this.die();
             this.world.addEntity(new EntityItem(this.world, this.locX, this.locY, this.locZ, new ItemStack(Item.PAINTING)));
