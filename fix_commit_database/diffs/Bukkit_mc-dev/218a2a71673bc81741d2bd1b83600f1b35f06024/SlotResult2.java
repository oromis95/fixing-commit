@@ -2,11 +2,11 @@ package net.minecraft.server;
 
 public class SlotResult2 extends Slot {
 
-    private EntityHuman d;
+    private EntityHuman e;
 
     public SlotResult2(EntityHuman entityhuman, IInventory iinventory, int i, int j, int k) {
         super(iinventory, i, j, k);
-        this.d = entityhuman;
+        this.e = entityhuman;
     }
 
     public boolean isAllowed(ItemStack itemstack) {
@@ -14,13 +14,13 @@ public class SlotResult2 extends Slot {
     }
 
     public void a(ItemStack itemstack) {
-        itemstack.b(this.d.world, this.d);
+        itemstack.c(this.e.world, this.e);
         if (itemstack.id == Item.IRON_INGOT.id) {
-            this.d.a(AchievementList.k, 1);
+            this.e.a((Statistic) AchievementList.k, 1);
         }
 
         if (itemstack.id == Item.COOKED_FISH.id) {
-            this.d.a(AchievementList.p, 1);
+            this.e.a((Statistic) AchievementList.p, 1);
         }
 
         super.a(itemstack);
