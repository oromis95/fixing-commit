@@ -2,7 +2,9 @@ package net.minecraft.server;
 
 import java.net.InetAddress;
 import java.net.ServerSocket;
+import java.net.Socket;
 import java.util.ArrayList;
+import java.util.HashMap;
 import java.util.logging.Level;
 import java.util.logging.Logger;
 
@@ -16,6 +18,7 @@ public class NetworkListenThread {
     private ArrayList g = new ArrayList();
     private ArrayList h = new ArrayList();
     public MinecraftServer c;
+    private HashMap i = new HashMap();
 
     public NetworkListenThread(MinecraftServer minecraftserver, InetAddress inetaddress, int i) {
         this.c = minecraftserver;
@@ -26,6 +29,15 @@ public class NetworkListenThread {
         this.e.start();
     }
 
+    public void a(Socket socket) {
+        InetAddress inetaddress = socket.getInetAddress();
+        HashMap hashmap = this.i;
+
+        synchronized (this.i) {
+            this.i.remove(inetaddress);
+        }
+    }
+
     public void a(NetServerHandler netserverhandler) {
         this.h.add(netserverhandler);
     }
@@ -80,7 +92,11 @@ public class NetworkListenThread {
         return networklistenthread.d;
     }
 
-    static int b(NetworkListenThread networklistenthread) {
+    static HashMap b(NetworkListenThread networklistenthread) {
+        return networklistenthread.i;
+    }
+
+    static int c(NetworkListenThread networklistenthread) {
         return networklistenthread.f++;
     }
 
