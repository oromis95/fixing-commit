@@ -10,6 +10,10 @@ public class BlockGrass extends Block {
         this.a(true);
     }
 
+    public int a(int i, int j) {
+        return i == 1 ? 0 : (i == 0 ? 2 : 3);
+    }
+
     public void a(World world, int i, int j, int k, Random random) {
         if (!world.isStatic) {
             if (world.getLightLevel(i, j + 1, k) < 4 && Block.q[world.getTypeId(i, j + 1, k)] > 2) {
