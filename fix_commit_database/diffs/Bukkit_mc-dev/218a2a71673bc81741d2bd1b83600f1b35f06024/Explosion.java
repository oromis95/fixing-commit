@@ -105,7 +105,7 @@ public class Explosion {
                 double d9 = (double) this.world.a(vec3d, entity.boundingBox);
                 double d10 = (1.0D - d7) * d9;
 
-                entity.damageEntity(this.source, (int) ((d10 * d10 + d10) / 2.0D * 8.0D * (double) this.size + 1.0D));
+                entity.damageEntity(DamageSource.k, (int) ((d10 * d10 + d10) / 2.0D * 8.0D * (double) this.size + 1.0D));
                 entity.motX += d0 * d10;
                 entity.motY += d1 * d10;
                 entity.motZ += d2 * d10;
@@ -134,6 +134,7 @@ public class Explosion {
 
     public void a(boolean flag) {
         this.world.makeSound(this.posX, this.posY, this.posZ, "random.explode", 4.0F, (1.0F + (this.world.random.nextFloat() - this.world.random.nextFloat()) * 0.2F) * 0.7F);
+        this.world.a("hugeexplosion", this.posX, this.posY, this.posZ, 0.0D, 0.0D, 0.0D);
         ArrayList arraylist = new ArrayList();
 
         arraylist.addAll(this.blocks);
@@ -170,7 +171,7 @@ public class Explosion {
             if (i1 > 0) {
                 Block.byId[i1].dropNaturally(this.world, j, k, l, this.world.getData(j, k, l), 0.3F);
                 this.world.setTypeId(j, k, l, 0);
-                Block.byId[i1].d(this.world, j, k, l);
+                Block.byId[i1].a_(this.world, j, k, l);
             }
         }
     }
