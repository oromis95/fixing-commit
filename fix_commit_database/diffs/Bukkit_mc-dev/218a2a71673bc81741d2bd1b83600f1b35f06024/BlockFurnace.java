@@ -18,8 +18,8 @@ public class BlockFurnace extends BlockContainer {
         return Block.FURNACE.id;
     }
 
-    public void c(World world, int i, int j, int k) {
-        super.c(world, i, j, k);
+    public void a(World world, int i, int j, int k) {
+        super.a(world, i, j, k);
         this.g(world, i, j, k);
     }
 
@@ -61,7 +61,10 @@ public class BlockFurnace extends BlockContainer {
         } else {
             TileEntityFurnace tileentityfurnace = (TileEntityFurnace) world.getTileEntity(i, j, k);
 
-            entityhuman.a(tileentityfurnace);
+            if (tileentityfurnace != null) {
+                entityhuman.a(tileentityfurnace);
+            }
+
             return true;
         }
     }
@@ -79,11 +82,13 @@ public class BlockFurnace extends BlockContainer {
 
         c = false;
         world.setData(i, j, k, l);
-        tileentity.j();
-        world.setTileEntity(i, j, k, tileentity);
+        if (tileentity != null) {
+            tileentity.n();
+            world.setTileEntity(i, j, k, tileentity);
+        }
     }
 
-    protected TileEntity a_() {
+    public TileEntity a_() {
         return new TileEntityFurnace();
     }
 
@@ -111,29 +116,31 @@ public class BlockFurnace extends BlockContainer {
         if (!c) {
             TileEntityFurnace tileentityfurnace = (TileEntityFurnace) world.getTileEntity(i, j, k);
 
-            for (int l = 0; l < tileentityfurnace.getSize(); ++l) {
-                ItemStack itemstack = tileentityfurnace.getItem(l);
+            if (tileentityfurnace != null) {
+                for (int l = 0; l < tileentityfurnace.getSize(); ++l) {
+                    ItemStack itemstack = tileentityfurnace.getItem(l);
 
-                if (itemstack != null) {
-                    float f = this.a.nextFloat() * 0.8F + 0.1F;
-                    float f1 = this.a.nextFloat() * 0.8F + 0.1F;
-                    float f2 = this.a.nextFloat() * 0.8F + 0.1F;
+                    if (itemstack != null) {
+                        float f = this.a.nextFloat() * 0.8F + 0.1F;
+                        float f1 = this.a.nextFloat() * 0.8F + 0.1F;
+                        float f2 = this.a.nextFloat() * 0.8F + 0.1F;
 
-                    while (itemstack.count > 0) {
-                        int i1 = this.a.nextInt(21) + 10;
+                        while (itemstack.count > 0) {
+                            int i1 = this.a.nextInt(21) + 10;
 
-                        if (i1 > itemstack.count) {
-                            i1 = itemstack.count;
-                        }
+                            if (i1 > itemstack.count) {
+                                i1 = itemstack.count;
+                            }
 
-                        itemstack.count -= i1;
-                        EntityItem entityitem = new EntityItem(world, (double) ((float) i + f), (double) ((float) j + f1), (double) ((float) k + f2), new ItemStack(itemstack.id, i1, itemstack.getData()));
-                        float f3 = 0.05F;
+                            itemstack.count -= i1;
+                            EntityItem entityitem = new EntityItem(world, (double) ((float) i + f), (double) ((float) j + f1), (double) ((float) k + f2), new ItemStack(itemstack.id, i1, itemstack.getData()));
+                            float f3 = 0.05F;
 
-                        entityitem.motX = (double) ((float) this.a.nextGaussian() * f3);
-                        entityitem.motY = (double) ((float) this.a.nextGaussian() * f3 + 0.2F);
-                        entityitem.motZ = (double) ((float) this.a.nextGaussian() * f3);
-                        world.addEntity(entityitem);
+                            entityitem.motX = (double) ((float) this.a.nextGaussian() * f3);
+                            entityitem.motY = (double) ((float) this.a.nextGaussian() * f3 + 0.2F);
+                            entityitem.motZ = (double) ((float) this.a.nextGaussian() * f3);
+                            world.addEntity(entityitem);
+                        }
                     }
                 }
             }
