@@ -2,14 +2,14 @@ package net.minecraft.server;
 
 class SlotArmor extends Slot {
 
-    final int d;
+    final int e;
 
-    final ContainerPlayer e;
+    final ContainerPlayer f;
 
     SlotArmor(ContainerPlayer containerplayer, IInventory iinventory, int i, int j, int k, int l) {
         super(iinventory, i, j, k);
-        this.e = containerplayer;
-        this.d = l;
+        this.f = containerplayer;
+        this.e = l;
     }
 
     public int d() {
@@ -17,6 +17,6 @@ class SlotArmor extends Slot {
     }
 
     public boolean isAllowed(ItemStack itemstack) {
-        return itemstack.getItem() instanceof ItemArmor ? ((ItemArmor) itemstack.getItem()).bk == this.d : (itemstack.getItem().id == Block.PUMPKIN.id ? this.d == 0 : false);
+        return itemstack.getItem() instanceof ItemArmor ? ((ItemArmor) itemstack.getItem()).bt == this.e : (itemstack.getItem().id == Block.PUMPKIN.id ? this.e == 0 : false);
     }
 }
