@@ -42,8 +42,8 @@ public class EntityFireball extends Entity {
         this.e = d2 / d3 * 0.1D;
     }
 
-    public void m_() {
-        super.m_();
+    public void s_() {
+        super.s_();
         this.fireTicks = 10;
         if (this.a > 0) {
             --this.a;
@@ -88,13 +88,13 @@ public class EntityFireball extends Entity {
         for (int j = 0; j < list.size(); ++j) {
             Entity entity1 = (Entity) list.get(j);
 
-            if (entity1.l_() && (entity1 != this.shooter || this.l >= 25)) {
+            if (entity1.r_() && (entity1 != this.shooter || this.l >= 25)) {
                 float f = 0.3F;
                 AxisAlignedBB axisalignedbb = entity1.boundingBox.b((double) f, (double) f, (double) f);
                 MovingObjectPosition movingobjectposition1 = axisalignedbb.a(vec3d, vec3d1);
 
                 if (movingobjectposition1 != null) {
-                    double d1 = vec3d.a(movingobjectposition1.f);
+                    double d1 = vec3d.b(movingobjectposition1.f);
 
                     if (d1 < d0 || d0 == 0.0D) {
                         entity = entity1;
@@ -110,7 +110,7 @@ public class EntityFireball extends Entity {
 
         if (movingobjectposition != null) {
             if (!this.world.isStatic) {
-                if (movingobjectposition.entity != null && movingobjectposition.entity.damageEntity(this.shooter, 0)) {
+                if (movingobjectposition.entity != null && movingobjectposition.entity.damageEntity(DamageSource.a(this, this.shooter), 0)) {
                     ;
                 }
 
@@ -147,7 +147,7 @@ public class EntityFireball extends Entity {
         this.yaw = this.lastYaw + (this.yaw - this.lastYaw) * 0.2F;
         float f2 = 0.95F;
 
-        if (this.ad()) {
+        if (this.ao()) {
             for (int k = 0; k < 4; ++k) {
                 float f3 = 0.25F;
 
@@ -185,14 +185,14 @@ public class EntityFireball extends Entity {
         this.j = nbttagcompound.c("inGround") == 1;
     }
 
-    public boolean l_() {
+    public boolean r_() {
         return true;
     }
 
-    public boolean damageEntity(Entity entity, int i) {
-        this.af();
-        if (entity != null) {
-            Vec3D vec3d = entity.Z();
+    public boolean damageEntity(DamageSource damagesource, int i) {
+        this.aq();
+        if (damagesource.a() != null) {
+            Vec3D vec3d = damagesource.a().ai();
 
             if (vec3d != null) {
                 this.motX = vec3d.a;
