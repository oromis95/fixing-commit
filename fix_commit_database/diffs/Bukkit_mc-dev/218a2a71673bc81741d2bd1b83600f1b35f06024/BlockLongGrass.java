@@ -18,4 +18,13 @@ public class BlockLongGrass extends BlockFlower {
     public int a(int i, Random random) {
         return random.nextInt(8) == 0 ? Item.SEEDS.id : -1;
     }
+
+    public void a(World world, EntityHuman entityhuman, int i, int j, int k, int l) {
+        if (!world.isStatic && entityhuman.K() != null && entityhuman.K().id == Item.SHEARS.id) {
+            entityhuman.a(StatisticList.C[this.id], 1);
+            this.a(world, i, j, k, new ItemStack(Block.LONG_GRASS, 1, l));
+        } else {
+            super.a(world, entityhuman, i, j, k, l);
+        }
+    }
 }
