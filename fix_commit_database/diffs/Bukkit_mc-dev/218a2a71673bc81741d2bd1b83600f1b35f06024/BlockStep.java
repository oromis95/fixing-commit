@@ -4,7 +4,7 @@ import java.util.Random;
 
 public class BlockStep extends Block {
 
-    public static final String[] a = new String[] { "stone", "sand", "wood", "cobble"};
+    public static final String[] a = new String[] { "stone", "sand", "wood", "cobble", "brick", "smoothStoneBrick"};
     private boolean b;
 
     public BlockStep(int i, boolean flag) {
@@ -18,7 +18,7 @@ public class BlockStep extends Block {
     }
 
     public int a(int i, int j) {
-        return j == 0 ? (i <= 1 ? 6 : 5) : (j == 1 ? (i == 0 ? 208 : (i == 1 ? 176 : 192)) : (j == 2 ? 4 : (j == 3 ? 16 : 6)));
+        return j == 0 ? (i <= 1 ? 6 : 5) : (j == 1 ? (i == 0 ? 208 : (i == 1 ? 176 : 192)) : (j == 2 ? 4 : (j == 3 ? 16 : (j == 4 ? Block.BRICK.textureId : (j == 5 ? Block.SMOOTH_BRICK.textureId : 6)))));
     }
 
     public int a(int i) {
@@ -29,9 +29,9 @@ public class BlockStep extends Block {
         return this.b;
     }
 
-    public void c(World world, int i, int j, int k) {
+    public void a(World world, int i, int j, int k) {
         if (this != Block.STEP) {
-            super.c(world, i, j, k);
+            super.a(world, i, j, k);
         }
 
         int l = world.getTypeId(i, j - 1, k);
