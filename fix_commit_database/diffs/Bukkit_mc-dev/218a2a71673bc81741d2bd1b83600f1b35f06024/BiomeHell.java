@@ -2,11 +2,12 @@ package net.minecraft.server;
 
 public class BiomeHell extends BiomeBase {
 
-    public BiomeHell() {
-        this.s.clear();
-        this.t.clear();
-        this.u.clear();
-        this.s.add(new BiomeMeta(EntityGhast.class, 10));
-        this.s.add(new BiomeMeta(EntityPigZombie.class, 10));
+    public BiomeHell(int i) {
+        super(i);
+        this.v.clear();
+        this.w.clear();
+        this.x.clear();
+        this.v.add(new BiomeMeta(EntityGhast.class, 10, 4, 4));
+        this.v.add(new BiomeMeta(EntityPigZombie.class, 10, 4, 4));
     }
 }
