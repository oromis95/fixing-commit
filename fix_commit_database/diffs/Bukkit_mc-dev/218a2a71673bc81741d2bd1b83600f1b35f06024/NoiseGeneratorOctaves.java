@@ -28,26 +28,38 @@ public class NoiseGeneratorOctaves extends NoiseGenerator {
         return d2;
     }
 
-    public double[] a(double[] adouble, double d0, double d1, double d2, int i, int j, int k, double d3, double d4, double d5) {
+    public double[] a(double[] adouble, int i, int j, int k, int l, int i1, int j1, double d0, double d1, double d2) {
         if (adouble == null) {
-            adouble = new double[i * j * k];
+            adouble = new double[l * i1 * j1];
         } else {
-            for (int l = 0; l < adouble.length; ++l) {
-                adouble[l] = 0.0D;
+            for (int k1 = 0; k1 < adouble.length; ++k1) {
+                adouble[k1] = 0.0D;
             }
         }
 
-        double d6 = 1.0D;
+        double d3 = 1.0D;
+
+        for (int l1 = 0; l1 < this.b; ++l1) {
+            double d4 = (double) i * d3 * d0;
+            double d5 = (double) j * d3 * d1;
+            double d6 = (double) k * d3 * d2;
+            long i2 = MathHelper.c(d4);
+            long j2 = MathHelper.c(d6);
 
-        for (int i1 = 0; i1 < this.b; ++i1) {
-            this.a[i1].a(adouble, d0, d1, d2, i, j, k, d3 * d6, d4 * d6, d5 * d6, d6);
-            d6 /= 2.0D;
+            d4 -= (double) i2;
+            d6 -= (double) j2;
+            i2 %= 16777216L;
+            j2 %= 16777216L;
+            d4 += (double) i2;
+            d6 += (double) j2;
+            this.a[l1].a(adouble, d4, d5, d6, l, i1, j1, d0 * d3, d1 * d3, d2 * d3, d3);
+            d3 /= 2.0D;
         }
 
         return adouble;
     }
 
     public double[] a(double[] adouble, int i, int j, int k, int l, double d0, double d1, double d2) {
-        return this.a(adouble, (double) i, 10.0D, (double) j, k, 1, l, d0, 1.0D, d1);
+        return this.a(adouble, i, 10, j, k, 1, l, d0, 1.0D, d1);
     }
 }
