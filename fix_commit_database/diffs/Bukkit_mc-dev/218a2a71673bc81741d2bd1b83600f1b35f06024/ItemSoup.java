@@ -4,10 +4,11 @@ public class ItemSoup extends ItemFood {
 
     public ItemSoup(int i, int j) {
         super(i, j, false);
+        this.c(1);
     }
 
-    public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
-        super.a(itemstack, world, entityhuman);
+    public ItemStack b(ItemStack itemstack, World world, EntityHuman entityhuman) {
+        super.b(itemstack, world, entityhuman);
         return new ItemStack(Item.BOWL);
     }
 }
