@@ -10,9 +10,11 @@ public class ItemReed extends Item {
     }
 
     public boolean a(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
-        if (world.getTypeId(i, j, k) == Block.SNOW.id) {
+        int i1 = world.getTypeId(i, j, k);
+
+        if (i1 == Block.SNOW.id) {
             l = 0;
-        } else {
+        } else if (i1 != Block.VINE.id) {
             if (l == 0) {
                 --j;
             }
@@ -38,15 +40,20 @@ public class ItemReed extends Item {
             }
         }
 
-        if (itemstack.count == 0) {
+        if (!entityhuman.c(i, j, k)) {
+            return false;
+        } else if (itemstack.count == 0) {
             return false;
         } else {
             if (world.a(this.id, i, j, k, false, l)) {
                 Block block = Block.byId[this.id];
 
                 if (world.setTypeId(i, j, k, this.id)) {
-                    Block.byId[this.id].postPlace(world, i, j, k, l);
-                    Block.byId[this.id].postPlace(world, i, j, k, entityhuman);
+                    if (world.getTypeId(i, j, k) == this.id) {
+                        Block.byId[this.id].postPlace(world, i, j, k, l);
+                        Block.byId[this.id].postPlace(world, i, j, k, entityhuman);
+                    }
+
                     world.makeSound((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), block.stepSound.getName(), (block.stepSound.getVolume1() + 1.0F) / 2.0F, block.stepSound.getVolume2() * 0.8F);
                     --itemstack.count;
                 }
