@@ -9,28 +9,45 @@ public class EntitySkeleton extends EntityMonster {
         this.texture = "/mob/skeleton.png";
     }
 
-    protected String g() {
+    protected String h() {
         return "mob.skeleton";
     }
 
-    protected String h() {
+    protected String i() {
         return "mob.skeletonhurt";
     }
 
-    protected String i() {
+    protected String j() {
         return "mob.skeletonhurt";
     }
 
-    public void v() {
-        if (this.world.d()) {
-            float f = this.c(1.0F);
+    public boolean a(DamageSource damagesource, int i) {
+        return super.damageEntity(damagesource, i);
+    }
+
+    public void a(DamageSource damagesource) {
+        super.die(damagesource);
+        if (damagesource.e() instanceof EntityArrow && damagesource.a() instanceof EntityHuman) {
+            EntityHuman entityhuman = (EntityHuman) damagesource.a();
+            double d0 = entityhuman.locX - this.locX;
+            double d1 = entityhuman.locZ - this.locZ;
+
+            if (d0 * d0 + d1 * d1 >= 2500.0D) {
+                entityhuman.a((Statistic) AchievementList.v);
+            }
+        }
+    }
+
+    public void s() {
+        if (this.world.d() && !this.world.isStatic) {
+            float f = this.a_(1.0F);
 
             if (f > 0.5F && this.world.isChunkLoaded(MathHelper.floor(this.locX), MathHelper.floor(this.locY), MathHelper.floor(this.locZ)) && this.random.nextFloat() * 30.0F < (f - 0.4F) * 2.0F) {
                 this.fireTicks = 300;
             }
         }
 
-        super.v();
+        super.s();
     }
 
     protected void a(Entity entity, float f) {
@@ -39,16 +56,14 @@ public class EntitySkeleton extends EntityMonster {
             double d1 = entity.locZ - this.locZ;
 
             if (this.attackTicks == 0) {
-                EntityArrow entityarrow = new EntityArrow(this.world, this);
-
-                ++entityarrow.locY;
-                double d2 = entity.locY + (double) entity.t() - 0.20000000298023224D - entityarrow.locY;
+                EntityArrow entityarrow = new EntityArrow(this.world, this, 1.0F);
+                double d2 = entity.locY + (double) entity.t() - 0.699999988079071D - entityarrow.locY;
                 float f1 = MathHelper.a(d0 * d0 + d1 * d1) * 0.2F;
 
                 this.world.makeSound(this, "random.bow", 1.0F, 1.0F / (this.random.nextFloat() * 0.4F + 0.8F));
                 this.world.addEntity(entityarrow);
-                entityarrow.a(d0, d2 + (double) f1, d1, 0.6F, 12.0F);
-                this.attackTicks = 30;
+                entityarrow.a(d0, d2 + (double) f1, d1, 1.6F, 12.0F);
+                this.attackTicks = 60;
             }
 
             this.yaw = (float) (Math.atan2(d1, d0) * 180.0D / 3.1415927410125732D) - 90.0F;
@@ -64,11 +79,11 @@ public class EntitySkeleton extends EntityMonster {
         super.a(nbttagcompound);
     }
 
-    protected int j() {
+    protected int k() {
         return Item.ARROW.id;
     }
 
-    protected void q() {
+    protected void a(boolean flag) {
         int i = this.random.nextInt(3);
 
         int j;
