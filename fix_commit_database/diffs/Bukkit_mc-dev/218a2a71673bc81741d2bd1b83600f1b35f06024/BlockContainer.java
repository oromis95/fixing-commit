@@ -4,23 +4,32 @@ public abstract class BlockContainer extends Block {
 
     protected BlockContainer(int i, Material material) {
         super(i, material);
-        isTileEntity[i] = true;
+        isTileEntity[this.id] = true;
     }
 
     protected BlockContainer(int i, int j, Material material) {
         super(i, j, material);
-        isTileEntity[i] = true;
+        isTileEntity[this.id] = true;
     }
 
-    public void c(World world, int i, int j, int k) {
-        super.c(world, i, j, k);
+    public void a(World world, int i, int j, int k) {
+        super.a(world, i, j, k);
         world.setTileEntity(i, j, k, this.a_());
     }
 
     public void remove(World world, int i, int j, int k) {
         super.remove(world, i, j, k);
-        world.o(i, j, k);
+        world.n(i, j, k);
     }
 
-    protected abstract TileEntity a_();
+    public abstract TileEntity a_();
+
+    public void a(World world, int i, int j, int k, int l, int i1) {
+        super.a(world, i, j, k, l, i1);
+        TileEntity tileentity = world.getTileEntity(i, j, k);
+
+        if (tileentity != null) {
+            tileentity.b(l, i1);
+        }
+    }
 }
