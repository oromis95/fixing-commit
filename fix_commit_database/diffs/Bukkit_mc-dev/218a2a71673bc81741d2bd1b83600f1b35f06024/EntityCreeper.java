@@ -31,7 +31,7 @@ public class EntityCreeper extends EntityMonster {
     protected void b(Entity entity, float f) {
         if (!this.world.isStatic) {
             if (this.fuseTicks > 0) {
-                this.e(-1);
+                this.b(-1);
                 --this.fuseTicks;
                 if (this.fuseTicks < 0) {
                     this.fuseTicks = 0;
@@ -40,10 +40,10 @@ public class EntityCreeper extends EntityMonster {
         }
     }
 
-    public void m_() {
+    public void s_() {
         this.b = this.fuseTicks;
         if (this.world.isStatic) {
-            int i = this.x();
+            int i = this.w();
 
             if (i > 0 && this.fuseTicks == 0) {
                 this.world.makeSound(this, "random.fuse", 1.0F, 0.5F);
@@ -59,9 +59,9 @@ public class EntityCreeper extends EntityMonster {
             }
         }
 
-        super.m_();
+        super.s_();
         if (this.target == null && this.fuseTicks > 0) {
-            this.e(-1);
+            this.b(-1);
             --this.fuseTicks;
             if (this.fuseTicks < 0) {
                 this.fuseTicks = 0;
@@ -69,27 +69,27 @@ public class EntityCreeper extends EntityMonster {
         }
     }
 
-    protected String h() {
+    protected String i() {
         return "mob.creeper";
     }
 
-    protected String i() {
+    protected String j() {
         return "mob.creeperdeath";
     }
 
-    public void die(Entity entity) {
-        super.die(entity);
-        if (entity instanceof EntitySkeleton) {
+    public void die(DamageSource damagesource) {
+        super.die(damagesource);
+        if (damagesource.a() instanceof EntitySkeleton) {
             this.b(Item.GOLD_RECORD.id + this.random.nextInt(2), 1);
         }
     }
 
     protected void a(Entity entity, float f) {
         if (!this.world.isStatic) {
-            int i = this.x();
+            int i = this.w();
 
             if ((i > 0 || f >= 3.0F) && (i <= 0 || f >= 7.0F)) {
-                this.e(-1);
+                this.b(-1);
                 --this.fuseTicks;
                 if (this.fuseTicks < 0) {
                     this.fuseTicks = 0;
@@ -99,7 +99,7 @@ public class EntityCreeper extends EntityMonster {
                     this.world.makeSound(this, "random.fuse", 1.0F, 0.5F);
                 }
 
-                this.e(1);
+                this.b(1);
                 ++this.fuseTicks;
                 if (this.fuseTicks >= 30) {
                     if (this.isPowered()) {
@@ -120,15 +120,15 @@ public class EntityCreeper extends EntityMonster {
         return this.datawatcher.a(17) == 1;
     }
 
-    protected int j() {
+    protected int k() {
         return Item.SULPHUR.id;
     }
 
-    private int x() {
+    private int w() {
         return this.datawatcher.a(16);
     }
 
-    private void e(int i) {
+    private void b(int i) {
         this.datawatcher.watch(16, Byte.valueOf((byte) i));
     }
 
