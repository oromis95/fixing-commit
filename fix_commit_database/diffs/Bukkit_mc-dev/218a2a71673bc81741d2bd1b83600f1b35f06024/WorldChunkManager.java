@@ -1,23 +1,38 @@
 package net.minecraft.server;
 
+import java.util.ArrayList;
+import java.util.List;
 import java.util.Random;
 
 public class WorldChunkManager {
 
-    private NoiseGeneratorOctaves2 e;
-    private NoiseGeneratorOctaves2 f;
-    private NoiseGeneratorOctaves2 g;
-    public double[] temperature;
-    public double[] rain;
-    public double[] c;
-    public BiomeBase[] d;
-
-    protected WorldChunkManager() {}
+    private GenLayer temperature;
+    private GenLayer rain;
+    private GenLayer c;
+    private GenLayer d;
+    private BiomeCache e;
+    private List f;
+
+    protected WorldChunkManager() {
+        this.e = new BiomeCache(this);
+        this.f = new ArrayList();
+        this.f.add(BiomeBase.FOREST);
+        this.f.add(BiomeBase.SWAMPLAND);
+        this.f.add(BiomeBase.TAIGA);
+    }
 
     public WorldChunkManager(World world) {
-        this.e = new NoiseGeneratorOctaves2(new Random(world.getSeed() * 9871L), 4);
-        this.f = new NoiseGeneratorOctaves2(new Random(world.getSeed() * 39811L), 4);
-        this.g = new NoiseGeneratorOctaves2(new Random(world.getSeed() * 543321L), 2);
+        this();
+        GenLayer[] agenlayer = GenLayer.a(world.getSeed());
+
+        this.temperature = agenlayer[0];
+        this.rain = agenlayer[1];
+        this.c = agenlayer[2];
+        this.d = agenlayer[3];
+    }
+
+    public List a() {
+        return this.f;
     }
 
     public BiomeBase a(ChunkCoordIntPair chunkcoordintpair) {
@@ -25,91 +40,138 @@ public class WorldChunkManager {
     }
 
     public BiomeBase getBiome(int i, int j) {
-        return this.getBiomeData(i, j, 1, 1)[0];
+        return this.e.a(i, j);
     }
 
-    public BiomeBase[] getBiomeData(int i, int j, int k, int l) {
-        this.d = this.a(this.d, i, j, k, l);
-        return this.d;
-    }
+    public float[] b(float[] afloat, int i, int j, int k, int l) {
+        IntCache.a();
+        if (afloat == null || afloat.length < k * l) {
+            afloat = new float[k * l];
+        }
+
+        int[] aint = this.d.a(i, j, k, l);
+
+        for (int i1 = 0; i1 < k * l; ++i1) {
+            float f = (float) aint[i1] / 65536.0F;
+
+            if (f > 1.0F) {
+                f = 1.0F;
+            }
 
-    public double[] a(double[] adouble, int i, int j, int k, int l) {
-        if (adouble == null || adouble.length < k * l) {
-            adouble = new double[k * l];
+            afloat[i1] = f;
         }
 
-        adouble = this.e.a(adouble, (double) i, (double) j, k, l, 0.02500000037252903D, 0.02500000037252903D, 0.25D);
-        this.c = this.g.a(this.c, (double) i, (double) j, k, l, 0.25D, 0.25D, 0.5882352941176471D);
-        int i1 = 0;
+        return afloat;
+    }
 
-        for (int j1 = 0; j1 < k; ++j1) {
-            for (int k1 = 0; k1 < l; ++k1) {
-                double d0 = this.c[i1] * 1.1D + 0.5D;
-                double d1 = 0.01D;
-                double d2 = 1.0D - d1;
-                double d3 = (adouble[i1] * 0.15D + 0.7D) * d2 + d0 * d1;
+    public float[] a(float[] afloat, int i, int j, int k, int l) {
+        IntCache.a();
+        if (afloat == null || afloat.length < k * l) {
+            afloat = new float[k * l];
+        }
 
-                d3 = 1.0D - (1.0D - d3) * (1.0D - d3);
-                if (d3 < 0.0D) {
-                    d3 = 0.0D;
-                }
+        int[] aint = this.c.a(i, j, k, l);
 
-                if (d3 > 1.0D) {
-                    d3 = 1.0D;
-                }
+        for (int i1 = 0; i1 < k * l; ++i1) {
+            float f = (float) aint[i1] / 65536.0F;
 
-                adouble[i1] = d3;
-                ++i1;
+            if (f > 1.0F) {
+                f = 1.0F;
             }
+
+            afloat[i1] = f;
         }
 
-        return adouble;
+        return afloat;
+    }
+
+    public BiomeBase[] b(BiomeBase[] abiomebase, int i, int j, int k, int l) {
+        IntCache.a();
+        if (abiomebase == null || abiomebase.length < k * l) {
+            abiomebase = new BiomeBase[k * l];
+        }
+
+        int[] aint = this.temperature.a(i, j, k, l);
+
+        for (int i1 = 0; i1 < k * l; ++i1) {
+            abiomebase[i1] = BiomeBase.a[aint[i1]];
+        }
+
+        return abiomebase;
     }
 
     public BiomeBase[] a(BiomeBase[] abiomebase, int i, int j, int k, int l) {
+        return this.a(abiomebase, i, j, k, l, true);
+    }
+
+    public BiomeBase[] a(BiomeBase[] abiomebase, int i, int j, int k, int l, boolean flag) {
+        IntCache.a();
         if (abiomebase == null || abiomebase.length < k * l) {
             abiomebase = new BiomeBase[k * l];
         }
 
-        this.temperature = this.e.a(this.temperature, (double) i, (double) j, k, k, 0.02500000037252903D, 0.02500000037252903D, 0.25D);
-        this.rain = this.f.a(this.rain, (double) i, (double) j, k, k, 0.05000000074505806D, 0.05000000074505806D, 0.3333333333333333D);
-        this.c = this.g.a(this.c, (double) i, (double) j, k, k, 0.25D, 0.25D, 0.5882352941176471D);
-        int i1 = 0;
-
-        for (int j1 = 0; j1 < k; ++j1) {
-            for (int k1 = 0; k1 < l; ++k1) {
-                double d0 = this.c[i1] * 1.1D + 0.5D;
-                double d1 = 0.01D;
-                double d2 = 1.0D - d1;
-                double d3 = (this.temperature[i1] * 0.15D + 0.7D) * d2 + d0 * d1;
-
-                d1 = 0.0020D;
-                d2 = 1.0D - d1;
-                double d4 = (this.rain[i1] * 0.15D + 0.5D) * d2 + d0 * d1;
-
-                d3 = 1.0D - (1.0D - d3) * (1.0D - d3);
-                if (d3 < 0.0D) {
-                    d3 = 0.0D;
-                }
-
-                if (d4 < 0.0D) {
-                    d4 = 0.0D;
-                }
-
-                if (d3 > 1.0D) {
-                    d3 = 1.0D;
-                }
-
-                if (d4 > 1.0D) {
-                    d4 = 1.0D;
-                }
-
-                this.temperature[i1] = d3;
-                this.rain[i1] = d4;
-                abiomebase[i1++] = BiomeBase.a(d3, d4);
+        if (flag && k == 16 && l == 16 && (i & 15) == 0 && (j & 15) == 0) {
+            BiomeBase[] abiomebase1 = this.e.b(i, j);
+
+            System.arraycopy(abiomebase1, 0, abiomebase, 0, k * l);
+            return abiomebase;
+        } else {
+            int[] aint = this.rain.a(i, j, k, l);
+
+            for (int i1 = 0; i1 < k * l; ++i1) {
+                abiomebase[i1] = BiomeBase.a[aint[i1]];
             }
+
+            return abiomebase;
         }
+    }
 
-        return abiomebase;
+    public boolean a(int i, int j, int k, List list) {
+        int l = i - k >> 2;
+        int i1 = j - k >> 2;
+        int j1 = i + k >> 2;
+        int k1 = j + k >> 2;
+        int l1 = j1 - l + 1;
+        int i2 = k1 - i1 + 1;
+        int[] aint = this.temperature.a(l, i1, l1, i2);
+
+        for (int j2 = 0; j2 < l1 * i2; ++j2) {
+            BiomeBase biomebase = BiomeBase.a[aint[j2]];
+
+            if (!list.contains(biomebase)) {
+                return false;
+            }
+        }
+
+        return true;
+    }
+
+    public ChunkPosition a(int i, int j, int k, List list, Random random) {
+        int l = i - k >> 2;
+        int i1 = j - k >> 2;
+        int j1 = i + k >> 2;
+        int k1 = j + k >> 2;
+        int l1 = j1 - l + 1;
+        int i2 = k1 - i1 + 1;
+        int[] aint = this.temperature.a(l, i1, l1, i2);
+        ChunkPosition chunkposition = null;
+        int j2 = 0;
+
+        for (int k2 = 0; k2 < aint.length; ++k2) {
+            int l2 = l + k2 % l1 << 2;
+            int i3 = i1 + k2 / l1 << 2;
+            BiomeBase biomebase = BiomeBase.a[aint[k2]];
+
+            if (list.contains(biomebase) && (chunkposition == null || random.nextInt(j2 + 1) == 0)) {
+                chunkposition = new ChunkPosition(l2, 0, i3);
+                ++j2;
+            }
+        }
+
+        return chunkposition;
+    }
+
+    public void b() {
+        this.e.a();
     }
 }
