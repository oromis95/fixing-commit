@@ -10,17 +10,17 @@ public class ItemFishingRod extends Item {
 
     public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
         if (entityhuman.hookedFish != null) {
-            int i = entityhuman.hookedFish.h();
+            int i = entityhuman.hookedFish.i();
 
             itemstack.damage(i, entityhuman);
-            entityhuman.w();
+            entityhuman.v();
         } else {
             world.makeSound(entityhuman, "random.bow", 0.5F, 0.4F / (b.nextFloat() * 0.4F + 0.8F));
             if (!world.isStatic) {
                 world.addEntity(new EntityFish(world, entityhuman));
             }
 
-            entityhuman.w();
+            entityhuman.v();
         }
 
         return itemstack;
