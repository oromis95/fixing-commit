@@ -1,5 +1,7 @@
 package net.minecraft.server;
 
+import java.util.Random;
+
 public class MathHelper {
 
     private static float[] a = new float[65536];
@@ -34,10 +36,20 @@ public class MathHelper {
         return d0 < (double) i ? i - 1 : i;
     }
 
+    public static long c(double d0) {
+        long i = (long) d0;
+
+        return d0 < (double) i ? i - 1L : i;
+    }
+
     public static float abs(float f) {
         return f >= 0.0F ? f : -f;
     }
 
+    public static int a(int i) {
+        return i >= 0 ? i : -i;
+    }
+
     public static double a(double d0, double d1) {
         if (d0 < 0.0D) {
             d0 = -d0;
@@ -50,6 +62,10 @@ public class MathHelper {
         return d0 > d1 ? d0 : d1;
     }
 
+    public static int a(Random random, int i, int j) {
+        return i >= j ? i : random.nextInt(j - i + 1) + i;
+    }
+
     static {
         for (int i = 0; i < 65536; ++i) {
             a[i] = (float) Math.sin((double) i * 3.141592653589793D * 2.0D / 65536.0D);
