@@ -7,13 +7,50 @@ public class ItemBow extends Item {
         this.maxStackSize = 1;
     }
 
-    public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
-        if (entityhuman.inventory.b(Item.ARROW.id)) {
-            world.makeSound(entityhuman, "random.bow", 1.0F, 1.0F / (b.nextFloat() * 0.4F + 0.8F));
+    public void a(ItemStack itemstack, World world, EntityHuman entityhuman, int i) {
+        if (entityhuman.inventory.c(Item.ARROW.id)) {
+            int j = this.c(itemstack) - i;
+            float f = (float) j / 20.0F;
+
+            f = (f * f + f * 2.0F) / 3.0F;
+            if ((double) f < 0.1D) {
+                return;
+            }
+
+            if (f > 1.0F) {
+                f = 1.0F;
+            }
+
+            EntityArrow entityarrow = new EntityArrow(world, entityhuman, f * 2.0F);
+
+            if (f == 1.0F) {
+                entityarrow.d = true;
+            }
+
+            world.makeSound(entityhuman, "random.bow", 1.0F, 1.0F / (b.nextFloat() * 0.4F + 1.2F) + f * 0.5F);
+            entityhuman.inventory.b(Item.ARROW.id);
             if (!world.isStatic) {
-                world.addEntity(new EntityArrow(world, entityhuman));
+                world.addEntity(entityarrow);
             }
         }
+    }
+
+    public ItemStack b(ItemStack itemstack, World world, EntityHuman entityhuman) {
+        return itemstack;
+    }
+
+    public int c(ItemStack itemstack) {
+        return 72000;
+    }
+
+    public EnumAnimation b(ItemStack itemstack) {
+        return EnumAnimation.d;
+    }
+
+    public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
+        if (entityhuman.inventory.c(Item.ARROW.id)) {
+            entityhuman.a(itemstack, this.c(itemstack));
+        }
 
         return itemstack;
     }
