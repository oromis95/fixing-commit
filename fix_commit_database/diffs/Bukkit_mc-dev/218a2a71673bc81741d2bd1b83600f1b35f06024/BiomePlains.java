@@ -0,0 +1,11 @@
+package net.minecraft.server;
+
+public class BiomePlains extends BiomeBase {
+
+    protected BiomePlains(int i) {
+        super(i);
+        this.u.r = -999;
+        this.u.s = 4;
+        this.u.t = 10;
+    }
+}
