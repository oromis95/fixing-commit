@@ -2,8 +2,11 @@ package net.minecraft.server;
 
 public class ItemLog extends ItemBlock {
 
-    public ItemLog(int i) {
+    private Block a;
+
+    public ItemLog(int i, Block block) {
         super(i);
+        this.a = block;
         this.d(0);
         this.a(true);
     }
