@@ -5,21 +5,25 @@ import java.io.DataOutputStream;
 
 public class Packet70Bed extends Packet {
 
-    public static final String[] a = new String[] { "tile.bed.notValid", null, null};
+    public static final String[] a = new String[] { "tile.bed.notValid", null, null, "gameMode.changed"};
     public int b;
+    public int c;
 
     public Packet70Bed() {}
 
-    public Packet70Bed(int i) {
+    public Packet70Bed(int i, int j) {
         this.b = i;
+        this.c = j;
     }
 
     public void a(DataInputStream datainputstream) {
         this.b = datainputstream.readByte();
+        this.c = datainputstream.readByte();
     }
 
     public void a(DataOutputStream dataoutputstream) {
         dataoutputstream.writeByte(this.b);
+        dataoutputstream.writeByte(this.c);
     }
 
     public void a(NetHandler nethandler) {
@@ -27,6 +31,6 @@ public class Packet70Bed extends Packet {
     }
 
     public int a() {
-        return 1;
+        return 2;
     }
 }
