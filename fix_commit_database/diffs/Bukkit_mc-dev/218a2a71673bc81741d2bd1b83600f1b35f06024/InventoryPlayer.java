@@ -17,11 +17,11 @@ public class InventoryPlayer implements IInventory {
         return this.itemInHandIndex < 9 && this.itemInHandIndex >= 0 ? this.items[this.itemInHandIndex] : null;
     }
 
-    public static int e() {
+    public static int g() {
         return 9;
     }
 
-    private int d(int i) {
+    private int e(int i) {
         for (int j = 0; j < this.items.length; ++j) {
             if (this.items[j] != null && this.items[j].id == i) {
                 return j;
@@ -41,7 +41,7 @@ public class InventoryPlayer implements IInventory {
         return -1;
     }
 
-    private int k() {
+    private int m() {
         for (int i = 0; i < this.items.length; ++i) {
             if (this.items[i] == null) {
                 return i;
@@ -57,7 +57,7 @@ public class InventoryPlayer implements IInventory {
         int k = this.firstPartial(itemstack);
 
         if (k < 0) {
-            k = this.k();
+            k = this.m();
         }
 
         if (k < 0) {
@@ -88,7 +88,7 @@ public class InventoryPlayer implements IInventory {
         }
     }
 
-    public void f() {
+    public void h() {
         for (int i = 0; i < this.items.length; ++i) {
             if (this.items[i] != null) {
                 this.items[i].a(this.d.world, this.d, i, this.itemInHandIndex == i);
@@ -97,7 +97,7 @@ public class InventoryPlayer implements IInventory {
     }
 
     public boolean b(int i) {
-        int j = this.d(i);
+        int j = this.e(i);
 
         if (j < 0) {
             return false;
@@ -110,11 +110,17 @@ public class InventoryPlayer implements IInventory {
         }
     }
 
+    public boolean c(int i) {
+        int j = this.e(i);
+
+        return j >= 0;
+    }
+
     public boolean pickup(ItemStack itemstack) {
         int i;
 
         if (itemstack.f()) {
-            i = this.k();
+            i = this.m();
             if (i >= 0) {
                 this.items[i] = ItemStack.b(itemstack);
                 this.items[i].b = 5;
@@ -190,7 +196,7 @@ public class InventoryPlayer implements IInventory {
             if (this.items[i] != null) {
                 nbttagcompound = new NBTTagCompound();
                 nbttagcompound.a("Slot", (byte) i);
-                this.items[i].a(nbttagcompound);
+                this.items[i].b(nbttagcompound);
                 nbttaglist.a((NBTBase) nbttagcompound);
             }
         }
@@ -199,7 +205,7 @@ public class InventoryPlayer implements IInventory {
             if (this.armor[i] != null) {
                 nbttagcompound = new NBTTagCompound();
                 nbttagcompound.a("Slot", (byte) (i + 100));
-                this.armor[i].a(nbttagcompound);
+                this.armor[i].b(nbttagcompound);
                 nbttaglist.a((NBTBase) nbttagcompound);
             }
         }
@@ -214,9 +220,9 @@ public class InventoryPlayer implements IInventory {
         for (int i = 0; i < nbttaglist.c(); ++i) {
             NBTTagCompound nbttagcompound = (NBTTagCompound) nbttaglist.a(i);
             int j = nbttagcompound.c("Slot") & 255;
-            ItemStack itemstack = new ItemStack(nbttagcompound);
+            ItemStack itemstack = ItemStack.a(nbttagcompound);
 
-            if (itemstack.getItem() != null) {
+            if (itemstack != null) {
                 if (j >= 0 && j < this.items.length) {
                     this.items[j] = itemstack;
                 }
@@ -258,7 +264,7 @@ public class InventoryPlayer implements IInventory {
     }
 
     public boolean b(Block block) {
-        if (block.material.i()) {
+        if (block.material.k()) {
             return true;
         } else {
             ItemStack itemstack = this.getItem(this.itemInHandIndex);
@@ -267,7 +273,7 @@ public class InventoryPlayer implements IInventory {
         }
     }
 
-    public int g() {
+    public int i() {
         int i = 0;
         int j = 0;
         int k = 0;
@@ -280,7 +286,7 @@ public class InventoryPlayer implements IInventory {
 
                 j += k1;
                 k += i1;
-                int l1 = ((ItemArmor) this.armor[l].getItem()).bl;
+                int l1 = ((ItemArmor) this.armor[l].getItem()).bu;
 
                 i += l1;
             }
@@ -293,7 +299,7 @@ public class InventoryPlayer implements IInventory {
         }
     }
 
-    public void c(int i) {
+    public void d(int i) {
         for (int j = 0; j < this.armor.length; ++j) {
             if (this.armor[j] != null && this.armor[j].getItem() instanceof ItemArmor) {
                 this.armor[j].damage(i, this.d);
@@ -305,7 +311,7 @@ public class InventoryPlayer implements IInventory {
         }
     }
 
-    public void h() {
+    public void j() {
         int i;
 
         for (i = 0; i < this.items.length; ++i) {
@@ -332,12 +338,12 @@ public class InventoryPlayer implements IInventory {
         this.d.a(itemstack);
     }
 
-    public ItemStack j() {
+    public ItemStack l() {
         return this.f;
     }
 
-    public boolean a_(EntityHuman entityhuman) {
-        return this.d.dead ? false : entityhuman.g(this.d) <= 64.0D;
+    public boolean a(EntityHuman entityhuman) {
+        return this.d.dead ? false : entityhuman.h(this.d) <= 64.0D;
     }
 
     public boolean c(ItemStack itemstack) {
@@ -357,4 +363,8 @@ public class InventoryPlayer implements IInventory {
 
         return false;
     }
+
+    public void e() {}
+
+    public void t_() {}
 }
