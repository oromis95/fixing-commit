@@ -1,26 +1,27 @@
 package net.minecraft.server;
 
-public class EntityMonster extends EntityCreature implements IMonster {
+public abstract class EntityMonster extends EntityCreature implements IMonster {
 
     protected int damage = 2;
 
     public EntityMonster(World world) {
         super(world);
         this.health = 20;
+        this.ax = 5;
     }
 
-    public void v() {
-        float f = this.c(1.0F);
+    public void s() {
+        float f = this.a_(1.0F);
 
         if (f > 0.5F) {
-            this.ay += 2;
+            this.aO += 2;
         }
 
-        super.v();
+        super.s();
     }
 
-    public void m_() {
-        super.m_();
+    public void s_() {
+        super.s_();
         if (!this.world.isStatic && this.world.spawnMonsters == 0) {
             this.die();
         }
@@ -29,11 +30,13 @@ public class EntityMonster extends EntityCreature implements IMonster {
     protected Entity findTarget() {
         EntityHuman entityhuman = this.world.findNearbyPlayer(this, 16.0D);
 
-        return entityhuman != null && this.e(entityhuman) ? entityhuman : null;
+        return entityhuman != null && this.f(entityhuman) ? entityhuman : null;
     }
 
-    public boolean damageEntity(Entity entity, int i) {
-        if (super.damageEntity(entity, i)) {
+    public boolean damageEntity(DamageSource damagesource, int i) {
+        if (super.damageEntity(damagesource, i)) {
+            Entity entity = damagesource.a();
+
             if (this.passenger != entity && this.vehicle != entity) {
                 if (entity != this) {
                     this.target = entity;
@@ -48,15 +51,19 @@ public class EntityMonster extends EntityCreature implements IMonster {
         }
     }
 
+    protected boolean c(Entity entity) {
+        return entity.damageEntity(DamageSource.a((EntityLiving) this), this.damage);
+    }
+
     protected void a(Entity entity, float f) {
         if (this.attackTicks <= 0 && f < 2.0F && entity.boundingBox.e > this.boundingBox.b && entity.boundingBox.b < this.boundingBox.e) {
             this.attackTicks = 20;
-            entity.damageEntity(this, this.damage);
+            this.c(entity);
         }
     }
 
     protected float a(int i, int j, int k) {
-        return 0.5F - this.world.n(i, j, k);
+        return 0.5F - this.world.m(i, j, k);
     }
 
     public void b(NBTTagCompound nbttagcompound) {
@@ -77,12 +84,12 @@ public class EntityMonster extends EntityCreature implements IMonster {
         } else {
             int l = this.world.getLightLevel(i, j, k);
 
-            if (this.world.u()) {
-                int i1 = this.world.f;
+            if (this.world.t()) {
+                int i1 = this.world.k;
 
-                this.world.f = 10;
+                this.world.k = 10;
                 l = this.world.getLightLevel(i, j, k);
-                this.world.f = i1;
+                this.world.k = i1;
             }
 
             return l <= this.random.nextInt(8) && super.d();
