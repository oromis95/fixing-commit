@@ -2,10 +2,11 @@ package net.minecraft.server;
 
 public class BiomeSky extends BiomeBase {
 
-    public BiomeSky() {
-        this.s.clear();
-        this.t.clear();
-        this.u.clear();
-        this.t.add(new BiomeMeta(EntityChicken.class, 10));
+    public BiomeSky(int i) {
+        super(i);
+        this.v.clear();
+        this.w.clear();
+        this.x.clear();
+        this.w.add(new BiomeMeta(EntityChicken.class, 10, 4, 4));
     }
 }
