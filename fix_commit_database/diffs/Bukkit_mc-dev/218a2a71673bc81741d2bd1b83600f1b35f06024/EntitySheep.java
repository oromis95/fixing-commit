@@ -17,21 +17,17 @@ public class EntitySheep extends EntityAnimal {
         this.datawatcher.a(16, new Byte((byte) 0));
     }
 
-    public boolean damageEntity(Entity entity, int i) {
-        return super.damageEntity(entity, i);
-    }
-
-    protected void q() {
+    protected void a(boolean flag) {
         if (!this.isSheared()) {
             this.a(new ItemStack(Block.WOOL.id, 1, this.getColor()), 0.0F);
         }
     }
 
-    protected int j() {
+    protected int k() {
         return Block.WOOL.id;
     }
 
-    public boolean a(EntityHuman entityhuman) {
+    public boolean b(EntityHuman entityhuman) {
         ItemStack itemstack = entityhuman.inventory.getItemInHand();
 
         if (itemstack != null && itemstack.id == Item.SHEARS.id && !this.isSheared()) {
@@ -66,15 +62,15 @@ public class EntitySheep extends EntityAnimal {
         this.setColor(nbttagcompound.c("Color"));
     }
 
-    protected String g() {
+    protected String h() {
         return "mob.sheep";
     }
 
-    protected String h() {
+    protected String i() {
         return "mob.sheep";
     }
 
-    protected String i() {
+    protected String j() {
         return "mob.sheep";
     }
 
