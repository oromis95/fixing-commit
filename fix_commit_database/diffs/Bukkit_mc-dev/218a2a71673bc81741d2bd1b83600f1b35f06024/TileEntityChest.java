@@ -3,6 +3,15 @@ package net.minecraft.server;
 public class TileEntityChest extends TileEntity implements IInventory {
 
     private ItemStack[] items = new ItemStack[36];
+    public boolean a = false;
+    public TileEntityChest b;
+    public TileEntityChest c;
+    public TileEntityChest d;
+    public TileEntityChest e;
+    public float f;
+    public float g;
+    public int h;
+    private int q;
 
     public TileEntityChest() {}
 
@@ -61,7 +70,7 @@ public class TileEntityChest extends TileEntity implements IInventory {
             int j = nbttagcompound1.c("Slot") & 255;
 
             if (j >= 0 && j < this.items.length) {
-                this.items[j] = new ItemStack(nbttagcompound1);
+                this.items[j] = ItemStack.a(nbttagcompound1);
             }
         }
     }
@@ -75,7 +84,7 @@ public class TileEntityChest extends TileEntity implements IInventory {
                 NBTTagCompound nbttagcompound1 = new NBTTagCompound();
 
                 nbttagcompound1.a("Slot", (byte) i);
-                this.items[i].a(nbttagcompound1);
+                this.items[i].b(nbttagcompound1);
                 nbttaglist.a((NBTBase) nbttagcompound1);
             }
         }
@@ -87,7 +96,131 @@ public class TileEntityChest extends TileEntity implements IInventory {
         return 64;
     }
 
-    public boolean a_(EntityHuman entityhuman) {
+    public boolean a(EntityHuman entityhuman) {
         return this.world.getTileEntity(this.x, this.y, this.z) != this ? false : entityhuman.e((double) this.x + 0.5D, (double) this.y + 0.5D, (double) this.z + 0.5D) <= 64.0D;
     }
+
+    public void g() {
+        super.g();
+        this.a = false;
+    }
+
+    public void h() {
+        if (!this.a) {
+            this.a = true;
+            this.b = null;
+            this.c = null;
+            this.d = null;
+            this.e = null;
+            if (this.world.getTypeId(this.x - 1, this.y, this.z) == Block.CHEST.id) {
+                this.d = (TileEntityChest) this.world.getTileEntity(this.x - 1, this.y, this.z);
+            }
+
+            if (this.world.getTypeId(this.x + 1, this.y, this.z) == Block.CHEST.id) {
+                this.c = (TileEntityChest) this.world.getTileEntity(this.x + 1, this.y, this.z);
+            }
+
+            if (this.world.getTypeId(this.x, this.y, this.z - 1) == Block.CHEST.id) {
+                this.b = (TileEntityChest) this.world.getTileEntity(this.x, this.y, this.z - 1);
+            }
+
+            if (this.world.getTypeId(this.x, this.y, this.z + 1) == Block.CHEST.id) {
+                this.e = (TileEntityChest) this.world.getTileEntity(this.x, this.y, this.z + 1);
+            }
+
+            if (this.b != null) {
+                this.b.g();
+            }
+
+            if (this.e != null) {
+                this.e.g();
+            }
+
+            if (this.c != null) {
+                this.c.g();
+            }
+
+            if (this.d != null) {
+                this.d.g();
+            }
+        }
+    }
+
+    public void h_() {
+        super.h_();
+        this.h();
+        if (++this.q % 20 * 4 == 0) {
+            this.world.playNote(this.x, this.y, this.z, 1, this.h);
+        }
+
+        this.g = this.f;
+        float f = 0.1F;
+        double d0;
+        double d1;
+
+        if (this.h > 0 && this.f == 0.0F && this.b == null && this.d == null) {
+            d0 = (double) this.x + 0.5D;
+            d1 = (double) this.z + 0.5D;
+            if (this.e != null) {
+                d1 += 0.5D;
+            }
+
+            if (this.c != null) {
+                d0 += 0.5D;
+            }
+
+            this.world.makeSound(d0, (double) this.y + 0.5D, d1, "random.door_open", 1.0F, this.world.random.nextFloat() * 0.1F + 0.9F);
+        }
+
+        if (this.h == 0 && this.f > 0.0F || this.h > 0 && this.f < 1.0F) {
+            if (this.h > 0) {
+                this.f += f;
+            } else {
+                this.f -= f;
+            }
+
+            if (this.f > 1.0F) {
+                this.f = 1.0F;
+            }
+
+            if (this.f < 0.0F) {
+                this.f = 0.0F;
+                if (this.b == null && this.d == null) {
+                    d0 = (double) this.x + 0.5D;
+                    d1 = (double) this.z + 0.5D;
+                    if (this.e != null) {
+                        d1 += 0.5D;
+                    }
+
+                    if (this.c != null) {
+                        d0 += 0.5D;
+                    }
+
+                    this.world.makeSound(d0, (double) this.y + 0.5D, d1, "random.door_close", 1.0F, this.world.random.nextFloat() * 0.1F + 0.9F);
+                }
+            }
+        }
+    }
+
+    public void b(int i, int j) {
+        if (i == 1) {
+            this.h = j;
+        }
+    }
+
+    public void e() {
+        ++this.h;
+        this.world.playNote(this.x, this.y, this.z, 1, this.h);
+    }
+
+    public void t_() {
+        --this.h;
+        this.world.playNote(this.x, this.y, this.z, 1, this.h);
+    }
+
+    public void i() {
+        this.g();
+        this.h();
+        super.i();
+    }
 }
