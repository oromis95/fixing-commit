@@ -77,7 +77,7 @@ public class NetworkManager {
         }
     }
 
-    private boolean f() {
+    private boolean g() {
         boolean flag = false;
 
         try {
@@ -130,7 +130,7 @@ public class NetworkManager {
         this.r.interrupt();
     }
 
-    private boolean g() {
+    private boolean h() {
         boolean flag = false;
 
         try {
@@ -206,7 +206,7 @@ public class NetworkManager {
             this.w = 0;
         }
 
-        int i = 100;
+        int i = 1000;
 
         while (!this.m.isEmpty() && i-- >= 0) {
             Packet packet = (Packet) this.m.remove(0);
@@ -235,6 +235,10 @@ public class NetworkManager {
         return this.lowPriorityQueue.size();
     }
 
+    public Socket f() {
+        return this.socket;
+    }
+
     static boolean a(NetworkManager networkmanager) {
         return networkmanager.l;
     }
@@ -244,11 +248,11 @@ public class NetworkManager {
     }
 
     static boolean c(NetworkManager networkmanager) {
-        return networkmanager.g();
+        return networkmanager.h();
     }
 
     static boolean d(NetworkManager networkmanager) {
-        return networkmanager.f();
+        return networkmanager.g();
     }
 
     static DataOutputStream e(NetworkManager networkmanager) {
