@@ -7,7 +7,7 @@ public class MapGenCavesHell extends MapGenBase {
     public MapGenCavesHell() {}
 
     protected void a(int i, int j, byte[] abyte, double d0, double d1, double d2) {
-        this.a(i, j, abyte, d0, d1, d2, 1.0F + this.b.nextFloat() * 6.0F, 0.0F, 0.0F, -1, -1, 0.5D);
+        this.a(i, j, abyte, d0, d1, d2, 1.0F + this.c.nextFloat() * 6.0F, 0.0F, 0.0F, -1, -1, 0.5D);
     }
 
     protected void a(int i, int j, byte[] abyte, double d0, double d1, double d2, float f, float f1, float f2, int k, int l, double d3) {
@@ -15,10 +15,10 @@ public class MapGenCavesHell extends MapGenBase {
         double d5 = (double) (j * 16 + 8);
         float f3 = 0.0F;
         float f4 = 0.0F;
-        Random random = new Random(this.b.nextLong());
+        Random random = new Random(this.c.nextLong());
 
         if (l <= 0) {
-            int i1 = this.a * 16 - 16;
+            int i1 = this.b * 16 - 16;
 
             l = i1 - random.nextInt(i1 / 4);
         }
@@ -89,8 +89,10 @@ public class MapGenCavesHell extends MapGenBase {
                         i2 = 1;
                     }
 
-                    if (j2 > 120) {
-                        j2 = 120;
+                    this.d.getClass();
+                    if (j2 > 128 - 8) {
+                        this.d.getClass();
+                        j2 = 128 - 8;
                     }
 
                     if (k2 < 0) {
@@ -105,18 +107,24 @@ public class MapGenCavesHell extends MapGenBase {
 
                     int i3;
                     int j3;
+                    int k3;
+
+                    for (k3 = k1; !flag2 && k3 < l1; ++k3) {
+                        for (int l3 = k2; !flag2 && l3 < l2; ++l3) {
+                            for (int i4 = j2 + 1; !flag2 && i4 >= i2 - 1; --i4) {
+                                i3 = k3 * 16 + l3;
+                                this.d.getClass();
+                                j3 = i3 * 128 + i4;
+                                if (i4 >= 0) {
+                                    this.d.getClass();
+                                    if (i4 < 128) {
+                                        if (abyte[j3] == Block.LAVA.id || abyte[j3] == Block.STATIONARY_LAVA.id) {
+                                            flag2 = true;
+                                        }
 
-                    for (j3 = k1; !flag2 && j3 < l1; ++j3) {
-                        for (int k3 = k2; !flag2 && k3 < l2; ++k3) {
-                            for (int l3 = j2 + 1; !flag2 && l3 >= i2 - 1; --l3) {
-                                i3 = (j3 * 16 + k3) * 128 + l3;
-                                if (l3 >= 0 && l3 < 128) {
-                                    if (abyte[i3] == Block.LAVA.id || abyte[i3] == Block.STATIONARY_LAVA.id) {
-                                        flag2 = true;
-                                    }
-
-                                    if (l3 != i2 - 1 && j3 != k1 && j3 != l1 - 1 && k3 != k2 && k3 != l2 - 1) {
-                                        l3 = i2;
+                                        if (i4 != i2 - 1 && k3 != k1 && k3 != l1 - 1 && l3 != k2 && l3 != l2 - 1) {
+                                            i4 = i2;
+                                        }
                                     }
                                 }
                             }
@@ -124,25 +132,28 @@ public class MapGenCavesHell extends MapGenBase {
                     }
 
                     if (!flag2) {
-                        for (j3 = k1; j3 < l1; ++j3) {
-                            double d12 = ((double) (j3 + i * 16) + 0.5D - d0) / d6;
+                        for (k3 = k1; k3 < l1; ++k3) {
+                            double d12 = ((double) (k3 + i * 16) + 0.5D - d0) / d6;
+
+                            for (j3 = k2; j3 < l2; ++j3) {
+                                double d13 = ((double) (j3 + j * 16) + 0.5D - d2) / d6;
 
-                            for (i3 = k2; i3 < l2; ++i3) {
-                                double d13 = ((double) (i3 + j * 16) + 0.5D - d2) / d6;
-                                int i4 = (j3 * 16 + i3) * 128 + j2;
+                                i3 = k3 * 16 + j3;
+                                this.d.getClass();
+                                int j4 = i3 * 128 + j2;
 
-                                for (int j4 = j2 - 1; j4 >= i2; --j4) {
-                                    double d14 = ((double) j4 + 0.5D - d1) / d7;
+                                for (int k4 = j2 - 1; k4 >= i2; --k4) {
+                                    double d14 = ((double) k4 + 0.5D - d1) / d7;
 
                                     if (d14 > -0.7D && d12 * d12 + d14 * d14 + d13 * d13 < 1.0D) {
-                                        byte b0 = abyte[i4];
+                                        byte b0 = abyte[j4];
 
                                         if (b0 == Block.NETHERRACK.id || b0 == Block.DIRT.id || b0 == Block.GRASS.id) {
-                                            abyte[i4] = 0;
+                                            abyte[j4] = 0;
                                         }
                                     }
 
-                                    --i4;
+                                    --j4;
                                 }
                             }
                         }
@@ -157,27 +168,30 @@ public class MapGenCavesHell extends MapGenBase {
     }
 
     protected void a(World world, int i, int j, int k, int l, byte[] abyte) {
-        int i1 = this.b.nextInt(this.b.nextInt(this.b.nextInt(10) + 1) + 1);
+        int i1 = this.c.nextInt(this.c.nextInt(this.c.nextInt(10) + 1) + 1);
 
-        if (this.b.nextInt(5) != 0) {
+        if (this.c.nextInt(5) != 0) {
             i1 = 0;
         }
 
         for (int j1 = 0; j1 < i1; ++j1) {
-            double d0 = (double) (i * 16 + this.b.nextInt(16));
-            double d1 = (double) this.b.nextInt(128);
-            double d2 = (double) (j * 16 + this.b.nextInt(16));
+            double d0 = (double) (i * 16 + this.c.nextInt(16));
+            Random random = this.c;
+
+            world.getClass();
+            double d1 = (double) random.nextInt(128);
+            double d2 = (double) (j * 16 + this.c.nextInt(16));
             int k1 = 1;
 
-            if (this.b.nextInt(4) == 0) {
+            if (this.c.nextInt(4) == 0) {
                 this.a(k, l, abyte, d0, d1, d2);
-                k1 += this.b.nextInt(4);
+                k1 += this.c.nextInt(4);
             }
 
             for (int l1 = 0; l1 < k1; ++l1) {
-                float f = this.b.nextFloat() * 3.1415927F * 2.0F;
-                float f1 = (this.b.nextFloat() - 0.5F) * 2.0F / 8.0F;
-                float f2 = this.b.nextFloat() * 2.0F + this.b.nextFloat();
+                float f = this.c.nextFloat() * 3.1415927F * 2.0F;
+                float f1 = (this.c.nextFloat() - 0.5F) * 2.0F / 8.0F;
+                float f2 = this.c.nextFloat() * 2.0F + this.c.nextFloat();
 
                 this.a(k, l, abyte, d0, d1, d2, f2 * 2.0F, f, f1, 0, 0, 0.5D);
             }
