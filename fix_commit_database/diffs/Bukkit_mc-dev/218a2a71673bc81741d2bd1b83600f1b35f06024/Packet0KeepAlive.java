@@ -5,15 +5,27 @@ import java.io.DataOutputStream;
 
 public class Packet0KeepAlive extends Packet {
 
+    public int a;
+
     public Packet0KeepAlive() {}
 
-    public void a(NetHandler nethandler) {}
+    public Packet0KeepAlive(int i) {
+        this.a = i;
+    }
+
+    public void a(NetHandler nethandler) {
+        nethandler.a(this);
+    }
 
-    public void a(DataInputStream datainputstream) {}
+    public void a(DataInputStream datainputstream) {
+        this.a = datainputstream.readInt();
+    }
 
-    public void a(DataOutputStream dataoutputstream) {}
+    public void a(DataOutputStream dataoutputstream) {
+        dataoutputstream.writeInt(this.a);
+    }
 
     public int a() {
-        return 0;
+        return 4;
     }
 }
