@@ -2,10 +2,10 @@ package net.minecraft.server;
 
 public class ItemSpade extends ItemTool {
 
-    private static Block[] bk = new Block[] { Block.GRASS, Block.DIRT, Block.SAND, Block.GRAVEL, Block.SNOW, Block.SNOW_BLOCK, Block.CLAY, Block.SOIL};
+    private static Block[] bt = new Block[] { Block.GRASS, Block.DIRT, Block.SAND, Block.GRAVEL, Block.SNOW, Block.SNOW_BLOCK, Block.CLAY, Block.SOIL};
 
     public ItemSpade(int i, EnumToolMaterial enumtoolmaterial) {
-        super(i, 1, enumtoolmaterial, bk);
+        super(i, 1, enumtoolmaterial, bt);
     }
 
     public boolean a(Block block) {
