@@ -5,12 +5,20 @@ import java.io.DataOutputStream;
 
 public class Packet9Respawn extends Packet {
 
-    public byte a;
+    public long a;
+    public int b;
+    public int c;
+    public int d;
+    public int e;
 
     public Packet9Respawn() {}
 
-    public Packet9Respawn(byte b0) {
-        this.a = b0;
+    public Packet9Respawn(byte b0, byte b1, long i, int j, int k) {
+        this.b = b0;
+        this.c = b1;
+        this.a = i;
+        this.d = j;
+        this.e = k;
     }
 
     public void a(NetHandler nethandler) {
@@ -18,14 +26,22 @@ public class Packet9Respawn extends Packet {
     }
 
     public void a(DataInputStream datainputstream) {
-        this.a = datainputstream.readByte();
+        this.b = datainputstream.readByte();
+        this.c = datainputstream.readByte();
+        this.e = datainputstream.readByte();
+        this.d = datainputstream.readShort();
+        this.a = datainputstream.readLong();
     }
 
     public void a(DataOutputStream dataoutputstream) {
-        dataoutputstream.writeByte(this.a);
+        dataoutputstream.writeByte(this.b);
+        dataoutputstream.writeByte(this.c);
+        dataoutputstream.writeByte(this.e);
+        dataoutputstream.writeShort(this.d);
+        dataoutputstream.writeLong(this.a);
     }
 
     public int a() {
-        return 1;
+        return 13;
     }
 }
