@@ -11,7 +11,7 @@ public abstract class EntityLiving extends Entity {
 
     private static final float[] b = new float[] { 0.0F, 0.0F, 0.1F, 0.2F};
     private static final float[] c = new float[] { 0.0F, 0.0F, 0.25F, 0.5F};
-    private static final float[] d = new float[] { 0.0F, 0.0F, 0.05F, 0.02F};
+    private static final float[] d = new float[] { 0.0F, 0.0F, 0.05F, 0.07F};
     public static final float[] au = new float[] { 0.0F, 0.1F, 0.15F, 0.45F};
     public int maxNoDamageTicks = 20;
     public float aw;
@@ -146,7 +146,7 @@ public abstract class EntityLiving extends Entity {
         return this.navigation;
     }
 
-    public EntitySenses aD() {
+    public EntitySenses getEntitySenses() {
         return this.bP;
     }
 
@@ -172,7 +172,7 @@ public abstract class EntityLiving extends Entity {
         return this.bC;
     }
 
-    public float ao() {
+    public float getHeadRotation() {
         return this.aA;
     }
 
@@ -1034,7 +1034,7 @@ public abstract class EntityLiving extends Entity {
         nbttagcompound.setShort("HurtTime", (short) this.hurtTicks);
         nbttagcompound.setShort("DeathTime", (short) this.deathTicks);
         nbttagcompound.setShort("AttackTime", (short) this.attackTicks);
-        nbttagcompound.setBoolean("CanPickUpLoot", this.bS());
+        nbttagcompound.setBoolean("CanPickUpLoot", this.bT());
         nbttagcompound.setBoolean("PersistenceRequired", this.persistent);
         NBTTagList nbttaglist = new NBTTagList();
 
@@ -1224,7 +1224,7 @@ public abstract class EntityLiving extends Entity {
 
         this.world.methodProfiler.b();
         this.world.methodProfiler.a("looting");
-        if (!this.world.isStatic && this.bS() && !this.bd && this.world.getGameRules().getBoolean("mobGriefing")) {
+        if (!this.world.isStatic && this.bT() && !this.bd && this.world.getGameRules().getBoolean("mobGriefing")) {
             List list = this.world.a(EntityItem.class, this.boundingBox.grow(1.0D, 0.0D, 1.0D));
             Iterator iterator = list.iterator();
 
@@ -1320,7 +1320,7 @@ public abstract class EntityLiving extends Entity {
         return this.health <= 0;
     }
 
-    public boolean bk() {
+    public boolean isBlocking() {
         return false;
     }
 
@@ -2010,11 +2010,15 @@ public abstract class EntityLiving extends Entity {
         this.dropChances[i] = f;
     }
 
-    public boolean bS() {
+    public boolean bT() {
         return this.canPickUpLoot;
     }
 
     public void h(boolean flag) {
         this.canPickUpLoot = flag;
     }
+
+    public boolean bU() {
+        return this.persistent;
+    }
 }
