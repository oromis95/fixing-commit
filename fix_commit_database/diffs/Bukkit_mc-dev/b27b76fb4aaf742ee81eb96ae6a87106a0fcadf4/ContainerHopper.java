@@ -2,10 +2,10 @@ package net.minecraft.server;
 
 public class ContainerHopper extends Container {
 
-    private final IInventory a;
+    private final IInventory hopper;
 
     public ContainerHopper(PlayerInventory playerinventory, IInventory iinventory) {
-        this.a = iinventory;
+        this.hopper = iinventory;
         iinventory.startOpen();
         byte b0 = 51;
 
@@ -27,7 +27,7 @@ public class ContainerHopper extends Container {
     }
 
     public boolean a(EntityHuman entityhuman) {
-        return this.a.a(entityhuman);
+        return this.hopper.a(entityhuman);
     }
 
     public ItemStack b(EntityHuman entityhuman, int i) {
@@ -38,11 +38,11 @@ public class ContainerHopper extends Container {
             ItemStack itemstack1 = slot.getItem();
 
             itemstack = itemstack1.cloneItemStack();
-            if (i < this.a.getSize()) {
-                if (!this.a(itemstack1, this.a.getSize(), this.c.size(), true)) {
+            if (i < this.hopper.getSize()) {
+                if (!this.a(itemstack1, this.hopper.getSize(), this.c.size(), true)) {
                     return null;
                 }
-            } else if (!this.a(itemstack1, 0, this.a.getSize(), false)) {
+            } else if (!this.a(itemstack1, 0, this.hopper.getSize(), false)) {
                 return null;
             }
 
@@ -58,6 +58,6 @@ public class ContainerHopper extends Container {
 
     public void b(EntityHuman entityhuman) {
         super.b(entityhuman);
-        this.a.g();
+        this.hopper.g();
     }
 }
