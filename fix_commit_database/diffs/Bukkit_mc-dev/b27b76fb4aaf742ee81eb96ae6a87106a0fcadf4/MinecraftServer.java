@@ -58,16 +58,17 @@ public abstract class MinecraftServer implements ICommandListener, Runnable, IMo
     private long Q;
     private String R;
     private boolean S;
+    private boolean T = false;
 
     public MinecraftServer(File file1) {
         k = this;
         this.universe = file1;
         this.p = new CommandDispatcher();
         this.convertable = new WorldLoaderServer(file1);
-        this.am();
+        this.an();
     }
 
-    private void am() {
+    private void an() {
         DispenserRegistry.a();
     }
 
@@ -519,7 +520,7 @@ public abstract class MinecraftServer implements ICommandListener, Runnable, IMo
             }
 
             if (flag) {
-                dedicatedserver.ao();
+                dedicatedserver.ap();
             }
 
             dedicatedserver.t();
@@ -566,7 +567,7 @@ public abstract class MinecraftServer implements ICommandListener, Runnable, IMo
     }
 
     public String getVersion() {
-        return "1.5.1";
+        return "1.5.2";
     }
 
     public int y() {
@@ -813,7 +814,7 @@ public abstract class MinecraftServer implements ICommandListener, Runnable, IMo
                 mojangstatisticsgenerator.a("world[" + i + "][generator_name]", worlddata.getType().name());
                 mojangstatisticsgenerator.a("world[" + i + "][generator_version]", Integer.valueOf(worlddata.getType().getVersion()));
                 mojangstatisticsgenerator.a("world[" + i + "][height]", Integer.valueOf(this.C));
-                mojangstatisticsgenerator.a("world[" + i + "][chunks_loaded]", Integer.valueOf(worldserver.J().getLoadedChunks()));
+                mojangstatisticsgenerator.a("world[" + i + "][chunks_loaded]", Integer.valueOf(worldserver.K().getLoadedChunks()));
                 ++i;
             }
         }
@@ -944,6 +945,14 @@ public abstract class MinecraftServer implements ICommandListener, Runnable, IMo
 
     public abstract IConsoleLogManager getLogger();
 
+    public void setForceGamemode(boolean flag) {
+        this.T = flag;
+    }
+
+    public boolean getForceGamemode() {
+        return this.T;
+    }
+
     public static PlayerList a(MinecraftServer minecraftserver) {
         return minecraftserver.s;
     }
