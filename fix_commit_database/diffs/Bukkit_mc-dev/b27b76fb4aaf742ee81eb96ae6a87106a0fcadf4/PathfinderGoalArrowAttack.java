@@ -57,7 +57,7 @@ public class PathfinderGoalArrowAttack extends PathfinderGoal {
 
     public void e() {
         double d0 = this.a.e(this.c.locX, this.c.boundingBox.b, this.c.locZ);
-        boolean flag = this.a.aD().canSee(this.c);
+        boolean flag = this.a.getEntitySenses().canSee(this.c);
 
         if (flag) {
             ++this.f;
