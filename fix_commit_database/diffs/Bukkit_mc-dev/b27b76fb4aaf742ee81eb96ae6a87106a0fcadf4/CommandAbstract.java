@@ -77,7 +77,7 @@ public abstract class CommandAbstract implements ICommand {
         if (entityplayer != null) {
             return entityplayer;
         } else {
-            entityplayer = MinecraftServer.getServer().getPlayerList().f(s);
+            entityplayer = MinecraftServer.getServer().getPlayerList().getPlayer(s);
             if (entityplayer == null) {
                 throw new ExceptionPlayerNotFound();
             } else {
