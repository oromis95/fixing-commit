@@ -5,6 +5,7 @@ public class EntityEnderman extends EntityMonster {
     private static boolean[] d = new boolean[256];
     private int e = 0;
     private int f = 0;
+    private boolean g;
 
     public EntityEnderman(World world) {
         super(world);
@@ -42,6 +43,7 @@ public class EntityEnderman extends EntityMonster {
 
         if (entityhuman != null) {
             if (this.e(entityhuman)) {
+                this.g = true;
                 if (this.f == 0) {
                     this.world.makeSound(entityhuman, "mob.endermen.stare", 1.0F, 1.0F);
                 }
@@ -119,12 +121,13 @@ public class EntityEnderman extends EntityMonster {
             this.world.addParticle("portal", this.locX + (this.random.nextDouble() - 0.5D) * (double) this.width, this.locY + this.random.nextDouble() * (double) this.length - 0.25D, this.locZ + (this.random.nextDouble() - 0.5D) * (double) this.width, (this.random.nextDouble() - 0.5D) * 2.0D, -this.random.nextDouble(), (this.random.nextDouble() - 0.5D) * 2.0D);
         }
 
-        if (this.world.u() && !this.world.isStatic) {
+        if (this.world.v() && !this.world.isStatic) {
             float f = this.c(1.0F);
 
             if (f > 0.5F && this.world.l(MathHelper.floor(this.locX), MathHelper.floor(this.locY), MathHelper.floor(this.locZ)) && this.random.nextFloat() * 30.0F < (f - 0.4F) * 2.0F) {
                 this.target = null;
                 this.a(false);
+                this.g = false;
                 this.m();
             }
         }
@@ -132,9 +135,14 @@ public class EntityEnderman extends EntityMonster {
         if (this.F() || this.isBurning()) {
             this.target = null;
             this.a(false);
+            this.g = false;
             this.m();
         }
 
+        if (this.q() && !this.g && this.random.nextInt(100) == 0) {
+            this.a(false);
+        }
+
         this.bG = false;
         if (this.target != null) {
             this.a(this.target, 100.0F, 100.0F);
@@ -290,7 +298,13 @@ public class EntityEnderman extends EntityMonster {
             return false;
         } else {
             this.a(true);
+            if (damagesource instanceof EntityDamageSource && damagesource.getEntity() instanceof EntityHuman) {
+                this.g = true;
+            }
+
             if (damagesource instanceof EntityDamageSourceIndirect) {
+                this.g = false;
+
                 for (int j = 0; j < 64; ++j) {
                     if (this.m()) {
                         return true;
