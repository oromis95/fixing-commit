@@ -6,23 +6,15 @@ import java.io.DataOutputStream;
 public class Packet16BlockItemSwitch extends Packet {
 
     public int a;
-    public int b;
 
     public Packet16BlockItemSwitch() {}
 
-    public Packet16BlockItemSwitch(int i, int j) {
-        this.a = i;
-        this.b = j;
-    }
-
     public void a(DataInputStream datainputstream) {
-        this.a = datainputstream.readInt();
-        this.b = datainputstream.readShort();
+        this.a = datainputstream.readShort();
     }
 
     public void a(DataOutputStream dataoutputstream) {
-        dataoutputstream.writeInt(this.a);
-        dataoutputstream.writeShort(this.b);
+        dataoutputstream.writeShort(this.a);
     }
 
     public void a(NetHandler nethandler) {
@@ -30,6 +22,6 @@ public class Packet16BlockItemSwitch extends Packet {
     }
 
     public int a() {
-        return 6;
+        return 2;
     }
 }
