@@ -205,7 +205,7 @@ public class EntitySnowball extends Entity {
     }
 
     public void b(EntityHuman entityhuman) {
-        if (this.f && this.aj == entityhuman && this.a <= 0 && entityhuman.am.a(new ItemStack(Item.ARROW.aW, 1))) {
+        if (this.f && this.aj == entityhuman && this.a <= 0 && entityhuman.an.a(new ItemStack(Item.ARROW.aW, 1))) {
             this.l.a(this, "random.pop", 0.2F, ((this.W.nextFloat() - this.W.nextFloat()) * 0.7F + 1.0F) * 2.0F);
             entityhuman.c(this, 1);
             this.l();
