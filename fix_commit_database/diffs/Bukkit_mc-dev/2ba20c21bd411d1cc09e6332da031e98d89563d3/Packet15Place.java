@@ -9,24 +9,39 @@ public class Packet15Place extends Packet {
     public int b;
     public int c;
     public int d;
-    public int e;
+    public ItemStack e;
 
     public Packet15Place() {}
 
     public void a(DataInputStream datainputstream) {
-        this.a = datainputstream.readShort();
-        this.b = datainputstream.readInt();
-        this.c = datainputstream.read();
-        this.d = datainputstream.readInt();
-        this.e = datainputstream.read();
+        this.a = datainputstream.readInt();
+        this.b = datainputstream.read();
+        this.c = datainputstream.readInt();
+        this.d = datainputstream.read();
+        short short1 = datainputstream.readShort();
+
+        if (short1 >= 0) {
+            byte b0 = datainputstream.readByte();
+            byte b1 = datainputstream.readByte();
+
+            this.e = new ItemStack(short1, b0, b1);
+        } else {
+            this.e = null;
+        }
     }
 
     public void a(DataOutputStream dataoutputstream) {
-        dataoutputstream.writeShort(this.a);
-        dataoutputstream.writeInt(this.b);
-        dataoutputstream.write(this.c);
-        dataoutputstream.writeInt(this.d);
-        dataoutputstream.write(this.e);
+        dataoutputstream.writeInt(this.a);
+        dataoutputstream.write(this.b);
+        dataoutputstream.writeInt(this.c);
+        dataoutputstream.write(this.d);
+        if (this.e == null) {
+            dataoutputstream.writeShort(-1);
+        } else {
+            dataoutputstream.writeShort(this.e.c);
+            dataoutputstream.writeByte(this.e.a);
+            dataoutputstream.writeByte(this.e.d);
+        }
     }
 
     public void a(NetHandler nethandler) {
@@ -34,6 +49,6 @@ public class Packet15Place extends Packet {
     }
 
     public int a() {
-        return 12;
+        return 14;
     }
 }
