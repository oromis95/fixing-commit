@@ -1,7 +1,5 @@
 package net.minecraft.server;
 
-import java.io.ByteArrayInputStream;
-import java.io.ByteArrayOutputStream;
 import java.io.DataInput;
 import java.io.DataInputStream;
 import java.io.DataOutput;
@@ -40,33 +38,6 @@ public class CompressedStreamTools {
         }
     }
 
-    public static NBTTagCompound a(byte[] abyte) {
-        DataInputStream datainputstream = new DataInputStream(new GZIPInputStream(new ByteArrayInputStream(abyte)));
-
-        NBTTagCompound nbttagcompound;
-
-        try {
-            nbttagcompound = a((DataInput) datainputstream);
-        } finally {
-            datainputstream.close();
-        }
-
-        return nbttagcompound;
-    }
-
-    public static byte[] a(NBTTagCompound nbttagcompound) {
-        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
-        DataOutputStream dataoutputstream = new DataOutputStream(new GZIPOutputStream(bytearrayoutputstream));
-
-        try {
-            a(nbttagcompound, (DataOutput) dataoutputstream);
-        } finally {
-            dataoutputstream.close();
-        }
-
-        return bytearrayoutputstream.toByteArray();
-    }
-
     public static NBTTagCompound a(DataInput datainput) {
         NBTBase nbtbase = NBTBase.b(datainput);
 
