@@ -25,4 +25,14 @@ public class TileEntitySign extends TileEntity {
             }
         }
     }
+
+    public Packet f() {
+        String[] astring = new String[4];
+
+        for (int i = 0; i < 4; ++i) {
+            astring[i] = this.e[i];
+        }
+
+        return new Packet130(this.b, this.c, this.d, astring);
+    }
 }
