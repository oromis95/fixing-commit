@@ -13,7 +13,7 @@ public class WorldGenLakes extends WorldGenerator {
     public boolean a(World world, Random random, int i, int j, int k) {
         i -= 8;
 
-        for (k -= 8; j > 0 && world.a(i, j, k) == 0; --j) {
+        for (k -= 8; j > 0 && world.e(i, j, k); --j) {
             ;
         }
 
@@ -47,14 +47,14 @@ public class WorldGenLakes extends WorldGenerator {
             }
         }
 
+        boolean flag;
         int i2;
         int j2;
 
         for (i1 = 0; i1 < 16; ++i1) {
             for (i2 = 0; i2 < 16; ++i2) {
                 for (j2 = 0; j2 < 8; ++j2) {
-                    boolean flag = !aboolean[(i1 * 16 + i2) * 8 + j2] && (i1 < 15 && aboolean[((i1 + 1) * 16 + i2) * 8 + j2] || i1 > 0 && aboolean[((i1 - 1) * 16 + i2) * 8 + j2] || i2 < 15 && aboolean[(i1 * 16 + i2 + 1) * 8 + j2] || i2 > 0 && aboolean[(i1 * 16 + (i2 - 1)) * 8 + j2] || j2 < 7 && aboolean[(i1 * 16 + i2) * 8 + j2 + 1] || j2 > 0 && aboolean[(i1 * 16 + i2) * 8 + (j2 - 1)]);
-
+                    flag = !aboolean[(i1 * 16 + i2) * 8 + j2] && (i1 < 15 && aboolean[((i1 + 1) * 16 + i2) * 8 + j2] || i1 > 0 && aboolean[((i1 - 1) * 16 + i2) * 8 + j2] || i2 < 15 && aboolean[(i1 * 16 + i2 + 1) * 8 + j2] || i2 > 0 && aboolean[(i1 * 16 + (i2 - 1)) * 8 + j2] || j2 < 7 && aboolean[(i1 * 16 + i2) * 8 + j2 + 1] || j2 > 0 && aboolean[(i1 * 16 + i2) * 8 + (j2 - 1)]);
                     if (flag) {
                         Material material = world.c(i + i1, j + j2, k + i2);
 
@@ -90,6 +90,19 @@ public class WorldGenLakes extends WorldGenerator {
             }
         }
 
+        if (Block.m[this.a].bs == Material.g) {
+            for (i1 = 0; i1 < 16; ++i1) {
+                for (i2 = 0; i2 < 16; ++i2) {
+                    for (j2 = 0; j2 < 8; ++j2) {
+                        flag = !aboolean[(i1 * 16 + i2) * 8 + j2] && (i1 < 15 && aboolean[((i1 + 1) * 16 + i2) * 8 + j2] || i1 > 0 && aboolean[((i1 - 1) * 16 + i2) * 8 + j2] || i2 < 15 && aboolean[(i1 * 16 + i2 + 1) * 8 + j2] || i2 > 0 && aboolean[(i1 * 16 + (i2 - 1)) * 8 + j2] || j2 < 7 && aboolean[(i1 * 16 + i2) * 8 + j2 + 1] || j2 > 0 && aboolean[(i1 * 16 + i2) * 8 + (j2 - 1)]);
+                        if (flag && (j2 < 4 || random.nextInt(2) != 0) && world.c(i + i1, j + j2, k + i2).a()) {
+                            world.d(i + i1, j + j2, k + i2, Block.STONE.bh);
+                        }
+                    }
+                }
+            }
+        }
+
         return true;
     }
 }
