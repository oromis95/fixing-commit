@@ -7,7 +7,7 @@ public class WorldGenLightStone2 extends WorldGenerator {
     public WorldGenLightStone2() {}
 
     public boolean a(World world, Random random, int i, int j, int k) {
-        if (world.a(i, j, k) != 0) {
+        if (!world.e(i, j, k)) {
             return false;
         } else if (world.a(i, j + 1, k) != Block.NETHERRACK.bh) {
             return false;
