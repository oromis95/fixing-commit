@@ -11,19 +11,21 @@ public class BlockGrass extends Block {
     }
 
     public void a(World world, int i, int j, int k, Random random) {
-        if (world.h(i, j + 1, k) < 4 && world.c(i, j + 1, k).b()) {
-            if (random.nextInt(4) != 0) {
-                return;
-            }
+        if (!world.z) {
+            if (world.i(i, j + 1, k) < 4 && world.c(i, j + 1, k).b()) {
+                if (random.nextInt(4) != 0) {
+                    return;
+                }
 
-            world.d(i, j, k, Block.DIRT.bh);
-        } else if (world.h(i, j + 1, k) >= 9) {
-            int l = i + random.nextInt(3) - 1;
-            int i1 = j + random.nextInt(5) - 3;
-            int j1 = k + random.nextInt(3) - 1;
+                world.d(i, j, k, Block.DIRT.bh);
+            } else if (world.i(i, j + 1, k) >= 9) {
+                int l = i + random.nextInt(3) - 1;
+                int i1 = j + random.nextInt(5) - 3;
+                int j1 = k + random.nextInt(3) - 1;
 
-            if (world.a(l, i1, j1) == Block.DIRT.bh && world.h(l, i1 + 1, j1) >= 4 && !world.c(l, i1 + 1, j1).b()) {
-                world.d(l, i1, j1, Block.GRASS.bh);
+                if (world.a(l, i1, j1) == Block.DIRT.bh && world.i(l, i1 + 1, j1) >= 4 && !world.c(l, i1 + 1, j1).b()) {
+                    world.d(l, i1, j1, Block.GRASS.bh);
+                }
             }
         }
     }
