@@ -395,7 +395,7 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
                     int j2 = this.world.getTypeId(k1, l1, i2);
 
                     if (j2 != 0) {
-                        if (j2 != Block.OBSIDIAN.id && j2 != Block.WHITESTONE.id && j2 != Block.BEDROCK.id) {
+                        if (j2 != Block.OBSIDIAN.id && j2 != Block.WHITESTONE.id && j2 != Block.BEDROCK.id && this.world.getGameRules().getBoolean("mobGriefing")) {
                             flag1 = this.world.setAir(k1, l1, i2) || flag1;
                         } else {
                             flag = true;
