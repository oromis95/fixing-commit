@@ -7,7 +7,7 @@ public class EntityTNTPrimed extends Entity {
     public EntityTNTPrimed(World world) {
         super(world);
         this.a = 0;
-        this.aD = true;
+        this.aE = true;
         this.b(0.98F, 0.98F);
         this.height = this.width / 2.0F;
     }
@@ -26,17 +26,17 @@ public class EntityTNTPrimed extends Entity {
         this.lastZ = d2;
     }
 
-    protected void a() {}
+    protected void b() {}
 
-    protected boolean l() {
+    protected boolean n() {
         return false;
     }
 
-    public boolean d_() {
+    public boolean o_() {
         return !this.dead;
     }
 
-    public void f_() {
+    public void p_() {
         this.lastX = this.locX;
         this.lastY = this.locY;
         this.lastZ = this.locZ;
@@ -65,11 +65,11 @@ public class EntityTNTPrimed extends Entity {
         this.world.a((Entity) null, this.locX, this.locY, this.locZ, f);
     }
 
-    protected void a(NBTTagCompound nbttagcompound) {
+    protected void b(NBTTagCompound nbttagcompound) {
         nbttagcompound.a("Fuse", (byte) this.a);
     }
 
-    protected void b(NBTTagCompound nbttagcompound) {
+    protected void a(NBTTagCompound nbttagcompound) {
         this.a = nbttagcompound.c("Fuse");
     }
 }
