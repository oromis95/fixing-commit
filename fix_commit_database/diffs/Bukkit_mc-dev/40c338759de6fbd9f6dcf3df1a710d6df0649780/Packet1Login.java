@@ -7,34 +7,30 @@ public class Packet1Login extends Packet {
 
     public int a;
     public String name;
-    public String c;
-    public long d;
-    public byte e;
+    public long c;
+    public byte d;
 
     public Packet1Login() {}
 
-    public Packet1Login(String s, String s1, int i, long j, byte b0) {
+    public Packet1Login(String s, int i, long j, byte b0) {
         this.name = s;
-        this.c = s1;
         this.a = i;
-        this.d = j;
-        this.e = b0;
+        this.c = j;
+        this.d = b0;
     }
 
     public void a(DataInputStream datainputstream) {
         this.a = datainputstream.readInt();
-        this.name = datainputstream.readUTF();
-        this.c = datainputstream.readUTF();
-        this.d = datainputstream.readLong();
-        this.e = datainputstream.readByte();
+        this.name = a(datainputstream, 16);
+        this.c = datainputstream.readLong();
+        this.d = datainputstream.readByte();
     }
 
     public void a(DataOutputStream dataoutputstream) {
         dataoutputstream.writeInt(this.a);
-        dataoutputstream.writeUTF(this.name);
-        dataoutputstream.writeUTF(this.c);
-        dataoutputstream.writeLong(this.d);
-        dataoutputstream.writeByte(this.e);
+        a(this.name, dataoutputstream);
+        dataoutputstream.writeLong(this.c);
+        dataoutputstream.writeByte(this.d);
     }
 
     public void a(NetHandler nethandler) {
@@ -42,6 +38,6 @@ public class Packet1Login extends Packet {
     }
 
     public int a() {
-        return 4 + this.name.length() + this.c.length() + 4 + 5;
+        return 4 + this.name.length() + 4 + 5;
     }
 }
