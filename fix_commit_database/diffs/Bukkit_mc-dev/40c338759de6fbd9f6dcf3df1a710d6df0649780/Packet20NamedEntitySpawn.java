@@ -31,7 +31,7 @@ public class Packet20NamedEntitySpawn extends Packet {
 
     public void a(DataInputStream datainputstream) {
         this.a = datainputstream.readInt();
-        this.b = datainputstream.readUTF();
+        this.b = a(datainputstream, 16);
         this.c = datainputstream.readInt();
         this.d = datainputstream.readInt();
         this.e = datainputstream.readInt();
@@ -42,7 +42,7 @@ public class Packet20NamedEntitySpawn extends Packet {
 
     public void a(DataOutputStream dataoutputstream) {
         dataoutputstream.writeInt(this.a);
-        dataoutputstream.writeUTF(this.b);
+        a(this.b, dataoutputstream);
         dataoutputstream.writeInt(this.c);
         dataoutputstream.writeInt(this.d);
         dataoutputstream.writeInt(this.e);
