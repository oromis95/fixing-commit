@@ -189,4 +189,8 @@ public class BlockDoor extends Block {
     public boolean canPlace(World world, int i, int j, int k) {
         return j >= 127 ? false : world.d(i, j - 1, k) && super.canPlace(world, i, j, k) && super.canPlace(world, i, j + 1, k);
     }
+
+    public static boolean e(int i) {
+        return (i & 4) != 0;
+    }
 }
