@@ -5,11 +5,11 @@ public class EntityZombie extends EntityMonster {
     public EntityZombie(World world) {
         super(world);
         this.texture = "/mob/zombie.png";
-        this.az = 0.5F;
+        this.aA = 0.5F;
         this.damage = 5;
     }
 
-    public void r() {
+    public void u() {
         if (this.world.d()) {
             float f = this.c(1.0F);
 
@@ -18,22 +18,22 @@ public class EntityZombie extends EntityMonster {
             }
         }
 
-        super.r();
+        super.u();
     }
 
-    protected String e() {
+    protected String g() {
         return "mob.zombie";
     }
 
-    protected String f() {
+    protected String h() {
         return "mob.zombiehurt";
     }
 
-    protected String g() {
+    protected String i() {
         return "mob.zombiedeath";
     }
 
-    protected int h() {
+    protected int j() {
         return Item.FEATHER.id;
     }
 }
