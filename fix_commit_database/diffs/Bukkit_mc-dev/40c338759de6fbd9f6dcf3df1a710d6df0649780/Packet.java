@@ -5,20 +5,24 @@ import java.io.DataOutputStream;
 import java.io.EOFException;
 import java.io.IOException;
 import java.util.HashMap;
+import java.util.HashSet;
 import java.util.Map;
+import java.util.Set;
 
 public abstract class Packet {
 
     private static Map a = new HashMap();
     private static Map b = new HashMap();
+    private static Set c = new HashSet();
+    private static Set d = new HashSet();
     public final long timestamp = System.currentTimeMillis();
     public boolean k = false;
-    private static HashMap c;
-    private static int d;
+    private static HashMap e;
+    private static int f;
 
     public Packet() {}
 
-    static void a(int i, Class oclass) {
+    static void a(int i, boolean flag, boolean flag1, Class oclass) {
         if (a.containsKey(Integer.valueOf(i))) {
             throw new IllegalArgumentException("Duplicate packet id:" + i);
         } else if (b.containsKey(oclass)) {
@@ -26,6 +30,13 @@ public abstract class Packet {
         } else {
             a.put(Integer.valueOf(i), oclass);
             b.put(oclass, Integer.valueOf(i));
+            if (flag) {
+                c.add(Integer.valueOf(i));
+            }
+
+            if (flag1) {
+                d.add(Integer.valueOf(i));
+            }
         }
     }
 
@@ -45,8 +56,8 @@ public abstract class Packet {
         return ((Integer) b.get(this.getClass())).intValue();
     }
 
-    public static Packet b(DataInputStream datainputstream) {
-        boolean flag = false;
+    public static Packet a(DataInputStream datainputstream, boolean flag) {
+        boolean flag1 = false;
         Packet packet = null;
 
         int i;
@@ -57,6 +68,10 @@ public abstract class Packet {
                 return null;
             }
 
+            if (flag && !d.contains(Integer.valueOf(i)) || !flag && !c.contains(Integer.valueOf(i))) {
+                throw new IOException("Bad packet id " + i);
+            }
+
             packet = a(i);
             if (packet == null) {
                 throw new IOException("Bad packet id " + i);
@@ -68,16 +83,16 @@ public abstract class Packet {
             return null;
         }
 
-        PacketCounter packetcounter = (PacketCounter) c.get(Integer.valueOf(i));
+        PacketCounter packetcounter = (PacketCounter) e.get(Integer.valueOf(i));
 
         if (packetcounter == null) {
             packetcounter = new PacketCounter((EmptyClass1) null);
-            c.put(Integer.valueOf(i), packetcounter);
+            e.put(Integer.valueOf(i), packetcounter);
         }
 
         packetcounter.a(packet.a());
-        ++d;
-        if (d % 1000 == 0) {
+        ++f;
+        if (f % 1000 == 0) {
             ;
         }
 
@@ -89,6 +104,33 @@ public abstract class Packet {
         packet.a(dataoutputstream);
     }
 
+    public static void a(String s, DataOutputStream dataoutputstream) {
+        if (s.length() > 32767) {
+            throw new IOException("String too big");
+        } else {
+            dataoutputstream.writeShort(s.length());
+            dataoutputstream.writeChars(s);
+        }
+    }
+
+    public static String a(DataInputStream datainputstream, int i) {
+        short short1 = datainputstream.readShort();
+
+        if (short1 > i) {
+            throw new IOException("Received string length longer than maximum allowed (" + short1 + " > " + i + ")");
+        } else if (short1 < 0) {
+            throw new IOException("Received string length is less than zero! Weird string!");
+        } else {
+            StringBuilder stringbuilder = new StringBuilder();
+
+            for (int j = 0; j < short1; ++j) {
+                stringbuilder.append(datainputstream.readChar());
+            }
+
+            return stringbuilder.toString();
+        }
+    }
+
     public abstract void a(DataInputStream datainputstream);
 
     public abstract void a(DataOutputStream dataoutputstream);
@@ -98,60 +140,62 @@ public abstract class Packet {
     public abstract int a();
 
     static {
-        a(0, Packet0KeepAlive.class);
-        a(1, Packet1Login.class);
-        a(2, Packet2Handshake.class);
-        a(3, Packet3Chat.class);
-        a(4, Packet4UpdateTime.class);
-        a(5, Packet5EntityEquipment.class);
-        a(6, Packet6SpawnPosition.class);
-        a(7, Packet7UseEntity.class);
-        a(8, Packet8UpdateHealth.class);
-        a(9, Packet9Respawn.class);
-        a(10, Packet10Flying.class);
-        a(11, Packet11PlayerPosition.class);
-        a(12, Packet12PlayerLook.class);
-        a(13, Packet13PlayerLookMove.class);
-        a(14, Packet14BlockDig.class);
-        a(15, Packet15Place.class);
-        a(16, Packet16BlockItemSwitch.class);
-        a(17, Packet17.class);
-        a(18, Packet18ArmAnimation.class);
-        a(19, Packet19EntityAction.class);
-        a(20, Packet20NamedEntitySpawn.class);
-        a(21, Packet21PickupSpawn.class);
-        a(22, Packet22Collect.class);
-        a(23, Packet23VehicleSpawn.class);
-        a(24, Packet24MobSpawn.class);
-        a(25, Packet25EntityPainting.class);
-        a(27, Packet27.class);
-        a(28, Packet28EntityVelocity.class);
-        a(29, Packet29DestroyEntity.class);
-        a(30, Packet30Entity.class);
-        a(31, Packet31RelEntityMove.class);
-        a(32, Packet32EntityLook.class);
-        a(33, Packet33RelEntityMoveLook.class);
-        a(34, Packet34EntityTeleport.class);
-        a(38, Packet38EntityStatus.class);
-        a(39, Packet39AttachEntity.class);
-        a(40, Packet40EntityMetadata.class);
-        a(50, Packet50PreChunk.class);
-        a(51, Packet51MapChunk.class);
-        a(52, Packet52MultiBlockChange.class);
-        a(53, Packet53BlockChange.class);
-        a(54, Packet54PlayNoteBlock.class);
-        a(60, Packet60Explosion.class);
-        a(70, Packet70Bed.class);
-        a(100, Packet100OpenWindow.class);
-        a(101, Packet101CloseWindow.class);
-        a(102, Packet102WindowClick.class);
-        a(103, Packet103SetSlot.class);
-        a(104, Packet104WindowItems.class);
-        a(105, Packet105CraftProgressBar.class);
-        a(106, Packet106Transaction.class);
-        a(130, Packet130UpdateSign.class);
-        a(255, Packet255KickDisconnect.class);
-        c = new HashMap();
-        d = 0;
+        a(0, true, true, Packet0KeepAlive.class);
+        a(1, true, true, Packet1Login.class);
+        a(2, true, true, Packet2Handshake.class);
+        a(3, true, true, Packet3Chat.class);
+        a(4, true, false, Packet4UpdateTime.class);
+        a(5, true, false, Packet5EntityEquipment.class);
+        a(6, true, false, Packet6SpawnPosition.class);
+        a(7, false, true, Packet7UseEntity.class);
+        a(8, true, false, Packet8UpdateHealth.class);
+        a(9, true, true, Packet9Respawn.class);
+        a(10, true, true, Packet10Flying.class);
+        a(11, true, true, Packet11PlayerPosition.class);
+        a(12, true, true, Packet12PlayerLook.class);
+        a(13, true, true, Packet13PlayerLookMove.class);
+        a(14, false, true, Packet14BlockDig.class);
+        a(15, false, true, Packet15Place.class);
+        a(16, false, true, Packet16BlockItemSwitch.class);
+        a(17, true, false, Packet17.class);
+        a(18, true, true, Packet18ArmAnimation.class);
+        a(19, false, true, Packet19EntityAction.class);
+        a(20, true, false, Packet20NamedEntitySpawn.class);
+        a(21, true, false, Packet21PickupSpawn.class);
+        a(22, true, false, Packet22Collect.class);
+        a(23, true, false, Packet23VehicleSpawn.class);
+        a(24, true, false, Packet24MobSpawn.class);
+        a(25, true, false, Packet25EntityPainting.class);
+        a(27, false, true, Packet27.class);
+        a(28, true, false, Packet28EntityVelocity.class);
+        a(29, true, false, Packet29DestroyEntity.class);
+        a(30, true, false, Packet30Entity.class);
+        a(31, true, false, Packet31RelEntityMove.class);
+        a(32, true, false, Packet32EntityLook.class);
+        a(33, true, false, Packet33RelEntityMoveLook.class);
+        a(34, true, false, Packet34EntityTeleport.class);
+        a(38, true, false, Packet38EntityStatus.class);
+        a(39, true, false, Packet39AttachEntity.class);
+        a(40, true, false, Packet40EntityMetadata.class);
+        a(50, true, false, Packet50PreChunk.class);
+        a(51, true, false, Packet51MapChunk.class);
+        a(52, true, false, Packet52MultiBlockChange.class);
+        a(53, true, false, Packet53BlockChange.class);
+        a(54, true, false, Packet54PlayNoteBlock.class);
+        a(60, true, false, Packet60Explosion.class);
+        a(70, true, false, Packet70Bed.class);
+        a(71, true, false, Packet71Weather.class);
+        a(100, true, false, Packet100OpenWindow.class);
+        a(101, true, true, Packet101CloseWindow.class);
+        a(102, false, true, Packet102WindowClick.class);
+        a(103, true, false, Packet103SetSlot.class);
+        a(104, true, false, Packet104WindowItems.class);
+        a(105, true, false, Packet105CraftProgressBar.class);
+        a(106, true, true, Packet106Transaction.class);
+        a(130, true, true, Packet130UpdateSign.class);
+        a(200, true, false, Packet200Statistic.class);
+        a(255, true, true, Packet255KickDisconnect.class);
+        e = new HashMap();
+        f = 0;
     }
 }
