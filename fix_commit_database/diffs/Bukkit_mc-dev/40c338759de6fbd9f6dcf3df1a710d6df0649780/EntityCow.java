@@ -8,31 +8,31 @@ public class EntityCow extends EntityAnimal {
         this.b(0.9F, 1.3F);
     }
 
-    public void a(NBTTagCompound nbttagcompound) {
-        super.a(nbttagcompound);
-    }
-
     public void b(NBTTagCompound nbttagcompound) {
         super.b(nbttagcompound);
     }
 
-    protected String e() {
+    public void a(NBTTagCompound nbttagcompound) {
+        super.a(nbttagcompound);
+    }
+
+    protected String g() {
         return "mob.cow";
     }
 
-    protected String f() {
+    protected String h() {
         return "mob.cowhurt";
     }
 
-    protected String g() {
+    protected String i() {
         return "mob.cowhurt";
     }
 
-    protected float i() {
+    protected float k() {
         return 0.4F;
     }
 
-    protected int h() {
+    protected int j() {
         return Item.LEATHER.id;
     }
 
