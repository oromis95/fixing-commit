@@ -41,56 +41,61 @@ public class BlockFire extends Block {
 
     public void a(World world, int i, int j, int k, Random random) {
         boolean flag = world.getTypeId(i, j - 1, k) == Block.NETHERRACK.id;
-        int l = world.getData(i, j, k);
 
-        if (l < 15) {
-            world.setData(i, j, k, l + 1);
-            world.c(i, j, k, this.id, this.b());
-        }
-
-        if (!flag && !this.g(world, i, j, k)) {
-            if (!world.d(i, j - 1, k) || l > 3) {
-                world.setTypeId(i, j, k, 0);
-            }
-        } else if (!flag && !this.b(world, i, j - 1, k) && l == 15 && random.nextInt(4) == 0) {
+        if (!flag && world.v() && (world.q(i, j, k) || world.q(i - 1, j, k) || world.q(i + 1, j, k) || world.q(i, j, k - 1) || world.q(i, j, k + 1))) {
             world.setTypeId(i, j, k, 0);
         } else {
-            if (l % 2 == 0 && l > 2) {
-                this.a(world, i + 1, j, k, 300, random);
-                this.a(world, i - 1, j, k, 300, random);
-                this.a(world, i, j - 1, k, 250, random);
-                this.a(world, i, j + 1, k, 250, random);
-                this.a(world, i, j, k - 1, 300, random);
-                this.a(world, i, j, k + 1, 300, random);
-
-                for (int i1 = i - 1; i1 <= i + 1; ++i1) {
-                    for (int j1 = k - 1; j1 <= k + 1; ++j1) {
-                        for (int k1 = j - 1; k1 <= j + 4; ++k1) {
-                            if (i1 != i || k1 != j || j1 != k) {
-                                int l1 = 100;
-
-                                if (k1 > j + 1) {
-                                    l1 += (k1 - (j + 1)) * 100;
-                                }
+            int l = world.getData(i, j, k);
 
-                                int i2 = this.h(world, i1, k1, j1);
+            if (l < 15) {
+                world.setData(i, j, k, l + 1);
+                world.c(i, j, k, this.id, this.b());
+            }
 
-                                if (i2 > 0 && random.nextInt(l1) <= i2) {
-                                    world.setTypeId(i1, k1, j1, this.id);
+            if (!flag && !this.g(world, i, j, k)) {
+                if (!world.d(i, j - 1, k) || l > 3) {
+                    world.setTypeId(i, j, k, 0);
+                }
+            } else if (!flag && !this.b(world, i, j - 1, k) && l == 15 && random.nextInt(4) == 0) {
+                world.setTypeId(i, j, k, 0);
+            } else {
+                if (l % 2 == 0 && l > 2) {
+                    this.a(world, i + 1, j, k, 300, random);
+                    this.a(world, i - 1, j, k, 300, random);
+                    this.a(world, i, j - 1, k, 250, random);
+                    this.a(world, i, j + 1, k, 250, random);
+                    this.a(world, i, j, k - 1, 300, random);
+                    this.a(world, i, j, k + 1, 300, random);
+
+                    for (int i1 = i - 1; i1 <= i + 1; ++i1) {
+                        for (int j1 = k - 1; j1 <= k + 1; ++j1) {
+                            for (int k1 = j - 1; k1 <= j + 4; ++k1) {
+                                if (i1 != i || k1 != j || j1 != k) {
+                                    int l1 = 100;
+
+                                    if (k1 > j + 1) {
+                                        l1 += (k1 - (j + 1)) * 100;
+                                    }
+
+                                    int i2 = this.h(world, i1, k1, j1);
+
+                                    if (i2 > 0 && random.nextInt(l1) <= i2 && (!world.v() || !world.q(i1, k1, j1)) && !world.q(i1 - 1, k1, k) && !world.q(i1 + 1, k1, j1) && !world.q(i1, k1, j1 - 1) && !world.q(i1, k1, j1 + 1)) {
+                                        world.setTypeId(i1, k1, j1, this.id);
+                                    }
                                 }
                             }
                         }
                     }
                 }
-            }
 
-            if (l == 15) {
-                this.a(world, i + 1, j, k, 1, random);
-                this.a(world, i - 1, j, k, 1, random);
-                this.a(world, i, j - 1, k, 1, random);
-                this.a(world, i, j + 1, k, 1, random);
-                this.a(world, i, j, k - 1, 1, random);
-                this.a(world, i, j, k + 1, 1, random);
+                if (l == 15) {
+                    this.a(world, i + 1, j, k, 1, random);
+                    this.a(world, i - 1, j, k, 1, random);
+                    this.a(world, i, j - 1, k, 1, random);
+                    this.a(world, i, j + 1, k, 1, random);
+                    this.a(world, i, j, k - 1, 1, random);
+                    this.a(world, i, j, k + 1, 1, random);
+                }
             }
         }
     }
@@ -101,7 +106,7 @@ public class BlockFire extends Block {
         if (random.nextInt(l) < i1) {
             boolean flag = world.getTypeId(i, j, k) == Block.TNT.id;
 
-            if (random.nextInt(2) == 0) {
+            if (random.nextInt(2) == 0 && !world.q(i, j, k)) {
                 world.setTypeId(i, j, k, this.id);
             } else {
                 world.setTypeId(i, j, k, 0);
@@ -134,7 +139,7 @@ public class BlockFire extends Block {
         }
     }
 
-    public boolean d() {
+    public boolean n_() {
         return false;
     }
 
