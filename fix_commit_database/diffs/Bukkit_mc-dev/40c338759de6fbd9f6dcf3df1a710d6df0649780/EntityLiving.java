@@ -5,118 +5,118 @@ import java.util.List;
 public abstract class EntityLiving extends Entity {
 
     public int maxNoDamageTicks = 20;
-    public float D;
     public float E;
-    public float F = 0.0F;
+    public float F;
     public float G = 0.0F;
-    protected float H;
+    public float H = 0.0F;
     protected float I;
     protected float J;
     protected float K;
-    protected boolean L = true;
+    protected float L;
+    protected boolean M = true;
     protected String texture = "/mob/char.png";
-    protected boolean N = true;
-    protected float O = 0.0F;
-    protected String P = null;
-    protected float Q = 1.0F;
-    protected int R = 0;
-    protected float S = 0.0F;
-    public boolean T = false;
-    public float U;
+    protected boolean O = true;
+    protected float P = 0.0F;
+    protected String Q = null;
+    protected float R = 1.0F;
+    protected int S = 0;
+    protected float T = 0.0F;
+    public boolean U = false;
     public float V;
+    public float W;
     public int health = 10;
-    public int X;
+    public int Y;
     private int a;
     public int hurtTicks;
-    public int Z;
-    public float aa = 0.0F;
+    public int aa;
+    public float ab = 0.0F;
     public int deathTicks = 0;
     public int attackTicks = 0;
-    public float ad;
     public float ae;
-    protected boolean af = false;
-    public int ag = -1;
-    public float ah = (float) (Math.random() * 0.8999999761581421D + 0.10000000149011612D);
-    public float ai;
+    public float af;
+    protected boolean ag = false;
+    public int ah = -1;
+    public float ai = (float) (Math.random() * 0.8999999761581421D + 0.10000000149011612D);
     public float aj;
     public float ak;
-    protected int al;
-    protected double am;
+    public float al;
+    protected int am;
     protected double an;
     protected double ao;
     protected double ap;
     protected double aq;
-    float ar = 0.0F;
+    protected double ar;
+    float as = 0.0F;
     protected int lastDamage = 0;
-    protected int at = 0;
-    protected float au;
+    protected int au = 0;
     protected float av;
     protected float aw;
-    protected boolean ax = false;
-    protected float ay = 0.0F;
-    protected float az = 0.7F;
+    protected float ax;
+    protected boolean ay = false;
+    protected float az = 0.0F;
+    protected float aA = 0.7F;
     private Entity b;
-    protected int aA = 0;
+    protected int aB = 0;
 
     public EntityLiving(World world) {
         super(world);
-        this.aD = true;
-        this.E = (float) (Math.random() + 1.0D) * 0.01F;
+        this.aE = true;
+        this.F = (float) (Math.random() + 1.0D) * 0.01F;
         this.setPosition(this.locX, this.locY, this.locZ);
-        this.D = (float) Math.random() * 12398.0F;
+        this.E = (float) Math.random() * 12398.0F;
         this.yaw = (float) (Math.random() * 3.1415927410125732D * 2.0D);
-        this.bm = 0.5F;
+        this.bo = 0.5F;
     }
 
-    protected void a() {}
+    protected void b() {}
 
     public boolean e(Entity entity) {
-        return this.world.a(Vec3D.create(this.locX, this.locY + (double) this.q(), this.locZ), Vec3D.create(entity.locX, entity.locY + (double) entity.q(), entity.locZ)) == null;
+        return this.world.a(Vec3D.create(this.locX, this.locY + (double) this.s(), this.locZ), Vec3D.create(entity.locX, entity.locY + (double) entity.s(), entity.locZ)) == null;
     }
 
-    public boolean d_() {
+    public boolean o_() {
         return !this.dead;
     }
 
-    public boolean e_() {
+    public boolean d_() {
         return !this.dead;
     }
 
-    public float q() {
+    public float s() {
         return this.width * 0.85F;
     }
 
-    public int c() {
+    public int e() {
         return 80;
     }
 
-    public void J() {
-        String s = this.e();
+    public void M() {
+        String s = this.g();
 
         if (s != null) {
-            this.world.makeSound(this, s, this.i(), (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
+            this.world.makeSound(this, s, this.k(), (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
         }
     }
 
-    public void K() {
-        this.U = this.V;
-        super.K();
+    public void N() {
+        this.V = this.W;
+        super.N();
         if (this.random.nextInt(1000) < this.a++) {
-            this.a = -this.c();
-            this.J();
+            this.a = -this.e();
+            this.M();
         }
 
-        if (this.M() && this.E()) {
+        if (this.P() && this.H()) {
             this.damageEntity((Entity) null, 1);
         }
 
-        if (this.by || this.world.isStatic) {
+        if (this.bz || this.world.isStatic) {
             this.fireTicks = 0;
         }
 
         int i;
 
-        if (this.M() && this.a(Material.WATER) && !this.b_()) {
+        if (this.P() && this.a(Material.WATER) && !this.b_()) {
             --this.airTicks;
             if (this.airTicks == -20) {
                 this.airTicks = 0;
@@ -137,7 +137,7 @@ public abstract class EntityLiving extends Entity {
             this.airTicks = this.maxAirTicks;
         }
 
-        this.ad = this.ae;
+        this.ae = this.af;
         if (this.attackTicks > 0) {
             --this.attackTicks;
         }
@@ -153,7 +153,7 @@ public abstract class EntityLiving extends Entity {
         if (this.health <= 0) {
             ++this.deathTicks;
             if (this.deathTicks > 20) {
-                this.P();
+                this.T();
                 this.die();
 
                 for (i = 0; i < 20; ++i) {
@@ -166,13 +166,13 @@ public abstract class EntityLiving extends Entity {
             }
         }
 
-        this.K = this.J;
-        this.G = this.F;
+        this.L = this.K;
+        this.H = this.G;
         this.lastYaw = this.yaw;
         this.lastPitch = this.pitch;
     }
 
-    public void L() {
+    public void O() {
         for (int i = 0; i < 20; ++i) {
             double d0 = this.random.nextGaussian() * 0.02D;
             double d1 = this.random.nextGaussian() * 0.02D;
@@ -183,22 +183,22 @@ public abstract class EntityLiving extends Entity {
         }
     }
 
-    public void o_() {
-        super.o_();
-        this.H = this.I;
-        this.I = 0.0F;
+    public void B() {
+        super.B();
+        this.I = this.J;
+        this.J = 0.0F;
     }
 
-    public void f_() {
-        super.f_();
-        this.r();
+    public void p_() {
+        super.p_();
+        this.u();
         double d0 = this.locX - this.lastX;
         double d1 = this.locZ - this.lastZ;
         float f = MathHelper.a(d0 * d0 + d1 * d1);
-        float f1 = this.F;
+        float f1 = this.G;
         float f2 = 0.0F;
 
-        this.H = this.I;
+        this.I = this.J;
         float f3 = 0.0F;
 
         if (f > 0.05F) {
@@ -207,7 +207,7 @@ public abstract class EntityLiving extends Entity {
             f1 = (float) Math.atan2(d1, d0) * 180.0F / 3.1415927F - 90.0F;
         }
 
-        if (this.V > 0.0F) {
+        if (this.W > 0.0F) {
             f1 = this.yaw;
         }
 
@@ -215,11 +215,11 @@ public abstract class EntityLiving extends Entity {
             f3 = 0.0F;
         }
 
-        this.I += (f3 - this.I) * 0.3F;
+        this.J += (f3 - this.J) * 0.3F;
 
         float f4;
 
-        for (f4 = f1 - this.F; f4 < -180.0F; f4 += 360.0F) {
+        for (f4 = f1 - this.G; f4 < -180.0F; f4 += 360.0F) {
             ;
         }
 
@@ -227,11 +227,11 @@ public abstract class EntityLiving extends Entity {
             f4 -= 360.0F;
         }
 
-        this.F += f4 * 0.3F;
+        this.G += f4 * 0.3F;
 
         float f5;
 
-        for (f5 = this.yaw - this.F; f5 < -180.0F; f5 += 360.0F) {
+        for (f5 = this.yaw - this.G; f5 < -180.0F; f5 += 360.0F) {
             ;
         }
 
@@ -249,9 +249,9 @@ public abstract class EntityLiving extends Entity {
             f5 = 75.0F;
         }
 
-        this.F = this.yaw - f5;
+        this.G = this.yaw - f5;
         if (f5 * f5 > 2500.0F) {
-            this.F += f5 * 0.2F;
+            this.G += f5 * 0.2F;
         }
 
         if (flag) {
@@ -266,12 +266,12 @@ public abstract class EntityLiving extends Entity {
             this.lastYaw += 360.0F;
         }
 
-        while (this.F - this.G < -180.0F) {
-            this.G -= 360.0F;
+        while (this.G - this.H < -180.0F) {
+            this.H -= 360.0F;
         }
 
-        while (this.F - this.G >= 180.0F) {
-            this.G += 360.0F;
+        while (this.G - this.H >= 180.0F) {
+            this.H += 360.0F;
         }
 
         while (this.pitch - this.lastPitch < -180.0F) {
@@ -282,7 +282,7 @@ public abstract class EntityLiving extends Entity {
             this.lastPitch += 360.0F;
         }
 
-        this.J += f2;
+        this.K += f2;
     }
 
     protected void b(float f, float f1) {
@@ -304,11 +304,11 @@ public abstract class EntityLiving extends Entity {
         if (this.world.isStatic) {
             return false;
         } else {
-            this.at = 0;
+            this.au = 0;
             if (this.health <= 0) {
                 return false;
             } else {
-                this.aj = 1.5F;
+                this.ak = 1.5F;
                 boolean flag = true;
 
                 if ((float) this.noDamageTicks > (float) this.maxNoDamageTicks / 2.0F) {
@@ -321,16 +321,16 @@ public abstract class EntityLiving extends Entity {
                     flag = false;
                 } else {
                     this.lastDamage = i;
-                    this.X = this.health;
+                    this.Y = this.health;
                     this.noDamageTicks = this.maxNoDamageTicks;
                     this.c(i);
-                    this.hurtTicks = this.Z = 10;
+                    this.hurtTicks = this.aa = 10;
                 }
 
-                this.aa = 0.0F;
+                this.ab = 0.0F;
                 if (flag) {
                     this.world.a(this, (byte) 2);
-                    this.V();
+                    this.ab();
                     if (entity != null) {
                         double d0 = entity.locX - this.locX;
 
@@ -340,21 +340,21 @@ public abstract class EntityLiving extends Entity {
                             d0 = (Math.random() - Math.random()) * 0.01D;
                         }
 
-                        this.aa = (float) (Math.atan2(d1, d0) * 180.0D / 3.1415927410125732D) - this.yaw;
+                        this.ab = (float) (Math.atan2(d1, d0) * 180.0D / 3.1415927410125732D) - this.yaw;
                         this.a(entity, i, d0, d1);
                     } else {
-                        this.aa = (float) ((int) (Math.random() * 2.0D) * 180);
+                        this.ab = (float) ((int) (Math.random() * 2.0D) * 180);
                     }
                 }
 
                 if (this.health <= 0) {
                     if (flag) {
-                        this.world.makeSound(this, this.g(), this.i(), (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
+                        this.world.makeSound(this, this.i(), this.k(), (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
                     }
 
                     this.a(entity);
                 } else if (flag) {
-                    this.world.makeSound(this, this.f(), this.i(), (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
+                    this.world.makeSound(this, this.h(), this.k(), (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
                 }
 
                 return true;
@@ -366,19 +366,19 @@ public abstract class EntityLiving extends Entity {
         this.health -= i;
     }
 
-    protected float i() {
+    protected float k() {
         return 1.0F;
     }
 
-    protected String e() {
+    protected String g() {
         return null;
     }
 
-    protected String f() {
+    protected String h() {
         return "random.hurt";
     }
 
-    protected String g() {
+    protected String i() {
         return "random.hurt";
     }
 
@@ -398,20 +398,24 @@ public abstract class EntityLiving extends Entity {
     }
 
     public void a(Entity entity) {
-        if (this.R >= 0 && entity != null) {
-            entity.c(this, this.R);
+        if (this.S >= 0 && entity != null) {
+            entity.c(this, this.S);
         }
 
-        this.af = true;
+        if (entity != null) {
+            entity.a(this);
+        }
+
+        this.ag = true;
         if (!this.world.isStatic) {
-            this.p();
+            this.r();
         }
 
         this.world.a(this, (byte) 3);
     }
 
-    protected void p() {
-        int i = this.h();
+    protected void r() {
+        int i = this.j();
 
         if (i > 0) {
             int j = this.random.nextInt(3);
@@ -422,7 +426,7 @@ public abstract class EntityLiving extends Entity {
         }
     }
 
-    protected int h() {
+    protected int j() {
         return 0;
     }
 
@@ -444,7 +448,7 @@ public abstract class EntityLiving extends Entity {
     public void a(float f, float f1) {
         double d0;
 
-        if (this.g_()) {
+        if (this.Z()) {
             d0 = this.locY;
             this.a(f, f1, 0.02F);
             this.move(this.motX, this.motY, this.motZ);
@@ -455,7 +459,7 @@ public abstract class EntityLiving extends Entity {
             if (this.positionChanged && this.b(this.motX, this.motY + 0.6000000238418579D - this.locY + d0, this.motZ)) {
                 this.motY = 0.30000001192092896D;
             }
-        } else if (this.U()) {
+        } else if (this.aa()) {
             d0 = this.locY;
             this.a(f, f1, 0.02F);
             this.move(this.motX, this.motY, this.motZ);
@@ -491,7 +495,7 @@ public abstract class EntityLiving extends Entity {
                 }
             }
 
-            if (this.n()) {
+            if (this.p()) {
                 this.fallDistance = 0.0F;
                 if (this.motY < -0.15D) {
                     this.motY = -0.15D;
@@ -503,7 +507,7 @@ public abstract class EntityLiving extends Entity {
             }
 
             this.move(this.motX, this.motY, this.motZ);
-            if (this.positionChanged && this.n()) {
+            if (this.positionChanged && this.p()) {
                 this.motY = 0.2D;
             }
 
@@ -513,7 +517,7 @@ public abstract class EntityLiving extends Entity {
             this.motZ *= (double) f2;
         }
 
-        this.ai = this.aj;
+        this.aj = this.ak;
         d0 = this.locX - this.lastX;
         double d1 = this.locZ - this.lastZ;
         float f4 = MathHelper.a(d0 * d0 + d1 * d1) * 4.0F;
@@ -522,26 +526,26 @@ public abstract class EntityLiving extends Entity {
             f4 = 1.0F;
         }
 
-        this.aj += (f4 - this.aj) * 0.4F;
-        this.ak += this.aj;
+        this.ak += (f4 - this.ak) * 0.4F;
+        this.al += this.ak;
     }
 
-    public boolean n() {
+    public boolean p() {
         int i = MathHelper.floor(this.locX);
         int j = MathHelper.floor(this.boundingBox.b);
         int k = MathHelper.floor(this.locZ);
 
-        return this.world.getTypeId(i, j, k) == Block.LADDER.id || this.world.getTypeId(i, j + 1, k) == Block.LADDER.id;
+        return this.world.getTypeId(i, j, k) == Block.LADDER.id;
     }
 
-    public void a(NBTTagCompound nbttagcompound) {
+    public void b(NBTTagCompound nbttagcompound) {
         nbttagcompound.a("Health", (short) this.health);
         nbttagcompound.a("HurtTime", (short) this.hurtTicks);
         nbttagcompound.a("DeathTime", (short) this.deathTicks);
         nbttagcompound.a("AttackTime", (short) this.attackTicks);
     }
 
-    public void b(NBTTagCompound nbttagcompound) {
+    public void a(NBTTagCompound nbttagcompound) {
         this.health = nbttagcompound.d("Health");
         if (!nbttagcompound.hasKey("Health")) {
             this.health = 10;
@@ -552,7 +556,7 @@ public abstract class EntityLiving extends Entity {
         this.attackTicks = nbttagcompound.d("AttackTime");
     }
 
-    public boolean M() {
+    public boolean P() {
         return !this.dead && this.health > 0;
     }
 
@@ -560,15 +564,15 @@ public abstract class EntityLiving extends Entity {
         return false;
     }
 
-    public void r() {
-        if (this.al > 0) {
-            double d0 = this.locX + (this.am - this.locX) / (double) this.al;
-            double d1 = this.locY + (this.an - this.locY) / (double) this.al;
-            double d2 = this.locZ + (this.ao - this.locZ) / (double) this.al;
+    public void u() {
+        if (this.am > 0) {
+            double d0 = this.locX + (this.an - this.locX) / (double) this.am;
+            double d1 = this.locY + (this.ao - this.locY) / (double) this.am;
+            double d2 = this.locZ + (this.ap - this.locZ) / (double) this.am;
 
             double d3;
 
-            for (d3 = this.ap - (double) this.yaw; d3 < -180.0D; d3 += 360.0D) {
+            for (d3 = this.aq - (double) this.yaw; d3 < -180.0D; d3 += 360.0D) {
                 ;
             }
 
@@ -576,69 +580,68 @@ public abstract class EntityLiving extends Entity {
                 d3 -= 360.0D;
             }
 
-            this.yaw = (float) ((double) this.yaw + d3 / (double) this.al);
-            this.pitch = (float) ((double) this.pitch + (this.aq - (double) this.pitch) / (double) this.al);
-            --this.al;
+            this.yaw = (float) ((double) this.yaw + d3 / (double) this.am);
+            this.pitch = (float) ((double) this.pitch + (this.ar - (double) this.pitch) / (double) this.am);
+            --this.am;
             this.setPosition(d0, d1, d2);
             this.c(this.yaw, this.pitch);
         }
 
-        if (this.p_()) {
-            this.ax = false;
-            this.au = 0.0F;
+        if (this.A()) {
+            this.ay = false;
             this.av = 0.0F;
             this.aw = 0.0F;
-        } else if (!this.T) {
+            this.ax = 0.0F;
+        } else if (!this.U) {
             this.c_();
         }
 
-        boolean flag = this.g_();
-        boolean flag1 = this.U();
+        boolean flag = this.Z();
+        boolean flag1 = this.aa();
 
-        if (this.ax) {
+        if (this.ay) {
             if (flag) {
                 this.motY += 0.03999999910593033D;
             } else if (flag1) {
                 this.motY += 0.03999999910593033D;
             } else if (this.onGround) {
-                this.I();
+                this.L();
             }
         }
 
-        this.au *= 0.98F;
         this.av *= 0.98F;
-        this.aw *= 0.9F;
-        this.a(this.au, this.av);
+        this.aw *= 0.98F;
+        this.ax *= 0.9F;
+        this.a(this.av, this.aw);
         List list = this.world.b((Entity) this, this.boundingBox.b(0.20000000298023224D, 0.0D, 0.20000000298023224D));
 
         if (list != null && list.size() > 0) {
             for (int i = 0; i < list.size(); ++i) {
                 Entity entity = (Entity) list.get(i);
 
-                if (entity.e_()) {
+                if (entity.d_()) {
                     entity.collide(this);
                 }
             }
         }
     }
 
-    protected boolean p_() {
+    protected boolean A() {
         return this.health <= 0;
     }
 
-    protected void I() {
+    protected void L() {
         this.motY = 0.41999998688697815D;
     }
 
-    protected boolean s() {
+    protected boolean l_() {
         return true;
     }
 
-    protected void c_() {
-        ++this.at;
+    protected void Q() {
         EntityHuman entityhuman = this.world.a(this, -1.0D);
 
-        if (this.s() && entityhuman != null) {
+        if (this.l_() && entityhuman != null) {
             double d0 = entityhuman.locX - this.locX;
             double d1 = entityhuman.locY - this.locY;
             double d2 = entityhuman.locZ - this.locZ;
@@ -648,53 +651,59 @@ public abstract class EntityLiving extends Entity {
                 this.die();
             }
 
-            if (this.at > 600 && this.random.nextInt(800) == 0) {
+            if (this.au > 600 && this.random.nextInt(800) == 0) {
                 if (d3 < 1024.0D) {
-                    this.at = 0;
+                    this.au = 0;
                 } else {
                     this.die();
                 }
             }
         }
+    }
 
-        this.au = 0.0F;
+    protected void c_() {
+        ++this.au;
+        EntityHuman entityhuman = this.world.a(this, -1.0D);
+
+        this.Q();
         this.av = 0.0F;
+        this.aw = 0.0F;
         float f = 8.0F;
 
         if (this.random.nextFloat() < 0.02F) {
             entityhuman = this.world.a(this, (double) f);
             if (entityhuman != null) {
                 this.b = entityhuman;
-                this.aA = 10 + this.random.nextInt(20);
+                this.aB = 10 + this.random.nextInt(20);
             } else {
-                this.aw = (this.random.nextFloat() - 0.5F) * 20.0F;
+                this.ax = (this.random.nextFloat() - 0.5F) * 20.0F;
             }
         }
 
         if (this.b != null) {
-            this.a(this.b, 10.0F, (float) this.n_());
-            if (this.aA-- <= 0 || this.b.dead || this.b.g(this) > (double) (f * f)) {
+            this.a(this.b, 10.0F, (float) this.v());
+            if (this.aB-- <= 0 || this.b.dead || this.b.g(this) > (double) (f * f)) {
                 this.b = null;
             }
         } else {
             if (this.random.nextFloat() < 0.05F) {
-                this.aw = (this.random.nextFloat() - 0.5F) * 20.0F;
+                this.ax = (this.random.nextFloat() - 0.5F) * 20.0F;
             }
 
-            this.yaw += this.aw;
-            this.pitch = this.ay;
+            this.yaw += this.ax;
+            this.pitch = this.az;
         }
 
-        boolean flag = this.g_();
-        boolean flag1 = this.U();
+        boolean flag = this.Z();
+        boolean flag1 = this.aa();
 
         if (flag || flag1) {
-            this.ax = this.random.nextFloat() < 0.8F;
+            this.ay = this.random.nextFloat() < 0.8F;
         }
     }
 
-    protected int n_() {
-        return 10;
+    protected int v() {
+        return 40;
     }
 
     public void a(Entity entity, float f, float f1) {
@@ -705,24 +714,24 @@ public abstract class EntityLiving extends Entity {
         if (entity instanceof EntityLiving) {
             EntityLiving entityliving = (EntityLiving) entity;
 
-            d2 = this.locY + (double) this.q() - (entityliving.locY + (double) entityliving.q());
+            d2 = this.locY + (double) this.s() - (entityliving.locY + (double) entityliving.s());
         } else {
-            d2 = (entity.boundingBox.b + entity.boundingBox.e) / 2.0D - (this.locY + (double) this.q());
+            d2 = (entity.boundingBox.b + entity.boundingBox.e) / 2.0D - (this.locY + (double) this.s());
         }
 
         double d3 = (double) MathHelper.a(d0 * d0 + d1 * d1);
         float f2 = (float) (Math.atan2(d1, d0) * 180.0D / 3.1415927410125732D) - 90.0F;
-        float f3 = (float) (Math.atan2(d2, d3) * 180.0D / 3.1415927410125732D);
+        float f3 = (float) (-(Math.atan2(d2, d3) * 180.0D / 3.1415927410125732D));
 
         this.pitch = -this.b(this.pitch, f3, f1);
         this.yaw = this.b(this.yaw, f2, f);
     }
 
-    public boolean N() {
+    public boolean R() {
         return this.b != null;
     }
 
-    public Entity O() {
+    public Entity S() {
         return this.b;
     }
 
@@ -748,17 +757,17 @@ public abstract class EntityLiving extends Entity {
         return f + f3;
     }
 
-    public void P() {}
+    public void T() {}
 
-    public boolean b() {
-        return this.world.containsEntity(this.boundingBox) && this.world.getEntities(this, this.boundingBox).size() == 0 && !this.world.b(this.boundingBox);
+    public boolean d() {
+        return this.world.containsEntity(this.boundingBox) && this.world.getEntities(this, this.boundingBox).size() == 0 && !this.world.c(this.boundingBox);
     }
 
-    protected void Q() {
+    protected void U() {
         this.damageEntity((Entity) null, 4);
     }
 
-    public Vec3D R() {
+    public Vec3D V() {
         return this.b(1.0F);
     }
 
@@ -786,7 +795,7 @@ public abstract class EntityLiving extends Entity {
         }
     }
 
-    public int j() {
+    public int l() {
         return 4;
     }
 
