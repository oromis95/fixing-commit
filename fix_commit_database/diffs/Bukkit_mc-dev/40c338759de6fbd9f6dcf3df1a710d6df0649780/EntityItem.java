@@ -21,7 +21,7 @@ public class EntityItem extends Entity {
         this.motZ = (double) ((float) (Math.random() * 0.20000000298023224D - 0.10000000149011612D));
     }
 
-    protected boolean l() {
+    protected boolean n() {
         return false;
     }
 
@@ -31,10 +31,10 @@ public class EntityItem extends Entity {
         this.height = this.width / 2.0F;
     }
 
-    protected void a() {}
+    protected void b() {}
 
-    public void f_() {
-        super.f_();
+    public void p_() {
+        super.p_();
         if (this.pickupDelay > 0) {
             --this.pickupDelay;
         }
@@ -77,7 +77,7 @@ public class EntityItem extends Entity {
         }
     }
 
-    public boolean g_() {
+    public boolean f_() {
         return this.world.a(this.boundingBox, Material.WATER, this);
     }
 
@@ -164,7 +164,7 @@ public class EntityItem extends Entity {
     }
 
     public boolean damageEntity(Entity entity, int i) {
-        this.V();
+        this.ab();
         this.f -= i;
         if (this.f <= 0) {
             this.die();
@@ -173,13 +173,13 @@ public class EntityItem extends Entity {
         return false;
     }
 
-    public void a(NBTTagCompound nbttagcompound) {
+    public void b(NBTTagCompound nbttagcompound) {
         nbttagcompound.a("Health", (short) ((byte) this.f));
         nbttagcompound.a("Age", (short) this.b);
         nbttagcompound.a("Item", this.itemStack.a(new NBTTagCompound()));
     }
 
-    public void b(NBTTagCompound nbttagcompound) {
+    public void a(NBTTagCompound nbttagcompound) {
         this.f = nbttagcompound.d("Health") & 255;
         this.b = nbttagcompound.d("Age");
         NBTTagCompound nbttagcompound1 = nbttagcompound.k("Item");
@@ -192,6 +192,14 @@ public class EntityItem extends Entity {
             int i = this.itemStack.count;
 
             if (this.pickupDelay == 0 && entityhuman.inventory.canHold(this.itemStack)) {
+                if (this.itemStack.id == Block.LOG.id) {
+                    entityhuman.a((Statistic) AchievementList.g);
+                }
+
+                if (this.itemStack.id == Item.LEATHER.id) {
+                    entityhuman.a((Statistic) AchievementList.t);
+                }
+
                 this.world.makeSound(this, "random.pop", 0.2F, ((this.random.nextFloat() - this.random.nextFloat()) * 0.7F + 1.0F) * 2.0F);
                 entityhuman.receive(this, i);
                 this.die();
