@@ -7,39 +7,41 @@ public class ItemRedstone extends Item {
     }
 
     public boolean a(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
-        if (l == 0) {
-            --j;
-        }
+        if (world.getTypeId(i, j, k) != Block.SNOW.id) {
+            if (l == 0) {
+                --j;
+            }
 
-        if (l == 1) {
-            ++j;
-        }
+            if (l == 1) {
+                ++j;
+            }
 
-        if (l == 2) {
-            --k;
-        }
+            if (l == 2) {
+                --k;
+            }
 
-        if (l == 3) {
-            ++k;
-        }
+            if (l == 3) {
+                ++k;
+            }
 
-        if (l == 4) {
-            --i;
-        }
+            if (l == 4) {
+                --i;
+            }
 
-        if (l == 5) {
-            ++i;
-        }
+            if (l == 5) {
+                ++i;
+            }
 
-        if (!world.isEmpty(i, j, k)) {
-            return false;
-        } else {
-            if (Block.REDSTONE_WIRE.canPlace(world, i, j, k)) {
-                --itemstack.count;
-                world.setTypeId(i, j, k, Block.REDSTONE_WIRE.id);
+            if (!world.isEmpty(i, j, k)) {
+                return false;
             }
+        }
 
-            return true;
+        if (Block.REDSTONE_WIRE.canPlace(world, i, j, k)) {
+            --itemstack.count;
+            world.setTypeId(i, j, k, Block.REDSTONE_WIRE.id);
         }
+
+        return true;
     }
 }
