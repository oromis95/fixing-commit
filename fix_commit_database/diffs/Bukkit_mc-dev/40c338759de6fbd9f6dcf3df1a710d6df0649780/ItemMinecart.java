@@ -13,7 +13,7 @@ public class ItemMinecart extends Item {
     public boolean a(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
         int i1 = world.getTypeId(i, j, k);
 
-        if (i1 == Block.RAILS.id) {
+        if (BlockMinecartTrack.c(i1)) {
             if (!world.isStatic) {
                 world.addEntity(new EntityMinecart(world, (double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), this.a));
             }
