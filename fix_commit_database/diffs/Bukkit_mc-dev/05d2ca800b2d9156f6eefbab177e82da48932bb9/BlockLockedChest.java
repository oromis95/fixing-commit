@@ -1,5 +1,7 @@
 package net.minecraft.server;
 
+import java.util.Random;
+
 public class BlockLockedChest extends Block {
 
     protected BlockLockedChest(int i) {
@@ -12,33 +14,10 @@ public class BlockLockedChest extends Block {
     }
 
     public boolean canPlace(World world, int i, int j, int k) {
-        int l = 0;
-
-        if (world.getTypeId(i - 1, j, k) == this.id) {
-            ++l;
-        }
-
-        if (world.getTypeId(i + 1, j, k) == this.id) {
-            ++l;
-        }
-
-        if (world.getTypeId(i, j, k - 1) == this.id) {
-            ++l;
-        }
-
-        if (world.getTypeId(i, j, k + 1) == this.id) {
-            ++l;
-        }
-
-        return l > 1 ? false : (this.g(world, i - 1, j, k) ? false : (this.g(world, i + 1, j, k) ? false : (this.g(world, i, j, k - 1) ? false : !this.g(world, i, j, k + 1))));
-    }
-
-    private boolean g(World world, int i, int j, int k) {
-        return world.getTypeId(i, j, k) != this.id ? false : (world.getTypeId(i - 1, j, k) == this.id ? true : (world.getTypeId(i + 1, j, k) == this.id ? true : (world.getTypeId(i, j, k - 1) == this.id ? true : world.getTypeId(i, j, k + 1) == this.id)));
+        return true;
     }
 
-    public boolean interact(World world, int i, int j, int k, EntityHuman entityhuman) {
-        entityhuman.J();
-        return true;
+    public void a(World world, int i, int j, int k, Random random) {
+        world.setTypeId(i, j, k, 0);
     }
 }
