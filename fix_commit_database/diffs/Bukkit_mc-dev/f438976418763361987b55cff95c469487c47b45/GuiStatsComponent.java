@@ -12,14 +12,14 @@ public class GuiStatsComponent extends JComponent {
     private static final DecimalFormat a = new DecimalFormat("########0.000");
     private int[] b = new int[256];
     private int c = 0;
-    private String[] d = new String[10];
+    private String[] d = new String[11];
     private final MinecraftServer e;
 
     public GuiStatsComponent(MinecraftServer minecraftserver) {
         this.e = minecraftserver;
-        this.setPreferredSize(new Dimension(356, 246));
-        this.setMinimumSize(new Dimension(356, 246));
-        this.setMaximumSize(new Dimension(356, 246));
+        this.setPreferredSize(new Dimension(456, 246));
+        this.setMinimumSize(new Dimension(456, 246));
+        this.setMaximumSize(new Dimension(456, 246));
         (new Timer(500, new GuiStatsListener(this))).start();
         this.setBackground(Color.BLACK);
     }
@@ -38,6 +38,7 @@ public class GuiStatsComponent extends JComponent {
                 this.d[5 + j] = "Lvl " + j + " tick: " + a.format(this.a(this.e.k[j]) * 1.0E-6D) + " ms";
                 if (this.e.worldServer[j] != null && this.e.worldServer[j].chunkProviderServer != null) {
                     this.d[5 + j] = this.d[5 + j] + ", " + this.e.worldServer[j].chunkProviderServer.getName();
+                    this.d[5 + j] = this.d[5 + j] + ", Vec3: " + this.e.worldServer[j].getVec3DPool().d() + " / " + this.e.worldServer[j].getVec3DPool().c();
                 }
             }
         }
@@ -62,7 +63,7 @@ public class GuiStatsComponent extends JComponent {
 
     public void paint(Graphics graphics) {
         graphics.setColor(new Color(16777215));
-        graphics.fillRect(0, 0, 356, 246);
+        graphics.fillRect(0, 0, 456, 246);
 
         int i;
 
