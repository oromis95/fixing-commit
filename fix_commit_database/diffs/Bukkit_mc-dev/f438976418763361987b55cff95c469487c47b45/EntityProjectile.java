@@ -3,7 +3,7 @@ package net.minecraft.server;
 import java.util.Iterator;
 import java.util.List;
 
-public abstract class EntityProjectile extends Entity {
+public abstract class EntityProjectile extends Entity implements IProjectile {
 
     private int blockX = -1;
     private int blockY = -1;
@@ -36,8 +36,8 @@ public abstract class EntityProjectile extends Entity {
 
         this.motX = (double) (-MathHelper.sin(this.yaw / 180.0F * 3.1415927F) * MathHelper.cos(this.pitch / 180.0F * 3.1415927F) * f);
         this.motZ = (double) (MathHelper.cos(this.yaw / 180.0F * 3.1415927F) * MathHelper.cos(this.pitch / 180.0F * 3.1415927F) * f);
-        this.motY = (double) (-MathHelper.sin((this.pitch + this.g()) / 180.0F * 3.1415927F) * f);
-        this.c(this.motX, this.motY, this.motZ, this.d(), 1.0F);
+        this.motY = (double) (-MathHelper.sin((this.pitch + this.d()) / 180.0F * 3.1415927F) * f);
+        this.shoot(this.motX, this.motY, this.motZ, this.c(), 1.0F);
     }
 
     public EntityProjectile(World world, double d0, double d1, double d2) {
@@ -48,15 +48,15 @@ public abstract class EntityProjectile extends Entity {
         this.height = 0.0F;
     }
 
-    protected float d() {
+    protected float c() {
         return 1.5F;
     }
 
-    protected float g() {
+    protected float d() {
         return 0.0F;
     }
 
-    public void c(double d0, double d1, double d2, float f, float f1) {
+    public void shoot(double d0, double d1, double d2, float f, float f1) {
         float f2 = MathHelper.sqrt(d0 * d0 + d1 * d1 + d2 * d2);
 
         d0 /= (double) f2;
@@ -78,11 +78,11 @@ public abstract class EntityProjectile extends Entity {
         this.h = 0;
     }
 
-    public void h_() {
-        this.S = this.locX;
-        this.T = this.locY;
-        this.U = this.locZ;
-        super.h_();
+    public void j_() {
+        this.T = this.locX;
+        this.U = this.locY;
+        this.V = this.locZ;
+        super.j_();
         if (this.shake > 0) {
             --this.shake;
         }
@@ -109,14 +109,14 @@ public abstract class EntityProjectile extends Entity {
             ++this.i;
         }
 
-        Vec3D vec3d = Vec3D.a().create(this.locX, this.locY, this.locZ);
-        Vec3D vec3d1 = Vec3D.a().create(this.locX + this.motX, this.locY + this.motY, this.locZ + this.motZ);
+        Vec3D vec3d = this.world.getVec3DPool().create(this.locX, this.locY, this.locZ);
+        Vec3D vec3d1 = this.world.getVec3DPool().create(this.locX + this.motX, this.locY + this.motY, this.locZ + this.motZ);
         MovingObjectPosition movingobjectposition = this.world.a(vec3d, vec3d1);
 
-        vec3d = Vec3D.a().create(this.locX, this.locY, this.locZ);
-        vec3d1 = Vec3D.a().create(this.locX + this.motX, this.locY + this.motY, this.locZ + this.motZ);
+        vec3d = this.world.getVec3DPool().create(this.locX, this.locY, this.locZ);
+        vec3d1 = this.world.getVec3DPool().create(this.locX + this.motX, this.locY + this.motY, this.locZ + this.motZ);
         if (movingobjectposition != null) {
-            vec3d1 = Vec3D.a().create(movingobjectposition.pos.a, movingobjectposition.pos.b, movingobjectposition.pos.c);
+            vec3d1 = this.world.getVec3DPool().create(movingobjectposition.pos.c, movingobjectposition.pos.d, movingobjectposition.pos.e);
         }
 
         if (!this.world.isStatic) {
@@ -150,7 +150,11 @@ public abstract class EntityProjectile extends Entity {
         }
 
         if (movingobjectposition != null) {
-            this.a(movingobjectposition);
+            if (movingobjectposition.type == EnumMovingObjectType.TILE && this.world.getTypeId(movingobjectposition.b, movingobjectposition.c, movingobjectposition.d) == Block.PORTAL.id) {
+                this.aa();
+            } else {
+                this.a(movingobjectposition);
+            }
         }
 
         this.locX += this.motX;
@@ -179,13 +183,13 @@ public abstract class EntityProjectile extends Entity {
         this.pitch = this.lastPitch + (this.pitch - this.lastPitch) * 0.2F;
         this.yaw = this.lastYaw + (this.yaw - this.lastYaw) * 0.2F;
         float f2 = 0.99F;
-        float f3 = this.h();
+        float f3 = this.g();
 
         if (this.H()) {
             for (int j = 0; j < 4; ++j) {
                 float f4 = 0.25F;
 
-                this.world.a("bubble", this.locX - this.motX * (double) f4, this.locY - this.motY * (double) f4, this.locZ - this.motZ * (double) f4, this.motX, this.motY, this.motZ);
+                this.world.addParticle("bubble", this.locX - this.motX * (double) f4, this.locY - this.motY * (double) f4, this.locZ - this.motZ * (double) f4, this.motX, this.motY, this.motZ);
             }
 
             f2 = 0.8F;
@@ -198,7 +202,7 @@ public abstract class EntityProjectile extends Entity {
         this.setPosition(this.locX, this.locY, this.locZ);
     }
 
-    protected float h() {
+    protected float g() {
         return 0.03F;
     }
 
