@@ -44,15 +44,15 @@ public class ContainerMerchant extends Container {
         super.a(iinventory);
     }
 
-    public void c(int i) {
+    public void b(int i) {
         this.f.c(i);
     }
 
     public boolean c(EntityHuman entityhuman) {
-        return this.merchant.l_() == entityhuman;
+        return this.merchant.m_() == entityhuman;
     }
 
-    public ItemStack b(int i) {
+    public ItemStack b(EntityHuman entityhuman, int i) {
         ItemStack itemstack = null;
         Slot slot = (Slot) this.b.get(i);
 
@@ -88,7 +88,7 @@ public class ContainerMerchant extends Container {
                 return null;
             }
 
-            slot.b(itemstack1);
+            slot.a(entityhuman, itemstack1);
         }
 
         return itemstack;
