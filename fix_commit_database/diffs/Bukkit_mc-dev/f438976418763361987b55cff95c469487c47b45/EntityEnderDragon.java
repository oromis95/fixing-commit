@@ -3,45 +3,48 @@ package net.minecraft.server;
 import java.util.Iterator;
 import java.util.List;
 
-public class EntityEnderDragon extends EntityComplex {
+public class EntityEnderDragon extends EntityLiving implements IComplex {
 
+    public double a;
     public double b;
     public double c;
-    public double d;
-    public double[][] e = new double[64][3];
-    public int f = -1;
+    public double[][] d = new double[64][3];
+    public int e = -1;
     public EntityComplexPart[] children;
+    public EntityComplexPart g;
     public EntityComplexPart h;
     public EntityComplexPart i;
     public EntityComplexPart j;
-    public EntityComplexPart by;
-    public EntityComplexPart bz;
-    public EntityComplexPart bA;
-    public EntityComplexPart bB;
-    public float bC = 0.0F;
-    public float bD = 0.0F;
-    public boolean bE = false;
-    public boolean bF = false;
-    private Entity bI;
-    public int bG = 0;
-    public EntityEnderCrystal bH = null;
+    public EntityComplexPart bK;
+    public EntityComplexPart bL;
+    public EntityComplexPart bM;
+    public float bN = 0.0F;
+    public float bO = 0.0F;
+    public boolean bP = false;
+    public boolean bQ = false;
+    private Entity bT;
+    public int bR = 0;
+    public EntityEnderCrystal bS = null;
 
     public EntityEnderDragon(World world) {
         super(world);
-        this.children = new EntityComplexPart[] { this.h = new EntityComplexPart(this, "head", 6.0F, 6.0F), this.i = new EntityComplexPart(this, "body", 8.0F, 8.0F), this.j = new EntityComplexPart(this, "tail", 4.0F, 4.0F), this.by = new EntityComplexPart(this, "tail", 4.0F, 4.0F), this.bz = new EntityComplexPart(this, "tail", 4.0F, 4.0F), this.bA = new EntityComplexPart(this, "wing", 4.0F, 4.0F), this.bB = new EntityComplexPart(this, "wing", 4.0F, 4.0F)};
-        this.a = 200;
-        this.setHealth(this.a);
+        this.children = new EntityComplexPart[] { this.g = new EntityComplexPart(this, "head", 6.0F, 6.0F), this.h = new EntityComplexPart(this, "body", 8.0F, 8.0F), this.i = new EntityComplexPart(this, "tail", 4.0F, 4.0F), this.j = new EntityComplexPart(this, "tail", 4.0F, 4.0F), this.bK = new EntityComplexPart(this, "tail", 4.0F, 4.0F), this.bL = new EntityComplexPart(this, "wing", 4.0F, 4.0F), this.bM = new EntityComplexPart(this, "wing", 4.0F, 4.0F)};
+        this.setHealth(this.getMaxHealth());
         this.texture = "/mob/enderdragon/ender.png";
         this.a(16.0F, 8.0F);
-        this.X = true;
+        this.Y = true;
         this.fireProof = true;
-        this.c = 100.0D;
-        this.ak = true;
+        this.b = 100.0D;
+        this.al = true;
+    }
+
+    public int getMaxHealth() {
+        return 200;
     }
 
     protected void a() {
         super.a();
-        this.datawatcher.a(16, new Integer(this.a));
+        this.datawatcher.a(16, new Integer(this.getMaxHealth()));
     }
 
     public double[] a(int i, float f) {
@@ -50,59 +53,66 @@ public class EntityEnderDragon extends EntityComplex {
         }
 
         f = 1.0F - f;
-        int j = this.f - i * 1 & 63;
-        int k = this.f - i * 1 - 1 & 63;
+        int j = this.e - i * 1 & 63;
+        int k = this.e - i * 1 - 1 & 63;
         double[] adouble = new double[3];
-        double d0 = this.e[j][0];
-        double d1 = MathHelper.g(this.e[k][0] - d0);
+        double d0 = this.d[j][0];
+        double d1 = MathHelper.g(this.d[k][0] - d0);
 
         adouble[0] = d0 + d1 * (double) f;
-        d0 = this.e[j][1];
-        d1 = this.e[k][1] - d0;
+        d0 = this.d[j][1];
+        d1 = this.d[k][1] - d0;
         adouble[1] = d0 + d1 * (double) f;
-        adouble[2] = this.e[j][2] + (this.e[k][2] - this.e[j][2]) * (double) f;
+        adouble[2] = this.d[j][2] + (this.d[k][2] - this.d[j][2]) * (double) f;
         return adouble;
     }
 
-    public void d() {
-        this.bC = this.bD;
+    public void c() {
+        float f;
+        float f1;
+
         if (!this.world.isStatic) {
             this.datawatcher.watch(16, Integer.valueOf(this.health));
+        } else {
+            f = MathHelper.cos(this.bO * 3.1415927F * 2.0F);
+            f1 = MathHelper.cos(this.bN * 3.1415927F * 2.0F);
+            if (f1 <= -0.3F && f >= -0.3F) {
+                this.world.b(this.locX, this.locY, this.locZ, "mob.enderdragon.wings", 5.0F, 0.8F + this.random.nextFloat() * 0.3F);
+            }
         }
 
-        float f;
-        float f1;
-        float d05;
+        this.bN = this.bO;
+        float f2;
 
         if (this.health <= 0) {
             f = (this.random.nextFloat() - 0.5F) * 8.0F;
-            d05 = (this.random.nextFloat() - 0.5F) * 4.0F;
-            f1 = (this.random.nextFloat() - 0.5F) * 8.0F;
-            this.world.a("largeexplode", this.locX + (double) f, this.locY + 2.0D + (double) d05, this.locZ + (double) f1, 0.0D, 0.0D, 0.0D);
+            f1 = (this.random.nextFloat() - 0.5F) * 4.0F;
+            f2 = (this.random.nextFloat() - 0.5F) * 8.0F;
+            this.world.addParticle("largeexplode", this.locX + (double) f, this.locY + 2.0D + (double) f1, this.locZ + (double) f2, 0.0D, 0.0D, 0.0D);
         } else {
-            this.j();
+            this.h();
             f = 0.2F / (MathHelper.sqrt(this.motX * this.motX + this.motZ * this.motZ) * 10.0F + 1.0F);
             f *= (float) Math.pow(2.0D, this.motY);
-            if (this.bF) {
-                this.bD += f * 0.5F;
+            if (this.bQ) {
+                this.bO += f * 0.5F;
             } else {
-                this.bD += f;
+                this.bO += f;
             }
 
             this.yaw = MathHelper.g(this.yaw);
-            if (this.f < 0) {
-                for (int i = 0; i < this.e.length; ++i) {
-                    this.e[i][0] = (double) this.yaw;
-                    this.e[i][1] = this.locY;
+            if (this.e < 0) {
+                for (int d05 = 0; d05 < this.d.length; ++d05) {
+                    this.d[d05][0] = (double) this.yaw;
+                    this.d[d05][1] = this.locY;
                 }
             }
 
-            if (++this.f == this.e.length) {
-                this.f = 0;
+            if (++this.e == this.d.length) {
+                this.e = 0;
             }
 
-            this.e[this.f][0] = (double) this.yaw;
-            this.e[this.f][1] = this.locY;
+            this.d[this.e][0] = (double) this.yaw;
+            this.d[this.e][1] = this.locY;
             double d0;
             double d1;
             double d2;
@@ -110,27 +120,27 @@ public class EntityEnderDragon extends EntityComplex {
             float f3;
 
             if (this.world.isStatic) {
-                if (this.bi > 0) {
-                    d0 = this.locX + (this.bj - this.locX) / (double) this.bi;
-                    d1 = this.locY + (this.bk - this.locY) / (double) this.bi;
-                    d2 = this.locZ + (this.bl - this.locZ) / (double) this.bi;
-                    d3 = MathHelper.g(this.bm - (double) this.yaw);
-                    this.yaw = (float) ((double) this.yaw + d3 / (double) this.bi);
-                    this.pitch = (float) ((double) this.pitch + (this.bn - (double) this.pitch) / (double) this.bi);
-                    --this.bi;
+                if (this.bu > 0) {
+                    d0 = this.locX + (this.bv - this.locX) / (double) this.bu;
+                    d1 = this.locY + (this.bw - this.locY) / (double) this.bu;
+                    d2 = this.locZ + (this.bx - this.locZ) / (double) this.bu;
+                    d3 = MathHelper.g(this.by - (double) this.yaw);
+                    this.yaw = (float) ((double) this.yaw + d3 / (double) this.bu);
+                    this.pitch = (float) ((double) this.pitch + (this.bz - (double) this.pitch) / (double) this.bu);
+                    --this.bu;
                     this.setPosition(d0, d1, d2);
                     this.b(this.yaw, this.pitch);
                 }
             } else {
-                d0 = this.b - this.locX;
-                d1 = this.c - this.locY;
-                d2 = this.d - this.locZ;
+                d0 = this.a - this.locX;
+                d1 = this.b - this.locY;
+                d2 = this.c - this.locZ;
                 d3 = d0 * d0 + d1 * d1 + d2 * d2;
-                if (this.bI != null) {
-                    this.b = this.bI.locX;
-                    this.d = this.bI.locZ;
-                    double d4 = this.b - this.locX;
-                    double d5 = this.d - this.locZ;
+                if (this.bT != null) {
+                    this.a = this.bT.locX;
+                    this.c = this.bT.locZ;
+                    double d4 = this.a - this.locX;
+                    double d5 = this.c - this.locZ;
                     double d6 = Math.sqrt(d4 * d4 + d5 * d5);
                     double d7 = 0.4000000059604645D + d6 / 80.0D - 1.0D;
 
@@ -138,14 +148,14 @@ public class EntityEnderDragon extends EntityComplex {
                         d7 = 10.0D;
                     }
 
-                    this.c = this.bI.boundingBox.b + d7;
+                    this.b = this.bT.boundingBox.b + d7;
                 } else {
-                    this.b += this.random.nextGaussian() * 2.0D;
-                    this.d += this.random.nextGaussian() * 2.0D;
+                    this.a += this.random.nextGaussian() * 2.0D;
+                    this.c += this.random.nextGaussian() * 2.0D;
                 }
 
-                if (this.bE || d3 < 100.0D || d3 > 22500.0D || this.positionChanged || this.G) {
-                    this.k();
+                if (this.bP || d3 < 100.0D || d3 > 22500.0D || this.positionChanged || this.G) {
+                    this.i();
                 }
 
                 d1 /= (double) MathHelper.sqrt(d0 * d0 + d2 * d2);
@@ -171,15 +181,15 @@ public class EntityEnderDragon extends EntityComplex {
                     d9 = -50.0D;
                 }
 
-                Vec3D vec3d = Vec3D.a().create(this.b - this.locX, this.c - this.locY, this.d - this.locZ).b();
-                Vec3D vec3d1 = Vec3D.a().create((double) MathHelper.sin(this.yaw * 3.1415927F / 180.0F), this.motY, (double) (-MathHelper.cos(this.yaw * 3.1415927F / 180.0F))).b();
+                Vec3D vec3d = this.world.getVec3DPool().create(this.a - this.locX, this.b - this.locY, this.c - this.locZ).a();
+                Vec3D vec3d1 = this.world.getVec3DPool().create((double) MathHelper.sin(this.yaw * 3.1415927F / 180.0F), this.motY, (double) (-MathHelper.cos(this.yaw * 3.1415927F / 180.0F))).a();
                 float f4 = (float) (vec3d1.b(vec3d) + 0.5D) / 1.5F;
 
                 if (f4 < 0.0F) {
                     f4 = 0.0F;
                 }
 
-                this.bt *= 0.8F;
+                this.bF *= 0.8F;
                 float f5 = MathHelper.sqrt(this.motX * this.motX + this.motZ * this.motZ) * 1.0F + 1.0F;
                 double d10 = Math.sqrt(this.motX * this.motX + this.motZ * this.motZ) * 1.0D + 1.0D;
 
@@ -187,19 +197,19 @@ public class EntityEnderDragon extends EntityComplex {
                     d10 = 40.0D;
                 }
 
-                this.bt = (float) ((double) this.bt + d9 * (0.699999988079071D / d10 / (double) f5));
-                this.yaw += this.bt * 0.1F;
+                this.bF = (float) ((double) this.bF + d9 * (0.699999988079071D / d10 / (double) f5));
+                this.yaw += this.bF * 0.1F;
                 float f6 = (float) (2.0D / (d10 + 1.0D));
                 float f7 = 0.06F;
 
                 this.a(0.0F, -1.0F, f7 * (f4 * f6 + (1.0F - f6)));
-                if (this.bF) {
+                if (this.bQ) {
                     this.move(this.motX * 0.800000011920929D, this.motY * 0.800000011920929D, this.motZ * 0.800000011920929D);
                 } else {
                     this.move(this.motX, this.motY, this.motZ);
                 }
 
-                Vec3D vec3d2 = Vec3D.a().create(this.motX, this.motY, this.motZ).b();
+                Vec3D vec3d2 = this.world.getVec3DPool().create(this.motX, this.motY, this.motZ).a();
                 float f8 = (float) (vec3d2.b(vec3d1) + 1.0D) / 2.0F;
 
                 f8 = 0.8F + 0.15F * f8;
@@ -208,58 +218,58 @@ public class EntityEnderDragon extends EntityComplex {
                 this.motY *= 0.9100000262260437D;
             }
 
-            this.aq = this.yaw;
-            this.h.width = this.h.length = 3.0F;
+            this.aw = this.yaw;
+            this.g.width = this.g.length = 3.0F;
+            this.i.width = this.i.length = 2.0F;
             this.j.width = this.j.length = 2.0F;
-            this.by.width = this.by.length = 2.0F;
-            this.bz.width = this.bz.length = 2.0F;
-            this.i.length = 3.0F;
-            this.i.width = 5.0F;
-            this.bA.length = 2.0F;
-            this.bA.width = 4.0F;
-            this.bB.length = 3.0F;
-            this.bB.width = 4.0F;
-            d05 = (float) (this.a(5, 1.0F)[1] - this.a(10, 1.0F)[1]) * 10.0F / 180.0F * 3.1415927F;
-            f1 = MathHelper.cos(d05);
-            float f9 = -MathHelper.sin(d05);
+            this.bK.width = this.bK.length = 2.0F;
+            this.h.length = 3.0F;
+            this.h.width = 5.0F;
+            this.bL.length = 2.0F;
+            this.bL.width = 4.0F;
+            this.bM.length = 3.0F;
+            this.bM.width = 4.0F;
+            f1 = (float) (this.a(5, 1.0F)[1] - this.a(10, 1.0F)[1]) * 10.0F / 180.0F * 3.1415927F;
+            f2 = MathHelper.cos(f1);
+            float f9 = -MathHelper.sin(f1);
             float f10 = this.yaw * 3.1415927F / 180.0F;
             float f11 = MathHelper.sin(f10);
             float f12 = MathHelper.cos(f10);
 
-            this.i.h_();
-            this.i.setPositionRotation(this.locX + (double) (f11 * 0.5F), this.locY, this.locZ - (double) (f12 * 0.5F), 0.0F, 0.0F);
-            this.bA.h_();
-            this.bA.setPositionRotation(this.locX + (double) (f12 * 4.5F), this.locY + 2.0D, this.locZ + (double) (f11 * 4.5F), 0.0F, 0.0F);
-            this.bB.h_();
-            this.bB.setPositionRotation(this.locX - (double) (f12 * 4.5F), this.locY + 2.0D, this.locZ - (double) (f11 * 4.5F), 0.0F, 0.0F);
+            this.h.j_();
+            this.h.setPositionRotation(this.locX + (double) (f11 * 0.5F), this.locY, this.locZ - (double) (f12 * 0.5F), 0.0F, 0.0F);
+            this.bL.j_();
+            this.bL.setPositionRotation(this.locX + (double) (f12 * 4.5F), this.locY + 2.0D, this.locZ + (double) (f11 * 4.5F), 0.0F, 0.0F);
+            this.bM.j_();
+            this.bM.setPositionRotation(this.locX - (double) (f12 * 4.5F), this.locY + 2.0D, this.locZ - (double) (f11 * 4.5F), 0.0F, 0.0F);
             if (!this.world.isStatic && this.hurtTicks == 0) {
-                this.a(this.world.getEntities(this, this.bA.boundingBox.grow(4.0D, 2.0D, 4.0D).d(0.0D, -2.0D, 0.0D)));
-                this.a(this.world.getEntities(this, this.bB.boundingBox.grow(4.0D, 2.0D, 4.0D).d(0.0D, -2.0D, 0.0D)));
-                this.b(this.world.getEntities(this, this.h.boundingBox.grow(1.0D, 1.0D, 1.0D)));
+                this.a(this.world.getEntities(this, this.bL.boundingBox.grow(4.0D, 2.0D, 4.0D).d(0.0D, -2.0D, 0.0D)));
+                this.a(this.world.getEntities(this, this.bM.boundingBox.grow(4.0D, 2.0D, 4.0D).d(0.0D, -2.0D, 0.0D)));
+                this.b(this.world.getEntities(this, this.g.boundingBox.grow(1.0D, 1.0D, 1.0D)));
             }
 
             double[] adouble = this.a(5, 1.0F);
             double[] adouble1 = this.a(0, 1.0F);
 
-            f3 = MathHelper.sin(this.yaw * 3.1415927F / 180.0F - this.bt * 0.01F);
-            float f13 = MathHelper.cos(this.yaw * 3.1415927F / 180.0F - this.bt * 0.01F);
+            f3 = MathHelper.sin(this.yaw * 3.1415927F / 180.0F - this.bF * 0.01F);
+            float f13 = MathHelper.cos(this.yaw * 3.1415927F / 180.0F - this.bF * 0.01F);
 
-            this.h.h_();
-            this.h.setPositionRotation(this.locX + (double) (f3 * 5.5F * f1), this.locY + (adouble1[1] - adouble[1]) * 1.0D + (double) (f9 * 5.5F), this.locZ - (double) (f13 * 5.5F * f1), 0.0F, 0.0F);
+            this.g.j_();
+            this.g.setPositionRotation(this.locX + (double) (f3 * 5.5F * f2), this.locY + (adouble1[1] - adouble[1]) * 1.0D + (double) (f9 * 5.5F), this.locZ - (double) (f13 * 5.5F * f2), 0.0F, 0.0F);
 
             for (int j = 0; j < 3; ++j) {
                 EntityComplexPart entitycomplexpart = null;
 
                 if (j == 0) {
-                    entitycomplexpart = this.j;
+                    entitycomplexpart = this.i;
                 }
 
                 if (j == 1) {
-                    entitycomplexpart = this.by;
+                    entitycomplexpart = this.j;
                 }
 
                 if (j == 2) {
-                    entitycomplexpart = this.bz;
+                    entitycomplexpart = this.bK;
                 }
 
                 double[] adouble2 = this.a(12 + j * 2, 1.0F);
@@ -269,25 +279,25 @@ public class EntityEnderDragon extends EntityComplex {
                 float f17 = 1.5F;
                 float f18 = (float) (j + 1) * 2.0F;
 
-                entitycomplexpart.h_();
-                entitycomplexpart.setPositionRotation(this.locX - (double) ((f11 * f17 + f15 * f18) * f1), this.locY + (adouble2[1] - adouble[1]) * 1.0D - (double) ((f18 + f17) * f9) + 1.5D, this.locZ + (double) ((f12 * f17 + f16 * f18) * f1), 0.0F, 0.0F);
+                entitycomplexpart.j_();
+                entitycomplexpart.setPositionRotation(this.locX - (double) ((f11 * f17 + f15 * f18) * f2), this.locY + (adouble2[1] - adouble[1]) * 1.0D - (double) ((f18 + f17) * f9) + 1.5D, this.locZ + (double) ((f12 * f17 + f16 * f18) * f2), 0.0F, 0.0F);
             }
 
             if (!this.world.isStatic) {
-                this.bF = this.a(this.h.boundingBox) | this.a(this.i.boundingBox);
+                this.bQ = this.a(this.g.boundingBox) | this.a(this.h.boundingBox);
             }
         }
     }
 
-    private void j() {
-        if (this.bH != null) {
-            if (this.bH.dead) {
+    private void h() {
+        if (this.bS != null) {
+            if (this.bS.dead) {
                 if (!this.world.isStatic) {
-                    this.a(this.h, DamageSource.EXPLOSION, 10);
+                    this.a(this.g, DamageSource.EXPLOSION, 10);
                 }
 
-                this.bH = null;
-            } else if (this.ticksLived % 10 == 0 && this.health < this.a) {
+                this.bS = null;
+            } else if (this.ticksLived % 10 == 0 && this.health < this.getMaxHealth()) {
                 ++this.health;
             }
         }
@@ -309,13 +319,13 @@ public class EntityEnderDragon extends EntityComplex {
                 }
             }
 
-            this.bH = entityendercrystal;
+            this.bS = entityendercrystal;
         }
     }
 
     private void a(List list) {
-        double d0 = (this.i.boundingBox.a + this.i.boundingBox.d) / 2.0D;
-        double d1 = (this.i.boundingBox.c + this.i.boundingBox.f) / 2.0D;
+        double d0 = (this.h.boundingBox.a + this.h.boundingBox.d) / 2.0D;
+        double d1 = (this.h.boundingBox.c + this.h.boundingBox.f) / 2.0D;
         Iterator iterator = list.iterator();
 
         while (iterator.hasNext()) {
@@ -343,27 +353,27 @@ public class EntityEnderDragon extends EntityComplex {
         }
     }
 
-    private void k() {
-        this.bE = false;
+    private void i() {
+        this.bP = false;
         if (this.random.nextInt(2) == 0 && !this.world.players.isEmpty()) {
-            this.bI = (Entity) this.world.players.get(this.random.nextInt(this.world.players.size()));
+            this.bT = (Entity) this.world.players.get(this.random.nextInt(this.world.players.size()));
         } else {
             boolean flag = false;
 
             do {
-                this.b = 0.0D;
-                this.c = (double) (70.0F + this.random.nextFloat() * 50.0F);
-                this.d = 0.0D;
-                this.b += (double) (this.random.nextFloat() * 120.0F - 60.0F);
-                this.d += (double) (this.random.nextFloat() * 120.0F - 60.0F);
-                double d0 = this.locX - this.b;
-                double d1 = this.locY - this.c;
-                double d2 = this.locZ - this.d;
+                this.a = 0.0D;
+                this.b = (double) (70.0F + this.random.nextFloat() * 50.0F);
+                this.c = 0.0D;
+                this.a += (double) (this.random.nextFloat() * 120.0F - 60.0F);
+                this.c += (double) (this.random.nextFloat() * 120.0F - 60.0F);
+                double d0 = this.locX - this.a;
+                double d1 = this.locY - this.b;
+                double d2 = this.locZ - this.c;
 
                 flag = d0 * d0 + d1 * d1 + d2 * d2 > 100.0D;
             } while (!flag);
 
-            this.bI = null;
+            this.bT = null;
         }
     }
 
@@ -403,14 +413,14 @@ public class EntityEnderDragon extends EntityComplex {
             double d1 = axisalignedbb.b + (axisalignedbb.e - axisalignedbb.b) * (double) this.random.nextFloat();
             double d2 = axisalignedbb.c + (axisalignedbb.f - axisalignedbb.c) * (double) this.random.nextFloat();
 
-            this.world.a("largeexplode", d0, d1, d2, 0.0D, 0.0D, 0.0D);
+            this.world.addParticle("largeexplode", d0, d1, d2, 0.0D, 0.0D, 0.0D);
         }
 
         return flag;
     }
 
     public boolean a(EntityComplexPart entitycomplexpart, DamageSource damagesource, int i) {
-        if (entitycomplexpart != this.h) {
+        if (entitycomplexpart != this.g) {
             i = i / 4 + 1;
         }
 
@@ -418,10 +428,10 @@ public class EntityEnderDragon extends EntityComplex {
         float f1 = MathHelper.sin(f);
         float f2 = MathHelper.cos(f);
 
-        this.b = this.locX + (double) (f1 * 5.0F) + (double) ((this.random.nextFloat() - 0.5F) * 2.0F);
-        this.c = this.locY + (double) (this.random.nextFloat() * 3.0F) + 1.0D;
-        this.d = this.locZ - (double) (f2 * 5.0F) + (double) ((this.random.nextFloat() - 0.5F) * 2.0F);
-        this.bI = null;
+        this.a = this.locX + (double) (f1 * 5.0F) + (double) ((this.random.nextFloat() - 0.5F) * 2.0F);
+        this.b = this.locY + (double) (this.random.nextFloat() * 3.0F) + 1.0D;
+        this.c = this.locZ - (double) (f2 * 5.0F) + (double) ((this.random.nextFloat() - 0.5F) * 2.0F);
+        this.bT = null;
         if (damagesource.getEntity() instanceof EntityHuman || damagesource == DamageSource.EXPLOSION) {
             this.dealDamage(damagesource, i);
         }
@@ -429,32 +439,46 @@ public class EntityEnderDragon extends EntityComplex {
         return true;
     }
 
-    protected void aI() {
-        ++this.bG;
-        if (this.bG >= 180 && this.bG <= 200) {
+    public boolean damageEntity(DamageSource damagesource, int i) {
+        return false;
+    }
+
+    protected boolean dealDamage(DamageSource damagesource, int i) {
+        return super.damageEntity(damagesource, i);
+    }
+
+    protected void aO() {
+        ++this.bR;
+        if (this.bR >= 180 && this.bR <= 200) {
             float f = (this.random.nextFloat() - 0.5F) * 8.0F;
             float f1 = (this.random.nextFloat() - 0.5F) * 4.0F;
             float f2 = (this.random.nextFloat() - 0.5F) * 8.0F;
 
-            this.world.a("hugeexplosion", this.locX + (double) f, this.locY + 2.0D + (double) f1, this.locZ + (double) f2, 0.0D, 0.0D, 0.0D);
+            this.world.addParticle("hugeexplosion", this.locX + (double) f, this.locY + 2.0D + (double) f1, this.locZ + (double) f2, 0.0D, 0.0D, 0.0D);
         }
 
         int i;
         int j;
 
-        if (!this.world.isStatic && this.bG > 150 && this.bG % 5 == 0) {
-            i = 1000;
+        if (!this.world.isStatic) {
+            if (this.bR > 150 && this.bR % 5 == 0) {
+                i = 1000;
 
-            while (i > 0) {
-                j = EntityExperienceOrb.getOrbValue(i);
-                i -= j;
-                this.world.addEntity(new EntityExperienceOrb(this.world, this.locX, this.locY, this.locZ, j));
+                while (i > 0) {
+                    j = EntityExperienceOrb.getOrbValue(i);
+                    i -= j;
+                    this.world.addEntity(new EntityExperienceOrb(this.world, this.locX, this.locY, this.locZ, j));
+                }
+            }
+
+            if (this.bR == 1) {
+                this.world.e(1018, (int) this.locX, (int) this.locY, (int) this.locZ, 0);
             }
         }
 
         this.move(0.0D, 0.10000000149011612D, 0.0D);
-        this.aq = this.yaw += 20.0F;
-        if (this.bG == 200 && !this.world.isStatic) {
+        this.aw = this.yaw += 20.0F;
+        if (this.bR == 200 && !this.world.isStatic) {
             i = 2000;
 
             while (i > 0) {
@@ -463,12 +487,12 @@ public class EntityEnderDragon extends EntityComplex {
                 this.world.addEntity(new EntityExperienceOrb(this.world, this.locX, this.locY, this.locZ, j));
             }
 
-            this.a(MathHelper.floor(this.locX), MathHelper.floor(this.locZ));
+            this.c(MathHelper.floor(this.locX), MathHelper.floor(this.locZ));
             this.die();
         }
     }
 
-    private void a(int i, int j) {
+    private void c(int i, int j) {
         byte b0 = 64;
 
         BlockEnderPortal.a = true;
@@ -510,13 +534,29 @@ public class EntityEnderDragon extends EntityComplex {
         BlockEnderPortal.a = false;
     }
 
-    protected void bb() {}
+    protected void bh() {}
 
-    public Entity[] al() {
+    public Entity[] ao() {
         return this.children;
     }
 
     public boolean L() {
         return false;
     }
+
+    public World d() {
+        return this.world;
+    }
+
+    protected String aW() {
+        return "mob.enderdragon.growl";
+    }
+
+    protected String aX() {
+        return "mob.enderdragon.hit";
+    }
+
+    protected float aV() {
+        return 5.0F;
+    }
 }
