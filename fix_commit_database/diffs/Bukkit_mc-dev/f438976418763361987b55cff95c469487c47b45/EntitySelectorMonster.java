@@ -0,0 +1,10 @@
+package net.minecraft.server;
+
+final class EntitySelectorMonster implements IEntitySelector {
+
+    EntitySelectorMonster() {}
+
+    public boolean a(Entity entity) {
+        return entity instanceof IMonster;
+    }
+}
