@@ -9,8 +9,10 @@ public final class WorldSettings {
     private final WorldType e;
     private boolean f;
     private boolean g;
+    private String h;
 
     public WorldSettings(long i, EnumGamemode enumgamemode, boolean flag, boolean flag1, WorldType worldtype) {
+        this.h = "";
         this.a = i;
         this.b = enumgamemode;
         this.c = flag;
@@ -27,6 +29,11 @@ public final class WorldSettings {
         return this;
     }
 
+    public WorldSettings a(String s) {
+        this.h = s;
+        return this;
+    }
+
     public boolean c() {
         return this.g;
     }
@@ -58,4 +65,8 @@ public final class WorldSettings {
     public static EnumGamemode a(int i) {
         return EnumGamemode.a(i);
     }
+
+    public String j() {
+        return this.h;
+    }
 }
