@@ -15,18 +15,18 @@ public class PathfinderGoalMoveTowardsRestriction extends PathfinderGoal {
     }
 
     public boolean a() {
-        if (this.a.aB()) {
+        if (this.a.aH()) {
             return false;
         } else {
-            ChunkCoordinates chunkcoordinates = this.a.aC();
-            Vec3D vec3d = RandomPositionGenerator.a(this.a, 16, 7, Vec3D.a().create((double) chunkcoordinates.x, (double) chunkcoordinates.y, (double) chunkcoordinates.z));
+            ChunkCoordinates chunkcoordinates = this.a.aI();
+            Vec3D vec3d = RandomPositionGenerator.a(this.a, 16, 7, this.a.world.getVec3DPool().create((double) chunkcoordinates.x, (double) chunkcoordinates.y, (double) chunkcoordinates.z));
 
             if (vec3d == null) {
                 return false;
             } else {
-                this.b = vec3d.a;
-                this.c = vec3d.b;
-                this.d = vec3d.c;
+                this.b = vec3d.c;
+                this.c = vec3d.d;
+                this.d = vec3d.e;
                 return true;
             }
         }
@@ -36,7 +36,7 @@ public class PathfinderGoalMoveTowardsRestriction extends PathfinderGoal {
         return !this.a.getNavigation().f();
     }
 
-    public void e() {
+    public void c() {
         this.a.getNavigation().a(this.b, this.c, this.d, this.e);
     }
 }
