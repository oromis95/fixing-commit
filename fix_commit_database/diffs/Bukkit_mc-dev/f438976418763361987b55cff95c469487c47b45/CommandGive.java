@@ -6,17 +6,21 @@ public class CommandGive extends CommandAbstract {
 
     public CommandGive() {}
 
-    public String b() {
+    public String c() {
         return "give";
     }
 
+    public int a() {
+        return 2;
+    }
+
     public String a(ICommandListener icommandlistener) {
         return icommandlistener.a("commands.give.usage", new Object[0]);
     }
 
     public void b(ICommandListener icommandlistener, String[] astring) {
         if (astring.length >= 2) {
-            EntityHuman entityhuman = this.a(astring[0]);
+            EntityPlayer entityplayer = c(icommandlistener, astring[0]);
             int i = a(icommandlistener, astring[1], 1);
             int j = 1;
             int k = 0;
@@ -33,9 +37,10 @@ public class CommandGive extends CommandAbstract {
                 }
 
                 ItemStack itemstack = new ItemStack(i, j, k);
+                EntityItem entityitem = entityplayer.drop(itemstack);
 
-                entityhuman.drop(itemstack);
-                a(icommandlistener, "commands.give.success", new Object[] { Item.byId[i].i(itemstack), Integer.valueOf(i), Integer.valueOf(j), entityhuman.getLocalizedName()});
+                entityitem.pickupDelay = 0;
+                a(icommandlistener, "commands.give.success", new Object[] { Item.byId[i].i(itemstack), Integer.valueOf(i), Integer.valueOf(j), entityplayer.getLocalizedName()});
             }
         } else {
             throw new ExceptionUsage("commands.give.usage", new Object[0]);
@@ -43,20 +48,14 @@ public class CommandGive extends CommandAbstract {
     }
 
     public List a(ICommandListener icommandlistener, String[] astring) {
-        return astring.length == 1 ? a(astring, this.c()) : null;
+        return astring.length == 1 ? a(astring, this.d()) : null;
     }
 
-    protected EntityHuman a(String s) {
-        EntityPlayer entityplayer = MinecraftServer.getServer().getServerConfigurationManager().f(s);
-
-        if (entityplayer == null) {
-            throw new ExceptionPlayerNotFound();
-        } else {
-            return entityplayer;
-        }
+    protected String[] d() {
+        return MinecraftServer.getServer().getPlayers();
     }
 
-    protected String[] c() {
-        return MinecraftServer.getServer().getPlayers();
+    public boolean a(int i) {
+        return i == 0;
     }
 }
