@@ -17,7 +17,7 @@ public class BlockSandStone extends Block {
         return i == 1 ? this.textureId - 16 : (i == 0 ? this.textureId + 16 : this.textureId);
     }
 
-    protected int getDropData(int i) {
+    public int getDropData(int i) {
         return i;
     }
 }
