@@ -12,17 +12,13 @@ public class Packet21PickupSpawn extends Packet {
     public byte e;
     public byte f;
     public byte g;
-    public int h;
-    public int i;
-    public int j;
+    public ItemStack h;
 
     public Packet21PickupSpawn() {}
 
     public Packet21PickupSpawn(EntityItem entityitem) {
         this.a = entityitem.id;
-        this.h = entityitem.itemStack.id;
-        this.i = entityitem.itemStack.count;
-        this.j = entityitem.itemStack.getData();
+        this.h = entityitem.itemStack.cloneItemStack();
         this.b = MathHelper.floor(entityitem.locX * 32.0D);
         this.c = MathHelper.floor(entityitem.locY * 32.0D);
         this.d = MathHelper.floor(entityitem.locZ * 32.0D);
@@ -33,9 +29,7 @@ public class Packet21PickupSpawn extends Packet {
 
     public void a(DataInputStream datainputstream) {
         this.a = datainputstream.readInt();
-        this.h = datainputstream.readShort();
-        this.i = datainputstream.readByte();
-        this.j = datainputstream.readShort();
+        this.h = c(datainputstream);
         this.b = datainputstream.readInt();
         this.c = datainputstream.readInt();
         this.d = datainputstream.readInt();
@@ -46,9 +40,7 @@ public class Packet21PickupSpawn extends Packet {
 
     public void a(DataOutputStream dataoutputstream) {
         dataoutputstream.writeInt(this.a);
-        dataoutputstream.writeShort(this.h);
-        dataoutputstream.writeByte(this.i);
-        dataoutputstream.writeShort(this.j);
+        a(this.h, dataoutputstream);
         dataoutputstream.writeInt(this.b);
         dataoutputstream.writeInt(this.c);
         dataoutputstream.writeInt(this.d);
