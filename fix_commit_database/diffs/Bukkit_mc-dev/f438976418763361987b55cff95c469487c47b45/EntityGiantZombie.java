@@ -5,8 +5,7 @@ public class EntityGiantZombie extends EntityMonster {
     public EntityGiantZombie(World world) {
         super(world);
         this.texture = "/mob/zombie.png";
-        this.bw = 0.5F;
-        this.damage = 50;
+        this.bI = 0.5F;
         this.height *= 6.0F;
         this.a(this.width * 6.0F, this.length * 6.0F);
     }
@@ -18,4 +17,8 @@ public class EntityGiantZombie extends EntityMonster {
     public float a(int i, int j, int k) {
         return this.world.o(i, j, k) - 0.5F;
     }
+
+    public int c(Entity entity) {
+        return 50;
+    }
 }
