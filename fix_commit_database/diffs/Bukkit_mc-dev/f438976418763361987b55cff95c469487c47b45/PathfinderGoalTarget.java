@@ -25,7 +25,7 @@ public abstract class PathfinderGoalTarget extends PathfinderGoal {
     }
 
     public boolean b() {
-        EntityLiving entityliving = this.d.az();
+        EntityLiving entityliving = this.d.aF();
 
         if (entityliving == null) {
             return false;
@@ -35,7 +35,7 @@ public abstract class PathfinderGoalTarget extends PathfinderGoal {
             return false;
         } else {
             if (this.f) {
-                if (this.d.at().canSee(entityliving)) {
+                if (this.d.az().canSee(entityliving)) {
                     this.g = 0;
                 } else if (++this.g > 60) {
                     return false;
@@ -46,13 +46,13 @@ public abstract class PathfinderGoalTarget extends PathfinderGoal {
         }
     }
 
-    public void e() {
+    public void c() {
         this.b = 0;
         this.c = 0;
         this.g = 0;
     }
 
-    public void c() {
+    public void d() {
         this.d.b((EntityLiving) null);
     }
 
@@ -63,51 +63,47 @@ public abstract class PathfinderGoalTarget extends PathfinderGoal {
             return false;
         } else if (!entityliving.isAlive()) {
             return false;
-        } else if (entityliving.boundingBox.e > this.d.boundingBox.b && entityliving.boundingBox.b < this.d.boundingBox.e) {
-            if (!this.d.a(entityliving.getClass())) {
+        } else if (!this.d.a(entityliving.getClass())) {
+            return false;
+        } else {
+            if (this.d instanceof EntityTameableAnimal && ((EntityTameableAnimal) this.d).isTamed()) {
+                if (entityliving instanceof EntityTameableAnimal && ((EntityTameableAnimal) entityliving).isTamed()) {
+                    return false;
+                }
+
+                if (entityliving == ((EntityTameableAnimal) this.d).getOwner()) {
+                    return false;
+                }
+            } else if (entityliving instanceof EntityHuman && !flag && ((EntityHuman) entityliving).abilities.isInvulnerable) {
+                return false;
+            }
+
+            if (!this.d.e(MathHelper.floor(entityliving.locX), MathHelper.floor(entityliving.locY), MathHelper.floor(entityliving.locZ))) {
+                return false;
+            } else if (this.f && !this.d.az().canSee(entityliving)) {
                 return false;
             } else {
-                if (this.d instanceof EntityTameableAnimal && ((EntityTameableAnimal) this.d).isTamed()) {
-                    if (entityliving instanceof EntityTameableAnimal && ((EntityTameableAnimal) entityliving).isTamed()) {
-                        return false;
+                if (this.a) {
+                    if (--this.c <= 0) {
+                        this.b = 0;
                     }
 
-                    if (entityliving == ((EntityTameableAnimal) this.d).getOwner()) {
-                        return false;
+                    if (this.b == 0) {
+                        this.b = this.a(entityliving) ? 1 : 2;
                     }
-                } else if (entityliving instanceof EntityHuman && !flag && ((EntityHuman) entityliving).abilities.isInvulnerable) {
-                    return false;
-                }
 
-                if (!this.d.d(MathHelper.floor(entityliving.locX), MathHelper.floor(entityliving.locY), MathHelper.floor(entityliving.locZ))) {
-                    return false;
-                } else if (this.f && !this.d.at().canSee(entityliving)) {
-                    return false;
-                } else {
-                    if (this.a) {
-                        if (--this.c <= 0) {
-                            this.b = 0;
-                        }
-
-                        if (this.b == 0) {
-                            this.b = this.a(entityliving) ? 1 : 2;
-                        }
-
-                        if (this.b == 2) {
-                            return false;
-                        }
+                    if (this.b == 2) {
+                        return false;
                     }
-
-                    return true;
                 }
+
+                return true;
             }
-        } else {
-            return false;
         }
     }
 
     private boolean a(EntityLiving entityliving) {
-        this.c = 10 + this.d.au().nextInt(5);
+        this.c = 10 + this.d.aA().nextInt(5);
         PathEntity pathentity = this.d.getNavigation().a(entityliving);
 
         if (pathentity == null) {
