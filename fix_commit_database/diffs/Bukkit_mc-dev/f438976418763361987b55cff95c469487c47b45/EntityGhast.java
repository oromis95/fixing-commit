@@ -2,21 +2,21 @@ package net.minecraft.server;
 
 public class EntityGhast extends EntityFlying implements IMonster {
 
-    public int a = 0;
-    public double b;
+    public int b = 0;
     public double c;
     public double d;
+    public double e;
     private Entity target = null;
-    private int h = 0;
-    public int e = 0;
+    private int i = 0;
     public int f = 0;
+    public int g = 0;
 
     public EntityGhast(World world) {
         super(world);
         this.texture = "/mob/ghast.png";
         this.a(4.0F, 4.0F);
         this.fireProof = true;
-        this.aV = 5;
+        this.bc = 5;
     }
 
     public boolean damageEntity(DamageSource damagesource, int i) {
@@ -38,42 +38,42 @@ public class EntityGhast extends EntityFlying implements IMonster {
         return 10;
     }
 
-    public void h_() {
-        super.h_();
+    public void j_() {
+        super.j_();
         byte b0 = this.datawatcher.getByte(16);
 
         this.texture = b0 == 1 ? "/mob/ghast_fire.png" : "/mob/ghast.png";
     }
 
-    protected void be() {
+    protected void bk() {
         if (!this.world.isStatic && this.world.difficulty == 0) {
             this.die();
         }
 
-        this.bb();
-        this.e = this.f;
-        double d0 = this.b - this.locX;
-        double d1 = this.c - this.locY;
-        double d2 = this.d - this.locZ;
+        this.bh();
+        this.f = this.g;
+        double d0 = this.c - this.locX;
+        double d1 = this.d - this.locY;
+        double d2 = this.e - this.locZ;
         double d3 = d0 * d0 + d1 * d1 + d2 * d2;
 
         if (d3 < 1.0D || d3 > 3600.0D) {
-            this.b = this.locX + (double) ((this.random.nextFloat() * 2.0F - 1.0F) * 16.0F);
-            this.c = this.locY + (double) ((this.random.nextFloat() * 2.0F - 1.0F) * 16.0F);
-            this.d = this.locZ + (double) ((this.random.nextFloat() * 2.0F - 1.0F) * 16.0F);
+            this.c = this.locX + (double) ((this.random.nextFloat() * 2.0F - 1.0F) * 16.0F);
+            this.d = this.locY + (double) ((this.random.nextFloat() * 2.0F - 1.0F) * 16.0F);
+            this.e = this.locZ + (double) ((this.random.nextFloat() * 2.0F - 1.0F) * 16.0F);
         }
 
-        if (this.a-- <= 0) {
-            this.a += this.random.nextInt(5) + 2;
+        if (this.b-- <= 0) {
+            this.b += this.random.nextInt(5) + 2;
             d3 = (double) MathHelper.sqrt(d3);
-            if (this.a(this.b, this.c, this.d, d3)) {
+            if (this.a(this.c, this.d, this.e, d3)) {
                 this.motX += d0 / d3 * 0.1D;
                 this.motY += d1 / d3 * 0.1D;
                 this.motZ += d2 / d3 * 0.1D;
             } else {
-                this.b = this.locX;
-                this.c = this.locY;
-                this.d = this.locZ;
+                this.c = this.locX;
+                this.d = this.locY;
+                this.e = this.locZ;
             }
         }
 
@@ -81,10 +81,10 @@ public class EntityGhast extends EntityFlying implements IMonster {
             this.target = null;
         }
 
-        if (this.target == null || this.h-- <= 0) {
+        if (this.target == null || this.i-- <= 0) {
             this.target = this.world.findNearbyVulnerablePlayer(this, 100.0D);
             if (this.target != null) {
-                this.h = 20;
+                this.i = 20;
             }
         }
 
@@ -95,38 +95,38 @@ public class EntityGhast extends EntityFlying implements IMonster {
             double d6 = this.target.boundingBox.b + (double) (this.target.length / 2.0F) - (this.locY + (double) (this.length / 2.0F));
             double d7 = this.target.locZ - this.locZ;
 
-            this.aq = this.yaw = -((float) Math.atan2(d5, d7)) * 180.0F / 3.1415927F;
-            if (this.l(this.target)) {
-                if (this.f == 10) {
+            this.aw = this.yaw = -((float) Math.atan2(d5, d7)) * 180.0F / 3.1415927F;
+            if (this.m(this.target)) {
+                if (this.g == 10) {
                     this.world.a((EntityHuman) null, 1007, (int) this.locX, (int) this.locY, (int) this.locZ, 0);
                 }
 
-                ++this.f;
-                if (this.f == 20) {
+                ++this.g;
+                if (this.g == 20) {
                     this.world.a((EntityHuman) null, 1008, (int) this.locX, (int) this.locY, (int) this.locZ, 0);
-                    EntityFireball entityfireball = new EntityFireball(this.world, this, d5, d6, d7);
+                    EntityLargeFireball entitylargefireball = new EntityLargeFireball(this.world, this, d5, d6, d7);
                     double d8 = 4.0D;
                     Vec3D vec3d = this.i(1.0F);
 
-                    entityfireball.locX = this.locX + vec3d.a * d8;
-                    entityfireball.locY = this.locY + (double) (this.length / 2.0F) + 0.5D;
-                    entityfireball.locZ = this.locZ + vec3d.c * d8;
-                    this.world.addEntity(entityfireball);
-                    this.f = -40;
+                    entitylargefireball.locX = this.locX + vec3d.c * d8;
+                    entitylargefireball.locY = this.locY + (double) (this.length / 2.0F) + 0.5D;
+                    entitylargefireball.locZ = this.locZ + vec3d.e * d8;
+                    this.world.addEntity(entitylargefireball);
+                    this.g = -40;
                 }
-            } else if (this.f > 0) {
-                --this.f;
+            } else if (this.g > 0) {
+                --this.g;
             }
         } else {
-            this.aq = this.yaw = -((float) Math.atan2(this.motX, this.motZ)) * 180.0F / 3.1415927F;
-            if (this.f > 0) {
-                --this.f;
+            this.aw = this.yaw = -((float) Math.atan2(this.motX, this.motZ)) * 180.0F / 3.1415927F;
+            if (this.g > 0) {
+                --this.g;
             }
         }
 
         if (!this.world.isStatic) {
             byte b0 = this.datawatcher.getByte(16);
-            byte b1 = (byte) (this.f > 10 ? 1 : 0);
+            byte b1 = (byte) (this.g > 10 ? 1 : 0);
 
             if (b0 != b1) {
                 this.datawatcher.watch(16, Byte.valueOf(b1));
@@ -135,9 +135,9 @@ public class EntityGhast extends EntityFlying implements IMonster {
     }
 
     private boolean a(double d0, double d1, double d2, double d3) {
-        double d4 = (this.b - this.locX) / d3;
-        double d5 = (this.c - this.locY) / d3;
-        double d6 = (this.d - this.locZ) / d3;
+        double d4 = (this.c - this.locX) / d3;
+        double d5 = (this.d - this.locY) / d3;
+        double d6 = (this.e - this.locZ) / d3;
         AxisAlignedBB axisalignedbb = this.boundingBox.clone();
 
         for (int i = 1; (double) i < d3; ++i) {
@@ -150,15 +150,15 @@ public class EntityGhast extends EntityFlying implements IMonster {
         return true;
     }
 
-    protected String aQ() {
+    protected String aW() {
         return "mob.ghast.moan";
     }
 
-    protected String aR() {
+    protected String aX() {
         return "mob.ghast.scream";
     }
 
-    protected String aS() {
+    protected String aY() {
         return "mob.ghast.death";
     }
 
@@ -182,7 +182,7 @@ public class EntityGhast extends EntityFlying implements IMonster {
         }
     }
 
-    protected float aP() {
+    protected float aV() {
         return 10.0F;
     }
 
@@ -190,7 +190,7 @@ public class EntityGhast extends EntityFlying implements IMonster {
         return this.random.nextInt(20) == 0 && super.canSpawn() && this.world.difficulty > 0;
     }
 
-    public int bl() {
+    public int bs() {
         return 1;
     }
 }
