@@ -14,49 +14,52 @@ public class Block {
     public static final StepSound i = new StepSound("stone", 1.0F, 1.5F);
     public static final StepSound j = new StepSoundStone("stone", 1.0F, 1.0F);
     public static final StepSound k = new StepSound("cloth", 1.0F, 1.0F);
-    public static final StepSound l = new StepSoundSand("sand", 1.0F, 1.0F);
+    public static final StepSound l = new StepSound("sand", 1.0F, 1.0F);
+    public static final StepSound m = new StepSound("snow", 1.0F, 1.0F);
+    public static final StepSound n = new StepSoundLadder("ladder", 1.0F, 1.0F);
+    public static final StepSound o = new StepSoundAnvil("anvil", 0.3F, 1.0F);
     public static final Block[] byId = new Block[4096];
-    public static final boolean[] n = new boolean[4096];
+    public static final boolean[] q = new boolean[4096];
     public static final int[] lightBlock = new int[4096];
-    public static final boolean[] p = new boolean[4096];
+    public static final boolean[] s = new boolean[4096];
     public static final int[] lightEmission = new int[4096];
-    public static final boolean[] r = new boolean[4096];
-    public static boolean[] s = new boolean[4096];
+    public static final boolean[] u = new boolean[4096];
+    public static boolean[] v = new boolean[4096];
     public static final Block STONE = (new BlockStone(1, 1)).c(1.5F).b(10.0F).a(h).b("stone");
     public static final BlockGrass GRASS = (BlockGrass) (new BlockGrass(2)).c(0.6F).a(g).b("grass");
     public static final Block DIRT = (new BlockDirt(3, 2)).c(0.5F).a(f).b("dirt");
     public static final Block COBBLESTONE = (new Block(4, 16, Material.STONE)).c(2.0F).b(10.0F).a(h).b("stonebrick").a(CreativeModeTab.b);
-    public static final Block WOOD = (new BlockWood(5)).c(2.0F).b(5.0F).a(e).b("wood").p();
-    public static final Block SAPLING = (new BlockSapling(6, 15)).c(0.0F).a(g).b("sapling").p();
-    public static final Block BEDROCK = (new Block(7, 17, Material.STONE)).q().b(6000000.0F).a(h).b("bedrock").v().a(CreativeModeTab.b);
-    public static final Block WATER = (new BlockFlowing(8, Material.WATER)).c(100.0F).h(3).b("water").v().p();
-    public static final Block STATIONARY_WATER = (new BlockStationary(9, Material.WATER)).c(100.0F).h(3).b("water").v().p();
-    public static final Block LAVA = (new BlockFlowing(10, Material.LAVA)).c(0.0F).a(1.0F).h(255).b("lava").v().p();
-    public static final Block STATIONARY_LAVA = (new BlockStationary(11, Material.LAVA)).c(100.0F).a(1.0F).h(255).b("lava").v().p();
+    public static final Block WOOD = (new BlockWood(5)).c(2.0F).b(5.0F).a(e).b("wood").r();
+    public static final Block SAPLING = (new BlockSapling(6, 15)).c(0.0F).a(g).b("sapling").r();
+    public static final Block BEDROCK = (new Block(7, 17, Material.STONE)).s().b(6000000.0F).a(h).b("bedrock").D().a(CreativeModeTab.b);
+    public static final Block WATER = (new BlockFlowing(8, Material.WATER)).c(100.0F).h(3).b("water").D().r();
+    public static final Block STATIONARY_WATER = (new BlockStationary(9, Material.WATER)).c(100.0F).h(3).b("water").D().r();
+    public static final Block LAVA = (new BlockFlowing(10, Material.LAVA)).c(0.0F).a(1.0F).h(255).b("lava").D().r();
+    public static final Block STATIONARY_LAVA = (new BlockStationary(11, Material.LAVA)).c(100.0F).a(1.0F).h(255).b("lava").D().r();
     public static final Block SAND = (new BlockSand(12, 18)).c(0.5F).a(l).b("sand");
     public static final Block GRAVEL = (new BlockGravel(13, 19)).c(0.6F).a(f).b("gravel");
     public static final Block GOLD_ORE = (new BlockOre(14, 32)).c(3.0F).b(5.0F).a(h).b("oreGold");
     public static final Block IRON_ORE = (new BlockOre(15, 33)).c(3.0F).b(5.0F).a(h).b("oreIron");
     public static final Block COAL_ORE = (new BlockOre(16, 34)).c(3.0F).b(5.0F).a(h).b("oreCoal");
-    public static final Block LOG = (new BlockLog(17)).c(2.0F).a(e).b("log").p();
-    public static final BlockLeaves LEAVES = (BlockLeaves) (new BlockLeaves(18, 52)).c(0.2F).h(1).a(g).b("leaves").p();
+    public static final Block LOG = (new BlockLog(17)).c(2.0F).a(e).b("log").r();
+    public static final BlockLeaves LEAVES = (BlockLeaves) (new BlockLeaves(18, 52)).c(0.2F).h(1).a(g).b("leaves").r();
     public static final Block SPONGE = (new BlockSponge(19)).c(0.6F).a(g).b("sponge");
     public static final Block GLASS = (new BlockGlass(20, 49, Material.SHATTERABLE, false)).c(0.3F).a(j).b("glass");
     public static final Block LAPIS_ORE = (new BlockOre(21, 160)).c(3.0F).b(5.0F).a(h).b("oreLapis");
     public static final Block LAPIS_BLOCK = (new Block(22, 144, Material.STONE)).c(3.0F).b(5.0F).a(h).b("blockLapis").a(CreativeModeTab.b);
-    public static final Block DISPENSER = (new BlockDispenser(23)).c(3.5F).a(h).b("dispenser").p();
-    public static final Block SANDSTONE = (new BlockSandStone(24)).a(h).c(0.8F).b("sandStone").p();
-    public static final Block NOTE_BLOCK = (new BlockNote(25)).c(0.8F).b("musicBlock").p();
-    public static final Block BED = (new BlockBed(26)).c(0.2F).b("bed").v().p();
-    public static final Block GOLDEN_RAIL = (new BlockMinecartTrack(27, 179, true)).c(0.7F).a(i).b("goldenRail").p();
-    public static final Block DETECTOR_RAIL = (new BlockMinecartDetector(28, 195)).c(0.7F).a(i).b("detectorRail").p();
-    public static final Block PISTON_STICKY = (new BlockPiston(29, 106, true)).b("pistonStickyBase").p();
+    public static final Block DISPENSER = (new BlockDispenser(23)).c(3.5F).a(h).b("dispenser").r();
+    public static final Block SANDSTONE = (new BlockSandStone(24)).a(h).c(0.8F).b("sandStone").r();
+    public static final Block NOTE_BLOCK = (new BlockNote(25)).c(0.8F).b("musicBlock").r();
+    public static final Block BED = (new BlockBed(26)).c(0.2F).b("bed").D().r();
+    public static final Block GOLDEN_RAIL = (new BlockMinecartTrack(27, 179, true)).c(0.7F).a(i).b("goldenRail").r();
+    public static final Block DETECTOR_RAIL = (new BlockMinecartDetector(28, 195)).c(0.7F).a(i).b("detectorRail").r();
+    public static final Block PISTON_STICKY = (new BlockPiston(29, 106, true)).b("pistonStickyBase").r();
     public static final Block WEB = (new BlockWeb(30, 11)).h(1).c(4.0F).b("web");
     public static final BlockLongGrass LONG_GRASS = (BlockLongGrass) (new BlockLongGrass(31, 39)).c(0.0F).a(g).b("tallgrass");
     public static final BlockDeadBush DEAD_BUSH = (BlockDeadBush) (new BlockDeadBush(32, 55)).c(0.0F).a(g).b("deadbush");
-    public static final Block PISTON = (new BlockPiston(33, 107, false)).b("pistonBase").p();
-    public static final BlockPistonExtension PISTON_EXTENSION = (BlockPistonExtension) (new BlockPistonExtension(34, 107)).p();
-    public static final Block WOOL = (new BlockCloth()).c(0.8F).a(k).b("cloth").p();
+    public static final Block PISTON = (new BlockPiston(33, 107, false)).b("pistonBase").r();
+    public static final BlockPistonExtension PISTON_EXTENSION = (BlockPistonExtension) (new BlockPistonExtension(34, 107)).r();
+    public static final Block WOOL = (new BlockCloth()).c(0.8F).a(k).b("cloth").r();
     public static final BlockPistonMoving PISTON_MOVING = new BlockPistonMoving(36);
     public static final BlockFlower YELLOW_FLOWER = (BlockFlower) (new BlockFlower(37, 13)).c(0.0F).a(g).b("flower");
     public static final BlockFlower RED_ROSE = (BlockFlower) (new BlockFlower(38, 12)).c(0.0F).a(g).b("rose");
@@ -71,118 +74,127 @@ public class Block {
     public static final Block BOOKSHELF = (new BlockBookshelf(47, 35)).c(1.5F).a(e).b("bookshelf");
     public static final Block MOSSY_COBBLESTONE = (new Block(48, 36, Material.STONE)).c(2.0F).b(10.0F).a(h).b("stoneMoss").a(CreativeModeTab.b);
     public static final Block OBSIDIAN = (new BlockObsidian(49, 37)).c(50.0F).b(2000.0F).a(h).b("obsidian");
-    public static final Block TORCH = (new BlockTorch(50, 80)).c(0.0F).a(0.9375F).a(e).b("torch").p();
-    public static final BlockFire FIRE = (BlockFire) (new BlockFire(51, 31)).c(0.0F).a(1.0F).a(e).b("fire").v();
-    public static final Block MOB_SPAWNER = (new BlockMobSpawner(52, 65)).c(5.0F).a(i).b("mobSpawner").v();
-    public static final Block WOOD_STAIRS = (new BlockStairs(53, WOOD, 0)).b("stairsWood").p();
-    public static final Block CHEST = (new BlockChest(54)).c(2.5F).a(e).b("chest").p();
-    public static final Block REDSTONE_WIRE = (new BlockRedstoneWire(55, 164)).c(0.0F).a(d).b("redstoneDust").v().p();
+    public static final Block TORCH = (new BlockTorch(50, 80)).c(0.0F).a(0.9375F).a(e).b("torch").r();
+    public static final BlockFire FIRE = (BlockFire) (new BlockFire(51, 31)).c(0.0F).a(1.0F).a(e).b("fire").D();
+    public static final Block MOB_SPAWNER = (new BlockMobSpawner(52, 65)).c(5.0F).a(i).b("mobSpawner").D();
+    public static final Block WOOD_STAIRS = (new BlockStairs(53, WOOD, 0)).b("stairsWood").r();
+    public static final Block CHEST = (new BlockChest(54)).c(2.5F).a(e).b("chest").r();
+    public static final Block REDSTONE_WIRE = (new BlockRedstoneWire(55, 164)).c(0.0F).a(d).b("redstoneDust").D().r();
     public static final Block DIAMOND_ORE = (new BlockOre(56, 50)).c(3.0F).b(5.0F).a(h).b("oreDiamond");
     public static final Block DIAMOND_BLOCK = (new BlockOreBlock(57, 24)).c(5.0F).b(10.0F).a(i).b("blockDiamond");
     public static final Block WORKBENCH = (new BlockWorkbench(58)).c(2.5F).a(e).b("workbench");
-    public static final Block CROPS = (new BlockCrops(59, 88)).c(0.0F).a(g).b("crops").v().p();
-    public static final Block SOIL = (new BlockSoil(60)).c(0.6F).a(f).b("farmland").p();
-    public static final Block FURNACE = (new BlockFurnace(61, false)).c(3.5F).a(h).b("furnace").p().a(CreativeModeTab.c);
-    public static final Block BURNING_FURNACE = (new BlockFurnace(62, true)).c(3.5F).a(h).a(0.875F).b("furnace").p();
-    public static final Block SIGN_POST = (new BlockSign(63, TileEntitySign.class, true)).c(1.0F).a(e).b("sign").v().p();
-    public static final Block WOODEN_DOOR = (new BlockDoor(64, Material.WOOD)).c(3.0F).a(e).b("doorWood").v().p();
-    public static final Block LADDER = (new BlockLadder(65, 83)).c(0.4F).a(e).b("ladder").p();
-    public static final Block RAILS = (new BlockMinecartTrack(66, 128, false)).c(0.7F).a(i).b("rail").p();
-    public static final Block COBBLESTONE_STAIRS = (new BlockStairs(67, COBBLESTONE, 0)).b("stairsStone").p();
-    public static final Block WALL_SIGN = (new BlockSign(68, TileEntitySign.class, false)).c(1.0F).a(e).b("sign").v().p();
-    public static final Block LEVER = (new BlockLever(69, 96)).c(0.5F).a(e).b("lever").p();
-    public static final Block STONE_PLATE = (new BlockPressurePlate(70, STONE.textureId, EnumMobType.MOBS, Material.STONE)).c(0.5F).a(h).b("pressurePlate").p();
-    public static final Block IRON_DOOR_BLOCK = (new BlockDoor(71, Material.ORE)).c(5.0F).a(i).b("doorIron").v().p();
-    public static final Block WOOD_PLATE = (new BlockPressurePlate(72, WOOD.textureId, EnumMobType.EVERYTHING, Material.WOOD)).c(0.5F).a(e).b("pressurePlate").p();
-    public static final Block REDSTONE_ORE = (new BlockRedstoneOre(73, 51, false)).c(3.0F).b(5.0F).a(h).b("oreRedstone").p().a(CreativeModeTab.b);
-    public static final Block GLOWING_REDSTONE_ORE = (new BlockRedstoneOre(74, 51, true)).a(0.625F).c(3.0F).b(5.0F).a(h).b("oreRedstone").p();
-    public static final Block REDSTONE_TORCH_OFF = (new BlockRedstoneTorch(75, 115, false)).c(0.0F).a(e).b("notGate").p();
-    public static final Block REDSTONE_TORCH_ON = (new BlockRedstoneTorch(76, 99, true)).c(0.0F).a(0.5F).a(e).b("notGate").p().a(CreativeModeTab.d);
-    public static final Block STONE_BUTTON = (new BlockButton(77, STONE.textureId)).c(0.5F).a(h).b("button").p();
-    public static final Block SNOW = (new BlockSnow(78, 66)).c(0.1F).a(k).b("snow").p().h(0);
+    public static final Block CROPS = (new BlockCrops(59, 88)).b("crops");
+    public static final Block SOIL = (new BlockSoil(60)).c(0.6F).a(f).b("farmland").r();
+    public static final Block FURNACE = (new BlockFurnace(61, false)).c(3.5F).a(h).b("furnace").r().a(CreativeModeTab.c);
+    public static final Block BURNING_FURNACE = (new BlockFurnace(62, true)).c(3.5F).a(h).a(0.875F).b("furnace").r();
+    public static final Block SIGN_POST = (new BlockSign(63, TileEntitySign.class, true)).c(1.0F).a(e).b("sign").D().r();
+    public static final Block WOODEN_DOOR = (new BlockDoor(64, Material.WOOD)).c(3.0F).a(e).b("doorWood").D().r();
+    public static final Block LADDER = (new BlockLadder(65, 83)).c(0.4F).a(n).b("ladder").r();
+    public static final Block RAILS = (new BlockMinecartTrack(66, 128, false)).c(0.7F).a(i).b("rail").r();
+    public static final Block COBBLESTONE_STAIRS = (new BlockStairs(67, COBBLESTONE, 0)).b("stairsStone").r();
+    public static final Block WALL_SIGN = (new BlockSign(68, TileEntitySign.class, false)).c(1.0F).a(e).b("sign").D().r();
+    public static final Block LEVER = (new BlockLever(69, 96)).c(0.5F).a(e).b("lever").r();
+    public static final Block STONE_PLATE = (new BlockPressurePlate(70, STONE.textureId, EnumMobType.MOBS, Material.STONE)).c(0.5F).a(h).b("pressurePlate").r();
+    public static final Block IRON_DOOR_BLOCK = (new BlockDoor(71, Material.ORE)).c(5.0F).a(i).b("doorIron").D().r();
+    public static final Block WOOD_PLATE = (new BlockPressurePlate(72, WOOD.textureId, EnumMobType.EVERYTHING, Material.WOOD)).c(0.5F).a(e).b("pressurePlate").r();
+    public static final Block REDSTONE_ORE = (new BlockRedstoneOre(73, 51, false)).c(3.0F).b(5.0F).a(h).b("oreRedstone").r().a(CreativeModeTab.b);
+    public static final Block GLOWING_REDSTONE_ORE = (new BlockRedstoneOre(74, 51, true)).a(0.625F).c(3.0F).b(5.0F).a(h).b("oreRedstone").r();
+    public static final Block REDSTONE_TORCH_OFF = (new BlockRedstoneTorch(75, 115, false)).c(0.0F).a(e).b("notGate").r();
+    public static final Block REDSTONE_TORCH_ON = (new BlockRedstoneTorch(76, 99, true)).c(0.0F).a(0.5F).a(e).b("notGate").r().a(CreativeModeTab.d);
+    public static final Block STONE_BUTTON = (new BlockButton(77, STONE.textureId, false)).c(0.5F).a(h).b("button").r();
+    public static final Block SNOW = (new BlockSnow(78, 66)).c(0.1F).a(m).b("snow").r().h(0);
     public static final Block ICE = (new BlockIce(79, 67)).c(0.5F).h(3).a(j).b("ice");
-    public static final Block SNOW_BLOCK = (new BlockSnowBlock(80, 66)).c(0.2F).a(k).b("snow");
+    public static final Block SNOW_BLOCK = (new BlockSnowBlock(80, 66)).c(0.2F).a(m).b("snow");
     public static final Block CACTUS = (new BlockCactus(81, 70)).c(0.4F).a(k).b("cactus");
     public static final Block CLAY = (new BlockClay(82, 72)).c(0.6F).a(f).b("clay");
-    public static final Block SUGAR_CANE_BLOCK = (new BlockReed(83, 73)).c(0.0F).a(g).b("reeds").v();
-    public static final Block JUKEBOX = (new BlockJukeBox(84, 74)).c(2.0F).b(10.0F).a(h).b("jukebox").p();
+    public static final Block SUGAR_CANE_BLOCK = (new BlockReed(83, 73)).c(0.0F).a(g).b("reeds").D();
+    public static final Block JUKEBOX = (new BlockJukeBox(84, 74)).c(2.0F).b(10.0F).a(h).b("jukebox").r();
     public static final Block FENCE = (new BlockFence(85, 4)).c(2.0F).b(5.0F).a(e).b("fence");
-    public static final Block PUMPKIN = (new BlockPumpkin(86, 102, false)).c(1.0F).a(e).b("pumpkin").p();
+    public static final Block PUMPKIN = (new BlockPumpkin(86, 102, false)).c(1.0F).a(e).b("pumpkin").r();
     public static final Block NETHERRACK = (new BlockBloodStone(87, 103)).c(0.4F).a(h).b("hellrock");
     public static final Block SOUL_SAND = (new BlockSlowSand(88, 104)).c(0.5F).a(l).b("hellsand");
     public static final Block GLOWSTONE = (new BlockLightStone(89, 105, Material.SHATTERABLE)).c(0.3F).a(j).a(1.0F).b("lightgem");
     public static final BlockPortal PORTAL = (BlockPortal) (new BlockPortal(90, 14)).c(-1.0F).a(j).a(0.75F).b("portal");
-    public static final Block JACK_O_LANTERN = (new BlockPumpkin(91, 102, true)).c(1.0F).a(e).a(1.0F).b("litpumpkin").p();
-    public static final Block CAKE_BLOCK = (new BlockCake(92, 121)).c(0.5F).a(k).b("cake").v().p();
-    public static final Block DIODE_OFF = (new BlockDiode(93, false)).c(0.0F).a(e).b("diode").v().p();
-    public static final Block DIODE_ON = (new BlockDiode(94, true)).c(0.0F).a(0.625F).a(e).b("diode").v().p();
-    public static final Block LOCKED_CHEST = (new BlockLockedChest(95)).c(0.0F).a(1.0F).a(e).b("lockedchest").b(true).p();
-    public static final Block TRAP_DOOR = (new BlockTrapdoor(96, Material.WOOD)).c(3.0F).a(e).b("trapdoor").v().p();
+    public static final Block JACK_O_LANTERN = (new BlockPumpkin(91, 102, true)).c(1.0F).a(e).a(1.0F).b("litpumpkin").r();
+    public static final Block CAKE_BLOCK = (new BlockCake(92, 121)).c(0.5F).a(k).b("cake").D().r();
+    public static final Block DIODE_OFF = (new BlockDiode(93, false)).c(0.0F).a(e).b("diode").D().r();
+    public static final Block DIODE_ON = (new BlockDiode(94, true)).c(0.0F).a(0.625F).a(e).b("diode").D().r();
+    public static final Block LOCKED_CHEST = (new BlockLockedChest(95)).c(0.0F).a(1.0F).a(e).b("lockedchest").b(true).r();
+    public static final Block TRAP_DOOR = (new BlockTrapdoor(96, Material.WOOD)).c(3.0F).a(e).b("trapdoor").D().r();
     public static final Block MONSTER_EGGS = (new BlockMonsterEggs(97)).c(0.75F).b("monsterStoneEgg");
     public static final Block SMOOTH_BRICK = (new BlockSmoothBrick(98)).c(1.5F).b(10.0F).a(h).b("stonebricksmooth");
-    public static final Block BIG_MUSHROOM_1 = (new BlockHugeMushroom(99, Material.WOOD, 142, 0)).c(0.2F).a(e).b("mushroom").p();
-    public static final Block BIG_MUSHROOM_2 = (new BlockHugeMushroom(100, Material.WOOD, 142, 1)).c(0.2F).a(e).b("mushroom").p();
+    public static final Block BIG_MUSHROOM_1 = (new BlockHugeMushroom(99, Material.WOOD, 142, 0)).c(0.2F).a(e).b("mushroom").r();
+    public static final Block BIG_MUSHROOM_2 = (new BlockHugeMushroom(100, Material.WOOD, 142, 1)).c(0.2F).a(e).b("mushroom").r();
     public static final Block IRON_FENCE = (new BlockThinFence(101, 85, 85, Material.ORE, true)).c(5.0F).b(10.0F).a(i).b("fenceIron");
     public static final Block THIN_GLASS = (new BlockThinFence(102, 49, 148, Material.SHATTERABLE, false)).c(0.3F).a(j).b("thinGlass");
     public static final Block MELON = (new BlockMelon(103)).c(1.0F).a(e).b("melon");
-    public static final Block PUMPKIN_STEM = (new BlockStem(104, PUMPKIN)).c(0.0F).a(e).b("pumpkinStem").p();
-    public static final Block MELON_STEM = (new BlockStem(105, MELON)).c(0.0F).a(e).b("pumpkinStem").p();
-    public static final Block VINE = (new BlockVine(106)).c(0.2F).a(g).b("vine").p();
-    public static final Block FENCE_GATE = (new BlockFenceGate(107, 4)).c(2.0F).b(5.0F).a(e).b("fenceGate").p();
-    public static final Block BRICK_STAIRS = (new BlockStairs(108, BRICK, 0)).b("stairsBrick").p();
-    public static final Block STONE_STAIRS = (new BlockStairs(109, SMOOTH_BRICK, 0)).b("stairsStoneBrickSmooth").p();
+    public static final Block PUMPKIN_STEM = (new BlockStem(104, PUMPKIN)).c(0.0F).a(e).b("pumpkinStem").r();
+    public static final Block MELON_STEM = (new BlockStem(105, MELON)).c(0.0F).a(e).b("pumpkinStem").r();
+    public static final Block VINE = (new BlockVine(106)).c(0.2F).a(g).b("vine").r();
+    public static final Block FENCE_GATE = (new BlockFenceGate(107, 4)).c(2.0F).b(5.0F).a(e).b("fenceGate").r();
+    public static final Block BRICK_STAIRS = (new BlockStairs(108, BRICK, 0)).b("stairsBrick").r();
+    public static final Block STONE_STAIRS = (new BlockStairs(109, SMOOTH_BRICK, 0)).b("stairsStoneBrickSmooth").r();
     public static final BlockMycel MYCEL = (BlockMycel) (new BlockMycel(110)).c(0.6F).a(g).b("mycel");
     public static final Block WATER_LILY = (new BlockWaterLily(111, 76)).c(0.0F).a(g).b("waterlily");
     public static final Block NETHER_BRICK = (new Block(112, 224, Material.STONE)).c(2.0F).b(10.0F).a(h).b("netherBrick").a(CreativeModeTab.b);
     public static final Block NETHER_FENCE = (new BlockFence(113, 224, Material.STONE)).c(2.0F).b(10.0F).a(h).b("netherFence");
-    public static final Block NETHER_BRICK_STAIRS = (new BlockStairs(114, NETHER_BRICK, 0)).b("stairsNetherBrick").p();
-    public static final Block NETHER_WART = (new BlockNetherWart(115)).b("netherStalk").p();
+    public static final Block NETHER_BRICK_STAIRS = (new BlockStairs(114, NETHER_BRICK, 0)).b("stairsNetherBrick").r();
+    public static final Block NETHER_WART = (new BlockNetherWart(115)).b("netherStalk").r();
     public static final Block ENCHANTMENT_TABLE = (new BlockEnchantmentTable(116)).c(5.0F).b(2000.0F).b("enchantmentTable");
-    public static final Block BREWING_STAND = (new BlockBrewingStand(117)).c(0.5F).a(0.125F).b("brewingStand").p();
-    public static final Block CAULDRON = (new BlockCauldron(118)).c(2.0F).b("cauldron").p();
+    public static final Block BREWING_STAND = (new BlockBrewingStand(117)).c(0.5F).a(0.125F).b("brewingStand").r();
+    public static final Block CAULDRON = (new BlockCauldron(118)).c(2.0F).b("cauldron").r();
     public static final Block ENDER_PORTAL = (new BlockEnderPortal(119, Material.PORTAL)).c(-1.0F).b(6000000.0F);
-    public static final Block ENDER_PORTAL_FRAME = (new BlockEnderPortalFrame(120)).a(j).a(0.125F).c(-1.0F).b("endPortalFrame").p().b(6000000.0F).a(CreativeModeTab.c);
+    public static final Block ENDER_PORTAL_FRAME = (new BlockEnderPortalFrame(120)).a(j).a(0.125F).c(-1.0F).b("endPortalFrame").r().b(6000000.0F).a(CreativeModeTab.c);
     public static final Block WHITESTONE = (new Block(121, 175, Material.STONE)).c(3.0F).b(15.0F).a(h).b("whiteStone").a(CreativeModeTab.b);
     public static final Block DRAGON_EGG = (new BlockDragonEgg(122, 167)).c(3.0F).b(15.0F).a(h).a(0.125F).b("dragonEgg");
     public static final Block REDSTONE_LAMP_OFF = (new BlockRedstoneLamp(123, false)).c(0.3F).a(j).b("redstoneLight").a(CreativeModeTab.d);
     public static final Block REDSTONE_LAMP_ON = (new BlockRedstoneLamp(124, true)).c(0.3F).a(j).b("redstoneLight");
     public static final BlockStepAbstract WOOD_DOUBLE_STEP = (BlockStepAbstract) (new BlockWoodStep(125, true)).c(2.0F).b(5.0F).a(e).b("woodSlab");
     public static final BlockStepAbstract WOOD_STEP = (BlockStepAbstract) (new BlockWoodStep(126, false)).c(2.0F).b(5.0F).a(e).b("woodSlab");
-    public static final Block COCOA = (new BlockCocoa(127)).c(0.2F).b(5.0F).a(e).b("cocoa").p();
-    public static final Block SANDSTONE_STAIRS = (new BlockStairs(128, SANDSTONE, 0)).b("stairsSandStone").p();
+    public static final Block COCOA = (new BlockCocoa(127)).c(0.2F).b(5.0F).a(e).b("cocoa").r();
+    public static final Block SANDSTONE_STAIRS = (new BlockStairs(128, SANDSTONE, 0)).b("stairsSandStone").r();
     public static final Block EMERALD_ORE = (new BlockOre(129, 171)).c(3.0F).b(5.0F).a(h).b("oreEmerald");
-    public static final Block ENDER_CHEST = (new BlockEnderChest(130)).c(22.5F).b(1000.0F).a(h).b("enderChest").p().a(0.5F);
-    public static final BlockTripwireHook TRIPWIRE_SOURCE = (BlockTripwireHook) (new BlockTripwireHook(131)).b("tripWireSource").p();
-    public static final Block TRIPWIRE = (new BlockTripwire(132)).b("tripWire").p();
+    public static final Block ENDER_CHEST = (new BlockEnderChest(130)).c(22.5F).b(1000.0F).a(h).b("enderChest").r().a(0.5F);
+    public static final BlockTripwireHook TRIPWIRE_SOURCE = (BlockTripwireHook) (new BlockTripwireHook(131)).b("tripWireSource").r();
+    public static final Block TRIPWIRE = (new BlockTripwire(132)).b("tripWire").r();
     public static final Block EMERALD_BLOCK = (new BlockOreBlock(133, 25)).c(5.0F).b(10.0F).a(i).b("blockEmerald");
-    public static final Block SPRUCE_WOOD_STAIRS = (new BlockStairs(134, WOOD, 1)).b("stairsWoodSpruce").p();
-    public static final Block BIRCH_WOOD_STAIRS = (new BlockStairs(135, WOOD, 2)).b("stairsWoodBirch").p();
-    public static final Block JUNGLE_WOOD_STAIRS = (new BlockStairs(136, WOOD, 3)).b("stairsWoodJungle").p();
+    public static final Block SPRUCE_WOOD_STAIRS = (new BlockStairs(134, WOOD, 1)).b("stairsWoodSpruce").r();
+    public static final Block BIRCH_WOOD_STAIRS = (new BlockStairs(135, WOOD, 2)).b("stairsWoodBirch").r();
+    public static final Block JUNGLE_WOOD_STAIRS = (new BlockStairs(136, WOOD, 3)).b("stairsWoodJungle").r();
+    public static final Block COMMAND = (new BlockCommand(137)).b("commandBlock");
+    public static final Block BEACON = (new BlockBeacon(138)).b("beacon").a(1.0F);
+    public static final Block COBBLE_WALL = (new BlockCobbleWall(139, COBBLESTONE)).b("cobbleWall");
+    public static final Block FLOWER_POT = (new BlockFlowerPot(140)).c(0.0F).a(d).b("flowerPot");
+    public static final Block CARROTS = (new BlockCarrots(141)).b("carrots");
+    public static final Block POTATOES = (new BlockPotatoes(142)).b("potatoes");
+    public static final Block WOOD_BUTTON = (new BlockButton(143, WOOD.textureId, true)).c(0.5F).a(e).b("button").r();
+    public static final Block SKULL = (new BlockSkull(144)).c(1.0F).a(h).b("skull").r();
+    public static final Block ANVIL = (new BlockAnvil(145)).c(5.0F).a(o).b(2000.0F).b("anvil").r();
     public int textureId;
     public final int id;
     protected float strength;
     protected float durability;
-    protected boolean cd;
-    protected boolean ce;
-    protected boolean cf;
+    protected boolean cp;
+    protected boolean cq;
+    protected boolean cr;
     protected boolean isTileEntity;
-    public double minX;
-    public double minY;
-    public double minZ;
-    public double maxX;
-    public double maxY;
-    public double maxZ;
+    protected double minX;
+    protected double minY;
+    protected double minZ;
+    protected double maxX;
+    protected double maxY;
+    protected double maxZ;
     public StepSound stepSound;
-    public float co;
+    public float cA;
     public final Material material;
     public float frictionFactor;
     private String name;
 
     protected Block(int i, Material material) {
-        this.cd = true;
-        this.ce = true;
+        this.cp = true;
+        this.cq = true;
         this.stepSound = d;
-        this.co = 1.0F;
+        this.cA = 1.0F;
         this.frictionFactor = 0.6F;
         if (byId[i] != null) {
             throw new IllegalArgumentException("Slot " + i + " is already occupied by " + byId[i] + " when adding " + this);
@@ -191,18 +203,18 @@ public class Block {
             byId[i] = this;
             this.id = i;
             this.a(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
-            n[i] = this.d();
-            lightBlock[i] = this.d() ? 255 : 0;
-            p[i] = !material.blocksLight();
+            q[i] = this.c();
+            lightBlock[i] = this.c() ? 255 : 0;
+            s[i] = !material.blocksLight();
         }
     }
 
-    protected Block p() {
-        r[this.id] = true;
+    protected Block r() {
+        u[this.id] = true;
         return this;
     }
 
-    protected void r_() {}
+    protected void t_() {}
 
     protected Block(int i, int j, Material material) {
         this(i, material);
@@ -232,10 +244,10 @@ public class Block {
     public static boolean i(int i) {
         Block block = byId[i];
 
-        return block == null ? false : block.material.k() && block.c();
+        return block == null ? false : block.material.k() && block.b();
     }
 
-    public boolean c() {
+    public boolean b() {
         return true;
     }
 
@@ -243,7 +255,7 @@ public class Block {
         return !this.material.isSolid();
     }
 
-    public int b() {
+    public int d() {
         return 0;
     }
 
@@ -256,7 +268,7 @@ public class Block {
         return this;
     }
 
-    protected Block q() {
+    protected Block s() {
         this.c(-1.0F);
         return this;
     }
@@ -266,19 +278,19 @@ public class Block {
     }
 
     protected Block b(boolean flag) {
-        this.cf = flag;
+        this.cr = flag;
         return this;
     }
 
-    public boolean r() {
-        return this.cf;
+    public boolean isTicking() {
+        return this.cr;
     }
 
-    public boolean s() {
+    public boolean u() {
         return this.isTileEntity;
     }
 
-    public void a(float f, float f1, float f2, float f3, float f4, float f5) {
+    protected final void a(float f, float f1, float f2, float f3, float f4, float f5) {
         this.minX = (double) f;
         this.minY = (double) f1;
         this.minZ = (double) f2;
@@ -287,7 +299,7 @@ public class Block {
         this.maxZ = (double) f5;
     }
 
-    public boolean d(IBlockAccess iblockaccess, int i, int j, int k, int l) {
+    public boolean a_(IBlockAccess iblockaccess, int i, int j, int k, int l) {
         return iblockaccess.getMaterial(i, j, k).isBuildable();
     }
 
@@ -311,15 +323,15 @@ public class Block {
         return AxisAlignedBB.a().a((double) i + this.minX, (double) j + this.minY, (double) k + this.minZ, (double) i + this.maxX, (double) j + this.maxY, (double) k + this.maxZ);
     }
 
-    public boolean d() {
+    public boolean c() {
         return true;
     }
 
     public boolean a(int i, boolean flag) {
-        return this.l();
+        return this.m();
     }
 
-    public boolean l() {
+    public boolean m() {
         return true;
     }
 
@@ -329,7 +341,7 @@ public class Block {
 
     public void doPhysics(World world, int i, int j, int k, int l) {}
 
-    public int p_() {
+    public int r_() {
         return 10;
     }
 
@@ -372,7 +384,7 @@ public class Block {
     }
 
     protected void a(World world, int i, int j, int k, ItemStack itemstack) {
-        if (!world.isStatic) {
+        if (!world.isStatic && world.getGameRules().getBoolean("doTileDrops")) {
             float f = 0.7F;
             double d0 = (double) (world.random.nextFloat() * f) + (double) (1.0F - f) * 0.5D;
             double d1 = (double) (world.random.nextFloat() * f) + (double) (1.0F - f) * 0.5D;
@@ -384,7 +396,7 @@ public class Block {
         }
     }
 
-    protected void g(World world, int i, int j, int k, int l) {
+    protected void f(World world, int i, int j, int k, int l) {
         if (!world.isStatic) {
             while (l > 0) {
                 int i1 = EntityExperienceOrb.getOrbValue(l);
@@ -395,7 +407,7 @@ public class Block {
         }
     }
 
-    protected int getDropData(int i) {
+    public int getDropData(int i) {
         return 0;
     }
 
@@ -498,15 +510,15 @@ public class Block {
     }
 
     private boolean a(Vec3D vec3d) {
-        return vec3d == null ? false : vec3d.b >= this.minY && vec3d.b <= this.maxY && vec3d.c >= this.minZ && vec3d.c <= this.maxZ;
+        return vec3d == null ? false : vec3d.d >= this.minY && vec3d.d <= this.maxY && vec3d.e >= this.minZ && vec3d.e <= this.maxZ;
     }
 
     private boolean b(Vec3D vec3d) {
-        return vec3d == null ? false : vec3d.a >= this.minX && vec3d.a <= this.maxX && vec3d.c >= this.minZ && vec3d.c <= this.maxZ;
+        return vec3d == null ? false : vec3d.c >= this.minX && vec3d.c <= this.maxX && vec3d.e >= this.minZ && vec3d.e <= this.maxZ;
     }
 
     private boolean c(Vec3D vec3d) {
-        return vec3d == null ? false : vec3d.a >= this.minX && vec3d.a <= this.maxX && vec3d.b >= this.minY && vec3d.b <= this.maxY;
+        return vec3d == null ? false : vec3d.c >= this.minX && vec3d.c <= this.maxX && vec3d.d >= this.minY && vec3d.d <= this.maxY;
     }
 
     public void wasExploded(World world, int i, int j, int k) {}
@@ -535,7 +547,31 @@ public class Block {
 
     public void updateShape(IBlockAccess iblockaccess, int i, int j, int k) {}
 
-    public boolean a(IBlockAccess iblockaccess, int i, int j, int k, int l) {
+    public final double v() {
+        return this.minX;
+    }
+
+    public final double w() {
+        return this.maxX;
+    }
+
+    public final double x() {
+        return this.minY;
+    }
+
+    public final double y() {
+        return this.maxY;
+    }
+
+    public final double z() {
+        return this.minZ;
+    }
+
+    public final double A() {
+        return this.maxZ;
+    }
+
+    public boolean b(IBlockAccess iblockaccess, int i, int j, int k, int l) {
         return false;
     }
 
@@ -545,7 +581,7 @@ public class Block {
 
     public void a(World world, int i, int j, int k, Entity entity) {}
 
-    public boolean c(World world, int i, int j, int k, int l) {
+    public boolean c(IBlockAccess iblockaccess, int i, int j, int k, int l) {
         return false;
     }
 
@@ -554,27 +590,27 @@ public class Block {
     public void a(World world, EntityHuman entityhuman, int i, int j, int k, int l) {
         entityhuman.a(StatisticList.C[this.id], 1);
         entityhuman.j(0.025F);
-        if (this.q_() && EnchantmentManager.hasSilkTouchEnchantment(entityhuman.inventory)) {
-            ItemStack itemstack = this.c_(l);
+        if (this.s_() && EnchantmentManager.hasSilkTouchEnchantment(entityhuman)) {
+            ItemStack itemstack = this.f_(l);
 
             if (itemstack != null) {
                 this.a(world, i, j, k, itemstack);
             }
         } else {
-            int i1 = EnchantmentManager.getBonusBlockLootEnchantmentLevel(entityhuman.inventory);
+            int i1 = EnchantmentManager.getBonusBlockLootEnchantmentLevel(entityhuman);
 
             this.c(world, i, j, k, l, i1);
         }
     }
 
-    protected boolean q_() {
-        return this.c() && !this.isTileEntity;
+    protected boolean s_() {
+        return this.b() && !this.isTileEntity;
     }
 
-    protected ItemStack c_(int i) {
+    protected ItemStack f_(int i) {
         int j = 0;
 
-        if (this.id >= 0 && this.id < Item.byId.length && Item.byId[this.id].k()) {
+        if (this.id >= 0 && this.id < Item.byId.length && Item.byId[this.id].l()) {
             j = i;
         }
 
@@ -606,21 +642,25 @@ public class Block {
 
     public void b(World world, int i, int j, int k, int l, int i1) {}
 
-    public boolean u() {
-        return this.ce;
+    public boolean C() {
+        return this.cq;
     }
 
-    protected Block v() {
-        this.ce = false;
+    protected Block D() {
+        this.cq = false;
         return this;
     }
 
-    public int e() {
+    public int q_() {
         return this.material.getPushReaction();
     }
 
     public void a(World world, int i, int j, int k, Entity entity, float f) {}
 
+    public int getDropData(World world, int i, int j, int k) {
+        return this.getDropData(world.getData(i, j, k));
+    }
+
     public Block a(CreativeModeTab creativemodetab) {
         this.creativeTab = creativemodetab;
         return this;
@@ -628,41 +668,45 @@ public class Block {
 
     public void a(World world, int i, int j, int k, int l, EntityHuman entityhuman) {}
 
-    public void h(World world, int i, int j, int k, int l) {}
+    public void g(World world, int i, int j, int k, int l) {}
 
     public void f(World world, int i, int j, int k) {}
 
-    public void a(World world, long i, long j) {}
+    public boolean l() {
+        return true;
+    }
 
     static {
         Item.byId[WOOL.id] = (new ItemCloth(WOOL.id - 256)).b("cloth");
-        Item.byId[LOG.id] = (new ItemLog(LOG.id - 256, LOG)).b("log");
-        Item.byId[WOOD.id] = (new ItemWood(WOOD.id - 256, WOOD)).b("wood");
-        Item.byId[MONSTER_EGGS.id] = (new ItemMonsterEggs(MONSTER_EGGS.id - 256)).b("monsterStoneEgg");
-        Item.byId[SMOOTH_BRICK.id] = (new ItemSmoothStone(SMOOTH_BRICK.id - 256, SMOOTH_BRICK)).b("stonebricksmooth");
-        Item.byId[SANDSTONE.id] = (new ItemSandStone(SANDSTONE.id - 256, SANDSTONE)).b("sandStone");
+        Item.byId[LOG.id] = (new ItemMultiTexture(LOG.id - 256, LOG, BlockLog.a)).b("log");
+        Item.byId[WOOD.id] = (new ItemMultiTexture(WOOD.id - 256, WOOD, BlockWood.a)).b("wood");
+        Item.byId[MONSTER_EGGS.id] = (new ItemMultiTexture(MONSTER_EGGS.id - 256, MONSTER_EGGS, BlockMonsterEggs.a)).b("monsterStoneEgg");
+        Item.byId[SMOOTH_BRICK.id] = (new ItemMultiTexture(SMOOTH_BRICK.id - 256, SMOOTH_BRICK, BlockSmoothBrick.a)).b("stonebricksmooth");
+        Item.byId[SANDSTONE.id] = (new ItemMultiTexture(SANDSTONE.id - 256, SANDSTONE, BlockSandStone.a)).b("sandStone");
         Item.byId[STEP.id] = (new ItemStep(STEP.id - 256, STEP, DOUBLE_STEP, false)).b("stoneSlab");
         Item.byId[DOUBLE_STEP.id] = (new ItemStep(DOUBLE_STEP.id - 256, STEP, DOUBLE_STEP, true)).b("stoneSlab");
         Item.byId[WOOD_STEP.id] = (new ItemStep(WOOD_STEP.id - 256, WOOD_STEP, WOOD_DOUBLE_STEP, false)).b("woodSlab");
         Item.byId[WOOD_DOUBLE_STEP.id] = (new ItemStep(WOOD_DOUBLE_STEP.id - 256, WOOD_STEP, WOOD_DOUBLE_STEP, true)).b("woodSlab");
-        Item.byId[SAPLING.id] = (new ItemSapling(SAPLING.id - 256)).b("sapling");
+        Item.byId[SAPLING.id] = (new ItemMultiTexture(SAPLING.id - 256, SAPLING, BlockSapling.a)).b("sapling");
         Item.byId[LEAVES.id] = (new ItemLeaves(LEAVES.id - 256)).b("leaves");
         Item.byId[VINE.id] = new ItemWithAuxData(VINE.id - 256, false);
         Item.byId[LONG_GRASS.id] = (new ItemWithAuxData(LONG_GRASS.id - 256, true)).a(new String[] { "shrub", "grass", "fern"});
         Item.byId[WATER_LILY.id] = new ItemWaterLily(WATER_LILY.id - 256);
         Item.byId[PISTON.id] = new ItemPiston(PISTON.id - 256);
         Item.byId[PISTON_STICKY.id] = new ItemPiston(PISTON_STICKY.id - 256);
+        Item.byId[COBBLE_WALL.id] = (new ItemMultiTexture(COBBLE_WALL.id - 256, COBBLE_WALL, BlockCobbleWall.a)).b("cobbleWall");
+        Item.byId[ANVIL.id] = (new ItemAnvil(ANVIL)).b("anvil");
 
         for (int i = 0; i < 256; ++i) {
             if (byId[i] != null) {
                 if (Item.byId[i] == null) {
                     Item.byId[i] = new ItemBlock(i - 256);
-                    byId[i].r_();
+                    byId[i].t_();
                 }
 
                 boolean flag = false;
 
-                if (i > 0 && byId[i].b() == 10) {
+                if (i > 0 && byId[i].d() == 10) {
                     flag = true;
                 }
 
@@ -674,7 +718,7 @@ public class Block {
                     flag = true;
                 }
 
-                if (p[i]) {
+                if (s[i]) {
                     flag = true;
                 }
 
@@ -682,11 +726,11 @@ public class Block {
                     flag = true;
                 }
 
-                s[i] = flag;
+                v[i] = flag;
             }
         }
 
-        p[0] = true;
+        s[0] = true;
         StatisticList.b();
     }
 }
