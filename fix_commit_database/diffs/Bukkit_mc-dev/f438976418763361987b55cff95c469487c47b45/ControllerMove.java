@@ -46,7 +46,7 @@ public class ControllerMove {
                 float f = (float) (Math.atan2(d1, d0) * 180.0D / 3.1415927410125732D) - 90.0F;
 
                 this.a.yaw = this.a(this.a.yaw, f, 30.0F);
-                this.a.e(this.e);
+                this.a.e(this.e * this.a.by());
                 if (d2 > 0.0D && d0 * d0 + d1 * d1 < 1.0D) {
                     this.a.getControllerJump().a();
                 }
