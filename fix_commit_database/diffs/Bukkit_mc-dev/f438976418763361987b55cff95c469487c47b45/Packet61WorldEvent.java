@@ -10,15 +10,17 @@ public class Packet61WorldEvent extends Packet {
     public int c;
     public int d;
     public int e;
+    private boolean f;
 
     public Packet61WorldEvent() {}
 
-    public Packet61WorldEvent(int i, int j, int k, int l, int i1) {
+    public Packet61WorldEvent(int i, int j, int k, int l, int i1, boolean flag) {
         this.a = i;
         this.c = j;
         this.d = k;
         this.e = l;
         this.b = i1;
+        this.f = flag;
     }
 
     public void a(DataInputStream datainputstream) {
@@ -27,6 +29,7 @@ public class Packet61WorldEvent extends Packet {
         this.d = datainputstream.readByte() & 255;
         this.e = datainputstream.readInt();
         this.b = datainputstream.readInt();
+        this.f = datainputstream.readBoolean();
     }
 
     public void a(DataOutputStream dataoutputstream) {
@@ -35,6 +38,7 @@ public class Packet61WorldEvent extends Packet {
         dataoutputstream.writeByte(this.d & 255);
         dataoutputstream.writeInt(this.e);
         dataoutputstream.writeInt(this.b);
+        dataoutputstream.writeBoolean(this.f);
     }
 
     public void handle(NetHandler nethandler) {
@@ -42,6 +46,6 @@ public class Packet61WorldEvent extends Packet {
     }
 
     public int a() {
-        return 20;
+        return 21;
     }
 }
