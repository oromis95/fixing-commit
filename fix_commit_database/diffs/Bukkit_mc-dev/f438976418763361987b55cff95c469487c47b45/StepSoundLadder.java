@@ -0,0 +1,12 @@
+package net.minecraft.server;
+
+final class StepSoundLadder extends StepSound {
+
+    StepSoundLadder(String s, float f, float f1) {
+        super(s, f, f1);
+    }
+
+    public String a() {
+        return "dig.wood";
+    }
+}
