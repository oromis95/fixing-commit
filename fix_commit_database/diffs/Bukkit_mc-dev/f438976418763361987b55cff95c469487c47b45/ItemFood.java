@@ -5,18 +5,18 @@ public class ItemFood extends Item {
     public final int a;
     private final int b;
     private final float c;
-    private final boolean bY;
-    private boolean bZ;
-    private int ca;
-    private int cb;
-    private int cc;
-    private float cd;
+    private final boolean ck;
+    private boolean cl;
+    private int cm;
+    private int cn;
+    private int co;
+    private float cp;
 
     public ItemFood(int i, int j, float f, boolean flag) {
         super(i);
         this.a = 32;
         this.b = j;
-        this.bY = flag;
+        this.ck = flag;
         this.c = f;
         this.a(CreativeModeTab.h);
     }
@@ -34,8 +34,8 @@ public class ItemFood extends Item {
     }
 
     protected void c(ItemStack itemstack, World world, EntityHuman entityhuman) {
-        if (!world.isStatic && this.ca > 0 && world.random.nextFloat() < this.cd) {
-            entityhuman.addEffect(new MobEffect(this.ca, this.cb * 20, this.cc));
+        if (!world.isStatic && this.cm > 0 && world.random.nextFloat() < this.cp) {
+            entityhuman.addEffect(new MobEffect(this.cm, this.cn * 20, this.co));
         }
     }
 
@@ -43,12 +43,12 @@ public class ItemFood extends Item {
         return 32;
     }
 
-    public EnumAnimation b(ItemStack itemstack) {
+    public EnumAnimation d_(ItemStack itemstack) {
         return EnumAnimation.b;
     }
 
     public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
-        if (entityhuman.e(this.bZ)) {
+        if (entityhuman.f(this.cl)) {
             entityhuman.a(itemstack, this.a(itemstack));
         }
 
@@ -63,20 +63,20 @@ public class ItemFood extends Item {
         return this.c;
     }
 
-    public boolean h() {
-        return this.bY;
+    public boolean i() {
+        return this.ck;
     }
 
     public ItemFood a(int i, int j, int k, float f) {
-        this.ca = i;
-        this.cb = j;
-        this.cc = k;
-        this.cd = f;
+        this.cm = i;
+        this.cn = j;
+        this.co = k;
+        this.cp = f;
         return this;
     }
 
-    public ItemFood i() {
-        this.bZ = true;
+    public ItemFood j() {
+        this.cl = true;
         return this;
     }
 }
