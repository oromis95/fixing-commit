@@ -3,14 +3,15 @@ package net.minecraft.server;
 public class WorldType {
 
     public static final WorldType[] types = new WorldType[16];
-    public static final WorldType NORMAL = (new WorldType(0, "default", 1)).f();
+    public static final WorldType NORMAL = (new WorldType(0, "default", 1)).g();
     public static final WorldType FLAT = new WorldType(1, "flat");
     public static final WorldType LARGE_BIOMES = new WorldType(2, "largeBiomes");
     public static final WorldType NORMAL_1_1 = (new WorldType(8, "default_1_1", 0)).a(false);
+    private final int f;
     private final String name;
     private final int version;
-    private boolean h;
     private boolean i;
+    private boolean j;
 
     private WorldType(int i, String s) {
         this(i, s, 0);
@@ -19,7 +20,8 @@ public class WorldType {
     private WorldType(int i, String s, int j) {
         this.name = s;
         this.version = j;
-        this.h = true;
+        this.i = true;
+        this.f = i;
         types[i] = this;
     }
 
@@ -36,17 +38,17 @@ public class WorldType {
     }
 
     private WorldType a(boolean flag) {
-        this.h = flag;
+        this.i = flag;
         return this;
     }
 
-    private WorldType f() {
-        this.i = true;
+    private WorldType g() {
+        this.j = true;
         return this;
     }
 
     public boolean e() {
-        return this.i;
+        return this.j;
     }
 
     public static WorldType getType(String s) {
