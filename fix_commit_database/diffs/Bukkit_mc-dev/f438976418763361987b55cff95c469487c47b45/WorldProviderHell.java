@@ -5,9 +5,9 @@ public class WorldProviderHell extends WorldProvider {
     public WorldProviderHell() {}
 
     public void b() {
-        this.c = new WorldChunkManagerHell(BiomeBase.HELL, 1.0F, 0.0F);
-        this.d = true;
+        this.d = new WorldChunkManagerHell(BiomeBase.HELL, 1.0F, 0.0F);
         this.e = true;
+        this.f = true;
         this.dimension = -1;
     }
 
@@ -17,7 +17,7 @@ public class WorldProviderHell extends WorldProvider {
         for (int i = 0; i <= 15; ++i) {
             float f1 = 1.0F - (float) i / 15.0F;
 
-            this.f[i] = (1.0F - f1) / (f1 * 3.0F + 1.0F) * (1.0F - f) + f;
+            this.g[i] = (1.0F - f1) / (f1 * 3.0F + 1.0F) * (1.0F - f) + f;
         }
     }
 
