@@ -4,24 +4,24 @@ public class ItemWorldMap extends ItemWorldMapBase {
 
     protected ItemWorldMap(int i) {
         super(i);
-        this.d(1);
-        this.a(CreativeModeTab.f);
+        this.a(true);
     }
 
     public WorldMap getSavedMap(ItemStack itemstack, World world) {
+        String s = "map_" + itemstack.getData();
+        WorldMap worldmap = (WorldMap) world.a(WorldMap.class, s);
 
-        WorldMap worldmap = (WorldMap) world.a(WorldMap.class, "map_" + itemstack.getData());
-
-        if (worldmap == null) {
+        if (worldmap == null && !world.isStatic) {
             itemstack.setData(world.b("map"));
-            String s = "map_" + itemstack.getData();
-
+            s = "map_" + itemstack.getData();
             worldmap = new WorldMap(s);
-            worldmap.centerX = world.getWorldData().c();
-            worldmap.centerZ = world.getWorldData().e();
             worldmap.scale = 3;
+            int i = 128 * (1 << worldmap.scale);
+
+            worldmap.centerX = Math.round((float) world.getWorldData().c() / (float) i) * i;
+            worldmap.centerZ = Math.round((float) (world.getWorldData().e() / i)) * i;
             worldmap.map = (byte) world.worldProvider.dimension;
-            worldmap.a();
+            worldmap.c();
             world.a(s, (WorldMapBase) worldmap);
         }
 
@@ -29,7 +29,7 @@ public class ItemWorldMap extends ItemWorldMapBase {
     }
 
     public void a(World world, Entity entity, WorldMap worldmap) {
-        if (world.worldProvider.dimension == worldmap.map) {
+        if (world.worldProvider.dimension == worldmap.map && entity instanceof EntityHuman) {
             short short1 = 128;
             short short2 = 128;
             int i = 1 << worldmap.scale;
@@ -39,14 +39,16 @@ public class ItemWorldMap extends ItemWorldMapBase {
             int i1 = MathHelper.floor(entity.locZ - (double) k) / i + short2 / 2;
             int j1 = 128 / i;
 
-            if (world.worldProvider.e) {
+            if (world.worldProvider.f) {
                 j1 /= 2;
             }
 
-            ++worldmap.f;
+            WorldMapHumanTracker worldmaphumantracker = worldmap.a((EntityHuman) entity);
+
+            ++worldmaphumantracker.d;
 
             for (int k1 = l - j1 + 1; k1 < l + j1; ++k1) {
-                if ((k1 & 15) == (worldmap.f & 15)) {
+                if ((k1 & 15) == (worldmaphumantracker.d & 15)) {
                     int l1 = 255;
                     int i2 = 0;
                     double d0 = 0.0D;
@@ -58,9 +60,6 @@ public class ItemWorldMap extends ItemWorldMapBase {
                             boolean flag = k2 * k2 + l2 * l2 > (j1 - 2) * (j1 - 2);
                             int i3 = (j / i + k1 - short1 / 2) * i;
                             int j3 = (k / i + j2 - short2 / 2) * i;
-                            byte b0 = 0;
-                            byte b1 = 0;
-                            byte b2 = 0;
                             int[] aint = new int[256];
                             Chunk chunk = world.getChunkAtWorldCoords(i3, j3);
 
@@ -74,10 +73,10 @@ public class ItemWorldMap extends ItemWorldMapBase {
                                 int l4;
                                 int i5;
 
-                                if (world.worldProvider.e) {
-                                    l4 = i3 + j3 * 231871;
-                                    l4 = l4 * l4 * 31287121 + l4 * 11;
-                                    if ((l4 >> 20 & 1) == 0) {
+                                if (world.worldProvider.f) {
+                                    j4 = i3 + j3 * 231871;
+                                    j4 = j4 * j4 * 31287121 + j4 * 11;
+                                    if ((j4 >> 20 & 1) == 0) {
                                         aint[Block.DIRT.id] += 10;
                                     } else {
                                         aint[Block.STONE.id] += 10;
@@ -85,91 +84,87 @@ public class ItemWorldMap extends ItemWorldMapBase {
 
                                     d1 = 100.0D;
                                 } else {
-                                    for (l4 = 0; l4 < i; ++l4) {
-                                        for (j4 = 0; j4 < i; ++j4) {
-                                            k4 = chunk.b(l4 + k3, j4 + l3) + 1;
+                                    for (j4 = 0; j4 < i; ++j4) {
+                                        for (k4 = 0; k4 < i; ++k4) {
+                                            l4 = chunk.b(j4 + k3, k4 + l3) + 1;
                                             int j5 = 0;
 
-                                            if (k4 > 1) {
-                                                boolean flag1 = false;
+                                            if (l4 > 1) {
+                                                boolean flag1;
 
                                                 do {
                                                     flag1 = true;
-                                                    j5 = chunk.getTypeId(l4 + k3, k4 - 1, j4 + l3);
+                                                    j5 = chunk.getTypeId(j4 + k3, l4 - 1, k4 + l3);
                                                     if (j5 == 0) {
                                                         flag1 = false;
-                                                    } else if (k4 > 0 && j5 > 0 && Block.byId[j5].material.F == MaterialMapColor.b) {
+                                                    } else if (l4 > 0 && j5 > 0 && Block.byId[j5].material.G == MaterialMapColor.b) {
                                                         flag1 = false;
                                                     }
 
                                                     if (!flag1) {
-                                                        --k4;
-                                                        if (k4 <= 0) {
+                                                        --l4;
+                                                        if (l4 <= 0) {
                                                             break;
                                                         }
 
-                                                        j5 = chunk.getTypeId(l4 + k3, k4 - 1, j4 + l3);
+                                                        j5 = chunk.getTypeId(j4 + k3, l4 - 1, k4 + l3);
                                                     }
-                                                } while (k4 > 0 && !flag1);
+                                                } while (l4 > 0 && !flag1);
 
-                                                if (k4 > 0 && j5 != 0 && Block.byId[j5].material.isLiquid()) {
-                                                    i5 = k4 - 1;
+                                                if (l4 > 0 && j5 != 0 && Block.byId[j5].material.isLiquid()) {
+                                                    i5 = l4 - 1;
                                                     boolean flag2 = false;
 
                                                     int k5;
 
                                                     do {
-                                                        k5 = chunk.getTypeId(l4 + k3, i5--, j4 + l3);
+                                                        k5 = chunk.getTypeId(j4 + k3, i5--, k4 + l3);
                                                         ++i4;
                                                     } while (i5 > 0 && k5 != 0 && Block.byId[k5].material.isLiquid());
                                                 }
                                             }
 
-                                            d1 += (double) k4 / (double) (i * i);
+                                            d1 += (double) l4 / (double) (i * i);
                                             ++aint[j5];
                                         }
                                     }
                                 }
 
                                 i4 /= i * i;
-                                int l5 = b0 / (i * i);
-
-                                l5 = b1 / (i * i);
-                                l5 = b2 / (i * i);
-                                l4 = 0;
                                 j4 = 0;
+                                k4 = 0;
 
-                                for (k4 = 0; k4 < 256; ++k4) {
-                                    if (aint[k4] > l4) {
-                                        j4 = k4;
-                                        l4 = aint[k4];
+                                for (l4 = 0; l4 < 256; ++l4) {
+                                    if (aint[l4] > j4) {
+                                        k4 = l4;
+                                        j4 = aint[l4];
                                     }
                                 }
 
                                 double d2 = (d1 - d0) * 4.0D / (double) (i + 4) + ((double) (k1 + j2 & 1) - 0.5D) * 0.4D;
-                                byte b3 = 1;
+                                byte b0 = 1;
 
                                 if (d2 > 0.6D) {
-                                    b3 = 2;
+                                    b0 = 2;
                                 }
 
                                 if (d2 < -0.6D) {
-                                    b3 = 0;
+                                    b0 = 0;
                                 }
 
                                 i5 = 0;
-                                if (j4 > 0) {
-                                    MaterialMapColor materialmapcolor = Block.byId[j4].material.F;
+                                if (k4 > 0) {
+                                    MaterialMapColor materialmapcolor = Block.byId[k4].material.G;
 
                                     if (materialmapcolor == MaterialMapColor.n) {
                                         d2 = (double) i4 * 0.1D + (double) (k1 + j2 & 1) * 0.2D;
-                                        b3 = 1;
+                                        b0 = 1;
                                         if (d2 < 0.5D) {
-                                            b3 = 2;
+                                            b0 = 2;
                                         }
 
                                         if (d2 > 0.9D) {
-                                            b3 = 0;
+                                            b0 = 0;
                                         }
                                     }
 
@@ -178,10 +173,10 @@ public class ItemWorldMap extends ItemWorldMapBase {
 
                                 d0 = d1;
                                 if (j2 >= 0 && k2 * k2 + l2 * l2 < j1 * j1 && (!flag || (k1 + j2 & 1) != 0)) {
-                                    byte b4 = worldmap.colors[k1 + j2 * short1];
-                                    byte b5 = (byte) (i5 * 4 + b3);
+                                    byte b1 = worldmap.colors[k1 + j2 * short1];
+                                    byte b2 = (byte) (i5 * 4 + b0);
 
-                                    if (b4 != b5) {
+                                    if (b1 != b2) {
                                         if (l1 > j2) {
                                             l1 = j2;
                                         }
@@ -190,7 +185,7 @@ public class ItemWorldMap extends ItemWorldMapBase {
                                             i2 = j2;
                                         }
 
-                                        worldmap.colors[k1 + j2 * short1] = b5;
+                                        worldmap.colors[k1 + j2 * short1] = b2;
                                     }
                                 }
                             }
@@ -221,22 +216,29 @@ public class ItemWorldMap extends ItemWorldMapBase {
         }
     }
 
-    public void d(ItemStack itemstack, World world, EntityHuman entityhuman) {
-        itemstack.setData(world.b("map"));
-        String s = "map_" + itemstack.getData();
-        WorldMap worldmap = new WorldMap(s);
-
-        world.a(s, (WorldMapBase) worldmap);
-        worldmap.centerX = MathHelper.floor(entityhuman.locX);
-        worldmap.centerZ = MathHelper.floor(entityhuman.locZ);
-        worldmap.scale = 3;
-        worldmap.map = (byte) world.worldProvider.dimension;
-        worldmap.a();
-    }
-
     public Packet c(ItemStack itemstack, World world, EntityHuman entityhuman) {
         byte[] abyte = this.getSavedMap(itemstack, world).getUpdatePacket(itemstack, world, entityhuman);
 
         return abyte == null ? null : new Packet131ItemData((short) Item.MAP.id, (short) itemstack.getData(), abyte);
     }
+
+    public void d(ItemStack itemstack, World world, EntityHuman entityhuman) {
+        if (itemstack.hasTag() && itemstack.getTag().getBoolean("map_is_scaling")) {
+            WorldMap worldmap = Item.MAP.getSavedMap(itemstack, world);
+
+            itemstack.setData(world.b("map"));
+            WorldMap worldmap1 = new WorldMap("map_" + itemstack.getData());
+
+            worldmap1.scale = (byte) (worldmap.scale + 1);
+            if (worldmap1.scale > 4) {
+                worldmap1.scale = 4;
+            }
+
+            worldmap1.centerX = worldmap.centerX;
+            worldmap1.centerZ = worldmap.centerZ;
+            worldmap1.map = worldmap.map;
+            worldmap1.c();
+            world.a("map_" + itemstack.getData(), (WorldMapBase) worldmap1);
+        }
+    }
 }
