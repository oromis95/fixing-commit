@@ -11,26 +11,26 @@ public class PathfinderGoalSwell extends PathfinderGoal {
     }
 
     public boolean a() {
-        EntityLiving entityliving = this.a.az();
+        EntityLiving entityliving = this.a.aF();
 
-        return this.a.p() > 0 || entityliving != null && this.a.e(entityliving) < 9.0D;
+        return this.a.o() > 0 || entityliving != null && this.a.e(entityliving) < 9.0D;
     }
 
-    public void e() {
+    public void c() {
         this.a.getNavigation().g();
-        this.b = this.a.az();
+        this.b = this.a.aF();
     }
 
-    public void c() {
+    public void d() {
         this.b = null;
     }
 
-    public void d() {
+    public void e() {
         if (this.b == null) {
             this.a.a(-1);
         } else if (this.a.e(this.b) > 49.0D) {
             this.a.a(-1);
-        } else if (!this.a.at().canSee(this.b)) {
+        } else if (!this.a.az().canSee(this.b)) {
             this.a.a(-1);
         } else {
             this.a.a(1);
