@@ -18,7 +18,7 @@ public class PathfinderGoalTakeFlower extends PathfinderGoal {
     public boolean a() {
         if (this.a.getAge() >= 0) {
             return false;
-        } else if (!this.a.world.s()) {
+        } else if (!this.a.world.t()) {
             return false;
         } else {
             List list = this.a.world.a(EntityIronGolem.class, this.a.boundingBox.grow(6.0D, 2.0D, 6.0D));
@@ -31,7 +31,7 @@ public class PathfinderGoalTakeFlower extends PathfinderGoal {
                 while (iterator.hasNext()) {
                     EntityIronGolem entityirongolem = (EntityIronGolem) iterator.next();
 
-                    if (entityirongolem.p() > 0) {
+                    if (entityirongolem.o() > 0) {
                         this.b = entityirongolem;
                         break;
                     }
@@ -43,29 +43,29 @@ public class PathfinderGoalTakeFlower extends PathfinderGoal {
     }
 
     public boolean b() {
-        return this.b.p() > 0;
+        return this.b.o() > 0;
     }
 
-    public void e() {
-        this.c = this.a.au().nextInt(320);
+    public void c() {
+        this.c = this.a.aA().nextInt(320);
         this.d = false;
         this.b.getNavigation().g();
     }
 
-    public void c() {
+    public void d() {
         this.b = null;
         this.a.getNavigation().g();
     }
 
-    public void d() {
+    public void e() {
         this.a.getControllerLook().a(this.b, 30.0F, 30.0F);
-        if (this.b.p() == this.c) {
+        if (this.b.o() == this.c) {
             this.a.getNavigation().a((EntityLiving) this.b, 0.15F);
             this.d = true;
         }
 
         if (this.d && this.a.e(this.b) < 4.0D) {
-            this.b.e(false);
+            this.b.f(false);
             this.a.getNavigation().g();
         }
     }
