@@ -11,9 +11,13 @@ public class Packet40EntityMetadata extends Packet {
 
     public Packet40EntityMetadata() {}
 
-    public Packet40EntityMetadata(int i, DataWatcher datawatcher) {
+    public Packet40EntityMetadata(int i, DataWatcher datawatcher, boolean flag) {
         this.a = i;
-        this.b = datawatcher.b();
+        if (flag) {
+            this.b = datawatcher.c();
+        } else {
+            this.b = datawatcher.b();
+        }
     }
 
     public void a(DataInputStream datainputstream) {
@@ -33,8 +37,4 @@ public class Packet40EntityMetadata extends Packet {
     public int a() {
         return 5;
     }
-
-    public boolean a_() {
-        return true;
-    }
 }
