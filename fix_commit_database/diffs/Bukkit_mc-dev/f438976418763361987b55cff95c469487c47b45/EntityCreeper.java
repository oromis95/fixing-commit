@@ -2,8 +2,10 @@ package net.minecraft.server;
 
 public class EntityCreeper extends EntityMonster {
 
-    int fuseTicks;
-    int e;
+    private int d;
+    private int fuseTicks;
+    private int maxFuseTicks = 30;
+    private int explosionRadius = 3;
 
     public EntityCreeper(World world) {
         super(world);
@@ -19,10 +21,22 @@ public class EntityCreeper extends EntityMonster {
         this.targetSelector.a(2, new PathfinderGoalHurtByTarget(this, false));
     }
 
-    public boolean aV() {
+    public boolean bb() {
         return true;
     }
 
+    public int as() {
+        return this.aF() == null ? 3 : 3 + (this.health - 1);
+    }
+
+    protected void a(float f) {
+        super.a(f);
+        this.fuseTicks = (int) ((float) this.fuseTicks + f * 1.5F);
+        if (this.fuseTicks > this.maxFuseTicks - 5) {
+            this.fuseTicks = this.maxFuseTicks - 5;
+        }
+    }
+
     public int getMaxHealth() {
         return 20;
     }
@@ -38,17 +52,27 @@ public class EntityCreeper extends EntityMonster {
         if (this.datawatcher.getByte(17) == 1) {
             nbttagcompound.setBoolean("powered", true);
         }
+
+        nbttagcompound.setShort("Fuse", (short) this.maxFuseTicks);
+        nbttagcompound.setByte("ExplosionRadius", (byte) this.explosionRadius);
     }
 
     public void a(NBTTagCompound nbttagcompound) {
         super.a(nbttagcompound);
         this.datawatcher.watch(17, Byte.valueOf((byte) (nbttagcompound.getBoolean("powered") ? 1 : 0)));
+        if (nbttagcompound.hasKey("Fuse")) {
+            this.maxFuseTicks = nbttagcompound.getShort("Fuse");
+        }
+
+        if (nbttagcompound.hasKey("ExplosionRadius")) {
+            this.explosionRadius = nbttagcompound.getByte("ExplosionRadius");
+        }
     }
 
-    public void h_() {
+    public void j_() {
         if (this.isAlive()) {
-            this.e = this.fuseTicks;
-            int i = this.p();
+            this.d = this.fuseTicks;
+            int i = this.o();
 
             if (i > 0 && this.fuseTicks == 0) {
                 this.world.makeSound(this, "random.fuse", 1.0F, 0.5F);
@@ -59,13 +83,15 @@ public class EntityCreeper extends EntityMonster {
                 this.fuseTicks = 0;
             }
 
-            if (this.fuseTicks >= 30) {
-                this.fuseTicks = 30;
+            if (this.fuseTicks >= this.maxFuseTicks) {
+                this.fuseTicks = this.maxFuseTicks;
                 if (!this.world.isStatic) {
+                    boolean flag = this.world.getGameRules().getBoolean("mobGriefing");
+
                     if (this.isPowered()) {
-                        this.world.explode(this, this.locX, this.locY, this.locZ, 6.0F);
+                        this.world.explode(this, this.locX, this.locY, this.locZ, (float) (this.explosionRadius * 2), flag);
                     } else {
-                        this.world.explode(this, this.locX, this.locY, this.locZ, 3.0F);
+                        this.world.explode(this, this.locX, this.locY, this.locZ, (float) this.explosionRadius, flag);
                     }
 
                     this.die();
@@ -73,15 +99,15 @@ public class EntityCreeper extends EntityMonster {
             }
         }
 
-        super.h_();
+        super.j_();
     }
 
-    protected String aR() {
-        return "mob.creeper";
+    protected String aX() {
+        return "mob.creeper.say";
     }
 
-    protected String aS() {
-        return "mob.creeperdeath";
+    protected String aY() {
+        return "mob.creeper.death";
     }
 
     public void die(DamageSource damagesource) {
@@ -91,7 +117,7 @@ public class EntityCreeper extends EntityMonster {
         }
     }
 
-    public boolean k(Entity entity) {
+    public boolean l(Entity entity) {
         return true;
     }
 
@@ -103,7 +129,7 @@ public class EntityCreeper extends EntityMonster {
         return Item.SULPHUR.id;
     }
 
-    public int p() {
+    public int o() {
         return this.datawatcher.getByte(16);
     }
 
