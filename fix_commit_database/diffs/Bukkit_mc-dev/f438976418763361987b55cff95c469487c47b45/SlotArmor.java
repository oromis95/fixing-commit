@@ -17,6 +17,6 @@ class SlotArmor extends Slot {
     }
 
     public boolean isAllowed(ItemStack itemstack) {
-        return itemstack.getItem() instanceof ItemArmor ? ((ItemArmor) itemstack.getItem()).a == this.a : (itemstack.getItem().id == Block.PUMPKIN.id ? this.a == 0 : false);
+        return itemstack == null ? false : (itemstack.getItem() instanceof ItemArmor ? ((ItemArmor) itemstack.getItem()).a == this.a : (itemstack.getItem().id != Block.PUMPKIN.id && itemstack.getItem().id != Item.SKULL.id ? false : this.a == 0));
     }
 }
