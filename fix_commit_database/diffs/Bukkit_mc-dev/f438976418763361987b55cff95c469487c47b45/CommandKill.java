@@ -4,14 +4,18 @@ public class CommandKill extends CommandAbstract {
 
     public CommandKill() {}
 
-    public String b() {
+    public String c() {
         return "kill";
     }
 
+    public int a() {
+        return 0;
+    }
+
     public void b(ICommandListener icommandlistener, String[] astring) {
-        EntityHuman entityhuman = c(icommandlistener);
+        EntityPlayer entityplayer = c(icommandlistener);
 
-        entityhuman.damageEntity(DamageSource.OUT_OF_WORLD, 1000);
-        icommandlistener.sendMessage("Ouch. That look like it hurt.");
+        entityplayer.damageEntity(DamageSource.OUT_OF_WORLD, 1000);
+        icommandlistener.sendMessage("Ouch. That looks like it hurt.");
     }
 }
