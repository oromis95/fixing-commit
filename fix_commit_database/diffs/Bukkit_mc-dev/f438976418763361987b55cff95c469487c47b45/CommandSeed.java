@@ -5,16 +5,20 @@ public class CommandSeed extends CommandAbstract {
     public CommandSeed() {}
 
     public boolean b(ICommandListener icommandlistener) {
-        return MinecraftServer.getServer().H() || super.b(icommandlistener);
+        return MinecraftServer.getServer().I() || super.b(icommandlistener);
     }
 
-    public String b() {
+    public String c() {
         return "seed";
     }
 
+    public int a() {
+        return 2;
+    }
+
     public void b(ICommandListener icommandlistener, String[] astring) {
-        EntityHuman entityhuman = c(icommandlistener);
+        EntityPlayer entityplayer = c(icommandlistener);
 
-        icommandlistener.sendMessage("Seed: " + entityhuman.world.getSeed());
+        icommandlistener.sendMessage("Seed: " + entityplayer.world.getSeed());
     }
 }
