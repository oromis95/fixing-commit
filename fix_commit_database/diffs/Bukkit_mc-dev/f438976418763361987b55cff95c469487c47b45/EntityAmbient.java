@@ -0,0 +1,8 @@
+package net.minecraft.server;
+
+public abstract class EntityAmbient extends EntityLiving implements IAnimal {
+
+    public EntityAmbient(World world) {
+        super(world);
+    }
+}
