@@ -5,14 +5,22 @@ import java.util.Random;
 public class WorldGenLargeFeatureStart extends StructureStart {
 
     public WorldGenLargeFeatureStart(World world, Random random, int i, int j) {
-        if (world.getBiome(i * 16 + 8, j * 16 + 8) == BiomeBase.JUNGLE) {
-            WorldGenJungleTemple worldgenjungletemple = new WorldGenJungleTemple(random, i * 16, j * 16);
+        BiomeBase biomebase = world.getBiome(i * 16 + 8, j * 16 + 8);
 
-            this.a.add(worldgenjungletemple);
+        if (biomebase != BiomeBase.JUNGLE && biomebase != BiomeBase.JUNGLE_HILLS) {
+            if (biomebase == BiomeBase.SWAMPLAND) {
+                WorldGenWitchHut worldgenwitchhut = new WorldGenWitchHut(random, i * 16, j * 16);
+
+                this.a.add(worldgenwitchhut);
+            } else {
+                WorldGenPyramidPiece worldgenpyramidpiece = new WorldGenPyramidPiece(random, i * 16, j * 16);
+
+                this.a.add(worldgenpyramidpiece);
+            }
         } else {
-            WorldGenPyramidPiece worldgenpyramidpiece = new WorldGenPyramidPiece(random, i * 16, j * 16);
+            WorldGenJungleTemple worldgenjungletemple = new WorldGenJungleTemple(random, i * 16, j * 16);
 
-            this.a.add(worldgenpyramidpiece);
+            this.a.add(worldgenjungletemple);
         }
 
         this.c();
