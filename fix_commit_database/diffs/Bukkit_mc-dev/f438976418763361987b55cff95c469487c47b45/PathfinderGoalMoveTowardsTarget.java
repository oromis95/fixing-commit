@@ -18,20 +18,20 @@ public class PathfinderGoalMoveTowardsTarget extends PathfinderGoal {
     }
 
     public boolean a() {
-        this.b = this.a.az();
+        this.b = this.a.aF();
         if (this.b == null) {
             return false;
         } else if (this.b.e(this.a) > (double) (this.g * this.g)) {
             return false;
         } else {
-            Vec3D vec3d = RandomPositionGenerator.a(this.a, 16, 7, Vec3D.a().create(this.b.locX, this.b.locY, this.b.locZ));
+            Vec3D vec3d = RandomPositionGenerator.a(this.a, 16, 7, this.a.world.getVec3DPool().create(this.b.locX, this.b.locY, this.b.locZ));
 
             if (vec3d == null) {
                 return false;
             } else {
-                this.c = vec3d.a;
-                this.d = vec3d.b;
-                this.e = vec3d.c;
+                this.c = vec3d.c;
+                this.d = vec3d.d;
+                this.e = vec3d.e;
                 return true;
             }
         }
@@ -41,11 +41,11 @@ public class PathfinderGoalMoveTowardsTarget extends PathfinderGoal {
         return !this.a.getNavigation().f() && this.b.isAlive() && this.b.e(this.a) < (double) (this.g * this.g);
     }
 
-    public void c() {
+    public void d() {
         this.b = null;
     }
 
-    public void e() {
+    public void c() {
         this.a.getNavigation().a(this.c, this.d, this.e, this.f);
     }
 }
