@@ -28,7 +28,7 @@ public class VillageSiege {
                 return;
             }
         } else {
-            if (this.world.s()) {
+            if (this.world.t()) {
                 this.c = 0;
                 return;
             }
@@ -139,12 +139,14 @@ public class VillageSiege {
 
             try {
                 entityzombie = new EntityZombie(this.world);
+                entityzombie.bD();
+                entityzombie.setVillager(false);
             } catch (Exception exception) {
                 exception.printStackTrace();
                 return false;
             }
 
-            entityzombie.setPositionRotation(vec3d.a, vec3d.b, vec3d.c, this.world.random.nextFloat() * 360.0F, 0.0F);
+            entityzombie.setPositionRotation(vec3d.c, vec3d.d, vec3d.e, this.world.random.nextFloat() * 360.0F, 0.0F);
             this.world.addEntity(entityzombie);
             ChunkCoordinates chunkcoordinates = this.f.getCenter();
 
@@ -160,7 +162,7 @@ public class VillageSiege {
             int k1 = k + this.world.random.nextInt(16) - 8;
 
             if (this.f.a(i1, j1, k1) && SpawnerCreature.a(EnumCreatureType.MONSTER, this.world, i1, j1, k1)) {
-                return Vec3D.a().create((double) i1, (double) j1, (double) k1);
+                this.world.getVec3DPool().create((double) i1, (double) j1, (double) k1);
             }
         }
 
