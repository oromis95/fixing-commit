@@ -6,5 +6,6 @@ public class Facing {
     public static final int[] b = new int[] { 0, 0, 0, 0, -1, 1};
     public static final int[] c = new int[] { -1, 1, 0, 0, 0, 0};
     public static final int[] d = new int[] { 0, 0, -1, 1, 0, 0};
+    public static final String[] e = new String[] { "DOWN", "UP", "NORTH", "SOUTH", "WEST", "EAST"};
 
 }
