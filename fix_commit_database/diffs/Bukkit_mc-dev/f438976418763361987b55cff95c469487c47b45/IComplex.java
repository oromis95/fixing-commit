@@ -0,0 +1,8 @@
+package net.minecraft.server;
+
+public interface IComplex {
+
+    World d();
+
+    boolean a(EntityComplexPart entitycomplexpart, DamageSource damagesource, int i);
+}
