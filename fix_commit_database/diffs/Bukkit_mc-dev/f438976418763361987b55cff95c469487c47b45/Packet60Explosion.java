@@ -26,9 +26,9 @@ public class Packet60Explosion extends Packet {
         this.d = f;
         this.e = new ArrayList(list);
         if (vec3d != null) {
-            this.f = (float) vec3d.a;
-            this.g = (float) vec3d.b;
-            this.h = (float) vec3d.c;
+            this.f = (float) vec3d.c;
+            this.g = (float) vec3d.d;
+            this.h = (float) vec3d.e;
         }
     }
 
