@@ -6,47 +6,72 @@ public class CommandXp extends CommandAbstract {
 
     public CommandXp() {}
 
-    public String b() {
+    public String c() {
         return "xp";
     }
 
+    public int a() {
+        return 2;
+    }
+
     public String a(ICommandListener icommandlistener) {
         return icommandlistener.a("commands.xp.usage", new Object[0]);
     }
 
     public void b(ICommandListener icommandlistener, String[] astring) {
-        if (astring.length > 0) {
-            int i = a(icommandlistener, astring[0], 0, 5000);
-            EntityHuman entityhuman;
+        if (astring.length <= 0) {
+            throw new ExceptionUsage("commands.xp.usage", new Object[0]);
+        } else {
+            String s = astring[0];
+            boolean flag = s.endsWith("l") || s.endsWith("L");
+
+            if (flag && s.length() > 1) {
+                s = s.substring(0, s.length() - 1);
+            }
+
+            int i = a(icommandlistener, s);
+            boolean flag1 = i < 0;
+
+            if (flag1) {
+                i *= -1;
+            }
+
+            EntityPlayer entityplayer;
 
             if (astring.length > 1) {
-                entityhuman = this.a(astring[1]);
+                entityplayer = c(icommandlistener, astring[1]);
             } else {
-                entityhuman = c(icommandlistener);
+                entityplayer = c(icommandlistener);
             }
 
-            entityhuman.giveExp(i);
-            a(icommandlistener, "commands.xp.success", new Object[] { Integer.valueOf(i), entityhuman.getLocalizedName()});
-        } else {
-            throw new ExceptionUsage("commands.xp.usage", new Object[0]);
+            if (flag) {
+                if (flag1) {
+                    entityplayer.levelDown(-i);
+                    a(icommandlistener, "commands.xp.success.negative.levels", new Object[] { Integer.valueOf(i), entityplayer.getLocalizedName()});
+                } else {
+                    entityplayer.levelDown(i);
+                    a(icommandlistener, "commands.xp.success.levels", new Object[] { Integer.valueOf(i), entityplayer.getLocalizedName()});
+                }
+            } else {
+                if (flag1) {
+                    throw new ExceptionUsage("commands.xp.failure.widthdrawXp", new Object[0]);
+                }
+
+                entityplayer.giveExp(i);
+                a(icommandlistener, "commands.xp.success", new Object[] { Integer.valueOf(i), entityplayer.getLocalizedName()});
+            }
         }
     }
 
     public List a(ICommandListener icommandlistener, String[] astring) {
-        return astring.length == 2 ? a(astring, this.c()) : null;
+        return astring.length == 2 ? a(astring, this.d()) : null;
     }
 
-    protected EntityHuman a(String s) {
-        EntityPlayer entityplayer = MinecraftServer.getServer().getServerConfigurationManager().f(s);
-
-        if (entityplayer == null) {
-            throw new ExceptionPlayerNotFound();
-        } else {
-            return entityplayer;
-        }
+    protected String[] d() {
+        return MinecraftServer.getServer().getPlayers();
     }
 
-    protected String[] c() {
-        return MinecraftServer.getServer().getPlayers();
+    public boolean a(int i) {
+        return i == 1;
     }
 }
