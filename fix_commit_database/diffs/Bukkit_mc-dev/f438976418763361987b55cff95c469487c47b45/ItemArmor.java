@@ -2,15 +2,15 @@ package net.minecraft.server;
 
 public class ItemArmor extends Item {
 
-    private static final int[] bY = new int[] { 11, 16, 15, 13};
+    private static final int[] ck = new int[] { 11, 16, 15, 13};
     public final int a;
     public final int b;
     public final int c;
-    private final EnumArmorMaterial bZ;
+    private final EnumArmorMaterial cl;
 
     public ItemArmor(int i, EnumArmorMaterial enumarmormaterial, int j, int k) {
         super(i);
-        this.bZ = enumarmormaterial;
+        this.cl = enumarmormaterial;
         this.a = k;
         this.c = j;
         this.b = enumarmormaterial.b(k);
@@ -19,11 +19,74 @@ public class ItemArmor extends Item {
         this.a(CreativeModeTab.j);
     }
 
-    public int b() {
-        return this.bZ.a();
+    public int c() {
+        return this.cl.a();
     }
 
-    static int[] c() {
-        return bY;
+    public EnumArmorMaterial d() {
+        return this.cl;
+    }
+
+    public boolean b_(ItemStack itemstack) {
+        return this.cl != EnumArmorMaterial.CLOTH ? false : (!itemstack.hasTag() ? false : (!itemstack.getTag().hasKey("display") ? false : itemstack.getTag().getCompound("display").hasKey("color")));
+    }
+
+    public int b(ItemStack itemstack) {
+        if (this.cl != EnumArmorMaterial.CLOTH) {
+            return -1;
+        } else {
+            NBTTagCompound nbttagcompound = itemstack.getTag();
+
+            if (nbttagcompound == null) {
+                return 10511680;
+            } else {
+                NBTTagCompound nbttagcompound1 = nbttagcompound.getCompound("display");
+
+                return nbttagcompound1 == null ? 10511680 : (nbttagcompound1.hasKey("color") ? nbttagcompound1.getInt("color") : 10511680);
+            }
+        }
+    }
+
+    public void c(ItemStack itemstack) {
+        if (this.cl == EnumArmorMaterial.CLOTH) {
+            NBTTagCompound nbttagcompound = itemstack.getTag();
+
+            if (nbttagcompound != null) {
+                NBTTagCompound nbttagcompound1 = nbttagcompound.getCompound("display");
+
+                if (nbttagcompound1.hasKey("color")) {
+                    nbttagcompound1.o("color");
+                }
+            }
+        }
+    }
+
+    public void b(ItemStack itemstack, int i) {
+        if (this.cl != EnumArmorMaterial.CLOTH) {
+            throw new UnsupportedOperationException("Can\'t dye non-leather!");
+        } else {
+            NBTTagCompound nbttagcompound = itemstack.getTag();
+
+            if (nbttagcompound == null) {
+                nbttagcompound = new NBTTagCompound();
+                itemstack.setTag(nbttagcompound);
+            }
+
+            NBTTagCompound nbttagcompound1 = nbttagcompound.getCompound("display");
+
+            if (!nbttagcompound.hasKey("display")) {
+                nbttagcompound.setCompound("display", nbttagcompound1);
+            }
+
+            nbttagcompound1.setInt("color", i);
+        }
+    }
+
+    public boolean a(ItemStack itemstack, ItemStack itemstack1) {
+        return this.cl.b() == itemstack1.id ? true : super.a(itemstack, itemstack1);
+    }
+
+    static int[] e() {
+        return ck;
     }
 }
