@@ -6,10 +6,14 @@ public class CommandBanList extends CommandAbstract {
 
     public CommandBanList() {}
 
-    public String b() {
+    public String c() {
         return "banlist";
     }
 
+    public int a() {
+        return 3;
+    }
+
     public boolean b(ICommandListener icommandlistener) {
         return (MinecraftServer.getServer().getServerConfigurationManager().getIPBans().isEnabled() || MinecraftServer.getServer().getServerConfigurationManager().getNameBans().isEnabled()) && super.b(icommandlistener);
     }
