@@ -2,6 +2,8 @@ package net.minecraft.server;
 
 public class EntityPig extends EntityAnimal {
 
+    private final PathfinderGoalPassengerCarrotStick d;
+
     public EntityPig(World world) {
         super(world);
         this.texture = "/mob/pig.png";
@@ -11,15 +13,17 @@ public class EntityPig extends EntityAnimal {
 
         this.goalSelector.a(0, new PathfinderGoalFloat(this));
         this.goalSelector.a(1, new PathfinderGoalPanic(this, 0.38F));
-        this.goalSelector.a(2, new PathfinderGoalBreed(this, f));
-        this.goalSelector.a(3, new PathfinderGoalTempt(this, 0.25F, Item.WHEAT.id, false));
-        this.goalSelector.a(4, new PathfinderGoalFollowParent(this, 0.28F));
-        this.goalSelector.a(5, new PathfinderGoalRandomStroll(this, f));
-        this.goalSelector.a(6, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 6.0F));
-        this.goalSelector.a(7, new PathfinderGoalRandomLookaround(this));
+        this.goalSelector.a(2, this.d = new PathfinderGoalPassengerCarrotStick(this, 0.34F));
+        this.goalSelector.a(3, new PathfinderGoalBreed(this, f));
+        this.goalSelector.a(4, new PathfinderGoalTempt(this, 0.3F, Item.CARROT_STICK.id, false));
+        this.goalSelector.a(4, new PathfinderGoalTempt(this, 0.3F, Item.CARROT.id, false));
+        this.goalSelector.a(5, new PathfinderGoalFollowParent(this, 0.28F));
+        this.goalSelector.a(6, new PathfinderGoalRandomStroll(this, f));
+        this.goalSelector.a(7, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 6.0F));
+        this.goalSelector.a(8, new PathfinderGoalRandomLookaround(this));
     }
 
-    public boolean aV() {
+    public boolean bb() {
         return true;
     }
 
@@ -27,6 +31,16 @@ public class EntityPig extends EntityAnimal {
         return 10;
     }
 
+    protected void bi() {
+        super.bi();
+    }
+
+    public boolean bF() {
+        ItemStack itemstack = ((EntityHuman) this.passenger).bA();
+
+        return itemstack != null && itemstack.id == Item.CARROT_STICK.id;
+    }
+
     protected void a() {
         super.a();
         this.datawatcher.a(16, Byte.valueOf((byte) 0));
@@ -42,16 +56,20 @@ public class EntityPig extends EntityAnimal {
         this.setSaddle(nbttagcompound.getBoolean("Saddle"));
     }
 
-    protected String aQ() {
-        return "mob.pig";
+    protected String aW() {
+        return "mob.pig.say";
+    }
+
+    protected String aX() {
+        return "mob.pig.say";
     }
 
-    protected String aR() {
-        return "mob.pig";
+    protected String aY() {
+        return "mob.pig.death";
     }
 
-    protected String aS() {
-        return "mob.pigdeath";
+    protected void a(int i, int j, int k, int l) {
+        this.world.makeSound(this, "mob.pig.step", 0.15F, 1.0F);
     }
 
     public boolean c(EntityHuman entityhuman) {
@@ -79,6 +97,10 @@ public class EntityPig extends EntityAnimal {
                 this.b(Item.PORK.id, 1);
             }
         }
+
+        if (this.hasSaddle()) {
+            this.b(Item.SADDLE.id, 1);
+        }
     }
 
     public boolean hasSaddle() {
@@ -113,4 +135,12 @@ public class EntityPig extends EntityAnimal {
     public EntityAnimal createChild(EntityAnimal entityanimal) {
         return new EntityPig(this.world);
     }
+
+    public boolean c(ItemStack itemstack) {
+        return itemstack != null && itemstack.id == Item.CARROT.id;
+    }
+
+    public PathfinderGoalPassengerCarrotStick n() {
+        return this.d;
+    }
 }
