@@ -8,57 +8,61 @@ import java.util.Random;
 
 public abstract class EntityLiving extends Entity {
 
+    private static final float[] b = new float[] { 0.0F, 0.0F, 0.005F, 0.01F};
+    private static final float[] c = new float[] { 0.0F, 0.0F, 0.05F, 0.1F};
+    private static final float[] d = new float[] { 0.0F, 0.0F, 0.005F, 0.02F};
+    public static final float[] as = new float[] { 0.0F, 0.01F, 0.07F, 0.2F};
     public int maxNoDamageTicks = 20;
-    public float ao;
-    public float ap;
-    public float aq = 0.0F;
-    public float ar = 0.0F;
-    public float as = 0.0F;
-    public float at = 0.0F;
-    protected float au;
-    protected float av;
-    protected float aw;
-    protected float ax;
-    protected boolean ay = true;
+    public float au;
+    public float av;
+    public float aw = 0.0F;
+    public float ax = 0.0F;
+    public float ay = 0.0F;
+    public float az = 0.0F;
+    protected float aA;
+    protected float aB;
+    protected float aC;
+    protected float aD;
+    protected boolean aE = true;
     protected String texture = "/mob/char.png";
-    protected boolean aA = true;
-    protected float aB = 0.0F;
-    protected String aC = null;
-    protected float aD = 1.0F;
-    protected int aE = 0;
-    protected float aF = 0.0F;
-    public float aG = 0.1F;
-    public float aH = 0.02F;
-    public float aI;
-    public float aJ;
+    protected boolean aG = true;
+    protected float aH = 0.0F;
+    protected String aI = null;
+    protected float aJ = 1.0F;
+    protected int aK = 0;
+    protected float aL = 0.0F;
+    public float aM = 0.1F;
+    public float aN = 0.02F;
+    public float aO;
+    public float aP;
     protected int health = this.getMaxHealth();
-    public int aL;
-    protected int aM;
-    private int a;
+    public int aR;
+    protected int aS;
+    public int aT;
     public int hurtTicks;
-    public int aO;
-    public float aP = 0.0F;
+    public int aV;
+    public float aW = 0.0F;
     public int deathTicks = 0;
     public int attackTicks = 0;
-    public float aS;
-    public float aT;
-    protected boolean aU = false;
-    protected int aV;
-    public int aW = -1;
-    public float aX = (float) (Math.random() * 0.8999999761581421D + 0.10000000149011612D);
-    public float aY;
     public float aZ;
     public float ba;
+    protected boolean bb = false;
+    protected int bc;
+    public int bd = -1;
+    public float be = (float) (Math.random() * 0.8999999761581421D + 0.10000000149011612D);
+    public float bf;
+    public float bg;
+    public float bh;
     protected EntityHuman killer = null;
     protected int lastDamageByPlayerTime = 0;
     private EntityLiving lastDamager = null;
-    private int c = 0;
-    private EntityLiving d = null;
-    public int bd = 0;
-    public int be = 0;
+    private int f = 0;
+    private EntityLiving g = null;
+    public int bk = 0;
+    public int bl = 0;
     protected HashMap effects = new HashMap();
     private boolean updateEffects = true;
-    private int f;
+    private int i;
     private ControllerLook lookController;
     private ControllerMove moveController;
     private ControllerJump jumpController;
@@ -66,29 +70,37 @@ public abstract class EntityLiving extends Entity {
     private Navigation navigation;
     protected final PathfinderGoalSelector goalSelector;
     protected final PathfinderGoalSelector targetSelector;
-    private EntityLiving bz;
-    private EntitySenses bA;
-    private float bB;
-    private ChunkCoordinates bC = new ChunkCoordinates(0, 0, 0);
-    private float bD = -1.0F;
-    protected int bi;
-    protected double bj;
-    protected double bk;
-    protected double bl;
-    protected double bm;
-    protected double bn;
-    float bo = 0.0F;
+    private EntityLiving bO;
+    private EntitySenses bP;
+    private float bQ;
+    private ChunkCoordinates bR = new ChunkCoordinates(0, 0, 0);
+    private float bS = -1.0F;
+    private ItemStack[] equipment = new ItemStack[5];
+    protected float[] dropChances = new float[5];
+    private ItemStack[] bU = new ItemStack[5];
+    public boolean bq = false;
+    public int br = 0;
+    protected boolean canPickUpLoot = false;
+    private boolean persistent = false;
+    protected boolean invulnerable = false;
+    protected int bu;
+    protected double bv;
+    protected double bw;
+    protected double bx;
+    protected double by;
+    protected double bz;
+    float bA = 0.0F;
     protected int lastDamage = 0;
-    protected int bq = 0;
-    protected float br;
-    protected float bs;
-    protected float bt;
-    protected boolean bu = false;
-    protected float bv = 0.0F;
-    protected float bw = 0.7F;
-    private int bE = 0;
-    private Entity bF;
-    protected int bx = 0;
+    protected int bC = 0;
+    protected float bD;
+    protected float bE;
+    protected float bF;
+    protected boolean bG = false;
+    protected float bH = 0.0F;
+    protected float bI = 0.7F;
+    private int bW = 0;
+    private Entity bX;
+    protected int bJ = 0;
 
     public EntityLiving(World world) {
         super(world);
@@ -100,13 +112,18 @@ public abstract class EntityLiving extends Entity {
         this.jumpController = new ControllerJump(this);
         this.senses = new EntityAIBodyControl(this);
         this.navigation = new Navigation(this, world, 16.0F);
-        this.bA = new EntitySenses(this);
-        this.ap = (float) (Math.random() + 1.0D) * 0.01F;
+        this.bP = new EntitySenses(this);
+        this.av = (float) (Math.random() + 1.0D) * 0.01F;
         this.setPosition(this.locX, this.locY, this.locZ);
-        this.ao = (float) Math.random() * 12398.0F;
+        this.au = (float) Math.random() * 12398.0F;
         this.yaw = (float) (Math.random() * 3.1415927410125732D * 2.0D);
-        this.as = this.yaw;
-        this.W = 0.5F;
+        this.ay = this.yaw;
+
+        for (int i = 0; i < this.dropChances.length; ++i) {
+            this.dropChances[i] = 0.05F;
+        }
+
+        this.X = 0.5F;
     }
 
     public ControllerLook getControllerLook() {
@@ -125,104 +142,124 @@ public abstract class EntityLiving extends Entity {
         return this.navigation;
     }
 
-    public EntitySenses at() {
-        return this.bA;
+    public EntitySenses az() {
+        return this.bP;
     }
 
-    public Random au() {
+    public Random aA() {
         return this.random;
     }
 
-    public EntityLiving av() {
+    public EntityLiving aB() {
         return this.lastDamager;
     }
 
-    public EntityLiving aw() {
-        return this.d;
+    public EntityLiving aC() {
+        return this.g;
     }
 
-    public void j(Entity entity) {
+    public void k(Entity entity) {
         if (entity instanceof EntityLiving) {
-            this.d = (EntityLiving) entity;
+            this.g = (EntityLiving) entity;
         }
     }
 
-    public int ax() {
-        return this.bq;
+    public int aD() {
+        return this.bC;
     }
 
-    public float am() {
-        return this.as;
+    public float ap() {
+        return this.ay;
     }
 
-    public float ay() {
-        return this.bB;
+    public float aE() {
+        return this.bQ;
     }
 
     public void e(float f) {
-        this.bB = f;
+        this.bQ = f;
         this.f(f);
     }
 
-    public boolean k(Entity entity) {
-        this.j(entity);
+    public boolean l(Entity entity) {
+        this.k(entity);
         return false;
     }
 
-    public EntityLiving az() {
-        return this.bz;
+    public EntityLiving aF() {
+        return this.bO;
     }
 
     public void b(EntityLiving entityliving) {
-        this.bz = entityliving;
+        this.bO = entityliving;
     }
 
     public boolean a(Class oclass) {
         return EntityCreeper.class != oclass && EntityGhast.class != oclass;
     }
 
-    public void aA() {}
+    public void aG() {}
+
+    protected void a(double d0, boolean flag) {
+        if (flag && this.fallDistance > 0.0F) {
+            int i = MathHelper.floor(this.locX);
+            int j = MathHelper.floor(this.locY - 0.20000000298023224D - (double) this.height);
+            int k = MathHelper.floor(this.locZ);
+            int l = this.world.getTypeId(i, j, k);
+
+            if (l == 0 && this.world.getTypeId(i, j - 1, k) == Block.FENCE.id) {
+                l = this.world.getTypeId(i, j - 1, k);
+            }
+
+            if (l > 0) {
+                Block.byId[l].a(this.world, i, j, k, this, this.fallDistance);
+            }
+        }
+
+        super.a(d0, flag);
+    }
 
-    public boolean aB() {
-        return this.d(MathHelper.floor(this.locX), MathHelper.floor(this.locY), MathHelper.floor(this.locZ));
+    public boolean aH() {
+        return this.e(MathHelper.floor(this.locX), MathHelper.floor(this.locY), MathHelper.floor(this.locZ));
     }
 
-    public boolean d(int i, int j, int k) {
-        return this.bD == -1.0F ? true : this.bC.e(i, j, k) < this.bD * this.bD;
+    public boolean e(int i, int j, int k) {
+        return this.bS == -1.0F ? true : this.bR.e(i, j, k) < this.bS * this.bS;
     }
 
     public void b(int i, int j, int k, int l) {
-        this.bC.b(i, j, k);
-        this.bD = (float) l;
+        this.bR.b(i, j, k);
+        this.bS = (float) l;
     }
 
-    public ChunkCoordinates aC() {
-        return this.bC;
+    public ChunkCoordinates aI() {
+        return this.bR;
     }
 
-    public float aD() {
-        return this.bD;
+    public float aJ() {
+        return this.bS;
     }
 
-    public void aE() {
-        this.bD = -1.0F;
+    public void aK() {
+        this.bS = -1.0F;
     }
 
-    public boolean aF() {
-        return this.bD != -1.0F;
+    public boolean aL() {
+        return this.bS != -1.0F;
     }
 
     public void c(EntityLiving entityliving) {
         this.lastDamager = entityliving;
-        this.c = this.lastDamager != null ? 60 : 0;
+        this.f = this.lastDamager != null ? 60 : 0;
     }
 
     protected void a() {
-        this.datawatcher.a(8, Integer.valueOf(this.f));
+        this.datawatcher.a(8, Integer.valueOf(this.i));
+        this.datawatcher.a(9, Byte.valueOf((byte) 0));
     }
 
-    public boolean l(Entity entity) {
-        return this.world.a(Vec3D.a().create(this.locX, this.locY + (double) this.getHeadHeight(), this.locZ), Vec3D.a().create(entity.locX, entity.locY + (double) entity.getHeadHeight(), entity.locZ)) == null;
+    public boolean m(Entity entity) {
+        return this.world.a(this.world.getVec3DPool().create(this.locX, this.locY + (double) this.getHeadHeight(), this.locZ), this.world.getVec3DPool().create(entity.locX, entity.locY + (double) entity.getHeadHeight(), entity.locZ)) == null;
     }
 
     public boolean L() {
@@ -237,25 +274,25 @@ public abstract class EntityLiving extends Entity {
         return this.length * 0.85F;
     }
 
-    public int aG() {
+    public int aM() {
         return 80;
     }
 
-    public void aH() {
-        String s = this.aQ();
+    public void aN() {
+        String s = this.aW();
 
         if (s != null) {
-            this.world.makeSound(this, s, this.aP(), this.i());
+            this.world.makeSound(this, s, this.aV(), this.h());
         }
     }
 
-    public void z() {
-        this.aI = this.aJ;
-        super.z();
+    public void y() {
+        this.aO = this.aP;
+        super.y();
         this.world.methodProfiler.a("mobBaseTick");
-        if (this.isAlive() && this.random.nextInt(1000) < this.a++) {
-            this.a = -this.aG();
-            this.aH();
+        if (this.isAlive() && this.random.nextInt(1000) < this.aT++) {
+            this.aT = -this.aM();
+            this.aN();
         }
 
         if (this.isAlive() && this.inBlock()) {
@@ -266,8 +303,8 @@ public abstract class EntityLiving extends Entity {
             this.extinguish();
         }
 
-        if (this.isAlive() && this.a(Material.WATER) && !this.aU() && !this.effects.containsKey(Integer.valueOf(MobEffectList.WATER_BREATHING.id))) {
-            this.setAirTicks(this.h(this.getAirTicks()));
+        if (this.isAlive() && this.a(Material.WATER) && !this.ba() && !this.effects.containsKey(Integer.valueOf(MobEffectList.WATER_BREATHING.id))) {
+            this.setAirTicks(this.g(this.getAirTicks()));
             if (this.getAirTicks() == -20) {
                 this.setAirTicks(0);
 
@@ -276,7 +313,7 @@ public abstract class EntityLiving extends Entity {
                     float f1 = this.random.nextFloat() - this.random.nextFloat();
                     float f2 = this.random.nextFloat() - this.random.nextFloat();
 
-                    this.world.a("bubble", this.locX + (double) f, this.locY + (double) f1, this.locZ + (double) f2, this.motX, this.motY, this.motZ);
+                    this.world.addParticle("bubble", this.locX + (double) f, this.locY + (double) f1, this.locZ + (double) f2, this.motX, this.motY, this.motZ);
                 }
 
                 this.damageEntity(DamageSource.DROWN, 2);
@@ -287,7 +324,7 @@ public abstract class EntityLiving extends Entity {
             this.setAirTicks(300);
         }
 
-        this.aS = this.aT;
+        this.aZ = this.ba;
         if (this.attackTicks > 0) {
             --this.attackTicks;
         }
@@ -301,7 +338,7 @@ public abstract class EntityLiving extends Entity {
         }
 
         if (this.health <= 0) {
-            this.aI();
+            this.aO();
         }
 
         if (this.lastDamageByPlayerTime > 0) {
@@ -310,30 +347,30 @@ public abstract class EntityLiving extends Entity {
             this.killer = null;
         }
 
-        if (this.d != null && !this.d.isAlive()) {
-            this.d = null;
+        if (this.g != null && !this.g.isAlive()) {
+            this.g = null;
         }
 
         if (this.lastDamager != null) {
             if (!this.lastDamager.isAlive()) {
                 this.c((EntityLiving) null);
-            } else if (this.c > 0) {
-                --this.c;
+            } else if (this.f > 0) {
+                --this.f;
             } else {
                 this.c((EntityLiving) null);
             }
         }
 
-        this.bo();
+        this.bu();
+        this.aD = this.aC;
         this.ax = this.aw;
-        this.ar = this.aq;
-        this.at = this.as;
+        this.az = this.ay;
         this.lastYaw = this.yaw;
         this.lastPitch = this.pitch;
         this.world.methodProfiler.b();
     }
 
-    protected void aI() {
+    protected void aO() {
         ++this.deathTicks;
         if (this.deathTicks == 20) {
             int i;
@@ -356,62 +393,75 @@ public abstract class EntityLiving extends Entity {
                 double d1 = this.random.nextGaussian() * 0.02D;
                 double d2 = this.random.nextGaussian() * 0.02D;
 
-                this.world.a("explode", this.locX + (double) (this.random.nextFloat() * this.width * 2.0F) - (double) this.width, this.locY + (double) (this.random.nextFloat() * this.length), this.locZ + (double) (this.random.nextFloat() * this.width * 2.0F) - (double) this.width, d0, d1, d2);
+                this.world.addParticle("explode", this.locX + (double) (this.random.nextFloat() * this.width * 2.0F) - (double) this.width, this.locY + (double) (this.random.nextFloat() * this.length), this.locZ + (double) (this.random.nextFloat() * this.width * 2.0F) - (double) this.width, d0, d1, d2);
             }
         }
     }
 
-    protected int h(int i) {
-        return i - 1;
+    protected int g(int i) {
+        int j = EnchantmentManager.getOxygenEnchantmentLevel(this);
+
+        return j > 0 && this.random.nextInt(j + 1) > 0 ? i : i - 1;
     }
 
     protected int getExpValue(EntityHuman entityhuman) {
-        return this.aV;
+        return this.bc;
     }
 
     protected boolean alwaysGivesExp() {
         return false;
     }
 
-    public void aK() {
+    public void aQ() {
         for (int i = 0; i < 20; ++i) {
             double d0 = this.random.nextGaussian() * 0.02D;
             double d1 = this.random.nextGaussian() * 0.02D;
             double d2 = this.random.nextGaussian() * 0.02D;
             double d3 = 10.0D;
 
-            this.world.a("explode", this.locX + (double) (this.random.nextFloat() * this.width * 2.0F) - (double) this.width - d0 * d3, this.locY + (double) (this.random.nextFloat() * this.length) - d1 * d3, this.locZ + (double) (this.random.nextFloat() * this.width * 2.0F) - (double) this.width - d2 * d3, d0, d1, d2);
+            this.world.addParticle("explode", this.locX + (double) (this.random.nextFloat() * this.width * 2.0F) - (double) this.width - d0 * d3, this.locY + (double) (this.random.nextFloat() * this.length) - d1 * d3, this.locZ + (double) (this.random.nextFloat() * this.width * 2.0F) - (double) this.width - d2 * d3, d0, d1, d2);
         }
     }
 
     public void U() {
         super.U();
-        this.au = this.av;
-        this.av = 0.0F;
+        this.aA = this.aB;
+        this.aB = 0.0F;
         this.fallDistance = 0.0F;
     }
 
-    public void h_() {
-        super.h_();
-        if (this.bd > 0) {
-            if (this.be <= 0) {
-                this.be = 60;
+    public void j_() {
+        super.j_();
+        if (!this.world.isStatic) {
+            for (int i = 0; i < 5; ++i) {
+                ItemStack itemstack = this.getEquipment(i);
+
+                if (!ItemStack.matches(itemstack, this.bU[i])) {
+                    ((WorldServer) this.world).getTracker().a(this, new Packet5EntityEquipment(this.id, i, itemstack));
+                    this.bU[i] = itemstack == null ? null : itemstack.cloneItemStack();
+                }
+            }
+        }
+
+        if (this.bk > 0) {
+            if (this.bl <= 0) {
+                this.bl = 60;
             }
 
-            --this.be;
-            if (this.be <= 0) {
-                --this.bd;
+            --this.bl;
+            if (this.bl <= 0) {
+                --this.bk;
             }
         }
 
-        this.d();
+        this.c();
         double d0 = this.locX - this.lastX;
         double d1 = this.locZ - this.lastZ;
         float f = (float) (d0 * d0 + d1 * d1);
-        float f1 = this.aq;
+        float f1 = this.aw;
         float f2 = 0.0F;
 
-        this.au = this.av;
+        this.aA = this.aB;
         float f3 = 0.0F;
 
         if (f > 0.0025000002F) {
@@ -420,7 +470,7 @@ public abstract class EntityLiving extends Entity {
             f1 = (float) Math.atan2(d1, d0) * 180.0F / 3.1415927F - 90.0F;
         }
 
-        if (this.aJ > 0.0F) {
+        if (this.aP > 0.0F) {
             f1 = this.yaw;
         }
 
@@ -428,15 +478,15 @@ public abstract class EntityLiving extends Entity {
             f3 = 0.0F;
         }
 
-        this.av += (f3 - this.av) * 0.3F;
+        this.aB += (f3 - this.aB) * 0.3F;
         this.world.methodProfiler.a("headTurn");
-        if (this.aV()) {
+        if (this.bb()) {
             this.senses.a();
         } else {
-            float f4 = MathHelper.g(f1 - this.aq);
+            float f4 = MathHelper.g(f1 - this.aw);
 
-            this.aq += f4 * 0.3F;
-            float f5 = MathHelper.g(this.yaw - this.aq);
+            this.aw += f4 * 0.3F;
+            float f5 = MathHelper.g(this.yaw - this.aw);
             boolean flag = f5 < -90.0F || f5 >= 90.0F;
 
             if (f5 < -75.0F) {
@@ -447,9 +497,9 @@ public abstract class EntityLiving extends Entity {
                 f5 = 75.0F;
             }
 
-            this.aq = this.yaw - f5;
+            this.aw = this.yaw - f5;
             if (f5 * f5 > 2500.0F) {
-                this.aq += f5 * 0.2F;
+                this.aw += f5 * 0.2F;
             }
 
             if (flag) {
@@ -468,12 +518,12 @@ public abstract class EntityLiving extends Entity {
             this.lastYaw += 360.0F;
         }
 
-        while (this.aq - this.ar < -180.0F) {
-            this.ar -= 360.0F;
+        while (this.aw - this.ax < -180.0F) {
+            this.ax -= 360.0F;
         }
 
-        while (this.aq - this.ar >= 180.0F) {
-            this.ar += 360.0F;
+        while (this.aw - this.ax >= 180.0F) {
+            this.ax += 360.0F;
         }
 
         while (this.pitch - this.lastPitch < -180.0F) {
@@ -484,16 +534,16 @@ public abstract class EntityLiving extends Entity {
             this.lastPitch += 360.0F;
         }
 
-        while (this.as - this.at < -180.0F) {
-            this.at -= 360.0F;
+        while (this.ay - this.az < -180.0F) {
+            this.az -= 360.0F;
         }
 
-        while (this.as - this.at >= 180.0F) {
-            this.at += 360.0F;
+        while (this.ay - this.az >= 180.0F) {
+            this.az += 360.0F;
         }
 
         this.world.methodProfiler.b();
-        this.aw += f2;
+        this.aC += f2;
     }
 
     public void heal(int i) {
@@ -524,13 +574,17 @@ public abstract class EntityLiving extends Entity {
         if (this.world.isStatic) {
             return false;
         } else {
-            this.bq = 0;
+            this.bC = 0;
             if (this.health <= 0) {
                 return false;
             } else if (damagesource.k() && this.hasEffect(MobEffectList.FIRE_RESISTANCE)) {
                 return false;
             } else {
-                this.aZ = 1.5F;
+                if ((damagesource == DamageSource.ANVIL || damagesource == DamageSource.FALLING_BLOCK) && this.getEquipment(4) != null) {
+                    i = (int) ((float) i * 0.55F);
+                }
+
+                this.bg = 1.5F;
                 boolean flag = true;
 
                 if ((float) this.noDamageTicks > (float) this.maxNoDamageTicks / 2.0F) {
@@ -543,13 +597,13 @@ public abstract class EntityLiving extends Entity {
                     flag = false;
                 } else {
                     this.lastDamage = i;
-                    this.aL = this.health;
+                    this.aR = this.health;
                     this.noDamageTicks = this.maxNoDamageTicks;
                     this.d(damagesource, i);
-                    this.hurtTicks = this.aO = 10;
+                    this.hurtTicks = this.aV = 10;
                 }
 
-                this.aP = 0.0F;
+                this.aW = 0.0F;
                 Entity entity = damagesource.getEntity();
 
                 if (entity != null) {
@@ -585,21 +639,21 @@ public abstract class EntityLiving extends Entity {
                             d0 = (Math.random() - Math.random()) * 0.01D;
                         }
 
-                        this.aP = (float) (Math.atan2(d1, d0) * 180.0D / 3.1415927410125732D) - this.yaw;
+                        this.aW = (float) (Math.atan2(d1, d0) * 180.0D / 3.1415927410125732D) - this.yaw;
                         this.a(entity, i, d0, d1);
                     } else {
-                        this.aP = (float) ((int) (Math.random() * 2.0D) * 180);
+                        this.aW = (float) ((int) (Math.random() * 2.0D) * 180);
                     }
                 }
 
                 if (this.health <= 0) {
                     if (flag) {
-                        this.world.makeSound(this, this.aS(), this.aP(), this.i());
+                        this.world.makeSound(this, this.aY(), this.aV(), this.h());
                     }
 
                     this.die(damagesource);
                 } else if (flag) {
-                    this.world.makeSound(this, this.aR(), this.aP(), this.i());
+                    this.world.makeSound(this, this.aX(), this.aV(), this.h());
                 }
 
                 return true;
@@ -607,24 +661,38 @@ public abstract class EntityLiving extends Entity {
         }
     }
 
-    private float i() {
+    private float h() {
         return this.isBaby() ? (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.5F : (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F;
     }
 
-    public int aO() {
-        return 0;
+    public int aU() {
+        int i = 0;
+        ItemStack[] aitemstack = this.getEquipment();
+        int j = aitemstack.length;
+
+        for (int k = 0; k < j; ++k) {
+            ItemStack itemstack = aitemstack[k];
+
+            if (itemstack != null && itemstack.getItem() instanceof ItemArmor) {
+                int l = ((ItemArmor) itemstack.getItem()).b;
+
+                i += l;
+            }
+        }
+
+        return i;
     }
 
     protected void k(int i) {}
 
     protected int b(DamageSource damagesource, int i) {
         if (!damagesource.ignoresArmor()) {
-            int j = 25 - this.aO();
-            int k = i * j + this.aM;
+            int j = 25 - this.aU();
+            int k = i * j + this.aS;
 
             this.k(i);
             i = k / 25;
-            this.aM = k % 25;
+            this.aS = k % 25;
         }
 
         return i;
@@ -634,39 +702,41 @@ public abstract class EntityLiving extends Entity {
         if (this.hasEffect(MobEffectList.RESISTANCE)) {
             int j = (this.getEffect(MobEffectList.RESISTANCE).getAmplifier() + 1) * 5;
             int k = 25 - j;
-            int l = i * k + this.aM;
+            int l = i * k + this.aS;
 
             i = l / 25;
-            this.aM = l % 25;
+            this.aS = l % 25;
         }
 
         return i;
     }
 
     protected void d(DamageSource damagesource, int i) {
-        i = this.b(damagesource, i);
-        i = this.c(damagesource, i);
-        this.health -= i;
+        if (!this.invulnerable) {
+            i = this.b(damagesource, i);
+            i = this.c(damagesource, i);
+            this.health -= i;
+        }
     }
 
-    protected float aP() {
+    protected float aV() {
         return 1.0F;
     }
 
-    protected String aQ() {
+    protected String aW() {
         return null;
     }
 
-    protected String aR() {
-        return "damage.hurtflesh";
+    protected String aX() {
+        return "damage.hit";
     }
 
-    protected String aS() {
-        return "damage.hurtflesh";
+    protected String aY() {
+        return "damage.hit";
     }
 
     public void a(Entity entity, int i, double d0, double d1) {
-        this.al = true;
+        this.am = true;
         float f = MathHelper.sqrt(d0 * d0 + d1 * d1);
         float f1 = 0.4F;
 
@@ -684,24 +754,25 @@ public abstract class EntityLiving extends Entity {
     public void die(DamageSource damagesource) {
         Entity entity = damagesource.getEntity();
 
-        if (this.aE >= 0 && entity != null) {
-            entity.c(this, this.aE);
+        if (this.aK >= 0 && entity != null) {
+            entity.c(this, this.aK);
         }
 
         if (entity != null) {
             entity.a(this);
         }
 
-        this.aU = true;
+        this.bb = true;
         if (!this.world.isStatic) {
             int i = 0;
 
             if (entity instanceof EntityHuman) {
-                i = EnchantmentManager.getBonusMonsterLootEnchantmentLevel(((EntityHuman) entity).inventory);
+                i = EnchantmentManager.getBonusMonsterLootEnchantmentLevel((EntityLiving) entity);
             }
 
-            if (!this.isBaby()) {
+            if (!this.isBaby() && this.world.getGameRules().getBoolean("doMobLoot")) {
                 this.dropDeathLoot(this.lastDamageByPlayerTime > 0, i);
+                this.dropEquipment(this.lastDamageByPlayerTime > 0, i);
                 if (this.lastDamageByPlayerTime > 0) {
                     int j = this.random.nextInt(200) - i;
 
@@ -764,7 +835,7 @@ public abstract class EntityLiving extends Entity {
 
         if (this.H() && (!(this instanceof EntityHuman) || !((EntityHuman) this).abilities.isFlying)) {
             d0 = this.locY;
-            this.a(f, f1, this.aV() ? 0.04F : 0.02F);
+            this.a(f, f1, this.bb() ? 0.04F : 0.02F);
             this.move(this.motX, this.motY, this.motZ);
             this.motX *= 0.800000011920929D;
             this.motY *= 0.800000011920929D;
@@ -800,15 +871,15 @@ public abstract class EntityLiving extends Entity {
             float f4;
 
             if (this.onGround) {
-                if (this.aV()) {
-                    f4 = this.ay();
+                if (this.bb()) {
+                    f4 = this.aE();
                 } else {
-                    f4 = this.aG;
+                    f4 = this.aM;
                 }
 
                 f4 *= f3;
             } else {
-                f4 = this.aH;
+                f4 = this.aN;
             }
 
             this.a(f, f1, f4);
@@ -822,7 +893,7 @@ public abstract class EntityLiving extends Entity {
                 }
             }
 
-            if (this.f_()) {
+            if (this.g_()) {
                 float f5 = 0.15F;
 
                 if (this.motX < (double) (-f5)) {
@@ -854,7 +925,7 @@ public abstract class EntityLiving extends Entity {
             }
 
             this.move(this.motX, this.motY, this.motZ);
-            if (this.positionChanged && this.f_()) {
+            if (this.positionChanged && this.g_()) {
                 this.motY = 0.2D;
             }
 
@@ -864,7 +935,7 @@ public abstract class EntityLiving extends Entity {
             this.motZ *= (double) f2;
         }
 
-        this.aY = this.aZ;
+        this.bf = this.bg;
         d0 = this.locX - this.lastX;
         double d1 = this.locZ - this.lastZ;
         float f6 = MathHelper.sqrt(d0 * d0 + d1 * d1) * 4.0F;
@@ -873,11 +944,11 @@ public abstract class EntityLiving extends Entity {
             f6 = 1.0F;
         }
 
-        this.aZ += (f6 - this.aZ) * 0.4F;
-        this.ba += this.aZ;
+        this.bg += (f6 - this.bg) * 0.4F;
+        this.bh += this.bg;
     }
 
-    public boolean f_() {
+    public boolean g_() {
         int i = MathHelper.floor(this.locX);
         int j = MathHelper.floor(this.boundingBox.b);
         int k = MathHelper.floor(this.locZ);
@@ -891,22 +962,44 @@ public abstract class EntityLiving extends Entity {
         nbttagcompound.setShort("HurtTime", (short) this.hurtTicks);
         nbttagcompound.setShort("DeathTime", (short) this.deathTicks);
         nbttagcompound.setShort("AttackTime", (short) this.attackTicks);
+        nbttagcompound.setBoolean("CanPickUpLoot", this.canPickUpLoot);
+        nbttagcompound.setBoolean("PersistenceRequired", this.persistent);
+        nbttagcompound.setBoolean("Invulnerable", this.invulnerable);
+        NBTTagList nbttaglist = new NBTTagList();
+
+        for (int i = 0; i < this.equipment.length; ++i) {
+            NBTTagCompound nbttagcompound1 = new NBTTagCompound();
+
+            if (this.equipment[i] != null) {
+                this.equipment[i].save(nbttagcompound1);
+            }
+
+            nbttaglist.add(nbttagcompound1);
+        }
+
+        nbttagcompound.set("Equipment", nbttaglist);
+        NBTTagList nbttaglist1;
+
         if (!this.effects.isEmpty()) {
-            NBTTagList nbttaglist = new NBTTagList();
+            nbttaglist1 = new NBTTagList();
             Iterator iterator = this.effects.values().iterator();
 
             while (iterator.hasNext()) {
                 MobEffect mobeffect = (MobEffect) iterator.next();
-                NBTTagCompound nbttagcompound1 = new NBTTagCompound();
 
-                nbttagcompound1.setByte("Id", (byte) mobeffect.getEffectId());
-                nbttagcompound1.setByte("Amplifier", (byte) mobeffect.getAmplifier());
-                nbttagcompound1.setInt("Duration", mobeffect.getDuration());
-                nbttaglist.add(nbttagcompound1);
+                nbttaglist1.add(mobeffect.a(new NBTTagCompound()));
             }
 
-            nbttagcompound.set("ActiveEffects", nbttaglist);
+            nbttagcompound.set("ActiveEffects", nbttaglist1);
         }
+
+        nbttaglist1 = new NBTTagList();
+
+        for (int j = 0; j < this.dropChances.length; ++j) {
+            nbttaglist1.add(new NBTTagFloat(j + "", this.dropChances[j]));
+        }
+
+        nbttagcompound.set("DropChances", nbttaglist1);
     }
 
     public void a(NBTTagCompound nbttagcompound) {
@@ -922,16 +1015,36 @@ public abstract class EntityLiving extends Entity {
         this.hurtTicks = nbttagcompound.getShort("HurtTime");
         this.deathTicks = nbttagcompound.getShort("DeathTime");
         this.attackTicks = nbttagcompound.getShort("AttackTime");
+        this.canPickUpLoot = nbttagcompound.getBoolean("CanPickUpLoot");
+        this.persistent = nbttagcompound.getBoolean("PersistenceRequired");
+        this.invulnerable = nbttagcompound.getBoolean("Invulnerable");
+        NBTTagList nbttaglist;
+        int i;
+
+        if (nbttagcompound.hasKey("Equipment")) {
+            nbttaglist = nbttagcompound.getList("Equipment");
+
+            for (i = 0; i < this.equipment.length; ++i) {
+                this.equipment[i] = ItemStack.a((NBTTagCompound) nbttaglist.get(i));
+            }
+        }
+
         if (nbttagcompound.hasKey("ActiveEffects")) {
-            NBTTagList nbttaglist = nbttagcompound.getList("ActiveEffects");
+            nbttaglist = nbttagcompound.getList("ActiveEffects");
 
-            for (int i = 0; i < nbttaglist.size(); ++i) {
+            for (i = 0; i < nbttaglist.size(); ++i) {
                 NBTTagCompound nbttagcompound1 = (NBTTagCompound) nbttaglist.get(i);
-                byte b0 = nbttagcompound1.getByte("Id");
-                byte b1 = nbttagcompound1.getByte("Amplifier");
-                int j = nbttagcompound1.getInt("Duration");
+                MobEffect mobeffect = MobEffect.b(nbttagcompound1);
+
+                this.effects.put(Integer.valueOf(mobeffect.getEffectId()), mobeffect);
+            }
+        }
+
+        if (nbttagcompound.hasKey("DropChances")) {
+            nbttaglist = nbttagcompound.getList("DropChances");
 
-                this.effects.put(Integer.valueOf(b0), new MobEffect(b0, j, b1));
+            for (i = 0; i < nbttaglist.size(); ++i) {
+                this.dropChances[i] = ((NBTTagFloat) nbttaglist.get(i)).data;
             }
         }
     }
@@ -940,32 +1053,32 @@ public abstract class EntityLiving extends Entity {
         return !this.dead && this.health > 0;
     }
 
-    public boolean aU() {
+    public boolean ba() {
         return false;
     }
 
     public void f(float f) {
-        this.bs = f;
+        this.bE = f;
     }
 
-    public void d(boolean flag) {
-        this.bu = flag;
+    public void e(boolean flag) {
+        this.bG = flag;
     }
 
-    public void d() {
-        if (this.bE > 0) {
-            --this.bE;
+    public void c() {
+        if (this.bW > 0) {
+            --this.bW;
         }
 
-        if (this.bi > 0) {
-            double d0 = this.locX + (this.bj - this.locX) / (double) this.bi;
-            double d1 = this.locY + (this.bk - this.locY) / (double) this.bi;
-            double d2 = this.locZ + (this.bl - this.locZ) / (double) this.bi;
-            double d3 = MathHelper.g(this.bm - (double) this.yaw);
+        if (this.bu > 0) {
+            double d0 = this.locX + (this.bv - this.locX) / (double) this.bu;
+            double d1 = this.locY + (this.bw - this.locY) / (double) this.bu;
+            double d2 = this.locZ + (this.bx - this.locZ) / (double) this.bu;
+            double d3 = MathHelper.g(this.by - (double) this.yaw);
 
-            this.yaw = (float) ((double) this.yaw + d3 / (double) this.bi);
-            this.pitch = (float) ((double) this.pitch + (this.bn - (double) this.pitch) / (double) this.bi);
-            --this.bi;
+            this.yaw = (float) ((double) this.yaw + d3 / (double) this.bu);
+            this.pitch = (float) ((double) this.pitch + (this.bz - (double) this.pitch) / (double) this.bu);
+            --this.bu;
             this.setPosition(d0, d1, d2);
             this.b(this.yaw, this.pitch);
         }
@@ -983,62 +1096,129 @@ public abstract class EntityLiving extends Entity {
         }
 
         this.world.methodProfiler.a("ai");
-        if (this.aX()) {
-            this.bu = false;
-            this.br = 0.0F;
-            this.bs = 0.0F;
-            this.bt = 0.0F;
-        } else if (this.aW()) {
-            if (this.aV()) {
+        if (this.bd()) {
+            this.bG = false;
+            this.bD = 0.0F;
+            this.bE = 0.0F;
+            this.bF = 0.0F;
+        } else if (this.bc()) {
+            if (this.bb()) {
                 this.world.methodProfiler.a("newAi");
-                this.bc();
+                this.bi();
                 this.world.methodProfiler.b();
             } else {
                 this.world.methodProfiler.a("oldAi");
-                this.be();
+                this.bk();
                 this.world.methodProfiler.b();
-                this.as = this.yaw;
+                this.ay = this.yaw;
             }
         }
 
         this.world.methodProfiler.b();
         this.world.methodProfiler.a("jump");
-        if (this.bu) {
+        if (this.bG) {
             if (!this.H() && !this.J()) {
-                if (this.onGround && this.bE == 0) {
-                    this.aZ();
-                    this.bE = 10;
+                if (this.onGround && this.bW == 0) {
+                    this.bf();
+                    this.bW = 10;
                 }
             } else {
                 this.motY += 0.03999999910593033D;
             }
         } else {
-            this.bE = 0;
+            this.bW = 0;
         }
 
         this.world.methodProfiler.b();
         this.world.methodProfiler.a("travel");
-        this.br *= 0.98F;
-        this.bs *= 0.98F;
-        this.bt *= 0.9F;
-        float f = this.aG;
-
-        this.aG *= this.bs();
-        this.e(this.br, this.bs);
-        this.aG = f;
+        this.bD *= 0.98F;
+        this.bE *= 0.98F;
+        this.bF *= 0.9F;
+        float f = this.aM;
+
+        this.aM *= this.by();
+        this.e(this.bD, this.bE);
+        this.aM = f;
         this.world.methodProfiler.b();
         this.world.methodProfiler.a("push");
-        if (!this.world.isStatic) {
-            List list = this.world.getEntities(this, this.boundingBox.grow(0.20000000298023224D, 0.0D, 0.20000000298023224D));
+        List list;
+        Iterator iterator;
 
+        if (!this.world.isStatic) {
+            list = this.world.getEntities(this, this.boundingBox.grow(0.20000000298023224D, 0.0D, 0.20000000298023224D));
             if (list != null && !list.isEmpty()) {
-                Iterator iterator = list.iterator();
+                iterator = list.iterator();
 
                 while (iterator.hasNext()) {
                     Entity entity = (Entity) iterator.next();
 
                     if (entity.M()) {
-                        entity.collide(this);
+                        this.n(entity);
+                    }
+                }
+            }
+        }
+
+        this.world.methodProfiler.b();
+        this.world.methodProfiler.a("looting");
+        if (!this.world.isStatic && this.canPickUpLoot && this.world.getGameRules().getBoolean("mobGriefing")) {
+            list = this.world.a(EntityItem.class, this.boundingBox.grow(1.0D, 0.0D, 1.0D));
+            iterator = list.iterator();
+
+            while (iterator.hasNext()) {
+                EntityItem entityitem = (EntityItem) iterator.next();
+
+                if (!entityitem.dead && entityitem.itemStack != null) {
+                    ItemStack itemstack = entityitem.itemStack;
+                    int i = b(itemstack);
+
+                    if (i > -1) {
+                        boolean flag = true;
+                        ItemStack itemstack1 = this.getEquipment(i);
+
+                        if (itemstack1 != null) {
+                            if (i == 0) {
+                                if (itemstack.getItem() instanceof ItemSword && !(itemstack1.getItem() instanceof ItemSword)) {
+                                    flag = true;
+                                } else if (itemstack.getItem() instanceof ItemSword && itemstack1.getItem() instanceof ItemSword) {
+                                    ItemSword itemsword = (ItemSword) itemstack.getItem();
+                                    ItemSword itemsword1 = (ItemSword) itemstack1.getItem();
+
+                                    if (itemsword.g() == itemsword1.g()) {
+                                        flag = itemstack.getData() > itemstack1.getData() || itemstack.hasTag() && !itemstack1.hasTag();
+                                    } else {
+                                        flag = itemsword.g() > itemsword1.g();
+                                    }
+                                } else {
+                                    flag = false;
+                                }
+                            } else if (itemstack.getItem() instanceof ItemArmor && !(itemstack1.getItem() instanceof ItemArmor)) {
+                                flag = true;
+                            } else if (itemstack.getItem() instanceof ItemArmor && itemstack1.getItem() instanceof ItemArmor) {
+                                ItemArmor itemarmor = (ItemArmor) itemstack.getItem();
+                                ItemArmor itemarmor1 = (ItemArmor) itemstack1.getItem();
+
+                                if (itemarmor.b == itemarmor1.b) {
+                                    flag = itemstack.getData() > itemstack1.getData() || itemstack.hasTag() && !itemstack1.hasTag();
+                                } else {
+                                    flag = itemarmor.b > itemarmor1.b;
+                                }
+                            } else {
+                                flag = false;
+                            }
+                        }
+
+                        if (flag) {
+                            if (itemstack1 != null && this.random.nextFloat() - 0.1F < this.dropChances[i]) {
+                                this.a(itemstack1, 0.0F);
+                            }
+
+                            this.setEquipment(i, itemstack);
+                            this.dropChances[i] = 2.0F;
+                            this.persistent = true;
+                            this.receive(entityitem, 1);
+                            entityitem.die();
+                        }
                     }
                 }
             }
@@ -1047,23 +1227,27 @@ public abstract class EntityLiving extends Entity {
         this.world.methodProfiler.b();
     }
 
-    protected boolean aV() {
+    protected void n(Entity entity) {
+        entity.collide(this);
+    }
+
+    protected boolean bb() {
         return false;
     }
 
-    protected boolean aW() {
+    protected boolean bc() {
         return !this.world.isStatic;
     }
 
-    protected boolean aX() {
+    protected boolean bd() {
         return this.health <= 0;
     }
 
-    public boolean aY() {
+    public boolean be() {
         return false;
     }
 
-    protected void aZ() {
+    protected void bf() {
         this.motY = 0.41999998688697815D;
         if (this.hasEffect(MobEffectList.JUMP)) {
             this.motY += (double) ((float) (this.getEffect(MobEffectList.JUMP).getAmplifier() + 1) * 0.1F);
@@ -1076,41 +1260,43 @@ public abstract class EntityLiving extends Entity {
             this.motZ += (double) (MathHelper.cos(f) * 0.2F);
         }
 
-        this.al = true;
+        this.am = true;
     }
 
-    protected boolean ba() {
+    protected boolean bg() {
         return true;
     }
 
-    protected void bb() {
-        EntityHuman entityhuman = this.world.findNearbyPlayer(this, -1.0D);
+    protected void bh() {
+        if (!this.persistent) {
+            EntityHuman entityhuman = this.world.findNearbyPlayer(this, -1.0D);
 
-        if (entityhuman != null) {
-            double d0 = entityhuman.locX - this.locX;
-            double d1 = entityhuman.locY - this.locY;
-            double d2 = entityhuman.locZ - this.locZ;
-            double d3 = d0 * d0 + d1 * d1 + d2 * d2;
+            if (entityhuman != null) {
+                double d0 = entityhuman.locX - this.locX;
+                double d1 = entityhuman.locY - this.locY;
+                double d2 = entityhuman.locZ - this.locZ;
+                double d3 = d0 * d0 + d1 * d1 + d2 * d2;
 
-            if (this.ba() && d3 > 16384.0D) {
-                this.die();
-            }
+                if (this.bg() && d3 > 16384.0D) {
+                    this.die();
+                }
 
-            if (this.bq > 600 && this.random.nextInt(800) == 0 && d3 > 1024.0D && this.ba()) {
-                this.die();
-            } else if (d3 < 1024.0D) {
-                this.bq = 0;
+                if (this.bC > 600 && this.random.nextInt(800) == 0 && d3 > 1024.0D && this.bg()) {
+                    this.die();
+                } else if (d3 < 1024.0D) {
+                    this.bC = 0;
+                }
             }
         }
     }
 
-    protected void bc() {
-        ++this.bq;
+    protected void bi() {
+        ++this.bC;
         this.world.methodProfiler.a("checkDespawn");
-        this.bb();
+        this.bh();
         this.world.methodProfiler.b();
         this.world.methodProfiler.a("sensing");
-        this.bA.a();
+        this.bP.a();
         this.world.methodProfiler.b();
         this.world.methodProfiler.a("targetSelector");
         this.targetSelector.a();
@@ -1122,7 +1308,7 @@ public abstract class EntityLiving extends Entity {
         this.navigation.e();
         this.world.methodProfiler.b();
         this.world.methodProfiler.a("mob tick");
-        this.bd();
+        this.bj();
         this.world.methodProfiler.b();
         this.world.methodProfiler.a("controls");
         this.world.methodProfiler.a("move");
@@ -1135,49 +1321,65 @@ public abstract class EntityLiving extends Entity {
         this.world.methodProfiler.b();
     }
 
-    protected void bd() {}
+    protected void bj() {}
 
-    protected void be() {
-        ++this.bq;
-        this.bb();
-        this.br = 0.0F;
-        this.bs = 0.0F;
+    protected void bk() {
+        ++this.bC;
+        this.bh();
+        this.bD = 0.0F;
+        this.bE = 0.0F;
         float f = 8.0F;
 
         if (this.random.nextFloat() < 0.02F) {
             EntityHuman entityhuman = this.world.findNearbyPlayer(this, (double) f);
 
             if (entityhuman != null) {
-                this.bF = entityhuman;
-                this.bx = 10 + this.random.nextInt(20);
+                this.bX = entityhuman;
+                this.bJ = 10 + this.random.nextInt(20);
             } else {
-                this.bt = (this.random.nextFloat() - 0.5F) * 20.0F;
+                this.bF = (this.random.nextFloat() - 0.5F) * 20.0F;
             }
         }
 
-        if (this.bF != null) {
-            this.a(this.bF, 10.0F, (float) this.bf());
-            if (this.bx-- <= 0 || this.bF.dead || this.bF.e((Entity) this) > (double) (f * f)) {
-                this.bF = null;
+        if (this.bX != null) {
+            this.a(this.bX, 10.0F, (float) this.bm());
+            if (this.bJ-- <= 0 || this.bX.dead || this.bX.e((Entity) this) > (double) (f * f)) {
+                this.bX = null;
             }
         } else {
             if (this.random.nextFloat() < 0.05F) {
-                this.bt = (this.random.nextFloat() - 0.5F) * 20.0F;
+                this.bF = (this.random.nextFloat() - 0.5F) * 20.0F;
             }
 
-            this.yaw += this.bt;
-            this.pitch = this.bv;
+            this.yaw += this.bF;
+            this.pitch = this.bH;
         }
 
         boolean flag = this.H();
         boolean flag1 = this.J();
 
         if (flag || flag1) {
-            this.bu = this.random.nextFloat() < 0.8F;
+            this.bG = this.random.nextFloat() < 0.8F;
+        }
+    }
+
+    protected void bl() {
+        int i = this.i();
+
+        if (this.bq) {
+            ++this.br;
+            if (this.br >= i) {
+                this.br = 0;
+                this.bq = false;
+            }
+        } else {
+            this.br = 0;
         }
+
+        this.aP = (float) this.br / (float) i;
     }
 
-    public int bf() {
+    public int bm() {
         return 40;
     }
 
@@ -1239,7 +1441,7 @@ public abstract class EntityLiving extends Entity {
             f2 = MathHelper.sin(-this.yaw * 0.017453292F - 3.1415927F);
             f3 = -MathHelper.cos(-this.pitch * 0.017453292F);
             f4 = MathHelper.sin(-this.pitch * 0.017453292F);
-            return Vec3D.a().create((double) (f2 * f3), (double) f4, (double) (f1 * f3));
+            return this.world.getVec3DPool().create((double) (f2 * f3), (double) f4, (double) (f1 * f3));
         } else {
             f1 = this.lastPitch + (this.pitch - this.lastPitch) * f;
             f2 = this.lastYaw + (this.yaw - this.lastYaw) * f;
@@ -1248,11 +1450,11 @@ public abstract class EntityLiving extends Entity {
             float f5 = -MathHelper.cos(-f1 * 0.017453292F);
             float f6 = MathHelper.sin(-f1 * 0.017453292F);
 
-            return Vec3D.a().create((double) (f4 * f5), (double) f6, (double) (f3 * f5));
+            return this.world.getVec3DPool().create((double) (f4 * f5), (double) f6, (double) (f3 * f5));
         }
     }
 
-    public int bl() {
+    public int bs() {
         return 4;
     }
 
@@ -1260,7 +1462,7 @@ public abstract class EntityLiving extends Entity {
         return false;
     }
 
-    protected void bo() {
+    protected void bu() {
         Iterator iterator = this.effects.keySet().iterator();
 
         while (iterator.hasNext()) {
@@ -1278,29 +1480,47 @@ public abstract class EntityLiving extends Entity {
         if (this.updateEffects) {
             if (!this.world.isStatic) {
                 if (this.effects.isEmpty()) {
+                    this.datawatcher.watch(9, Byte.valueOf((byte) 0));
                     this.datawatcher.watch(8, Integer.valueOf(0));
+                    this.setInvisible(false);
                 } else {
                     i = PotionBrewer.a(this.effects.values());
+                    this.datawatcher.watch(9, Byte.valueOf((byte) (PotionBrewer.b(this.effects.values()) ? 1 : 0)));
                     this.datawatcher.watch(8, Integer.valueOf(i));
+                    this.setInvisible(this.hasEffect(MobEffectList.INVISIBILITY.id));
                 }
             }
 
             this.updateEffects = false;
         }
 
-        if (this.random.nextBoolean()) {
-            i = this.datawatcher.getInt(8);
-            if (i > 0) {
+        i = this.datawatcher.getInt(8);
+        boolean flag = this.datawatcher.getByte(9) > 0;
+
+        if (i > 0) {
+            boolean flag1 = false;
+
+            if (!this.isInvisible()) {
+                flag1 = this.random.nextBoolean();
+            } else {
+                flag1 = this.random.nextInt(15) == 0;
+            }
+
+            if (flag) {
+                flag1 &= this.random.nextInt(5) == 0;
+            }
+
+            if (flag1 && i > 0) {
                 double d0 = (double) (i >> 16 & 255) / 255.0D;
                 double d1 = (double) (i >> 8 & 255) / 255.0D;
                 double d2 = (double) (i >> 0 & 255) / 255.0D;
 
-                this.world.a("mobSpell", this.locX + (this.random.nextDouble() - 0.5D) * (double) this.width, this.locY + this.random.nextDouble() * (double) this.length - (double) this.height, this.locZ + (this.random.nextDouble() - 0.5D) * (double) this.width, d0, d1, d2);
+                this.world.addParticle(flag ? "mobSpellAmbient" : "mobSpell", this.locX + (this.random.nextDouble() - 0.5D) * (double) this.width, this.locY + this.random.nextDouble() * (double) this.length - (double) this.height, this.locZ + (this.random.nextDouble() - 0.5D) * (double) this.width, d0, d1, d2);
             }
         }
     }
 
-    public void bp() {
+    public void bv() {
         Iterator iterator = this.effects.keySet().iterator();
 
         while (iterator.hasNext()) {
@@ -1318,6 +1538,10 @@ public abstract class EntityLiving extends Entity {
         return this.effects.values();
     }
 
+    public boolean hasEffect(int i) {
+        return this.effects.containsKey(Integer.valueOf(i));
+    }
+
     public boolean hasEffect(MobEffectList mobeffectlist) {
         return this.effects.containsKey(Integer.valueOf(mobeffectlist.id));
     }
@@ -1350,10 +1574,18 @@ public abstract class EntityLiving extends Entity {
         return true;
     }
 
-    public boolean br() {
+    public boolean bx() {
         return this.getMonsterType() == EnumMonsterType.UNDEAD;
     }
 
+    public void o(int i) {
+        MobEffect mobeffect = (MobEffect) this.effects.remove(Integer.valueOf(i));
+
+        if (mobeffect != null) {
+            this.c(mobeffect);
+        }
+    }
+
     protected void a(MobEffect mobeffect) {
         this.updateEffects = true;
     }
@@ -1366,7 +1598,7 @@ public abstract class EntityLiving extends Entity {
         this.updateEffects = true;
     }
 
-    protected float bs() {
+    public float by() {
         float f = 1.0F;
 
         if (this.hasEffect(MobEffectList.FASTER_MOVEMENT)) {
@@ -1396,16 +1628,247 @@ public abstract class EntityLiving extends Entity {
         this.world.makeSound(this, "random.break", 0.8F, 0.8F + this.world.random.nextFloat() * 0.4F);
 
         for (int i = 0; i < 5; ++i) {
-            Vec3D vec3d = Vec3D.a().create(((double) this.random.nextFloat() - 0.5D) * 0.1D, Math.random() * 0.1D + 0.1D, 0.0D);
+            Vec3D vec3d = this.world.getVec3DPool().create(((double) this.random.nextFloat() - 0.5D) * 0.1D, Math.random() * 0.1D + 0.1D, 0.0D);
 
             vec3d.a(-this.pitch * 3.1415927F / 180.0F);
             vec3d.b(-this.yaw * 3.1415927F / 180.0F);
-            Vec3D vec3d1 = Vec3D.a().create(((double) this.random.nextFloat() - 0.5D) * 0.3D, (double) (-this.random.nextFloat()) * 0.6D - 0.3D, 0.6D);
+            Vec3D vec3d1 = this.world.getVec3DPool().create(((double) this.random.nextFloat() - 0.5D) * 0.3D, (double) (-this.random.nextFloat()) * 0.6D - 0.3D, 0.6D);
 
             vec3d1.a(-this.pitch * 3.1415927F / 180.0F);
             vec3d1.b(-this.yaw * 3.1415927F / 180.0F);
             vec3d1 = vec3d1.add(this.locX, this.locY + (double) this.getHeadHeight(), this.locZ);
-            this.world.a("iconcrack_" + itemstack.getItem().id, vec3d1.a, vec3d1.b, vec3d1.c, vec3d.a, vec3d.b + 0.05D, vec3d.c);
+            this.world.addParticle("iconcrack_" + itemstack.getItem().id, vec3d1.c, vec3d1.d, vec3d1.e, vec3d.c, vec3d.d + 0.05D, vec3d.e);
+        }
+    }
+
+    public int as() {
+        if (this.aF() == null) {
+            return 3;
+        } else {
+            int i = (int) ((float) this.health - (float) this.getMaxHealth() * 0.33F);
+
+            i -= (3 - this.world.difficulty) * 4;
+            if (i < 0) {
+                i = 0;
+            }
+
+            return i + 3;
+        }
+    }
+
+    public ItemStack bA() {
+        return this.equipment[0];
+    }
+
+    public ItemStack getEquipment(int i) {
+        return this.equipment[i];
+    }
+
+    public ItemStack q(int i) {
+        return this.equipment[i + 1];
+    }
+
+    public void setEquipment(int i, ItemStack itemstack) {
+        this.equipment[i] = itemstack;
+    }
+
+    public ItemStack[] getEquipment() {
+        return this.equipment;
+    }
+
+    protected void dropEquipment(boolean flag, int i) {
+        for (int j = 0; j < this.getEquipment().length; ++j) {
+            ItemStack itemstack = this.getEquipment(j);
+            boolean flag1 = this.dropChances[j] > 1.0F;
+
+            if (itemstack != null && (flag || flag1) && this.random.nextFloat() - (float) i * 0.01F < this.dropChances[j]) {
+                if (!flag1 && itemstack.f()) {
+                    int k = Math.max(itemstack.k() - 25, 1);
+                    int l = itemstack.k() - this.random.nextInt(this.random.nextInt(k) + 1);
+
+                    if (l > k) {
+                        l = k;
+                    }
+
+                    if (l < 1) {
+                        l = 1;
+                    }
+
+                    itemstack.setData(l);
+                }
+
+                this.a(itemstack, 0.0F);
+            }
         }
     }
+
+    protected void bB() {
+        if (this.random.nextFloat() < d[this.world.difficulty]) {
+            int i = this.random.nextInt(2);
+            float f = this.world.difficulty == 3 ? 0.1F : 0.25F;
+
+            if (this.random.nextFloat() < 0.07F) {
+                ++i;
+            }
+
+            if (this.random.nextFloat() < 0.07F) {
+                ++i;
+            }
+
+            if (this.random.nextFloat() < 0.07F) {
+                ++i;
+            }
+
+            for (int j = 3; j >= 0; --j) {
+                ItemStack itemstack = this.q(j);
+
+                if (j < 3 && this.random.nextFloat() < f) {
+                    break;
+                }
+
+                if (itemstack == null) {
+                    Item item = a(j + 1, i);
+
+                    if (item != null) {
+                        this.setEquipment(j + 1, new ItemStack(item));
+                    }
+                }
+            }
+        }
+    }
+
+    public void receive(Entity entity, int i) {
+        if (!entity.dead && !this.world.isStatic) {
+            EntityTracker entitytracker = ((WorldServer) this.world).getTracker();
+
+            if (entity instanceof EntityItem) {
+                entitytracker.a(entity, new Packet22Collect(entity.id, this.id));
+            }
+
+            if (entity instanceof EntityArrow) {
+                entitytracker.a(entity, new Packet22Collect(entity.id, this.id));
+            }
+
+            if (entity instanceof EntityExperienceOrb) {
+                entitytracker.a(entity, new Packet22Collect(entity.id, this.id));
+            }
+        }
+    }
+
+    public static int b(ItemStack itemstack) {
+        if (itemstack.id != Block.PUMPKIN.id && itemstack.id != Item.SKULL.id) {
+            if (itemstack.getItem() instanceof ItemArmor) {
+                switch (((ItemArmor) itemstack.getItem()).a) {
+                case 0:
+                    return 4;
+
+                case 1:
+                    return 3;
+
+                case 2:
+                    return 2;
+
+                case 3:
+                    return 1;
+                }
+            }
+
+            return 0;
+        } else {
+            return 4;
+        }
+    }
+
+    public static Item a(int i, int j) {
+        switch (i) {
+        case 4:
+            if (j == 0) {
+                return Item.LEATHER_HELMET;
+            } else if (j == 1) {
+                return Item.GOLD_HELMET;
+            } else if (j == 2) {
+                return Item.CHAINMAIL_HELMET;
+            } else if (j == 3) {
+                return Item.IRON_HELMET;
+            } else if (j == 4) {
+                return Item.DIAMOND_HELMET;
+            }
+
+        case 3:
+            if (j == 0) {
+                return Item.LEATHER_CHESTPLATE;
+            } else if (j == 1) {
+                return Item.GOLD_CHESTPLATE;
+            } else if (j == 2) {
+                return Item.CHAINMAIL_CHESTPLATE;
+            } else if (j == 3) {
+                return Item.IRON_CHESTPLATE;
+            } else if (j == 4) {
+                return Item.DIAMOND_CHESTPLATE;
+            }
+
+        case 2:
+            if (j == 0) {
+                return Item.LEATHER_LEGGINGS;
+            } else if (j == 1) {
+                return Item.GOLD_LEGGINGS;
+            } else if (j == 2) {
+                return Item.CHAINMAIL_LEGGINGS;
+            } else if (j == 3) {
+                return Item.IRON_LEGGINGS;
+            } else if (j == 4) {
+                return Item.DIAMOND_LEGGINGS;
+            }
+
+        case 1:
+            if (j == 0) {
+                return Item.LEATHER_BOOTS;
+            } else if (j == 1) {
+                return Item.GOLD_BOOTS;
+            } else if (j == 2) {
+                return Item.CHAINMAIL_BOOTS;
+            } else if (j == 3) {
+                return Item.IRON_BOOTS;
+            } else if (j == 4) {
+                return Item.DIAMOND_BOOTS;
+            }
+
+        default:
+            return null;
+        }
+    }
+
+    protected void bC() {
+        if (this.bA() != null && this.random.nextFloat() < b[this.world.difficulty]) {
+            EnchantmentManager.a(this.random, this.bA(), 5);
+        }
+
+        for (int i = 0; i < 4; ++i) {
+            ItemStack itemstack = this.q(i);
+
+            if (itemstack != null && this.random.nextFloat() < c[this.world.difficulty]) {
+                EnchantmentManager.a(this.random, itemstack, 5);
+            }
+        }
+    }
+
+    public void bD() {}
+
+    private int i() {
+        return this.hasEffect(MobEffectList.FASTER_DIG) ? 6 - (1 + this.getEffect(MobEffectList.FASTER_DIG).getAmplifier()) * 1 : (this.hasEffect(MobEffectList.SLOWER_DIG) ? 6 + (1 + this.getEffect(MobEffectList.SLOWER_DIG).getAmplifier()) * 2 : 6);
+    }
+
+    public void bE() {
+        if (!this.bq || this.br >= this.i() / 2 || this.br < 0) {
+            this.br = -1;
+            this.bq = true;
+            if (this.world instanceof WorldServer) {
+                ((WorldServer) this.world).getTracker().a(this, new Packet18ArmAnimation(this, 1));
+            }
+        }
+    }
+
+    public boolean bF() {
+        return false;
+    }
 }
