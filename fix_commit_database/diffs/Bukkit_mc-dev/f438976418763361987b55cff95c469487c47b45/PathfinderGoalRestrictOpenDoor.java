@@ -10,7 +10,7 @@ public class PathfinderGoalRestrictOpenDoor extends PathfinderGoal {
     }
 
     public boolean a() {
-        if (this.a.world.s()) {
+        if (this.a.world.t()) {
             return false;
         } else {
             Village village = this.a.world.villages.getClosestVillage(MathHelper.floor(this.a.locX), MathHelper.floor(this.a.locY), MathHelper.floor(this.a.locZ), 16);
@@ -25,21 +25,21 @@ public class PathfinderGoalRestrictOpenDoor extends PathfinderGoal {
     }
 
     public boolean b() {
-        return this.a.world.s() ? false : !this.b.g && this.b.a(MathHelper.floor(this.a.locX), MathHelper.floor(this.a.locZ));
+        return this.a.world.t() ? false : !this.b.removed && this.b.a(MathHelper.floor(this.a.locX), MathHelper.floor(this.a.locZ));
     }
 
-    public void e() {
+    public void c() {
         this.a.getNavigation().b(false);
         this.a.getNavigation().c(false);
     }
 
-    public void c() {
+    public void d() {
         this.a.getNavigation().b(true);
         this.a.getNavigation().c(true);
         this.b = null;
     }
 
-    public void d() {
+    public void e() {
         this.b.e();
     }
 }
