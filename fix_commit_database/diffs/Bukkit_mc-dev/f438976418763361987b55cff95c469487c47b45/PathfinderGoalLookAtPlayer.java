@@ -26,13 +26,13 @@ public class PathfinderGoalLookAtPlayer extends PathfinderGoal {
     }
 
     public boolean a() {
-        if (this.b.au().nextFloat() >= this.e) {
+        if (this.b.aA().nextFloat() >= this.e) {
             return false;
         } else {
             if (this.f == EntityHuman.class) {
                 this.a = this.b.world.findNearbyPlayer(this.b, (double) this.c);
             } else {
-                this.a = this.b.world.a(this.f, this.b.boundingBox.grow((double) this.c, 3.0D, (double) this.c), this.b);
+                this.a = this.b.world.a(this.f, this.b.boundingBox.grow((double) this.c, 3.0D, (double) this.c), (Entity) this.b);
             }
 
             return this.a != null;
@@ -43,16 +43,16 @@ public class PathfinderGoalLookAtPlayer extends PathfinderGoal {
         return !this.a.isAlive() ? false : (this.b.e(this.a) > (double) (this.c * this.c) ? false : this.d > 0);
     }
 
-    public void e() {
-        this.d = 40 + this.b.au().nextInt(40);
+    public void c() {
+        this.d = 40 + this.b.aA().nextInt(40);
     }
 
-    public void c() {
+    public void d() {
         this.a = null;
     }
 
-    public void d() {
-        this.b.getControllerLook().a(this.a.locX, this.a.locY + (double) this.a.getHeadHeight(), this.a.locZ, 10.0F, (float) this.b.bf());
+    public void e() {
+        this.b.getControllerLook().a(this.a.locX, this.a.locY + (double) this.a.getHeadHeight(), this.a.locZ, 10.0F, (float) this.b.bm());
         --this.d;
     }
 }
