@@ -34,4 +34,20 @@ public class SharedConstants {
     public static final boolean isAllowedChatCharacter(char c0) {
         return c0 != 167 && (allowedCharacters.indexOf(c0) >= 0 || c0 > 32);
     }
+
+    public static String a(String s) {
+        StringBuilder stringbuilder = new StringBuilder();
+        char[] achar = s.toCharArray();
+        int i = achar.length;
+
+        for (int j = 0; j < i; ++j) {
+            char c0 = achar[j];
+
+            if (isAllowedChatCharacter(c0)) {
+                stringbuilder.append(c0);
+            }
+        }
+
+        return stringbuilder.toString();
+    }
 }
