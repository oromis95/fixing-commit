@@ -5,11 +5,22 @@ public class MobEffect {
     private int effectId;
     private int duration;
     private int amplification;
+    private boolean splash;
+    private boolean ambient;
+
+    public MobEffect(int i, int j) {
+        this(i, j, 0);
+    }
 
     public MobEffect(int i, int j, int k) {
+        this(i, j, k, false);
+    }
+
+    public MobEffect(int i, int j, int k, boolean flag) {
         this.effectId = i;
         this.duration = j;
         this.amplification = k;
+        this.ambient = flag;
     }
 
     public MobEffect(MobEffect mobeffect) {
@@ -28,6 +39,8 @@ public class MobEffect {
             this.duration = mobeffect.duration;
         } else if (mobeffect.amplification == this.amplification && this.duration < mobeffect.duration) {
             this.duration = mobeffect.duration;
+        } else if (!mobeffect.ambient && this.ambient) {
+            this.ambient = mobeffect.ambient;
         }
     }
 
@@ -43,19 +56,27 @@ public class MobEffect {
         return this.amplification;
     }
 
+    public void setSplash(boolean flag) {
+        this.splash = flag;
+    }
+
+    public boolean isAmbient() {
+        return this.ambient;
+    }
+
     public boolean tick(EntityLiving entityliving) {
         if (this.duration > 0) {
             if (MobEffectList.byId[this.effectId].a(this.duration, this.amplification)) {
                 this.b(entityliving);
             }
 
-            this.e();
+            this.g();
         }
 
         return this.duration > 0;
     }
 
-    private int e() {
+    private int g() {
         return --this.duration;
     }
 
@@ -65,7 +86,7 @@ public class MobEffect {
         }
     }
 
-    public String d() {
+    public String f() {
         return MobEffectList.byId[this.effectId].a();
     }
 
@@ -77,9 +98,13 @@ public class MobEffect {
         String s = "";
 
         if (this.getAmplifier() > 0) {
-            s = this.d() + " x " + (this.getAmplifier() + 1) + ", Duration: " + this.getDuration();
+            s = this.f() + " x " + (this.getAmplifier() + 1) + ", Duration: " + this.getDuration();
         } else {
-            s = this.d() + ", Duration: " + this.getDuration();
+            s = this.f() + ", Duration: " + this.getDuration();
+        }
+
+        if (this.splash) {
+            s = s + ", Splash: true";
         }
 
         return MobEffectList.byId[this.effectId].i() ? "(" + s + ")" : s;
@@ -91,7 +116,24 @@ public class MobEffect {
         } else {
             MobEffect mobeffect = (MobEffect) object;
 
-            return this.effectId == mobeffect.effectId && this.amplification == mobeffect.amplification && this.duration == mobeffect.duration;
+            return this.effectId == mobeffect.effectId && this.amplification == mobeffect.amplification && this.duration == mobeffect.duration && this.splash == mobeffect.splash && this.ambient == mobeffect.ambient;
         }
     }
+
+    public NBTTagCompound a(NBTTagCompound nbttagcompound) {
+        nbttagcompound.setByte("Id", (byte) this.getEffectId());
+        nbttagcompound.setByte("Amplifier", (byte) this.getAmplifier());
+        nbttagcompound.setInt("Duration", this.getDuration());
+        nbttagcompound.setBoolean("Ambient", this.isAmbient());
+        return nbttagcompound;
+    }
+
+    public static MobEffect b(NBTTagCompound nbttagcompound) {
+        byte b0 = nbttagcompound.getByte("Id");
+        byte b1 = nbttagcompound.getByte("Amplifier");
+        int i = nbttagcompound.getInt("Duration");
+        boolean flag = nbttagcompound.getBoolean("Ambient");
+
+        return new MobEffect(b0, i, b1, flag);
+    }
 }
