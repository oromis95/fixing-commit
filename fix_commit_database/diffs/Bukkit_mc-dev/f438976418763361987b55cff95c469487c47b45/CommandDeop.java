@@ -6,10 +6,14 @@ public class CommandDeop extends CommandAbstract {
 
     public CommandDeop() {}
 
-    public String b() {
+    public String c() {
         return "deop";
     }
 
+    public int a() {
+        return 3;
+    }
+
     public String a(ICommandListener icommandlistener) {
         return icommandlistener.a("commands.deop.usage", new Object[0]);
     }
