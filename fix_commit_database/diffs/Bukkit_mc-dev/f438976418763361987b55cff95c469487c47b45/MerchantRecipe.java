@@ -6,6 +6,7 @@ public class MerchantRecipe {
     private ItemStack buyingItem2;
     private ItemStack sellingItem;
     private int uses;
+    private int maxUses;
 
     public MerchantRecipe(NBTTagCompound nbttagcompound) {
         this.a(nbttagcompound);
@@ -15,6 +16,7 @@ public class MerchantRecipe {
         this.buyingItem1 = itemstack;
         this.buyingItem2 = itemstack1;
         this.sellingItem = itemstack2;
+        this.maxUses = 7;
     }
 
     public MerchantRecipe(ItemStack itemstack, ItemStack itemstack1) {
@@ -49,14 +51,18 @@ public class MerchantRecipe {
         return this.a(merchantrecipe) && (this.buyingItem1.count < merchantrecipe.buyingItem1.count || this.buyingItem2 != null && this.buyingItem2.count < merchantrecipe.buyingItem2.count);
     }
 
-    public int getUses() {
-        return this.uses;
-    }
-
     public void f() {
         ++this.uses;
     }
 
+    public void a(int i) {
+        this.maxUses += i;
+    }
+
+    public boolean g() {
+        return this.uses >= this.maxUses;
+    }
+
     public void a(NBTTagCompound nbttagcompound) {
         NBTTagCompound nbttagcompound1 = nbttagcompound.getCompound("buy");
 
@@ -71,9 +77,15 @@ public class MerchantRecipe {
         if (nbttagcompound.hasKey("uses")) {
             this.uses = nbttagcompound.getInt("uses");
         }
+
+        if (nbttagcompound.hasKey("maxUses")) {
+            this.maxUses = nbttagcompound.getInt("maxUses");
+        } else {
+            this.maxUses = 7;
+        }
     }
 
-    public NBTTagCompound g() {
+    public NBTTagCompound i() {
         NBTTagCompound nbttagcompound = new NBTTagCompound();
 
         nbttagcompound.setCompound("buy", this.buyingItem1.save(new NBTTagCompound("buy")));
@@ -83,6 +95,7 @@ public class MerchantRecipe {
         }
 
         nbttagcompound.setInt("uses", this.uses);
+        nbttagcompound.setInt("maxUses", this.maxUses);
         return nbttagcompound;
     }
 }
