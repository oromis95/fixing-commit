@@ -6,11 +6,32 @@ import java.util.Random;
 public class WorldGenVillageFarm2 extends WorldGenVillagePiece {
 
     private int a = -1;
+    private int b;
+    private int c;
+    private int d;
+    private int h;
 
     public WorldGenVillageFarm2(WorldGenVillageStartPiece worldgenvillagestartpiece, int i, Random random, StructureBoundingBox structureboundingbox, int j) {
         super(worldgenvillagestartpiece, i);
         this.f = j;
         this.e = structureboundingbox;
+        this.b = this.a(random);
+        this.c = this.a(random);
+        this.d = this.a(random);
+        this.h = this.a(random);
+    }
+
+    private int a(Random random) {
+        switch (random.nextInt(5)) {
+        case 0:
+            return Block.CARROTS.id;
+
+        case 1:
+            return Block.POTATOES.id;
+
+        default:
+            return Block.CROPS.id;
+        }
     }
 
     public static WorldGenVillageFarm2 a(WorldGenVillageStartPiece worldgenvillagestartpiece, List list, Random random, int i, int j, int k, int l, int i1) {
@@ -45,14 +66,14 @@ public class WorldGenVillageFarm2 extends WorldGenVillagePiece {
         int i;
 
         for (i = 1; i <= 7; ++i) {
-            this.a(world, Block.CROPS.id, MathHelper.a(random, 2, 7), 1, 1, i, structureboundingbox);
-            this.a(world, Block.CROPS.id, MathHelper.a(random, 2, 7), 2, 1, i, structureboundingbox);
-            this.a(world, Block.CROPS.id, MathHelper.a(random, 2, 7), 4, 1, i, structureboundingbox);
-            this.a(world, Block.CROPS.id, MathHelper.a(random, 2, 7), 5, 1, i, structureboundingbox);
-            this.a(world, Block.CROPS.id, MathHelper.a(random, 2, 7), 7, 1, i, structureboundingbox);
-            this.a(world, Block.CROPS.id, MathHelper.a(random, 2, 7), 8, 1, i, structureboundingbox);
-            this.a(world, Block.CROPS.id, MathHelper.a(random, 2, 7), 10, 1, i, structureboundingbox);
-            this.a(world, Block.CROPS.id, MathHelper.a(random, 2, 7), 11, 1, i, structureboundingbox);
+            this.a(world, this.b, MathHelper.nextInt(random, 2, 7), 1, 1, i, structureboundingbox);
+            this.a(world, this.b, MathHelper.nextInt(random, 2, 7), 2, 1, i, structureboundingbox);
+            this.a(world, this.c, MathHelper.nextInt(random, 2, 7), 4, 1, i, structureboundingbox);
+            this.a(world, this.c, MathHelper.nextInt(random, 2, 7), 5, 1, i, structureboundingbox);
+            this.a(world, this.d, MathHelper.nextInt(random, 2, 7), 7, 1, i, structureboundingbox);
+            this.a(world, this.d, MathHelper.nextInt(random, 2, 7), 8, 1, i, structureboundingbox);
+            this.a(world, this.h, MathHelper.nextInt(random, 2, 7), 10, 1, i, structureboundingbox);
+            this.a(world, this.h, MathHelper.nextInt(random, 2, 7), 11, 1, i, structureboundingbox);
         }
 
         for (i = 0; i < 9; ++i) {
