@@ -94,9 +94,9 @@ public class Navigation {
                 Vec3D vec3d = this.i();
 
                 this.h = this.g;
-                this.i.a = vec3d.a;
-                this.i.b = vec3d.b;
                 this.i.c = vec3d.c;
+                this.i.d = vec3d.d;
+                this.i.e = vec3d.e;
                 return true;
             }
         }
@@ -117,7 +117,7 @@ public class Navigation {
                 Vec3D vec3d = this.c.a((Entity) this.a);
 
                 if (vec3d != null) {
-                    this.a.getControllerMove().a(vec3d.a, vec3d.b, vec3d.c, this.d);
+                    this.a.getControllerMove().a(vec3d.c, vec3d.d, vec3d.e, this.d);
                 }
             }
         }
@@ -128,7 +128,7 @@ public class Navigation {
         int i = this.c.d();
 
         for (int j = this.c.e(); j < this.c.d(); ++j) {
-            if (this.c.a(j).b != (int) vec3d.b) {
+            if (this.c.a(j).b != (int) vec3d.d) {
                 i = j;
                 break;
             }
@@ -161,9 +161,9 @@ public class Navigation {
             }
 
             this.h = this.g;
-            this.i.a = vec3d.a;
-            this.i.b = vec3d.b;
             this.i.c = vec3d.c;
+            this.i.d = vec3d.d;
+            this.i.e = vec3d.e;
         }
     }
 
@@ -176,7 +176,7 @@ public class Navigation {
     }
 
     private Vec3D i() {
-        return Vec3D.a().create(this.a.locX, (double) this.j(), this.a.locZ);
+        return this.b.getVec3DPool().create(this.a.locX, (double) this.j(), this.a.locZ);
     }
 
     private int j() {
@@ -223,10 +223,10 @@ public class Navigation {
     }
 
     private boolean a(Vec3D vec3d, Vec3D vec3d1, int i, int j, int k) {
-        int l = MathHelper.floor(vec3d.a);
-        int i1 = MathHelper.floor(vec3d.c);
-        double d0 = vec3d1.a - vec3d.a;
-        double d1 = vec3d1.c - vec3d.c;
+        int l = MathHelper.floor(vec3d.c);
+        int i1 = MathHelper.floor(vec3d.e);
+        double d0 = vec3d1.c - vec3d.c;
+        double d1 = vec3d1.e - vec3d.e;
         double d2 = d0 * d0 + d1 * d1;
 
         if (d2 < 1.0E-8D) {
@@ -238,15 +238,15 @@ public class Navigation {
             d1 *= d3;
             i += 2;
             k += 2;
-            if (!this.a(l, (int) vec3d.b, i1, i, j, k, vec3d, d0, d1)) {
+            if (!this.a(l, (int) vec3d.d, i1, i, j, k, vec3d, d0, d1)) {
                 return false;
             } else {
                 i -= 2;
                 k -= 2;
                 double d4 = 1.0D / Math.abs(d0);
                 double d5 = 1.0D / Math.abs(d1);
-                double d6 = (double) (l * 1) - vec3d.a;
-                double d7 = (double) (i1 * 1) - vec3d.c;
+                double d6 = (double) (l * 1) - vec3d.c;
+                double d7 = (double) (i1 * 1) - vec3d.e;
 
                 if (d0 >= 0.0D) {
                     ++d6;
@@ -260,8 +260,8 @@ public class Navigation {
                 d7 /= d1;
                 int j1 = d0 < 0.0D ? -1 : 1;
                 int k1 = d1 < 0.0D ? -1 : 1;
-                int l1 = MathHelper.floor(vec3d1.a);
-                int i2 = MathHelper.floor(vec3d1.c);
+                int l1 = MathHelper.floor(vec3d1.c);
+                int i2 = MathHelper.floor(vec3d1.e);
                 int j2 = l1 - l;
                 int k2 = i2 - i1;
 
@@ -279,7 +279,7 @@ public class Navigation {
                         i1 += k1;
                         k2 = i2 - i1;
                     }
-                } while (this.a(l, (int) vec3d.b, i1, i, j, k, vec3d, d0, d1));
+                } while (this.a(l, (int) vec3d.d, i1, i, j, k, vec3d, d0, d1));
 
                 return false;
             }
@@ -295,8 +295,8 @@ public class Navigation {
         } else {
             for (int i2 = k1; i2 < k1 + l; ++i2) {
                 for (int j2 = l1; j2 < l1 + j1; ++j2) {
-                    double d2 = (double) i2 + 0.5D - vec3d.a;
-                    double d3 = (double) j2 + 0.5D - vec3d.c;
+                    double d2 = (double) i2 + 0.5D - vec3d.c;
+                    double d3 = (double) j2 + 0.5D - vec3d.e;
 
                     if (d2 * d0 + d3 * d1 >= 0.0D) {
                         int k2 = this.b.getTypeId(i2, j - 1, j2);
@@ -326,8 +326,8 @@ public class Navigation {
         for (int k1 = i; k1 < i + l; ++k1) {
             for (int l1 = j; l1 < j + i1; ++l1) {
                 for (int i2 = k; i2 < k + j1; ++i2) {
-                    double d2 = (double) k1 + 0.5D - vec3d.a;
-                    double d3 = (double) i2 + 0.5D - vec3d.c;
+                    double d2 = (double) k1 + 0.5D - vec3d.c;
+                    double d3 = (double) i2 + 0.5D - vec3d.e;
 
                     if (d2 * d0 + d3 * d1 >= 0.0D) {
                         int j2 = this.b.getTypeId(k1, l1, i2);
