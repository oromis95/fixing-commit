@@ -2,18 +2,45 @@ package net.minecraft.server;
 
 import java.util.ArrayList;
 import java.util.Collections;
+import java.util.Iterator;
 import java.util.List;
+import java.util.Map;
 import java.util.Random;
+import java.util.Map.Entry;
 
 public class WorldGenStronghold extends StructureGenerator {
 
     private BiomeBase[] e;
     private boolean f;
     private ChunkCoordIntPair[] g;
+    private double h;
+    private int i;
 
     public WorldGenStronghold() {
         this.e = new BiomeBase[] { BiomeBase.DESERT, BiomeBase.FOREST, BiomeBase.EXTREME_HILLS, BiomeBase.SWAMPLAND, BiomeBase.TAIGA, BiomeBase.ICE_PLAINS, BiomeBase.ICE_MOUNTAINS, BiomeBase.DESERT_HILLS, BiomeBase.FOREST_HILLS, BiomeBase.SMALL_MOUNTAINS, BiomeBase.JUNGLE, BiomeBase.JUNGLE_HILLS};
         this.g = new ChunkCoordIntPair[3];
+        this.h = 32.0D;
+        this.i = 3;
+    }
+
+    public WorldGenStronghold(Map map) {
+        this.e = new BiomeBase[] { BiomeBase.DESERT, BiomeBase.FOREST, BiomeBase.EXTREME_HILLS, BiomeBase.SWAMPLAND, BiomeBase.TAIGA, BiomeBase.ICE_PLAINS, BiomeBase.ICE_MOUNTAINS, BiomeBase.DESERT_HILLS, BiomeBase.FOREST_HILLS, BiomeBase.SMALL_MOUNTAINS, BiomeBase.JUNGLE, BiomeBase.JUNGLE_HILLS};
+        this.g = new ChunkCoordIntPair[3];
+        this.h = 32.0D;
+        this.i = 3;
+        Iterator iterator = map.entrySet().iterator();
+
+        while (iterator.hasNext()) {
+            Entry entry = (Entry) iterator.next();
+
+            if (((String) entry.getKey()).equals("distance")) {
+                this.h = MathHelper.a((String) entry.getValue(), this.h, 1.0D);
+            } else if (((String) entry.getKey()).equals("count")) {
+                this.g = new ChunkCoordIntPair[MathHelper.a((String) entry.getValue(), this.g.length, 1)];
+            } else if (((String) entry.getKey()).equals("spread")) {
+                this.i = MathHelper.a((String) entry.getValue(), this.i, 1);
+            }
+        }
     }
 
     protected boolean a(int i, int j) {
@@ -22,38 +49,40 @@ public class WorldGenStronghold extends StructureGenerator {
 
             random.setSeed(this.c.getSeed());
             double d0 = random.nextDouble() * 3.141592653589793D * 2.0D;
+            int k = 1;
 
-            for (int k = 0; k < this.g.length; ++k) {
-                double d1 = (1.25D + random.nextDouble()) * 32.0D;
-                int l = (int) Math.round(Math.cos(d0) * d1);
-                int i1 = (int) Math.round(Math.sin(d0) * d1);
+            for (int l = 0; l < this.g.length; ++l) {
+                double d1 = (1.25D * (double) k + random.nextDouble()) * this.h * (double) k;
+                int i1 = (int) Math.round(Math.cos(d0) * d1);
+                int j1 = (int) Math.round(Math.sin(d0) * d1);
                 ArrayList arraylist = new ArrayList();
 
                 Collections.addAll(arraylist, this.e);
-                ChunkPosition chunkposition = this.c.getWorldChunkManager().a((l << 4) + 8, (i1 << 4) + 8, 112, arraylist, random);
+                ChunkPosition chunkposition = this.c.getWorldChunkManager().a((i1 << 4) + 8, (j1 << 4) + 8, 112, arraylist, random);
 
                 if (chunkposition != null) {
-                    l = chunkposition.x >> 4;
-                    i1 = chunkposition.z >> 4;
-                } else {
-                    System.out.println("Placed stronghold in INVALID biome at (" + l + ", " + i1 + ")");
+                    i1 = chunkposition.x >> 4;
+                    j1 = chunkposition.z >> 4;
                 }
 
-                this.g[k] = new ChunkCoordIntPair(l, i1);
-                d0 += 6.283185307179586D / (double) this.g.length;
+                this.g[l] = new ChunkCoordIntPair(i1, j1);
+                d0 += 6.283185307179586D * (double) k / (double) this.i;
+                if (l == this.i) {
+                    k += 2 + random.nextInt(5);
+                    this.i += 1 + random.nextInt(2);
+                }
             }
 
             this.f = true;
         }
 
         ChunkCoordIntPair[] achunkcoordintpair = this.g;
-        int j1 = achunkcoordintpair.length;
+        int k1 = achunkcoordintpair.length;
 
-        for (int k1 = 0; k1 < j1; ++k1) {
-            ChunkCoordIntPair chunkcoordintpair = achunkcoordintpair[k1];
+        for (int l1 = 0; l1 < k1; ++l1) {
+            ChunkCoordIntPair chunkcoordintpair = achunkcoordintpair[l1];
 
             if (i == chunkcoordintpair.x && j == chunkcoordintpair.z) {
-                System.out.println(i + ", " + j);
                 return true;
             }
         }
@@ -61,7 +90,7 @@ public class WorldGenStronghold extends StructureGenerator {
         return false;
     }
 
-    protected List o_() {
+    protected List p_() {
         ArrayList arraylist = new ArrayList();
         ChunkCoordIntPair[] achunkcoordintpair = this.g;
         int i = achunkcoordintpair.length;
