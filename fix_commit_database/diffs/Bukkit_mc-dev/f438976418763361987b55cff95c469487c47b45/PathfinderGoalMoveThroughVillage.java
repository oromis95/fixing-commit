@@ -22,7 +22,7 @@ public class PathfinderGoalMoveThroughVillage extends PathfinderGoal {
 
     public boolean a() {
         this.f();
-        if (this.e && this.a.world.s()) {
+        if (this.e && this.a.world.t()) {
             return false;
         } else {
             Village village = this.a.world.villages.getClosestVillage(MathHelper.floor(this.a.locX), MathHelper.floor(this.a.locY), MathHelper.floor(this.a.locZ), 0);
@@ -42,13 +42,13 @@ public class PathfinderGoalMoveThroughVillage extends PathfinderGoal {
                     if (this.c != null) {
                         return true;
                     } else {
-                        Vec3D vec3d = RandomPositionGenerator.a(this.a, 10, 7, Vec3D.a().create((double) this.d.locX, (double) this.d.locY, (double) this.d.locZ));
+                        Vec3D vec3d = RandomPositionGenerator.a(this.a, 10, 7, this.a.world.getVec3DPool().create((double) this.d.locX, (double) this.d.locY, (double) this.d.locZ));
 
                         if (vec3d == null) {
                             return false;
                         } else {
                             this.a.getNavigation().b(false);
-                            this.c = this.a.getNavigation().a(vec3d.a, vec3d.b, vec3d.c);
+                            this.c = this.a.getNavigation().a(vec3d.c, vec3d.d, vec3d.e);
                             this.a.getNavigation().b(flag);
                             return this.c != null;
                         }
@@ -68,11 +68,11 @@ public class PathfinderGoalMoveThroughVillage extends PathfinderGoal {
         }
     }
 
-    public void e() {
+    public void c() {
         this.a.getNavigation().a(this.c, this.b);
     }
 
-    public void c() {
+    public void d() {
         if (this.a.getNavigation().f() || this.a.e((double) this.d.locX, (double) this.d.locY, (double) this.d.locZ) < 16.0D) {
             this.f.add(this.d);
         }
