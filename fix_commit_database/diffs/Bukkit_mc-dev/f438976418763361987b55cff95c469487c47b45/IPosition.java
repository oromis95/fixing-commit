@@ -0,0 +1,10 @@
+package net.minecraft.server;
+
+public interface IPosition {
+
+    double getX();
+
+    double getY();
+
+    double getZ();
+}
