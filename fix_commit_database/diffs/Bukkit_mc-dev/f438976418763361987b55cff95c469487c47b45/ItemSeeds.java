@@ -15,7 +15,7 @@ public class ItemSeeds extends Item {
     public boolean interactWith(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l, float f, float f1, float f2) {
         if (l != 1) {
             return false;
-        } else if (entityhuman.e(i, j, k) && entityhuman.e(i, j + 1, k)) {
+        } else if (entityhuman.a(i, j, k, l, itemstack) && entityhuman.a(i, j + 1, k, l, itemstack)) {
             int i1 = world.getTypeId(i, j, k);
 
             if (i1 == this.b && world.isEmpty(i, j + 1, k)) {
