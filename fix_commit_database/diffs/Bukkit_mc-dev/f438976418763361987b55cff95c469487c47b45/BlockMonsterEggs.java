@@ -22,7 +22,7 @@ public class BlockMonsterEggs extends Block {
 
             entitysilverfish.setPositionRotation((double) i + 0.5D, (double) j, (double) k + 0.5D, 0.0F, 0.0F);
             world.addEntity(entitysilverfish);
-            entitysilverfish.aK();
+            entitysilverfish.aQ();
         }
 
         super.postBreak(world, i, j, k, l);
@@ -40,7 +40,7 @@ public class BlockMonsterEggs extends Block {
         return i == Block.COBBLESTONE.id ? 1 : (i == Block.SMOOTH_BRICK.id ? 2 : 0);
     }
 
-    protected ItemStack c_(int i) {
+    protected ItemStack f_(int i) {
         Block block = Block.STONE;
 
         if (i == 1) {
@@ -53,4 +53,8 @@ public class BlockMonsterEggs extends Block {
 
         return new ItemStack(block);
     }
+
+    public int getDropData(World world, int i, int j, int k) {
+        return world.getData(i, j, k);
+    }
 }
