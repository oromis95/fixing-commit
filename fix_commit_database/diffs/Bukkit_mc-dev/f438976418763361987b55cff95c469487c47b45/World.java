@@ -1,6 +1,7 @@
 package net.minecraft.server;
 
 import java.util.ArrayList;
+import java.util.Calendar;
 import java.util.Collection;
 import java.util.HashSet;
 import java.util.Iterator;
@@ -11,45 +12,47 @@ import java.util.concurrent.Callable;
 
 public abstract class World implements IBlockAccess {
 
-    public boolean e = false;
+    public boolean d = false;
     public List entityList = new ArrayList();
-    protected List g = new ArrayList();
+    protected List f = new ArrayList();
     public List tileEntityList = new ArrayList();
     private List a = new ArrayList();
     private List b = new ArrayList();
     public List players = new ArrayList();
-    public List j = new ArrayList();
+    public List i = new ArrayList();
     private long c = 16777215L;
-    public int k = 0;
-    protected int l = (new Random()).nextInt();
-    protected final int m = 1013904223;
+    public int j = 0;
+    protected int k = (new Random()).nextInt();
+    protected final int l = 1013904223;
+    protected float m;
     protected float n;
     protected float o;
     protected float p;
-    protected float q;
-    protected int r = 0;
-    public int s = 0;
+    protected int q = 0;
+    public int r = 0;
     public boolean suppressPhysics = false;
     public int difficulty;
     public Random random = new Random();
     public final WorldProvider worldProvider;
-    protected List x = new ArrayList();
+    protected List w = new ArrayList();
     protected IChunkProvider chunkProvider;
     protected final IDataManager dataManager;
     protected WorldData worldData;
     public boolean isLoading;
     public WorldMapCollection worldMaps;
-    public final VillageCollection villages = new VillageCollection(this);
+    public final VillageCollection villages;
     protected final VillageSiege siegeManager = new VillageSiege(this);
     public final MethodProfiler methodProfiler;
-    private ArrayList d = new ArrayList();
-    private boolean L;
+    private final Vec3DPool K = new Vec3DPool(300, 2000);
+    private final Calendar L = Calendar.getInstance();
+    private ArrayList M = new ArrayList();
+    private boolean N;
     protected boolean allowMonsters = true;
     protected boolean allowAnimals = true;
     protected Set chunkTickList = new HashSet();
-    private int M;
-    int[] J;
-    private List N;
+    private int O;
+    int[] I;
+    private List P;
     public boolean isStatic;
 
     public BiomeBase getBiome(int i, int j) {
@@ -57,21 +60,21 @@ public abstract class World implements IBlockAccess {
             Chunk chunk = this.getChunkAtWorldCoords(i, j);
 
             if (chunk != null) {
-                return chunk.a(i & 15, j & 15, this.worldProvider.c);
+                return chunk.a(i & 15, j & 15, this.worldProvider.d);
             }
         }
 
-        return this.worldProvider.c.getBiome(i, j);
+        return this.worldProvider.d.getBiome(i, j);
     }
 
     public WorldChunkManager getWorldChunkManager() {
-        return this.worldProvider.c;
+        return this.worldProvider.d;
     }
 
     public World(IDataManager idatamanager, String s, WorldSettings worldsettings, WorldProvider worldprovider, MethodProfiler methodprofiler) {
-        this.M = this.random.nextInt(12000);
-        this.J = new int['\u8000'];
-        this.N = new ArrayList();
+        this.O = this.random.nextInt(12000);
+        this.I = new int['\u8000'];
+        this.P = new ArrayList();
         this.isStatic = false;
         this.dataManager = idatamanager;
         this.methodProfiler = methodprofiler;
@@ -79,8 +82,8 @@ public abstract class World implements IBlockAccess {
         this.worldData = idatamanager.getWorldData();
         if (worldprovider != null) {
             this.worldProvider = worldprovider;
-        } else if (this.worldData != null && this.worldData.i() != 0) {
-            this.worldProvider = WorldProvider.byDimension(this.worldData.i());
+        } else if (this.worldData != null && this.worldData.j() != 0) {
+            this.worldProvider = WorldProvider.byDimension(this.worldData.j());
         } else {
             this.worldProvider = WorldProvider.byDimension(0);
         }
@@ -92,17 +95,27 @@ public abstract class World implements IBlockAccess {
         }
 
         this.worldProvider.a(this);
-        this.chunkProvider = this.i();
+        this.chunkProvider = this.j();
         if (!this.worldData.isInitialized()) {
             this.a(worldsettings);
             this.worldData.d(true);
         }
 
-        this.v();
+        VillageCollection villagecollection = (VillageCollection) this.worldMaps.get(VillageCollection.class, "villages");
+
+        if (villagecollection == null) {
+            this.villages = new VillageCollection(this);
+            this.worldMaps.a("villages", this.villages);
+        } else {
+            this.villages = villagecollection;
+            this.villages.a(this);
+        }
+
+        this.w();
         this.a();
     }
 
-    protected abstract IChunkProvider i();
+    protected abstract IChunkProvider j();
 
     protected void a(WorldSettings worldsettings) {
         this.worldData.d(true);
@@ -133,7 +146,7 @@ public abstract class World implements IBlockAccess {
     public boolean isTileEntity(int i, int j, int k) {
         int l = this.getTypeId(i, j, k);
 
-        return Block.byId[l] != null && Block.byId[l].s();
+        return Block.byId[l] != null && Block.byId[l].u();
     }
 
     public boolean isLoaded(int i, int j, int k) {
@@ -141,10 +154,10 @@ public abstract class World implements IBlockAccess {
     }
 
     public boolean areChunksLoaded(int i, int j, int k, int l) {
-        return this.c(i - l, j - l, k - l, i + l, j + l, k + l);
+        return this.d(i - l, j - l, k - l, i + l, j + l, k + l);
     }
 
-    public boolean c(int i, int j, int k, int l, int i1, int j1) {
+    public boolean d(int i, int j, int k, int l, int i1, int j1) {
         if (i1 >= 0 && j < 256) {
             i >>= 4;
             k >>= 4;
@@ -271,7 +284,7 @@ public abstract class World implements IBlockAccess {
                 int j1 = k & 15;
                 boolean flag = chunk.b(i1, j, j1, l);
 
-                if (flag && (this.isStatic || chunk.seenByPlayer && Block.r[chunk.getTypeId(i1, j, j1) & 4095])) {
+                if (flag && (this.isStatic || chunk.seenByPlayer && Block.u[chunk.getTypeId(i1, j, j1) & 4095])) {
                     this.notify(i, j, k);
                 }
 
@@ -301,7 +314,7 @@ public abstract class World implements IBlockAccess {
     }
 
     public void notify(int i, int j, int k) {
-        Iterator iterator = this.x.iterator();
+        Iterator iterator = this.w.iterator();
 
         while (iterator.hasNext()) {
             IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
@@ -323,17 +336,17 @@ public abstract class World implements IBlockAccess {
             k = i1;
         }
 
-        if (!this.worldProvider.e) {
+        if (!this.worldProvider.f) {
             for (i1 = k; i1 <= l; ++i1) {
                 this.c(EnumSkyBlock.SKY, i, i1, j);
             }
         }
 
-        this.d(i, k, j, i, l, j);
+        this.e(i, k, j, i, l, j);
     }
 
     public void i(int i, int j, int k) {
-        Iterator iterator = this.x.iterator();
+        Iterator iterator = this.w.iterator();
 
         while (iterator.hasNext()) {
             IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
@@ -342,8 +355,8 @@ public abstract class World implements IBlockAccess {
         }
     }
 
-    public void d(int i, int j, int k, int l, int i1, int j1) {
-        Iterator iterator = this.x.iterator();
+    public void e(int i, int j, int k, int l, int i1, int j1) {
+        Iterator iterator = this.w.iterator();
 
         while (iterator.hasNext()) {
             IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
@@ -434,7 +447,7 @@ public abstract class World implements IBlockAccess {
 
                 i &= 15;
                 k &= 15;
-                return chunk.c(i, j, k, this.k);
+                return chunk.c(i, j, k, this.j);
             }
         } else {
             return 15;
@@ -455,6 +468,20 @@ public abstract class World implements IBlockAccess {
         }
     }
 
+    public int g(int i, int j) {
+        if (i >= -30000000 && j >= -30000000 && i < 30000000 && j < 30000000) {
+            if (!this.isChunkLoaded(i >> 4, j >> 4)) {
+                return 0;
+            } else {
+                Chunk chunk = this.getChunkAt(i >> 4, j >> 4);
+
+                return chunk.p;
+            }
+        } else {
+            return 0;
+        }
+    }
+
     public int b(EnumSkyBlock enumskyblock, int i, int j, int k) {
         if (j < 0) {
             j = 0;
@@ -488,7 +515,7 @@ public abstract class World implements IBlockAccess {
                         Chunk chunk = this.getChunkAt(i >> 4, k >> 4);
 
                         chunk.a(enumskyblock, i & 15, j, k & 15, l);
-                        Iterator iterator = this.x.iterator();
+                        Iterator iterator = this.w.iterator();
 
                         while (iterator.hasNext()) {
                             IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
@@ -502,7 +529,7 @@ public abstract class World implements IBlockAccess {
     }
 
     public void n(int i, int j, int k) {
-        Iterator iterator = this.x.iterator();
+        Iterator iterator = this.w.iterator();
 
         while (iterator.hasNext()) {
             IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
@@ -512,11 +539,11 @@ public abstract class World implements IBlockAccess {
     }
 
     public float o(int i, int j, int k) {
-        return this.worldProvider.f[this.getLightLevel(i, j, k)];
+        return this.worldProvider.g[this.getLightLevel(i, j, k)];
     }
 
-    public boolean s() {
-        return this.k < 4;
+    public boolean t() {
+        return this.j < 4;
     }
 
     public MovingObjectPosition a(Vec3D vec3d, Vec3D vec3d1) {
@@ -528,14 +555,14 @@ public abstract class World implements IBlockAccess {
     }
 
     public MovingObjectPosition rayTrace(Vec3D vec3d, Vec3D vec3d1, boolean flag, boolean flag1) {
-        if (!Double.isNaN(vec3d.a) && !Double.isNaN(vec3d.b) && !Double.isNaN(vec3d.c)) {
-            if (!Double.isNaN(vec3d1.a) && !Double.isNaN(vec3d1.b) && !Double.isNaN(vec3d1.c)) {
-                int i = MathHelper.floor(vec3d1.a);
-                int j = MathHelper.floor(vec3d1.b);
-                int k = MathHelper.floor(vec3d1.c);
-                int l = MathHelper.floor(vec3d.a);
-                int i1 = MathHelper.floor(vec3d.b);
-                int j1 = MathHelper.floor(vec3d.c);
+        if (!Double.isNaN(vec3d.c) && !Double.isNaN(vec3d.d) && !Double.isNaN(vec3d.e)) {
+            if (!Double.isNaN(vec3d1.c) && !Double.isNaN(vec3d1.d) && !Double.isNaN(vec3d1.e)) {
+                int i = MathHelper.floor(vec3d1.c);
+                int j = MathHelper.floor(vec3d1.d);
+                int k = MathHelper.floor(vec3d1.e);
+                int l = MathHelper.floor(vec3d.c);
+                int i1 = MathHelper.floor(vec3d.d);
+                int j1 = MathHelper.floor(vec3d.e);
                 int k1 = this.getTypeId(l, i1, j1);
                 int l1 = this.getData(l, i1, j1);
                 Block block = Block.byId[k1];
@@ -551,7 +578,7 @@ public abstract class World implements IBlockAccess {
                 k1 = 200;
 
                 while (k1-- >= 0) {
-                    if (Double.isNaN(vec3d.a) || Double.isNaN(vec3d.b) || Double.isNaN(vec3d.c)) {
+                    if (Double.isNaN(vec3d.c) || Double.isNaN(vec3d.d) || Double.isNaN(vec3d.e)) {
                         return null;
                     }
 
@@ -593,20 +620,20 @@ public abstract class World implements IBlockAccess {
                     double d3 = 999.0D;
                     double d4 = 999.0D;
                     double d5 = 999.0D;
-                    double d6 = vec3d1.a - vec3d.a;
-                    double d7 = vec3d1.b - vec3d.b;
-                    double d8 = vec3d1.c - vec3d.c;
+                    double d6 = vec3d1.c - vec3d.c;
+                    double d7 = vec3d1.d - vec3d.d;
+                    double d8 = vec3d1.e - vec3d.e;
 
                     if (flag2) {
-                        d3 = (d0 - vec3d.a) / d6;
+                        d3 = (d0 - vec3d.c) / d6;
                     }
 
                     if (flag3) {
-                        d4 = (d1 - vec3d.b) / d7;
+                        d4 = (d1 - vec3d.d) / d7;
                     }
 
                     if (flag4) {
-                        d5 = (d2 - vec3d.c) / d8;
+                        d5 = (d2 - vec3d.e) / d8;
                     }
 
                     boolean flag5 = false;
@@ -619,9 +646,9 @@ public abstract class World implements IBlockAccess {
                             b0 = 5;
                         }
 
-                        vec3d.a = d0;
-                        vec3d.b += d7 * d3;
-                        vec3d.c += d8 * d3;
+                        vec3d.c = d0;
+                        vec3d.d += d7 * d3;
+                        vec3d.e += d8 * d3;
                     } else if (d4 < d5) {
                         if (j > i1) {
                             b0 = 0;
@@ -629,9 +656,9 @@ public abstract class World implements IBlockAccess {
                             b0 = 1;
                         }
 
-                        vec3d.a += d6 * d4;
-                        vec3d.b = d1;
-                        vec3d.c += d8 * d4;
+                        vec3d.c += d6 * d4;
+                        vec3d.d = d1;
+                        vec3d.e += d8 * d4;
                     } else {
                         if (k > j1) {
                             b0 = 2;
@@ -639,29 +666,29 @@ public abstract class World implements IBlockAccess {
                             b0 = 3;
                         }
 
-                        vec3d.a += d6 * d5;
-                        vec3d.b += d7 * d5;
-                        vec3d.c = d2;
+                        vec3d.c += d6 * d5;
+                        vec3d.d += d7 * d5;
+                        vec3d.e = d2;
                     }
 
-                    Vec3D vec3d2 = Vec3D.a().create(vec3d.a, vec3d.b, vec3d.c);
+                    Vec3D vec3d2 = this.getVec3DPool().create(vec3d.c, vec3d.d, vec3d.e);
 
-                    l = (int) (vec3d2.a = (double) MathHelper.floor(vec3d.a));
+                    l = (int) (vec3d2.c = (double) MathHelper.floor(vec3d.c));
                     if (b0 == 5) {
                         --l;
-                        ++vec3d2.a;
+                        ++vec3d2.c;
                     }
 
-                    i1 = (int) (vec3d2.b = (double) MathHelper.floor(vec3d.b));
+                    i1 = (int) (vec3d2.d = (double) MathHelper.floor(vec3d.d));
                     if (b0 == 1) {
                         --i1;
-                        ++vec3d2.b;
+                        ++vec3d2.d;
                     }
 
-                    j1 = (int) (vec3d2.c = (double) MathHelper.floor(vec3d.c));
+                    j1 = (int) (vec3d2.e = (double) MathHelper.floor(vec3d.e));
                     if (b0 == 3) {
                         --j1;
-                        ++vec3d2.c;
+                        ++vec3d2.e;
                     }
 
                     int i2 = this.getTypeId(l, i1, j1);
@@ -688,7 +715,7 @@ public abstract class World implements IBlockAccess {
 
     public void makeSound(Entity entity, String s, float f, float f1) {
         if (entity != null && s != null) {
-            Iterator iterator = this.x.iterator();
+            Iterator iterator = this.w.iterator();
 
             while (iterator.hasNext()) {
                 IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
@@ -700,7 +727,7 @@ public abstract class World implements IBlockAccess {
 
     public void makeSound(double d0, double d1, double d2, String s, float f, float f1) {
         if (s != null) {
-            Iterator iterator = this.x.iterator();
+            Iterator iterator = this.w.iterator();
 
             while (iterator.hasNext()) {
                 IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
@@ -710,8 +737,10 @@ public abstract class World implements IBlockAccess {
         }
     }
 
+    public void b(double d0, double d1, double d2, String s, float f, float f1) {}
+
     public void a(String s, int i, int j, int k) {
-        Iterator iterator = this.x.iterator();
+        Iterator iterator = this.w.iterator();
 
         while (iterator.hasNext()) {
             IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
@@ -720,8 +749,8 @@ public abstract class World implements IBlockAccess {
         }
     }
 
-    public void a(String s, double d0, double d1, double d2, double d3, double d4, double d5) {
-        Iterator iterator = this.x.iterator();
+    public void addParticle(String s, double d0, double d1, double d2, double d3, double d4, double d5) {
+        Iterator iterator = this.w.iterator();
 
         while (iterator.hasNext()) {
             IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
@@ -731,7 +760,7 @@ public abstract class World implements IBlockAccess {
     }
 
     public boolean strikeLightning(Entity entity) {
-        this.j.add(entity);
+        this.i.add(entity);
         return true;
     }
 
@@ -762,7 +791,7 @@ public abstract class World implements IBlockAccess {
     }
 
     protected void a(Entity entity) {
-        Iterator iterator = this.x.iterator();
+        Iterator iterator = this.w.iterator();
 
         while (iterator.hasNext()) {
             IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
@@ -772,7 +801,7 @@ public abstract class World implements IBlockAccess {
     }
 
     protected void b(Entity entity) {
-        Iterator iterator = this.x.iterator();
+        Iterator iterator = this.w.iterator();
 
         while (iterator.hasNext()) {
             IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
@@ -804,10 +833,10 @@ public abstract class World implements IBlockAccess {
             this.everyoneSleeping();
         }
 
-        int i = entity.ah;
-        int j = entity.aj;
+        int i = entity.ai;
+        int j = entity.ak;
 
-        if (entity.ag && this.isChunkLoaded(i, j)) {
+        if (entity.ah && this.isChunkLoaded(i, j)) {
             this.getChunkAt(i, j).b(entity);
         }
 
@@ -816,11 +845,11 @@ public abstract class World implements IBlockAccess {
     }
 
     public void addIWorldAccess(IWorldAccess iworldaccess) {
-        this.x.add(iworldaccess);
+        this.w.add(iworldaccess);
     }
 
     public List getCubes(Entity entity, AxisAlignedBB axisalignedbb) {
-        this.d.clear();
+        this.M.clear();
         int i = MathHelper.floor(axisalignedbb.a);
         int j = MathHelper.floor(axisalignedbb.d + 1.0D);
         int k = MathHelper.floor(axisalignedbb.b);
@@ -835,7 +864,7 @@ public abstract class World implements IBlockAccess {
                         Block block = Block.byId[this.getTypeId(k1, i2, l1)];
 
                         if (block != null) {
-                            block.a(this, k1, i2, l1, axisalignedbb, this.d, entity);
+                            block.a(this, k1, i2, l1, axisalignedbb, this.M, entity);
                         }
                     }
                 }
@@ -851,20 +880,20 @@ public abstract class World implements IBlockAccess {
             AxisAlignedBB axisalignedbb1 = entity1.E();
 
             if (axisalignedbb1 != null && axisalignedbb1.a(axisalignedbb)) {
-                this.d.add(axisalignedbb1);
+                this.M.add(axisalignedbb1);
             }
 
             axisalignedbb1 = entity.g(entity1);
             if (axisalignedbb1 != null && axisalignedbb1.a(axisalignedbb)) {
-                this.d.add(axisalignedbb1);
+                this.M.add(axisalignedbb1);
             }
         }
 
-        return this.d;
+        return this.M;
     }
 
     public List a(AxisAlignedBB axisalignedbb) {
-        this.d.clear();
+        this.M.clear();
         int i = MathHelper.floor(axisalignedbb.a);
         int j = MathHelper.floor(axisalignedbb.d + 1.0D);
         int k = MathHelper.floor(axisalignedbb.b);
@@ -879,14 +908,14 @@ public abstract class World implements IBlockAccess {
                         Block block = Block.byId[this.getTypeId(k1, i2, l1)];
 
                         if (block != null) {
-                            block.a(this, k1, i2, l1, axisalignedbb, this.d, (Entity) null);
+                            block.a(this, k1, i2, l1, axisalignedbb, this.M, (Entity) null);
                         }
                     }
                 }
             }
         }
 
-        return this.d;
+        return this.M;
     }
 
     public int a(float f) {
@@ -909,14 +938,14 @@ public abstract class World implements IBlockAccess {
     }
 
     public float c(float f) {
-        return this.worldProvider.a(this.worldData.getTime(), f);
+        return this.worldProvider.a(this.worldData.g(), f);
     }
 
-    public int g(int i, int j) {
+    public int h(int i, int j) {
         return this.getChunkAtWorldCoords(i, j).d(i & 15, j & 15);
     }
 
-    public int h(int i, int j) {
+    public int i(int i, int j) {
         Chunk chunk = this.getChunkAtWorldCoords(i, j);
         int k = chunk.h() + 15;
 
@@ -935,6 +964,8 @@ public abstract class World implements IBlockAccess {
 
     public void a(int i, int j, int k, int l, int i1) {}
 
+    public void a(int i, int j, int k, int l, int i1, int j1) {}
+
     public void b(int i, int j, int k, int l, int i1) {}
 
     public void tickEntities() {
@@ -944,38 +975,38 @@ public abstract class World implements IBlockAccess {
         int i;
         Entity entity;
 
-        for (i = 0; i < this.j.size(); ++i) {
-            entity = (Entity) this.j.get(i);
-            entity.h_();
+        for (i = 0; i < this.i.size(); ++i) {
+            entity = (Entity) this.i.get(i);
+            entity.j_();
             if (entity.dead) {
-                this.j.remove(i--);
+                this.i.remove(i--);
             }
         }
 
         this.methodProfiler.c("remove");
-        this.entityList.removeAll(this.g);
-        Iterator iterator = this.g.iterator();
+        this.entityList.removeAll(this.f);
+        Iterator iterator = this.f.iterator();
 
         int j;
         int k;
 
         while (iterator.hasNext()) {
             entity = (Entity) iterator.next();
-            j = entity.ah;
-            k = entity.aj;
-            if (entity.ag && this.isChunkLoaded(j, k)) {
+            j = entity.ai;
+            k = entity.ak;
+            if (entity.ah && this.isChunkLoaded(j, k)) {
                 this.getChunkAt(j, k).b(entity);
             }
         }
 
-        iterator = this.g.iterator();
+        iterator = this.f.iterator();
 
         while (iterator.hasNext()) {
             entity = (Entity) iterator.next();
             this.b(entity);
         }
 
-        this.g.clear();
+        this.f.clear();
         this.methodProfiler.c("regular");
 
         for (i = 0; i < this.entityList.size(); ++i) {
@@ -997,9 +1028,9 @@ public abstract class World implements IBlockAccess {
             this.methodProfiler.b();
             this.methodProfiler.a("remove");
             if (entity.dead) {
-                j = entity.ah;
-                k = entity.aj;
-                if (entity.ag && this.isChunkLoaded(j, k)) {
+                j = entity.ai;
+                k = entity.ak;
+                if (entity.ah && this.isChunkLoaded(j, k)) {
                     this.getChunkAt(j, k).b(entity);
                 }
 
@@ -1011,17 +1042,17 @@ public abstract class World implements IBlockAccess {
         }
 
         this.methodProfiler.c("tileEntities");
-        this.L = true;
+        this.N = true;
         iterator = this.tileEntityList.iterator();
 
         while (iterator.hasNext()) {
             TileEntity tileentity = (TileEntity) iterator.next();
 
-            if (!tileentity.p() && tileentity.m() && this.isLoaded(tileentity.x, tileentity.y, tileentity.z)) {
+            if (!tileentity.r() && tileentity.o() && this.isLoaded(tileentity.x, tileentity.y, tileentity.z)) {
                 tileentity.g();
             }
 
-            if (tileentity.p()) {
+            if (tileentity.r()) {
                 iterator.remove();
                 if (this.isChunkLoaded(tileentity.x >> 4, tileentity.z >> 4)) {
                     Chunk chunk = this.getChunkAt(tileentity.x >> 4, tileentity.z >> 4);
@@ -1033,7 +1064,7 @@ public abstract class World implements IBlockAccess {
             }
         }
 
-        this.L = false;
+        this.N = false;
         if (!this.b.isEmpty()) {
             this.tileEntityList.removeAll(this.b);
             this.b.clear();
@@ -1046,7 +1077,7 @@ public abstract class World implements IBlockAccess {
             while (iterator1.hasNext()) {
                 TileEntity tileentity1 = (TileEntity) iterator1.next();
 
-                if (!tileentity1.p()) {
+                if (!tileentity1.r()) {
                     if (!this.tileEntityList.contains(tileentity1)) {
                         this.tileEntityList.add(tileentity1);
                     }
@@ -1071,7 +1102,7 @@ public abstract class World implements IBlockAccess {
     }
 
     public void a(Collection collection) {
-        if (this.L) {
+        if (this.N) {
             this.a.addAll(collection);
         } else {
             this.tileEntityList.addAll(collection);
@@ -1087,31 +1118,31 @@ public abstract class World implements IBlockAccess {
         int j = MathHelper.floor(entity.locZ);
         byte b0 = 32;
 
-        if (!flag || this.c(i - b0, 0, j - b0, i + b0, 0, j + b0)) {
-            entity.S = entity.locX;
-            entity.T = entity.locY;
-            entity.U = entity.locZ;
+        if (!flag || this.d(i - b0, 0, j - b0, i + b0, 0, j + b0)) {
+            entity.T = entity.locX;
+            entity.U = entity.locY;
+            entity.V = entity.locZ;
             entity.lastYaw = entity.yaw;
             entity.lastPitch = entity.pitch;
-            if (flag && entity.ag) {
+            if (flag && entity.ah) {
                 if (entity.vehicle != null) {
                     entity.U();
                 } else {
-                    entity.h_();
+                    entity.j_();
                 }
             }
 
             this.methodProfiler.a("chunkCheck");
             if (Double.isNaN(entity.locX) || Double.isInfinite(entity.locX)) {
-                entity.locX = entity.S;
+                entity.locX = entity.T;
             }
 
             if (Double.isNaN(entity.locY) || Double.isInfinite(entity.locY)) {
-                entity.locY = entity.T;
+                entity.locY = entity.U;
             }
 
             if (Double.isNaN(entity.locZ) || Double.isInfinite(entity.locZ)) {
-                entity.locZ = entity.U;
+                entity.locZ = entity.V;
             }
 
             if (Double.isNaN((double) entity.pitch) || Double.isInfinite((double) entity.pitch)) {
@@ -1126,21 +1157,21 @@ public abstract class World implements IBlockAccess {
             int l = MathHelper.floor(entity.locY / 16.0D);
             int i1 = MathHelper.floor(entity.locZ / 16.0D);
 
-            if (!entity.ag || entity.ah != k || entity.ai != l || entity.aj != i1) {
-                if (entity.ag && this.isChunkLoaded(entity.ah, entity.aj)) {
-                    this.getChunkAt(entity.ah, entity.aj).a(entity, entity.ai);
+            if (!entity.ah || entity.ai != k || entity.aj != l || entity.ak != i1) {
+                if (entity.ah && this.isChunkLoaded(entity.ai, entity.ak)) {
+                    this.getChunkAt(entity.ai, entity.ak).a(entity, entity.aj);
                 }
 
                 if (this.isChunkLoaded(k, i1)) {
-                    entity.ag = true;
+                    entity.ah = true;
                     this.getChunkAt(k, i1).a(entity);
                 } else {
-                    entity.ag = false;
+                    entity.ah = false;
                 }
             }
 
             this.methodProfiler.b();
-            if (flag && entity.ag && entity.passenger != null) {
+            if (flag && entity.ah && entity.passenger != null) {
                 if (!entity.passenger.dead && entity.passenger.vehicle == entity) {
                     this.playerJoinedWorld(entity.passenger);
                 } else {
@@ -1250,7 +1281,7 @@ public abstract class World implements IBlockAccess {
         int i1 = MathHelper.floor(axisalignedbb.c);
         int j1 = MathHelper.floor(axisalignedbb.f + 1.0D);
 
-        if (this.c(i, k, i1, j, l, j1)) {
+        if (this.d(i, k, i1, j, l, j1)) {
             for (int k1 = i; k1 < j; ++k1) {
                 for (int l1 = k; l1 < l; ++l1) {
                     for (int i2 = i1; i2 < j1; ++i2) {
@@ -1275,11 +1306,11 @@ public abstract class World implements IBlockAccess {
         int i1 = MathHelper.floor(axisalignedbb.c);
         int j1 = MathHelper.floor(axisalignedbb.f + 1.0D);
 
-        if (!this.c(i, k, i1, j, l, j1)) {
+        if (!this.d(i, k, i1, j, l, j1)) {
             return false;
         } else {
             boolean flag = false;
-            Vec3D vec3d = Vec3D.a().create(0.0D, 0.0D, 0.0D);
+            Vec3D vec3d = this.getVec3DPool().create(0.0D, 0.0D, 0.0D);
 
             for (int k1 = i; k1 < j; ++k1) {
                 for (int l1 = k; l1 < l; ++l1) {
@@ -1298,13 +1329,13 @@ public abstract class World implements IBlockAccess {
                 }
             }
 
-            if (vec3d.c() > 0.0D) {
-                vec3d = vec3d.b();
+            if (vec3d.b() > 0.0D) {
+                vec3d = vec3d.a();
                 double d1 = 0.014D;
 
-                entity.motX += vec3d.a * d1;
-                entity.motY += vec3d.b * d1;
-                entity.motZ += vec3d.c * d1;
+                entity.motX += vec3d.c * d1;
+                entity.motY += vec3d.d * d1;
+                entity.motZ += vec3d.e * d1;
             }
 
             return flag;
@@ -1366,14 +1397,15 @@ public abstract class World implements IBlockAccess {
         return false;
     }
 
-    public Explosion explode(Entity entity, double d0, double d1, double d2, float f) {
-        return this.createExplosion(entity, d0, d1, d2, f, false);
+    public Explosion explode(Entity entity, double d0, double d1, double d2, float f, boolean flag) {
+        return this.createExplosion(entity, d0, d1, d2, f, false, flag);
     }
 
-    public Explosion createExplosion(Entity entity, double d0, double d1, double d2, float f, boolean flag) {
+    public Explosion createExplosion(Entity entity, double d0, double d1, double d2, float f, boolean flag, boolean flag1) {
         Explosion explosion = new Explosion(this, entity, d0, d1, d2, f);
 
         explosion.a = flag;
+        explosion.b = flag1;
         explosion.a();
         explosion.a(true);
         return explosion;
@@ -1393,7 +1425,7 @@ public abstract class World implements IBlockAccess {
                     double d4 = axisalignedbb.b + (axisalignedbb.e - axisalignedbb.b) * (double) f1;
                     double d5 = axisalignedbb.c + (axisalignedbb.f - axisalignedbb.c) * (double) f2;
 
-                    if (this.a(Vec3D.a().create(d3, d4, d5), vec3d) == null) {
+                    if (this.a(this.getVec3DPool().create(d3, d4, d5), vec3d) == null) {
                         ++i;
                     }
 
@@ -1456,7 +1488,7 @@ public abstract class World implements IBlockAccess {
                     while (iterator.hasNext()) {
                         TileEntity tileentity1 = (TileEntity) iterator.next();
 
-                        if (!tileentity1.p() && tileentity1.x == i && tileentity1.y == j && tileentity1.z == k) {
+                        if (!tileentity1.r() && tileentity1.x == i && tileentity1.y == j && tileentity1.z == k) {
                             tileentity = tileentity1;
                             break;
                         }
@@ -1469,8 +1501,8 @@ public abstract class World implements IBlockAccess {
     }
 
     public void setTileEntity(int i, int j, int k, TileEntity tileentity) {
-        if (tileentity != null && !tileentity.p()) {
-            if (this.L) {
+        if (tileentity != null && !tileentity.r()) {
+            if (this.N) {
                 tileentity.x = i;
                 tileentity.y = j;
                 tileentity.z = k;
@@ -1489,8 +1521,8 @@ public abstract class World implements IBlockAccess {
     public void q(int i, int j, int k) {
         TileEntity tileentity = this.getTileEntity(i, j, k);
 
-        if (tileentity != null && this.L) {
-            tileentity.j();
+        if (tileentity != null && this.N) {
+            tileentity.w_();
             this.a.remove(tileentity);
         } else {
             if (tileentity != null) {
@@ -1513,7 +1545,7 @@ public abstract class World implements IBlockAccess {
     public boolean r(int i, int j, int k) {
         Block block = Block.byId[this.getTypeId(i, j, k)];
 
-        return block == null ? false : block.d();
+        return block == null ? false : block.c();
     }
 
     public boolean s(int i, int j, int k) {
@@ -1523,7 +1555,7 @@ public abstract class World implements IBlockAccess {
     public boolean t(int i, int j, int k) {
         Block block = Block.byId[this.getTypeId(i, j, k)];
 
-        return block == null ? false : (block.material.k() && block.c() ? true : (block instanceof BlockStairs ? (this.getData(i, j, k) & 4) == 4 : (block instanceof BlockStepAbstract ? (this.getData(i, j, k) & 8) == 8 : false)));
+        return block == null ? false : (block.material.k() && block.b() ? true : (block instanceof BlockStairs ? (this.getData(i, j, k) & 4) == 4 : (block instanceof BlockStepAbstract ? (this.getData(i, j, k) & 8) == 8 : false)));
     }
 
     public boolean b(int i, int j, int k, boolean flag) {
@@ -1533,7 +1565,7 @@ public abstract class World implements IBlockAccess {
             if (chunk != null && !chunk.isEmpty()) {
                 Block block = Block.byId[this.getTypeId(i, j, k)];
 
-                return block == null ? false : block.material.k() && block.c();
+                return block == null ? false : block.material.k() && block.b();
             } else {
                 return flag;
             }
@@ -1542,11 +1574,11 @@ public abstract class World implements IBlockAccess {
         }
     }
 
-    public void v() {
+    public void w() {
         int i = this.a(1.0F);
 
-        if (i != this.k) {
-            this.k = i;
+        if (i != this.j) {
+            this.j = i;
         }
     }
 
@@ -1556,22 +1588,22 @@ public abstract class World implements IBlockAccess {
     }
 
     public void doTick() {
-        this.m();
+        this.n();
     }
 
     private void a() {
         if (this.worldData.hasStorm()) {
-            this.o = 1.0F;
+            this.n = 1.0F;
             if (this.worldData.isThundering()) {
-                this.q = 1.0F;
+                this.p = 1.0F;
             }
         }
     }
 
-    protected void m() {
-        if (!this.worldProvider.e) {
-            if (this.r > 0) {
-                --this.r;
+    protected void n() {
+        if (!this.worldProvider.f) {
+            if (this.q > 0) {
+                --this.q;
             }
 
             int i = this.worldData.getThunderDuration();
@@ -1606,43 +1638,43 @@ public abstract class World implements IBlockAccess {
                 }
             }
 
-            this.n = this.o;
+            this.m = this.n;
             if (this.worldData.hasStorm()) {
-                this.o = (float) ((double) this.o + 0.01D);
+                this.n = (float) ((double) this.n + 0.01D);
             } else {
-                this.o = (float) ((double) this.o - 0.01D);
+                this.n = (float) ((double) this.n - 0.01D);
             }
 
-            if (this.o < 0.0F) {
-                this.o = 0.0F;
+            if (this.n < 0.0F) {
+                this.n = 0.0F;
             }
 
-            if (this.o > 1.0F) {
-                this.o = 1.0F;
+            if (this.n > 1.0F) {
+                this.n = 1.0F;
             }
 
-            this.p = this.q;
+            this.o = this.p;
             if (this.worldData.isThundering()) {
-                this.q = (float) ((double) this.q + 0.01D);
+                this.p = (float) ((double) this.p + 0.01D);
             } else {
-                this.q = (float) ((double) this.q - 0.01D);
+                this.p = (float) ((double) this.p - 0.01D);
             }
 
-            if (this.q < 0.0F) {
-                this.q = 0.0F;
+            if (this.p < 0.0F) {
+                this.p = 0.0F;
             }
 
-            if (this.q > 1.0F) {
-                this.q = 1.0F;
+            if (this.p > 1.0F) {
+                this.p = 1.0F;
             }
         }
     }
 
-    public void w() {
+    public void x() {
         this.worldData.setWeatherDuration(1);
     }
 
-    protected void x() {
+    protected void y() {
         this.chunkTickList.clear();
         this.methodProfiler.a("buildList");
 
@@ -1665,8 +1697,8 @@ public abstract class World implements IBlockAccess {
         }
 
         this.methodProfiler.b();
-        if (this.M > 0) {
-            --this.M;
+        if (this.O > 0) {
+            --this.O;
         }
 
         this.methodProfiler.a("playerCheckLight");
@@ -1685,9 +1717,9 @@ public abstract class World implements IBlockAccess {
 
     protected void a(int i, int j, Chunk chunk) {
         this.methodProfiler.c("moodSound");
-        if (this.M == 0) {
-            this.l = this.l * 3 + 1013904223;
-            int k = this.l >> 2;
+        if (this.O == 0 && !this.isStatic) {
+            this.k = this.k * 3 + 1013904223;
+            int k = this.k >> 2;
             int l = k & 15;
             int i1 = k >> 8 & 15;
             int j1 = k >> 16 & 127;
@@ -1700,7 +1732,7 @@ public abstract class World implements IBlockAccess {
 
                 if (entityhuman != null && entityhuman.e((double) l + 0.5D, (double) j1 + 0.5D, (double) i1 + 0.5D) > 4.0D) {
                     this.makeSound((double) l + 0.5D, (double) j1 + 0.5D, (double) i1 + 0.5D, "ambient.cave.cave", 0.7F, 0.8F + this.random.nextFloat() * 0.2F);
-                    this.M = this.random.nextInt(12000) + 6000;
+                    this.O = this.random.nextInt(12000) + 6000;
                 }
             }
         }
@@ -1710,7 +1742,7 @@ public abstract class World implements IBlockAccess {
     }
 
     protected void g() {
-        this.x();
+        this.y();
     }
 
     public boolean u(int i, int j, int k) {
@@ -1785,14 +1817,14 @@ public abstract class World implements IBlockAccess {
     }
 
     public void x(int i, int j, int k) {
-        if (!this.worldProvider.e) {
+        if (!this.worldProvider.f) {
             this.c(EnumSkyBlock.SKY, i, j, k);
         }
 
         this.c(EnumSkyBlock.BLOCK, i, j, k);
     }
 
-    private int a(int i, int j, int k, int l, int i1, int j1) {
+    private int b(int i, int j, int k, int l, int i1, int j1) {
         int k1 = 0;
 
         if (this.j(j, k, l)) {
@@ -1837,7 +1869,7 @@ public abstract class World implements IBlockAccess {
         return k1;
     }
 
-    private int f(int i, int j, int k, int l, int i1, int j1) {
+    private int g(int i, int j, int k, int l, int i1, int j1) {
         int k1 = Block.lightEmission[i1];
         int l1 = this.b(EnumSkyBlock.BLOCK, j - 1, k, l) - j1;
         int i2 = this.b(EnumSkyBlock.BLOCK, j + 1, k, l) - j1;
@@ -1892,9 +1924,9 @@ public abstract class World implements IBlockAccess {
             int i2;
 
             if (enumskyblock == EnumSkyBlock.SKY) {
-                i2 = this.a(j1, i, j, k, k1, l1);
+                i2 = this.b(j1, i, j, k, k1, l1);
             } else {
-                i2 = this.f(j1, i, j, k, k1, l1);
+                i2 = this.g(j1, i, j, k, k1, l1);
             }
 
             int j2;
@@ -1907,16 +1939,16 @@ public abstract class World implements IBlockAccess {
             int i4;
 
             if (i2 > j1) {
-                this.J[i1++] = 133152;
+                this.I[i1++] = 133152;
             } else if (i2 < j1) {
                 if (enumskyblock != EnumSkyBlock.BLOCK) {
                     ;
                 }
 
-                this.J[i1++] = 133152 + (j1 << 18);
+                this.I[i1++] = 133152 + (j1 << 18);
 
                 while (l < i1) {
-                    k1 = this.J[l++];
+                    k1 = this.I[l++];
                     l1 = (k1 & 63) - 32 + i;
                     i2 = (k1 >> 6 & 63) - 32 + j;
                     j2 = (k1 >> 12 & 63) - 32 + k;
@@ -1954,8 +1986,8 @@ public abstract class World implements IBlockAccess {
                                         i5 = 1;
                                     }
 
-                                    if (l2 == k2 - i5 && i1 < this.J.length) {
-                                        this.J[i1++] = j4 - i + 32 + (k4 - j + 32 << 6) + (l4 - k + 32 << 12) + (k2 - i5 << 18);
+                                    if (l2 == k2 - i5 && i1 < this.I.length) {
+                                        this.I[i1++] = j4 - i + 32 + (k4 - j + 32 << 6) + (l4 - k + 32 << 12) + (k2 - i5 << 18);
                                     }
                                 }
                             }
@@ -1967,10 +1999,10 @@ public abstract class World implements IBlockAccess {
             }
 
             this.methodProfiler.b();
-            this.methodProfiler.a("tcp < tcc");
+            this.methodProfiler.a("checkedPosition < toCheckCount");
 
             while (l < i1) {
-                k1 = this.J[l++];
+                k1 = this.I[l++];
                 l1 = (k1 & 63) - 32 + i;
                 i2 = (k1 >> 6 & 63) - 32 + j;
                 j2 = (k1 >> 12 & 63) - 32 + k;
@@ -1984,9 +2016,9 @@ public abstract class World implements IBlockAccess {
                 boolean flag2 = false;
 
                 if (enumskyblock == EnumSkyBlock.SKY) {
-                    k3 = this.a(k2, l1, i2, j2, l2, i3);
+                    k3 = this.b(k2, l1, i2, j2, l2, i3);
                 } else {
-                    k3 = this.f(k2, l1, i2, j2, l2, i3);
+                    k3 = this.g(k2, l1, i2, j2, l2, i3);
                 }
 
                 if (k3 != k2) {
@@ -2007,29 +2039,29 @@ public abstract class World implements IBlockAccess {
                             l3 = -l3;
                         }
 
-                        if (j3 + i4 + l3 < 17 && i1 < this.J.length - 6) {
+                        if (j3 + i4 + l3 < 17 && i1 < this.I.length - 6) {
                             if (this.b(enumskyblock, l1 - 1, i2, j2) < k3) {
-                                this.J[i1++] = l1 - 1 - i + 32 + (i2 - j + 32 << 6) + (j2 - k + 32 << 12);
+                                this.I[i1++] = l1 - 1 - i + 32 + (i2 - j + 32 << 6) + (j2 - k + 32 << 12);
                             }
 
                             if (this.b(enumskyblock, l1 + 1, i2, j2) < k3) {
-                                this.J[i1++] = l1 + 1 - i + 32 + (i2 - j + 32 << 6) + (j2 - k + 32 << 12);
+                                this.I[i1++] = l1 + 1 - i + 32 + (i2 - j + 32 << 6) + (j2 - k + 32 << 12);
                             }
 
                             if (this.b(enumskyblock, l1, i2 - 1, j2) < k3) {
-                                this.J[i1++] = l1 - i + 32 + (i2 - 1 - j + 32 << 6) + (j2 - k + 32 << 12);
+                                this.I[i1++] = l1 - i + 32 + (i2 - 1 - j + 32 << 6) + (j2 - k + 32 << 12);
                             }
 
                             if (this.b(enumskyblock, l1, i2 + 1, j2) < k3) {
-                                this.J[i1++] = l1 - i + 32 + (i2 + 1 - j + 32 << 6) + (j2 - k + 32 << 12);
+                                this.I[i1++] = l1 - i + 32 + (i2 + 1 - j + 32 << 6) + (j2 - k + 32 << 12);
                             }
 
                             if (this.b(enumskyblock, l1, i2, j2 - 1) < k3) {
-                                this.J[i1++] = l1 - i + 32 + (i2 - j + 32 << 6) + (j2 - 1 - k + 32 << 12);
+                                this.I[i1++] = l1 - i + 32 + (i2 - j + 32 << 6) + (j2 - 1 - k + 32 << 12);
                             }
 
                             if (this.b(enumskyblock, l1, i2, j2 + 1) < k3) {
-                                this.J[i1++] = l1 - i + 32 + (i2 - j + 32 << 6) + (j2 + 1 - k + 32 << 12);
+                                this.I[i1++] = l1 - i + 32 + (i2 - j + 32 << 6) + (j2 + 1 - k + 32 << 12);
                             }
                         }
                     }
@@ -2049,7 +2081,7 @@ public abstract class World implements IBlockAccess {
     }
 
     public List getEntities(Entity entity, AxisAlignedBB axisalignedbb) {
-        this.N.clear();
+        this.P.clear();
         int i = MathHelper.floor((axisalignedbb.a - 2.0D) / 16.0D);
         int j = MathHelper.floor((axisalignedbb.d + 2.0D) / 16.0D);
         int k = MathHelper.floor((axisalignedbb.c - 2.0D) / 16.0D);
@@ -2058,15 +2090,19 @@ public abstract class World implements IBlockAccess {
         for (int i1 = i; i1 <= j; ++i1) {
             for (int j1 = k; j1 <= l; ++j1) {
                 if (this.isChunkLoaded(i1, j1)) {
-                    this.getChunkAt(i1, j1).a(entity, axisalignedbb, this.N);
+                    this.getChunkAt(i1, j1).a(entity, axisalignedbb, this.P);
                 }
             }
         }
 
-        return this.N;
+        return this.P;
     }
 
     public List a(Class oclass, AxisAlignedBB axisalignedbb) {
+        return this.a(oclass, axisalignedbb, (IEntitySelector) null);
+    }
+
+    public List a(Class oclass, AxisAlignedBB axisalignedbb, IEntitySelector ientityselector) {
         int i = MathHelper.floor((axisalignedbb.a - 2.0D) / 16.0D);
         int j = MathHelper.floor((axisalignedbb.d + 2.0D) / 16.0D);
         int k = MathHelper.floor((axisalignedbb.c - 2.0D) / 16.0D);
@@ -2076,7 +2112,7 @@ public abstract class World implements IBlockAccess {
         for (int i1 = i; i1 <= j; ++i1) {
             for (int j1 = k; j1 <= l; ++j1) {
                 if (this.isChunkLoaded(i1, j1)) {
-                    this.getChunkAt(i1, j1).a(oclass, axisalignedbb, arraylist);
+                    this.getChunkAt(i1, j1).a(oclass, axisalignedbb, arraylist, ientityselector);
                 }
             }
         }
@@ -2106,6 +2142,8 @@ public abstract class World implements IBlockAccess {
         return entity1;
     }
 
+    public abstract Entity getEntity(int i);
+
     public void b(int i, int j, int k, TileEntity tileentity) {
         if (this.isLoaded(i, j, k)) {
             this.getChunkAtWorldCoords(i, k).e();
@@ -2135,7 +2173,7 @@ public abstract class World implements IBlockAccess {
     }
 
     public void b(List list) {
-        this.g.addAll(list);
+        this.f.addAll(list);
     }
 
     public boolean mayPlace(int i, int j, int k, int l, boolean flag, int i1, Entity entity) {
@@ -2155,7 +2193,7 @@ public abstract class World implements IBlockAccess {
                 block = null;
             }
 
-            return i > 0 && block == null && block1.canPlace(this, j, k, l, i1);
+            return block != null && block.material == Material.ORIENTABLE && block1 == Block.ANVIL ? true : i > 0 && block == null && block1.canPlace(this, j, k, l, i1);
         }
     }
 
@@ -2213,7 +2251,7 @@ public abstract class World implements IBlockAccess {
         } else {
             int i1 = this.getTypeId(i, j, k);
 
-            return i1 == 0 ? false : Block.byId[i1].a(this, i, j, k, l);
+            return i1 == 0 ? false : Block.byId[i1].b(this, i, j, k, l);
         }
     }
 
@@ -2255,8 +2293,23 @@ public abstract class World implements IBlockAccess {
 
             if (!entityhuman1.abilities.isInvulnerable) {
                 double d5 = entityhuman1.e(d0, d1, d2);
+                double d6 = d3;
 
-                if ((d3 < 0.0D || d5 < d3 * d3) && (d4 == -1.0D || d5 < d4)) {
+                if (entityhuman1.isSneaking()) {
+                    d6 = d3 * 0.800000011920929D;
+                }
+
+                if (entityhuman1.isInvisible()) {
+                    float f = entityhuman1.bO();
+
+                    if (f < 0.1F) {
+                        f = 0.1F;
+                    }
+
+                    d6 *= (double) (0.7F * f);
+                }
+
+                if ((d3 < 0.0D || d5 < d6 * d6) && (d4 == -1.0D || d5 < d4)) {
                     d4 = d5;
                     entityhuman = entityhuman1;
                 }
@@ -2276,14 +2329,10 @@ public abstract class World implements IBlockAccess {
         return null;
     }
 
-    public void B() {
+    public void C() {
         this.dataManager.checkSession();
     }
 
-    public void setTime(long i) {
-        this.worldData.b(i);
-    }
-
     public long getSeed() {
         return this.worldData.getSeed();
     }
@@ -2292,6 +2341,14 @@ public abstract class World implements IBlockAccess {
         return this.worldData.getTime();
     }
 
+    public long F() {
+        return this.worldData.g();
+    }
+
+    public void setTime(long i) {
+        this.worldData.c(i);
+    }
+
     public ChunkCoordinates getSpawn() {
         return new ChunkCoordinates(this.worldData.c(), this.worldData.d(), this.worldData.e());
     }
@@ -2302,7 +2359,7 @@ public abstract class World implements IBlockAccess {
 
     public void broadcastEntityEffect(Entity entity, byte b0) {}
 
-    public IChunkProvider F() {
+    public IChunkProvider H() {
         return this.chunkProvider;
     }
 
@@ -2320,30 +2377,34 @@ public abstract class World implements IBlockAccess {
         return this.worldData;
     }
 
+    public GameRules getGameRules() {
+        return this.worldData.getGameRules();
+    }
+
     public void everyoneSleeping() {}
 
     public float i(float f) {
-        return (this.p + (this.q - this.p) * f) * this.j(f);
+        return (this.o + (this.p - this.o) * f) * this.j(f);
     }
 
     public float j(float f) {
-        return this.n + (this.o - this.n) * f;
+        return this.m + (this.n - this.m) * f;
     }
 
-    public boolean I() {
+    public boolean L() {
         return (double) this.i(1.0F) > 0.9D;
     }
 
-    public boolean J() {
+    public boolean M() {
         return (double) this.j(1.0F) > 0.2D;
     }
 
     public boolean B(int i, int j, int k) {
-        if (!this.J()) {
+        if (!this.M()) {
             return false;
         } else if (!this.j(i, j, k)) {
             return false;
-        } else if (this.g(i, k) > j) {
+        } else if (this.h(i, k) > j) {
             return false;
         } else {
             BiomeBase biomebase = this.getBiome(i, k);
@@ -2370,13 +2431,19 @@ public abstract class World implements IBlockAccess {
         return this.worldMaps.a(s);
     }
 
+    public void e(int i, int j, int k, int l, int i1) {
+        for (int j1 = 0; j1 < this.w.size(); ++j1) {
+            ((IWorldAccess) this.w.get(j1)).a(i, j, k, l, i1);
+        }
+    }
+
     public void triggerEffect(int i, int j, int k, int l, int i1) {
         this.a((EntityHuman) null, i, j, k, l, i1);
     }
 
     public void a(EntityHuman entityhuman, int i, int j, int k, int l, int i1) {
-        for (int j1 = 0; j1 < this.x.size(); ++j1) {
-            ((IWorldAccess) this.x.get(j1)).a(entityhuman, i, j, k, l, i1);
+        for (int j1 = 0; j1 < this.w.size(); ++j1) {
+            ((IWorldAccess) this.w.get(j1)).a(entityhuman, i, j, k, l, i1);
         }
     }
 
@@ -2384,8 +2451,12 @@ public abstract class World implements IBlockAccess {
         return 256;
     }
 
-    public int L() {
-        return this.worldProvider.e ? 128 : 256;
+    public int O() {
+        return this.worldProvider.f ? 128 : 256;
+    }
+
+    public IUpdatePlayerListBox a(EntityMinecart entityminecart) {
+        return null;
     }
 
     public Random D(int i, int j, int k) {
@@ -2395,12 +2466,8 @@ public abstract class World implements IBlockAccess {
         return this.random;
     }
 
-    public boolean updateLights() {
-        return false;
-    }
-
     public ChunkPosition b(String s, int i, int j, int k) {
-        return this.F().findNearestMapFeature(this, s, i, j, k);
+        return this.H().findNearestMapFeature(this, s, i, j, k);
     }
 
     public CrashReport a(CrashReport crashreport) {
@@ -2410,13 +2477,22 @@ public abstract class World implements IBlockAccess {
         return crashreport;
     }
 
-    public void f(int i, int j, int k, int l, int i1) {
-        Iterator iterator = this.x.iterator();
+    public void g(int i, int j, int k, int l, int i1) {
+        Iterator iterator = this.w.iterator();
 
         while (iterator.hasNext()) {
             IWorldAccess iworldaccess = (IWorldAccess) iterator.next();
 
-            iworldaccess.a(i, j, k, l, i1);
+            iworldaccess.b(i, j, k, l, i1);
         }
     }
+
+    public Vec3DPool getVec3DPool() {
+        return this.K;
+    }
+
+    public Calendar S() {
+        this.L.setTimeInMillis(System.currentTimeMillis());
+        return this.L;
+    }
 }
