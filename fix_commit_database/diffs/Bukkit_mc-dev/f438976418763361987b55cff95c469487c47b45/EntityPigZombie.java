@@ -7,27 +7,25 @@ public class EntityPigZombie extends EntityZombie {
 
     private int angerLevel = 0;
     private int soundDelay = 0;
-    private static final ItemStack g = new ItemStack(Item.GOLD_SWORD, 1);
 
     public EntityPigZombie(World world) {
         super(world);
         this.texture = "/mob/pigzombie.png";
-        this.bw = 0.5F;
-        this.damage = 5;
+        this.bI = 0.5F;
         this.fireProof = true;
     }
 
-    protected boolean aV() {
+    protected boolean bb() {
         return false;
     }
 
-    public void h_() {
-        this.bw = this.target != null ? 0.95F : 0.5F;
+    public void j_() {
+        this.bI = this.target != null ? 0.95F : 0.5F;
         if (this.soundDelay > 0 && --this.soundDelay == 0) {
-            this.world.makeSound(this, "mob.zombiepig.zpigangry", this.aP() * 2.0F, ((this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F) * 1.8F);
+            this.world.makeSound(this, "mob.zombiepig.zpigangry", this.aV() * 2.0F, ((this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F) * 1.8F);
         }
 
-        super.h_();
+        super.j_();
     }
 
     public boolean canSpawn() {
@@ -61,31 +59,31 @@ public class EntityPigZombie extends EntityZombie {
                 if (entity1 instanceof EntityPigZombie) {
                     EntityPigZombie entitypigzombie = (EntityPigZombie) entity1;
 
-                    entitypigzombie.c(entity);
+                    entitypigzombie.o(entity);
                 }
             }
 
-            this.c(entity);
+            this.o(entity);
         }
 
         return super.damageEntity(damagesource, i);
     }
 
-    private void c(Entity entity) {
+    private void o(Entity entity) {
         this.target = entity;
         this.angerLevel = 400 + this.random.nextInt(400);
         this.soundDelay = this.random.nextInt(40);
     }
 
-    protected String aQ() {
+    protected String aW() {
         return "mob.zombiepig.zpig";
     }
 
-    protected String aR() {
+    protected String aX() {
         return "mob.zombiepig.zpighurt";
     }
 
-    protected String aS() {
+    protected String aY() {
         return "mob.zombiepig.zpigdeath";
     }
 
@@ -105,26 +103,35 @@ public class EntityPigZombie extends EntityZombie {
         }
     }
 
+    public boolean c(EntityHuman entityhuman) {
+        return false;
+    }
+
     protected void l(int i) {
-        if (i > 0) {
-            ItemStack itemstack = new ItemStack(Item.GOLD_SWORD);
-
-            EnchantmentManager.a(this.random, itemstack, 5);
-            this.a(itemstack, 0.0F);
-        } else {
-            int j = this.random.nextInt(3);
-
-            if (j == 0) {
-                this.b(Item.GOLD_INGOT.id, 1);
-            } else if (j == 1) {
-                this.b(Item.GOLD_SWORD.id, 1);
-            } else if (j == 2) {
-                this.b(Item.GOLD_HELMET.id, 1);
-            }
-        }
+        this.b(Item.GOLD_INGOT.id, 1);
     }
 
     protected int getLootId() {
         return Item.ROTTEN_FLESH.id;
     }
+
+    protected void bB() {
+        this.setEquipment(0, new ItemStack(Item.GOLD_SWORD));
+    }
+
+    public void bD() {
+        super.bD();
+        this.setVillager(false);
+    }
+
+    public int c(Entity entity) {
+        ItemStack itemstack = this.bA();
+        int i = 5;
+
+        if (itemstack != null) {
+            i += itemstack.a((Entity) this);
+        }
+
+        return i;
+    }
 }
