@@ -1,16 +1,27 @@
 package net.minecraft.server;
 
+import java.util.ArrayList;
+import java.util.Iterator;
+
 public class EntityFallingBlock extends Entity {
 
     public int id;
     public int data;
     public int c;
     public boolean dropItem;
+    private boolean e;
+    private boolean hurtEntities;
+    private int fallHurtMax;
+    private float fallHurtAmount;
 
     public EntityFallingBlock(World world) {
         super(world);
         this.c = 0;
         this.dropItem = true;
+        this.e = false;
+        this.hurtEntities = false;
+        this.fallHurtMax = 20;
+        this.fallHurtAmount = 2.0F;
     }
 
     public EntityFallingBlock(World world, double d0, double d1, double d2, int i) {
@@ -21,6 +32,10 @@ public class EntityFallingBlock extends Entity {
         super(world);
         this.c = 0;
         this.dropItem = true;
+        this.e = false;
+        this.hurtEntities = false;
+        this.fallHurtMax = 20;
+        this.fallHurtAmount = 2.0F;
         this.id = i;
         this.data = j;
         this.m = true;
@@ -35,7 +50,7 @@ public class EntityFallingBlock extends Entity {
         this.lastZ = d2;
     }
 
-    protected boolean e_() {
+    protected boolean f_() {
         return false;
     }
 
@@ -45,7 +60,7 @@ public class EntityFallingBlock extends Entity {
         return !this.dead;
     }
 
-    public void h_() {
+    public void j_() {
         if (this.id == 0) {
             this.die();
         } else {
@@ -77,13 +92,17 @@ public class EntityFallingBlock extends Entity {
                     this.motY *= -0.5D;
                     if (this.world.getTypeId(i, j, k) != Block.PISTON_MOVING.id) {
                         this.die();
-                        if ((!this.world.mayPlace(this.id, i, j, k, true, 1, (Entity) null) || BlockSand.canFall(this.world, i, j - 1, k) || !this.world.setTypeIdAndData(i, j, k, this.id, this.data)) && !this.world.isStatic && this.dropItem) {
-                            this.a(new ItemStack(this.id, 1, this.data), 0.0F);
+                        if (!this.e && this.world.mayPlace(this.id, i, j, k, true, 1, (Entity) null) && !BlockSand.canFall(this.world, i, j - 1, k) && this.world.setTypeIdAndData(i, j, k, this.id, this.data)) {
+                            if (Block.byId[this.id] instanceof BlockSand) {
+                                ((BlockSand) Block.byId[this.id]).a_(this.world, i, j, k, this.data);
+                            }
+                        } else if (this.dropItem && !this.e) {
+                            this.a(new ItemStack(this.id, 1, Block.byId[this.id].getDropData(this.data)), 0.0F);
                         }
                     }
                 } else if (this.c > 100 && !this.world.isStatic && (j < 1 || j > 256) || this.c > 600) {
                     if (this.dropItem) {
-                        this.b(this.id, 1);
+                        this.a(new ItemStack(this.id, 1, Block.byId[this.id].getDropData(this.data)), 0.0F);
                     }
 
                     this.die();
@@ -92,17 +111,58 @@ public class EntityFallingBlock extends Entity {
         }
     }
 
+    protected void a(float f) {
+        if (this.hurtEntities) {
+            int i = MathHelper.f(f - 1.0F);
+
+            if (i > 0) {
+                ArrayList arraylist = new ArrayList(this.world.getEntities(this, this.boundingBox));
+                DamageSource damagesource = this.id == Block.ANVIL.id ? DamageSource.ANVIL : DamageSource.FALLING_BLOCK;
+                Iterator iterator = arraylist.iterator();
+
+                while (iterator.hasNext()) {
+                    Entity entity = (Entity) iterator.next();
+
+                    entity.damageEntity(damagesource, Math.min(MathHelper.d((float) i * this.fallHurtAmount), this.fallHurtMax));
+                }
+
+                if (this.id == Block.ANVIL.id && (double) this.random.nextFloat() < 0.05000000074505806D + (double) i * 0.05D) {
+                    int j = this.data >> 2;
+                    int k = this.data & 3;
+
+                    ++j;
+                    if (j > 2) {
+                        this.e = true;
+                    } else {
+                        this.data = k | j << 2;
+                    }
+                }
+            }
+        }
+    }
+
     protected void b(NBTTagCompound nbttagcompound) {
         nbttagcompound.setByte("Tile", (byte) this.id);
         nbttagcompound.setByte("Data", (byte) this.data);
         nbttagcompound.setByte("Time", (byte) this.c);
         nbttagcompound.setBoolean("DropItem", this.dropItem);
+        nbttagcompound.setBoolean("HurtEntities", this.hurtEntities);
+        nbttagcompound.setFloat("FallHurtAmount", this.fallHurtAmount);
+        nbttagcompound.setInt("FallHurtMax", this.fallHurtMax);
     }
 
     protected void a(NBTTagCompound nbttagcompound) {
         this.id = nbttagcompound.getByte("Tile") & 255;
         this.data = nbttagcompound.getByte("Data") & 255;
         this.c = nbttagcompound.getByte("Time") & 255;
+        if (nbttagcompound.hasKey("HurtEntities")) {
+            this.hurtEntities = nbttagcompound.getBoolean("HurtEntities");
+            this.fallHurtAmount = nbttagcompound.getFloat("FallHurtAmount");
+            this.fallHurtMax = nbttagcompound.getInt("FallHurtMax");
+        } else if (this.id == Block.ANVIL.id) {
+            this.hurtEntities = true;
+        }
+
         if (nbttagcompound.hasKey("DropItem")) {
             this.dropItem = nbttagcompound.getBoolean("DropItem");
         }
@@ -111,4 +171,8 @@ public class EntityFallingBlock extends Entity {
             this.id = Block.SAND.id;
         }
     }
+
+    public void e(boolean flag) {
+        this.hurtEntities = flag;
+    }
 }
