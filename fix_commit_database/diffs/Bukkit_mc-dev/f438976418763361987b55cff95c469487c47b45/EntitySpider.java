@@ -6,7 +6,7 @@ public class EntitySpider extends EntityMonster {
         super(world);
         this.texture = "/mob/spider.png";
         this.a(1.4F, 0.9F);
-        this.bw = 0.8F;
+        this.bI = 0.8F;
     }
 
     protected void a() {
@@ -14,10 +14,10 @@ public class EntitySpider extends EntityMonster {
         this.datawatcher.a(16, new Byte((byte) 0));
     }
 
-    public void h_() {
-        super.h_();
+    public void j_() {
+        super.j_();
         if (!this.world.isStatic) {
-            this.e(this.positionChanged);
+            this.f(this.positionChanged);
         }
     }
 
@@ -29,10 +29,6 @@ public class EntitySpider extends EntityMonster {
         return (double) this.length * 0.75D - 0.5D;
     }
 
-    protected boolean e_() {
-        return false;
-    }
-
     protected Entity findTarget() {
         float f = this.c(1.0F);
 
@@ -45,16 +41,20 @@ public class EntitySpider extends EntityMonster {
         }
     }
 
-    protected String aQ() {
-        return "mob.spider";
+    protected String aW() {
+        return "mob.spider.say";
+    }
+
+    protected String aX() {
+        return "mob.spider.say";
     }
 
-    protected String aR() {
-        return "mob.spider";
+    protected String aY() {
+        return "mob.spider.death";
     }
 
-    protected String aS() {
-        return "mob.spiderdeath";
+    protected void a(int i, int j, int k, int l) {
+        this.world.makeSound(this, "mob.spider.step", 0.15F, 1.0F);
     }
 
     protected void a(Entity entity, float f) {
@@ -90,11 +90,11 @@ public class EntitySpider extends EntityMonster {
         }
     }
 
-    public boolean f_() {
-        return this.p();
+    public boolean g_() {
+        return this.o();
     }
 
-    public void aj() {}
+    public void am() {}
 
     public EnumMonsterType getMonsterType() {
         return EnumMonsterType.ARTHROPOD;
@@ -104,11 +104,11 @@ public class EntitySpider extends EntityMonster {
         return mobeffect.getEffectId() == MobEffectList.POISON.id ? false : super.e(mobeffect);
     }
 
-    public boolean p() {
+    public boolean o() {
         return (this.datawatcher.getByte(16) & 1) != 0;
     }
 
-    public void e(boolean flag) {
+    public void f(boolean flag) {
         byte b0 = this.datawatcher.getByte(16);
 
         if (flag) {
@@ -119,4 +119,15 @@ public class EntitySpider extends EntityMonster {
 
         this.datawatcher.watch(16, Byte.valueOf(b0));
     }
+
+    public void bD() {
+        if (this.world.random.nextInt(100) == 0) {
+            EntitySkeleton entityskeleton = new EntitySkeleton(this.world);
+
+            entityskeleton.setPositionRotation(this.locX, this.locY, this.locZ, this.yaw, 0.0F);
+            entityskeleton.bD();
+            this.world.addEntity(entityskeleton);
+            entityskeleton.mount(this);
+        }
+    }
 }
