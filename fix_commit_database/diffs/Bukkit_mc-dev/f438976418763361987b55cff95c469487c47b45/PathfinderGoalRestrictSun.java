@@ -9,14 +9,14 @@ public class PathfinderGoalRestrictSun extends PathfinderGoal {
     }
 
     public boolean a() {
-        return this.a.world.s();
+        return this.a.world.t();
     }
 
-    public void e() {
+    public void c() {
         this.a.getNavigation().d(true);
     }
 
-    public void c() {
+    public void d() {
         this.a.getNavigation().d(false);
     }
 }
