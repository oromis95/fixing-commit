@@ -47,11 +47,11 @@ public abstract class BlockFluids extends Block {
         }
     }
 
-    public boolean c() {
+    public boolean b() {
         return false;
     }
 
-    public boolean d() {
+    public boolean c() {
         return false;
     }
 
@@ -59,17 +59,17 @@ public abstract class BlockFluids extends Block {
         return flag && i == 0;
     }
 
-    public boolean d(IBlockAccess iblockaccess, int i, int j, int k, int l) {
+    public boolean a_(IBlockAccess iblockaccess, int i, int j, int k, int l) {
         Material material = iblockaccess.getMaterial(i, j, k);
 
-        return material == this.material ? false : (l == 1 ? true : (material == Material.ICE ? false : super.d(iblockaccess, i, j, k, l)));
+        return material == this.material ? false : (l == 1 ? true : (material == Material.ICE ? false : super.a_(iblockaccess, i, j, k, l)));
     }
 
     public AxisAlignedBB e(World world, int i, int j, int k) {
         return null;
     }
 
-    public int b() {
+    public int d() {
         return 4;
     }
 
@@ -81,8 +81,8 @@ public abstract class BlockFluids extends Block {
         return 0;
     }
 
-    private Vec3D i(IBlockAccess iblockaccess, int i, int j, int k) {
-        Vec3D vec3d = Vec3D.a().create(0.0D, 0.0D, 0.0D);
+    private Vec3D g(IBlockAccess iblockaccess, int i, int j, int k) {
+        Vec3D vec3d = iblockaccess.getVec3DPool().create(0.0D, 0.0D, 0.0D);
         int l = this.d(iblockaccess, i, j, k);
 
         for (int i1 = 0; i1 < 4; ++i1) {
@@ -125,56 +125,56 @@ public abstract class BlockFluids extends Block {
         if (iblockaccess.getData(i, j, k) >= 8) {
             boolean flag = false;
 
-            if (flag || this.d(iblockaccess, i, j, k - 1, 2)) {
+            if (flag || this.a_(iblockaccess, i, j, k - 1, 2)) {
                 flag = true;
             }
 
-            if (flag || this.d(iblockaccess, i, j, k + 1, 3)) {
+            if (flag || this.a_(iblockaccess, i, j, k + 1, 3)) {
                 flag = true;
             }
 
-            if (flag || this.d(iblockaccess, i - 1, j, k, 4)) {
+            if (flag || this.a_(iblockaccess, i - 1, j, k, 4)) {
                 flag = true;
             }
 
-            if (flag || this.d(iblockaccess, i + 1, j, k, 5)) {
+            if (flag || this.a_(iblockaccess, i + 1, j, k, 5)) {
                 flag = true;
             }
 
-            if (flag || this.d(iblockaccess, i, j + 1, k - 1, 2)) {
+            if (flag || this.a_(iblockaccess, i, j + 1, k - 1, 2)) {
                 flag = true;
             }
 
-            if (flag || this.d(iblockaccess, i, j + 1, k + 1, 3)) {
+            if (flag || this.a_(iblockaccess, i, j + 1, k + 1, 3)) {
                 flag = true;
             }
 
-            if (flag || this.d(iblockaccess, i - 1, j + 1, k, 4)) {
+            if (flag || this.a_(iblockaccess, i - 1, j + 1, k, 4)) {
                 flag = true;
             }
 
-            if (flag || this.d(iblockaccess, i + 1, j + 1, k, 5)) {
+            if (flag || this.a_(iblockaccess, i + 1, j + 1, k, 5)) {
                 flag = true;
             }
 
             if (flag) {
-                vec3d = vec3d.b().add(0.0D, -6.0D, 0.0D);
+                vec3d = vec3d.a().add(0.0D, -6.0D, 0.0D);
             }
         }
 
-        vec3d = vec3d.b();
+        vec3d = vec3d.a();
         return vec3d;
     }
 
     public void a(World world, int i, int j, int k, Entity entity, Vec3D vec3d) {
-        Vec3D vec3d1 = this.i(world, i, j, k);
+        Vec3D vec3d1 = this.g(world, i, j, k);
 
-        vec3d.a += vec3d1.a;
-        vec3d.b += vec3d1.b;
         vec3d.c += vec3d1.c;
+        vec3d.d += vec3d1.d;
+        vec3d.e += vec3d1.e;
     }
 
-    public int p_() {
+    public int r_() {
         return this.material == Material.WATER ? 5 : (this.material == Material.LAVA ? 30 : 0);
     }
 
@@ -230,7 +230,7 @@ public abstract class BlockFluids extends Block {
         world.makeSound((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), "random.fizz", 0.5F, 2.6F + (world.random.nextFloat() - world.random.nextFloat()) * 0.8F);
 
         for (int l = 0; l < 8; ++l) {
-            world.a("largesmoke", (double) i + Math.random(), (double) j + 1.2D, (double) k + Math.random(), 0.0D, 0.0D, 0.0D);
+            world.addParticle("largesmoke", (double) i + Math.random(), (double) j + 1.2D, (double) k + Math.random(), 0.0D, 0.0D, 0.0D);
         }
     }
 }
