@@ -8,11 +8,11 @@ public class PortalTravelAgent {
 
     public PortalTravelAgent() {}
 
-    public void a(World world, Entity entity) {
+    public void a(World world, Entity entity, double d0, double d1, double d2, float f) {
         if (world.worldProvider.dimension != 1) {
-            if (!this.b(world, entity)) {
-                this.c(world, entity);
-                this.b(world, entity);
+            if (!this.b(world, entity, d0, d1, d2, f)) {
+                this.a(world, entity);
+                this.b(world, entity, d0, d1, d2, f);
             }
         } else {
             int i = MathHelper.floor(entity.locX);
@@ -39,34 +39,35 @@ public class PortalTravelAgent {
         }
     }
 
-    public boolean b(World world, Entity entity) {
+    public boolean b(World world, Entity entity, double d0, double d1, double d2, float f) {
         short short1 = 128;
-        double d0 = -1.0D;
+        double d3 = -1.0D;
         int i = 0;
         int j = 0;
         int k = 0;
         int l = MathHelper.floor(entity.locX);
         int i1 = MathHelper.floor(entity.locZ);
 
-        double d1;
+        int j1;
+        double d4;
 
-        for (int j1 = l - short1; j1 <= l + short1; ++j1) {
-            double d2 = (double) j1 + 0.5D - entity.locX;
+        for (j1 = l - short1; j1 <= l + short1; ++j1) {
+            double d5 = (double) j1 + 0.5D - entity.locX;
 
             for (int k1 = i1 - short1; k1 <= i1 + short1; ++k1) {
-                double d3 = (double) k1 + 0.5D - entity.locZ;
+                double d6 = (double) k1 + 0.5D - entity.locZ;
 
-                for (int l1 = world.L() - 1; l1 >= 0; --l1) {
+                for (int l1 = world.O() - 1; l1 >= 0; --l1) {
                     if (world.getTypeId(j1, l1, k1) == Block.PORTAL.id) {
                         while (world.getTypeId(j1, l1 - 1, k1) == Block.PORTAL.id) {
                             --l1;
                         }
 
-                        d1 = (double) l1 + 0.5D - entity.locY;
-                        double d4 = d2 * d2 + d1 * d1 + d3 * d3;
+                        d4 = (double) l1 + 0.5D - entity.locY;
+                        double d7 = d5 * d5 + d4 * d4 + d6 * d6;
 
-                        if (d0 < 0.0D || d4 < d0) {
-                            d0 = d4;
+                        if (d3 < 0.0D || d7 < d3) {
+                            d3 = d7;
                             i = j1;
                             j = l1;
                             k = k1;
@@ -76,36 +77,106 @@ public class PortalTravelAgent {
             }
         }
 
-        if (d0 >= 0.0D) {
-            double d5 = (double) i + 0.5D;
-            double d6 = (double) j + 0.5D;
+        if (d3 < 0.0D) {
+            return false;
+        } else {
+            double d8 = (double) i + 0.5D;
+            double d9 = (double) j + 0.5D;
+
+            d4 = (double) k + 0.5D;
+            int i2 = -1;
 
-            d1 = (double) k + 0.5D;
             if (world.getTypeId(i - 1, j, k) == Block.PORTAL.id) {
-                d5 -= 0.5D;
+                i2 = 2;
             }
 
             if (world.getTypeId(i + 1, j, k) == Block.PORTAL.id) {
-                d5 += 0.5D;
+                i2 = 0;
             }
 
             if (world.getTypeId(i, j, k - 1) == Block.PORTAL.id) {
-                d1 -= 0.5D;
+                i2 = 3;
             }
 
             if (world.getTypeId(i, j, k + 1) == Block.PORTAL.id) {
-                d1 += 0.5D;
+                i2 = 1;
             }
 
-            entity.setPositionRotation(d5, d6, d1, entity.yaw, 0.0F);
-            entity.motX = entity.motY = entity.motZ = 0.0D;
+            int j2 = entity.at();
+
+            if (i2 > -1) {
+                int k2 = Direction.h[i2];
+                int l2 = Direction.a[i2];
+                int i3 = Direction.b[i2];
+                int j3 = Direction.a[k2];
+                int k3 = Direction.b[k2];
+                boolean flag = !world.isEmpty(i + l2 + j3, j, k + i3 + k3) || !world.isEmpty(i + l2 + j3, j + 1, k + i3 + k3);
+                boolean flag1 = !world.isEmpty(i + l2, j, k + i3) || !world.isEmpty(i + l2, j + 1, k + i3);
+
+                if (flag && flag1) {
+                    i2 = Direction.f[i2];
+                    k2 = Direction.f[k2];
+                    l2 = Direction.a[i2];
+                    i3 = Direction.b[i2];
+                    j3 = Direction.a[k2];
+                    k3 = Direction.b[k2];
+                    j1 = i - j3;
+                    d8 -= (double) j3;
+                    int l3 = k - k3;
+
+                    d4 -= (double) k3;
+                    flag = !world.isEmpty(j1 + l2 + j3, j, l3 + i3 + k3) || !world.isEmpty(j1 + l2 + j3, j + 1, l3 + i3 + k3);
+                    flag1 = !world.isEmpty(j1 + l2, j, l3 + i3) || !world.isEmpty(j1 + l2, j + 1, l3 + i3);
+                }
+
+                float f1 = 0.5F;
+                float f2 = 0.5F;
+
+                if (!flag && flag1) {
+                    f1 = 1.0F;
+                } else if (flag && !flag1) {
+                    f1 = 0.0F;
+                } else if (flag && flag1) {
+                    f2 = 0.0F;
+                }
+
+                d8 += (double) ((float) j3 * f1 + f2 * (float) l2);
+                d4 += (double) ((float) k3 * f1 + f2 * (float) i3);
+                float f3 = 0.0F;
+                float f4 = 0.0F;
+                float f5 = 0.0F;
+                float f6 = 0.0F;
+
+                if (i2 == j2) {
+                    f3 = 1.0F;
+                    f4 = 1.0F;
+                } else if (i2 == Direction.f[j2]) {
+                    f3 = -1.0F;
+                    f4 = -1.0F;
+                } else if (i2 == Direction.g[j2]) {
+                    f5 = 1.0F;
+                    f6 = -1.0F;
+                } else {
+                    f5 = -1.0F;
+                    f6 = 1.0F;
+                }
+
+                double d10 = entity.motX;
+                double d11 = entity.motZ;
+
+                entity.motX = d10 * (double) f3 + d11 * (double) f6;
+                entity.motZ = d10 * (double) f5 + d11 * (double) f4;
+                entity.yaw = f - (float) (j2 * 90) + (float) (i2 * 90);
+            } else {
+                entity.motX = entity.motY = entity.motZ = 0.0D;
+            }
+
+            entity.setPositionRotation(d8, d9, d4, entity.yaw, entity.pitch);
             return true;
-        } else {
-            return false;
         }
     }
 
-    public boolean c(World world, Entity entity) {
+    public boolean a(World world, Entity entity) {
         byte b0 = 16;
         double d0 = -1.0D;
         int i = MathHelper.floor(entity.locX);
@@ -140,7 +211,7 @@ public class PortalTravelAgent {
                 d2 = (double) j2 + 0.5D - entity.locZ;
 
                 label274:
-                for (l2 = world.L() - 1; l2 >= 0; --l2) {
+                for (l2 = world.O() - 1; l2 >= 0; --l2) {
                     if (world.isEmpty(i2, l2, j2)) {
                         while (l2 > 0 && world.isEmpty(i2, l2 - 1, j2)) {
                             --l2;
@@ -191,7 +262,7 @@ public class PortalTravelAgent {
                     d2 = (double) j2 + 0.5D - entity.locZ;
 
                     label222:
-                    for (l2 = world.L() - 1; l2 >= 0; --l2) {
+                    for (l2 = world.O() - 1; l2 >= 0; --l2) {
                         if (world.isEmpty(i2, l2, j2)) {
                             while (l2 > 0 && world.isEmpty(i2, l2 - 1, j2)) {
                                 --l2;
@@ -247,8 +318,8 @@ public class PortalTravelAgent {
                 i1 = 70;
             }
 
-            if (i1 > world.L() - 10) {
-                i1 = world.L() - 10;
+            if (i1 > world.O() - 10) {
+                i1 = world.O() - 10;
             }
 
             j5 = i1;
