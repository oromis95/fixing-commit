@@ -64,6 +64,10 @@ public class EntityBoat extends Entity {
             this.damage += i * 10;
             this.ae();
             if (this.damage > 40) {
+                if (this.passenger != null) {
+                    this.passenger.mount(this);
+                }
+
                 int j;
 
                 for (j = 0; j < 3; ++j) {
@@ -157,7 +161,11 @@ public class EntityBoat extends Entity {
                 d3 = d0 * 2.0D - 1.0D;
                 this.motY += 0.03999999910593033D * d3;
             } else {
-                this.motY += 0.0010000000474974513D;
+                if (this.motY < 0.0D) {
+                    this.motY /= 2.0D;
+                }
+
+                this.motY += 0.007000000216066837D;
             }
 
             if (this.passenger != null) {
