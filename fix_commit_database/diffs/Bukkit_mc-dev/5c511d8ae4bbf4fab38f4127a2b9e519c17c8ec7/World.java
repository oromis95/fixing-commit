@@ -1427,33 +1427,33 @@ public class World implements IBlockAccess {
                     return;
                 }
 
-                if (!this.b(k1, l1).g()) {
-                    int i2 = this.C.size();
-                    int j2;
-
-                    if (flag) {
-                        j2 = 5;
-                        if (j2 > i2) {
-                            j2 = i2;
-                        }
+                if (this.b(k1, l1).g()) {
+                    return;
+                }
 
-                        for (int k2 = 0; k2 < j2; ++k2) {
-                            MetadataChunkBlock metadatachunkblock = (MetadataChunkBlock) this.C.get(this.C.size() - k2 - 1);
+                int i2 = this.C.size();
+                int j2;
 
-                            if (metadatachunkblock.a == enumskyblock && metadatachunkblock.a(i, j, k, l, i1, j1)) {
-                                return;
-                            }
-                        }
+                if (flag) {
+                    j2 = 5;
+                    if (j2 > i2) {
+                        j2 = i2;
                     }
 
-                    this.C.add(new MetadataChunkBlock(enumskyblock, i, j, k, l, i1, j1));
-                    j2 = 1000000;
-                    if (this.C.size() > 1000000) {
-                        System.out.println("More than " + j2 + " updates, aborting lighting updates");
-                        this.C.clear();
+                    for (int k2 = 0; k2 < j2; ++k2) {
+                        MetadataChunkBlock metadatachunkblock = (MetadataChunkBlock) this.C.get(this.C.size() - k2 - 1);
+
+                        if (metadatachunkblock.a == enumskyblock && metadatachunkblock.a(i, j, k, l, i1, j1)) {
+                            return;
+                        }
                     }
+                }
 
-                    return;
+                this.C.add(new MetadataChunkBlock(enumskyblock, i, j, k, l, i1, j1));
+                j2 = 1000000;
+                if (this.C.size() > 1000000) {
+                    System.out.println("More than " + j2 + " updates, aborting lighting updates");
+                    this.C.clear();
                 }
             } finally {
                 --A;
