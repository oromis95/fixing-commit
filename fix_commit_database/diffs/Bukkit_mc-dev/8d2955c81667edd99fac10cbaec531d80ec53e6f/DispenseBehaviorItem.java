@@ -8,12 +8,12 @@ public class DispenseBehaviorItem implements IDispenseBehavior {
         ItemStack itemstack1 = this.b(isourceblock, itemstack);
 
         this.a(isourceblock);
-        this.a(isourceblock, EnumFacing.a(isourceblock.h()));
+        this.a(isourceblock, BlockDispenser.j_(isourceblock.h()));
         return itemstack1;
     }
 
     protected ItemStack b(ISourceBlock isourceblock, ItemStack itemstack) {
-        EnumFacing enumfacing = EnumFacing.a(isourceblock.h());
+        EnumFacing enumfacing = BlockDispenser.j_(isourceblock.h());
         IPosition iposition = BlockDispenser.a(isourceblock);
         ItemStack itemstack1 = itemstack.a(1);
 
