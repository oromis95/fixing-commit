@@ -1,12 +1,8 @@
 package net.minecraft.server;
 
-public class DispenseBehaviorSnowBall extends DispenseBehaviorProjectile {
+final class DispenseBehaviorSnowBall extends DispenseBehaviorProjectile {
 
-    final MinecraftServer b;
-
-    public DispenseBehaviorSnowBall(MinecraftServer minecraftserver) {
-        this.b = minecraftserver;
-    }
+    DispenseBehaviorSnowBall() {}
 
     protected IProjectile a(World world, IPosition iposition) {
         return new EntitySnowball(world, iposition.getX(), iposition.getY(), iposition.getZ());
