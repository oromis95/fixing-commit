@@ -1,15 +1,11 @@
 package net.minecraft.server;
 
-public class DispenseBehaviorFireworks extends DispenseBehaviorItem {
+final class DispenseBehaviorFireworks extends DispenseBehaviorItem {
 
-    final MinecraftServer b;
-
-    public DispenseBehaviorFireworks(MinecraftServer minecraftserver) {
-        this.b = minecraftserver;
-    }
+    DispenseBehaviorFireworks() {}
 
     public ItemStack b(ISourceBlock isourceblock, ItemStack itemstack) {
-        EnumFacing enumfacing = EnumFacing.a(isourceblock.h());
+        EnumFacing enumfacing = BlockDispenser.j_(isourceblock.h());
         double d0 = isourceblock.getX() + (double) enumfacing.c();
         double d1 = (double) ((float) isourceblock.getBlockY() + 0.2F);
         double d2 = isourceblock.getZ() + (double) enumfacing.e();
