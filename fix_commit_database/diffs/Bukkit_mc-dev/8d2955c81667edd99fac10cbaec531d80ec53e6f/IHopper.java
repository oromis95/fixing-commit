@@ -0,0 +1,12 @@
+package net.minecraft.server;
+
+public interface IHopper extends IInventory {
+
+    World getWorld();
+
+    double aA();
+
+    double aB();
+
+    double aC();
+}
