@@ -7,11 +7,10 @@ public class BlockRedstoneLamp extends Block {
     private final boolean a;
 
     public BlockRedstoneLamp(int i, boolean flag) {
-        super(i, 211, Material.BUILDABLE_GLASS);
+        super(i, Material.BUILDABLE_GLASS);
         this.a = flag;
         if (flag) {
             this.a(1.0F);
-            ++this.textureId;
         }
     }
 
@@ -20,7 +19,7 @@ public class BlockRedstoneLamp extends Block {
             if (this.a && !world.isBlockIndirectlyPowered(i, j, k)) {
                 world.a(i, j, k, this.id, 4);
             } else if (!this.a && world.isBlockIndirectlyPowered(i, j, k)) {
-                world.setTypeId(i, j, k, Block.REDSTONE_LAMP_ON.id);
+                world.setTypeIdAndData(i, j, k, Block.REDSTONE_LAMP_ON.id, 0, 2);
             }
         }
     }
@@ -30,14 +29,14 @@ public class BlockRedstoneLamp extends Block {
             if (this.a && !world.isBlockIndirectlyPowered(i, j, k)) {
                 world.a(i, j, k, this.id, 4);
             } else if (!this.a && world.isBlockIndirectlyPowered(i, j, k)) {
-                world.setTypeId(i, j, k, Block.REDSTONE_LAMP_ON.id);
+                world.setTypeIdAndData(i, j, k, Block.REDSTONE_LAMP_ON.id, 0, 2);
             }
         }
     }
 
-    public void b(World world, int i, int j, int k, Random random) {
+    public void a(World world, int i, int j, int k, Random random) {
         if (!world.isStatic && this.a && !world.isBlockIndirectlyPowered(i, j, k)) {
-            world.setTypeId(i, j, k, Block.REDSTONE_LAMP_OFF.id);
+            world.setTypeIdAndData(i, j, k, Block.REDSTONE_LAMP_OFF.id, 0, 2);
         }
     }
 
