@@ -2,13 +2,16 @@ package net.minecraft.server;
 
 public abstract class EntityAgeable extends EntityCreature {
 
+    private float d = -1.0F;
+    private float e;
+
     public EntityAgeable(World world) {
         super(world);
     }
 
     public abstract EntityAgeable createChild(EntityAgeable entityageable);
 
-    public boolean a(EntityHuman entityhuman) {
+    public boolean a_(EntityHuman entityhuman) {
         ItemStack itemstack = entityhuman.inventory.getItemInHand();
 
         if (itemstack != null && itemstack.id == Item.MONSTER_EGG.id && !this.world.isStatic) {
@@ -21,6 +24,10 @@ public abstract class EntityAgeable extends EntityCreature {
                     entityageable.setAge(-24000);
                     entityageable.setPositionRotation(this.locX, this.locY, this.locZ, 0.0F, 0.0F);
                     this.world.addEntity(entityageable);
+                    if (itemstack.hasName()) {
+                        entityageable.setCustomName(itemstack.getName());
+                    }
+
                     if (!entityhuman.abilities.canInstantlyBuild) {
                         --itemstack.count;
                         if (itemstack.count <= 0) {
@@ -31,7 +38,7 @@ public abstract class EntityAgeable extends EntityCreature {
             }
         }
 
-        return super.a(entityhuman);
+        return super.a_(entityhuman);
     }
 
     protected void a() {
@@ -45,6 +52,7 @@ public abstract class EntityAgeable extends EntityCreature {
 
     public void setAge(int i) {
         this.datawatcher.watch(12, Integer.valueOf(i));
+        this.a(this.isBaby());
     }
 
     public void b(NBTTagCompound nbttagcompound) {
@@ -59,18 +67,40 @@ public abstract class EntityAgeable extends EntityCreature {
 
     public void c() {
         super.c();
-        int i = this.getAge();
-
-        if (i < 0) {
-            ++i;
-            this.setAge(i);
-        } else if (i > 0) {
-            --i;
-            this.setAge(i);
+        if (this.world.isStatic) {
+            this.a(this.isBaby());
+        } else {
+            int i = this.getAge();
+
+            if (i < 0) {
+                ++i;
+                this.setAge(i);
+            } else if (i > 0) {
+                --i;
+                this.setAge(i);
+            }
         }
     }
 
     public boolean isBaby() {
         return this.getAge() < 0;
     }
+
+    public void a(boolean flag) {
+        this.j(flag ? 0.5F : 1.0F);
+    }
+
+    protected final void a(float f, float f1) {
+        boolean flag = this.d > 0.0F;
+
+        this.d = f;
+        this.e = f1;
+        if (!flag) {
+            this.j(1.0F);
+        }
+    }
+
+    private void j(float f) {
+        super.a(this.d * f, this.e * f);
+    }
 }
