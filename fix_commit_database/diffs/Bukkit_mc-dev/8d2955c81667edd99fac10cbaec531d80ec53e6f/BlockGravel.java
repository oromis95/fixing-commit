@@ -4,8 +4,8 @@ import java.util.Random;
 
 public class BlockGravel extends BlockSand {
 
-    public BlockGravel(int i, int j) {
-        super(i, j);
+    public BlockGravel(int i) {
+        super(i);
     }
 
     public int getDropType(int i, Random random, int j) {
