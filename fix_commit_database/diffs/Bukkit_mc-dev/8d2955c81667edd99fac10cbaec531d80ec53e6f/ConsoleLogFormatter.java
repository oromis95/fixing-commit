@@ -4,38 +4,29 @@ import java.io.PrintWriter;
 import java.io.StringWriter;
 import java.text.SimpleDateFormat;
 import java.util.logging.Formatter;
-import java.util.logging.Level;
 import java.util.logging.LogRecord;
 
-final class ConsoleLogFormatter extends Formatter {
+class ConsoleLogFormatter extends Formatter {
 
-    private SimpleDateFormat a = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
+    private SimpleDateFormat b;
 
-    ConsoleLogFormatter() {}
+    final ConsoleLogManager a;
+
+    private ConsoleLogFormatter(ConsoleLogManager consolelogmanager) {
+        this.a = consolelogmanager;
+        this.b = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
+    }
 
     public String format(LogRecord logrecord) {
         StringBuilder stringbuilder = new StringBuilder();
 
-        stringbuilder.append(this.a.format(Long.valueOf(logrecord.getMillis())));
-        Level level = logrecord.getLevel();
-
-        if (level == Level.FINEST) {
-            stringbuilder.append(" [FINEST] ");
-        } else if (level == Level.FINER) {
-            stringbuilder.append(" [FINER] ");
-        } else if (level == Level.FINE) {
-            stringbuilder.append(" [FINE] ");
-        } else if (level == Level.INFO) {
-            stringbuilder.append(" [INFO] ");
-        } else if (level == Level.WARNING) {
-            stringbuilder.append(" [WARNING] ");
-        } else if (level == Level.SEVERE) {
-            stringbuilder.append(" [SEVERE] ");
-        } else if (level == Level.SEVERE) {
-            stringbuilder.append(" [").append(level.getLocalizedName()).append("] ");
+        stringbuilder.append(this.b.format(Long.valueOf(logrecord.getMillis())));
+        if (ConsoleLogManager.a(this.a) != null) {
+            stringbuilder.append(ConsoleLogManager.a(this.a));
         }
 
-        stringbuilder.append(logrecord.getMessage());
+        stringbuilder.append(" [").append(logrecord.getLevel().getName()).append("] ");
+        stringbuilder.append(this.formatMessage(logrecord));
         stringbuilder.append('\n');
         Throwable throwable = logrecord.getThrown();
 
@@ -48,4 +39,8 @@ final class ConsoleLogFormatter extends Formatter {
 
         return stringbuilder.toString();
     }
+
+    ConsoleLogFormatter(ConsoleLogManager consolelogmanager, EmptyClass3 emptyclass3) {
+        this(consolelogmanager);
+    }
 }
