@@ -10,10 +10,10 @@ public class ItemCarrotStick extends Item {
     }
 
     public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
-        if (entityhuman.ag() && entityhuman.vehicle instanceof EntityPig) {
+        if (entityhuman.af() && entityhuman.vehicle instanceof EntityPig) {
             EntityPig entitypig = (EntityPig) entityhuman.vehicle;
 
-            if (entitypig.n().h() && itemstack.k() - itemstack.getData() >= 7) {
+            if (entitypig.n().h() && itemstack.l() - itemstack.getData() >= 7) {
                 entitypig.n().g();
                 itemstack.damage(7, entityhuman);
                 if (itemstack.count == 0) {
