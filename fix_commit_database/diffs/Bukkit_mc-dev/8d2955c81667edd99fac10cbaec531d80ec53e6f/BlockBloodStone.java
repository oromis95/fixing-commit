@@ -2,8 +2,8 @@ package net.minecraft.server;
 
 public class BlockBloodStone extends Block {
 
-    public BlockBloodStone(int i, int j) {
-        super(i, j, Material.STONE);
+    public BlockBloodStone(int i) {
+        super(i, Material.STONE);
         this.a(CreativeModeTab.b);
     }
 }
