@@ -27,6 +27,10 @@ public class TileEntity {
         }
     }
 
+    public World getWorld() {
+        return this.world;
+    }
+
     public void b(World world) {
         this.world = world;
     }
@@ -54,7 +58,7 @@ public class TileEntity {
         }
     }
 
-    public void g() {}
+    public void h() {}
 
     public static TileEntity c(NBTTagCompound nbttagcompound) {
         TileEntity tileentity = null;
@@ -72,7 +76,7 @@ public class TileEntity {
         if (tileentity != null) {
             tileentity.a(nbttagcompound);
         } else {
-            System.out.println("Skipping TileEntity with id " + nbttagcompound.getString("id"));
+            MinecraftServer.getServer().getLogger().warning("Skipping TileEntity with id " + nbttagcompound.getString("id"));
         }
 
         return tileentity;
@@ -90,6 +94,9 @@ public class TileEntity {
         if (this.world != null) {
             this.p = this.world.getData(this.x, this.y, this.z);
             this.world.b(this.x, this.y, this.z, this);
+            if (this.q() != null) {
+                this.world.m(this.x, this.y, this.z, this.q().id);
+            }
         }
     }
 
@@ -117,16 +124,20 @@ public class TileEntity {
         this.o = false;
     }
 
-    public void b(int i, int j) {}
+    public boolean b(int i, int j) {
+        return false;
+    }
 
-    public void h() {
+    public void i() {
         this.q = null;
         this.p = -1;
     }
 
     public void a(CrashReportSystemDetails crashreportsystemdetails) {
         crashreportsystemdetails.a("Name", (Callable) (new CrashReportTileEntityName(this)));
-        CrashReportSystemDetails.a(crashreportsystemdetails, this.x, this.y, this.z, this.q.id, this.p);
+        CrashReportSystemDetails.a(crashreportsystemdetails, this.x, this.y, this.z, this.q().id, this.p());
+        crashreportsystemdetails.a("Actual block type", (Callable) (new CrashReportTileEntityType(this)));
+        crashreportsystemdetails.a("Actual block data value", (Callable) (new CrashReportTileEntityData(this)));
     }
 
     static Map t() {
@@ -139,6 +150,7 @@ public class TileEntity {
         a(TileEntityEnderChest.class, "EnderChest");
         a(TileEntityRecordPlayer.class, "RecordPlayer");
         a(TileEntityDispenser.class, "Trap");
+        a(TileEntityDropper.class, "Dropper");
         a(TileEntitySign.class, "Sign");
         a(TileEntityMobSpawner.class, "MobSpawner");
         a(TileEntityNote.class, "Music");
@@ -149,5 +161,8 @@ public class TileEntity {
         a(TileEntityCommand.class, "Control");
         a(TileEntityBeacon.class, "Beacon");
         a(TileEntitySkull.class, "Skull");
+        a(TileEntityLightDetector.class, "DLDetector");
+        a(TileEntityHopper.class, "Hopper");
+        a(TileEntityComparator.class, "Comparator");
     }
 }
