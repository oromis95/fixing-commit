@@ -17,7 +17,7 @@ public class EntityCow extends EntityAnimal {
         this.goalSelector.a(7, new PathfinderGoalRandomLookaround(this));
     }
 
-    public boolean be() {
+    public boolean bh() {
         return true;
     }
 
@@ -25,15 +25,15 @@ public class EntityCow extends EntityAnimal {
         return 10;
     }
 
-    protected String aY() {
+    protected String bb() {
         return "mob.cow.say";
     }
 
-    protected String aZ() {
+    protected String bc() {
         return "mob.cow.hurt";
     }
 
-    protected String ba() {
+    protected String bd() {
         return "mob.cow.hurt";
     }
 
@@ -41,7 +41,7 @@ public class EntityCow extends EntityAnimal {
         this.makeSound("mob.cow.step", 0.15F, 1.0F);
     }
 
-    protected float aX() {
+    protected float ba() {
         return 0.4F;
     }
 
@@ -69,7 +69,7 @@ public class EntityCow extends EntityAnimal {
         }
     }
 
-    public boolean a(EntityHuman entityhuman) {
+    public boolean a_(EntityHuman entityhuman) {
         ItemStack itemstack = entityhuman.inventory.getItemInHand();
 
         if (itemstack != null && itemstack.id == Item.BUCKET.id) {
@@ -81,7 +81,7 @@ public class EntityCow extends EntityAnimal {
 
             return true;
         } else {
-            return super.a(entityhuman);
+            return super.a_(entityhuman);
         }
     }
 
