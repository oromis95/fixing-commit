@@ -23,7 +23,7 @@ public class PathfinderGoalFleeSun extends PathfinderGoal {
             return false;
         } else if (!this.a.isBurning()) {
             return false;
-        } else if (!this.f.k(MathHelper.floor(this.a.locX), (int) this.a.boundingBox.b, MathHelper.floor(this.a.locZ))) {
+        } else if (!this.f.l(MathHelper.floor(this.a.locX), (int) this.a.boundingBox.b, MathHelper.floor(this.a.locZ))) {
             return false;
         } else {
             Vec3D vec3d = this.f();
@@ -48,14 +48,14 @@ public class PathfinderGoalFleeSun extends PathfinderGoal {
     }
 
     private Vec3D f() {
-        Random random = this.a.aB();
+        Random random = this.a.aE();
 
         for (int i = 0; i < 10; ++i) {
             int j = MathHelper.floor(this.a.locX + (double) random.nextInt(20) - 10.0D);
             int k = MathHelper.floor(this.a.boundingBox.b + (double) random.nextInt(6) - 3.0D);
             int l = MathHelper.floor(this.a.locZ + (double) random.nextInt(20) - 10.0D);
 
-            if (!this.f.k(j, k, l) && this.a.a(j, k, l) < 0.0F) {
+            if (!this.f.l(j, k, l) && this.a.a(j, k, l) < 0.0F) {
                 return this.f.getVec3DPool().create((double) j, (double) k, (double) l);
             }
         }
