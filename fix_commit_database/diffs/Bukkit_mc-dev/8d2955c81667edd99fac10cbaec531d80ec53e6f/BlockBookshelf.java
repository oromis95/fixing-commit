@@ -4,15 +4,11 @@ import java.util.Random;
 
 public class BlockBookshelf extends Block {
 
-    public BlockBookshelf(int i, int j) {
-        super(i, j, Material.WOOD);
+    public BlockBookshelf(int i) {
+        super(i, Material.WOOD);
         this.a(CreativeModeTab.b);
     }
 
-    public int a(int i) {
-        return i <= 1 ? 4 : this.textureId;
-    }
-
     public int a(Random random) {
         return 3;
     }
