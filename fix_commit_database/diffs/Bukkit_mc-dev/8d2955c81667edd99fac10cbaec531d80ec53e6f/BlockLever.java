@@ -2,12 +2,12 @@ package net.minecraft.server;
 
 public class BlockLever extends Block {
 
-    protected BlockLever(int i, int j) {
-        super(i, j, Material.ORIENTABLE);
+    protected BlockLever(int i) {
+        super(i, Material.ORIENTABLE);
         this.a(CreativeModeTab.d);
     }
 
-    public AxisAlignedBB e(World world, int i, int j, int k) {
+    public AxisAlignedBB b(World world, int i, int j, int k) {
         return null;
     }
 
@@ -24,46 +24,66 @@ public class BlockLever extends Block {
     }
 
     public boolean canPlace(World world, int i, int j, int k, int l) {
-        return l == 0 && world.t(i, j + 1, k) ? true : (l == 1 && world.v(i, j - 1, k) ? true : (l == 2 && world.t(i, j, k + 1) ? true : (l == 3 && world.t(i, j, k - 1) ? true : (l == 4 && world.t(i + 1, j, k) ? true : l == 5 && world.t(i - 1, j, k)))));
+        return l == 0 && world.u(i, j + 1, k) ? true : (l == 1 && world.w(i, j - 1, k) ? true : (l == 2 && world.u(i, j, k + 1) ? true : (l == 3 && world.u(i, j, k - 1) ? true : (l == 4 && world.u(i + 1, j, k) ? true : l == 5 && world.u(i - 1, j, k)))));
     }
 
     public boolean canPlace(World world, int i, int j, int k) {
-        return world.t(i - 1, j, k) ? true : (world.t(i + 1, j, k) ? true : (world.t(i, j, k - 1) ? true : (world.t(i, j, k + 1) ? true : (world.v(i, j - 1, k) ? true : world.t(i, j + 1, k)))));
+        return world.u(i - 1, j, k) ? true : (world.u(i + 1, j, k) ? true : (world.u(i, j, k - 1) ? true : (world.u(i, j, k + 1) ? true : (world.w(i, j - 1, k) ? true : world.u(i, j + 1, k)))));
     }
 
     public int getPlacedData(World world, int i, int j, int k, int l, float f, float f1, float f2, int i1) {
         int j1 = i1 & 8;
         int k1 = i1 & 7;
+        byte b0 = -1;
 
-        k1 = -1;
-        if (l == 0 && world.t(i, j + 1, k)) {
-            k1 = world.random.nextBoolean() ? 0 : 7;
+        if (l == 0 && world.u(i, j + 1, k)) {
+            b0 = 0;
         }
 
-        if (l == 1 && world.v(i, j - 1, k)) {
-            k1 = 5 + world.random.nextInt(2);
+        if (l == 1 && world.w(i, j - 1, k)) {
+            b0 = 5;
         }
 
-        if (l == 2 && world.t(i, j, k + 1)) {
-            k1 = 4;
+        if (l == 2 && world.u(i, j, k + 1)) {
+            b0 = 4;
         }
 
-        if (l == 3 && world.t(i, j, k - 1)) {
-            k1 = 3;
+        if (l == 3 && world.u(i, j, k - 1)) {
+            b0 = 3;
         }
 
-        if (l == 4 && world.t(i + 1, j, k)) {
-            k1 = 2;
+        if (l == 4 && world.u(i + 1, j, k)) {
+            b0 = 2;
         }
 
-        if (l == 5 && world.t(i - 1, j, k)) {
-            k1 = 1;
+        if (l == 5 && world.u(i - 1, j, k)) {
+            b0 = 1;
         }
 
-        return k1 + j1;
+        return b0 + j1;
     }
 
-    public static int e(int i) {
+    public void postPlace(World world, int i, int j, int k, EntityLiving entityliving, ItemStack itemstack) {
+        int l = world.getData(i, j, k);
+        int i1 = l & 7;
+        int j1 = l & 8;
+
+        if (i1 == d(1)) {
+            if ((MathHelper.floor((double) (entityliving.yaw * 4.0F / 360.0F) + 0.5D) & 1) == 0) {
+                world.setData(i, j, k, 5 | j1, 2);
+            } else {
+                world.setData(i, j, k, 6 | j1, 2);
+            }
+        } else if (i1 == d(0)) {
+            if ((MathHelper.floor((double) (entityliving.yaw * 4.0F / 360.0F) + 0.5D) & 1) == 0) {
+                world.setData(i, j, k, 7 | j1, 2);
+            } else {
+                world.setData(i, j, k, 0 | j1, 2);
+            }
+        }
+    }
+
+    public static int d(int i) {
         switch (i) {
         case 0:
             return 0;
@@ -89,53 +109,53 @@ public class BlockLever extends Block {
     }
 
     public void doPhysics(World world, int i, int j, int k, int l) {
-        if (this.l(world, i, j, k)) {
+        if (this.k(world, i, j, k)) {
             int i1 = world.getData(i, j, k) & 7;
             boolean flag = false;
 
-            if (!world.t(i - 1, j, k) && i1 == 1) {
+            if (!world.u(i - 1, j, k) && i1 == 1) {
                 flag = true;
             }
 
-            if (!world.t(i + 1, j, k) && i1 == 2) {
+            if (!world.u(i + 1, j, k) && i1 == 2) {
                 flag = true;
             }
 
-            if (!world.t(i, j, k - 1) && i1 == 3) {
+            if (!world.u(i, j, k - 1) && i1 == 3) {
                 flag = true;
             }
 
-            if (!world.t(i, j, k + 1) && i1 == 4) {
+            if (!world.u(i, j, k + 1) && i1 == 4) {
                 flag = true;
             }
 
-            if (!world.v(i, j - 1, k) && i1 == 5) {
+            if (!world.w(i, j - 1, k) && i1 == 5) {
                 flag = true;
             }
 
-            if (!world.v(i, j - 1, k) && i1 == 6) {
+            if (!world.w(i, j - 1, k) && i1 == 6) {
                 flag = true;
             }
 
-            if (!world.t(i, j + 1, k) && i1 == 0) {
+            if (!world.u(i, j + 1, k) && i1 == 0) {
                 flag = true;
             }
 
-            if (!world.t(i, j + 1, k) && i1 == 7) {
+            if (!world.u(i, j + 1, k) && i1 == 7) {
                 flag = true;
             }
 
             if (flag) {
                 this.c(world, i, j, k, world.getData(i, j, k), 0);
-                world.setTypeId(i, j, k, 0);
+                world.setAir(i, j, k);
             }
         }
     }
 
-    private boolean l(World world, int i, int j, int k) {
+    private boolean k(World world, int i, int j, int k) {
         if (!this.canPlace(world, i, j, k)) {
             this.c(world, i, j, k, world.getData(i, j, k), 0);
-            world.setTypeId(i, j, k, 0);
+            world.setAir(i, j, k);
             return false;
         } else {
             return true;
@@ -165,8 +185,6 @@ public class BlockLever extends Block {
         }
     }
 
-    public void attack(World world, int i, int j, int k, EntityHuman entityhuman) {}
-
     public boolean interact(World world, int i, int j, int k, EntityHuman entityhuman, int l, float f, float f1, float f2) {
         if (world.isStatic) {
             return true;
@@ -175,8 +193,7 @@ public class BlockLever extends Block {
             int j1 = i1 & 7;
             int k1 = 8 - (i1 & 8);
 
-            world.setData(i, j, k, j1 + k1);
-            world.e(i, j, k, i, j, k);
+            world.setData(i, j, k, j1 + k1, 3);
             world.makeSound((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.click", 0.3F, k1 > 0 ? 0.6F : 0.5F);
             world.applyPhysics(i, j, k, this.id);
             if (j1 == 1) {
@@ -224,19 +241,19 @@ public class BlockLever extends Block {
         super.remove(world, i, j, k, l, i1);
     }
 
-    public boolean b(IBlockAccess iblockaccess, int i, int j, int k, int l) {
-        return (iblockaccess.getData(i, j, k) & 8) > 0;
+    public int b(IBlockAccess iblockaccess, int i, int j, int k, int l) {
+        return (iblockaccess.getData(i, j, k) & 8) > 0 ? 15 : 0;
     }
 
-    public boolean c(IBlockAccess iblockaccess, int i, int j, int k, int l) {
+    public int c(IBlockAccess iblockaccess, int i, int j, int k, int l) {
         int i1 = iblockaccess.getData(i, j, k);
 
         if ((i1 & 8) == 0) {
-            return false;
+            return 0;
         } else {
             int j1 = i1 & 7;
 
-            return j1 == 0 && l == 0 ? true : (j1 == 7 && l == 0 ? true : (j1 == 6 && l == 1 ? true : (j1 == 5 && l == 1 ? true : (j1 == 4 && l == 2 ? true : (j1 == 3 && l == 3 ? true : (j1 == 2 && l == 4 ? true : j1 == 1 && l == 5))))));
+            return j1 == 0 && l == 0 ? 15 : (j1 == 7 && l == 0 ? 15 : (j1 == 6 && l == 1 ? 15 : (j1 == 5 && l == 1 ? 15 : (j1 == 4 && l == 2 ? 15 : (j1 == 3 && l == 3 ? 15 : (j1 == 2 && l == 4 ? 15 : (j1 == 1 && l == 5 ? 15 : 0)))))));
         }
     }
 
