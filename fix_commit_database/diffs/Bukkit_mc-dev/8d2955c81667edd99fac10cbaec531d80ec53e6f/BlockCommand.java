@@ -5,10 +5,10 @@ import java.util.Random;
 public class BlockCommand extends BlockContainer {
 
     public BlockCommand(int i) {
-        super(i, 184, Material.ORE);
+        super(i, Material.ORE);
     }
 
-    public TileEntity a(World world) {
+    public TileEntity b(World world) {
         return new TileEntityCommand();
     }
 
@@ -19,23 +19,26 @@ public class BlockCommand extends BlockContainer {
             boolean flag1 = (i1 & 1) != 0;
 
             if (flag && !flag1) {
-                world.setRawData(i, j, k, i1 | 1);
-                world.a(i, j, k, this.id, this.r_());
+                world.setData(i, j, k, i1 | 1, 4);
+                world.a(i, j, k, this.id, this.a(world));
             } else if (!flag && flag1) {
-                world.setRawData(i, j, k, i1 & -2);
+                world.setData(i, j, k, i1 & -2, 4);
             }
         }
     }
 
-    public void b(World world, int i, int j, int k, Random random) {
+    public void a(World world, int i, int j, int k, Random random) {
         TileEntity tileentity = world.getTileEntity(i, j, k);
 
         if (tileentity != null && tileentity instanceof TileEntityCommand) {
-            ((TileEntityCommand) tileentity).a(world);
+            TileEntityCommand tileentitycommand = (TileEntityCommand) tileentity;
+
+            tileentitycommand.a(tileentitycommand.a(world));
+            world.m(i, j, k, this.id);
         }
     }
 
-    public int r_() {
+    public int a(World world) {
         return 1;
     }
 
@@ -48,4 +51,22 @@ public class BlockCommand extends BlockContainer {
 
         return true;
     }
+
+    public boolean q_() {
+        return true;
+    }
+
+    public int b_(World world, int i, int j, int k, int l) {
+        TileEntity tileentity = world.getTileEntity(i, j, k);
+
+        return tileentity != null && tileentity instanceof TileEntityCommand ? ((TileEntityCommand) tileentity).d() : 0;
+    }
+
+    public void postPlace(World world, int i, int j, int k, EntityLiving entityliving, ItemStack itemstack) {
+        TileEntityCommand tileentitycommand = (TileEntityCommand) world.getTileEntity(i, j, k);
+
+        if (itemstack.hasName()) {
+            tileentitycommand.c(itemstack.getName());
+        }
+    }
 }
