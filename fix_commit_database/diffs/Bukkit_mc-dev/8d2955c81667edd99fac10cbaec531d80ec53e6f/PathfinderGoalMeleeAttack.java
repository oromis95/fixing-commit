@@ -43,7 +43,7 @@ public class PathfinderGoalMeleeAttack extends PathfinderGoal {
     public boolean b() {
         EntityLiving entityliving = this.b.getGoalTarget();
 
-        return entityliving == null ? false : (!this.c.isAlive() ? false : (!this.f ? !this.b.getNavigation().f() : this.b.e(MathHelper.floor(this.c.locX), MathHelper.floor(this.c.locY), MathHelper.floor(this.c.locZ))));
+        return entityliving == null ? false : (!this.c.isAlive() ? false : (!this.f ? !this.b.getNavigation().f() : this.b.d(MathHelper.floor(this.c.locX), MathHelper.floor(this.c.locY), MathHelper.floor(this.c.locZ))));
     }
 
     public void c() {
@@ -58,8 +58,8 @@ public class PathfinderGoalMeleeAttack extends PathfinderGoal {
 
     public void e() {
         this.b.getControllerLook().a(this.c, 30.0F, 30.0F);
-        if ((this.f || this.b.aA().canSee(this.c)) && --this.i <= 0) {
-            this.i = 4 + this.b.aB().nextInt(7);
+        if ((this.f || this.b.aD().canSee(this.c)) && --this.i <= 0) {
+            this.i = 4 + this.b.aE().nextInt(7);
             this.b.getNavigation().a(this.c, this.e);
         }
 
@@ -69,8 +69,8 @@ public class PathfinderGoalMeleeAttack extends PathfinderGoal {
         if (this.b.e(this.c.locX, this.c.boundingBox.b, this.c.locZ) <= d0) {
             if (this.d <= 0) {
                 this.d = 20;
-                if (this.b.bD() != null) {
-                    this.b.bH();
+                if (this.b.bG() != null) {
+                    this.b.bK();
                 }
 
                 this.b.m(this.c);
