@@ -13,7 +13,7 @@ public class WorldGenPumpkin extends WorldGenerator {
             int k1 = k + random.nextInt(8) - random.nextInt(8);
 
             if (world.isEmpty(i1, j1, k1) && world.getTypeId(i1, j1 - 1, k1) == Block.GRASS.id && Block.PUMPKIN.canPlace(world, i1, j1, k1)) {
-                world.setRawTypeIdAndData(i1, j1, k1, Block.PUMPKIN.id, random.nextInt(4));
+                world.setTypeIdAndData(i1, j1, k1, Block.PUMPKIN.id, random.nextInt(4), 2);
             }
         }
 
