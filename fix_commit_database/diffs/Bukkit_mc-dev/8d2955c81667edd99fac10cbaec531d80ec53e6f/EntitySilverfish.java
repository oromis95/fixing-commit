@@ -8,7 +8,7 @@ public class EntitySilverfish extends EntityMonster {
         super(world);
         this.texture = "/mob/silverfish.png";
         this.a(0.3F, 0.7F);
-        this.bH = 0.6F;
+        this.bI = 0.6F;
     }
 
     public int getMaxHealth() {
@@ -25,15 +25,15 @@ public class EntitySilverfish extends EntityMonster {
         return this.world.findNearbyVulnerablePlayer(this, d0);
     }
 
-    protected String aY() {
+    protected String bb() {
         return "mob.silverfish.say";
     }
 
-    protected String aZ() {
+    protected String bc() {
         return "mob.silverfish.hit";
     }
 
-    protected String ba() {
+    protected String bd() {
         return "mob.silverfish.kill";
     }
 
@@ -64,13 +64,13 @@ public class EntitySilverfish extends EntityMonster {
         return 0;
     }
 
-    public void j_() {
-        this.ax = this.yaw;
-        super.j_();
+    public void l_() {
+        this.ay = this.yaw;
+        super.l_();
     }
 
-    protected void bn() {
-        super.bn();
+    protected void bq() {
+        super.bq();
         if (!this.world.isStatic) {
             int i;
             int j;
@@ -91,8 +91,7 @@ public class EntitySilverfish extends EntityMonster {
                                 int k1 = this.world.getTypeId(i + i1, j + l, k + j1);
 
                                 if (k1 == Block.MONSTER_EGGS.id) {
-                                    this.world.triggerEffect(2001, i + i1, j + l, k + j1, Block.MONSTER_EGGS.id + (this.world.getData(i + i1, j + l, k + j1) << 12));
-                                    this.world.setTypeId(i + i1, j + l, k + j1, 0);
+                                    this.world.setAir(i + i1, j + l, k + j1, false);
                                     Block.MONSTER_EGGS.postBreak(this.world, i + i1, j + l, k + j1, 0);
                                     if (this.random.nextBoolean()) {
                                         flag = true;
@@ -112,9 +111,9 @@ public class EntitySilverfish extends EntityMonster {
                 int l1 = this.random.nextInt(6);
 
                 l = this.world.getTypeId(i + Facing.b[l1], j + Facing.c[l1], k + Facing.d[l1]);
-                if (BlockMonsterEggs.e(l)) {
-                    this.world.setTypeIdAndData(i + Facing.b[l1], j + Facing.c[l1], k + Facing.d[l1], Block.MONSTER_EGGS.id, BlockMonsterEggs.f(l));
-                    this.aR();
+                if (BlockMonsterEggs.d(l)) {
+                    this.world.setTypeIdAndData(i + Facing.b[l1], j + Facing.c[l1], k + Facing.d[l1], Block.MONSTER_EGGS.id, BlockMonsterEggs.e(l), 3);
+                    this.aU();
                     this.die();
                 } else {
                     this.i();
