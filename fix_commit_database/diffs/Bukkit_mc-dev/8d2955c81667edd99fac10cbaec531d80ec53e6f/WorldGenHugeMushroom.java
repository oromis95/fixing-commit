@@ -137,7 +137,7 @@ public class WorldGenHugeMushroom extends WorldGenerator {
                                     l2 = 0;
                                 }
 
-                                if ((l2 != 0 || j >= j + i1 - 1) && !Block.q[world.getTypeId(i2, k1, k2)]) {
+                                if ((l2 != 0 || j >= j + i1 - 1) && !Block.s[world.getTypeId(i2, k1, k2)]) {
                                     this.setTypeAndData(world, i2, k1, k2, Block.BIG_MUSHROOM_1.id + l, l2);
                                 }
                             }
@@ -146,7 +146,7 @@ public class WorldGenHugeMushroom extends WorldGenerator {
 
                     for (k1 = 0; k1 < i1; ++k1) {
                         l1 = world.getTypeId(i, j + k1, k);
-                        if (!Block.q[l1]) {
+                        if (!Block.s[l1]) {
                             this.setTypeAndData(world, i, j + k1, k, Block.BIG_MUSHROOM_1.id + l, 10);
                         }
                     }
