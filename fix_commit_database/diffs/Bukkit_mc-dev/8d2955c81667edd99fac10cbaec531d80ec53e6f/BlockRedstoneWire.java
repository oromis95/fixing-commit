@@ -10,16 +10,12 @@ public class BlockRedstoneWire extends Block {
     private boolean a = true;
     private Set b = new HashSet();
 
-    public BlockRedstoneWire(int i, int j) {
-        super(i, j, Material.ORIENTABLE);
+    public BlockRedstoneWire(int i) {
+        super(i, Material.ORIENTABLE);
         this.a(0.0F, 0.0F, 0.0F, 1.0F, 0.0625F, 1.0F);
     }
 
-    public int a(int i, int j) {
-        return this.textureId;
-    }
-
-    public AxisAlignedBB e(World world, int i, int j, int k) {
+    public AxisAlignedBB b(World world, int i, int j, int k) {
         return null;
     }
 
@@ -36,10 +32,10 @@ public class BlockRedstoneWire extends Block {
     }
 
     public boolean canPlace(World world, int i, int j, int k) {
-        return world.v(i, j - 1, k) || world.getTypeId(i, j - 1, k) == Block.GLOWSTONE.id;
+        return world.w(i, j - 1, k) || world.getTypeId(i, j - 1, k) == Block.GLOWSTONE.id;
     }
 
-    private void l(World world, int i, int j, int k) {
+    private void k(World world, int i, int j, int k) {
         this.a(world, i, j, k, i, j, k);
         ArrayList arraylist = new ArrayList(this.b);
 
@@ -54,125 +50,77 @@ public class BlockRedstoneWire extends Block {
 
     private void a(World world, int i, int j, int k, int l, int i1, int j1) {
         int k1 = world.getData(i, j, k);
-        int l1 = 0;
+        byte b0 = 0;
+        int l1 = this.getPower(world, l, i1, j1, b0);
 
         this.a = false;
-        boolean flag = world.isBlockIndirectlyPowered(i, j, k);
+        int i2 = world.getHighestNeighborSignal(i, j, k);
 
         this.a = true;
-        int i2;
-        int j2;
-        int k2;
-
-        if (flag) {
-            l1 = 15;
-        } else {
-            for (i2 = 0; i2 < 4; ++i2) {
-                j2 = i;
-                k2 = k;
-                if (i2 == 0) {
-                    j2 = i - 1;
-                }
-
-                if (i2 == 1) {
-                    ++j2;
-                }
+        if (i2 > 0 && i2 > l1 - 1) {
+            l1 = i2;
+        }
 
-                if (i2 == 2) {
-                    k2 = k - 1;
-                }
+        int j2 = 0;
 
-                if (i2 == 3) {
-                    ++k2;
-                }
+        for (int k2 = 0; k2 < 4; ++k2) {
+            int l2 = i;
+            int i3 = k;
 
-                if (j2 != l || j != i1 || k2 != j1) {
-                    l1 = this.getPower(world, j2, j, k2, l1);
-                }
-
-                if (world.t(j2, j, k2) && !world.t(i, j + 1, k)) {
-                    if (j2 != l || j + 1 != i1 || k2 != j1) {
-                        l1 = this.getPower(world, j2, j + 1, k2, l1);
-                    }
-                } else if (!world.t(j2, j, k2) && (j2 != l || j - 1 != i1 || k2 != j1)) {
-                    l1 = this.getPower(world, j2, j - 1, k2, l1);
-                }
+            if (k2 == 0) {
+                l2 = i - 1;
             }
 
-            if (l1 > 0) {
-                --l1;
-            } else {
-                l1 = 0;
+            if (k2 == 1) {
+                ++l2;
             }
-        }
-
-        if (k1 != l1) {
-            world.suppressPhysics = true;
-            world.setData(i, j, k, l1);
-            world.e(i, j, k, i, j, k);
-            world.suppressPhysics = false;
-
-            for (i2 = 0; i2 < 4; ++i2) {
-                j2 = i;
-                k2 = k;
-                int l2 = j - 1;
-
-                if (i2 == 0) {
-                    j2 = i - 1;
-                }
-
-                if (i2 == 1) {
-                    ++j2;
-                }
 
-                if (i2 == 2) {
-                    k2 = k - 1;
-                }
-
-                if (i2 == 3) {
-                    ++k2;
-                }
-
-                if (world.t(j2, j, k2)) {
-                    l2 += 2;
-                }
+            if (k2 == 2) {
+                i3 = k - 1;
+            }
 
-                boolean flag1 = false;
-                int i3 = this.getPower(world, j2, j, k2, -1);
+            if (k2 == 3) {
+                ++i3;
+            }
 
-                l1 = world.getData(i, j, k);
-                if (l1 > 0) {
-                    --l1;
-                }
+            if (l2 != l || i3 != j1) {
+                j2 = this.getPower(world, l2, j, i3, j2);
+            }
 
-                if (i3 >= 0 && i3 != l1) {
-                    this.a(world, j2, j, k2, i, j, k);
+            if (world.u(l2, j, i3) && !world.u(i, j + 1, k)) {
+                if ((l2 != l || i3 != j1) && j >= i1) {
+                    j2 = this.getPower(world, l2, j + 1, i3, j2);
                 }
+            } else if (!world.u(l2, j, i3) && (l2 != l || i3 != j1) && j <= i1) {
+                j2 = this.getPower(world, l2, j - 1, i3, j2);
+            }
+        }
 
-                i3 = this.getPower(world, j2, l2, k2, -1);
-                l1 = world.getData(i, j, k);
-                if (l1 > 0) {
-                    --l1;
-                }
+        if (j2 > l1) {
+            l1 = j2 - 1;
+        } else if (l1 > 0) {
+            --l1;
+        } else {
+            l1 = 0;
+        }
 
-                if (i3 >= 0 && i3 != l1) {
-                    this.a(world, j2, l2, k2, i, j, k);
-                }
-            }
+        if (i2 > l1 - 1) {
+            l1 = i2;
+        }
 
-            if (k1 < l1 || l1 == 0) {
-                this.b.add(new ChunkPosition(i, j, k));
-                this.b.add(new ChunkPosition(i - 1, j, k));
-                this.b.add(new ChunkPosition(i + 1, j, k));
-                this.b.add(new ChunkPosition(i, j - 1, k));
-                this.b.add(new ChunkPosition(i, j + 1, k));
-                this.b.add(new ChunkPosition(i, j, k - 1));
-                this.b.add(new ChunkPosition(i, j, k + 1));
-            }
+        if (k1 != l1) {
+            world.setData(i, j, k, l1, 2);
+            this.b.add(new ChunkPosition(i, j, k));
+            this.b.add(new ChunkPosition(i - 1, j, k));
+            this.b.add(new ChunkPosition(i + 1, j, k));
+            this.b.add(new ChunkPosition(i, j - 1, k));
+            this.b.add(new ChunkPosition(i, j + 1, k));
+            this.b.add(new ChunkPosition(i, j, k - 1));
+            this.b.add(new ChunkPosition(i, j, k + 1));
         }
     }
 
-    private void n(World world, int i, int j, int k) {
+    private void m(World world, int i, int j, int k) {
         if (world.getTypeId(i, j, k) == this.id) {
             world.applyPhysics(i, j, k, this.id);
             world.applyPhysics(i - 1, j, k, this.id);
@@ -187,35 +135,35 @@ public class BlockRedstoneWire extends Block {
     public void onPlace(World world, int i, int j, int k) {
         super.onPlace(world, i, j, k);
         if (!world.isStatic) {
-            this.l(world, i, j, k);
+            this.k(world, i, j, k);
             world.applyPhysics(i, j + 1, k, this.id);
             world.applyPhysics(i, j - 1, k, this.id);
-            this.n(world, i - 1, j, k);
-            this.n(world, i + 1, j, k);
-            this.n(world, i, j, k - 1);
-            this.n(world, i, j, k + 1);
-            if (world.t(i - 1, j, k)) {
-                this.n(world, i - 1, j + 1, k);
+            this.m(world, i - 1, j, k);
+            this.m(world, i + 1, j, k);
+            this.m(world, i, j, k - 1);
+            this.m(world, i, j, k + 1);
+            if (world.u(i - 1, j, k)) {
+                this.m(world, i - 1, j + 1, k);
             } else {
-                this.n(world, i - 1, j - 1, k);
+                this.m(world, i - 1, j - 1, k);
             }
 
-            if (world.t(i + 1, j, k)) {
-                this.n(world, i + 1, j + 1, k);
+            if (world.u(i + 1, j, k)) {
+                this.m(world, i + 1, j + 1, k);
             } else {
-                this.n(world, i + 1, j - 1, k);
+                this.m(world, i + 1, j - 1, k);
             }
 
-            if (world.t(i, j, k - 1)) {
-                this.n(world, i, j + 1, k - 1);
+            if (world.u(i, j, k - 1)) {
+                this.m(world, i, j + 1, k - 1);
             } else {
-                this.n(world, i, j - 1, k - 1);
+                this.m(world, i, j - 1, k - 1);
             }
 
-            if (world.t(i, j, k + 1)) {
-                this.n(world, i, j + 1, k + 1);
+            if (world.u(i, j, k + 1)) {
+                this.m(world, i, j + 1, k + 1);
             } else {
-                this.n(world, i, j - 1, k + 1);
+                this.m(world, i, j - 1, k + 1);
             }
         }
     }
@@ -229,33 +177,33 @@ public class BlockRedstoneWire extends Block {
             world.applyPhysics(i - 1, j, k, this.id);
             world.applyPhysics(i, j, k + 1, this.id);
             world.applyPhysics(i, j, k - 1, this.id);
-            this.l(world, i, j, k);
-            this.n(world, i - 1, j, k);
-            this.n(world, i + 1, j, k);
-            this.n(world, i, j, k - 1);
-            this.n(world, i, j, k + 1);
-            if (world.t(i - 1, j, k)) {
-                this.n(world, i - 1, j + 1, k);
+            this.k(world, i, j, k);
+            this.m(world, i - 1, j, k);
+            this.m(world, i + 1, j, k);
+            this.m(world, i, j, k - 1);
+            this.m(world, i, j, k + 1);
+            if (world.u(i - 1, j, k)) {
+                this.m(world, i - 1, j + 1, k);
             } else {
-                this.n(world, i - 1, j - 1, k);
+                this.m(world, i - 1, j - 1, k);
             }
 
-            if (world.t(i + 1, j, k)) {
-                this.n(world, i + 1, j + 1, k);
+            if (world.u(i + 1, j, k)) {
+                this.m(world, i + 1, j + 1, k);
             } else {
-                this.n(world, i + 1, j - 1, k);
+                this.m(world, i + 1, j - 1, k);
             }
 
-            if (world.t(i, j, k - 1)) {
-                this.n(world, i, j + 1, k - 1);
+            if (world.u(i, j, k - 1)) {
+                this.m(world, i, j + 1, k - 1);
             } else {
-                this.n(world, i, j - 1, k - 1);
+                this.m(world, i, j - 1, k - 1);
             }
 
-            if (world.t(i, j, k + 1)) {
-                this.n(world, i, j + 1, k + 1);
+            if (world.u(i, j, k + 1)) {
+                this.m(world, i, j + 1, k + 1);
             } else {
-                this.n(world, i, j - 1, k + 1);
+                this.m(world, i, j - 1, k + 1);
             }
         }
     }
@@ -272,14 +220,13 @@ public class BlockRedstoneWire extends Block {
 
     public void doPhysics(World world, int i, int j, int k, int l) {
         if (!world.isStatic) {
-            int i1 = world.getData(i, j, k);
             boolean flag = this.canPlace(world, i, j, k);
 
             if (flag) {
-                this.l(world, i, j, k);
+                this.k(world, i, j, k);
             } else {
-                this.c(world, i, j, k, i1, 0);
-                world.setTypeId(i, j, k, 0);
+                this.c(world, i, j, k, 0, 0);
+                world.setAir(i, j, k);
             }
 
             super.doPhysics(world, i, j, k, l);
@@ -290,42 +237,46 @@ public class BlockRedstoneWire extends Block {
         return Item.REDSTONE.id;
     }
 
-    public boolean c(IBlockAccess iblockaccess, int i, int j, int k, int l) {
-        return !this.a ? false : this.b(iblockaccess, i, j, k, l);
+    public int c(IBlockAccess iblockaccess, int i, int j, int k, int l) {
+        return !this.a ? 0 : this.b(iblockaccess, i, j, k, l);
     }
 
-    public boolean b(IBlockAccess iblockaccess, int i, int j, int k, int l) {
+    public int b(IBlockAccess iblockaccess, int i, int j, int k, int l) {
         if (!this.a) {
-            return false;
-        } else if (iblockaccess.getData(i, j, k) == 0) {
-            return false;
-        } else if (l == 1) {
-            return true;
+            return 0;
         } else {
-            boolean flag = g(iblockaccess, i - 1, j, k, 1) || !iblockaccess.t(i - 1, j, k) && g(iblockaccess, i - 1, j - 1, k, -1);
-            boolean flag1 = g(iblockaccess, i + 1, j, k, 3) || !iblockaccess.t(i + 1, j, k) && g(iblockaccess, i + 1, j - 1, k, -1);
-            boolean flag2 = g(iblockaccess, i, j, k - 1, 2) || !iblockaccess.t(i, j, k - 1) && g(iblockaccess, i, j - 1, k - 1, -1);
-            boolean flag3 = g(iblockaccess, i, j, k + 1, 0) || !iblockaccess.t(i, j, k + 1) && g(iblockaccess, i, j - 1, k + 1, -1);
-
-            if (!iblockaccess.t(i, j + 1, k)) {
-                if (iblockaccess.t(i - 1, j, k) && g(iblockaccess, i - 1, j + 1, k, -1)) {
-                    flag = true;
-                }
+            int i1 = iblockaccess.getData(i, j, k);
 
-                if (iblockaccess.t(i + 1, j, k) && g(iblockaccess, i + 1, j + 1, k, -1)) {
-                    flag1 = true;
-                }
+            if (i1 == 0) {
+                return 0;
+            } else if (l == 1) {
+                return i1;
+            } else {
+                boolean flag = g(iblockaccess, i - 1, j, k, 1) || !iblockaccess.u(i - 1, j, k) && g(iblockaccess, i - 1, j - 1, k, -1);
+                boolean flag1 = g(iblockaccess, i + 1, j, k, 3) || !iblockaccess.u(i + 1, j, k) && g(iblockaccess, i + 1, j - 1, k, -1);
+                boolean flag2 = g(iblockaccess, i, j, k - 1, 2) || !iblockaccess.u(i, j, k - 1) && g(iblockaccess, i, j - 1, k - 1, -1);
+                boolean flag3 = g(iblockaccess, i, j, k + 1, 0) || !iblockaccess.u(i, j, k + 1) && g(iblockaccess, i, j - 1, k + 1, -1);
+
+                if (!iblockaccess.u(i, j + 1, k)) {
+                    if (iblockaccess.u(i - 1, j, k) && g(iblockaccess, i - 1, j + 1, k, -1)) {
+                        flag = true;
+                    }
 
-                if (iblockaccess.t(i, j, k - 1) && g(iblockaccess, i, j + 1, k - 1, -1)) {
-                    flag2 = true;
-                }
+                    if (iblockaccess.u(i + 1, j, k) && g(iblockaccess, i + 1, j + 1, k, -1)) {
+                        flag1 = true;
+                    }
+
+                    if (iblockaccess.u(i, j, k - 1) && g(iblockaccess, i, j + 1, k - 1, -1)) {
+                        flag2 = true;
+                    }
 
-                if (iblockaccess.t(i, j, k + 1) && g(iblockaccess, i, j + 1, k + 1, -1)) {
-                    flag3 = true;
+                    if (iblockaccess.u(i, j, k + 1) && g(iblockaccess, i, j + 1, k + 1, -1)) {
+                        flag3 = true;
+                    }
                 }
-            }
 
-            return !flag2 && !flag1 && !flag && !flag3 && l >= 2 && l <= 5 ? true : (l == 2 && flag2 && !flag && !flag1 ? true : (l == 3 && flag3 && !flag && !flag1 ? true : (l == 4 && flag && !flag2 && !flag3 ? true : l == 5 && flag1 && !flag2 && !flag3)));
+                return !flag2 && !flag1 && !flag && !flag3 && l >= 2 && l <= 5 ? i1 : (l == 2 && flag2 && !flag && !flag1 ? i1 : (l == 3 && flag3 && !flag && !flag1 ? i1 : (l == 4 && flag && !flag2 && !flag3 ? i1 : (l == 5 && flag1 && !flag2 && !flag3 ? i1 : 0))));
+            }
         }
     }
 
@@ -340,7 +291,7 @@ public class BlockRedstoneWire extends Block {
             return true;
         } else if (i1 == 0) {
             return false;
-        } else if (i1 != Block.DIODE_OFF.id && i1 != Block.DIODE_ON.id) {
+        } else if (!Block.DIODE_OFF.g(i1)) {
             return Block.byId[i1].isPowerSource() && l != -1;
         } else {
             int j1 = iblockaccess.getData(i, j, k);
