@@ -14,7 +14,7 @@ public class PathfinderGoalOfferFlower extends PathfinderGoal {
     public boolean a() {
         if (!this.a.world.u()) {
             return false;
-        } else if (this.a.aB().nextInt(8000) != 0) {
+        } else if (this.a.aE().nextInt(8000) != 0) {
             return false;
         } else {
             this.b = (EntityVillager) this.a.world.a(EntityVillager.class, this.a.boundingBox.grow(6.0D, 2.0D, 6.0D), (Entity) this.a);
@@ -28,11 +28,11 @@ public class PathfinderGoalOfferFlower extends PathfinderGoal {
 
     public void c() {
         this.c = 400;
-        this.a.f(true);
+        this.a.a(true);
     }
 
     public void d() {
-        this.a.f(false);
+        this.a.a(false);
         this.b = null;
     }
 
