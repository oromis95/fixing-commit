@@ -43,11 +43,11 @@ public class EntityEnderSignal extends Entity {
         this.f = this.random.nextInt(5) > 0;
     }
 
-    public void j_() {
-        this.T = this.locX;
-        this.U = this.locY;
-        this.V = this.locZ;
-        super.j_();
+    public void l_() {
+        this.U = this.locX;
+        this.V = this.locY;
+        this.W = this.locZ;
+        super.l_();
         this.locX += this.motX;
         this.locY += this.motY;
         this.locZ += this.motZ;
@@ -96,7 +96,7 @@ public class EntityEnderSignal extends Entity {
 
         float f3 = 0.25F;
 
-        if (this.H()) {
+        if (this.G()) {
             for (int i = 0; i < 4; ++i) {
                 this.world.addParticle("bubble", this.locX - this.motX * (double) f3, this.locY - this.motY * (double) f3, this.locZ - this.motZ * (double) f3, this.motX, this.motY, this.motZ);
             }
@@ -126,7 +126,7 @@ public class EntityEnderSignal extends Entity {
         return 1.0F;
     }
 
-    public boolean aq() {
+    public boolean ap() {
         return false;
     }
 }
