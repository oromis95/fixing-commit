@@ -11,19 +11,19 @@ public class ItemEnderEye extends Item {
         int i1 = world.getTypeId(i, j, k);
         int j1 = world.getData(i, j, k);
 
-        if (entityhuman.a(i, j, k, l, itemstack) && i1 == Block.ENDER_PORTAL_FRAME.id && !BlockEnderPortalFrame.e(j1)) {
+        if (entityhuman.a(i, j, k, l, itemstack) && i1 == Block.ENDER_PORTAL_FRAME.id && !BlockEnderPortalFrame.d(j1)) {
             if (world.isStatic) {
                 return true;
             } else {
-                world.setData(i, j, k, j1 + 4);
+                world.setData(i, j, k, j1 + 4, 2);
                 --itemstack.count;
 
                 int k1;
 
                 for (k1 = 0; k1 < 16; ++k1) {
-                    double d0 = (double) ((float) i + (5.0F + d.nextFloat() * 6.0F) / 16.0F);
+                    double d0 = (double) ((float) i + (5.0F + e.nextFloat() * 6.0F) / 16.0F);
                     double d1 = (double) ((float) j + 0.8125F);
-                    double d2 = (double) ((float) k + (5.0F + d.nextFloat() * 6.0F) / 16.0F);
+                    double d2 = (double) ((float) k + (5.0F + e.nextFloat() * 6.0F) / 16.0F);
                     double d3 = 0.0D;
                     double d4 = 0.0D;
                     double d5 = 0.0D;
@@ -50,7 +50,7 @@ public class ItemEnderEye extends Item {
                     l2 = world.getTypeId(j3, j, k3);
                     if (l2 == Block.ENDER_PORTAL_FRAME.id) {
                         i3 = world.getData(j3, j, k3);
-                        if (!BlockEnderPortalFrame.e(i3)) {
+                        if (!BlockEnderPortalFrame.d(i3)) {
                             flag1 = false;
                             break;
                         }
@@ -71,7 +71,7 @@ public class ItemEnderEye extends Item {
                         k3 += Direction.b[k1] * 4;
                         l2 = world.getTypeId(j3, j, k3);
                         i3 = world.getData(j3, j, k3);
-                        if (l2 != Block.ENDER_PORTAL_FRAME.id || !BlockEnderPortalFrame.e(i3)) {
+                        if (l2 != Block.ENDER_PORTAL_FRAME.id || !BlockEnderPortalFrame.d(i3)) {
                             flag1 = false;
                             break;
                         }
@@ -86,7 +86,7 @@ public class ItemEnderEye extends Item {
                             i3 = world.getTypeId(k3, j, l2);
                             int l3 = world.getData(k3, j, l2);
 
-                            if (i3 != Block.ENDER_PORTAL_FRAME.id || !BlockEnderPortalFrame.e(l3)) {
+                            if (i3 != Block.ENDER_PORTAL_FRAME.id || !BlockEnderPortalFrame.d(l3)) {
                                 flag1 = false;
                                 break;
                             }
@@ -100,7 +100,7 @@ public class ItemEnderEye extends Item {
                                 l2 = k + Direction.b[j2] * k2;
                                 k3 += Direction.a[k1] * j3;
                                 l2 += Direction.b[k1] * j3;
-                                world.setTypeId(k3, j, l2, Block.ENDER_PORTAL.id);
+                                world.setTypeIdAndData(k3, j, l2, Block.ENDER_PORTAL.id, 0, 2);
                             }
                         }
                     }
@@ -132,7 +132,7 @@ public class ItemEnderEye extends Item {
 
                 entityendersignal.a((double) chunkposition.x, chunkposition.y, (double) chunkposition.z);
                 world.addEntity(entityendersignal);
-                world.makeSound(entityhuman, "random.bow", 0.5F, 0.4F / (d.nextFloat() * 0.4F + 0.8F));
+                world.makeSound(entityhuman, "random.bow", 0.5F, 0.4F / (e.nextFloat() * 0.4F + 0.8F));
                 world.a((EntityHuman) null, 1002, (int) entityhuman.locX, (int) entityhuman.locY, (int) entityhuman.locZ, 0);
                 if (!entityhuman.abilities.canInstantlyBuild) {
                     --itemstack.count;
