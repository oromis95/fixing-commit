@@ -5,17 +5,17 @@ import java.util.Random;
 
 public class BlockPistonExtension extends Block {
 
-    private int a = -1;
+    private IIcon a = null;
 
-    public BlockPistonExtension(int i, int j) {
-        super(i, j, Material.PISTON);
-        this.a(h);
+    public BlockPistonExtension(int i) {
+        super(i, Material.PISTON);
+        this.a(j);
         this.c(0.5F);
     }
 
     public void remove(World world, int i, int j, int k, int l, int i1) {
         super.remove(world, i, j, k, l, i1);
-        int j1 = Facing.OPPOSITE_FACING[f(i1)];
+        int j1 = Facing.OPPOSITE_FACING[d(i1)];
 
         i += Facing.b[j1];
         j += Facing.c[j1];
@@ -24,19 +24,13 @@ public class BlockPistonExtension extends Block {
 
         if (k1 == Block.PISTON.id || k1 == Block.PISTON_STICKY.id) {
             i1 = world.getData(i, j, k);
-            if (BlockPiston.f(i1)) {
+            if (BlockPiston.e(i1)) {
                 Block.byId[k1].c(world, i, j, k, i1, 0);
-                world.setTypeId(i, j, k, 0);
+                world.setAir(i, j, k);
             }
         }
     }
 
-    public int a(int i, int j) {
-        int k = f(j);
-
-        return i == k ? (this.a >= 0 ? this.a : ((j & 8) != 0 ? this.textureId - 1 : this.textureId)) : (k < 6 && i == Facing.OPPOSITE_FACING[k] ? 107 : 108);
-    }
-
     public int d() {
         return 17;
     }
@@ -64,7 +58,7 @@ public class BlockPistonExtension extends Block {
     public void a(World world, int i, int j, int k, AxisAlignedBB axisalignedbb, List list, Entity entity) {
         int l = world.getData(i, j, k);
 
-        switch (f(l)) {
+        switch (d(l)) {
         case 0:
             this.a(0.0F, 0.0F, 0.0F, 1.0F, 0.25F, 1.0F);
             super.a(world, i, j, k, axisalignedbb, list, entity);
@@ -113,7 +107,7 @@ public class BlockPistonExtension extends Block {
     public void updateShape(IBlockAccess iblockaccess, int i, int j, int k) {
         int l = iblockaccess.getData(i, j, k);
 
-        switch (f(l)) {
+        switch (d(l)) {
         case 0:
             this.a(0.0F, 0.0F, 0.0F, 1.0F, 0.25F, 1.0F);
             break;
@@ -140,17 +134,17 @@ public class BlockPistonExtension extends Block {
     }
 
     public void doPhysics(World world, int i, int j, int k, int l) {
-        int i1 = f(world.getData(i, j, k));
+        int i1 = d(world.getData(i, j, k));
         int j1 = world.getTypeId(i - Facing.b[i1], j - Facing.c[i1], k - Facing.d[i1]);
 
         if (j1 != Block.PISTON.id && j1 != Block.PISTON_STICKY.id) {
-            world.setTypeId(i, j, k, 0);
+            world.setAir(i, j, k);
         } else {
             Block.byId[j1].doPhysics(world, i - Facing.b[i1], j - Facing.c[i1], k - Facing.d[i1], l);
         }
     }
 
-    public static int f(int i) {
+    public static int d(int i) {
         return i & 7;
     }
 }
