@@ -25,7 +25,7 @@ public class RandomPositionGenerator {
     }
 
     private static Vec3D c(EntityCreature entitycreature, int i, int j, Vec3D vec3d) {
-        Random random = entitycreature.aB();
+        Random random = entitycreature.aE();
         boolean flag = false;
         int k = 0;
         int l = 0;
@@ -33,9 +33,9 @@ public class RandomPositionGenerator {
         float f = -99999.0F;
         boolean flag1;
 
-        if (entitycreature.aM()) {
-            double d0 = (double) (entitycreature.aJ().e(MathHelper.floor(entitycreature.locX), MathHelper.floor(entitycreature.locY), MathHelper.floor(entitycreature.locZ)) + 4.0F);
-            double d1 = (double) (entitycreature.aK() + (float) i);
+        if (entitycreature.aP()) {
+            double d0 = (double) (entitycreature.aM().e(MathHelper.floor(entitycreature.locX), MathHelper.floor(entitycreature.locY), MathHelper.floor(entitycreature.locZ)) + 4.0F);
+            double d1 = (double) (entitycreature.aN() + (float) i);
 
             flag1 = d0 < d1 * d1;
         } else {
@@ -51,7 +51,7 @@ public class RandomPositionGenerator {
                 k1 += MathHelper.floor(entitycreature.locX);
                 l1 += MathHelper.floor(entitycreature.locY);
                 i2 += MathHelper.floor(entitycreature.locZ);
-                if (!flag1 || entitycreature.e(k1, l1, i2)) {
+                if (!flag1 || entitycreature.d(k1, l1, i2)) {
                     float f1 = entitycreature.a(k1, l1, i2);
 
                     if (f1 > f) {
