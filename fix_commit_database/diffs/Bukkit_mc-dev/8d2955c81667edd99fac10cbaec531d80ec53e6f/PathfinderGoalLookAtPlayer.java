@@ -26,7 +26,7 @@ public class PathfinderGoalLookAtPlayer extends PathfinderGoal {
     }
 
     public boolean a() {
-        if (this.b.aB().nextFloat() >= this.e) {
+        if (this.b.aE().nextFloat() >= this.e) {
             return false;
         } else {
             if (this.f == EntityHuman.class) {
@@ -44,7 +44,7 @@ public class PathfinderGoalLookAtPlayer extends PathfinderGoal {
     }
 
     public void c() {
-        this.d = 40 + this.b.aB().nextInt(40);
+        this.d = 40 + this.b.aE().nextInt(40);
     }
 
     public void d() {
@@ -52,7 +52,7 @@ public class PathfinderGoalLookAtPlayer extends PathfinderGoal {
     }
 
     public void e() {
-        this.b.getControllerLook().a(this.a.locX, this.a.locY + (double) this.a.getHeadHeight(), this.a.locZ, 10.0F, (float) this.b.bp());
+        this.b.getControllerLook().a(this.a.locX, this.a.locY + (double) this.a.getHeadHeight(), this.a.locZ, 10.0F, (float) this.b.bs());
         --this.d;
     }
 }
