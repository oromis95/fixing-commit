@@ -2,8 +2,8 @@ package net.minecraft.server;
 
 public class BlockDirt extends Block {
 
-    protected BlockDirt(int i, int j) {
-        super(i, j, Material.EARTH);
+    protected BlockDirt(int i) {
+        super(i, Material.EARTH);
         this.a(CreativeModeTab.b);
     }
 }
