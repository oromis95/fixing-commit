@@ -15,25 +15,25 @@ public class PathfinderGoalHurtByTarget extends PathfinderGoalTarget {
     }
 
     public boolean a() {
-        return this.a(this.d.aC(), true);
+        return this.a(this.d.aF(), true);
     }
 
     public boolean b() {
-        return this.d.aC() != null && this.d.aC() != this.b;
+        return this.d.aF() != null && this.d.aF() != this.b;
     }
 
     public void c() {
-        this.d.setGoalTarget(this.d.aC());
-        this.b = this.d.aC();
+        this.d.setGoalTarget(this.d.aF());
+        this.b = this.d.aF();
         if (this.a) {
-            List list = this.d.world.a(this.d.getClass(), AxisAlignedBB.a().a(this.d.locX, this.d.locY, this.d.locZ, this.d.locX + 1.0D, this.d.locY + 1.0D, this.d.locZ + 1.0D).grow((double) this.e, 4.0D, (double) this.e));
+            List list = this.d.world.a(this.d.getClass(), AxisAlignedBB.a().a(this.d.locX, this.d.locY, this.d.locZ, this.d.locX + 1.0D, this.d.locY + 1.0D, this.d.locZ + 1.0D).grow((double) this.e, 10.0D, (double) this.e));
             Iterator iterator = list.iterator();
 
             while (iterator.hasNext()) {
                 EntityLiving entityliving = (EntityLiving) iterator.next();
 
                 if (this.d != entityliving && entityliving.getGoalTarget() == null) {
-                    entityliving.setGoalTarget(this.d.aC());
+                    entityliving.setGoalTarget(this.d.aF());
                 }
             }
         }
