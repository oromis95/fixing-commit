@@ -33,7 +33,7 @@ public class InventoryMerchant implements IInventory {
                 itemstack = this.itemsInSlots[i];
                 this.itemsInSlots[i] = null;
                 if (this.d(i)) {
-                    this.g();
+                    this.h();
                 }
 
                 return itemstack;
@@ -44,7 +44,7 @@ public class InventoryMerchant implements IInventory {
                 }
 
                 if (this.d(i)) {
-                    this.g();
+                    this.h();
                 }
 
                 return itemstack;
@@ -76,7 +76,7 @@ public class InventoryMerchant implements IInventory {
         }
 
         if (this.d(i)) {
-            this.g();
+            this.h();
         }
     }
 
@@ -84,23 +84,31 @@ public class InventoryMerchant implements IInventory {
         return "mob.villager";
     }
 
+    public boolean c() {
+        return false;
+    }
+
     public int getMaxStackSize() {
         return 64;
     }
 
-    public boolean a_(EntityHuman entityhuman) {
+    public boolean a(EntityHuman entityhuman) {
         return this.merchant.m_() == entityhuman;
     }
 
     public void startOpen() {}
 
-    public void f() {}
+    public void g() {}
+
+    public boolean b(int i, ItemStack itemstack) {
+        return true;
+    }
 
     public void update() {
-        this.g();
+        this.h();
     }
 
-    public void g() {
+    public void h() {
         this.recipe = null;
         ItemStack itemstack = this.itemsInSlots[0];
         ItemStack itemstack1 = this.itemsInSlots[1];
@@ -142,6 +150,6 @@ public class InventoryMerchant implements IInventory {
 
     public void c(int i) {
         this.e = i;
-        this.g();
+        this.h();
     }
 }
