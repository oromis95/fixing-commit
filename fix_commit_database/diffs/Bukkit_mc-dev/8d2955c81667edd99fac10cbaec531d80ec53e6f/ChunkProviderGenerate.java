@@ -390,12 +390,12 @@ public class ChunkProviderGenerate implements IChunkProvider {
         for (k1 = 0; k1 < 16; ++k1) {
             for (l1 = 0; l1 < 16; ++l1) {
                 i2 = this.p.h(k + k1, l + l1);
-                if (this.p.w(k1 + k, i2 - 1, l1 + l)) {
-                    this.p.setTypeId(k1 + k, i2 - 1, l1 + l, Block.ICE.id);
+                if (this.p.x(k1 + k, i2 - 1, l1 + l)) {
+                    this.p.setTypeIdAndData(k1 + k, i2 - 1, l1 + l, Block.ICE.id, 0, 2);
                 }
 
-                if (this.p.y(k1 + k, i2, l1 + l)) {
-                    this.p.setTypeId(k1 + k, i2, l1 + l, Block.SNOW.id);
+                if (this.p.z(k1 + k, i2, l1 + l)) {
+                    this.p.setTypeIdAndData(k1 + k, i2, l1 + l, Block.SNOW.id, 0, 2);
                 }
             }
         }
