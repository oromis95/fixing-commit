@@ -5,11 +5,12 @@ import java.util.Random;
 
 public class BlockChest extends BlockContainer {
 
-    private Random a = new Random();
+    private final Random b = new Random();
+    public final int a;
 
-    protected BlockChest(int i) {
+    protected BlockChest(int i, int j) {
         super(i, Material.WOOD);
-        this.textureId = 26;
+        this.a = j;
         this.a(CreativeModeTab.c);
         this.a(0.0625F, 0.0F, 0.0625F, 0.9375F, 0.875F, 0.9375F);
     }
@@ -42,30 +43,30 @@ public class BlockChest extends BlockContainer {
 
     public void onPlace(World world, int i, int j, int k) {
         super.onPlace(world, i, j, k);
-        this.d_(world, i, j, k);
+        this.f_(world, i, j, k);
         int l = world.getTypeId(i, j, k - 1);
         int i1 = world.getTypeId(i, j, k + 1);
         int j1 = world.getTypeId(i - 1, j, k);
         int k1 = world.getTypeId(i + 1, j, k);
 
         if (l == this.id) {
-            this.d_(world, i, j, k - 1);
+            this.f_(world, i, j, k - 1);
         }
 
         if (i1 == this.id) {
-            this.d_(world, i, j, k + 1);
+            this.f_(world, i, j, k + 1);
         }
 
         if (j1 == this.id) {
-            this.d_(world, i - 1, j, k);
+            this.f_(world, i - 1, j, k);
         }
 
         if (k1 == this.id) {
-            this.d_(world, i + 1, j, k);
+            this.f_(world, i + 1, j, k);
         }
     }
 
-    public void postPlace(World world, int i, int j, int k, EntityLiving entityliving) {
+    public void postPlace(World world, int i, int j, int k, EntityLiving entityliving, ItemStack itemstack) {
         int l = world.getTypeId(i, j, k - 1);
         int i1 = world.getTypeId(i, j, k + 1);
         int j1 = world.getTypeId(i - 1, j, k);
@@ -90,31 +91,35 @@ public class BlockChest extends BlockContainer {
         }
 
         if (l != this.id && i1 != this.id && j1 != this.id && k1 != this.id) {
-            world.setData(i, j, k, b0);
+            world.setData(i, j, k, b0, 3);
         } else {
             if ((l == this.id || i1 == this.id) && (b0 == 4 || b0 == 5)) {
                 if (l == this.id) {
-                    world.setData(i, j, k - 1, b0);
+                    world.setData(i, j, k - 1, b0, 3);
                 } else {
-                    world.setData(i, j, k + 1, b0);
+                    world.setData(i, j, k + 1, b0, 3);
                 }
 
-                world.setData(i, j, k, b0);
+                world.setData(i, j, k, b0, 3);
             }
 
             if ((j1 == this.id || k1 == this.id) && (b0 == 2 || b0 == 3)) {
                 if (j1 == this.id) {
-                    world.setData(i - 1, j, k, b0);
+                    world.setData(i - 1, j, k, b0, 3);
                 } else {
-                    world.setData(i + 1, j, k, b0);
+                    world.setData(i + 1, j, k, b0, 3);
                 }
 
-                world.setData(i, j, k, b0);
+                world.setData(i, j, k, b0, 3);
             }
         }
+
+        if (itemstack.hasName()) {
+            ((TileEntityChest) world.getTileEntity(i, j, k)).a(itemstack.getName());
+        }
     }
 
-    public void d_(World world, int i, int j, int k) {
+    public void f_(World world, int i, int j, int k) {
         if (!world.isStatic) {
             int l = world.getTypeId(i, j, k - 1);
             int i1 = world.getTypeId(i, j, k + 1);
@@ -130,19 +135,19 @@ public class BlockChest extends BlockContainer {
             if (l != this.id && i1 != this.id) {
                 if (j1 != this.id && k1 != this.id) {
                     b0 = 3;
-                    if (Block.q[l] && !Block.q[i1]) {
+                    if (Block.s[l] && !Block.s[i1]) {
                         b0 = 3;
                     }
 
-                    if (Block.q[i1] && !Block.q[l]) {
+                    if (Block.s[i1] && !Block.s[l]) {
                         b0 = 2;
                     }
 
-                    if (Block.q[j1] && !Block.q[k1]) {
+                    if (Block.s[j1] && !Block.s[k1]) {
                         b0 = 5;
                     }
 
-                    if (Block.q[k1] && !Block.q[j1]) {
+                    if (Block.s[k1] && !Block.s[j1]) {
                         b0 = 4;
                     }
                 } else {
@@ -160,11 +165,11 @@ public class BlockChest extends BlockContainer {
                         b0 = 2;
                     }
 
-                    if ((Block.q[l] || Block.q[l1]) && !Block.q[i1] && !Block.q[i2]) {
+                    if ((Block.s[l] || Block.s[l1]) && !Block.s[i1] && !Block.s[i2]) {
                         b0 = 3;
                     }
 
-                    if ((Block.q[i1] || Block.q[i2]) && !Block.q[l] && !Block.q[l1]) {
+                    if ((Block.s[i1] || Block.s[i2]) && !Block.s[l] && !Block.s[l1]) {
                         b0 = 2;
                     }
                 }
@@ -183,23 +188,19 @@ public class BlockChest extends BlockContainer {
                     b0 = 4;
                 }
 
-                if ((Block.q[j1] || Block.q[l1]) && !Block.q[k1] && !Block.q[i2]) {
+                if ((Block.s[j1] || Block.s[l1]) && !Block.s[k1] && !Block.s[i2]) {
                     b0 = 5;
                 }
 
-                if ((Block.q[k1] || Block.q[i2]) && !Block.q[j1] && !Block.q[l1]) {
+                if ((Block.s[k1] || Block.s[i2]) && !Block.s[j1] && !Block.s[l1]) {
                     b0 = 4;
                 }
             }
 
-            world.setData(i, j, k, b0);
+            world.setData(i, j, k, b0, 3);
         }
     }
 
-    public int a(int i) {
-        return 4;
-    }
-
     public boolean canPlace(World world, int i, int j, int k) {
         int l = 0;
 
@@ -219,10 +220,10 @@ public class BlockChest extends BlockContainer {
             ++l;
         }
 
-        return l > 1 ? false : (this.l(world, i - 1, j, k) ? false : (this.l(world, i + 1, j, k) ? false : (this.l(world, i, j, k - 1) ? false : !this.l(world, i, j, k + 1))));
+        return l > 1 ? false : (this.k(world, i - 1, j, k) ? false : (this.k(world, i + 1, j, k) ? false : (this.k(world, i, j, k - 1) ? false : !this.k(world, i, j, k + 1))));
     }
 
-    private boolean l(World world, int i, int j, int k) {
+    private boolean k(World world, int i, int j, int k) {
         return world.getTypeId(i, j, k) != this.id ? false : (world.getTypeId(i - 1, j, k) == this.id ? true : (world.getTypeId(i + 1, j, k) == this.id ? true : (world.getTypeId(i, j, k - 1) == this.id ? true : world.getTypeId(i, j, k + 1) == this.id)));
     }
 
@@ -231,7 +232,7 @@ public class BlockChest extends BlockContainer {
         TileEntityChest tileentitychest = (TileEntityChest) world.getTileEntity(i, j, k);
 
         if (tileentitychest != null) {
-            tileentitychest.h();
+            tileentitychest.i();
         }
     }
 
@@ -243,13 +244,13 @@ public class BlockChest extends BlockContainer {
                 ItemStack itemstack = tileentitychest.getItem(j1);
 
                 if (itemstack != null) {
-                    float f = this.a.nextFloat() * 0.8F + 0.1F;
-                    float f1 = this.a.nextFloat() * 0.8F + 0.1F;
+                    float f = this.b.nextFloat() * 0.8F + 0.1F;
+                    float f1 = this.b.nextFloat() * 0.8F + 0.1F;
 
                     EntityItem entityitem;
 
-                    for (float f2 = this.a.nextFloat() * 0.8F + 0.1F; itemstack.count > 0; world.addEntity(entityitem)) {
-                        int k1 = this.a.nextInt(21) + 10;
+                    for (float f2 = this.b.nextFloat() * 0.8F + 0.1F; itemstack.count > 0; world.addEntity(entityitem)) {
+                        int k1 = this.b.nextInt(21) + 10;
 
                         if (k1 > itemstack.count) {
                             k1 = itemstack.count;
@@ -259,9 +260,9 @@ public class BlockChest extends BlockContainer {
                         entityitem = new EntityItem(world, (double) ((float) i + f), (double) ((float) j + f1), (double) ((float) k + f2), new ItemStack(itemstack.id, k1, itemstack.getData()));
                         float f3 = 0.05F;
 
-                        entityitem.motX = (double) ((float) this.a.nextGaussian() * f3);
-                        entityitem.motY = (double) ((float) this.a.nextGaussian() * f3 + 0.2F);
-                        entityitem.motZ = (double) ((float) this.a.nextGaussian() * f3);
+                        entityitem.motX = (double) ((float) this.b.nextGaussian() * f3);
+                        entityitem.motY = (double) ((float) this.b.nextGaussian() * f3 + 0.2F);
+                        entityitem.motZ = (double) ((float) this.b.nextGaussian() * f3);
                         if (itemstack.hasTag()) {
                             entityitem.getItemStack().setTag((NBTTagCompound) itemstack.getTag().clone());
                         }
@@ -274,22 +275,36 @@ public class BlockChest extends BlockContainer {
     }
 
     public boolean interact(World world, int i, int j, int k, EntityHuman entityhuman, int l, float f, float f1, float f2) {
+        if (world.isStatic) {
+            return true;
+        } else {
+            IInventory iinventory = this.g_(world, i, j, k);
+
+            if (iinventory != null) {
+                entityhuman.openContainer(iinventory);
+            }
+
+            return true;
+        }
+    }
+
+    public IInventory g_(World world, int i, int j, int k) {
         Object object = (TileEntityChest) world.getTileEntity(i, j, k);
 
         if (object == null) {
-            return true;
-        } else if (world.t(i, j + 1, k)) {
-            return true;
-        } else if (n(world, i, j, k)) {
-            return true;
-        } else if (world.getTypeId(i - 1, j, k) == this.id && (world.t(i - 1, j + 1, k) || n(world, i - 1, j, k))) {
-            return true;
-        } else if (world.getTypeId(i + 1, j, k) == this.id && (world.t(i + 1, j + 1, k) || n(world, i + 1, j, k))) {
-            return true;
-        } else if (world.getTypeId(i, j, k - 1) == this.id && (world.t(i, j + 1, k - 1) || n(world, i, j, k - 1))) {
-            return true;
-        } else if (world.getTypeId(i, j, k + 1) == this.id && (world.t(i, j + 1, k + 1) || n(world, i, j, k + 1))) {
-            return true;
+            return null;
+        } else if (world.u(i, j + 1, k)) {
+            return null;
+        } else if (m(world, i, j, k)) {
+            return null;
+        } else if (world.getTypeId(i - 1, j, k) == this.id && (world.u(i - 1, j + 1, k) || m(world, i - 1, j, k))) {
+            return null;
+        } else if (world.getTypeId(i + 1, j, k) == this.id && (world.u(i + 1, j + 1, k) || m(world, i + 1, j, k))) {
+            return null;
+        } else if (world.getTypeId(i, j, k - 1) == this.id && (world.u(i, j + 1, k - 1) || m(world, i, j, k - 1))) {
+            return null;
+        } else if (world.getTypeId(i, j, k + 1) == this.id && (world.u(i, j + 1, k + 1) || m(world, i, j, k + 1))) {
+            return null;
         } else {
             if (world.getTypeId(i - 1, j, k) == this.id) {
                 object = new InventoryLargeChest("container.chestDouble", (TileEntityChest) world.getTileEntity(i - 1, j, k), (IInventory) object);
@@ -307,20 +322,35 @@ public class BlockChest extends BlockContainer {
                 object = new InventoryLargeChest("container.chestDouble", (IInventory) object, (TileEntityChest) world.getTileEntity(i, j, k + 1));
             }
 
-            if (world.isStatic) {
-                return true;
-            } else {
-                entityhuman.openContainer((IInventory) object);
-                return true;
-            }
+            return (IInventory) object;
+        }
+    }
+
+    public TileEntity b(World world) {
+        TileEntityChest tileentitychest = new TileEntityChest();
+
+        return tileentitychest;
+    }
+
+    public boolean isPowerSource() {
+        return this.a == 1;
+    }
+
+    public int b(IBlockAccess iblockaccess, int i, int j, int k, int l) {
+        if (!this.isPowerSource()) {
+            return 0;
+        } else {
+            int i1 = ((TileEntityChest) iblockaccess.getTileEntity(i, j, k)).h;
+
+            return MathHelper.a(i1, 0, 15);
         }
     }
 
-    public TileEntity a(World world) {
-        return new TileEntityChest();
+    public int c(IBlockAccess iblockaccess, int i, int j, int k, int l) {
+        return l == 1 ? this.b(iblockaccess, i, j, k, l) : 0;
     }
 
-    private static boolean n(World world, int i, int j, int k) {
+    private static boolean m(World world, int i, int j, int k) {
         Iterator iterator = world.a(EntityOcelot.class, AxisAlignedBB.a().a((double) i, (double) (j + 1), (double) k, (double) (i + 1), (double) (j + 2), (double) (k + 1))).iterator();
 
         EntityOcelot entityocelot;
@@ -337,4 +367,12 @@ public class BlockChest extends BlockContainer {
 
         return true;
     }
+
+    public boolean q_() {
+        return true;
+    }
+
+    public int b_(World world, int i, int j, int k, int l) {
+        return Container.b(this.g_(world, i, j, k));
+    }
 }
