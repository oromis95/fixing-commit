@@ -0,0 +1,8 @@
+package net.minecraft.server;
+
+public interface IWorldInventory extends IInventory {
+
+    int c(int i);
+
+    int d(int i);
+}
