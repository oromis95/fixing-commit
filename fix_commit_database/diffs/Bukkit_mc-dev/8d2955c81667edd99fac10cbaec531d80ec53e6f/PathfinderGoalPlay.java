@@ -19,7 +19,7 @@ public class PathfinderGoalPlay extends PathfinderGoal {
     public boolean a() {
         if (this.a.getAge() >= 0) {
             return false;
-        } else if (this.a.aB().nextInt(400) != 0) {
+        } else if (this.a.aE().nextInt(400) != 0) {
             return false;
         } else {
             List list = this.a.world.a(EntityVillager.class, this.a.boundingBox.grow(6.0D, 3.0D, 6.0D));
@@ -57,14 +57,14 @@ public class PathfinderGoalPlay extends PathfinderGoal {
 
     public void c() {
         if (this.b != null) {
-            this.a.g(true);
+            this.a.j(true);
         }
 
         this.d = 1000;
     }
 
     public void d() {
-        this.a.g(false);
+        this.a.j(false);
         this.b = null;
     }
 
