@@ -3,28 +3,19 @@ package net.minecraft.server;
 public class BlockCloth extends Block {
 
     public BlockCloth() {
-        super(35, 64, Material.CLOTH);
+        super(35, Material.CLOTH);
         this.a(CreativeModeTab.b);
     }
 
-    public int a(int i, int j) {
-        if (j == 0) {
-            return this.textureId;
-        } else {
-            j = ~(j & 15);
-            return 113 + ((j & 8) >> 3) + (j & 7) * 16;
-        }
-    }
-
     public int getDropData(int i) {
         return i;
     }
 
-    public static int e_(int i) {
+    public static int g_(int i) {
         return ~i & 15;
     }
 
-    public static int d(int i) {
+    public static int c(int i) {
         return ~i & 15;
     }
 }
