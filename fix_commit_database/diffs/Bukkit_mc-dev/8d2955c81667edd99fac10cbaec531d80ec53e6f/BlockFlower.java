@@ -4,9 +4,8 @@ import java.util.Random;
 
 public class BlockFlower extends Block {
 
-    protected BlockFlower(int i, int j, Material material) {
+    protected BlockFlower(int i, Material material) {
         super(i, material);
-        this.textureId = j;
         this.b(true);
         float f = 0.2F;
 
@@ -14,39 +13,39 @@ public class BlockFlower extends Block {
         this.a(CreativeModeTab.c);
     }
 
-    protected BlockFlower(int i, int j) {
-        this(i, j, Material.PLANT);
+    protected BlockFlower(int i) {
+        this(i, Material.PLANT);
     }
 
     public boolean canPlace(World world, int i, int j, int k) {
-        return super.canPlace(world, i, j, k) && this.d_(world.getTypeId(i, j - 1, k));
+        return super.canPlace(world, i, j, k) && this.f_(world.getTypeId(i, j - 1, k));
     }
 
-    protected boolean d_(int i) {
+    protected boolean f_(int i) {
         return i == Block.GRASS.id || i == Block.DIRT.id || i == Block.SOIL.id;
     }
 
     public void doPhysics(World world, int i, int j, int k, int l) {
         super.doPhysics(world, i, j, k, l);
-        this.c(world, i, j, k);
+        this.e(world, i, j, k);
     }
 
-    public void b(World world, int i, int j, int k, Random random) {
-        this.c(world, i, j, k);
+    public void a(World world, int i, int j, int k, Random random) {
+        this.e(world, i, j, k);
     }
 
-    protected final void c(World world, int i, int j, int k) {
-        if (!this.d(world, i, j, k)) {
+    protected final void e(World world, int i, int j, int k) {
+        if (!this.f(world, i, j, k)) {
             this.c(world, i, j, k, world.getData(i, j, k), 0);
-            world.setTypeId(i, j, k, 0);
+            world.setAir(i, j, k);
         }
     }
 
-    public boolean d(World world, int i, int j, int k) {
-        return (world.l(i, j, k) >= 8 || world.k(i, j, k)) && this.d_(world.getTypeId(i, j - 1, k));
+    public boolean f(World world, int i, int j, int k) {
+        return (world.m(i, j, k) >= 8 || world.l(i, j, k)) && this.f_(world.getTypeId(i, j - 1, k));
     }
 
-    public AxisAlignedBB e(World world, int i, int j, int k) {
+    public AxisAlignedBB b(World world, int i, int j, int k) {
         return null;
     }
 
