@@ -29,11 +29,10 @@ public abstract class World implements IBlockAccess {
     protected float o;
     protected float p;
     public int q = 0;
-    public boolean suppressPhysics = false;
     public int difficulty;
     public Random random = new Random();
     public final WorldProvider worldProvider;
-    protected List v = new ArrayList();
+    protected List u = new ArrayList();
     protected IChunkProvider chunkProvider;
     protected final IDataManager dataManager;
     protected WorldData worldData;
@@ -44,14 +43,15 @@ public abstract class World implements IBlockAccess {
     public final MethodProfiler methodProfiler;
     private final Vec3DPool J = new Vec3DPool(300, 2000);
     private final Calendar K = Calendar.getInstance();
-    private ArrayList L = new ArrayList();
-    private boolean M;
+    protected Scoreboard scoreboard = new Scoreboard();
+    private final IConsoleLogManager logAgent;
+    private ArrayList M = new ArrayList();
+    private boolean N;
     protected boolean allowMonsters = true;
     protected boolean allowAnimals = true;
     protected Set chunkTickList = new HashSet();
-    private int N;
+    private int O;
     int[] H;
-    private List O;
     public boolean isStatic;
 
     public BiomeBase getBiome(int i, int j) {
@@ -70,14 +70,14 @@ public abstract class World implements IBlockAccess {
         return this.worldProvider.d;
     }
 
-    public World(IDataManager idatamanager, String s, WorldSettings worldsettings, WorldProvider worldprovider, MethodProfiler methodprofiler) {
-        this.N = this.random.nextInt(12000);
+    public World(IDataManager idatamanager, String s, WorldSettings worldsettings, WorldProvider worldprovider, MethodProfiler methodprofiler, IConsoleLogManager iconsolelogmanager) {
+        this.O = this.random.nextInt(12000);
         this.H = new int['\u8000'];
-        this.O = new ArrayList();
         this.isStatic = false;
         this.dataManager = idatamanager;
         this.methodProfiler = methodprofiler;
         this.worldMaps = new WorldMapCollection(idatamanager);
+        this.logAgent = iconsolelogmanager;
         this.worldData = idatamanager.getWorldData();
         if (worldprovider != null) {
             this.worldProvider = worldprovider;
@@ -123,7 +123,7 @@ public abstract class World implements IBlockAccess {
             this.villages.a(this);
         }
 
-        this.x();
+        this.y();
         this.a();
     }
 
@@ -169,10 +169,6 @@ public abstract class World implements IBlockAccess {
         }
     }
 
-    public int b(int i, int j, int k) {
-        return i >= -30000000 && k >= -30000000 && i < 30000000 && k < 30000000 ? (j < 0 ? 0 : (j >= 256 ? 0 : this.getChunkAt(i >> 4, k >> 4).b(i & 15, j, k & 15))) : 0;
-    }
-
     public boolean isEmpty(int i, int j, int k) {
         return this.getTypeId(i, j, k) == 0;
     }
@@ -180,7 +176,7 @@ public abstract class World implements IBlockAccess {
     public boolean isTileEntity(int i, int j, int k) {
         int l = this.getTypeId(i, j, k);
 
-        return Block.byId[l] != null && Block.byId[l].u();
+        return Block.byId[l] != null && Block.byId[l].t();
     }
 
     public int e(int i, int j, int k) {
@@ -194,10 +190,10 @@ public abstract class World implements IBlockAccess {
     }
 
     public boolean areChunksLoaded(int i, int j, int k, int l) {
-        return this.d(i - l, j - l, k - l, i + l, j + l, k + l);
+        return this.e(i - l, j - l, k - l, i + l, j + l, k + l);
     }
 
-    public boolean d(int i, int j, int k, int l, int i1, int j1) {
+    public boolean e(int i, int j, int k, int l, int i1, int j1) {
         if (i1 >= 0 && j < 256) {
             i >>= 4;
             k >>= 4;
@@ -230,11 +226,7 @@ public abstract class World implements IBlockAccess {
         return this.chunkProvider.getOrCreateChunk(i, j);
     }
 
-    public boolean setRawTypeIdAndData(int i, int j, int k, int l, int i1) {
-        return this.setRawTypeIdAndData(i, j, k, l, i1, true);
-    }
-
-    public boolean setRawTypeIdAndData(int i, int j, int k, int l, int i1, boolean flag) {
+    public boolean setTypeIdAndData(int i, int j, int k, int l, int i1, int j1) {
         if (i >= -30000000 && k >= -30000000 && i < 30000000 && k < 30000000) {
             if (j < 0) {
                 return false;
@@ -242,37 +234,30 @@ public abstract class World implements IBlockAccess {
                 return false;
             } else {
                 Chunk chunk = this.getChunkAt(i >> 4, k >> 4);
-                boolean flag1 = chunk.a(i & 15, j, k & 15, l, i1);
+                int k1 = 0;
 
-                this.methodProfiler.a("checkLight");
-                this.z(i, j, k);
-                this.methodProfiler.b();
-                if (flag && flag1 && (this.isStatic || chunk.seenByPlayer)) {
-                    this.notify(i, j, k);
+                if ((j1 & 1) != 0) {
+                    k1 = chunk.getTypeId(i & 15, j, k & 15);
                 }
 
-                return flag1;
-            }
-        } else {
-            return false;
-        }
-    }
-
-    public boolean setRawTypeId(int i, int j, int k, int l) {
-        if (i >= -30000000 && k >= -30000000 && i < 30000000 && k < 30000000) {
-            if (j < 0) {
-                return false;
-            } else if (j >= 256) {
-                return false;
-            } else {
-                Chunk chunk = this.getChunkAt(i >> 4, k >> 4);
-                boolean flag = chunk.a(i & 15, j, k & 15, l);
+                boolean flag = chunk.a(i & 15, j, k & 15, l, i1);
 
                 this.methodProfiler.a("checkLight");
-                this.z(i, j, k);
+                this.A(i, j, k);
                 this.methodProfiler.b();
-                if (flag && (this.isStatic || chunk.seenByPlayer)) {
-                    this.notify(i, j, k);
+                if (flag) {
+                    if ((j1 & 2) != 0 && (!this.isStatic || (j1 & 4) == 0)) {
+                        this.notify(i, j, k);
+                    }
+
+                    if (!this.isStatic && (j1 & 1) != 0) {
+                        this.update(i, j, k, k1);
+                        Block block = Block.byId[l];
+
+                        if (block != null && block.q_()) {
+                            this.m(i, j, k, l);
+                        }
+                    }
                 }
 
                 return flag;
@@ -306,13 +291,7 @@ public abstract class World implements IBlockAccess {
         }
     }
 
-    public void setData(int i, int j, int k, int l) {
-        if (this.setRawData(i, j, k, l)) {
-            this.update(i, j, k, this.getTypeId(i, j, k));
-        }
-    }
-
-    public boolean setRawData(int i, int j, int k, int l) {
+    public boolean setData(int i, int j, int k, int l, int i1) {
         if (i >= -30000000 && k >= -30000000 && i < 30000000 && k < 30000000) {
             if (j < 0) {
                 return false;
@@ -320,12 +299,25 @@ public abstract class World implements IBlockAccess {
                 return false;
             } else {
                 Chunk chunk = this.getChunkAt(i >> 4, k >> 4);
-                int i1 = i & 15;
-                int j1 = k & 15;
-                boolean flag = chunk.b(i1, j, j1, l);
+                int j1 = i & 15;
+                int k1 = k & 15;
+                boolean flag = chunk.b(j1, j, k1, l);
+
+                if (flag) {
+                    int l1 = chunk.getTypeId(j1, j, k1);
 
-                if (flag && (this.isStatic || chunk.seenByPlayer && Block.u[chunk.getTypeId(i1, j, j1) & 4095])) {
-                    this.notify(i, j, k);
+                    if ((i1 & 2) != 0 && (!this.isStatic || (i1 & 4) == 0)) {
+                        this.notify(i, j, k);
+                    }
+
+                    if (!this.isStatic && (i1 & 1) != 0) {
+                        this.update(i, j, k, l1);
+                        Block block = Block.byId[l1];
+
+                        if (block != null && block.q_()) {
+                            this.m(i, j, k, l1);
+                        }
+                    }
                 }
 
                 return flag;
@@ -335,27 +327,34 @@ public abstract class World implements IBlockAccess {
         }
     }
 
-    public boolean setTypeId(int i, int j, int k, int l) {
-        if (this.setRawTypeId(i, j, k, l)) {
-            this.update(i, j, k, l);
-            return true;
-        } else {
-            return false;
-        }
+    public boolean setAir(int i, int j, int k) {
+        return this.setTypeIdAndData(i, j, k, 0, 0, 3);
     }
 
-    public boolean setTypeIdAndData(int i, int j, int k, int l, int i1) {
-        if (this.setRawTypeIdAndData(i, j, k, l, i1)) {
-            this.update(i, j, k, l);
-            return true;
+    public boolean setAir(int i, int j, int k, boolean flag) {
+        int l = this.getTypeId(i, j, k);
+
+        if (l > 0) {
+            int i1 = this.getData(i, j, k);
+
+            this.triggerEffect(2001, i, j, k, l + (i1 << 12));
+            if (flag) {
+                Block.byId[l].c(this, i, j, k, i1, 0);
+            }
+
+            return this.setTypeIdAndData(i, j, k, 0, 0, 3);
         } else {
             return false;
         }
     }
 
+    public boolean setTypeIdUpdate(int i, int j, int k, int l) {
+        return this.setTypeIdAndData(i, j, k, l, 0, 3);
+    }
+
     public void notify(int i, int j, int k) {
-        for (int l = 0; l < this.v.size(); ++l) {
-            ((IWorldAccess) this.v.get(l)).a(i, j, k);
+        for (int l = 0; l < this.u.size(); ++l) {
+            ((IWorldAccess) this.u.get(l)).a(i, j, k);
         }
     }
 
@@ -363,7 +362,7 @@ public abstract class World implements IBlockAccess {
         this.applyPhysics(i, j, k, l);
     }
 
-    public void g(int i, int j, int k, int l) {
+    public void e(int i, int j, int k, int l) {
         int i1;
 
         if (k > l) {
@@ -378,32 +377,52 @@ public abstract class World implements IBlockAccess {
             }
         }
 
-        this.e(i, k, j, i, l, j);
+        this.g(i, k, j, i, l, j);
     }
 
-    public void j(int i, int j, int k) {
-        for (int l = 0; l < this.v.size(); ++l) {
-            ((IWorldAccess) this.v.get(l)).a(i, j, k, i, j, k);
+    public void g(int i, int j, int k, int l, int i1, int j1) {
+        for (int k1 = 0; k1 < this.u.size(); ++k1) {
+            ((IWorldAccess) this.u.get(k1)).a(i, j, k, l, i1, j1);
         }
     }
 
-    public void e(int i, int j, int k, int l, int i1, int j1) {
-        for (int k1 = 0; k1 < this.v.size(); ++k1) {
-            ((IWorldAccess) this.v.get(k1)).a(i, j, k, l, i1, j1);
-        }
+    public void applyPhysics(int i, int j, int k, int l) {
+        this.g(i - 1, j, k, l);
+        this.g(i + 1, j, k, l);
+        this.g(i, j - 1, k, l);
+        this.g(i, j + 1, k, l);
+        this.g(i, j, k - 1, l);
+        this.g(i, j, k + 1, l);
     }
 
-    public void applyPhysics(int i, int j, int k, int l) {
-        this.m(i - 1, j, k, l);
-        this.m(i + 1, j, k, l);
-        this.m(i, j - 1, k, l);
-        this.m(i, j + 1, k, l);
-        this.m(i, j, k - 1, l);
-        this.m(i, j, k + 1, l);
+    public void c(int i, int j, int k, int l, int i1) {
+        if (i1 != 4) {
+            this.g(i - 1, j, k, l);
+        }
+
+        if (i1 != 5) {
+            this.g(i + 1, j, k, l);
+        }
+
+        if (i1 != 0) {
+            this.g(i, j - 1, k, l);
+        }
+
+        if (i1 != 1) {
+            this.g(i, j + 1, k, l);
+        }
+
+        if (i1 != 2) {
+            this.g(i, j, k - 1, l);
+        }
+
+        if (i1 != 3) {
+            this.g(i, j, k + 1, l);
+        }
     }
 
-    private void m(int i, int j, int k, int l) {
-        if (!this.suppressPhysics && !this.isStatic) {
+    public void g(int i, int j, int k, int l) {
+        if (!this.isStatic) {
             int i1 = this.getTypeId(i, j, k);
             Block block = Block.byId[i1];
 
@@ -430,11 +449,15 @@ public abstract class World implements IBlockAccess {
         }
     }
 
-    public boolean k(int i, int j, int k) {
+    public boolean a(int i, int j, int k, int l) {
+        return false;
+    }
+
+    public boolean l(int i, int j, int k) {
         return this.getChunkAt(i >> 4, k >> 4).d(i & 15, j, k & 15);
     }
 
-    public int l(int i, int j, int k) {
+    public int m(int i, int j, int k) {
         if (j < 0) {
             return 0;
         } else {
@@ -447,20 +470,20 @@ public abstract class World implements IBlockAccess {
     }
 
     public int getLightLevel(int i, int j, int k) {
-        return this.a(i, j, k, true);
+        return this.b(i, j, k, true);
     }
 
-    public int a(int i, int j, int k, boolean flag) {
+    public int b(int i, int j, int k, boolean flag) {
         if (i >= -30000000 && k >= -30000000 && i < 30000000 && k < 30000000) {
             if (flag) {
                 int l = this.getTypeId(i, j, k);
 
-                if (l == Block.STEP.id || l == Block.WOOD_STEP.id || l == Block.SOIL.id || l == Block.COBBLESTONE_STAIRS.id || l == Block.WOOD_STAIRS.id) {
-                    int i1 = this.a(i, j + 1, k, false);
-                    int j1 = this.a(i + 1, j, k, false);
-                    int k1 = this.a(i - 1, j, k, false);
-                    int l1 = this.a(i, j, k + 1, false);
-                    int i2 = this.a(i, j, k - 1, false);
+                if (Block.w[l]) {
+                    int i1 = this.b(i, j + 1, k, false);
+                    int j1 = this.b(i + 1, j, k, false);
+                    int k1 = this.b(i - 1, j, k, false);
+                    int l1 = this.b(i, j, k + 1, false);
+                    int i2 = this.b(i, j, k - 1, false);
 
                     if (j1 > i1) {
                         i1 = j1;
@@ -562,8 +585,8 @@ public abstract class World implements IBlockAccess {
 
                         chunk.a(enumskyblock, i & 15, j, k & 15, l);
 
-                        for (int i1 = 0; i1 < this.v.size(); ++i1) {
-                            ((IWorldAccess) this.v.get(i1)).b(i, j, k);
+                        for (int i1 = 0; i1 < this.u.size(); ++i1) {
+                            ((IWorldAccess) this.u.get(i1)).b(i, j, k);
                         }
                     }
                 }
@@ -571,13 +594,13 @@ public abstract class World implements IBlockAccess {
         }
     }
 
-    public void o(int i, int j, int k) {
-        for (int l = 0; l < this.v.size(); ++l) {
-            ((IWorldAccess) this.v.get(l)).b(i, j, k);
+    public void p(int i, int j, int k) {
+        for (int l = 0; l < this.u.size(); ++l) {
+            ((IWorldAccess) this.u.get(l)).b(i, j, k);
         }
     }
 
-    public float p(int i, int j, int k) {
+    public float q(int i, int j, int k) {
         return this.worldProvider.g[this.getLightLevel(i, j, k)];
     }
 
@@ -606,7 +629,7 @@ public abstract class World implements IBlockAccess {
                 int l1 = this.getData(l, i1, j1);
                 Block block = Block.byId[k1];
 
-                if ((!flag1 || block == null || block.e(this, l, i1, j1) != null) && k1 > 0 && block.a(l1, flag)) {
+                if ((!flag1 || block == null || block.b(this, l, i1, j1) != null) && k1 > 0 && block.a(l1, flag)) {
                     MovingObjectPosition movingobjectposition = block.a(this, l, i1, j1, vec3d, vec3d1);
 
                     if (movingobjectposition != null) {
@@ -734,7 +757,7 @@ public abstract class World implements IBlockAccess {
                     int j2 = this.getData(l, i1, j1);
                     Block block1 = Block.byId[i2];
 
-                    if ((!flag1 || block1 == null || block1.e(this, l, i1, j1) != null) && i2 > 0 && block1.a(j2, flag)) {
+                    if ((!flag1 || block1 == null || block1.b(this, l, i1, j1) != null) && i2 > 0 && block1.a(j2, flag)) {
                         MovingObjectPosition movingobjectposition1 = block1.a(this, l, i1, j1, vec3d, vec3d1);
 
                         if (movingobjectposition1 != null) {
@@ -754,24 +777,24 @@ public abstract class World implements IBlockAccess {
 
     public void makeSound(Entity entity, String s, float f, float f1) {
         if (entity != null && s != null) {
-            for (int i = 0; i < this.v.size(); ++i) {
-                ((IWorldAccess) this.v.get(i)).a(s, entity.locX, entity.locY - (double) entity.height, entity.locZ, f, f1);
+            for (int i = 0; i < this.u.size(); ++i) {
+                ((IWorldAccess) this.u.get(i)).a(s, entity.locX, entity.locY - (double) entity.height, entity.locZ, f, f1);
             }
         }
     }
 
     public void a(EntityHuman entityhuman, String s, float f, float f1) {
         if (entityhuman != null && s != null) {
-            for (int i = 0; i < this.v.size(); ++i) {
-                ((IWorldAccess) this.v.get(i)).a(entityhuman, s, entityhuman.locX, entityhuman.locY - (double) entityhuman.height, entityhuman.locZ, f, f1);
+            for (int i = 0; i < this.u.size(); ++i) {
+                ((IWorldAccess) this.u.get(i)).a(entityhuman, s, entityhuman.locX, entityhuman.locY - (double) entityhuman.height, entityhuman.locZ, f, f1);
             }
         }
     }
 
     public void makeSound(double d0, double d1, double d2, String s, float f, float f1) {
         if (s != null) {
-            for (int i = 0; i < this.v.size(); ++i) {
-                ((IWorldAccess) this.v.get(i)).a(s, d0, d1, d2, f, f1);
+            for (int i = 0; i < this.u.size(); ++i) {
+                ((IWorldAccess) this.u.get(i)).a(s, d0, d1, d2, f, f1);
             }
         }
     }
@@ -779,14 +802,14 @@ public abstract class World implements IBlockAccess {
     public void a(double d0, double d1, double d2, String s, float f, float f1, boolean flag) {}
 
     public void a(String s, int i, int j, int k) {
-        for (int l = 0; l < this.v.size(); ++l) {
-            ((IWorldAccess) this.v.get(l)).a(s, i, j, k);
+        for (int l = 0; l < this.u.size(); ++l) {
+            ((IWorldAccess) this.u.get(l)).a(s, i, j, k);
         }
     }
 
     public void addParticle(String s, double d0, double d1, double d2, double d3, double d4, double d5) {
-        for (int i = 0; i < this.v.size(); ++i) {
-            ((IWorldAccess) this.v.get(i)).a(s, d0, d1, d2, d3, d4, d5);
+        for (int i = 0; i < this.u.size(); ++i) {
+            ((IWorldAccess) this.u.get(i)).a(s, d0, d1, d2, d3, d4, d5);
         }
     }
 
@@ -798,7 +821,7 @@ public abstract class World implements IBlockAccess {
     public boolean addEntity(Entity entity) {
         int i = MathHelper.floor(entity.locX / 16.0D);
         int j = MathHelper.floor(entity.locZ / 16.0D);
-        boolean flag = false;
+        boolean flag = entity.p;
 
         if (entity instanceof EntityHuman) {
             flag = true;
@@ -822,14 +845,14 @@ public abstract class World implements IBlockAccess {
     }
 
     protected void a(Entity entity) {
-        for (int i = 0; i < this.v.size(); ++i) {
-            ((IWorldAccess) this.v.get(i)).a(entity);
+        for (int i = 0; i < this.u.size(); ++i) {
+            ((IWorldAccess) this.u.get(i)).a(entity);
         }
     }
 
     protected void b(Entity entity) {
-        for (int i = 0; i < this.v.size(); ++i) {
-            ((IWorldAccess) this.v.get(i)).b(entity);
+        for (int i = 0; i < this.u.size(); ++i) {
+            ((IWorldAccess) this.u.get(i)).b(entity);
         }
     }
 
@@ -856,10 +879,10 @@ public abstract class World implements IBlockAccess {
             this.everyoneSleeping();
         }
 
-        int i = entity.ai;
-        int j = entity.ak;
+        int i = entity.aj;
+        int j = entity.al;
 
-        if (entity.ah && this.isChunkLoaded(i, j)) {
+        if (entity.ai && this.isChunkLoaded(i, j)) {
             this.getChunkAt(i, j).b(entity);
         }
 
@@ -868,11 +891,11 @@ public abstract class World implements IBlockAccess {
     }
 
     public void addIWorldAccess(IWorldAccess iworldaccess) {
-        this.v.add(iworldaccess);
+        this.u.add(iworldaccess);
     }
 
     public List getCubes(Entity entity, AxisAlignedBB axisalignedbb) {
-        this.L.clear();
+        this.M.clear();
         int i = MathHelper.floor(axisalignedbb.a);
         int j = MathHelper.floor(axisalignedbb.d + 1.0D);
         int k = MathHelper.floor(axisalignedbb.b);
@@ -887,7 +910,7 @@ public abstract class World implements IBlockAccess {
                         Block block = Block.byId[this.getTypeId(k1, i2, l1)];
 
                         if (block != null) {
-                            block.a(this, k1, i2, l1, axisalignedbb, this.L, entity);
+                            block.a(this, k1, i2, l1, axisalignedbb, this.M, entity);
                         }
                     }
                 }
@@ -898,23 +921,23 @@ public abstract class World implements IBlockAccess {
         List list = this.getEntities(entity, axisalignedbb.grow(d0, d0, d0));
 
         for (int j2 = 0; j2 < list.size(); ++j2) {
-            AxisAlignedBB axisalignedbb1 = ((Entity) list.get(j2)).E();
+            AxisAlignedBB axisalignedbb1 = ((Entity) list.get(j2)).D();
 
             if (axisalignedbb1 != null && axisalignedbb1.a(axisalignedbb)) {
-                this.L.add(axisalignedbb1);
+                this.M.add(axisalignedbb1);
             }
 
             axisalignedbb1 = entity.g((Entity) list.get(j2));
             if (axisalignedbb1 != null && axisalignedbb1.a(axisalignedbb)) {
-                this.L.add(axisalignedbb1);
+                this.M.add(axisalignedbb1);
             }
         }
 
-        return this.L;
+        return this.M;
     }
 
     public List a(AxisAlignedBB axisalignedbb) {
-        this.L.clear();
+        this.M.clear();
         int i = MathHelper.floor(axisalignedbb.a);
         int j = MathHelper.floor(axisalignedbb.d + 1.0D);
         int k = MathHelper.floor(axisalignedbb.b);
@@ -929,14 +952,14 @@ public abstract class World implements IBlockAccess {
                         Block block = Block.byId[this.getTypeId(k1, i2, l1)];
 
                         if (block != null) {
-                            block.a(this, k1, i2, l1, axisalignedbb, this.L, (Entity) null);
+                            block.a(this, k1, i2, l1, axisalignedbb, this.M, (Entity) null);
                         }
                     }
                 }
             }
         }
 
-        return this.L;
+        return this.M;
     }
 
     public int a(float f) {
@@ -952,8 +975,8 @@ public abstract class World implements IBlockAccess {
         }
 
         f2 = 1.0F - f2;
-        f2 = (float) ((double) f2 * (1.0D - (double) (this.j(f) * 5.0F) / 16.0D));
         f2 = (float) ((double) f2 * (1.0D - (double) (this.i(f) * 5.0F) / 16.0D));
+        f2 = (float) ((double) f2 * (1.0D - (double) (this.h(f) * 5.0F) / 16.0D));
         f2 = 1.0F - f2;
         return (int) (f2 * 11.0F);
     }
@@ -962,6 +985,16 @@ public abstract class World implements IBlockAccess {
         return this.worldProvider.a(this.worldData.getDayTime(), f);
     }
 
+    public int v() {
+        return this.worldProvider.a(this.worldData.getDayTime());
+    }
+
+    public float d(float f) {
+        float f1 = this.c(f);
+
+        return f1 * 3.1415927F * 2.0F;
+    }
+
     public int h(int i, int j) {
         return this.getChunkAtWorldCoords(i, j).d(i & 15, j & 15);
     }
@@ -987,7 +1020,7 @@ public abstract class World implements IBlockAccess {
 
     public void a(int i, int j, int k, int l, int i1, int j1) {}
 
-    public void b(int i, int j, int k, int l, int i1) {}
+    public void b(int i, int j, int k, int l, int i1, int j1) {}
 
     public void tickEntities() {
         this.methodProfiler.a("entities");
@@ -1003,7 +1036,7 @@ public abstract class World implements IBlockAccess {
 
             try {
                 ++entity.ticksLived;
-                entity.j_();
+                entity.l_();
             } catch (Throwable throwable) {
                 crashreport = CrashReport.a(throwable, "Ticking entity");
                 crashreportsystemdetails = crashreport.a("Entity being ticked");
@@ -1029,9 +1062,9 @@ public abstract class World implements IBlockAccess {
 
         for (i = 0; i < this.f.size(); ++i) {
             entity = (Entity) this.f.get(i);
-            j = entity.ai;
-            k = entity.ak;
-            if (entity.ah && this.isChunkLoaded(j, k)) {
+            j = entity.aj;
+            k = entity.al;
+            if (entity.ai && this.isChunkLoaded(j, k)) {
                 this.getChunkAt(j, k).b(entity);
             }
         }
@@ -1061,12 +1094,7 @@ public abstract class World implements IBlockAccess {
                 } catch (Throwable throwable1) {
                     crashreport = CrashReport.a(throwable1, "Ticking entity");
                     crashreportsystemdetails = crashreport.a("Entity being ticked");
-                    if (entity == null) {
-                        crashreportsystemdetails.a("Entity", "~~NULL~~");
-                    } else {
-                        entity.a(crashreportsystemdetails);
-                    }
-
+                    entity.a(crashreportsystemdetails);
                     throw new ReportedException(crashreport);
                 }
             }
@@ -1074,9 +1102,9 @@ public abstract class World implements IBlockAccess {
             this.methodProfiler.b();
             this.methodProfiler.a("remove");
             if (entity.dead) {
-                j = entity.ai;
-                k = entity.ak;
-                if (entity.ah && this.isChunkLoaded(j, k)) {
+                j = entity.aj;
+                k = entity.al;
+                if (entity.ai && this.isChunkLoaded(j, k)) {
                     this.getChunkAt(j, k).b(entity);
                 }
 
@@ -1088,7 +1116,7 @@ public abstract class World implements IBlockAccess {
         }
 
         this.methodProfiler.c("tileEntities");
-        this.M = true;
+        this.N = true;
         Iterator iterator = this.tileEntityList.iterator();
 
         while (iterator.hasNext()) {
@@ -1096,16 +1124,11 @@ public abstract class World implements IBlockAccess {
 
             if (!tileentity.r() && tileentity.o() && this.isLoaded(tileentity.x, tileentity.y, tileentity.z)) {
                 try {
-                    tileentity.g();
+                    tileentity.h();
                 } catch (Throwable throwable2) {
                     crashreport = CrashReport.a(throwable2, "Ticking tile entity");
                     crashreportsystemdetails = crashreport.a("Tile entity being ticked");
-                    if (tileentity == null) {
-                        crashreportsystemdetails.a("Tile entity", "~~NULL~~");
-                    } else {
-                        tileentity.a(crashreportsystemdetails);
-                    }
-
+                    tileentity.a(crashreportsystemdetails);
                     throw new ReportedException(crashreport);
                 }
             }
@@ -1122,7 +1145,7 @@ public abstract class World implements IBlockAccess {
             }
         }
 
-        this.M = false;
+        this.N = false;
         if (!this.b.isEmpty()) {
             this.tileEntityList.removeAll(this.b);
             this.b.clear();
@@ -1158,7 +1181,7 @@ public abstract class World implements IBlockAccess {
     }
 
     public void a(Collection collection) {
-        if (this.M) {
+        if (this.N) {
             this.a.addAll(collection);
         } else {
             this.tileEntityList.addAll(collection);
@@ -1174,32 +1197,32 @@ public abstract class World implements IBlockAccess {
         int j = MathHelper.floor(entity.locZ);
         byte b0 = 32;
 
-        if (!flag || this.d(i - b0, 0, j - b0, i + b0, 0, j + b0)) {
-            entity.T = entity.locX;
-            entity.U = entity.locY;
-            entity.V = entity.locZ;
+        if (!flag || this.e(i - b0, 0, j - b0, i + b0, 0, j + b0)) {
+            entity.U = entity.locX;
+            entity.V = entity.locY;
+            entity.W = entity.locZ;
             entity.lastYaw = entity.yaw;
             entity.lastPitch = entity.pitch;
-            if (flag && entity.ah) {
+            if (flag && entity.ai) {
                 if (entity.vehicle != null) {
-                    entity.U();
+                    entity.T();
                 } else {
                     ++entity.ticksLived;
-                    entity.j_();
+                    entity.l_();
                 }
             }
 
             this.methodProfiler.a("chunkCheck");
             if (Double.isNaN(entity.locX) || Double.isInfinite(entity.locX)) {
-                entity.locX = entity.T;
+                entity.locX = entity.U;
             }
 
             if (Double.isNaN(entity.locY) || Double.isInfinite(entity.locY)) {
-                entity.locY = entity.U;
+                entity.locY = entity.V;
             }
 
             if (Double.isNaN(entity.locZ) || Double.isInfinite(entity.locZ)) {
-                entity.locZ = entity.V;
+                entity.locZ = entity.W;
             }
 
             if (Double.isNaN((double) entity.pitch) || Double.isInfinite((double) entity.pitch)) {
@@ -1214,21 +1237,21 @@ public abstract class World implements IBlockAccess {
             int l = MathHelper.floor(entity.locY / 16.0D);
             int i1 = MathHelper.floor(entity.locZ / 16.0D);
 
-            if (!entity.ah || entity.ai != k || entity.aj != l || entity.ak != i1) {
-                if (entity.ah && this.isChunkLoaded(entity.ai, entity.ak)) {
-                    this.getChunkAt(entity.ai, entity.ak).a(entity, entity.aj);
+            if (!entity.ai || entity.aj != k || entity.ak != l || entity.al != i1) {
+                if (entity.ai && this.isChunkLoaded(entity.aj, entity.al)) {
+                    this.getChunkAt(entity.aj, entity.al).a(entity, entity.ak);
                 }
 
                 if (this.isChunkLoaded(k, i1)) {
-                    entity.ah = true;
+                    entity.ai = true;
                     this.getChunkAt(k, i1).a(entity);
                 } else {
-                    entity.ah = false;
+                    entity.ai = false;
                 }
             }
 
             this.methodProfiler.b();
-            if (flag && entity.ah && entity.passenger != null) {
+            if (flag && entity.ai && entity.passenger != null) {
                 if (!entity.passenger.dead && entity.passenger.vehicle == entity) {
                     this.playerJoinedWorld(entity.passenger);
                 } else {
@@ -1335,7 +1358,7 @@ public abstract class World implements IBlockAccess {
         int i1 = MathHelper.floor(axisalignedbb.c);
         int j1 = MathHelper.floor(axisalignedbb.f + 1.0D);
 
-        if (this.d(i, k, i1, j, l, j1)) {
+        if (this.e(i, k, i1, j, l, j1)) {
             for (int k1 = i; k1 < j; ++k1) {
                 for (int l1 = k; l1 < l; ++l1) {
                     for (int i2 = i1; i2 < j1; ++i2) {
@@ -1360,7 +1383,7 @@ public abstract class World implements IBlockAccess {
         int i1 = MathHelper.floor(axisalignedbb.c);
         int j1 = MathHelper.floor(axisalignedbb.f + 1.0D);
 
-        if (!this.d(i, k, i1, j, l, j1)) {
+        if (!this.e(i, k, i1, j, l, j1)) {
             return false;
         } else {
             boolean flag = false;
@@ -1372,7 +1395,7 @@ public abstract class World implements IBlockAccess {
                         Block block = Block.byId[this.getTypeId(k1, l1, i2)];
 
                         if (block != null && block.material == material) {
-                            double d0 = (double) ((float) (l1 + 1) - BlockFluids.e(this.getData(k1, l1, i2)));
+                            double d0 = (double) ((float) (l1 + 1) - BlockFluids.d(this.getData(k1, l1, i2)));
 
                             if ((double) l >= d0) {
                                 flag = true;
@@ -1383,7 +1406,7 @@ public abstract class World implements IBlockAccess {
                 }
             }
 
-            if (vec3d.b() > 0.0D) {
+            if (vec3d.b() > 0.0D && entity.aw()) {
                 vec3d = vec3d.a();
                 double d1 = 0.014D;
 
@@ -1518,7 +1541,7 @@ public abstract class World implements IBlockAccess {
 
         if (this.getTypeId(i, j, k) == Block.FIRE.id) {
             this.a(entityhuman, 1004, i, j, k, 0);
-            this.setTypeId(i, j, k, 0);
+            this.setAir(i, j, k);
             return true;
         } else {
             return false;
@@ -1526,38 +1549,62 @@ public abstract class World implements IBlockAccess {
     }
 
     public TileEntity getTileEntity(int i, int j, int k) {
-        if (j >= 256) {
-            return null;
-        } else {
-            Chunk chunk = this.getChunkAt(i >> 4, k >> 4);
+        if (j >= 0 && j < 256) {
+            TileEntity tileentity = null;
+            int l;
+            TileEntity tileentity1;
+
+            if (this.N) {
+                for (l = 0; l < this.a.size(); ++l) {
+                    tileentity1 = (TileEntity) this.a.get(l);
+                    if (!tileentity1.r() && tileentity1.x == i && tileentity1.y == j && tileentity1.z == k) {
+                        tileentity = tileentity1;
+                        break;
+                    }
+                }
+            }
 
-            if (chunk == null) {
-                return null;
-            } else {
-                TileEntity tileentity = chunk.e(i & 15, j, k & 15);
+            if (tileentity == null) {
+                Chunk chunk = this.getChunkAt(i >> 4, k >> 4);
 
-                if (tileentity == null) {
-                    for (int l = 0; l < this.a.size(); ++l) {
-                        TileEntity tileentity1 = (TileEntity) this.a.get(l);
+                if (chunk != null) {
+                    tileentity = chunk.e(i & 15, j, k & 15);
+                }
+            }
 
-                        if (!tileentity1.r() && tileentity1.x == i && tileentity1.y == j && tileentity1.z == k) {
-                            tileentity = tileentity1;
-                            break;
-                        }
+            if (tileentity == null) {
+                for (l = 0; l < this.a.size(); ++l) {
+                    tileentity1 = (TileEntity) this.a.get(l);
+                    if (!tileentity1.r() && tileentity1.x == i && tileentity1.y == j && tileentity1.z == k) {
+                        tileentity = tileentity1;
+                        break;
                     }
                 }
-
-                return tileentity;
             }
+
+            return tileentity;
+        } else {
+            return null;
         }
     }
 
     public void setTileEntity(int i, int j, int k, TileEntity tileentity) {
         if (tileentity != null && !tileentity.r()) {
-            if (this.M) {
+            if (this.N) {
                 tileentity.x = i;
                 tileentity.y = j;
                 tileentity.z = k;
+                Iterator iterator = this.a.iterator();
+
+                while (iterator.hasNext()) {
+                    TileEntity tileentity1 = (TileEntity) iterator.next();
+
+                    if (tileentity1.x == i && tileentity1.y == j && tileentity1.z == k) {
+                        tileentity1.w_();
+                        iterator.remove();
+                    }
+                }
+
                 this.a.add(tileentity);
             } else {
                 this.tileEntityList.add(tileentity);
@@ -1570,10 +1617,10 @@ public abstract class World implements IBlockAccess {
         }
     }
 
-    public void r(int i, int j, int k) {
+    public void s(int i, int j, int k) {
         TileEntity tileentity = this.getTileEntity(i, j, k);
 
-        if (tileentity != null && this.M) {
+        if (tileentity != null && this.N) {
             tileentity.w_();
             this.a.remove(tileentity);
         } else {
@@ -1594,21 +1641,21 @@ public abstract class World implements IBlockAccess {
         this.b.add(tileentity);
     }
 
-    public boolean s(int i, int j, int k) {
+    public boolean t(int i, int j, int k) {
         Block block = Block.byId[this.getTypeId(i, j, k)];
 
         return block == null ? false : block.c();
     }
 
-    public boolean t(int i, int j, int k) {
-        return Block.i(this.getTypeId(i, j, k));
+    public boolean u(int i, int j, int k) {
+        return Block.l(this.getTypeId(i, j, k));
     }
 
-    public boolean u(int i, int j, int k) {
+    public boolean v(int i, int j, int k) {
         int l = this.getTypeId(i, j, k);
 
         if (l != 0 && Block.byId[l] != null) {
-            AxisAlignedBB axisalignedbb = Block.byId[l].e(this, i, j, k);
+            AxisAlignedBB axisalignedbb = Block.byId[l].b(this, i, j, k);
 
             return axisalignedbb != null && axisalignedbb.b() >= 1.0D;
         } else {
@@ -1616,13 +1663,13 @@ public abstract class World implements IBlockAccess {
         }
     }
 
-    public boolean v(int i, int j, int k) {
+    public boolean w(int i, int j, int k) {
         Block block = Block.byId[this.getTypeId(i, j, k)];
 
-        return block == null ? false : (block.material.k() && block.b() ? true : (block instanceof BlockStairs ? (this.getData(i, j, k) & 4) == 4 : (block instanceof BlockStepAbstract ? (this.getData(i, j, k) & 8) == 8 : false)));
+        return block == null ? false : (block.material.k() && block.b() ? true : (block instanceof BlockStairs ? (this.getData(i, j, k) & 4) == 4 : (block instanceof BlockStepAbstract ? (this.getData(i, j, k) & 8) == 8 : (block instanceof BlockHopper ? true : (block instanceof BlockSnow ? (this.getData(i, j, k) & 7) == 7 : false)))));
     }
 
-    public boolean b(int i, int j, int k, boolean flag) {
+    public boolean c(int i, int j, int k, boolean flag) {
         if (i >= -30000000 && k >= -30000000 && i < 30000000 && k < 30000000) {
             Chunk chunk = this.chunkProvider.getOrCreateChunk(i >> 4, k >> 4);
 
@@ -1638,7 +1685,7 @@ public abstract class World implements IBlockAccess {
         }
     }
 
-    public void x() {
+    public void y() {
         int i = this.a(1.0F);
 
         if (i != this.j) {
@@ -1730,11 +1777,11 @@ public abstract class World implements IBlockAccess {
         }
     }
 
-    public void y() {
+    public void z() {
         this.worldData.setWeatherDuration(1);
     }
 
-    protected void z() {
+    protected void A() {
         this.chunkTickList.clear();
         this.methodProfiler.a("buildList");
 
@@ -1757,8 +1804,8 @@ public abstract class World implements IBlockAccess {
         }
 
         this.methodProfiler.b();
-        if (this.N > 0) {
-            --this.N;
+        if (this.O > 0) {
+            --this.O;
         }
 
         this.methodProfiler.a("playerCheckLight");
@@ -1769,7 +1816,7 @@ public abstract class World implements IBlockAccess {
             k = MathHelper.floor(entityhuman.locY) + this.random.nextInt(11) - 5;
             int j1 = MathHelper.floor(entityhuman.locZ) + this.random.nextInt(11) - 5;
 
-            this.z(j, k, j1);
+            this.A(j, k, j1);
         }
 
         this.methodProfiler.b();
@@ -1777,7 +1824,7 @@ public abstract class World implements IBlockAccess {
 
     protected void a(int i, int j, Chunk chunk) {
         this.methodProfiler.c("moodSound");
-        if (this.N == 0 && !this.isStatic) {
+        if (this.O == 0 && !this.isStatic) {
             this.k = this.k * 3 + 1013904223;
             int k = this.k >> 2;
             int l = k & 15;
@@ -1787,12 +1834,12 @@ public abstract class World implements IBlockAccess {
 
             l += i;
             i1 += j;
-            if (k1 == 0 && this.l(l, j1, i1) <= this.random.nextInt(8) && this.b(EnumSkyBlock.SKY, l, j1, i1) <= 0) {
+            if (k1 == 0 && this.m(l, j1, i1) <= this.random.nextInt(8) && this.b(EnumSkyBlock.SKY, l, j1, i1) <= 0) {
                 EntityHuman entityhuman = this.findNearbyPlayer((double) l + 0.5D, (double) j1 + 0.5D, (double) i1 + 0.5D, 8.0D);
 
                 if (entityhuman != null && entityhuman.e((double) l + 0.5D, (double) j1 + 0.5D, (double) i1 + 0.5D) > 4.0D) {
                     this.makeSound((double) l + 0.5D, (double) j1 + 0.5D, (double) i1 + 0.5D, "ambient.cave.cave", 0.7F, 0.8F + this.random.nextFloat() * 0.2F);
-                    this.N = this.random.nextInt(12000) + 6000;
+                    this.O = this.random.nextInt(12000) + 6000;
                 }
             }
         }
@@ -1802,18 +1849,18 @@ public abstract class World implements IBlockAccess {
     }
 
     protected void g() {
-        this.z();
+        this.A();
     }
 
-    public boolean w(int i, int j, int k) {
-        return this.c(i, j, k, false);
+    public boolean x(int i, int j, int k) {
+        return this.d(i, j, k, false);
     }
 
-    public boolean x(int i, int j, int k) {
-        return this.c(i, j, k, true);
+    public boolean y(int i, int j, int k) {
+        return this.d(i, j, k, true);
     }
 
-    public boolean c(int i, int j, int k, boolean flag) {
+    public boolean d(int i, int j, int k, boolean flag) {
         BiomeBase biomebase = this.getBiome(i, k);
         float f = biomebase.j();
 
@@ -1856,7 +1903,7 @@ public abstract class World implements IBlockAccess {
         }
     }
 
-    public boolean y(int i, int j, int k) {
+    public boolean z(int i, int j, int k) {
         BiomeBase biomebase = this.getBiome(i, k);
         float f = biomebase.j();
 
@@ -1876,7 +1923,7 @@ public abstract class World implements IBlockAccess {
         }
     }
 
-    public void z(int i, int j, int k) {
+    public void A(int i, int j, int k) {
         if (!this.worldProvider.f) {
             this.c(EnumSkyBlock.SKY, i, j, k);
         }
@@ -1884,85 +1931,45 @@ public abstract class World implements IBlockAccess {
         this.c(EnumSkyBlock.BLOCK, i, j, k);
     }
 
-    private int b(int i, int j, int k, int l, int i1, int j1) {
-        int k1 = 0;
-
-        if (this.k(j, k, l)) {
-            k1 = 15;
+    private int a(int i, int j, int k, EnumSkyBlock enumskyblock) {
+        if (enumskyblock == EnumSkyBlock.SKY && this.l(i, j, k)) {
+            return 15;
         } else {
-            if (j1 == 0) {
-                j1 = 1;
-            }
+            int l = this.getTypeId(i, j, k);
+            int i1 = enumskyblock == EnumSkyBlock.SKY ? 0 : Block.lightEmission[l];
+            int j1 = Block.lightBlock[l];
 
-            int l1 = this.b(EnumSkyBlock.SKY, j - 1, k, l) - j1;
-            int i2 = this.b(EnumSkyBlock.SKY, j + 1, k, l) - j1;
-            int j2 = this.b(EnumSkyBlock.SKY, j, k - 1, l) - j1;
-            int k2 = this.b(EnumSkyBlock.SKY, j, k + 1, l) - j1;
-            int l2 = this.b(EnumSkyBlock.SKY, j, k, l - 1) - j1;
-            int i3 = this.b(EnumSkyBlock.SKY, j, k, l + 1) - j1;
-
-            if (l1 > k1) {
-                k1 = l1;
-            }
-
-            if (i2 > k1) {
-                k1 = i2;
+            if (j1 >= 15 && Block.lightEmission[l] > 0) {
+                j1 = 1;
             }
 
-            if (j2 > k1) {
-                k1 = j2;
+            if (j1 < 1) {
+                j1 = 1;
             }
 
-            if (k2 > k1) {
-                k1 = k2;
-            }
+            if (j1 >= 15) {
+                return 0;
+            } else if (i1 >= 14) {
+                return i1;
+            } else {
+                for (int k1 = 0; k1 < 6; ++k1) {
+                    int l1 = i + Facing.b[k1];
+                    int i2 = j + Facing.c[k1];
+                    int j2 = k + Facing.d[k1];
+                    int k2 = this.b(enumskyblock, l1, i2, j2) - j1;
+
+                    if (k2 > i1) {
+                        i1 = k2;
+                    }
 
-            if (l2 > k1) {
-                k1 = l2;
-            }
+                    if (i1 >= 14) {
+                        return i1;
+                    }
+                }
 
-            if (i3 > k1) {
-                k1 = i3;
+                return i1;
             }
         }
-
-        return k1;
-    }
-
-    private int g(int i, int j, int k, int l, int i1, int j1) {
-        int k1 = Block.lightEmission[i1];
-        int l1 = this.b(EnumSkyBlock.BLOCK, j - 1, k, l) - j1;
-        int i2 = this.b(EnumSkyBlock.BLOCK, j + 1, k, l) - j1;
-        int j2 = this.b(EnumSkyBlock.BLOCK, j, k - 1, l) - j1;
-        int k2 = this.b(EnumSkyBlock.BLOCK, j, k + 1, l) - j1;
-        int l2 = this.b(EnumSkyBlock.BLOCK, j, k, l - 1) - j1;
-        int i3 = this.b(EnumSkyBlock.BLOCK, j, k, l + 1) - j1;
-
-        if (l1 > k1) {
-            k1 = l1;
-        }
-
-        if (i2 > k1) {
-            k1 = i2;
-        }
-
-        if (j2 > k1) {
-            k1 = j2;
-        }
-
-        if (k2 > k1) {
-            k1 = k2;
-        }
-
-        if (l2 > k1) {
-            k1 = l2;
-        }
-
-        if (i3 > k1) {
-            k1 = i3;
-        }
-
-        return k1;
     }
 
     public void c(EnumSkyBlock enumskyblock, int i, int j, int k) {
@@ -1972,23 +1979,9 @@ public abstract class World implements IBlockAccess {
 
             this.methodProfiler.a("getBrightness");
             int j1 = this.b(enumskyblock, i, j, k);
-            boolean flag = false;
-            int k1 = this.getTypeId(i, j, k);
-            int l1 = this.b(i, j, k);
-
-            if (l1 == 0) {
-                l1 = 1;
-            }
-
-            boolean flag1 = false;
+            int k1 = this.a(i, j, k, enumskyblock);
+            int l1;
             int i2;
-
-            if (enumskyblock == EnumSkyBlock.SKY) {
-                i2 = this.b(j1, i, j, k, k1, l1);
-            } else {
-                i2 = this.g(j1, i, j, k, k1, l1);
-            }
-
             int j2;
             int k2;
             int l2;
@@ -1996,58 +1989,35 @@ public abstract class World implements IBlockAccess {
             int j3;
             int k3;
             int l3;
-            int i4;
 
-            if (i2 > j1) {
+            if (k1 > j1) {
                 this.H[i1++] = 133152;
-            } else if (i2 < j1) {
-                if (enumskyblock != EnumSkyBlock.BLOCK) {
-                    ;
-                }
-
-                this.H[i1++] = 133152 + (j1 << 18);
+            } else if (k1 < j1) {
+                this.H[i1++] = 133152 | j1 << 18;
 
                 while (l < i1) {
-                    k1 = this.H[l++];
-                    l1 = (k1 & 63) - 32 + i;
-                    i2 = (k1 >> 6 & 63) - 32 + j;
-                    j2 = (k1 >> 12 & 63) - 32 + k;
-                    k2 = k1 >> 18 & 15;
-                    l2 = this.b(enumskyblock, l1, i2, j2);
-                    if (l2 == k2) {
-                        this.b(enumskyblock, l1, i2, j2, 0);
-                        if (k2 > 0) {
-                            i3 = l1 - i;
-                            k3 = i2 - j;
-                            j3 = j2 - k;
-                            if (i3 < 0) {
-                                i3 = -i3;
-                            }
-
-                            if (k3 < 0) {
-                                k3 = -k3;
-                            }
-
-                            if (j3 < 0) {
-                                j3 = -j3;
-                            }
-
-                            if (i3 + k3 + j3 < 17) {
-                                for (i4 = 0; i4 < 6; ++i4) {
-                                    l3 = i4 % 2 * 2 - 1;
-                                    int j4 = l1 + i4 / 2 % 3 / 2 * l3;
-                                    int k4 = i2 + (i4 / 2 + 1) % 3 / 2 * l3;
-                                    int l4 = j2 + (i4 / 2 + 2) % 3 / 2 * l3;
-
-                                    l2 = this.b(enumskyblock, j4, k4, l4);
-                                    int i5 = Block.lightBlock[this.getTypeId(j4, k4, l4)];
-
-                                    if (i5 == 0) {
-                                        i5 = 1;
-                                    }
-
-                                    if (l2 == k2 - i5 && i1 < this.H.length) {
-                                        this.H[i1++] = j4 - i + 32 + (k4 - j + 32 << 6) + (l4 - k + 32 << 12) + (k2 - i5 << 18);
+                    l1 = this.H[l++];
+                    i2 = (l1 & 63) - 32 + i;
+                    j2 = (l1 >> 6 & 63) - 32 + j;
+                    k2 = (l1 >> 12 & 63) - 32 + k;
+                    l2 = l1 >> 18 & 15;
+                    i3 = this.b(enumskyblock, i2, j2, k2);
+                    if (i3 == l2) {
+                        this.b(enumskyblock, i2, j2, k2, 0);
+                        if (l2 > 0) {
+                            j3 = MathHelper.a(i2 - i);
+                            l3 = MathHelper.a(j2 - j);
+                            k3 = MathHelper.a(k2 - k);
+                            if (j3 + l3 + k3 < 17) {
+                                for (int i4 = 0; i4 < 6; ++i4) {
+                                    int j4 = i2 + Facing.b[i4];
+                                    int k4 = j2 + Facing.c[i4];
+                                    int l4 = k2 + Facing.d[i4];
+                                    int i5 = Math.max(1, Block.lightBlock[this.getTypeId(j4, k4, l4)]);
+
+                                    i3 = this.b(enumskyblock, j4, k4, l4);
+                                    if (i3 == l2 - i5 && i1 < this.H.length) {
+                                        this.H[i1++] = j4 - i + 32 | k4 - j + 32 << 6 | l4 - k + 32 << 12 | l2 - i5 << 18;
                                     }
                                 }
                             }
@@ -2062,66 +2032,43 @@ public abstract class World implements IBlockAccess {
             this.methodProfiler.a("checkedPosition < toCheckCount");
 
             while (l < i1) {
-                k1 = this.H[l++];
-                l1 = (k1 & 63) - 32 + i;
-                i2 = (k1 >> 6 & 63) - 32 + j;
-                j2 = (k1 >> 12 & 63) - 32 + k;
-                k2 = this.b(enumskyblock, l1, i2, j2);
-                l2 = this.getTypeId(l1, i2, j2);
-                i3 = Block.lightBlock[l2];
-                if (i3 == 0) {
-                    i3 = 1;
-                }
-
-                boolean flag2 = false;
-
-                if (enumskyblock == EnumSkyBlock.SKY) {
-                    k3 = this.b(k2, l1, i2, j2, l2, i3);
-                } else {
-                    k3 = this.g(k2, l1, i2, j2, l2, i3);
-                }
-
-                if (k3 != k2) {
-                    this.b(enumskyblock, l1, i2, j2, k3);
-                    if (k3 > k2) {
-                        j3 = l1 - i;
-                        i4 = i2 - j;
-                        l3 = j2 - k;
-                        if (j3 < 0) {
-                            j3 = -j3;
-                        }
-
-                        if (i4 < 0) {
-                            i4 = -i4;
-                        }
-
-                        if (l3 < 0) {
-                            l3 = -l3;
-                        }
-
-                        if (j3 + i4 + l3 < 17 && i1 < this.H.length - 6) {
-                            if (this.b(enumskyblock, l1 - 1, i2, j2) < k3) {
-                                this.H[i1++] = l1 - 1 - i + 32 + (i2 - j + 32 << 6) + (j2 - k + 32 << 12);
+                l1 = this.H[l++];
+                i2 = (l1 & 63) - 32 + i;
+                j2 = (l1 >> 6 & 63) - 32 + j;
+                k2 = (l1 >> 12 & 63) - 32 + k;
+                l2 = this.b(enumskyblock, i2, j2, k2);
+                i3 = this.a(i2, j2, k2, enumskyblock);
+                if (i3 != l2) {
+                    this.b(enumskyblock, i2, j2, k2, i3);
+                    if (i3 > l2) {
+                        j3 = Math.abs(i2 - i);
+                        l3 = Math.abs(j2 - j);
+                        k3 = Math.abs(k2 - k);
+                        boolean flag = i1 < this.H.length - 6;
+
+                        if (j3 + l3 + k3 < 17 && flag) {
+                            if (this.b(enumskyblock, i2 - 1, j2, k2) < i3) {
+                                this.H[i1++] = i2 - 1 - i + 32 + (j2 - j + 32 << 6) + (k2 - k + 32 << 12);
                             }
 
-                            if (this.b(enumskyblock, l1 + 1, i2, j2) < k3) {
-                                this.H[i1++] = l1 + 1 - i + 32 + (i2 - j + 32 << 6) + (j2 - k + 32 << 12);
+                            if (this.b(enumskyblock, i2 + 1, j2, k2) < i3) {
+                                this.H[i1++] = i2 + 1 - i + 32 + (j2 - j + 32 << 6) + (k2 - k + 32 << 12);
                             }
 
-                            if (this.b(enumskyblock, l1, i2 - 1, j2) < k3) {
-                                this.H[i1++] = l1 - i + 32 + (i2 - 1 - j + 32 << 6) + (j2 - k + 32 << 12);
+                            if (this.b(enumskyblock, i2, j2 - 1, k2) < i3) {
+                                this.H[i1++] = i2 - i + 32 + (j2 - 1 - j + 32 << 6) + (k2 - k + 32 << 12);
                             }
 
-                            if (this.b(enumskyblock, l1, i2 + 1, j2) < k3) {
-                                this.H[i1++] = l1 - i + 32 + (i2 + 1 - j + 32 << 6) + (j2 - k + 32 << 12);
+                            if (this.b(enumskyblock, i2, j2 + 1, k2) < i3) {
+                                this.H[i1++] = i2 - i + 32 + (j2 + 1 - j + 32 << 6) + (k2 - k + 32 << 12);
                             }
 
-                            if (this.b(enumskyblock, l1, i2, j2 - 1) < k3) {
-                                this.H[i1++] = l1 - i + 32 + (i2 - j + 32 << 6) + (j2 - 1 - k + 32 << 12);
+                            if (this.b(enumskyblock, i2, j2, k2 - 1) < i3) {
+                                this.H[i1++] = i2 - i + 32 + (j2 - j + 32 << 6) + (k2 - 1 - k + 32 << 12);
                             }
 
-                            if (this.b(enumskyblock, l1, i2, j2 + 1) < k3) {
-                                this.H[i1++] = l1 - i + 32 + (i2 - j + 32 << 6) + (j2 + 1 - k + 32 << 12);
+                            if (this.b(enumskyblock, i2, j2, k2 + 1) < i3) {
+                                this.H[i1++] = i2 - i + 32 + (j2 - j + 32 << 6) + (k2 + 1 - k + 32 << 12);
                             }
                         }
                     }
@@ -2141,7 +2088,11 @@ public abstract class World implements IBlockAccess {
     }
 
     public List getEntities(Entity entity, AxisAlignedBB axisalignedbb) {
-        this.O.clear();
+        return this.getEntities(entity, axisalignedbb, (IEntitySelector) null);
+    }
+
+    public List getEntities(Entity entity, AxisAlignedBB axisalignedbb, IEntitySelector ientityselector) {
+        ArrayList arraylist = new ArrayList();
         int i = MathHelper.floor((axisalignedbb.a - 2.0D) / 16.0D);
         int j = MathHelper.floor((axisalignedbb.d + 2.0D) / 16.0D);
         int k = MathHelper.floor((axisalignedbb.c - 2.0D) / 16.0D);
@@ -2150,12 +2101,12 @@ public abstract class World implements IBlockAccess {
         for (int i1 = i; i1 <= j; ++i1) {
             for (int j1 = k; j1 <= l; ++j1) {
                 if (this.isChunkLoaded(i1, j1)) {
-                    this.getChunkAt(i1, j1).a(entity, axisalignedbb, this.O);
+                    this.getChunkAt(i1, j1).a(entity, axisalignedbb, arraylist, ientityselector);
                 }
             }
         }
 
-        return this.O;
+        return arraylist;
     }
 
     public List a(Class oclass, AxisAlignedBB axisalignedbb) {
@@ -2235,11 +2186,11 @@ public abstract class World implements IBlockAccess {
         this.f.addAll(list);
     }
 
-    public boolean mayPlace(int i, int j, int k, int l, boolean flag, int i1, Entity entity) {
+    public boolean mayPlace(int i, int j, int k, int l, boolean flag, int i1, Entity entity, ItemStack itemstack) {
         int j1 = this.getTypeId(j, k, l);
         Block block = Block.byId[j1];
         Block block1 = Block.byId[i];
-        AxisAlignedBB axisalignedbb = block1.e(this, j, k, l);
+        AxisAlignedBB axisalignedbb = block1.b(this, j, k, l);
 
         if (flag) {
             axisalignedbb = null;
@@ -2252,7 +2203,7 @@ public abstract class World implements IBlockAccess {
                 block = null;
             }
 
-            return block != null && block.material == Material.ORIENTABLE && block1 == Block.ANVIL ? true : i > 0 && block == null && block1.canPlace(this, j, k, l, i1);
+            return block != null && block.material == Material.ORIENTABLE && block1 == Block.ANVIL ? true : i > 0 && block == null && block1.canPlace(this, j, k, l, i1, itemstack);
         }
     }
 
@@ -2294,28 +2245,78 @@ public abstract class World implements IBlockAccess {
         return pathentity;
     }
 
-    public boolean isBlockFacePowered(int i, int j, int k, int l) {
+    public int getBlockPower(int i, int j, int k, int l) {
         int i1 = this.getTypeId(i, j, k);
 
-        return i1 == 0 ? false : Block.byId[i1].c(this, i, j, k, l);
+        return i1 == 0 ? 0 : Block.byId[i1].c(this, i, j, k, l);
+    }
+
+    public int getBlockPower(int i, int j, int k) {
+        byte b0 = 0;
+        int l = Math.max(b0, this.getBlockPower(i, j - 1, k, 0));
+
+        if (l >= 15) {
+            return l;
+        } else {
+            l = Math.max(l, this.getBlockPower(i, j + 1, k, 1));
+            if (l >= 15) {
+                return l;
+            } else {
+                l = Math.max(l, this.getBlockPower(i, j, k - 1, 2));
+                if (l >= 15) {
+                    return l;
+                } else {
+                    l = Math.max(l, this.getBlockPower(i, j, k + 1, 3));
+                    if (l >= 15) {
+                        return l;
+                    } else {
+                        l = Math.max(l, this.getBlockPower(i - 1, j, k, 4));
+                        if (l >= 15) {
+                            return l;
+                        } else {
+                            l = Math.max(l, this.getBlockPower(i + 1, j, k, 5));
+                            return l >= 15 ? l : l;
+                        }
+                    }
+                }
+            }
+        }
     }
 
-    public boolean isBlockPowered(int i, int j, int k) {
-        return this.isBlockFacePowered(i, j - 1, k, 0) ? true : (this.isBlockFacePowered(i, j + 1, k, 1) ? true : (this.isBlockFacePowered(i, j, k - 1, 2) ? true : (this.isBlockFacePowered(i, j, k + 1, 3) ? true : (this.isBlockFacePowered(i - 1, j, k, 4) ? true : this.isBlockFacePowered(i + 1, j, k, 5)))));
+    public boolean isBlockFacePowered(int i, int j, int k, int l) {
+        return this.getBlockFacePower(i, j, k, l) > 0;
     }
 
-    public boolean isBlockFaceIndirectlyPowered(int i, int j, int k, int l) {
-        if (this.t(i, j, k)) {
-            return this.isBlockPowered(i, j, k);
+    public int getBlockFacePower(int i, int j, int k, int l) {
+        if (this.u(i, j, k)) {
+            return this.getBlockPower(i, j, k);
         } else {
             int i1 = this.getTypeId(i, j, k);
 
-            return i1 == 0 ? false : Block.byId[i1].b(this, i, j, k, l);
+            return i1 == 0 ? 0 : Block.byId[i1].b(this, i, j, k, l);
         }
     }
 
     public boolean isBlockIndirectlyPowered(int i, int j, int k) {
-        return this.isBlockFaceIndirectlyPowered(i, j - 1, k, 0) ? true : (this.isBlockFaceIndirectlyPowered(i, j + 1, k, 1) ? true : (this.isBlockFaceIndirectlyPowered(i, j, k - 1, 2) ? true : (this.isBlockFaceIndirectlyPowered(i, j, k + 1, 3) ? true : (this.isBlockFaceIndirectlyPowered(i - 1, j, k, 4) ? true : this.isBlockFaceIndirectlyPowered(i + 1, j, k, 5)))));
+        return this.getBlockFacePower(i, j - 1, k, 0) > 0 ? true : (this.getBlockFacePower(i, j + 1, k, 1) > 0 ? true : (this.getBlockFacePower(i, j, k - 1, 2) > 0 ? true : (this.getBlockFacePower(i, j, k + 1, 3) > 0 ? true : (this.getBlockFacePower(i - 1, j, k, 4) > 0 ? true : this.getBlockFacePower(i + 1, j, k, 5) > 0))));
+    }
+
+    public int getHighestNeighborSignal(int i, int j, int k) {
+        int l = 0;
+
+        for (int i1 = 0; i1 < 6; ++i1) {
+            int j1 = this.getBlockFacePower(i + Facing.b[i1], j + Facing.c[i1], k + Facing.d[i1], i1);
+
+            if (j1 >= 15) {
+                return 15;
+            }
+
+            if (j1 > l) {
+                l = j1;
+            }
+        }
+
+        return l;
     }
 
     public EntityHuman findNearbyPlayer(Entity entity, double d0) {
@@ -2359,7 +2360,7 @@ public abstract class World implements IBlockAccess {
                 }
 
                 if (entityhuman1.isInvisible()) {
-                    float f = entityhuman1.bR();
+                    float f = entityhuman1.ca();
 
                     if (f < 0.1F) {
                         f = 0.1F;
@@ -2388,7 +2389,7 @@ public abstract class World implements IBlockAccess {
         return null;
     }
 
-    public void D() {
+    public void E() {
         this.dataManager.checkSession();
     }
 
@@ -2418,7 +2419,7 @@ public abstract class World implements IBlockAccess {
 
     public void broadcastEntityEffect(Entity entity, byte b0) {}
 
-    public IChunkProvider I() {
+    public IChunkProvider J() {
         return this.chunkProvider;
     }
 
@@ -2442,26 +2443,26 @@ public abstract class World implements IBlockAccess {
 
     public void everyoneSleeping() {}
 
-    public float i(float f) {
-        return (this.o + (this.p - this.o) * f) * this.j(f);
+    public float h(float f) {
+        return (this.o + (this.p - this.o) * f) * this.i(f);
     }
 
-    public float j(float f) {
+    public float i(float f) {
         return this.m + (this.n - this.m) * f;
     }
 
-    public boolean M() {
-        return (double) this.i(1.0F) > 0.9D;
+    public boolean N() {
+        return (double) this.h(1.0F) > 0.9D;
     }
 
-    public boolean N() {
-        return (double) this.j(1.0F) > 0.2D;
+    public boolean O() {
+        return (double) this.i(1.0F) > 0.2D;
     }
 
-    public boolean D(int i, int j, int k) {
-        if (!this.N()) {
+    public boolean F(int i, int j, int k) {
+        if (!this.O()) {
             return false;
-        } else if (!this.k(i, j, k)) {
+        } else if (!this.l(i, j, k)) {
             return false;
         } else if (this.h(i, k) > j) {
             return false;
@@ -2472,7 +2473,7 @@ public abstract class World implements IBlockAccess {
         }
     }
 
-    public boolean E(int i, int j, int k) {
+    public boolean G(int i, int j, int k) {
         BiomeBase biomebase = this.getBiome(i, k);
 
         return biomebase.e();
@@ -2490,9 +2491,9 @@ public abstract class World implements IBlockAccess {
         return this.worldMaps.a(s);
     }
 
-    public void e(int i, int j, int k, int l, int i1) {
-        for (int j1 = 0; j1 < this.v.size(); ++j1) {
-            ((IWorldAccess) this.v.get(j1)).a(i, j, k, l, i1);
+    public void d(int i, int j, int k, int l, int i1) {
+        for (int j1 = 0; j1 < this.u.size(); ++j1) {
+            ((IWorldAccess) this.u.get(j1)).a(i, j, k, l, i1);
         }
     }
 
@@ -2502,8 +2503,8 @@ public abstract class World implements IBlockAccess {
 
     public void a(EntityHuman entityhuman, int i, int j, int k, int l, int i1) {
         try {
-            for (int j1 = 0; j1 < this.v.size(); ++j1) {
-                ((IWorldAccess) this.v.get(j1)).a(entityhuman, i, j, k, l, i1);
+            for (int j1 = 0; j1 < this.u.size(); ++j1) {
+                ((IWorldAccess) this.u.get(j1)).a(entityhuman, i, j, k, l, i1);
             }
         } catch (Throwable throwable) {
             CrashReport crashreport = CrashReport.a(throwable, "Playing level event");
@@ -2521,15 +2522,15 @@ public abstract class World implements IBlockAccess {
         return 256;
     }
 
-    public int P() {
+    public int Q() {
         return this.worldProvider.f ? 128 : 256;
     }
 
-    public IUpdatePlayerListBox a(EntityMinecart entityminecart) {
+    public IUpdatePlayerListBox a(EntityMinecartAbstract entityminecartabstract) {
         return null;
     }
 
-    public Random F(int i, int j, int k) {
+    public Random H(int i, int j, int k) {
         long l = (long) i * 341873128712L + (long) j * 132897987541L + this.getWorldData().getSeed() + (long) k;
 
         this.random.setSeed(l);
@@ -2537,7 +2538,7 @@ public abstract class World implements IBlockAccess {
     }
 
     public ChunkPosition b(String s, int i, int j, int k) {
-        return this.I().findNearestMapFeature(this, s, i, j, k);
+        return this.J().findNearestMapFeature(this, s, i, j, k);
     }
 
     public CrashReportSystemDetails a(CrashReport crashreport) {
@@ -2556,9 +2557,9 @@ public abstract class World implements IBlockAccess {
         return crashreportsystemdetails;
     }
 
-    public void g(int i, int j, int k, int l, int i1) {
-        for (int j1 = 0; j1 < this.v.size(); ++j1) {
-            IWorldAccess iworldaccess = (IWorldAccess) this.v.get(j1);
+    public void f(int i, int j, int k, int l, int i1) {
+        for (int j1 = 0; j1 < this.u.size(); ++j1) {
+            IWorldAccess iworldaccess = (IWorldAccess) this.u.get(j1);
 
             iworldaccess.b(i, j, k, l, i1);
         }
@@ -2568,11 +2569,43 @@ public abstract class World implements IBlockAccess {
         return this.J;
     }
 
-    public Calendar T() {
+    public Calendar U() {
         if (this.getTime() % 600L == 0L) {
             this.K.setTimeInMillis(System.currentTimeMillis());
         }
 
         return this.K;
     }
+
+    public Scoreboard getScoreboard() {
+        return this.scoreboard;
+    }
+
+    public void m(int i, int j, int k, int l) {
+        for (int i1 = 0; i1 < 4; ++i1) {
+            int j1 = i + Direction.a[i1];
+            int k1 = k + Direction.b[i1];
+            int l1 = this.getTypeId(j1, j, k1);
+
+            if (l1 != 0) {
+                Block block = Block.byId[l1];
+
+                if (Block.REDSTONE_COMPARATOR_OFF.g(l1)) {
+                    block.doPhysics(this, j1, j, k1, l);
+                } else if (Block.l(l1)) {
+                    j1 += Direction.a[i1];
+                    k1 += Direction.b[i1];
+                    l1 = this.getTypeId(j1, j, k1);
+                    block = Block.byId[l1];
+                    if (Block.REDSTONE_COMPARATOR_OFF.g(l1)) {
+                        block.doPhysics(this, j1, j, k1, l);
+                    }
+                }
+            }
+        }
+    }
+
+    public IConsoleLogManager getLogger() {
+        return this.logAgent;
+    }
 }
