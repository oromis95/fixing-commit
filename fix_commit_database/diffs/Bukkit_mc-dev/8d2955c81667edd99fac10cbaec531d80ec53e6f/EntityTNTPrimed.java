@@ -3,6 +3,7 @@ package net.minecraft.server;
 public class EntityTNTPrimed extends Entity {
 
     public int fuseTicks;
+    private EntityLiving source;
 
     public EntityTNTPrimed(World world) {
         super(world);
@@ -12,7 +13,7 @@ public class EntityTNTPrimed extends Entity {
         this.height = this.length / 2.0F;
     }
 
-    public EntityTNTPrimed(World world, double d0, double d1, double d2) {
+    public EntityTNTPrimed(World world, double d0, double d1, double d2, EntityLiving entityliving) {
         this(world);
         this.setPosition(d0, d1, d2);
         float f = (float) (Math.random() * 3.1415927410125732D * 2.0D);
@@ -24,6 +25,7 @@ public class EntityTNTPrimed extends Entity {
         this.lastX = d0;
         this.lastY = d1;
         this.lastZ = d2;
+        this.source = entityliving;
     }
 
     protected void a() {}
@@ -32,11 +34,11 @@ public class EntityTNTPrimed extends Entity {
         return false;
     }
 
-    public boolean L() {
+    public boolean K() {
         return !this.dead;
     }
 
-    public void j_() {
+    public void l_() {
         this.lastX = this.locX;
         this.lastY = this.locY;
         this.lastZ = this.locZ;
@@ -64,7 +66,7 @@ public class EntityTNTPrimed extends Entity {
     private void explode() {
         float f = 4.0F;
 
-        this.world.explode((Entity) null, this.locX, this.locY, this.locZ, f, true);
+        this.world.explode(this, this.locX, this.locY, this.locZ, f, true);
     }
 
     protected void b(NBTTagCompound nbttagcompound) {
@@ -74,4 +76,8 @@ public class EntityTNTPrimed extends Entity {
     protected void a(NBTTagCompound nbttagcompound) {
         this.fuseTicks = nbttagcompound.getByte("Fuse");
     }
+
+    public EntityLiving getSource() {
+        return this.source;
+    }
 }
