@@ -15,27 +15,27 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
     public EntityComplexPart h;
     public EntityComplexPart i;
     public EntityComplexPart j;
-    public EntityComplexPart bJ;
     public EntityComplexPart bK;
     public EntityComplexPart bL;
-    public float bM = 0.0F;
+    public EntityComplexPart bM;
     public float bN = 0.0F;
-    public boolean bO = false;
+    public float bO = 0.0F;
     public boolean bP = false;
-    private Entity bS;
-    public int bQ = 0;
-    public EntityEnderCrystal bR = null;
+    public boolean bQ = false;
+    private Entity bT;
+    public int bR = 0;
+    public EntityEnderCrystal bS = null;
 
     public EntityEnderDragon(World world) {
         super(world);
-        this.children = new EntityComplexPart[] { this.g = new EntityComplexPart(this, "head", 6.0F, 6.0F), this.h = new EntityComplexPart(this, "body", 8.0F, 8.0F), this.i = new EntityComplexPart(this, "tail", 4.0F, 4.0F), this.j = new EntityComplexPart(this, "tail", 4.0F, 4.0F), this.bJ = new EntityComplexPart(this, "tail", 4.0F, 4.0F), this.bK = new EntityComplexPart(this, "wing", 4.0F, 4.0F), this.bL = new EntityComplexPart(this, "wing", 4.0F, 4.0F)};
+        this.children = new EntityComplexPart[] { this.g = new EntityComplexPart(this, "head", 6.0F, 6.0F), this.h = new EntityComplexPart(this, "body", 8.0F, 8.0F), this.i = new EntityComplexPart(this, "tail", 4.0F, 4.0F), this.j = new EntityComplexPart(this, "tail", 4.0F, 4.0F), this.bK = new EntityComplexPart(this, "tail", 4.0F, 4.0F), this.bL = new EntityComplexPart(this, "wing", 4.0F, 4.0F), this.bM = new EntityComplexPart(this, "wing", 4.0F, 4.0F)};
         this.setHealth(this.getMaxHealth());
         this.texture = "/mob/enderdragon/ender.png";
         this.a(16.0F, 8.0F);
-        this.Y = true;
+        this.Z = true;
         this.fireProof = true;
         this.b = 100.0D;
-        this.al = true;
+        this.am = true;
     }
 
     public int getMaxHealth() {
@@ -47,7 +47,7 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
         this.datawatcher.a(16, new Integer(this.getMaxHealth()));
     }
 
-    public double[] a(int i, float f) {
+    public double[] b(int i, float f) {
         if (this.health <= 0) {
             f = 0.0F;
         }
@@ -74,14 +74,14 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
         if (!this.world.isStatic) {
             this.datawatcher.watch(16, Integer.valueOf(this.health));
         } else {
-            f = MathHelper.cos(this.bN * 3.1415927F * 2.0F);
-            f1 = MathHelper.cos(this.bM * 3.1415927F * 2.0F);
+            f = MathHelper.cos(this.bO * 3.1415927F * 2.0F);
+            f1 = MathHelper.cos(this.bN * 3.1415927F * 2.0F);
             if (f1 <= -0.3F && f >= -0.3F) {
                 this.world.a(this.locX, this.locY, this.locZ, "mob.enderdragon.wings", 5.0F, 0.8F + this.random.nextFloat() * 0.3F, false);
             }
         }
 
-        this.bM = this.bN;
+        this.bN = this.bO;
         float f2;
 
         if (this.health <= 0) {
@@ -93,10 +93,10 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
             this.h();
             f = 0.2F / (MathHelper.sqrt(this.motX * this.motX + this.motZ * this.motZ) * 10.0F + 1.0F);
             f *= (float) Math.pow(2.0D, this.motY);
-            if (this.bP) {
-                this.bN += f * 0.5F;
+            if (this.bQ) {
+                this.bO += f * 0.5F;
             } else {
-                this.bN += f;
+                this.bO += f;
             }
 
             this.yaw = MathHelper.g(this.yaw);
@@ -120,14 +120,14 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
             float f3;
 
             if (this.world.isStatic) {
-                if (this.bt > 0) {
-                    d0 = this.locX + (this.bu - this.locX) / (double) this.bt;
-                    d1 = this.locY + (this.bv - this.locY) / (double) this.bt;
-                    d2 = this.locZ + (this.bw - this.locZ) / (double) this.bt;
-                    d3 = MathHelper.g(this.bx - (double) this.yaw);
-                    this.yaw = (float) ((double) this.yaw + d3 / (double) this.bt);
-                    this.pitch = (float) ((double) this.pitch + (this.by - (double) this.pitch) / (double) this.bt);
-                    --this.bt;
+                if (this.bu > 0) {
+                    d0 = this.locX + (this.bv - this.locX) / (double) this.bu;
+                    d1 = this.locY + (this.bw - this.locY) / (double) this.bu;
+                    d2 = this.locZ + (this.bx - this.locZ) / (double) this.bu;
+                    d3 = MathHelper.g(this.by - (double) this.yaw);
+                    this.yaw = (float) ((double) this.yaw + d3 / (double) this.bu);
+                    this.pitch = (float) ((double) this.pitch + (this.bz - (double) this.pitch) / (double) this.bu);
+                    --this.bu;
                     this.setPosition(d0, d1, d2);
                     this.b(this.yaw, this.pitch);
                 }
@@ -136,9 +136,9 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
                 d1 = this.b - this.locY;
                 d2 = this.c - this.locZ;
                 d3 = d0 * d0 + d1 * d1 + d2 * d2;
-                if (this.bS != null) {
-                    this.a = this.bS.locX;
-                    this.c = this.bS.locZ;
+                if (this.bT != null) {
+                    this.a = this.bT.locX;
+                    this.c = this.bT.locZ;
                     double d4 = this.a - this.locX;
                     double d5 = this.c - this.locZ;
                     double d6 = Math.sqrt(d4 * d4 + d5 * d5);
@@ -148,13 +148,13 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
                         d7 = 10.0D;
                     }
 
-                    this.b = this.bS.boundingBox.b + d7;
+                    this.b = this.bT.boundingBox.b + d7;
                 } else {
                     this.a += this.random.nextGaussian() * 2.0D;
                     this.c += this.random.nextGaussian() * 2.0D;
                 }
 
-                if (this.bO || d3 < 100.0D || d3 > 22500.0D || this.positionChanged || this.G) {
+                if (this.bP || d3 < 100.0D || d3 > 22500.0D || this.positionChanged || this.H) {
                     this.i();
                 }
 
@@ -189,7 +189,7 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
                     f4 = 0.0F;
                 }
 
-                this.bE *= 0.8F;
+                this.bF *= 0.8F;
                 float f5 = MathHelper.sqrt(this.motX * this.motX + this.motZ * this.motZ) * 1.0F + 1.0F;
                 double d10 = Math.sqrt(this.motX * this.motX + this.motZ * this.motZ) * 1.0D + 1.0D;
 
@@ -197,13 +197,13 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
                     d10 = 40.0D;
                 }
 
-                this.bE = (float) ((double) this.bE + d9 * (0.699999988079071D / d10 / (double) f5));
-                this.yaw += this.bE * 0.1F;
+                this.bF = (float) ((double) this.bF + d9 * (0.699999988079071D / d10 / (double) f5));
+                this.yaw += this.bF * 0.1F;
                 float f6 = (float) (2.0D / (d10 + 1.0D));
                 float f7 = 0.06F;
 
                 this.a(0.0F, -1.0F, f7 * (f4 * f6 + (1.0F - f6)));
-                if (this.bP) {
+                if (this.bQ) {
                     this.move(this.motX * 0.800000011920929D, this.motY * 0.800000011920929D, this.motZ * 0.800000011920929D);
                 } else {
                     this.move(this.motX, this.motY, this.motZ);
@@ -218,43 +218,43 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
                 this.motY *= 0.9100000262260437D;
             }
 
-            this.ax = this.yaw;
+            this.ay = this.yaw;
             this.g.width = this.g.length = 3.0F;
             this.i.width = this.i.length = 2.0F;
             this.j.width = this.j.length = 2.0F;
-            this.bJ.width = this.bJ.length = 2.0F;
+            this.bK.width = this.bK.length = 2.0F;
             this.h.length = 3.0F;
             this.h.width = 5.0F;
-            this.bK.length = 2.0F;
-            this.bK.width = 4.0F;
-            this.bL.length = 3.0F;
+            this.bL.length = 2.0F;
             this.bL.width = 4.0F;
-            f1 = (float) (this.a(5, 1.0F)[1] - this.a(10, 1.0F)[1]) * 10.0F / 180.0F * 3.1415927F;
+            this.bM.length = 3.0F;
+            this.bM.width = 4.0F;
+            f1 = (float) (this.b(5, 1.0F)[1] - this.b(10, 1.0F)[1]) * 10.0F / 180.0F * 3.1415927F;
             f2 = MathHelper.cos(f1);
             float f9 = -MathHelper.sin(f1);
             float f10 = this.yaw * 3.1415927F / 180.0F;
             float f11 = MathHelper.sin(f10);
             float f12 = MathHelper.cos(f10);
 
-            this.h.j_();
+            this.h.l_();
             this.h.setPositionRotation(this.locX + (double) (f11 * 0.5F), this.locY, this.locZ - (double) (f12 * 0.5F), 0.0F, 0.0F);
-            this.bK.j_();
-            this.bK.setPositionRotation(this.locX + (double) (f12 * 4.5F), this.locY + 2.0D, this.locZ + (double) (f11 * 4.5F), 0.0F, 0.0F);
-            this.bL.j_();
-            this.bL.setPositionRotation(this.locX - (double) (f12 * 4.5F), this.locY + 2.0D, this.locZ - (double) (f11 * 4.5F), 0.0F, 0.0F);
+            this.bL.l_();
+            this.bL.setPositionRotation(this.locX + (double) (f12 * 4.5F), this.locY + 2.0D, this.locZ + (double) (f11 * 4.5F), 0.0F, 0.0F);
+            this.bM.l_();
+            this.bM.setPositionRotation(this.locX - (double) (f12 * 4.5F), this.locY + 2.0D, this.locZ - (double) (f11 * 4.5F), 0.0F, 0.0F);
             if (!this.world.isStatic && this.hurtTicks == 0) {
-                this.a(this.world.getEntities(this, this.bK.boundingBox.grow(4.0D, 2.0D, 4.0D).d(0.0D, -2.0D, 0.0D)));
                 this.a(this.world.getEntities(this, this.bL.boundingBox.grow(4.0D, 2.0D, 4.0D).d(0.0D, -2.0D, 0.0D)));
+                this.a(this.world.getEntities(this, this.bM.boundingBox.grow(4.0D, 2.0D, 4.0D).d(0.0D, -2.0D, 0.0D)));
                 this.b(this.world.getEntities(this, this.g.boundingBox.grow(1.0D, 1.0D, 1.0D)));
             }
 
-            double[] adouble = this.a(5, 1.0F);
-            double[] adouble1 = this.a(0, 1.0F);
+            double[] adouble = this.b(5, 1.0F);
+            double[] adouble1 = this.b(0, 1.0F);
 
-            f3 = MathHelper.sin(this.yaw * 3.1415927F / 180.0F - this.bE * 0.01F);
-            float f13 = MathHelper.cos(this.yaw * 3.1415927F / 180.0F - this.bE * 0.01F);
+            f3 = MathHelper.sin(this.yaw * 3.1415927F / 180.0F - this.bF * 0.01F);
+            float f13 = MathHelper.cos(this.yaw * 3.1415927F / 180.0F - this.bF * 0.01F);
 
-            this.g.j_();
+            this.g.l_();
             this.g.setPositionRotation(this.locX + (double) (f3 * 5.5F * f2), this.locY + (adouble1[1] - adouble[1]) * 1.0D + (double) (f9 * 5.5F), this.locZ - (double) (f13 * 5.5F * f2), 0.0F, 0.0F);
 
             for (int j = 0; j < 3; ++j) {
@@ -269,36 +269,36 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
                 }
 
                 if (j == 2) {
-                    entitycomplexpart = this.bJ;
+                    entitycomplexpart = this.bK;
                 }
 
-                double[] adouble2 = this.a(12 + j * 2, 1.0F);
+                double[] adouble2 = this.b(12 + j * 2, 1.0F);
                 float f14 = this.yaw * 3.1415927F / 180.0F + this.b(adouble2[0] - adouble[0]) * 3.1415927F / 180.0F * 1.0F;
                 float f15 = MathHelper.sin(f14);
                 float f16 = MathHelper.cos(f14);
                 float f17 = 1.5F;
                 float f18 = (float) (j + 1) * 2.0F;
 
-                entitycomplexpart.j_();
+                entitycomplexpart.l_();
                 entitycomplexpart.setPositionRotation(this.locX - (double) ((f11 * f17 + f15 * f18) * f2), this.locY + (adouble2[1] - adouble[1]) * 1.0D - (double) ((f18 + f17) * f9) + 1.5D, this.locZ + (double) ((f12 * f17 + f16 * f18) * f2), 0.0F, 0.0F);
             }
 
             if (!this.world.isStatic) {
-                this.bP = this.a(this.g.boundingBox) | this.a(this.h.boundingBox);
+                this.bQ = this.a(this.g.boundingBox) | this.a(this.h.boundingBox);
             }
         }
     }
 
     private void h() {
-        if (this.bR != null) {
-            if (this.bR.dead) {
+        if (this.bS != null) {
+            if (this.bS.dead) {
                 if (!this.world.isStatic) {
-                    this.a(this.g, DamageSource.EXPLOSION, 10);
+                    this.a(this.g, DamageSource.explosion((Explosion) null), 10);
                 }
 
-                this.bR = null;
-            } else if (this.ticksLived % 10 == 0 && this.health < this.getMaxHealth()) {
-                ++this.health;
+                this.bS = null;
+            } else if (this.ticksLived % 10 == 0 && this.getHealth() < this.getMaxHealth()) {
+                this.setHealth(this.getHealth() + 1);
             }
         }
 
@@ -319,7 +319,7 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
                 }
             }
 
-            this.bR = entityendercrystal;
+            this.bS = entityendercrystal;
         }
     }
 
@@ -352,9 +352,9 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
     }
 
     private void i() {
-        this.bO = false;
+        this.bP = false;
         if (this.random.nextInt(2) == 0 && !this.world.players.isEmpty()) {
-            this.bS = (Entity) this.world.players.get(this.random.nextInt(this.world.players.size()));
+            this.bT = (Entity) this.world.players.get(this.random.nextInt(this.world.players.size()));
         } else {
             boolean flag = false;
 
@@ -371,7 +371,7 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
                 flag = d0 * d0 + d1 * d1 + d2 * d2 > 100.0D;
             } while (!flag);
 
-            this.bS = null;
+            this.bT = null;
         }
     }
 
@@ -396,8 +396,7 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
 
                     if (j2 != 0) {
                         if (j2 != Block.OBSIDIAN.id && j2 != Block.WHITESTONE.id && j2 != Block.BEDROCK.id) {
-                            flag1 = true;
-                            this.world.setTypeId(k1, l1, i2, 0);
+                            flag1 = this.world.setAir(k1, l1, i2) || flag1;
                         } else {
                             flag = true;
                         }
@@ -429,8 +428,8 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
         this.a = this.locX + (double) (f1 * 5.0F) + (double) ((this.random.nextFloat() - 0.5F) * 2.0F);
         this.b = this.locY + (double) (this.random.nextFloat() * 3.0F) + 1.0D;
         this.c = this.locZ - (double) (f2 * 5.0F) + (double) ((this.random.nextFloat() - 0.5F) * 2.0F);
-        this.bS = null;
-        if (damagesource.getEntity() instanceof EntityHuman || damagesource == DamageSource.EXPLOSION) {
+        this.bT = null;
+        if (damagesource.getEntity() instanceof EntityHuman || damagesource.c()) {
             this.dealDamage(damagesource, i);
         }
 
@@ -445,9 +444,9 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
         return super.damageEntity(damagesource, i);
     }
 
-    protected void aP() {
-        ++this.bQ;
-        if (this.bQ >= 180 && this.bQ <= 200) {
+    protected void aS() {
+        ++this.bR;
+        if (this.bR >= 180 && this.bR <= 200) {
             float f = (this.random.nextFloat() - 0.5F) * 8.0F;
             float f1 = (this.random.nextFloat() - 0.5F) * 4.0F;
             float f2 = (this.random.nextFloat() - 0.5F) * 8.0F;
@@ -459,7 +458,7 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
         int j;
 
         if (!this.world.isStatic) {
-            if (this.bQ > 150 && this.bQ % 5 == 0) {
+            if (this.bR > 150 && this.bR % 5 == 0) {
                 i = 1000;
 
                 while (i > 0) {
@@ -469,14 +468,14 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
                 }
             }
 
-            if (this.bQ == 1) {
-                this.world.e(1018, (int) this.locX, (int) this.locY, (int) this.locZ, 0);
+            if (this.bR == 1) {
+                this.world.d(1018, (int) this.locX, (int) this.locY, (int) this.locZ, 0);
             }
         }
 
         this.move(0.0D, 0.10000000149011612D, 0.0D);
-        this.ax = this.yaw += 20.0F;
-        if (this.bQ == 200 && !this.world.isStatic) {
+        this.ay = this.yaw += 20.0F;
+        if (this.bR == 200 && !this.world.isStatic) {
             i = 2000;
 
             while (i > 0) {
@@ -506,39 +505,39 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
                     if (d2 <= ((double) b1 - 0.5D) * ((double) b1 - 0.5D)) {
                         if (k < b0) {
                             if (d2 <= ((double) (b1 - 1) - 0.5D) * ((double) (b1 - 1) - 0.5D)) {
-                                this.world.setTypeId(l, k, i1, Block.BEDROCK.id);
+                                this.world.setTypeIdUpdate(l, k, i1, Block.BEDROCK.id);
                             }
                         } else if (k > b0) {
-                            this.world.setTypeId(l, k, i1, 0);
+                            this.world.setTypeIdUpdate(l, k, i1, 0);
                         } else if (d2 > ((double) (b1 - 1) - 0.5D) * ((double) (b1 - 1) - 0.5D)) {
-                            this.world.setTypeId(l, k, i1, Block.BEDROCK.id);
+                            this.world.setTypeIdUpdate(l, k, i1, Block.BEDROCK.id);
                         } else {
-                            this.world.setTypeId(l, k, i1, Block.ENDER_PORTAL.id);
+                            this.world.setTypeIdUpdate(l, k, i1, Block.ENDER_PORTAL.id);
                         }
                     }
                 }
             }
         }
 
-        this.world.setTypeId(i, b0 + 0, j, Block.BEDROCK.id);
-        this.world.setTypeId(i, b0 + 1, j, Block.BEDROCK.id);
-        this.world.setTypeId(i, b0 + 2, j, Block.BEDROCK.id);
-        this.world.setTypeId(i - 1, b0 + 2, j, Block.TORCH.id);
-        this.world.setTypeId(i + 1, b0 + 2, j, Block.TORCH.id);
-        this.world.setTypeId(i, b0 + 2, j - 1, Block.TORCH.id);
-        this.world.setTypeId(i, b0 + 2, j + 1, Block.TORCH.id);
-        this.world.setTypeId(i, b0 + 3, j, Block.BEDROCK.id);
-        this.world.setTypeId(i, b0 + 4, j, Block.DRAGON_EGG.id);
+        this.world.setTypeIdUpdate(i, b0 + 0, j, Block.BEDROCK.id);
+        this.world.setTypeIdUpdate(i, b0 + 1, j, Block.BEDROCK.id);
+        this.world.setTypeIdUpdate(i, b0 + 2, j, Block.BEDROCK.id);
+        this.world.setTypeIdUpdate(i - 1, b0 + 2, j, Block.TORCH.id);
+        this.world.setTypeIdUpdate(i + 1, b0 + 2, j, Block.TORCH.id);
+        this.world.setTypeIdUpdate(i, b0 + 2, j - 1, Block.TORCH.id);
+        this.world.setTypeIdUpdate(i, b0 + 2, j + 1, Block.TORCH.id);
+        this.world.setTypeIdUpdate(i, b0 + 3, j, Block.BEDROCK.id);
+        this.world.setTypeIdUpdate(i, b0 + 4, j, Block.DRAGON_EGG.id);
         BlockEnderPortal.a = false;
     }
 
-    protected void bk() {}
+    protected void bn() {}
 
-    public Entity[] ao() {
+    public Entity[] an() {
         return this.children;
     }
 
-    public boolean L() {
+    public boolean K() {
         return false;
     }
 
@@ -546,15 +545,15 @@ public class EntityEnderDragon extends EntityLiving implements IComplex {
         return this.world;
     }
 
-    protected String aY() {
+    protected String bb() {
         return "mob.enderdragon.growl";
     }
 
-    protected String aZ() {
+    protected String bc() {
         return "mob.enderdragon.hit";
     }
 
-    protected float aX() {
+    protected float ba() {
         return 5.0F;
     }
 }
