@@ -9,14 +9,16 @@ public class Packet100OpenWindow extends Packet {
     public int b;
     public String c;
     public int d;
+    public boolean e;
 
     public Packet100OpenWindow() {}
 
-    public Packet100OpenWindow(int i, int j, String s, int k) {
+    public Packet100OpenWindow(int i, int j, String s, int k, boolean flag) {
         this.a = i;
         this.b = j;
         this.c = s;
         this.d = k;
+        this.e = flag;
     }
 
     public void handle(Connection connection) {
@@ -28,6 +30,7 @@ public class Packet100OpenWindow extends Packet {
         this.b = datainputstream.readByte() & 255;
         this.c = a(datainputstream, 32);
         this.d = datainputstream.readByte() & 255;
+        this.e = datainputstream.readBoolean();
     }
 
     public void a(DataOutputStream dataoutputstream) {
@@ -35,9 +38,10 @@ public class Packet100OpenWindow extends Packet {
         dataoutputstream.writeByte(this.b & 255);
         a(this.c, dataoutputstream);
         dataoutputstream.writeByte(this.d & 255);
+        dataoutputstream.writeBoolean(this.e);
     }
 
     public int a() {
-        return 3 + this.c.length();
+        return 4 + this.c.length();
     }
 }
