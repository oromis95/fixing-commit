@@ -29,13 +29,13 @@ public class EntitySheep extends EntityAnimal {
         this.e.setItem(1, new ItemStack(Item.INK_SACK, 1, 0));
     }
 
-    protected boolean be() {
+    protected boolean bh() {
         return true;
     }
 
-    protected void bl() {
+    protected void bo() {
         this.f = this.g.f();
-        super.bl();
+        super.bo();
     }
 
     public void c() {
@@ -65,7 +65,7 @@ public class EntitySheep extends EntityAnimal {
         return Block.WOOL.id;
     }
 
-    public boolean a(EntityHuman entityhuman) {
+    public boolean a_(EntityHuman entityhuman) {
         ItemStack itemstack = entityhuman.inventory.getItemInHand();
 
         if (itemstack != null && itemstack.id == Item.SHEARS.id && !this.isSheared() && !this.isBaby()) {
@@ -86,7 +86,7 @@ public class EntitySheep extends EntityAnimal {
             this.makeSound("mob.sheep.shear", 1.0F, 1.0F);
         }
 
-        return super.a(entityhuman);
+        return super.a_(entityhuman);
     }
 
     public void b(NBTTagCompound nbttagcompound) {
@@ -101,15 +101,15 @@ public class EntitySheep extends EntityAnimal {
         this.setColor(nbttagcompound.getByte("Color"));
     }
 
-    protected String aY() {
+    protected String bb() {
         return "mob.sheep.say";
     }
 
-    protected String aZ() {
+    protected String bc() {
         return "mob.sheep.say";
     }
 
-    protected String ba() {
+    protected String bd() {
         return "mob.sheep.say";
     }
 
@@ -156,7 +156,7 @@ public class EntitySheep extends EntityAnimal {
         return entitysheep1;
     }
 
-    public void aH() {
+    public void aK() {
         this.setSheared(false);
         if (this.isBaby()) {
             int i = this.getAge() + 1200;
@@ -169,7 +169,7 @@ public class EntitySheep extends EntityAnimal {
         }
     }
 
-    public void bG() {
+    public void bJ() {
         this.setColor(a(this.world.random));
     }
 
