@@ -3,26 +3,14 @@ package net.minecraft.server;
 public class BlockCarrots extends BlockCrops {
 
     public BlockCarrots(int i) {
-        super(i, 200);
+        super(i);
     }
 
-    public int a(int i, int j) {
-        if (j < 7) {
-            if (j == 6) {
-                j = 5;
-            }
-
-            return this.textureId + (j >> 1);
-        } else {
-            return this.textureId + 3;
-        }
-    }
-
-    protected int h() {
+    protected int j() {
         return Item.CARROT.id;
     }
 
-    protected int j() {
+    protected int k() {
         return Item.CARROT.id;
     }
 }
