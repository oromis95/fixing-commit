@@ -77,7 +77,7 @@ public class WorldGenLakes extends WorldGenerator {
                 for (i2 = 0; i2 < 16; ++i2) {
                     for (j2 = 0; j2 < 8; ++j2) {
                         if (aboolean[(i1 * 16 + i2) * 8 + j2]) {
-                            world.setRawTypeId(i + i1, j + j2, k + i2, j2 >= 4 ? 0 : this.a);
+                            world.setTypeIdAndData(i + i1, j + j2, k + i2, j2 >= 4 ? 0 : this.a, 0, 2);
                         }
                     }
                 }
@@ -90,9 +90,9 @@ public class WorldGenLakes extends WorldGenerator {
                             BiomeBase biomebase = world.getBiome(i + i1, k + i2);
 
                             if (biomebase.A == Block.MYCEL.id) {
-                                world.setRawTypeId(i + i1, j + j2 - 1, k + i2, Block.MYCEL.id);
+                                world.setTypeIdAndData(i + i1, j + j2 - 1, k + i2, Block.MYCEL.id, 0, 2);
                             } else {
-                                world.setRawTypeId(i + i1, j + j2 - 1, k + i2, Block.GRASS.id);
+                                world.setTypeIdAndData(i + i1, j + j2 - 1, k + i2, Block.GRASS.id, 0, 2);
                             }
                         }
                     }
@@ -105,7 +105,7 @@ public class WorldGenLakes extends WorldGenerator {
                         for (j2 = 0; j2 < 8; ++j2) {
                             flag = !aboolean[(i1 * 16 + i2) * 8 + j2] && (i1 < 15 && aboolean[((i1 + 1) * 16 + i2) * 8 + j2] || i1 > 0 && aboolean[((i1 - 1) * 16 + i2) * 8 + j2] || i2 < 15 && aboolean[(i1 * 16 + i2 + 1) * 8 + j2] || i2 > 0 && aboolean[(i1 * 16 + (i2 - 1)) * 8 + j2] || j2 < 7 && aboolean[(i1 * 16 + i2) * 8 + j2 + 1] || j2 > 0 && aboolean[(i1 * 16 + i2) * 8 + (j2 - 1)]);
                             if (flag && (j2 < 4 || random.nextInt(2) != 0) && world.getMaterial(i + i1, j + j2, k + i2).isBuildable()) {
-                                world.setRawTypeId(i + i1, j + j2, k + i2, Block.STONE.id);
+                                world.setTypeIdAndData(i + i1, j + j2, k + i2, Block.STONE.id, 0, 2);
                             }
                         }
                     }
@@ -117,8 +117,8 @@ public class WorldGenLakes extends WorldGenerator {
                     for (i2 = 0; i2 < 16; ++i2) {
                         byte b0 = 4;
 
-                        if (world.w(i + i1, j + b0, k + i2)) {
-                            world.setRawTypeId(i + i1, j + b0, k + i2, Block.ICE.id);
+                        if (world.x(i + i1, j + b0, k + i2)) {
+                            world.setTypeIdAndData(i + i1, j + b0, k + i2, Block.ICE.id, 0, 2);
                         }
                     }
                 }
