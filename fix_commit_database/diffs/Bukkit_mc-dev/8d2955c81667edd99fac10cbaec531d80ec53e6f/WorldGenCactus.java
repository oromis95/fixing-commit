@@ -16,8 +16,8 @@ public class WorldGenCactus extends WorldGenerator {
                 int l1 = 1 + random.nextInt(random.nextInt(3) + 1);
 
                 for (int i2 = 0; i2 < l1; ++i2) {
-                    if (Block.CACTUS.d(world, i1, j1 + i2, k1)) {
-                        world.setRawTypeId(i1, j1 + i2, k1, Block.CACTUS.id);
+                    if (Block.CACTUS.f(world, i1, j1 + i2, k1)) {
+                        world.setTypeIdAndData(i1, j1 + i2, k1, Block.CACTUS.id, 0, 2);
                     }
                 }
             }
