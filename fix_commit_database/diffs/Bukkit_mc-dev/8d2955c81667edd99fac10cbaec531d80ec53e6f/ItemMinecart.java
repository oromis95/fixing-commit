@@ -2,6 +2,7 @@ package net.minecraft.server;
 
 public class ItemMinecart extends Item {
 
+    private static final IDispenseBehavior b = new DispenseBehaviorMinecart();
     public int a;
 
     public ItemMinecart(int i, int j) {
@@ -9,14 +10,21 @@ public class ItemMinecart extends Item {
         this.maxStackSize = 1;
         this.a = j;
         this.a(CreativeModeTab.e);
+        BlockDispenser.a.a(this, b);
     }
 
     public boolean interactWith(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l, float f, float f1, float f2) {
         int i1 = world.getTypeId(i, j, k);
 
-        if (BlockMinecartTrack.e(i1)) {
+        if (BlockMinecartTrackAbstract.d_(i1)) {
             if (!world.isStatic) {
-                world.addEntity(new EntityMinecart(world, (double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), this.a));
+                EntityMinecartAbstract entityminecartabstract = EntityMinecartAbstract.a(world, (double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), this.a);
+
+                if (itemstack.hasName()) {
+                    entityminecartabstract.a(itemstack.getName());
+                }
+
+                world.addEntity(entityminecartabstract);
             }
 
             --itemstack.count;
