@@ -69,12 +69,12 @@ public class Explosion {
 
                             if (k1 > 0) {
                                 Block block = Block.byId[k1];
-                                float f3 = this.source != null ? this.source.a(this, block, l, i1, j1) : block.a(this.source);
+                                float f3 = this.source != null ? this.source.a(this, this.world, l, i1, j1, block) : block.a(this.source);
 
                                 f1 -= (f3 + 0.3F) * f2;
                             }
 
-                            if (f1 > 0.0F) {
+                            if (f1 > 0.0F && (this.source == null || this.source.a(this, this.world, l, i1, j1, k1, f1))) {
                                 hashset.add(new ChunkPosition(l, i1, j1));
                             }
 
@@ -115,7 +115,7 @@ public class Explosion {
                     double d9 = (double) this.world.a(vec3d, entity.boundingBox);
                     double d10 = (1.0D - d7) * d9;
 
-                    entity.damageEntity(DamageSource.EXPLOSION, (int) ((d10 * d10 + d10) / 2.0D * 8.0D * (double) this.size + 1.0D));
+                    entity.damageEntity(DamageSource.explosion(this), (int) ((d10 * d10 + d10) / 2.0D * 8.0D * (double) this.size + 1.0D));
                     double d11 = EnchantmentProtection.a(entity, d10);
 
                     entity.motX += d0 * d11;
@@ -181,14 +181,11 @@ public class Explosion {
                     Block block = Block.byId[l];
 
                     if (block.a(this)) {
-                        block.dropNaturally(this.world, i, j, k, this.world.getData(i, j, k), 0.3F, 0);
+                        block.dropNaturally(this.world, i, j, k, this.world.getData(i, j, k), 1.0F / this.size, 0);
                     }
 
-                    if (this.world.setRawTypeIdAndData(i, j, k, 0, 0, this.world.isStatic)) {
-                        this.world.applyPhysics(i, j, k, 0);
-                    }
-
-                    block.wasExploded(this.world, i, j, k);
+                    this.world.setTypeIdAndData(i, j, k, 0, 0, 3);
+                    block.wasExploded(this.world, i, j, k, this);
                 }
             }
         }
@@ -204,8 +201,8 @@ public class Explosion {
                 l = this.world.getTypeId(i, j, k);
                 int i1 = this.world.getTypeId(i, j - 1, k);
 
-                if (l == 0 && Block.q[i1] && this.j.nextInt(3) == 0) {
-                    this.world.setTypeId(i, j, k, Block.FIRE.id);
+                if (l == 0 && Block.s[i1] && this.j.nextInt(3) == 0) {
+                    this.world.setTypeIdUpdate(i, j, k, Block.FIRE.id);
                 }
             }
         }
@@ -214,4 +211,8 @@ public class Explosion {
     public Map b() {
         return this.l;
     }
+
+    public EntityLiving c() {
+        return this.source == null ? null : (this.source instanceof EntityTNTPrimed ? ((EntityTNTPrimed) this.source).getSource() : (this.source instanceof EntityLiving ? (EntityLiving) this.source : null));
+    }
 }
