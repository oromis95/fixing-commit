@@ -4,17 +4,17 @@ import java.util.Calendar;
 
 public class EntitySkeleton extends EntityMonster implements IRangedEntity {
 
-    private PathfinderGoalArrowAttack d = new PathfinderGoalArrowAttack(this, 0.25F, 60, 10.0F);
+    private PathfinderGoalArrowAttack d = new PathfinderGoalArrowAttack(this, 0.25F, 20, 60, 15.0F);
     private PathfinderGoalMeleeAttack e = new PathfinderGoalMeleeAttack(this, EntityHuman.class, 0.31F, false);
 
     public EntitySkeleton(World world) {
         super(world);
         this.texture = "/mob/skeleton.png";
-        this.bH = 0.25F;
+        this.bI = 0.25F;
         this.goalSelector.a(1, new PathfinderGoalFloat(this));
         this.goalSelector.a(2, new PathfinderGoalRestrictSun(this));
-        this.goalSelector.a(3, new PathfinderGoalFleeSun(this, this.bH));
-        this.goalSelector.a(5, new PathfinderGoalRandomStroll(this, this.bH));
+        this.goalSelector.a(3, new PathfinderGoalFleeSun(this, this.bI));
+        this.goalSelector.a(5, new PathfinderGoalRandomStroll(this, this.bI));
         this.goalSelector.a(6, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 8.0F));
         this.goalSelector.a(6, new PathfinderGoalRandomLookaround(this));
         this.targetSelector.a(1, new PathfinderGoalHurtByTarget(this, false));
@@ -29,7 +29,7 @@ public class EntitySkeleton extends EntityMonster implements IRangedEntity {
         this.datawatcher.a(13, new Byte((byte) 0));
     }
 
-    public boolean be() {
+    public boolean bh() {
         return true;
     }
 
@@ -37,15 +37,15 @@ public class EntitySkeleton extends EntityMonster implements IRangedEntity {
         return 20;
     }
 
-    protected String aY() {
+    protected String bb() {
         return "mob.skeleton.say";
     }
 
-    protected String aZ() {
+    protected String bc() {
         return "mob.skeleton.hurt";
     }
 
-    protected String ba() {
+    protected String bd() {
         return "mob.skeleton.death";
     }
 
@@ -67,7 +67,7 @@ public class EntitySkeleton extends EntityMonster implements IRangedEntity {
 
     public int c(Entity entity) {
         if (this.getSkeletonType() == 1) {
-            ItemStack itemstack = this.bD();
+            ItemStack itemstack = this.bG();
             int i = 4;
 
             if (itemstack != null) {
@@ -88,14 +88,14 @@ public class EntitySkeleton extends EntityMonster implements IRangedEntity {
         if (this.world.u() && !this.world.isStatic) {
             float f = this.c(1.0F);
 
-            if (f > 0.5F && this.random.nextFloat() * 30.0F < (f - 0.4F) * 2.0F && this.world.k(MathHelper.floor(this.locX), MathHelper.floor(this.locY), MathHelper.floor(this.locZ))) {
+            if (f > 0.5F && this.random.nextFloat() * 30.0F < (f - 0.4F) * 2.0F && this.world.l(MathHelper.floor(this.locX), MathHelper.floor(this.locY), MathHelper.floor(this.locZ))) {
                 boolean flag = true;
                 ItemStack itemstack = this.getEquipment(4);
 
                 if (itemstack != null) {
-                    if (itemstack.f()) {
-                        itemstack.setData(itemstack.i() + this.random.nextInt(2));
-                        if (itemstack.i() >= itemstack.k()) {
+                    if (itemstack.g()) {
+                        itemstack.setData(itemstack.j() + this.random.nextInt(2));
+                        if (itemstack.j() >= itemstack.l()) {
                             this.a(itemstack);
                             this.setEquipment(4, (ItemStack) null);
                         }
@@ -110,12 +110,16 @@ public class EntitySkeleton extends EntityMonster implements IRangedEntity {
             }
         }
 
+        if (this.world.isStatic && this.getSkeletonType() == 1) {
+            this.a(0.72F, 2.34F);
+        }
+
         super.c();
     }
 
     public void die(DamageSource damagesource) {
         super.die(damagesource);
-        if (damagesource.f() instanceof EntityArrow && damagesource.getEntity() instanceof EntityHuman) {
+        if (damagesource.h() instanceof EntityArrow && damagesource.getEntity() instanceof EntityHuman) {
             EntityHuman entityhuman = (EntityHuman) damagesource.getEntity();
             double d0 = entityhuman.locX - this.locX;
             double d1 = entityhuman.locZ - this.locZ;
@@ -161,25 +165,25 @@ public class EntitySkeleton extends EntityMonster implements IRangedEntity {
         }
     }
 
-    protected void bE() {
-        super.bE();
+    protected void bH() {
+        super.bH();
         this.setEquipment(0, new ItemStack(Item.BOW));
     }
 
-    public void bG() {
-        if (this.world.worldProvider instanceof WorldProviderHell && this.aB().nextInt(5) > 0) {
+    public void bJ() {
+        if (this.world.worldProvider instanceof WorldProviderHell && this.aE().nextInt(5) > 0) {
             this.goalSelector.a(4, this.e);
             this.setSkeletonType(1);
             this.setEquipment(0, new ItemStack(Item.STONE_SWORD));
         } else {
             this.goalSelector.a(4, this.d);
-            this.bE();
-            this.bF();
+            this.bH();
+            this.bI();
         }
 
-        this.canPickUpLoot = this.random.nextFloat() < at[this.world.difficulty];
+        this.h(this.random.nextFloat() < au[this.world.difficulty]);
         if (this.getEquipment(4) == null) {
-            Calendar calendar = this.world.T();
+            Calendar calendar = this.world.U();
 
             if (calendar.get(2) + 1 == 10 && calendar.get(5) == 31 && this.random.nextFloat() < 0.25F) {
                 this.setEquipment(4, new ItemStack(this.random.nextFloat() < 0.1F ? Block.JACK_O_LANTERN : Block.PUMPKIN));
@@ -191,7 +195,7 @@ public class EntitySkeleton extends EntityMonster implements IRangedEntity {
     public void m() {
         this.goalSelector.a((PathfinderGoal) this.e);
         this.goalSelector.a((PathfinderGoal) this.d);
-        ItemStack itemstack = this.bD();
+        ItemStack itemstack = this.bG();
 
         if (itemstack != null && itemstack.id == Item.BOW.id) {
             this.goalSelector.a(4, this.d);
@@ -200,11 +204,12 @@ public class EntitySkeleton extends EntityMonster implements IRangedEntity {
         }
     }
 
-    public void d(EntityLiving entityliving) {
-        EntityArrow entityarrow = new EntityArrow(this.world, this, entityliving, 1.6F, 12.0F);
-        int i = EnchantmentManager.getEnchantmentLevel(Enchantment.ARROW_DAMAGE.id, this.bD());
-        int j = EnchantmentManager.getEnchantmentLevel(Enchantment.ARROW_KNOCKBACK.id, this.bD());
+    public void a(EntityLiving entityliving, float f) {
+        EntityArrow entityarrow = new EntityArrow(this.world, this, entityliving, 1.6F, (float) (14 - this.world.difficulty * 4));
+        int i = EnchantmentManager.getEnchantmentLevel(Enchantment.ARROW_DAMAGE.id, this.bG());
+        int j = EnchantmentManager.getEnchantmentLevel(Enchantment.ARROW_KNOCKBACK.id, this.bG());
 
+        entityarrow.b((double) (f * 2.0F) + this.random.nextGaussian() * 0.25D + (double) ((float) this.world.difficulty * 0.11F));
         if (i > 0) {
             entityarrow.b(entityarrow.c() + (double) i * 0.5D + 0.5D);
         }
@@ -213,11 +218,11 @@ public class EntitySkeleton extends EntityMonster implements IRangedEntity {
             entityarrow.a(j);
         }
 
-        if (EnchantmentManager.getEnchantmentLevel(Enchantment.ARROW_FIRE.id, this.bD()) > 0 || this.getSkeletonType() == 1) {
+        if (EnchantmentManager.getEnchantmentLevel(Enchantment.ARROW_FIRE.id, this.bG()) > 0 || this.getSkeletonType() == 1) {
             entityarrow.setOnFire(100);
         }
 
-        this.makeSound("random.bow", 1.0F, 1.0F / (this.aB().nextFloat() * 0.4F + 0.8F));
+        this.makeSound("random.bow", 1.0F, 1.0F / (this.aE().nextFloat() * 0.4F + 0.8F));
         this.world.addEntity(entityarrow);
     }
 
@@ -229,7 +234,7 @@ public class EntitySkeleton extends EntityMonster implements IRangedEntity {
         this.datawatcher.watch(13, Byte.valueOf((byte) i));
         this.fireProof = i == 1;
         if (i == 1) {
-            this.a(0.72F, 2.16F);
+            this.a(0.72F, 2.34F);
         } else {
             this.a(0.6F, 1.8F);
         }
