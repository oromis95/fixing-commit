@@ -8,11 +8,11 @@ public class BlockEnderPortal extends BlockContainer {
     public static boolean a = false;
 
     protected BlockEnderPortal(int i, Material material) {
-        super(i, 0, material);
+        super(i, material);
         this.a(1.0F);
     }
 
-    public TileEntity a(World world) {
+    public TileEntity b(World world) {
         return new TileEntityEnderPortal();
     }
 
@@ -38,7 +38,7 @@ public class BlockEnderPortal extends BlockContainer {
 
     public void a(World world, int i, int j, int k, Entity entity) {
         if (entity.vehicle == null && entity.passenger == null && !world.isStatic) {
-            entity.b(1);
+            entity.c(1);
         }
     }
 
@@ -49,7 +49,7 @@ public class BlockEnderPortal extends BlockContainer {
     public void onPlace(World world, int i, int j, int k) {
         if (!a) {
             if (world.worldProvider.dimension != 0) {
-                world.setTypeId(i, j, k, 0);
+                world.setAir(i, j, k);
             }
         }
     }
