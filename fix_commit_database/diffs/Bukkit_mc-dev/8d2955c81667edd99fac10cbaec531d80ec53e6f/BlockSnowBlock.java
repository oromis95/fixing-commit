@@ -4,8 +4,8 @@ import java.util.Random;
 
 public class BlockSnowBlock extends Block {
 
-    protected BlockSnowBlock(int i, int j) {
-        super(i, j, Material.SNOW_BLOCK);
+    protected BlockSnowBlock(int i) {
+        super(i, Material.SNOW_BLOCK);
         this.b(true);
         this.a(CreativeModeTab.b);
     }
@@ -18,10 +18,10 @@ public class BlockSnowBlock extends Block {
         return 4;
     }
 
-    public void b(World world, int i, int j, int k, Random random) {
+    public void a(World world, int i, int j, int k, Random random) {
         if (world.b(EnumSkyBlock.BLOCK, i, j, k) > 11) {
             this.c(world, i, j, k, world.getData(i, j, k), 0);
-            world.setTypeId(i, j, k, 0);
+            world.setAir(i, j, k);
         }
     }
 }
