@@ -4,17 +4,19 @@ import java.util.Random;
 
 public class BlockSnow extends Block {
 
-    protected BlockSnow(int i, int j) {
-        super(i, j, Material.SNOW_LAYER);
+    protected BlockSnow(int i) {
+        super(i, Material.SNOW_LAYER);
         this.a(0.0F, 0.0F, 0.0F, 1.0F, 0.125F, 1.0F);
         this.b(true);
         this.a(CreativeModeTab.c);
+        this.d(0);
     }
 
-    public AxisAlignedBB e(World world, int i, int j, int k) {
+    public AxisAlignedBB b(World world, int i, int j, int k) {
         int l = world.getData(i, j, k) & 7;
+        float f = 0.125F;
 
-        return l >= 3 ? AxisAlignedBB.a().a((double) i + this.minX, (double) j + this.minY, (double) k + this.minZ, (double) i + this.maxX, (double) ((float) j + 0.5F), (double) k + this.maxZ) : null;
+        return AxisAlignedBB.a().a((double) i + this.minX, (double) j + this.minY, (double) k + this.minZ, (double) i + this.maxX, (double) ((float) j + (float) l * f), (double) k + this.maxZ);
     }
 
     public boolean c() {
@@ -25,9 +27,17 @@ public class BlockSnow extends Block {
         return false;
     }
 
+    public void g() {
+        this.d(0);
+    }
+
     public void updateShape(IBlockAccess iblockaccess, int i, int j, int k) {
-        int l = iblockaccess.getData(i, j, k) & 7;
-        float f = (float) (2 * (1 + l)) / 16.0F;
+        this.d(iblockaccess.getData(i, j, k));
+    }
+
+    protected void d(int i) {
+        int j = i & 7;
+        float f = (float) (2 * (1 + j)) / 16.0F;
 
         this.a(0.0F, 0.0F, 0.0F, 1.0F, f, 1.0F);
     }
@@ -35,17 +45,17 @@ public class BlockSnow extends Block {
     public boolean canPlace(World world, int i, int j, int k) {
         int l = world.getTypeId(i, j - 1, k);
 
-        return l != 0 && (l == Block.LEAVES.id || Block.byId[l].c()) ? world.getMaterial(i, j - 1, k).isSolid() : false;
+        return l == 0 ? false : (l == this.id && (world.getData(i, j - 1, k) & 7) == 7 ? true : (l != Block.LEAVES.id && !Block.byId[l].c() ? false : world.getMaterial(i, j - 1, k).isSolid()));
     }
 
     public void doPhysics(World world, int i, int j, int k, int l) {
-        this.n(world, i, j, k);
+        this.m(world, i, j, k);
     }
 
-    private boolean n(World world, int i, int j, int k) {
+    private boolean m(World world, int i, int j, int k) {
         if (!this.canPlace(world, i, j, k)) {
             this.c(world, i, j, k, world.getData(i, j, k), 0);
-            world.setTypeId(i, j, k, 0);
+            world.setAir(i, j, k);
             return false;
         } else {
             return true;
@@ -54,9 +64,10 @@ public class BlockSnow extends Block {
 
     public void a(World world, EntityHuman entityhuman, int i, int j, int k, int l) {
         int i1 = Item.SNOW_BALL.id;
+        int j1 = l & 7;
 
-        this.b(world, i, j, k, new ItemStack(i1, 1, 0));
-        world.setTypeId(i, j, k, 0);
+        this.b(world, i, j, k, new ItemStack(i1, j1 + 1, 0));
+        world.setAir(i, j, k);
         entityhuman.a(StatisticList.C[this.id], 1);
     }
 
@@ -68,10 +79,10 @@ public class BlockSnow extends Block {
         return 0;
     }
 
-    public void b(World world, int i, int j, int k, Random random) {
+    public void a(World world, int i, int j, int k, Random random) {
         if (world.b(EnumSkyBlock.BLOCK, i, j, k) > 11) {
             this.c(world, i, j, k, world.getData(i, j, k), 0);
-            world.setTypeId(i, j, k, 0);
+            world.setAir(i, j, k);
         }
     }
 }
