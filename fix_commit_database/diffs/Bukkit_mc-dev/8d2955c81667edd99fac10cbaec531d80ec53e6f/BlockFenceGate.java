@@ -2,8 +2,8 @@ package net.minecraft.server;
 
 public class BlockFenceGate extends BlockDirectional {
 
-    public BlockFenceGate(int i, int j) {
-        super(i, j, Material.WOOD);
+    public BlockFenceGate(int i) {
+        super(i, Material.WOOD);
         this.a(CreativeModeTab.d);
     }
 
@@ -11,14 +11,14 @@ public class BlockFenceGate extends BlockDirectional {
         return !world.getMaterial(i, j - 1, k).isBuildable() ? false : super.canPlace(world, i, j, k);
     }
 
-    public AxisAlignedBB e(World world, int i, int j, int k) {
+    public AxisAlignedBB b(World world, int i, int j, int k) {
         int l = world.getData(i, j, k);
 
-        return c(l) ? null : (l != 2 && l != 0 ? AxisAlignedBB.a().a((double) ((float) i + 0.375F), (double) j, (double) k, (double) ((float) i + 0.625F), (double) ((float) j + 1.5F), (double) (k + 1)) : AxisAlignedBB.a().a((double) i, (double) j, (double) ((float) k + 0.375F), (double) (i + 1), (double) ((float) j + 1.5F), (double) ((float) k + 0.625F)));
+        return k_(l) ? null : (l != 2 && l != 0 ? AxisAlignedBB.a().a((double) ((float) i + 0.375F), (double) j, (double) k, (double) ((float) i + 0.625F), (double) ((float) j + 1.5F), (double) (k + 1)) : AxisAlignedBB.a().a((double) i, (double) j, (double) ((float) k + 0.375F), (double) (i + 1), (double) ((float) j + 1.5F), (double) ((float) k + 0.625F)));
     }
 
     public void updateShape(IBlockAccess iblockaccess, int i, int j, int k) {
-        int l = e(iblockaccess.getData(i, j, k));
+        int l = j(iblockaccess.getData(i, j, k));
 
         if (l != 2 && l != 0) {
             this.a(0.375F, 0.0F, 0.0F, 0.625F, 1.0F, 1.0F);
@@ -35,34 +35,34 @@ public class BlockFenceGate extends BlockDirectional {
         return false;
     }
 
-    public boolean c(IBlockAccess iblockaccess, int i, int j, int k) {
-        return c(iblockaccess.getData(i, j, k));
+    public boolean b(IBlockAccess iblockaccess, int i, int j, int k) {
+        return k_(iblockaccess.getData(i, j, k));
     }
 
     public int d() {
         return 21;
     }
 
-    public void postPlace(World world, int i, int j, int k, EntityLiving entityliving) {
+    public void postPlace(World world, int i, int j, int k, EntityLiving entityliving, ItemStack itemstack) {
         int l = (MathHelper.floor((double) (entityliving.yaw * 4.0F / 360.0F) + 0.5D) & 3) % 4;
 
-        world.setData(i, j, k, l);
+        world.setData(i, j, k, l, 2);
     }
 
     public boolean interact(World world, int i, int j, int k, EntityHuman entityhuman, int l, float f, float f1, float f2) {
         int i1 = world.getData(i, j, k);
 
-        if (c(i1)) {
-            world.setData(i, j, k, i1 & -5);
+        if (k_(i1)) {
+            world.setData(i, j, k, i1 & -5, 2);
         } else {
             int j1 = (MathHelper.floor((double) (entityhuman.yaw * 4.0F / 360.0F) + 0.5D) & 3) % 4;
-            int k1 = e(i1);
+            int k1 = j(i1);
 
             if (k1 == (j1 + 2) % 4) {
                 i1 = j1;
             }
 
-            world.setData(i, j, k, i1 | 4);
+            world.setData(i, j, k, i1 | 4, 2);
         }
 
         world.a(entityhuman, 1003, i, j, k, 0);
@@ -74,19 +74,19 @@ public class BlockFenceGate extends BlockDirectional {
             int i1 = world.getData(i, j, k);
             boolean flag = world.isBlockIndirectlyPowered(i, j, k);
 
-            if (flag || l > 0 && Block.byId[l].isPowerSource() || l == 0) {
-                if (flag && !c(i1)) {
-                    world.setData(i, j, k, i1 | 4);
+            if (flag || l > 0 && Block.byId[l].isPowerSource()) {
+                if (flag && !k_(i1)) {
+                    world.setData(i, j, k, i1 | 4, 2);
                     world.a((EntityHuman) null, 1003, i, j, k, 0);
-                } else if (!flag && c(i1)) {
-                    world.setData(i, j, k, i1 & -5);
+                } else if (!flag && k_(i1)) {
+                    world.setData(i, j, k, i1 & -5, 2);
                     world.a((EntityHuman) null, 1003, i, j, k, 0);
                 }
             }
         }
     }
 
-    public static boolean c(int i) {
+    public static boolean k_(int i) {
         return (i & 4) != 0;
     }
 }
