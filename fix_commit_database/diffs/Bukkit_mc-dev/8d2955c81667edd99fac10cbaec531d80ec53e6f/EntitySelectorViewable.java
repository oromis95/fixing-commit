@@ -0,0 +1,14 @@
+package net.minecraft.server;
+
+class EntitySelectorViewable implements IEntitySelector {
+
+    final PathfinderGoalAvoidPlayer c;
+
+    EntitySelectorViewable(PathfinderGoalAvoidPlayer pathfindergoalavoidplayer) {
+        this.c = pathfindergoalavoidplayer;
+    }
+
+    public boolean a(Entity entity) {
+        return entity.isAlive() && PathfinderGoalAvoidPlayer.a(this.c).aD().canSee(entity);
+    }
+}
