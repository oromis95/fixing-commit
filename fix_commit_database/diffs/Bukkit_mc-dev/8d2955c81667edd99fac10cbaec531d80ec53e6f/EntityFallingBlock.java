@@ -9,19 +9,21 @@ public class EntityFallingBlock extends Entity {
     public int data;
     public int c;
     public boolean dropItem;
-    private boolean e;
+    private boolean f;
     private boolean hurtEntities;
     private int fallHurtMax;
     private float fallHurtAmount;
+    public NBTTagCompound tileEntityData;
 
     public EntityFallingBlock(World world) {
         super(world);
         this.c = 0;
         this.dropItem = true;
-        this.e = false;
+        this.f = false;
         this.hurtEntities = false;
         this.fallHurtMax = 40;
         this.fallHurtAmount = 2.0F;
+        this.tileEntityData = null;
     }
 
     public EntityFallingBlock(World world, double d0, double d1, double d2, int i) {
@@ -32,10 +34,11 @@ public class EntityFallingBlock extends Entity {
         super(world);
         this.c = 0;
         this.dropItem = true;
-        this.e = false;
+        this.f = false;
         this.hurtEntities = false;
         this.fallHurtMax = 40;
         this.fallHurtAmount = 2.0F;
+        this.tileEntityData = null;
         this.id = i;
         this.data = j;
         this.m = true;
@@ -56,11 +59,11 @@ public class EntityFallingBlock extends Entity {
 
     protected void a() {}
 
-    public boolean L() {
+    public boolean K() {
         return !this.dead;
     }
 
-    public void j_() {
+    public void l_() {
         if (this.id == 0) {
             this.die();
         } else {
@@ -79,12 +82,12 @@ public class EntityFallingBlock extends Entity {
                 int k = MathHelper.floor(this.locZ);
 
                 if (this.c == 1) {
-                    if (this.c != 1 || this.world.getTypeId(i, j, k) != this.id) {
+                    if (this.world.getTypeId(i, j, k) != this.id) {
                         this.die();
                         return;
                     }
 
-                    this.world.setTypeId(i, j, k, 0);
+                    this.world.setAir(i, j, k);
                 }
 
                 if (this.onGround) {
@@ -93,11 +96,33 @@ public class EntityFallingBlock extends Entity {
                     this.motY *= -0.5D;
                     if (this.world.getTypeId(i, j, k) != Block.PISTON_MOVING.id) {
                         this.die();
-                        if (!this.e && this.world.mayPlace(this.id, i, j, k, true, 1, (Entity) null) && !BlockSand.canFall(this.world, i, j - 1, k) && this.world.setTypeIdAndData(i, j, k, this.id, this.data)) {
+                        if (!this.f && this.world.mayPlace(this.id, i, j, k, true, 1, (Entity) null, (ItemStack) null) && !BlockSand.canFall(this.world, i, j - 1, k) && this.world.setTypeIdAndData(i, j, k, this.id, this.data, 3)) {
                             if (Block.byId[this.id] instanceof BlockSand) {
                                 ((BlockSand) Block.byId[this.id]).a_(this.world, i, j, k, this.data);
                             }
-                        } else if (this.dropItem && !this.e) {
+
+                            if (this.tileEntityData != null && Block.byId[this.id] instanceof IContainer) {
+                                TileEntity tileentity = this.world.getTileEntity(i, j, k);
+
+                                if (tileentity != null) {
+                                    NBTTagCompound nbttagcompound = new NBTTagCompound();
+
+                                    tileentity.b(nbttagcompound);
+                                    Iterator iterator = this.tileEntityData.c().iterator();
+
+                                    while (iterator.hasNext()) {
+                                        NBTBase nbtbase = (NBTBase) iterator.next();
+
+                                        if (!nbtbase.getName().equals("x") && !nbtbase.getName().equals("y") && !nbtbase.getName().equals("z")) {
+                                            nbttagcompound.set(nbtbase.getName(), nbtbase.clone());
+                                        }
+                                    }
+
+                                    tileentity.a(nbttagcompound);
+                                    tileentity.update();
+                                }
+                            }
+                        } else if (this.dropItem && !this.f) {
                             this.a(new ItemStack(this.id, 1, Block.byId[this.id].getDropData(this.data)), 0.0F);
                         }
                     }
@@ -133,7 +158,7 @@ public class EntityFallingBlock extends Entity {
 
                     ++j;
                     if (j > 2) {
-                        this.e = true;
+                        this.f = true;
                     } else {
                         this.data = k | j << 2;
                     }
@@ -144,16 +169,25 @@ public class EntityFallingBlock extends Entity {
 
     protected void b(NBTTagCompound nbttagcompound) {
         nbttagcompound.setByte("Tile", (byte) this.id);
+        nbttagcompound.setInt("TileID", this.id);
         nbttagcompound.setByte("Data", (byte) this.data);
         nbttagcompound.setByte("Time", (byte) this.c);
         nbttagcompound.setBoolean("DropItem", this.dropItem);
         nbttagcompound.setBoolean("HurtEntities", this.hurtEntities);
         nbttagcompound.setFloat("FallHurtAmount", this.fallHurtAmount);
         nbttagcompound.setInt("FallHurtMax", this.fallHurtMax);
+        if (this.tileEntityData != null) {
+            nbttagcompound.setCompound("TileEntityData", this.tileEntityData);
+        }
     }
 
     protected void a(NBTTagCompound nbttagcompound) {
-        this.id = nbttagcompound.getByte("Tile") & 255;
+        if (nbttagcompound.hasKey("TileID")) {
+            this.id = nbttagcompound.getInt("TileID");
+        } else {
+            this.id = nbttagcompound.getByte("Tile") & 255;
+        }
+
         this.data = nbttagcompound.getByte("Data") & 255;
         this.c = nbttagcompound.getByte("Time") & 255;
         if (nbttagcompound.hasKey("HurtEntities")) {
@@ -168,12 +202,16 @@ public class EntityFallingBlock extends Entity {
             this.dropItem = nbttagcompound.getBoolean("DropItem");
         }
 
+        if (nbttagcompound.hasKey("TileEntityData")) {
+            this.tileEntityData = nbttagcompound.getCompound("TileEntityData");
+        }
+
         if (this.id == 0) {
             this.id = Block.SAND.id;
         }
     }
 
-    public void e(boolean flag) {
+    public void a(boolean flag) {
         this.hurtEntities = flag;
     }
 
