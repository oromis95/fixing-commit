@@ -4,8 +4,8 @@ class ContainerAnvilInventory extends InventorySubcontainer {
 
     final ContainerAnvil a;
 
-    ContainerAnvilInventory(ContainerAnvil containeranvil, String s, int i) {
-        super(s, i);
+    ContainerAnvilInventory(ContainerAnvil containeranvil, String s, boolean flag, int i) {
+        super(s, flag, i);
         this.a = containeranvil;
     }
 
@@ -13,4 +13,8 @@ class ContainerAnvilInventory extends InventorySubcontainer {
         super.update();
         this.a.a((IInventory) this);
     }
+
+    public boolean b(int i, ItemStack itemstack) {
+        return true;
+    }
 }
