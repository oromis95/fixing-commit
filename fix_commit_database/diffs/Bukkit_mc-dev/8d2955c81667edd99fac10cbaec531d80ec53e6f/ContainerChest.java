@@ -32,7 +32,7 @@ public class ContainerChest extends Container {
     }
 
     public boolean a(EntityHuman entityhuman) {
-        return this.container.a_(entityhuman);
+        return this.container.a(entityhuman);
     }
 
     public ItemStack b(EntityHuman entityhuman, int i) {
@@ -63,10 +63,10 @@ public class ContainerChest extends Container {
 
     public void b(EntityHuman entityhuman) {
         super.b(entityhuman);
-        this.container.f();
+        this.container.g();
     }
 
-    public IInventory d() {
+    public IInventory e() {
         return this.container;
     }
 }
