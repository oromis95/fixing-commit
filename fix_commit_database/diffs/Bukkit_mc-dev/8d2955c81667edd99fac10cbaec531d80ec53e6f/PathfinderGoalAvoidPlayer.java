@@ -4,78 +4,79 @@ import java.util.List;
 
 public class PathfinderGoalAvoidPlayer extends PathfinderGoal {
 
-    private EntityCreature a;
-    private float b;
+    public final IEntitySelector a = new EntitySelectorViewable(this);
+    private EntityCreature b;
     private float c;
-    private Entity d;
-    private float e;
-    private PathEntity f;
-    private Navigation g;
-    private Class h;
+    private float d;
+    private Entity e;
+    private float f;
+    private PathEntity g;
+    private Navigation h;
+    private Class i;
 
     public PathfinderGoalAvoidPlayer(EntityCreature entitycreature, Class oclass, float f, float f1, float f2) {
-        this.a = entitycreature;
-        this.h = oclass;
-        this.e = f;
-        this.b = f1;
-        this.c = f2;
-        this.g = entitycreature.getNavigation();
+        this.b = entitycreature;
+        this.i = oclass;
+        this.f = f;
+        this.c = f1;
+        this.d = f2;
+        this.h = entitycreature.getNavigation();
         this.a(1);
     }
 
     public boolean a() {
-        if (this.h == EntityHuman.class) {
-            if (this.a instanceof EntityTameableAnimal && ((EntityTameableAnimal) this.a).isTamed()) {
+        if (this.i == EntityHuman.class) {
+            if (this.b instanceof EntityTameableAnimal && ((EntityTameableAnimal) this.b).isTamed()) {
                 return false;
             }
 
-            this.d = this.a.world.findNearbyPlayer(this.a, (double) this.e);
-            if (this.d == null) {
+            this.e = this.b.world.findNearbyPlayer(this.b, (double) this.f);
+            if (this.e == null) {
                 return false;
             }
         } else {
-            List list = this.a.world.a(this.h, this.a.boundingBox.grow((double) this.e, 3.0D, (double) this.e));
+            List list = this.b.world.a(this.i, this.b.boundingBox.grow((double) this.f, 3.0D, (double) this.f), this.a);
 
             if (list.isEmpty()) {
                 return false;
             }
 
-            this.d = (Entity) list.get(0);
+            this.e = (Entity) list.get(0);
         }
 
-        if (!this.a.aA().canSee(this.d)) {
+        Vec3D vec3d = RandomPositionGenerator.b(this.b, 16, 7, this.b.world.getVec3DPool().create(this.e.locX, this.e.locY, this.e.locZ));
+
+        if (vec3d == null) {
+            return false;
+        } else if (this.e.e(vec3d.c, vec3d.d, vec3d.e) < this.e.e((Entity) this.b)) {
             return false;
         } else {
-            Vec3D vec3d = RandomPositionGenerator.b(this.a, 16, 7, this.a.world.getVec3DPool().create(this.d.locX, this.d.locY, this.d.locZ));
-
-            if (vec3d == null) {
-                return false;
-            } else if (this.d.e(vec3d.c, vec3d.d, vec3d.e) < this.d.e((Entity) this.a)) {
-                return false;
-            } else {
-                this.f = this.g.a(vec3d.c, vec3d.d, vec3d.e);
-                return this.f == null ? false : this.f.b(vec3d);
-            }
+            this.g = this.h.a(vec3d.c, vec3d.d, vec3d.e);
+            return this.g == null ? false : this.g.b(vec3d);
         }
     }
 
     public boolean b() {
-        return !this.g.f();
+        return !this.h.f();
     }
 
     public void c() {
-        this.g.a(this.f, this.b);
+        this.h.a(this.g, this.c);
     }
 
     public void d() {
-        this.d = null;
+        this.e = null;
     }
 
     public void e() {
-        if (this.a.e(this.d) < 49.0D) {
-            this.a.getNavigation().a(this.c);
+        if (this.b.e(this.e) < 49.0D) {
+            this.b.getNavigation().a(this.d);
         } else {
-            this.a.getNavigation().a(this.b);
+            this.b.getNavigation().a(this.c);
         }
     }
+
+    static EntityCreature a(PathfinderGoalAvoidPlayer pathfindergoalavoidplayer) {
+        return pathfindergoalavoidplayer.b;
+    }
 }
