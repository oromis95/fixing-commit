@@ -41,8 +41,8 @@ public class ItemFireball extends Item {
                 int i1 = world.getTypeId(i, j, k);
 
                 if (i1 == 0) {
-                    world.makeSound((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "fire.ignite", 1.0F, d.nextFloat() * 0.4F + 0.8F);
-                    world.setTypeId(i, j, k, Block.FIRE.id);
+                    world.makeSound((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "fire.ignite", 1.0F, e.nextFloat() * 0.4F + 0.8F);
+                    world.setTypeIdUpdate(i, j, k, Block.FIRE.id);
                 }
 
                 if (!entityhuman.abilities.canInstantlyBuild) {
