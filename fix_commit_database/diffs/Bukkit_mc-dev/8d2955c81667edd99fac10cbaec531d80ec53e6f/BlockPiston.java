@@ -4,22 +4,16 @@ import java.util.List;
 
 public class BlockPiston extends Block {
 
-    private boolean a;
+    private final boolean a;
 
-    public BlockPiston(int i, int j, boolean flag) {
-        super(i, j, Material.PISTON);
+    public BlockPiston(int i, boolean flag) {
+        super(i, Material.PISTON);
         this.a = flag;
-        this.a(h);
+        this.a(j);
         this.c(0.5F);
         this.a(CreativeModeTab.d);
     }
 
-    public int a(int i, int j) {
-        int k = e(j);
-
-        return k > 5 ? this.textureId : (i == k ? (!f(j) && this.minX <= 0.0D && this.minY <= 0.0D && this.minZ <= 0.0D && this.maxX >= 1.0D && this.maxY >= 1.0D && this.maxZ >= 1.0D ? this.textureId : 110) : (i == Facing.OPPOSITE_FACING[k] ? 109 : 108));
-    }
-
     public int d() {
         return 16;
     }
@@ -32,62 +26,70 @@ public class BlockPiston extends Block {
         return false;
     }
 
-    public void postPlace(World world, int i, int j, int k, EntityLiving entityliving) {
-        int l = b(world, i, j, k, (EntityHuman) entityliving);
+    public void postPlace(World world, int i, int j, int k, EntityLiving entityliving, ItemStack itemstack) {
+        int l = a(world, i, j, k, entityliving);
 
-        world.setData(i, j, k, l);
+        world.setData(i, j, k, l, 2);
         if (!world.isStatic) {
-            this.l(world, i, j, k);
+            this.k(world, i, j, k);
         }
     }
 
     public void doPhysics(World world, int i, int j, int k, int l) {
         if (!world.isStatic) {
-            this.l(world, i, j, k);
+            this.k(world, i, j, k);
         }
     }
 
     public void onPlace(World world, int i, int j, int k) {
         if (!world.isStatic && world.getTileEntity(i, j, k) == null) {
-            this.l(world, i, j, k);
+            this.k(world, i, j, k);
         }
     }
 
-    private void l(World world, int i, int j, int k) {
+    private void k(World world, int i, int j, int k) {
         int l = world.getData(i, j, k);
-        int i1 = e(l);
+        int i1 = d(l);
 
         if (i1 != 7) {
             boolean flag = this.d(world, i, j, k, i1);
 
-            if (flag && !f(l)) {
-                if (i(world, i, j, k, i1)) {
+            if (flag && !e(l)) {
+                if (e(world, i, j, k, i1)) {
                     world.playNote(i, j, k, this.id, 0, i1);
                 }
-            } else if (!flag && f(l)) {
+            } else if (!flag && e(l)) {
+                world.setData(i, j, k, i1, 2);
                 world.playNote(i, j, k, this.id, 1, i1);
             }
         }
     }
 
     private boolean d(World world, int i, int j, int k, int l) {
-        return l != 0 && world.isBlockFaceIndirectlyPowered(i, j - 1, k, 0) ? true : (l != 1 && world.isBlockFaceIndirectlyPowered(i, j + 1, k, 1) ? true : (l != 2 && world.isBlockFaceIndirectlyPowered(i, j, k - 1, 2) ? true : (l != 3 && world.isBlockFaceIndirectlyPowered(i, j, k + 1, 3) ? true : (l != 5 && world.isBlockFaceIndirectlyPowered(i + 1, j, k, 5) ? true : (l != 4 && world.isBlockFaceIndirectlyPowered(i - 1, j, k, 4) ? true : (world.isBlockFaceIndirectlyPowered(i, j, k, 0) ? true : (world.isBlockFaceIndirectlyPowered(i, j + 2, k, 1) ? true : (world.isBlockFaceIndirectlyPowered(i, j + 1, k - 1, 2) ? true : (world.isBlockFaceIndirectlyPowered(i, j + 1, k + 1, 3) ? true : (world.isBlockFaceIndirectlyPowered(i - 1, j + 1, k, 4) ? true : world.isBlockFaceIndirectlyPowered(i + 1, j + 1, k, 5)))))))))));
+        return l != 0 && world.isBlockFacePowered(i, j - 1, k, 0) ? true : (l != 1 && world.isBlockFacePowered(i, j + 1, k, 1) ? true : (l != 2 && world.isBlockFacePowered(i, j, k - 1, 2) ? true : (l != 3 && world.isBlockFacePowered(i, j, k + 1, 3) ? true : (l != 5 && world.isBlockFacePowered(i + 1, j, k, 5) ? true : (l != 4 && world.isBlockFacePowered(i - 1, j, k, 4) ? true : (world.isBlockFacePowered(i, j, k, 0) ? true : (world.isBlockFacePowered(i, j + 2, k, 1) ? true : (world.isBlockFacePowered(i, j + 1, k - 1, 2) ? true : (world.isBlockFacePowered(i, j + 1, k + 1, 3) ? true : (world.isBlockFacePowered(i - 1, j + 1, k, 4) ? true : world.isBlockFacePowered(i + 1, j + 1, k, 5)))))))))));
     }
 
-    public void b(World world, int i, int j, int k, int l, int i1) {
-        if (l == 0) {
-            world.setRawData(i, j, k, i1 | 8);
-        } else {
-            world.setRawData(i, j, k, i1);
+    public boolean b(World world, int i, int j, int k, int l, int i1) {
+        if (!world.isStatic) {
+            boolean flag = this.d(world, i, j, k, i1);
+
+            if (flag && l == 1) {
+                world.setData(i, j, k, i1 | 8, 2);
+                return false;
+            }
+
+            if (!flag && l == 0) {
+                return false;
+            }
         }
 
         if (l == 0) {
-            if (this.j(world, i, j, k, i1)) {
-                world.setData(i, j, k, i1 | 8);
-                world.makeSound((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "tile.piston.out", 0.5F, world.random.nextFloat() * 0.25F + 0.6F);
-            } else {
-                world.setRawData(i, j, k, i1);
+            if (!this.f(world, i, j, k, i1)) {
+                return false;
             }
+
+            world.setData(i, j, k, i1 | 8, 2);
+            world.makeSound((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "tile.piston.out", 0.5F, world.random.nextFloat() * 0.25F + 0.6F);
         } else if (l == 1) {
             TileEntity tileentity = world.getTileEntity(i + Facing.b[i1], j + Facing.c[i1], k + Facing.d[i1]);
 
@@ -95,7 +97,7 @@ public class BlockPiston extends Block {
                 ((TileEntityPiston) tileentity).f();
             }
 
-            world.setRawTypeIdAndData(i, j, k, Block.PISTON_MOVING.id, i1);
+            world.setTypeIdAndData(i, j, k, Block.PISTON_MOVING.id, i1, 3);
             world.setTileEntity(i, j, k, BlockPistonMoving.a(this.id, i1, i1, false, true));
             if (this.a) {
                 int j1 = i + Facing.b[i1] * 2;
@@ -103,7 +105,7 @@ public class BlockPiston extends Block {
                 int l1 = k + Facing.d[i1] * 2;
                 int i2 = world.getTypeId(j1, k1, l1);
                 int j2 = world.getData(j1, k1, l1);
-                boolean flag = false;
+                boolean flag1 = false;
 
                 if (i2 == Block.PISTON_MOVING.id) {
                     TileEntity tileentity1 = world.getTileEntity(j1, k1, l1);
@@ -115,34 +117,36 @@ public class BlockPiston extends Block {
                             tileentitypiston.f();
                             i2 = tileentitypiston.a();
                             j2 = tileentitypiston.p();
-                            flag = true;
+                            flag1 = true;
                         }
                     }
                 }
 
-                if (!flag && i2 > 0 && a(i2, world, j1, k1, l1, false) && (Block.byId[i2].q_() == 0 || i2 == Block.PISTON.id || i2 == Block.PISTON_STICKY.id)) {
+                if (!flag1 && i2 > 0 && a(i2, world, j1, k1, l1, false) && (Block.byId[i2].h() == 0 || i2 == Block.PISTON.id || i2 == Block.PISTON_STICKY.id)) {
                     i += Facing.b[i1];
                     j += Facing.c[i1];
                     k += Facing.d[i1];
-                    world.setRawTypeIdAndData(i, j, k, Block.PISTON_MOVING.id, j2);
+                    world.setTypeIdAndData(i, j, k, Block.PISTON_MOVING.id, j2, 3);
                     world.setTileEntity(i, j, k, BlockPistonMoving.a(i2, j2, i1, false, false));
-                    world.setTypeId(j1, k1, l1, 0);
-                } else if (!flag) {
-                    world.setTypeId(i + Facing.b[i1], j + Facing.c[i1], k + Facing.d[i1], 0);
+                    world.setAir(j1, k1, l1);
+                } else if (!flag1) {
+                    world.setAir(i + Facing.b[i1], j + Facing.c[i1], k + Facing.d[i1]);
                 }
             } else {
-                world.setTypeId(i + Facing.b[i1], j + Facing.c[i1], k + Facing.d[i1], 0);
+                world.setAir(i + Facing.b[i1], j + Facing.c[i1], k + Facing.d[i1]);
             }
 
             world.makeSound((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "tile.piston.in", 0.5F, world.random.nextFloat() * 0.15F + 0.6F);
         }
+
+        return true;
     }
 
     public void updateShape(IBlockAccess iblockaccess, int i, int j, int k) {
         int l = iblockaccess.getData(i, j, k);
 
-        if (f(l)) {
-            switch (e(l)) {
+        if (e(l)) {
+            switch (d(l)) {
             case 0:
                 this.a(0.0F, 0.25F, 0.0F, 1.0F, 1.0F, 1.0F);
                 break;
@@ -171,7 +175,7 @@ public class BlockPiston extends Block {
         }
     }
 
-    public void f() {
+    public void g() {
         this.a(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
     }
 
@@ -180,26 +184,26 @@ public class BlockPiston extends Block {
         super.a(world, i, j, k, axisalignedbb, list, entity);
     }
 
-    public AxisAlignedBB e(World world, int i, int j, int k) {
+    public AxisAlignedBB b(World world, int i, int j, int k) {
         this.updateShape(world, i, j, k);
-        return super.e(world, i, j, k);
+        return super.b(world, i, j, k);
     }
 
     public boolean b() {
         return false;
     }
 
-    public static int e(int i) {
+    public static int d(int i) {
         return i & 7;
     }
 
-    public static boolean f(int i) {
+    public static boolean e(int i) {
         return (i & 8) != 0;
     }
 
-    public static int b(World world, int i, int j, int k, EntityHuman entityhuman) {
-        if (MathHelper.abs((float) entityhuman.locX - (float) i) < 2.0F && MathHelper.abs((float) entityhuman.locZ - (float) k) < 2.0F) {
-            double d0 = entityhuman.locY + 1.82D - (double) entityhuman.height;
+    public static int a(World world, int i, int j, int k, EntityLiving entityliving) {
+        if (MathHelper.abs((float) entityliving.locX - (float) i) < 2.0F && MathHelper.abs((float) entityliving.locZ - (float) k) < 2.0F) {
+            double d0 = entityliving.locY + 1.82D - (double) entityliving.height;
 
             if (d0 - (double) j > 2.0D) {
                 return 1;
@@ -210,7 +214,7 @@ public class BlockPiston extends Block {
             }
         }
 
-        int l = MathHelper.floor((double) (entityhuman.yaw * 4.0F / 360.0F) + 0.5D) & 3;
+        int l = MathHelper.floor((double) (entityliving.yaw * 4.0F / 360.0F) + 0.5D) & 3;
 
         return l == 0 ? 2 : (l == 1 ? 5 : (l == 2 ? 3 : (l == 3 ? 4 : 0)));
     }
@@ -220,26 +224,30 @@ public class BlockPiston extends Block {
             return false;
         } else {
             if (i != Block.PISTON.id && i != Block.PISTON_STICKY.id) {
-                if (Block.byId[i].m(world, j, k, l) == -1.0F) {
+                if (Block.byId[i].l(world, j, k, l) == -1.0F) {
                     return false;
                 }
 
-                if (Block.byId[i].q_() == 2) {
+                if (Block.byId[i].h() == 2) {
                     return false;
                 }
 
-                if (!flag && Block.byId[i].q_() == 1) {
-                    return false;
+                if (Block.byId[i].h() == 1) {
+                    if (!flag) {
+                        return false;
+                    }
+
+                    return true;
                 }
-            } else if (f(world.getData(j, k, l))) {
+            } else if (e(world.getData(j, k, l))) {
                 return false;
             }
 
-            return !(Block.byId[i] instanceof BlockContainer);
+            return !(Block.byId[i] instanceof IContainer);
         }
     }
 
-    private static boolean i(World world, int i, int j, int k, int l) {
+    private static boolean e(World world, int i, int j, int k, int l) {
         int i1 = i + Facing.b[l];
         int j1 = j + Facing.c[l];
         int k1 = k + Facing.d[l];
@@ -258,7 +266,7 @@ public class BlockPiston extends Block {
                         return false;
                     }
 
-                    if (Block.byId[i2].q_() != 1) {
+                    if (Block.byId[i2].h() != 1) {
                         if (l1 == 12) {
                             return false;
                         }
@@ -276,7 +284,7 @@ public class BlockPiston extends Block {
         }
     }
 
-    private boolean j(World world, int i, int j, int k, int l) {
+    private boolean f(World world, int i, int j, int k, int l) {
         int i1 = i + Facing.b[l];
         int j1 = j + Facing.c[l];
         int k1 = k + Facing.d[l];
@@ -296,7 +304,7 @@ public class BlockPiston extends Block {
                         return false;
                     }
 
-                    if (Block.byId[i2].q_() != 1) {
+                    if (Block.byId[i2].h() != 1) {
                         if (l1 == 12) {
                             return false;
                         }
@@ -309,28 +317,51 @@ public class BlockPiston extends Block {
                     }
 
                     Block.byId[i2].c(world, i1, j1, k1, world.getData(i1, j1, k1), 0);
-                    world.setTypeId(i1, j1, k1, 0);
+                    world.setAir(i1, j1, k1);
                 }
             }
 
-            while (i1 != i || j1 != j || k1 != k) {
-                l1 = i1 - Facing.b[l];
-                i2 = j1 - Facing.c[l];
-                int j2 = k1 - Facing.d[l];
-                int k2 = world.getTypeId(l1, i2, j2);
-                int l2 = world.getData(l1, i2, j2);
-
-                if (k2 == this.id && l1 == i && i2 == j && j2 == k) {
-                    world.setRawTypeIdAndData(i1, j1, k1, Block.PISTON_MOVING.id, l | (this.a ? 8 : 0), false);
+            l1 = i1;
+            i2 = j1;
+            int j2 = k1;
+            int k2 = 0;
+
+            int[] aint;
+            int l2;
+            int i3;
+            int j3;
+
+            for (aint = new int[13]; i1 != i || j1 != j || k1 != k; k1 = j3) {
+                l2 = i1 - Facing.b[l];
+                i3 = j1 - Facing.c[l];
+                j3 = k1 - Facing.d[l];
+                int k3 = world.getTypeId(l2, i3, j3);
+                int l3 = world.getData(l2, i3, j3);
+
+                if (k3 == this.id && l2 == i && i3 == j && j3 == k) {
+                    world.setTypeIdAndData(i1, j1, k1, Block.PISTON_MOVING.id, l | (this.a ? 8 : 0), 4);
                     world.setTileEntity(i1, j1, k1, BlockPistonMoving.a(Block.PISTON_EXTENSION.id, l | (this.a ? 8 : 0), l, true, false));
                 } else {
-                    world.setRawTypeIdAndData(i1, j1, k1, Block.PISTON_MOVING.id, l2, false);
-                    world.setTileEntity(i1, j1, k1, BlockPistonMoving.a(k2, l2, l, true, false));
+                    world.setTypeIdAndData(i1, j1, k1, Block.PISTON_MOVING.id, l3, 4);
+                    world.setTileEntity(i1, j1, k1, BlockPistonMoving.a(k3, l3, l, true, false));
                 }
 
-                i1 = l1;
-                j1 = i2;
-                k1 = j2;
+                aint[k2++] = k3;
+                i1 = l2;
+                j1 = i3;
+            }
+
+            i1 = l1;
+            j1 = i2;
+            k1 = j2;
+
+            for (k2 = 0; i1 != i || j1 != j || k1 != k; k1 = j3) {
+                l2 = i1 - Facing.b[l];
+                i3 = j1 - Facing.c[l];
+                j3 = k1 - Facing.d[l];
+                world.applyPhysics(l2, i3, j3, aint[k2++]);
+                i1 = l2;
+                j1 = i3;
             }
 
             return true;
