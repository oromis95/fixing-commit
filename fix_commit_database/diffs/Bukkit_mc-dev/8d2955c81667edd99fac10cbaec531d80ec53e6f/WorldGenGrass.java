@@ -24,8 +24,8 @@ public class WorldGenGrass extends WorldGenerator {
             int k1 = j + random.nextInt(4) - random.nextInt(4);
             int l1 = k + random.nextInt(8) - random.nextInt(8);
 
-            if (world.isEmpty(j1, k1, l1) && Block.byId[this.a].d(world, j1, k1, l1)) {
-                world.setRawTypeIdAndData(j1, k1, l1, this.a, this.b);
+            if (world.isEmpty(j1, k1, l1) && Block.byId[this.a].f(world, j1, k1, l1)) {
+                world.setTypeIdAndData(j1, k1, l1, this.a, this.b, 2);
             }
         }
 
