@@ -10,9 +10,9 @@ public interface IBlockAccess {
 
     Material getMaterial(int i, int j, int k);
 
-    boolean t(int i, int j, int k);
+    boolean u(int i, int j, int k);
 
     Vec3DPool getVec3DPool();
 
-    boolean isBlockFacePowered(int i, int j, int k, int l);
+    int getBlockPower(int i, int j, int k, int l);
 }
