@@ -78,11 +78,11 @@ public abstract class EntityProjectile extends Entity implements IProjectile {
         this.i = 0;
     }
 
-    public void j_() {
-        this.T = this.locX;
-        this.U = this.locY;
-        this.V = this.locZ;
-        super.j_();
+    public void l_() {
+        this.U = this.locX;
+        this.V = this.locY;
+        this.W = this.locZ;
+        super.l_();
         if (this.shake > 0) {
             --this.shake;
         }
@@ -128,7 +128,7 @@ public abstract class EntityProjectile extends Entity implements IProjectile {
             for (int j = 0; j < list.size(); ++j) {
                 Entity entity1 = (Entity) list.get(j);
 
-                if (entity1.L() && (entity1 != entityliving || this.j >= 5)) {
+                if (entity1.K() && (entity1 != entityliving || this.j >= 5)) {
                     float f = 0.3F;
                     AxisAlignedBB axisalignedbb = entity1.boundingBox.grow((double) f, (double) f, (double) f);
                     MovingObjectPosition movingobjectposition1 = axisalignedbb.a(vec3d, vec3d1);
@@ -151,7 +151,7 @@ public abstract class EntityProjectile extends Entity implements IProjectile {
 
         if (movingobjectposition != null) {
             if (movingobjectposition.type == EnumMovingObjectType.TILE && this.world.getTypeId(movingobjectposition.b, movingobjectposition.c, movingobjectposition.d) == Block.PORTAL.id) {
-                this.aa();
+                this.Z();
             } else {
                 this.a(movingobjectposition);
             }
@@ -185,7 +185,7 @@ public abstract class EntityProjectile extends Entity implements IProjectile {
         float f2 = 0.99F;
         float f3 = this.g();
 
-        if (this.H()) {
+        if (this.G()) {
             for (int k = 0; k < 4; ++k) {
                 float f4 = 0.25F;
 
