@@ -1,12 +1,8 @@
 package net.minecraft.server;
 
-public class DispenseBehaviorEgg extends DispenseBehaviorProjectile {
+final class DispenseBehaviorEgg extends DispenseBehaviorProjectile {
 
-    final MinecraftServer b;
-
-    public DispenseBehaviorEgg(MinecraftServer minecraftserver) {
-        this.b = minecraftserver;
-    }
+    DispenseBehaviorEgg() {}
 
     protected IProjectile a(World world, IPosition iposition) {
         return new EntityEgg(world, iposition.getX(), iposition.getY(), iposition.getZ());
