@@ -8,45 +8,42 @@ import java.util.ArrayList;
 import java.util.Collections;
 import java.util.HashMap;
 import java.util.List;
-import java.util.logging.Level;
-import java.util.logging.Logger;
 
 public class DedicatedServerConnectionThread extends Thread {
 
-    private static Logger a = Logger.getLogger("Minecraft");
-    private final List b = Collections.synchronizedList(new ArrayList());
-    private final HashMap c = new HashMap();
-    private int d = 0;
-    private final ServerSocket e;
-    private ServerConnection f;
-    private final InetAddress g;
-    private final int h;
+    private final List a = Collections.synchronizedList(new ArrayList());
+    private final HashMap b = new HashMap();
+    private int c = 0;
+    private final ServerSocket d;
+    private ServerConnection e;
+    private final InetAddress f;
+    private final int g;
 
     public DedicatedServerConnectionThread(ServerConnection serverconnection, InetAddress inetaddress, int i) {
         super("Listen thread");
-        this.f = serverconnection;
-        this.h = i;
-        this.e = new ServerSocket(i, 0, inetaddress);
-        this.g = inetaddress == null ? this.e.getInetAddress() : inetaddress;
-        this.e.setPerformancePreferences(0, 2, 1);
+        this.e = serverconnection;
+        this.g = i;
+        this.d = new ServerSocket(i, 0, inetaddress);
+        this.f = inetaddress == null ? this.d.getInetAddress() : inetaddress;
+        this.d.setPerformancePreferences(0, 2, 1);
     }
 
     public void a() {
-        List list = this.b;
+        List list = this.a;
 
-        synchronized (this.b) {
-            for (int i = 0; i < this.b.size(); ++i) {
-                PendingConnection pendingconnection = (PendingConnection) this.b.get(i);
+        synchronized (this.a) {
+            for (int i = 0; i < this.a.size(); ++i) {
+                PendingConnection pendingconnection = (PendingConnection) this.a.get(i);
 
                 try {
                     pendingconnection.c();
                 } catch (Exception exception) {
                     pendingconnection.disconnect("Internal server error");
-                    a.log(Level.WARNING, "Failed to handle packet for " + pendingconnection.getName() + ": " + exception, exception);
+                    this.e.d().getLogger().warning("Failed to handle packet for " + pendingconnection.getName() + ": " + exception, (Throwable) exception);
                 }
 
-                if (pendingconnection.c) {
-                    this.b.remove(i--);
+                if (pendingconnection.b) {
+                    this.a.remove(i--);
                 }
 
                 pendingconnection.networkManager.a();
@@ -55,24 +52,24 @@ public class DedicatedServerConnectionThread extends Thread {
     }
 
     public void run() {
-        while (this.f.b) {
+        while (this.e.a) {
             try {
-                Socket socket = this.e.accept();
+                Socket socket = this.d.accept();
                 InetAddress inetaddress = socket.getInetAddress();
                 long i = System.currentTimeMillis();
-                HashMap hashmap = this.c;
+                HashMap hashmap = this.b;
 
-                synchronized (this.c) {
-                    if (this.c.containsKey(inetaddress) && !b(inetaddress) && i - ((Long) this.c.get(inetaddress)).longValue() < 4000L) {
-                        this.c.put(inetaddress, Long.valueOf(i));
+                synchronized (this.b) {
+                    if (this.b.containsKey(inetaddress) && !b(inetaddress) && i - ((Long) this.b.get(inetaddress)).longValue() < 4000L) {
+                        this.b.put(inetaddress, Long.valueOf(i));
                         socket.close();
                         continue;
                     }
 
-                    this.c.put(inetaddress, Long.valueOf(i));
+                    this.b.put(inetaddress, Long.valueOf(i));
                 }
 
-                PendingConnection pendingconnection = new PendingConnection(this.f.d(), socket, "Connection #" + this.d++);
+                PendingConnection pendingconnection = new PendingConnection(this.e.d(), socket, "Connection #" + this.c++);
 
                 this.a(pendingconnection);
             } catch (IOException ioexception) {
@@ -80,17 +77,17 @@ public class DedicatedServerConnectionThread extends Thread {
             }
         }
 
-        System.out.println("Closing listening thread");
+        this.e.d().getLogger().info("Closing listening thread");
     }
 
     private void a(PendingConnection pendingconnection) {
         if (pendingconnection == null) {
             throw new IllegalArgumentException("Got null pendingconnection!");
         } else {
-            List list = this.b;
+            List list = this.a;
 
-            synchronized (this.b) {
-                this.b.add(pendingconnection);
+            synchronized (this.a) {
+                this.a.add(pendingconnection);
             }
         }
     }
@@ -101,17 +98,17 @@ public class DedicatedServerConnectionThread extends Thread {
 
     public void a(InetAddress inetaddress) {
         if (inetaddress != null) {
-            HashMap hashmap = this.c;
+            HashMap hashmap = this.b;
 
-            synchronized (this.c) {
-                this.c.remove(inetaddress);
+            synchronized (this.b) {
+                this.b.remove(inetaddress);
             }
         }
     }
 
     public void b() {
         try {
-            this.e.close();
+            this.d.close();
         } catch (Throwable throwable) {
             ;
         }
