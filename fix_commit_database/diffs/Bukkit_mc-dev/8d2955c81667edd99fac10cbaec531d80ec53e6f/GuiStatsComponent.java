@@ -30,12 +30,12 @@ public class GuiStatsComponent extends JComponent {
         System.gc();
         this.d[0] = "Memory use: " + i / 1024L / 1024L + " mb (" + Runtime.getRuntime().freeMemory() * 100L / Runtime.getRuntime().maxMemory() + "% free)";
         this.d[1] = "Threads: " + NetworkManager.a.get() + " + " + NetworkManager.b.get();
-        this.d[2] = "Avg tick: " + a.format(this.a(this.e.j) * 1.0E-6D) + " ms";
-        this.d[3] = "Avg sent: " + (int) this.a(this.e.f) + ", Avg size: " + (int) this.a(this.e.g);
-        this.d[4] = "Avg rec: " + (int) this.a(this.e.h) + ", Avg size: " + (int) this.a(this.e.i);
+        this.d[2] = "Avg tick: " + a.format(this.a(this.e.i) * 1.0E-6D) + " ms";
+        this.d[3] = "Avg sent: " + (int) this.a(this.e.e) + ", Avg size: " + (int) this.a(this.e.f);
+        this.d[4] = "Avg rec: " + (int) this.a(this.e.g) + ", Avg size: " + (int) this.a(this.e.h);
         if (this.e.worldServer != null) {
             for (int j = 0; j < this.e.worldServer.length; ++j) {
-                this.d[5 + j] = "Lvl " + j + " tick: " + a.format(this.a(this.e.k[j]) * 1.0E-6D) + " ms";
+                this.d[5 + j] = "Lvl " + j + " tick: " + a.format(this.a(this.e.j[j]) * 1.0E-6D) + " ms";
                 if (this.e.worldServer[j] != null && this.e.worldServer[j].chunkProviderServer != null) {
                     this.d[5 + j] = this.d[5 + j] + ", " + this.e.worldServer[j].chunkProviderServer.getName();
                     this.d[5 + j] = this.d[5 + j] + ", Vec3: " + this.e.worldServer[j].getVec3DPool().d() + " / " + this.e.worldServer[j].getVec3DPool().c();
@@ -43,7 +43,7 @@ public class GuiStatsComponent extends JComponent {
             }
         }
 
-        this.b[this.c++ & 255] = (int) (this.a(this.e.g) * 100.0D / 12500.0D);
+        this.b[this.c++ & 255] = (int) (this.a(this.e.f) * 100.0D / 12500.0D);
         this.repaint();
     }
 
