@@ -4,8 +4,8 @@ import java.util.Random;
 
 public class BlockClay extends Block {
 
-    public BlockClay(int i, int j) {
-        super(i, j, Material.CLAY);
+    public BlockClay(int i) {
+        super(i, Material.CLAY);
         this.a(CreativeModeTab.b);
     }
 
