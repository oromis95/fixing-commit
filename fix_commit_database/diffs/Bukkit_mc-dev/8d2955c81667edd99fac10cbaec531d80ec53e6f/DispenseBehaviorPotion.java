@@ -1,17 +1,12 @@
 package net.minecraft.server;
 
-public class DispenseBehaviorPotion implements IDispenseBehavior {
+final class DispenseBehaviorPotion implements IDispenseBehavior {
 
-    private final DispenseBehaviorItem c;
+    private final DispenseBehaviorItem b = new DispenseBehaviorItem();
 
-    final MinecraftServer b;
-
-    public DispenseBehaviorPotion(MinecraftServer minecraftserver) {
-        this.b = minecraftserver;
-        this.c = new DispenseBehaviorItem();
-    }
+    DispenseBehaviorPotion() {}
 
     public ItemStack a(ISourceBlock isourceblock, ItemStack itemstack) {
-        return ItemPotion.g(itemstack.getData()) ? (new DispenseBehaviorThrownPotion(this, itemstack)).a(isourceblock, itemstack) : this.c.a(isourceblock, itemstack);
+        return ItemPotion.f(itemstack.getData()) ? (new DispenseBehaviorThrownPotion(this, itemstack)).a(isourceblock, itemstack) : this.b.a(isourceblock, itemstack);
     }
 }
