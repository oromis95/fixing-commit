@@ -13,7 +13,7 @@ public class PathfinderGoalRandomLookaround extends PathfinderGoal {
     }
 
     public boolean a() {
-        return this.a.aB().nextFloat() < 0.02F;
+        return this.a.aE().nextFloat() < 0.02F;
     }
 
     public boolean b() {
@@ -21,15 +21,15 @@ public class PathfinderGoalRandomLookaround extends PathfinderGoal {
     }
 
     public void c() {
-        double d0 = 6.283185307179586D * this.a.aB().nextDouble();
+        double d0 = 6.283185307179586D * this.a.aE().nextDouble();
 
         this.b = Math.cos(d0);
         this.c = Math.sin(d0);
-        this.d = 20 + this.a.aB().nextInt(20);
+        this.d = 20 + this.a.aE().nextInt(20);
     }
 
     public void e() {
         --this.d;
-        this.a.getControllerLook().a(this.a.locX + this.b, this.a.locY + (double) this.a.getHeadHeight(), this.a.locZ + this.c, 10.0F, (float) this.a.bp());
+        this.a.getControllerLook().a(this.a.locX + this.b, this.a.locY + (double) this.a.getHeadHeight(), this.a.locZ + this.c, 10.0F, (float) this.a.bs());
     }
 }
