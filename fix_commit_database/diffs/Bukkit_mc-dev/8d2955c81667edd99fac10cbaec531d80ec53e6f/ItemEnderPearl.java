@@ -15,7 +15,7 @@ public class ItemEnderPearl extends Item {
             return itemstack;
         } else {
             --itemstack.count;
-            world.makeSound(entityhuman, "random.bow", 0.5F, 0.4F / (d.nextFloat() * 0.4F + 0.8F));
+            world.makeSound(entityhuman, "random.bow", 0.5F, 0.4F / (e.nextFloat() * 0.4F + 0.8F));
             if (!world.isStatic) {
                 world.addEntity(new EntityEnderPearl(world, entityhuman));
             }
