@@ -6,34 +6,34 @@ public class WorldGenCavesHell extends WorldGenBase {
 
     public WorldGenCavesHell() {}
 
-    protected void a(int i, int j, byte[] abyte, double d0, double d1, double d2) {
-        this.a(i, j, abyte, d0, d1, d2, 1.0F + this.b.nextFloat() * 6.0F, 0.0F, 0.0F, -1, -1, 0.5D);
+    protected void a(long i, int j, int k, byte[] abyte, double d0, double d1, double d2) {
+        this.a(i, j, k, abyte, d0, d1, d2, 1.0F + this.b.nextFloat() * 6.0F, 0.0F, 0.0F, -1, -1, 0.5D);
     }
 
-    protected void a(int i, int j, byte[] abyte, double d0, double d1, double d2, float f, float f1, float f2, int k, int l, double d3) {
-        double d4 = (double) (i * 16 + 8);
-        double d5 = (double) (j * 16 + 8);
+    protected void a(long i, int j, int k, byte[] abyte, double d0, double d1, double d2, float f, float f1, float f2, int l, int i1, double d3) {
+        double d4 = (double) (j * 16 + 8);
+        double d5 = (double) (k * 16 + 8);
         float f3 = 0.0F;
         float f4 = 0.0F;
-        Random random = new Random(this.b.nextLong());
+        Random random = new Random(i);
 
-        if (l <= 0) {
-            int i1 = this.a * 16 - 16;
+        if (i1 <= 0) {
+            int j1 = this.a * 16 - 16;
 
-            l = i1 - random.nextInt(i1 / 4);
+            i1 = j1 - random.nextInt(j1 / 4);
         }
 
         boolean flag = false;
 
-        if (k == -1) {
-            k = l / 2;
+        if (l == -1) {
+            l = i1 / 2;
             flag = true;
         }
 
-        int j1 = random.nextInt(l / 2) + l / 4;
+        int k1 = random.nextInt(i1 / 2) + i1 / 4;
 
-        for (boolean flag1 = random.nextInt(6) == 0; k < l; ++k) {
-            double d6 = 1.5D + (double) (MathHelper.sin((float) k * 3.1415927F / (float) l) * f * 1.0F);
+        for (boolean flag1 = random.nextInt(6) == 0; l < i1; ++l) {
+            double d6 = 1.5D + (double) (MathHelper.sin((float) l * 3.1415927F / (float) i1) * f * 1.0F);
             double d7 = d6 * d3;
             float f5 = MathHelper.cos(f2);
             float f6 = MathHelper.sin(f2);
@@ -53,16 +53,16 @@ public class WorldGenCavesHell extends WorldGenBase {
             f3 *= 0.75F;
             f4 += (random.nextFloat() - random.nextFloat()) * random.nextFloat() * 2.0F;
             f3 += (random.nextFloat() - random.nextFloat()) * random.nextFloat() * 4.0F;
-            if (!flag && k == j1 && f > 1.0F) {
-                this.a(i, j, abyte, d0, d1, d2, random.nextFloat() * 0.5F + 0.5F, f1 - 1.5707964F, f2 / 3.0F, k, l, 1.0D);
-                this.a(i, j, abyte, d0, d1, d2, random.nextFloat() * 0.5F + 0.5F, f1 + 1.5707964F, f2 / 3.0F, k, l, 1.0D);
+            if (!flag && l == k1 && f > 1.0F) {
+                this.a(random.nextLong(), j, k, abyte, d0, d1, d2, random.nextFloat() * 0.5F + 0.5F, f1 - 1.5707964F, f2 / 3.0F, l, i1, 1.0D);
+                this.a(random.nextLong(), j, k, abyte, d0, d1, d2, random.nextFloat() * 0.5F + 0.5F, f1 + 1.5707964F, f2 / 3.0F, l, i1, 1.0D);
                 return;
             }
 
             if (flag || random.nextInt(4) != 0) {
                 double d8 = d0 - d4;
                 double d9 = d2 - d5;
-                double d10 = (double) (l - k);
+                double d10 = (double) (i1 - l);
                 double d11 = (double) (f + 2.0F + 16.0F);
 
                 if (d8 * d8 + d9 * d9 - d10 * d10 > d11 * d11) {
@@ -70,53 +70,53 @@ public class WorldGenCavesHell extends WorldGenBase {
                 }
 
                 if (d0 >= d4 - 16.0D - d6 * 2.0D && d2 >= d5 - 16.0D - d6 * 2.0D && d0 <= d4 + 16.0D + d6 * 2.0D && d2 <= d5 + 16.0D + d6 * 2.0D) {
-                    int k1 = MathHelper.floor(d0 - d6) - i * 16 - 1;
-                    int l1 = MathHelper.floor(d0 + d6) - i * 16 + 1;
-                    int i2 = MathHelper.floor(d1 - d7) - 1;
-                    int j2 = MathHelper.floor(d1 + d7) + 1;
-                    int k2 = MathHelper.floor(d2 - d6) - j * 16 - 1;
-                    int l2 = MathHelper.floor(d2 + d6) - j * 16 + 1;
-
-                    if (k1 < 0) {
-                        k1 = 0;
+                    int l1 = MathHelper.floor(d0 - d6) - j * 16 - 1;
+                    int i2 = MathHelper.floor(d0 + d6) - j * 16 + 1;
+                    int j2 = MathHelper.floor(d1 - d7) - 1;
+                    int k2 = MathHelper.floor(d1 + d7) + 1;
+                    int l2 = MathHelper.floor(d2 - d6) - k * 16 - 1;
+                    int i3 = MathHelper.floor(d2 + d6) - k * 16 + 1;
+
+                    if (l1 < 0) {
+                        l1 = 0;
                     }
 
-                    if (l1 > 16) {
-                        l1 = 16;
+                    if (i2 > 16) {
+                        i2 = 16;
                     }
 
-                    if (i2 < 1) {
-                        i2 = 1;
+                    if (j2 < 1) {
+                        j2 = 1;
                     }
 
-                    if (j2 > 120) {
-                        j2 = 120;
+                    if (k2 > 120) {
+                        k2 = 120;
                     }
 
-                    if (k2 < 0) {
-                        k2 = 0;
+                    if (l2 < 0) {
+                        l2 = 0;
                     }
 
-                    if (l2 > 16) {
-                        l2 = 16;
+                    if (i3 > 16) {
+                        i3 = 16;
                     }
 
                     boolean flag2 = false;
 
-                    int i3;
                     int j3;
-
-                    for (j3 = k1; !flag2 && j3 < l1; ++j3) {
-                        for (int k3 = k2; !flag2 && k3 < l2; ++k3) {
-                            for (int l3 = j2 + 1; !flag2 && l3 >= i2 - 1; --l3) {
-                                i3 = (j3 * 16 + k3) * 128 + l3;
-                                if (l3 >= 0 && l3 < 128) {
-                                    if (abyte[i3] == Block.LAVA.id || abyte[i3] == Block.STATIONARY_LAVA.id) {
+                    int k3;
+
+                    for (j3 = l1; !flag2 && j3 < i2; ++j3) {
+                        for (int l3 = l2; !flag2 && l3 < i3; ++l3) {
+                            for (int i4 = k2 + 1; !flag2 && i4 >= j2 - 1; --i4) {
+                                k3 = (j3 * 16 + l3) * 128 + i4;
+                                if (i4 >= 0 && i4 < 128) {
+                                    if (abyte[k3] == Block.LAVA.id || abyte[k3] == Block.STATIONARY_LAVA.id) {
                                         flag2 = true;
                                     }
 
-                                    if (l3 != i2 - 1 && j3 != k1 && j3 != l1 - 1 && k3 != k2 && k3 != l2 - 1) {
-                                        l3 = i2;
+                                    if (i4 != j2 - 1 && j3 != l1 && j3 != i2 - 1 && l3 != l2 && l3 != i3 - 1) {
+                                        i4 = j2;
                                     }
                                 }
                             }
@@ -124,25 +124,25 @@ public class WorldGenCavesHell extends WorldGenBase {
                     }
 
                     if (!flag2) {
-                        for (j3 = k1; j3 < l1; ++j3) {
-                            double d12 = ((double) (j3 + i * 16) + 0.5D - d0) / d6;
+                        for (j3 = l1; j3 < i2; ++j3) {
+                            double d12 = ((double) (j3 + j * 16) + 0.5D - d0) / d6;
 
-                            for (i3 = k2; i3 < l2; ++i3) {
-                                double d13 = ((double) (i3 + j * 16) + 0.5D - d2) / d6;
-                                int i4 = (j3 * 16 + i3) * 128 + j2;
+                            for (k3 = l2; k3 < i3; ++k3) {
+                                double d13 = ((double) (k3 + k * 16) + 0.5D - d2) / d6;
+                                int j4 = (j3 * 16 + k3) * 128 + k2;
 
-                                for (int j4 = j2 - 1; j4 >= i2; --j4) {
-                                    double d14 = ((double) j4 + 0.5D - d1) / d7;
+                                for (int k4 = k2 - 1; k4 >= j2; --k4) {
+                                    double d14 = ((double) k4 + 0.5D - d1) / d7;
 
                                     if (d14 > -0.7D && d12 * d12 + d14 * d14 + d13 * d13 < 1.0D) {
-                                        byte b0 = abyte[i4];
+                                        byte b0 = abyte[j4];
 
                                         if (b0 == Block.NETHERRACK.id || b0 == Block.DIRT.id || b0 == Block.GRASS.id) {
-                                            abyte[i4] = 0;
+                                            abyte[j4] = 0;
                                         }
                                     }
 
-                                    --i4;
+                                    --j4;
                                 }
                             }
                         }
@@ -170,7 +170,7 @@ public class WorldGenCavesHell extends WorldGenBase {
             int k1 = 1;
 
             if (this.b.nextInt(4) == 0) {
-                this.a(k, l, abyte, d0, d1, d2);
+                this.a(this.b.nextLong(), k, l, abyte, d0, d1, d2);
                 k1 += this.b.nextInt(4);
             }
 
@@ -179,7 +179,7 @@ public class WorldGenCavesHell extends WorldGenBase {
                 float f1 = (this.b.nextFloat() - 0.5F) * 2.0F / 8.0F;
                 float f2 = this.b.nextFloat() * 2.0F + this.b.nextFloat();
 
-                this.a(k, l, abyte, d0, d1, d2, f2 * 2.0F, f, f1, 0, 0, 0.5D);
+                this.a(this.b.nextLong(), k, l, abyte, d0, d1, d2, f2 * 2.0F, f, f1, 0, 0, 0.5D);
             }
         }
     }
