@@ -4,11 +4,15 @@ import java.util.Random;
 
 public class BlockDoor extends Block {
 
+    private static final String[] a = new String[] { "doorWood_lower", "doorWood_upper", "doorIron_lower", "doorIron_upper"};
+    private final int b;
+
     protected BlockDoor(int i, Material material) {
         super(i, material);
-        this.textureId = 97;
         if (material == Material.ORE) {
-            ++this.textureId;
+            this.b = 2;
+        } else {
+            this.b = 0;
         }
 
         float f = 0.5F;
@@ -21,8 +25,8 @@ public class BlockDoor extends Block {
         return false;
     }
 
-    public boolean c(IBlockAccess iblockaccess, int i, int j, int k) {
-        int l = this.b_(iblockaccess, i, j, k);
+    public boolean b(IBlockAccess iblockaccess, int i, int j, int k) {
+        int l = this.c_(iblockaccess, i, j, k);
 
         return (l & 4) != 0;
     }
@@ -35,24 +39,24 @@ public class BlockDoor extends Block {
         return 7;
     }
 
-    public AxisAlignedBB e(World world, int i, int j, int k) {
+    public AxisAlignedBB b(World world, int i, int j, int k) {
         this.updateShape(world, i, j, k);
-        return super.e(world, i, j, k);
+        return super.b(world, i, j, k);
     }
 
     public void updateShape(IBlockAccess iblockaccess, int i, int j, int k) {
-        this.e(this.b_(iblockaccess, i, j, k));
+        this.d(this.c_(iblockaccess, i, j, k));
     }
 
     public int d(IBlockAccess iblockaccess, int i, int j, int k) {
-        return this.b_(iblockaccess, i, j, k) & 3;
+        return this.c_(iblockaccess, i, j, k) & 3;
     }
 
-    public boolean a_(IBlockAccess iblockaccess, int i, int j, int k) {
-        return (this.b_(iblockaccess, i, j, k) & 4) != 0;
+    public boolean b_(IBlockAccess iblockaccess, int i, int j, int k) {
+        return (this.c_(iblockaccess, i, j, k) & 4) != 0;
     }
 
-    private void e(int i) {
+    private void d(int i) {
         float f = 0.1875F;
 
         this.a(0.0F, 0.0F, 0.0F, 1.0F, 2.0F, 1.0F);
@@ -109,16 +113,16 @@ public class BlockDoor extends Block {
         if (this.material == Material.ORE) {
             return true;
         } else {
-            int i1 = this.b_(world, i, j, k);
+            int i1 = this.c_(world, i, j, k);
             int j1 = i1 & 7;
 
             j1 ^= 4;
             if ((i1 & 8) == 0) {
-                world.setData(i, j, k, j1);
-                world.e(i, j, k, i, j, k);
+                world.setData(i, j, k, j1, 2);
+                world.g(i, j, k, i, j, k);
             } else {
-                world.setData(i, j - 1, k, j1);
-                world.e(i, j - 1, k, i, j, k);
+                world.setData(i, j - 1, k, j1, 2);
+                world.g(i, j - 1, k, i, j, k);
             }
 
             world.a(entityhuman, 1003, i, j, k, 0);
@@ -127,7 +131,7 @@ public class BlockDoor extends Block {
     }
 
     public void setDoor(World world, int i, int j, int k, boolean flag) {
-        int l = this.b_(world, i, j, k);
+        int l = this.c_(world, i, j, k);
         boolean flag1 = (l & 4) != 0;
 
         if (flag1 != flag) {
@@ -135,11 +139,11 @@ public class BlockDoor extends Block {
 
             i1 ^= 4;
             if ((l & 8) == 0) {
-                world.setData(i, j, k, i1);
-                world.e(i, j, k, i, j, k);
+                world.setData(i, j, k, i1, 2);
+                world.g(i, j, k, i, j, k);
             } else {
-                world.setData(i, j - 1, k, i1);
-                world.e(i, j - 1, k, i, j, k);
+                world.setData(i, j - 1, k, i1, 2);
+                world.g(i, j - 1, k, i, j, k);
             }
 
             world.a((EntityHuman) null, 1003, i, j, k, 0);
@@ -153,15 +157,15 @@ public class BlockDoor extends Block {
             boolean flag = false;
 
             if (world.getTypeId(i, j + 1, k) != this.id) {
-                world.setTypeId(i, j, k, 0);
+                world.setAir(i, j, k);
                 flag = true;
             }
 
-            if (!world.v(i, j - 1, k)) {
-                world.setTypeId(i, j, k, 0);
+            if (!world.w(i, j - 1, k)) {
+                world.setAir(i, j, k);
                 flag = true;
                 if (world.getTypeId(i, j + 1, k) == this.id) {
-                    world.setTypeId(i, j + 1, k, 0);
+                    world.setAir(i, j + 1, k);
                 }
             }
 
@@ -178,7 +182,7 @@ public class BlockDoor extends Block {
             }
         } else {
             if (world.getTypeId(i, j - 1, k) != this.id) {
-                world.setTypeId(i, j, k, 0);
+                world.setAir(i, j, k);
             }
 
             if (l > 0 && l != this.id) {
@@ -197,14 +201,14 @@ public class BlockDoor extends Block {
     }
 
     public boolean canPlace(World world, int i, int j, int k) {
-        return j >= 255 ? false : world.v(i, j - 1, k) && super.canPlace(world, i, j, k) && super.canPlace(world, i, j + 1, k);
+        return j >= 255 ? false : world.w(i, j - 1, k) && super.canPlace(world, i, j, k) && super.canPlace(world, i, j + 1, k);
     }
 
-    public int q_() {
+    public int h() {
         return 1;
     }
 
-    public int b_(IBlockAccess iblockaccess, int i, int j, int k) {
+    public int c_(IBlockAccess iblockaccess, int i, int j, int k) {
         int l = iblockaccess.getData(i, j, k);
         boolean flag = (l & 8) != 0;
         int i1;
@@ -225,7 +229,7 @@ public class BlockDoor extends Block {
 
     public void a(World world, int i, int j, int k, int l, EntityHuman entityhuman) {
         if (entityhuman.abilities.canInstantlyBuild && (l & 8) != 0 && world.getTypeId(i, j - 1, k) == this.id) {
-            world.setTypeId(i, j - 1, k, 0);
+            world.setAir(i, j - 1, k);
         }
     }
 }
