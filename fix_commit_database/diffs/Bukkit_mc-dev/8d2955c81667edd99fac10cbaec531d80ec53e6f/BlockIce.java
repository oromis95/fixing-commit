@@ -4,8 +4,8 @@ import java.util.Random;
 
 public class BlockIce extends BlockHalfTransparant {
 
-    public BlockIce(int i, int j) {
-        super(i, j, Material.ICE, false);
+    public BlockIce(int i) {
+        super(i, "ice", Material.ICE, false);
         this.frictionFactor = 0.98F;
         this.b(true);
         this.a(CreativeModeTab.b);
@@ -14,15 +14,15 @@ public class BlockIce extends BlockHalfTransparant {
     public void a(World world, EntityHuman entityhuman, int i, int j, int k, int l) {
         entityhuman.a(StatisticList.C[this.id], 1);
         entityhuman.j(0.025F);
-        if (this.s_() && EnchantmentManager.hasSilkTouchEnchantment(entityhuman)) {
-            ItemStack itemstack = this.f_(l);
+        if (this.r_() && EnchantmentManager.hasSilkTouchEnchantment(entityhuman)) {
+            ItemStack itemstack = this.c_(l);
 
             if (itemstack != null) {
                 this.b(world, i, j, k, itemstack);
             }
         } else {
             if (world.worldProvider.e) {
-                world.setTypeId(i, j, k, 0);
+                world.setAir(i, j, k);
                 return;
             }
 
@@ -32,7 +32,7 @@ public class BlockIce extends BlockHalfTransparant {
             Material material = world.getMaterial(i, j - 1, k);
 
             if (material.isSolid() || material.isLiquid()) {
-                world.setTypeId(i, j, k, Block.WATER.id);
+                world.setTypeIdUpdate(i, j, k, Block.WATER.id);
             }
         }
     }
@@ -41,19 +41,19 @@ public class BlockIce extends BlockHalfTransparant {
         return 0;
     }
 
-    public void b(World world, int i, int j, int k, Random random) {
+    public void a(World world, int i, int j, int k, Random random) {
         if (world.b(EnumSkyBlock.BLOCK, i, j, k) > 11 - Block.lightBlock[this.id]) {
             if (world.worldProvider.e) {
-                world.setTypeId(i, j, k, 0);
+                world.setAir(i, j, k);
                 return;
             }
 
             this.c(world, i, j, k, world.getData(i, j, k), 0);
-            world.setTypeId(i, j, k, Block.STATIONARY_WATER.id);
+            world.setTypeIdUpdate(i, j, k, Block.STATIONARY_WATER.id);
         }
     }
 
-    public int q_() {
+    public int h() {
         return 0;
     }
 }
