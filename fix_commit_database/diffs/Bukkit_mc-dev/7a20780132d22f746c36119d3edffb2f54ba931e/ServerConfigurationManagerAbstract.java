@@ -340,12 +340,8 @@ public abstract class ServerConfigurationManagerAbstract {
     }
 
     public void sendAll(Packet packet) {
-        Iterator iterator = this.players.iterator();
-
-        while (iterator.hasNext()) {
-            EntityPlayer entityplayer = (EntityPlayer) iterator.next();
-
-            entityplayer.netServerHandler.sendPacket(packet);
+        for (int i = 0; i < this.players.size(); ++i) {
+            ((EntityPlayer) this.players.get(i)).netServerHandler.sendPacket(packet);
         }
     }
 
