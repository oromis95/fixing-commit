@@ -32,7 +32,7 @@ public class DedicatedServer extends MinecraftServer implements IMinecraftServer
         threadcommandreader.setDaemon(true);
         threadcommandreader.start();
         ConsoleLogManager.init();
-        log.info("Starting minecraft server version 1.3.1");
+        log.info("Starting minecraft server version 1.3.2");
         if (Runtime.getRuntime().maxMemory() / 1024L / 1024L < 512L) {
             log.warning("To start the server with more ram, launch it as \"java -Xmx1024M -Xms1024M -jar minecraft_server.jar\"");
         }
@@ -159,7 +159,7 @@ public class DedicatedServer extends MinecraftServer implements IMinecraftServer
 
     protected void a(CrashReport crashreport) {
         while (this.isRunning()) {
-            this.ah();
+            this.ai();
 
             try {
                 Thread.sleep(10L);
@@ -170,8 +170,9 @@ public class DedicatedServer extends MinecraftServer implements IMinecraftServer
     }
 
     protected CrashReport b(CrashReport crashreport) {
+        crashreport = super.b(crashreport);
         crashreport.a("Type", (Callable) (new CrashReportType(this)));
-        return super.b(crashreport);
+        return crashreport;
     }
 
     protected void o() {
@@ -180,7 +181,7 @@ public class DedicatedServer extends MinecraftServer implements IMinecraftServer
 
     protected void q() {
         super.q();
-        this.ah();
+        this.ai();
     }
 
     public boolean getAllowNether() {
@@ -192,8 +193,8 @@ public class DedicatedServer extends MinecraftServer implements IMinecraftServer
     }
 
     public void a(MojangStatisticsGenerator mojangstatisticsgenerator) {
-        mojangstatisticsgenerator.a("whitelist_enabled", Boolean.valueOf(this.ai().getHasWhitelist()));
-        mojangstatisticsgenerator.a("whitelist_count", Integer.valueOf(this.ai().getWhitelisted().size()));
+        mojangstatisticsgenerator.a("whitelist_enabled", Boolean.valueOf(this.aj().getHasWhitelist()));
+        mojangstatisticsgenerator.a("whitelist_count", Integer.valueOf(this.aj().getWhitelisted().size()));
         super.a(mojangstatisticsgenerator);
     }
 
@@ -205,7 +206,7 @@ public class DedicatedServer extends MinecraftServer implements IMinecraftServer
         this.l.add(new ServerCommand(s, icommandlistener));
     }
 
-    public void ah() {
+    public void ai() {
         while (!this.l.isEmpty()) {
             ServerCommand servercommand = (ServerCommand) this.l.remove(0);
 
@@ -217,7 +218,7 @@ public class DedicatedServer extends MinecraftServer implements IMinecraftServer
         return true;
     }
 
-    public ServerConfigurationManager ai() {
+    public ServerConfigurationManager aj() {
         return (ServerConfigurationManager) super.getServerConfigurationManager();
     }
 
@@ -251,7 +252,7 @@ public class DedicatedServer extends MinecraftServer implements IMinecraftServer
         return file1 != null ? file1.getAbsolutePath() : "No settings file";
     }
 
-    public void aj() {
+    public void ak() {
         ServerGUI.a(this);
         this.s = true;
     }
@@ -265,6 +266,6 @@ public class DedicatedServer extends MinecraftServer implements IMinecraftServer
     }
 
     public ServerConfigurationManagerAbstract getServerConfigurationManager() {
-        return this.ai();
+        return this.aj();
     }
 }
