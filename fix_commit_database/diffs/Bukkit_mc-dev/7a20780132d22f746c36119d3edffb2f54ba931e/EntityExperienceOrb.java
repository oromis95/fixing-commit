@@ -7,6 +7,8 @@ public class EntityExperienceOrb extends Entity {
     public int c;
     private int d = 5;
     private int value;
+    private EntityHuman targetPlayer;
+    private int targetTime;
 
     public EntityExperienceOrb(World world, double d0, double d1, double d2, int i) {
         super(world);
@@ -51,12 +53,19 @@ public class EntityExperienceOrb extends Entity {
 
         this.i(this.locX, (this.boundingBox.b + this.boundingBox.e) / 2.0D, this.locZ);
         double d0 = 8.0D;
-        EntityHuman entityhuman = this.world.findNearbyPlayer(this, d0);
 
-        if (entityhuman != null) {
-            double d1 = (entityhuman.locX - this.locX) / d0;
-            double d2 = (entityhuman.locY + (double) entityhuman.getHeadHeight() - this.locY) / d0;
-            double d3 = (entityhuman.locZ - this.locZ) / d0;
+        if (this.targetTime < this.a - 20 + this.id % 100) {
+            if (this.targetPlayer == null || this.targetPlayer.e(this) > d0 * d0) {
+                this.targetPlayer = this.world.findNearbyPlayer(this, d0);
+            }
+
+            this.targetTime = this.a;
+        }
+
+        if (this.targetPlayer != null) {
+            double d1 = (this.targetPlayer.locX - this.locX) / d0;
+            double d2 = (this.targetPlayer.locY + (double) this.targetPlayer.getHeadHeight() - this.locY) / d0;
+            double d3 = (this.targetPlayer.locZ - this.locZ) / d0;
             double d4 = Math.sqrt(d1 * d1 + d2 * d2 + d3 * d3);
             double d5 = 1.0D - d4;
 
