@@ -44,7 +44,7 @@ public class EntitySkeleton extends EntityMonster {
     }
 
     public void d() {
-        if (this.world.r() && !this.world.isStatic) {
+        if (this.world.s() && !this.world.isStatic) {
             float f = this.c(1.0F);
 
             if (f > 0.5F && this.world.j(MathHelper.floor(this.locX), MathHelper.floor(this.locY), MathHelper.floor(this.locZ)) && this.random.nextFloat() * 30.0F < (f - 0.4F) * 2.0F) {
