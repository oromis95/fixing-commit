@@ -241,7 +241,7 @@ public class Chunk {
         }
 
         if (i1 != l) {
-            this.world.g(i, k, i1, l);
+            this.world.g(i + this.x * 16, k + this.z * 16, i1, l);
             this.heightMap[k << 4 | i] = i1;
             int j1 = this.x * 16 + i;
             int k1 = this.z * 16 + k;
