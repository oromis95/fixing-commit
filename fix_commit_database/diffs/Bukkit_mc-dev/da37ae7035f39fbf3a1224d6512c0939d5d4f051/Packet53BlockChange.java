@@ -8,8 +8,8 @@ public class Packet53BlockChange extends Packet {
     public int a;
     public int b;
     public int c;
-    public int d;
-    public int e;
+    public int material;
+    public int data;
 
     public Packet53BlockChange() {
         this.k = true;
@@ -20,24 +20,24 @@ public class Packet53BlockChange extends Packet {
         this.a = i;
         this.b = j;
         this.c = k;
-        this.d = world.getTypeId(i, j, k);
-        this.e = world.getData(i, j, k);
+        this.material = world.getTypeId(i, j, k);
+        this.data = world.getData(i, j, k);
     }
 
     public void a(DataInputStream datainputstream) {
         this.a = datainputstream.readInt();
         this.b = datainputstream.read();
         this.c = datainputstream.readInt();
-        this.d = datainputstream.read();
-        this.e = datainputstream.read();
+        this.material = datainputstream.read();
+        this.data = datainputstream.read();
     }
 
     public void a(DataOutputStream dataoutputstream) {
         dataoutputstream.writeInt(this.a);
         dataoutputstream.write(this.b);
         dataoutputstream.writeInt(this.c);
-        dataoutputstream.write(this.d);
-        dataoutputstream.write(this.e);
+        dataoutputstream.write(this.material);
+        dataoutputstream.write(this.data);
     }
 
     public void a(NetHandler nethandler) {
