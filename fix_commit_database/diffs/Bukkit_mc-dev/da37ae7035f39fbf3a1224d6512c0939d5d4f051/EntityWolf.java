@@ -36,10 +36,10 @@ public class EntityWolf extends EntityAnimal {
         super.b(nbttagcompound);
         nbttagcompound.a("Angry", this.isAngry());
         nbttagcompound.a("Sitting", this.isSitting());
-        if (this.x() == null) {
+        if (this.getOwnerName() == null) {
             nbttagcompound.setString("Owner", "");
         } else {
-            nbttagcompound.setString("Owner", this.x());
+            nbttagcompound.setString("Owner", this.getOwnerName());
         }
     }
 
@@ -50,17 +50,17 @@ public class EntityWolf extends EntityAnimal {
         String s = nbttagcompound.getString("Owner");
 
         if (s.length() > 0) {
-            this.a(s);
-            this.d(true);
+            this.setOwnerName(s);
+            this.setTamed(true);
         }
     }
 
     protected boolean l_() {
-        return !this.A();
+        return !this.isTamed();
     }
 
     protected String g() {
-        return this.isAngry() ? "mob.wolf.growl" : (this.random.nextInt(3) == 0 ? (this.A() && this.datawatcher.b(18) < 10 ? "mob.wolf.whine" : "mob.wolf.panting") : "mob.wolf.bark");
+        return this.isAngry() ? "mob.wolf.growl" : (this.random.nextInt(3) == 0 ? (this.isTamed() && this.datawatcher.b(18) < 10 ? "mob.wolf.whine" : "mob.wolf.panting") : "mob.wolf.bark");
     }
 
     protected String h() {
@@ -81,8 +81,8 @@ public class EntityWolf extends EntityAnimal {
 
     protected void c_() {
         super.c_();
-        if (!this.e && !this.B() && this.A() && this.vehicle == null) {
-            EntityHuman entityhuman = this.world.a(this.x());
+        if (!this.e && !this.B() && this.isTamed() && this.vehicle == null) {
+            EntityHuman entityhuman = this.world.a(this.getOwnerName());
 
             if (entityhuman != null) {
                 float f = entityhuman.f(this);
@@ -93,11 +93,11 @@ public class EntityWolf extends EntityAnimal {
             } else if (!this.ac()) {
                 this.setSitting(true);
             }
-        } else if (this.target == null && !this.B() && !this.A() && this.world.random.nextInt(100) == 0) {
+        } else if (this.target == null && !this.B() && !this.isTamed() && this.world.random.nextInt(100) == 0) {
             List list = this.world.a(EntitySheep.class, AxisAlignedBB.b(this.locX, this.locY, this.locZ, this.locX + 1.0D, this.locY + 1.0D, this.locZ + 1.0D).b(16.0D, 4.0D, 16.0D));
 
             if (!list.isEmpty()) {
-                this.c((Entity) list.get(this.world.random.nextInt(list.size())));
+                this.setTarget((Entity) list.get(this.world.random.nextInt(list.size())));
             }
         }
 
@@ -106,7 +106,7 @@ public class EntityWolf extends EntityAnimal {
         }
 
         if (!this.world.isStatic) {
-            this.datawatcher.b(18, Integer.valueOf(this.health));
+            this.datawatcher.watch(18, Integer.valueOf(this.health));
         }
     }
 
@@ -121,9 +121,9 @@ public class EntityWolf extends EntityAnimal {
                 ItemStack itemstack = entityhuman.inventory.getItemInHand();
 
                 if (itemstack != null) {
-                    if (!this.A() && itemstack.id == Item.BONE.id) {
+                    if (!this.isTamed() && itemstack.id == Item.BONE.id) {
                         this.a = true;
-                    } else if (this.A() && Item.byId[itemstack.id] instanceof ItemFood) {
+                    } else if (this.isTamed() && Item.byId[itemstack.id] instanceof ItemFood) {
                         this.a = ((ItemFood) Item.byId[itemstack.id]).l();
                     }
                 }
@@ -209,7 +209,7 @@ public class EntityWolf extends EntityAnimal {
                 }
             }
         } else {
-            this.a(pathentity);
+            this.setPathEntity(pathentity);
         }
     }
 
@@ -226,7 +226,7 @@ public class EntityWolf extends EntityAnimal {
         if (!super.damageEntity((Entity) entity, i)) {
             return false;
         } else {
-            if (!this.A() && !this.isAngry()) {
+            if (!this.isTamed() && !this.isAngry()) {
                 if (entity instanceof EntityHuman) {
                     this.setAngry(true);
                     this.target = (Entity) entity;
@@ -244,7 +244,7 @@ public class EntityWolf extends EntityAnimal {
                         Entity entity1 = (Entity) iterator.next();
                         EntityWolf entitywolf = (EntityWolf) entity1;
 
-                        if (!entitywolf.A() && entitywolf.target == null) {
+                        if (!entitywolf.isTamed() && entitywolf.target == null) {
                             entitywolf.target = (Entity) entity;
                             if (entity instanceof EntityHuman) {
                                 entitywolf.setAngry(true);
@@ -253,7 +253,7 @@ public class EntityWolf extends EntityAnimal {
                     }
                 }
             } else if (entity != this && entity != null) {
-                if (this.A() && entity instanceof EntityHuman && ((EntityHuman) entity).name.equalsIgnoreCase(this.x())) {
+                if (this.isTamed() && entity instanceof EntityHuman && ((EntityHuman) entity).name.equalsIgnoreCase(this.getOwnerName())) {
                     return true;
                 }
 
@@ -265,7 +265,7 @@ public class EntityWolf extends EntityAnimal {
     }
 
     protected Entity findTarget() {
-        return this.isAngry() ? this.world.a(this, 16.0D) : null;
+        return this.isAngry() ? this.world.findNearbyPlayer(this, 16.0D) : null;
     }
 
     protected void a(Entity entity, float f) {
@@ -283,7 +283,7 @@ public class EntityWolf extends EntityAnimal {
             this.attackTicks = 20;
             byte b0 = 2;
 
-            if (this.A()) {
+            if (this.isTamed()) {
                 b0 = 4;
             }
 
@@ -294,7 +294,7 @@ public class EntityWolf extends EntityAnimal {
     public boolean a(EntityHuman entityhuman) {
         ItemStack itemstack = entityhuman.inventory.getItemInHand();
 
-        if (!this.A()) {
+        if (!this.isTamed()) {
             if (itemstack != null && itemstack.id == Item.BONE.id && !this.isAngry()) {
                 --itemstack.count;
                 if (itemstack.count <= 0) {
@@ -303,11 +303,11 @@ public class EntityWolf extends EntityAnimal {
 
                 if (!this.world.isStatic) {
                     if (this.random.nextInt(3) == 0) {
-                        this.d(true);
-                        this.a((PathEntity) null);
+                        this.setTamed(true);
+                        this.setPathEntity((PathEntity) null);
                         this.setSitting(true);
                         this.health = 20;
-                        this.a(entityhuman.name);
+                        this.setOwnerName(entityhuman.name);
                         this.a(true);
                         this.world.a(this, (byte) 7);
                     } else {
@@ -333,11 +333,11 @@ public class EntityWolf extends EntityAnimal {
                 }
             }
 
-            if (entityhuman.name.equalsIgnoreCase(this.x())) {
+            if (entityhuman.name.equalsIgnoreCase(this.getOwnerName())) {
                 if (!this.world.isStatic) {
                     this.setSitting(!this.isSitting());
                     this.aC = false;
-                    this.a((PathEntity) null);
+                    this.setPathEntity((PathEntity) null);
                 }
 
                 return true;
@@ -367,12 +367,12 @@ public class EntityWolf extends EntityAnimal {
         return 8;
     }
 
-    public String x() {
+    public String getOwnerName() {
         return this.datawatcher.c(17);
     }
 
-    public void a(String s) {
-        this.datawatcher.b(17, s);
+    public void setOwnerName(String s) {
+        this.datawatcher.watch(17, s);
     }
 
     public boolean isSitting() {
@@ -383,9 +383,9 @@ public class EntityWolf extends EntityAnimal {
         byte b0 = this.datawatcher.a(16);
 
         if (flag) {
-            this.datawatcher.b(16, Byte.valueOf((byte) (b0 | 1)));
+            this.datawatcher.watch(16, Byte.valueOf((byte) (b0 | 1)));
         } else {
-            this.datawatcher.b(16, Byte.valueOf((byte) (b0 & -2)));
+            this.datawatcher.watch(16, Byte.valueOf((byte) (b0 & -2)));
         }
     }
 
@@ -397,23 +397,23 @@ public class EntityWolf extends EntityAnimal {
         byte b0 = this.datawatcher.a(16);
 
         if (flag) {
-            this.datawatcher.b(16, Byte.valueOf((byte) (b0 | 2)));
+            this.datawatcher.watch(16, Byte.valueOf((byte) (b0 | 2)));
         } else {
-            this.datawatcher.b(16, Byte.valueOf((byte) (b0 & -3)));
+            this.datawatcher.watch(16, Byte.valueOf((byte) (b0 & -3)));
         }
     }
 
-    public boolean A() {
+    public boolean isTamed() {
         return (this.datawatcher.a(16) & 4) != 0;
     }
 
-    public void d(boolean flag) {
+    public void setTamed(boolean flag) {
         byte b0 = this.datawatcher.a(16);
 
         if (flag) {
-            this.datawatcher.b(16, Byte.valueOf((byte) (b0 | 4)));
+            this.datawatcher.watch(16, Byte.valueOf((byte) (b0 | 4)));
         } else {
-            this.datawatcher.b(16, Byte.valueOf((byte) (b0 & -5)));
+            this.datawatcher.watch(16, Byte.valueOf((byte) (b0 & -5)));
         }
     }
 }
