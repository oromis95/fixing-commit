@@ -31,9 +31,9 @@ public class InventoryPlayer implements IInventory {
         return -1;
     }
 
-    private int d(ItemStack itemstack) {
+    private int firstPartial(ItemStack itemstack) {
         for (int i = 0; i < this.items.length; ++i) {
-            if (this.items[i] != null && this.items[i].id == itemstack.id && this.items[i].c() && this.items[i].count < this.items[i].b() && this.items[i].count < this.getMaxStackSize() && (!this.items[i].e() || this.items[i].getData() == itemstack.getData())) {
+            if (this.items[i] != null && this.items[i].id == itemstack.id && this.items[i].isStackable() && this.items[i].count < this.items[i].getMaxStackSize() && this.items[i].count < this.getMaxStackSize() && (!this.items[i].usesData() || this.items[i].getData() == itemstack.getData())) {
                 return i;
             }
         }
@@ -54,7 +54,7 @@ public class InventoryPlayer implements IInventory {
     private int e(ItemStack itemstack) {
         int i = itemstack.id;
         int j = itemstack.count;
-        int k = this.d(itemstack);
+        int k = this.firstPartial(itemstack);
 
         if (k < 0) {
             k = this.k();
@@ -69,8 +69,8 @@ public class InventoryPlayer implements IInventory {
 
             int l = j;
 
-            if (j > this.items[k].b() - this.items[k].count) {
-                l = this.items[k].b() - this.items[k].count;
+            if (j > this.items[k].getMaxStackSize() - this.items[k].count) {
+                l = this.items[k].getMaxStackSize() - this.items[k].count;
             }
 
             if (l > this.getMaxStackSize() - this.items[k].count) {
@@ -110,7 +110,7 @@ public class InventoryPlayer implements IInventory {
         }
     }
 
-    public boolean canHold(ItemStack itemstack) {
+    public boolean pickup(ItemStack itemstack) {
         int i;
 
         if (itemstack.f()) {
@@ -133,7 +133,7 @@ public class InventoryPlayer implements IInventory {
         }
     }
 
-    public ItemStack a(int i, int j) {
+    public ItemStack splitStack(int i, int j) {
         ItemStack[] aitemstack = this.items;
 
         if (i >= this.items.length) {
