@@ -22,8 +22,8 @@ public class NetworkManager {
     private DataOutputStream output;
     private boolean l = true;
     private List m = Collections.synchronizedList(new ArrayList());
-    private List n = Collections.synchronizedList(new ArrayList());
-    private List o = Collections.synchronizedList(new ArrayList());
+    private List highPriorityQueue = Collections.synchronizedList(new ArrayList());
+    private List lowPriorityQueue = Collections.synchronizedList(new ArrayList());
     private NetHandler p;
     private boolean q = false;
     private Thread r;
@@ -36,7 +36,7 @@ public class NetworkManager {
     public static int[] d = new int[256];
     public static int[] e = new int[256];
     public int f = 0;
-    private int y = 50;
+    private int lowPriorityQueueDelay = 50;
 
     public NetworkManager(Socket socket, String s, NetHandler nethandler) {
         this.socket = socket;
@@ -62,16 +62,16 @@ public class NetworkManager {
         this.p = nethandler;
     }
 
-    public void a(Packet packet) {
+    public void queue(Packet packet) {
         if (!this.q) {
             Object object = this.g;
 
             synchronized (this.g) {
                 this.x += packet.a() + 1;
                 if (packet.k) {
-                    this.o.add(packet);
+                    this.lowPriorityQueue.add(packet);
                 } else {
-                    this.n.add(packet);
+                    this.highPriorityQueue.add(packet);
                 }
             }
         }
@@ -86,10 +86,10 @@ public class NetworkManager {
             int i;
             int[] aint;
 
-            if (!this.n.isEmpty() && (this.f == 0 || System.currentTimeMillis() - ((Packet) this.n.get(0)).timestamp >= (long) this.f)) {
+            if (!this.highPriorityQueue.isEmpty() && (this.f == 0 || System.currentTimeMillis() - ((Packet) this.highPriorityQueue.get(0)).timestamp >= (long) this.f)) {
                 object = this.g;
                 synchronized (this.g) {
-                    packet = (Packet) this.n.remove(0);
+                    packet = (Packet) this.highPriorityQueue.remove(0);
                     this.x -= packet.a() + 1;
                 }
 
@@ -100,10 +100,10 @@ public class NetworkManager {
                 flag = true;
             }
 
-            if (this.y-- <= 0 && !this.o.isEmpty() && (this.f == 0 || System.currentTimeMillis() - ((Packet) this.o.get(0)).timestamp >= (long) this.f)) {
+            if (this.lowPriorityQueueDelay-- <= 0 && !this.lowPriorityQueue.isEmpty() && (this.f == 0 || System.currentTimeMillis() - ((Packet) this.lowPriorityQueue.get(0)).timestamp >= (long) this.f)) {
                 object = this.g;
                 synchronized (this.g) {
-                    packet = (Packet) this.o.remove(0);
+                    packet = (Packet) this.lowPriorityQueue.remove(0);
                     this.x -= packet.a() + 1;
                 }
 
@@ -111,7 +111,7 @@ public class NetworkManager {
                 aint = e;
                 i = packet.b();
                 aint[i] += packet.a() + 1;
-                this.y = 0;
+                this.lowPriorityQueueDelay = 0;
                 flag = true;
             }
 
@@ -232,7 +232,7 @@ public class NetworkManager {
     }
 
     public int e() {
-        return this.o.size();
+        return this.lowPriorityQueue.size();
     }
 
     static boolean a(NetworkManager networkmanager) {
