@@ -8,24 +8,24 @@ public class FurnaceRecipes {
     private static final FurnaceRecipes a = new FurnaceRecipes();
     private Map b = new HashMap();
 
-    public static final FurnaceRecipes a() {
+    public static final FurnaceRecipes getInstance() {
         return a;
     }
 
     private FurnaceRecipes() {
-        this.a(Block.IRON_ORE.id, new ItemStack(Item.IRON_INGOT));
-        this.a(Block.GOLD_ORE.id, new ItemStack(Item.GOLD_INGOT));
-        this.a(Block.DIAMOND_ORE.id, new ItemStack(Item.DIAMOND));
-        this.a(Block.SAND.id, new ItemStack(Block.GLASS));
-        this.a(Item.PORK.id, new ItemStack(Item.GRILLED_PORK));
-        this.a(Item.RAW_FISH.id, new ItemStack(Item.COOKED_FISH));
-        this.a(Block.COBBLESTONE.id, new ItemStack(Block.STONE));
-        this.a(Item.CLAY_BALL.id, new ItemStack(Item.CLAY_BRICK));
-        this.a(Block.CACTUS.id, new ItemStack(Item.INK_SACK, 1, 2));
-        this.a(Block.LOG.id, new ItemStack(Item.COAL, 1, 1));
+        this.registerRecipe(Block.IRON_ORE.id, new ItemStack(Item.IRON_INGOT));
+        this.registerRecipe(Block.GOLD_ORE.id, new ItemStack(Item.GOLD_INGOT));
+        this.registerRecipe(Block.DIAMOND_ORE.id, new ItemStack(Item.DIAMOND));
+        this.registerRecipe(Block.SAND.id, new ItemStack(Block.GLASS));
+        this.registerRecipe(Item.PORK.id, new ItemStack(Item.GRILLED_PORK));
+        this.registerRecipe(Item.RAW_FISH.id, new ItemStack(Item.COOKED_FISH));
+        this.registerRecipe(Block.COBBLESTONE.id, new ItemStack(Block.STONE));
+        this.registerRecipe(Item.CLAY_BALL.id, new ItemStack(Item.CLAY_BRICK));
+        this.registerRecipe(Block.CACTUS.id, new ItemStack(Item.INK_SACK, 1, 2));
+        this.registerRecipe(Block.LOG.id, new ItemStack(Item.COAL, 1, 1));
     }
 
-    public void a(int i, ItemStack itemstack) {
+    public void registerRecipe(int i, ItemStack itemstack) {
         this.b.put(Integer.valueOf(i), itemstack);
     }
 
