@@ -17,7 +17,7 @@ public class TileEntityDispenser extends TileEntity implements IInventory {
         return this.items[i];
     }
 
-    public ItemStack a(int i, int j) {
+    public ItemStack splitStack(int i, int j) {
         if (this.items[i] != null) {
             ItemStack itemstack;
 
@@ -51,7 +51,7 @@ public class TileEntityDispenser extends TileEntity implements IInventory {
         }
 
         if (i >= 0) {
-            return this.a(i, 1);
+            return this.splitStack(i, 1);
         } else {
             return null;
         }
@@ -108,6 +108,6 @@ public class TileEntityDispenser extends TileEntity implements IInventory {
     }
 
     public boolean a_(EntityHuman entityhuman) {
-        return this.world.getTileEntity(this.e, this.f, this.g) != this ? false : entityhuman.d((double) this.e + 0.5D, (double) this.f + 0.5D, (double) this.g + 0.5D) <= 64.0D;
+        return this.world.getTileEntity(this.x, this.y, this.z) != this ? false : entityhuman.d((double) this.x + 0.5D, (double) this.y + 0.5D, (double) this.z + 0.5D) <= 64.0D;
     }
 }
