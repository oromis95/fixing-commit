@@ -4,6 +4,6 @@ public class SecondaryWorldServer extends WorldServer {
 
     public SecondaryWorldServer(MinecraftServer minecraftserver, IDataManager idatamanager, String s, int i, long j, WorldServer worldserver) {
         super(minecraftserver, idatamanager, s, i, j);
-        this.z = worldserver.z;
+        this.worldMaps = worldserver.worldMaps;
     }
 }
