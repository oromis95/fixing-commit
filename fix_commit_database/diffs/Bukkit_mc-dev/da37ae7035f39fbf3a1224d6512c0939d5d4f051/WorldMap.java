@@ -9,7 +9,7 @@ public class WorldMap extends WorldMapBase {
 
     public int b;
     public int c;
-    public byte d;
+    public byte map;
     public byte e;
     public byte[] f = new byte[16384];
     public int g;
@@ -22,7 +22,7 @@ public class WorldMap extends WorldMapBase {
     }
 
     public void a(NBTTagCompound nbttagcompound) {
-        this.d = nbttagcompound.c("dimension");
+        this.map = nbttagcompound.c("dimension");
         this.b = nbttagcompound.e("xCenter");
         this.c = nbttagcompound.e("zCenter");
         this.e = nbttagcompound.c("scale");
@@ -63,7 +63,7 @@ public class WorldMap extends WorldMapBase {
     }
 
     public void b(NBTTagCompound nbttagcompound) {
-        nbttagcompound.a("dimension", this.d);
+        nbttagcompound.a("dimension", this.map);
         nbttagcompound.a("xCenter", this.b);
         nbttagcompound.a("zCenter", this.c);
         nbttagcompound.a("scale", this.e);
@@ -85,9 +85,9 @@ public class WorldMap extends WorldMapBase {
         for (int i = 0; i < this.h.size(); ++i) {
             WorldMapHumanTracker worldmaphumantracker1 = (WorldMapHumanTracker) this.h.get(i);
 
-            if (!worldmaphumantracker1.a.dead && worldmaphumantracker1.a.inventory.c(itemstack)) {
-                float f = (float) (worldmaphumantracker1.a.locX - (double) this.b) / (float) (1 << this.e);
-                float f1 = (float) (worldmaphumantracker1.a.locZ - (double) this.c) / (float) (1 << this.e);
+            if (!worldmaphumantracker1.trackee.dead && worldmaphumantracker1.trackee.inventory.c(itemstack)) {
+                float f = (float) (worldmaphumantracker1.trackee.locX - (double) this.b) / (float) (1 << this.e);
+                float f1 = (float) (worldmaphumantracker1.trackee.locZ - (double) this.c) / (float) (1 << this.e);
                 byte b0 = 64;
                 byte b1 = 64;
 
@@ -97,18 +97,18 @@ public class WorldMap extends WorldMapBase {
                     byte b4 = (byte) ((int) ((double) (f1 * 2.0F) + 0.5D));
                     byte b5 = (byte) ((int) ((double) (entityhuman.yaw * 16.0F / 360.0F) + 0.5D));
 
-                    if (this.d < 0) {
+                    if (this.map < 0) {
                         int j = this.g / 10;
 
                         b5 = (byte) (j * j * 34187121 + j * 121 >> 15 & 15);
                     }
 
-                    if (worldmaphumantracker1.a.dimension == this.d) {
+                    if (worldmaphumantracker1.trackee.dimension == this.map) {
                         this.i.add(new WorldMapOrienter(this, b2, b3, b4, b5));
                     }
                 }
             } else {
-                this.j.remove(worldmaphumantracker1.a);
+                this.j.remove(worldmaphumantracker1.trackee);
                 this.h.remove(worldmaphumantracker1);
             }
         }
