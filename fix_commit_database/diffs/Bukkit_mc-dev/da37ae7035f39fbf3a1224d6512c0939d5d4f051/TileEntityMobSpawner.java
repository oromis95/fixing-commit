@@ -16,15 +16,15 @@ public class TileEntityMobSpawner extends TileEntity {
     }
 
     public boolean a() {
-        return this.world.a((double) this.e + 0.5D, (double) this.f + 0.5D, (double) this.g + 0.5D, 16.0D) != null;
+        return this.world.a((double) this.x + 0.5D, (double) this.y + 0.5D, (double) this.z + 0.5D, 16.0D) != null;
     }
 
     public void g_() {
         this.c = this.b;
         if (this.a()) {
-            double d0 = (double) ((float) this.e + this.world.random.nextFloat());
-            double d1 = (double) ((float) this.f + this.world.random.nextFloat());
-            double d2 = (double) ((float) this.g + this.world.random.nextFloat());
+            double d0 = (double) ((float) this.x + this.world.random.nextFloat());
+            double d1 = (double) ((float) this.y + this.world.random.nextFloat());
+            double d2 = (double) ((float) this.z + this.world.random.nextFloat());
 
             this.world.a("smoke", d0, d1, d2, 0.0D, 0.0D, 0.0D);
             this.world.a("flame", d0, d1, d2, 0.0D, 0.0D, 0.0D);
@@ -49,7 +49,7 @@ public class TileEntityMobSpawner extends TileEntity {
                         return;
                     }
 
-                    int j = this.world.a(entityliving.getClass(), AxisAlignedBB.b((double) this.e, (double) this.f, (double) this.g, (double) (this.e + 1), (double) (this.f + 1), (double) (this.g + 1)).b(8.0D, 4.0D, 8.0D)).size();
+                    int j = this.world.a(entityliving.getClass(), AxisAlignedBB.b((double) this.x, (double) this.y, (double) this.z, (double) (this.x + 1), (double) (this.y + 1), (double) (this.z + 1)).b(8.0D, 4.0D, 8.0D)).size();
 
                     if (j >= 6) {
                         this.c();
@@ -57,18 +57,18 @@ public class TileEntityMobSpawner extends TileEntity {
                     }
 
                     if (entityliving != null) {
-                        double d3 = (double) this.e + (this.world.random.nextDouble() - this.world.random.nextDouble()) * 4.0D;
-                        double d4 = (double) (this.f + this.world.random.nextInt(3) - 1);
-                        double d5 = (double) this.g + (this.world.random.nextDouble() - this.world.random.nextDouble()) * 4.0D;
+                        double d3 = (double) this.x + (this.world.random.nextDouble() - this.world.random.nextDouble()) * 4.0D;
+                        double d4 = (double) (this.y + this.world.random.nextInt(3) - 1);
+                        double d5 = (double) this.z + (this.world.random.nextDouble() - this.world.random.nextDouble()) * 4.0D;
 
                         entityliving.setPositionRotation(d3, d4, d5, this.world.random.nextFloat() * 360.0F, 0.0F);
                         if (entityliving.d()) {
                             this.world.addEntity(entityliving);
 
                             for (int k = 0; k < 20; ++k) {
-                                d0 = (double) this.e + 0.5D + ((double) this.world.random.nextFloat() - 0.5D) * 2.0D;
-                                d1 = (double) this.f + 0.5D + ((double) this.world.random.nextFloat() - 0.5D) * 2.0D;
-                                d2 = (double) this.g + 0.5D + ((double) this.world.random.nextFloat() - 0.5D) * 2.0D;
+                                d0 = (double) this.x + 0.5D + ((double) this.world.random.nextFloat() - 0.5D) * 2.0D;
+                                d1 = (double) this.y + 0.5D + ((double) this.world.random.nextFloat() - 0.5D) * 2.0D;
+                                d2 = (double) this.z + 0.5D + ((double) this.world.random.nextFloat() - 0.5D) * 2.0D;
                                 this.world.a("smoke", d0, d1, d2, 0.0D, 0.0D, 0.0D);
                                 this.world.a("flame", d0, d1, d2, 0.0D, 0.0D, 0.0D);
                             }
