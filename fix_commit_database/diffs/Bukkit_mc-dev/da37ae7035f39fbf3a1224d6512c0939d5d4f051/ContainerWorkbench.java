@@ -2,8 +2,8 @@ package net.minecraft.server;
 
 public class ContainerWorkbench extends Container {
 
-    public InventoryCrafting a = new InventoryCrafting(this, 3, 3);
-    public IInventory b = new InventoryCraftResult();
+    public InventoryCrafting craftInventory = new InventoryCrafting(this, 3, 3);
+    public IInventory resultInventory = new InventoryCraftResult();
     private World c;
     private int h;
     private int i;
@@ -14,14 +14,14 @@ public class ContainerWorkbench extends Container {
         this.h = i;
         this.i = j;
         this.j = k;
-        this.a((Slot) (new SlotResult(inventoryplayer.d, this.a, this.b, 0, 124, 35)));
+        this.a((Slot) (new SlotResult(inventoryplayer.d, this.craftInventory, this.resultInventory, 0, 124, 35)));
 
         int l;
         int i1;
 
         for (l = 0; l < 3; ++l) {
             for (i1 = 0; i1 < 3; ++i1) {
-                this.a(new Slot(this.a, i1 + l * 3, 30 + i1 * 18, 17 + l * 18));
+                this.a(new Slot(this.craftInventory, i1 + l * 3, 30 + i1 * 18, 17 + l * 18));
             }
         }
 
@@ -35,18 +35,18 @@ public class ContainerWorkbench extends Container {
             this.a(new Slot(inventoryplayer, l, 8 + l * 18, 142));
         }
 
-        this.a((IInventory) this.a);
+        this.a((IInventory) this.craftInventory);
     }
 
     public void a(IInventory iinventory) {
-        this.b.setItem(0, CraftingManager.a().a(this.a));
+        this.resultInventory.setItem(0, CraftingManager.getInstance().craft(this.craftInventory));
     }
 
     public void a(EntityHuman entityhuman) {
         super.a(entityhuman);
 
         for (int i = 0; i < 9; ++i) {
-            ItemStack itemstack = this.a.getItem(i);
+            ItemStack itemstack = this.craftInventory.getItem(i);
 
             if (itemstack != null) {
                 entityhuman.b(itemstack);
@@ -65,7 +65,7 @@ public class ContainerWorkbench extends Container {
         if (slot != null && slot.b()) {
             ItemStack itemstack1 = slot.getItem();
 
-            itemstack = itemstack1.j();
+            itemstack = itemstack1.cloneItemStack();
             if (i == 0) {
                 this.a(itemstack1, 10, 46, true);
             } else if (i >= 10 && i < 37) {
