@@ -33,8 +33,8 @@ public class EntityItem extends Entity {
 
     protected void b() {}
 
-    public void p_() {
-        super.p_();
+    public void o_() {
+        super.o_();
         if (this.pickupDelay > 0) {
             --this.pickupDelay;
         }
@@ -50,7 +50,7 @@ public class EntityItem extends Entity {
             this.world.makeSound(this, "random.fizz", 0.4F, 2.0F + this.random.nextFloat() * 0.4F);
         }
 
-        this.g(this.locX, this.locY, this.locZ);
+        this.g(this.locX, (this.boundingBox.b + this.boundingBox.e) / 2.0D, this.locZ);
         this.move(this.motX, this.motY, this.motZ);
         float f = 0.98F;
 
@@ -86,7 +86,7 @@ public class EntityItem extends Entity {
     }
 
     public boolean damageEntity(Entity entity, int i) {
-        this.ac();
+        this.ae();
         this.f -= i;
         if (this.f <= 0) {
             this.die();
