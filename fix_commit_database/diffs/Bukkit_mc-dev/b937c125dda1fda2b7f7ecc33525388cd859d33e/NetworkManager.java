@@ -1,6 +1,5 @@
 package net.minecraft.server;
 
-import java.io.BufferedInputStream;
 import java.io.BufferedOutputStream;
 import java.io.DataInputStream;
 import java.io.DataOutputStream;
@@ -34,11 +33,10 @@ public class NetworkManager {
     private Object[] v;
     private int w = 0;
     private int x = 0;
-    private transient boolean y = false;
     public static int[] d = new int[256];
     public static int[] e = new int[256];
     public int f = 0;
-    private int z = 50;
+    private int y = 50;
 
     public NetworkManager(Socket socket, String s, NetHandler nethandler) {
         this.socket = socket;
@@ -52,8 +50,8 @@ public class NetworkManager {
             System.err.println(socketexception.getMessage());
         }
 
-        this.input = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
-        this.output = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
+        this.input = new DataInputStream(socket.getInputStream());
+        this.output = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream(), 5120));
         this.s = new NetworkReaderThread(this, s + " read thread");
         this.r = new NetworkWriterThread(this, s + " write thread");
         this.s.start();
@@ -98,11 +96,11 @@ public class NetworkManager {
                 Packet.a(packet, this.output);
                 aint = e;
                 i = packet.b();
-                aint[i] += packet.a();
+                aint[i] += packet.a() + 1;
                 flag = true;
             }
 
-            if (this.z-- <= 0 && !this.o.isEmpty() && (this.f == 0 || System.currentTimeMillis() - ((Packet) this.o.get(0)).timestamp >= (long) this.f)) {
+            if (this.y-- <= 0 && !this.o.isEmpty() && (this.f == 0 || System.currentTimeMillis() - ((Packet) this.o.get(0)).timestamp >= (long) this.f)) {
                 object = this.g;
                 synchronized (this.g) {
                     packet = (Packet) this.o.remove(0);
@@ -112,21 +110,24 @@ public class NetworkManager {
                 Packet.a(packet, this.output);
                 aint = e;
                 i = packet.b();
-                aint[i] += packet.a();
-                this.z = 50;
+                aint[i] += packet.a() + 1;
+                this.y = 0;
                 flag = true;
             }
+
+            return flag;
         } catch (Exception exception) {
             if (!this.t) {
                 this.a(exception);
             }
-        }
 
-        return flag;
+            return false;
+        }
     }
 
     public void a() {
-        this.y = true;
+        this.s.interrupt();
+        this.r.interrupt();
     }
 
     private boolean g() {
@@ -139,19 +140,21 @@ public class NetworkManager {
                 int[] aint = d;
                 int i = packet.b();
 
-                aint[i] += packet.a();
+                aint[i] += packet.a() + 1;
                 this.m.add(packet);
                 flag = true;
             } else {
                 this.a("disconnect.endOfStream", new Object[0]);
             }
+
+            return flag;
         } catch (Exception exception) {
             if (!this.t) {
                 this.a(exception);
             }
-        }
 
-        return flag;
+            return false;
+        }
     }
 
     private void a(Exception exception) {
@@ -211,6 +214,7 @@ public class NetworkManager {
             packet.a(this.p);
         }
 
+        this.a();
         if (this.t && this.m.isEmpty()) {
             this.p.a(this.u, this.v);
         }
@@ -247,16 +251,16 @@ public class NetworkManager {
         return networkmanager.f();
     }
 
-    static boolean e(NetworkManager networkmanager) {
-        return networkmanager.y;
+    static DataOutputStream e(NetworkManager networkmanager) {
+        return networkmanager.output;
     }
 
-    static boolean a(NetworkManager networkmanager, boolean flag) {
-        return networkmanager.y = flag;
+    static boolean f(NetworkManager networkmanager) {
+        return networkmanager.t;
     }
 
-    static DataOutputStream f(NetworkManager networkmanager) {
-        return networkmanager.output;
+    static void a(NetworkManager networkmanager, Exception exception) {
+        networkmanager.a(exception);
     }
 
     static Thread g(NetworkManager networkmanager) {
