@@ -29,16 +29,18 @@ public class EntityCreeper extends EntityMonster {
     }
 
     protected void b(Entity entity, float f) {
-        if (this.fuseTicks > 0) {
-            this.e(-1);
-            --this.fuseTicks;
-            if (this.fuseTicks < 0) {
-                this.fuseTicks = 0;
+        if (!this.world.isStatic) {
+            if (this.fuseTicks > 0) {
+                this.e(-1);
+                --this.fuseTicks;
+                if (this.fuseTicks < 0) {
+                    this.fuseTicks = 0;
+                }
             }
         }
     }
 
-    public void p_() {
+    public void o_() {
         this.b = this.fuseTicks;
         if (this.world.isStatic) {
             int i = this.x();
@@ -57,7 +59,7 @@ public class EntityCreeper extends EntityMonster {
             }
         }
 
-        super.p_();
+        super.o_();
         if (this.target == null && this.fuseTicks > 0) {
             this.e(-1);
             --this.fuseTicks;
@@ -83,32 +85,34 @@ public class EntityCreeper extends EntityMonster {
     }
 
     protected void a(Entity entity, float f) {
-        int i = this.x();
+        if (!this.world.isStatic) {
+            int i = this.x();
 
-        if ((i > 0 || f >= 3.0F) && (i <= 0 || f >= 7.0F)) {
-            this.e(-1);
-            --this.fuseTicks;
-            if (this.fuseTicks < 0) {
-                this.fuseTicks = 0;
-            }
-        } else {
-            if (this.fuseTicks == 0) {
-                this.world.makeSound(this, "random.fuse", 1.0F, 0.5F);
-            }
+            if ((i > 0 || f >= 3.0F) && (i <= 0 || f >= 7.0F)) {
+                this.e(-1);
+                --this.fuseTicks;
+                if (this.fuseTicks < 0) {
+                    this.fuseTicks = 0;
+                }
+            } else {
+                if (this.fuseTicks == 0) {
+                    this.world.makeSound(this, "random.fuse", 1.0F, 0.5F);
+                }
 
-            this.e(1);
-            ++this.fuseTicks;
-            if (this.fuseTicks >= 30) {
-                if (this.t()) {
-                    this.world.a(this, this.locX, this.locY, this.locZ, 6.0F);
-                } else {
-                    this.world.a(this, this.locX, this.locY, this.locZ, 3.0F);
+                this.e(1);
+                ++this.fuseTicks;
+                if (this.fuseTicks >= 30) {
+                    if (this.t()) {
+                        this.world.a(this, this.locX, this.locY, this.locZ, 6.0F);
+                    } else {
+                        this.world.a(this, this.locX, this.locY, this.locZ, 3.0F);
+                    }
+
+                    this.die();
                 }
 
-                this.die();
+                this.e = true;
             }
-
-            this.e = true;
         }
     }
 
