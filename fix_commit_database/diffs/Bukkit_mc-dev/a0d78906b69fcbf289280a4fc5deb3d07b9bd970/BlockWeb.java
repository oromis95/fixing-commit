@@ -1,9 +1,11 @@
 package net.minecraft.server;
 
+import java.util.Random;
+
 public class BlockWeb extends Block {
 
     public BlockWeb(int i, int j) {
-        super(i, j, Material.CLOTH);
+        super(i, j, Material.WEB);
     }
 
     public void a(World world, int i, int j, int k, Entity entity) {
@@ -17,4 +19,12 @@ public class BlockWeb extends Block {
     public AxisAlignedBB d(World world, int i, int j, int k) {
         return null;
     }
+
+    public boolean b() {
+        return false;
+    }
+
+    public int a(int i, Random random) {
+        return Item.STRING.id;
+    }
 }
