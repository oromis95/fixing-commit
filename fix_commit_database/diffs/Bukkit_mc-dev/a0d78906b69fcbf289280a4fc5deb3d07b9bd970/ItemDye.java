@@ -3,6 +3,7 @@ package net.minecraft.server;
 public class ItemDye extends Item {
 
     public static final String[] a = new String[] { "black", "red", "green", "brown", "blue", "purple", "cyan", "silver", "gray", "pink", "lime", "yellow", "lightBlue", "magenta", "orange", "white"};
+    public static final int[] bk = new int[] { 1973019, 11743532, 3887386, 5320730, 2437522, 8073150, 2651799, 2651799, 4408131, 14188952, 4312372, 14602026, 6719955, 12801229, 15435844, 15790320};
 
     public ItemDye(int i) {
         super(i);
@@ -46,7 +47,7 @@ public class ItemDye extends Item {
                             k1 += b.nextInt(3) - 1;
                             l1 += (b.nextInt(3) - 1) * b.nextInt(3) / 2;
                             i2 += b.nextInt(3) - 1;
-                            if (world.getTypeId(k1, l1 - 1, i2) != Block.GRASS.id || world.d(k1, l1, i2)) {
+                            if (world.getTypeId(k1, l1 - 1, i2) != Block.GRASS.id || world.e(k1, l1, i2)) {
                                 continue label53;
                             }
                         }
