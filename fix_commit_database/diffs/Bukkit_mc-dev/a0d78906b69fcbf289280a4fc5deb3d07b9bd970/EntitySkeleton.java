@@ -21,7 +21,7 @@ public class EntitySkeleton extends EntityMonster {
         return "mob.skeletonhurt";
     }
 
-    public void u() {
+    public void v() {
         if (this.world.d()) {
             float f = this.c(1.0F);
 
@@ -30,7 +30,7 @@ public class EntitySkeleton extends EntityMonster {
             }
         }
 
-        super.u();
+        super.v();
     }
 
     protected void a(Entity entity, float f) {
@@ -42,7 +42,7 @@ public class EntitySkeleton extends EntityMonster {
                 EntityArrow entityarrow = new EntityArrow(this.world, this);
 
                 ++entityarrow.locY;
-                double d2 = entity.locY + (double) entity.s() - 0.20000000298023224D - entityarrow.locY;
+                double d2 = entity.locY + (double) entity.t() - 0.20000000298023224D - entityarrow.locY;
                 float f1 = MathHelper.a(d0 * d0 + d1 * d1) * 0.2F;
 
                 this.world.makeSound(this, "random.bow", 1.0F, 1.0F / (this.random.nextFloat() * 0.4F + 0.8F));
@@ -68,7 +68,7 @@ public class EntitySkeleton extends EntityMonster {
         return Item.ARROW.id;
     }
 
-    protected void r() {
+    protected void q() {
         int i = this.random.nextInt(3);
 
         int j;
