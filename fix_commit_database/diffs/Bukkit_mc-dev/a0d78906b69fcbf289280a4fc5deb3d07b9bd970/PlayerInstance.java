@@ -174,7 +174,7 @@ class PlayerInstance {
 
     private void sendTileEntity(TileEntity tileentity) {
         if (tileentity != null) {
-            Packet packet = tileentity.e();
+            Packet packet = tileentity.f();
 
             if (packet != null) {
                 this.sendAll(packet);
