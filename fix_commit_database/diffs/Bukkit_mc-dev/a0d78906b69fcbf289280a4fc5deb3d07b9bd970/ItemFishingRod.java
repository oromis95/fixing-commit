@@ -13,14 +13,14 @@ public class ItemFishingRod extends Item {
             int i = entityhuman.hookedFish.h();
 
             itemstack.damage(i, entityhuman);
-            entityhuman.k_();
+            entityhuman.w();
         } else {
             world.makeSound(entityhuman, "random.bow", 0.5F, 0.4F / (b.nextFloat() * 0.4F + 0.8F));
             if (!world.isStatic) {
                 world.addEntity(new EntityFish(world, entityhuman));
             }
 
-            entityhuman.k_();
+            entityhuman.w();
         }
 
         return itemstack;
