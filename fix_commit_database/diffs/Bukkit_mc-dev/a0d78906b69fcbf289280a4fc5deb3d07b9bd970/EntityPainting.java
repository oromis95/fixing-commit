@@ -112,7 +112,7 @@ public class EntityPainting extends Entity {
         return i == 32 ? 0.5F : (i == 64 ? 0.5F : 0.0F);
     }
 
-    public void o_() {
+    public void m_() {
         if (this.f++ == 100 && !this.world.isStatic) {
             this.f = 0;
             if (!this.h()) {
@@ -180,14 +180,14 @@ public class EntityPainting extends Entity {
         }
     }
 
-    public boolean n_() {
+    public boolean l_() {
         return true;
     }
 
     public boolean damageEntity(Entity entity, int i) {
         if (!this.dead && !this.world.isStatic) {
             this.die();
-            this.ae();
+            this.af();
             this.world.addEntity(new EntityItem(this.world, this.locX, this.locY, this.locZ, new ItemStack(Item.PAINTING)));
         }
 
