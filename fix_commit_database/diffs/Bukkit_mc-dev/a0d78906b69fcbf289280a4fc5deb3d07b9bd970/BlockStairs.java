@@ -96,8 +96,8 @@ public class BlockStairs extends Block {
         this.a.a(world, i, j, k, entity, vec3d);
     }
 
-    public boolean m_() {
-        return this.a.m_();
+    public boolean k_() {
+        return this.a.k_();
     }
 
     public boolean a(int i, boolean flag) {
@@ -121,10 +121,6 @@ public class BlockStairs extends Block {
         this.a.dropNaturally(world, i, j, k, l, f);
     }
 
-    public void b_(World world, int i, int j, int k, int l) {
-        this.a.b_(world, i, j, k, l);
-    }
-
     public void b(World world, int i, int j, int k, Entity entity) {
         this.a.b(world, i, j, k, entity);
     }
