@@ -33,8 +33,8 @@ public class EntityItem extends Entity {
 
     protected void b() {}
 
-    public void o_() {
-        super.o_();
+    public void m_() {
+        super.m_();
         if (this.pickupDelay > 0) {
             --this.pickupDelay;
         }
@@ -86,7 +86,7 @@ public class EntityItem extends Entity {
     }
 
     public boolean damageEntity(Entity entity, int i) {
-        this.ae();
+        this.af();
         this.f -= i;
         if (this.f <= 0) {
             this.die();
