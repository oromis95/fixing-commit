@@ -42,8 +42,8 @@ public class EntityFireball extends Entity {
         this.e = d2 / d3 * 0.1D;
     }
 
-    public void o_() {
-        super.o_();
+    public void m_() {
+        super.m_();
         this.fireTicks = 10;
         if (this.a > 0) {
             --this.a;
@@ -88,7 +88,7 @@ public class EntityFireball extends Entity {
         for (int j = 0; j < list.size(); ++j) {
             Entity entity1 = (Entity) list.get(j);
 
-            if (entity1.n_() && (entity1 != this.shooter || this.l >= 25)) {
+            if (entity1.l_() && (entity1 != this.shooter || this.l >= 25)) {
                 float f = 0.3F;
                 AxisAlignedBB axisalignedbb = entity1.boundingBox.b((double) f, (double) f, (double) f);
                 MovingObjectPosition movingobjectposition1 = axisalignedbb.a(vec3d, vec3d1);
@@ -147,7 +147,7 @@ public class EntityFireball extends Entity {
         this.yaw = this.lastYaw + (this.yaw - this.lastYaw) * 0.2F;
         float f2 = 0.95F;
 
-        if (this.ac()) {
+        if (this.ad()) {
             for (int k = 0; k < 4; ++k) {
                 float f3 = 0.25F;
 
@@ -185,14 +185,14 @@ public class EntityFireball extends Entity {
         this.j = nbttagcompound.c("inGround") == 1;
     }
 
-    public boolean n_() {
+    public boolean l_() {
         return true;
     }
 
     public boolean damageEntity(Entity entity, int i) {
-        this.ae();
+        this.af();
         if (entity != null) {
-            Vec3D vec3d = entity.Y();
+            Vec3D vec3d = entity.Z();
 
             if (vec3d != null) {
                 this.motX = vec3d.a;
