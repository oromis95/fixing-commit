@@ -25,7 +25,7 @@ public class EntitySnowball extends Entity {
         super(world);
         this.shooter = entityliving;
         this.b(0.25F, 0.25F);
-        this.setPositionRotation(entityliving.locX, entityliving.locY + (double) entityliving.s(), entityliving.locZ, entityliving.yaw, entityliving.pitch);
+        this.setPositionRotation(entityliving.locX, entityliving.locY + (double) entityliving.t(), entityliving.locZ, entityliving.yaw, entityliving.pitch);
         this.locX -= (double) (MathHelper.cos(this.yaw / 180.0F * 3.1415927F) * 0.16F);
         this.locY -= 0.10000000149011612D;
         this.locZ -= (double) (MathHelper.sin(this.yaw / 180.0F * 3.1415927F) * 0.16F);
@@ -69,11 +69,11 @@ public class EntitySnowball extends Entity {
         this.h = 0;
     }
 
-    public void o_() {
+    public void m_() {
         this.bo = this.locX;
         this.bp = this.locY;
         this.bq = this.locZ;
-        super.o_();
+        super.m_();
         if (this.a > 0) {
             --this.a;
         }
@@ -118,7 +118,7 @@ public class EntitySnowball extends Entity {
             for (int j = 0; j < list.size(); ++j) {
                 Entity entity1 = (Entity) list.get(j);
 
-                if (entity1.n_() && (entity1 != this.shooter || this.i >= 5)) {
+                if (entity1.l_() && (entity1 != this.shooter || this.i >= 5)) {
                     float f = 0.3F;
                     AxisAlignedBB axisalignedbb = entity1.boundingBox.b((double) f, (double) f, (double) f);
                     MovingObjectPosition movingobjectposition1 = axisalignedbb.a(vec3d, vec3d1);
@@ -179,7 +179,7 @@ public class EntitySnowball extends Entity {
         float f2 = 0.99F;
         float f3 = 0.03F;
 
-        if (this.ac()) {
+        if (this.ad()) {
             for (int l = 0; l < 4; ++l) {
                 float f4 = 0.25F;
 
