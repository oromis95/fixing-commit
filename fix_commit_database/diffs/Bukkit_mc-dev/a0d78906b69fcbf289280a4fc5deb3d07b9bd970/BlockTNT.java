@@ -14,7 +14,7 @@ public class BlockTNT extends Block {
 
     public void doPhysics(World world, int i, int j, int k, int l) {
         if (l > 0 && Block.byId[l].isPowerSource() && world.isBlockIndirectlyPowered(i, j, k)) {
-            this.postBreak(world, i, j, k, 0);
+            this.postBreak(world, i, j, k, 1);
             world.setTypeId(i, j, k, 0);
         }
     }
@@ -32,10 +32,26 @@ public class BlockTNT extends Block {
 
     public void postBreak(World world, int i, int j, int k, int l) {
         if (!world.isStatic) {
-            EntityTNTPrimed entitytntprimed = new EntityTNTPrimed(world, (double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F));
+            if ((l & 1) == 0) {
+                this.a(world, i, j, k, new ItemStack(Block.TNT.id, 1, 0));
+            } else {
+                EntityTNTPrimed entitytntprimed = new EntityTNTPrimed(world, (double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F));
+
+                world.addEntity(entitytntprimed);
+                world.makeSound(entitytntprimed, "random.fuse", 1.0F, 1.0F);
+            }
+        }
+    }
 
-            world.addEntity(entitytntprimed);
-            world.makeSound(entitytntprimed, "random.fuse", 1.0F, 1.0F);
+    public void b(World world, int i, int j, int k, EntityHuman entityhuman) {
+        if (entityhuman.G() != null && entityhuman.G().id == Item.FLINT_AND_STEEL.id) {
+            world.setRawData(i, j, k, 1);
         }
+
+        super.b(world, i, j, k, entityhuman);
+    }
+
+    public boolean interact(World world, int i, int j, int k, EntityHuman entityhuman) {
+        return super.interact(world, i, j, k, entityhuman);
     }
 }
