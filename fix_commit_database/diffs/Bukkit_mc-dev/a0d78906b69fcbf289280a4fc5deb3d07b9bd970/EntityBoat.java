@@ -62,7 +62,7 @@ public class EntityBoat extends Entity {
             this.c = -this.c;
             this.b = 10;
             this.damage += i * 10;
-            this.ae();
+            this.af();
             if (this.damage > 40) {
                 if (this.passenger != null) {
                     this.passenger.mount(this);
@@ -87,12 +87,12 @@ public class EntityBoat extends Entity {
         }
     }
 
-    public boolean n_() {
+    public boolean l_() {
         return !this.dead;
     }
 
-    public void o_() {
-        super.o_();
+    public void m_() {
+        super.m_();
         if (this.b > 0) {
             --this.b;
         }
@@ -303,7 +303,7 @@ public class EntityBoat extends Entity {
             double d0 = Math.cos((double) this.yaw * 3.141592653589793D / 180.0D) * 0.4D;
             double d1 = Math.sin((double) this.yaw * 3.141592653589793D / 180.0D) * 0.4D;
 
-            this.passenger.setPosition(this.locX + d0, this.locY + this.m() + this.passenger.H(), this.locZ + d1);
+            this.passenger.setPosition(this.locX + d0, this.locY + this.m() + this.passenger.I(), this.locZ + d1);
         }
     }
 
