@@ -12,6 +12,14 @@ public class BlockTNT extends Block {
         return i == 0 ? this.textureId + 2 : (i == 1 ? this.textureId + 1 : this.textureId);
     }
 
+    public void c(World world, int i, int j, int k) {
+        super.c(world, i, j, k);
+        if (world.isBlockIndirectlyPowered(i, j, k)) {
+            this.postBreak(world, i, j, k, 1);
+            world.setTypeId(i, j, k, 0);
+        }
+    }
+
     public void doPhysics(World world, int i, int j, int k, int l) {
         if (l > 0 && Block.byId[l].isPowerSource() && world.isBlockIndirectlyPowered(i, j, k)) {
             this.postBreak(world, i, j, k, 1);
@@ -23,7 +31,7 @@ public class BlockTNT extends Block {
         return 0;
     }
 
-    public void c(World world, int i, int j, int k) {
+    public void d(World world, int i, int j, int k) {
         EntityTNTPrimed entitytntprimed = new EntityTNTPrimed(world, (double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F));
 
         entitytntprimed.fuseTicks = world.random.nextInt(entitytntprimed.fuseTicks / 4) + entitytntprimed.fuseTicks / 8;
