@@ -14,6 +14,6 @@ public class BlockMushroom extends BlockFlower {
     }
 
     public boolean f(World world, int i, int j, int k) {
-        return world.j(i, j, k) <= 13 && this.c(world.a(i, j - 1, k));
+        return world.j(i, j, k) <= 13 && this.c(world.getTypeId(i, j - 1, k));
     }
 }
