@@ -5,8 +5,8 @@ import java.util.Random;
 public class BlockReed extends Block {
 
     protected BlockReed(int i, int j) {
-        super(i, Material.i);
-        this.bh = j;
+        super(i, Material.PLANT);
+        this.textureId = j;
         float f = 0.375F;
 
         this.a(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, 1.0F, 0.5F + f);
@@ -14,18 +14,18 @@ public class BlockReed extends Block {
     }
 
     public void a(World world, int i, int j, int k, Random random) {
-        if (world.e(i, j + 1, k)) {
+        if (world.isEmpty(i, j + 1, k)) {
             int l;
 
-            for (l = 1; world.a(i, j - l, k) == this.bi; ++l) {
+            for (l = 1; world.getTypeId(i, j - l, k) == this.id; ++l) {
                 ;
             }
 
             if (l < 3) {
-                int i1 = world.b(i, j, k);
+                int i1 = world.getData(i, j, k);
 
                 if (i1 == 15) {
-                    world.e(i, j + 1, k, this.bi);
+                    world.e(i, j + 1, k, this.id);
                     world.c(i, j, k, 0);
                 } else {
                     world.c(i, j, k, i1 + 1);
@@ -35,9 +35,9 @@ public class BlockReed extends Block {
     }
 
     public boolean a(World world, int i, int j, int k) {
-        int l = world.a(i, j - 1, k);
+        int l = world.getTypeId(i, j - 1, k);
 
-        return l == this.bi ? true : (l != Block.GRASS.bi && l != Block.DIRT.bi ? false : (world.c(i - 1, j - 1, k) == Material.f ? true : (world.c(i + 1, j - 1, k) == Material.f ? true : (world.c(i, j - 1, k - 1) == Material.f ? true : world.c(i, j - 1, k + 1) == Material.f))));
+        return l == this.id ? true : (l != Block.GRASS.id && l != Block.DIRT.id ? false : (world.getMaterial(i - 1, j - 1, k) == Material.WATER ? true : (world.getMaterial(i + 1, j - 1, k) == Material.WATER ? true : (world.getMaterial(i, j - 1, k - 1) == Material.WATER ? true : world.getMaterial(i, j - 1, k + 1) == Material.WATER))));
     }
 
     public void b(World world, int i, int j, int k, int l) {
@@ -46,7 +46,7 @@ public class BlockReed extends Block {
 
     protected final void g(World world, int i, int j, int k) {
         if (!this.f(world, i, j, k)) {
-            this.a_(world, i, j, k, world.b(i, j, k));
+            this.a_(world, i, j, k, world.getData(i, j, k));
             world.e(i, j, k, 0);
         }
     }
@@ -60,7 +60,7 @@ public class BlockReed extends Block {
     }
 
     public int a(int i, Random random) {
-        return Item.SUGAR_CANE.ba;
+        return Item.SUGAR_CANE.id;
     }
 
     public boolean a() {
