@@ -8,7 +8,7 @@ public class BlockPressurePlate extends Block {
     private EnumMobType a;
 
     protected BlockPressurePlate(int i, int j, EnumMobType enummobtype) {
-        super(i, j, Material.d);
+        super(i, j, Material.STONE);
         this.a = enummobtype;
         this.a(true);
         float f = 0.0625F;
@@ -42,29 +42,29 @@ public class BlockPressurePlate extends Block {
         }
 
         if (flag) {
-            this.a_(world, i, j, k, world.b(i, j, k));
+            this.a_(world, i, j, k, world.getData(i, j, k));
             world.e(i, j, k, 0);
         }
     }
 
     public void a(World world, int i, int j, int k, Random random) {
-        if (!world.z) {
-            if (world.b(i, j, k) != 0) {
+        if (!world.isStatic) {
+            if (world.getData(i, j, k) != 0) {
                 this.g(world, i, j, k);
             }
         }
     }
 
     public void a(World world, int i, int j, int k, Entity entity) {
-        if (!world.z) {
-            if (world.b(i, j, k) != 1) {
+        if (!world.isStatic) {
+            if (world.getData(i, j, k) != 1) {
                 this.g(world, i, j, k);
             }
         }
     }
 
     private void g(World world, int i, int j, int k) {
-        boolean flag = world.b(i, j, k) == 1;
+        boolean flag = world.getData(i, j, k) == 1;
         boolean flag1 = false;
         float f = 0.125F;
         List list = null;
@@ -87,38 +87,38 @@ public class BlockPressurePlate extends Block {
 
         if (flag1 && !flag) {
             world.c(i, j, k, 1);
-            world.h(i, j, k, this.bi);
-            world.h(i, j - 1, k, this.bi);
+            world.h(i, j, k, this.id);
+            world.h(i, j - 1, k, this.id);
             world.b(i, j, k, i, j, k);
             world.a((double) i + 0.5D, (double) j + 0.1D, (double) k + 0.5D, "random.click", 0.3F, 0.6F);
         }
 
         if (!flag1 && flag) {
             world.c(i, j, k, 0);
-            world.h(i, j, k, this.bi);
-            world.h(i, j - 1, k, this.bi);
+            world.h(i, j, k, this.id);
+            world.h(i, j - 1, k, this.id);
             world.b(i, j, k, i, j, k);
             world.a((double) i + 0.5D, (double) j + 0.1D, (double) k + 0.5D, "random.click", 0.3F, 0.5F);
         }
 
         if (flag1) {
-            world.i(i, j, k, this.bi);
+            world.i(i, j, k, this.id);
         }
     }
 
     public void b(World world, int i, int j, int k) {
-        int l = world.b(i, j, k);
+        int l = world.getData(i, j, k);
 
         if (l > 0) {
-            world.h(i, j, k, this.bi);
-            world.h(i, j - 1, k, this.bi);
+            world.h(i, j, k, this.id);
+            world.h(i, j - 1, k, this.id);
         }
 
         super.b(world, i, j, k);
     }
 
     public void a(IBlockAccess iblockaccess, int i, int j, int k) {
-        boolean flag = iblockaccess.b(i, j, k) == 1;
+        boolean flag = iblockaccess.getData(i, j, k) == 1;
         float f = 0.0625F;
 
         if (flag) {
@@ -129,11 +129,11 @@ public class BlockPressurePlate extends Block {
     }
 
     public boolean b(IBlockAccess iblockaccess, int i, int j, int k, int l) {
-        return iblockaccess.b(i, j, k) > 0;
+        return iblockaccess.getData(i, j, k) > 0;
     }
 
     public boolean d(World world, int i, int j, int k, int l) {
-        return world.b(i, j, k) == 0 ? false : l == 1;
+        return world.getData(i, j, k) == 0 ? false : l == 1;
     }
 
     public boolean c() {
