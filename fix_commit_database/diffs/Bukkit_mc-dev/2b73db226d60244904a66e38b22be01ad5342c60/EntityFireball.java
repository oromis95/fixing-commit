@@ -28,13 +28,13 @@ public class EntityFireball extends Entity {
         super(world);
         this.an = entityliving;
         this.a(1.0F, 1.0F);
-        this.c(entityliving.p, entityliving.q, entityliving.r, entityliving.v, entityliving.w);
-        this.a(this.p, this.q, this.r);
-        this.H = 0.0F;
-        this.s = this.t = this.u = 0.0D;
-        d0 += this.W.nextGaussian() * 0.4D;
-        d1 += this.W.nextGaussian() * 0.4D;
-        d2 += this.W.nextGaussian() * 0.4D;
+        this.c(entityliving.locX, entityliving.locY, entityliving.locZ, entityliving.yaw, entityliving.pitch);
+        this.a(this.locX, this.locY, this.locZ);
+        this.height = 0.0F;
+        this.motX = this.motY = this.motZ = 0.0D;
+        d0 += this.random.nextGaussian() * 0.4D;
+        d1 += this.random.nextGaussian() * 0.4D;
+        d2 += this.random.nextGaussian() * 0.4D;
         double d3 = (double) MathHelper.a(d0 * d0 + d1 * d1 + d2 * d2);
 
         this.b = d0 / d3 * 0.1D;
@@ -44,13 +44,13 @@ public class EntityFireball extends Entity {
 
     public void b_() {
         super.b_();
-        this.Z = 10;
+        this.fireTicks = 10;
         if (this.a > 0) {
             --this.a;
         }
 
         if (this.am) {
-            int i = this.l.a(this.e, this.f, this.ak);
+            int i = this.world.getTypeId(this.e, this.f, this.ak);
 
             if (i == this.al) {
                 ++this.ao;
@@ -62,27 +62,27 @@ public class EntityFireball extends Entity {
             }
 
             this.am = false;
-            this.s *= (double) (this.W.nextFloat() * 0.2F);
-            this.t *= (double) (this.W.nextFloat() * 0.2F);
-            this.u *= (double) (this.W.nextFloat() * 0.2F);
+            this.motX *= (double) (this.random.nextFloat() * 0.2F);
+            this.motY *= (double) (this.random.nextFloat() * 0.2F);
+            this.motZ *= (double) (this.random.nextFloat() * 0.2F);
             this.ao = 0;
             this.ap = 0;
         } else {
             ++this.ap;
         }
 
-        Vec3D vec3d = Vec3D.b(this.p, this.q, this.r);
-        Vec3D vec3d1 = Vec3D.b(this.p + this.s, this.q + this.t, this.r + this.u);
-        MovingObjectPosition movingobjectposition = this.l.a(vec3d, vec3d1);
+        Vec3D vec3d = Vec3D.b(this.locX, this.locY, this.locZ);
+        Vec3D vec3d1 = Vec3D.b(this.locX + this.motX, this.locY + this.motY, this.locZ + this.motZ);
+        MovingObjectPosition movingobjectposition = this.world.a(vec3d, vec3d1);
 
-        vec3d = Vec3D.b(this.p, this.q, this.r);
-        vec3d1 = Vec3D.b(this.p + this.s, this.q + this.t, this.r + this.u);
+        vec3d = Vec3D.b(this.locX, this.locY, this.locZ);
+        vec3d1 = Vec3D.b(this.locX + this.motX, this.locY + this.motY, this.locZ + this.motZ);
         if (movingobjectposition != null) {
             vec3d1 = Vec3D.b(movingobjectposition.f.a, movingobjectposition.f.b, movingobjectposition.f.c);
         }
 
         Entity entity = null;
-        List list = this.l.b((Entity) this, this.z.a(this.s, this.t, this.u).b(1.0D, 1.0D, 1.0D));
+        List list = this.world.b((Entity) this, this.boundingBox.a(this.motX, this.motY, this.motZ).b(1.0D, 1.0D, 1.0D));
         double d0 = 0.0D;
 
         for (int j = 0; j < list.size(); ++j) {
@@ -90,7 +90,7 @@ public class EntityFireball extends Entity {
 
             if (entity1.c_() && (entity1 != this.an || this.ap >= 25)) {
                 float f = 0.3F;
-                AxisAlignedBB axisalignedbb = entity1.z.b((double) f, (double) f, (double) f);
+                AxisAlignedBB axisalignedbb = entity1.boundingBox.b((double) f, (double) f, (double) f);
                 MovingObjectPosition movingobjectposition1 = axisalignedbb.a(vec3d, vec3d1);
 
                 if (movingobjectposition1 != null) {
@@ -113,55 +113,55 @@ public class EntityFireball extends Entity {
                 ;
             }
 
-            this.l.a((Entity) null, this.p, this.q, this.r, 1.0F, true);
+            this.world.a((Entity) null, this.locX, this.locY, this.locZ, 1.0F, true);
             this.q();
         }
 
-        this.p += this.s;
-        this.q += this.t;
-        this.r += this.u;
-        float f1 = MathHelper.a(this.s * this.s + this.u * this.u);
+        this.locX += this.motX;
+        this.locY += this.motY;
+        this.locZ += this.motZ;
+        float f1 = MathHelper.a(this.motX * this.motX + this.motZ * this.motZ);
 
-        this.v = (float) (Math.atan2(this.s, this.u) * 180.0D / 3.1415927410125732D);
+        this.yaw = (float) (Math.atan2(this.motX, this.motZ) * 180.0D / 3.1415927410125732D);
 
-        for (this.w = (float) (Math.atan2(this.t, (double) f1) * 180.0D / 3.1415927410125732D); this.w - this.y < -180.0F; this.y -= 360.0F) {
+        for (this.pitch = (float) (Math.atan2(this.motY, (double) f1) * 180.0D / 3.1415927410125732D); this.pitch - this.lastPitch < -180.0F; this.lastPitch -= 360.0F) {
             ;
         }
 
-        while (this.w - this.y >= 180.0F) {
-            this.y += 360.0F;
+        while (this.pitch - this.lastPitch >= 180.0F) {
+            this.lastPitch += 360.0F;
         }
 
-        while (this.v - this.x < -180.0F) {
-            this.x -= 360.0F;
+        while (this.yaw - this.lastYaw < -180.0F) {
+            this.lastYaw -= 360.0F;
         }
 
-        while (this.v - this.x >= 180.0F) {
-            this.x += 360.0F;
+        while (this.yaw - this.lastYaw >= 180.0F) {
+            this.lastYaw += 360.0F;
         }
 
-        this.w = this.y + (this.w - this.y) * 0.2F;
-        this.v = this.x + (this.v - this.x) * 0.2F;
+        this.pitch = this.lastPitch + (this.pitch - this.lastPitch) * 0.2F;
+        this.yaw = this.lastYaw + (this.yaw - this.lastYaw) * 0.2F;
         float f2 = 0.95F;
 
         if (this.v()) {
             for (int k = 0; k < 4; ++k) {
                 float f3 = 0.25F;
 
-                this.l.a("bubble", this.p - this.s * (double) f3, this.q - this.t * (double) f3, this.r - this.u * (double) f3, this.s, this.t, this.u);
+                this.world.a("bubble", this.locX - this.motX * (double) f3, this.locY - this.motY * (double) f3, this.locZ - this.motZ * (double) f3, this.motX, this.motY, this.motZ);
             }
 
             f2 = 0.8F;
         }
 
-        this.s += this.b;
-        this.t += this.c;
-        this.u += this.d;
-        this.s *= (double) f2;
-        this.t *= (double) f2;
-        this.u *= (double) f2;
-        this.l.a("smoke", this.p, this.q + 0.5D, this.r, 0.0D, 0.0D, 0.0D);
-        this.a(this.p, this.q, this.r);
+        this.motX += this.b;
+        this.motY += this.c;
+        this.motZ += this.d;
+        this.motX *= (double) f2;
+        this.motY *= (double) f2;
+        this.motZ *= (double) f2;
+        this.world.a("smoke", this.locX, this.locY + 0.5D, this.locZ, 0.0D, 0.0D, 0.0D);
+        this.a(this.locX, this.locY, this.locZ);
     }
 
     public void a(NBTTagCompound nbttagcompound) {
@@ -192,12 +192,12 @@ public class EntityFireball extends Entity {
             Vec3D vec3d = entity.G();
 
             if (vec3d != null) {
-                this.s = vec3d.a;
-                this.t = vec3d.b;
-                this.u = vec3d.c;
-                this.b = this.s * 0.1D;
-                this.c = this.t * 0.1D;
-                this.d = this.u * 0.1D;
+                this.motX = vec3d.a;
+                this.motY = vec3d.b;
+                this.motZ = vec3d.c;
+                this.b = this.motX * 0.1D;
+                this.c = this.motY * 0.1D;
+                this.d = this.motZ * 0.1D;
             }
 
             return true;
