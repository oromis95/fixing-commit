@@ -6,18 +6,18 @@ public class ItemBucket extends Item {
 
     public ItemBucket(int i, int j) {
         super(i);
-        this.bb = 1;
-        this.bc = 64;
+        this.maxStackSize = 1;
+        this.durability = 64;
         this.a = j;
     }
 
     public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
         float f = 1.0F;
-        float f1 = entityhuman.y + (entityhuman.w - entityhuman.y) * f;
-        float f2 = entityhuman.x + (entityhuman.v - entityhuman.x) * f;
-        double d0 = entityhuman.m + (entityhuman.p - entityhuman.m) * (double) f;
-        double d1 = entityhuman.n + (entityhuman.q - entityhuman.n) * (double) f + 1.62D - (double) entityhuman.H;
-        double d2 = entityhuman.o + (entityhuman.r - entityhuman.o) * (double) f;
+        float f1 = entityhuman.lastPitch + (entityhuman.pitch - entityhuman.lastPitch) * f;
+        float f2 = entityhuman.lastYaw + (entityhuman.yaw - entityhuman.lastYaw) * f;
+        double d0 = entityhuman.lastX + (entityhuman.locX - entityhuman.lastX) * (double) f;
+        double d1 = entityhuman.lastY + (entityhuman.locY - entityhuman.lastY) * (double) f + 1.62D - (double) entityhuman.height;
+        double d2 = entityhuman.lastZ + (entityhuman.locZ - entityhuman.lastZ) * (double) f;
         Vec3D vec3d = Vec3D.b(d0, d1, d2);
         float f3 = MathHelper.b(-f2 * 0.017453292F - 3.1415927F);
         float f4 = MathHelper.a(-f2 * 0.017453292F - 3.1415927F);
@@ -42,12 +42,12 @@ public class ItemBucket extends Item {
                 }
 
                 if (this.a == 0) {
-                    if (world.c(i, j, k) == Material.f && world.b(i, j, k) == 0) {
+                    if (world.getMaterial(i, j, k) == Material.WATER && world.getData(i, j, k) == 0) {
                         world.e(i, j, k, 0);
                         return new ItemStack(Item.WATER_BUCKET);
                     }
 
-                    if (world.c(i, j, k) == Material.g && world.b(i, j, k) == 0) {
+                    if (world.getMaterial(i, j, k) == Material.LAVA && world.getData(i, j, k) == 0) {
                         world.e(i, j, k, 0);
                         return new ItemStack(Item.LAVA_BUCKET);
                     }
@@ -80,8 +80,8 @@ public class ItemBucket extends Item {
                         ++i;
                     }
 
-                    if (world.e(i, j, k) || !world.c(i, j, k).a()) {
-                        if (world.q.d && this.a == Block.WATER.bi) {
+                    if (world.isEmpty(i, j, k) || !world.getMaterial(i, j, k).isBuildable()) {
+                        if (world.q.d && this.a == Block.WATER.id) {
                             world.a(d0 + 0.5D, d1 + 0.5D, d2 + 0.5D, "random.fizz", 0.5F, 2.6F + (world.l.nextFloat() - world.l.nextFloat()) * 0.8F);
 
                             for (int l = 0; l < 8; ++l) {
