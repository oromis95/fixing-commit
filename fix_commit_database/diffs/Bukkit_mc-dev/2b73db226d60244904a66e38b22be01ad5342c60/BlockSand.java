@@ -7,15 +7,15 @@ public class BlockSand extends Block {
     public static boolean a = false;
 
     public BlockSand(int i, int j) {
-        super(i, j, Material.m);
+        super(i, j, Material.SAND);
     }
 
     public void e(World world, int i, int j, int k) {
-        world.i(i, j, k, this.bi);
+        world.i(i, j, k, this.id);
     }
 
     public void b(World world, int i, int j, int k, int l) {
-        world.i(i, j, k, this.bi);
+        world.i(i, j, k, this.id);
     }
 
     public void a(World world, int i, int j, int k, Random random) {
@@ -27,7 +27,7 @@ public class BlockSand extends Block {
             byte b0 = 32;
 
             if (!a && world.a(i - b0, j - b0, k - b0, i + b0, j + b0, k + b0)) {
-                EntityFallingSand entityfallingsand = new EntityFallingSand(world, (double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), this.bi);
+                EntityFallingSand entityfallingsand = new EntityFallingSand(world, (double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), this.id);
 
                 world.a((Entity) entityfallingsand);
             } else {
@@ -38,7 +38,7 @@ public class BlockSand extends Block {
                 }
 
                 if (j > 0) {
-                    world.e(i, j, k, this.bi);
+                    world.e(i, j, k, this.id);
                 }
             }
         }
@@ -49,16 +49,16 @@ public class BlockSand extends Block {
     }
 
     public static boolean g(World world, int i, int j, int k) {
-        int l = world.a(i, j, k);
+        int l = world.getTypeId(i, j, k);
 
         if (l == 0) {
             return true;
-        } else if (l == Block.FIRE.bi) {
+        } else if (l == Block.FIRE.id) {
             return true;
         } else {
-            Material material = Block.m[l].bt;
+            Material material = Block.byId[l].material;
 
-            return material == Material.f ? true : material == Material.g;
+            return material == Material.WATER ? true : material == Material.LAVA;
         }
     }
 }
