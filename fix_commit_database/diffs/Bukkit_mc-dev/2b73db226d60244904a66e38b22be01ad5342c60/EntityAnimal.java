@@ -7,7 +7,7 @@ public abstract class EntityAnimal extends EntityCreature implements IAnimal {
     }
 
     protected float a(int i, int j, int k) {
-        return this.l.a(i, j - 1, k) == Block.GRASS.bi ? 10.0F : this.l.l(i, j, k) - 0.5F;
+        return this.world.getTypeId(i, j - 1, k) == Block.GRASS.id ? 10.0F : this.world.l(i, j, k) - 0.5F;
     }
 
     public void a(NBTTagCompound nbttagcompound) {
@@ -19,11 +19,11 @@ public abstract class EntityAnimal extends EntityCreature implements IAnimal {
     }
 
     public boolean b() {
-        int i = MathHelper.b(this.p);
-        int j = MathHelper.b(this.z.b);
-        int k = MathHelper.b(this.r);
+        int i = MathHelper.b(this.locX);
+        int j = MathHelper.b(this.boundingBox.b);
+        int k = MathHelper.b(this.locZ);
 
-        return this.l.a(i, j - 1, k) == Block.GRASS.bi && this.l.j(i, j, k) > 8 && super.b();
+        return this.world.getTypeId(i, j - 1, k) == Block.GRASS.id && this.world.j(i, j, k) > 8 && super.b();
     }
 
     public int c() {
