@@ -4,18 +4,18 @@ public class ItemFishingRod extends Item {
 
     public ItemFishingRod(int i) {
         super(i);
-        this.bc = 64;
+        this.durability = 64;
     }
 
     public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
-        if (entityhuman.aE != null) {
-            int i = entityhuman.aE.d();
+        if (entityhuman.hookedFish != null) {
+            int i = entityhuman.hookedFish.d();
 
             itemstack.b(i);
             entityhuman.K();
         } else {
             world.a(entityhuman, "random.bow", 0.5F, 0.4F / (b.nextFloat() * 0.4F + 0.8F));
-            if (!world.z) {
+            if (!world.isStatic) {
                 world.a((Entity) (new EntityFish(world, entityhuman)));
             }
 
