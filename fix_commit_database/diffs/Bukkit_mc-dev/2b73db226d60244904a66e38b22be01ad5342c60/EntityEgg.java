@@ -25,18 +25,18 @@ public class EntityEgg extends Entity {
         super(world);
         this.ak = entityliving;
         this.a(0.25F, 0.25F);
-        this.c(entityliving.p, entityliving.q + (double) entityliving.w(), entityliving.r, entityliving.v, entityliving.w);
-        this.p -= (double) (MathHelper.b(this.v / 180.0F * 3.1415927F) * 0.16F);
-        this.q -= 0.10000000149011612D;
-        this.r -= (double) (MathHelper.a(this.v / 180.0F * 3.1415927F) * 0.16F);
-        this.a(this.p, this.q, this.r);
-        this.H = 0.0F;
+        this.c(entityliving.locX, entityliving.locY + (double) entityliving.w(), entityliving.locZ, entityliving.yaw, entityliving.pitch);
+        this.locX -= (double) (MathHelper.b(this.yaw / 180.0F * 3.1415927F) * 0.16F);
+        this.locY -= 0.10000000149011612D;
+        this.locZ -= (double) (MathHelper.a(this.yaw / 180.0F * 3.1415927F) * 0.16F);
+        this.a(this.locX, this.locY, this.locZ);
+        this.height = 0.0F;
         float f = 0.4F;
 
-        this.s = (double) (-MathHelper.a(this.v / 180.0F * 3.1415927F) * MathHelper.b(this.w / 180.0F * 3.1415927F) * f);
-        this.u = (double) (MathHelper.b(this.v / 180.0F * 3.1415927F) * MathHelper.b(this.w / 180.0F * 3.1415927F) * f);
-        this.t = (double) (-MathHelper.a(this.w / 180.0F * 3.1415927F) * f);
-        this.a(this.s, this.t, this.u, 1.5F, 1.0F);
+        this.motX = (double) (-MathHelper.a(this.yaw / 180.0F * 3.1415927F) * MathHelper.b(this.pitch / 180.0F * 3.1415927F) * f);
+        this.motZ = (double) (MathHelper.b(this.yaw / 180.0F * 3.1415927F) * MathHelper.b(this.pitch / 180.0F * 3.1415927F) * f);
+        this.motY = (double) (-MathHelper.a(this.pitch / 180.0F * 3.1415927F) * f);
+        this.a(this.motX, this.motY, this.motZ, 1.5F, 1.0F);
     }
 
     public EntityEgg(World world, double d0, double d1, double d2) {
@@ -44,7 +44,7 @@ public class EntityEgg extends Entity {
         this.al = 0;
         this.a(0.25F, 0.25F);
         this.a(d0, d1, d2);
-        this.H = 0.0F;
+        this.height = 0.0F;
     }
 
     public void a(double d0, double d1, double d2, float f, float f1) {
@@ -53,33 +53,33 @@ public class EntityEgg extends Entity {
         d0 /= (double) f2;
         d1 /= (double) f2;
         d2 /= (double) f2;
-        d0 += this.W.nextGaussian() * 0.007499999832361937D * (double) f1;
-        d1 += this.W.nextGaussian() * 0.007499999832361937D * (double) f1;
-        d2 += this.W.nextGaussian() * 0.007499999832361937D * (double) f1;
+        d0 += this.random.nextGaussian() * 0.007499999832361937D * (double) f1;
+        d1 += this.random.nextGaussian() * 0.007499999832361937D * (double) f1;
+        d2 += this.random.nextGaussian() * 0.007499999832361937D * (double) f1;
         d0 *= (double) f;
         d1 *= (double) f;
         d2 *= (double) f;
-        this.s = d0;
-        this.t = d1;
-        this.u = d2;
+        this.motX = d0;
+        this.motY = d1;
+        this.motZ = d2;
         float f3 = MathHelper.a(d0 * d0 + d2 * d2);
 
-        this.x = this.v = (float) (Math.atan2(d0, d2) * 180.0D / 3.1415927410125732D);
-        this.y = this.w = (float) (Math.atan2(d1, (double) f3) * 180.0D / 3.1415927410125732D);
+        this.lastYaw = this.yaw = (float) (Math.atan2(d0, d2) * 180.0D / 3.1415927410125732D);
+        this.lastPitch = this.pitch = (float) (Math.atan2(d1, (double) f3) * 180.0D / 3.1415927410125732D);
         this.al = 0;
     }
 
     public void b_() {
-        this.O = this.p;
-        this.P = this.q;
-        this.Q = this.r;
+        this.O = this.locX;
+        this.P = this.locY;
+        this.Q = this.locZ;
         super.b_();
         if (this.a > 0) {
             --this.a;
         }
 
         if (this.f) {
-            int i = this.l.a(this.b, this.c, this.d);
+            int i = this.world.getTypeId(this.b, this.c, this.d);
 
             if (i == this.e) {
                 ++this.al;
@@ -91,28 +91,28 @@ public class EntityEgg extends Entity {
             }
 
             this.f = false;
-            this.s *= (double) (this.W.nextFloat() * 0.2F);
-            this.t *= (double) (this.W.nextFloat() * 0.2F);
-            this.u *= (double) (this.W.nextFloat() * 0.2F);
+            this.motX *= (double) (this.random.nextFloat() * 0.2F);
+            this.motY *= (double) (this.random.nextFloat() * 0.2F);
+            this.motZ *= (double) (this.random.nextFloat() * 0.2F);
             this.al = 0;
             this.am = 0;
         } else {
             ++this.am;
         }
 
-        Vec3D vec3d = Vec3D.b(this.p, this.q, this.r);
-        Vec3D vec3d1 = Vec3D.b(this.p + this.s, this.q + this.t, this.r + this.u);
-        MovingObjectPosition movingobjectposition = this.l.a(vec3d, vec3d1);
+        Vec3D vec3d = Vec3D.b(this.locX, this.locY, this.locZ);
+        Vec3D vec3d1 = Vec3D.b(this.locX + this.motX, this.locY + this.motY, this.locZ + this.motZ);
+        MovingObjectPosition movingobjectposition = this.world.a(vec3d, vec3d1);
 
-        vec3d = Vec3D.b(this.p, this.q, this.r);
-        vec3d1 = Vec3D.b(this.p + this.s, this.q + this.t, this.r + this.u);
+        vec3d = Vec3D.b(this.locX, this.locY, this.locZ);
+        vec3d1 = Vec3D.b(this.locX + this.motX, this.locY + this.motY, this.locZ + this.motZ);
         if (movingobjectposition != null) {
             vec3d1 = Vec3D.b(movingobjectposition.f.a, movingobjectposition.f.b, movingobjectposition.f.c);
         }
 
-        if (!this.l.z) {
+        if (!this.world.isStatic) {
             Entity entity = null;
-            List list = this.l.b((Entity) this, this.z.a(this.s, this.t, this.u).b(1.0D, 1.0D, 1.0D));
+            List list = this.world.b((Entity) this, this.boundingBox.a(this.motX, this.motY, this.motZ).b(1.0D, 1.0D, 1.0D));
             double d0 = 0.0D;
 
             for (int j = 0; j < list.size(); ++j) {
@@ -120,7 +120,7 @@ public class EntityEgg extends Entity {
 
                 if (entity1.c_() && (entity1 != this.ak || this.am >= 5)) {
                     float f = 0.3F;
-                    AxisAlignedBB axisalignedbb = entity1.z.b((double) f, (double) f, (double) f);
+                    AxisAlignedBB axisalignedbb = entity1.boundingBox.b((double) f, (double) f, (double) f);
                     MovingObjectPosition movingobjectposition1 = axisalignedbb.a(vec3d, vec3d1);
 
                     if (movingobjectposition1 != null) {
@@ -144,53 +144,53 @@ public class EntityEgg extends Entity {
                 ;
             }
 
-            if (!this.l.z && this.W.nextInt(8) == 0) {
+            if (!this.world.isStatic && this.random.nextInt(8) == 0) {
                 byte b0 = 1;
 
-                if (this.W.nextInt(32) == 0) {
+                if (this.random.nextInt(32) == 0) {
                     b0 = 4;
                 }
 
                 for (int k = 0; k < b0; ++k) {
-                    EntityChicken entitychicken = new EntityChicken(this.l);
+                    EntityChicken entitychicken = new EntityChicken(this.world);
 
-                    entitychicken.c(this.p, this.q, this.r, this.v, 0.0F);
-                    this.l.a((Entity) entitychicken);
+                    entitychicken.c(this.locX, this.locY, this.locZ, this.yaw, 0.0F);
+                    this.world.a((Entity) entitychicken);
                 }
             }
 
             for (int l = 0; l < 8; ++l) {
-                this.l.a("snowballpoof", this.p, this.q, this.r, 0.0D, 0.0D, 0.0D);
+                this.world.a("snowballpoof", this.locX, this.locY, this.locZ, 0.0D, 0.0D, 0.0D);
             }
 
             this.q();
         }
 
-        this.p += this.s;
-        this.q += this.t;
-        this.r += this.u;
-        float f1 = MathHelper.a(this.s * this.s + this.u * this.u);
+        this.locX += this.motX;
+        this.locY += this.motY;
+        this.locZ += this.motZ;
+        float f1 = MathHelper.a(this.motX * this.motX + this.motZ * this.motZ);
 
-        this.v = (float) (Math.atan2(this.s, this.u) * 180.0D / 3.1415927410125732D);
+        this.yaw = (float) (Math.atan2(this.motX, this.motZ) * 180.0D / 3.1415927410125732D);
 
-        for (this.w = (float) (Math.atan2(this.t, (double) f1) * 180.0D / 3.1415927410125732D); this.w - this.y < -180.0F; this.y -= 360.0F) {
+        for (this.pitch = (float) (Math.atan2(this.motY, (double) f1) * 180.0D / 3.1415927410125732D); this.pitch - this.lastPitch < -180.0F; this.lastPitch -= 360.0F) {
             ;
         }
 
-        while (this.w - this.y >= 180.0F) {
-            this.y += 360.0F;
+        while (this.pitch - this.lastPitch >= 180.0F) {
+            this.lastPitch += 360.0F;
         }
 
-        while (this.v - this.x < -180.0F) {
-            this.x -= 360.0F;
+        while (this.yaw - this.lastYaw < -180.0F) {
+            this.lastYaw -= 360.0F;
         }
 
-        while (this.v - this.x >= 180.0F) {
-            this.x += 360.0F;
+        while (this.yaw - this.lastYaw >= 180.0F) {
+            this.lastYaw += 360.0F;
         }
 
-        this.w = this.y + (this.w - this.y) * 0.2F;
-        this.v = this.x + (this.v - this.x) * 0.2F;
+        this.pitch = this.lastPitch + (this.pitch - this.lastPitch) * 0.2F;
+        this.yaw = this.lastYaw + (this.yaw - this.lastYaw) * 0.2F;
         float f2 = 0.99F;
         float f3 = 0.03F;
 
@@ -198,17 +198,17 @@ public class EntityEgg extends Entity {
             for (int i1 = 0; i1 < 4; ++i1) {
                 float f4 = 0.25F;
 
-                this.l.a("bubble", this.p - this.s * (double) f4, this.q - this.t * (double) f4, this.r - this.u * (double) f4, this.s, this.t, this.u);
+                this.world.a("bubble", this.locX - this.motX * (double) f4, this.locY - this.motY * (double) f4, this.locZ - this.motZ * (double) f4, this.motX, this.motY, this.motZ);
             }
 
             f2 = 0.8F;
         }
 
-        this.s *= (double) f2;
-        this.t *= (double) f2;
-        this.u *= (double) f2;
-        this.t -= (double) f3;
-        this.a(this.p, this.q, this.r);
+        this.motX *= (double) f2;
+        this.motY *= (double) f2;
+        this.motZ *= (double) f2;
+        this.motY -= (double) f3;
+        this.a(this.locX, this.locY, this.locZ);
     }
 
     public void a(NBTTagCompound nbttagcompound) {
@@ -230,8 +230,8 @@ public class EntityEgg extends Entity {
     }
 
     public void b(EntityHuman entityhuman) {
-        if (this.f && this.ak == entityhuman && this.a <= 0 && entityhuman.an.a(new ItemStack(Item.ARROW, 1))) {
-            this.l.a(this, "random.pop", 0.2F, ((this.W.nextFloat() - this.W.nextFloat()) * 0.7F + 1.0F) * 2.0F);
+        if (this.f && this.ak == entityhuman && this.a <= 0 && entityhuman.inventory.a(new ItemStack(Item.ARROW, 1))) {
+            this.world.a(this, "random.pop", 0.2F, ((this.random.nextFloat() - this.random.nextFloat()) * 0.7F + 1.0F) * 2.0F);
             entityhuman.c(this, 1);
             this.q();
         }
