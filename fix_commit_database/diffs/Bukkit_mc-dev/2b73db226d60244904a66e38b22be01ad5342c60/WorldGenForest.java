@@ -30,8 +30,8 @@ public class WorldGenForest extends WorldGenerator {
                 for (j1 = i - b0; j1 <= i + b0 && flag; ++j1) {
                     for (k1 = k - b0; k1 <= k + b0 && flag; ++k1) {
                         if (i1 >= 0 && i1 < 128) {
-                            l1 = world.a(j1, i1, k1);
-                            if (l1 != 0 && l1 != Block.LEAVES.bi) {
+                            l1 = world.getTypeId(j1, i1, k1);
+                            if (l1 != 0 && l1 != Block.LEAVES.id) {
                                 flag = false;
                             }
                         } else {
@@ -44,9 +44,9 @@ public class WorldGenForest extends WorldGenerator {
             if (!flag) {
                 return false;
             } else {
-                i1 = world.a(i, j - 1, k);
-                if ((i1 == Block.GRASS.bi || i1 == Block.DIRT.bi) && j < 128 - l - 1) {
-                    world.b(i, j - 1, k, Block.DIRT.bi);
+                i1 = world.getTypeId(i, j - 1, k);
+                if ((i1 == Block.GRASS.id || i1 == Block.DIRT.id) && j < 128 - l - 1) {
+                    world.setTypeId(i, j - 1, k, Block.DIRT.id);
 
                     int i2;
 
@@ -60,17 +60,17 @@ public class WorldGenForest extends WorldGenerator {
                             for (int k2 = k - k1; k2 <= k + k1; ++k2) {
                                 int l2 = k2 - k;
 
-                                if ((Math.abs(j2) != k1 || Math.abs(l2) != k1 || random.nextInt(2) != 0 && j1 != 0) && !Block.o[world.a(l1, i2, k2)]) {
-                                    world.a(l1, i2, k2, Block.LEAVES.bi, 2);
+                                if ((Math.abs(j2) != k1 || Math.abs(l2) != k1 || random.nextInt(2) != 0 && j1 != 0) && !Block.o[world.getTypeId(l1, i2, k2)]) {
+                                    world.setTypeIdAndData(l1, i2, k2, Block.LEAVES.id, 2);
                                 }
                             }
                         }
                     }
 
                     for (i2 = 0; i2 < l; ++i2) {
-                        j1 = world.a(i, j + i2, k);
-                        if (j1 == 0 || j1 == Block.LEAVES.bi) {
-                            world.a(i, j + i2, k, Block.LOG.bi, 2);
+                        j1 = world.getTypeId(i, j + i2, k);
+                        if (j1 == 0 || j1 == Block.LEAVES.id) {
+                            world.setTypeIdAndData(i, j + i2, k, Block.LOG.id, 2);
                         }
                     }
 
