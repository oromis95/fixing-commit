@@ -11,8 +11,8 @@ public class Packet39AttachEntity extends Packet {
     public Packet39AttachEntity() {}
 
     public Packet39AttachEntity(Entity entity, Entity entity1) {
-        this.a = entity.g;
-        this.b = entity1 != null ? entity1.g : -1;
+        this.a = entity.id;
+        this.b = entity1 != null ? entity1.id : -1;
     }
 
     public int a() {
