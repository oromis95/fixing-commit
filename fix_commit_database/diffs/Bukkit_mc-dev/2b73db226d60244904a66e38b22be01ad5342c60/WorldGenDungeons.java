@@ -19,17 +19,17 @@ public class WorldGenDungeons extends WorldGenerator {
         for (k1 = i - l - 1; k1 <= i + l + 1; ++k1) {
             for (l1 = j - 1; l1 <= j + b0 + 1; ++l1) {
                 for (i2 = k - i1 - 1; i2 <= k + i1 + 1; ++i2) {
-                    Material material = world.c(k1, l1, i2);
+                    Material material = world.getMaterial(k1, l1, i2);
 
-                    if (l1 == j - 1 && !material.a()) {
+                    if (l1 == j - 1 && !material.isBuildable()) {
                         return false;
                     }
 
-                    if (l1 == j + b0 + 1 && !material.a()) {
+                    if (l1 == j + b0 + 1 && !material.isBuildable()) {
                         return false;
                     }
 
-                    if ((k1 == i - l - 1 || k1 == i + l + 1 || i2 == k - i1 - 1 || i2 == k + i1 + 1) && l1 == j && world.e(k1, l1, i2) && world.e(k1, l1 + 1, i2)) {
+                    if ((k1 == i - l - 1 || k1 == i + l + 1 || i2 == k - i1 - 1 || i2 == k + i1 + 1) && l1 == j && world.isEmpty(k1, l1, i2) && world.isEmpty(k1, l1 + 1, i2)) {
                         ++j1;
                     }
                 }
@@ -42,13 +42,13 @@ public class WorldGenDungeons extends WorldGenerator {
                     for (i2 = k - i1 - 1; i2 <= k + i1 + 1; ++i2) {
                         if (k1 != i - l - 1 && l1 != j - 1 && i2 != k - i1 - 1 && k1 != i + l + 1 && l1 != j + b0 + 1 && i2 != k + i1 + 1) {
                             world.e(k1, l1, i2, 0);
-                        } else if (l1 >= 0 && !world.c(k1, l1 - 1, i2).a()) {
+                        } else if (l1 >= 0 && !world.getMaterial(k1, l1 - 1, i2).isBuildable()) {
                             world.e(k1, l1, i2, 0);
-                        } else if (world.c(k1, l1, i2).a()) {
+                        } else if (world.getMaterial(k1, l1, i2).isBuildable()) {
                             if (l1 == j - 1 && random.nextInt(4) != 0) {
-                                world.e(k1, l1, i2, Block.MOSSY_COBBLESTONE.bi);
+                                world.e(k1, l1, i2, Block.MOSSY_COBBLESTONE.id);
                             } else {
-                                world.e(k1, l1, i2, Block.COBBLESTONE.bi);
+                                world.e(k1, l1, i2, Block.COBBLESTONE.id);
                             }
                         }
                     }
@@ -66,28 +66,28 @@ public class WorldGenDungeons extends WorldGenerator {
                             i2 = i + random.nextInt(l * 2 + 1) - l;
                             int j2 = k + random.nextInt(i1 * 2 + 1) - i1;
 
-                            if (world.e(i2, j, j2)) {
+                            if (world.isEmpty(i2, j, j2)) {
                                 int k2 = 0;
 
-                                if (world.c(i2 - 1, j, j2).a()) {
+                                if (world.getMaterial(i2 - 1, j, j2).isBuildable()) {
                                     ++k2;
                                 }
 
-                                if (world.c(i2 + 1, j, j2).a()) {
+                                if (world.getMaterial(i2 + 1, j, j2).isBuildable()) {
                                     ++k2;
                                 }
 
-                                if (world.c(i2, j, j2 - 1).a()) {
+                                if (world.getMaterial(i2, j, j2 - 1).isBuildable()) {
                                     ++k2;
                                 }
 
-                                if (world.c(i2, j, j2 + 1).a()) {
+                                if (world.getMaterial(i2, j, j2 + 1).isBuildable()) {
                                     ++k2;
                                 }
 
                                 if (k2 == 1) {
-                                    world.e(i2, j, j2, Block.CHEST.bi);
-                                    TileEntityChest tileentitychest = (TileEntityChest) world.m(i2, j, j2);
+                                    world.e(i2, j, j2, Block.CHEST.id);
+                                    TileEntityChest tileentitychest = (TileEntityChest) world.getTileEntity(i2, j, j2);
 
                                     for (int l2 = 0; l2 < 8; ++l2) {
                                         ItemStack itemstack = this.a(random);
@@ -110,8 +110,8 @@ public class WorldGenDungeons extends WorldGenerator {
                 }
             }
 
-            world.e(i, j, k, Block.MOB_SPAWNER.bi);
-            TileEntityMobSpawner tileentitymobspawner = (TileEntityMobSpawner) world.m(i, j, k);
+            world.e(i, j, k, Block.MOB_SPAWNER.id);
+            TileEntityMobSpawner tileentitymobspawner = (TileEntityMobSpawner) world.getTileEntity(i, j, k);
 
             tileentitymobspawner.a(this.b(random));
             return true;
@@ -123,7 +123,7 @@ public class WorldGenDungeons extends WorldGenerator {
     private ItemStack a(Random random) {
         int i = random.nextInt(11);
 
-        return i == 0 ? new ItemStack(Item.SADDLE) : (i == 1 ? new ItemStack(Item.IRON_INGOT, random.nextInt(4) + 1) : (i == 2 ? new ItemStack(Item.BREAD) : (i == 3 ? new ItemStack(Item.WHEAT, random.nextInt(4) + 1) : (i == 4 ? new ItemStack(Item.SULPHUR, random.nextInt(4) + 1) : (i == 5 ? new ItemStack(Item.STRING, random.nextInt(4) + 1) : (i == 6 ? new ItemStack(Item.BUCKET) : (i == 7 && random.nextInt(100) == 0 ? new ItemStack(Item.GOLDEN_APPLE) : (i == 8 && random.nextInt(2) == 0 ? new ItemStack(Item.REDSTONE, random.nextInt(4) + 1) : (i == 9 && random.nextInt(10) == 0 ? new ItemStack(Item.c[Item.GOLD_RECORD.ba + random.nextInt(2)]) : null)))))))));
+        return i == 0 ? new ItemStack(Item.SADDLE) : (i == 1 ? new ItemStack(Item.IRON_INGOT, random.nextInt(4) + 1) : (i == 2 ? new ItemStack(Item.BREAD) : (i == 3 ? new ItemStack(Item.WHEAT, random.nextInt(4) + 1) : (i == 4 ? new ItemStack(Item.SULPHUR, random.nextInt(4) + 1) : (i == 5 ? new ItemStack(Item.STRING, random.nextInt(4) + 1) : (i == 6 ? new ItemStack(Item.BUCKET) : (i == 7 && random.nextInt(100) == 0 ? new ItemStack(Item.GOLDEN_APPLE) : (i == 8 && random.nextInt(2) == 0 ? new ItemStack(Item.REDSTONE, random.nextInt(4) + 1) : (i == 9 && random.nextInt(10) == 0 ? new ItemStack(Item.byId[Item.GOLD_RECORD.id + random.nextInt(2)]) : null)))))))));
     }
 
     private String b(Random random) {
