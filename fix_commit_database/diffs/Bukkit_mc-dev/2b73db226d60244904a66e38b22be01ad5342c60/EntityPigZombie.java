@@ -10,7 +10,7 @@ public class EntityPigZombie extends EntityZombie {
 
     public EntityPigZombie(World world) {
         super(world);
-        this.aP = "/mob/pigzombie.png";
+        this.texture = "/mob/pigzombie.png";
         this.bC = 0.5F;
         this.c = 5;
         this.ae = true;
@@ -19,14 +19,14 @@ public class EntityPigZombie extends EntityZombie {
     public void b_() {
         this.bC = this.d != null ? 0.95F : 0.5F;
         if (this.b > 0 && --this.b == 0) {
-            this.l.a(this, "mob.zombiepig.zpigangry", this.i() * 2.0F, ((this.W.nextFloat() - this.W.nextFloat()) * 0.2F + 1.0F) * 1.8F);
+            this.world.a(this, "mob.zombiepig.zpigangry", this.i() * 2.0F, ((this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F) * 1.8F);
         }
 
         super.b_();
     }
 
     public boolean b() {
-        return this.l.k > 0 && this.l.a(this.z) && this.l.a((Entity) this, this.z).size() == 0 && !this.l.b(this.z);
+        return this.world.k > 0 && this.world.a(this.boundingBox) && this.world.a((Entity) this, this.boundingBox).size() == 0 && !this.world.b(this.boundingBox);
     }
 
     public void a(NBTTagCompound nbttagcompound) {
@@ -49,7 +49,7 @@ public class EntityPigZombie extends EntityZombie {
 
     public boolean a(Entity entity, int i) {
         if (entity instanceof EntityHuman) {
-            List list = this.l.b((Entity) this, this.z.b(32.0D, 32.0D, 32.0D));
+            List list = this.world.b((Entity) this, this.boundingBox.b(32.0D, 32.0D, 32.0D));
 
             for (int j = 0; j < list.size(); ++j) {
                 Entity entity1 = (Entity) list.get(j);
@@ -69,8 +69,8 @@ public class EntityPigZombie extends EntityZombie {
 
     private void g(Entity entity) {
         this.d = entity;
-        this.a = 400 + this.W.nextInt(400);
-        this.b = this.W.nextInt(40);
+        this.a = 400 + this.random.nextInt(400);
+        this.b = this.random.nextInt(40);
     }
 
     protected String e() {
@@ -86,6 +86,6 @@ public class EntityPigZombie extends EntityZombie {
     }
 
     protected int h() {
-        return Item.GRILLED_PORK.ba;
+        return Item.GRILLED_PORK.id;
     }
 }
