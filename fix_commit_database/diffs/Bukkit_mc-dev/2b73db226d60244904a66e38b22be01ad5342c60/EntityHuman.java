@@ -4,17 +4,17 @@ import java.util.List;
 
 public abstract class EntityHuman extends EntityLiving {
 
-    public InventoryPlayer an = new InventoryPlayer(this);
-    public Container ao;
-    public Container ap;
+    public InventoryPlayer inventory = new InventoryPlayer(this);
+    public Container defaultContainer;
+    public Container activeContainer;
     public byte aq = 0;
     public int ar = 0;
     public float as;
     public float at;
     public boolean au = false;
     public int av = 0;
-    public String aw;
-    public int ax;
+    public String name;
+    public int dimension;
     public double ay;
     public double az;
     public double aA;
@@ -22,58 +22,58 @@ public abstract class EntityHuman extends EntityLiving {
     public double aC;
     public double aD;
     private int a = 0;
-    public EntityFish aE = null;
+    public EntityFish hookedFish = null;
 
     public EntityHuman(World world) {
         super(world);
-        this.ao = new ContainerPlayer(this.an, !world.z);
-        this.ap = this.ao;
-        this.H = 1.62F;
-        this.c((double) world.m + 0.5D, (double) (world.n + 1), (double) world.o + 0.5D, 0.0F, 0.0F);
-        this.aZ = 20;
+        this.defaultContainer = new ContainerPlayer(this.inventory, !world.isStatic);
+        this.activeContainer = this.defaultContainer;
+        this.height = 1.62F;
+        this.c((double) world.spawnX + 0.5D, (double) (world.spawnY + 1), (double) world.spawnZ + 0.5D, 0.0F, 0.0F);
+        this.health = 20;
         this.aS = "humanoid";
         this.aR = 180.0F;
-        this.Y = 20;
-        this.aP = "/mob/char.png";
+        this.maxFireTicks = 20;
+        this.texture = "/mob/char.png";
     }
 
     public void b_() {
         super.b_();
-        if (!this.l.z && this.ap != null && !this.ap.b(this)) {
+        if (!this.world.isStatic && this.activeContainer != null && !this.activeContainer.b(this)) {
             this.L();
-            this.ap = this.ao;
+            this.activeContainer = this.defaultContainer;
         }
 
         this.ay = this.aB;
         this.az = this.aC;
         this.aA = this.aD;
-        double d0 = this.p - this.aB;
-        double d1 = this.q - this.aC;
-        double d2 = this.r - this.aD;
+        double d0 = this.locX - this.aB;
+        double d1 = this.locY - this.aC;
+        double d2 = this.locZ - this.aD;
         double d3 = 10.0D;
 
         if (d0 > d3) {
-            this.ay = this.aB = this.p;
+            this.ay = this.aB = this.locX;
         }
 
         if (d2 > d3) {
-            this.aA = this.aD = this.r;
+            this.aA = this.aD = this.locZ;
         }
 
         if (d1 > d3) {
-            this.az = this.aC = this.q;
+            this.az = this.aC = this.locY;
         }
 
         if (d0 < -d3) {
-            this.ay = this.aB = this.p;
+            this.ay = this.aB = this.locX;
         }
 
         if (d2 < -d3) {
-            this.aA = this.aD = this.r;
+            this.aA = this.aD = this.locZ;
         }
 
         if (d1 < -d3) {
-            this.az = this.aC = this.q;
+            this.az = this.aC = this.locY;
         }
 
         this.aB += d0 * 0.25D;
@@ -82,7 +82,7 @@ public abstract class EntityHuman extends EntityLiving {
     }
 
     protected void L() {
-        this.ap = this.ao;
+        this.activeContainer = this.defaultContainer;
     }
 
     public void D() {
@@ -106,38 +106,38 @@ public abstract class EntityHuman extends EntityLiving {
     }
 
     public void o() {
-        if (this.l.k == 0 && this.aZ < 20 && this.X % 20 * 12 == 0) {
+        if (this.world.k == 0 && this.health < 20 && this.ticksLived % 20 * 12 == 0) {
             this.d(1);
         }
 
-        this.an.f();
+        this.inventory.f();
         this.as = this.at;
         super.o();
-        float f = MathHelper.a(this.s * this.s + this.u * this.u);
-        float f1 = (float) Math.atan(-this.t * 0.20000000298023224D) * 15.0F;
+        float f = MathHelper.a(this.motX * this.motX + this.motZ * this.motZ);
+        float f1 = (float) Math.atan(-this.motY * 0.20000000298023224D) * 15.0F;
 
         if (f > 0.1F) {
             f = 0.1F;
         }
 
-        if (!this.A || this.aZ <= 0) {
+        if (!this.onGround || this.health <= 0) {
             f = 0.0F;
         }
 
-        if (this.A || this.aZ <= 0) {
+        if (this.onGround || this.health <= 0) {
             f1 = 0.0F;
         }
 
         this.at += (f - this.at) * 0.4F;
         this.bh += (f1 - this.bh) * 0.8F;
-        if (this.aZ > 0) {
-            List list = this.l.b((Entity) this, this.z.b(1.0D, 0.0D, 1.0D));
+        if (this.health > 0) {
+            List list = this.world.b((Entity) this, this.boundingBox.b(1.0D, 0.0D, 1.0D));
 
             if (list != null) {
                 for (int i = 0; i < list.size(); ++i) {
                     Entity entity = (Entity) list.get(i);
 
-                    if (!entity.G) {
+                    if (!entity.dead) {
                         this.j(entity);
                     }
                 }
@@ -152,21 +152,21 @@ public abstract class EntityHuman extends EntityLiving {
     public void f(Entity entity) {
         super.f(entity);
         this.a(0.2F, 0.2F);
-        this.a(this.p, this.q, this.r);
-        this.t = 0.10000000149011612D;
-        if (this.aw.equals("Notch")) {
+        this.a(this.locX, this.locY, this.locZ);
+        this.motY = 0.10000000149011612D;
+        if (this.name.equals("Notch")) {
             this.a(new ItemStack(Item.APPLE, 1), true);
         }
 
-        this.an.h();
+        this.inventory.h();
         if (entity != null) {
-            this.s = (double) (-MathHelper.b((this.bd + this.v) * 3.1415927F / 180.0F) * 0.1F);
-            this.u = (double) (-MathHelper.a((this.bd + this.v) * 3.1415927F / 180.0F) * 0.1F);
+            this.motX = (double) (-MathHelper.b((this.bd + this.yaw) * 3.1415927F / 180.0F) * 0.1F);
+            this.motZ = (double) (-MathHelper.a((this.bd + this.yaw) * 3.1415927F / 180.0F) * 0.1F);
         } else {
-            this.s = this.u = 0.0D;
+            this.motX = this.motZ = 0.0D;
         }
 
-        this.H = 0.1F;
+        this.height = 0.1F;
     }
 
     public void b(Entity entity, int i) {
@@ -174,7 +174,7 @@ public abstract class EntityHuman extends EntityLiving {
     }
 
     public void O() {
-        this.a(this.an.b(this.an.c, 1), false);
+        this.a(this.inventory.b(this.inventory.c, 1), false);
     }
 
     public void b(ItemStack itemstack) {
@@ -183,30 +183,30 @@ public abstract class EntityHuman extends EntityLiving {
 
     public void a(ItemStack itemstack, boolean flag) {
         if (itemstack != null) {
-            EntityItem entityitem = new EntityItem(this.l, this.p, this.q - 0.30000001192092896D + (double) this.w(), this.r, itemstack);
+            EntityItem entityitem = new EntityItem(this.world, this.locX, this.locY - 0.30000001192092896D + (double) this.w(), this.locZ, itemstack);
 
             entityitem.c = 40;
             float f = 0.1F;
             float f1;
 
             if (flag) {
-                f1 = this.W.nextFloat() * 0.5F;
-                float f2 = this.W.nextFloat() * 3.1415927F * 2.0F;
+                f1 = this.random.nextFloat() * 0.5F;
+                float f2 = this.random.nextFloat() * 3.1415927F * 2.0F;
 
-                entityitem.s = (double) (-MathHelper.a(f2) * f1);
-                entityitem.u = (double) (MathHelper.b(f2) * f1);
-                entityitem.t = 0.20000000298023224D;
+                entityitem.motX = (double) (-MathHelper.a(f2) * f1);
+                entityitem.motZ = (double) (MathHelper.b(f2) * f1);
+                entityitem.motY = 0.20000000298023224D;
             } else {
                 f = 0.3F;
-                entityitem.s = (double) (-MathHelper.a(this.v / 180.0F * 3.1415927F) * MathHelper.b(this.w / 180.0F * 3.1415927F) * f);
-                entityitem.u = (double) (MathHelper.b(this.v / 180.0F * 3.1415927F) * MathHelper.b(this.w / 180.0F * 3.1415927F) * f);
-                entityitem.t = (double) (-MathHelper.a(this.w / 180.0F * 3.1415927F) * f + 0.1F);
+                entityitem.motX = (double) (-MathHelper.a(this.yaw / 180.0F * 3.1415927F) * MathHelper.b(this.pitch / 180.0F * 3.1415927F) * f);
+                entityitem.motZ = (double) (MathHelper.b(this.yaw / 180.0F * 3.1415927F) * MathHelper.b(this.pitch / 180.0F * 3.1415927F) * f);
+                entityitem.motY = (double) (-MathHelper.a(this.pitch / 180.0F * 3.1415927F) * f + 0.1F);
                 f = 0.02F;
-                f1 = this.W.nextFloat() * 3.1415927F * 2.0F;
-                f *= this.W.nextFloat();
-                entityitem.s += Math.cos((double) f1) * (double) f;
-                entityitem.t += (double) ((this.W.nextFloat() - this.W.nextFloat()) * 0.1F);
-                entityitem.u += Math.sin((double) f1) * (double) f;
+                f1 = this.random.nextFloat() * 3.1415927F * 2.0F;
+                f *= this.random.nextFloat();
+                entityitem.motX += Math.cos((double) f1) * (double) f;
+                entityitem.motY += (double) ((this.random.nextFloat() - this.random.nextFloat()) * 0.1F);
+                entityitem.motZ += Math.sin((double) f1) * (double) f;
             }
 
             this.a(entityitem);
@@ -214,17 +214,17 @@ public abstract class EntityHuman extends EntityLiving {
     }
 
     protected void a(EntityItem entityitem) {
-        this.l.a((Entity) entityitem);
+        this.world.a((Entity) entityitem);
     }
 
     public float a(Block block) {
-        float f = this.an.a(block);
+        float f = this.inventory.a(block);
 
-        if (this.a(Material.f)) {
+        if (this.a(Material.WATER)) {
             f /= 5.0F;
         }
 
-        if (!this.A) {
+        if (!this.onGround) {
             f /= 5.0F;
         }
 
@@ -232,21 +232,21 @@ public abstract class EntityHuman extends EntityLiving {
     }
 
     public boolean b(Block block) {
-        return this.an.b(block);
+        return this.inventory.b(block);
     }
 
     public void b(NBTTagCompound nbttagcompound) {
         super.b(nbttagcompound);
         NBTTagList nbttaglist = nbttagcompound.k("Inventory");
 
-        this.an.b(nbttaglist);
-        this.ax = nbttagcompound.d("Dimension");
+        this.inventory.b(nbttaglist);
+        this.dimension = nbttagcompound.d("Dimension");
     }
 
     public void a(NBTTagCompound nbttagcompound) {
         super.a(nbttagcompound);
-        nbttagcompound.a("Inventory", (NBTBase) this.an.a(new NBTTagList()));
-        nbttagcompound.a("Dimension", this.ax);
+        nbttagcompound.a("Inventory", (NBTBase) this.inventory.a(new NBTTagList()));
+        nbttagcompound.a("Dimension", this.dimension);
     }
 
     public void a(IInventory iinventory) {}
@@ -261,19 +261,19 @@ public abstract class EntityHuman extends EntityLiving {
 
     public boolean a(Entity entity, int i) {
         this.bw = 0;
-        if (this.aZ <= 0) {
+        if (this.health <= 0) {
             return false;
         } else {
             if (entity instanceof EntityMonster || entity instanceof EntityArrow) {
-                if (this.l.k == 0) {
+                if (this.world.k == 0) {
                     i = 0;
                 }
 
-                if (this.l.k == 1) {
+                if (this.world.k == 1) {
                     i = i / 3 + 1;
                 }
 
-                if (this.l.k == 3) {
+                if (this.world.k == 3) {
                     i = i * 3 / 2;
                 }
             }
@@ -283,10 +283,10 @@ public abstract class EntityHuman extends EntityLiving {
     }
 
     protected void e(int i) {
-        int j = 25 - this.an.g();
+        int j = 25 - this.inventory.g();
         int k = i * j + this.a;
 
-        this.an.c(i);
+        this.inventory.c(i);
         i = k / 25;
         this.a = k % 25;
         super.e(i);
@@ -304,7 +304,7 @@ public abstract class EntityHuman extends EntityLiving {
 
             if (itemstack != null && entity instanceof EntityLiving) {
                 itemstack.b((EntityLiving) entity);
-                if (itemstack.a <= 0) {
+                if (itemstack.count <= 0) {
                     itemstack.a(this);
                     this.Q();
                 }
@@ -313,15 +313,15 @@ public abstract class EntityHuman extends EntityLiving {
     }
 
     public ItemStack P() {
-        return this.an.e();
+        return this.inventory.e();
     }
 
     public void Q() {
-        this.an.a(this.an.c, (ItemStack) null);
+        this.inventory.a(this.inventory.c, (ItemStack) null);
     }
 
     public double F() {
-        return (double) (this.H - 0.5F);
+        return (double) (this.height - 0.5F);
     }
 
     public void K() {
@@ -330,7 +330,7 @@ public abstract class EntityHuman extends EntityLiving {
     }
 
     public void h(Entity entity) {
-        int i = this.an.a(entity);
+        int i = this.inventory.a(entity);
 
         if (i > 0) {
             entity.a(this, i);
@@ -338,7 +338,7 @@ public abstract class EntityHuman extends EntityLiving {
 
             if (itemstack != null && entity instanceof EntityLiving) {
                 itemstack.a((EntityLiving) entity);
-                if (itemstack.a <= 0) {
+                if (itemstack.count <= 0) {
                     itemstack.a(this);
                     this.Q();
                 }
@@ -350,9 +350,9 @@ public abstract class EntityHuman extends EntityLiving {
 
     public void q() {
         super.q();
-        this.ao.a(this);
-        if (this.ap != null) {
-            this.ap.a(this);
+        this.defaultContainer.a(this);
+        if (this.activeContainer != null) {
+            this.activeContainer.a(this);
         }
     }
 }
