@@ -5,8 +5,8 @@ import java.util.Random;
 public class BlockIce extends BlockBreakable {
 
     public BlockIce(int i, int j) {
-        super(i, j, Material.r, false);
-        this.bu = 0.98F;
+        super(i, j, Material.ICE, false);
+        this.frictionFactor = 0.98F;
         this.a(true);
     }
 
@@ -15,10 +15,10 @@ public class BlockIce extends BlockBreakable {
     }
 
     public void b(World world, int i, int j, int k) {
-        Material material = world.c(i, j - 1, k);
+        Material material = world.getMaterial(i, j - 1, k);
 
-        if (material.c() || material.d()) {
-            world.e(i, j, k, Block.WATER.bi);
+        if (material.isSolid() || material.isLiquid()) {
+            world.e(i, j, k, Block.WATER.id);
         }
     }
 
@@ -27,9 +27,9 @@ public class BlockIce extends BlockBreakable {
     }
 
     public void a(World world, int i, int j, int k, Random random) {
-        if (world.a(EnumSkyBlock.BLOCK, i, j, k) > 11 - Block.q[this.bi]) {
-            this.a_(world, i, j, k, world.b(i, j, k));
-            world.e(i, j, k, Block.STATIONARY_WATER.bi);
+        if (world.a(EnumSkyBlock.BLOCK, i, j, k) > 11 - Block.q[this.id]) {
+            this.a_(world, i, j, k, world.getData(i, j, k));
+            world.e(i, j, k, Block.STATIONARY_WATER.id);
         }
     }
 }
