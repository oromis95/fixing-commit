@@ -12,12 +12,12 @@ public class WorldGenReed extends WorldGenerator {
             int j1 = j;
             int k1 = k + random.nextInt(4) - random.nextInt(4);
 
-            if (world.e(i1, j, k1) && (world.c(i1 - 1, j - 1, k1) == Material.f || world.c(i1 + 1, j - 1, k1) == Material.f || world.c(i1, j - 1, k1 - 1) == Material.f || world.c(i1, j - 1, k1 + 1) == Material.f)) {
+            if (world.isEmpty(i1, j, k1) && (world.getMaterial(i1 - 1, j - 1, k1) == Material.WATER || world.getMaterial(i1 + 1, j - 1, k1) == Material.WATER || world.getMaterial(i1, j - 1, k1 - 1) == Material.WATER || world.getMaterial(i1, j - 1, k1 + 1) == Material.WATER)) {
                 int l1 = 2 + random.nextInt(random.nextInt(3) + 1);
 
                 for (int i2 = 0; i2 < l1; ++i2) {
                     if (Block.SUGAR_CANE_BLOCK.f(world, i1, j1 + i2, k1)) {
-                        world.b(i1, j1 + i2, k1, Block.SUGAR_CANE_BLOCK.bi);
+                        world.setTypeId(i1, j1 + i2, k1, Block.SUGAR_CANE_BLOCK.id);
                     }
                 }
             }
