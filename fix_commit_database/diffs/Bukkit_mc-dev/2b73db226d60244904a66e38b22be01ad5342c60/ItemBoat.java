@@ -4,16 +4,16 @@ public class ItemBoat extends Item {
 
     public ItemBoat(int i) {
         super(i);
-        this.bb = 1;
+        this.maxStackSize = 1;
     }
 
     public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
         float f = 1.0F;
-        float f1 = entityhuman.y + (entityhuman.w - entityhuman.y) * f;
-        float f2 = entityhuman.x + (entityhuman.v - entityhuman.x) * f;
-        double d0 = entityhuman.m + (entityhuman.p - entityhuman.m) * (double) f;
-        double d1 = entityhuman.n + (entityhuman.q - entityhuman.n) * (double) f + 1.62D - (double) entityhuman.H;
-        double d2 = entityhuman.o + (entityhuman.r - entityhuman.o) * (double) f;
+        float f1 = entityhuman.lastPitch + (entityhuman.pitch - entityhuman.lastPitch) * f;
+        float f2 = entityhuman.lastYaw + (entityhuman.yaw - entityhuman.lastYaw) * f;
+        double d0 = entityhuman.lastX + (entityhuman.locX - entityhuman.lastX) * (double) f;
+        double d1 = entityhuman.lastY + (entityhuman.locY - entityhuman.lastY) * (double) f + 1.62D - (double) entityhuman.height;
+        double d2 = entityhuman.lastZ + (entityhuman.locZ - entityhuman.lastZ) * (double) f;
         Vec3D vec3d = Vec3D.b(d0, d1, d2);
         float f3 = MathHelper.b(-f2 * 0.017453292F - 3.1415927F);
         float f4 = MathHelper.a(-f2 * 0.017453292F - 3.1415927F);
@@ -33,11 +33,11 @@ public class ItemBoat extends Item {
                 int j = movingobjectposition.c;
                 int k = movingobjectposition.d;
 
-                if (!world.z) {
+                if (!world.isStatic) {
                     world.a((Entity) (new EntityBoat(world, (double) ((float) i + 0.5F), (double) ((float) j + 1.5F), (double) ((float) k + 0.5F))));
                 }
 
-                --itemstack.a;
+                --itemstack.count;
             }
 
             return itemstack;
