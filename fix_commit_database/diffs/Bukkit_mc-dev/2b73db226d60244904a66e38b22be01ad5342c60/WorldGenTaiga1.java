@@ -32,8 +32,8 @@ public class WorldGenTaiga1 extends WorldGenerator {
                 for (i2 = i - l2; i2 <= i + l2 && flag; ++i2) {
                     for (j2 = k - l2; j2 <= k + l2 && flag; ++j2) {
                         if (l1 >= 0 && l1 < 128) {
-                            k2 = world.a(i2, l1, j2);
-                            if (k2 != 0 && k2 != Block.LEAVES.bi) {
+                            k2 = world.getTypeId(i2, l1, j2);
+                            if (k2 != 0 && k2 != Block.LEAVES.id) {
                                 flag = false;
                             }
                         } else {
@@ -46,9 +46,9 @@ public class WorldGenTaiga1 extends WorldGenerator {
             if (!flag) {
                 return false;
             } else {
-                l1 = world.a(i, j - 1, k);
-                if ((l1 == Block.GRASS.bi || l1 == Block.DIRT.bi) && j < 128 - l - 1) {
-                    world.b(i, j - 1, k, Block.DIRT.bi);
+                l1 = world.getTypeId(i, j - 1, k);
+                if ((l1 == Block.GRASS.id || l1 == Block.DIRT.id) && j < 128 - l - 1) {
+                    world.setTypeId(i, j - 1, k, Block.DIRT.id);
                     l2 = 0;
 
                     for (i2 = j + l; i2 >= j + i1; --i2) {
@@ -58,8 +58,8 @@ public class WorldGenTaiga1 extends WorldGenerator {
                             for (int i3 = k - l2; i3 <= k + l2; ++i3) {
                                 int j3 = i3 - k;
 
-                                if ((Math.abs(k2) != l2 || Math.abs(j3) != l2 || l2 <= 0) && !Block.o[world.a(j2, i2, i3)]) {
-                                    world.a(j2, i2, i3, Block.LEAVES.bi, 1);
+                                if ((Math.abs(k2) != l2 || Math.abs(j3) != l2 || l2 <= 0) && !Block.o[world.getTypeId(j2, i2, i3)]) {
+                                    world.setTypeIdAndData(j2, i2, i3, Block.LEAVES.id, 1);
                                 }
                             }
                         }
@@ -72,9 +72,9 @@ public class WorldGenTaiga1 extends WorldGenerator {
                     }
 
                     for (i2 = 0; i2 < l - 1; ++i2) {
-                        j2 = world.a(i, j + i2, k);
-                        if (j2 == 0 || j2 == Block.LEAVES.bi) {
-                            world.a(i, j + i2, k, Block.LOG.bi, 1);
+                        j2 = world.getTypeId(i, j + i2, k);
+                        if (j2 == 0 || j2 == Block.LEAVES.id) {
+                            world.setTypeIdAndData(i, j + i2, k, Block.LOG.id, 1);
                         }
                     }
 
