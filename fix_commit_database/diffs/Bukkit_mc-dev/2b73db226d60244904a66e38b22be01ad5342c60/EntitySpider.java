@@ -4,13 +4,13 @@ public class EntitySpider extends EntityMonster {
 
     public EntitySpider(World world) {
         super(world);
-        this.aP = "/mob/spider.png";
+        this.texture = "/mob/spider.png";
         this.a(1.4F, 0.9F);
         this.bC = 0.8F;
     }
 
     public double k() {
-        return (double) this.J * 0.75D - 0.5D;
+        return (double) this.width * 0.75D - 0.5D;
     }
 
     protected Entity l() {
@@ -19,7 +19,7 @@ public class EntitySpider extends EntityMonster {
         if (f < 0.5F) {
             double d0 = 16.0D;
 
-            return this.l.a(this, d0);
+            return this.world.a(this, d0);
         } else {
             return null;
         }
@@ -40,18 +40,18 @@ public class EntitySpider extends EntityMonster {
     protected void a(Entity entity, float f) {
         float f1 = this.b(1.0F);
 
-        if (f1 > 0.5F && this.W.nextInt(100) == 0) {
+        if (f1 > 0.5F && this.random.nextInt(100) == 0) {
             this.d = null;
         } else {
-            if (f > 2.0F && f < 6.0F && this.W.nextInt(10) == 0) {
-                if (this.A) {
-                    double d0 = entity.p - this.p;
-                    double d1 = entity.r - this.r;
+            if (f > 2.0F && f < 6.0F && this.random.nextInt(10) == 0) {
+                if (this.onGround) {
+                    double d0 = entity.locX - this.locX;
+                    double d1 = entity.locZ - this.locZ;
                     float f2 = MathHelper.a(d0 * d0 + d1 * d1);
 
-                    this.s = d0 / (double) f2 * 0.5D * 0.800000011920929D + this.s * 0.20000000298023224D;
-                    this.u = d1 / (double) f2 * 0.5D * 0.800000011920929D + this.u * 0.20000000298023224D;
-                    this.t = 0.4000000059604645D;
+                    this.motX = d0 / (double) f2 * 0.5D * 0.800000011920929D + this.motX * 0.20000000298023224D;
+                    this.motZ = d1 / (double) f2 * 0.5D * 0.800000011920929D + this.motZ * 0.20000000298023224D;
+                    this.motY = 0.4000000059604645D;
                 }
             } else {
                 super.a(entity, f);
@@ -68,7 +68,7 @@ public class EntitySpider extends EntityMonster {
     }
 
     protected int h() {
-        return Item.STRING.ba;
+        return Item.STRING.id;
     }
 
     public boolean m() {
