@@ -4,13 +4,13 @@ public class ItemBow extends Item {
 
     public ItemBow(int i) {
         super(i);
-        this.bb = 1;
+        this.maxStackSize = 1;
     }
 
     public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
-        if (entityhuman.an.b(Item.ARROW.ba)) {
+        if (entityhuman.inventory.b(Item.ARROW.id)) {
             world.a(entityhuman, "random.bow", 1.0F, 1.0F / (b.nextFloat() * 0.4F + 0.8F));
-            if (!world.z) {
+            if (!world.isStatic) {
                 world.a((Entity) (new EntityArrow(world, entityhuman)));
             }
         }
