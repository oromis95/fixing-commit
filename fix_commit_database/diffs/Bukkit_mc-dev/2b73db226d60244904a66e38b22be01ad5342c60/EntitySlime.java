@@ -9,18 +9,18 @@ public class EntitySlime extends EntityLiving implements IMonster {
 
     public EntitySlime(World world) {
         super(world);
-        this.aP = "/mob/slime.png";
-        this.c = 1 << this.W.nextInt(3);
-        this.H = 0.0F;
-        this.d = this.W.nextInt(20) + 10;
+        this.texture = "/mob/slime.png";
+        this.c = 1 << this.random.nextInt(3);
+        this.height = 0.0F;
+        this.d = this.random.nextInt(20) + 10;
         this.a(this.c);
     }
 
     public void a(int i) {
         this.c = i;
         this.a(0.6F * (float) i, 0.6F * (float) i);
-        this.aZ = i * i;
-        this.a(this.p, this.q, this.r);
+        this.health = i * i;
+        this.a(this.locX, this.locY, this.locZ);
     }
 
     public void a(NBTTagCompound nbttagcompound) {
@@ -35,21 +35,21 @@ public class EntitySlime extends EntityLiving implements IMonster {
 
     public void b_() {
         this.b = this.a;
-        boolean flag = this.A;
+        boolean flag = this.onGround;
 
         super.b_();
-        if (this.A && !flag) {
+        if (this.onGround && !flag) {
             for (int i = 0; i < this.c * 8; ++i) {
-                float f = this.W.nextFloat() * 3.1415927F * 2.0F;
-                float f1 = this.W.nextFloat() * 0.5F + 0.5F;
+                float f = this.random.nextFloat() * 3.1415927F * 2.0F;
+                float f1 = this.random.nextFloat() * 0.5F + 0.5F;
                 float f2 = MathHelper.a(f) * (float) this.c * 0.5F * f1;
                 float f3 = MathHelper.b(f) * (float) this.c * 0.5F * f1;
 
-                this.l.a("slime", this.p + (double) f2, this.z.b, this.r + (double) f3, 0.0D, 0.0D, 0.0D);
+                this.world.a("slime", this.locX + (double) f2, this.boundingBox.b, this.locZ + (double) f3, 0.0D, 0.0D, 0.0D);
             }
 
             if (this.c > 2) {
-                this.l.a(this, "mob.slime", this.i(), ((this.W.nextFloat() - this.W.nextFloat()) * 0.2F + 1.0F) / 0.8F);
+                this.world.a(this, "mob.slime", this.i(), ((this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F) / 0.8F);
             }
 
             this.a = -0.5F;
@@ -59,44 +59,44 @@ public class EntitySlime extends EntityLiving implements IMonster {
     }
 
     protected void d() {
-        EntityHuman entityhuman = this.l.a(this, 16.0D);
+        EntityHuman entityhuman = this.world.a(this, 16.0D);
 
         if (entityhuman != null) {
             this.b(entityhuman, 10.0F);
         }
 
-        if (this.A && this.d-- <= 0) {
-            this.d = this.W.nextInt(20) + 10;
+        if (this.onGround && this.d-- <= 0) {
+            this.d = this.random.nextInt(20) + 10;
             if (entityhuman != null) {
                 this.d /= 3;
             }
 
             this.bA = true;
             if (this.c > 1) {
-                this.l.a(this, "mob.slime", this.i(), ((this.W.nextFloat() - this.W.nextFloat()) * 0.2F + 1.0F) * 0.8F);
+                this.world.a(this, "mob.slime", this.i(), ((this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F) * 0.8F);
             }
 
             this.a = 1.0F;
-            this.bx = 1.0F - this.W.nextFloat() * 2.0F;
+            this.bx = 1.0F - this.random.nextFloat() * 2.0F;
             this.by = (float) (1 * this.c);
         } else {
             this.bA = false;
-            if (this.A) {
+            if (this.onGround) {
                 this.bx = this.by = 0.0F;
             }
         }
     }
 
     public void q() {
-        if (this.c > 1 && this.aZ == 0) {
+        if (this.c > 1 && this.health == 0) {
             for (int i = 0; i < 4; ++i) {
                 float f = ((float) (i % 2) - 0.5F) * (float) this.c / 4.0F;
                 float f1 = ((float) (i / 2) - 0.5F) * (float) this.c / 4.0F;
-                EntitySlime entityslime = new EntitySlime(this.l);
+                EntitySlime entityslime = new EntitySlime(this.world);
 
                 entityslime.a(this.c / 2);
-                entityslime.c(this.p + (double) f, this.q + 0.5D, this.r + (double) f1, this.W.nextFloat() * 360.0F, 0.0F);
-                this.l.a((Entity) entityslime);
+                entityslime.c(this.locX + (double) f, this.locY + 0.5D, this.locZ + (double) f1, this.random.nextFloat() * 360.0F, 0.0F);
+                this.world.a((Entity) entityslime);
             }
         }
 
@@ -105,7 +105,7 @@ public class EntitySlime extends EntityLiving implements IMonster {
 
     public void b(EntityHuman entityhuman) {
         if (this.c > 1 && this.i(entityhuman) && (double) this.a(entityhuman) < 0.6D * (double) this.c && entityhuman.a(this, this.c)) {
-            this.l.a(this, "mob.slimeattack", 1.0F, (this.W.nextFloat() - this.W.nextFloat()) * 0.2F + 1.0F);
+            this.world.a(this, "mob.slimeattack", 1.0F, (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
         }
     }
 
@@ -118,13 +118,13 @@ public class EntitySlime extends EntityLiving implements IMonster {
     }
 
     protected int h() {
-        return this.c == 1 ? Item.SLIME_BALL.ba : 0;
+        return this.c == 1 ? Item.SLIME_BALL.id : 0;
     }
 
     public boolean b() {
-        Chunk chunk = this.l.b(MathHelper.b(this.p), MathHelper.b(this.r));
+        Chunk chunk = this.world.b(MathHelper.b(this.locX), MathHelper.b(this.locZ));
 
-        return (this.c == 1 || this.l.k > 0) && this.W.nextInt(10) == 0 && chunk.a(987234911L).nextInt(10) == 0 && this.q < 16.0D;
+        return (this.c == 1 || this.world.k > 0) && this.random.nextInt(10) == 0 && chunk.a(987234911L).nextInt(10) == 0 && this.locY < 16.0D;
     }
 
     protected float i() {
