@@ -12,17 +12,17 @@ public class ItemDye extends Item {
 
     public boolean a(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
         if (itemstack.h() == 15) {
-            int i1 = world.a(i, j, k);
+            int i1 = world.getTypeId(i, j, k);
 
-            if (i1 == Block.SAPLING.bi) {
+            if (i1 == Block.SAPLING.id) {
                 ((BlockSapling) Block.SAPLING).b(world, i, j, k, world.l);
-                --itemstack.a;
+                --itemstack.count;
                 return true;
             }
 
-            if (i1 == Block.CROPS.bi) {
+            if (i1 == Block.CROPS.id) {
                 ((BlockCrops) Block.CROPS).c(world, i, j, k);
-                --itemstack.a;
+                --itemstack.count;
                 return true;
             }
         }
@@ -37,7 +37,7 @@ public class ItemDye extends Item {
 
             if (!entitysheep.f_() && entitysheep.e_() != i) {
                 entitysheep.a(i);
-                --itemstack.a;
+                --itemstack.count;
             }
         }
     }
