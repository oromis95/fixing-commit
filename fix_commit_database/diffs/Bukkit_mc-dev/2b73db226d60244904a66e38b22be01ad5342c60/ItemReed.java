@@ -6,11 +6,11 @@ public class ItemReed extends Item {
 
     public ItemReed(int i, Block block) {
         super(i);
-        this.a = block.bi;
+        this.a = block.id;
     }
 
     public boolean a(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
-        if (world.a(i, j, k) == Block.SNOW.bi) {
+        if (world.getTypeId(i, j, k) == Block.SNOW.id) {
             l = 0;
         } else {
             if (l == 0) {
@@ -38,16 +38,16 @@ public class ItemReed extends Item {
             }
         }
 
-        if (itemstack.a == 0) {
+        if (itemstack.count == 0) {
             return false;
         } else {
             if (world.a(this.a, i, j, k, false)) {
-                Block block = Block.m[this.a];
+                Block block = Block.byId[this.a];
 
                 if (world.e(i, j, k, this.a)) {
-                    Block.m[this.a].c(world, i, j, k, l);
-                    world.a((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), block.br.c(), (block.br.a() + 1.0F) / 2.0F, block.br.b() * 0.8F);
-                    --itemstack.a;
+                    Block.byId[this.a].c(world, i, j, k, l);
+                    world.a((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), block.stepSound.c(), (block.stepSound.a() + 1.0F) / 2.0F, block.stepSound.b() * 0.8F);
+                    --itemstack.count;
                 }
             }
 
