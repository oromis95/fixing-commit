@@ -13,16 +13,16 @@ public class FurnaceRecipes {
     }
 
     private FurnaceRecipes() {
-        this.a(Block.IRON_ORE.bi, new ItemStack(Item.IRON_INGOT));
-        this.a(Block.GOLD_ORE.bi, new ItemStack(Item.GOLD_INGOT));
-        this.a(Block.DIAMOND_ORE.bi, new ItemStack(Item.DIAMOND));
-        this.a(Block.SAND.bi, new ItemStack(Block.GLASS));
-        this.a(Item.PORK.ba, new ItemStack(Item.GRILLED_PORK));
-        this.a(Item.RAW_FISH.ba, new ItemStack(Item.COOKED_FISH));
-        this.a(Block.COBBLESTONE.bi, new ItemStack(Block.STONE));
-        this.a(Item.CLAY_BALL.ba, new ItemStack(Item.CLAY_BRICK));
-        this.a(Block.CACTUS.bi, new ItemStack(Item.INK_SACK, 1, 2));
-        this.a(Block.LOG.bi, new ItemStack(Item.COAL, 1, 1));
+        this.a(Block.IRON_ORE.id, new ItemStack(Item.IRON_INGOT));
+        this.a(Block.GOLD_ORE.id, new ItemStack(Item.GOLD_INGOT));
+        this.a(Block.DIAMOND_ORE.id, new ItemStack(Item.DIAMOND));
+        this.a(Block.SAND.id, new ItemStack(Block.GLASS));
+        this.a(Item.PORK.id, new ItemStack(Item.GRILLED_PORK));
+        this.a(Item.RAW_FISH.id, new ItemStack(Item.COOKED_FISH));
+        this.a(Block.COBBLESTONE.id, new ItemStack(Block.STONE));
+        this.a(Item.CLAY_BALL.id, new ItemStack(Item.CLAY_BRICK));
+        this.a(Block.CACTUS.id, new ItemStack(Item.INK_SACK, 1, 2));
+        this.a(Block.LOG.id, new ItemStack(Item.COAL, 1, 1));
     }
 
     public void a(int i, ItemStack itemstack) {
