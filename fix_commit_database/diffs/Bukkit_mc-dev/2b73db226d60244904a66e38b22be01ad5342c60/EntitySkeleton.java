@@ -6,7 +6,7 @@ public class EntitySkeleton extends EntityMonster {
 
     public EntitySkeleton(World world) {
         super(world);
-        this.aP = "/mob/skeleton.png";
+        this.texture = "/mob/skeleton.png";
     }
 
     protected String e() {
@@ -22,11 +22,11 @@ public class EntitySkeleton extends EntityMonster {
     }
 
     public void o() {
-        if (this.l.b()) {
+        if (this.world.b()) {
             float f = this.b(1.0F);
 
-            if (f > 0.5F && this.l.i(MathHelper.b(this.p), MathHelper.b(this.q), MathHelper.b(this.r)) && this.W.nextFloat() * 30.0F < (f - 0.4F) * 2.0F) {
-                this.Z = 300;
+            if (f > 0.5F && this.world.i(MathHelper.b(this.locX), MathHelper.b(this.locY), MathHelper.b(this.locZ)) && this.random.nextFloat() * 30.0F < (f - 0.4F) * 2.0F) {
+                this.fireTicks = 300;
             }
         }
 
@@ -35,23 +35,23 @@ public class EntitySkeleton extends EntityMonster {
 
     protected void a(Entity entity, float f) {
         if (f < 10.0F) {
-            double d0 = entity.p - this.p;
-            double d1 = entity.r - this.r;
+            double d0 = entity.locX - this.locX;
+            double d1 = entity.locZ - this.locZ;
 
-            if (this.bf == 0) {
-                EntityArrow entityarrow = new EntityArrow(this.l, this);
+            if (this.attackTicks == 0) {
+                EntityArrow entityarrow = new EntityArrow(this.world, this);
 
-                ++entityarrow.q;
-                double d2 = entity.q - 0.20000000298023224D - entityarrow.q;
+                ++entityarrow.locY;
+                double d2 = entity.locY - 0.20000000298023224D - entityarrow.locY;
                 float f1 = MathHelper.a(d0 * d0 + d1 * d1) * 0.2F;
 
-                this.l.a(this, "random.bow", 1.0F, 1.0F / (this.W.nextFloat() * 0.4F + 0.8F));
-                this.l.a((Entity) entityarrow);
+                this.world.a(this, "random.bow", 1.0F, 1.0F / (this.random.nextFloat() * 0.4F + 0.8F));
+                this.world.a((Entity) entityarrow);
                 entityarrow.a(d0, d2 + (double) f1, d1, 0.6F, 12.0F);
-                this.bf = 30;
+                this.attackTicks = 30;
             }
 
-            this.v = (float) (Math.atan2(d1, d0) * 180.0D / 3.1415927410125732D) - 90.0F;
+            this.yaw = (float) (Math.atan2(d1, d0) * 180.0D / 3.1415927410125732D) - 90.0F;
             this.e = true;
         }
     }
@@ -65,22 +65,22 @@ public class EntitySkeleton extends EntityMonster {
     }
 
     protected int h() {
-        return Item.ARROW.ba;
+        return Item.ARROW.id;
     }
 
     protected void g_() {
-        int i = this.W.nextInt(3);
+        int i = this.random.nextInt(3);
 
         int j;
 
         for (j = 0; j < i; ++j) {
-            this.a(Item.ARROW.ba, 1);
+            this.a(Item.ARROW.id, 1);
         }
 
-        i = this.W.nextInt(3);
+        i = this.random.nextInt(3);
 
         for (j = 0; j < i; ++j) {
-            this.a(Item.BONE.ba, 1);
+            this.a(Item.BONE.id, 1);
         }
     }
 }
