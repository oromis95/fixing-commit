@@ -7,14 +7,14 @@ public class ItemRecord extends Item {
     protected ItemRecord(int i, String s) {
         super(i);
         this.a = s;
-        this.bb = 1;
+        this.maxStackSize = 1;
     }
 
     public boolean a(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
-        if (world.a(i, j, k) == Block.JUKEBOX.bi && world.b(i, j, k) == 0) {
-            world.c(i, j, k, this.ba - Item.GOLD_RECORD.ba + 1);
+        if (world.getTypeId(i, j, k) == Block.JUKEBOX.id && world.getData(i, j, k) == 0) {
+            world.c(i, j, k, this.id - Item.GOLD_RECORD.id + 1);
             world.a(this.a, i, j, k);
-            --itemstack.a;
+            --itemstack.count;
             return true;
         } else {
             return false;
