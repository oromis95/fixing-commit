@@ -6,9 +6,9 @@ public class BlockDoor extends Block {
 
     protected BlockDoor(int i, Material material) {
         super(i, material);
-        this.bh = 97;
-        if (material == Material.e) {
-            ++this.bh;
+        this.textureId = 97;
+        if (material == Material.ORE) {
+            ++this.textureId;
         }
 
         float f = 0.5F;
@@ -27,7 +27,7 @@ public class BlockDoor extends Block {
     }
 
     public void a(IBlockAccess iblockaccess, int i, int j, int k) {
-        this.c(this.d(iblockaccess.b(i, j, k)));
+        this.c(this.d(iblockaccess.getData(i, j, k)));
     }
 
     public void c(int i) {
@@ -56,19 +56,19 @@ public class BlockDoor extends Block {
     }
 
     public boolean a(World world, int i, int j, int k, EntityHuman entityhuman) {
-        if (this.bt == Material.e) {
+        if (this.material == Material.ORE) {
             return true;
         } else {
-            int l = world.b(i, j, k);
+            int l = world.getData(i, j, k);
 
             if ((l & 8) != 0) {
-                if (world.a(i, j - 1, k) == this.bi) {
+                if (world.getTypeId(i, j - 1, k) == this.id) {
                     this.a(world, i, j - 1, k, entityhuman);
                 }
 
                 return true;
             } else {
-                if (world.a(i, j + 1, k) == this.bi) {
+                if (world.getTypeId(i, j + 1, k) == this.id) {
                     world.c(i, j + 1, k, (l ^ 4) + 8);
                 }
 
@@ -86,17 +86,17 @@ public class BlockDoor extends Block {
     }
 
     public void a(World world, int i, int j, int k, boolean flag) {
-        int l = world.b(i, j, k);
+        int l = world.getData(i, j, k);
 
         if ((l & 8) != 0) {
-            if (world.a(i, j - 1, k) == this.bi) {
+            if (world.getTypeId(i, j - 1, k) == this.id) {
                 this.a(world, i, j - 1, k, flag);
             }
         } else {
-            boolean flag1 = (world.b(i, j, k) & 4) > 0;
+            boolean flag1 = (world.getData(i, j, k) & 4) > 0;
 
             if (flag1 != flag) {
-                if (world.a(i, j + 1, k) == this.bi) {
+                if (world.getTypeId(i, j + 1, k) == this.id) {
                     world.c(i, j + 1, k, (l ^ 4) + 8);
                 }
 
@@ -112,20 +112,20 @@ public class BlockDoor extends Block {
     }
 
     public void b(World world, int i, int j, int k, int l) {
-        int i1 = world.b(i, j, k);
+        int i1 = world.getData(i, j, k);
 
         if ((i1 & 8) != 0) {
-            if (world.a(i, j - 1, k) != this.bi) {
+            if (world.getTypeId(i, j - 1, k) != this.id) {
                 world.e(i, j, k, 0);
             }
 
-            if (l > 0 && Block.m[l].c()) {
+            if (l > 0 && Block.byId[l].c()) {
                 this.b(world, i, j - 1, k, l);
             }
         } else {
             boolean flag = false;
 
-            if (world.a(i, j + 1, k) != this.bi) {
+            if (world.getTypeId(i, j + 1, k) != this.id) {
                 world.e(i, j, k, 0);
                 flag = true;
             }
@@ -133,14 +133,14 @@ public class BlockDoor extends Block {
             if (!world.d(i, j - 1, k)) {
                 world.e(i, j, k, 0);
                 flag = true;
-                if (world.a(i, j + 1, k) == this.bi) {
+                if (world.getTypeId(i, j + 1, k) == this.id) {
                     world.e(i, j + 1, k, 0);
                 }
             }
 
             if (flag) {
                 this.a_(world, i, j, k, i1);
-            } else if (l > 0 && Block.m[l].c()) {
+            } else if (l > 0 && Block.byId[l].c()) {
                 boolean flag1 = world.p(i, j, k) || world.p(i, j + 1, k);
 
                 this.a(world, i, j, k, flag1);
@@ -149,7 +149,7 @@ public class BlockDoor extends Block {
     }
 
     public int a(int i, Random random) {
-        return (i & 8) != 0 ? 0 : (this.bt == Material.e ? Item.IRON_DOOR.ba : Item.WOOD_DOOR.ba);
+        return (i & 8) != 0 ? 0 : (this.material == Material.ORE ? Item.IRON_DOOR.id : Item.WOOD_DOOR.id);
     }
 
     public MovingObjectPosition a(World world, int i, int j, int k, Vec3D vec3d, Vec3D vec3d1) {
