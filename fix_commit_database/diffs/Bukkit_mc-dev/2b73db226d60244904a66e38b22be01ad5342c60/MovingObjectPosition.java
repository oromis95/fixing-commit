@@ -22,6 +22,6 @@ public class MovingObjectPosition {
     public MovingObjectPosition(Entity entity) {
         this.a = EnumMovingObjectType.ENTITY;
         this.g = entity;
-        this.f = Vec3D.b(entity.p, entity.q, entity.r);
+        this.f = Vec3D.b(entity.locX, entity.locY, entity.locZ);
     }
 }
