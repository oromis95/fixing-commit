@@ -7,11 +7,11 @@ public class ItemBlock extends Item {
     public ItemBlock(int i) {
         super(i);
         this.a = i + 256;
-        this.b(Block.m[i + 256].a(2));
+        this.b(Block.byId[i + 256].a(2));
     }
 
     public boolean a(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
-        if (world.a(i, j, k) == Block.SNOW.bi) {
+        if (world.getTypeId(i, j, k) == Block.SNOW.id) {
             l = 0;
         } else {
             if (l == 0) {
@@ -39,17 +39,17 @@ public class ItemBlock extends Item {
             }
         }
 
-        if (itemstack.a == 0) {
+        if (itemstack.count == 0) {
             return false;
         } else {
             if (world.a(this.a, i, j, k, false)) {
-                Block block = Block.m[this.a];
+                Block block = Block.byId[this.a];
 
                 if (world.b(i, j, k, this.a, this.a(itemstack.h()))) {
-                    Block.m[this.a].c(world, i, j, k, l);
-                    Block.m[this.a].a(world, i, j, k, (EntityLiving) entityhuman);
-                    world.a((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), block.br.c(), (block.br.a() + 1.0F) / 2.0F, block.br.b() * 0.8F);
-                    --itemstack.a;
+                    Block.byId[this.a].c(world, i, j, k, l);
+                    Block.byId[this.a].a(world, i, j, k, (EntityLiving) entityhuman);
+                    world.a((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), block.stepSound.c(), (block.stepSound.a() + 1.0F) / 2.0F, block.stepSound.b() * 0.8F);
+                    --itemstack.count;
                 }
             }
 
@@ -58,6 +58,6 @@ public class ItemBlock extends Item {
     }
 
     public String a() {
-        return Block.m[this.a].e();
+        return Block.byId[this.a].e();
     }
 }
