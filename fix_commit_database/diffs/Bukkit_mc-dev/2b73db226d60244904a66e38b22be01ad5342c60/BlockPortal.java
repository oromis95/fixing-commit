@@ -5,7 +5,7 @@ import java.util.Random;
 public class BlockPortal extends BlockBreakable {
 
     public BlockPortal(int i, int j) {
-        super(i, j, Material.x, false);
+        super(i, j, Material.PORTAL, false);
     }
 
     public AxisAlignedBB d(World world, int i, int j, int k) {
@@ -16,7 +16,7 @@ public class BlockPortal extends BlockBreakable {
         float f;
         float f1;
 
-        if (iblockaccess.a(i - 1, j, k) != this.bi && iblockaccess.a(i + 1, j, k) != this.bi) {
+        if (iblockaccess.getTypeId(i - 1, j, k) != this.id && iblockaccess.getTypeId(i + 1, j, k) != this.id) {
             f = 0.125F;
             f1 = 0.5F;
             this.a(0.5F - f, 0.0F, 0.5F - f1, 0.5F + f, 1.0F, 0.5F + f1);
@@ -35,11 +35,11 @@ public class BlockPortal extends BlockBreakable {
         byte b0 = 0;
         byte b1 = 0;
 
-        if (world.a(i - 1, j, k) == Block.OBSIDIAN.bi || world.a(i + 1, j, k) == Block.OBSIDIAN.bi) {
+        if (world.getTypeId(i - 1, j, k) == Block.OBSIDIAN.id || world.getTypeId(i + 1, j, k) == Block.OBSIDIAN.id) {
             b0 = 1;
         }
 
-        if (world.a(i, j, k - 1) == Block.OBSIDIAN.bi || world.a(i, j, k + 1) == Block.OBSIDIAN.bi) {
+        if (world.getTypeId(i, j, k - 1) == Block.OBSIDIAN.id || world.getTypeId(i, j, k + 1) == Block.OBSIDIAN.id) {
             b1 = 1;
         }
 
@@ -47,7 +47,7 @@ public class BlockPortal extends BlockBreakable {
         if (b0 == b1) {
             return false;
         } else {
-            if (world.a(i - b0, j, k - b1) == 0) {
+            if (world.getTypeId(i - b0, j, k - b1) == 0) {
                 i -= b0;
                 k -= b1;
             }
@@ -60,13 +60,13 @@ public class BlockPortal extends BlockBreakable {
                     boolean flag = l == -1 || l == 2 || i1 == -1 || i1 == 3;
 
                     if (l != -1 && l != 2 || i1 != -1 && i1 != 3) {
-                        int j1 = world.a(i + b0 * l, j + i1, k + b1 * l);
+                        int j1 = world.getTypeId(i + b0 * l, j + i1, k + b1 * l);
 
                         if (flag) {
-                            if (j1 != Block.OBSIDIAN.bi) {
+                            if (j1 != Block.OBSIDIAN.id) {
                                 return false;
                             }
-                        } else if (j1 != 0 && j1 != Block.FIRE.bi) {
+                        } else if (j1 != 0 && j1 != Block.FIRE.id) {
                             return false;
                         }
                     }
@@ -77,7 +77,7 @@ public class BlockPortal extends BlockBreakable {
 
             for (l = 0; l < 2; ++l) {
                 for (i1 = 0; i1 < 3; ++i1) {
-                    world.e(i + b0 * l, j + i1, k + b1 * l, Block.PORTAL.bi);
+                    world.e(i + b0 * l, j + i1, k + b1 * l, Block.PORTAL.id);
                 }
             }
 
@@ -90,33 +90,33 @@ public class BlockPortal extends BlockBreakable {
         byte b0 = 0;
         byte b1 = 1;
 
-        if (world.a(i - 1, j, k) == this.bi || world.a(i + 1, j, k) == this.bi) {
+        if (world.getTypeId(i - 1, j, k) == this.id || world.getTypeId(i + 1, j, k) == this.id) {
             b0 = 1;
             b1 = 0;
         }
 
         int i1;
 
-        for (i1 = j; world.a(i, i1 - 1, k) == this.bi; --i1) {
+        for (i1 = j; world.getTypeId(i, i1 - 1, k) == this.id; --i1) {
             ;
         }
 
-        if (world.a(i, i1 - 1, k) != Block.OBSIDIAN.bi) {
+        if (world.getTypeId(i, i1 - 1, k) != Block.OBSIDIAN.id) {
             world.e(i, j, k, 0);
         } else {
             int j1;
 
-            for (j1 = 1; j1 < 4 && world.a(i, i1 + j1, k) == this.bi; ++j1) {
+            for (j1 = 1; j1 < 4 && world.getTypeId(i, i1 + j1, k) == this.id; ++j1) {
                 ;
             }
 
-            if (j1 == 3 && world.a(i, i1 + j1, k) == Block.OBSIDIAN.bi) {
-                boolean flag = world.a(i - 1, j, k) == this.bi || world.a(i + 1, j, k) == this.bi;
-                boolean flag1 = world.a(i, j, k - 1) == this.bi || world.a(i, j, k + 1) == this.bi;
+            if (j1 == 3 && world.getTypeId(i, i1 + j1, k) == Block.OBSIDIAN.id) {
+                boolean flag = world.getTypeId(i - 1, j, k) == this.id || world.getTypeId(i + 1, j, k) == this.id;
+                boolean flag1 = world.getTypeId(i, j, k - 1) == this.id || world.getTypeId(i, j, k + 1) == this.id;
 
                 if (flag && flag1) {
                     world.e(i, j, k, 0);
-                } else if ((world.a(i + b0, j, k + b1) != Block.OBSIDIAN.bi || world.a(i - b0, j, k - b1) != this.bi) && (world.a(i - b0, j, k - b1) != Block.OBSIDIAN.bi || world.a(i + b0, j, k + b1) != this.bi)) {
+                } else if ((world.getTypeId(i + b0, j, k + b1) != Block.OBSIDIAN.id || world.getTypeId(i - b0, j, k - b1) != this.id) && (world.getTypeId(i - b0, j, k - b1) != Block.OBSIDIAN.id || world.getTypeId(i + b0, j, k + b1) != this.id)) {
                     world.e(i, j, k, 0);
                 }
             } else {
@@ -134,7 +134,7 @@ public class BlockPortal extends BlockBreakable {
     }
 
     public void a(World world, int i, int j, int k, Entity entity) {
-        if (!world.z) {
+        if (!world.isStatic) {
             entity.H();
         }
     }
