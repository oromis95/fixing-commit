@@ -3,8 +3,8 @@ package net.minecraft.server;
 public class BlockSponge extends Block {
 
     protected BlockSponge(int i) {
-        super(i, Material.j);
-        this.bh = 48;
+        super(i, Material.SPONGE);
+        this.textureId = 48;
     }
 
     public void e(World world, int i, int j, int k) {
@@ -13,7 +13,7 @@ public class BlockSponge extends Block {
         for (int l = i - b0; l <= i + b0; ++l) {
             for (int i1 = j - b0; i1 <= j + b0; ++i1) {
                 for (int j1 = k - b0; j1 <= k + b0; ++j1) {
-                    if (world.c(l, i1, j1) == Material.f) {
+                    if (world.getMaterial(l, i1, j1) == Material.WATER) {
                         ;
                     }
                 }
@@ -27,7 +27,7 @@ public class BlockSponge extends Block {
         for (int l = i - b0; l <= i + b0; ++l) {
             for (int i1 = j - b0; i1 <= j + b0; ++i1) {
                 for (int j1 = k - b0; j1 <= k + b0; ++j1) {
-                    world.h(l, i1, j1, world.a(l, i1, j1));
+                    world.h(l, i1, j1, world.getTypeId(l, i1, j1));
                 }
             }
         }
