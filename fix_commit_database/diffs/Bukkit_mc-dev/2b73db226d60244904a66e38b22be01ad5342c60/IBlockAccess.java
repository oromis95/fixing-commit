@@ -2,11 +2,11 @@ package net.minecraft.server;
 
 public interface IBlockAccess {
 
-    int a(int i, int j, int k);
+    int getTypeId(int i, int j, int k);
 
-    int b(int i, int j, int k);
+    int getData(int i, int j, int k);
 
-    Material c(int i, int j, int k);
+    Material getMaterial(int i, int j, int k);
 
     boolean d(int i, int j, int k);
 }
