@@ -17,7 +17,7 @@ public class EntityCreature extends EntityLiving {
         if (this.d == null) {
             this.d = this.l();
             if (this.d != null) {
-                this.a = this.l.a(this, this.d, f);
+                this.a = this.world.a(this, this.d, f);
             }
         } else if (!this.d.B()) {
             this.d = null;
@@ -29,9 +29,9 @@ public class EntityCreature extends EntityLiving {
             }
         }
 
-        if (!this.e && this.d != null && (this.a == null || this.W.nextInt(20) == 0)) {
-            this.a = this.l.a(this, this.d, f);
-        } else if (this.a == null && this.W.nextInt(80) == 0 || this.W.nextInt(80) == 0) {
+        if (!this.e && this.d != null && (this.a == null || this.random.nextInt(20) == 0)) {
+            this.a = this.world.a(this, this.d, f);
+        } else if (this.a == null && this.random.nextInt(80) == 0 || this.random.nextInt(80) == 0) {
             boolean flag = false;
             int i = -1;
             int j = -1;
@@ -39,9 +39,9 @@ public class EntityCreature extends EntityLiving {
             float f2 = -99999.0F;
 
             for (int l = 0; l < 10; ++l) {
-                int i1 = MathHelper.b(this.p + (double) this.W.nextInt(13) - 6.0D);
-                int j1 = MathHelper.b(this.q + (double) this.W.nextInt(7) - 3.0D);
-                int k1 = MathHelper.b(this.r + (double) this.W.nextInt(13) - 6.0D);
+                int i1 = MathHelper.b(this.locX + (double) this.random.nextInt(13) - 6.0D);
+                int j1 = MathHelper.b(this.locY + (double) this.random.nextInt(7) - 3.0D);
+                int k1 = MathHelper.b(this.locZ + (double) this.random.nextInt(13) - 6.0D);
                 float f3 = this.a(i1, j1, k1);
 
                 if (f3 > f2) {
@@ -54,20 +54,20 @@ public class EntityCreature extends EntityLiving {
             }
 
             if (flag) {
-                this.a = this.l.a(this, i, j, k, 10.0F);
+                this.a = this.world.a(this, i, j, k, 10.0F);
             }
         }
 
-        int l1 = MathHelper.b(this.z.b);
+        int l1 = MathHelper.b(this.boundingBox.b);
         boolean flag1 = this.v();
         boolean flag2 = this.x();
 
-        this.w = 0.0F;
-        if (this.a != null && this.W.nextInt(100) != 0) {
+        this.pitch = 0.0F;
+        if (this.a != null && this.random.nextInt(100) != 0) {
             Vec3D vec3d = this.a.a(this);
-            double d0 = (double) (this.I * 2.0F);
+            double d0 = (double) (this.length * 2.0F);
 
-            while (vec3d != null && vec3d.d(this.p, vec3d.b, this.r) < d0 * d0) {
+            while (vec3d != null && vec3d.d(this.locX, vec3d.b, this.locZ) < d0 * d0) {
                 this.a.a();
                 if (this.a.b()) {
                     vec3d = null;
@@ -79,11 +79,11 @@ public class EntityCreature extends EntityLiving {
 
             this.bA = false;
             if (vec3d != null) {
-                double d1 = vec3d.a - this.p;
-                double d2 = vec3d.c - this.r;
+                double d1 = vec3d.a - this.locX;
+                double d2 = vec3d.c - this.locZ;
                 double d3 = vec3d.b - (double) l1;
                 float f4 = (float) (Math.atan2(d2, d1) * 180.0D / 3.1415927410125732D) - 90.0F;
-                float f5 = f4 - this.v;
+                float f5 = f4 - this.yaw;
 
                 for (this.by = this.bC; f5 < -180.0F; f5 += 360.0F) {
                     ;
@@ -101,14 +101,14 @@ public class EntityCreature extends EntityLiving {
                     f5 = -30.0F;
                 }
 
-                this.v += f5;
+                this.yaw += f5;
                 if (this.e && this.d != null) {
-                    double d4 = this.d.p - this.p;
-                    double d5 = this.d.r - this.r;
-                    float f6 = this.v;
+                    double d4 = this.d.locX - this.locX;
+                    double d5 = this.d.locZ - this.locZ;
+                    float f6 = this.yaw;
 
-                    this.v = (float) (Math.atan2(d5, d4) * 180.0D / 3.1415927410125732D) - 90.0F;
-                    f5 = (f6 - this.v + 90.0F) * 3.1415927F / 180.0F;
+                    this.yaw = (float) (Math.atan2(d5, d4) * 180.0D / 3.1415927410125732D) - 90.0F;
+                    f5 = (f6 - this.yaw + 90.0F) * 3.1415927F / 180.0F;
                     this.bx = -MathHelper.a(f5) * this.by * 1.0F;
                     this.by = MathHelper.b(f5) * this.by * 1.0F;
                 }
@@ -126,7 +126,7 @@ public class EntityCreature extends EntityLiving {
                 this.bA = true;
             }
 
-            if (this.W.nextFloat() < 0.8F && (flag1 || flag2)) {
+            if (this.random.nextFloat() < 0.8F && (flag1 || flag2)) {
                 this.bA = true;
             }
         } else {
@@ -146,9 +146,9 @@ public class EntityCreature extends EntityLiving {
     }
 
     public boolean b() {
-        int i = MathHelper.b(this.p);
-        int j = MathHelper.b(this.z.b);
-        int k = MathHelper.b(this.r);
+        int i = MathHelper.b(this.locX);
+        int j = MathHelper.b(this.boundingBox.b);
+        int k = MathHelper.b(this.locZ);
 
         return super.b() && this.a(i, j, k) >= 0.0F;
     }
