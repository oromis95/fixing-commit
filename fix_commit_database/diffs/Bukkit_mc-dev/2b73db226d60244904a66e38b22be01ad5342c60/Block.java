@@ -14,7 +14,7 @@ public class Block {
     public static final StepSound j = new StepSoundStone("stone", 1.0F, 1.0F);
     public static final StepSound k = new StepSound("cloth", 1.0F, 1.0F);
     public static final StepSound l = new StepSoundSand("sand", 1.0F, 1.0F);
-    public static final Block[] m = new Block[256];
+    public static final Block[] byId = new Block[256];
     public static final boolean[] n = new boolean[256];
     public static final boolean[] o = new boolean[256];
     public static final boolean[] p = new boolean[256];
@@ -24,14 +24,14 @@ public class Block {
     public static final Block STONE = (new BlockStone(1, 1)).c(1.5F).b(10.0F).a(h).a("stone");
     public static final BlockGrass GRASS = (BlockGrass) (new BlockGrass(2)).c(0.6F).a(g).a("grass");
     public static final Block DIRT = (new BlockDirt(3, 2)).c(0.5F).a(f).a("dirt");
-    public static final Block COBBLESTONE = (new Block(4, 16, Material.d)).c(2.0F).b(10.0F).a(h).a("stonebrick");
-    public static final Block WOOD = (new Block(5, 4, Material.c)).c(2.0F).b(5.0F).a(e).a("wood");
+    public static final Block COBBLESTONE = (new Block(4, 16, Material.STONE)).c(2.0F).b(10.0F).a(h).a("stonebrick");
+    public static final Block WOOD = (new Block(5, 4, Material.WOOD)).c(2.0F).b(5.0F).a(e).a("wood");
     public static final Block SAPLING = (new BlockSapling(6, 15)).c(0.0F).a(g).a("sapling");
-    public static final Block BEDROCK = (new Block(7, 17, Material.d)).c(-1.0F).b(6000000.0F).a(h).a("bedrock");
-    public static final Block WATER = (new BlockFlowing(8, Material.f)).c(100.0F).e(3).a("water");
-    public static final Block STATIONARY_WATER = (new BlockStationary(9, Material.f)).c(100.0F).e(3).a("water");
-    public static final Block LAVA = (new BlockFlowing(10, Material.g)).c(0.0F).a(1.0F).e(255).a("lava");
-    public static final Block STATIONARY_LAVA = (new BlockStationary(11, Material.g)).c(100.0F).a(1.0F).e(255).a("lava");
+    public static final Block BEDROCK = (new Block(7, 17, Material.STONE)).c(-1.0F).b(6000000.0F).a(h).a("bedrock");
+    public static final Block WATER = (new BlockFlowing(8, Material.WATER)).c(100.0F).e(3).a("water");
+    public static final Block STATIONARY_WATER = (new BlockStationary(9, Material.WATER)).c(100.0F).e(3).a("water");
+    public static final Block LAVA = (new BlockFlowing(10, Material.LAVA)).c(0.0F).a(1.0F).e(255).a("lava");
+    public static final Block STATIONARY_LAVA = (new BlockStationary(11, Material.LAVA)).c(100.0F).a(1.0F).e(255).a("lava");
     public static final Block SAND = (new BlockSand(12, 18)).c(0.5F).a(l).a("sand");
     public static final Block GRAVEL = (new BlockGravel(13, 19)).c(0.6F).a(f).a("gravel");
     public static final Block GOLD_ORE = (new BlockOre(14, 32)).c(3.0F).b(5.0F).a(h).a("oreGold");
@@ -40,9 +40,9 @@ public class Block {
     public static final Block LOG = (new BlockLog(17)).c(2.0F).a(e).a("log");
     public static final BlockLeaves LEAVES = (BlockLeaves) (new BlockLeaves(18, 52)).c(0.2F).e(1).a(g).a("leaves");
     public static final Block SPONGE = (new BlockSponge(19)).c(0.6F).a(g).a("sponge");
-    public static final Block GLASS = (new BlockGlass(20, 49, Material.o, false)).c(0.3F).a(j).a("glass");
+    public static final Block GLASS = (new BlockGlass(20, 49, Material.SHATTERABLE, false)).c(0.3F).a(j).a("glass");
     public static final Block LAPIS_ORE = (new BlockOre(21, 160)).c(3.0F).b(5.0F).a(h).a("oreLapis");
-    public static final Block LAPIS_BLOCK = (new Block(22, 144, Material.d)).c(3.0F).b(5.0F).a(h).a("blockLapis");
+    public static final Block LAPIS_BLOCK = (new Block(22, 144, Material.STONE)).c(3.0F).b(5.0F).a(h).a("blockLapis");
     public static final Block DISPENSER = (new BlockDispenser(23)).c(3.5F).a(h).a("dispenser");
     public static final Block SANDSTONE = (new BlockSandStone(24)).a(h).c(0.8F).a("sandStone");
     public static final Block NOTE_BLOCK = (new BlockNote(25)).c(0.8F).a("musicBlock");
@@ -65,10 +65,10 @@ public class Block {
     public static final Block IRON_BLOCK = (new BlockOreBlock(42, 22)).c(5.0F).b(10.0F).a(i).a("blockIron");
     public static final Block DOUBLE_STEP = (new BlockStep(43, true)).c(2.0F).b(10.0F).a(h).a("stoneSlab");
     public static final Block STEP = (new BlockStep(44, false)).c(2.0F).b(10.0F).a(h).a("stoneSlab");
-    public static final Block BRICK = (new Block(45, 7, Material.d)).c(2.0F).b(10.0F).a(h).a("brick");
+    public static final Block BRICK = (new Block(45, 7, Material.STONE)).c(2.0F).b(10.0F).a(h).a("brick");
     public static final Block TNT = (new BlockTNT(46, 8)).c(0.0F).a(g).a("tnt");
     public static final Block BOOKSHELF = (new BlockBookshelf(47, 35)).c(1.5F).a(e).a("bookshelf");
-    public static final Block MOSSY_COBBLESTONE = (new Block(48, 36, Material.d)).c(2.0F).b(10.0F).a(h).a("stoneMoss");
+    public static final Block MOSSY_COBBLESTONE = (new Block(48, 36, Material.STONE)).c(2.0F).b(10.0F).a(h).a("stoneMoss");
     public static final Block OBSIDIAN = (new BlockObsidian(49, 37)).c(10.0F).b(2000.0F).a(h).a("obsidian");
     public static final Block TORCH = (new BlockTorch(50, 80)).c(0.0F).a(0.9375F).a(e).a("torch");
     public static final BlockFire FIRE = (BlockFire) (new BlockFire(51, 31)).c(0.0F).a(1.0F).a(e).a("fire");
@@ -84,20 +84,20 @@ public class Block {
     public static final Block FURNACE = (new BlockFurnace(61, false)).c(3.5F).a(h).a("furnace");
     public static final Block BURNING_FURNACE = (new BlockFurnace(62, true)).c(3.5F).a(h).a(0.875F).a("furnace");
     public static final Block SIGN_POST = (new BlockSign(63, TileEntitySign.class, true)).c(1.0F).a(e).a("sign");
-    public static final Block WOODEN_DOOR = (new BlockDoor(64, Material.c)).c(3.0F).a(e).a("doorWood");
+    public static final Block WOODEN_DOOR = (new BlockDoor(64, Material.WOOD)).c(3.0F).a(e).a("doorWood");
     public static final Block LADDER = (new BlockLadder(65, 83)).c(0.4F).a(e).a("ladder");
     public static final Block RAILS = (new BlockMinecartTrack(66, 128)).c(0.7F).a(i).a("rail");
     public static final Block COBBLESTONE_STAIRS = (new BlockStairs(67, COBBLESTONE)).a("stairsStone");
     public static final Block WALL_SIGN = (new BlockSign(68, TileEntitySign.class, false)).c(1.0F).a(e).a("sign");
     public static final Block LEVER = (new BlockLever(69, 96)).c(0.5F).a(e).a("lever");
-    public static final Block STONE_PLATE = (new BlockPressurePlate(70, STONE.bh, EnumMobType.MOBS)).c(0.5F).a(h).a("pressurePlate");
-    public static final Block IRON_DOOR_BLOCK = (new BlockDoor(71, Material.e)).c(5.0F).a(i).a("doorIron");
-    public static final Block WOOD_PLATE = (new BlockPressurePlate(72, WOOD.bh, EnumMobType.EVERYTHING)).c(0.5F).a(e).a("pressurePlate");
+    public static final Block STONE_PLATE = (new BlockPressurePlate(70, STONE.textureId, EnumMobType.MOBS)).c(0.5F).a(h).a("pressurePlate");
+    public static final Block IRON_DOOR_BLOCK = (new BlockDoor(71, Material.ORE)).c(5.0F).a(i).a("doorIron");
+    public static final Block WOOD_PLATE = (new BlockPressurePlate(72, WOOD.textureId, EnumMobType.EVERYTHING)).c(0.5F).a(e).a("pressurePlate");
     public static final Block REDSTONE_ORE = (new BlockRedstoneOre(73, 51, false)).c(3.0F).b(5.0F).a(h).a("oreRedstone");
     public static final Block GLOWING_REDSTONE_ORE = (new BlockRedstoneOre(74, 51, true)).a(0.625F).c(3.0F).b(5.0F).a(h).a("oreRedstone");
     public static final Block REDSTONE_TORCH_OFF = (new BlockRedstoneTorch(75, 115, false)).c(0.0F).a(e).a("notGate");
     public static final Block REDSTONE_TORCH_ON = (new BlockRedstoneTorch(76, 99, true)).c(0.0F).a(0.5F).a(e).a("notGate");
-    public static final Block STONE_BUTTON = (new BlockButton(77, STONE.bh)).c(0.5F).a(h).a("button");
+    public static final Block STONE_BUTTON = (new BlockButton(77, STONE.textureId)).c(0.5F).a(h).a("button");
     public static final Block SNOW = (new BlockSnow(78, 66)).c(0.1F).a(k).a("snow");
     public static final Block ICE = (new BlockIce(79, 67)).c(0.5F).e(3).a(j).a("ice");
     public static final Block SNOW_BLOCK = (new BlockSnowBlock(80, 66)).c(0.2F).a(k).a("snow");
@@ -109,36 +109,36 @@ public class Block {
     public static final Block PUMPKIN = (new BlockPumpkin(86, 102, false)).c(1.0F).a(e).a("pumpkin");
     public static final Block NETHERRACK = (new BlockBloodStone(87, 103)).c(0.4F).a(h).a("hellrock");
     public static final Block SOUL_SAND = (new BlockSlowSand(88, 104)).c(0.5F).a(l).a("hellsand");
-    public static final Block GLOWSTONE = (new BlockLightStone(89, 105, Material.o)).c(0.3F).a(j).a(1.0F).a("lightgem");
+    public static final Block GLOWSTONE = (new BlockLightStone(89, 105, Material.SHATTERABLE)).c(0.3F).a(j).a(1.0F).a("lightgem");
     public static final BlockPortal PORTAL = (BlockPortal) (new BlockPortal(90, 14)).c(-1.0F).a(j).a(0.75F).a("portal");
     public static final Block JACK_O_LANTERN = (new BlockPumpkin(91, 102, true)).c(1.0F).a(e).a(1.0F).a("litpumpkin");
     public static final Block CAKE_BLOCK = (new BlockCake(92, 121)).c(0.5F).a(k).a("cake");
-    public int bh;
-    public final int bi;
-    protected float bj;
-    protected float bk;
-    public double bl;
-    public double bm;
-    public double bn;
-    public double bo;
-    public double bp;
-    public double bq;
-    public StepSound br;
+    public int textureId;
+    public final int id;
+    protected float strength;
+    protected float durability;
+    public double minX;
+    public double minY;
+    public double minZ;
+    public double maxX;
+    public double maxY;
+    public double maxZ;
+    public StepSound stepSound;
     public float bs;
-    public final Material bt;
-    public float bu;
-    private String a;
+    public final Material material;
+    public float frictionFactor;
+    private String name;
 
     protected Block(int i, Material material) {
-        this.br = d;
+        this.stepSound = d;
         this.bs = 1.0F;
-        this.bu = 0.6F;
-        if (m[i] != null) {
-            throw new IllegalArgumentException("Slot " + i + " is already occupied by " + m[i] + " when adding " + this);
+        this.frictionFactor = 0.6F;
+        if (byId[i] != null) {
+            throw new IllegalArgumentException("Slot " + i + " is already occupied by " + byId[i] + " when adding " + this);
         } else {
-            this.bt = material;
-            m[i] = this;
-            this.bi = i;
+            this.material = material;
+            byId[i] = this;
+            this.id = i;
             this.a(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
             o[i] = this.a();
             q[i] = this.a() ? 255 : 0;
@@ -149,26 +149,26 @@ public class Block {
 
     protected Block(int i, int j, Material material) {
         this(i, material);
-        this.bh = j;
+        this.textureId = j;
     }
 
     protected Block a(StepSound stepsound) {
-        this.br = stepsound;
+        this.stepSound = stepsound;
         return this;
     }
 
     protected Block e(int i) {
-        q[this.bi] = i;
+        q[this.id] = i;
         return this;
     }
 
     protected Block a(float f) {
-        s[this.bi] = (int) (15.0F * f);
+        s[this.id] = (int) (15.0F * f);
         return this;
     }
 
     protected Block b(float f) {
-        this.bk = f * 3.0F;
+        this.durability = f * 3.0F;
         return this;
     }
 
@@ -177,33 +177,33 @@ public class Block {
     }
 
     protected Block c(float f) {
-        this.bj = f;
-        if (this.bk < f * 5.0F) {
-            this.bk = f * 5.0F;
+        this.strength = f;
+        if (this.durability < f * 5.0F) {
+            this.durability = f * 5.0F;
         }
 
         return this;
     }
 
     protected void a(boolean flag) {
-        n[this.bi] = flag;
+        n[this.id] = flag;
     }
 
     public void a(float f, float f1, float f2, float f3, float f4, float f5) {
-        this.bl = (double) f;
-        this.bm = (double) f1;
-        this.bn = (double) f2;
-        this.bo = (double) f3;
-        this.bp = (double) f4;
-        this.bq = (double) f5;
+        this.minX = (double) f;
+        this.minY = (double) f1;
+        this.minZ = (double) f2;
+        this.maxX = (double) f3;
+        this.maxY = (double) f4;
+        this.maxZ = (double) f5;
     }
 
     public boolean a(IBlockAccess iblockaccess, int i, int j, int k, int l) {
-        return l == 0 && this.bm > 0.0D ? true : (l == 1 && this.bp < 1.0D ? true : (l == 2 && this.bn > 0.0D ? true : (l == 3 && this.bq < 1.0D ? true : (l == 4 && this.bl > 0.0D ? true : (l == 5 && this.bo < 1.0D ? true : !iblockaccess.d(i, j, k))))));
+        return l == 0 && this.minY > 0.0D ? true : (l == 1 && this.maxY < 1.0D ? true : (l == 2 && this.minZ > 0.0D ? true : (l == 3 && this.maxZ < 1.0D ? true : (l == 4 && this.minX > 0.0D ? true : (l == 5 && this.maxX < 1.0D ? true : !iblockaccess.d(i, j, k))))));
     }
 
     public int a(int i) {
-        return this.bh;
+        return this.textureId;
     }
 
     public void a(World world, int i, int j, int k, AxisAlignedBB axisalignedbb, ArrayList arraylist) {
@@ -215,7 +215,7 @@ public class Block {
     }
 
     public AxisAlignedBB d(World world, int i, int j, int k) {
-        return AxisAlignedBB.b((double) i + this.bl, (double) j + this.bm, (double) k + this.bn, (double) i + this.bo, (double) j + this.bp, (double) k + this.bq);
+        return AxisAlignedBB.b((double) i + this.minX, (double) j + this.minY, (double) k + this.minZ, (double) i + this.maxX, (double) j + this.maxY, (double) k + this.maxZ);
     }
 
     public boolean a() {
@@ -249,11 +249,11 @@ public class Block {
     }
 
     public int a(int i, Random random) {
-        return this.bi;
+        return this.id;
     }
 
     public float a(EntityHuman entityhuman) {
-        return this.bj < 0.0F ? 0.0F : (!entityhuman.b(this) ? 1.0F / this.bj / 100.0F : entityhuman.a(this) / this.bj / 30.0F);
+        return this.strength < 0.0F ? 0.0F : (!entityhuman.b(this) ? 1.0F / this.strength / 100.0F : entityhuman.a(this) / this.strength / 30.0F);
     }
 
     public void a_(World world, int i, int j, int k, int l) {
@@ -261,7 +261,7 @@ public class Block {
     }
 
     public void a(World world, int i, int j, int k, int l, float f) {
-        if (!world.z) {
+        if (!world.isStatic) {
             int i1 = this.a(world.l);
 
             for (int j1 = 0; j1 < i1; ++j1) {
@@ -288,19 +288,19 @@ public class Block {
     }
 
     public float a(Entity entity) {
-        return this.bk / 5.0F;
+        return this.durability / 5.0F;
     }
 
     public MovingObjectPosition a(World world, int i, int j, int k, Vec3D vec3d, Vec3D vec3d1) {
         this.a((IBlockAccess) world, i, j, k);
         vec3d = vec3d.c((double) (-i), (double) (-j), (double) (-k));
         vec3d1 = vec3d1.c((double) (-i), (double) (-j), (double) (-k));
-        Vec3D vec3d2 = vec3d.a(vec3d1, this.bl);
-        Vec3D vec3d3 = vec3d.a(vec3d1, this.bo);
-        Vec3D vec3d4 = vec3d.b(vec3d1, this.bm);
-        Vec3D vec3d5 = vec3d.b(vec3d1, this.bp);
-        Vec3D vec3d6 = vec3d.c(vec3d1, this.bn);
-        Vec3D vec3d7 = vec3d.c(vec3d1, this.bq);
+        Vec3D vec3d2 = vec3d.a(vec3d1, this.minX);
+        Vec3D vec3d3 = vec3d.a(vec3d1, this.maxX);
+        Vec3D vec3d4 = vec3d.b(vec3d1, this.minY);
+        Vec3D vec3d5 = vec3d.b(vec3d1, this.maxY);
+        Vec3D vec3d6 = vec3d.c(vec3d1, this.minZ);
+        Vec3D vec3d7 = vec3d.c(vec3d1, this.maxZ);
 
         if (!this.a(vec3d2)) {
             vec3d2 = null;
@@ -386,23 +386,23 @@ public class Block {
     }
 
     private boolean a(Vec3D vec3d) {
-        return vec3d == null ? false : vec3d.b >= this.bm && vec3d.b <= this.bp && vec3d.c >= this.bn && vec3d.c <= this.bq;
+        return vec3d == null ? false : vec3d.b >= this.minY && vec3d.b <= this.maxY && vec3d.c >= this.minZ && vec3d.c <= this.maxZ;
     }
 
     private boolean b(Vec3D vec3d) {
-        return vec3d == null ? false : vec3d.a >= this.bl && vec3d.a <= this.bo && vec3d.c >= this.bn && vec3d.c <= this.bq;
+        return vec3d == null ? false : vec3d.a >= this.minX && vec3d.a <= this.maxX && vec3d.c >= this.minZ && vec3d.c <= this.maxZ;
     }
 
     private boolean c(Vec3D vec3d) {
-        return vec3d == null ? false : vec3d.a >= this.bl && vec3d.a <= this.bo && vec3d.b >= this.bm && vec3d.b <= this.bp;
+        return vec3d == null ? false : vec3d.a >= this.minX && vec3d.a <= this.maxX && vec3d.b >= this.minY && vec3d.b <= this.maxY;
     }
 
     public void a_(World world, int i, int j, int k) {}
 
     public boolean a(World world, int i, int j, int k) {
-        int l = world.a(i, j, k);
+        int l = world.getTypeId(i, j, k);
 
-        return l == 0 || m[l].bt.d();
+        return l == 0 || byId[l].material.isLiquid();
     }
 
     public boolean a(World world, int i, int j, int k, EntityHuman entityhuman) {
@@ -444,23 +444,23 @@ public class Block {
     public void a(World world, int i, int j, int k, EntityLiving entityliving) {}
 
     public Block a(String s) {
-        this.a = "tile." + s;
+        this.name = "tile." + s;
         return this;
     }
 
     public String e() {
-        return this.a;
+        return this.name;
     }
 
     public void a(World world, int i, int j, int k, int l, int i1) {}
 
     static {
-        Item.c[WOOL.bi] = (new ItemCloth(WOOL.bi - 256)).a("cloth");
-        Item.c[LOG.bi] = (new ItemLog(LOG.bi - 256)).a("log");
+        Item.byId[WOOL.id] = (new ItemCloth(WOOL.id - 256)).a("cloth");
+        Item.byId[LOG.id] = (new ItemLog(LOG.id - 256)).a("log");
 
         for (int i = 0; i < 256; ++i) {
-            if (m[i] != null && Item.c[i] == null) {
-                Item.c[i] = new ItemBlock(i - 256);
+            if (byId[i] != null && Item.byId[i] == null) {
+                Item.byId[i] = new ItemBlock(i - 256);
             }
         }
     }
