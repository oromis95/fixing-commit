@@ -4,15 +4,15 @@ public class MaterialTransparent extends Material {
 
     public MaterialTransparent() {}
 
-    public boolean a() {
+    public boolean isBuildable() {
         return false;
     }
 
-    public boolean b() {
+    public boolean blocksLight() {
         return false;
     }
 
-    public boolean c() {
+    public boolean isSolid() {
         return false;
     }
 }
