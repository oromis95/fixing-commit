@@ -5,14 +5,14 @@ public class BlockPumpkin extends Block {
     private boolean a;
 
     protected BlockPumpkin(int i, int j, boolean flag) {
-        super(i, Material.w);
-        this.bh = j;
+        super(i, Material.PUMPKIN);
+        this.textureId = j;
         this.a(true);
         this.a = flag;
     }
 
     public int a(int i) {
-        return i == 1 ? this.bh : (i == 0 ? this.bh : (i == 3 ? this.bh + 1 + 16 : this.bh + 16));
+        return i == 1 ? this.textureId : (i == 0 ? this.textureId : (i == 3 ? this.textureId + 1 + 16 : this.textureId + 16));
     }
 
     public void e(World world, int i, int j, int k) {
@@ -20,13 +20,13 @@ public class BlockPumpkin extends Block {
     }
 
     public boolean a(World world, int i, int j, int k) {
-        int l = world.a(i, j, k);
+        int l = world.getTypeId(i, j, k);
 
-        return (l == 0 || Block.m[l].bt.d()) && world.d(i, j - 1, k);
+        return (l == 0 || Block.byId[l].material.isLiquid()) && world.d(i, j - 1, k);
     }
 
     public void a(World world, int i, int j, int k, EntityLiving entityliving) {
-        int l = MathHelper.b((double) (entityliving.v * 4.0F / 360.0F) + 0.5D) & 3;
+        int l = MathHelper.b((double) (entityliving.yaw * 4.0F / 360.0F) + 0.5D) & 3;
 
         world.c(i, j, k, l);
     }
