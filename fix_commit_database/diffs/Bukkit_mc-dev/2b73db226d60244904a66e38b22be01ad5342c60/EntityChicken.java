@@ -12,17 +12,17 @@ public class EntityChicken extends EntityAnimal {
 
     public EntityChicken(World world) {
         super(world);
-        this.aP = "/mob/chicken.png";
+        this.texture = "/mob/chicken.png";
         this.a(0.3F, 0.4F);
-        this.aZ = 4;
-        this.am = this.W.nextInt(6000) + 6000;
+        this.health = 4;
+        this.am = this.random.nextInt(6000) + 6000;
     }
 
     public void o() {
         super.o();
         this.ak = this.b;
         this.f = this.c;
-        this.c = (float) ((double) this.c + (double) (this.A ? -1 : 4) * 0.3D);
+        this.c = (float) ((double) this.c + (double) (this.onGround ? -1 : 4) * 0.3D);
         if (this.c < 0.0F) {
             this.c = 0.0F;
         }
@@ -31,20 +31,20 @@ public class EntityChicken extends EntityAnimal {
             this.c = 1.0F;
         }
 
-        if (!this.A && this.al < 1.0F) {
+        if (!this.onGround && this.al < 1.0F) {
             this.al = 1.0F;
         }
 
         this.al = (float) ((double) this.al * 0.9D);
-        if (!this.A && this.t < 0.0D) {
-            this.t *= 0.6D;
+        if (!this.onGround && this.motY < 0.0D) {
+            this.motY *= 0.6D;
         }
 
         this.b += this.al * 2.0F;
-        if (!this.l.z && --this.am <= 0) {
-            this.l.a(this, "mob.chickenplop", 1.0F, (this.W.nextFloat() - this.W.nextFloat()) * 0.2F + 1.0F);
-            this.a(Item.EGG.ba, 1);
-            this.am = this.W.nextInt(6000) + 6000;
+        if (!this.world.isStatic && --this.am <= 0) {
+            this.world.a(this, "mob.chickenplop", 1.0F, (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
+            this.a(Item.EGG.id, 1);
+            this.am = this.random.nextInt(6000) + 6000;
         }
     }
 
@@ -71,6 +71,6 @@ public class EntityChicken extends EntityAnimal {
     }
 
     protected int h() {
-        return Item.FEATHER.ba;
+        return Item.FEATHER.id;
     }
 }
