@@ -19,9 +19,9 @@ public class EntitySquid extends EntityWaterAnimal {
 
     public EntitySquid(World world) {
         super(world);
-        this.aP = "/mob/squid.png";
+        this.texture = "/mob/squid.png";
         this.a(0.95F, 0.95F);
-        this.ap = 1.0F / (this.W.nextFloat() + 1.0F) * 0.2F;
+        this.ap = 1.0F / (this.random.nextFloat() + 1.0F) * 0.2F;
     }
 
     public void a(NBTTagCompound nbttagcompound) {
@@ -53,7 +53,7 @@ public class EntitySquid extends EntityWaterAnimal {
     }
 
     protected void g_() {
-        int i = this.W.nextInt(3) + 1;
+        int i = this.random.nextInt(3) + 1;
 
         for (int j = 0; j < i; ++j) {
             this.a(new ItemStack(Item.INK_SACK, 1, 0), 0.0F);
@@ -61,10 +61,10 @@ public class EntitySquid extends EntityWaterAnimal {
     }
 
     public boolean a(EntityHuman entityhuman) {
-        ItemStack itemstack = entityhuman.an.e();
+        ItemStack itemstack = entityhuman.inventory.e();
 
-        if (itemstack != null && itemstack.c == Item.BUCKET.ba) {
-            entityhuman.an.a(entityhuman.an.c, new ItemStack(Item.MILK_BUCKET));
+        if (itemstack != null && itemstack.id == Item.BUCKET.id) {
+            entityhuman.inventory.a(entityhuman.inventory.c, new ItemStack(Item.MILK_BUCKET));
             return true;
         } else {
             return false;
@@ -72,7 +72,7 @@ public class EntitySquid extends EntityWaterAnimal {
     }
 
     public boolean v() {
-        return this.l.a(this.z.b(0.0D, -0.6000000238418579D, 0.0D), Material.f, this);
+        return this.world.a(this.boundingBox.b(0.0D, -0.6000000238418579D, 0.0D), Material.WATER, this);
     }
 
     public void o() {
@@ -84,8 +84,8 @@ public class EntitySquid extends EntityWaterAnimal {
         this.ak += this.ap;
         if (this.ak > 6.2831855F) {
             this.ak -= 6.2831855F;
-            if (this.W.nextInt(10) == 0) {
-                this.ap = 1.0F / (this.W.nextFloat() + 1.0F) * 0.2F;
+            if (this.random.nextInt(10) == 0) {
+                this.ap = 1.0F / (this.random.nextFloat() + 1.0F) * 0.2F;
             }
         }
 
@@ -108,23 +108,23 @@ public class EntitySquid extends EntityWaterAnimal {
             }
 
             if (!this.aW) {
-                this.s = (double) (this.ar * this.ao);
-                this.t = (double) (this.as * this.ao);
-                this.u = (double) (this.at * this.ao);
+                this.motX = (double) (this.ar * this.ao);
+                this.motY = (double) (this.as * this.ao);
+                this.motZ = (double) (this.at * this.ao);
             }
 
-            f = MathHelper.a(this.s * this.s + this.u * this.u);
-            this.aI += (-((float) Math.atan2(this.s, this.u)) * 180.0F / 3.1415927F - this.aI) * 0.1F;
-            this.v = this.aI;
+            f = MathHelper.a(this.motX * this.motX + this.motZ * this.motZ);
+            this.aI += (-((float) Math.atan2(this.motX, this.motZ)) * 180.0F / 3.1415927F - this.aI) * 0.1F;
+            this.yaw = this.aI;
             this.c += 3.1415927F * this.aq * 1.5F;
-            this.a += (-((float) Math.atan2((double) f, this.t)) * 180.0F / 3.1415927F - this.a) * 0.1F;
+            this.a += (-((float) Math.atan2((double) f, this.motY)) * 180.0F / 3.1415927F - this.a) * 0.1F;
         } else {
             this.am = MathHelper.e(MathHelper.a(this.ak)) * 3.1415927F * 0.25F;
             if (!this.aW) {
-                this.s = 0.0D;
-                this.t -= 0.08D;
-                this.t *= 0.9800000190734863D;
-                this.u = 0.0D;
+                this.motX = 0.0D;
+                this.motY -= 0.08D;
+                this.motY *= 0.9800000190734863D;
+                this.motZ = 0.0D;
             }
 
             this.a = (float) ((double) this.a + (double) (-90.0F - this.a) * 0.02D);
@@ -132,15 +132,15 @@ public class EntitySquid extends EntityWaterAnimal {
     }
 
     public void c(float f, float f1) {
-        this.c(this.s, this.t, this.u);
+        this.c(this.motX, this.motY, this.motZ);
     }
 
     protected void d() {
-        if (this.W.nextInt(50) == 0 || !this.ab || this.ar == 0.0F && this.as == 0.0F && this.at == 0.0F) {
-            float f = this.W.nextFloat() * 3.1415927F * 2.0F;
+        if (this.random.nextInt(50) == 0 || !this.ab || this.ar == 0.0F && this.as == 0.0F && this.at == 0.0F) {
+            float f = this.random.nextFloat() * 3.1415927F * 2.0F;
 
             this.ar = MathHelper.b(f) * 0.2F;
-            this.as = -0.1F + this.W.nextFloat() * 0.2F;
+            this.as = -0.1F + this.random.nextFloat() * 0.2F;
             this.at = MathHelper.a(f) * 0.2F;
         }
     }
