@@ -4,15 +4,15 @@ import java.util.List;
 
 public class EntityArrow extends Entity {
 
-    private int b = -1;
     private int c = -1;
     private int d = -1;
-    private int e = 0;
-    private boolean f = false;
+    private int e = -1;
+    private int f = 0;
+    private boolean aj = false;
     public int a = 0;
-    private EntityLiving ai;
-    private int aj;
-    private int ak = 0;
+    public EntityLiving b;
+    private int ak;
+    private int al = 0;
 
     public EntityArrow(World world) {
         super(world);
@@ -21,14 +21,14 @@ public class EntityArrow extends Entity {
 
     public EntityArrow(World world, EntityLiving entityliving) {
         super(world);
-        this.ai = entityliving;
+        this.b = entityliving;
         this.a(0.5F, 0.5F);
         this.c(entityliving.p, entityliving.q, entityliving.r, entityliving.v, entityliving.w);
         this.p -= (double) (MathHelper.b(this.v / 180.0F * 3.1415927F) * 0.16F);
         this.q -= 0.10000000149011612D;
         this.r -= (double) (MathHelper.a(this.v / 180.0F * 3.1415927F) * 0.16F);
         this.a(this.p, this.q, this.r);
-        this.G = 0.0F;
+        this.H = 0.0F;
         this.s = (double) (-MathHelper.a(this.v / 180.0F * 3.1415927F) * MathHelper.b(this.w / 180.0F * 3.1415927F));
         this.u = (double) (MathHelper.b(this.v / 180.0F * 3.1415927F) * MathHelper.b(this.w / 180.0F * 3.1415927F));
         this.t = (double) (-MathHelper.a(this.w / 180.0F * 3.1415927F));
@@ -41,9 +41,9 @@ public class EntityArrow extends Entity {
         d0 /= (double) f2;
         d1 /= (double) f2;
         d2 /= (double) f2;
-        d0 += this.V.nextGaussian() * 0.007499999832361937D * (double) f1;
-        d1 += this.V.nextGaussian() * 0.007499999832361937D * (double) f1;
-        d2 += this.V.nextGaussian() * 0.007499999832361937D * (double) f1;
+        d0 += this.W.nextGaussian() * 0.007499999832361937D * (double) f1;
+        d1 += this.W.nextGaussian() * 0.007499999832361937D * (double) f1;
+        d2 += this.W.nextGaussian() * 0.007499999832361937D * (double) f1;
         d0 *= (double) f;
         d1 *= (double) f;
         d2 *= (double) f;
@@ -54,7 +54,7 @@ public class EntityArrow extends Entity {
 
         this.x = this.v = (float) (Math.atan2(d0, d2) * 180.0D / 3.1415927410125732D);
         this.y = this.w = (float) (Math.atan2(d1, (double) f3) * 180.0D / 3.1415927410125732D);
-        this.aj = 0;
+        this.ak = 0;
     }
 
     public void b_() {
@@ -63,26 +63,26 @@ public class EntityArrow extends Entity {
             --this.a;
         }
 
-        if (this.f) {
-            int i = this.l.a(this.b, this.c, this.d);
+        if (this.aj) {
+            int i = this.l.a(this.c, this.d, this.e);
 
-            if (i == this.e) {
-                ++this.aj;
-                if (this.aj == 1200) {
+            if (i == this.f) {
+                ++this.ak;
+                if (this.ak == 1200) {
                     this.l();
                 }
 
                 return;
             }
 
-            this.f = false;
-            this.s *= (double) (this.V.nextFloat() * 0.2F);
-            this.t *= (double) (this.V.nextFloat() * 0.2F);
-            this.u *= (double) (this.V.nextFloat() * 0.2F);
-            this.aj = 0;
+            this.aj = false;
+            this.s *= (double) (this.W.nextFloat() * 0.2F);
+            this.t *= (double) (this.W.nextFloat() * 0.2F);
+            this.u *= (double) (this.W.nextFloat() * 0.2F);
             this.ak = 0;
+            this.al = 0;
         } else {
-            ++this.ak;
+            ++this.al;
         }
 
         Vec3D vec3d = Vec3D.b(this.p, this.q, this.r);
@@ -104,7 +104,7 @@ public class EntityArrow extends Entity {
         for (int j = 0; j < list.size(); ++j) {
             Entity entity1 = (Entity) list.get(j);
 
-            if (entity1.c_() && (entity1 != this.ai || this.ak >= 5)) {
+            if (entity1.c_() && (entity1 != this.b || this.al >= 5)) {
                 f = 0.3F;
                 AxisAlignedBB axisalignedbb = entity1.z.b((double) f, (double) f, (double) f);
                 MovingObjectPosition movingobjectposition1 = axisalignedbb.a(vec3d, vec3d1);
@@ -128,8 +128,8 @@ public class EntityArrow extends Entity {
 
         if (movingobjectposition != null) {
             if (movingobjectposition.g != null) {
-                if (movingobjectposition.g.a(this.ai, 4)) {
-                    this.l.a(this, "random.drr", 1.0F, 1.2F / (this.V.nextFloat() * 0.2F + 0.9F));
+                if (movingobjectposition.g.a(this.b, 4)) {
+                    this.l.a(this, "random.drr", 1.0F, 1.2F / (this.W.nextFloat() * 0.2F + 0.9F));
                     this.l();
                 } else {
                     this.s *= -0.10000000149011612D;
@@ -137,13 +137,13 @@ public class EntityArrow extends Entity {
                     this.u *= -0.10000000149011612D;
                     this.v += 180.0F;
                     this.x += 180.0F;
-                    this.ak = 0;
+                    this.al = 0;
                 }
             } else {
-                this.b = movingobjectposition.b;
-                this.c = movingobjectposition.c;
-                this.d = movingobjectposition.d;
-                this.e = this.l.a(this.b, this.c, this.d);
+                this.c = movingobjectposition.b;
+                this.d = movingobjectposition.c;
+                this.e = movingobjectposition.d;
+                this.f = this.l.a(this.c, this.d, this.e);
                 this.s = (double) ((float) (movingobjectposition.f.a - this.p));
                 this.t = (double) ((float) (movingobjectposition.f.b - this.q));
                 this.u = (double) ((float) (movingobjectposition.f.c - this.r));
@@ -151,8 +151,8 @@ public class EntityArrow extends Entity {
                 this.p -= this.s / (double) f1 * 0.05000000074505806D;
                 this.q -= this.t / (double) f1 * 0.05000000074505806D;
                 this.r -= this.u / (double) f1 * 0.05000000074505806D;
-                this.l.a(this, "random.drr", 1.0F, 1.2F / (this.V.nextFloat() * 0.2F + 0.9F));
-                this.f = true;
+                this.l.a(this, "random.drr", 1.0F, 1.2F / (this.W.nextFloat() * 0.2F + 0.9F));
+                this.aj = true;
                 this.a = 7;
             }
         }
@@ -202,26 +202,26 @@ public class EntityArrow extends Entity {
     }
 
     public void a(NBTTagCompound nbttagcompound) {
-        nbttagcompound.a("xTile", (short) this.b);
-        nbttagcompound.a("yTile", (short) this.c);
-        nbttagcompound.a("zTile", (short) this.d);
-        nbttagcompound.a("inTile", (byte) this.e);
+        nbttagcompound.a("xTile", (short) this.c);
+        nbttagcompound.a("yTile", (short) this.d);
+        nbttagcompound.a("zTile", (short) this.e);
+        nbttagcompound.a("inTile", (byte) this.f);
         nbttagcompound.a("shake", (byte) this.a);
-        nbttagcompound.a("inGround", (byte) (this.f ? 1 : 0));
+        nbttagcompound.a("inGround", (byte) (this.aj ? 1 : 0));
     }
 
     public void b(NBTTagCompound nbttagcompound) {
-        this.b = nbttagcompound.c("xTile");
-        this.c = nbttagcompound.c("yTile");
-        this.d = nbttagcompound.c("zTile");
-        this.e = nbttagcompound.b("inTile") & 255;
+        this.c = nbttagcompound.c("xTile");
+        this.d = nbttagcompound.c("yTile");
+        this.e = nbttagcompound.c("zTile");
+        this.f = nbttagcompound.b("inTile") & 255;
         this.a = nbttagcompound.b("shake") & 255;
-        this.f = nbttagcompound.b("inGround") == 1;
+        this.aj = nbttagcompound.b("inGround") == 1;
     }
 
     public void b(EntityHuman entityhuman) {
-        if (this.f && this.ai == entityhuman && this.a <= 0 && entityhuman.ak.a(new ItemStack(Item.ARROW.aW, 1))) {
-            this.l.a(this, "random.pop", 0.2F, ((this.V.nextFloat() - this.V.nextFloat()) * 0.7F + 1.0F) * 2.0F);
+        if (this.aj && this.b == entityhuman && this.a <= 0 && entityhuman.al.a(new ItemStack(Item.ARROW.aW, 1))) {
+            this.l.a(this, "random.pop", 0.2F, ((this.W.nextFloat() - this.W.nextFloat()) * 0.7F + 1.0F) * 2.0F);
             entityhuman.c(this, 1);
             this.l();
         }
