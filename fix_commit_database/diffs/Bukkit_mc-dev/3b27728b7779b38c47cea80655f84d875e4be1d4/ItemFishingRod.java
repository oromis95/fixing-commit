@@ -8,18 +8,18 @@ public class ItemFishingRod extends Item {
     }
 
     public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
-        if (entityhuman.at != null) {
-            int i = entityhuman.at.c();
+        if (entityhuman.au != null) {
+            int i = entityhuman.au.c();
 
             itemstack.a(i);
-            entityhuman.E();
+            entityhuman.F();
         } else {
             world.a(entityhuman, "random.bow", 0.5F, 0.4F / (b.nextFloat() * 0.4F + 0.8F));
             if (!world.z) {
                 world.a((Entity) (new EntityFish(world, entityhuman)));
             }
 
-            entityhuman.E();
+            entityhuman.F();
         }
 
         return itemstack;
