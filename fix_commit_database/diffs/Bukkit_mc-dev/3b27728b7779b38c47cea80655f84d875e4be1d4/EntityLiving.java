@@ -4,86 +4,86 @@ import java.util.List;
 
 public class EntityLiving extends Entity {
 
-    public int au = 20;
-    public float av;
+    public int av = 20;
     public float aw;
     public float ax;
-    public float ay = 0.0F;
+    public float ay;
     public float az = 0.0F;
-    protected float aA;
+    public float aA = 0.0F;
     protected float aB;
     protected float aC;
     protected float aD;
-    protected boolean aE = true;
-    protected String aF = "/mob/char.png";
-    protected boolean aG = true;
-    protected float aH = 0.0F;
-    protected String aI = null;
-    protected float aJ = 1.0F;
-    protected int aK = 0;
-    protected float aL = 0.0F;
-    public boolean aM = false;
-    public float aN;
+    protected float aE;
+    protected boolean aF = true;
+    protected String aG = "/mob/char.png";
+    protected boolean aH = true;
+    protected float aI = 0.0F;
+    protected String aJ = null;
+    protected float aK = 1.0F;
+    protected int aL = 0;
+    protected float aM = 0.0F;
+    public boolean aN = false;
     public float aO;
-    public int aP = 10;
-    public int aQ;
-    private int a;
+    public float aP;
+    public int aQ = 10;
     public int aR;
+    private int a;
     public int aS;
-    public float aT = 0.0F;
-    public int aU = 0;
+    public int aT;
+    public float aU = 0.0F;
     public int aV = 0;
-    public float aW;
+    public int aW = 0;
     public float aX;
-    protected boolean aY = false;
-    public int aZ = -1;
-    public float ba = (float) (Math.random() * 0.8999999761581421D + 0.10000000149011612D);
-    public float bb;
+    public float aY;
+    protected boolean aZ = false;
+    public int ba = -1;
+    public float bb = (float) (Math.random() * 0.8999999761581421D + 0.10000000149011612D);
     public float bc;
     public float bd;
-    protected int be;
-    protected double bf;
+    public float be;
+    protected int bf;
     protected double bg;
     protected double bh;
     protected double bi;
     protected double bj;
-    float bk = 0.0F;
-    private int b = 0;
-    protected int bl = 0;
-    protected float bm;
-    protected float bn;
+    protected double bk;
+    float bl = 0.0F;
+    protected int bm = 0;
+    protected int bn = 0;
     protected float bo;
-    protected boolean bp = false;
-    protected float bq = 0.0F;
-    protected float br = 0.7F;
-    private Entity c;
-    private int d = 0;
+    protected float bp;
+    protected float bq;
+    protected boolean br = false;
+    protected float bs = 0.0F;
+    protected float bt = 0.7F;
+    private Entity b;
+    private int c = 0;
 
     public EntityLiving(World world) {
         super(world);
         this.i = true;
-        this.ax = (float) (Math.random() + 1.0D) * 0.01F;
+        this.ay = (float) (Math.random() + 1.0D) * 0.01F;
         this.a(this.p, this.q, this.r);
-        this.av = (float) Math.random() * 12398.0F;
+        this.aw = (float) Math.random() * 12398.0F;
         this.v = (float) (Math.random() * 3.1415927410125732D * 2.0D);
-        this.aw = 1.0F;
-        this.R = 0.5F;
+        this.ax = 1.0F;
+        this.S = 0.5F;
     }
 
-    public boolean g(Entity entity) {
+    public boolean i(Entity entity) {
         return this.l.a(Vec3D.b(this.p, this.q + (double) this.s(), this.r), Vec3D.b(entity.p, entity.q + (double) entity.s(), entity.r)) == null;
     }
 
     public boolean c_() {
-        return !this.F;
+        return !this.G;
     }
 
-    public boolean u() {
-        return !this.F;
+    public boolean v() {
+        return !this.G;
     }
 
     public float s() {
-        return this.I * 0.85F;
+        return this.J * 0.85F;
     }
 
     public int b() {
@@ -91,36 +91,36 @@ public class EntityLiving extends Entity {
     }
 
     public void m() {
-        this.aN = this.aO;
+        this.aO = this.aP;
         super.m();
-        if (this.V.nextInt(1000) < this.a++) {
+        if (this.W.nextInt(1000) < this.a++) {
             this.a = -this.b();
             String s = this.d();
 
             if (s != null) {
-                this.l.a(this, s, this.h(), (this.V.nextFloat() - this.V.nextFloat()) * 0.2F + 1.0F);
+                this.l.a(this, s, this.h(), (this.W.nextFloat() - this.W.nextFloat()) * 0.2F + 1.0F);
             }
         }
 
-        if (this.w() && this.x()) {
+        if (this.x() && this.y()) {
             this.a((Entity) null, 1);
         }
 
-        if (this.ad) {
-            this.Y = 0;
+        if (this.ae || this.l.z) {
+            this.Z = 0;
         }
 
         int i;
 
-        if (this.w() && this.a(Material.f)) {
-            --this.ac;
-            if (this.ac == -20) {
-                this.ac = 0;
+        if (this.x() && this.a(Material.f)) {
+            --this.ad;
+            if (this.ad == -20) {
+                this.ad = 0;
 
                 for (i = 0; i < 8; ++i) {
-                    float f = this.V.nextFloat() - this.V.nextFloat();
-                    float f1 = this.V.nextFloat() - this.V.nextFloat();
-                    float f2 = this.V.nextFloat() - this.V.nextFloat();
+                    float f = this.W.nextFloat() - this.W.nextFloat();
+                    float f1 = this.W.nextFloat() - this.W.nextFloat();
+                    float f2 = this.W.nextFloat() - this.W.nextFloat();
 
                     this.l.a("bubble", this.p + (double) f, this.q + (double) f1, this.r + (double) f2, this.s, this.t, this.u);
                 }
@@ -128,73 +128,73 @@ public class EntityLiving extends Entity {
                 this.a((Entity) null, 2);
             }
 
-            this.Y = 0;
+            this.Z = 0;
         } else {
-            this.ac = this.Z;
+            this.ad = this.aa;
         }
 
-        this.aW = this.aX;
-        if (this.aV > 0) {
-            --this.aV;
+        this.aX = this.aY;
+        if (this.aW > 0) {
+            --this.aW;
         }
 
-        if (this.aR > 0) {
-            --this.aR;
+        if (this.aS > 0) {
+            --this.aS;
         }
 
-        if (this.ab > 0) {
-            --this.ab;
+        if (this.ac > 0) {
+            --this.ac;
         }
 
-        if (this.aP <= 0) {
-            ++this.aU;
-            if (this.aU > 20) {
-                this.K();
+        if (this.aQ <= 0) {
+            ++this.aV;
+            if (this.aV > 20) {
+                this.M();
                 this.l();
 
                 for (i = 0; i < 20; ++i) {
-                    double d0 = this.V.nextGaussian() * 0.02D;
-                    double d1 = this.V.nextGaussian() * 0.02D;
-                    double d2 = this.V.nextGaussian() * 0.02D;
+                    double d0 = this.W.nextGaussian() * 0.02D;
+                    double d1 = this.W.nextGaussian() * 0.02D;
+                    double d2 = this.W.nextGaussian() * 0.02D;
 
-                    this.l.a("explode", this.p + (double) (this.V.nextFloat() * this.H * 2.0F) - (double) this.H, this.q + (double) (this.V.nextFloat() * this.I), this.r + (double) (this.V.nextFloat() * this.H * 2.0F) - (double) this.H, d0, d1, d2);
+                    this.l.a("explode", this.p + (double) (this.W.nextFloat() * this.I * 2.0F) - (double) this.I, this.q + (double) (this.W.nextFloat() * this.J), this.r + (double) (this.W.nextFloat() * this.I * 2.0F) - (double) this.I, d0, d1, d2);
                 }
             }
         }
 
-        this.aD = this.aC;
-        this.az = this.ay;
+        this.aE = this.aD;
+        this.aA = this.az;
         this.x = this.v;
         this.y = this.w;
     }
 
-    public void I() {
+    public void K() {
         for (int i = 0; i < 20; ++i) {
-            double d0 = this.V.nextGaussian() * 0.02D;
-            double d1 = this.V.nextGaussian() * 0.02D;
-            double d2 = this.V.nextGaussian() * 0.02D;
+            double d0 = this.W.nextGaussian() * 0.02D;
+            double d1 = this.W.nextGaussian() * 0.02D;
+            double d2 = this.W.nextGaussian() * 0.02D;
             double d3 = 10.0D;
 
-            this.l.a("explode", this.p + (double) (this.V.nextFloat() * this.H * 2.0F) - (double) this.H - d0 * d3, this.q + (double) (this.V.nextFloat() * this.I) - d1 * d3, this.r + (double) (this.V.nextFloat() * this.H * 2.0F) - (double) this.H - d2 * d3, d0, d1, d2);
+            this.l.a("explode", this.p + (double) (this.W.nextFloat() * this.I * 2.0F) - (double) this.I - d0 * d3, this.q + (double) (this.W.nextFloat() * this.J) - d1 * d3, this.r + (double) (this.W.nextFloat() * this.I * 2.0F) - (double) this.I - d2 * d3, d0, d1, d2);
         }
     }
 
-    public void y() {
-        super.y();
-        this.aA = this.aB;
-        this.aB = 0.0F;
+    public void z() {
+        super.z();
+        this.aB = this.aC;
+        this.aC = 0.0F;
     }
 
     public void b_() {
         super.b_();
-        this.D();
+        this.E();
         double d0 = this.p - this.m;
         double d1 = this.r - this.o;
         float f = MathHelper.a(d0 * d0 + d1 * d1);
-        float f1 = this.ay;
+        float f1 = this.az;
         float f2 = 0.0F;
 
-        this.aA = this.aB;
+        this.aB = this.aC;
         float f3 = 0.0F;
 
         if (f > 0.05F) {
@@ -203,7 +203,7 @@ public class EntityLiving extends Entity {
             f1 = (float) Math.atan2(d1, d0) * 180.0F / 3.1415927F - 90.0F;
         }
 
-        if (this.aO > 0.0F) {
+        if (this.aP > 0.0F) {
             f1 = this.v;
         }
 
@@ -211,11 +211,11 @@ public class EntityLiving extends Entity {
             f3 = 0.0F;
         }
 
-        this.aB += (f3 - this.aB) * 0.3F;
+        this.aC += (f3 - this.aC) * 0.3F;
 
         float f4;
 
-        for (f4 = f1 - this.ay; f4 < -180.0F; f4 += 360.0F) {
+        for (f4 = f1 - this.az; f4 < -180.0F; f4 += 360.0F) {
             ;
         }
 
@@ -223,11 +223,11 @@ public class EntityLiving extends Entity {
             f4 -= 360.0F;
         }
 
-        this.ay += f4 * 0.3F;
+        this.az += f4 * 0.3F;
 
         float f5;
 
-        for (f5 = this.v - this.ay; f5 < -180.0F; f5 += 360.0F) {
+        for (f5 = this.v - this.az; f5 < -180.0F; f5 += 360.0F) {
             ;
         }
 
@@ -245,9 +245,9 @@ public class EntityLiving extends Entity {
             f5 = 75.0F;
         }
 
-        this.ay = this.v - f5;
+        this.az = this.v - f5;
         if (f5 * f5 > 2500.0F) {
-            this.ay += f5 * 0.2F;
+            this.az += f5 * 0.2F;
         }
 
         if (flag) {
@@ -262,12 +262,12 @@ public class EntityLiving extends Entity {
             this.x += 360.0F;
         }
 
-        while (this.ay - this.az < -180.0F) {
-            this.az -= 360.0F;
+        while (this.az - this.aA < -180.0F) {
+            this.aA -= 360.0F;
         }
 
-        while (this.ay - this.az >= 180.0F) {
-            this.az += 360.0F;
+        while (this.az - this.aA >= 180.0F) {
+            this.aA += 360.0F;
         }
 
         while (this.w - this.y < -180.0F) {
@@ -278,7 +278,7 @@ public class EntityLiving extends Entity {
             this.y += 360.0F;
         }
 
-        this.aC += f2;
+        this.aD += f2;
     }
 
     protected void a(float f, float f1) {
@@ -286,78 +286,79 @@ public class EntityLiving extends Entity {
     }
 
     public void a(int i) {
-        if (this.aP > 0) {
-            this.aP += i;
-            if (this.aP > 20) {
-                this.aP = 20;
+        if (this.aQ > 0) {
+            this.aQ += i;
+            if (this.aQ > 20) {
+                this.aQ = 20;
             }
 
-            this.ab = this.au / 2;
+            this.ac = this.av / 2;
         }
     }
 
     public boolean a(Entity entity, int i) {
         if (this.l.z) {
-            i = 0;
-        }
-
-        this.bl = 0;
-        if (this.aP <= 0) {
             return false;
         } else {
-            this.bc = 1.5F;
-            boolean flag = true;
+            this.bn = 0;
+            if (this.aQ <= 0) {
+                return false;
+            } else {
+                this.bd = 1.5F;
+                boolean flag = true;
+
+                if ((float) this.ac > (float) this.av / 2.0F) {
+                    if (i <= this.bm) {
+                        return false;
+                    }
 
-            if ((float) this.ab > (float) this.au / 2.0F) {
-                if (i <= this.b) {
-                    return false;
+                    this.c(i - this.bm);
+                    this.bm = i;
+                    flag = false;
+                } else {
+                    this.bm = i;
+                    this.aR = this.aQ;
+                    this.ac = this.av;
+                    this.c(i);
+                    this.aS = this.aT = 10;
                 }
 
-                this.c(i - this.b);
-                this.b = i;
-                flag = false;
-            } else {
-                this.b = i;
-                this.aQ = this.aP;
-                this.ab = this.au;
-                this.c(i);
-                this.aR = this.aS = 10;
-            }
+                this.aU = 0.0F;
+                if (flag) {
+                    this.u();
+                    if (entity != null) {
+                        double d0 = entity.p - this.p;
 
-            this.aT = 0.0F;
-            if (flag) {
-                if (entity != null) {
-                    double d0 = entity.p - this.p;
+                        double d1;
 
-                    double d1;
+                        for (d1 = entity.r - this.r; d0 * d0 + d1 * d1 < 1.0E-4D; d1 = (Math.random() - Math.random()) * 0.01D) {
+                            d0 = (Math.random() - Math.random()) * 0.01D;
+                        }
 
-                    for (d1 = entity.r - this.r; d0 * d0 + d1 * d1 < 1.0E-4D; d1 = (Math.random() - Math.random()) * 0.01D) {
-                        d0 = (Math.random() - Math.random()) * 0.01D;
+                        this.aU = (float) (Math.atan2(d1, d0) * 180.0D / 3.1415927410125732D) - this.v;
+                        this.a(entity, i, d0, d1);
+                    } else {
+                        this.aU = (float) ((int) (Math.random() * 2.0D) * 180);
                     }
-
-                    this.aT = (float) (Math.atan2(d1, d0) * 180.0D / 3.1415927410125732D) - this.v;
-                    this.a(entity, i, d0, d1);
-                } else {
-                    this.aT = (float) ((int) (Math.random() * 2.0D) * 180);
                 }
-            }
 
-            if (this.aP <= 0) {
-                if (flag) {
-                    this.l.a(this, this.f(), this.h(), (this.V.nextFloat() - this.V.nextFloat()) * 0.2F + 1.0F);
+                if (this.aQ <= 0) {
+                    if (flag) {
+                        this.l.a(this, this.f(), this.h(), (this.W.nextFloat() - this.W.nextFloat()) * 0.2F + 1.0F);
+                    }
+
+                    this.f(entity);
+                } else if (flag) {
+                    this.l.a(this, this.e(), this.h(), (this.W.nextFloat() - this.W.nextFloat()) * 0.2F + 1.0F);
                 }
 
-                this.f(entity);
-            } else if (flag) {
-                this.l.a(this, this.e(), this.h(), (this.V.nextFloat() - this.V.nextFloat()) * 0.2F + 1.0F);
+                return true;
             }
-
-            return true;
         }
     }
 
     protected void c(int i) {
-        this.aP -= i;
+        this.aQ -= i;
     }
 
     protected float h() {
@@ -392,20 +393,24 @@ public class EntityLiving extends Entity {
     }
 
     public void f(Entity entity) {
-        if (this.aK > 0 && entity != null) {
-            entity.b(this, this.aK);
+        if (this.aL > 0 && entity != null) {
+            entity.b(this, this.aL);
         }
 
-        this.aY = true;
-        int i = this.g();
+        this.aZ = true;
+        if (!this.l.z) {
+            int i = this.g();
 
-        if (i > 0) {
-            int j = this.V.nextInt(3);
+            if (i > 0) {
+                int j = this.W.nextInt(3);
 
-            for (int k = 0; k < j; ++k) {
-                this.a(i, 1);
+                for (int k = 0; k < j; ++k) {
+                    this.a(i, 1);
+                }
             }
         }
+
+        this.l.a(this, (byte) 3);
     }
 
     protected int g() {
@@ -417,7 +422,7 @@ public class EntityLiving extends Entity {
 
         if (i > 0) {
             this.a((Entity) null, i);
-            int j = this.l.a(MathHelper.b(this.p), MathHelper.b(this.q - 0.20000000298023224D - (double) this.G), MathHelper.b(this.r));
+            int j = this.l.a(MathHelper.b(this.p), MathHelper.b(this.q - 0.20000000298023224D - (double) this.H), MathHelper.b(this.r));
 
             if (j > 0) {
                 StepSound stepsound = Block.m[j].bq;
@@ -478,7 +483,7 @@ public class EntityLiving extends Entity {
             }
 
             if (this.d_()) {
-                this.M = 0.0F;
+                this.N = 0.0F;
                 if (this.t < -0.15D) {
                     this.t = -0.15D;
                 }
@@ -495,7 +500,7 @@ public class EntityLiving extends Entity {
             this.u *= (double) f2;
         }
 
-        this.bb = this.bc;
+        this.bc = this.bd;
         d0 = this.p - this.m;
         double d1 = this.r - this.o;
         float f4 = MathHelper.a(d0 * d0 + d1 * d1) * 4.0F;
@@ -504,8 +509,8 @@ public class EntityLiving extends Entity {
             f4 = 1.0F;
         }
 
-        this.bc += (f4 - this.bc) * 0.4F;
-        this.bd += this.bc;
+        this.bd += (f4 - this.bd) * 0.4F;
+        this.be += this.bd;
     }
 
     public boolean d_() {
@@ -517,36 +522,36 @@ public class EntityLiving extends Entity {
     }
 
     public void a(NBTTagCompound nbttagcompound) {
-        nbttagcompound.a("Health", (short) this.aP);
-        nbttagcompound.a("HurtTime", (short) this.aR);
-        nbttagcompound.a("DeathTime", (short) this.aU);
-        nbttagcompound.a("AttackTime", (short) this.aV);
+        nbttagcompound.a("Health", (short) this.aQ);
+        nbttagcompound.a("HurtTime", (short) this.aS);
+        nbttagcompound.a("DeathTime", (short) this.aV);
+        nbttagcompound.a("AttackTime", (short) this.aW);
     }
 
     public void b(NBTTagCompound nbttagcompound) {
-        this.aP = nbttagcompound.c("Health");
+        this.aQ = nbttagcompound.c("Health");
         if (!nbttagcompound.a("Health")) {
-            this.aP = 10;
+            this.aQ = 10;
         }
 
-        this.aR = nbttagcompound.c("HurtTime");
-        this.aU = nbttagcompound.c("DeathTime");
-        this.aV = nbttagcompound.c("AttackTime");
+        this.aS = nbttagcompound.c("HurtTime");
+        this.aV = nbttagcompound.c("DeathTime");
+        this.aW = nbttagcompound.c("AttackTime");
     }
 
-    public boolean w() {
-        return !this.F && this.aP > 0;
+    public boolean x() {
+        return !this.G && this.aQ > 0;
     }
 
-    public void D() {
-        if (this.be > 0) {
-            double d0 = this.p + (this.bf - this.p) / (double) this.be;
-            double d1 = this.q + (this.bg - this.q) / (double) this.be;
-            double d2 = this.r + (this.bh - this.r) / (double) this.be;
+    public void E() {
+        if (this.bf > 0) {
+            double d0 = this.p + (this.bg - this.p) / (double) this.bf;
+            double d1 = this.q + (this.bh - this.q) / (double) this.bf;
+            double d2 = this.r + (this.bi - this.r) / (double) this.bf;
 
             double d3;
 
-            for (d3 = this.bi - (double) this.v; d3 < -180.0D; d3 += 360.0D) {
+            for (d3 = this.bj - (double) this.v; d3 < -180.0D; d3 += 360.0D) {
                 ;
             }
 
@@ -554,58 +559,58 @@ public class EntityLiving extends Entity {
                 d3 -= 360.0D;
             }
 
-            this.v = (float) ((double) this.v + d3 / (double) this.be);
-            this.w = (float) ((double) this.w + (this.bj - (double) this.w) / (double) this.be);
-            --this.be;
+            this.v = (float) ((double) this.v + d3 / (double) this.bf);
+            this.w = (float) ((double) this.w + (this.bk - (double) this.w) / (double) this.bf);
+            --this.bf;
             this.a(d0, d1, d2);
             this.b(this.v, this.w);
         }
 
-        if (this.aP <= 0) {
-            this.bp = false;
-            this.bm = 0.0F;
-            this.bn = 0.0F;
+        if (this.aQ <= 0) {
+            this.br = false;
             this.bo = 0.0F;
-        } else if (!this.aM) {
+            this.bp = 0.0F;
+            this.bq = 0.0F;
+        } else if (!this.aN) {
             this.c();
         }
 
         boolean flag = this.r();
         boolean flag1 = this.t();
 
-        if (this.bp) {
+        if (this.br) {
             if (flag) {
                 this.t += 0.03999999910593033D;
             } else if (flag1) {
                 this.t += 0.03999999910593033D;
             } else if (this.A) {
-                this.J();
+                this.L();
             }
         }
 
-        this.bm *= 0.98F;
-        this.bn *= 0.98F;
-        this.bo *= 0.9F;
-        this.c(this.bm, this.bn);
+        this.bo *= 0.98F;
+        this.bp *= 0.98F;
+        this.bq *= 0.9F;
+        this.c(this.bo, this.bp);
         List list = this.l.b((Entity) this, this.z.b(0.20000000298023224D, 0.0D, 0.20000000298023224D));
 
         if (list != null && list.size() > 0) {
             for (int i = 0; i < list.size(); ++i) {
                 Entity entity = (Entity) list.get(i);
 
-                if (entity.u()) {
+                if (entity.v()) {
                     entity.c((Entity) this);
                 }
             }
         }
     }
 
-    protected void J() {
+    protected void L() {
         this.t = 0.41999998688697815D;
     }
 
     protected void c() {
-        ++this.bl;
+        ++this.bn;
         EntityHuman entityhuman = this.l.a(this, -1.0D);
 
         if (entityhuman != null) {
@@ -618,48 +623,48 @@ public class EntityLiving extends Entity {
                 this.l();
             }
 
-            if (this.bl > 600 && this.V.nextInt(800) == 0) {
+            if (this.bn > 600 && this.W.nextInt(800) == 0) {
                 if (d3 < 1024.0D) {
-                    this.bl = 0;
+                    this.bn = 0;
                 } else {
                     this.l();
                 }
             }
         }
 
-        this.bm = 0.0F;
-        this.bn = 0.0F;
+        this.bo = 0.0F;
+        this.bp = 0.0F;
         float f = 8.0F;
 
-        if (this.V.nextFloat() < 0.02F) {
+        if (this.W.nextFloat() < 0.02F) {
             entityhuman = this.l.a(this, (double) f);
             if (entityhuman != null) {
-                this.c = entityhuman;
-                this.d = 10 + this.V.nextInt(20);
+                this.b = entityhuman;
+                this.c = 10 + this.W.nextInt(20);
             } else {
-                this.bo = (this.V.nextFloat() - 0.5F) * 20.0F;
+                this.bq = (this.W.nextFloat() - 0.5F) * 20.0F;
             }
         }
 
-        if (this.c != null) {
-            this.b(this.c, 10.0F);
-            if (this.d-- <= 0 || this.c.F || this.c.b((Entity) this) > (double) (f * f)) {
-                this.c = null;
+        if (this.b != null) {
+            this.b(this.b, 10.0F);
+            if (this.c-- <= 0 || this.b.G || this.b.b((Entity) this) > (double) (f * f)) {
+                this.b = null;
             }
         } else {
-            if (this.V.nextFloat() < 0.05F) {
-                this.bo = (this.V.nextFloat() - 0.5F) * 20.0F;
+            if (this.W.nextFloat() < 0.05F) {
+                this.bq = (this.W.nextFloat() - 0.5F) * 20.0F;
             }
 
-            this.v += this.bo;
-            this.w = this.bq;
+            this.v += this.bq;
+            this.w = this.bs;
         }
 
         boolean flag = this.r();
         boolean flag1 = this.t();
 
         if (flag || flag1) {
-            this.bp = this.V.nextFloat() < 0.8F;
+            this.br = this.W.nextFloat() < 0.8F;
         }
     }
 
@@ -706,7 +711,7 @@ public class EntityLiving extends Entity {
         return f + f3;
     }
 
-    public void K() {}
+    public void M() {}
 
     public boolean a() {
         return this.l.a(this.z) && this.l.a((Entity) this, this.z).size() == 0 && !this.l.b(this.z);
@@ -716,23 +721,11 @@ public class EntityLiving extends Entity {
         this.a((Entity) null, 4);
     }
 
-    public Vec3D c(float f) {
-        if (f == 1.0F) {
-            return Vec3D.b(this.p, this.q, this.r);
-        } else {
-            double d0 = this.m + (this.p - this.m) * (double) f;
-            double d1 = this.n + (this.q - this.n) * (double) f;
-            double d2 = this.o + (this.r - this.o) * (double) f;
-
-            return Vec3D.b(d0, d1, d2);
-        }
-    }
-
-    public Vec3D B() {
-        return this.d(1.0F);
+    public Vec3D C() {
+        return this.c(1.0F);
     }
 
-    public Vec3D d(float f) {
+    public Vec3D c(float f) {
         float f1;
         float f2;
         float f3;
@@ -756,14 +749,6 @@ public class EntityLiving extends Entity {
         }
     }
 
-    public MovingObjectPosition a(double d0, float f) {
-        Vec3D vec3d = this.c(f);
-        Vec3D vec3d1 = this.d(f);
-        Vec3D vec3d2 = vec3d.c(vec3d1.a * d0, vec3d1.b * d0, vec3d1.c * d0);
-
-        return this.l.a(vec3d, vec3d2);
-    }
-
     public int i() {
         return 4;
     }
