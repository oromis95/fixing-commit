@@ -80,6 +80,8 @@ public abstract class Packet {
         a(5, Packet5PlayerInventory.class);
         a(6, Packet6SpawnPosition.class);
         a(7, Packet7.class);
+        a(8, Packet8.class);
+        a(9, Packet9.class);
         a(10, Packet10Flying.class);
         a(11, Packet11PlayerPosition.class);
         a(12, Packet12PlayerLook.class);
@@ -101,6 +103,7 @@ public abstract class Packet {
         a(32, Packet32EntityLook.class);
         a(33, Packet33RelEntityMoveLook.class);
         a(34, Packet34EntityTeleport.class);
+        a(38, Packet38.class);
         a(39, Packet39.class);
         a(50, Packet50PreChunk.class);
         a(51, Packet51MapChunk.class);
