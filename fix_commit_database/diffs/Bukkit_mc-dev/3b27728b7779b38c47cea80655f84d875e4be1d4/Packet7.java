@@ -7,17 +7,20 @@ public class Packet7 extends Packet {
 
     public int a;
     public int b;
+    public int c;
 
     public Packet7() {}
 
     public void a(DataInputStream datainputstream) {
         this.a = datainputstream.readInt();
         this.b = datainputstream.readInt();
+        this.c = datainputstream.readByte();
     }
 
     public void a(DataOutputStream dataoutputstream) {
         dataoutputstream.writeInt(this.a);
         dataoutputstream.writeInt(this.b);
+        dataoutputstream.writeByte(this.c);
     }
 
     public void a(NetHandler nethandler) {
@@ -25,6 +28,6 @@ public class Packet7 extends Packet {
     }
 
     public int a() {
-        return 8;
+        return 9;
     }
 }
