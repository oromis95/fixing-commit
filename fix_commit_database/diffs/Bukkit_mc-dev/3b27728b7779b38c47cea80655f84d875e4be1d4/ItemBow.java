@@ -8,7 +8,7 @@ public class ItemBow extends Item {
     }
 
     public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
-        if (entityhuman.ak.b(Item.ARROW.aW)) {
+        if (entityhuman.al.b(Item.ARROW.aW)) {
             world.a(entityhuman, "random.bow", 1.0F, 1.0F / (b.nextFloat() * 0.4F + 0.8F));
             world.a((Entity) (new EntityArrow(world, entityhuman)));
         }
