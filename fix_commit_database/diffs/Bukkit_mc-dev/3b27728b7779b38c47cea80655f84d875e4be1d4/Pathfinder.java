@@ -23,8 +23,8 @@ public class Pathfinder {
         this.b.a();
         this.c.a();
         PathPoint pathpoint = this.a(MathHelper.b(entity.z.a), MathHelper.b(entity.z.b), MathHelper.b(entity.z.c));
-        PathPoint pathpoint1 = this.a(MathHelper.b(d0 - (double) (entity.H / 2.0F)), MathHelper.b(d1), MathHelper.b(d2 - (double) (entity.H / 2.0F)));
-        PathPoint pathpoint2 = new PathPoint(MathHelper.d(entity.H + 1.0F), MathHelper.d(entity.I + 1.0F), MathHelper.d(entity.H + 1.0F));
+        PathPoint pathpoint1 = this.a(MathHelper.b(d0 - (double) (entity.I / 2.0F)), MathHelper.b(d1), MathHelper.b(d2 - (double) (entity.I / 2.0F)));
+        PathPoint pathpoint2 = new PathPoint(MathHelper.d(entity.I + 1.0F), MathHelper.d(entity.J + 1.0F), MathHelper.d(entity.I + 1.0F));
         PathEntity pathentity = this.a(entity, pathpoint, pathpoint1, pathpoint2, f);
 
         return pathentity;
