@@ -4,14 +4,14 @@ public class EntityCreature extends EntityLiving {
 
     private PathEntity a;
     protected Entity f;
-    protected boolean ai = false;
+    protected boolean aj = false;
 
     public EntityCreature(World world) {
         super(world);
     }
 
     protected void c() {
-        this.ai = false;
+        this.aj = false;
         float f = 16.0F;
 
         if (this.f == null) {
@@ -19,19 +19,19 @@ public class EntityCreature extends EntityLiving {
             if (this.f != null) {
                 this.a = this.l.a(this, this.f, f);
             }
-        } else if (!this.f.w()) {
+        } else if (!this.f.x()) {
             this.f = null;
         } else {
             float f1 = this.f.a((Entity) this);
 
-            if (this.g(this.f)) {
+            if (this.i(this.f)) {
                 this.a(this.f, f1);
             }
         }
 
-        if (!this.ai && this.f != null && (this.a == null || this.V.nextInt(20) == 0)) {
+        if (!this.aj && this.f != null && (this.a == null || this.W.nextInt(20) == 0)) {
             this.a = this.l.a(this, this.f, f);
-        } else if (this.a == null && this.V.nextInt(80) == 0 || this.V.nextInt(80) == 0) {
+        } else if (this.a == null && this.W.nextInt(80) == 0 || this.W.nextInt(80) == 0) {
             boolean flag = false;
             int i = -1;
             int j = -1;
@@ -39,9 +39,9 @@ public class EntityCreature extends EntityLiving {
             float f2 = -99999.0F;
 
             for (int l = 0; l < 10; ++l) {
-                int i1 = MathHelper.b(this.p + (double) this.V.nextInt(13) - 6.0D);
-                int j1 = MathHelper.b(this.q + (double) this.V.nextInt(7) - 3.0D);
-                int k1 = MathHelper.b(this.r + (double) this.V.nextInt(13) - 6.0D);
+                int i1 = MathHelper.b(this.p + (double) this.W.nextInt(13) - 6.0D);
+                int j1 = MathHelper.b(this.q + (double) this.W.nextInt(7) - 3.0D);
+                int k1 = MathHelper.b(this.r + (double) this.W.nextInt(13) - 6.0D);
                 float f3 = this.a(i1, j1, k1);
 
                 if (f3 > f2) {
@@ -63,9 +63,9 @@ public class EntityCreature extends EntityLiving {
         boolean flag2 = this.t();
 
         this.w = 0.0F;
-        if (this.a != null && this.V.nextInt(100) != 0) {
+        if (this.a != null && this.W.nextInt(100) != 0) {
             Vec3D vec3d = this.a.a(this);
-            double d0 = (double) (this.H * 2.0F);
+            double d0 = (double) (this.I * 2.0F);
 
             while (vec3d != null && vec3d.d(this.p, vec3d.b, this.r) < d0 * d0) {
                 this.a.a();
@@ -77,7 +77,7 @@ public class EntityCreature extends EntityLiving {
                 }
             }
 
-            this.bp = false;
+            this.br = false;
             if (vec3d != null) {
                 double d1 = vec3d.a - this.p;
                 double d2 = vec3d.c - this.r;
@@ -85,7 +85,7 @@ public class EntityCreature extends EntityLiving {
                 float f4 = (float) (Math.atan2(d2, d1) * 180.0D / 3.1415927410125732D) - 90.0F;
                 float f5 = f4 - this.v;
 
-                for (this.bn = this.br; f5 < -180.0F; f5 += 360.0F) {
+                for (this.bp = this.bt; f5 < -180.0F; f5 += 360.0F) {
                     ;
                 }
 
@@ -102,19 +102,19 @@ public class EntityCreature extends EntityLiving {
                 }
 
                 this.v += f5;
-                if (this.ai && this.f != null) {
+                if (this.aj && this.f != null) {
                     double d4 = this.f.p - this.p;
                     double d5 = this.f.r - this.r;
                     float f6 = this.v;
 
                     this.v = (float) (Math.atan2(d5, d4) * 180.0D / 3.1415927410125732D) - 90.0F;
                     f5 = (f6 - this.v + 90.0F) * 3.1415927F / 180.0F;
-                    this.bm = -MathHelper.a(f5) * this.bn * 1.0F;
-                    this.bn = MathHelper.b(f5) * this.bn * 1.0F;
+                    this.bo = -MathHelper.a(f5) * this.bp * 1.0F;
+                    this.bp = MathHelper.b(f5) * this.bp * 1.0F;
                 }
 
                 if (d3 > 0.0D) {
-                    this.bp = true;
+                    this.br = true;
                 }
             }
 
@@ -123,11 +123,11 @@ public class EntityCreature extends EntityLiving {
             }
 
             if (this.B) {
-                this.bp = true;
+                this.br = true;
             }
 
-            if (this.V.nextFloat() < 0.8F && (flag1 || flag2)) {
-                this.bp = true;
+            if (this.W.nextFloat() < 0.8F && (flag1 || flag2)) {
+                this.br = true;
             }
         } else {
             super.c();
