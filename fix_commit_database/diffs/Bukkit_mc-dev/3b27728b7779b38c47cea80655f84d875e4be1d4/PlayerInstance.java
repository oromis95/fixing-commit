@@ -35,7 +35,7 @@ class PlayerInstance {
         if (this.b.contains(entityplayer)) {
             throw new IllegalStateException("Failed to add player. " + entityplayer + " already is in chunk " + this.c + ", " + this.d);
         } else {
-            entityplayer.ai.add(this.e);
+            entityplayer.aj.add(this.e);
             entityplayer.a.b((Packet) (new Packet50PreChunk(this.e.a, this.e.b, true)));
             this.b.add(entityplayer);
             entityplayer.f.add(this.e);
@@ -59,7 +59,7 @@ class PlayerInstance {
             }
 
             entityplayer.f.remove(this.e);
-            if (entityplayer.ai.contains(this.e)) {
+            if (entityplayer.aj.contains(this.e)) {
                 entityplayer.a.b((Packet) (new Packet50PreChunk(this.c, this.d, false)));
             }
         }
@@ -114,7 +114,7 @@ class PlayerInstance {
         for (int i = 0; i < this.b.size(); ++i) {
             EntityPlayer entityplayer = (EntityPlayer) this.b.get(i);
 
-            if (entityplayer.ai.contains(this.e)) {
+            if (entityplayer.aj.contains(this.e)) {
                 entityplayer.a.b(packet);
             }
         }
