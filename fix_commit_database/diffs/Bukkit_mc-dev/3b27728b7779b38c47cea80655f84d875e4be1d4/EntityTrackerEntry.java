@@ -9,35 +9,36 @@ public class EntityTrackerEntry {
 
     public Entity a;
     public boolean b = false;
-    public int c;
+    public boolean c = false;
     public int d;
     public int e;
     public int f;
     public int g;
     public int h;
     public int i;
-    public double j;
+    public int j;
     public double k;
     public double l;
-    public int m = 0;
-    private double p;
+    public double m;
+    public int n = 0;
     private double q;
     private double r;
-    private boolean s = false;
-    private boolean t;
-    public boolean n = false;
-    public Set o = new HashSet();
+    private double s;
+    private boolean t = false;
+    private boolean u;
+    public boolean o = false;
+    public Set p = new HashSet();
 
     public EntityTrackerEntry(Entity entity, int i, int j, boolean flag) {
         this.a = entity;
-        this.c = i;
-        this.d = j;
-        this.t = flag;
-        this.e = MathHelper.b(entity.p * 32.0D);
-        this.f = MathHelper.b(entity.q * 32.0D);
-        this.g = MathHelper.b(entity.r * 32.0D);
-        this.h = MathHelper.d(entity.v * 256.0F / 360.0F);
-        this.i = MathHelper.d(entity.w * 256.0F / 360.0F);
+        this.d = i;
+        this.e = j;
+        this.u = flag;
+        this.f = MathHelper.b(entity.p * 32.0D);
+        this.g = MathHelper.b(entity.q * 32.0D);
+        this.h = MathHelper.b(entity.r * 32.0D);
+        this.i = MathHelper.d(entity.v * 256.0F / 360.0F);
+        this.j = MathHelper.d(entity.w * 256.0F / 360.0F);
     }
 
     public boolean equals(Object object) {
@@ -49,27 +50,32 @@ public class EntityTrackerEntry {
     }
 
     public void a(List list) {
-        this.n = false;
-        if (!this.s || this.a.d(this.p, this.q, this.r) > 16.0D) {
+        if (this.a.E) {
+            this.a((Packet) (new Packet18ArmAnimation(this.a, 2)));
+            this.a.E = false;
+        }
+
+        this.o = false;
+        if (!this.t || this.a.d(this.q, this.r, this.s) > 16.0D) {
             this.b(list);
-            this.p = this.a.p;
-            this.q = this.a.q;
-            this.r = this.a.r;
-            this.s = true;
-            this.n = true;
+            this.q = this.a.p;
+            this.r = this.a.q;
+            this.s = this.a.r;
+            this.t = true;
+            this.o = true;
         }
 
-        if (this.m++ % this.d == 0) {
+        if (this.n++ % this.e == 0) {
             int i = MathHelper.b(this.a.p * 32.0D);
             int j = MathHelper.b(this.a.q * 32.0D);
             int k = MathHelper.b(this.a.r * 32.0D);
             int l = MathHelper.d(this.a.v * 256.0F / 360.0F);
             int i1 = MathHelper.d(this.a.w * 256.0F / 360.0F);
-            boolean flag = i != this.e || j != this.f || k != this.g;
-            boolean flag1 = l != this.h || i1 != this.i;
-            int j1 = i - this.e;
-            int k1 = j - this.f;
-            int l1 = k - this.g;
+            boolean flag = i != this.f || j != this.g || k != this.h;
+            boolean flag1 = l != this.i || i1 != this.j;
+            int j1 = i - this.f;
+            int k1 = j - this.g;
+            int l1 = k - this.h;
             Object object = null;
 
             if (j1 >= -128 && j1 < 128 && k1 >= -128 && k1 < 128 && l1 >= -128 && l1 < 128) {
@@ -86,18 +92,18 @@ public class EntityTrackerEntry {
                 object = new Packet34EntityTeleport(this.a.g, i, j, k, (byte) l, (byte) i1);
             }
 
-            if (this.t) {
-                double d0 = this.a.s - this.j;
-                double d1 = this.a.t - this.k;
-                double d2 = this.a.u - this.l;
+            if (this.u) {
+                double d0 = this.a.s - this.k;
+                double d1 = this.a.t - this.l;
+                double d2 = this.a.u - this.m;
                 double d3 = 0.02D;
                 double d4 = d0 * d0 + d1 * d1 + d2 * d2;
 
                 if (d4 > d3 * d3 || d4 > 0.0D && this.a.s == 0.0D && this.a.t == 0.0D && this.a.u == 0.0D) {
-                    this.j = this.a.s;
-                    this.k = this.a.t;
-                    this.l = this.a.u;
-                    this.a((Packet) (new Packet28(this.a.g, this.j, this.k, this.l)));
+                    this.k = this.a.s;
+                    this.l = this.a.t;
+                    this.m = this.a.u;
+                    this.a((Packet) (new Packet28(this.a.g, this.k, this.l, this.m)));
                 }
             }
 
@@ -107,22 +113,30 @@ public class EntityTrackerEntry {
 
             if (this.b && this.a.k == null) {
                 this.b = false;
-                this.a((Packet) (new Packet18ArmAnimation(this.a, 3)));
+                this.a((Packet) (new Packet18ArmAnimation(this.a, 101)));
             } else if (!this.b && this.a.k != null) {
                 this.b = true;
-                this.a((Packet) (new Packet18ArmAnimation(this.a, 2)));
+                this.a((Packet) (new Packet18ArmAnimation(this.a, 100)));
+            }
+
+            if (this.c && this.a.Z <= 0) {
+                this.c = false;
+                this.a((Packet) (new Packet18ArmAnimation(this.a, 103)));
+            } else if (!this.c && this.a.Z > 0) {
+                this.c = true;
+                this.a((Packet) (new Packet18ArmAnimation(this.a, 102)));
             }
 
-            this.e = i;
-            this.f = j;
-            this.g = k;
-            this.h = l;
-            this.i = i1;
+            this.f = i;
+            this.g = j;
+            this.h = k;
+            this.i = l;
+            this.j = i1;
         }
     }
 
     public void a(Packet packet) {
-        Iterator iterator = this.o.iterator();
+        Iterator iterator = this.p.iterator();
 
         while (iterator.hasNext()) {
             EntityPlayer entityplayer = (EntityPlayer) iterator.next();
@@ -137,23 +151,27 @@ public class EntityTrackerEntry {
 
     public void a(EntityPlayer entityplayer) {
         if (entityplayer != this.a) {
-            double d0 = entityplayer.p - (double) (this.e / 32);
-            double d1 = entityplayer.r - (double) (this.g / 32);
+            double d0 = entityplayer.p - (double) (this.f / 32);
+            double d1 = entityplayer.r - (double) (this.h / 32);
 
-            if (d0 >= (double) (-this.c) && d0 <= (double) this.c && d1 >= (double) (-this.c) && d1 <= (double) this.c) {
-                if (!this.o.contains(entityplayer)) {
-                    this.o.add(entityplayer);
+            if (d0 >= (double) (-this.d) && d0 <= (double) this.d && d1 >= (double) (-this.d) && d1 <= (double) this.d) {
+                if (!this.p.contains(entityplayer)) {
+                    this.p.add(entityplayer);
                     entityplayer.a.b(this.b());
                     if (this.b) {
-                        entityplayer.a.b((Packet) (new Packet18ArmAnimation(this.a, 2)));
+                        entityplayer.a.b((Packet) (new Packet18ArmAnimation(this.a, 100)));
+                    }
+
+                    if (this.c) {
+                        entityplayer.a.b((Packet) (new Packet18ArmAnimation(this.a, 102)));
                     }
 
-                    if (this.t) {
+                    if (this.u) {
                         entityplayer.a.b((Packet) (new Packet28(this.a.g, this.a.s, this.a.t, this.a.u)));
                     }
                 }
-            } else if (this.o.contains(entityplayer)) {
-                this.o.remove(entityplayer);
+            } else if (this.p.contains(entityplayer)) {
+                this.p.remove(entityplayer);
                 entityplayer.a.b((Packet) (new Packet29DestroyEntity(this.a.g)));
             }
         }
@@ -199,6 +217,8 @@ public class EntityTrackerEntry {
                 return new Packet24MobSpawn((EntityLiving) this.a);
             } else if (this.a instanceof EntityFish) {
                 return new Packet23VehicleSpawn(this.a, 90);
+            } else if (this.a instanceof EntityTNTPrimed) {
+                return new Packet23VehicleSpawn(this.a, 50);
             } else {
                 throw new IllegalArgumentException("Don\'t know how to add " + this.a.getClass() + "!");
             }
