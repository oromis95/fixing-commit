@@ -7,4 +7,19 @@ public class ItemSaddle extends Item {
         this.aX = 1;
         this.aY = 64;
     }
+
+    public void b(ItemStack itemstack, EntityLiving entityliving) {
+        if (entityliving instanceof EntityPig) {
+            EntityPig entitypig = (EntityPig) entityliving;
+
+            if (!entitypig.a) {
+                entitypig.a = true;
+                --itemstack.a;
+            }
+        }
+    }
+
+    public void a(ItemStack itemstack, EntityLiving entityliving) {
+        this.b(itemstack, entityliving);
+    }
 }
