@@ -4,13 +4,13 @@ public class EntitySpider extends EntityMonster {
 
     public EntitySpider(World world) {
         super(world);
-        this.aF = "/mob/spider.png";
+        this.aG = "/mob/spider.png";
         this.a(1.4F, 0.9F);
-        this.br = 0.8F;
+        this.bt = 0.8F;
     }
 
     public double j() {
-        return (double) this.I * 0.75D - 0.5D;
+        return (double) this.J * 0.75D - 0.5D;
     }
 
     protected Entity k() {
@@ -40,10 +40,10 @@ public class EntitySpider extends EntityMonster {
     protected void a(Entity entity, float f) {
         float f1 = this.b(1.0F);
 
-        if (f1 > 0.5F && this.V.nextInt(100) == 0) {
+        if (f1 > 0.5F && this.W.nextInt(100) == 0) {
             this.f = null;
         } else {
-            if (f > 2.0F && f < 6.0F && this.V.nextInt(10) == 0) {
+            if (f > 2.0F && f < 6.0F && this.W.nextInt(10) == 0) {
                 if (this.A) {
                     double d0 = entity.p - this.p;
                     double d1 = entity.r - this.r;
