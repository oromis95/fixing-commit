@@ -18,24 +18,28 @@ public class ItemHoe extends Item {
             Block block = Block.SOIL;
 
             world.a((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), block.bq.c(), (block.bq.a() + 1.0F) / 2.0F, block.bq.b() * 0.8F);
-            world.d(i, j, k, block.bh);
-            itemstack.a(1);
-            if (world.l.nextInt(8) == 0 && i1 == Block.GRASS.bh) {
-                byte b0 = 1;
-
-                for (int j1 = 0; j1 < b0; ++j1) {
-                    float f = 0.7F;
-                    float f1 = world.l.nextFloat() * f + (1.0F - f) * 0.5F;
-                    float f2 = 1.2F;
-                    float f3 = world.l.nextFloat() * f + (1.0F - f) * 0.5F;
-                    EntityItem entityitem = new EntityItem(world, (double) ((float) i + f1), (double) ((float) j + f2), (double) ((float) k + f3), new ItemStack(Item.SEEDS));
-
-                    entityitem.c = 10;
-                    world.a((Entity) entityitem);
+            if (world.z) {
+                return true;
+            } else {
+                world.d(i, j, k, block.bh);
+                itemstack.a(1);
+                if (world.l.nextInt(8) == 0 && i1 == Block.GRASS.bh) {
+                    byte b0 = 1;
+
+                    for (int j1 = 0; j1 < b0; ++j1) {
+                        float f = 0.7F;
+                        float f1 = world.l.nextFloat() * f + (1.0F - f) * 0.5F;
+                        float f2 = 1.2F;
+                        float f3 = world.l.nextFloat() * f + (1.0F - f) * 0.5F;
+                        EntityItem entityitem = new EntityItem(world, (double) ((float) i + f1), (double) ((float) j + f2), (double) ((float) k + f3), new ItemStack(Item.SEEDS));
+
+                        entityitem.c = 10;
+                        world.a((Entity) entityitem);
+                    }
                 }
-            }
 
-            return true;
+                return true;
+            }
         }
     }
 }
