@@ -10,7 +10,10 @@ public class ItemSnowball extends Item {
     public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
         --itemstack.a;
         world.a(entityhuman, "random.bow", 0.5F, 0.4F / (b.nextFloat() * 0.4F + 0.8F));
-        world.a((Entity) (new EntitySnowball(world, entityhuman)));
+        if (!world.z) {
+            world.a((Entity) (new EntitySnowball(world, entityhuman)));
+        }
+
         return itemstack;
     }
 }
