@@ -226,7 +226,7 @@ public class EntityPainting extends Entity {
         this.b(this.a);
     }
 
-    public void a_(double d0, double d1, double d2) {
+    public void move(double d0, double d1, double d2) {
         if (!this.world.isStatic && d0 * d0 + d1 * d1 + d2 * d2 > 0.0D) {
             this.die();
             this.world.addEntity(new EntityItem(this.world, this.locX, this.locY, this.locZ, new ItemStack(Item.PAINTING)));
