@@ -4,10 +4,10 @@ public class BlockOreBlock extends Block {
 
     public BlockOreBlock(int i, int j) {
         super(i, Material.e);
-        this.bh = j;
+        this.bg = j;
     }
 
     public int a(int i) {
-        return this.bh - 16;
+        return this.bg - 16;
     }
 }
