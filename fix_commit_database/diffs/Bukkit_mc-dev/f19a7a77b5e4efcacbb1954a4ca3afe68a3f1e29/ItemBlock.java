@@ -7,11 +7,11 @@ public class ItemBlock extends Item {
     public ItemBlock(int i) {
         super(i);
         this.a = i + 256;
-        this.a(Block.n[i + 256].a(2));
+        this.a(Block.m[i + 256].a(2));
     }
 
     public boolean a(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
-        if (world.a(i, j, k) == Block.SNOW.bi) {
+        if (world.a(i, j, k) == Block.SNOW.bh) {
             l = 0;
         } else {
             if (l == 0) {
@@ -43,12 +43,12 @@ public class ItemBlock extends Item {
             return false;
         } else {
             if (world.a(this.a, i, j, k, false)) {
-                Block block = Block.n[this.a];
+                Block block = Block.m[this.a];
 
                 if (world.d(i, j, k, this.a)) {
-                    Block.n[this.a].c(world, i, j, k, l);
-                    Block.n[this.a].a(world, i, j, k, (EntityLiving) entityhuman);
-                    world.a((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), block.br.c(), (block.br.a() + 1.0F) / 2.0F, block.br.b() * 0.8F);
+                    Block.m[this.a].c(world, i, j, k, l);
+                    Block.m[this.a].a(world, i, j, k, (EntityLiving) entityhuman);
+                    world.a((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), block.bq.c(), (block.bq.a() + 1.0F) / 2.0F, block.bq.b() * 0.8F);
                     --itemstack.a;
                 }
             }
