@@ -11,7 +11,7 @@ public class ItemRecord extends Item {
     }
 
     public boolean a(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
-        if (world.a(i, j, k) == Block.JUKEBOX.bi && world.b(i, j, k) == 0) {
+        if (world.a(i, j, k) == Block.JUKEBOX.bh && world.b(i, j, k) == 0) {
             world.b(i, j, k, this.aW - Item.GOLD_RECORD.aW + 1);
             world.a(this.a, i, j, k);
             --itemstack.a;
