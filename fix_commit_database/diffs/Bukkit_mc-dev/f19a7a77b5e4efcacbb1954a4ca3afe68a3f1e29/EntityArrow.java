@@ -219,7 +219,7 @@ public class EntityArrow extends Entity {
         this.f = nbttagcompound.b("inGround") == 1;
     }
 
-    public void a(EntityHuman entityhuman) {
+    public void b(EntityHuman entityhuman) {
         if (this.f && this.ai == entityhuman && this.a <= 0 && entityhuman.ak.a(new ItemStack(Item.ARROW.aW, 1))) {
             this.l.a(this, "random.pop", 0.2F, ((this.V.nextFloat() - this.V.nextFloat()) * 0.7F + 1.0F) * 2.0F);
             entityhuman.c(this, 1);
