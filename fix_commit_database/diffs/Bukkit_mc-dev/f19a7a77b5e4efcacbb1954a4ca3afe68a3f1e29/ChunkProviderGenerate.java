@@ -83,14 +83,14 @@ public class ChunkProviderGenerate implements IChunkProvider {
 
                                 if (k1 * 8 + l1 < b1) {
                                     if (d17 < 0.5D && k1 * 8 + l1 >= b1 - 1) {
-                                        l2 = Block.ICE.bi;
+                                        l2 = Block.ICE.bh;
                                     } else {
-                                        l2 = Block.STATIONARY_WATER.bi;
+                                        l2 = Block.STATIONARY_WATER.bh;
                                     }
                                 }
 
                                 if (d15 > 0.0D) {
-                                    l2 = Block.STONE.bi;
+                                    l2 = Block.STONE.bh;
                                 }
 
                                 abyte[j2] = (byte) l2;
@@ -134,17 +134,17 @@ public class ChunkProviderGenerate implements IChunkProvider {
                     int l1 = (k * 16 + l) * 128 + k1;
 
                     if (k1 <= 0 + this.j.nextInt(5)) {
-                        abyte[l1] = (byte) Block.BEDROCK.bi;
+                        abyte[l1] = (byte) Block.BEDROCK.bh;
                     } else {
                         byte b3 = abyte[l1];
 
                         if (b3 == 0) {
                             j1 = -1;
-                        } else if (b3 == Block.STONE.bi) {
+                        } else if (b3 == Block.STONE.bh) {
                             if (j1 == -1) {
                                 if (i1 <= 0) {
                                     b1 = 0;
-                                    b2 = (byte) Block.STONE.bi;
+                                    b2 = (byte) Block.STONE.bh;
                                 } else if (k1 >= b0 - 4 && k1 <= b0 + 1) {
                                     b1 = biomebase.o;
                                     b2 = biomebase.p;
@@ -153,20 +153,20 @@ public class ChunkProviderGenerate implements IChunkProvider {
                                     }
 
                                     if (flag1) {
-                                        b2 = (byte) Block.GRAVEL.bi;
+                                        b2 = (byte) Block.GRAVEL.bh;
                                     }
 
                                     if (flag) {
-                                        b1 = (byte) Block.SAND.bi;
+                                        b1 = (byte) Block.SAND.bh;
                                     }
 
                                     if (flag) {
-                                        b2 = (byte) Block.SAND.bi;
+                                        b2 = (byte) Block.SAND.bh;
                                     }
                                 }
 
                                 if (k1 < b0 && b1 == 0) {
-                                    b1 = (byte) Block.STATIONARY_WATER.bi;
+                                    b1 = (byte) Block.STATIONARY_WATER.bh;
                                 }
 
                                 j1 = i1;
@@ -349,49 +349,49 @@ public class ChunkProviderGenerate implements IChunkProvider {
             l1 = k + this.j.nextInt(16);
             i2 = this.j.nextInt(128);
             j2 = l + this.j.nextInt(16);
-            (new WorldGenMinable(Block.DIRT.bi, 32)).a(this.p, this.j, l1, i2, j2);
+            (new WorldGenMinable(Block.DIRT.bh, 32)).a(this.p, this.j, l1, i2, j2);
         }
 
         for (k1 = 0; k1 < 10; ++k1) {
             l1 = k + this.j.nextInt(16);
             i2 = this.j.nextInt(128);
             j2 = l + this.j.nextInt(16);
-            (new WorldGenMinable(Block.GRAVEL.bi, 32)).a(this.p, this.j, l1, i2, j2);
+            (new WorldGenMinable(Block.GRAVEL.bh, 32)).a(this.p, this.j, l1, i2, j2);
         }
 
         for (k1 = 0; k1 < 20; ++k1) {
             l1 = k + this.j.nextInt(16);
             i2 = this.j.nextInt(128);
             j2 = l + this.j.nextInt(16);
-            (new WorldGenMinable(Block.COAL_ORE.bi, 16)).a(this.p, this.j, l1, i2, j2);
+            (new WorldGenMinable(Block.COAL_ORE.bh, 16)).a(this.p, this.j, l1, i2, j2);
         }
 
         for (k1 = 0; k1 < 20; ++k1) {
             l1 = k + this.j.nextInt(16);
             i2 = this.j.nextInt(64);
             j2 = l + this.j.nextInt(16);
-            (new WorldGenMinable(Block.IRON_ORE.bi, 8)).a(this.p, this.j, l1, i2, j2);
+            (new WorldGenMinable(Block.IRON_ORE.bh, 8)).a(this.p, this.j, l1, i2, j2);
         }
 
         for (k1 = 0; k1 < 2; ++k1) {
             l1 = k + this.j.nextInt(16);
             i2 = this.j.nextInt(32);
             j2 = l + this.j.nextInt(16);
-            (new WorldGenMinable(Block.GOLD_ORE.bi, 8)).a(this.p, this.j, l1, i2, j2);
+            (new WorldGenMinable(Block.GOLD_ORE.bh, 8)).a(this.p, this.j, l1, i2, j2);
         }
 
         for (k1 = 0; k1 < 8; ++k1) {
             l1 = k + this.j.nextInt(16);
             i2 = this.j.nextInt(16);
             j2 = l + this.j.nextInt(16);
-            (new WorldGenMinable(Block.REDSTONE_ORE.bi, 7)).a(this.p, this.j, l1, i2, j2);
+            (new WorldGenMinable(Block.REDSTONE_ORE.bh, 7)).a(this.p, this.j, l1, i2, j2);
         }
 
         for (k1 = 0; k1 < 1; ++k1) {
             l1 = k + this.j.nextInt(16);
             i2 = this.j.nextInt(16);
             j2 = l + this.j.nextInt(16);
-            (new WorldGenMinable(Block.DIAMOND_ORE.bi, 7)).a(this.p, this.j, l1, i2, j2);
+            (new WorldGenMinable(Block.DIAMOND_ORE.bh, 7)).a(this.p, this.j, l1, i2, j2);
         }
 
         d0 = 0.5D;
@@ -455,28 +455,28 @@ public class ChunkProviderGenerate implements IChunkProvider {
             k2 = k + this.j.nextInt(16) + 8;
             l2 = this.j.nextInt(128);
             i3 = l + this.j.nextInt(16) + 8;
-            (new WorldGenFlowers(Block.YELLOW_FLOWER.bi)).a(this.p, this.j, k2, l2, i3);
+            (new WorldGenFlowers(Block.YELLOW_FLOWER.bh)).a(this.p, this.j, k2, l2, i3);
         }
 
         if (this.j.nextInt(2) == 0) {
             j2 = k + this.j.nextInt(16) + 8;
             k2 = this.j.nextInt(128);
             l2 = l + this.j.nextInt(16) + 8;
-            (new WorldGenFlowers(Block.RED_ROSE.bi)).a(this.p, this.j, j2, k2, l2);
+            (new WorldGenFlowers(Block.RED_ROSE.bh)).a(this.p, this.j, j2, k2, l2);
         }
 
         if (this.j.nextInt(4) == 0) {
             j2 = k + this.j.nextInt(16) + 8;
             k2 = this.j.nextInt(128);
             l2 = l + this.j.nextInt(16) + 8;
-            (new WorldGenFlowers(Block.BROWN_MUSHROOM.bi)).a(this.p, this.j, j2, k2, l2);
+            (new WorldGenFlowers(Block.BROWN_MUSHROOM.bh)).a(this.p, this.j, j2, k2, l2);
         }
 
         if (this.j.nextInt(8) == 0) {
             j2 = k + this.j.nextInt(16) + 8;
             k2 = this.j.nextInt(128);
             l2 = l + this.j.nextInt(16) + 8;
-            (new WorldGenFlowers(Block.RED_MUSHROOM.bi)).a(this.p, this.j, j2, k2, l2);
+            (new WorldGenFlowers(Block.RED_MUSHROOM.bh)).a(this.p, this.j, j2, k2, l2);
         }
 
         for (j2 = 0; j2 < 10; ++j2) {
@@ -511,14 +511,14 @@ public class ChunkProviderGenerate implements IChunkProvider {
             l2 = k + this.j.nextInt(16) + 8;
             i3 = this.j.nextInt(this.j.nextInt(120) + 8);
             j3 = l + this.j.nextInt(16) + 8;
-            (new WorldGenLiquids(Block.WATER.bi)).a(this.p, this.j, l2, i3, j3);
+            (new WorldGenLiquids(Block.WATER.bh)).a(this.p, this.j, l2, i3, j3);
         }
 
         for (k2 = 0; k2 < 20; ++k2) {
             l2 = k + this.j.nextInt(16) + 8;
             i3 = this.j.nextInt(this.j.nextInt(this.j.nextInt(112) + 8) + 8);
             j3 = l + this.j.nextInt(16) + 8;
-            (new WorldGenLiquids(Block.LAVA.bi)).a(this.p, this.j, l2, i3, j3);
+            (new WorldGenLiquids(Block.LAVA.bh)).a(this.p, this.j, l2, i3, j3);
         }
 
         this.w = this.p.a().a(this.w, k + 8, l + 8, 16, 16);
@@ -531,7 +531,7 @@ public class ChunkProviderGenerate implements IChunkProvider {
                 double d1 = this.w[i3 * 16 + j3] - (double) (k3 - 64) / 64.0D * 0.3D;
 
                 if (d1 < 0.5D && k3 > 0 && k3 < 128 && this.p.a(k2, k3, l2) == 0 && this.p.c(k2, k3 - 1, l2).c() && this.p.c(k2, k3 - 1, l2) != Material.r) {
-                    this.p.d(k2, k3, l2, Block.SNOW.bi);
+                    this.p.d(k2, k3, l2, Block.SNOW.bh);
                 }
             }
         }
