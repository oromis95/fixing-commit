@@ -11,30 +11,30 @@ public class WorldGenHellLava extends WorldGenerator {
     }
 
     public boolean a(World world, Random random, int i, int j, int k) {
-        if (world.a(i, j + 1, k) != Block.NETHERRACK.bi) {
+        if (world.a(i, j + 1, k) != Block.NETHERRACK.bh) {
             return false;
-        } else if (world.a(i, j, k) != 0 && world.a(i, j, k) != Block.NETHERRACK.bi) {
+        } else if (world.a(i, j, k) != 0 && world.a(i, j, k) != Block.NETHERRACK.bh) {
             return false;
         } else {
             int l = 0;
 
-            if (world.a(i - 1, j, k) == Block.NETHERRACK.bi) {
+            if (world.a(i - 1, j, k) == Block.NETHERRACK.bh) {
                 ++l;
             }
 
-            if (world.a(i + 1, j, k) == Block.NETHERRACK.bi) {
+            if (world.a(i + 1, j, k) == Block.NETHERRACK.bh) {
                 ++l;
             }
 
-            if (world.a(i, j, k - 1) == Block.NETHERRACK.bi) {
+            if (world.a(i, j, k - 1) == Block.NETHERRACK.bh) {
                 ++l;
             }
 
-            if (world.a(i, j, k + 1) == Block.NETHERRACK.bi) {
+            if (world.a(i, j, k + 1) == Block.NETHERRACK.bh) {
                 ++l;
             }
 
-            if (world.a(i, j - 1, k) == Block.NETHERRACK.bi) {
+            if (world.a(i, j - 1, k) == Block.NETHERRACK.bh) {
                 ++l;
             }
 
@@ -63,7 +63,7 @@ public class WorldGenHellLava extends WorldGenerator {
             if (l == 4 && i1 == 1) {
                 world.d(i, j, k, this.a);
                 world.a = true;
-                Block.n[this.a].a(world, i, j, k, random);
+                Block.m[this.a].a(world, i, j, k, random);
                 world.a = false;
             }
 
