@@ -49,8 +49,8 @@ public class ItemDoor extends Item {
 
                 int j1 = (world.d(i - b0, j, k - b1) ? 1 : 0) + (world.d(i - b0, j + 1, k - b1) ? 1 : 0);
                 int k1 = (world.d(i + b0, j, k + b1) ? 1 : 0) + (world.d(i + b0, j + 1, k + b1) ? 1 : 0);
-                boolean flag = world.a(i - b0, j, k - b1) == block.bi || world.a(i - b0, j + 1, k - b1) == block.bi;
-                boolean flag1 = world.a(i + b0, j, k + b1) == block.bi || world.a(i + b0, j + 1, k + b1) == block.bi;
+                boolean flag = world.a(i - b0, j, k - b1) == block.bh || world.a(i - b0, j + 1, k - b1) == block.bh;
+                boolean flag1 = world.a(i + b0, j, k + b1) == block.bh || world.a(i + b0, j + 1, k + b1) == block.bh;
                 boolean flag2 = false;
 
                 if (flag && !flag1) {
@@ -64,9 +64,9 @@ public class ItemDoor extends Item {
                     i1 += 4;
                 }
 
-                world.d(i, j, k, block.bi);
+                world.d(i, j, k, block.bh);
                 world.b(i, j, k, i1);
-                world.d(i, j + 1, k, block.bi);
+                world.d(i, j + 1, k, block.bh);
                 world.b(i, j + 1, k, i1 + 8);
                 --itemstack.a;
                 return true;
