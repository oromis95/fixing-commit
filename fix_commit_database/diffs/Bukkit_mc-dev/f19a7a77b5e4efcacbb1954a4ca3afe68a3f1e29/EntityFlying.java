@@ -9,8 +9,6 @@ public class EntityFlying extends EntityLiving {
     protected void a(float f) {}
 
     public void c(float f, float f1) {
-        double d0;
-
         if (this.r()) {
             this.a(f, f1, 0.02F);
             this.c(this.s, this.t, this.u);
@@ -18,7 +16,6 @@ public class EntityFlying extends EntityLiving {
             this.t *= 0.800000011920929D;
             this.u *= 0.800000011920929D;
         } else if (this.t()) {
-            d0 = this.q;
             this.a(f, f1, 0.02F);
             this.c(this.s, this.t, this.u);
             this.s *= 0.5D;
@@ -32,7 +29,7 @@ public class EntityFlying extends EntityLiving {
                 int i = this.l.a(MathHelper.b(this.p), MathHelper.b(this.z.b) - 1, MathHelper.b(this.r));
 
                 if (i > 0) {
-                    f2 = Block.n[i].bu * 0.91F;
+                    f2 = Block.m[i].bt * 0.91F;
                 }
             }
 
@@ -45,7 +42,7 @@ public class EntityFlying extends EntityLiving {
                 int j = this.l.a(MathHelper.b(this.p), MathHelper.b(this.z.b) - 1, MathHelper.b(this.r));
 
                 if (j > 0) {
-                    f2 = Block.n[j].bu * 0.91F;
+                    f2 = Block.m[j].bt * 0.91F;
                 }
             }
 
@@ -56,7 +53,7 @@ public class EntityFlying extends EntityLiving {
         }
 
         this.bb = this.bc;
-        d0 = this.p - this.m;
+        double d0 = this.p - this.m;
         double d1 = this.r - this.o;
         float f4 = MathHelper.a(d0 * d0 + d1 * d1) * 4.0F;
 
