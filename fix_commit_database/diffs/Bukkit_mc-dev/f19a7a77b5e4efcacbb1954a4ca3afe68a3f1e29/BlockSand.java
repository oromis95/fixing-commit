@@ -11,11 +11,11 @@ public class BlockSand extends Block {
     }
 
     public void e(World world, int i, int j, int k) {
-        world.h(i, j, k, this.bi);
+        world.h(i, j, k, this.bh);
     }
 
     public void b(World world, int i, int j, int k, int l) {
-        world.h(i, j, k, this.bi);
+        world.h(i, j, k, this.bh);
     }
 
     public void a(World world, int i, int j, int k, Random random) {
@@ -24,7 +24,7 @@ public class BlockSand extends Block {
 
     private void h(World world, int i, int j, int k) {
         if (g(world, i, j - 1, k) && j >= 0) {
-            EntityFallingSand entityfallingsand = new EntityFallingSand(world, (float) i + 0.5F, (float) j + 0.5F, (float) k + 0.5F, this.bi);
+            EntityFallingSand entityfallingsand = new EntityFallingSand(world, (float) i + 0.5F, (float) j + 0.5F, (float) k + 0.5F, this.bh);
 
             if (a) {
                 while (!entityfallingsand.F) {
@@ -45,10 +45,10 @@ public class BlockSand extends Block {
 
         if (l == 0) {
             return true;
-        } else if (l == Block.FIRE.bi) {
+        } else if (l == Block.FIRE.bh) {
             return true;
         } else {
-            Material material = Block.n[l].bt;
+            Material material = Block.m[l].bs;
 
             return material == Material.f ? true : material == Material.g;
         }
