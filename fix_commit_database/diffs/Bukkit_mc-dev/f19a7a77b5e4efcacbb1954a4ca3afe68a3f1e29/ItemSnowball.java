@@ -6,4 +6,11 @@ public class ItemSnowball extends Item {
         super(i);
         this.aX = 16;
     }
+
+    public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
+        --itemstack.a;
+        world.a(entityhuman, "random.bow", 0.5F, 0.4F / (b.nextFloat() * 0.4F + 0.8F));
+        world.a((Entity) (new EntitySnowball(world, entityhuman)));
+        return itemstack;
+    }
 }
