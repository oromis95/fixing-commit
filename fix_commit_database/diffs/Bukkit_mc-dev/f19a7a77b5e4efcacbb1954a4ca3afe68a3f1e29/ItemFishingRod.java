@@ -6,4 +6,22 @@ public class ItemFishingRod extends Item {
         super(i);
         this.aY = 64;
     }
+
+    public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
+        if (entityhuman.at != null) {
+            int i = entityhuman.at.c();
+
+            itemstack.a(i);
+            entityhuman.E();
+        } else {
+            world.a(entityhuman, "random.bow", 0.5F, 0.4F / (b.nextFloat() * 0.4F + 0.8F));
+            if (!world.z) {
+                world.a((Entity) (new EntityFish(world, entityhuman)));
+            }
+
+            entityhuman.E();
+        }
+
+        return itemstack;
+    }
 }
