@@ -6,4 +6,13 @@ public class ItemBow extends Item {
         super(i);
         this.aX = 1;
     }
+
+    public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
+        if (entityhuman.ak.b(Item.ARROW.aW)) {
+            world.a(entityhuman, "random.bow", 1.0F, 1.0F / (b.nextFloat() * 0.4F + 0.8F));
+            world.a((Entity) (new EntityArrow(world, entityhuman)));
+        }
+
+        return itemstack;
+    }
 }
