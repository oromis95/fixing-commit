@@ -6,7 +6,7 @@ public class BlockReed extends Block {
 
     protected BlockReed(int i, int j) {
         super(i, Material.i);
-        this.bh = j;
+        this.bg = j;
         float f = 0.375F;
 
         this.a(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, 1.0F, 0.5F + f);
@@ -17,7 +17,7 @@ public class BlockReed extends Block {
         if (world.a(i, j + 1, k) == 0) {
             int l;
 
-            for (l = 1; world.a(i, j - l, k) == this.bi; ++l) {
+            for (l = 1; world.a(i, j - l, k) == this.bh; ++l) {
                 ;
             }
 
@@ -25,7 +25,7 @@ public class BlockReed extends Block {
                 int i1 = world.b(i, j, k);
 
                 if (i1 == 15) {
-                    world.d(i, j + 1, k, this.bi);
+                    world.d(i, j + 1, k, this.bh);
                     world.b(i, j, k, 0);
                 } else {
                     world.b(i, j, k, i1 + 1);
@@ -37,7 +37,7 @@ public class BlockReed extends Block {
     public boolean a(World world, int i, int j, int k) {
         int l = world.a(i, j - 1, k);
 
-        return l == this.bi ? true : (l != Block.GRASS.bi && l != Block.DIRT.bi ? false : (world.c(i - 1, j - 1, k) == Material.f ? true : (world.c(i + 1, j - 1, k) == Material.f ? true : (world.c(i, j - 1, k - 1) == Material.f ? true : world.c(i, j - 1, k + 1) == Material.f))));
+        return l == this.bh ? true : (l != Block.GRASS.bh && l != Block.DIRT.bh ? false : (world.c(i - 1, j - 1, k) == Material.f ? true : (world.c(i + 1, j - 1, k) == Material.f ? true : (world.c(i, j - 1, k - 1) == Material.f ? true : world.c(i, j - 1, k + 1) == Material.f))));
     }
 
     public void b(World world, int i, int j, int k, int l) {
