@@ -12,7 +12,7 @@ public final class ItemStack {
     }
 
     public ItemStack(Block block, int i) {
-        this(block.bi, i);
+        this(block.bh, i);
     }
 
     public ItemStack(Item item) {
@@ -57,6 +57,10 @@ public final class ItemStack {
         return this.a().a(this, block);
     }
 
+    public ItemStack a(World world, EntityHuman entityhuman) {
+        return this.a().a(this, world, entityhuman);
+    }
+
     public NBTTagCompound a(NBTTagCompound nbttagcompound) {
         nbttagcompound.a("id", (short) this.c);
         nbttagcompound.a("Count", (byte) this.a);
