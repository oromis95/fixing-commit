@@ -9,7 +9,9 @@ public class WorldProviderHell extends WorldProvider {
     public void a() {
         this.b = new WorldChunkManagerHell(BiomeBase.HELL, 1.0D, 0.0D);
         this.c = true;
-        this.e = -1;
+        this.d = true;
+        this.e = true;
+        this.g = -1;
     }
 
     protected void b() {
@@ -18,7 +20,7 @@ public class WorldProviderHell extends WorldProvider {
         for (int i = 0; i <= 15; ++i) {
             float f1 = 1.0F - (float) i / 15.0F;
 
-            this.d[i] = (1.0F - f1) / (f1 * 3.0F + 1.0F) * (1.0F - f) + f;
+            this.f[i] = (1.0F - f1) / (f1 * 3.0F + 1.0F) * (1.0F - f) + f;
         }
     }
 
@@ -36,7 +38,7 @@ public class WorldProviderHell extends WorldProvider {
     public boolean a(int i, int j) {
         int k = this.a.a(i, j);
 
-        return k == Block.BEDROCK.bi ? false : (k == 0 ? false : Block.p[k]);
+        return k == Block.BEDROCK.bh ? false : (k == 0 ? false : Block.o[k]);
     }
 
     public float a(long i, float f) {
