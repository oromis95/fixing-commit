@@ -134,17 +134,17 @@ public class BlockLever extends Block {
         world.b(i, j, k, i1 + j1);
         world.b(i, j, k, i, j, k);
         world.a((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.click", 0.3F, j1 > 0 ? 0.6F : 0.5F);
-        world.g(i, j, k, this.bi);
+        world.g(i, j, k, this.bh);
         if (i1 == 1) {
-            world.g(i - 1, j, k, this.bi);
+            world.g(i - 1, j, k, this.bh);
         } else if (i1 == 2) {
-            world.g(i + 1, j, k, this.bi);
+            world.g(i + 1, j, k, this.bh);
         } else if (i1 == 3) {
-            world.g(i, j, k - 1, this.bi);
+            world.g(i, j, k - 1, this.bh);
         } else if (i1 == 4) {
-            world.g(i, j, k + 1, this.bi);
+            world.g(i, j, k + 1, this.bh);
         } else {
-            world.g(i, j - 1, k, this.bi);
+            world.g(i, j - 1, k, this.bh);
         }
 
         return true;
@@ -154,19 +154,19 @@ public class BlockLever extends Block {
         int l = world.b(i, j, k);
 
         if ((l & 8) > 0) {
-            world.g(i, j, k, this.bi);
+            world.g(i, j, k, this.bh);
             int i1 = l & 7;
 
             if (i1 == 1) {
-                world.g(i - 1, j, k, this.bi);
+                world.g(i - 1, j, k, this.bh);
             } else if (i1 == 2) {
-                world.g(i + 1, j, k, this.bi);
+                world.g(i + 1, j, k, this.bh);
             } else if (i1 == 3) {
-                world.g(i, j, k - 1, this.bi);
+                world.g(i, j, k - 1, this.bh);
             } else if (i1 == 4) {
-                world.g(i, j, k + 1, this.bi);
+                world.g(i, j, k + 1, this.bh);
             } else {
-                world.g(i, j - 1, k, this.bi);
+                world.g(i, j - 1, k, this.bh);
             }
         }
 
