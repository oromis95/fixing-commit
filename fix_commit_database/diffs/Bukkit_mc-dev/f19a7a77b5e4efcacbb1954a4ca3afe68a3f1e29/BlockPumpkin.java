@@ -6,13 +6,13 @@ public class BlockPumpkin extends Block {
 
     protected BlockPumpkin(int i, int j, boolean flag) {
         super(i, Material.w);
-        this.bh = j;
+        this.bg = j;
         this.a(true);
         this.a = flag;
     }
 
     public int a(int i) {
-        return i == 1 ? this.bh : (i == 0 ? this.bh : (i == 3 ? this.bh + 1 + 16 : this.bh + 16));
+        return i == 1 ? this.bg : (i == 0 ? this.bg : (i == 3 ? this.bg + 1 + 16 : this.bg + 16));
     }
 
     public void e(World world, int i, int j, int k) {
@@ -22,7 +22,7 @@ public class BlockPumpkin extends Block {
     public boolean a(World world, int i, int j, int k) {
         int l = world.a(i, j, k);
 
-        return (l == 0 || Block.n[l].bt.d()) && world.d(i, j - 1, k);
+        return (l == 0 || Block.m[l].bs.d()) && world.d(i, j - 1, k);
     }
 
     public void a(World world, int i, int j, int k, EntityLiving entityliving) {
