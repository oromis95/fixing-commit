@@ -9,11 +9,11 @@ public class BlockTNT extends Block {
     }
 
     public int a(int i) {
-        return i == 0 ? this.bh + 2 : (i == 1 ? this.bh + 1 : this.bh);
+        return i == 0 ? this.bg + 2 : (i == 1 ? this.bg + 1 : this.bg);
     }
 
     public void b(World world, int i, int j, int k, int l) {
-        if (l > 0 && Block.n[l].c() && world.n(i, j, k)) {
+        if (l > 0 && Block.m[l].c() && world.n(i, j, k)) {
             this.a(world, i, j, k, 0);
             world.d(i, j, k, 0);
         }
