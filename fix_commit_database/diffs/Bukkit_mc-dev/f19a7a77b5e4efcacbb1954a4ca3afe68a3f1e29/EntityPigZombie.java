@@ -4,20 +4,20 @@ import java.util.List;
 
 public class EntityPigZombie extends EntityZombie {
 
-    private boolean a = false;
+    private int a = 0;
     private int b = 0;
     private static final ItemStack c = new ItemStack(Item.GOLD_SWORD, 1);
 
     public EntityPigZombie(World world) {
         super(world);
         this.aF = "/mob/pigzombie.png";
-        this.bl = 0.5F;
+        this.br = 0.5F;
         this.e = 5;
         this.ad = true;
     }
 
     public void b_() {
-        this.bl = this.f != null ? 0.95F : 0.5F;
+        this.br = this.f != null ? 0.95F : 0.5F;
         if (this.b > 0 && --this.b == 0) {
             this.l.a(this, "mob.zombiepig.zpigangry", this.h() * 2.0F, ((this.V.nextFloat() - this.V.nextFloat()) * 0.2F + 1.0F) * 1.8F);
         }
@@ -31,16 +31,16 @@ public class EntityPigZombie extends EntityZombie {
 
     public void a(NBTTagCompound nbttagcompound) {
         super.a(nbttagcompound);
-        nbttagcompound.a("Angry", this.a);
+        nbttagcompound.a("Anger", (short) this.a);
     }
 
     public void b(NBTTagCompound nbttagcompound) {
         super.b(nbttagcompound);
-        this.a = nbttagcompound.l("Angry");
+        this.a = nbttagcompound.c("Anger");
     }
 
     protected Entity k() {
-        return !this.a ? null : super.k();
+        return this.a == 0 ? null : super.k();
     }
 
     public void D() {
@@ -69,7 +69,7 @@ public class EntityPigZombie extends EntityZombie {
 
     private void h(Entity entity) {
         this.f = entity;
-        this.a = true;
+        this.a = 400 + this.V.nextInt(400);
         this.b = this.V.nextInt(40);
     }
 
