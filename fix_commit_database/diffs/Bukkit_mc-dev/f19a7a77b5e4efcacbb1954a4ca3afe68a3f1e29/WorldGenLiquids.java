@@ -11,28 +11,28 @@ public class WorldGenLiquids extends WorldGenerator {
     }
 
     public boolean a(World world, Random random, int i, int j, int k) {
-        if (world.a(i, j + 1, k) != Block.STONE.bi) {
+        if (world.a(i, j + 1, k) != Block.STONE.bh) {
             return false;
-        } else if (world.a(i, j - 1, k) != Block.STONE.bi) {
+        } else if (world.a(i, j - 1, k) != Block.STONE.bh) {
             return false;
-        } else if (world.a(i, j, k) != 0 && world.a(i, j, k) != Block.STONE.bi) {
+        } else if (world.a(i, j, k) != 0 && world.a(i, j, k) != Block.STONE.bh) {
             return false;
         } else {
             int l = 0;
 
-            if (world.a(i - 1, j, k) == Block.STONE.bi) {
+            if (world.a(i - 1, j, k) == Block.STONE.bh) {
                 ++l;
             }
 
-            if (world.a(i + 1, j, k) == Block.STONE.bi) {
+            if (world.a(i + 1, j, k) == Block.STONE.bh) {
                 ++l;
             }
 
-            if (world.a(i, j, k - 1) == Block.STONE.bi) {
+            if (world.a(i, j, k - 1) == Block.STONE.bh) {
                 ++l;
             }
 
-            if (world.a(i, j, k + 1) == Block.STONE.bi) {
+            if (world.a(i, j, k + 1) == Block.STONE.bh) {
                 ++l;
             }
 
@@ -57,7 +57,7 @@ public class WorldGenLiquids extends WorldGenerator {
             if (l == 3 && i1 == 1) {
                 world.d(i, j, k, this.a);
                 world.a = true;
-                Block.n[this.a].a(world, i, j, k, random);
+                Block.m[this.a].a(world, i, j, k, random);
                 world.a = false;
             }
 
