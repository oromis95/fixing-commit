@@ -76,17 +76,18 @@ public class BlockCrops extends BlockFlower {
 
     public void a(World world, int i, int j, int k, int l) {
         super.a(world, i, j, k, l);
-
-        for (int i1 = 0; i1 < 3; ++i1) {
-            if (world.l.nextInt(15) <= l) {
-                float f = 0.7F;
-                float f1 = world.l.nextFloat() * f + (1.0F - f) * 0.5F;
-                float f2 = world.l.nextFloat() * f + (1.0F - f) * 0.5F;
-                float f3 = world.l.nextFloat() * f + (1.0F - f) * 0.5F;
-                EntityItem entityitem = new EntityItem(world, (double) ((float) i + f1), (double) ((float) j + f2), (double) ((float) k + f3), new ItemStack(Item.SEEDS));
-
-                entityitem.c = 10;
-                world.a((Entity) entityitem);
+        if (!world.z) {
+            for (int i1 = 0; i1 < 3; ++i1) {
+                if (world.l.nextInt(15) <= l) {
+                    float f = 0.7F;
+                    float f1 = world.l.nextFloat() * f + (1.0F - f) * 0.5F;
+                    float f2 = world.l.nextFloat() * f + (1.0F - f) * 0.5F;
+                    float f3 = world.l.nextFloat() * f + (1.0F - f) * 0.5F;
+                    EntityItem entityitem = new EntityItem(world, (double) ((float) i + f1), (double) ((float) j + f2), (double) ((float) k + f3), new ItemStack(Item.SEEDS));
+
+                    entityitem.c = 10;
+                    world.a((Entity) entityitem);
+                }
             }
         }
     }
