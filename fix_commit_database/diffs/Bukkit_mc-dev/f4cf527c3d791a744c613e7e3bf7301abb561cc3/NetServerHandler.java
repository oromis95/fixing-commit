@@ -14,7 +14,6 @@ public class NetServerHandler extends NetHandler implements ICommandListener {
     private double h;
     private double i;
     private boolean j = true;
-    private String color;
     private ItemStack k = null;
 
     public NetServerHandler(MinecraftServer minecraftserver, NetworkManager networkmanager, EntityPlayer entityplayer) {
@@ -23,7 +22,6 @@ public class NetServerHandler extends NetHandler implements ICommandListener {
         networkmanager.a((NetHandler) this);
         this.e = entityplayer;
         entityplayer.a = this;
-        this.color = Integer.toHexString((int) (Math.random() * 15.0D));
     }
 
     public void a() {
@@ -41,11 +39,11 @@ public class NetServerHandler extends NetHandler implements ICommandListener {
     }
 
     public void a(Packet10Flying packet10flying) {
-        double d1;
+        double d0;
 
         if (!this.j) {
-            d1 = packet10flying.b - this.h;
-            if (packet10flying.a == this.g && d1 * d1 < 0.01D && packet10flying.c == this.i) {
+            d0 = packet10flying.b - this.h;
+            if (packet10flying.a == this.g && d0 * d0 < 0.01D && packet10flying.c == this.i) {
                 this.j = true;
             }
         }
@@ -54,61 +52,61 @@ public class NetServerHandler extends NetHandler implements ICommandListener {
             this.g = this.e.l;
             this.h = this.e.m;
             this.i = this.e.n;
-            d1 = this.e.l;
-            double d2 = this.e.m;
-            double d3 = this.e.n;
-            float f1 = this.e.r;
-            float f2 = this.e.s;
-            double d4;
+            d0 = this.e.l;
+            double d1 = this.e.m;
+            double d2 = this.e.n;
+            float f = this.e.r;
+            float f1 = this.e.s;
+            double d3;
 
             if (packet10flying.h) {
-                d1 = packet10flying.a;
-                d2 = packet10flying.b;
-                d3 = packet10flying.c;
-                d4 = packet10flying.d - packet10flying.b;
-                if (d4 > 1.65D || d4 < 0.1D) {
+                d0 = packet10flying.a;
+                d1 = packet10flying.b;
+                d2 = packet10flying.c;
+                d3 = packet10flying.d - packet10flying.b;
+                if (d3 > 1.65D || d3 < 0.1D) {
                     this.c("Illegal stance");
-                    a.warning(this.e.aq + " had an illegal stance: " + d4);
+                    a.warning(this.e.aq + " had an illegal stance: " + d3);
                 }
 
                 this.e.ai = packet10flying.d;
             }
 
             if (packet10flying.i) {
-                f1 = packet10flying.e;
-                f2 = packet10flying.f;
+                f = packet10flying.e;
+                f1 = packet10flying.f;
             }
 
             this.e.i();
             this.e.M = 0.0F;
-            this.e.b(this.g, this.h, this.i, f1, f2);
-            d4 = d1 - this.e.l;
-            double d5 = d2 - this.e.m;
-            double d6 = d3 - this.e.n;
-            float f3 = 0.0625F;
-            boolean l = this.d.e.a(this.e, this.e.v.b().e((double) f3, (double) f3, (double) f3)).size() == 0;
-
-            this.e.c(d4, d5, d6);
-            d4 = d1 - this.e.l;
-            d5 = d2 - this.e.m;
-            if (d5 > -0.5D || d5 < 0.5D) {
-                d5 = 0.0D;
+            this.e.b(this.g, this.h, this.i, f, f1);
+            d3 = d0 - this.e.l;
+            double d4 = d1 - this.e.m;
+            double d5 = d2 - this.e.n;
+            float f2 = 0.0625F;
+            boolean flag = this.d.e.a(this.e, this.e.v.b().e((double) f2, (double) f2, (double) f2)).size() == 0;
+
+            this.e.c(d3, d4, d5);
+            d3 = d0 - this.e.l;
+            d4 = d1 - this.e.m;
+            if (d4 > -0.5D || d4 < 0.5D) {
+                d4 = 0.0D;
             }
 
-            d6 = d3 - this.e.n;
-            double d7 = d4 * d4 + d5 * d5 + d6 * d6;
-            boolean i1 = false;
+            d5 = d2 - this.e.n;
+            double d6 = d3 * d3 + d4 * d4 + d5 * d5;
+            boolean flag1 = false;
 
-            if (d7 > 0.0625D) {
-                i1 = true;
+            if (d6 > 0.0625D) {
+                flag1 = true;
                 a.warning(this.e.aq + " moved wrongly!");
             }
 
-            this.e.b(d1, d2, d3, f1, f2);
-            boolean i2 = this.d.e.a(this.e, this.e.v.b().e((double) f3, (double) f3, (double) f3)).size() == 0;
+            this.e.b(d0, d1, d2, f, f1);
+            boolean flag2 = this.d.e.a(this.e, this.e.v.b().e((double) f2, (double) f2, (double) f2)).size() == 0;
 
-            if (l && (i1 || !i2)) {
-                this.a(this.g, this.h, this.i, f1, f2);
+            if (flag && (flag1 || !flag2)) {
+                this.a(this.g, this.h, this.i, f, f1);
                 return;
             }
 
@@ -123,67 +121,67 @@ public class NetServerHandler extends NetHandler implements ICommandListener {
         this.h = d1;
         this.i = d2;
         this.e.b(d0, d1, d2, f, f1);
-        this.e.a.b((Packet) (new Packet13PlayerLookMove(d0, d1 + 1.620000004768372D, d1, d2, f, f1, false)));
+        this.e.a.b((Packet) (new Packet13PlayerLookMove(d0, d1 + 1.6200000047683716D, d1, d2, f, f1, false)));
     }
 
     public void a(Packet14BlockDig packet14blockdig) {
         this.e.aj.a[this.e.aj.d] = this.k;
-        boolean bool = this.d.e.z = this.d.f.g(this.e.aq);
-        boolean l = false;
+        boolean flag = this.d.e.z = this.d.f.g(this.e.aq);
+        boolean flag1 = false;
 
         if (packet14blockdig.e == 0) {
-            l = true;
+            flag1 = true;
         }
 
         if (packet14blockdig.e == 1) {
-            l = true;
+            flag1 = true;
         }
 
-        if (l) {
-            double d1 = this.e.m;
+        if (flag1) {
+            double d0 = this.e.m;
 
             this.e.m = this.e.ai;
-            MovingObjectPosition i3 = this.e.a(4.0D, 1.0F);
+            MovingObjectPosition movingobjectposition = this.e.a(4.0D, 1.0F);
 
-            this.e.m = d1;
-            if (i3 == null) {
+            this.e.m = d0;
+            if (movingobjectposition == null) {
                 return;
             }
 
-            if (i3.b != packet14blockdig.a || i3.c != packet14blockdig.b || i3.d != packet14blockdig.c || i3.e != packet14blockdig.d) {
+            if (movingobjectposition.b != packet14blockdig.a || movingobjectposition.c != packet14blockdig.b || movingobjectposition.d != packet14blockdig.c || movingobjectposition.e != packet14blockdig.d) {
                 return;
             }
         }
 
-        int i1 = packet14blockdig.a;
-        int i2 = packet14blockdig.b;
-        int i31 = packet14blockdig.c;
-        int i4 = packet14blockdig.d;
-        int i5 = (int) MathHelper.e((float) (i1 - this.d.e.n));
-        int i6 = (int) MathHelper.e((float) (i31 - this.d.e.p));
+        int i = packet14blockdig.a;
+        int j = packet14blockdig.b;
+        int k = packet14blockdig.c;
+        int l = packet14blockdig.d;
+        int i1 = (int) MathHelper.e((float) (i - this.d.e.n));
+        int j1 = (int) MathHelper.e((float) (k - this.d.e.p));
 
-        if (i5 > i6) {
-            i6 = i5;
+        if (i1 > j1) {
+            j1 = i1;
         }
 
         if (packet14blockdig.e == 0) {
-            if (i6 > 16 || bool) {
-                this.e.ad.a(i1, i2, i31);
+            if (j1 > 16 || flag) {
+                this.e.ad.a(i, j, k);
             }
         } else if (packet14blockdig.e == 2) {
             this.e.ad.a();
         } else if (packet14blockdig.e == 1) {
-            if (i6 > 16 || bool) {
-                this.e.ad.a(i1, i2, i31, i4);
+            if (j1 > 16 || flag) {
+                this.e.ad.a(i, j, k, l);
             }
         } else if (packet14blockdig.e == 3) {
-            double d2 = this.e.l - ((double) i1 + 0.5D);
-            double d3 = this.e.m - ((double) i2 + 0.5D);
-            double d4 = this.e.n - ((double) i31 + 0.5D);
-            double d5 = d2 * d2 + d3 * d3 + d4 * d4;
+            double d1 = this.e.l - ((double) i + 0.5D);
+            double d2 = this.e.m - ((double) j + 0.5D);
+            double d3 = this.e.n - ((double) k + 0.5D);
+            double d4 = d1 * d1 + d2 * d2 + d3 * d3;
 
-            if (d5 < 256.0D) {
-                this.e.a.b((Packet) (new Packet53BlockChange(i1, i2, i31, this.d.e)));
+            if (d4 < 256.0D) {
+                this.e.a.b((Packet) (new Packet53BlockChange(i, j, k, this.d.e)));
             }
         }
 
@@ -191,25 +189,25 @@ public class NetServerHandler extends NetHandler implements ICommandListener {
     }
 
     public void a(Packet15Place packet15place) {
-        boolean bool = this.d.e.z = this.d.f.g(this.e.aq);
-        int l = packet15place.b;
-        int i1 = packet15place.c;
-        int i2 = packet15place.d;
-        int i3 = packet15place.e;
-        int i4 = (int) MathHelper.e((float) (l - this.d.e.n));
-        int i5 = (int) MathHelper.e((float) (i2 - this.d.e.p));
-
-        if (i4 > i5) {
-            i5 = i4;
+        boolean flag = this.d.e.z = this.d.f.g(this.e.aq);
+        int i = packet15place.b;
+        int j = packet15place.c;
+        int k = packet15place.d;
+        int l = packet15place.e;
+        int i1 = (int) MathHelper.e((float) (i - this.d.e.n));
+        int j1 = (int) MathHelper.e((float) (k - this.d.e.p));
+
+        if (i1 > j1) {
+            j1 = i1;
         }
 
-        if (i5 > 16 || bool) {
-            ItemStack localgp = packet15place.a >= 0 ? new ItemStack(packet15place.a) : null;
+        if (j1 > 16 || flag) {
+            ItemStack itemstack = packet15place.a >= 0 ? new ItemStack(packet15place.a) : null;
 
-            this.e.ad.a(this.e, this.d.e, localgp, l, i1, i2, i3);
+            this.e.ad.a(this.e, this.d.e, itemstack, i, j, k, l);
         }
 
-        this.e.a.b((Packet) (new Packet53BlockChange(l, i1, i2, this.d.e)));
+        this.e.a.b((Packet) (new Packet53BlockChange(i, j, k, this.d.e)));
         this.d.e.z = false;
     }
 
@@ -220,7 +218,7 @@ public class NetServerHandler extends NetHandler implements ICommandListener {
     }
 
     public void a(Packet packet) {
-        a.warning(super.getClass() + " wasn\'t prepared to deal with a " + packet.getClass());
+        a.warning(this.getClass() + " wasn\'t prepared to deal with a " + packet.getClass());
         this.c("Protocol error, unexpected packet");
     }
 
@@ -229,53 +227,53 @@ public class NetServerHandler extends NetHandler implements ICommandListener {
     }
 
     public void a(Packet16BlockItemSwitch packet16blockitemswitch) {
-        int l = packet16blockitemswitch.b;
+        int i = packet16blockitemswitch.b;
 
         this.e.aj.d = this.e.aj.a.length - 1;
-        if (l == 0) {
+        if (i == 0) {
             this.k = null;
         } else {
-            this.k = new ItemStack(l);
+            this.k = new ItemStack(i);
         }
 
         this.e.aj.a[this.e.aj.d] = this.k;
-        this.d.k.a(this.e, new Packet16BlockItemSwitch(this.e.c, l));
+        this.d.k.a(this.e, new Packet16BlockItemSwitch(this.e.c, i));
     }
 
     public void a(Packet21PickupSpawn packet21pickupspawn) {
-        double d1 = (double) packet21pickupspawn.b / 32.0D;
-        double d2 = (double) packet21pickupspawn.c / 32.0D;
-        double d3 = (double) packet21pickupspawn.d / 32.0D;
-        EntityItem localfn = new EntityItem(this.d.e, d1, d2, d3, new ItemStack(packet21pickupspawn.h, packet21pickupspawn.i));
-
-        localfn.o = (double) packet21pickupspawn.e / 128.0D;
-        localfn.p = (double) packet21pickupspawn.f / 128.0D;
-        localfn.q = (double) packet21pickupspawn.g / 128.0D;
-        localfn.ad = 10;
-        this.d.e.a(localfn);
+        double d0 = (double) packet21pickupspawn.b / 32.0D;
+        double d1 = (double) packet21pickupspawn.c / 32.0D;
+        double d2 = (double) packet21pickupspawn.d / 32.0D;
+        EntityItem entityitem = new EntityItem(this.d.e, d0, d1, d2, new ItemStack(packet21pickupspawn.h, packet21pickupspawn.i));
+
+        entityitem.o = (double) packet21pickupspawn.e / 128.0D;
+        entityitem.p = (double) packet21pickupspawn.f / 128.0D;
+        entityitem.q = (double) packet21pickupspawn.g / 128.0D;
+        entityitem.ad = 10;
+        this.d.e.a(entityitem);
     }
 
     public void a(Packet3Chat packet3chat) {
-        String str = packet3chat.a;
+        String s = packet3chat.a;
 
-        if (str.length() > 100) {
+        if (s.length() > 100) {
             this.c("Chat message too long");
         } else {
-            str = str.trim();
+            s = s.trim();
 
-            for (int l = 0; l < str.length(); ++l) {
-                if (" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_\'abcdefghijklmnopqrstuvwxyz{|}~⌂\u00C7\u00FC\u00E9\u00E2\u00E4\u00E0\u00E5\u00E7\u00EA\u00EB\u00E8\u00EF\u00EE\u00EC\u00C4\u00C5\u00C9\u00E6\u00C6\u00F4\u00F6\u00F2\u00FB\u00F9\u00FF\u00D6\u00DC\u00F8\u00A3\u00D8\u00D7ƒ\u00E1\u00ED\u00F3\u00FA\u00F1\u00D1\u00AA\u00BA\u00BF\u00AE\u00AC\u00BD\u00BC\u00A1\u00AB\u00BB".indexOf(str.charAt(l)) < 0) {
+            for (int i = 0; i < s.length(); ++i) {
+                if (" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_\'abcdefghijklmnopqrstuvwxyz{|}~⌂\u00C7\u00FC\u00E9\u00E2\u00E4\u00E0\u00E5\u00E7\u00EA\u00EB\u00E8\u00EF\u00EE\u00EC\u00C4\u00C5\u00C9\u00E6\u00C6\u00F4\u00F6\u00F2\u00FB\u00F9\u00FF\u00D6\u00DC\u00F8\u00A3\u00D8\u00D7ƒ\u00E1\u00ED\u00F3\u00FA\u00F1\u00D1\u00AA\u00BA\u00BF\u00AE\u00AC\u00BD\u00BC\u00A1\u00AB\u00BB".indexOf(s.charAt(i)) < 0) {
                     this.c("Illegal characters in chat");
                     return;
                 }
             }
 
-            if (str.startsWith("/")) {
-                this.d(str);
+            if (s.startsWith("/")) {
+                this.d(s);
             } else {
-                str = "<\u00A7" + this.color + this.e.aq + "\u00A7f> " + str;
-                a.info(str);
-                this.d.f.a((Packet) (new Packet3Chat(str)));
+                s = "<" + this.e.aq + "> " + s;
+                a.info(s);
+                this.d.f.a((Packet) (new Packet3Chat(s)));
             }
         }
     }
@@ -286,58 +284,58 @@ public class NetServerHandler extends NetHandler implements ICommandListener {
             a.info(s);
             this.d.f.a((Packet) (new Packet3Chat(s)));
         } else if (s.toLowerCase().startsWith("/tell ")) {
-            String[] l = s.split(" ");
+            String[] astring = s.split(" ");
 
-            if (l.length >= 3) {
+            if (astring.length >= 3) {
                 s = s.substring(s.indexOf(" ")).trim();
                 s = s.substring(s.indexOf(" ")).trim();
                 s = "\u00A77" + this.e.aq + " whispers " + s;
-                a.info(s + " to " + l[1]);
-                if (!this.d.f.a(l[1], (Packet) (new Packet3Chat(s)))) {
+                a.info(s + " to " + astring[1]);
+                if (!this.d.f.a(astring[1], (Packet) (new Packet3Chat(s)))) {
                     this.b((Packet) (new Packet3Chat("\u00A7cThere\'s no player by that name online.")));
                 }
             }
-        } else if (s.toLowerCase().startsWith("/color ")) {
-            String s1 = s.substring(s.indexOf(" ")).trim();
-
-            if (s1.matches("[0123456789abcdefABCDEF]")) {
-                this.color = s1;
-                this.b((Packet) (new Packet3Chat("\u00A7" + s1 + "Your color has changed to " + s1)));
-            } else {
-                this.b((Packet) (new Packet3Chat("\u00A7c/color only accepts a single hex digit.")));
-                this.b((Packet) (new Packet3Chat("Values: \u00A700\u00A711\u00A722\u00A733\u00A744\u00A755\u00A766\u00A777\u00A788\u00A799\u00A7AA\u00A7BB\u00A7CC\u00A7DD\u00A7EE\u00A7FF")));
-            }
-        } else if (s.toLowerCase().equalsIgnoreCase("/color")) {
-            this.b((Packet) (new Packet3Chat("\u00A7cUse /color to change the color of your name.")));
-            this.b((Packet) (new Packet3Chat("Values: \u00A700\u00A711\u00A722\u00A733\u00A744\u00A755\u00A766\u00A777\u00A788\u00A799\u00A7AA\u00A7BB\u00A7CC\u00A7DD\u00A7EE\u00A7FF")));
         } else {
             int i;
 
-            if (s.toLowerCase().equalsIgnoreCase("/playerlist")) {
-                for (i = 0; i < this.d.f.b.size(); ++i) {
-                    EntityPlayer str = (EntityPlayer) this.d.f.b.get(i);
-
-                    this.b((Packet) (new Packet3Chat("\u00A7" + str.a.color + str.aq)));
-                }
-            } else if (s.toLowerCase().equalsIgnoreCase("/help")) {
-                this.b((Packet) (new Packet3Chat("\u00A7e/home - return to the spawn point")));
-                this.b((Packet) (new Packet3Chat("\u00A7e/playerlist - list the players on the server")));
-                this.b((Packet) (new Packet3Chat("\u00A7e/tell - whisper to a player")));
-                this.b((Packet) (new Packet3Chat("\u00A7e/color - change your name\'s color")));
-            } else if (s.toLowerCase().equalsIgnoreCase("/home")) {
+            if (s.toLowerCase().equalsIgnoreCase("/home")) {
                 a.info(this.e.aq + " returned home");
                 i = this.d.e.d(this.d.e.n, this.d.e.p);
                 this.a((double) this.d.e.n + 0.5D, (double) i + 1.5D, (double) this.d.e.p + 0.5D, 0.0F, 0.0F);
+            } else if (s.toLowerCase().equalsIgnoreCase("/iron")) {
+                if (MinecraftServer.b.containsKey(this.e.aq)) {
+                    a.info(this.e.aq + " failed to iron!");
+                    this.b((Packet) (new Packet3Chat("\u00A7cYou can\'t /iron again so soon!")));
+                } else {
+                    MinecraftServer.b.put(this.e.aq, Integer.valueOf(6000));
+                    a.info(this.e.aq + " ironed!");
+
+                    for (i = 0; i < 4; ++i) {
+                        this.e.a(new ItemStack(Item.IRON_INGOT, 1));
+                    }
+                }
+            } else if (s.toLowerCase().equalsIgnoreCase("/wood")) {
+                if (MinecraftServer.b.containsKey(this.e.aq)) {
+                    a.info(this.e.aq + " failed to wood!");
+                    this.b((Packet) (new Packet3Chat("\u00A7cYou can\'t /wood again so soon!")));
+                } else {
+                    MinecraftServer.b.put(this.e.aq, Integer.valueOf(6000));
+                    a.info(this.e.aq + " wooded!");
+
+                    for (i = 0; i < 4; ++i) {
+                        this.e.a(new ItemStack(Block.SAPLING, 1));
+                    }
+                }
             } else {
-                String s2;
+                String s1;
 
                 if (this.d.f.g(this.e.aq)) {
-                    s2 = s.substring(1);
-                    a.info(this.e.aq + " issued server command: " + s2);
-                    this.d.a(s2, (ICommandListener) this);
+                    s1 = s.substring(1);
+                    a.info(this.e.aq + " issued server command: " + s1);
+                    this.d.a(s1, (ICommandListener) this);
                 } else {
-                    s2 = s.substring(1);
-                    a.info(this.e.aq + " tried command: " + s2);
+                    s1 = s.substring(1);
+                    a.info(this.e.aq + " tried command: " + s1);
                 }
             }
         }
@@ -386,11 +384,11 @@ public class NetServerHandler extends NetHandler implements ICommandListener {
     }
 
     public void a(Packet59ComplexEntity packet59complexentity) {
-        TileEntity localas = this.d.e.k(packet59complexentity.a, packet59complexentity.b, packet59complexentity.c);
+        TileEntity tileentity = this.d.e.k(packet59complexentity.a, packet59complexentity.b, packet59complexentity.c);
 
-        if (localas != null) {
-            localas.a(packet59complexentity.e);
-            localas.c();
+        if (tileentity != null) {
+            tileentity.a(packet59complexentity.e);
+            tileentity.c();
         }
     }
 }
