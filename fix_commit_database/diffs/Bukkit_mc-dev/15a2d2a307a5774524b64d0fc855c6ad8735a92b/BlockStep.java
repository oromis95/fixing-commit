@@ -1,5 +1,6 @@
 package net.minecraft.server;
 
+import java.util.ArrayList;
 import java.util.Random;
 
 public class BlockStep extends Block {
@@ -13,14 +14,43 @@ public class BlockStep extends Block {
         if (!flag) {
             this.a(0.0F, 0.0F, 0.0F, 1.0F, 0.5F, 1.0F);
         } else {
-            o[i] = true;
+            n[i] = true;
         }
 
-        this.g(255);
+        this.f(255);
+    }
+
+    public void updateShape(IBlockAccess iblockaccess, int i, int j, int k) {
+        if (this.b) {
+            this.a(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
+        } else {
+            boolean flag = (iblockaccess.getData(i, j, k) & 8) != 0;
+
+            if (flag) {
+                this.a(0.0F, 0.5F, 0.0F, 1.0F, 1.0F, 1.0F);
+            } else {
+                this.a(0.0F, 0.0F, 0.0F, 1.0F, 0.5F, 1.0F);
+            }
+        }
+    }
+
+    public void f() {
+        if (this.b) {
+            this.a(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
+        } else {
+            this.a(0.0F, 0.0F, 0.0F, 1.0F, 0.5F, 1.0F);
+        }
+    }
+
+    public void a(World world, int i, int j, int k, AxisAlignedBB axisalignedbb, ArrayList arraylist) {
+        this.updateShape(world, i, j, k);
+        super.a(world, i, j, k, axisalignedbb, arraylist);
     }
 
     public int a(int i, int j) {
-        return j == 0 ? (i <= 1 ? 6 : 5) : (j == 1 ? (i == 0 ? 208 : (i == 1 ? 176 : 192)) : (j == 2 ? 4 : (j == 3 ? 16 : (j == 4 ? Block.BRICK.textureId : (j == 5 ? Block.SMOOTH_BRICK.textureId : 6)))));
+        int k = j & 7;
+
+        return k == 0 ? (i <= 1 ? 6 : 5) : (k == 1 ? (i == 0 ? 208 : (i == 1 ? 176 : 192)) : (k == 2 ? 4 : (k == 3 ? 16 : (k == 4 ? Block.BRICK.textureId : (k == 5 ? Block.SMOOTH_BRICK.textureId : 6)))));
     }
 
     public int a(int i) {
@@ -31,7 +61,13 @@ public class BlockStep extends Block {
         return this.b;
     }
 
-    public void onPlace(World world, int i, int j, int k) {}
+    public void postPlace(World world, int i, int j, int k, int l) {
+        if (l == 0 && !this.b) {
+            int i1 = world.getData(i, j, k) & 7;
+
+            world.setData(i, j, k, i1 | 8);
+        }
+    }
 
     public int getDropType(int i, Random random, int j) {
         return Block.STEP.id;
@@ -42,7 +78,7 @@ public class BlockStep extends Block {
     }
 
     protected int getDropData(int i) {
-        return i;
+        return i & 7;
     }
 
     public boolean b() {
@@ -50,6 +86,6 @@ public class BlockStep extends Block {
     }
 
     protected ItemStack a_(int i) {
-        return new ItemStack(Block.STEP.id, 1, i);
+        return new ItemStack(Block.STEP.id, 1, i & 7);
     }
 }
