@@ -8,17 +8,16 @@ public class WorldProviderHell extends WorldProvider {
         this.c = new WorldChunkManagerHell(BiomeBase.HELL, 1.0F, 0.0F);
         this.d = true;
         this.e = true;
-        this.f = true;
         this.dimension = -1;
     }
 
-    protected void f() {
+    protected void g() {
         float f = 0.1F;
 
         for (int i = 0; i <= 15; ++i) {
             float f1 = 1.0F - (float) i / 15.0F;
 
-            this.g[i] = (1.0F - f1) / (f1 * 3.0F + 1.0F) * (1.0F - f) + f;
+            this.f[i] = (1.0F - f1) / (f1 * 3.0F + 1.0F) * (1.0F - f) + f;
         }
     }
 
@@ -26,6 +25,10 @@ public class WorldProviderHell extends WorldProvider {
         return new ChunkProviderHell(this.a, this.a.getSeed());
     }
 
+    public boolean d() {
+        return false;
+    }
+
     public boolean canSpawn(int i, int j) {
         return false;
     }
