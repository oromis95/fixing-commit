@@ -72,8 +72,8 @@ public class EntityFishingHook extends Entity {
         this.i = 0;
     }
 
-    public void y_() {
-        super.y_();
+    public void G_() {
+        super.G_();
         if (this.l > 0) {
             double d0 = this.locX + (this.m - this.locX) / (double) this.l;
             double d1 = this.locY + (this.n - this.locY) / (double) this.l;
@@ -96,9 +96,9 @@ public class EntityFishingHook extends Entity {
             this.c(this.yaw, this.pitch);
         } else {
             if (!this.world.isStatic) {
-                ItemStack itemstack = this.owner.Q();
+                ItemStack itemstack = this.owner.T();
 
-                if (this.owner.dead || !this.owner.isAlive() || itemstack == null || itemstack.getItem() != Item.FISHING_ROD || this.i(this.owner) > 1024.0D) {
+                if (this.owner.dead || !this.owner.isAlive() || itemstack == null || itemstack.getItem() != Item.FISHING_ROD || this.j(this.owner) > 1024.0D) {
                     this.die();
                     this.owner.hookedFish = null;
                     return;
@@ -161,7 +161,7 @@ public class EntityFishingHook extends Entity {
             for (int j = 0; j < list.size(); ++j) {
                 Entity entity1 = (Entity) list.get(j);
 
-                if (entity1.e_() && (entity1 != this.owner || this.j >= 5)) {
+                if (entity1.o_() && (entity1 != this.owner || this.j >= 5)) {
                     float f = 0.3F;
                     AxisAlignedBB axisalignedbb = entity1.boundingBox.grow((double) f, (double) f, (double) f);
                     MovingObjectPosition movingobjectposition1 = axisalignedbb.a(vec3d, vec3d1);
@@ -239,7 +239,7 @@ public class EntityFishingHook extends Entity {
                     } else {
                         short short1 = 500;
 
-                        if (this.world.v(MathHelper.floor(this.locX), MathHelper.floor(this.locY) + 1, MathHelper.floor(this.locZ))) {
+                        if (this.world.y(MathHelper.floor(this.locX), MathHelper.floor(this.locY) + 1, MathHelper.floor(this.locZ))) {
                             short1 = 300;
                         }
 
@@ -305,7 +305,7 @@ public class EntityFishingHook extends Entity {
         this.h = nbttagcompound.getByte("inGround") == 1;
     }
 
-    public int j() {
+    public int k() {
         byte b0 = 0;
 
         if (this.hooked != null) {
