@@ -4,8 +4,8 @@ public class BiomePlains extends BiomeBase {
 
     protected BiomePlains(int i) {
         super(i);
-        this.G.z = -999;
-        this.G.A = 4;
-        this.G.B = 10;
+        this.I.z = -999;
+        this.I.A = 4;
+        this.I.B = 10;
     }
 }
