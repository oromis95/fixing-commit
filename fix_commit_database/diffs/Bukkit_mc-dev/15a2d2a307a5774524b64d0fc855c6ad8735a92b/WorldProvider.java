@@ -7,10 +7,9 @@ public abstract class WorldProvider {
     public WorldChunkManager c;
     public boolean d = false;
     public boolean e = false;
-    public boolean f = false;
-    public float[] g = new float[16];
+    public float[] f = new float[16];
     public int dimension = 0;
-    private float[] i = new float[4];
+    private float[] h = new float[4];
 
     public WorldProvider() {}
 
@@ -18,16 +17,16 @@ public abstract class WorldProvider {
         this.a = world;
         this.type = world.getWorldData().getType();
         this.a();
-        this.f();
+        this.g();
     }
 
-    protected void f() {
+    protected void g() {
         float f = 0.0F;
 
         for (int i = 0; i <= 15; ++i) {
             float f1 = 1.0F - (float) i / 15.0F;
 
-            this.g[i] = (1.0F - f1) / (f1 * 3.0F + 1.0F) * (1.0F - f) + f;
+            this.f[i] = (1.0F - f1) / (f1 * 3.0F + 1.0F) * (1.0F - f) + f;
         }
     }
 
@@ -44,7 +43,7 @@ public abstract class WorldProvider {
     }
 
     public boolean canSpawn(int i, int j) {
-        int k = this.a.a(i, j);
+        int k = this.a.b(i, j);
 
         return k == Block.GRASS.id;
     }
@@ -68,6 +67,10 @@ public abstract class WorldProvider {
         return f1;
     }
 
+    public boolean d() {
+        return true;
+    }
+
     public boolean c() {
         return true;
     }
@@ -76,11 +79,11 @@ public abstract class WorldProvider {
         return (WorldProvider) (i == -1 ? new WorldProviderHell() : (i == 0 ? new WorldProviderNormal() : (i == 1 ? new WorldProviderTheEnd() : null)));
     }
 
-    public ChunkCoordinates d() {
+    public ChunkCoordinates e() {
         return null;
     }
 
     public int getSeaLevel() {
-        return this.type == WorldType.FLAT ? 4 : this.a.height / 2;
+        return this.type == WorldType.FLAT ? 4 : 64;
     }
 }
