@@ -6,6 +6,19 @@ public class EntityCow extends EntityAnimal {
         super(world);
         this.texture = "/mob/cow.png";
         this.b(0.9F, 1.3F);
+        this.ak().a(true);
+        this.goalSelector.a(0, new PathfinderGoalFloat(this));
+        this.goalSelector.a(1, new PathfinderGoalPanic(this, 0.38F));
+        this.goalSelector.a(2, new PathfinderGoalBreed(this, 0.2F));
+        this.goalSelector.a(3, new PathfinderGoalTempt(this, 0.25F, Item.WHEAT.id, false));
+        this.goalSelector.a(4, new PathfinderGoalFollowParent(this, 0.25F));
+        this.goalSelector.a(5, new PathfinderGoalRandomStroll(this, 0.2F));
+        this.goalSelector.a(6, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 6.0F));
+        this.goalSelector.a(7, new PathfinderGoalRandomLookaround(this));
+    }
+
+    public boolean c_() {
+        return true;
     }
 
     public int getMaxHealth() {
@@ -20,19 +33,19 @@ public class EntityCow extends EntityAnimal {
         super.a(nbttagcompound);
     }
 
-    protected String c_() {
+    protected String i() {
         return "mob.cow";
     }
 
-    protected String m() {
+    protected String j() {
         return "mob.cowhurt";
     }
 
-    protected String n() {
+    protected String k() {
         return "mob.cowhurt";
     }
 
-    protected float o() {
+    protected float p() {
         return 0.4F;
     }
 
@@ -71,7 +84,7 @@ public class EntityCow extends EntityAnimal {
         }
     }
 
-    protected EntityAnimal createChild(EntityAnimal entityanimal) {
+    public EntityAnimal createChild(EntityAnimal entityanimal) {
         return new EntityCow(this.world);
     }
 }
