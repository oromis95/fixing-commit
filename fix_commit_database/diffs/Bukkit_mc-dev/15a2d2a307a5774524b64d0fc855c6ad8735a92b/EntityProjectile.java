@@ -77,11 +77,11 @@ public abstract class EntityProjectile extends Entity {
         this.h = 0;
     }
 
-    public void y_() {
+    public void G_() {
         this.bL = this.locX;
         this.bM = this.locY;
         this.bN = this.locZ;
-        super.y_();
+        super.G_();
         if (this.shake > 0) {
             --this.shake;
         }
@@ -126,7 +126,7 @@ public abstract class EntityProjectile extends Entity {
             for (int j = 0; j < list.size(); ++j) {
                 Entity entity1 = (Entity) list.get(j);
 
-                if (entity1.e_() && (entity1 != this.shooter || this.i >= 5)) {
+                if (entity1.o_() && (entity1 != this.shooter || this.i >= 5)) {
                     float f = 0.3F;
                     AxisAlignedBB axisalignedbb = entity1.boundingBox.grow((double) f, (double) f, (double) f);
                     MovingObjectPosition movingobjectposition1 = axisalignedbb.a(vec3d, vec3d1);
@@ -179,7 +179,7 @@ public abstract class EntityProjectile extends Entity {
         float f2 = 0.99F;
         float f3 = this.e();
 
-        if (this.aK()) {
+        if (this.aT()) {
             for (int k = 0; k < 4; ++k) {
                 float f4 = 0.25F;
 
