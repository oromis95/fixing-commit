@@ -89,8 +89,8 @@ public class WorldGenCaves extends WorldGenBase {
                         j2 = 1;
                     }
 
-                    if (k2 > this.d.height - 8) {
-                        k2 = this.d.height - 8;
+                    if (k2 > 120) {
+                        k2 = 120;
                     }
 
                     if (l2 < 0) {
@@ -109,8 +109,8 @@ public class WorldGenCaves extends WorldGenBase {
                     for (j3 = l1; !flag2 && j3 < i2; ++j3) {
                         for (int l3 = l2; !flag2 && l3 < i3; ++l3) {
                             for (int i4 = k2 + 1; !flag2 && i4 >= j2 - 1; --i4) {
-                                k3 = (j3 * 16 + l3) * this.d.height + i4;
-                                if (i4 >= 0 && i4 < this.d.height) {
+                                k3 = (j3 * 16 + l3) * 128 + i4;
+                                if (i4 >= 0 && i4 < 128) {
                                     if (abyte[k3] == Block.WATER.id || abyte[k3] == Block.STATIONARY_WATER.id) {
                                         flag2 = true;
                                     }
@@ -129,7 +129,7 @@ public class WorldGenCaves extends WorldGenBase {
 
                             for (k3 = l2; k3 < i3; ++k3) {
                                 double d13 = ((double) (k3 + k * 16) + 0.5D - d2) / d6;
-                                int j4 = (j3 * 16 + k3) * this.d.height + k2;
+                                int j4 = (j3 * 16 + k3) * 128 + k2;
                                 boolean flag3 = false;
 
                                 if (d12 * d12 + d13 * d13 < 1.0D) {
@@ -149,7 +149,7 @@ public class WorldGenCaves extends WorldGenBase {
                                                 } else {
                                                     abyte[j4] = 0;
                                                     if (flag3 && abyte[j4 - 1] == Block.DIRT.id) {
-                                                        abyte[j4 - 1] = this.d.getWorldChunkManager().getBiome(j3 + j * 16, k3 + k * 16).y;
+                                                        abyte[j4 - 1] = this.d.getBiome(j3 + j * 16, k3 + k * 16).A;
                                                     }
                                                 }
                                             }
@@ -179,7 +179,7 @@ public class WorldGenCaves extends WorldGenBase {
 
         for (int j1 = 0; j1 < i1; ++j1) {
             double d0 = (double) (i * 16 + this.c.nextInt(16));
-            double d1 = (double) this.c.nextInt(this.c.nextInt(world.height - 8) + 8);
+            double d1 = (double) this.c.nextInt(this.c.nextInt(120) + 8);
             double d2 = (double) (j * 16 + this.c.nextInt(16));
             int k1 = 1;
 
