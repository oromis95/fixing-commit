@@ -4,8 +4,8 @@ import java.util.Random;
 
 public class BlockFlower extends Block {
 
-    protected BlockFlower(int i, int j) {
-        super(i, Material.PLANT);
+    protected BlockFlower(int i, int j, Material material) {
+        super(i, material);
         this.textureId = j;
         this.a(true);
         float f = 0.2F;
@@ -13,6 +13,10 @@ public class BlockFlower extends Block {
         this.a(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, f * 3.0F, 0.5F + f);
     }
 
+    protected BlockFlower(int i, int j) {
+        this(i, j, Material.PLANT);
+    }
+
     public boolean canPlace(World world, int i, int j, int k) {
         return super.canPlace(world, i, j, k) && this.d(world.getTypeId(i, j - 1, k));
     }
@@ -38,7 +42,7 @@ public class BlockFlower extends Block {
     }
 
     public boolean f(World world, int i, int j, int k) {
-        return (world.k(i, j, k) >= 8 || world.isChunkLoaded(i, j, k)) && this.d(world.getTypeId(i, j - 1, k));
+        return (world.m(i, j, k) >= 8 || world.isChunkLoaded(i, j, k)) && this.d(world.getTypeId(i, j - 1, k));
     }
 
     public AxisAlignedBB e(World world, int i, int j, int k) {
