@@ -17,9 +17,9 @@ public class EntityTNTPrimed extends Entity {
         this.setPosition(d0, d1, d2);
         float f = (float) (Math.random() * 3.1415927410125732D * 2.0D);
 
-        this.motX = (double) (-MathHelper.sin(f * 3.1415927F / 180.0F) * 0.02F);
+        this.motX = (double) (-((float) Math.sin((double) f)) * 0.02F);
         this.motY = 0.20000000298023224D;
-        this.motZ = (double) (-MathHelper.cos(f * 3.1415927F / 180.0F) * 0.02F);
+        this.motZ = (double) (-((float) Math.cos((double) f)) * 0.02F);
         this.fuseTicks = 80;
         this.lastX = d0;
         this.lastY = d1;
@@ -32,11 +32,11 @@ public class EntityTNTPrimed extends Entity {
         return false;
     }
 
-    public boolean e_() {
+    public boolean o_() {
         return !this.dead;
     }
 
-    public void y_() {
+    public void G_() {
         this.lastX = this.locX;
         this.lastY = this.locY;
         this.lastZ = this.locZ;
