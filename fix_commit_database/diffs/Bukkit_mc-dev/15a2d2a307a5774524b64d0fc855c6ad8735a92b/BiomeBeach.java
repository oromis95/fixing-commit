@@ -4,12 +4,12 @@ public class BiomeBeach extends BiomeBase {
 
     public BiomeBeach(int i) {
         super(i);
-        this.I.clear();
-        this.y = (byte) Block.SAND.id;
-        this.z = (byte) Block.SAND.id;
-        this.G.z = -999;
-        this.G.C = 0;
-        this.G.E = 0;
-        this.G.F = 0;
+        this.K.clear();
+        this.A = (byte) Block.SAND.id;
+        this.B = (byte) Block.SAND.id;
+        this.I.z = -999;
+        this.I.C = 0;
+        this.I.E = 0;
+        this.I.F = 0;
     }
 }
