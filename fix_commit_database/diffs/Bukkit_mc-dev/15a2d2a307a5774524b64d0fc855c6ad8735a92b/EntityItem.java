@@ -3,10 +3,9 @@ package net.minecraft.server;
 public class EntityItem extends Entity {
 
     public ItemStack itemStack;
-    private int e;
     public int age = 0;
     public int pickupDelay;
-    private int f = 5;
+    private int e = 5;
     public float d = (float) (Math.random() * 3.141592653589793D * 2.0D);
 
     public EntityItem(World world, double d0, double d1, double d2, ItemStack itemstack) {
@@ -33,8 +32,8 @@ public class EntityItem extends Entity {
 
     protected void b() {}
 
-    public void y_() {
-        super.y_();
+    public void G_() {
+        super.G_();
         if (this.pickupDelay > 0) {
             --this.pickupDelay;
         }
@@ -70,14 +69,17 @@ public class EntityItem extends Entity {
             this.motY *= -0.5D;
         }
 
-        ++this.e;
         ++this.age;
         if (this.age >= 6000) {
             this.die();
         }
     }
 
-    public boolean i_() {
+    public void k() {
+        this.age = 4800;
+    }
+
+    public boolean h_() {
         return this.world.a(this.boundingBox, Material.WATER, this);
     }
 
@@ -86,9 +88,9 @@ public class EntityItem extends Entity {
     }
 
     public boolean damageEntity(DamageSource damagesource, int i) {
-        this.aM();
-        this.f -= i;
-        if (this.f <= 0) {
+        this.aV();
+        this.e -= i;
+        if (this.e <= 0) {
             this.die();
         }
 
@@ -96,13 +98,13 @@ public class EntityItem extends Entity {
     }
 
     public void b(NBTTagCompound nbttagcompound) {
-        nbttagcompound.setShort("Health", (short) ((byte) this.f));
+        nbttagcompound.setShort("Health", (short) ((byte) this.e));
         nbttagcompound.setShort("Age", (short) this.age);
         nbttagcompound.setCompound("Item", this.itemStack.save(new NBTTagCompound()));
     }
 
     public void a(NBTTagCompound nbttagcompound) {
-        this.f = nbttagcompound.getShort("Health") & 255;
+        this.e = nbttagcompound.getShort("Health") & 255;
         this.age = nbttagcompound.getShort("Age");
         NBTTagCompound nbttagcompound1 = nbttagcompound.getCompound("Item");
 
@@ -145,4 +147,8 @@ public class EntityItem extends Entity {
     public String getLocalizedName() {
         return LocaleI18n.get("item." + this.itemStack.k());
     }
+
+    public boolean k_() {
+        return false;
+    }
 }
