@@ -85,6 +85,10 @@ public class NBTTagCompound extends NBTBase {
         this.map.put(s, new NBTTagByteArray(s, abyte));
     }
 
+    public void setIntArray(String s, int[] aint) {
+        this.map.put(s, new NBTTagIntArray(s, aint));
+    }
+
     public void setCompound(String s, NBTTagCompound nbttagcompound) {
         this.map.put(s, nbttagcompound.setName(s));
     }
@@ -133,6 +137,10 @@ public class NBTTagCompound extends NBTBase {
         return !this.map.containsKey(s) ? new byte[0] : ((NBTTagByteArray) this.map.get(s)).data;
     }
 
+    public int[] getIntArray(String s) {
+        return !this.map.containsKey(s) ? new int[0] : ((NBTTagIntArray) this.map.get(s)).data;
+    }
+
     public NBTTagCompound getCompound(String s) {
         return !this.map.containsKey(s) ? new NBTTagCompound(s) : (NBTTagCompound) this.map.get(s);
     }
