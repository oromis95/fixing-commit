@@ -13,7 +13,7 @@ public class WorldGenTaiga1 extends WorldGenerator {
         int k1 = 1 + random.nextInt(j1 + 1);
         boolean flag = true;
 
-        if (j >= 1 && j + l + 1 <= world.height) {
+        if (j >= 1 && j + l + 1 <= 128) {
             int l1;
             int i2;
             int j2;
@@ -31,7 +31,7 @@ public class WorldGenTaiga1 extends WorldGenerator {
 
                 for (i2 = i - l2; i2 <= i + l2 && flag; ++i2) {
                     for (j2 = k - l2; j2 <= k + l2 && flag; ++j2) {
-                        if (l1 >= 0 && l1 < world.height) {
+                        if (l1 >= 0 && l1 < 128) {
                             k2 = world.getTypeId(i2, l1, j2);
                             if (k2 != 0 && k2 != Block.LEAVES.id) {
                                 flag = false;
@@ -47,7 +47,7 @@ public class WorldGenTaiga1 extends WorldGenerator {
                 return false;
             } else {
                 l1 = world.getTypeId(i, j - 1, k);
-                if ((l1 == Block.GRASS.id || l1 == Block.DIRT.id) && j < world.height - l - 1) {
+                if ((l1 == Block.GRASS.id || l1 == Block.DIRT.id) && j < 128 - l - 1) {
                     world.setRawTypeId(i, j - 1, k, Block.DIRT.id);
                     l2 = 0;
 
@@ -58,7 +58,7 @@ public class WorldGenTaiga1 extends WorldGenerator {
                             for (int i3 = k - l2; i3 <= k + l2; ++i3) {
                                 int j3 = i3 - k;
 
-                                if ((Math.abs(k2) != l2 || Math.abs(j3) != l2 || l2 <= 0) && !Block.o[world.getTypeId(j2, i2, i3)]) {
+                                if ((Math.abs(k2) != l2 || Math.abs(j3) != l2 || l2 <= 0) && !Block.n[world.getTypeId(j2, i2, i3)]) {
                                     world.setRawTypeIdAndData(j2, i2, i3, Block.LEAVES.id, 1);
                                 }
                             }
