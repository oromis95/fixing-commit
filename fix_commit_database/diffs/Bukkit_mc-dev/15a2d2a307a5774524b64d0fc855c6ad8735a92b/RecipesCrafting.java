@@ -12,5 +12,6 @@ public class RecipesCrafting {
         craftingmanager.registerShapedRecipe(new ItemStack(Block.SMOOTH_BRICK, 4), new Object[] { "##", "##", Character.valueOf('#'), Block.STONE});
         craftingmanager.registerShapedRecipe(new ItemStack(Block.IRON_FENCE, 16), new Object[] { "###", "###", Character.valueOf('#'), Item.IRON_INGOT});
         craftingmanager.registerShapedRecipe(new ItemStack(Block.THIN_GLASS, 16), new Object[] { "###", "###", Character.valueOf('#'), Block.GLASS});
+        craftingmanager.registerShapedRecipe(new ItemStack(Block.REDSTONE_LAMP_OFF, 1), new Object[] { " R ", "RGR", " R ", Character.valueOf('R'), Item.REDSTONE, Character.valueOf('G'), Block.GLOWSTONE});
     }
 }
