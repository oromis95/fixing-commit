@@ -25,21 +25,21 @@ public class RemoteControlSession extends RemoteConnectionThread {
         while (true) {
             try {
                 if (!this.running) {
-                    return;
+                    break;
                 }
 
                 try {
                     BufferedInputStream bufferedinputstream = new BufferedInputStream(this.h.getInputStream());
                     int i = bufferedinputstream.read(this.i, 0, 1460);
 
-                    if (10 <= i) {
-                        byte b0 = 0;
-                        int j = StatusChallengeUtils.b(this.i, 0, i);
+                    if (10 > i) {
+                        return;
+                    }
 
-                        if (j != i - 4) {
-                            return;
-                        }
+                    byte b0 = 0;
+                    int j = StatusChallengeUtils.b(this.i, 0, i);
 
+                    if (j == i - 4) {
                         int k = b0 + 4;
                         int l = StatusChallengeUtils.b(this.i, k, i);
 
@@ -92,7 +92,7 @@ public class RemoteControlSession extends RemoteConnectionThread {
                 }
             } catch (Exception exception1) {
                 System.out.println(exception1);
-                return;
+                break;
             } finally {
                 this.f();
             }
