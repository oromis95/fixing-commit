@@ -4,20 +4,20 @@ public class ItemFood extends Item {
 
     public final int a;
     private final int b;
-    private final float bS;
-    private final boolean bT;
-    private boolean bU;
-    private int bV;
-    private int bW;
+    private final float bU;
+    private final boolean bV;
+    private boolean bW;
     private int bX;
-    private float bY;
+    private int bY;
+    private int bZ;
+    private float ca;
 
     public ItemFood(int i, int j, float f, boolean flag) {
         super(i);
         this.a = 32;
         this.b = j;
-        this.bT = flag;
-        this.bS = f;
+        this.bV = flag;
+        this.bU = f;
     }
 
     public ItemFood(int i, int j, boolean flag) {
@@ -28,8 +28,8 @@ public class ItemFood extends Item {
         --itemstack.count;
         entityhuman.getFoodData().a(this);
         world.makeSound(entityhuman, "random.burp", 0.5F, world.random.nextFloat() * 0.1F + 0.9F);
-        if (!world.isStatic && this.bV > 0 && world.random.nextFloat() < this.bY) {
-            entityhuman.addEffect(new MobEffect(this.bV, this.bW * 20, this.bX));
+        if (!world.isStatic && this.bX > 0 && world.random.nextFloat() < this.ca) {
+            entityhuman.addEffect(new MobEffect(this.bX, this.bY * 20, this.bZ));
         }
 
         return itemstack;
@@ -44,7 +44,7 @@ public class ItemFood extends Item {
     }
 
     public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
-        if (entityhuman.b(this.bU)) {
+        if (entityhuman.b(this.bW)) {
             entityhuman.a(itemstack, this.c(itemstack));
         }
 
@@ -56,23 +56,23 @@ public class ItemFood extends Item {
     }
 
     public float getSaturationModifier() {
-        return this.bS;
+        return this.bU;
     }
 
     public boolean q() {
-        return this.bT;
+        return this.bV;
     }
 
     public ItemFood a(int i, int j, int k, float f) {
-        this.bV = i;
-        this.bW = j;
-        this.bX = k;
-        this.bY = f;
+        this.bX = i;
+        this.bY = j;
+        this.bZ = k;
+        this.ca = f;
         return this;
     }
 
     public ItemFood r() {
-        this.bU = true;
+        this.bW = true;
         return this;
     }
 
