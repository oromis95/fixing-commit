@@ -46,7 +46,7 @@ public class ContainerWorkbench extends Container {
         super.a(entityhuman);
         if (!this.c.isStatic) {
             for (int i = 0; i < 9; ++i) {
-                ItemStack itemstack = this.craftInventory.getItem(i);
+                ItemStack itemstack = this.craftInventory.splitWithoutUpdate(i);
 
                 if (itemstack != null) {
                     entityhuman.drop(itemstack);
@@ -71,6 +71,8 @@ public class ContainerWorkbench extends Container {
                 if (!this.a(itemstack1, 10, 46, true)) {
                     return null;
                 }
+
+                slot.a(itemstack1, itemstack);
             } else if (i >= 10 && i < 37) {
                 if (!this.a(itemstack1, 37, 46, false)) {
                     return null;
@@ -93,7 +95,7 @@ public class ContainerWorkbench extends Container {
                 return null;
             }
 
-            slot.b(itemstack1);
+            slot.c(itemstack1);
         }
 
         return itemstack;
