@@ -10,17 +10,17 @@ public class ItemFishingRod extends Item {
 
     public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
         if (entityhuman.hookedFish != null) {
-            int i = entityhuman.hookedFish.j();
+            int i = entityhuman.hookedFish.k();
 
             itemstack.damage(i, entityhuman);
-            entityhuman.s_();
+            entityhuman.D();
         } else {
             world.makeSound(entityhuman, "random.bow", 0.5F, 0.4F / (c.nextFloat() * 0.4F + 0.8F));
             if (!world.isStatic) {
                 world.addEntity(new EntityFishingHook(world, entityhuman));
             }
 
-            entityhuman.s_();
+            entityhuman.D();
         }
 
         return itemstack;
