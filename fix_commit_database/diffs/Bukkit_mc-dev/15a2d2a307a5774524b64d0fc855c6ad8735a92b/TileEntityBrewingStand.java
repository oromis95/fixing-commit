@@ -12,14 +12,14 @@ public class TileEntityBrewingStand extends TileEntity implements IInventory {
     public TileEntityBrewingStand() {}
 
     public String getName() {
-        return "Brewing Stand";
+        return "container.brewing";
     }
 
     public int getSize() {
         return this.items.length;
     }
 
-    public void l_() {
+    public void q_() {
         if (this.brewTime > 0) {
             --this.brewTime;
             if (this.brewTime == 0) {
@@ -44,10 +44,10 @@ public class TileEntityBrewingStand extends TileEntity implements IInventory {
             this.world.setData(this.x, this.y, this.z, i);
         }
 
-        super.l_();
+        super.q_();
     }
 
-    public int h() {
+    public int i() {
         return this.brewTime;
     }
 
@@ -174,6 +174,17 @@ public class TileEntityBrewingStand extends TileEntity implements IInventory {
         }
     }
 
+    public ItemStack splitWithoutUpdate(int i) {
+        if (i >= 0 && i < this.items.length) {
+            ItemStack itemstack = this.items[i];
+
+            this.items[i] = null;
+            return itemstack;
+        } else {
+            return null;
+        }
+    }
+
     public void setItem(int i, ItemStack itemstack) {
         if (i >= 0 && i < this.items.length) {
             this.items[i] = itemstack;
