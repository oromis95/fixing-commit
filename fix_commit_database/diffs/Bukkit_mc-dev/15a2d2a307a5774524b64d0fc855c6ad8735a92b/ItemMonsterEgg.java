@@ -4,7 +4,6 @@ public class ItemMonsterEgg extends Item {
 
     public ItemMonsterEgg(int i) {
         super(i);
-        this.e(1);
         this.a(true);
     }
 
@@ -12,21 +11,38 @@ public class ItemMonsterEgg extends Item {
         if (world.isStatic) {
             return true;
         } else {
+            int i1 = world.getTypeId(i, j, k);
+
             i += Facing.b[l];
             j += Facing.c[l];
             k += Facing.d[l];
-            Entity entity = EntityTypes.a(itemstack.getData(), world);
+            double d0 = 0.0D;
 
-            if (entity != null) {
-                if (!entityhuman.abilities.canInstantlyBuild) {
-                    --itemstack.count;
-                }
+            if (l == 1 && i1 == Block.FENCE.id || i1 == Block.NETHER_FENCE.id) {
+                d0 = 0.5D;
+            }
 
-                entity.setPositionRotation((double) i + 0.5D, (double) j, (double) k + 0.5D, 0.0F, 0.0F);
-                world.addEntity(entity);
+            if (a(world, itemstack.getData(), (double) i + 0.5D, (double) j + d0, (double) k + 0.5D) && !entityhuman.abilities.canInstantlyBuild) {
+                --itemstack.count;
             }
 
             return true;
         }
     }
+
+    public static boolean a(World world, int i, double d0, double d1, double d2) {
+        if (!EntityTypes.a.containsKey(Integer.valueOf(i))) {
+            return false;
+        } else {
+            Entity entity = EntityTypes.a(i, world);
+
+            if (entity != null) {
+                entity.setPositionRotation(d0, d1, d2, world.random.nextFloat() * 360.0F, 0.0F);
+                world.addEntity(entity);
+                ((EntityLiving) entity).ay();
+            }
+
+            return entity != null;
+        }
+    }
 }
