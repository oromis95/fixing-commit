@@ -9,18 +9,18 @@ public abstract class EntityMonster extends EntityCreature implements IMonster {
         this.aA = 5;
     }
 
-    public void d() {
-        float f = this.a(1.0F);
+    public void e() {
+        float f = this.b(1.0F);
 
         if (f > 0.5F) {
             this.aV += 2;
         }
 
-        super.d();
+        super.e();
     }
 
-    public void y_() {
-        super.y_();
+    public void G_() {
+        super.G_();
         if (!this.world.isStatic && this.world.difficulty == 0) {
             this.die();
         }
@@ -29,7 +29,7 @@ public abstract class EntityMonster extends EntityCreature implements IMonster {
     protected Entity findTarget() {
         EntityHuman entityhuman = this.world.findNearbyVulnerablePlayer(this, 16.0D);
 
-        return entityhuman != null && this.g(entityhuman) ? entityhuman : null;
+        return entityhuman != null && this.h(entityhuman) ? entityhuman : null;
     }
 
     public boolean damageEntity(DamageSource damagesource, int i) {
@@ -39,7 +39,6 @@ public abstract class EntityMonster extends EntityCreature implements IMonster {
             if (this.passenger != entity && this.vehicle != entity) {
                 if (entity != this) {
                     this.target = entity;
-                    this.lastDamager = entity instanceof EntityLiving ? (EntityLiving) entity : null;
                 }
 
                 return true;
@@ -51,7 +50,7 @@ public abstract class EntityMonster extends EntityCreature implements IMonster {
         }
     }
 
-    public boolean d(Entity entity) {
+    public boolean a(Entity entity) {
         int i = this.damage;
 
         if (this.hasEffect(MobEffectList.INCREASE_DAMAGE)) {
@@ -68,12 +67,12 @@ public abstract class EntityMonster extends EntityCreature implements IMonster {
     protected void a(Entity entity, float f) {
         if (this.attackTicks <= 0 && f < 2.0F && entity.boundingBox.e > this.boundingBox.b && entity.boundingBox.b < this.boundingBox.e) {
             this.attackTicks = 20;
-            this.d(entity);
+            this.a(entity);
         }
     }
 
     public float a(int i, int j, int k) {
-        return 0.5F - this.world.m(i, j, k);
+        return 0.5F - this.world.p(i, j, k);
     }
 
     public void b(NBTTagCompound nbttagcompound) {
@@ -84,7 +83,7 @@ public abstract class EntityMonster extends EntityCreature implements IMonster {
         super.a(nbttagcompound);
     }
 
-    protected boolean z() {
+    protected boolean D() {
         int i = MathHelper.floor(this.locX);
         int j = MathHelper.floor(this.boundingBox.b);
         int k = MathHelper.floor(this.locZ);
@@ -94,12 +93,12 @@ public abstract class EntityMonster extends EntityCreature implements IMonster {
         } else {
             int l = this.world.getLightLevel(i, j, k);
 
-            if (this.world.v()) {
-                int i1 = this.world.k;
+            if (this.world.w()) {
+                int i1 = this.world.f;
 
-                this.world.k = 10;
+                this.world.f = 10;
                 l = this.world.getLightLevel(i, j, k);
-                this.world.k = i1;
+                this.world.f = i1;
             }
 
             return l <= this.random.nextInt(8);
@@ -107,6 +106,6 @@ public abstract class EntityMonster extends EntityCreature implements IMonster {
     }
 
     public boolean canSpawn() {
-        return this.z() && super.canSpawn();
+        return this.D() && super.canSpawn();
     }
 }
