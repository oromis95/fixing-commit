@@ -32,8 +32,8 @@ public class EntityExperienceOrb extends Entity {
 
     protected void b() {}
 
-    public void y_() {
-        super.y_();
+    public void G_() {
+        super.G_();
         if (this.c > 0) {
             --this.c;
         }
@@ -94,7 +94,7 @@ public class EntityExperienceOrb extends Entity {
         }
     }
 
-    public boolean i_() {
+    public boolean h_() {
         return this.world.a(this.boundingBox, Material.WATER, this);
     }
 
@@ -103,7 +103,7 @@ public class EntityExperienceOrb extends Entity {
     }
 
     public boolean damageEntity(DamageSource damagesource, int i) {
-        this.aM();
+        this.aV();
         this.d -= i;
         if (this.d <= 0) {
             this.die();
@@ -136,11 +136,15 @@ public class EntityExperienceOrb extends Entity {
         }
     }
 
-    public int g() {
+    public int y_() {
         return this.value;
     }
 
     public static int getOrbValue(int i) {
         return i >= 2477 ? 2477 : (i >= 1237 ? 1237 : (i >= 617 ? 617 : (i >= 307 ? 307 : (i >= 149 ? 149 : (i >= 73 ? 73 : (i >= 37 ? 37 : (i >= 17 ? 17 : (i >= 7 ? 7 : (i >= 3 ? 3 : 1)))))))));
     }
+
+    public boolean k_() {
+        return false;
+    }
 }
