@@ -14,33 +14,31 @@ public class Block {
     public static final StepSound j = new StepSoundStone("stone", 1.0F, 1.0F);
     public static final StepSound k = new StepSound("cloth", 1.0F, 1.0F);
     public static final StepSound l = new StepSoundSand("sand", 1.0F, 1.0F);
-    public static final Block[] byId = new Block[256];
-    public static final boolean[] n = new boolean[256];
-    public static final boolean[] o = new boolean[256];
-    public static final boolean[] isTileEntity = new boolean[256];
-    public static final int[] lightBlock = new int[256];
-    public static final boolean[] r = new boolean[256];
-    public static final int[] lightEmission = new int[256];
-    public static final boolean[] t = new boolean[256];
-    public static boolean[] u = new boolean[256];
+    public static final Block[] byId = new Block[4096];
+    public static final boolean[] n = new boolean[4096];
+    public static final int[] lightBlock = new int[4096];
+    public static final boolean[] p = new boolean[4096];
+    public static final int[] lightEmission = new int[4096];
+    public static final boolean[] r = new boolean[4096];
+    public static boolean[] s = new boolean[4096];
     public static final Block STONE = (new BlockStone(1, 1)).c(1.5F).b(10.0F).a(h).a("stone");
     public static final BlockGrass GRASS = (BlockGrass) (new BlockGrass(2)).c(0.6F).a(g).a("grass");
     public static final Block DIRT = (new BlockDirt(3, 2)).c(0.5F).a(f).a("dirt");
     public static final Block COBBLESTONE = (new Block(4, 16, Material.STONE)).c(2.0F).b(10.0F).a(h).a("stonebrick");
     public static final Block WOOD = (new Block(5, 4, Material.WOOD)).c(2.0F).b(5.0F).a(e).a("wood").i();
     public static final Block SAPLING = (new BlockSapling(6, 15)).c(0.0F).a(g).a("sapling").i();
-    public static final Block BEDROCK = (new Block(7, 17, Material.STONE)).k().b(6000000.0F).a(h).a("bedrock").p();
-    public static final Block WATER = (new BlockFlowing(8, Material.WATER)).c(100.0F).g(3).a("water").p().i();
-    public static final Block STATIONARY_WATER = (new BlockStationary(9, Material.WATER)).c(100.0F).g(3).a("water").p().i();
-    public static final Block LAVA = (new BlockFlowing(10, Material.LAVA)).c(0.0F).a(1.0F).g(255).a("lava").p().i();
-    public static final Block STATIONARY_LAVA = (new BlockStationary(11, Material.LAVA)).c(100.0F).a(1.0F).g(255).a("lava").p().i();
+    public static final Block BEDROCK = (new Block(7, 17, Material.STONE)).k().b(6000000.0F).a(h).a("bedrock").r();
+    public static final Block WATER = (new BlockFlowing(8, Material.WATER)).c(100.0F).f(3).a("water").r().i();
+    public static final Block STATIONARY_WATER = (new BlockStationary(9, Material.WATER)).c(100.0F).f(3).a("water").r().i();
+    public static final Block LAVA = (new BlockFlowing(10, Material.LAVA)).c(0.0F).a(1.0F).f(255).a("lava").r().i();
+    public static final Block STATIONARY_LAVA = (new BlockStationary(11, Material.LAVA)).c(100.0F).a(1.0F).f(255).a("lava").r().i();
     public static final Block SAND = (new BlockSand(12, 18)).c(0.5F).a(l).a("sand");
     public static final Block GRAVEL = (new BlockGravel(13, 19)).c(0.6F).a(f).a("gravel");
     public static final Block GOLD_ORE = (new BlockOre(14, 32)).c(3.0F).b(5.0F).a(h).a("oreGold");
     public static final Block IRON_ORE = (new BlockOre(15, 33)).c(3.0F).b(5.0F).a(h).a("oreIron");
     public static final Block COAL_ORE = (new BlockOre(16, 34)).c(3.0F).b(5.0F).a(h).a("oreCoal");
     public static final Block LOG = (new BlockLog(17)).c(2.0F).a(e).a("log").i();
-    public static final BlockLeaves LEAVES = (BlockLeaves) (new BlockLeaves(18, 52)).c(0.2F).g(1).a(g).a("leaves").i();
+    public static final BlockLeaves LEAVES = (BlockLeaves) (new BlockLeaves(18, 52)).c(0.2F).f(1).a(g).a("leaves").i();
     public static final Block SPONGE = (new BlockSponge(19)).c(0.6F).a(g).a("sponge");
     public static final Block GLASS = (new BlockGlass(20, 49, Material.SHATTERABLE, false)).c(0.3F).a(j).a("glass");
     public static final Block LAPIS_ORE = (new BlockOre(21, 160)).c(3.0F).b(5.0F).a(h).a("oreLapis");
@@ -48,11 +46,11 @@ public class Block {
     public static final Block DISPENSER = (new BlockDispenser(23)).c(3.5F).a(h).a("dispenser").i();
     public static final Block SANDSTONE = (new BlockSandStone(24)).a(h).c(0.8F).a("sandStone");
     public static final Block NOTE_BLOCK = (new BlockNote(25)).c(0.8F).a("musicBlock").i();
-    public static final Block BED = (new BlockBed(26)).c(0.2F).a("bed").p().i();
+    public static final Block BED = (new BlockBed(26)).c(0.2F).a("bed").r().i();
     public static final Block GOLDEN_RAIL = (new BlockMinecartTrack(27, 179, true)).c(0.7F).a(i).a("goldenRail").i();
     public static final Block DETECTOR_RAIL = (new BlockMinecartDetector(28, 195)).c(0.7F).a(i).a("detectorRail").i();
     public static final Block PISTON_STICKY = (new BlockPiston(29, 106, true)).a("pistonStickyBase").i();
-    public static final Block WEB = (new BlockWeb(30, 11)).g(1).c(4.0F).a("web");
+    public static final Block WEB = (new BlockWeb(30, 11)).f(1).c(4.0F).a("web");
     public static final BlockLongGrass LONG_GRASS = (BlockLongGrass) (new BlockLongGrass(31, 39)).c(0.0F).a(g).a("tallgrass");
     public static final BlockDeadBush DEAD_BUSH = (BlockDeadBush) (new BlockDeadBush(32, 55)).c(0.0F).a(g).a("deadbush");
     public static final Block PISTON = (new BlockPiston(33, 107, false)).a("pistonBase").i();
@@ -73,39 +71,39 @@ public class Block {
     public static final Block MOSSY_COBBLESTONE = (new Block(48, 36, Material.STONE)).c(2.0F).b(10.0F).a(h).a("stoneMoss");
     public static final Block OBSIDIAN = (new BlockObsidian(49, 37)).c(50.0F).b(2000.0F).a(h).a("obsidian");
     public static final Block TORCH = (new BlockTorch(50, 80)).c(0.0F).a(0.9375F).a(e).a("torch").i();
-    public static final BlockFire FIRE = (BlockFire) (new BlockFire(51, 31)).c(0.0F).a(1.0F).a(e).a("fire").p();
-    public static final Block MOB_SPAWNER = (new BlockMobSpawner(52, 65)).c(5.0F).a(i).a("mobSpawner").p();
+    public static final BlockFire FIRE = (BlockFire) (new BlockFire(51, 31)).c(0.0F).a(1.0F).a(e).a("fire").r();
+    public static final Block MOB_SPAWNER = (new BlockMobSpawner(52, 65)).c(5.0F).a(i).a("mobSpawner").r();
     public static final Block WOOD_STAIRS = (new BlockStairs(53, WOOD)).a("stairsWood").i();
     public static final Block CHEST = (new BlockChest(54)).c(2.5F).a(e).a("chest").i();
-    public static final Block REDSTONE_WIRE = (new BlockRedstoneWire(55, 164)).c(0.0F).a(d).a("redstoneDust").p().i();
+    public static final Block REDSTONE_WIRE = (new BlockRedstoneWire(55, 164)).c(0.0F).a(d).a("redstoneDust").r().i();
     public static final Block DIAMOND_ORE = (new BlockOre(56, 50)).c(3.0F).b(5.0F).a(h).a("oreDiamond");
     public static final Block DIAMOND_BLOCK = (new BlockOreBlock(57, 24)).c(5.0F).b(10.0F).a(i).a("blockDiamond");
     public static final Block WORKBENCH = (new BlockWorkbench(58)).c(2.5F).a(e).a("workbench");
-    public static final Block CROPS = (new BlockCrops(59, 88)).c(0.0F).a(g).a("crops").p().i();
+    public static final Block CROPS = (new BlockCrops(59, 88)).c(0.0F).a(g).a("crops").r().i();
     public static final Block SOIL = (new BlockSoil(60)).c(0.6F).a(f).a("farmland").i();
     public static final Block FURNACE = (new BlockFurnace(61, false)).c(3.5F).a(h).a("furnace").i();
     public static final Block BURNING_FURNACE = (new BlockFurnace(62, true)).c(3.5F).a(h).a(0.875F).a("furnace").i();
-    public static final Block SIGN_POST = (new BlockSign(63, TileEntitySign.class, true)).c(1.0F).a(e).a("sign").p().i();
-    public static final Block WOODEN_DOOR = (new BlockDoor(64, Material.WOOD)).c(3.0F).a(e).a("doorWood").p().i();
+    public static final Block SIGN_POST = (new BlockSign(63, TileEntitySign.class, true)).c(1.0F).a(e).a("sign").r().i();
+    public static final Block WOODEN_DOOR = (new BlockDoor(64, Material.WOOD)).c(3.0F).a(e).a("doorWood").r().i();
     public static final Block LADDER = (new BlockLadder(65, 83)).c(0.4F).a(e).a("ladder").i();
     public static final Block RAILS = (new BlockMinecartTrack(66, 128, false)).c(0.7F).a(i).a("rail").i();
     public static final Block COBBLESTONE_STAIRS = (new BlockStairs(67, COBBLESTONE)).a("stairsStone").i();
-    public static final Block WALL_SIGN = (new BlockSign(68, TileEntitySign.class, false)).c(1.0F).a(e).a("sign").p().i();
+    public static final Block WALL_SIGN = (new BlockSign(68, TileEntitySign.class, false)).c(1.0F).a(e).a("sign").r().i();
     public static final Block LEVER = (new BlockLever(69, 96)).c(0.5F).a(e).a("lever").i();
     public static final Block STONE_PLATE = (new BlockPressurePlate(70, STONE.textureId, EnumMobType.MOBS, Material.STONE)).c(0.5F).a(h).a("pressurePlate").i();
-    public static final Block IRON_DOOR_BLOCK = (new BlockDoor(71, Material.ORE)).c(5.0F).a(i).a("doorIron").p().i();
+    public static final Block IRON_DOOR_BLOCK = (new BlockDoor(71, Material.ORE)).c(5.0F).a(i).a("doorIron").r().i();
     public static final Block WOOD_PLATE = (new BlockPressurePlate(72, WOOD.textureId, EnumMobType.EVERYTHING, Material.WOOD)).c(0.5F).a(e).a("pressurePlate").i();
     public static final Block REDSTONE_ORE = (new BlockRedstoneOre(73, 51, false)).c(3.0F).b(5.0F).a(h).a("oreRedstone").i();
     public static final Block GLOWING_REDSTONE_ORE = (new BlockRedstoneOre(74, 51, true)).a(0.625F).c(3.0F).b(5.0F).a(h).a("oreRedstone").i();
     public static final Block REDSTONE_TORCH_OFF = (new BlockRedstoneTorch(75, 115, false)).c(0.0F).a(e).a("notGate").i();
     public static final Block REDSTONE_TORCH_ON = (new BlockRedstoneTorch(76, 99, true)).c(0.0F).a(0.5F).a(e).a("notGate").i();
     public static final Block STONE_BUTTON = (new BlockButton(77, STONE.textureId)).c(0.5F).a(h).a("button").i();
-    public static final Block SNOW = (new BlockSnow(78, 66)).c(0.1F).a(k).a("snow").g(0);
-    public static final Block ICE = (new BlockIce(79, 67)).c(0.5F).g(3).a(j).a("ice");
+    public static final Block SNOW = (new BlockSnow(78, 66)).c(0.1F).a(k).a("snow").f(0);
+    public static final Block ICE = (new BlockIce(79, 67)).c(0.5F).f(3).a(j).a("ice");
     public static final Block SNOW_BLOCK = (new BlockSnowBlock(80, 66)).c(0.2F).a(k).a("snow");
     public static final Block CACTUS = (new BlockCactus(81, 70)).c(0.4F).a(k).a("cactus");
     public static final Block CLAY = (new BlockClay(82, 72)).c(0.6F).a(f).a("clay");
-    public static final Block SUGAR_CANE_BLOCK = (new BlockReed(83, 73)).c(0.0F).a(g).a("reeds").p();
+    public static final Block SUGAR_CANE_BLOCK = (new BlockReed(83, 73)).c(0.0F).a(g).a("reeds").r();
     public static final Block JUKEBOX = (new BlockJukeBox(84, 74)).c(2.0F).b(10.0F).a(h).a("jukebox").i();
     public static final Block FENCE = (new BlockFence(85, 4)).c(2.0F).b(5.0F).a(e).a("fence");
     public static final Block PUMPKIN = (new BlockPumpkin(86, 102, false)).c(1.0F).a(e).a("pumpkin").i();
@@ -114,11 +112,11 @@ public class Block {
     public static final Block GLOWSTONE = (new BlockLightStone(89, 105, Material.SHATTERABLE)).c(0.3F).a(j).a(1.0F).a("lightgem");
     public static final BlockPortal PORTAL = (BlockPortal) (new BlockPortal(90, 14)).c(-1.0F).a(j).a(0.75F).a("portal");
     public static final Block JACK_O_LANTERN = (new BlockPumpkin(91, 102, true)).c(1.0F).a(e).a(1.0F).a("litpumpkin").i();
-    public static final Block CAKE_BLOCK = (new BlockCake(92, 121)).c(0.5F).a(k).a("cake").p().i();
-    public static final Block DIODE_OFF = (new BlockDiode(93, false)).c(0.0F).a(e).a("diode").p().i();
-    public static final Block DIODE_ON = (new BlockDiode(94, true)).c(0.0F).a(0.625F).a(e).a("diode").p().i();
+    public static final Block CAKE_BLOCK = (new BlockCake(92, 121)).c(0.5F).a(k).a("cake").r().i();
+    public static final Block DIODE_OFF = (new BlockDiode(93, false)).c(0.0F).a(e).a("diode").r().i();
+    public static final Block DIODE_ON = (new BlockDiode(94, true)).c(0.0F).a(0.625F).a(e).a("diode").r().i();
     public static final Block LOCKED_CHEST = (new BlockLockedChest(95)).c(0.0F).a(1.0F).a(e).a("lockedchest").a(true).i();
-    public static final Block TRAP_DOOR = (new BlockTrapdoor(96, Material.WOOD)).c(3.0F).a(e).a("trapdoor").p().i();
+    public static final Block TRAP_DOOR = (new BlockTrapdoor(96, Material.WOOD)).c(3.0F).a(e).a("trapdoor").r().i();
     public static final Block MONSTER_EGGS = (new BlockMonsterEggs(97)).c(0.75F);
     public static final Block SMOOTH_BRICK = (new BlockSmoothBrick(98)).c(1.5F).b(10.0F).a(h).a("stonebricksmooth");
     public static final Block BIG_MUSHROOM_1 = (new BlockHugeMushroom(99, Material.WOOD, 142, 0)).c(0.2F).a(e).a("mushroom").i();
@@ -145,12 +143,16 @@ public class Block {
     public static final Block ENDER_PORTAL_FRAME = (new BlockEnderPortalFrame(120)).a(j).a(0.125F).c(-1.0F).a("endPortalFrame").i().b(6000000.0F);
     public static final Block WHITESTONE = (new Block(121, 175, Material.STONE)).c(3.0F).b(15.0F).a(h).a("whiteStone");
     public static final Block DRAGON_EGG = (new BlockDragonEgg(122, 167)).c(3.0F).b(15.0F).a(h).a(0.125F).a("dragonEgg");
+    public static final Block REDSTONE_LAMP_OFF = (new BlockRedstoneLamp(123, false)).c(0.3F).a(j).a("redstoneLight");
+    public static final Block REDSTONE_LAMP_ON = (new BlockRedstoneLamp(124, true)).c(0.3F).a(j).a("redstoneLight");
     public int textureId;
     public final int id;
     protected float strength;
     protected float durability;
     protected boolean bR;
     protected boolean bS;
+    protected boolean bT;
+    protected boolean isTileEntity;
     public double minX;
     public double minY;
     public double minZ;
@@ -158,7 +160,7 @@ public class Block {
     public double maxY;
     public double maxZ;
     public StepSound stepSound;
-    public float ca;
+    public float cc;
     public final Material material;
     public float frictionFactor;
     private String name;
@@ -167,7 +169,7 @@ public class Block {
         this.bR = true;
         this.bS = true;
         this.stepSound = d;
-        this.ca = 1.0F;
+        this.cc = 1.0F;
         this.frictionFactor = 0.6F;
         if (byId[i] != null) {
             throw new IllegalArgumentException("Slot " + i + " is already occupied by " + byId[i] + " when adding " + this);
@@ -176,15 +178,14 @@ public class Block {
             byId[i] = this;
             this.id = i;
             this.a(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
-            o[i] = this.a();
+            n[i] = this.a();
             lightBlock[i] = this.a() ? 255 : 0;
-            r[i] = !material.blocksLight();
-            isTileEntity[i] = false;
+            p[i] = !material.blocksLight();
         }
     }
 
     protected Block i() {
-        t[this.id] = true;
+        r[this.id] = true;
         return this;
     }
 
@@ -200,7 +201,7 @@ public class Block {
         return this;
     }
 
-    protected Block g(int i) {
+    protected Block f(int i) {
         lightBlock[this.id] = i;
         return this;
     }
@@ -215,10 +216,20 @@ public class Block {
         return this;
     }
 
+    public static boolean g(int i) {
+        Block block = byId[i];
+
+        return block == null ? false : block.material.j() && block.b();
+    }
+
     public boolean b() {
         return true;
     }
 
+    public boolean b(IBlockAccess iblockaccess, int i, int j, int k) {
+        return !this.material.isSolid();
+    }
+
     public int c() {
         return 0;
     }
@@ -242,10 +253,18 @@ public class Block {
     }
 
     protected Block a(boolean flag) {
-        n[this.id] = flag;
+        this.bT = flag;
         return this;
     }
 
+    public boolean m() {
+        return this.bT;
+    }
+
+    public boolean n() {
+        return this.isTileEntity;
+    }
+
     public void a(float f, float f1, float f2, float f3, float f4, float f5) {
         this.minX = (double) f;
         this.minY = (double) f1;
@@ -284,10 +303,10 @@ public class Block {
     }
 
     public boolean a(int i, boolean flag) {
-        return this.x_();
+        return this.F_();
     }
 
-    public boolean x_() {
+    public boolean F_() {
         return true;
     }
 
@@ -509,7 +528,7 @@ public class Block {
     public void a(World world, EntityHuman entityhuman, int i, int j, int k, int l) {
         entityhuman.a(StatisticList.C[this.id], 1);
         entityhuman.c(0.025F);
-        if (this.b() && !isTileEntity[this.id] && EnchantmentManager.hasSilkTouchEnchantment(entityhuman.inventory)) {
+        if (this.b() && !this.isTileEntity && EnchantmentManager.hasSilkTouchEnchantment(entityhuman.inventory)) {
             ItemStack itemstack = this.a_(l);
 
             if (itemstack != null) {
@@ -548,20 +567,20 @@ public class Block {
     }
 
     public String getName() {
-        return LocaleI18n.get(this.n() + ".name");
+        return LocaleI18n.get(this.p() + ".name");
     }
 
-    public String n() {
+    public String p() {
         return this.name;
     }
 
     public void a(World world, int i, int j, int k, int l, int i1) {}
 
-    public boolean o() {
+    public boolean q() {
         return this.bS;
     }
 
-    protected Block p() {
+    protected Block r() {
         this.bS = false;
         return this;
     }
@@ -606,11 +625,15 @@ public class Block {
                     flag = true;
                 }
 
-                u[i] = flag;
+                if (p[i]) {
+                    flag = true;
+                }
+
+                s[i] = flag;
             }
         }
 
-        r[0] = true;
+        p[0] = true;
         StatisticList.b();
     }
 }
