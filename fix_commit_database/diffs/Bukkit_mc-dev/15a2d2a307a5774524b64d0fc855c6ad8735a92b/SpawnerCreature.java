@@ -14,9 +14,10 @@ public final class SpawnerCreature {
     public SpawnerCreature() {}
 
     protected static ChunkPosition getRandomPosition(World world, int i, int j) {
-        int k = i + world.random.nextInt(16);
-        int l = world.random.nextInt(world.height);
-        int i1 = j + world.random.nextInt(16);
+        Chunk chunk = world.getChunkAt(i, j);
+        int k = i * 16 + world.random.nextInt(16);
+        int l = world.random.nextInt(chunk == null ? 128 : Math.max(128, chunk.g()));
+        int i1 = j * 16 + world.random.nextInt(16);
 
         return new ChunkPosition(k, l, i1);
     }
@@ -68,7 +69,7 @@ public final class SpawnerCreature {
                         ChunkCoordIntPair chunkcoordintpair1 = (ChunkCoordIntPair) iterator.next();
 
                         if (!((Boolean) b.get(chunkcoordintpair1)).booleanValue()) {
-                            ChunkPosition chunkposition = getRandomPosition(world, chunkcoordintpair1.x * 16, chunkcoordintpair1.z * 16);
+                            ChunkPosition chunkposition = getRandomPosition(world, chunkcoordintpair1.x, chunkcoordintpair1.z);
                             int k1 = chunkposition.x;
                             int l1 = chunkposition.y;
                             int i2 = chunkposition.z;
@@ -124,7 +125,7 @@ public final class SpawnerCreature {
                                                                 ++j2;
                                                                 world.addEntity(entityliving);
                                                                 a(entityliving, world, f, f1, f2);
-                                                                if (j2 >= entityliving.p()) {
+                                                                if (j2 >= entityliving.q()) {
                                                                     continue label108;
                                                                 }
                                                             }
@@ -153,8 +154,14 @@ public final class SpawnerCreature {
         }
     }
 
-    private static boolean a(EnumCreatureType enumcreaturetype, World world, int i, int j, int k) {
-        return enumcreaturetype.c() == Material.WATER ? world.getMaterial(i, j, k).isLiquid() && !world.e(i, j + 1, k) : world.e(i, j - 1, k) && !world.e(i, j, k) && !world.getMaterial(i, j, k).isLiquid() && !world.e(i, j + 1, k);
+    public static boolean a(EnumCreatureType enumcreaturetype, World world, int i, int j, int k) {
+        if (enumcreaturetype.c() == Material.WATER) {
+            return world.getMaterial(i, j, k).isLiquid() && !world.e(i, j + 1, k);
+        } else {
+            int l = world.getTypeId(i, j - 1, k);
+
+            return Block.g(l) && l != Block.BEDROCK.id && !world.e(i, j, k) && !world.getMaterial(i, j, k).isLiquid() && !world.e(i, j + 1, k);
+        }
     }
 
     private static void a(EntityLiving entityliving, World world, float f, float f1, float f2) {
@@ -166,6 +173,14 @@ public final class SpawnerCreature {
             entityskeleton.mount(entityliving);
         } else if (entityliving instanceof EntitySheep) {
             ((EntitySheep) entityliving).setColor(EntitySheep.a(world.random));
+        } else if (entityliving instanceof EntityOcelot && world.random.nextInt(7) == 0) {
+            for (int i = 0; i < 2; ++i) {
+                EntityOcelot entityocelot = new EntityOcelot(world);
+
+                entityocelot.setPositionRotation((double) f, (double) f1, (double) f2, entityliving.yaw, 0.0F);
+                entityocelot.setAge(-24000);
+                world.addEntity(entityocelot);
+            }
         }
     }
 
@@ -173,7 +188,7 @@ public final class SpawnerCreature {
         List list = biomebase.getMobs(EnumCreatureType.CREATURE);
 
         if (!list.isEmpty()) {
-            while (random.nextFloat() < biomebase.d()) {
+            while (random.nextFloat() < biomebase.e()) {
                 BiomeMeta biomemeta = (BiomeMeta) WeightedRandom.a(world.random, (Collection) list);
                 int i1 = biomemeta.b + random.nextInt(1 + biomemeta.c - biomemeta.b);
                 int j1 = i + random.nextInt(k);
@@ -185,7 +200,7 @@ public final class SpawnerCreature {
                     boolean flag = false;
 
                     for (int k2 = 0; !flag && k2 < 4; ++k2) {
-                        int l2 = world.f(j1, k1);
+                        int l2 = world.g(j1, k1);
 
                         if (a(EnumCreatureType.CREATURE, world, j1, l2, k1)) {
                             float f = (float) j1 + 0.5F;
