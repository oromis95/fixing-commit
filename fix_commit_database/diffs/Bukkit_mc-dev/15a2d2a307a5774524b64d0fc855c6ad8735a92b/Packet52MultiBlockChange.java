@@ -1,16 +1,17 @@
 package net.minecraft.server;
 
+import java.io.ByteArrayOutputStream;
 import java.io.DataInputStream;
 import java.io.DataOutputStream;
+import java.io.IOException;
 
 public class Packet52MultiBlockChange extends Packet {
 
     public int a;
     public int b;
-    public short[] c;
-    public byte[] d;
-    public byte[] e;
-    public int f;
+    public byte[] c;
+    public int d;
+    private static byte[] e = new byte[0];
 
     public Packet52MultiBlockChange() {
         this.lowPriority = true;
@@ -20,50 +21,62 @@ public class Packet52MultiBlockChange extends Packet {
         this.lowPriority = true;
         this.a = i;
         this.b = j;
-        this.f = k;
-        this.c = new short[k];
-        this.d = new byte[k];
-        this.e = new byte[k];
+        this.d = k;
+        int l = 4 * k;
         Chunk chunk = world.getChunkAt(i, j);
 
-        for (int l = 0; l < k; ++l) {
-            int i1 = ashort[l] >> 12 & 15;
-            int j1 = ashort[l] >> 8 & 15;
-            int k1 = ashort[l] & 255;
+        try {
+            if (k >= 64) {
+                System.out.println("ChunkTilesUpdatePacket compress " + k);
+                if (e.length < l) {
+                    e = new byte[l];
+                }
+            } else {
+                ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream(l);
+                DataOutputStream dataoutputstream = new DataOutputStream(bytearrayoutputstream);
 
-            this.c[l] = ashort[l];
-            this.d[l] = (byte) chunk.getTypeId(i1, k1, j1);
-            this.e[l] = (byte) chunk.getData(i1, k1, j1);
+                for (int i1 = 0; i1 < k; ++i1) {
+                    int j1 = ashort[i1] >> 12 & 15;
+                    int k1 = ashort[i1] >> 8 & 15;
+                    int l1 = ashort[i1] & 255;
+
+                    dataoutputstream.writeShort(ashort[i1]);
+                    dataoutputstream.writeShort((short) ((chunk.getTypeId(j1, l1, k1) & 4095) << 4 | chunk.getData(j1, l1, k1) & 15));
+                }
+
+                this.c = bytearrayoutputstream.toByteArray();
+                if (this.c.length != l) {
+                    throw new RuntimeException("Expected length " + l + " doesn\'t match received length " + this.c.length);
+                }
+            }
+        } catch (IOException ioexception) {
+            System.err.println(ioexception.getMessage());
+            this.c = null;
         }
     }
 
     public void a(DataInputStream datainputstream) {
         this.a = datainputstream.readInt();
         this.b = datainputstream.readInt();
-        this.f = datainputstream.readShort() & '\uffff';
-        this.c = new short[this.f];
-        this.d = new byte[this.f];
-        this.e = new byte[this.f];
+        this.d = datainputstream.readShort() & '\uffff';
+        int i = datainputstream.readInt();
 
-        for (int i = 0; i < this.f; ++i) {
-            this.c[i] = datainputstream.readShort();
+        if (i > 0) {
+            this.c = new byte[i];
+            datainputstream.readFully(this.c);
         }
-
-        datainputstream.readFully(this.d);
-        datainputstream.readFully(this.e);
     }
 
     public void a(DataOutputStream dataoutputstream) {
         dataoutputstream.writeInt(this.a);
         dataoutputstream.writeInt(this.b);
-        dataoutputstream.writeShort((short) this.f);
-
-        for (int i = 0; i < this.f; ++i) {
-            dataoutputstream.writeShort(this.c[i]);
+        dataoutputstream.writeShort((short) this.d);
+        if (this.c != null) {
+            dataoutputstream.writeInt(this.c.length);
+            dataoutputstream.write(this.c);
+        } else {
+            dataoutputstream.writeInt(0);
         }
-
-        dataoutputstream.write(this.d);
-        dataoutputstream.write(this.e);
     }
 
     public void handle(NetHandler nethandler) {
@@ -71,6 +84,6 @@ public class Packet52MultiBlockChange extends Packet {
     }
 
     public int a() {
-        return 10 + this.f * 4;
+        return 10 + this.d * 4;
     }
 }
