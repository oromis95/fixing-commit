@@ -112,7 +112,7 @@ public class EntityPainting extends Entity {
         return i == 32 ? 0.5F : (i == 64 ? 0.5F : 0.0F);
     }
 
-    public void y_() {
+    public void G_() {
         if (this.f++ == 100 && !this.world.isStatic) {
             this.f = 0;
             if (!this.survives()) {
@@ -180,14 +180,14 @@ public class EntityPainting extends Entity {
         }
     }
 
-    public boolean e_() {
+    public boolean o_() {
         return true;
     }
 
     public boolean damageEntity(DamageSource damagesource, int i) {
         if (!this.dead && !this.world.isStatic) {
             this.die();
-            this.aM();
+            this.aV();
             this.world.addEntity(new EntityItem(this.world, this.locX, this.locY, this.locZ, new ItemStack(Item.PAINTING)));
         }
 
