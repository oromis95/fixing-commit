@@ -4,15 +4,15 @@ public class BiomeMushrooms extends BiomeBase {
 
     public BiomeMushrooms(int i) {
         super(i);
-        this.G.z = -100;
-        this.G.A = -100;
-        this.G.B = -100;
-        this.G.D = 1;
-        this.G.J = 1;
-        this.y = (byte) Block.MYCEL.id;
-        this.H.clear();
-        this.I.clear();
+        this.I.z = -100;
+        this.I.A = -100;
+        this.I.B = -100;
+        this.I.D = 1;
+        this.I.J = 1;
+        this.A = (byte) Block.MYCEL.id;
         this.J.clear();
-        this.I.add(new BiomeMeta(EntityMushroomCow.class, 8, 4, 8));
+        this.K.clear();
+        this.L.clear();
+        this.K.add(new BiomeMeta(EntityMushroomCow.class, 8, 4, 8));
     }
 }
