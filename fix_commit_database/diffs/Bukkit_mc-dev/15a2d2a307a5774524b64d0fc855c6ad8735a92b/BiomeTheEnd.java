@@ -4,12 +4,12 @@ public class BiomeTheEnd extends BiomeBase {
 
     public BiomeTheEnd(int i) {
         super(i);
-        this.H.clear();
-        this.I.clear();
         this.J.clear();
-        this.H.add(new BiomeMeta(EntityEnderman.class, 10, 4, 4));
-        this.y = (byte) Block.DIRT.id;
-        this.z = (byte) Block.DIRT.id;
-        this.G = new BiomeTheEndDecorator(this);
+        this.K.clear();
+        this.L.clear();
+        this.J.add(new BiomeMeta(EntityEnderman.class, 10, 4, 4));
+        this.A = (byte) Block.DIRT.id;
+        this.B = (byte) Block.DIRT.id;
+        this.I = new BiomeTheEndDecorator(this);
     }
 }
