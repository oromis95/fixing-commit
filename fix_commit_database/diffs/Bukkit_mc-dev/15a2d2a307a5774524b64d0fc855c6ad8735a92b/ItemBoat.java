@@ -30,7 +30,7 @@ public class ItemBoat extends Item {
         if (movingobjectposition == null) {
             return itemstack;
         } else {
-            Vec3D vec3d2 = entityhuman.e(f);
+            Vec3D vec3d2 = entityhuman.f(f);
             boolean flag = false;
             float f9 = 1.0F;
             List list = world.getEntities(entityhuman, entityhuman.boundingBox.a(vec3d2.a * d3, vec3d2.b * d3, vec3d2.c * d3).grow((double) f9, (double) f9, (double) f9));
@@ -38,7 +38,7 @@ public class ItemBoat extends Item {
             for (int i = 0; i < list.size(); ++i) {
                 Entity entity = (Entity) list.get(i);
 
-                if (entity.e_()) {
+                if (entity.o_()) {
                     float f10 = entity.j_();
                     AxisAlignedBB axisalignedbb = entity.boundingBox.grow((double) f10, (double) f10, (double) f10);
 
