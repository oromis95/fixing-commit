@@ -12,6 +12,10 @@ public class BlockStationary extends BlockFluids {
         }
     }
 
+    public boolean b(IBlockAccess iblockaccess, int i, int j, int k) {
+        return this.material != Material.LAVA;
+    }
+
     public void doPhysics(World world, int i, int j, int k, int l) {
         super.doPhysics(world, i, j, k, l);
         if (world.getTypeId(i, j, k) == this.id) {
@@ -33,12 +37,14 @@ public class BlockStationary extends BlockFluids {
         if (this.material == Material.LAVA) {
             int l = random.nextInt(3);
 
-            for (int i1 = 0; i1 < l; ++i1) {
+            int i1;
+            int j1;
+
+            for (i1 = 0; i1 < l; ++i1) {
                 i += random.nextInt(3) - 1;
                 ++j;
                 k += random.nextInt(3) - 1;
-                int j1 = world.getTypeId(i, j, k);
-
+                j1 = world.getTypeId(i, j, k);
                 if (j1 == 0) {
                     if (this.j(world, i - 1, j, k) || this.j(world, i + 1, j, k) || this.j(world, i, j, k - 1) || this.j(world, i, j, k + 1) || this.j(world, i, j - 1, k) || this.j(world, i, j + 1, k)) {
                         world.setTypeId(i, j, k, Block.FIRE.id);
@@ -48,6 +54,19 @@ public class BlockStationary extends BlockFluids {
                     return;
                 }
             }
+
+            if (l == 0) {
+                i1 = i;
+                j1 = k;
+
+                for (int k1 = 0; k1 < 3; ++k1) {
+                    i = i1 + random.nextInt(3) - 1;
+                    k = j1 + random.nextInt(3) - 1;
+                    if (world.isEmpty(i, j + 1, k) && this.j(world, i, j, k)) {
+                        world.setTypeId(i, j + 1, k, Block.FIRE.id);
+                    }
+                }
+            }
         }
     }
 
