@@ -7,61 +7,57 @@ public class Packet1Login extends Packet {
 
     public int a;
     public String name;
-    public long c;
-    public WorldType d;
+    public WorldType c;
+    public int d;
     public int e;
     public byte f;
     public byte g;
     public byte h;
-    public byte i;
 
     public Packet1Login() {}
 
-    public Packet1Login(String s, int i, long j, WorldType worldtype, int k, byte b0, byte b1, byte b2, byte b3) {
+    public Packet1Login(String s, int i, WorldType worldtype, int j, int k, byte b0, byte b1, byte b2) {
         this.name = s;
         this.a = i;
-        this.c = j;
-        this.d = worldtype;
+        this.c = worldtype;
+        this.e = k;
         this.f = b0;
+        this.d = j;
         this.g = b1;
-        this.e = k;
         this.h = b2;
-        this.i = b3;
     }
 
     public void a(DataInputStream datainputstream) {
         this.a = datainputstream.readInt();
         this.name = a(datainputstream, 16);
-        this.c = datainputstream.readLong();
         String s = a(datainputstream, 16);
 
-        this.d = WorldType.getType(s);
-        if (this.d == null) {
-            this.d = WorldType.NORMAL;
+        this.c = WorldType.getType(s);
+        if (this.c == null) {
+            this.c = WorldType.NORMAL;
         }
 
+        this.d = datainputstream.readInt();
         this.e = datainputstream.readInt();
         this.f = datainputstream.readByte();
         this.g = datainputstream.readByte();
         this.h = datainputstream.readByte();
-        this.i = datainputstream.readByte();
     }
 
     public void a(DataOutputStream dataoutputstream) {
         dataoutputstream.writeInt(this.a);
         a(this.name, dataoutputstream);
-        dataoutputstream.writeLong(this.c);
-        if (this.d == null) {
+        if (this.c == null) {
             a("", dataoutputstream);
         } else {
-            a(this.d.name(), dataoutputstream);
+            a(this.c.name(), dataoutputstream);
         }
 
+        dataoutputstream.writeInt(this.d);
         dataoutputstream.writeInt(this.e);
         dataoutputstream.writeByte(this.f);
         dataoutputstream.writeByte(this.g);
         dataoutputstream.writeByte(this.h);
-        dataoutputstream.writeByte(this.i);
     }
 
     public void handle(NetHandler nethandler) {
@@ -71,10 +67,10 @@ public class Packet1Login extends Packet {
     public int a() {
         int i = 0;
 
-        if (this.d != null) {
-            i = this.d.name().length();
+        if (this.c != null) {
+            i = this.c.name().length();
         }
 
-        return 4 + this.name.length() + 4 + 7 + 4 + i;
+        return 4 + this.name.length() + 4 + 7 + 7 + i;
     }
 }
