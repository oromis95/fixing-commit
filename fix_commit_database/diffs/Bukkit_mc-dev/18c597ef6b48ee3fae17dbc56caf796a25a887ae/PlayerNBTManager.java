@@ -79,18 +79,30 @@ public class PlayerNBTManager implements PlayerFileData, IDataManager {
 
     public WorldData c() {
         File file1 = new File(this.b, "level.dat");
+        NBTTagCompound nbttagcompound;
+        NBTTagCompound nbttagcompound1;
 
         if (file1.exists()) {
             try {
-                NBTTagCompound nbttagcompound = CompressedStreamTools.a((InputStream) (new FileInputStream(file1)));
-                NBTTagCompound nbttagcompound1 = nbttagcompound.k("Data");
-
+                nbttagcompound = CompressedStreamTools.a((InputStream) (new FileInputStream(file1)));
+                nbttagcompound1 = nbttagcompound.k("Data");
                 return new WorldData(nbttagcompound1);
             } catch (Exception exception) {
                 exception.printStackTrace();
             }
         }
 
+        file1 = new File(this.b, "level.dat_old");
+        if (file1.exists()) {
+            try {
+                nbttagcompound = CompressedStreamTools.a((InputStream) (new FileInputStream(file1)));
+                nbttagcompound1 = nbttagcompound.k("Data");
+                return new WorldData(nbttagcompound1);
+            } catch (Exception exception1) {
+                exception1.printStackTrace();
+            }
+        }
+
         return null;
     }
 
