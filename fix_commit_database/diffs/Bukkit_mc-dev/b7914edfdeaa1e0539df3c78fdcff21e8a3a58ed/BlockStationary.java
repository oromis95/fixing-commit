@@ -14,7 +14,7 @@ public class BlockStationary extends BlockFluids {
 
     public void b(World world, int i, int j, int k, int l) {
         super.b(world, i, j, k, l);
-        if (world.a(i, j, k) == this.bc) {
+        if (world.a(i, j, k) == this.bi) {
             this.i(world, i, j, k);
         }
     }
@@ -22,15 +22,15 @@ public class BlockStationary extends BlockFluids {
     private void i(World world, int i, int j, int k) {
         int l = world.b(i, j, k);
 
-        world.h = true;
-        world.a(i, j, k, this.bc - 1, l);
+        world.i = true;
+        world.a(i, j, k, this.bi - 1, l);
         world.b(i, j, k, i, j, k);
-        world.h(i, j, k, this.bc - 1);
-        world.h = false;
+        world.h(i, j, k, this.bi - 1);
+        world.i = false;
     }
 
     public void a(World world, int i, int j, int k, Random random) {
-        if (this.bn == Material.g) {
+        if (this.bt == Material.g) {
             int l = random.nextInt(3);
 
             for (int i1 = 0; i1 < l; ++i1) {
@@ -41,10 +41,10 @@ public class BlockStationary extends BlockFluids {
 
                 if (j1 == 0) {
                     if (this.j(world, i - 1, j, k) || this.j(world, i + 1, j, k) || this.j(world, i, j, k - 1) || this.j(world, i, j, k + 1) || this.j(world, i, j - 1, k) || this.j(world, i, j + 1, k)) {
-                        world.d(i, j, k, Block.FIRE.bc);
+                        world.d(i, j, k, Block.FIRE.bi);
                         return;
                     }
-                } else if (Block.n[j1].bn.c()) {
+                } else if (Block.n[j1].bt.c()) {
                     return;
                 }
             }
