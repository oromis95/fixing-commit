@@ -11,7 +11,7 @@ public class BlockRedstoneTorch extends BlockTorch {
 
     private boolean a(World world, int i, int j, int k, boolean flag) {
         if (flag) {
-            b.add(new RedstoneUpdateInfo(i, j, k, world.c));
+            b.add(new RedstoneUpdateInfo(i, j, k, world.e));
         }
 
         int l = 0;
@@ -36,7 +36,7 @@ public class BlockRedstoneTorch extends BlockTorch {
         this.a(true);
     }
 
-    public int c() {
+    public int b() {
         return 2;
     }
 
@@ -46,23 +46,23 @@ public class BlockRedstoneTorch extends BlockTorch {
         }
 
         if (this.a) {
-            world.g(i, j - 1, k, this.bc);
-            world.g(i, j + 1, k, this.bc);
-            world.g(i - 1, j, k, this.bc);
-            world.g(i + 1, j, k, this.bc);
-            world.g(i, j, k - 1, this.bc);
-            world.g(i, j, k + 1, this.bc);
+            world.g(i, j - 1, k, this.bi);
+            world.g(i, j + 1, k, this.bi);
+            world.g(i - 1, j, k, this.bi);
+            world.g(i + 1, j, k, this.bi);
+            world.g(i, j, k - 1, this.bi);
+            world.g(i, j, k + 1, this.bi);
         }
     }
 
     public void b(World world, int i, int j, int k) {
         if (this.a) {
-            world.g(i, j - 1, k, this.bc);
-            world.g(i, j + 1, k, this.bc);
-            world.g(i - 1, j, k, this.bc);
-            world.g(i + 1, j, k, this.bc);
-            world.g(i, j, k - 1, this.bc);
-            world.g(i, j, k + 1, this.bc);
+            world.g(i, j - 1, k, this.bi);
+            world.g(i, j + 1, k, this.bi);
+            world.g(i - 1, j, k, this.bi);
+            world.g(i + 1, j, k, this.bi);
+            world.g(i, j, k - 1, this.bi);
+            world.g(i, j, k + 1, this.bi);
         }
     }
 
@@ -85,15 +85,15 @@ public class BlockRedstoneTorch extends BlockTorch {
     public void a(World world, int i, int j, int k, Random random) {
         boolean flag = this.g(world, i, j, k);
 
-        while (b.size() > 0 && world.c - ((RedstoneUpdateInfo) b.get(0)).d > 100L) {
+        while (b.size() > 0 && world.e - ((RedstoneUpdateInfo) b.get(0)).d > 100L) {
             b.remove(0);
         }
 
         if (this.a) {
             if (flag) {
-                world.b(i, j, k, Block.REDSTONE_TORCH_OFF.bc, world.b(i, j, k));
+                world.b(i, j, k, Block.REDSTONE_TORCH_OFF.bi, world.b(i, j, k));
                 if (this.a(world, i, j, k, true)) {
-                    world.a((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), "random.fizz", 0.5F, 2.6F + (world.m.nextFloat() - world.m.nextFloat()) * 0.8F);
+                    world.a((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), "random.fizz", 0.5F, 2.6F + (world.l.nextFloat() - world.l.nextFloat()) * 0.8F);
 
                     for (int l = 0; l < 5; ++l) {
                         double d0 = (double) i + random.nextDouble() * 0.6D + 0.2D;
@@ -105,13 +105,13 @@ public class BlockRedstoneTorch extends BlockTorch {
                 }
             }
         } else if (!flag && !this.a(world, i, j, k, false)) {
-            world.b(i, j, k, Block.REDSTONE_TORCH_ON.bc, world.b(i, j, k));
+            world.b(i, j, k, Block.REDSTONE_TORCH_ON.bi, world.b(i, j, k));
         }
     }
 
     public void b(World world, int i, int j, int k, int l) {
         super.b(world, i, j, k, l);
-        world.h(i, j, k, this.bc);
+        world.h(i, j, k, this.bi);
     }
 
     public boolean d(World world, int i, int j, int k, int l) {
@@ -119,10 +119,10 @@ public class BlockRedstoneTorch extends BlockTorch {
     }
 
     public int a(int i, Random random) {
-        return Block.REDSTONE_TORCH_ON.bc;
+        return Block.REDSTONE_TORCH_ON.bi;
     }
 
-    public boolean d() {
+    public boolean c() {
         return true;
     }
 }
