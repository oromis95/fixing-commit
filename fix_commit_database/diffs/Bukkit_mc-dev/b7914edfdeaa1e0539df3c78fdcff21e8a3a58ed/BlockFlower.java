@@ -6,7 +6,7 @@ public class BlockFlower extends Block {
 
     protected BlockFlower(int i, int j) {
         super(i, Material.i);
-        this.bb = j;
+        this.bh = j;
         this.a(true);
         float f = 0.2F;
 
@@ -18,7 +18,7 @@ public class BlockFlower extends Block {
     }
 
     protected boolean b(int i) {
-        return i == Block.GRASS.bc || i == Block.DIRT.bc || i == Block.SOIL.bc;
+        return i == Block.GRASS.bi || i == Block.DIRT.bi || i == Block.SOIL.bi;
     }
 
     public void b(World world, int i, int j, int k, int l) {
@@ -45,11 +45,7 @@ public class BlockFlower extends Block {
         return null;
     }
 
-    public boolean b() {
+    public boolean a() {
         return false;
     }
-
-    public int a() {
-        return 1;
-    }
 }
