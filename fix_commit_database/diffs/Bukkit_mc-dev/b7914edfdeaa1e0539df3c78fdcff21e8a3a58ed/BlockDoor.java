@@ -6,9 +6,9 @@ public class BlockDoor extends Block {
 
     protected BlockDoor(int i, Material material) {
         super(i, material);
-        this.bb = 97;
+        this.bh = 97;
         if (material == Material.e) {
-            ++this.bb;
+            ++this.bh;
         }
 
         float f = 0.5F;
@@ -17,14 +17,10 @@ public class BlockDoor extends Block {
         this.a(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, f1, 0.5F + f);
     }
 
-    public boolean b() {
+    public boolean a() {
         return false;
     }
 
-    public int a() {
-        return 7;
-    }
-
     public AxisAlignedBB d(World world, int i, int j, int k) {
         this.a((IBlockAccess) world, i, j, k);
         return super.d(world, i, j, k);
@@ -60,28 +56,28 @@ public class BlockDoor extends Block {
     }
 
     public boolean a(World world, int i, int j, int k, EntityHuman entityhuman) {
-        if (this.bn == Material.e) {
+        if (this.bt == Material.e) {
             return true;
         } else {
             int l = world.b(i, j, k);
 
             if ((l & 8) != 0) {
-                if (world.a(i, j - 1, k) == this.bc) {
+                if (world.a(i, j - 1, k) == this.bi) {
                     this.a(world, i, j - 1, k, entityhuman);
                 }
 
                 return true;
             } else {
-                if (world.a(i, j + 1, k) == this.bc) {
+                if (world.a(i, j + 1, k) == this.bi) {
                     world.b(i, j + 1, k, (l ^ 4) + 8);
                 }
 
                 world.b(i, j, k, l ^ 4);
                 world.b(i, j - 1, k, i, j, k);
                 if (Math.random() < 0.5D) {
-                    world.a((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.door_open", 1.0F, world.m.nextFloat() * 0.1F + 0.9F);
+                    world.a((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.door_open", 1.0F, world.l.nextFloat() * 0.1F + 0.9F);
                 } else {
-                    world.a((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.door_close", 1.0F, world.m.nextFloat() * 0.1F + 0.9F);
+                    world.a((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.door_close", 1.0F, world.l.nextFloat() * 0.1F + 0.9F);
                 }
 
                 return true;
@@ -93,23 +89,23 @@ public class BlockDoor extends Block {
         int l = world.b(i, j, k);
 
         if ((l & 8) != 0) {
-            if (world.a(i, j - 1, k) == this.bc) {
+            if (world.a(i, j - 1, k) == this.bi) {
                 this.a(world, i, j - 1, k, flag);
             }
         } else {
             boolean flag1 = (world.b(i, j, k) & 4) > 0;
 
             if (flag1 != flag) {
-                if (world.a(i, j + 1, k) == this.bc) {
+                if (world.a(i, j + 1, k) == this.bi) {
                     world.b(i, j + 1, k, (l ^ 4) + 8);
                 }
 
                 world.b(i, j, k, l ^ 4);
                 world.b(i, j - 1, k, i, j, k);
                 if (Math.random() < 0.5D) {
-                    world.a((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.door_open", 1.0F, world.m.nextFloat() * 0.1F + 0.9F);
+                    world.a((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.door_open", 1.0F, world.l.nextFloat() * 0.1F + 0.9F);
                 } else {
-                    world.a((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.door_close", 1.0F, world.m.nextFloat() * 0.1F + 0.9F);
+                    world.a((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.door_close", 1.0F, world.l.nextFloat() * 0.1F + 0.9F);
                 }
             }
         }
@@ -119,17 +115,17 @@ public class BlockDoor extends Block {
         int i1 = world.b(i, j, k);
 
         if ((i1 & 8) != 0) {
-            if (world.a(i, j - 1, k) != this.bc) {
+            if (world.a(i, j - 1, k) != this.bi) {
                 world.d(i, j, k, 0);
             }
 
-            if (l > 0 && Block.n[l].d()) {
+            if (l > 0 && Block.n[l].c()) {
                 this.b(world, i, j - 1, k, l);
             }
         } else {
             boolean flag = false;
 
-            if (world.a(i, j + 1, k) != this.bc) {
+            if (world.a(i, j + 1, k) != this.bi) {
                 world.d(i, j, k, 0);
                 flag = true;
             }
@@ -137,14 +133,14 @@ public class BlockDoor extends Block {
             if (!world.d(i, j - 1, k)) {
                 world.d(i, j, k, 0);
                 flag = true;
-                if (world.a(i, j + 1, k) == this.bc) {
+                if (world.a(i, j + 1, k) == this.bi) {
                     world.d(i, j + 1, k, 0);
                 }
             }
 
             if (flag) {
                 this.a_(world, i, j, k, i1);
-            } else if (l > 0 && Block.n[l].d()) {
+            } else if (l > 0 && Block.n[l].c()) {
                 boolean flag1 = world.n(i, j, k) || world.n(i, j + 1, k);
 
                 this.a(world, i, j, k, flag1);
@@ -153,7 +149,7 @@ public class BlockDoor extends Block {
     }
 
     public int a(int i, Random random) {
-        return (i & 8) != 0 ? 0 : (this.bn == Material.e ? Item.IRON_DOOR.aS : Item.WOOD_DOOR.aS);
+        return (i & 8) != 0 ? 0 : (this.bt == Material.e ? Item.IRON_DOOR.aW : Item.WOOD_DOOR.aW);
     }
 
     public MovingObjectPosition a(World world, int i, int j, int k, Vec3D vec3d, Vec3D vec3d1) {
