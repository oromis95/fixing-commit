@@ -4,12 +4,12 @@ public class EntityCreeper extends EntityMonster {
 
     int a;
     int b;
-    int ad = 30;
-    int ae = -1;
+    int c = 30;
+    int d = -1;
 
     public EntityCreeper(World world) {
         super(world);
-        this.aC = "/mob/creeper.png";
+        this.aF = "/mob/creeper.png";
     }
 
     public void a(NBTTagCompound nbttagcompound) {
@@ -20,55 +20,55 @@ public class EntityCreeper extends EntityMonster {
         super.b(nbttagcompound);
     }
 
-    protected void d_() {
+    protected void c() {
         this.b = this.a;
-        if (this.a > 0 && this.ae < 0) {
+        if (this.a > 0 && this.d < 0) {
             --this.a;
         }
 
-        if (this.ae >= 0) {
-            this.ae = 2;
+        if (this.d >= 0) {
+            this.d = 2;
         }
 
-        super.d_();
-        if (this.ae != 1) {
-            this.ae = -1;
+        super.c();
+        if (this.d != 1) {
+            this.d = -1;
         }
     }
 
-    protected String d() {
+    protected String e() {
         return "mob.creeper";
     }
 
-    protected String e() {
+    protected String f() {
         return "mob.creeperdeath";
     }
 
     public void f(Entity entity) {
         super.f(entity);
         if (entity instanceof EntitySkeleton) {
-            this.a(Item.GOLD_RECORD.aS + this.R.nextInt(2), 1);
+            this.a(Item.GOLD_RECORD.aW + this.V.nextInt(2), 1);
         }
     }
 
     protected void a(Entity entity, float f) {
-        if (this.ae <= 0 && f < 3.0F || this.ae > 0 && f < 7.0F) {
+        if (this.d <= 0 && f < 3.0F || this.d > 0 && f < 7.0F) {
             if (this.a == 0) {
-                this.h.a(this, "random.fuse", 1.0F, 0.5F);
+                this.l.a(this, "random.fuse", 1.0F, 0.5F);
             }
 
-            this.ae = 1;
+            this.d = 1;
             ++this.a;
-            if (this.a == this.ad) {
-                this.h.a(this, this.l, this.m, this.n, 3.0F);
-                this.j();
+            if (this.a == this.c) {
+                this.l.a(this, this.p, this.q, this.r, 3.0F);
+                this.l();
             }
 
-            this.ah = true;
+            this.ai = true;
         }
     }
 
     protected int g() {
-        return Item.SULPHUR.aS;
+        return Item.SULPHUR.aW;
     }
 }
