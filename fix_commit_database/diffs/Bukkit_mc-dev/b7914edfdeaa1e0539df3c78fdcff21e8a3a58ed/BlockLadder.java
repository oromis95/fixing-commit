@@ -31,14 +31,10 @@ public class BlockLadder extends Block {
         return super.d(world, i, j, k);
     }
 
-    public boolean b() {
+    public boolean a() {
         return false;
     }
 
-    public int a() {
-        return 8;
-    }
-
     public boolean a(World world, int i, int j, int k) {
         return world.d(i - 1, j, k) ? true : (world.d(i + 1, j, k) ? true : (world.d(i, j, k - 1) ? true : world.d(i, j, k + 1)));
     }
