@@ -11,28 +11,28 @@ public class WorldGenLiquids extends WorldGenerator {
     }
 
     public boolean a(World world, Random random, int i, int j, int k) {
-        if (world.a(i, j + 1, k) != Block.STONE.bc) {
+        if (world.a(i, j + 1, k) != Block.STONE.bi) {
             return false;
-        } else if (world.a(i, j - 1, k) != Block.STONE.bc) {
+        } else if (world.a(i, j - 1, k) != Block.STONE.bi) {
             return false;
-        } else if (world.a(i, j, k) != 0 && world.a(i, j, k) != Block.STONE.bc) {
+        } else if (world.a(i, j, k) != 0 && world.a(i, j, k) != Block.STONE.bi) {
             return false;
         } else {
             int l = 0;
 
-            if (world.a(i - 1, j, k) == Block.STONE.bc) {
+            if (world.a(i - 1, j, k) == Block.STONE.bi) {
                 ++l;
             }
 
-            if (world.a(i + 1, j, k) == Block.STONE.bc) {
+            if (world.a(i + 1, j, k) == Block.STONE.bi) {
                 ++l;
             }
 
-            if (world.a(i, j, k - 1) == Block.STONE.bc) {
+            if (world.a(i, j, k - 1) == Block.STONE.bi) {
                 ++l;
             }
 
-            if (world.a(i, j, k + 1) == Block.STONE.bc) {
+            if (world.a(i, j, k + 1) == Block.STONE.bi) {
                 ++l;
             }
 
@@ -56,6 +56,9 @@ public class WorldGenLiquids extends WorldGenerator {
 
             if (l == 3 && i1 == 1) {
                 world.d(i, j, k, this.a);
+                world.a = true;
+                Block.n[this.a].a(world, i, j, k, random);
+                world.a = false;
             }
 
             return true;
