@@ -13,11 +13,11 @@ public class BlockButton extends Block {
         return null;
     }
 
-    public int c() {
+    public int b() {
         return 20;
     }
 
-    public boolean b() {
+    public boolean a() {
         return false;
     }
 
@@ -140,20 +140,20 @@ public class BlockButton extends Block {
             world.b(i, j, k, i1 + j1);
             world.b(i, j, k, i, j, k);
             world.a((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.click", 0.3F, 0.6F);
-            world.g(i, j, k, this.bc);
+            world.g(i, j, k, this.bi);
             if (i1 == 1) {
-                world.g(i - 1, j, k, this.bc);
+                world.g(i - 1, j, k, this.bi);
             } else if (i1 == 2) {
-                world.g(i + 1, j, k, this.bc);
+                world.g(i + 1, j, k, this.bi);
             } else if (i1 == 3) {
-                world.g(i, j, k - 1, this.bc);
+                world.g(i, j, k - 1, this.bi);
             } else if (i1 == 4) {
-                world.g(i, j, k + 1, this.bc);
+                world.g(i, j, k + 1, this.bi);
             } else {
-                world.g(i, j - 1, k, this.bc);
+                world.g(i, j - 1, k, this.bi);
             }
 
-            world.h(i, j, k, this.bc);
+            world.h(i, j, k, this.bi);
             return true;
         }
     }
@@ -162,19 +162,19 @@ public class BlockButton extends Block {
         int l = world.b(i, j, k);
 
         if ((l & 8) > 0) {
-            world.g(i, j, k, this.bc);
+            world.g(i, j, k, this.bi);
             int i1 = l & 7;
 
             if (i1 == 1) {
-                world.g(i - 1, j, k, this.bc);
+                world.g(i - 1, j, k, this.bi);
             } else if (i1 == 2) {
-                world.g(i + 1, j, k, this.bc);
+                world.g(i + 1, j, k, this.bi);
             } else if (i1 == 3) {
-                world.g(i, j, k - 1, this.bc);
+                world.g(i, j, k - 1, this.bi);
             } else if (i1 == 4) {
-                world.g(i, j, k + 1, this.bc);
+                world.g(i, j, k + 1, this.bi);
             } else {
-                world.g(i, j - 1, k, this.bc);
+                world.g(i, j - 1, k, this.bi);
             }
         }
 
@@ -197,7 +197,7 @@ public class BlockButton extends Block {
         }
     }
 
-    public boolean d() {
+    public boolean c() {
         return true;
     }
 
@@ -206,19 +206,19 @@ public class BlockButton extends Block {
 
         if ((l & 8) != 0) {
             world.b(i, j, k, l & 7);
-            world.g(i, j, k, this.bc);
+            world.g(i, j, k, this.bi);
             int i1 = l & 7;
 
             if (i1 == 1) {
-                world.g(i - 1, j, k, this.bc);
+                world.g(i - 1, j, k, this.bi);
             } else if (i1 == 2) {
-                world.g(i + 1, j, k, this.bc);
+                world.g(i + 1, j, k, this.bi);
             } else if (i1 == 3) {
-                world.g(i, j, k - 1, this.bc);
+                world.g(i, j, k - 1, this.bi);
             } else if (i1 == 4) {
-                world.g(i, j, k + 1, this.bc);
+                world.g(i, j, k + 1, this.bi);
             } else {
-                world.g(i, j - 1, k, this.bc);
+                world.g(i, j - 1, k, this.bi);
             }
 
             world.a((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.click", 0.3F, 0.5F);
