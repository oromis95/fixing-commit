@@ -11,11 +11,11 @@ public class BlockSand extends Block {
     }
 
     public void e(World world, int i, int j, int k) {
-        world.h(i, j, k, this.bc);
+        world.h(i, j, k, this.bi);
     }
 
     public void b(World world, int i, int j, int k, int l) {
-        world.h(i, j, k, this.bc);
+        world.h(i, j, k, this.bi);
     }
 
     public void a(World world, int i, int j, int k, Random random) {
@@ -24,10 +24,10 @@ public class BlockSand extends Block {
 
     private void h(World world, int i, int j, int k) {
         if (g(world, i, j - 1, k) && j >= 0) {
-            EntityFallingSand entityfallingsand = new EntityFallingSand(world, (float) i + 0.5F, (float) j + 0.5F, (float) k + 0.5F, this.bc);
+            EntityFallingSand entityfallingsand = new EntityFallingSand(world, (float) i + 0.5F, (float) j + 0.5F, (float) k + 0.5F, this.bi);
 
             if (a) {
-                while (!entityfallingsand.B) {
+                while (!entityfallingsand.F) {
                     entityfallingsand.b_();
                 }
             } else {
@@ -36,7 +36,7 @@ public class BlockSand extends Block {
         }
     }
 
-    public int c() {
+    public int b() {
         return 3;
     }
 
@@ -45,10 +45,10 @@ public class BlockSand extends Block {
 
         if (l == 0) {
             return true;
-        } else if (l == Block.FIRE.bc) {
+        } else if (l == Block.FIRE.bi) {
             return true;
         } else {
-            Material material = Block.n[l].bn;
+            Material material = Block.n[l].bt;
 
             return material == Material.f ? true : material == Material.g;
         }
