@@ -15,7 +15,7 @@ public class BlockFlowing extends BlockFluids {
     private void i(World world, int i, int j, int k) {
         int l = world.b(i, j, k);
 
-        world.a(i, j, k, this.bc + 1, l);
+        world.a(i, j, k, this.bi + 1, l);
         world.b(i, j, k, i, j, k);
         world.f(i, j, k);
     }
@@ -49,15 +49,15 @@ public class BlockFlowing extends BlockFluids {
                 }
             }
 
-            if (this.a >= 2 && this.bn == Material.f) {
+            if (this.a >= 2 && this.bt == Material.f) {
                 if (world.d(i, j - 1, k)) {
                     i1 = 0;
-                } else if (world.c(i, j - 1, k) == this.bn && world.b(i, j, k) == 0) {
+                } else if (world.c(i, j - 1, k) == this.bt && world.b(i, j, k) == 0) {
                     i1 = 0;
                 }
             }
 
-            if (this.bn == Material.g && l < 8 && i1 < 8 && i1 > l && random.nextInt(4) != 0) {
+            if (this.bt == Material.g && l < 8 && i1 < 8 && i1 > l && random.nextInt(4) != 0) {
                 i1 = l;
                 flag = false;
             }
@@ -68,8 +68,8 @@ public class BlockFlowing extends BlockFluids {
                     world.d(i, j, k, 0);
                 } else {
                     world.b(i, j, k, i1);
-                    world.h(i, j, k, this.bc);
-                    world.g(i, j, k, this.bc);
+                    world.h(i, j, k, this.bi);
+                    world.g(i, j, k, this.bi);
                 }
             } else if (flag) {
                 this.i(world, i, j, k);
@@ -80,9 +80,9 @@ public class BlockFlowing extends BlockFluids {
 
         if (this.l(world, i, j - 1, k)) {
             if (l >= 8) {
-                world.b(i, j - 1, k, this.bc, l);
+                world.b(i, j - 1, k, this.bi, l);
             } else {
-                world.b(i, j - 1, k, this.bc, l + 8);
+                world.b(i, j - 1, k, this.bi, l + 8);
             }
         } else if (l >= 0 && (l == 0 || this.k(world, i, j - 1, k))) {
             boolean[] aboolean = this.j(world, i, j, k);
@@ -119,14 +119,14 @@ public class BlockFlowing extends BlockFluids {
             int i1 = world.a(i, j, k);
 
             if (i1 > 0) {
-                if (this.bn == Material.g) {
+                if (this.bt == Material.g) {
                     this.h(world, i, j, k);
                 } else {
                     Block.n[i1].a_(world, i, j, k, world.b(i, j, k));
                 }
             }
 
-            world.b(i, j, k, this.bc, l);
+            world.b(i, j, k, this.bi, l);
         }
     }
 
@@ -154,7 +154,7 @@ public class BlockFlowing extends BlockFluids {
                     ++i2;
                 }
 
-                if (!this.k(world, l1, j, i2) && (world.c(l1, j, i2) != this.bn || world.b(l1, j, i2) != 0)) {
+                if (!this.k(world, l1, j, i2) && (world.c(l1, j, i2) != this.bt || world.b(l1, j, i2) != 0)) {
                     if (!this.k(world, l1, j - 1, i2)) {
                         return l;
                     }
@@ -198,7 +198,7 @@ public class BlockFlowing extends BlockFluids {
                 ++j1;
             }
 
-            if (!this.k(world, i1, j, j1) && (world.c(i1, j, j1) != this.bn || world.b(i1, j, j1) != 0)) {
+            if (!this.k(world, i1, j, j1) && (world.c(i1, j, j1) != this.bt || world.b(i1, j, j1) != 0)) {
                 if (!this.k(world, i1, j - 1, j1)) {
                     this.c[l] = 0;
                 } else {
@@ -225,11 +225,11 @@ public class BlockFlowing extends BlockFluids {
     private boolean k(World world, int i, int j, int k) {
         int l = world.a(i, j, k);
 
-        if (l != Block.WOODEN_DOOR.bc && l != Block.IRON_DOOR_BLOCK.bc && l != Block.SIGN_POST.bc && l != Block.LADDER.bc && l != Block.SUGAR_CANE_BLOCK.bc) {
+        if (l != Block.WOODEN_DOOR.bi && l != Block.IRON_DOOR_BLOCK.bi && l != Block.SIGN_POST.bi && l != Block.LADDER.bi && l != Block.SUGAR_CANE_BLOCK.bi) {
             if (l == 0) {
                 return false;
             } else {
-                Material material = Block.n[l].bn;
+                Material material = Block.n[l].bt;
 
                 return material.a();
             }
@@ -259,13 +259,13 @@ public class BlockFlowing extends BlockFluids {
     private boolean l(World world, int i, int j, int k) {
         Material material = world.c(i, j, k);
 
-        return material == this.bn ? false : (material == Material.g ? false : !this.k(world, i, j, k));
+        return material == this.bt ? false : (material == Material.g ? false : !this.k(world, i, j, k));
     }
 
     public void e(World world, int i, int j, int k) {
         super.e(world, i, j, k);
-        if (world.a(i, j, k) == this.bc) {
-            world.h(i, j, k, this.bc);
+        if (world.a(i, j, k) == this.bi) {
+            world.h(i, j, k, this.bi);
         }
     }
 }
