@@ -6,7 +6,7 @@ public class EntityCow extends EntityAnimal {
 
     public EntityCow(World world) {
         super(world);
-        this.aC = "/mob/cow.png";
+        this.aF = "/mob/cow.png";
         this.a(0.9F, 1.3F);
     }
 
@@ -18,23 +18,23 @@ public class EntityCow extends EntityAnimal {
         super.b(nbttagcompound);
     }
 
-    protected String c() {
+    protected String d() {
         return "mob.cow";
     }
 
-    protected String d() {
+    protected String e() {
         return "mob.cowhurt";
     }
 
-    protected String e() {
+    protected String f() {
         return "mob.cowhurt";
     }
 
-    protected float f() {
+    protected float h() {
         return 0.4F;
     }
 
     protected int g() {
-        return Item.LEATHER.aS;
+        return Item.LEATHER.aW;
     }
 }
