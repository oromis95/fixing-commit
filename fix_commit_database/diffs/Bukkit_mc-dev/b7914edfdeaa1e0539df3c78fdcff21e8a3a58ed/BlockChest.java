@@ -8,29 +8,29 @@ public class BlockChest extends BlockContainer {
 
     protected BlockChest(int i) {
         super(i, Material.c);
-        this.bb = 26;
+        this.bh = 26;
     }
 
     public int a(int i) {
-        return i == 1 ? this.bb - 1 : (i == 0 ? this.bb - 1 : (i == 3 ? this.bb + 1 : this.bb));
+        return i == 1 ? this.bh - 1 : (i == 0 ? this.bh - 1 : (i == 3 ? this.bh + 1 : this.bh));
     }
 
     public boolean a(World world, int i, int j, int k) {
         int l = 0;
 
-        if (world.a(i - 1, j, k) == this.bc) {
+        if (world.a(i - 1, j, k) == this.bi) {
             ++l;
         }
 
-        if (world.a(i + 1, j, k) == this.bc) {
+        if (world.a(i + 1, j, k) == this.bi) {
             ++l;
         }
 
-        if (world.a(i, j, k - 1) == this.bc) {
+        if (world.a(i, j, k - 1) == this.bi) {
             ++l;
         }
 
-        if (world.a(i, j, k + 1) == this.bc) {
+        if (world.a(i, j, k + 1) == this.bi) {
             ++l;
         }
 
@@ -38,7 +38,7 @@ public class BlockChest extends BlockContainer {
     }
 
     private boolean g(World world, int i, int j, int k) {
-        return world.a(i, j, k) != this.bc ? false : (world.a(i - 1, j, k) == this.bc ? true : (world.a(i + 1, j, k) == this.bc ? true : (world.a(i, j, k - 1) == this.bc ? true : world.a(i, j, k + 1) == this.bc)));
+        return world.a(i, j, k) != this.bi ? false : (world.a(i - 1, j, k) == this.bi ? true : (world.a(i + 1, j, k) == this.bi ? true : (world.a(i, j, k - 1) == this.bi ? true : world.a(i, j, k + 1) == this.bi)));
     }
 
     public void b(World world, int i, int j, int k) {
@@ -63,9 +63,9 @@ public class BlockChest extends BlockContainer {
                     EntityItem entityitem = new EntityItem(world, (double) ((float) i + f), (double) ((float) j + f1), (double) ((float) k + f2), new ItemStack(itemstack.c, i1, itemstack.d));
                     float f3 = 0.05F;
 
-                    entityitem.o = (double) ((float) this.a.nextGaussian() * f3);
-                    entityitem.p = (double) ((float) this.a.nextGaussian() * f3 + 0.2F);
-                    entityitem.q = (double) ((float) this.a.nextGaussian() * f3);
+                    entityitem.s = (double) ((float) this.a.nextGaussian() * f3);
+                    entityitem.t = (double) ((float) this.a.nextGaussian() * f3 + 0.2F);
+                    entityitem.u = (double) ((float) this.a.nextGaussian() * f3);
                     world.a((Entity) entityitem);
                 }
             }
@@ -79,28 +79,28 @@ public class BlockChest extends BlockContainer {
 
         if (world.d(i, j + 1, k)) {
             return true;
-        } else if (world.a(i - 1, j, k) == this.bc && world.d(i - 1, j + 1, k)) {
+        } else if (world.a(i - 1, j, k) == this.bi && world.d(i - 1, j + 1, k)) {
             return true;
-        } else if (world.a(i + 1, j, k) == this.bc && world.d(i + 1, j + 1, k)) {
+        } else if (world.a(i + 1, j, k) == this.bi && world.d(i + 1, j + 1, k)) {
             return true;
-        } else if (world.a(i, j, k - 1) == this.bc && world.d(i, j + 1, k - 1)) {
+        } else if (world.a(i, j, k - 1) == this.bi && world.d(i, j + 1, k - 1)) {
             return true;
-        } else if (world.a(i, j, k + 1) == this.bc && world.d(i, j + 1, k + 1)) {
+        } else if (world.a(i, j, k + 1) == this.bi && world.d(i, j + 1, k + 1)) {
             return true;
         } else {
-            if (world.a(i - 1, j, k) == this.bc) {
+            if (world.a(i - 1, j, k) == this.bi) {
                 object = new InventoryLargeChest("Large chest", (TileEntityChest) world.k(i - 1, j, k), (IInventory) object);
             }
 
-            if (world.a(i + 1, j, k) == this.bc) {
+            if (world.a(i + 1, j, k) == this.bi) {
                 object = new InventoryLargeChest("Large chest", (IInventory) object, (TileEntityChest) world.k(i + 1, j, k));
             }
 
-            if (world.a(i, j, k - 1) == this.bc) {
+            if (world.a(i, j, k - 1) == this.bi) {
                 object = new InventoryLargeChest("Large chest", (TileEntityChest) world.k(i, j, k - 1), (IInventory) object);
             }
 
-            if (world.a(i, j, k + 1) == this.bc) {
+            if (world.a(i, j, k + 1) == this.bi) {
                 object = new InventoryLargeChest("Large chest", (IInventory) object, (TileEntityChest) world.k(i, j, k + 1));
             }
 
