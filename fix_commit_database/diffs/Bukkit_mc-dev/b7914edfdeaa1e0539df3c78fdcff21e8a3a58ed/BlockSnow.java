@@ -14,14 +14,14 @@ public class BlockSnow extends Block {
         return null;
     }
 
-    public boolean b() {
+    public boolean a() {
         return false;
     }
 
     public boolean a(World world, int i, int j, int k) {
         int l = world.a(i, j - 1, k);
 
-        return l != 0 && Block.n[l].b() ? world.c(i, j - 1, k).c() : false;
+        return l != 0 && Block.n[l].a() ? world.c(i, j - 1, k).c() : false;
     }
 
     public void b(World world, int i, int j, int k, int l) {
@@ -39,7 +39,7 @@ public class BlockSnow extends Block {
     }
 
     public int a(int i, Random random) {
-        return Item.SNOW_BALL.aS;
+        return Item.SNOW_BALL.aW;
     }
 
     public int a(Random random) {
@@ -56,6 +56,6 @@ public class BlockSnow extends Block {
     public boolean a(IBlockAccess iblockaccess, int i, int j, int k, int l) {
         Material material = iblockaccess.c(i, j, k);
 
-        return l == 1 ? true : (material == this.bn ? false : super.a(iblockaccess, i, j, k, l));
+        return l == 1 ? true : (material == this.bt ? false : super.a(iblockaccess, i, j, k, l));
     }
 }
