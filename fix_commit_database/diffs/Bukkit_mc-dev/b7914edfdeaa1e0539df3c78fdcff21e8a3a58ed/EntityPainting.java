@@ -5,26 +5,26 @@ import java.util.List;
 
 public class EntityPainting extends Entity {
 
-    private int ad;
+    private int c;
     public int a;
-    private int ae;
-    private int af;
-    private int ag;
+    private int d;
+    private int e;
+    private int f;
     public EnumArt b;
 
     public EntityPainting(World world) {
         super(world);
-        this.ad = 0;
+        this.c = 0;
         this.a = 0;
-        this.C = 0.0F;
+        this.G = 0.0F;
         this.a(0.5F, 0.5F);
     }
 
     public EntityPainting(World world, int i, int j, int k, int l) {
         this(world);
-        this.ae = i;
-        this.af = j;
-        this.ag = k;
+        this.d = i;
+        this.e = j;
+        this.f = k;
         ArrayList arraylist = new ArrayList();
         EnumArt[] aenumart = EnumArt.values();
         int i1 = aenumart.length;
@@ -34,13 +34,13 @@ public class EntityPainting extends Entity {
 
             this.b = enumart;
             this.a(l);
-            if (this.b()) {
+            if (this.c()) {
                 arraylist.add(enumart);
             }
         }
 
         if (arraylist.size() > 0) {
-            this.b = (EnumArt) arraylist.get(this.R.nextInt(arraylist.size()));
+            this.b = (EnumArt) arraylist.get(this.V.nextInt(arraylist.size()));
         }
 
         this.a(l);
@@ -48,7 +48,7 @@ public class EntityPainting extends Entity {
 
     public void a(int i) {
         this.a = i;
-        this.t = this.r = (float) (i * 90);
+        this.x = this.v = (float) (i * 90);
         float f = (float) this.b.z;
         float f1 = (float) this.b.A;
         float f2 = (float) this.b.z;
@@ -62,9 +62,9 @@ public class EntityPainting extends Entity {
         f /= 32.0F;
         f1 /= 32.0F;
         f2 /= 32.0F;
-        float f3 = (float) this.ae + 0.5F;
-        float f4 = (float) this.af + 0.5F;
-        float f5 = (float) this.ag + 0.5F;
+        float f3 = (float) this.d + 0.5F;
+        float f4 = (float) this.e + 0.5F;
+        float f5 = (float) this.f + 0.5F;
         float f6 = 0.5625F;
 
         if (i == 0) {
@@ -103,7 +103,7 @@ public class EntityPainting extends Entity {
         this.a((double) f3, (double) f4, (double) f5);
         float f7 = -0.00625F;
 
-        this.v.c((double) (f3 - f - f7), (double) (f4 - f1 - f7), (double) (f5 - f2 - f7), (double) (f3 + f + f7), (double) (f4 + f1 + f7), (double) (f5 + f2 + f7));
+        this.z.c((double) (f3 - f - f7), (double) (f4 - f1 - f7), (double) (f5 - f2 - f7), (double) (f3 + f + f7), (double) (f4 + f1 + f7), (double) (f5 + f2 + f7));
     }
 
     private float c(int i) {
@@ -111,40 +111,40 @@ public class EntityPainting extends Entity {
     }
 
     public void b_() {
-        if (this.ad++ == 100 && !this.b()) {
-            this.ad = 0;
-            this.j();
-            this.h.a((Entity) (new EntityItem(this.h, this.l, this.m, this.n, new ItemStack(Item.PAINTING))));
+        if (this.c++ == 100 && !this.c()) {
+            this.c = 0;
+            this.l();
+            this.l.a((Entity) (new EntityItem(this.l, this.p, this.q, this.r, new ItemStack(Item.PAINTING))));
         }
     }
 
-    public boolean b() {
-        if (this.h.a((Entity) this, this.v).size() > 0) {
+    public boolean c() {
+        if (this.l.a((Entity) this, this.z).size() > 0) {
             return false;
         } else {
             int i = this.b.z / 16;
             int j = this.b.A / 16;
-            int k = this.ae;
-            int l = this.af;
-            int i1 = this.ag;
+            int k = this.d;
+            int l = this.e;
+            int i1 = this.f;
 
             if (this.a == 0) {
-                k = MathHelper.b(this.l - (double) ((float) this.b.z / 32.0F));
+                k = MathHelper.b(this.p - (double) ((float) this.b.z / 32.0F));
             }
 
             if (this.a == 1) {
-                i1 = MathHelper.b(this.n - (double) ((float) this.b.z / 32.0F));
+                i1 = MathHelper.b(this.r - (double) ((float) this.b.z / 32.0F));
             }
 
             if (this.a == 2) {
-                k = MathHelper.b(this.l - (double) ((float) this.b.z / 32.0F));
+                k = MathHelper.b(this.p - (double) ((float) this.b.z / 32.0F));
             }
 
             if (this.a == 3) {
-                i1 = MathHelper.b(this.n - (double) ((float) this.b.z / 32.0F));
+                i1 = MathHelper.b(this.r - (double) ((float) this.b.z / 32.0F));
             }
 
-            l = MathHelper.b(this.m - (double) ((float) this.b.A / 32.0F));
+            l = MathHelper.b(this.q - (double) ((float) this.b.A / 32.0F));
 
             int j1;
 
@@ -153,9 +153,9 @@ public class EntityPainting extends Entity {
                     Material material;
 
                     if (this.a != 0 && this.a != 2) {
-                        material = this.h.c(this.ae, l + j1, i1 + k1);
+                        material = this.l.c(this.d, l + j1, i1 + k1);
                     } else {
-                        material = this.h.c(k + k1, l + j1, this.ag);
+                        material = this.l.c(k + k1, l + j1, this.f);
                     }
 
                     if (!material.a()) {
@@ -164,7 +164,7 @@ public class EntityPainting extends Entity {
                 }
             }
 
-            List list = this.h.b((Entity) this, this.v);
+            List list = this.l.b((Entity) this, this.z);
 
             for (j1 = 0; j1 < list.size(); ++j1) {
                 if (list.get(j1) instanceof EntityPainting) {
@@ -181,24 +181,24 @@ public class EntityPainting extends Entity {
     }
 
     public boolean a(Entity entity, int i) {
-        this.j();
-        this.h.a((Entity) (new EntityItem(this.h, this.l, this.m, this.n, new ItemStack(Item.PAINTING))));
+        this.l();
+        this.l.a((Entity) (new EntityItem(this.l, this.p, this.q, this.r, new ItemStack(Item.PAINTING))));
         return true;
     }
 
     public void a(NBTTagCompound nbttagcompound) {
         nbttagcompound.a("Dir", (byte) this.a);
         nbttagcompound.a("Motive", this.b.y);
-        nbttagcompound.a("TileX", this.ae);
-        nbttagcompound.a("TileY", this.af);
-        nbttagcompound.a("TileZ", this.ag);
+        nbttagcompound.a("TileX", this.d);
+        nbttagcompound.a("TileY", this.e);
+        nbttagcompound.a("TileZ", this.f);
     }
 
     public void b(NBTTagCompound nbttagcompound) {
         this.a = nbttagcompound.b("Dir");
-        this.ae = nbttagcompound.d("TileX");
-        this.af = nbttagcompound.d("TileY");
-        this.ag = nbttagcompound.d("TileZ");
+        this.d = nbttagcompound.d("TileX");
+        this.e = nbttagcompound.d("TileY");
+        this.f = nbttagcompound.d("TileZ");
         String s = nbttagcompound.h("Motive");
         EnumArt[] aenumart = EnumArt.values();
         int i = aenumart.length;
