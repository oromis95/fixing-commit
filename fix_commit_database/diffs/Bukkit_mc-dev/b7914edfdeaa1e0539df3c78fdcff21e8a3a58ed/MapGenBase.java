@@ -9,16 +9,16 @@ public class MapGenBase {
 
     public MapGenBase() {}
 
-    public void a(ChunkProviderGenerate chunkprovidergenerate, World world, int i, int j, byte[] abyte) {
+    public void a(IChunkProvider ichunkprovider, World world, int i, int j, byte[] abyte) {
         int k = this.a;
 
-        this.b.setSeed(world.t);
+        this.b.setSeed(world.u);
         long l = this.b.nextLong() / 2L * 2L + 1L;
         long i1 = this.b.nextLong() / 2L * 2L + 1L;
 
         for (int j1 = i - k; j1 <= i + k; ++j1) {
             for (int k1 = j - k; k1 <= j + k; ++k1) {
-                this.b.setSeed((long) j1 * l + (long) k1 * i1 ^ world.t);
+                this.b.setSeed((long) j1 * l + (long) k1 * i1 ^ world.u);
                 this.a(world, j1, k1, i, j, abyte);
             }
         }
