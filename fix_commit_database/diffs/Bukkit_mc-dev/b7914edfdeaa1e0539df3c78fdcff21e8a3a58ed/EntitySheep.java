@@ -6,21 +6,21 @@ public class EntitySheep extends EntityAnimal {
 
     public EntitySheep(World world) {
         super(world);
-        this.aC = "/mob/sheep.png";
+        this.aF = "/mob/sheep.png";
         this.a(0.9F, 1.3F);
     }
 
     public boolean a(Entity entity, int i) {
         if (!this.a && entity instanceof EntityLiving) {
             this.a = true;
-            int j = 1 + this.R.nextInt(3);
+            int j = 1 + this.V.nextInt(3);
 
             for (int k = 0; k < j; ++k) {
-                EntityItem entityitem = this.a(Block.WOOL.bc, 1, 1.0F);
+                EntityItem entityitem = this.a(Block.WOOL.bi, 1, 1.0F);
 
-                entityitem.p += (double) (this.R.nextFloat() * 0.05F);
-                entityitem.o += (double) ((this.R.nextFloat() - this.R.nextFloat()) * 0.1F);
-                entityitem.q += (double) ((this.R.nextFloat() - this.R.nextFloat()) * 0.1F);
+                entityitem.t += (double) (this.V.nextFloat() * 0.05F);
+                entityitem.s += (double) ((this.V.nextFloat() - this.V.nextFloat()) * 0.1F);
+                entityitem.u += (double) ((this.V.nextFloat() - this.V.nextFloat()) * 0.1F);
             }
         }
 
@@ -37,15 +37,15 @@ public class EntitySheep extends EntityAnimal {
         this.a = nbttagcompound.l("Sheared");
     }
 
-    protected String c() {
+    protected String d() {
         return "mob.sheep";
     }
 
-    protected String d() {
+    protected String e() {
         return "mob.sheep";
     }
 
-    protected String e() {
+    protected String f() {
         return "mob.sheep";
     }
 }
