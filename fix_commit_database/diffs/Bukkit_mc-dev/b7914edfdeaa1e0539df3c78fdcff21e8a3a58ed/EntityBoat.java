@@ -6,55 +6,55 @@ public class EntityBoat extends Entity {
 
     public int a = 0;
     public int b = 0;
-    public int ad = 1;
+    public int c = 1;
 
     public EntityBoat(World world) {
         super(world);
-        this.e = true;
+        this.i = true;
         this.a(1.5F, 0.6F);
-        this.C = this.E / 2.0F;
-        this.H = false;
+        this.G = this.I / 2.0F;
+        this.L = false;
     }
 
     public AxisAlignedBB d(Entity entity) {
-        return entity.v;
+        return entity.z;
     }
 
-    public AxisAlignedBB n() {
-        return this.v;
+    public AxisAlignedBB q() {
+        return this.z;
     }
 
-    public boolean r() {
+    public boolean u() {
         return true;
     }
 
-    public double h() {
-        return (double) this.E * 0.0D - 0.30000001192092896D;
+    public double j() {
+        return (double) this.I * 0.0D - 0.30000001192092896D;
     }
 
     public boolean a(Entity entity, int i) {
-        this.ad = -this.ad;
+        this.c = -this.c;
         this.b = 10;
         this.a += i * 10;
         if (this.a > 40) {
             int j;
 
             for (j = 0; j < 3; ++j) {
-                this.a(Block.WOOD.bc, 1, 0.0F);
+                this.a(Block.WOOD.bi, 1, 0.0F);
             }
 
             for (j = 0; j < 2; ++j) {
-                this.a(Item.STICK.aS, 1, 0.0F);
+                this.a(Item.STICK.aW, 1, 0.0F);
             }
 
-            this.j();
+            this.l();
         }
 
         return true;
     }
 
     public boolean c_() {
-        return !this.B;
+        return !this.F;
     }
 
     public void b_() {
@@ -67,103 +67,103 @@ public class EntityBoat extends Entity {
             --this.a;
         }
 
-        this.i = this.l;
-        this.j = this.m;
-        this.k = this.n;
+        this.m = this.p;
+        this.n = this.q;
+        this.o = this.r;
         byte b0 = 5;
         double d0 = 0.0D;
 
         for (int i = 0; i < b0; ++i) {
-            double d1 = this.v.b + (this.v.e - this.v.b) * (double) (i + 0) / (double) b0 - 0.125D;
-            double d2 = this.v.b + (this.v.e - this.v.b) * (double) (i + 1) / (double) b0 - 0.125D;
-            AxisAlignedBB axisalignedbb = AxisAlignedBB.b(this.v.a, d1, this.v.c, this.v.d, d2, this.v.f);
+            double d1 = this.z.b + (this.z.e - this.z.b) * (double) (i + 0) / (double) b0 - 0.125D;
+            double d2 = this.z.b + (this.z.e - this.z.b) * (double) (i + 1) / (double) b0 - 0.125D;
+            AxisAlignedBB axisalignedbb = AxisAlignedBB.b(this.z.a, d1, this.z.c, this.z.d, d2, this.z.f);
 
-            if (this.h.b(axisalignedbb, Material.f)) {
+            if (this.l.b(axisalignedbb, Material.f)) {
                 d0 += 1.0D / (double) b0;
             }
         }
 
         double d3 = d0 * 2.0D - 1.0D;
 
-        this.p += 0.03999999910593033D * d3;
-        if (this.f != null) {
-            this.o += this.f.o * 0.2D;
-            this.q += this.f.q * 0.2D;
+        this.t += 0.03999999910593033D * d3;
+        if (this.j != null) {
+            this.s += this.j.s * 0.2D;
+            this.u += this.j.u * 0.2D;
         }
 
         double d4 = 0.4D;
 
-        if (this.o < -d4) {
-            this.o = -d4;
+        if (this.s < -d4) {
+            this.s = -d4;
         }
 
-        if (this.o > d4) {
-            this.o = d4;
+        if (this.s > d4) {
+            this.s = d4;
         }
 
-        if (this.q < -d4) {
-            this.q = -d4;
+        if (this.u < -d4) {
+            this.u = -d4;
         }
 
-        if (this.q > d4) {
-            this.q = d4;
+        if (this.u > d4) {
+            this.u = d4;
         }
 
-        if (this.w) {
-            this.o *= 0.5D;
-            this.p *= 0.5D;
-            this.q *= 0.5D;
+        if (this.A) {
+            this.s *= 0.5D;
+            this.t *= 0.5D;
+            this.u *= 0.5D;
         }
 
-        this.c(this.o, this.p, this.q);
-        double d5 = Math.sqrt(this.o * this.o + this.q * this.q);
+        this.c(this.s, this.t, this.u);
+        double d5 = Math.sqrt(this.s * this.s + this.u * this.u);
         double d6;
         double d7;
 
         if (d5 > 0.15D) {
-            d6 = Math.cos((double) this.r * 3.141592653589793D / 180.0D);
-            d7 = Math.sin((double) this.r * 3.141592653589793D / 180.0D);
+            d6 = Math.cos((double) this.v * 3.141592653589793D / 180.0D);
+            d7 = Math.sin((double) this.v * 3.141592653589793D / 180.0D);
 
             for (int j = 0; (double) j < 1.0D + d5 * 60.0D; ++j) {
-                double d8 = (double) (this.R.nextFloat() * 2.0F - 1.0F);
-                double d9 = (double) (this.R.nextInt(2) * 2 - 1) * 0.7D;
+                double d8 = (double) (this.V.nextFloat() * 2.0F - 1.0F);
+                double d9 = (double) (this.V.nextInt(2) * 2 - 1) * 0.7D;
                 double d10;
                 double d11;
 
-                if (this.R.nextBoolean()) {
-                    d10 = this.l - d6 * d8 * 0.8D + d7 * d9;
-                    d11 = this.n - d7 * d8 * 0.8D - d6 * d9;
-                    this.h.a("splash", d10, this.m - 0.125D, d11, this.o, this.p, this.q);
+                if (this.V.nextBoolean()) {
+                    d10 = this.p - d6 * d8 * 0.8D + d7 * d9;
+                    d11 = this.r - d7 * d8 * 0.8D - d6 * d9;
+                    this.l.a("splash", d10, this.q - 0.125D, d11, this.s, this.t, this.u);
                 } else {
-                    d10 = this.l + d6 + d7 * d8 * 0.7D;
-                    d11 = this.n + d7 - d6 * d8 * 0.7D;
-                    this.h.a("splash", d10, this.m - 0.125D, d11, this.o, this.p, this.q);
+                    d10 = this.p + d6 + d7 * d8 * 0.7D;
+                    d11 = this.r + d7 - d6 * d8 * 0.7D;
+                    this.l.a("splash", d10, this.q - 0.125D, d11, this.s, this.t, this.u);
                 }
             }
         }
 
-        if (this.x && d5 > 0.15D) {
-            this.j();
+        if (this.B && d5 > 0.15D) {
+            this.l();
 
             int k;
 
             for (k = 0; k < 3; ++k) {
-                this.a(Block.WOOD.bc, 1, 0.0F);
+                this.a(Block.WOOD.bi, 1, 0.0F);
             }
 
             for (k = 0; k < 2; ++k) {
-                this.a(Item.STICK.aS, 1, 0.0F);
+                this.a(Item.STICK.aW, 1, 0.0F);
             }
         } else {
-            this.o *= 0.9900000095367432D;
-            this.p *= 0.949999988079071D;
-            this.q *= 0.9900000095367432D;
+            this.s *= 0.9900000095367432D;
+            this.t *= 0.949999988079071D;
+            this.u *= 0.9900000095367432D;
         }
 
-        this.s = 0.0F;
-        d6 = (double) this.r;
-        d7 = this.i - this.l;
-        double d12 = this.k - this.n;
+        this.w = 0.0F;
+        d6 = (double) this.v;
+        d7 = this.m - this.p;
+        double d12 = this.o - this.r;
 
         if (d7 * d7 + d12 * d12 > 0.0010D) {
             d6 = (double) ((float) (Math.atan2(d12, d7) * 180.0D / 3.141592653589793D));
@@ -171,7 +171,7 @@ public class EntityBoat extends Entity {
 
         double d13;
 
-        for (d13 = d6 - (double) this.r; d13 >= 180.0D; d13 -= 360.0D) {
+        for (d13 = d6 - (double) this.v; d13 >= 180.0D; d13 -= 360.0D) {
             ;
         }
 
@@ -187,30 +187,30 @@ public class EntityBoat extends Entity {
             d13 = -20.0D;
         }
 
-        this.r = (float) ((double) this.r + d13);
-        this.b(this.r, this.s);
-        List list = this.h.b((Entity) this, this.v.b(0.20000000298023224D, 0.0D, 0.20000000298023224D));
+        this.v = (float) ((double) this.v + d13);
+        this.b(this.v, this.w);
+        List list = this.l.b((Entity) this, this.z.b(0.20000000298023224D, 0.0D, 0.20000000298023224D));
 
         if (list != null && list.size() > 0) {
             for (int l = 0; l < list.size(); ++l) {
                 Entity entity = (Entity) list.get(l);
 
-                if (entity != this.f && entity.r() && entity instanceof EntityBoat) {
+                if (entity != this.j && entity.u() && entity instanceof EntityBoat) {
                     entity.c((Entity) this);
                 }
             }
         }
 
-        if (this.f != null && this.f.B) {
-            this.f = null;
+        if (this.j != null && this.j.F) {
+            this.j = null;
         }
     }
 
-    protected void w() {
-        double d0 = Math.cos((double) this.r * 3.141592653589793D / 180.0D) * 0.4D;
-        double d1 = Math.sin((double) this.r * 3.141592653589793D / 180.0D) * 0.4D;
+    protected void z() {
+        double d0 = Math.cos((double) this.v * 3.141592653589793D / 180.0D) * 0.4D;
+        double d1 = Math.sin((double) this.v * 3.141592653589793D / 180.0D) * 0.4D;
 
-        this.f.a(this.l + d0, this.m + this.h() + this.f.x(), this.n + d1);
+        this.j.a(this.p + d0, this.q + this.j() + this.j.A(), this.r + d1);
     }
 
     protected void a(NBTTagCompound nbttagcompound) {}
