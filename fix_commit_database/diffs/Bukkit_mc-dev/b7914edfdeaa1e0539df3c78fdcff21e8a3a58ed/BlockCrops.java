@@ -6,7 +6,7 @@ public class BlockCrops extends BlockFlower {
 
     protected BlockCrops(int i, int j) {
         super(i, j);
-        this.bb = j;
+        this.bh = j;
         this.a(true);
         float f = 0.5F;
 
@@ -14,7 +14,7 @@ public class BlockCrops extends BlockFlower {
     }
 
     protected boolean b(int i) {
-        return i == Block.SOIL.bc;
+        return i == Block.SOIL.bi;
     }
 
     public void a(World world, int i, int j, int k, Random random) {
@@ -43,16 +43,16 @@ public class BlockCrops extends BlockFlower {
         int i2 = world.a(i + 1, j, k - 1);
         int j2 = world.a(i + 1, j, k + 1);
         int k2 = world.a(i - 1, j, k + 1);
-        boolean flag = j1 == this.bc || k1 == this.bc;
-        boolean flag1 = l == this.bc || i1 == this.bc;
-        boolean flag2 = l1 == this.bc || i2 == this.bc || j2 == this.bc || k2 == this.bc;
+        boolean flag = j1 == this.bi || k1 == this.bi;
+        boolean flag1 = l == this.bi || i1 == this.bi;
+        boolean flag2 = l1 == this.bi || i2 == this.bi || j2 == this.bi || k2 == this.bi;
 
         for (int l2 = i - 1; l2 <= i + 1; ++l2) {
             for (int i3 = k - 1; i3 <= k + 1; ++i3) {
                 int j3 = world.a(l2, j - 1, i3);
                 float f1 = 0.0F;
 
-                if (j3 == Block.SOIL.bc) {
+                if (j3 == Block.SOIL.bi) {
                     f1 = 1.0F;
                     if (world.b(l2, j - 1, i3) > 0) {
                         f1 = 3.0F;
@@ -74,30 +74,25 @@ public class BlockCrops extends BlockFlower {
         return f;
     }
 
-    public int a() {
-        return 6;
-    }
-
     public void a(World world, int i, int j, int k, int l) {
         super.a(world, i, j, k, l);
 
         for (int i1 = 0; i1 < 3; ++i1) {
-            if (world.m.nextInt(15) <= l) {
+            if (world.l.nextInt(15) <= l) {
                 float f = 0.7F;
-                float f1 = world.m.nextFloat() * f + (1.0F - f) * 0.5F;
-                float f2 = world.m.nextFloat() * f + (1.0F - f) * 0.5F;
-                float f3 = world.m.nextFloat() * f + (1.0F - f) * 0.5F;
+                float f1 = world.l.nextFloat() * f + (1.0F - f) * 0.5F;
+                float f2 = world.l.nextFloat() * f + (1.0F - f) * 0.5F;
+                float f3 = world.l.nextFloat() * f + (1.0F - f) * 0.5F;
                 EntityItem entityitem = new EntityItem(world, (double) ((float) i + f1), (double) ((float) j + f2), (double) ((float) k + f3), new ItemStack(Item.SEEDS));
 
-                entityitem.ad = 10;
+                entityitem.c = 10;
                 world.a((Entity) entityitem);
             }
         }
     }
 
     public int a(int i, Random random) {
-        System.out.println("Get resource: " + i);
-        return i == 7 ? Item.WHEAT.aS : -1;
+        return i == 7 ? Item.WHEAT.aW : -1;
     }
 
     public int a(Random random) {
