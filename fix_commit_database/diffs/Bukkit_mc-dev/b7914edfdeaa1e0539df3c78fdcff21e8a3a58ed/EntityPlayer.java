@@ -9,26 +9,32 @@ public class EntityPlayer extends EntityHuman {
 
     public NetServerHandler a;
     public MinecraftServer b;
-    public ItemInWorldManager ad;
-    public double ae;
-    public double af;
-    public List ag = new LinkedList();
-    public Set ah = new HashSet();
-    public double ai;
+    public ItemInWorldManager c;
+    public double d;
+    public double e;
+    public List f = new LinkedList();
+    public Set ai = new HashSet();
+    public double aj;
 
     public EntityPlayer(MinecraftServer minecraftserver, World world, String s, ItemInWorldManager iteminworldmanager) {
         super(world);
-        int i = world.n + this.R.nextInt(20) - 10;
-        int j = world.p + this.R.nextInt(20) - 10;
-        int k = world.d(i, j);
+        int i = world.m;
+        int j = world.o;
+        int k = world.n;
+
+        if (!world.q.c) {
+            i += this.V.nextInt(20) - 10;
+            k = world.e(i, j);
+            j += this.V.nextInt(20) - 10;
+        }
 
         this.c((double) i + 0.5D, (double) k, (double) j + 0.5D, 0.0F, 0.0F);
         this.b = minecraftserver;
-        this.N = 0.0F;
+        this.R = 0.0F;
         iteminworldmanager.a = this;
-        this.aq = s;
-        this.ad = iteminworldmanager;
-        this.C = 0.0F;
+        this.ar = s;
+        this.c = iteminworldmanager;
+        this.G = 0.0F;
     }
 
     public void b_() {}
@@ -41,13 +47,13 @@ public class EntityPlayer extends EntityHuman {
 
     public void a(int i) {}
 
-    public void i() {
+    public void k() {
         super.b_();
         ChunkCoordIntPair chunkcoordintpair = null;
         double d0 = 0.0D;
 
-        for (int i = 0; i < this.ag.size(); ++i) {
-            ChunkCoordIntPair chunkcoordintpair1 = (ChunkCoordIntPair) this.ag.get(i);
+        for (int i = 0; i < this.f.size(); ++i) {
+            ChunkCoordIntPair chunkcoordintpair1 = (ChunkCoordIntPair) this.f.get(i);
             double d1 = chunkcoordintpair1.a(this);
 
             if (i == 0 || d1 < d0) {
@@ -68,7 +74,7 @@ public class EntityPlayer extends EntityHuman {
             }
 
             if (flag) {
-                this.ag.remove(chunkcoordintpair);
+                this.f.remove(chunkcoordintpair);
                 this.a.b((Packet) (new Packet51MapChunk(chunkcoordintpair.a * 16, 0, chunkcoordintpair.b * 16, 16, 128, 16, this.b.e)));
                 List list = this.b.e.d(chunkcoordintpair.a * 16, 0, chunkcoordintpair.b * 16, chunkcoordintpair.a * 16 + 16, 128, chunkcoordintpair.b * 16 + 16);
 
@@ -81,30 +87,30 @@ public class EntityPlayer extends EntityHuman {
         }
     }
 
-    public void y() {
-        this.o = this.p = this.q = 0.0D;
-        this.bg = false;
-        super.y();
+    public void D() {
+        this.s = this.t = this.u = 0.0D;
+        this.bj = false;
+        super.D();
     }
 
     public void c(Entity entity, int i) {
-        if (!entity.B && entity instanceof EntityItem) {
+        if (!entity.F && entity instanceof EntityItem) {
             this.a.b((Packet) (new Packet17AddToInventory(((EntityItem) entity).a, i)));
-            this.b.k.a(entity, new Packet22Collect(entity.c, this.c));
+            this.b.k.a(entity, new Packet22Collect(entity.g, this.g));
         }
 
         super.c(entity, i);
     }
 
-    public void z() {
-        if (!this.ao) {
-            this.ap = -1;
-            this.ao = true;
+    public void E() {
+        if (!this.ap) {
+            this.aq = -1;
+            this.ap = true;
             this.b.k.a(this, new Packet18ArmAnimation(this, 1));
         }
     }
 
-    protected float p() {
+    protected float s() {
         return 1.62F;
     }
 }
