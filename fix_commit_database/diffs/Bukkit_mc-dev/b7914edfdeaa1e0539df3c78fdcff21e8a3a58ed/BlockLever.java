@@ -10,14 +10,10 @@ public class BlockLever extends Block {
         return null;
     }
 
-    public boolean b() {
+    public boolean a() {
         return false;
     }
 
-    public int a() {
-        return 12;
-    }
-
     public boolean a(World world, int i, int j, int k) {
         return world.d(i - 1, j, k) ? true : (world.d(i + 1, j, k) ? true : (world.d(i, j, k - 1) ? true : (world.d(i, j, k + 1) ? true : world.d(i, j - 1, k))));
     }
@@ -28,7 +24,7 @@ public class BlockLever extends Block {
 
         i1 &= 7;
         if (l == 1 && world.d(i, j - 1, k)) {
-            i1 = 5 + world.m.nextInt(2);
+            i1 = 5 + world.l.nextInt(2);
         }
 
         if (l == 2 && world.d(i, j, k + 1)) {
@@ -60,7 +56,7 @@ public class BlockLever extends Block {
         } else if (world.d(i, j, k + 1)) {
             world.b(i, j, k, 4);
         } else if (world.d(i, j - 1, k)) {
-            world.b(i, j, k, 5 + world.m.nextInt(2));
+            world.b(i, j, k, 5 + world.l.nextInt(2));
         }
 
         this.g(world, i, j, k);
@@ -138,17 +134,17 @@ public class BlockLever extends Block {
         world.b(i, j, k, i1 + j1);
         world.b(i, j, k, i, j, k);
         world.a((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.click", 0.3F, j1 > 0 ? 0.6F : 0.5F);
-        world.g(i, j, k, this.bc);
+        world.g(i, j, k, this.bi);
         if (i1 == 1) {
-            world.g(i - 1, j, k, this.bc);
+            world.g(i - 1, j, k, this.bi);
         } else if (i1 == 2) {
-            world.g(i + 1, j, k, this.bc);
+            world.g(i + 1, j, k, this.bi);
         } else if (i1 == 3) {
-            world.g(i, j, k - 1, this.bc);
+            world.g(i, j, k - 1, this.bi);
         } else if (i1 == 4) {
-            world.g(i, j, k + 1, this.bc);
+            world.g(i, j, k + 1, this.bi);
         } else {
-            world.g(i, j - 1, k, this.bc);
+            world.g(i, j - 1, k, this.bi);
         }
 
         return true;
@@ -158,19 +154,19 @@ public class BlockLever extends Block {
         int l = world.b(i, j, k);
 
         if ((l & 8) > 0) {
-            world.g(i, j, k, this.bc);
+            world.g(i, j, k, this.bi);
             int i1 = l & 7;
 
             if (i1 == 1) {
-                world.g(i - 1, j, k, this.bc);
+                world.g(i - 1, j, k, this.bi);
             } else if (i1 == 2) {
-                world.g(i + 1, j, k, this.bc);
+                world.g(i + 1, j, k, this.bi);
             } else if (i1 == 3) {
-                world.g(i, j, k - 1, this.bc);
+                world.g(i, j, k - 1, this.bi);
             } else if (i1 == 4) {
-                world.g(i, j, k + 1, this.bc);
+                world.g(i, j, k + 1, this.bi);
             } else {
-                world.g(i, j - 1, k, this.bc);
+                world.g(i, j - 1, k, this.bi);
             }
         }
 
@@ -193,7 +189,7 @@ public class BlockLever extends Block {
         }
     }
 
-    public boolean d() {
+    public boolean c() {
         return true;
     }
 }
