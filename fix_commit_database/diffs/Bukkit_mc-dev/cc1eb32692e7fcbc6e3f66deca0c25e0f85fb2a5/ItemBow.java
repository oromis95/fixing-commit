@@ -9,9 +9,9 @@ public class ItemBow extends Item {
 
     public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
         if (entityhuman.inventory.b(Item.ARROW.id)) {
-            world.a(entityhuman, "random.bow", 1.0F, 1.0F / (b.nextFloat() * 0.4F + 0.8F));
+            world.makeSound(entityhuman, "random.bow", 1.0F, 1.0F / (b.nextFloat() * 0.4F + 0.8F));
             if (!world.isStatic) {
-                world.a((Entity) (new EntityArrow(world, entityhuman)));
+                world.addEntity(new EntityArrow(world, entityhuman));
             }
         }
 
