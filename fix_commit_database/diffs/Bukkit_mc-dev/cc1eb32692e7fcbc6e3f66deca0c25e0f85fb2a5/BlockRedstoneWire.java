@@ -27,7 +27,7 @@ public class BlockRedstoneWire extends Block {
         return false;
     }
 
-    public boolean a(World world, int i, int j, int k) {
+    public boolean canPlace(World world, int i, int j, int k) {
         return world.d(i, j - 1, k);
     }
 
@@ -40,7 +40,7 @@ public class BlockRedstoneWire extends Block {
         for (int l = 0; l < arraylist.size(); ++l) {
             ChunkPosition chunkposition = (ChunkPosition) arraylist.get(l);
 
-            world.h(chunkposition.a, chunkposition.b, chunkposition.c, this.id);
+            world.applyPhysics(chunkposition.x, chunkposition.y, chunkposition.z, this.id);
         }
     }
 
@@ -49,7 +49,7 @@ public class BlockRedstoneWire extends Block {
         int l1 = 0;
 
         this.a = false;
-        boolean flag = world.p(i, j, k);
+        boolean flag = world.isBlockIndirectlyPowered(i, j, k);
 
         this.a = true;
         int i2;
@@ -79,15 +79,15 @@ public class BlockRedstoneWire extends Block {
                 }
 
                 if (j2 != l || j != i1 || k2 != j1) {
-                    l1 = this.g(world, j2, j, k2, l1);
+                    l1 = this.getPower(world, j2, j, k2, l1);
                 }
 
                 if (world.d(j2, j, k2) && !world.d(i, j + 1, k)) {
                     if (j2 != l || j + 1 != i1 || k2 != j1) {
-                        l1 = this.g(world, j2, j + 1, k2, l1);
+                        l1 = this.getPower(world, j2, j + 1, k2, l1);
                     }
                 } else if (!world.d(j2, j, k2) && (j2 != l || j - 1 != i1 || k2 != j1)) {
-                    l1 = this.g(world, j2, j - 1, k2, l1);
+                    l1 = this.getPower(world, j2, j - 1, k2, l1);
                 }
             }
 
@@ -100,7 +100,7 @@ public class BlockRedstoneWire extends Block {
 
         if (k1 != l1) {
             world.h = true;
-            world.c(i, j, k, l1);
+            world.setData(i, j, k, l1);
             world.b(i, j, k, i, j, k);
             world.h = false;
 
@@ -130,7 +130,7 @@ public class BlockRedstoneWire extends Block {
                 }
 
                 boolean flag1 = false;
-                int i3 = this.g(world, j2, j, k2, -1);
+                int i3 = this.getPower(world, j2, j, k2, -1);
 
                 l1 = world.getData(i, j, k);
                 if (l1 > 0) {
@@ -141,7 +141,7 @@ public class BlockRedstoneWire extends Block {
                     this.a(world, j2, j, k2, i, j, k);
                 }
 
-                i3 = this.g(world, j2, l2, k2, -1);
+                i3 = this.getPower(world, j2, l2, k2, -1);
                 l1 = world.getData(i, j, k);
                 if (l1 > 0) {
                     --l1;
@@ -166,13 +166,13 @@ public class BlockRedstoneWire extends Block {
 
     private void h(World world, int i, int j, int k) {
         if (world.getTypeId(i, j, k) == this.id) {
-            world.h(i, j, k, this.id);
-            world.h(i - 1, j, k, this.id);
-            world.h(i + 1, j, k, this.id);
-            world.h(i, j, k - 1, this.id);
-            world.h(i, j, k + 1, this.id);
-            world.h(i, j - 1, k, this.id);
-            world.h(i, j + 1, k, this.id);
+            world.applyPhysics(i, j, k, this.id);
+            world.applyPhysics(i - 1, j, k, this.id);
+            world.applyPhysics(i + 1, j, k, this.id);
+            world.applyPhysics(i, j, k - 1, this.id);
+            world.applyPhysics(i, j, k + 1, this.id);
+            world.applyPhysics(i, j - 1, k, this.id);
+            world.applyPhysics(i, j + 1, k, this.id);
         }
     }
 
@@ -180,8 +180,8 @@ public class BlockRedstoneWire extends Block {
         super.e(world, i, j, k);
         if (!world.isStatic) {
             this.g(world, i, j, k);
-            world.h(i, j + 1, k, this.id);
-            world.h(i, j - 1, k, this.id);
+            world.applyPhysics(i, j + 1, k, this.id);
+            world.applyPhysics(i, j - 1, k, this.id);
             this.h(world, i - 1, j, k);
             this.h(world, i + 1, j, k);
             this.h(world, i, j, k - 1);
@@ -212,11 +212,11 @@ public class BlockRedstoneWire extends Block {
         }
     }
 
-    public void b(World world, int i, int j, int k) {
-        super.b(world, i, j, k);
+    public void remove(World world, int i, int j, int k) {
+        super.remove(world, i, j, k);
         if (!world.isStatic) {
-            world.h(i, j + 1, k, this.id);
-            world.h(i, j - 1, k, this.id);
+            world.applyPhysics(i, j + 1, k, this.id);
+            world.applyPhysics(i, j - 1, k, this.id);
             this.g(world, i, j, k);
             this.h(world, i - 1, j, k);
             this.h(world, i + 1, j, k);
@@ -248,7 +248,7 @@ public class BlockRedstoneWire extends Block {
         }
     }
 
-    private int g(World world, int i, int j, int k, int l) {
+    private int getPower(World world, int i, int j, int k, int l) {
         if (world.getTypeId(i, j, k) != this.id) {
             return l;
         } else {
@@ -258,19 +258,19 @@ public class BlockRedstoneWire extends Block {
         }
     }
 
-    public void a(World world, int i, int j, int k, int l) {
+    public void doPhysics(World world, int i, int j, int k, int l) {
         if (!world.isStatic) {
             int i1 = world.getData(i, j, k);
-            boolean flag = this.a(world, i, j, k);
+            boolean flag = this.canPlace(world, i, j, k);
 
             if (!flag) {
-                this.b_(world, i, j, k, i1);
-                world.e(i, j, k, 0);
+                this.a_(world, i, j, k, i1);
+                world.setTypeId(i, j, k, 0);
             } else {
                 this.g(world, i, j, k);
             }
 
-            super.a(world, i, j, k, l);
+            super.doPhysics(world, i, j, k, l);
         }
     }
 
@@ -317,13 +317,13 @@ public class BlockRedstoneWire extends Block {
         }
     }
 
-    public boolean c() {
+    public boolean isPowerSource() {
         return this.a;
     }
 
     public static boolean b(IBlockAccess iblockaccess, int i, int j, int k) {
         int l = iblockaccess.getTypeId(i, j, k);
 
-        return l == Block.REDSTONE_WIRE.id ? true : (l == 0 ? false : Block.byId[l].c());
+        return l == Block.REDSTONE_WIRE.id ? true : (l == 0 ? false : Block.byId[l].isPowerSource());
     }
 }
