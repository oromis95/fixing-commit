@@ -10,40 +10,40 @@ public class EntityEgg extends Entity {
     private int e = 0;
     private boolean f = false;
     public int a = 0;
-    private EntityLiving g;
+    private EntityLiving thrower;
     private int h;
     private int i = 0;
 
     public EntityEgg(World world) {
         super(world);
-        this.a(0.25F, 0.25F);
+        this.b(0.25F, 0.25F);
     }
 
     protected void a() {}
 
     public EntityEgg(World world, EntityLiving entityliving) {
         super(world);
-        this.g = entityliving;
-        this.a(0.25F, 0.25F);
-        this.c(entityliving.locX, entityliving.locY + (double) entityliving.p(), entityliving.locZ, entityliving.yaw, entityliving.pitch);
-        this.locX -= (double) (MathHelper.b(this.yaw / 180.0F * 3.1415927F) * 0.16F);
+        this.thrower = entityliving;
+        this.b(0.25F, 0.25F);
+        this.setPositionRotation(entityliving.locX, entityliving.locY + (double) entityliving.q(), entityliving.locZ, entityliving.yaw, entityliving.pitch);
+        this.locX -= (double) (MathHelper.cos(this.yaw / 180.0F * 3.1415927F) * 0.16F);
         this.locY -= 0.10000000149011612D;
-        this.locZ -= (double) (MathHelper.a(this.yaw / 180.0F * 3.1415927F) * 0.16F);
-        this.a(this.locX, this.locY, this.locZ);
+        this.locZ -= (double) (MathHelper.sin(this.yaw / 180.0F * 3.1415927F) * 0.16F);
+        this.setPosition(this.locX, this.locY, this.locZ);
         this.height = 0.0F;
         float f = 0.4F;
 
-        this.motX = (double) (-MathHelper.a(this.yaw / 180.0F * 3.1415927F) * MathHelper.b(this.pitch / 180.0F * 3.1415927F) * f);
-        this.motZ = (double) (MathHelper.b(this.yaw / 180.0F * 3.1415927F) * MathHelper.b(this.pitch / 180.0F * 3.1415927F) * f);
-        this.motY = (double) (-MathHelper.a(this.pitch / 180.0F * 3.1415927F) * f);
+        this.motX = (double) (-MathHelper.sin(this.yaw / 180.0F * 3.1415927F) * MathHelper.cos(this.pitch / 180.0F * 3.1415927F) * f);
+        this.motZ = (double) (MathHelper.cos(this.yaw / 180.0F * 3.1415927F) * MathHelper.cos(this.pitch / 180.0F * 3.1415927F) * f);
+        this.motY = (double) (-MathHelper.sin(this.pitch / 180.0F * 3.1415927F) * f);
         this.a(this.motX, this.motY, this.motZ, 1.5F, 1.0F);
     }
 
     public EntityEgg(World world, double d0, double d1, double d2) {
         super(world);
         this.h = 0;
-        this.a(0.25F, 0.25F);
-        this.a(d0, d1, d2);
+        this.b(0.25F, 0.25F);
+        this.setPosition(d0, d1, d2);
         this.height = 0.0F;
     }
 
@@ -84,7 +84,7 @@ public class EntityEgg extends Entity {
             if (i == this.e) {
                 ++this.h;
                 if (this.h == 1200) {
-                    this.C();
+                    this.die();
                 }
 
                 return;
@@ -100,14 +100,14 @@ public class EntityEgg extends Entity {
             ++this.i;
         }
 
-        Vec3D vec3d = Vec3D.b(this.locX, this.locY, this.locZ);
-        Vec3D vec3d1 = Vec3D.b(this.locX + this.motX, this.locY + this.motY, this.locZ + this.motZ);
+        Vec3D vec3d = Vec3D.create(this.locX, this.locY, this.locZ);
+        Vec3D vec3d1 = Vec3D.create(this.locX + this.motX, this.locY + this.motY, this.locZ + this.motZ);
         MovingObjectPosition movingobjectposition = this.world.a(vec3d, vec3d1);
 
-        vec3d = Vec3D.b(this.locX, this.locY, this.locZ);
-        vec3d1 = Vec3D.b(this.locX + this.motX, this.locY + this.motY, this.locZ + this.motZ);
+        vec3d = Vec3D.create(this.locX, this.locY, this.locZ);
+        vec3d1 = Vec3D.create(this.locX + this.motX, this.locY + this.motY, this.locZ + this.motZ);
         if (movingobjectposition != null) {
-            vec3d1 = Vec3D.b(movingobjectposition.f.a, movingobjectposition.f.b, movingobjectposition.f.c);
+            vec3d1 = Vec3D.create(movingobjectposition.f.a, movingobjectposition.f.b, movingobjectposition.f.c);
         }
 
         if (!this.world.isStatic) {
@@ -118,7 +118,7 @@ public class EntityEgg extends Entity {
             for (int j = 0; j < list.size(); ++j) {
                 Entity entity1 = (Entity) list.get(j);
 
-                if (entity1.d_() && (entity1 != this.g || this.i >= 5)) {
+                if (entity1.d_() && (entity1 != this.thrower || this.i >= 5)) {
                     float f = 0.3F;
                     AxisAlignedBB axisalignedbb = entity1.boundingBox.b((double) f, (double) f, (double) f);
                     MovingObjectPosition movingobjectposition1 = axisalignedbb.a(vec3d, vec3d1);
@@ -140,7 +140,7 @@ public class EntityEgg extends Entity {
         }
 
         if (movingobjectposition != null) {
-            if (movingobjectposition.g != null && movingobjectposition.g.a(this.g, 0)) {
+            if (movingobjectposition.entity != null && movingobjectposition.entity.damageEntity(this.thrower, 0)) {
                 ;
             }
 
@@ -154,8 +154,8 @@ public class EntityEgg extends Entity {
                 for (int k = 0; k < b0; ++k) {
                     EntityChicken entitychicken = new EntityChicken(this.world);
 
-                    entitychicken.c(this.locX, this.locY, this.locZ, this.yaw, 0.0F);
-                    this.world.a((Entity) entitychicken);
+                    entitychicken.setPositionRotation(this.locX, this.locY, this.locZ, this.yaw, 0.0F);
+                    this.world.addEntity(entitychicken);
                 }
             }
 
@@ -163,7 +163,7 @@ public class EntityEgg extends Entity {
                 this.world.a("snowballpoof", this.locX, this.locY, this.locZ, 0.0D, 0.0D, 0.0D);
             }
 
-            this.C();
+            this.die();
         }
 
         this.locX += this.motX;
@@ -208,7 +208,7 @@ public class EntityEgg extends Entity {
         this.motY *= (double) f2;
         this.motZ *= (double) f2;
         this.motY -= (double) f3;
-        this.a(this.locX, this.locY, this.locZ);
+        this.setPosition(this.locX, this.locY, this.locZ);
     }
 
     public void a(NBTTagCompound nbttagcompound) {
@@ -230,10 +230,10 @@ public class EntityEgg extends Entity {
     }
 
     public void b(EntityHuman entityhuman) {
-        if (this.f && this.g == entityhuman && this.a <= 0 && entityhuman.inventory.a(new ItemStack(Item.ARROW, 1))) {
-            this.world.a(this, "random.pop", 0.2F, ((this.random.nextFloat() - this.random.nextFloat()) * 0.7F + 1.0F) * 2.0F);
-            entityhuman.b(this, 1);
-            this.C();
+        if (this.f && this.thrower == entityhuman && this.a <= 0 && entityhuman.inventory.canHold(new ItemStack(Item.ARROW, 1))) {
+            this.world.makeSound(this, "random.pop", 0.2F, ((this.random.nextFloat() - this.random.nextFloat()) * 0.7F + 1.0F) * 2.0F);
+            entityhuman.receive(this, 1);
+            this.die();
         }
     }
 }
