@@ -13,14 +13,14 @@ public final class SpawnerCreature {
     public SpawnerCreature() {}
 
     protected static ChunkPosition a(World world, int i, int j) {
-        int k = i + world.k.nextInt(16);
-        int l = world.k.nextInt(128);
-        int i1 = j + world.k.nextInt(16);
+        int k = i + world.random.nextInt(16);
+        int l = world.random.nextInt(128);
+        int i1 = j + world.random.nextInt(16);
 
         return new ChunkPosition(k, l, i1);
     }
 
-    public static final int a(World world, boolean flag, boolean flag1) {
+    public static final int spawnEntities(World world, boolean flag, boolean flag1) {
         if (!flag && !flag1) {
             return 0;
         } else {
@@ -29,11 +29,11 @@ public final class SpawnerCreature {
             int i;
             int j;
 
-            for (i = 0; i < world.d.size(); ++i) {
-                EntityHuman entityhuman = (EntityHuman) world.d.get(i);
-                int k = MathHelper.b(entityhuman.locX / 16.0D);
+            for (i = 0; i < world.players.size(); ++i) {
+                EntityHuman entityhuman = (EntityHuman) world.players.get(i);
+                int k = MathHelper.floor(entityhuman.locX / 16.0D);
 
-                j = MathHelper.b(entityhuman.locZ / 16.0D);
+                j = MathHelper.floor(entityhuman.locZ / 16.0D);
                 byte b0 = 8;
 
                 for (int l = -b0; l <= b0; ++l) {
@@ -44,7 +44,7 @@ public final class SpawnerCreature {
             }
 
             i = 0;
-            ChunkCoordinates chunkcoordinates = world.l();
+            ChunkCoordinates chunkcoordinates = world.getSpawn();
             EnumCreatureType[] aenumcreaturetype = EnumCreatureType.values();
 
             j = aenumcreaturetype.length;
@@ -55,64 +55,86 @@ public final class SpawnerCreature {
                 if ((!enumcreaturetype.d() || flag1) && (enumcreaturetype.d() || flag) && world.a(enumcreaturetype.a()) <= enumcreaturetype.b() * b.size() / 256) {
                     Iterator iterator = b.iterator();
 
-                    label91:
+                    label113:
                     while (iterator.hasNext()) {
                         ChunkCoordIntPair chunkcoordintpair = (ChunkCoordIntPair) iterator.next();
-                        BiomeBase biomebase = world.a().a(chunkcoordintpair);
-                        Class[] aclass = biomebase.a(enumcreaturetype);
+                        BiomeBase biomebase = world.getWorldChunkManager().a(chunkcoordintpair);
+                        List list = biomebase.a(enumcreaturetype);
 
-                        if (aclass != null && aclass.length != 0) {
-                            int k1 = world.k.nextInt(aclass.length);
-                            ChunkPosition chunkposition = a(world, chunkcoordintpair.a * 16, chunkcoordintpair.b * 16);
-                            int l1 = chunkposition.a;
-                            int i2 = chunkposition.b;
-                            int j2 = chunkposition.c;
+                        if (list != null && !list.isEmpty()) {
+                            int k1 = 0;
 
-                            if (!world.d(l1, i2, j2) && world.getMaterial(l1, i2, j2) == enumcreaturetype.c()) {
-                                int k2 = 0;
+                            BiomeMeta biomemeta;
 
-                                for (int l2 = 0; l2 < 3; ++l2) {
-                                    int i3 = l1;
+                            for (Iterator iterator1 = list.iterator(); iterator1.hasNext(); k1 += biomemeta.b) {
+                                biomemeta = (BiomeMeta) iterator1.next();
+                            }
+
+                            int l1 = world.random.nextInt(k1);
+
+                            biomemeta = (BiomeMeta) list.get(0);
+                            Iterator iterator2 = list.iterator();
+
+                            while (iterator2.hasNext()) {
+                                BiomeMeta biomemeta1 = (BiomeMeta) iterator2.next();
+
+                                l1 -= biomemeta1.b;
+                                if (l1 < 0) {
+                                    biomemeta = biomemeta1;
+                                    break;
+                                }
+                            }
+
+                            ChunkPosition chunkposition = a(world, chunkcoordintpair.x * 16, chunkcoordintpair.z * 16);
+                            int i2 = chunkposition.x;
+                            int j2 = chunkposition.y;
+                            int k2 = chunkposition.z;
+
+                            if (!world.d(i2, j2, k2) && world.getMaterial(i2, j2, k2) == enumcreaturetype.c()) {
+                                int l2 = 0;
+
+                                for (int i3 = 0; i3 < 3; ++i3) {
                                     int j3 = i2;
                                     int k3 = j2;
+                                    int l3 = k2;
                                     byte b1 = 6;
 
-                                    for (int l3 = 0; l3 < 4; ++l3) {
-                                        i3 += world.k.nextInt(b1) - world.k.nextInt(b1);
-                                        j3 += world.k.nextInt(1) - world.k.nextInt(1);
-                                        k3 += world.k.nextInt(b1) - world.k.nextInt(b1);
-                                        if (a(enumcreaturetype, world, i3, j3, k3)) {
-                                            float f = (float) i3 + 0.5F;
-                                            float f1 = (float) j3;
-                                            float f2 = (float) k3 + 0.5F;
+                                    for (int i4 = 0; i4 < 4; ++i4) {
+                                        j3 += world.random.nextInt(b1) - world.random.nextInt(b1);
+                                        k3 += world.random.nextInt(1) - world.random.nextInt(1);
+                                        l3 += world.random.nextInt(b1) - world.random.nextInt(b1);
+                                        if (a(enumcreaturetype, world, j3, k3, l3)) {
+                                            float f = (float) j3 + 0.5F;
+                                            float f1 = (float) k3;
+                                            float f2 = (float) l3 + 0.5F;
 
                                             if (world.a((double) f, (double) f1, (double) f2, 24.0D) == null) {
-                                                float f3 = f - (float) chunkcoordinates.a;
-                                                float f4 = f1 - (float) chunkcoordinates.b;
-                                                float f5 = f2 - (float) chunkcoordinates.c;
+                                                float f3 = f - (float) chunkcoordinates.x;
+                                                float f4 = f1 - (float) chunkcoordinates.y;
+                                                float f5 = f2 - (float) chunkcoordinates.z;
                                                 float f6 = f3 * f3 + f4 * f4 + f5 * f5;
 
                                                 if (f6 >= 576.0F) {
                                                     EntityLiving entityliving;
 
                                                     try {
-                                                        entityliving = (EntityLiving) aclass[k1].getConstructor(new Class[] { World.class}).newInstance(new Object[] { world});
+                                                        entityliving = (EntityLiving) biomemeta.a.getConstructor(new Class[] { World.class}).newInstance(new Object[] { world});
                                                     } catch (Exception exception) {
                                                         exception.printStackTrace();
                                                         return i;
                                                     }
 
-                                                    entityliving.c((double) f, (double) f1, (double) f2, world.k.nextFloat() * 360.0F, 0.0F);
+                                                    entityliving.setPositionRotation((double) f, (double) f1, (double) f2, world.random.nextFloat() * 360.0F, 0.0F);
                                                     if (entityliving.b()) {
-                                                        ++k2;
-                                                        world.a((Entity) entityliving);
+                                                        ++l2;
+                                                        world.addEntity(entityliving);
                                                         a(entityliving, world, f, f1, f2);
-                                                        if (k2 >= entityliving.j()) {
-                                                            continue label91;
+                                                        if (l2 >= entityliving.j()) {
+                                                            continue label113;
                                                         }
                                                     }
 
-                                                    i += k2;
+                                                    i += l2;
                                                 }
                                             }
                                         }
@@ -133,14 +155,14 @@ public final class SpawnerCreature {
     }
 
     private static void a(EntityLiving entityliving, World world, float f, float f1, float f2) {
-        if (entityliving instanceof EntitySpider && world.k.nextInt(100) == 0) {
+        if (entityliving instanceof EntitySpider && world.random.nextInt(100) == 0) {
             EntitySkeleton entityskeleton = new EntitySkeleton(world);
 
-            entityskeleton.c((double) f, (double) f1, (double) f2, entityliving.yaw, 0.0F);
-            world.a((Entity) entityskeleton);
-            entityskeleton.b(entityliving);
+            entityskeleton.setPositionRotation((double) f, (double) f1, (double) f2, entityliving.yaw, 0.0F);
+            world.addEntity(entityskeleton);
+            entityskeleton.mount(entityliving);
         } else if (entityliving instanceof EntitySheep) {
-            ((EntitySheep) entityliving).a_(EntitySheep.a(world.k));
+            ((EntitySheep) entityliving).setColor(EntitySheep.a(world.random));
         }
     }
 
@@ -157,9 +179,9 @@ public final class SpawnerCreature {
                 boolean flag1 = false;
 
                 for (int i = 0; i < 20 && !flag1; ++i) {
-                    int j = MathHelper.b(entityhuman.locX) + world.k.nextInt(32) - world.k.nextInt(32);
-                    int k = MathHelper.b(entityhuman.locZ) + world.k.nextInt(32) - world.k.nextInt(32);
-                    int l = MathHelper.b(entityhuman.locY) + world.k.nextInt(16) - world.k.nextInt(16);
+                    int j = MathHelper.floor(entityhuman.locX) + world.random.nextInt(32) - world.random.nextInt(32);
+                    int k = MathHelper.floor(entityhuman.locZ) + world.random.nextInt(32) - world.random.nextInt(32);
+                    int l = MathHelper.floor(entityhuman.locY) + world.random.nextInt(16) - world.random.nextInt(16);
 
                     if (l < 1) {
                         l = 1;
@@ -167,7 +189,7 @@ public final class SpawnerCreature {
                         l = 128;
                     }
 
-                    int i1 = world.k.nextInt(aclass.length);
+                    int i1 = world.random.nextInt(aclass.length);
 
                     int j1;
 
@@ -193,7 +215,7 @@ public final class SpawnerCreature {
                             return flag;
                         }
 
-                        entityliving.c((double) f, (double) f1, (double) f2, world.k.nextFloat() * 360.0F, 0.0F);
+                        entityliving.setPositionRotation((double) f, (double) f1, (double) f2, world.random.nextFloat() * 360.0F, 0.0F);
                         if (entityliving.b()) {
                             PathEntity pathentity = pathfinder.a(entityliving, entityhuman, 32.0F);
 
@@ -201,13 +223,17 @@ public final class SpawnerCreature {
                                 PathPoint pathpoint = pathentity.c();
 
                                 if (Math.abs((double) pathpoint.a - entityhuman.locX) < 1.5D && Math.abs((double) pathpoint.c - entityhuman.locZ) < 1.5D && Math.abs((double) pathpoint.b - entityhuman.locY) < 1.5D) {
-                                    ChunkCoordinates chunkcoordinates = BlockBed.g(world, MathHelper.b(entityhuman.locX), MathHelper.b(entityhuman.locY), MathHelper.b(entityhuman.locZ), 1);
+                                    ChunkCoordinates chunkcoordinates = BlockBed.f(world, MathHelper.floor(entityhuman.locX), MathHelper.floor(entityhuman.locY), MathHelper.floor(entityhuman.locZ), 1);
+
+                                    if (chunkcoordinates == null) {
+                                        chunkcoordinates = new ChunkCoordinates(j, j1 + 1, k);
+                                    }
 
-                                    entityliving.c((double) ((float) chunkcoordinates.a + 0.5F), (double) chunkcoordinates.b, (double) ((float) chunkcoordinates.c + 0.5F), 0.0F, 0.0F);
-                                    world.a((Entity) entityliving);
-                                    a(entityliving, world, (float) chunkcoordinates.a + 0.5F, (float) chunkcoordinates.b, (float) chunkcoordinates.c + 0.5F);
-                                    entityhuman.a(true, false);
-                                    entityliving.G();
+                                    entityliving.setPositionRotation((double) ((float) chunkcoordinates.x + 0.5F), (double) chunkcoordinates.y, (double) ((float) chunkcoordinates.z + 0.5F), 0.0F, 0.0F);
+                                    world.addEntity(entityliving);
+                                    a(entityliving, world, (float) chunkcoordinates.x + 0.5F, (float) chunkcoordinates.y, (float) chunkcoordinates.z + 0.5F);
+                                    entityhuman.a(true, false, false);
+                                    entityliving.K();
                                     flag = true;
                                     flag1 = true;
                                 }
