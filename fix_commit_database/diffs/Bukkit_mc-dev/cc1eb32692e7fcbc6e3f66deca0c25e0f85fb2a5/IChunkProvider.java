@@ -2,15 +2,17 @@ package net.minecraft.server;
 
 public interface IChunkProvider {
 
-    boolean a(int i, int j);
+    boolean isChunkLoaded(int i, int j);
 
-    Chunk b(int i, int j);
+    Chunk getOrCreateChunk(int i, int j);
 
-    void a(IChunkProvider ichunkprovider, int i, int j);
+    Chunk getChunkAt(int i, int j);
 
-    boolean a(boolean flag, IProgressUpdate iprogressupdate);
+    void getChunkAt(IChunkProvider ichunkprovider, int i, int j);
 
-    boolean a();
+    boolean saveChunks(boolean flag, IProgressUpdate iprogressupdate);
+
+    boolean unloadChunks();
 
     boolean b();
 }
