@@ -6,7 +6,7 @@ import java.util.Random;
 
 public class BlockRedstoneTorch extends BlockTorch {
 
-    private boolean a = false;
+    private boolean isOn = false;
     private static List b = new ArrayList();
 
     public int a(int i, int j) {
@@ -15,7 +15,7 @@ public class BlockRedstoneTorch extends BlockTorch {
 
     private boolean a(World world, int i, int j, int k, boolean flag) {
         if (flag) {
-            b.add(new RedstoneUpdateInfo(i, j, k, world.k()));
+            b.add(new RedstoneUpdateInfo(i, j, k, world.getTime()));
         }
 
         int l = 0;
@@ -36,7 +36,7 @@ public class BlockRedstoneTorch extends BlockTorch {
 
     protected BlockRedstoneTorch(int i, int j, boolean flag) {
         super(i, j);
-        this.a = flag;
+        this.isOn = flag;
         this.a(true);
     }
 
@@ -49,29 +49,29 @@ public class BlockRedstoneTorch extends BlockTorch {
             super.e(world, i, j, k);
         }
 
-        if (this.a) {
-            world.h(i, j - 1, k, this.id);
-            world.h(i, j + 1, k, this.id);
-            world.h(i - 1, j, k, this.id);
-            world.h(i + 1, j, k, this.id);
-            world.h(i, j, k - 1, this.id);
-            world.h(i, j, k + 1, this.id);
+        if (this.isOn) {
+            world.applyPhysics(i, j - 1, k, this.id);
+            world.applyPhysics(i, j + 1, k, this.id);
+            world.applyPhysics(i - 1, j, k, this.id);
+            world.applyPhysics(i + 1, j, k, this.id);
+            world.applyPhysics(i, j, k - 1, this.id);
+            world.applyPhysics(i, j, k + 1, this.id);
         }
     }
 
-    public void b(World world, int i, int j, int k) {
-        if (this.a) {
-            world.h(i, j - 1, k, this.id);
-            world.h(i, j + 1, k, this.id);
-            world.h(i - 1, j, k, this.id);
-            world.h(i + 1, j, k, this.id);
-            world.h(i, j, k - 1, this.id);
-            world.h(i, j, k + 1, this.id);
+    public void remove(World world, int i, int j, int k) {
+        if (this.isOn) {
+            world.applyPhysics(i, j - 1, k, this.id);
+            world.applyPhysics(i, j + 1, k, this.id);
+            world.applyPhysics(i - 1, j, k, this.id);
+            world.applyPhysics(i + 1, j, k, this.id);
+            world.applyPhysics(i, j, k - 1, this.id);
+            world.applyPhysics(i, j, k + 1, this.id);
         }
     }
 
     public boolean b(IBlockAccess iblockaccess, int i, int j, int k, int l) {
-        if (!this.a) {
+        if (!this.isOn) {
             return false;
         } else {
             int i1 = iblockaccess.getData(i, j, k);
@@ -83,21 +83,21 @@ public class BlockRedstoneTorch extends BlockTorch {
     private boolean g(World world, int i, int j, int k) {
         int l = world.getData(i, j, k);
 
-        return l == 5 && world.j(i, j - 1, k, 0) ? true : (l == 3 && world.j(i, j, k - 1, 2) ? true : (l == 4 && world.j(i, j, k + 1, 3) ? true : (l == 1 && world.j(i - 1, j, k, 4) ? true : l == 2 && world.j(i + 1, j, k, 5))));
+        return l == 5 && world.isBlockFaceIndirectlyPowered(i, j - 1, k, 0) ? true : (l == 3 && world.isBlockFaceIndirectlyPowered(i, j, k - 1, 2) ? true : (l == 4 && world.isBlockFaceIndirectlyPowered(i, j, k + 1, 3) ? true : (l == 1 && world.isBlockFaceIndirectlyPowered(i - 1, j, k, 4) ? true : l == 2 && world.isBlockFaceIndirectlyPowered(i + 1, j, k, 5))));
     }
 
     public void a(World world, int i, int j, int k, Random random) {
         boolean flag = this.g(world, i, j, k);
 
-        while (b.size() > 0 && world.k() - ((RedstoneUpdateInfo) b.get(0)).d > 100L) {
+        while (b.size() > 0 && world.getTime() - ((RedstoneUpdateInfo) b.get(0)).d > 100L) {
             b.remove(0);
         }
 
-        if (this.a) {
+        if (this.isOn) {
             if (flag) {
-                world.b(i, j, k, Block.REDSTONE_TORCH_OFF.id, world.getData(i, j, k));
+                world.setTypeIdAndData(i, j, k, Block.REDSTONE_TORCH_OFF.id, world.getData(i, j, k));
                 if (this.a(world, i, j, k, true)) {
-                    world.a((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), "random.fizz", 0.5F, 2.6F + (world.k.nextFloat() - world.k.nextFloat()) * 0.8F);
+                    world.makeSound((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), "random.fizz", 0.5F, 2.6F + (world.random.nextFloat() - world.random.nextFloat()) * 0.8F);
 
                     for (int l = 0; l < 5; ++l) {
                         double d0 = (double) i + random.nextDouble() * 0.6D + 0.2D;
@@ -109,12 +109,12 @@ public class BlockRedstoneTorch extends BlockTorch {
                 }
             }
         } else if (!flag && !this.a(world, i, j, k, false)) {
-            world.b(i, j, k, Block.REDSTONE_TORCH_ON.id, world.getData(i, j, k));
+            world.setTypeIdAndData(i, j, k, Block.REDSTONE_TORCH_ON.id, world.getData(i, j, k));
         }
     }
 
-    public void a(World world, int i, int j, int k, int l) {
-        super.a(world, i, j, k, l);
+    public void doPhysics(World world, int i, int j, int k, int l) {
+        super.doPhysics(world, i, j, k, l);
         world.c(i, j, k, this.id, this.b());
     }
 
@@ -126,7 +126,7 @@ public class BlockRedstoneTorch extends BlockTorch {
         return Block.REDSTONE_TORCH_ON.id;
     }
 
-    public boolean c() {
+    public boolean isPowerSource() {
         return true;
     }
 }
