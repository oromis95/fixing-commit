@@ -12,19 +12,22 @@ public class EntityFallingSand extends Entity {
     public EntityFallingSand(World world, double d0, double d1, double d2, int i) {
         super(world);
         this.a = i;
-        this.aC = true;
-        this.a(0.98F, 0.98F);
+        this.aD = true;
+        this.b(0.98F, 0.98F);
         this.height = this.width / 2.0F;
-        this.a(d0, d1, d2);
+        this.setPosition(d0, d1, d2);
         this.motX = 0.0D;
         this.motY = 0.0D;
         this.motZ = 0.0D;
-        this.bg = false;
         this.lastX = d0;
         this.lastY = d1;
         this.lastZ = d2;
     }
 
+    protected boolean l() {
+        return false;
+    }
+
     protected void a() {}
 
     public boolean d_() {
@@ -33,36 +36,36 @@ public class EntityFallingSand extends Entity {
 
     public void f_() {
         if (this.a == 0) {
-            this.C();
+            this.die();
         } else {
             this.lastX = this.locX;
             this.lastY = this.locY;
             this.lastZ = this.locZ;
             ++this.b;
             this.motY -= 0.03999999910593033D;
-            this.c(this.motX, this.motY, this.motZ);
+            this.move(this.motX, this.motY, this.motZ);
             this.motX *= 0.9800000190734863D;
             this.motY *= 0.9800000190734863D;
             this.motZ *= 0.9800000190734863D;
-            int i = MathHelper.b(this.locX);
-            int j = MathHelper.b(this.locY);
-            int k = MathHelper.b(this.locZ);
+            int i = MathHelper.floor(this.locX);
+            int j = MathHelper.floor(this.locY);
+            int k = MathHelper.floor(this.locZ);
 
             if (this.world.getTypeId(i, j, k) == this.a) {
-                this.world.e(i, j, k, 0);
+                this.world.setTypeId(i, j, k, 0);
             }
 
             if (this.onGround) {
                 this.motX *= 0.699999988079071D;
                 this.motZ *= 0.699999988079071D;
                 this.motY *= -0.5D;
-                this.C();
-                if ((!this.world.a(this.a, i, j, k, true) || !this.world.e(i, j, k, this.a)) && !this.world.isStatic) {
+                this.die();
+                if ((!this.world.a(this.a, i, j, k, true) || !this.world.setTypeId(i, j, k, this.a)) && !this.world.isStatic) {
                     this.b(this.a, 1);
                 }
             } else if (this.b > 100 && !this.world.isStatic) {
                 this.b(this.a, 1);
-                this.C();
+                this.die();
             }
         }
     }
