@@ -7,7 +7,7 @@ import java.util.Set;
 
 public class EntityTrackerEntry {
 
-    public Entity a;
+    public Entity tracker;
     public int b;
     public int c;
     public int d;
@@ -23,47 +23,47 @@ public class EntityTrackerEntry {
     private double p;
     private double q;
     private boolean r = false;
-    private boolean s;
+    private boolean isMoving;
     public boolean m = false;
-    public Set n = new HashSet();
+    public Set trackedPlayers = new HashSet();
 
     public EntityTrackerEntry(Entity entity, int i, int j, boolean flag) {
-        this.a = entity;
+        this.tracker = entity;
         this.b = i;
         this.c = j;
-        this.s = flag;
-        this.d = MathHelper.b(entity.locX * 32.0D);
-        this.e = MathHelper.b(entity.locY * 32.0D);
-        this.f = MathHelper.b(entity.locZ * 32.0D);
+        this.isMoving = flag;
+        this.d = MathHelper.floor(entity.locX * 32.0D);
+        this.e = MathHelper.floor(entity.locY * 32.0D);
+        this.f = MathHelper.floor(entity.locZ * 32.0D);
         this.g = MathHelper.d(entity.yaw * 256.0F / 360.0F);
         this.h = MathHelper.d(entity.pitch * 256.0F / 360.0F);
     }
 
     public boolean equals(Object object) {
-        return object instanceof EntityTrackerEntry ? ((EntityTrackerEntry) object).a.id == this.a.id : false;
+        return object instanceof EntityTrackerEntry ? ((EntityTrackerEntry) object).tracker.id == this.tracker.id : false;
     }
 
     public int hashCode() {
-        return this.a.id;
+        return this.tracker.id;
     }
 
-    public void a(List list) {
+    public void track(List list) {
         this.m = false;
-        if (!this.r || this.a.d(this.o, this.p, this.q) > 16.0D) {
-            this.o = this.a.locX;
-            this.p = this.a.locY;
-            this.q = this.a.locZ;
+        if (!this.r || this.tracker.d(this.o, this.p, this.q) > 16.0D) {
+            this.o = this.tracker.locX;
+            this.p = this.tracker.locY;
+            this.q = this.tracker.locZ;
             this.r = true;
             this.m = true;
-            this.b(list);
+            this.scanPlayers(list);
         }
 
         if (++this.l % this.c == 0) {
-            int i = MathHelper.b(this.a.locX * 32.0D);
-            int j = MathHelper.b(this.a.locY * 32.0D);
-            int k = MathHelper.b(this.a.locZ * 32.0D);
-            int l = MathHelper.d(this.a.yaw * 256.0F / 360.0F);
-            int i1 = MathHelper.d(this.a.pitch * 256.0F / 360.0F);
+            int i = MathHelper.floor(this.tracker.locX * 32.0D);
+            int j = MathHelper.floor(this.tracker.locY * 32.0D);
+            int k = MathHelper.floor(this.tracker.locZ * 32.0D);
+            int l = MathHelper.d(this.tracker.yaw * 256.0F / 360.0F);
+            int i1 = MathHelper.d(this.tracker.pitch * 256.0F / 360.0F);
             int j1 = i - this.d;
             int k1 = j - this.e;
             int l1 = k - this.f;
@@ -73,28 +73,28 @@ public class EntityTrackerEntry {
 
             if (j1 >= -128 && j1 < 128 && k1 >= -128 && k1 < 128 && l1 >= -128 && l1 < 128) {
                 if (flag && flag1) {
-                    object = new Packet33RelEntityMoveLook(this.a.id, (byte) j1, (byte) k1, (byte) l1, (byte) l, (byte) i1);
+                    object = new Packet33RelEntityMoveLook(this.tracker.id, (byte) j1, (byte) k1, (byte) l1, (byte) l, (byte) i1);
                 } else if (flag) {
-                    object = new Packet31RelEntityMove(this.a.id, (byte) j1, (byte) k1, (byte) l1);
+                    object = new Packet31RelEntityMove(this.tracker.id, (byte) j1, (byte) k1, (byte) l1);
                 } else if (flag1) {
-                    object = new Packet32EntityLook(this.a.id, (byte) l, (byte) i1);
+                    object = new Packet32EntityLook(this.tracker.id, (byte) l, (byte) i1);
                 }
             } else {
-                object = new Packet34EntityTeleport(this.a.id, i, j, k, (byte) l, (byte) i1);
+                object = new Packet34EntityTeleport(this.tracker.id, i, j, k, (byte) l, (byte) i1);
             }
 
-            if (this.s) {
-                double d0 = this.a.motX - this.i;
-                double d1 = this.a.motY - this.j;
-                double d2 = this.a.motZ - this.k;
+            if (this.isMoving) {
+                double d0 = this.tracker.motX - this.i;
+                double d1 = this.tracker.motY - this.j;
+                double d2 = this.tracker.motZ - this.k;
                 double d3 = 0.02D;
                 double d4 = d0 * d0 + d1 * d1 + d2 * d2;
 
-                if (d4 > d3 * d3 || d4 > 0.0D && this.a.motX == 0.0D && this.a.motY == 0.0D && this.a.motZ == 0.0D) {
-                    this.i = this.a.motX;
-                    this.j = this.a.motY;
-                    this.k = this.a.motZ;
-                    this.a((Packet) (new Packet28EntityVelocity(this.a.id, this.i, this.j, this.k)));
+                if (d4 > d3 * d3 || d4 > 0.0D && this.tracker.motX == 0.0D && this.tracker.motY == 0.0D && this.tracker.motZ == 0.0D) {
+                    this.i = this.tracker.motX;
+                    this.j = this.tracker.motY;
+                    this.k = this.tracker.motZ;
+                    this.a((Packet) (new Packet28EntityVelocity(this.tracker.id, this.i, this.j, this.k)));
                 }
             }
 
@@ -102,10 +102,10 @@ public class EntityTrackerEntry {
                 this.a((Packet) object);
             }
 
-            DataWatcher datawatcher = this.a.O();
+            DataWatcher datawatcher = this.tracker.T();
 
             if (datawatcher.a()) {
-                this.b((Packet) (new Packet40EntityMetadata(this.a.id, datawatcher)));
+                this.b((Packet) (new Packet40EntityMetadata(this.tracker.id, datawatcher)));
             }
 
             if (flag) {
@@ -120,141 +120,141 @@ public class EntityTrackerEntry {
             }
         }
 
-        if (this.a.aY) {
-            this.b((Packet) (new Packet28EntityVelocity(this.a)));
-            this.a.aY = false;
+        if (this.tracker.velocityChanged) {
+            this.b((Packet) (new Packet28EntityVelocity(this.tracker)));
+            this.tracker.velocityChanged = false;
         }
     }
 
     public void a(Packet packet) {
-        Iterator iterator = this.n.iterator();
+        Iterator iterator = this.trackedPlayers.iterator();
 
         while (iterator.hasNext()) {
             EntityPlayer entityplayer = (EntityPlayer) iterator.next();
 
-            entityplayer.a.b(packet);
+            entityplayer.netServerHandler.sendPacket(packet);
         }
     }
 
     public void b(Packet packet) {
         this.a(packet);
-        if (this.a instanceof EntityPlayer) {
-            ((EntityPlayer) this.a).a.b(packet);
+        if (this.tracker instanceof EntityPlayer) {
+            ((EntityPlayer) this.tracker).netServerHandler.sendPacket(packet);
         }
     }
 
     public void a() {
-        this.a((Packet) (new Packet29DestroyEntity(this.a.id)));
+        this.a((Packet) (new Packet29DestroyEntity(this.tracker.id)));
     }
 
     public void a(EntityPlayer entityplayer) {
-        if (this.n.contains(entityplayer)) {
-            this.n.remove(entityplayer);
+        if (this.trackedPlayers.contains(entityplayer)) {
+            this.trackedPlayers.remove(entityplayer);
         }
     }
 
     public void b(EntityPlayer entityplayer) {
-        if (entityplayer != this.a) {
+        if (entityplayer != this.tracker) {
             double d0 = entityplayer.locX - (double) (this.d / 32);
             double d1 = entityplayer.locZ - (double) (this.f / 32);
 
             if (d0 >= (double) (-this.b) && d0 <= (double) this.b && d1 >= (double) (-this.b) && d1 <= (double) this.b) {
-                if (!this.n.contains(entityplayer)) {
-                    this.n.add(entityplayer);
-                    entityplayer.a.b(this.b());
-                    if (this.s) {
-                        entityplayer.a.b((Packet) (new Packet28EntityVelocity(this.a.id, this.a.motX, this.a.motY, this.a.motZ)));
+                if (!this.trackedPlayers.contains(entityplayer)) {
+                    this.trackedPlayers.add(entityplayer);
+                    entityplayer.netServerHandler.sendPacket(this.b());
+                    if (this.isMoving) {
+                        entityplayer.netServerHandler.sendPacket(new Packet28EntityVelocity(this.tracker.id, this.tracker.motX, this.tracker.motY, this.tracker.motZ));
                     }
 
-                    ItemStack[] aitemstack = this.a.k_();
+                    ItemStack[] aitemstack = this.tracker.getEquipment();
 
                     if (aitemstack != null) {
                         for (int i = 0; i < aitemstack.length; ++i) {
-                            entityplayer.a.b((Packet) (new Packet5EntityEquipment(this.a.id, i, aitemstack[i])));
+                            entityplayer.netServerHandler.sendPacket(new Packet5EntityEquipment(this.tracker.id, i, aitemstack[i]));
                         }
                     }
                 }
-            } else if (this.n.contains(entityplayer)) {
-                this.n.remove(entityplayer);
-                entityplayer.a.b((Packet) (new Packet29DestroyEntity(this.a.id)));
+            } else if (this.trackedPlayers.contains(entityplayer)) {
+                this.trackedPlayers.remove(entityplayer);
+                entityplayer.netServerHandler.sendPacket(new Packet29DestroyEntity(this.tracker.id));
             }
         }
     }
 
-    public void b(List list) {
+    public void scanPlayers(List list) {
         for (int i = 0; i < list.size(); ++i) {
             this.b((EntityPlayer) list.get(i));
         }
     }
 
     private Packet b() {
-        if (this.a instanceof EntityItem) {
-            EntityItem entityitem = (EntityItem) this.a;
+        if (this.tracker instanceof EntityItem) {
+            EntityItem entityitem = (EntityItem) this.tracker;
             Packet21PickupSpawn packet21pickupspawn = new Packet21PickupSpawn(entityitem);
 
             entityitem.locX = (double) packet21pickupspawn.b / 32.0D;
             entityitem.locY = (double) packet21pickupspawn.c / 32.0D;
             entityitem.locZ = (double) packet21pickupspawn.d / 32.0D;
             return packet21pickupspawn;
-        } else if (this.a instanceof EntityPlayer) {
-            return new Packet20NamedEntitySpawn((EntityHuman) this.a);
+        } else if (this.tracker instanceof EntityPlayer) {
+            return new Packet20NamedEntitySpawn((EntityHuman) this.tracker);
         } else {
-            if (this.a instanceof EntityMinecart) {
-                EntityMinecart entityminecart = (EntityMinecart) this.a;
+            if (this.tracker instanceof EntityMinecart) {
+                EntityMinecart entityminecart = (EntityMinecart) this.tracker;
 
-                if (entityminecart.d == 0) {
-                    return new Packet23VehicleSpawn(this.a, 10);
+                if (entityminecart.type == 0) {
+                    return new Packet23VehicleSpawn(this.tracker, 10);
                 }
 
-                if (entityminecart.d == 1) {
-                    return new Packet23VehicleSpawn(this.a, 11);
+                if (entityminecart.type == 1) {
+                    return new Packet23VehicleSpawn(this.tracker, 11);
                 }
 
-                if (entityminecart.d == 2) {
-                    return new Packet23VehicleSpawn(this.a, 12);
+                if (entityminecart.type == 2) {
+                    return new Packet23VehicleSpawn(this.tracker, 12);
                 }
             }
 
-            if (this.a instanceof EntityBoat) {
-                return new Packet23VehicleSpawn(this.a, 1);
-            } else if (this.a instanceof IAnimal) {
-                return new Packet24MobSpawn((EntityLiving) this.a);
-            } else if (this.a instanceof EntityFish) {
-                return new Packet23VehicleSpawn(this.a, 90);
-            } else if (this.a instanceof EntityArrow) {
-                return new Packet23VehicleSpawn(this.a, 60);
-            } else if (this.a instanceof EntitySnowball) {
-                return new Packet23VehicleSpawn(this.a, 61);
-            } else if (this.a instanceof EntityEgg) {
-                return new Packet23VehicleSpawn(this.a, 62);
-            } else if (this.a instanceof EntityTNTPrimed) {
-                return new Packet23VehicleSpawn(this.a, 50);
+            if (this.tracker instanceof EntityBoat) {
+                return new Packet23VehicleSpawn(this.tracker, 1);
+            } else if (this.tracker instanceof IAnimal) {
+                return new Packet24MobSpawn((EntityLiving) this.tracker);
+            } else if (this.tracker instanceof EntityFish) {
+                return new Packet23VehicleSpawn(this.tracker, 90);
+            } else if (this.tracker instanceof EntityArrow) {
+                return new Packet23VehicleSpawn(this.tracker, 60);
+            } else if (this.tracker instanceof EntitySnowball) {
+                return new Packet23VehicleSpawn(this.tracker, 61);
+            } else if (this.tracker instanceof EntityEgg) {
+                return new Packet23VehicleSpawn(this.tracker, 62);
+            } else if (this.tracker instanceof EntityTNTPrimed) {
+                return new Packet23VehicleSpawn(this.tracker, 50);
             } else {
-                if (this.a instanceof EntityFallingSand) {
-                    EntityFallingSand entityfallingsand = (EntityFallingSand) this.a;
+                if (this.tracker instanceof EntityFallingSand) {
+                    EntityFallingSand entityfallingsand = (EntityFallingSand) this.tracker;
 
                     if (entityfallingsand.a == Block.SAND.id) {
-                        return new Packet23VehicleSpawn(this.a, 70);
+                        return new Packet23VehicleSpawn(this.tracker, 70);
                     }
 
                     if (entityfallingsand.a == Block.GRAVEL.id) {
-                        return new Packet23VehicleSpawn(this.a, 71);
+                        return new Packet23VehicleSpawn(this.tracker, 71);
                     }
                 }
 
-                if (this.a instanceof EntityPainting) {
-                    return new Packet25EntityPainting((EntityPainting) this.a);
+                if (this.tracker instanceof EntityPainting) {
+                    return new Packet25EntityPainting((EntityPainting) this.tracker);
                 } else {
-                    throw new IllegalArgumentException("Don\'t know how to add " + this.a.getClass() + "!");
+                    throw new IllegalArgumentException("Don\'t know how to add " + this.tracker.getClass() + "!");
                 }
             }
         }
     }
 
     public void c(EntityPlayer entityplayer) {
-        if (this.n.contains(entityplayer)) {
-            this.n.remove(entityplayer);
-            entityplayer.a.b((Packet) (new Packet29DestroyEntity(this.a.id)));
+        if (this.trackedPlayers.contains(entityplayer)) {
+            this.trackedPlayers.remove(entityplayer);
+            entityplayer.netServerHandler.sendPacket(new Packet29DestroyEntity(this.tracker.id));
         }
     }
 }
