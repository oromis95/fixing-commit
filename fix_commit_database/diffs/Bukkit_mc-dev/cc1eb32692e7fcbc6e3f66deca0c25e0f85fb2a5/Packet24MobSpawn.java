@@ -21,12 +21,12 @@ public class Packet24MobSpawn extends Packet {
     public Packet24MobSpawn(EntityLiving entityliving) {
         this.a = entityliving.id;
         this.b = (byte) EntityTypes.a(entityliving);
-        this.c = MathHelper.b(entityliving.locX * 32.0D);
-        this.d = MathHelper.b(entityliving.locY * 32.0D);
-        this.e = MathHelper.b(entityliving.locZ * 32.0D);
+        this.c = MathHelper.floor(entityliving.locX * 32.0D);
+        this.d = MathHelper.floor(entityliving.locY * 32.0D);
+        this.e = MathHelper.floor(entityliving.locZ * 32.0D);
         this.f = (byte) ((int) (entityliving.yaw * 256.0F / 360.0F));
         this.g = (byte) ((int) (entityliving.pitch * 256.0F / 360.0F));
-        this.h = entityliving.O();
+        this.h = entityliving.T();
     }
 
     public void a(DataInputStream datainputstream) {
