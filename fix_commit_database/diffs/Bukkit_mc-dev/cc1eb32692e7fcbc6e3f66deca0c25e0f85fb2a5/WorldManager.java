@@ -2,20 +2,20 @@ package net.minecraft.server;
 
 public class WorldManager implements IWorldAccess {
 
-    private MinecraftServer a;
+    private MinecraftServer server;
 
     public WorldManager(MinecraftServer minecraftserver) {
-        this.a = minecraftserver;
+        this.server = minecraftserver;
     }
 
     public void a(String s, double d0, double d1, double d2, double d3, double d4, double d5) {}
 
     public void a(Entity entity) {
-        this.a.k.a(entity);
+        this.server.tracker.a(entity);
     }
 
     public void b(Entity entity) {
-        this.a.k.b(entity);
+        this.server.tracker.untrackEntity(entity);
     }
 
     public void a(String s, double d0, double d1, double d2, float f, float f1) {}
@@ -25,12 +25,12 @@ public class WorldManager implements IWorldAccess {
     public void a() {}
 
     public void a(int i, int j, int k) {
-        this.a.f.a(i, j, k);
+        this.server.serverConfigurationManager.flagDirty(i, j, k);
     }
 
     public void a(String s, int i, int j, int k) {}
 
     public void a(int i, int j, int k, TileEntity tileentity) {
-        this.a.f.a(i, j, k, tileentity);
+        this.server.serverConfigurationManager.a(i, j, k, tileentity);
     }
 }
