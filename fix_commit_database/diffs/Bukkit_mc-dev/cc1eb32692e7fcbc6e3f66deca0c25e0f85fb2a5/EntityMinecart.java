@@ -4,16 +4,16 @@ import java.util.List;
 
 public class EntityMinecart extends Entity implements IInventory {
 
-    private ItemStack[] h;
-    public int a;
+    private ItemStack[] items;
+    public int damage;
     public int b;
     public int c;
     private boolean i;
-    public int d;
+    public int type;
     public int e;
     public double f;
     public double g;
-    private static final int[][][] j = new int[][][] { { { 0, 0, -1}, { 0, 0, 1}}, { { -1, 0, 0}, { 1, 0, 0}}, { { -1, -1, 0}, { 1, 0, 0}}, { { -1, 0, 0}, { 1, -1, 0}}, { { 0, 0, -1}, { 0, -1, 1}}, { { 0, -1, -1}, { 0, 0, 1}}, { { 0, 0, 1}, { 1, 0, 0}}, { { 0, 0, 1}, { -1, 0, 0}}, { { 0, 0, -1}, { -1, 0, 0}}, { { 0, 0, -1}, { 1, 0, 0}}};
+    private static final int[][][] matrix = new int[][][] { { { 0, 0, -1}, { 0, 0, 1}}, { { -1, 0, 0}, { 1, 0, 0}}, { { -1, -1, 0}, { 1, 0, 0}}, { { -1, 0, 0}, { 1, -1, 0}}, { { 0, 0, -1}, { 0, -1, 1}}, { { 0, -1, -1}, { 0, 0, 1}}, { { 0, 0, 1}, { 1, 0, 0}}, { { 0, 0, 1}, { -1, 0, 0}}, { { 0, 0, -1}, { -1, 0, 0}}, { { 0, 0, -1}, { 1, 0, 0}}};
     private int k;
     private double l;
     private double m;
@@ -23,15 +23,18 @@ public class EntityMinecart extends Entity implements IInventory {
 
     public EntityMinecart(World world) {
         super(world);
-        this.h = new ItemStack[36];
-        this.a = 0;
+        this.items = new ItemStack[36];
+        this.damage = 0;
         this.b = 0;
         this.c = 1;
         this.i = false;
-        this.aC = true;
-        this.a(0.98F, 0.7F);
+        this.aD = true;
+        this.b(0.98F, 0.7F);
         this.height = this.width / 2.0F;
-        this.bg = false;
+    }
+
+    protected boolean l() {
+        return false;
     }
 
     protected void a() {}
@@ -50,35 +53,35 @@ public class EntityMinecart extends Entity implements IInventory {
 
     public EntityMinecart(World world, double d0, double d1, double d2, int i) {
         this(world);
-        this.a(d0, d1 + (double) this.height, d2);
+        this.setPosition(d0, d1 + (double) this.height, d2);
         this.motX = 0.0D;
         this.motY = 0.0D;
         this.motZ = 0.0D;
         this.lastX = d0;
         this.lastY = d1;
         this.lastZ = d2;
-        this.d = i;
+        this.type = i;
     }
 
     public double k() {
         return (double) this.width * 0.0D - 0.30000001192092896D;
     }
 
-    public boolean a(Entity entity, int i) {
+    public boolean damageEntity(Entity entity, int i) {
         if (!this.world.isStatic && !this.dead) {
             this.c = -this.c;
             this.b = 10;
-            this.R();
-            this.a += i * 10;
-            if (this.a > 40) {
+            this.W();
+            this.damage += i * 10;
+            if (this.damage > 40) {
                 this.a(Item.MINECART.id, 1, 0.0F);
-                if (this.d == 1) {
+                if (this.type == 1) {
                     this.a(Block.CHEST.id, 1, 0.0F);
-                } else if (this.d == 2) {
+                } else if (this.type == 2) {
                     this.a(Block.FURNACE.id, 1, 0.0F);
                 }
 
-                this.C();
+                this.die();
             }
 
             return true;
@@ -91,9 +94,9 @@ public class EntityMinecart extends Entity implements IInventory {
         return !this.dead;
     }
 
-    public void C() {
-        for (int i = 0; i < this.m_(); ++i) {
-            ItemStack itemstack = this.c_(i);
+    public void die() {
+        for (int i = 0; i < this.getSize(); ++i) {
+            ItemStack itemstack = this.getItem(i);
 
             if (itemstack != null) {
                 float f = this.random.nextFloat() * 0.8F + 0.1F;
@@ -108,18 +111,18 @@ public class EntityMinecart extends Entity implements IInventory {
                     }
 
                     itemstack.count -= j;
-                    EntityItem entityitem = new EntityItem(this.world, this.locX + (double) f, this.locY + (double) f1, this.locZ + (double) f2, new ItemStack(itemstack.id, j, itemstack.h()));
+                    EntityItem entityitem = new EntityItem(this.world, this.locX + (double) f, this.locY + (double) f1, this.locZ + (double) f2, new ItemStack(itemstack.id, j, itemstack.getData()));
                     float f3 = 0.05F;
 
                     entityitem.motX = (double) ((float) this.random.nextGaussian() * f3);
                     entityitem.motY = (double) ((float) this.random.nextGaussian() * f3 + 0.2F);
                     entityitem.motZ = (double) ((float) this.random.nextGaussian() * f3);
-                    this.world.a((Entity) entityitem);
+                    this.world.addEntity(entityitem);
                 }
             }
         }
 
-        super.C();
+        super.die();
     }
 
     public void f_() {
@@ -127,8 +130,8 @@ public class EntityMinecart extends Entity implements IInventory {
             --this.b;
         }
 
-        if (this.a > 0) {
-            --this.a;
+        if (this.damage > 0) {
+            --this.damage;
         }
 
         double d0;
@@ -150,10 +153,10 @@ public class EntityMinecart extends Entity implements IInventory {
                 this.yaw = (float) ((double) this.yaw + d0 / (double) this.k);
                 this.pitch = (float) ((double) this.pitch + (this.p - (double) this.pitch) / (double) this.k);
                 --this.k;
-                this.a(d1, d2, d3);
+                this.setPosition(d1, d2, d3);
                 this.c(this.yaw, this.pitch);
             } else {
-                this.a(this.locX, this.locY, this.locZ);
+                this.setPosition(this.locX, this.locY, this.locZ);
                 this.c(this.yaw, this.pitch);
             }
         } else {
@@ -161,9 +164,9 @@ public class EntityMinecart extends Entity implements IInventory {
             this.lastY = this.locY;
             this.lastZ = this.locZ;
             this.motY -= 0.03999999910593033D;
-            int i = MathHelper.b(this.locX);
-            int j = MathHelper.b(this.locY);
-            int k = MathHelper.b(this.locZ);
+            int i = MathHelper.floor(this.locX);
+            int j = MathHelper.floor(this.locY);
+            int k = MathHelper.floor(this.locZ);
 
             if (this.world.getTypeId(i, j - 1, k) == Block.RAILS.id) {
                 --j;
@@ -198,7 +201,7 @@ public class EntityMinecart extends Entity implements IInventory {
                     this.motZ -= d0;
                 }
 
-                int[][] aint = j[l];
+                int[][] aint = matrix[l];
                 double d5 = (double) (aint[1][0] - aint[0][0]);
                 double d6 = (double) (aint[1][2] - aint[0][2]);
                 double d7 = Math.sqrt(d5 * d5 + d6 * d6);
@@ -240,7 +243,7 @@ public class EntityMinecart extends Entity implements IInventory {
 
                 this.locX = d11 + d5 * d10;
                 this.locZ = d12 + d6 * d10;
-                this.a(this.locX, this.locY + (double) this.height, this.locZ);
+                this.setPosition(this.locX, this.locY + (double) this.height, this.locZ);
                 d15 = this.motX;
                 d17 = this.motZ;
                 if (this.passenger != null) {
@@ -264,11 +267,11 @@ public class EntityMinecart extends Entity implements IInventory {
                     d17 = d4;
                 }
 
-                this.c(d15, 0.0D, d17);
-                if (aint[0][1] != 0 && MathHelper.b(this.locX) - i == aint[0][0] && MathHelper.b(this.locZ) - k == aint[0][2]) {
-                    this.a(this.locX, this.locY + (double) aint[0][1], this.locZ);
-                } else if (aint[1][1] != 0 && MathHelper.b(this.locX) - i == aint[1][0] && MathHelper.b(this.locZ) - k == aint[1][2]) {
-                    this.a(this.locX, this.locY + (double) aint[1][1], this.locZ);
+                this.move(d15, 0.0D, d17);
+                if (aint[0][1] != 0 && MathHelper.floor(this.locX) - i == aint[0][0] && MathHelper.floor(this.locZ) - k == aint[0][2]) {
+                    this.setPosition(this.locX, this.locY + (double) aint[0][1], this.locZ);
+                } else if (aint[1][1] != 0 && MathHelper.floor(this.locX) - i == aint[1][0] && MathHelper.floor(this.locZ) - k == aint[1][2]) {
+                    this.setPosition(this.locX, this.locY + (double) aint[1][1], this.locZ);
                 }
 
                 if (this.passenger != null) {
@@ -276,7 +279,7 @@ public class EntityMinecart extends Entity implements IInventory {
                     this.motY *= 0.0D;
                     this.motZ *= 0.996999979019165D;
                 } else {
-                    if (this.d == 2) {
+                    if (this.type == 2) {
                         d16 = (double) MathHelper.a(this.f * this.f + this.g * this.g);
                         if (d16 > 0.01D) {
                             flag = true;
@@ -312,11 +315,11 @@ public class EntityMinecart extends Entity implements IInventory {
                         this.motZ = this.motZ / d9 * (d9 + d19);
                     }
 
-                    this.a(this.locX, vec3d1.b, this.locZ);
+                    this.setPosition(this.locX, vec3d1.b, this.locZ);
                 }
 
-                int i1 = MathHelper.b(this.locX);
-                int j1 = MathHelper.b(this.locZ);
+                int i1 = MathHelper.floor(this.locX);
+                int j1 = MathHelper.floor(this.locZ);
 
                 if (i1 != i || j1 != k) {
                     d9 = Math.sqrt(this.motX * this.motX + this.motZ * this.motZ);
@@ -324,7 +327,7 @@ public class EntityMinecart extends Entity implements IInventory {
                     this.motZ = d9 * (double) (j1 - k);
                 }
 
-                if (this.d == 2) {
+                if (this.type == 2) {
                     double d20 = (double) MathHelper.a(this.f * this.f + this.g * this.g);
 
                     if (d20 > 0.01D && this.motX * this.motX + this.motZ * this.motZ > 0.0010D) {
@@ -362,7 +365,7 @@ public class EntityMinecart extends Entity implements IInventory {
                     this.motZ *= 0.5D;
                 }
 
-                this.c(this.motX, this.motY, this.motZ);
+                this.move(this.motX, this.motY, this.motZ);
                 if (!this.onGround) {
                     this.motX *= 0.949999988079071D;
                     this.motY *= 0.949999988079071D;
@@ -404,7 +407,7 @@ public class EntityMinecart extends Entity implements IInventory {
                     Entity entity = (Entity) list.get(k1);
 
                     if (entity != this.passenger && entity.e_() && entity instanceof EntityMinecart) {
-                        entity.h(this);
+                        entity.collide(this);
                     }
                 }
             }
@@ -425,9 +428,9 @@ public class EntityMinecart extends Entity implements IInventory {
     }
 
     public Vec3D g(double d0, double d1, double d2) {
-        int i = MathHelper.b(d0);
-        int j = MathHelper.b(d1);
-        int k = MathHelper.b(d2);
+        int i = MathHelper.floor(d0);
+        int j = MathHelper.floor(d1);
+        int k = MathHelper.floor(d2);
 
         if (this.world.getTypeId(i, j - 1, k) == Block.RAILS.id) {
             --j;
@@ -441,7 +444,7 @@ public class EntityMinecart extends Entity implements IInventory {
                 d1 = (double) (j + 1);
             }
 
-            int[][] aint = j[l];
+            int[][] aint = matrix[l];
             double d3 = 0.0D;
             double d4 = (double) i + 0.5D + (double) aint[0][0] * 0.5D;
             double d5 = (double) j + 0.5D + (double) aint[0][1] * 0.5D;
@@ -478,27 +481,27 @@ public class EntityMinecart extends Entity implements IInventory {
                 d1 += 0.5D;
             }
 
-            return Vec3D.b(d0, d1, d2);
+            return Vec3D.create(d0, d1, d2);
         } else {
             return null;
         }
     }
 
     protected void a(NBTTagCompound nbttagcompound) {
-        nbttagcompound.a("Type", this.d);
-        if (this.d == 2) {
+        nbttagcompound.a("Type", this.type);
+        if (this.type == 2) {
             nbttagcompound.a("PushX", this.f);
             nbttagcompound.a("PushZ", this.g);
             nbttagcompound.a("Fuel", (short) this.e);
-        } else if (this.d == 1) {
+        } else if (this.type == 1) {
             NBTTagList nbttaglist = new NBTTagList();
 
-            for (int i = 0; i < this.h.length; ++i) {
-                if (this.h[i] != null) {
+            for (int i = 0; i < this.items.length; ++i) {
+                if (this.items[i] != null) {
                     NBTTagCompound nbttagcompound1 = new NBTTagCompound();
 
                     nbttagcompound1.a("Slot", (byte) i);
-                    this.h[i].a(nbttagcompound1);
+                    this.items[i].a(nbttagcompound1);
                     nbttaglist.a((NBTBase) nbttagcompound1);
                 }
             }
@@ -508,32 +511,32 @@ public class EntityMinecart extends Entity implements IInventory {
     }
 
     protected void b(NBTTagCompound nbttagcompound) {
-        this.d = nbttagcompound.e("Type");
-        if (this.d == 2) {
+        this.type = nbttagcompound.e("Type");
+        if (this.type == 2) {
             this.f = nbttagcompound.h("PushX");
             this.g = nbttagcompound.h("PushZ");
             this.e = nbttagcompound.d("Fuel");
-        } else if (this.d == 1) {
+        } else if (this.type == 1) {
             NBTTagList nbttaglist = nbttagcompound.l("Items");
 
-            this.h = new ItemStack[this.m_()];
+            this.items = new ItemStack[this.getSize()];
 
             for (int i = 0; i < nbttaglist.c(); ++i) {
                 NBTTagCompound nbttagcompound1 = (NBTTagCompound) nbttaglist.a(i);
                 int j = nbttagcompound1.c("Slot") & 255;
 
-                if (j >= 0 && j < this.h.length) {
-                    this.h[j] = new ItemStack(nbttagcompound1);
+                if (j >= 0 && j < this.items.length) {
+                    this.items[j] = new ItemStack(nbttagcompound1);
                 }
             }
         }
     }
 
-    public void h(Entity entity) {
+    public void collide(Entity entity) {
         if (!this.world.isStatic) {
             if (entity != this.passenger) {
-                if (entity instanceof EntityLiving && !(entity instanceof EntityHuman) && this.d == 0 && this.motX * this.motX + this.motZ * this.motZ > 0.01D && this.passenger == null && entity.vehicle == null) {
-                    entity.b((Entity) this);
+                if (entity instanceof EntityLiving && !(entity instanceof EntityHuman) && this.type == 0 && this.motX * this.motX + this.motZ * this.motZ > 0.01D && this.passenger == null && entity.vehicle == null) {
+                    entity.mount(this);
                 }
 
                 double d0 = entity.locX - this.locX;
@@ -562,13 +565,13 @@ public class EntityMinecart extends Entity implements IInventory {
                         double d4 = entity.motX + this.motX;
                         double d5 = entity.motZ + this.motZ;
 
-                        if (((EntityMinecart) entity).d == 2 && this.d != 2) {
+                        if (((EntityMinecart) entity).type == 2 && this.type != 2) {
                             this.motX *= 0.20000000298023224D;
                             this.motZ *= 0.20000000298023224D;
                             this.f(entity.motX - d0, 0.0D, entity.motZ - d1);
                             entity.motX *= 0.699999988079071D;
                             entity.motZ *= 0.699999988079071D;
-                        } else if (((EntityMinecart) entity).d != 2 && this.d == 2) {
+                        } else if (((EntityMinecart) entity).type != 2 && this.type == 2) {
                             entity.motX *= 0.20000000298023224D;
                             entity.motZ *= 0.20000000298023224D;
                             entity.f(this.motX + d0, 0.0D, this.motZ + d1);
@@ -593,26 +596,26 @@ public class EntityMinecart extends Entity implements IInventory {
         }
     }
 
-    public int m_() {
+    public int getSize() {
         return 27;
     }
 
-    public ItemStack c_(int i) {
-        return this.h[i];
+    public ItemStack getItem(int i) {
+        return this.items[i];
     }
 
     public ItemStack a(int i, int j) {
-        if (this.h[i] != null) {
+        if (this.items[i] != null) {
             ItemStack itemstack;
 
-            if (this.h[i].count <= j) {
-                itemstack = this.h[i];
-                this.h[i] = null;
+            if (this.items[i].count <= j) {
+                itemstack = this.items[i];
+                this.items[i] = null;
                 return itemstack;
             } else {
-                itemstack = this.h[i].a(j);
-                if (this.h[i].count == 0) {
-                    this.h[i] = null;
+                itemstack = this.items[i].a(j);
+                if (this.items[i].count == 0) {
+                    this.items[i] = null;
                 }
 
                 return itemstack;
@@ -622,42 +625,42 @@ public class EntityMinecart extends Entity implements IInventory {
         }
     }
 
-    public void a(int i, ItemStack itemstack) {
-        this.h[i] = itemstack;
-        if (itemstack != null && itemstack.count > this.n_()) {
-            itemstack.count = this.n_();
+    public void setItem(int i, ItemStack itemstack) {
+        this.items[i] = itemstack;
+        if (itemstack != null && itemstack.count > this.getMaxStackSize()) {
+            itemstack.count = this.getMaxStackSize();
         }
     }
 
-    public String c() {
+    public String getName() {
         return "Minecart";
     }
 
-    public int n_() {
+    public int getMaxStackSize() {
         return 64;
     }
 
-    public void h() {}
+    public void update() {}
 
     public boolean a(EntityHuman entityhuman) {
-        if (this.d == 0) {
+        if (this.type == 0) {
             if (this.passenger != null && this.passenger instanceof EntityHuman && this.passenger != entityhuman) {
                 return true;
             }
 
             if (!this.world.isStatic) {
-                entityhuman.b((Entity) this);
+                entityhuman.mount(this);
             }
-        } else if (this.d == 1) {
+        } else if (this.type == 1) {
             if (!this.world.isStatic) {
                 entityhuman.a((IInventory) this);
             }
-        } else if (this.d == 2) {
-            ItemStack itemstack = entityhuman.inventory.b();
+        } else if (this.type == 2) {
+            ItemStack itemstack = entityhuman.inventory.getItemInHand();
 
             if (itemstack != null && itemstack.id == Item.COAL.id) {
                 if (--itemstack.count == 0) {
-                    entityhuman.inventory.a(entityhuman.inventory.c, (ItemStack) null);
+                    entityhuman.inventory.setItem(entityhuman.inventory.itemInHandIndex, (ItemStack) null);
                 }
 
                 this.e += 1200;
