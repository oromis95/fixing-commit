@@ -7,20 +7,19 @@ public class EntityTNTPrimed extends Entity {
     public EntityTNTPrimed(World world) {
         super(world);
         this.a = 0;
-        this.aC = true;
-        this.a(0.98F, 0.98F);
+        this.aD = true;
+        this.b(0.98F, 0.98F);
         this.height = this.width / 2.0F;
     }
 
     public EntityTNTPrimed(World world, double d0, double d1, double d2) {
         this(world);
-        this.a(d0, d1, d2);
+        this.setPosition(d0, d1, d2);
         float f = (float) (Math.random() * 3.1415927410125732D * 2.0D);
 
-        this.motX = (double) (-MathHelper.a(f * 3.1415927F / 180.0F) * 0.02F);
+        this.motX = (double) (-MathHelper.sin(f * 3.1415927F / 180.0F) * 0.02F);
         this.motY = 0.20000000298023224D;
-        this.motZ = (double) (-MathHelper.b(f * 3.1415927F / 180.0F) * 0.02F);
-        this.bg = false;
+        this.motZ = (double) (-MathHelper.cos(f * 3.1415927F / 180.0F) * 0.02F);
         this.a = 80;
         this.lastX = d0;
         this.lastY = d1;
@@ -29,6 +28,10 @@ public class EntityTNTPrimed extends Entity {
 
     protected void a() {}
 
+    protected boolean l() {
+        return false;
+    }
+
     public boolean d_() {
         return !this.dead;
     }
@@ -38,7 +41,7 @@ public class EntityTNTPrimed extends Entity {
         this.lastY = this.locY;
         this.lastZ = this.locZ;
         this.motY -= 0.03999999910593033D;
-        this.c(this.motX, this.motY, this.motZ);
+        this.move(this.motX, this.motY, this.motZ);
         this.motX *= 0.9800000190734863D;
         this.motY *= 0.9800000190734863D;
         this.motZ *= 0.9800000190734863D;
@@ -49,14 +52,14 @@ public class EntityTNTPrimed extends Entity {
         }
 
         if (this.a-- <= 0) {
-            this.C();
-            this.h();
+            this.die();
+            this.explode();
         } else {
             this.world.a("smoke", this.locX, this.locY + 0.5D, this.locZ, 0.0D, 0.0D, 0.0D);
         }
     }
 
-    private void h() {
+    private void explode() {
         float f = 4.0F;
 
         this.world.a((Entity) null, this.locX, this.locY, this.locZ, f);
