@@ -6,20 +6,20 @@ import java.io.DataOutputStream;
 public class Packet7UseEntity extends Packet {
 
     public int a;
-    public int b;
+    public int target;
     public int c;
 
     public Packet7UseEntity() {}
 
     public void a(DataInputStream datainputstream) {
         this.a = datainputstream.readInt();
-        this.b = datainputstream.readInt();
+        this.target = datainputstream.readInt();
         this.c = datainputstream.readByte();
     }
 
     public void a(DataOutputStream dataoutputstream) {
         dataoutputstream.writeInt(this.a);
-        dataoutputstream.writeInt(this.b);
+        dataoutputstream.writeInt(this.target);
         dataoutputstream.writeByte(this.c);
     }
 
