@@ -20,22 +20,23 @@ public class ChunkRegionLoader implements IChunkLoader {
         if (datainputstream != null) {
             NBTTagCompound nbttagcompound = CompressedStreamTools.a((DataInput) datainputstream);
 
-            if (!nbttagcompound.b("Level")) {
+            if (!nbttagcompound.hasKey("Level")) {
                 System.out.println("Chunk file at " + i + "," + j + " is missing level data, skipping");
                 return null;
-            } else if (!nbttagcompound.k("Level").b("Blocks")) {
+            } else if (!nbttagcompound.k("Level").hasKey("Blocks")) {
                 System.out.println("Chunk file at " + i + "," + j + " is missing block data, skipping");
                 return null;
             } else {
                 Chunk chunk = ChunkLoader.a(world, nbttagcompound.k("Level"));
 
                 if (!chunk.a(i, j)) {
-                    System.out.println("Chunk file at " + i + "," + j + " is in the wrong location; relocating. (Expected " + i + ", " + j + ", got " + chunk.j + ", " + chunk.k + ")");
+                    System.out.println("Chunk file at " + i + "," + j + " is in the wrong location; relocating. (Expected " + i + ", " + j + ", got " + chunk.x + ", " + chunk.z + ")");
                     nbttagcompound.a("xPos", i);
                     nbttagcompound.a("zPos", j);
                     chunk = ChunkLoader.a(world, nbttagcompound.k("Level"));
                 }
 
+                chunk.h();
                 return chunk;
             }
         } else {
@@ -44,10 +45,10 @@ public class ChunkRegionLoader implements IChunkLoader {
     }
 
     public void a(World world, Chunk chunk) {
-        world.i();
+        world.j();
 
         try {
-            DataOutputStream dataoutputstream = RegionFileCache.d(this.a, chunk.j, chunk.k);
+            DataOutputStream dataoutputstream = RegionFileCache.d(this.a, chunk.x, chunk.z);
             NBTTagCompound nbttagcompound = new NBTTagCompound();
             NBTTagCompound nbttagcompound1 = new NBTTagCompound();
 
@@ -55,9 +56,9 @@ public class ChunkRegionLoader implements IChunkLoader {
             ChunkLoader.a(chunk, world, nbttagcompound1);
             CompressedStreamTools.a(nbttagcompound, (DataOutput) dataoutputstream);
             dataoutputstream.close();
-            WorldData worlddata = world.n();
+            WorldData worlddata = world.p();
 
-            worlddata.b(worlddata.g() + (long) RegionFileCache.b(this.a, chunk.j, chunk.k));
+            worlddata.b(worlddata.g() + (long) RegionFileCache.b(this.a, chunk.x, chunk.z));
         } catch (Exception exception) {
             exception.printStackTrace();
         }
