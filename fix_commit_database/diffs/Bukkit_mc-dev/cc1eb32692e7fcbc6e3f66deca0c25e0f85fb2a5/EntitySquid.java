@@ -20,7 +20,7 @@ public class EntitySquid extends EntityWaterAnimal {
     public EntitySquid(World world) {
         super(world);
         this.texture = "/mob/squid.png";
-        this.a(0.95F, 0.95F);
+        this.b(0.95F, 0.95F);
         this.l = 1.0F / (this.random.nextFloat() + 1.0F) * 0.2F;
     }
 
@@ -52,7 +52,7 @@ public class EntitySquid extends EntityWaterAnimal {
         return 0;
     }
 
-    protected void o() {
+    protected void p() {
         int i = this.random.nextInt(3) + 1;
 
         for (int j = 0; j < i; ++j) {
@@ -68,8 +68,8 @@ public class EntitySquid extends EntityWaterAnimal {
         return this.world.a(this.boundingBox.b(0.0D, -0.6000000238418579D, 0.0D), Material.WATER, this);
     }
 
-    public void q() {
-        super.q();
+    public void r() {
+        super.r();
         this.b = this.a;
         this.f = this.c;
         this.h = this.g;
@@ -87,7 +87,7 @@ public class EntitySquid extends EntityWaterAnimal {
 
             if (this.g < 3.1415927F) {
                 f = this.g / 3.1415927F;
-                this.i = MathHelper.a(f * f * 3.1415927F) * 3.1415927F * 0.25F;
+                this.i = MathHelper.sin(f * f * 3.1415927F) * 3.1415927F * 0.25F;
                 if ((double) f > 0.75D) {
                     this.k = 1.0F;
                     this.m = 1.0F;
@@ -112,7 +112,7 @@ public class EntitySquid extends EntityWaterAnimal {
             this.c += 3.1415927F * this.m * 1.5F;
             this.a += (-((float) Math.atan2((double) f, this.motY)) * 180.0F / 3.1415927F - this.a) * 0.1F;
         } else {
-            this.i = MathHelper.e(MathHelper.a(this.g)) * 3.1415927F * 0.25F;
+            this.i = MathHelper.abs(MathHelper.sin(this.g)) * 3.1415927F * 0.25F;
             if (!this.T) {
                 this.motX = 0.0D;
                 this.motY -= 0.08D;
@@ -124,17 +124,17 @@ public class EntitySquid extends EntityWaterAnimal {
         }
     }
 
-    public void b(float f, float f1) {
-        this.c(this.motX, this.motY, this.motZ);
+    public void a(float f, float f1) {
+        this.move(this.motX, this.motY, this.motZ);
     }
 
     protected void c_() {
         if (this.random.nextInt(50) == 0 || !this.bv || this.n == 0.0F && this.o == 0.0F && this.p == 0.0F) {
             float f = this.random.nextFloat() * 3.1415927F * 2.0F;
 
-            this.n = MathHelper.b(f) * 0.2F;
+            this.n = MathHelper.cos(f) * 0.2F;
             this.o = -0.1F + this.random.nextFloat() * 0.2F;
-            this.p = MathHelper.a(f) * 0.2F;
+            this.p = MathHelper.sin(f) * 0.2F;
         }
     }
 }
