@@ -2,7 +2,7 @@ package net.minecraft.server;
 
 public class EntityCreeper extends EntityMonster {
 
-    int a;
+    int fuseTicks;
     int b;
 
     public EntityCreeper(World world) {
@@ -24,21 +24,21 @@ public class EntityCreeper extends EntityMonster {
     }
 
     public void f_() {
-        this.b = this.a;
+        this.b = this.fuseTicks;
         if (this.world.isStatic) {
-            int i = this.r();
+            int i = this.v();
 
-            if (i > 0 && this.a == 0) {
-                this.world.a(this, "random.fuse", 1.0F, 0.5F);
+            if (i > 0 && this.fuseTicks == 0) {
+                this.world.makeSound(this, "random.fuse", 1.0F, 0.5F);
             }
 
-            this.a += i;
-            if (this.a < 0) {
-                this.a = 0;
+            this.fuseTicks += i;
+            if (this.fuseTicks < 0) {
+                this.fuseTicks = 0;
             }
 
-            if (this.a >= 30) {
-                this.a = 30;
+            if (this.fuseTicks >= 30) {
+                this.fuseTicks = 30;
             }
         }
 
@@ -61,24 +61,24 @@ public class EntityCreeper extends EntityMonster {
     }
 
     protected void a(Entity entity, float f) {
-        int i = this.r();
+        int i = this.v();
 
         if ((i > 0 || f >= 3.0F) && (i <= 0 || f >= 7.0F)) {
             this.e(-1);
-            --this.a;
-            if (this.a < 0) {
-                this.a = 0;
+            --this.fuseTicks;
+            if (this.fuseTicks < 0) {
+                this.fuseTicks = 0;
             }
         } else {
-            if (this.a == 0) {
-                this.world.a(this, "random.fuse", 1.0F, 0.5F);
+            if (this.fuseTicks == 0) {
+                this.world.makeSound(this, "random.fuse", 1.0F, 0.5F);
             }
 
             this.e(1);
-            ++this.a;
-            if (this.a >= 30) {
+            ++this.fuseTicks;
+            if (this.fuseTicks >= 30) {
                 this.world.a(this, this.locX, this.locY, this.locZ, 3.0F);
-                this.C();
+                this.die();
             }
 
             this.e = true;
@@ -89,7 +89,7 @@ public class EntityCreeper extends EntityMonster {
         return Item.SULPHUR.id;
     }
 
-    private int r() {
+    private int v() {
         return this.datawatcher.a(16);
     }
 
