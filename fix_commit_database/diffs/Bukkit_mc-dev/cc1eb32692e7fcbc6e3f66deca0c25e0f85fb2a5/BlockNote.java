@@ -10,14 +10,14 @@ public class BlockNote extends BlockContainer {
         return this.textureId;
     }
 
-    public void a(World world, int i, int j, int k, int l) {
-        if (l > 0 && Block.byId[l].c()) {
-            boolean flag = world.o(i, j, k);
+    public void doPhysics(World world, int i, int j, int k, int l) {
+        if (l > 0 && Block.byId[l].isPowerSource()) {
+            boolean flag = world.isBlockPowered(i, j, k);
             TileEntityNote tileentitynote = (TileEntityNote) world.getTileEntity(i, j, k);
 
             if (tileentitynote.b != flag) {
                 if (flag) {
-                    tileentitynote.a(world, i, j, k);
+                    tileentitynote.play(world, i, j, k);
                 }
 
                 tileentitynote.b = flag;
@@ -25,14 +25,14 @@ public class BlockNote extends BlockContainer {
         }
     }
 
-    public boolean a(World world, int i, int j, int k, EntityHuman entityhuman) {
+    public boolean interact(World world, int i, int j, int k, EntityHuman entityhuman) {
         if (world.isStatic) {
             return true;
         } else {
             TileEntityNote tileentitynote = (TileEntityNote) world.getTileEntity(i, j, k);
 
             tileentitynote.a();
-            tileentitynote.a(world, i, j, k);
+            tileentitynote.play(world, i, j, k);
             return true;
         }
     }
@@ -41,7 +41,7 @@ public class BlockNote extends BlockContainer {
         if (!world.isStatic) {
             TileEntityNote tileentitynote = (TileEntityNote) world.getTileEntity(i, j, k);
 
-            tileentitynote.a(world, i, j, k);
+            tileentitynote.play(world, i, j, k);
         }
     }
 
@@ -69,7 +69,7 @@ public class BlockNote extends BlockContainer {
             s = "bassattack";
         }
 
-        world.a((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "note." + s, 3.0F, f);
+        world.makeSound((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "note." + s, 3.0F, f);
         world.a("note", (double) i + 0.5D, (double) j + 1.2D, (double) k + 0.5D, (double) i1 / 24.0D, 0.0D, 0.0D);
     }
 }
