@@ -74,7 +74,7 @@ public class WorldGenLakes extends WorldGenerator {
             for (i2 = 0; i2 < 16; ++i2) {
                 for (j2 = 0; j2 < 8; ++j2) {
                     if (aboolean[(i1 * 16 + i2) * 8 + j2]) {
-                        world.setTypeId(i + i1, j + j2, k + i2, j2 >= 4 ? 0 : this.a);
+                        world.setRawTypeId(i + i1, j + j2, k + i2, j2 >= 4 ? 0 : this.a);
                     }
                 }
             }
@@ -84,7 +84,7 @@ public class WorldGenLakes extends WorldGenerator {
             for (i2 = 0; i2 < 16; ++i2) {
                 for (j2 = 4; j2 < 8; ++j2) {
                     if (aboolean[(i1 * 16 + i2) * 8 + j2] && world.getTypeId(i + i1, j + j2 - 1, k + i2) == Block.DIRT.id && world.a(EnumSkyBlock.SKY, i + i1, j + j2, k + i2) > 0) {
-                        world.setTypeId(i + i1, j + j2 - 1, k + i2, Block.GRASS.id);
+                        world.setRawTypeId(i + i1, j + j2 - 1, k + i2, Block.GRASS.id);
                     }
                 }
             }
@@ -96,7 +96,7 @@ public class WorldGenLakes extends WorldGenerator {
                     for (j2 = 0; j2 < 8; ++j2) {
                         flag = !aboolean[(i1 * 16 + i2) * 8 + j2] && (i1 < 15 && aboolean[((i1 + 1) * 16 + i2) * 8 + j2] || i1 > 0 && aboolean[((i1 - 1) * 16 + i2) * 8 + j2] || i2 < 15 && aboolean[(i1 * 16 + i2 + 1) * 8 + j2] || i2 > 0 && aboolean[(i1 * 16 + (i2 - 1)) * 8 + j2] || j2 < 7 && aboolean[(i1 * 16 + i2) * 8 + j2 + 1] || j2 > 0 && aboolean[(i1 * 16 + i2) * 8 + (j2 - 1)]);
                         if (flag && (j2 < 4 || random.nextInt(2) != 0) && world.getMaterial(i + i1, j + j2, k + i2).isBuildable()) {
-                            world.setTypeId(i + i1, j + j2, k + i2, Block.STONE.id);
+                            world.setRawTypeId(i + i1, j + j2, k + i2, Block.STONE.id);
                         }
                     }
                 }
