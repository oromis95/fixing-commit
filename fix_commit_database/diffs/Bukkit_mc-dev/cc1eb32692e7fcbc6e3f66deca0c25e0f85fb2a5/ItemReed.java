@@ -2,11 +2,11 @@ package net.minecraft.server;
 
 public class ItemReed extends Item {
 
-    private int a;
+    private int id;
 
     public ItemReed(int i, Block block) {
         super(i);
-        this.a = block.id;
+        this.id = block.id;
     }
 
     public boolean a(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
@@ -41,13 +41,13 @@ public class ItemReed extends Item {
         if (itemstack.count == 0) {
             return false;
         } else {
-            if (world.a(this.a, i, j, k, false)) {
-                Block block = Block.byId[this.a];
+            if (world.a(this.id, i, j, k, false)) {
+                Block block = Block.byId[this.id];
 
-                if (world.e(i, j, k, this.a)) {
-                    Block.byId[this.a].d(world, i, j, k, l);
-                    Block.byId[this.a].a(world, i, j, k, (EntityLiving) entityhuman);
-                    world.a((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), block.stepSound.c(), (block.stepSound.a() + 1.0F) / 2.0F, block.stepSound.b() * 0.8F);
+                if (world.setTypeId(i, j, k, this.id)) {
+                    Block.byId[this.id].postPlace(world, i, j, k, l);
+                    Block.byId[this.id].postPlace(world, i, j, k, entityhuman);
+                    world.makeSound((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), block.stepSound.getName(), (block.stepSound.getVolume1() + 1.0F) / 2.0F, block.stepSound.getVolume2() * 0.8F);
                     --itemstack.count;
                 }
             }
