@@ -8,8 +8,8 @@ public class Packet15Place extends Packet {
     public int a;
     public int b;
     public int c;
-    public int d;
-    public ItemStack e;
+    public int face;
+    public ItemStack itemstack;
 
     public Packet15Place() {}
 
@@ -17,16 +17,16 @@ public class Packet15Place extends Packet {
         this.a = datainputstream.readInt();
         this.b = datainputstream.read();
         this.c = datainputstream.readInt();
-        this.d = datainputstream.read();
+        this.face = datainputstream.read();
         short short1 = datainputstream.readShort();
 
         if (short1 >= 0) {
             byte b0 = datainputstream.readByte();
             short short2 = datainputstream.readShort();
 
-            this.e = new ItemStack(short1, b0, short2);
+            this.itemstack = new ItemStack(short1, b0, short2);
         } else {
-            this.e = null;
+            this.itemstack = null;
         }
     }
 
@@ -34,13 +34,13 @@ public class Packet15Place extends Packet {
         dataoutputstream.writeInt(this.a);
         dataoutputstream.write(this.b);
         dataoutputstream.writeInt(this.c);
-        dataoutputstream.write(this.d);
-        if (this.e == null) {
+        dataoutputstream.write(this.face);
+        if (this.itemstack == null) {
             dataoutputstream.writeShort(-1);
         } else {
-            dataoutputstream.writeShort(this.e.id);
-            dataoutputstream.writeByte(this.e.count);
-            dataoutputstream.writeShort(this.e.h());
+            dataoutputstream.writeShort(this.itemstack.id);
+            dataoutputstream.writeByte(this.itemstack.count);
+            dataoutputstream.writeShort(this.itemstack.getData());
         }
     }
 
