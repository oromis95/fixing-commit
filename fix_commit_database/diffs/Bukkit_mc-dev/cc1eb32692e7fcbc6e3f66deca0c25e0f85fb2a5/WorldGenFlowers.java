@@ -17,7 +17,7 @@ public class WorldGenFlowers extends WorldGenerator {
             int k1 = k + random.nextInt(8) - random.nextInt(8);
 
             if (world.isEmpty(i1, j1, k1) && ((BlockFlower) Block.byId[this.a]).f(world, i1, j1, k1)) {
-                world.setTypeId(i1, j1, k1, this.a);
+                world.setRawTypeId(i1, j1, k1, this.a);
             }
         }
 
