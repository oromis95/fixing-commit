@@ -6,8 +6,8 @@ public class BlockFence extends Block {
         super(i, j, Material.WOOD);
     }
 
-    public boolean a(World world, int i, int j, int k) {
-        return world.getTypeId(i, j - 1, k) == this.id ? false : (!world.getMaterial(i, j - 1, k).isBuildable() ? false : super.a(world, i, j, k));
+    public boolean canPlace(World world, int i, int j, int k) {
+        return world.getTypeId(i, j - 1, k) == this.id ? false : (!world.getMaterial(i, j - 1, k).isBuildable() ? false : super.canPlace(world, i, j, k));
     }
 
     public AxisAlignedBB d(World world, int i, int j, int k) {
