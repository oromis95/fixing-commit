@@ -11,11 +11,11 @@ public class ItemDye extends Item {
     }
 
     public boolean a(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
-        if (itemstack.h() == 15) {
+        if (itemstack.getData() == 15) {
             int i1 = world.getTypeId(i, j, k);
 
             if (i1 == Block.SAPLING.id) {
-                ((BlockSapling) Block.SAPLING).b(world, i, j, k, world.k);
+                ((BlockSapling) Block.SAPLING).b(world, i, j, k, world.random);
                 --itemstack.count;
                 return true;
             }
@@ -30,13 +30,13 @@ public class ItemDye extends Item {
         return false;
     }
 
-    public void b(ItemStack itemstack, EntityLiving entityliving) {
+    public void a(ItemStack itemstack, EntityLiving entityliving) {
         if (entityliving instanceof EntitySheep) {
             EntitySheep entitysheep = (EntitySheep) entityliving;
-            int i = BlockCloth.c(itemstack.h());
+            int i = BlockCloth.c(itemstack.getData());
 
-            if (!entitysheep.j_() && entitysheep.n() != i) {
-                entitysheep.a_(i);
+            if (!entitysheep.isSheared() && entitysheep.getColor() != i) {
+                entitysheep.setColor(i);
                 --itemstack.count;
             }
         }
