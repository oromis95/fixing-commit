@@ -8,14 +8,14 @@ public class PlayerManager {
     private List a = new ArrayList();
     private PlayerList b = new PlayerList();
     private List c = new ArrayList();
-    private MinecraftServer d;
+    private MinecraftServer server;
     private final int[][] e = new int[][] { { 1, 0}, { 0, 1}, { -1, 0}, { 0, -1}};
 
     public PlayerManager(MinecraftServer minecraftserver) {
-        this.d = minecraftserver;
+        this.server = minecraftserver;
     }
 
-    public void a() {
+    public void flush() {
         for (int i = 0; i < this.c.size(); ++i) {
             ((PlayerInstance) this.c.get(i)).a();
         }
@@ -35,7 +35,7 @@ public class PlayerManager {
         return playerinstance;
     }
 
-    public void a(int i, int j, int k) {
+    public void flagDirty(int i, int j, int k) {
         int l = i >> 4;
         int i1 = k >> 4;
         PlayerInstance playerinstance = this.a(l, i1, false);
@@ -45,7 +45,7 @@ public class PlayerManager {
         }
     }
 
-    public void a(EntityPlayer entityplayer) {
+    public void addPlayer(EntityPlayer entityplayer) {
         int i = (int) entityplayer.locX >> 4;
         int j = (int) entityplayer.locZ >> 4;
 
@@ -83,7 +83,7 @@ public class PlayerManager {
         this.a.add(entityplayer);
     }
 
-    public void b(EntityPlayer entityplayer) {
+    public void removePlayer(EntityPlayer entityplayer) {
         int i = (int) entityplayer.d >> 4;
         int j = (int) entityplayer.e >> 4;
 
@@ -107,7 +107,7 @@ public class PlayerManager {
         return i1 >= -10 && i1 <= 10 ? j1 >= -10 && j1 <= 10 : false;
     }
 
-    public void c(EntityPlayer entityplayer) {
+    public void movePlayer(EntityPlayer entityplayer) {
         int i = (int) entityplayer.locX >> 4;
         int j = (int) entityplayer.locZ >> 4;
         double d0 = entityplayer.d - entityplayer.locX;
@@ -148,7 +148,7 @@ public class PlayerManager {
     }
 
     static MinecraftServer a(PlayerManager playermanager) {
-        return playermanager.d;
+        return playermanager.server;
     }
 
     static PlayerList b(PlayerManager playermanager) {
