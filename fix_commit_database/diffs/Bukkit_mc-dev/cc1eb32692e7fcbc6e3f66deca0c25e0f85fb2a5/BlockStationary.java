@@ -12,8 +12,8 @@ public class BlockStationary extends BlockFluids {
         }
     }
 
-    public void a(World world, int i, int j, int k, int l) {
-        super.a(world, i, j, k, l);
+    public void doPhysics(World world, int i, int j, int k, int l) {
+        super.doPhysics(world, i, j, k, l);
         if (world.getTypeId(i, j, k) == this.id) {
             this.i(world, i, j, k);
         }
@@ -23,7 +23,7 @@ public class BlockStationary extends BlockFluids {
         int l = world.getData(i, j, k);
 
         world.h = true;
-        world.setTypeIdAndData(i, j, k, this.id - 1, l);
+        world.setRawTypeIdAndData(i, j, k, this.id - 1, l);
         world.b(i, j, k, i, j, k);
         world.c(i, j, k, this.id - 1, this.b());
         world.h = false;
@@ -41,7 +41,7 @@ public class BlockStationary extends BlockFluids {
 
                 if (j1 == 0) {
                     if (this.j(world, i - 1, j, k) || this.j(world, i + 1, j, k) || this.j(world, i, j, k - 1) || this.j(world, i, j, k + 1) || this.j(world, i, j - 1, k) || this.j(world, i, j + 1, k)) {
-                        world.e(i, j, k, Block.FIRE.id);
+                        world.setTypeId(i, j, k, Block.FIRE.id);
                         return;
                     }
                 } else if (Block.byId[j1].material.isSolid()) {
