@@ -2,26 +2,28 @@ package net.minecraft.server;
 
 public class TileEntitySign extends TileEntity {
 
-    public String[] a = new String[] { "", "", "", ""};
+    public String[] lines = new String[] { "", "", "", ""};
     public int b = -1;
+    private boolean c = true;
 
     public TileEntitySign() {}
 
     public void b(NBTTagCompound nbttagcompound) {
         super.b(nbttagcompound);
-        nbttagcompound.a("Text1", this.a[0]);
-        nbttagcompound.a("Text2", this.a[1]);
-        nbttagcompound.a("Text3", this.a[2]);
-        nbttagcompound.a("Text4", this.a[3]);
+        nbttagcompound.setString("Text1", this.lines[0]);
+        nbttagcompound.setString("Text2", this.lines[1]);
+        nbttagcompound.setString("Text3", this.lines[2]);
+        nbttagcompound.setString("Text4", this.lines[3]);
     }
 
     public void a(NBTTagCompound nbttagcompound) {
+        this.c = false;
         super.a(nbttagcompound);
 
         for (int i = 0; i < 4; ++i) {
-            this.a[i] = nbttagcompound.i("Text" + (i + 1));
-            if (this.a[i].length() > 15) {
-                this.a[i] = this.a[i].substring(0, 15);
+            this.lines[i] = nbttagcompound.getString("Text" + (i + 1));
+            if (this.lines[i].length() > 15) {
+                this.lines[i] = this.lines[i].substring(0, 15);
             }
         }
     }
@@ -30,9 +32,13 @@ public class TileEntitySign extends TileEntity {
         String[] astring = new String[4];
 
         for (int i = 0; i < 4; ++i) {
-            astring[i] = this.a[i];
+            astring[i] = this.lines[i];
         }
 
         return new Packet130UpdateSign(this.e, this.f, this.g, astring);
     }
+
+    public boolean a() {
+        return this.c;
+    }
 }
