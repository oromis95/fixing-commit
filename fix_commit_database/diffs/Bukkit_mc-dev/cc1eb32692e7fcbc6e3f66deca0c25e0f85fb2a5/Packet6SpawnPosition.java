@@ -5,28 +5,28 @@ import java.io.DataOutputStream;
 
 public class Packet6SpawnPosition extends Packet {
 
-    public int a;
-    public int b;
-    public int c;
+    public int x;
+    public int y;
+    public int z;
 
     public Packet6SpawnPosition() {}
 
     public Packet6SpawnPosition(int i, int j, int k) {
-        this.a = i;
-        this.b = j;
-        this.c = k;
+        this.x = i;
+        this.y = j;
+        this.z = k;
     }
 
     public void a(DataInputStream datainputstream) {
-        this.a = datainputstream.readInt();
-        this.b = datainputstream.readInt();
-        this.c = datainputstream.readInt();
+        this.x = datainputstream.readInt();
+        this.y = datainputstream.readInt();
+        this.z = datainputstream.readInt();
     }
 
     public void a(DataOutputStream dataoutputstream) {
-        dataoutputstream.writeInt(this.a);
-        dataoutputstream.writeInt(this.b);
-        dataoutputstream.writeInt(this.c);
+        dataoutputstream.writeInt(this.x);
+        dataoutputstream.writeInt(this.y);
+        dataoutputstream.writeInt(this.z);
     }
 
     public void a(NetHandler nethandler) {
