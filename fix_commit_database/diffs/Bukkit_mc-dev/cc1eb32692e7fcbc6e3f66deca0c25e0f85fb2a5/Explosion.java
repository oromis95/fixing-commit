@@ -10,25 +10,25 @@ public class Explosion {
 
     public boolean a = false;
     private Random h = new Random();
-    private World i;
-    public double b;
-    public double c;
-    public double d;
-    public Entity e;
-    public float f;
+    private World world;
+    public double posX;
+    public double posY;
+    public double posZ;
+    public Entity source;
+    public float size;
     public Set g = new HashSet();
 
     public Explosion(World world, Entity entity, double d0, double d1, double d2, float f) {
-        this.i = world;
-        this.e = entity;
-        this.f = f;
-        this.b = d0;
-        this.c = d1;
-        this.d = d2;
+        this.world = world;
+        this.source = entity;
+        this.size = f;
+        this.posX = d0;
+        this.posY = d1;
+        this.posZ = d2;
     }
 
     public void a() {
-        float f = this.f;
+        float f = this.size;
         byte b0 = 16;
 
         int i;
@@ -50,20 +50,20 @@ public class Explosion {
                         d3 /= d6;
                         d4 /= d6;
                         d5 /= d6;
-                        float f1 = this.f * (0.7F + this.i.k.nextFloat() * 0.6F);
+                        float f1 = this.size * (0.7F + this.world.random.nextFloat() * 0.6F);
 
-                        d0 = this.b;
-                        d1 = this.c;
-                        d2 = this.d;
+                        d0 = this.posX;
+                        d1 = this.posY;
+                        d2 = this.posZ;
 
                         for (float f2 = 0.3F; f1 > 0.0F; f1 -= f2 * 0.75F) {
-                            int l = MathHelper.b(d0);
-                            int i1 = MathHelper.b(d1);
-                            int j1 = MathHelper.b(d2);
-                            int k1 = this.i.getTypeId(l, i1, j1);
+                            int l = MathHelper.floor(d0);
+                            int i1 = MathHelper.floor(d1);
+                            int j1 = MathHelper.floor(d2);
+                            int k1 = this.world.getTypeId(l, i1, j1);
 
                             if (k1 > 0) {
-                                f1 -= (Block.byId[k1].a(this.e) + 0.3F) * f2;
+                                f1 -= (Block.byId[k1].a(this.source) + 0.3F) * f2;
                             }
 
                             if (f1 > 0.0F) {
@@ -79,98 +79,98 @@ public class Explosion {
             }
         }
 
-        this.f *= 2.0F;
-        i = MathHelper.b(this.b - (double) this.f - 1.0D);
-        j = MathHelper.b(this.b + (double) this.f + 1.0D);
-        k = MathHelper.b(this.c - (double) this.f - 1.0D);
-        int l1 = MathHelper.b(this.c + (double) this.f + 1.0D);
-        int i2 = MathHelper.b(this.d - (double) this.f - 1.0D);
-        int j2 = MathHelper.b(this.d + (double) this.f + 1.0D);
-        List list = this.i.b(this.e, AxisAlignedBB.b((double) i, (double) k, (double) i2, (double) j, (double) l1, (double) j2));
-        Vec3D vec3d = Vec3D.b(this.b, this.c, this.d);
+        this.size *= 2.0F;
+        i = MathHelper.floor(this.posX - (double) this.size - 1.0D);
+        j = MathHelper.floor(this.posX + (double) this.size + 1.0D);
+        k = MathHelper.floor(this.posY - (double) this.size - 1.0D);
+        int l1 = MathHelper.floor(this.posY + (double) this.size + 1.0D);
+        int i2 = MathHelper.floor(this.posZ - (double) this.size - 1.0D);
+        int j2 = MathHelper.floor(this.posZ + (double) this.size + 1.0D);
+        List list = this.world.b(this.source, AxisAlignedBB.b((double) i, (double) k, (double) i2, (double) j, (double) l1, (double) j2));
+        Vec3D vec3d = Vec3D.create(this.posX, this.posY, this.posZ);
 
         for (int k2 = 0; k2 < list.size(); ++k2) {
             Entity entity = (Entity) list.get(k2);
-            double d7 = entity.e(this.b, this.c, this.d) / (double) this.f;
+            double d7 = entity.e(this.posX, this.posY, this.posZ) / (double) this.size;
 
             if (d7 <= 1.0D) {
-                d0 = entity.locX - this.b;
-                d1 = entity.locY - this.c;
-                d2 = entity.locZ - this.d;
+                d0 = entity.locX - this.posX;
+                d1 = entity.locY - this.posY;
+                d2 = entity.locZ - this.posZ;
                 double d8 = (double) MathHelper.a(d0 * d0 + d1 * d1 + d2 * d2);
 
                 d0 /= d8;
                 d1 /= d8;
                 d2 /= d8;
-                double d9 = (double) this.i.a(vec3d, entity.boundingBox);
+                double d9 = (double) this.world.a(vec3d, entity.boundingBox);
                 double d10 = (1.0D - d7) * d9;
 
-                entity.a(this.e, (int) ((d10 * d10 + d10) / 2.0D * 8.0D * (double) this.f + 1.0D));
+                entity.damageEntity(this.source, (int) ((d10 * d10 + d10) / 2.0D * 8.0D * (double) this.size + 1.0D));
                 entity.motX += d0 * d10;
                 entity.motY += d1 * d10;
                 entity.motZ += d2 * d10;
             }
         }
 
-        this.f = f;
+        this.size = f;
         ArrayList arraylist = new ArrayList();
 
         arraylist.addAll(this.g);
         if (this.a) {
             for (int l2 = arraylist.size() - 1; l2 >= 0; --l2) {
                 ChunkPosition chunkposition = (ChunkPosition) arraylist.get(l2);
-                int i3 = chunkposition.a;
-                int j3 = chunkposition.b;
-                int k3 = chunkposition.c;
-                int l3 = this.i.getTypeId(i3, j3, k3);
-                int i4 = this.i.getTypeId(i3, j3 - 1, k3);
+                int i3 = chunkposition.x;
+                int j3 = chunkposition.y;
+                int k3 = chunkposition.z;
+                int l3 = this.world.getTypeId(i3, j3, k3);
+                int i4 = this.world.getTypeId(i3, j3 - 1, k3);
 
                 if (l3 == 0 && Block.o[i4] && this.h.nextInt(3) == 0) {
-                    this.i.e(i3, j3, k3, Block.FIRE.id);
+                    this.world.setTypeId(i3, j3, k3, Block.FIRE.id);
                 }
             }
         }
     }
 
     public void b() {
-        this.i.a(this.b, this.c, this.d, "random.explode", 4.0F, (1.0F + (this.i.k.nextFloat() - this.i.k.nextFloat()) * 0.2F) * 0.7F);
+        this.world.makeSound(this.posX, this.posY, this.posZ, "random.explode", 4.0F, (1.0F + (this.world.random.nextFloat() - this.world.random.nextFloat()) * 0.2F) * 0.7F);
         ArrayList arraylist = new ArrayList();
 
         arraylist.addAll(this.g);
 
         for (int i = arraylist.size() - 1; i >= 0; --i) {
             ChunkPosition chunkposition = (ChunkPosition) arraylist.get(i);
-            int j = chunkposition.a;
-            int k = chunkposition.b;
-            int l = chunkposition.c;
-            int i1 = this.i.getTypeId(j, k, l);
+            int j = chunkposition.x;
+            int k = chunkposition.y;
+            int l = chunkposition.z;
+            int i1 = this.world.getTypeId(j, k, l);
 
             for (int j1 = 0; j1 < 1; ++j1) {
-                double d0 = (double) ((float) j + this.i.k.nextFloat());
-                double d1 = (double) ((float) k + this.i.k.nextFloat());
-                double d2 = (double) ((float) l + this.i.k.nextFloat());
-                double d3 = d0 - this.b;
-                double d4 = d1 - this.c;
-                double d5 = d2 - this.d;
+                double d0 = (double) ((float) j + this.world.random.nextFloat());
+                double d1 = (double) ((float) k + this.world.random.nextFloat());
+                double d2 = (double) ((float) l + this.world.random.nextFloat());
+                double d3 = d0 - this.posX;
+                double d4 = d1 - this.posY;
+                double d5 = d2 - this.posZ;
                 double d6 = (double) MathHelper.a(d3 * d3 + d4 * d4 + d5 * d5);
 
                 d3 /= d6;
                 d4 /= d6;
                 d5 /= d6;
-                double d7 = 0.5D / (d6 / (double) this.f + 0.1D);
+                double d7 = 0.5D / (d6 / (double) this.size + 0.1D);
 
-                d7 *= (double) (this.i.k.nextFloat() * this.i.k.nextFloat() + 0.3F);
+                d7 *= (double) (this.world.random.nextFloat() * this.world.random.nextFloat() + 0.3F);
                 d3 *= d7;
                 d4 *= d7;
                 d5 *= d7;
-                this.i.a("explode", (d0 + this.b * 1.0D) / 2.0D, (d1 + this.c * 1.0D) / 2.0D, (d2 + this.d * 1.0D) / 2.0D, d3, d4, d5);
-                this.i.a("smoke", d0, d1, d2, d3, d4, d5);
+                this.world.a("explode", (d0 + this.posX * 1.0D) / 2.0D, (d1 + this.posY * 1.0D) / 2.0D, (d2 + this.posZ * 1.0D) / 2.0D, d3, d4, d5);
+                this.world.a("smoke", d0, d1, d2, d3, d4, d5);
             }
 
             if (i1 > 0) {
-                Block.byId[i1].a(this.i, j, k, l, this.i.getData(j, k, l), 0.3F);
-                this.i.e(j, k, l, 0);
-                Block.byId[i1].c(this.i, j, k, l);
+                Block.byId[i1].dropNaturally(this.world, j, k, l, this.world.getData(j, k, l), 0.3F);
+                this.world.setTypeId(j, k, l, 0);
+                Block.byId[i1].c(this.world, j, k, l);
             }
         }
     }
