@@ -12,37 +12,37 @@ public class InventoryLargeChest implements IInventory {
         this.c = iinventory1;
     }
 
-    public int m_() {
-        return this.b.m_() + this.c.m_();
+    public int getSize() {
+        return this.b.getSize() + this.c.getSize();
     }
 
-    public String c() {
+    public String getName() {
         return this.a;
     }
 
-    public ItemStack c_(int i) {
-        return i >= this.b.m_() ? this.c.c_(i - this.b.m_()) : this.b.c_(i);
+    public ItemStack getItem(int i) {
+        return i >= this.b.getSize() ? this.c.getItem(i - this.b.getSize()) : this.b.getItem(i);
     }
 
     public ItemStack a(int i, int j) {
-        return i >= this.b.m_() ? this.c.a(i - this.b.m_(), j) : this.b.a(i, j);
+        return i >= this.b.getSize() ? this.c.a(i - this.b.getSize(), j) : this.b.a(i, j);
     }
 
-    public void a(int i, ItemStack itemstack) {
-        if (i >= this.b.m_()) {
-            this.c.a(i - this.b.m_(), itemstack);
+    public void setItem(int i, ItemStack itemstack) {
+        if (i >= this.b.getSize()) {
+            this.c.setItem(i - this.b.getSize(), itemstack);
         } else {
-            this.b.a(i, itemstack);
+            this.b.setItem(i, itemstack);
         }
     }
 
-    public int n_() {
-        return this.b.n_();
+    public int getMaxStackSize() {
+        return this.b.getMaxStackSize();
     }
 
-    public void h() {
-        this.b.h();
-        this.c.h();
+    public void update() {
+        this.b.update();
+        this.c.update();
     }
 
     public boolean a_(EntityHuman entityhuman) {
