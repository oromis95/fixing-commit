@@ -4,12 +4,12 @@ public abstract class BlockContainer extends Block {
 
     protected BlockContainer(int i, Material material) {
         super(i, material);
-        p[i] = true;
+        isTileEntity[i] = true;
     }
 
     protected BlockContainer(int i, int j, Material material) {
         super(i, j, material);
-        p[i] = true;
+        isTileEntity[i] = true;
     }
 
     public void e(World world, int i, int j, int k) {
@@ -17,8 +17,8 @@ public abstract class BlockContainer extends Block {
         world.setTileEntity(i, j, k, this.a_());
     }
 
-    public void b(World world, int i, int j, int k) {
-        super.b(world, i, j, k);
+    public void remove(World world, int i, int j, int k) {
+        super.remove(world, i, j, k);
         world.n(i, j, k);
     }
 
