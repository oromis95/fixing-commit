@@ -4,7 +4,7 @@ import java.util.List;
 
 public class EntityBoat extends Entity {
 
-    public int a;
+    public int damage;
     public int b;
     public int c;
     private int d;
@@ -16,13 +16,16 @@ public class EntityBoat extends Entity {
 
     public EntityBoat(World world) {
         super(world);
-        this.a = 0;
+        this.damage = 0;
         this.b = 0;
         this.c = 1;
-        this.aC = true;
-        this.a(1.5F, 0.6F);
+        this.aD = true;
+        this.b(1.5F, 0.6F);
         this.height = this.width / 2.0F;
-        this.bg = false;
+    }
+
+    protected boolean l() {
+        return false;
     }
 
     protected void a() {}
@@ -41,7 +44,7 @@ public class EntityBoat extends Entity {
 
     public EntityBoat(World world, double d0, double d1, double d2) {
         this(world);
-        this.a(d0, d1 + (double) this.height, d2);
+        this.setPosition(d0, d1 + (double) this.height, d2);
         this.motX = 0.0D;
         this.motY = 0.0D;
         this.motZ = 0.0D;
@@ -54,13 +57,13 @@ public class EntityBoat extends Entity {
         return (double) this.width * 0.0D - 0.30000001192092896D;
     }
 
-    public boolean a(Entity entity, int i) {
+    public boolean damageEntity(Entity entity, int i) {
         if (!this.world.isStatic && !this.dead) {
             this.c = -this.c;
             this.b = 10;
-            this.a += i * 10;
-            this.R();
-            if (this.a > 40) {
+            this.damage += i * 10;
+            this.W();
+            if (this.damage > 40) {
                 int j;
 
                 for (j = 0; j < 3; ++j) {
@@ -71,7 +74,7 @@ public class EntityBoat extends Entity {
                     this.a(Item.STICK.id, 1, 0.0F);
                 }
 
-                this.C();
+                this.die();
             }
 
             return true;
@@ -90,8 +93,8 @@ public class EntityBoat extends Entity {
             --this.b;
         }
 
-        if (this.a > 0) {
-            --this.a;
+        if (this.damage > 0) {
+            --this.damage;
         }
 
         this.lastX = this.locX;
@@ -132,13 +135,13 @@ public class EntityBoat extends Entity {
                 this.yaw = (float) ((double) this.yaw + d6 / (double) this.d);
                 this.pitch = (float) ((double) this.pitch + (this.i - (double) this.pitch) / (double) this.d);
                 --this.d;
-                this.a(d3, d4, d5);
+                this.setPosition(d3, d4, d5);
                 this.c(this.yaw, this.pitch);
             } else {
                 d3 = this.locX + this.motX;
                 d4 = this.locY + this.motY;
                 d5 = this.locZ + this.motZ;
-                this.a(d3, d4, d5);
+                this.setPosition(d3, d4, d5);
                 if (this.onGround) {
                     this.motX *= 0.5D;
                     this.motY *= 0.5D;
@@ -180,7 +183,7 @@ public class EntityBoat extends Entity {
                 this.motZ *= 0.5D;
             }
 
-            this.c(this.motX, this.motY, this.motZ);
+            this.move(this.motX, this.motY, this.motZ);
             d5 = Math.sqrt(this.motX * this.motX + this.motZ * this.motZ);
             double d7;
 
@@ -206,9 +209,9 @@ public class EntityBoat extends Entity {
                 }
             }
 
-            if (this.aV && d5 > 0.15D) {
+            if (this.positionChanged && d5 > 0.15D) {
                 if (!this.world.isStatic) {
-                    this.C();
+                    this.die();
 
                     int k;
 
@@ -262,7 +265,7 @@ public class EntityBoat extends Entity {
                     Entity entity = (Entity) list.get(l);
 
                     if (entity != this.passenger && entity.e_() && entity instanceof EntityBoat) {
-                        entity.h(this);
+                        entity.collide(this);
                     }
                 }
             }
@@ -278,7 +281,7 @@ public class EntityBoat extends Entity {
             double d0 = Math.cos((double) this.yaw * 3.141592653589793D / 180.0D) * 0.4D;
             double d1 = Math.sin((double) this.yaw * 3.141592653589793D / 180.0D) * 0.4D;
 
-            this.passenger.a(this.locX + d0, this.locY + this.k() + this.passenger.B(), this.locZ + d1);
+            this.passenger.setPosition(this.locX + d0, this.locY + this.k() + this.passenger.C(), this.locZ + d1);
         }
     }
 
@@ -291,7 +294,7 @@ public class EntityBoat extends Entity {
             return true;
         } else {
             if (!this.world.isStatic) {
-                entityhuman.b((Entity) this);
+                entityhuman.mount(this);
             }
 
             return true;
