@@ -5,7 +5,7 @@ public class ItemFlintAndSteel extends Item {
     public ItemFlintAndSteel(int i) {
         super(i);
         this.maxStackSize = 1;
-        this.durability = 64;
+        this.d(64);
     }
 
     public boolean a(ItemStack itemstack, EntityHuman entityhuman, World world, int i, int j, int k, int l) {
@@ -36,11 +36,11 @@ public class ItemFlintAndSteel extends Item {
         int i1 = world.getTypeId(i, j, k);
 
         if (i1 == 0) {
-            world.a((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "fire.ignite", 1.0F, b.nextFloat() * 0.4F + 0.8F);
-            world.e(i, j, k, Block.FIRE.id);
+            world.makeSound((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "fire.ignite", 1.0F, b.nextFloat() * 0.4F + 0.8F);
+            world.setTypeId(i, j, k, Block.FIRE.id);
         }
 
-        itemstack.b(1);
+        itemstack.damage(1, entityhuman);
         return true;
     }
 }
