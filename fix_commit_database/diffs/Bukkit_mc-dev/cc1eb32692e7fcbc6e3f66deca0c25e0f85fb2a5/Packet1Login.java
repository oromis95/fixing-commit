@@ -6,7 +6,7 @@ import java.io.DataOutputStream;
 public class Packet1Login extends Packet {
 
     public int a;
-    public String b;
+    public String name;
     public String c;
     public long d;
     public byte e;
@@ -14,7 +14,7 @@ public class Packet1Login extends Packet {
     public Packet1Login() {}
 
     public Packet1Login(String s, String s1, int i, long j, byte b0) {
-        this.b = s;
+        this.name = s;
         this.c = s1;
         this.a = i;
         this.d = j;
@@ -23,7 +23,7 @@ public class Packet1Login extends Packet {
 
     public void a(DataInputStream datainputstream) {
         this.a = datainputstream.readInt();
-        this.b = datainputstream.readUTF();
+        this.name = datainputstream.readUTF();
         this.c = datainputstream.readUTF();
         this.d = datainputstream.readLong();
         this.e = datainputstream.readByte();
@@ -31,7 +31,7 @@ public class Packet1Login extends Packet {
 
     public void a(DataOutputStream dataoutputstream) {
         dataoutputstream.writeInt(this.a);
-        dataoutputstream.writeUTF(this.b);
+        dataoutputstream.writeUTF(this.name);
         dataoutputstream.writeUTF(this.c);
         dataoutputstream.writeLong(this.d);
         dataoutputstream.writeByte(this.e);
@@ -42,6 +42,6 @@ public class Packet1Login extends Packet {
     }
 
     public int a() {
-        return 4 + this.b.length() + this.c.length() + 4 + 5;
+        return 4 + this.name.length() + this.c.length() + 4 + 5;
     }
 }
