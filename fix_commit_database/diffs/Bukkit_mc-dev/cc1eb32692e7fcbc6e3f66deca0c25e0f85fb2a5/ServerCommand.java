@@ -2,11 +2,11 @@ package net.minecraft.server;
 
 public class ServerCommand {
 
-    public final String a;
+    public final String command;
     public final ICommandListener b;
 
     public ServerCommand(String s, ICommandListener icommandlistener) {
-        this.a = s;
+        this.command = s;
         this.b = icommandlistener;
     }
 }
