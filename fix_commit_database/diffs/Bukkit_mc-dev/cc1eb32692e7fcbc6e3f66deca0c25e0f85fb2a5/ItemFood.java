@@ -3,10 +3,12 @@ package net.minecraft.server;
 public class ItemFood extends Item {
 
     private int a;
+    private boolean bi;
 
-    public ItemFood(int i, int j) {
+    public ItemFood(int i, int j, boolean flag) {
         super(i);
         this.a = j;
+        this.bi = flag;
         this.maxStackSize = 1;
     }
 
@@ -15,4 +17,12 @@ public class ItemFood extends Item {
         entityhuman.b(this.a);
         return itemstack;
     }
+
+    public int j() {
+        return this.a;
+    }
+
+    public boolean k() {
+        return this.bi;
+    }
 }
