@@ -2,41 +2,41 @@ package net.minecraft.server;
 
 public class EntityMonster extends EntityCreature implements IMonster {
 
-    protected int c = 2;
+    protected int damage = 2;
 
     public EntityMonster(World world) {
         super(world);
         this.health = 20;
     }
 
-    public void q() {
+    public void r() {
         float f = this.c(1.0F);
 
         if (f > 0.5F) {
             this.at += 2;
         }
 
-        super.q();
+        super.r();
     }
 
     public void f_() {
         super.f_();
-        if (this.world.j == 0) {
-            this.C();
+        if (this.world.spawnMonsters == 0) {
+            this.die();
         }
     }
 
-    protected Entity l() {
+    protected Entity findTarget() {
         EntityHuman entityhuman = this.world.a(this, 16.0D);
 
         return entityhuman != null && this.e(entityhuman) ? entityhuman : null;
     }
 
-    public boolean a(Entity entity, int i) {
-        if (super.a(entity, i)) {
+    public boolean damageEntity(Entity entity, int i) {
+        if (super.damageEntity(entity, i)) {
             if (this.passenger != entity && this.vehicle != entity) {
                 if (entity != this) {
-                    this.d = entity;
+                    this.target = entity;
                 }
 
                 return true;
@@ -49,9 +49,9 @@ public class EntityMonster extends EntityCreature implements IMonster {
     }
 
     protected void a(Entity entity, float f) {
-        if ((double) f < 1.5D && entity.boundingBox.e > this.boundingBox.b && entity.boundingBox.b < this.boundingBox.e) {
+        if (this.attackTicks <= 0 && f < 2.0F && entity.boundingBox.e > this.boundingBox.b && entity.boundingBox.b < this.boundingBox.e) {
             this.attackTicks = 20;
-            entity.a(this, this.c);
+            entity.damageEntity(this, this.damage);
         }
     }
 
@@ -68,14 +68,14 @@ public class EntityMonster extends EntityCreature implements IMonster {
     }
 
     public boolean b() {
-        int i = MathHelper.b(this.locX);
-        int j = MathHelper.b(this.boundingBox.b);
-        int k = MathHelper.b(this.locZ);
+        int i = MathHelper.floor(this.locX);
+        int j = MathHelper.floor(this.boundingBox.b);
+        int k = MathHelper.floor(this.locZ);
 
         if (this.world.a(EnumSkyBlock.SKY, i, j, k) > this.random.nextInt(32)) {
             return false;
         } else {
-            int l = this.world.j(i, j, k);
+            int l = this.world.getLightLevel(i, j, k);
 
             return l <= this.random.nextInt(8) && super.b();
         }
