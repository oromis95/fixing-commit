@@ -44,7 +44,12 @@ public class ItemSign extends Item {
                 }
 
                 --itemstack.a;
-                entityhuman.a((TileEntitySign) world.k(i, j, k));
+                TileEntitySign tileentitysign = (TileEntitySign) world.k(i, j, k);
+
+                if (tileentitysign != null) {
+                    entityhuman.a(tileentitysign);
+                }
+
                 return true;
             }
         }
