@@ -24,13 +24,13 @@ public class BlockSnow extends Block {
         return l != 0 && Block.byId[l].a() ? world.getMaterial(i, j - 1, k).isSolid() : false;
     }
 
-    public void b(World world, int i, int j, int k, int l) {
+    public void a(World world, int i, int j, int k, int l) {
         this.g(world, i, j, k);
     }
 
     private boolean g(World world, int i, int j, int k) {
         if (!this.a(world, i, j, k)) {
-            this.a_(world, i, j, k, world.getData(i, j, k));
+            this.b_(world, i, j, k, world.getData(i, j, k));
             world.e(i, j, k, 0);
             return false;
         } else {
@@ -38,12 +38,12 @@ public class BlockSnow extends Block {
         }
     }
 
-    public void g(World world, int i, int j, int k, int l) {
+    public void a_(World world, int i, int j, int k, int l) {
         int i1 = Item.SNOW_BALL.id;
         float f = 0.7F;
-        double d0 = (double) (world.l.nextFloat() * f) + (double) (1.0F - f) * 0.5D;
-        double d1 = (double) (world.l.nextFloat() * f) + (double) (1.0F - f) * 0.5D;
-        double d2 = (double) (world.l.nextFloat() * f) + (double) (1.0F - f) * 0.5D;
+        double d0 = (double) (world.k.nextFloat() * f) + (double) (1.0F - f) * 0.5D;
+        double d1 = (double) (world.k.nextFloat() * f) + (double) (1.0F - f) * 0.5D;
+        double d2 = (double) (world.k.nextFloat() * f) + (double) (1.0F - f) * 0.5D;
         EntityItem entityitem = new EntityItem(world, (double) i + d0, (double) j + d1, (double) k + d2, new ItemStack(i1, 1, 0));
 
         entityitem.c = 10;
@@ -61,7 +61,7 @@ public class BlockSnow extends Block {
 
     public void a(World world, int i, int j, int k, Random random) {
         if (world.a(EnumSkyBlock.BLOCK, i, j, k) > 11) {
-            this.a_(world, i, j, k, world.getData(i, j, k));
+            this.b_(world, i, j, k, world.getData(i, j, k));
             world.e(i, j, k, 0);
         }
     }
