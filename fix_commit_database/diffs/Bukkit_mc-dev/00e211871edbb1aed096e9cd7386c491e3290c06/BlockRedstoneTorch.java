@@ -9,9 +9,13 @@ public class BlockRedstoneTorch extends BlockTorch {
     private boolean a = false;
     private static List b = new ArrayList();
 
+    public int a(int i, int j) {
+        return i == 1 ? Block.REDSTONE_WIRE.a(i, j) : super.a(i, j);
+    }
+
     private boolean a(World world, int i, int j, int k, boolean flag) {
         if (flag) {
-            b.add(new RedstoneUpdateInfo(i, j, k, world.e));
+            b.add(new RedstoneUpdateInfo(i, j, k, world.k()));
         }
 
         int l = 0;
@@ -79,13 +83,13 @@ public class BlockRedstoneTorch extends BlockTorch {
     private boolean g(World world, int i, int j, int k) {
         int l = world.getData(i, j, k);
 
-        return l == 5 && world.k(i, j - 1, k, 0) ? true : (l == 3 && world.k(i, j, k - 1, 2) ? true : (l == 4 && world.k(i, j, k + 1, 3) ? true : (l == 1 && world.k(i - 1, j, k, 4) ? true : l == 2 && world.k(i + 1, j, k, 5))));
+        return l == 5 && world.j(i, j - 1, k, 0) ? true : (l == 3 && world.j(i, j, k - 1, 2) ? true : (l == 4 && world.j(i, j, k + 1, 3) ? true : (l == 1 && world.j(i - 1, j, k, 4) ? true : l == 2 && world.j(i + 1, j, k, 5))));
     }
 
     public void a(World world, int i, int j, int k, Random random) {
         boolean flag = this.g(world, i, j, k);
 
-        while (b.size() > 0 && world.e - ((RedstoneUpdateInfo) b.get(0)).d > 100L) {
+        while (b.size() > 0 && world.k() - ((RedstoneUpdateInfo) b.get(0)).d > 100L) {
             b.remove(0);
         }
 
@@ -93,7 +97,7 @@ public class BlockRedstoneTorch extends BlockTorch {
             if (flag) {
                 world.b(i, j, k, Block.REDSTONE_TORCH_OFF.id, world.getData(i, j, k));
                 if (this.a(world, i, j, k, true)) {
-                    world.a((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), "random.fizz", 0.5F, 2.6F + (world.l.nextFloat() - world.l.nextFloat()) * 0.8F);
+                    world.a((double) ((float) i + 0.5F), (double) ((float) j + 0.5F), (double) ((float) k + 0.5F), "random.fizz", 0.5F, 2.6F + (world.k.nextFloat() - world.k.nextFloat()) * 0.8F);
 
                     for (int l = 0; l < 5; ++l) {
                         double d0 = (double) i + random.nextDouble() * 0.6D + 0.2D;
@@ -109,13 +113,13 @@ public class BlockRedstoneTorch extends BlockTorch {
         }
     }
 
-    public void b(World world, int i, int j, int k, int l) {
-        super.b(world, i, j, k, l);
-        world.i(i, j, k, this.id);
+    public void a(World world, int i, int j, int k, int l) {
+        super.a(world, i, j, k, l);
+        world.c(i, j, k, this.id, this.b());
     }
 
-    public boolean d(World world, int i, int j, int k, int l) {
-        return l == 0 ? this.b((IBlockAccess) world, i, j, k, l) : false;
+    public boolean c(World world, int i, int j, int k, int l) {
+        return l == 0 ? this.b(world, i, j, k, l) : false;
     }
 
     public int a(int i, Random random) {
