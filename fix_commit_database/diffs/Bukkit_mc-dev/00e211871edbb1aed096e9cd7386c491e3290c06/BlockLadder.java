@@ -39,7 +39,7 @@ public class BlockLadder extends Block {
         return world.d(i - 1, j, k) ? true : (world.d(i + 1, j, k) ? true : (world.d(i, j, k - 1) ? true : world.d(i, j, k + 1)));
     }
 
-    public void c(World world, int i, int j, int k, int l) {
+    public void d(World world, int i, int j, int k, int l) {
         int i1 = world.getData(i, j, k);
 
         if ((i1 == 0 || l == 2) && world.d(i, j, k + 1)) {
@@ -61,7 +61,7 @@ public class BlockLadder extends Block {
         world.c(i, j, k, i1);
     }
 
-    public void b(World world, int i, int j, int k, int l) {
+    public void a(World world, int i, int j, int k, int l) {
         int i1 = world.getData(i, j, k);
         boolean flag = false;
 
@@ -82,11 +82,11 @@ public class BlockLadder extends Block {
         }
 
         if (!flag) {
-            this.a_(world, i, j, k, i1);
+            this.b_(world, i, j, k, i1);
             world.e(i, j, k, 0);
         }
 
-        super.b(world, i, j, k, l);
+        super.a(world, i, j, k, l);
     }
 
     public int a(Random random) {
