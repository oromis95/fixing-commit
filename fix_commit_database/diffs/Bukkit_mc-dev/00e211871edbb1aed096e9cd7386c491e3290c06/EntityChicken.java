@@ -6,21 +6,21 @@ public class EntityChicken extends EntityAnimal {
     public float b = 0.0F;
     public float c = 0.0F;
     public float f;
-    public float ak;
-    public float al = 1.0F;
-    public int am;
+    public float g;
+    public float h = 1.0F;
+    public int i;
 
     public EntityChicken(World world) {
         super(world);
         this.texture = "/mob/chicken.png";
         this.a(0.3F, 0.4F);
         this.health = 4;
-        this.am = this.random.nextInt(6000) + 6000;
+        this.i = this.random.nextInt(6000) + 6000;
     }
 
-    public void o() {
-        super.o();
-        this.ak = this.b;
+    public void q() {
+        super.q();
+        this.g = this.b;
         this.f = this.c;
         this.c = (float) ((double) this.c + (double) (this.onGround ? -1 : 4) * 0.3D);
         if (this.c < 0.0F) {
@@ -31,20 +31,20 @@ public class EntityChicken extends EntityAnimal {
             this.c = 1.0F;
         }
 
-        if (!this.onGround && this.al < 1.0F) {
-            this.al = 1.0F;
+        if (!this.onGround && this.h < 1.0F) {
+            this.h = 1.0F;
         }
 
-        this.al = (float) ((double) this.al * 0.9D);
+        this.h = (float) ((double) this.h * 0.9D);
         if (!this.onGround && this.motY < 0.0D) {
             this.motY *= 0.6D;
         }
 
-        this.b += this.al * 2.0F;
-        if (!this.world.isStatic && --this.am <= 0) {
+        this.b += this.h * 2.0F;
+        if (!this.world.isStatic && --this.i <= 0) {
             this.world.a(this, "mob.chickenplop", 1.0F, (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
-            this.a(Item.EGG.id, 1);
-            this.am = this.random.nextInt(6000) + 6000;
+            this.b(Item.EGG.id, 1);
+            this.i = this.random.nextInt(6000) + 6000;
         }
     }
 
