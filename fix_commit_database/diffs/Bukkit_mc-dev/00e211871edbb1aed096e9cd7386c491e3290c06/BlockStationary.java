@@ -12,8 +12,8 @@ public class BlockStationary extends BlockFluids {
         }
     }
 
-    public void b(World world, int i, int j, int k, int l) {
-        super.b(world, i, j, k, l);
+    public void a(World world, int i, int j, int k, int l) {
+        super.a(world, i, j, k, l);
         if (world.getTypeId(i, j, k) == this.id) {
             this.i(world, i, j, k);
         }
@@ -22,11 +22,11 @@ public class BlockStationary extends BlockFluids {
     private void i(World world, int i, int j, int k) {
         int l = world.getData(i, j, k);
 
-        world.i = true;
+        world.h = true;
         world.setTypeIdAndData(i, j, k, this.id - 1, l);
         world.b(i, j, k, i, j, k);
-        world.i(i, j, k, this.id - 1);
-        world.i = false;
+        world.c(i, j, k, this.id - 1, this.b());
+        world.h = false;
     }
 
     public void a(World world, int i, int j, int k, Random random) {
