@@ -10,9 +10,9 @@ public class EntitySnowball extends Entity {
     private int e = 0;
     private boolean f = false;
     public int a = 0;
-    private EntityLiving ak;
-    private int al;
-    private int am = 0;
+    private EntityLiving g;
+    private int h;
+    private int i = 0;
 
     public EntitySnowball(World world) {
         super(world);
@@ -23,9 +23,9 @@ public class EntitySnowball extends Entity {
 
     public EntitySnowball(World world, EntityLiving entityliving) {
         super(world);
-        this.ak = entityliving;
+        this.g = entityliving;
         this.a(0.25F, 0.25F);
-        this.c(entityliving.locX, entityliving.locY + (double) entityliving.w(), entityliving.locZ, entityliving.yaw, entityliving.pitch);
+        this.c(entityliving.locX, entityliving.locY + (double) entityliving.p(), entityliving.locZ, entityliving.yaw, entityliving.pitch);
         this.locX -= (double) (MathHelper.b(this.yaw / 180.0F * 3.1415927F) * 0.16F);
         this.locY -= 0.10000000149011612D;
         this.locZ -= (double) (MathHelper.a(this.yaw / 180.0F * 3.1415927F) * 0.16F);
@@ -41,7 +41,7 @@ public class EntitySnowball extends Entity {
 
     public EntitySnowball(World world, double d0, double d1, double d2) {
         super(world);
-        this.al = 0;
+        this.h = 0;
         this.a(0.25F, 0.25F);
         this.a(d0, d1, d2);
         this.height = 0.0F;
@@ -66,14 +66,14 @@ public class EntitySnowball extends Entity {
 
         this.lastYaw = this.yaw = (float) (Math.atan2(d0, d2) * 180.0D / 3.1415927410125732D);
         this.lastPitch = this.pitch = (float) (Math.atan2(d1, (double) f3) * 180.0D / 3.1415927410125732D);
-        this.al = 0;
+        this.h = 0;
     }
 
-    public void b_() {
-        this.O = this.locX;
-        this.P = this.locY;
-        this.Q = this.locZ;
-        super.b_();
+    public void f_() {
+        this.bi = this.locX;
+        this.bj = this.locY;
+        this.bk = this.locZ;
+        super.f_();
         if (this.a > 0) {
             --this.a;
         }
@@ -82,9 +82,9 @@ public class EntitySnowball extends Entity {
             int i = this.world.getTypeId(this.b, this.c, this.d);
 
             if (i == this.e) {
-                ++this.al;
-                if (this.al == 1200) {
-                    this.q();
+                ++this.h;
+                if (this.h == 1200) {
+                    this.C();
                 }
 
                 return;
@@ -94,10 +94,10 @@ public class EntitySnowball extends Entity {
             this.motX *= (double) (this.random.nextFloat() * 0.2F);
             this.motY *= (double) (this.random.nextFloat() * 0.2F);
             this.motZ *= (double) (this.random.nextFloat() * 0.2F);
-            this.al = 0;
-            this.am = 0;
+            this.h = 0;
+            this.i = 0;
         } else {
-            ++this.am;
+            ++this.i;
         }
 
         Vec3D vec3d = Vec3D.b(this.locX, this.locY, this.locZ);
@@ -118,7 +118,7 @@ public class EntitySnowball extends Entity {
             for (int j = 0; j < list.size(); ++j) {
                 Entity entity1 = (Entity) list.get(j);
 
-                if (entity1.c_() && (entity1 != this.ak || this.am >= 5)) {
+                if (entity1.d_() && (entity1 != this.g || this.i >= 5)) {
                     float f = 0.3F;
                     AxisAlignedBB axisalignedbb = entity1.boundingBox.b((double) f, (double) f, (double) f);
                     MovingObjectPosition movingobjectposition1 = axisalignedbb.a(vec3d, vec3d1);
@@ -140,7 +140,7 @@ public class EntitySnowball extends Entity {
         }
 
         if (movingobjectposition != null) {
-            if (movingobjectposition.g != null && movingobjectposition.g.a(this.ak, 0)) {
+            if (movingobjectposition.g != null && movingobjectposition.g.a(this.g, 0)) {
                 ;
             }
 
@@ -148,7 +148,7 @@ public class EntitySnowball extends Entity {
                 this.world.a("snowballpoof", this.locX, this.locY, this.locZ, 0.0D, 0.0D, 0.0D);
             }
 
-            this.q();
+            this.C();
         }
 
         this.locX += this.motX;
@@ -179,7 +179,7 @@ public class EntitySnowball extends Entity {
         float f2 = 0.99F;
         float f3 = 0.03F;
 
-        if (this.v()) {
+        if (this.g_()) {
             for (int l = 0; l < 4; ++l) {
                 float f4 = 0.25F;
 
@@ -206,19 +206,19 @@ public class EntitySnowball extends Entity {
     }
 
     public void b(NBTTagCompound nbttagcompound) {
-        this.b = nbttagcompound.c("xTile");
-        this.c = nbttagcompound.c("yTile");
-        this.d = nbttagcompound.c("zTile");
-        this.e = nbttagcompound.b("inTile") & 255;
-        this.a = nbttagcompound.b("shake") & 255;
-        this.f = nbttagcompound.b("inGround") == 1;
+        this.b = nbttagcompound.d("xTile");
+        this.c = nbttagcompound.d("yTile");
+        this.d = nbttagcompound.d("zTile");
+        this.e = nbttagcompound.c("inTile") & 255;
+        this.a = nbttagcompound.c("shake") & 255;
+        this.f = nbttagcompound.c("inGround") == 1;
     }
 
     public void b(EntityHuman entityhuman) {
-        if (this.f && this.ak == entityhuman && this.a <= 0 && entityhuman.inventory.a(new ItemStack(Item.ARROW, 1))) {
+        if (this.f && this.g == entityhuman && this.a <= 0 && entityhuman.inventory.a(new ItemStack(Item.ARROW, 1))) {
             this.world.a(this, "random.pop", 0.2F, ((this.random.nextFloat() - this.random.nextFloat()) * 0.7F + 1.0F) * 2.0F);
-            entityhuman.c(this, 1);
-            this.q();
+            entityhuman.b(this, 1);
+            this.C();
         }
     }
 }
