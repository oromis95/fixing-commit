@@ -9,17 +9,17 @@ public class ItemFishingRod extends Item {
 
     public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
         if (entityhuman.hookedFish != null) {
-            int i = entityhuman.hookedFish.d();
+            int i = entityhuman.hookedFish.h();
 
             itemstack.b(i);
-            entityhuman.K();
+            entityhuman.r();
         } else {
             world.a(entityhuman, "random.bow", 0.5F, 0.4F / (b.nextFloat() * 0.4F + 0.8F));
             if (!world.isStatic) {
                 world.a((Entity) (new EntityFish(world, entityhuman)));
             }
 
-            entityhuman.K();
+            entityhuman.r();
         }
 
         return itemstack;
