@@ -18,13 +18,13 @@ public class BlockLever extends Block {
         return world.d(i - 1, j, k) ? true : (world.d(i + 1, j, k) ? true : (world.d(i, j, k - 1) ? true : (world.d(i, j, k + 1) ? true : world.d(i, j - 1, k))));
     }
 
-    public void c(World world, int i, int j, int k, int l) {
+    public void d(World world, int i, int j, int k, int l) {
         int i1 = world.getData(i, j, k);
         int j1 = i1 & 8;
 
         i1 &= 7;
         if (l == 1 && world.d(i, j - 1, k)) {
-            i1 = 5 + world.l.nextInt(2);
+            i1 = 5 + world.k.nextInt(2);
         }
 
         if (l == 2 && world.d(i, j, k + 1)) {
@@ -46,23 +46,7 @@ public class BlockLever extends Block {
         world.c(i, j, k, i1 + j1);
     }
 
-    public void e(World world, int i, int j, int k) {
-        if (world.d(i - 1, j, k)) {
-            world.c(i, j, k, 1);
-        } else if (world.d(i + 1, j, k)) {
-            world.c(i, j, k, 2);
-        } else if (world.d(i, j, k - 1)) {
-            world.c(i, j, k, 3);
-        } else if (world.d(i, j, k + 1)) {
-            world.c(i, j, k, 4);
-        } else if (world.d(i, j - 1, k)) {
-            world.c(i, j, k, 5 + world.l.nextInt(2));
-        }
-
-        this.g(world, i, j, k);
-    }
-
-    public void b(World world, int i, int j, int k, int l) {
+    public void a(World world, int i, int j, int k, int l) {
         if (this.g(world, i, j, k)) {
             int i1 = world.getData(i, j, k) & 7;
             boolean flag = false;
@@ -88,7 +72,7 @@ public class BlockLever extends Block {
             }
 
             if (flag) {
-                this.a_(world, i, j, k, world.getData(i, j, k));
+                this.b_(world, i, j, k, world.getData(i, j, k));
                 world.e(i, j, k, 0);
             }
         }
@@ -96,7 +80,7 @@ public class BlockLever extends Block {
 
     private boolean g(World world, int i, int j, int k) {
         if (!this.a(world, i, j, k)) {
-            this.a_(world, i, j, k, world.getData(i, j, k));
+            this.b_(world, i, j, k, world.getData(i, j, k));
             world.e(i, j, k, 0);
             return false;
         } else {
@@ -181,7 +165,7 @@ public class BlockLever extends Block {
         return (iblockaccess.getData(i, j, k) & 8) > 0;
     }
 
-    public boolean d(World world, int i, int j, int k, int l) {
+    public boolean c(World world, int i, int j, int k, int l) {
         int i1 = world.getData(i, j, k);
 
         if ((i1 & 8) == 0) {
