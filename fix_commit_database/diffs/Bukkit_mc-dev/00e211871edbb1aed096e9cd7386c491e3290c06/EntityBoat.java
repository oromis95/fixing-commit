@@ -10,32 +10,32 @@ public class EntityBoat extends Entity {
     private int d;
     private double e;
     private double f;
-    private double ak;
-    private double al;
-    private double am;
+    private double g;
+    private double h;
+    private double i;
 
     public EntityBoat(World world) {
         super(world);
         this.a = 0;
         this.b = 0;
         this.c = 1;
-        this.i = true;
+        this.aC = true;
         this.a(1.5F, 0.6F);
         this.height = this.width / 2.0F;
-        this.M = false;
+        this.bg = false;
     }
 
     protected void a() {}
 
-    public AxisAlignedBB d(Entity entity) {
+    public AxisAlignedBB a_(Entity entity) {
         return entity.boundingBox;
     }
 
-    public AxisAlignedBB u() {
+    public AxisAlignedBB d() {
         return this.boundingBox;
     }
 
-    public boolean z() {
+    public boolean e_() {
         return true;
     }
 
@@ -59,7 +59,7 @@ public class EntityBoat extends Entity {
             this.c = -this.c;
             this.b = 10;
             this.a += i * 10;
-            this.y();
+            this.R();
             if (this.a > 40) {
                 int j;
 
@@ -71,7 +71,7 @@ public class EntityBoat extends Entity {
                     this.a(Item.STICK.id, 1, 0.0F);
                 }
 
-                this.q();
+                this.C();
             }
 
             return true;
@@ -80,12 +80,12 @@ public class EntityBoat extends Entity {
         }
     }
 
-    public boolean c_() {
+    public boolean d_() {
         return !this.dead;
     }
 
-    public void b_() {
-        super.b_();
+    public void f_() {
+        super.f_();
         if (this.b > 0) {
             --this.b;
         }
@@ -119,9 +119,9 @@ public class EntityBoat extends Entity {
             if (this.d > 0) {
                 d3 = this.locX + (this.e - this.locX) / (double) this.d;
                 d4 = this.locY + (this.f - this.locY) / (double) this.d;
-                d5 = this.locZ + (this.ak - this.locZ) / (double) this.d;
+                d5 = this.locZ + (this.g - this.locZ) / (double) this.d;
 
-                for (d6 = this.al - (double) this.yaw; d6 < -180.0D; d6 += 360.0D) {
+                for (d6 = this.h - (double) this.yaw; d6 < -180.0D; d6 += 360.0D) {
                     ;
                 }
 
@@ -130,10 +130,10 @@ public class EntityBoat extends Entity {
                 }
 
                 this.yaw = (float) ((double) this.yaw + d6 / (double) this.d);
-                this.pitch = (float) ((double) this.pitch + (this.am - (double) this.pitch) / (double) this.d);
+                this.pitch = (float) ((double) this.pitch + (this.i - (double) this.pitch) / (double) this.d);
                 --this.d;
                 this.a(d3, d4, d5);
-                this.b(this.yaw, this.pitch);
+                this.c(this.yaw, this.pitch);
             } else {
                 d3 = this.locX + this.motX;
                 d4 = this.locY + this.motY;
@@ -206,9 +206,9 @@ public class EntityBoat extends Entity {
                 }
             }
 
-            if (this.B && d5 > 0.15D) {
+            if (this.aV && d5 > 0.15D) {
                 if (!this.world.isStatic) {
-                    this.q();
+                    this.C();
 
                     int k;
 
@@ -254,15 +254,15 @@ public class EntityBoat extends Entity {
             }
 
             this.yaw = (float) ((double) this.yaw + d13);
-            this.b(this.yaw, this.pitch);
+            this.c(this.yaw, this.pitch);
             List list = this.world.b((Entity) this, this.boundingBox.b(0.20000000298023224D, 0.0D, 0.20000000298023224D));
 
             if (list != null && list.size() > 0) {
                 for (int l = 0; l < list.size(); ++l) {
                     Entity entity = (Entity) list.get(l);
 
-                    if (entity != this.passenger && entity.z() && entity instanceof EntityBoat) {
-                        entity.c((Entity) this);
+                    if (entity != this.passenger && entity.e_() && entity instanceof EntityBoat) {
+                        entity.h(this);
                     }
                 }
             }
@@ -273,12 +273,12 @@ public class EntityBoat extends Entity {
         }
     }
 
-    public void E() {
+    public void h_() {
         if (this.passenger != null) {
             double d0 = Math.cos((double) this.yaw * 3.141592653589793D / 180.0D) * 0.4D;
             double d1 = Math.sin((double) this.yaw * 3.141592653589793D / 180.0D) * 0.4D;
 
-            this.passenger.a(this.locX + d0, this.locY + this.k() + this.passenger.F(), this.locZ + d1);
+            this.passenger.a(this.locX + d0, this.locY + this.k() + this.passenger.B(), this.locZ + d1);
         }
     }
 
@@ -291,7 +291,7 @@ public class EntityBoat extends Entity {
             return true;
         } else {
             if (!this.world.isStatic) {
-                entityhuman.e(this);
+                entityhuman.b((Entity) this);
             }
 
             return true;
