@@ -7,7 +7,7 @@ public class EntityTNTPrimed extends Entity {
     public EntityTNTPrimed(World world) {
         super(world);
         this.a = 0;
-        this.i = true;
+        this.aC = true;
         this.a(0.98F, 0.98F);
         this.height = this.width / 2.0F;
     }
@@ -20,7 +20,7 @@ public class EntityTNTPrimed extends Entity {
         this.motX = (double) (-MathHelper.a(f * 3.1415927F / 180.0F) * 0.02F);
         this.motY = 0.20000000298023224D;
         this.motZ = (double) (-MathHelper.b(f * 3.1415927F / 180.0F) * 0.02F);
-        this.M = false;
+        this.bg = false;
         this.a = 80;
         this.lastX = d0;
         this.lastY = d1;
@@ -29,11 +29,11 @@ public class EntityTNTPrimed extends Entity {
 
     protected void a() {}
 
-    public boolean c_() {
+    public boolean d_() {
         return !this.dead;
     }
 
-    public void b_() {
+    public void f_() {
         this.lastX = this.locX;
         this.lastY = this.locY;
         this.lastZ = this.locZ;
@@ -49,14 +49,14 @@ public class EntityTNTPrimed extends Entity {
         }
 
         if (this.a-- <= 0) {
-            this.q();
-            this.d();
+            this.C();
+            this.h();
         } else {
             this.world.a("smoke", this.locX, this.locY + 0.5D, this.locZ, 0.0D, 0.0D, 0.0D);
         }
     }
 
-    private void d() {
+    private void h() {
         float f = 4.0F;
 
         this.world.a((Entity) null, this.locX, this.locY, this.locZ, f);
@@ -67,6 +67,6 @@ public class EntityTNTPrimed extends Entity {
     }
 
     protected void b(NBTTagCompound nbttagcompound) {
-        this.a = nbttagcompound.b("Fuse");
+        this.a = nbttagcompound.c("Fuse");
     }
 }
