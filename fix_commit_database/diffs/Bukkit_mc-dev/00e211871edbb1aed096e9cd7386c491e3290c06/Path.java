@@ -8,7 +8,7 @@ public class Path {
     public Path() {}
 
     public PathPoint a(PathPoint pathpoint) {
-        if (pathpoint.e >= 0) {
+        if (pathpoint.d >= 0) {
             throw new IllegalStateException("OW KNOWS!");
         } else {
             if (this.b == this.a.length) {
@@ -19,7 +19,7 @@ public class Path {
             }
 
             this.a[this.b] = pathpoint;
-            pathpoint.e = this.b;
+            pathpoint.d = this.b;
             this.a(this.b++);
             return pathpoint;
         }
@@ -38,18 +38,18 @@ public class Path {
             this.b(0);
         }
 
-        pathpoint.e = -1;
+        pathpoint.d = -1;
         return pathpoint;
     }
 
     public void a(PathPoint pathpoint, float f) {
-        float f1 = pathpoint.h;
+        float f1 = pathpoint.g;
 
-        pathpoint.h = f;
+        pathpoint.g = f;
         if (f < f1) {
-            this.a(pathpoint.e);
+            this.a(pathpoint.d);
         } else {
-            this.b(pathpoint.e);
+            this.b(pathpoint.d);
         }
     }
 
@@ -58,25 +58,25 @@ public class Path {
 
         int j;
 
-        for (float f = pathpoint.h; i > 0; i = j) {
+        for (float f = pathpoint.g; i > 0; i = j) {
             j = i - 1 >> 1;
             PathPoint pathpoint1 = this.a[j];
 
-            if (f >= pathpoint1.h) {
+            if (f >= pathpoint1.g) {
                 break;
             }
 
             this.a[i] = pathpoint1;
-            pathpoint1.e = i;
+            pathpoint1.d = i;
         }
 
         this.a[i] = pathpoint;
-        pathpoint.e = i;
+        pathpoint.d = i;
     }
 
     private void b(int i) {
         PathPoint pathpoint = this.a[i];
-        float f = pathpoint.h;
+        float f = pathpoint.g;
 
         while (true) {
             int j = 1 + (i << 1);
@@ -87,7 +87,7 @@ public class Path {
             }
 
             PathPoint pathpoint1 = this.a[j];
-            float f1 = pathpoint1.h;
+            float f1 = pathpoint1.g;
             PathPoint pathpoint2;
             float f2;
 
@@ -96,7 +96,7 @@ public class Path {
                 f2 = Float.POSITIVE_INFINITY;
             } else {
                 pathpoint2 = this.a[k];
-                f2 = pathpoint2.h;
+                f2 = pathpoint2.g;
             }
 
             if (f1 < f2) {
@@ -105,7 +105,7 @@ public class Path {
                 }
 
                 this.a[i] = pathpoint1;
-                pathpoint1.e = i;
+                pathpoint1.d = i;
                 i = j;
             } else {
                 if (f2 >= f) {
@@ -113,13 +113,13 @@ public class Path {
                 }
 
                 this.a[i] = pathpoint2;
-                pathpoint2.e = i;
+                pathpoint2.d = i;
                 i = k;
             }
         }
 
         this.a[i] = pathpoint;
-        pathpoint.e = i;
+        pathpoint.d = i;
     }
 
     public boolean c() {
