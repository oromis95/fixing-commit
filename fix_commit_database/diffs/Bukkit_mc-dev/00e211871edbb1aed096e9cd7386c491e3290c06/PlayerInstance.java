@@ -28,14 +28,14 @@ class PlayerInstance {
         this.c = i;
         this.d = j;
         this.e = new ChunkCoordIntPair(i, j);
-        PlayerManager.a(playermanager).e.A.d(i, j);
+        PlayerManager.a(playermanager).e.u.d(i, j);
     }
 
     public void a(EntityPlayer entityplayer) {
         if (this.b.contains(entityplayer)) {
             throw new IllegalStateException("Failed to add player. " + entityplayer + " already is in chunk " + this.c + ", " + this.d);
         } else {
-            entityplayer.ak.add(this.e);
+            entityplayer.g.add(this.e);
             entityplayer.a.b((Packet) (new Packet50PreChunk(this.e.a, this.e.b, true)));
             this.b.add(entityplayer);
             entityplayer.f.add(this.e);
@@ -55,11 +55,11 @@ class PlayerInstance {
                     PlayerManager.c(this.a).remove(this);
                 }
 
-                PlayerManager.a(this.a).e.A.c(this.c, this.d);
+                PlayerManager.a(this.a).e.u.c(this.c, this.d);
             }
 
             entityplayer.f.remove(this.e);
-            if (entityplayer.ak.contains(this.e)) {
+            if (entityplayer.g.contains(this.e)) {
                 entityplayer.a.b((Packet) (new Packet50PreChunk(this.c, this.d, false)));
             }
         }
@@ -114,7 +114,7 @@ class PlayerInstance {
         for (int i = 0; i < this.b.size(); ++i) {
             EntityPlayer entityplayer = (EntityPlayer) this.b.get(i);
 
-            if (entityplayer.ak.contains(this.e)) {
+            if (entityplayer.g.contains(this.e)) {
                 entityplayer.a.b(packet);
             }
         }
@@ -174,7 +174,7 @@ class PlayerInstance {
 
     private void a(TileEntity tileentity) {
         if (tileentity != null) {
-            Packet packet = tileentity.g();
+            Packet packet = tileentity.e();
 
             if (packet != null) {
                 this.a(packet);
