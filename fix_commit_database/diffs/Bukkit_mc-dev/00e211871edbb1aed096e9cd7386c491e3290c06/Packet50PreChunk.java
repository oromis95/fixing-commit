@@ -10,11 +10,11 @@ public class Packet50PreChunk extends Packet {
     public boolean c;
 
     public Packet50PreChunk() {
-        this.l = false;
+        this.k = false;
     }
 
     public Packet50PreChunk(int i, int j, boolean flag) {
-        this.l = false;
+        this.k = false;
         this.a = i;
         this.b = j;
         this.c = flag;
