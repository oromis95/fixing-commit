@@ -44,8 +44,8 @@ public class BlockChest extends BlockContainer {
     public void b(World world, int i, int j, int k) {
         TileEntityChest tileentitychest = (TileEntityChest) world.getTileEntity(i, j, k);
 
-        for (int l = 0; l < tileentitychest.h_(); ++l) {
-            ItemStack itemstack = tileentitychest.a(l);
+        for (int l = 0; l < tileentitychest.m_(); ++l) {
+            ItemStack itemstack = tileentitychest.c_(l);
 
             if (itemstack != null) {
                 float f = this.a.nextFloat() * 0.8F + 0.1F;
