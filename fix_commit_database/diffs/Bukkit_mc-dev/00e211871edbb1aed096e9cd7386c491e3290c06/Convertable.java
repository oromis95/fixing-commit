@@ -0,0 +1,8 @@
+package net.minecraft.server;
+
+public interface Convertable {
+
+    boolean a(String s);
+
+    boolean a(String s, IProgressUpdate iprogressupdate);
+}
