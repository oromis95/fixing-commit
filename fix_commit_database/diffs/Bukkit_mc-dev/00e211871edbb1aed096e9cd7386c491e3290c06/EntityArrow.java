@@ -8,11 +8,11 @@ public class EntityArrow extends Entity {
     private int d = -1;
     private int e = -1;
     private int f = 0;
-    private boolean ak = false;
+    private boolean g = false;
     public int a = 0;
     public EntityLiving b;
-    private int al;
-    private int am = 0;
+    private int h;
+    private int i = 0;
 
     public EntityArrow(World world) {
         super(world);
@@ -30,7 +30,7 @@ public class EntityArrow extends Entity {
         super(world);
         this.b = entityliving;
         this.a(0.5F, 0.5F);
-        this.c(entityliving.locX, entityliving.locY + (double) entityliving.w(), entityliving.locZ, entityliving.yaw, entityliving.pitch);
+        this.c(entityliving.locX, entityliving.locY + (double) entityliving.p(), entityliving.locZ, entityliving.yaw, entityliving.pitch);
         this.locX -= (double) (MathHelper.b(this.yaw / 180.0F * 3.1415927F) * 0.16F);
         this.locY -= 0.10000000149011612D;
         this.locZ -= (double) (MathHelper.a(this.yaw / 180.0F * 3.1415927F) * 0.16F);
@@ -63,11 +63,11 @@ public class EntityArrow extends Entity {
 
         this.lastYaw = this.yaw = (float) (Math.atan2(d0, d2) * 180.0D / 3.1415927410125732D);
         this.lastPitch = this.pitch = (float) (Math.atan2(d1, (double) f3) * 180.0D / 3.1415927410125732D);
-        this.al = 0;
+        this.h = 0;
     }
 
-    public void b_() {
-        super.b_();
+    public void f_() {
+        super.f_();
         if (this.lastPitch == 0.0F && this.lastYaw == 0.0F) {
             float f = MathHelper.a(this.motX * this.motX + this.motZ * this.motZ);
 
@@ -79,26 +79,26 @@ public class EntityArrow extends Entity {
             --this.a;
         }
 
-        if (this.ak) {
+        if (this.g) {
             int i = this.world.getTypeId(this.c, this.d, this.e);
 
             if (i == this.f) {
-                ++this.al;
-                if (this.al == 1200) {
-                    this.q();
+                ++this.h;
+                if (this.h == 1200) {
+                    this.C();
                 }
 
                 return;
             }
 
-            this.ak = false;
+            this.g = false;
             this.motX *= (double) (this.random.nextFloat() * 0.2F);
             this.motY *= (double) (this.random.nextFloat() * 0.2F);
             this.motZ *= (double) (this.random.nextFloat() * 0.2F);
-            this.al = 0;
-            this.am = 0;
+            this.h = 0;
+            this.i = 0;
         } else {
-            ++this.am;
+            ++this.i;
         }
 
         Vec3D vec3d = Vec3D.b(this.locX, this.locY, this.locZ);
@@ -120,7 +120,7 @@ public class EntityArrow extends Entity {
         for (int j = 0; j < list.size(); ++j) {
             Entity entity1 = (Entity) list.get(j);
 
-            if (entity1.c_() && (entity1 != this.b || this.am >= 5)) {
+            if (entity1.d_() && (entity1 != this.b || this.i >= 5)) {
                 f1 = 0.3F;
                 AxisAlignedBB axisalignedbb = entity1.boundingBox.b((double) f1, (double) f1, (double) f1);
                 MovingObjectPosition movingobjectposition1 = axisalignedbb.a(vec3d, vec3d1);
@@ -146,14 +146,14 @@ public class EntityArrow extends Entity {
             if (movingobjectposition.g != null) {
                 if (movingobjectposition.g.a(this.b, 4)) {
                     this.world.a(this, "random.drr", 1.0F, 1.2F / (this.random.nextFloat() * 0.2F + 0.9F));
-                    this.q();
+                    this.C();
                 } else {
                     this.motX *= -0.10000000149011612D;
                     this.motY *= -0.10000000149011612D;
                     this.motZ *= -0.10000000149011612D;
                     this.yaw += 180.0F;
                     this.lastYaw += 180.0F;
-                    this.am = 0;
+                    this.i = 0;
                 }
             } else {
                 this.c = movingobjectposition.b;
@@ -168,7 +168,7 @@ public class EntityArrow extends Entity {
                 this.locY -= this.motY / (double) f2 * 0.05000000074505806D;
                 this.locZ -= this.motZ / (double) f2 * 0.05000000074505806D;
                 this.world.a(this, "random.drr", 1.0F, 1.2F / (this.random.nextFloat() * 0.2F + 0.9F));
-                this.ak = true;
+                this.g = true;
                 this.a = 7;
             }
         }
@@ -200,7 +200,7 @@ public class EntityArrow extends Entity {
         float f3 = 0.99F;
 
         f1 = 0.03F;
-        if (this.v()) {
+        if (this.g_()) {
             for (int k = 0; k < 4; ++k) {
                 float f4 = 0.25F;
 
@@ -223,24 +223,24 @@ public class EntityArrow extends Entity {
         nbttagcompound.a("zTile", (short) this.e);
         nbttagcompound.a("inTile", (byte) this.f);
         nbttagcompound.a("shake", (byte) this.a);
-        nbttagcompound.a("inGround", (byte) (this.ak ? 1 : 0));
+        nbttagcompound.a("inGround", (byte) (this.g ? 1 : 0));
     }
 
     public void b(NBTTagCompound nbttagcompound) {
-        this.c = nbttagcompound.c("xTile");
-        this.d = nbttagcompound.c("yTile");
-        this.e = nbttagcompound.c("zTile");
-        this.f = nbttagcompound.b("inTile") & 255;
-        this.a = nbttagcompound.b("shake") & 255;
-        this.ak = nbttagcompound.b("inGround") == 1;
+        this.c = nbttagcompound.d("xTile");
+        this.d = nbttagcompound.d("yTile");
+        this.e = nbttagcompound.d("zTile");
+        this.f = nbttagcompound.c("inTile") & 255;
+        this.a = nbttagcompound.c("shake") & 255;
+        this.g = nbttagcompound.c("inGround") == 1;
     }
 
     public void b(EntityHuman entityhuman) {
         if (!this.world.isStatic) {
-            if (this.ak && this.b == entityhuman && this.a <= 0 && entityhuman.inventory.a(new ItemStack(Item.ARROW, 1))) {
+            if (this.g && this.b == entityhuman && this.a <= 0 && entityhuman.inventory.a(new ItemStack(Item.ARROW, 1))) {
                 this.world.a(this, "random.pop", 0.2F, ((this.random.nextFloat() - this.random.nextFloat()) * 0.7F + 1.0F) * 2.0F);
-                entityhuman.c(this, 1);
-                this.q();
+                entityhuman.b(this, 1);
+                this.C();
             }
         }
     }
