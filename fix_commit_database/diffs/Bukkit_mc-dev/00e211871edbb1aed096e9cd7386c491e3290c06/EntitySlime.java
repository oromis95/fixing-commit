@@ -13,10 +13,10 @@ public class EntitySlime extends EntityLiving implements IMonster {
         this.c = 1 << this.random.nextInt(3);
         this.height = 0.0F;
         this.d = this.random.nextInt(20) + 10;
-        this.a(this.c);
+        this.e(this.c);
     }
 
-    public void a(int i) {
+    public void e(int i) {
         this.c = i;
         this.a(0.6F * (float) i, 0.6F * (float) i);
         this.health = i * i;
@@ -30,14 +30,14 @@ public class EntitySlime extends EntityLiving implements IMonster {
 
     public void b(NBTTagCompound nbttagcompound) {
         super.b(nbttagcompound);
-        this.c = nbttagcompound.d("Size") + 1;
+        this.c = nbttagcompound.e("Size") + 1;
     }
 
-    public void b_() {
+    public void f_() {
         this.b = this.a;
         boolean flag = this.onGround;
 
-        super.b_();
+        super.f_();
         if (this.onGround && !flag) {
             for (int i = 0; i < this.c * 8; ++i) {
                 float f = this.random.nextFloat() * 3.1415927F * 2.0F;
@@ -58,7 +58,7 @@ public class EntitySlime extends EntityLiving implements IMonster {
         this.a *= 0.6F;
     }
 
-    protected void d() {
+    protected void c_() {
         EntityHuman entityhuman = this.world.a(this, 16.0D);
 
         if (entityhuman != null) {
@@ -71,40 +71,40 @@ public class EntitySlime extends EntityLiving implements IMonster {
                 this.d /= 3;
             }
 
-            this.bA = true;
+            this.ax = true;
             if (this.c > 1) {
                 this.world.a(this, "mob.slime", this.i(), ((this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F) * 0.8F);
             }
 
             this.a = 1.0F;
-            this.bx = 1.0F - this.random.nextFloat() * 2.0F;
-            this.by = (float) (1 * this.c);
+            this.au = 1.0F - this.random.nextFloat() * 2.0F;
+            this.av = (float) (1 * this.c);
         } else {
-            this.bA = false;
+            this.ax = false;
             if (this.onGround) {
-                this.bx = this.by = 0.0F;
+                this.au = this.av = 0.0F;
             }
         }
     }
 
-    public void q() {
+    public void C() {
         if (this.c > 1 && this.health == 0) {
             for (int i = 0; i < 4; ++i) {
                 float f = ((float) (i % 2) - 0.5F) * (float) this.c / 4.0F;
                 float f1 = ((float) (i / 2) - 0.5F) * (float) this.c / 4.0F;
                 EntitySlime entityslime = new EntitySlime(this.world);
 
-                entityslime.a(this.c / 2);
+                entityslime.e(this.c / 2);
                 entityslime.c(this.locX + (double) f, this.locY + 0.5D, this.locZ + (double) f1, this.random.nextFloat() * 360.0F, 0.0F);
                 this.world.a((Entity) entityslime);
             }
         }
 
-        super.q();
+        super.C();
     }
 
     public void b(EntityHuman entityhuman) {
-        if (this.c > 1 && this.i(entityhuman) && (double) this.a(entityhuman) < 0.6D * (double) this.c && entityhuman.a(this, this.c)) {
+        if (this.c > 1 && this.e(entityhuman) && (double) this.f(entityhuman) < 0.6D * (double) this.c && entityhuman.a(this, this.c)) {
             this.world.a(this, "mob.slimeattack", 1.0F, (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
         }
     }
@@ -124,7 +124,7 @@ public class EntitySlime extends EntityLiving implements IMonster {
     public boolean b() {
         Chunk chunk = this.world.b(MathHelper.b(this.locX), MathHelper.b(this.locZ));
 
-        return (this.c == 1 || this.world.k > 0) && this.random.nextInt(10) == 0 && chunk.a(987234911L).nextInt(10) == 0 && this.locY < 16.0D;
+        return (this.c == 1 || this.world.j > 0) && this.random.nextInt(10) == 0 && chunk.a(987234911L).nextInt(10) == 0 && this.locY < 16.0D;
     }
 
     protected float i() {
