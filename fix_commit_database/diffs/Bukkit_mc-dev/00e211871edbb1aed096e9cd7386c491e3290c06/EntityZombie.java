@@ -5,20 +5,20 @@ public class EntityZombie extends EntityMonster {
     public EntityZombie(World world) {
         super(world);
         this.texture = "/mob/zombie.png";
-        this.bC = 0.5F;
+        this.az = 0.5F;
         this.c = 5;
     }
 
-    public void o() {
-        if (this.world.b()) {
-            float f = this.b(1.0F);
+    public void q() {
+        if (this.world.c()) {
+            float f = this.c(1.0F);
 
             if (f > 0.5F && this.world.i(MathHelper.b(this.locX), MathHelper.b(this.locY), MathHelper.b(this.locZ)) && this.random.nextFloat() * 30.0F < (f - 0.4F) * 2.0F) {
                 this.fireTicks = 300;
             }
         }
 
-        super.o();
+        super.q();
     }
 
     protected String e() {
