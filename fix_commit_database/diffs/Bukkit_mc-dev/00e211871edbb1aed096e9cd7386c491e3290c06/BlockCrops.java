@@ -33,7 +33,7 @@ public class BlockCrops extends BlockFlower {
         }
     }
 
-    public void c(World world, int i, int j, int k) {
+    public void c_(World world, int i, int j, int k) {
         world.c(i, j, k, 7);
     }
 
@@ -78,15 +78,23 @@ public class BlockCrops extends BlockFlower {
         return f;
     }
 
-    public void a(World world, int i, int j, int k, int l) {
-        super.a(world, i, j, k, l);
+    public int a(int i, int j) {
+        if (j < 0) {
+            j = 7;
+        }
+
+        return this.textureId + j;
+    }
+
+    public void b(World world, int i, int j, int k, int l) {
+        super.b(world, i, j, k, l);
         if (!world.isStatic) {
             for (int i1 = 0; i1 < 3; ++i1) {
-                if (world.l.nextInt(15) <= l) {
+                if (world.k.nextInt(15) <= l) {
                     float f = 0.7F;
-                    float f1 = world.l.nextFloat() * f + (1.0F - f) * 0.5F;
-                    float f2 = world.l.nextFloat() * f + (1.0F - f) * 0.5F;
-                    float f3 = world.l.nextFloat() * f + (1.0F - f) * 0.5F;
+                    float f1 = world.k.nextFloat() * f + (1.0F - f) * 0.5F;
+                    float f2 = world.k.nextFloat() * f + (1.0F - f) * 0.5F;
+                    float f3 = world.k.nextFloat() * f + (1.0F - f) * 0.5F;
                     EntityItem entityitem = new EntityItem(world, (double) ((float) i + f1), (double) ((float) j + f2), (double) ((float) k + f3), new ItemStack(Item.SEEDS));
 
                     entityitem.c = 10;
