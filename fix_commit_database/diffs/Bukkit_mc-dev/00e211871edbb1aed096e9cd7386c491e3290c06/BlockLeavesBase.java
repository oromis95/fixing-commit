@@ -2,11 +2,11 @@ package net.minecraft.server;
 
 public class BlockLeavesBase extends Block {
 
-    protected boolean a;
+    protected boolean b;
 
     protected BlockLeavesBase(int i, int j, Material material, boolean flag) {
         super(i, j, material);
-        this.a = flag;
+        this.b = flag;
     }
 
     public boolean a() {
@@ -16,6 +16,6 @@ public class BlockLeavesBase extends Block {
     public boolean a(IBlockAccess iblockaccess, int i, int j, int k, int l) {
         int i1 = iblockaccess.getTypeId(i, j, k);
 
-        return !this.a && i1 == this.id ? false : super.a(iblockaccess, i, j, k, l);
+        return !this.b && i1 == this.id ? false : super.a(iblockaccess, i, j, k, l);
     }
 }
