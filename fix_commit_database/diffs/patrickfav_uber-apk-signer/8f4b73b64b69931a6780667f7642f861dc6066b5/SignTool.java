@@ -63,42 +63,48 @@ public class SignTool {
             }
             if (!arguments.onlyVerify) {
                 signingConfigGen = new SigningConfigGen(arguments);
-                log("Using keystore mode " + signingConfigGen.signingConfig.location + ".");
+                log(signingConfigGen.signingConfig.description());
             }
 
             long startTime = System.currentTimeMillis();
             int successCount = 0;
             int errorCount = 0;
+            int iterCount = 0;
 
             List<File> tempFilesToDelete = new ArrayList<>();
             for (File targetApkFile : targetApkFiles) {
                 if (targetApkFile.isFile() && FileUtil.getFileExtension(targetApkFile).toLowerCase().equals("apk")) {
-                    File rootSource = targetApkFile;
+                    iterCount++;
+                    File rootTargetFile = targetApkFile;
 
                     log("\n\r" + targetApkFile.getName());
                     if (arguments.dryRun) {
                         log("\t - (skip)");
+                        continue;
                     }
 
-                    if (!arguments.onlyVerify && verify(targetApkFile, rootSource, false, true)) {
+                    if (!arguments.onlyVerify && verifySign(targetApkFile, rootTargetFile, false, true)) {
                         logErr("\t - already signed SKIP");
                         errorCount++;
                         continue;
                     }
 
                     if (!arguments.onlyVerify) {
-                        targetApkFile = zipAlign(false, targetApkFile, rootSource, outFolder, zipAlignExecutor, arguments, executedCommands);
+                        targetApkFile = zipAlign(targetApkFile, rootTargetFile, outFolder, zipAlignExecutor, arguments, executedCommands);
 
                         if (targetApkFile == null) {
-                            throw new IllegalStateException("zipalign was not verified - this is very strange - maybe no read auth?");
+                            throw new IllegalStateException("could not execute zipalign");
+                        }
+
+                        if (!arguments.overwrite) {
+                            tempFilesToDelete.add(targetApkFile);
                         }
 
-                        tempFilesToDelete.add(targetApkFile);
-                        targetApkFile = sign(targetApkFile, rootSource, outFolder, signingConfigGen.signingConfig, arguments);
+                        targetApkFile = sign(targetApkFile, rootTargetFile, outFolder, signingConfigGen.signingConfig, arguments);
                     }
 
-                    boolean zipAlignVerified = arguments.skipZipAlign || zipAlign(true, targetApkFile, rootSource, outFolder, zipAlignExecutor, arguments, executedCommands) != null;
-                    boolean sigVerified = verify(targetApkFile, rootSource, arguments.verbose, false);
+                    boolean zipAlignVerified = arguments.skipZipAlign || verifyZipAlign(targetApkFile, rootTargetFile, zipAlignExecutor, arguments, executedCommands);
+                    boolean sigVerified = verifySign(targetApkFile, rootTargetFile, arguments.verbose, false);
 
                     if (zipAlignVerified && sigVerified) {
                         successCount++;
@@ -108,6 +114,10 @@ public class SignTool {
                 }
             }
 
+            if (iterCount == 0) {
+                log("No apks found.");
+            }
+
             for (File file : tempFilesToDelete) {
                 if (arguments.verbose) {
                     log("delete temp file " + file);
@@ -116,7 +126,8 @@ public class SignTool {
             }
 
 
-            log(String.format(Locale.US, "\n[%s] Successfully processed %d APKs with %d having errors in %.2f seconds.", new Date().toString(), successCount, errorCount, (double) (System.currentTimeMillis() - startTime) / 1000.0));
+            log(String.format(Locale.US, "\n[%s] Successfully processed %d APKs and %d errors in %.2f seconds.",
+                    new Date().toString(), successCount, errorCount, (double) (System.currentTimeMillis() - startTime) / 1000.0));
 
             if (arguments.debug) {
                 log(getCommandHistory(executedCommands));
@@ -142,61 +153,70 @@ public class SignTool {
         }
     }
 
-    private static File zipAlign(boolean onlyVerify, File targetApkFile, File rootTargetFile, File outFolder, ZipAlignExecutor executor, Arg arguments, List<CmdUtil.Result> cmdList) {
+    private static File zipAlign(File targetApkFile, File rootTargetFile, File outFolder, ZipAlignExecutor executor, Arg arguments, List<CmdUtil.Result> cmdList) {
         if (!arguments.skipZipAlign) {
-            File outFile = targetApkFile;
-            if (!onlyVerify) {
-                if (!arguments.overwrite) {
-                    String fileName = FileUtil.getFileNameWithoutExtension(targetApkFile);
-                    fileName = fileName.replace("-unaligned", "");
-                    fileName += "_aligned";
-                    outFile = new File(outFolder, fileName + "." + FileUtil.getFileExtension(targetApkFile));
-                }
 
-                if (outFile.exists()) {
-                    outFile.delete();
-                }
+            String fileName = FileUtil.getFileNameWithoutExtension(targetApkFile);
+            fileName = fileName.replace("-unaligned", "");
+            fileName += "-aligned";
+            File outFile = new File(outFolder, fileName + "." + FileUtil.getFileExtension(targetApkFile));
+
+            if (outFile.exists()) {
+                outFile.delete();
             }
 
             if (executor.isExecutableFound()) {
                 String logMsg = "\t- ";
 
-                if (!onlyVerify) {
-                    CmdUtil.Result zipAlignResult = CmdUtil.runCmd(CmdUtil.concat(executor.zipAlignExecutable, new String[]{"4", targetApkFile.getAbsolutePath(), outFile.getAbsolutePath()}));
-                    cmdList.add(zipAlignResult);
-                    if (zipAlignResult.success()) {
-                        logMsg += "aligned & ";
-                    } else {
-                        logMsg += "could not align ";
-                    }
+                CmdUtil.Result zipAlignResult = CmdUtil.runCmd(CmdUtil.concat(executor.zipAlignExecutable, new String[]{"4", targetApkFile.getAbsolutePath(), outFile.getAbsolutePath()}));
+                cmdList.add(zipAlignResult);
+                if (zipAlignResult.success()) {
+                    logMsg += "zipalign success";
+                } else {
+                    logMsg += "could not align ";
                 }
 
-                CmdUtil.Result zipAlignVerifyResult = CmdUtil.runCmd(CmdUtil.concat(executor.zipAlignExecutable, new String[]{"-c", "4", outFile.getAbsolutePath()}));
-                cmdList.add(zipAlignVerifyResult);
-                if (zipAlignVerifyResult.success()) {
-                    logMsg += "zipalign verified";
-                } else {
-                    logMsg += "zipalign VERIFY FAILED";
+                logConditionally(logMsg, outFile, !rootTargetFile.equals(outFile), false);
 
+                if (arguments.overwrite) {
+                    targetApkFile.delete();
+                    outFile.renameTo(targetApkFile);
+                    outFile = targetApkFile;
                 }
+                return zipAlignResult.success() ? outFile : null;
+            } else {
+                throw new IllegalArgumentException("could not find zipalign - either skip it or provide a proper location");
+            }
 
-                if (!rootTargetFile.equals(outFile) || !zipAlignVerifyResult.success()) {
-                    logMsg += " (" + outFile.getName() + ")";
-                }
+        }
+        return targetApkFile;
+    }
+
+
+    private static boolean verifyZipAlign(File targetApkFile, File rootTargetFile, ZipAlignExecutor executor, Arg arguments, List<CmdUtil.Result> cmdList) {
+        if (!arguments.skipZipAlign) {
+            if (executor.isExecutableFound()) {
+                String logMsg = "\t- ";
+
+                CmdUtil.Result zipAlignVerifyResult = CmdUtil.runCmd(CmdUtil.concat(executor.zipAlignExecutable, new String[]{"-c", "4", targetApkFile.getAbsolutePath()}));
+                cmdList.add(zipAlignVerifyResult);
+                boolean success = zipAlignVerifyResult.success();
 
-                if (zipAlignVerifyResult.success()) {
-                    log(logMsg);
+                if (success) {
+                    logMsg += "zipalign verified";
                 } else {
-                    logErr(logMsg);
+                    logMsg += "zipalign VERIFY FAILED";
                 }
 
-                return zipAlignVerifyResult.success() ? outFile : null;
+                logConditionally(logMsg, targetApkFile, !targetApkFile.equals(rootTargetFile), !success);
+
+                return zipAlignVerifyResult.success();
             } else {
                 throw new IllegalArgumentException("could not find zipalign - either skip it or provide a proper location");
             }
 
         }
-        return targetApkFile;
+        return true;
     }
 
     private static File sign(File targetApkFile, File rootTargetFile, File outFolder, SigningConfig signingConfig, Arg arguments) {
@@ -206,19 +226,23 @@ public class SignTool {
             if (!arguments.overwrite) {
                 String fileName = FileUtil.getFileNameWithoutExtension(targetApkFile);
                 fileName = fileName.replace("-unsigned", "");
-                fileName += "_signed";
+                if (signingConfig.isDebugType) {
+                    fileName += "-debugSigned";
+                } else {
+                    fileName += "-signed";
+                }
                 outFile = new File(outFolder, fileName + "." + FileUtil.getFileExtension(targetApkFile));
-            }
 
-            if (outFile.exists()) {
-                outFile.delete();
+                if (outFile.exists()) {
+                    outFile.delete();
+                }
             }
 
             String[] argArr = new String[]{
                     "sign",
                     "--ks", signingConfig.keystore.getAbsolutePath(),
-                    "--ks-pass", signingConfig.ksPass == null ? "stdout" : "pass:" + signingConfig.ksPass,
-                    "--key-pass", signingConfig.ksKeyPass == null ? "stdout" : "pass:" + signingConfig.ksPass,
+                    "--ks-pass", signingConfig.ksPass == null ? "stdin" : "pass:" + signingConfig.ksPass,
+                    "--key-pass", signingConfig.ksKeyPass == null ? "stdin" : "pass:" + signingConfig.ksPass,
                     "--ks-key-alias", signingConfig.ksAlias,
                     "--out", outFile.getAbsolutePath()
             };
@@ -233,7 +257,7 @@ public class SignTool {
 
             ApkSignerTool.main(argArr);
 
-            String logMsg = "\t- signed";
+            String logMsg = "\t- sign succuess";
 
 
             if (!rootTargetFile.equals(outFile)) {
@@ -249,37 +273,39 @@ public class SignTool {
         }
     }
 
-    private static boolean verify(File targetApkFile, File rootTargetFile, boolean verbose, boolean noLog) {
+    private static boolean verifySign(File targetApkFile, File rootTargetFile, boolean verbose, boolean noLog) {
         try {
             AndroidApkSignerVerify verifier = new AndroidApkSignerVerify();
-            AndroidApkSignerVerify.Result result = verifier.verify(targetApkFile, verbose, null, null, false, verbose);
+            AndroidApkSignerVerify.Result result = verifier.verify(targetApkFile, null, null, false);
 
             if (!noLog) {
                 String logMsg;
 
                 if (result.verified) {
-                    logMsg = "\t- signature verified";
+                    logMsg = "\t- signature verified [" + (result.v1Schema ? "v1" : "") + (result.v1Schema && result.v2Schema ? ", " : "") + (result.v2Schema ? "v2" : "") + "] ";
                 } else {
                     logMsg = "\t- signature VERIFY FAILED (" + targetApkFile.getName() + ")";
                 }
 
-                if (!rootTargetFile.equals(targetApkFile)) {
-                    logMsg += " (" + targetApkFile.getName() + ")";
-                }
+                logConditionally(logMsg, targetApkFile, !rootTargetFile.equals(targetApkFile), !result.verified);
 
                 if (result.verified) {
-                    log(logMsg);
-                } else {
-                    logErr(logMsg);
-                }
-
-                if (verbose) {
-                    log(result.log);
+                    for (AndroidApkSignerVerify.CertInfo certInfo : result.certInfoList) {
+                        log("\t\t" + certInfo.subjectAndIssuerDn);
+                        log("\t\tSHA256: " + certInfo.certSha256 + " / " + certInfo.sigAlgo);
+                        if (verbose) {
+                            log("\t\tSHA1: " + certInfo.certSha1);
+                            log("\t\tPublic Key SHA256: " + certInfo.pubSha256);
+                            log("\t\tPublic Key SHA1: " + certInfo.pubSha1);
+                            log("\t\tPublic Key Algo: " + certInfo.pubAlgo + " " + certInfo.pubKeysize);
+                        }
+                        log("\t\tExpires: " + certInfo.expiry.toString() + " / Begin: " + certInfo.beginValidity);
+                    }
                 }
             }
             return result.verified;
         } catch (Exception e) {
-            throw new IllegalStateException("could not verify " + targetApkFile + ": " + e.getMessage());
+            throw new IllegalStateException("could not verifySign " + targetApkFile + ": " + e.getMessage());
         }
     }
 
@@ -299,4 +325,16 @@ public class SignTool {
     private static void log(String msg) {
         System.out.println(msg);
     }
+
+    private static void logConditionally(String logMsg, File file, boolean appendFile, boolean error) {
+        if (appendFile) {
+            logMsg += " (" + file.getName() + ")";
+        }
+
+        if (error) {
+            logErr(logMsg);
+        } else {
+            log(logMsg);
+        }
+    }
 }
