@@ -50,12 +50,11 @@ public class FileArgParser {
             if (file.isDirectory()) {
                 parents.add(file);
             } else {
-                //Fixed bug: java -jar uber-apk-signer*.jar -a test_unsigned.apk throw NullPointerException
                 try {
                     file = new File(file.getCanonicalPath());
                     parents.add(file.getParentFile());
                 } catch (IOException e) {
-                    e.printStackTrace();
+                    throw new IllegalStateException("could not add parent folder", e);
                 }
             }
         }
