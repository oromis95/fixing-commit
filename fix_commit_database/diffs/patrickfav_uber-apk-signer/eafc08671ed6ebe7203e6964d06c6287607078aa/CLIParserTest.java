@@ -10,70 +10,92 @@ public class CLIParserTest {
     @Test
     public void testSimpleWithOnlyApkFile() throws Exception {
         Arg parsedArg = CLIParser.parse(asArgArray("-" + CLIParser.ARG_APK_FILE + " ./"));
-        Arg expectedArg = new Arg("./", false, null, null, null, null, false, false, false, false, false, false, null);
+        Arg expectedArg = new Arg("./", null, false, null, null, null, null, false, false, false, false, false, false, null);
+        assertEquals(expectedArg, parsedArg);
+    }
+
+    @Test
+    public void testWithOut() throws Exception {
+        Arg parsedArg = CLIParser.parse(asArgArray("-" + CLIParser.ARG_APK_FILE + " ./ --out ./test"));
+        Arg expectedArg = new Arg("./", "./test", false, null, null, null, null, false, false, false, false, false, false, null);
         assertEquals(expectedArg, parsedArg);
     }
 
     @Test
     public void testWithKeystore() throws Exception {
         Arg parsedArg = CLIParser.parse(asArgArray("-" + CLIParser.ARG_APK_FILE + " ./ --ks my-keystore.jks"));
-        Arg expectedArg = new Arg("./", false, "my-keystore.jks", null, null, null, false, false, false, false, false, false, null);
+        Arg expectedArg = new Arg("./", null, false, "my-keystore.jks", null, null, null, false, false, false, false, false, false, null);
         assertEquals(expectedArg, parsedArg);
     }
 
     @Test
     public void testWithKeystoreAndAlias() throws Exception {
         Arg parsedArg = CLIParser.parse(asArgArray("-" + CLIParser.ARG_APK_FILE + " ./ --ks my-keystore.jks --ksAlias debugAlias"));
-        Arg expectedArg = new Arg("./", false, "my-keystore.jks", null, null, "debugAlias", false, false, false, false, false, false, null);
+        Arg expectedArg = new Arg("./", null, false, "my-keystore.jks", null, null, "debugAlias", false, false, false, false, false, false, null);
         assertEquals(expectedArg, parsedArg);
     }
 
     @Test
     public void testWithKeystoreAndAliasAndPws() throws Exception {
         Arg parsedArg = CLIParser.parse(asArgArray("-" + CLIParser.ARG_APK_FILE + " ./ --ks my-keystore.jks --ksAlias debugAlias --ksPass secret --ksKeyPass secret2"));
-        Arg expectedArg = new Arg("./", false, "my-keystore.jks", "secret", "secret2", "debugAlias", false, false, false, false, false, false, null);
+        Arg expectedArg = new Arg("./", null, false, "my-keystore.jks", "secret", "secret2", "debugAlias", false, false, false, false, false, false, null);
         assertEquals(expectedArg, parsedArg);
     }
 
     @Test
     public void testWithOnlyVerify() throws Exception {
         Arg parsedArg = CLIParser.parse(asArgArray("-" + CLIParser.ARG_APK_FILE + " ./ --" + CLIParser.ARG_VERIFY));
-        Arg expectedArg = new Arg("./", false, null, null, null, null, false, false, false, false, false, true, null);
+        Arg expectedArg = new Arg("./", null, false, null, null, null, null, false, false, false, false, false, true, null);
         assertEquals(expectedArg, parsedArg);
     }
 
     @Test
     public void testWithDebug() throws Exception {
         Arg parsedArg = CLIParser.parse(asArgArray("-" + CLIParser.ARG_APK_FILE + "./ --debug"));
-        Arg expectedArg = new Arg("./", false, null, null, null, null, false, false, false, false, true, false, null);
+        Arg expectedArg = new Arg("./", null, false, null, null, null, null, false, false, false, false, true, false, null);
         assertEquals(expectedArg, parsedArg);
     }
 
+    @Test
+    public void testWithDebugSigned() throws Exception {
+        Arg parsedArg = CLIParser.parse(asArgArray("-" + CLIParser.ARG_APK_FILE + "./ --debugSigned"));
+        Arg expectedArg = new Arg("./", null, true, null, null, null, null, false, false, false, false, false, false, null);
+        assertEquals(expectedArg, parsedArg);
+    }
+
+    @Test
+    public void testWithDebugAndDebugSigned() throws Exception {
+        Arg parsedArg = CLIParser.parse(asArgArray("-" + CLIParser.ARG_APK_FILE + "./ --debug --debugSigned"));
+        Arg expectedArg = new Arg("./", null, true, null, null, null, null, false, false, false, false, true, false, null);
+        assertEquals(expectedArg, parsedArg);
+    }
+
+
     @Test
     public void testWithSkipZipAlign() throws Exception {
         Arg parsedArg = CLIParser.parse(asArgArray("-" + CLIParser.ARG_APK_FILE + "./ --skipZipAlign"));
-        Arg expectedArg = new Arg("./", false, null, null, null, null, false, false, false, true, false, false, null);
+        Arg expectedArg = new Arg("./", null, false, null, null, null, null, false, false, false, true, false, false, null);
         assertEquals(expectedArg, parsedArg);
     }
 
     @Test
     public void testWithOverwrite() throws Exception {
         Arg parsedArg = CLIParser.parse(asArgArray("-" + CLIParser.ARG_APK_FILE + "./ --overwrite"));
-        Arg expectedArg = new Arg("./", false, null, null, null, null, true, false, false, false, false, false, null);
+        Arg expectedArg = new Arg("./", null, false, null, null, null, null, true, false, false, false, false, false, null);
         assertEquals(expectedArg, parsedArg);
     }
 
     @Test
     public void testWithDryRun() throws Exception {
         Arg parsedArg = CLIParser.parse(asArgArray("-" + CLIParser.ARG_APK_FILE + "./ --dryRun"));
-        Arg expectedArg = new Arg("./", false, null, null, null, null, false, true, false, false, false, false, null);
+        Arg expectedArg = new Arg("./", null, false, null, null, null, null, false, true, false, false, false, false, null);
         assertEquals(expectedArg, parsedArg);
     }
 
     @Test
     public void testWithVerbose() throws Exception {
         Arg parsedArg = CLIParser.parse(asArgArray("-" + CLIParser.ARG_APK_FILE + "./ --verbose"));
-        Arg expectedArg = new Arg("./", false, null, null, null, null, false, false, true, false, false, false, null);
+        Arg expectedArg = new Arg("./", null, false, null, null, null, null, false, false, true, false, false, false, null);
         assertEquals(expectedArg, parsedArg);
     }
 
