@@ -2,10 +2,15 @@ package at.favre.tools.apksigner.signing;
 
 import at.favre.tools.apksigner.ui.Arg;
 import at.favre.tools.apksigner.util.CmdUtil;
+import at.favre.tools.apksigner.util.FileUtil;
 
 import java.io.File;
+import java.io.IOException;
 import java.nio.file.Files;
 import java.nio.file.StandardCopyOption;
+import java.nio.file.attribute.PosixFilePermission;
+import java.util.HashSet;
+import java.util.Set;
 
 public class ZipAlignExecutor {
     private enum Location {CUSTOM, PATH, BUILT_IN}
@@ -14,7 +19,7 @@ public class ZipAlignExecutor {
 
     public String[] zipAlignExecutable;
     private Location location;
-    private File tempLocation;
+    private File tmpFolder;
 
     public ZipAlignExecutor(Arg arg) {
         findLocation(arg);
@@ -50,14 +55,22 @@ public class ZipAlignExecutor {
                         fileName = "linux-zipalign-24_0_3";
                     }
 
-                    File tempLocation = File.createTempFile("temp_", "_" + fileName);
-                    Files.copy(getClass().getClassLoader().getResourceAsStream(fileName), tempLocation.toPath(), StandardCopyOption.REPLACE_EXISTING);
+                    tmpFolder = Files.createTempDirectory("uapksigner-tmp").toFile();
+                    File tmpZipAlign = File.createTempFile(fileName, null , tmpFolder);
+                    Files.copy(getClass().getClassLoader().getResourceAsStream(fileName), tmpZipAlign.toPath(), StandardCopyOption.REPLACE_EXISTING);
 
                     if (osType != CmdUtil.OS.WIN) {
-                        CmdUtil.runCmd(new String[]{"chmod", "+x", tempLocation.getAbsolutePath()});
+                        Set<PosixFilePermission> perms = new HashSet<>();
+                        perms.add(PosixFilePermission.OWNER_EXECUTE);
+
+                        Files.setPosixFilePermissions(tmpZipAlign.toPath(), perms);
+                        File lib64File = new File(new File(tmpFolder,"lib64"),"libc++.so");
+                        lib64File.mkdirs();
+                        Files.setPosixFilePermissions(lib64File.toPath(), perms);
+                        Files.copy(getClass().getClassLoader().getResourceAsStream("linux64-libc++.so"), lib64File.toPath(), StandardCopyOption.REPLACE_EXISTING);
                     }
 
-                    zipAlignExecutable = new String[]{tempLocation.getAbsolutePath()};
+                    zipAlignExecutable = new String[]{tmpZipAlign.getAbsolutePath()};
                     location = Location.BUILT_IN;
                 }
             }
@@ -71,9 +84,9 @@ public class ZipAlignExecutor {
     }
 
     public void cleanUp() {
-        if (tempLocation != null) {
-            tempLocation.delete();
-            tempLocation = null;
+        if (tmpFolder != null) {
+            FileUtil.removeRecursive(tmpFolder.toPath());
+            tmpFolder = null;
         }
     }
 
