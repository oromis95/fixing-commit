@@ -13,7 +13,5 @@ public class CmdUtilTest {
     public void testCanRunCommand() throws Exception {
         assertFalse("should not be able to run random", CmdUtil.canRunCmd(new String[]{"Thisadhpiwadahdjsahduhduwaheuawez27371236"}));
         assertTrue("should be able to run cmd 'java -version'",CmdUtil.canRunCmd(new String[]{"java","-version"}));
-        assertTrue("should be able to run cmd 'cd'",CmdUtil.canRunCmd(new String[]{"cmd","/c","cd"}));
-
     }
 }
