@@ -6,6 +6,7 @@ import at.favre.tools.apksigner.ui.CLIParserTest;
 import at.favre.tools.apksigner.ui.MultiKeystoreParser;
 import at.favre.tools.apksigner.util.FileUtil;
 import org.junit.Before;
+import org.junit.Ignore;
 import org.junit.Rule;
 import org.junit.Test;
 import org.junit.rules.TemporaryFolder;
@@ -103,7 +104,7 @@ public class SignToolTest {
     public void testSignMultiApkWithZipalign() throws Exception {
         List<File> uApks = copyToTestPath(originalFolder, unsingedApks);
 
-        String cmd = "-" + CLIParser.ARG_APK_FILE + " " + originalFolder.getAbsolutePath() + " -" + CLIParser.ARG_APK_OUT + " " + outFolder.getAbsolutePath();
+        String cmd = "-" + CLIParser.ARG_APK_FILE + " " + originalFolder.getAbsolutePath() + " -" + CLIParser.ARG_APK_OUT + " " + outFolder.getAbsolutePath()+" --debug";
         testAndCheck(cmd, outFolder, uApks);
     }
 
