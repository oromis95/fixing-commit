@@ -1,6 +1,9 @@
 package com.codepoetics.protonpack;
 
+import com.codepoetics.protonpack.comparators.Comparators;
+
 import java.util.ArrayList;
+import java.util.Comparator;
 import java.util.List;
 import java.util.Spliterator;
 import java.util.function.BiPredicate;
@@ -8,10 +11,10 @@ import java.util.function.Consumer;
 
 
 class AggregatingSpliterator<I> implements Spliterator<List<I>> {
-    
+
     private final Spliterator<I> source;
     private final BiPredicate<List<I>, I> condition;
-    
+
     private List<I> currentSlide = new ArrayList<>();
 
     AggregatingSpliterator(Spliterator<I> source, BiPredicate<List<I>, I> predicate) {
@@ -39,7 +42,7 @@ class AggregatingSpliterator<I> implements Spliterator<List<I>> {
     private boolean isSameSlide(I curEl) {
         return currentSlide.isEmpty() || condition.test(currentSlide, curEl);
     }
-    
+
     @Override
     public Spliterator<List<I>> trySplit() {
         return null;
@@ -54,4 +57,10 @@ class AggregatingSpliterator<I> implements Spliterator<List<I>> {
     public int characteristics() {
         return source.characteristics() & ~Spliterator.SIZED & ~Spliterator.CONCURRENT;
     }
-}
\ No newline at end of file
+
+    @Override
+    public Comparator<? super List<I>> getComparator() {
+        Comparator<? super I> comparator = source.getComparator();
+        return comparator == null ? null : Comparators.toListComparator(comparator);
+    }
+}
