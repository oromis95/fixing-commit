@@ -30,4 +30,14 @@ public class SkipWhileTest {
 
         assertThat(collected, contains(5, 6, 7, 8, 9, 10));
     }
+
+    @Test public void
+    skip_until_retains_sorted_property() {
+        Stream<Integer> sortedInts = Stream.of(5, 4, 3, 2, 1).sorted();
+        Stream<Integer> skipped = StreamUtils.skipUntil(sortedInts, i -> i > 3);
+
+        List<Integer> collected = skipped.collect(Collectors.toList());
+
+        assertThat(collected, contains(4, 5));
+    }
 }
