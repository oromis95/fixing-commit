@@ -33,11 +33,11 @@ public class TestEzLevelDbJni extends TestEzLevelDb {
     final RangeTable<String, Date, Integer> rangeTable = ezdb.getTable("testInverseOrder", hashKeySerde,
         hashRangeSerde, valueSerde);
     final Date now = new Date();
-    final Date oneDate = new Date(now.getTime() + 10000);
+    final Date oneDate = new Date(now.getTime() + 100000);
     rangeTable.put("one", oneDate, 1);
-    final Date twoDate = new Date(now.getTime() + 20000);
+    final Date twoDate = new Date(now.getTime() + 200000);
     rangeTable.put("one", twoDate, 2);
-    final Date threeDate = new Date(now.getTime() + 30000);
+    final Date threeDate = new Date(now.getTime() + 300000);
     rangeTable.put("one", threeDate, 3);
     final TableIterator<String, Date, Integer> range3 = rangeTable.range("one", now);
     Assert.assertEquals(1, (int) range3.next().getValue());
