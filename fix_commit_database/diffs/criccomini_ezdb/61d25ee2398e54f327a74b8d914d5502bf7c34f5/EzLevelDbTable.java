@@ -217,6 +217,11 @@ public class EzLevelDbTable<H, R, V> implements RangeTable<H, R, V> {
 		TableIterator<H, R, V> emptyIterator = reverseSeekToLast(hashKey, null,
 				null, keyBytesFrom, null, iterator, checkKeys);
 		if (emptyIterator != null) {
+			try {
+				iterator.close();
+			} catch (IOException e) {
+				throw new RuntimeException(e);
+			}
 			return emptyIterator;
 		}
 
@@ -354,6 +359,11 @@ public class EzLevelDbTable<H, R, V> implements RangeTable<H, R, V> {
 					null, null, keyBytesFromForSeekLast, null, iterator,
 					checkKeys);
 			if (emptyIterator != null) {
+				try {
+					iterator.close();
+				} catch (IOException e) {
+					throw new RuntimeException(e);
+				}
 				return emptyIterator;
 			}
 		}
@@ -455,6 +465,11 @@ public class EzLevelDbTable<H, R, V> implements RangeTable<H, R, V> {
 					null, toRangeKey, keyBytesFromForSeekLast, keyBytesTo,
 					iterator, checkKeys);
 			if (emptyIterator != null) {
+				try {
+					iterator.close();
+				} catch (IOException e) {
+					throw new RuntimeException(e);
+				}
 				return emptyIterator;
 			}
 		}
