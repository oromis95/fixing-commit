@@ -208,6 +208,7 @@ public class EzLmDbTable<H, R, V> implements RangeTable<H, R, V> {
 		TableIterator<H, R, V> emptyIterator = reverseSeekToLast(hashKey, null, null, keyBytesFrom, null, iterator,
 				checkKeys);
 		if (emptyIterator != null) {
+			iterator.close();
 			return emptyIterator;
 		}
 
@@ -323,6 +324,7 @@ public class EzLmDbTable<H, R, V> implements RangeTable<H, R, V> {
 			TableIterator<H, R, V> emptyIterator = reverseSeekToLast(hashKey, null, null, keyBytesFromForSeekLast, null,
 					iterator, checkKeys);
 			if (emptyIterator != null) {
+				iterator.close();
 				return emptyIterator;
 			}
 		}
@@ -405,6 +407,7 @@ public class EzLmDbTable<H, R, V> implements RangeTable<H, R, V> {
 			TableIterator<H, R, V> emptyIterator = reverseSeekToLast(hashKey, null, toRangeKey, keyBytesFromForSeekLast,
 					keyBytesTo, iterator, checkKeys);
 			if (emptyIterator != null) {
+				iterator.close();
 				return emptyIterator;
 			}
 		}
