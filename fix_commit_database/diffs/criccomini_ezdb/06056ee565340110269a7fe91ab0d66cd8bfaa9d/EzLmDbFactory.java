@@ -20,7 +20,7 @@ import org.lmdbjava.EnvFlags;
 public interface EzLmDbFactory {
 	public Env<ByteBuffer> create(File path, EnvFlags... envFlags) throws IOException;
 
-	public Dbi<ByteBuffer> open(Env<ByteBuffer> env, EzLmDbComparator comparator, DbiFlags... dbiFlags) throws IOException;
+	public Dbi<ByteBuffer> open(String tableName, Env<ByteBuffer> env, EzLmDbComparator comparator, DbiFlags... dbiFlags) throws IOException;
 
 	public void destroy(File path) throws IOException;
 }
