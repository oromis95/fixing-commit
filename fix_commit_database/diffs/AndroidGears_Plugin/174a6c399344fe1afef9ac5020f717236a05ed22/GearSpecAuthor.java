@@ -0,0 +1,25 @@
+package Models.GearSpec;
+
+/**
+ * Created by matthewyork on 3/31/14.
+ */
+public class GearSpecAuthor {
+    private String name;
+    private String email;
+
+    public String getName() {
+        return name;
+    }
+
+    public void setName(String name) {
+        this.name = name;
+    }
+
+    public String getEmail() {
+        return email;
+    }
+
+    public void setEmail(String email) {
+        this.email = email;
+    }
+}
