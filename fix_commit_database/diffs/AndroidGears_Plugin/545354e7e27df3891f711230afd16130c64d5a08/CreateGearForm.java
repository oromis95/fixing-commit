@@ -1,6 +1,5 @@
 package Forms;
 
-import CustomPanel.ImagePanel;
 import Models.GearSpec.GearSpec;
 import Models.GearSpec.GearSpecAuthor;
 import Models.GearSpec.GearSpecDependency;
@@ -58,7 +57,6 @@ public class CreateGearForm {
     private GearSpec newSpec;
 
 
-
     public CreateGearForm() {
         this.gson = new GsonBuilder().setPrettyPrinting().create();
         initAuthorTable();
@@ -71,8 +69,9 @@ public class CreateGearForm {
         AuthorModel = (DefaultTableModel) authorsTable.getModel();
         AuthorModel.addColumn("Author's Name");
         AuthorModel.addColumn("Author's Email");
-        AddAllNewAuthors(authors,AuthorModel);
+        AddAllNewAuthors(authors, AuthorModel);
     }
+
     private void initDependenciesTable() {//TODO add lint for the same library being added twice
         ArrayList<GearSpecDependency> dependencies = new ArrayList<GearSpecDependency>();
         DependencyModel = (DefaultTableModel) dependencyTable.getModel();
@@ -81,31 +80,30 @@ public class CreateGearForm {
         AddAllNewDependencies(dependencies, DependencyModel);
     }
 
-    private void AddAllNewAuthors(ArrayList<GearSpecAuthor> authors,DefaultTableModel model){
+    private void AddAllNewAuthors(ArrayList<GearSpecAuthor> authors, DefaultTableModel model) {
         for (GearSpecAuthor author : authors) {
-            AddNewAuthor(author,model);
+            AddNewAuthor(author, model);
         }
     }
 
-    private void AddNewAuthor(GearSpecAuthor gearSpecAuthor,DefaultTableModel model) {
-        model.addRow(new Object[]{gearSpecAuthor.getName(),gearSpecAuthor.getEmail()});
+    private void AddNewAuthor(GearSpecAuthor gearSpecAuthor, DefaultTableModel model) {
+        model.addRow(new Object[]{gearSpecAuthor.getName(), gearSpecAuthor.getEmail()});
     }
 
 
-    private void AddAllNewDependencies(ArrayList<GearSpecDependency> dependencies,DefaultTableModel model){
+    private void AddAllNewDependencies(ArrayList<GearSpecDependency> dependencies, DefaultTableModel model) {
         for (GearSpecDependency dependency : dependencies) {
             AddNewDependency(dependency, model);
         }
     }
 
-    private void AddNewDependency(GearSpecDependency gearSpecDependency,DefaultTableModel model) {
-        model.addRow(new Object[]{gearSpecDependency.getName(),gearSpecDependency.getVersion()});
+    private void AddNewDependency(GearSpecDependency gearSpecDependency, DefaultTableModel model) {
+        model.addRow(new Object[]{gearSpecDependency.getName(), gearSpecDependency.getVersion()});
     }
 
 
-
     private void initButtons() {
-        btnCreateAndroidGearSpec.addActionListener( new ActionListener() {
+        btnCreateAndroidGearSpec.addActionListener(new ActionListener() {
             @Override
             public void actionPerformed(ActionEvent e) {
                 newSpec = CreateNewGearSpec();
@@ -113,12 +111,12 @@ public class CreateGearForm {
             }
         });
 
-        btnAddAuthor.addActionListener( new ActionListener() {
+        btnAddAuthor.addActionListener(new ActionListener() {
             @Override
             public void actionPerformed(ActionEvent e) {
-                if(!txtAuthorName.getText().isEmpty()&&!txtAuthorEmail.getText().isEmpty()){
-                    AuthorModel.addRow(new Object[]{txtAuthorName.getText(),txtAuthorEmail.getText()});
-                    authors.add(new GearSpecAuthor(txtAuthorName.getText(),txtAuthorEmail.getText()));
+                if (!txtAuthorName.getText().isEmpty() && !txtAuthorEmail.getText().isEmpty()) {
+                    AuthorModel.addRow(new Object[]{txtAuthorName.getText(), txtAuthorEmail.getText()});
+                    authors.add(new GearSpecAuthor(txtAuthorName.getText(), txtAuthorEmail.getText()));
                     txtAuthorName.setText("");
                     txtAuthorEmail.setText("");
 
@@ -126,18 +124,18 @@ public class CreateGearForm {
             }
         });
 
-        btnAddDependency.addActionListener( new ActionListener() {
+        btnAddDependency.addActionListener(new ActionListener() {
             @Override
             public void actionPerformed(ActionEvent e) {
-                if(!txtDependencyName.getText().isEmpty()&&!txtDependencyVersion.getText().isEmpty()){
-                    DependencyModel.addRow(new Object[]{txtDependencyName.getText(),txtDependencyVersion.getText()});
-                    dependencies.add(new GearSpecDependency(txtDependencyName.getText(),txtDependencyVersion.getText()));
+                if (!txtDependencyName.getText().isEmpty() && !txtDependencyVersion.getText().isEmpty()) {
+                    DependencyModel.addRow(new Object[]{txtDependencyName.getText(), txtDependencyVersion.getText()});
+                    dependencies.add(new GearSpecDependency(txtDependencyName.getText(), txtDependencyVersion.getText()));
                     txtDependencyName.setText("");
                     txtDependencyVersion.setText("");
                 }
             }
         });
-        btnRemoveAuthor.addActionListener( new ActionListener() {
+        btnRemoveAuthor.addActionListener(new ActionListener() {
             @Override
             public void actionPerformed(ActionEvent e) {
                 AuthorModel.removeRow(authorsTable.getSelectedRow());
@@ -145,7 +143,7 @@ public class CreateGearForm {
             }
         });
 
-        btnRemoveDependency.addActionListener( new ActionListener() {
+        btnRemoveDependency.addActionListener(new ActionListener() {
             @Override
             public void actionPerformed(ActionEvent e) {
                 DependencyModel.removeRow(dependencyTable.getSelectedRow());
@@ -156,42 +154,41 @@ public class CreateGearForm {
 
     /**
      * Creates the Android Gear Spec Object
-      */
+     */
     private GearSpec CreateNewGearSpec() {
         GearSpec newSpec = new GearSpec();
-        if(!txtProjectName.getText().isEmpty())
+        if (!txtProjectName.getText().isEmpty())
             newSpec.setName(txtProjectName.getText());
 
 
-
-        if(!txtProjectVersion.getText().isEmpty())
+        if (!txtProjectVersion.getText().isEmpty())
             newSpec.setVersion(txtProjectVersion.getText());
 
-        if(!txtProjectTags.getText().isEmpty())
+        if (!txtProjectTags.getText().isEmpty())
             newSpec.setTags(ParseStringWithCommas(txtProjectTags.getText()));
 
-        if(!txtLibraryTag.getText().isEmpty() && !txtSourceLibLocation.getText().isEmpty() && !txtSourceURL.getText().isEmpty())//TODO check for all 3 urls and file paths source must end with .git
-            newSpec.setSource(new GearSpecSource(txtSourceURL.getText(),txtSourceLibLocation.getText(),txtLibraryTag.getText()));
+        if (!txtLibraryTag.getText().isEmpty() && !txtSourceLibLocation.getText().isEmpty() && !txtSourceURL.getText().isEmpty())//TODO check for all 3 urls and file paths source must end with .git
+            newSpec.setSource(new GearSpecSource(txtSourceLibLocation.getText(), txtSourceURL.getText(), txtLibraryTag.getText()));
 
-        if(!txtProjectSummary.getText().isEmpty())
+        if (!txtProjectSummary.getText().isEmpty())
             newSpec.setSummary(txtProjectSummary.getText());
 
-        if(!txtReleaseNotes.getText().isEmpty())
+        if (!txtReleaseNotes.getText().isEmpty())
             newSpec.setRelease_notes(txtReleaseNotes.getText());
 
-        if(!txtCopyRight.getText().isEmpty())
+        if (!txtCopyRight.getText().isEmpty())
             newSpec.setCopyright(txtCopyRight.getText());
 
-        if(!txtHomePage.getText().isEmpty())
+        if (!txtHomePage.getText().isEmpty())
             newSpec.setHomepage(txtHomePage.getText());
 
-        if(!txtLicense.getText().isEmpty())
+        if (!txtLicense.getText().isEmpty())
             newSpec.setLicense(txtLicense.getText());
 
-        if(!authors.isEmpty())
+        if (!authors.isEmpty())
             newSpec.setAuthors(authors);
 
-        if(!dependencies.isEmpty())
+        if (!dependencies.isEmpty())
             newSpec.setDependencies(dependencies);
 
         newSpec.setType(cbLibraryType.getSelectedItem().toString());
@@ -201,10 +198,9 @@ public class CreateGearForm {
     }
 
 
-
     private ArrayList<GearSpecAuthor> CreateAuthorsArray() {
         ArrayList<GearSpecAuthor> authors = new ArrayList<GearSpecAuthor>();
-        authors.add(new GearSpecAuthor(txtAuthorName.getText(),txtAuthorEmail.getText()));
+        authors.add(new GearSpecAuthor(txtAuthorName.getText(), txtAuthorEmail.getText()));
         return authors;
     }
 
@@ -212,74 +208,26 @@ public class CreateGearForm {
     private ArrayList<String> ParseStringWithCommas(String text) {
         String[] Strings = text.split(",");
         ArrayList<String> temp = new ArrayList<String>();
-        for(int i = 0 ; i < Strings.length; i++){
-            //Check for Spaces in front of the String.
-            Strings[i] = RemovePrecedingSpaces(Strings[i]);
-            //Check for Spaces at end of String.
-            Strings[i] = RemoveSpacesAtEnd(Strings[i]);
+        for (int i = 0; i < Strings.length; i++) {
+            //trims the whitespace from the beginning and the end of the string. leaving the whitespace in the middle alone
+            Strings[i] = trimExtraWhiteSpace(Strings[i]);
             //Is empty check
-            if(!Strings[i].isEmpty()){
+            if (!Strings[i].isEmpty()) {
                 temp.add(Strings[i]);
             }
         }
         return temp;
     }
-    //TODO Check if valid
 
     /**
-     * Checks for spaces at the end of the string and recursively calls itself until the deed is finished
-     * @param string String spaces are checked on
-     * @return
-     */
-    private String RemovePrecedingSpaces(String string) {
-        if(string.startsWith(" ")){
-            string = string.substring(1);
-            RemovePrecedingSpaces(string);
-        }
-        return string;
-    }
-    //TODO Check if valid
-    /**
-     * Checks for spaces in front of the string and recursively calls itself until the deed is finished
+     * Checks for spaces at the end of the string and at the beginning of the string and removes that whitespace.
+     *
      * @param string String spaces are checked on
-     * @return
+     * @return string with the whitespace trimmed off.
      */
-    private String RemoveSpacesAtEnd(String string) {
-        if(string.endsWith(" ")){
-            string = string.substring(0,string.length()-1);
-            RemoveSpacesAtEnd(string);
-        }
-        return string;
-    }
-
-// TODO THIS DOESNT WORK DO NOT REUSE JUST YET
-    private void checkImageURL(String urlString) {
-//        JFrame imageFrame = new JFrame();
-//        Image image = null;
-//        JLabel label;
-
-//        if(urlString.contains("www.")){
-//            imgProjectIcon.setImage("http://www.mkyong.com/image/mypic.jpg");
-//            try {
-//                URL url = new URL(urlString);
-//                image = ImageIO.read(url);
-//            } catch (IOException e) {
-//                e.printStackTrace();
-//            }
-//            label = new JLabel(new ImageIcon(image));
-//            imageFrame.add(label);
-//            imageFrame.setVisible(true);
-//        }else{
-//            try {
-//                File file = new File("/resources/icons/gears@2x.png");
-//                image = ImageIO.read(file);
-//            } catch (IOException e) {
-//                e.printStackTrace();
-//            }
-//            label = new JLabel(new ImageIcon(image));
-//            imageFrame.add(label);
-//            imageFrame.setVisible(true);
-//        }
+    private String trimExtraWhiteSpace(String string) {
+        //remove all whitespace after string and before string
+        return string.replaceAll("\\s+$", "").replaceAll("^\\s+", "");
     }
 
     private void createUIComponents() {
