@@ -3,17 +3,21 @@ package Forms;
 import javax.swing.*;
 import javax.swing.event.ListSelectionEvent;
 import javax.swing.event.ListSelectionListener;
+import java.awt.*;
 import java.awt.event.*;
 import java.io.*;
 import Panels.SpecDetailsPanel;
 import Renderers.GearSpecCellRenderer;
+import Utilities.OSValidator;
 import Utilities.Utils;
 import java.net.HttpURLConnection;
 import java.net.URL;
 import java.util.ArrayList;
 import Models.GearSpec.GearSpec;
+import Workers.GetProjectVersionsWorker;
 import Workers.GitWorker;
 import Workers.SearchProjectListWorker;
+import com.google.gson.Gson;
 import org.fit.cssbox.swingbox.BrowserPane;
 
 /**
@@ -23,8 +27,10 @@ public class ManageAndroidGearsForm{
     public static final int DETAILS_INNER_WIDTH = 240;
 
     File androidGearsDirectory;
+    private GearSpec selectedSpec;
     private ArrayList<GearSpec> searchProjects;
     private ArrayList<GearSpec> installedProjects;
+    private ArrayList<String> projectVersions;
     private BrowserPane swingbox;
 
     private JTextField SearchTextField;
@@ -40,6 +46,9 @@ public class ManageAndroidGearsForm{
     private JScrollPane ReadmeScrollPane;
     private JButton SyncButton;
     private JLabel StatusLabel;
+    private JList VersionsList;
+    private JLabel ChangeVersionsLabel;
+    private JButton InstallUninstallButton;
     private JTable SearchTable;
 
     private void createUIComponents() {
@@ -50,6 +59,7 @@ public class ManageAndroidGearsForm{
         setupSearchTable();
         setupSearchTextField();
         setupButtons();
+        setupMiscUI();
     }
 
     private void setupSearchTable() {
@@ -79,6 +89,13 @@ public class ManageAndroidGearsForm{
                 didSelectInstalledSpecAtIndex(InstalledList.getSelectedIndex());
             }
         });
+
+        VersionsList.addListSelectionListener(new ListSelectionListener() {
+            @Override
+            public void valueChanged(ListSelectionEvent listSelectionEvent) {
+                didSelectSpecVersion(VersionsList.getSelectedIndex());
+            }
+        });
     }
 
     private void setupSearchTextField() {
@@ -148,6 +165,12 @@ public class ManageAndroidGearsForm{
                 worker.run();
             }
         });
+
+        InstallUninstallButton.setVisible(false);
+    }
+
+    private void setupMiscUI() {
+        ChangeVersionsLabel.setFont(new Font(ChangeVersionsLabel.getFont().getName(), Font.PLAIN, 12));
     }
 
     private void reloadList(){
@@ -159,15 +182,23 @@ public class ManageAndroidGearsForm{
 
 
     ///////////////////////
-    // Spec Selection
+    // JList Selection
     ////////////////////////
 
     private void didSelectSearchSpecAtIndex(int index){
-        setDetailsForSpec(searchProjects.get(index));
+        selectedSpec = searchProjects.get(index);
+        setDetailsForSpec(selectedSpec, searchProjects.get(index).getVersion());
+        getVersionDetailsForSepc();
     }
 
     private void didSelectInstalledSpecAtIndex(int index){
-        setDetailsForSpec(installedProjects.get(index));
+        selectedSpec = installedProjects.get(index);
+        setDetailsForSpec(selectedSpec, installedProjects.get(index).getVersion()); //MAY NEED TO CHANGE
+        getVersionDetailsForSepc();
+    }
+
+    private void didSelectSpecVersion(int index) {
+        setDetailsForSpec(selectedSpec, projectVersions.get(index));
     }
 
 
@@ -175,14 +206,19 @@ public class ManageAndroidGearsForm{
     // Details Management
     ///////////////////////
 
-    private void setDetailsForSpec(GearSpec spec){
-        SpecDetailsPanel specDetailsPanel = new SpecDetailsPanel(spec);
+    private void setDetailsForSpec(GearSpec spec, String version){
+        //If it is the same as you have selected, don't do anything, else, get the specified version
+        if (!(spec.getName().equals(selectedSpec.getName()) && spec.getVersion().equals(version))){
+            selectedSpec = specForVersion(spec.getName(), version);
+        }
+
+        SpecDetailsPanel specDetailsPanel = new SpecDetailsPanel(selectedSpec);
 
-        if(spec.getHomepage() != null){
+        if(selectedSpec.getHomepage() != null){
             //Fetch page/readme
-            String fetchUrl = spec.getHomepage();
+            String fetchUrl = selectedSpec.getHomepage();
             Boolean isGithub = false;
-            if (spec.getHomepage().contains("github.com")) {
+            if (selectedSpec.getHomepage().contains("github.com")) {
                 isGithub = true;
                 fetchUrl = fetchUrl + "/blob/master/README.md";
             }
@@ -211,8 +247,47 @@ public class ManageAndroidGearsForm{
         DetailsScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
         DetailsScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
         //DetailsScrollPane.setPreferredSize(new Dimension(panel.getSize().width, panel.getSize().height));
+
+
+        //Set install/uninstall button
+        //CHECK HERE FOR INSTALLATION STATUS
+        InstallUninstallButton.setVisible(true);
+    }
+
+    private void getVersionDetailsForSepc(){
+        //Set versions
+        GetProjectVersionsWorker worker = new GetProjectVersionsWorker(selectedSpec){
+            @Override
+            protected void done() {
+                super.done();
+
+                projectVersions = this.versions;
+
+                VersionsList.setListData(projectVersions.toArray());
+                VersionsList.setCellRenderer(new DefaultListCellRenderer());
+                VersionsList.setVisibleRowCount(projectVersions.size());
+            }
+        };
+        worker.run();
+    }
+
+    private GearSpec specForVersion(String specName, String version){
+        //Get path separator
+        String pathSeparator = (OSValidator.isWindows()) ? "\\":"/";
+
+        File specFile = new File(Utils.androidGearsDirectory().getAbsolutePath()+pathSeparator+specName+pathSeparator+version+pathSeparator+specName+".gearspec");
+
+        if(specFile.exists()){
+            String specString = Utils.stringFromFile(specFile);
+
+            //Get spec
+            GearSpec spec = new Gson().fromJson(specString, GearSpec.class);
+
+            return spec;
         }
 
+        return new GearSpec();
+    }
 
 
     public static boolean ping(String url, int timeout) {
