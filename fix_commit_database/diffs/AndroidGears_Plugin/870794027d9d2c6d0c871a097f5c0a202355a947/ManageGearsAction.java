@@ -14,6 +14,7 @@ public class ManageGearsAction extends AnAction {
     public void actionPerformed(AnActionEvent e) {
         JFrame frame = new JFrame("Manage Android Gears");
         frame.setContentPane(new ManageAndroidGearsForm().MasterPanel);
+        frame.setResizable(false);
         frame.pack();
         frame.setLocationRelativeTo(null);
         frame.setVisible(true);
