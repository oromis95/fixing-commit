@@ -9,6 +9,7 @@ import com.jetbrains.php.lang.psi.resolve.types.PhpType;
 
 import java.util.ArrayList;
 import java.util.Collection;
+import java.util.HashSet;
 
 public class PhpIndexUtil {
 
@@ -17,10 +18,17 @@ public class PhpIndexUtil {
 	}
 
 	public static Collection<PhpClass> getClasses(PhpType type, PhpIndex phpIndex, PhpClass containingClass) {
+		return getClasses(type, phpIndex, containingClass, new HashSet<String>());
+	}
+
+	private static Collection<PhpClass> getClasses(PhpType type, PhpIndex phpIndex, PhpClass containingClass, Collection<String> recursionPrevention) {
 		Collection<PhpClass> classes = new ArrayList<PhpClass>();
 		for (String className : type.getTypes()) {
 			if (className.startsWith("#")) {
-				classes.addAll(getBySignature(className, phpIndex));
+				if (!recursionPrevention.contains(className)) {
+					recursionPrevention.add(className);
+					classes.addAll(getBySignature(className, phpIndex, recursionPrevention));
+				}
 			} else if (className.equals("$this")) {
 				if (containingClass != null) {
 					classes.add(containingClass);
@@ -33,11 +41,11 @@ public class PhpIndexUtil {
 		return classes;
 	}
 
-	private static Collection<PhpClass> getBySignature(String sig, PhpIndex phpIndex) {
+	private static Collection<PhpClass> getBySignature(String sig, PhpIndex phpIndex, Collection<String> recursionPrevention) {
 
 		Collection<PhpClass> classes = new ArrayList<PhpClass>();
 		for (PhpNamedElement el : phpIndex.getBySignature(sig)) {
-			classes.addAll(getClasses(el.getType(), phpIndex, el instanceof PhpClassMember ? ((PhpClassMember) el).getContainingClass() : null));
+			classes.addAll(getClasses(el.getType(), phpIndex, el instanceof PhpClassMember ? ((PhpClassMember) el).getContainingClass() : null, recursionPrevention));
 		}
 
 		return classes;
