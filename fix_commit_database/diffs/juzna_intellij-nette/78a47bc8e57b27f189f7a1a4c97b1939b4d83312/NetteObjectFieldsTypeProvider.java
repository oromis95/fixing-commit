@@ -3,7 +3,6 @@ package cz.juzna.intellij.nette.typeProvider;
 import com.intellij.openapi.project.DumbService;
 import com.intellij.openapi.project.Project;
 import com.intellij.psi.PsiElement;
-import com.intellij.util.containers.HashMap;
 import com.jetbrains.php.PhpIndex;
 import com.jetbrains.php.lang.psi.elements.FieldReference;
 import com.jetbrains.php.lang.psi.elements.Method;
@@ -12,10 +11,17 @@ import com.jetbrains.php.lang.psi.resolve.types.PhpTypeProvider2;
 import cz.juzna.intellij.nette.utils.MagicFieldsUtil;
 import org.jetbrains.annotations.Nullable;
 
+import java.lang.reflect.Field;
+import java.util.ArrayList;
 import java.util.Collection;
+import java.util.HashSet;
+import java.util.regex.Pattern;
 
 
 public class NetteObjectFieldsTypeProvider implements PhpTypeProvider2 {
+	final static String SEPARATOR = "\u0180";
+
+	private Collection<FieldReference> visited = new HashSet<FieldReference>();
 
 	@Override
 	public char getKey() {
@@ -35,23 +41,36 @@ public class NetteObjectFieldsTypeProvider implements PhpTypeProvider2 {
 		if (field.getClassReference() == null) {
 			return null;
 		}
-		HashMap<String, Collection<Method>> fields = MagicFieldsUtil.findMagicFields(field);
-		if (!fields.containsKey(field.getName())) {
+		if (visited.contains(field)) {
 			return null;
 		}
-		for (Method method : fields.get(field.getName())) {
-			if (method.getName().startsWith("get") || method.getName().startsWith("is")) {
-				return "#M#C" + method.getContainingClass().getFQN() + "." + method.getName();
-			}
+		visited.add(field);
+		Collection<Method> getters = MagicFieldsUtil.findGetters(field);
+		visited.remove(field);
+		if (getters.isEmpty()) {
+			return null;
+		}
+		StringBuilder signature = new StringBuilder();
+		for (Method method : getters) {
+			signature.append("#M#C").append(method.getContainingClass().getFQN()).append(".").append(method.getName()).append(SEPARATOR);
 		}
 
-		return null;
+		return signature.toString();
 	}
 
 	@Override
 	public Collection<? extends PhpNamedElement> getBySignature(String s, Project project) {
 		PhpIndex index = PhpIndex.getInstance(project);
-		return index.getBySignature(s);
+		Collection<PhpNamedElement> elements = new ArrayList<PhpNamedElement>();
+		while (s.contains(SEPARATOR)) {
+			String sig = s.substring(0, s.indexOf(SEPARATOR));
+			s = s.substring(s.indexOf(SEPARATOR) + 1);
+			if (!sig.isEmpty()) {
+				elements.addAll(index.getBySignature(sig));
+			}
+		}
+
+		return elements;
 	}
 
 
