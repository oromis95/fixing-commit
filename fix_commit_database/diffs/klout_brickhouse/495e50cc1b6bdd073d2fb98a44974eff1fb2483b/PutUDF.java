@@ -36,8 +36,8 @@ import org.apache.log4j.Logger;
  *
  */
 @Description(name="hbase_put",
-    value = "string _FUNC_(config, key, value) - Do a single HBase Put on a table.  Config must zookeeper quorum, " +
-        "table name, and column qualifier. Example of usage: \n" +
+    value = "string _FUNC_(config, key, value) - Do a single HBase Put on a table.  Config must contain zookeeper \n" +
+        "quorum, table name, column, and qualifier. Example of usage: \n" +
         "  hbase_put(map('hbase.zookeeper.quorum', 'hb-zoo1,hb-zoo2', \n" +
         "                'table_name', 'metrics', " +
         "                'family', 'c', " +
@@ -53,14 +53,14 @@ public class PutUDF extends UDF {
   static private String TABLE_NAME_TAG = "table_name";
   static private String ZOOKEEPER_QUORUM_TAG = "hbase.zookeeper.quorum";
 
-  private static Map<String, HTable> htableMap_ = new HashMap<String,HTable>();
-  private static Configuration config_ = new Configuration(true);
+  private static Map<String, HTable> htableMap = new HashMap<String,HTable>();
+  private static Configuration config = new Configuration(true);
 
-  public String evaluate(Map<String, String> config, String key, String value) {
-    if (!config.containsKey(FAMILY_TAG) ||
-        !config.containsKey(QUALIFIER_TAG) ||
-        !config.containsKey(TABLE_NAME_TAG) ||
-        !config.containsKey(ZOOKEEPER_QUORUM_TAG)) {
+  public String evaluate(Map<String, String> configIn, String key, String value) {
+    if (!configIn.containsKey(FAMILY_TAG) ||
+        !configIn.containsKey(QUALIFIER_TAG) ||
+        !configIn.containsKey(TABLE_NAME_TAG) ||
+        !configIn.containsKey(ZOOKEEPER_QUORUM_TAG)) {
       String errorMsg = "Error while doing HBase Puts. Config is missing for: " + FAMILY_TAG + " or " +
       QUALIFIER_TAG + " or " + TABLE_NAME_TAG + " or " + ZOOKEEPER_QUORUM_TAG;
       LOG.error(errorMsg);
@@ -68,9 +68,9 @@ public class PutUDF extends UDF {
     }
 
     try {
-      HTable table = getHTable(config.get(TABLE_NAME_TAG), config.get(ZOOKEEPER_QUORUM_TAG));
+      HTable table = getHTable(configIn.get(TABLE_NAME_TAG), configIn.get(ZOOKEEPER_QUORUM_TAG));
       Put thePut = new Put(key.getBytes());
-      thePut.add(config.get(FAMILY_TAG).getBytes(), config.get(QUALIFIER_TAG).getBytes(), value.getBytes());
+      thePut.add(configIn.get(FAMILY_TAG).getBytes(), configIn.get(QUALIFIER_TAG).getBytes(), value.getBytes());
 
       table.put(thePut);
       return "Put " + key + ":" + value;
@@ -81,23 +81,23 @@ public class PutUDF extends UDF {
   }
 
   private HTable getHTable(String tableName, String zkQuorum) throws IOException {
-    HTable table = htableMap_.get(tableName);
+    HTable table = htableMap.get(tableName);
     if(table == null) {
-      config_ = new Configuration(true);
-      Iterator<Entry<String,String>> iter = config_.iterator();
+      config = new Configuration(true);
+      Iterator<Entry<String,String>> iter = config.iterator();
       while(iter.hasNext()) {
         Entry<String,String> entry =  iter.next();
         LOG.info("BEFORE CONFIG = " + entry.getKey() + " == " + entry.getValue());
       }
-      config_.set("hbase.zookeeper.quorum", zkQuorum);
-      Configuration hbConfig = HBaseConfiguration.create(config_);
+      config.set("hbase.zookeeper.quorum", zkQuorum);
+      Configuration hbConfig = HBaseConfiguration.create(config);
       iter = hbConfig.iterator();
       while(iter.hasNext()) {
         Entry<String,String> entry =  iter.next();
         LOG.info("AFTER CONFIG = " + entry.getKey() + " == " + entry.getValue());
       }
       table = new HTable(hbConfig, tableName);
-      htableMap_.put(tableName, table);
+      htableMap.put(tableName, table);
     }
 
     return table;
