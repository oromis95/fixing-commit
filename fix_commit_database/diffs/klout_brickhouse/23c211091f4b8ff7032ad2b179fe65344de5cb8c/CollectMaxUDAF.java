@@ -24,52 +24,54 @@ import org.apache.hadoop.hive.serde2.objectinspector.primitive.DoubleObjectInspe
 import org.apache.hadoop.hive.serde2.objectinspector.primitive.IntObjectInspector;
 import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
 import org.apache.hadoop.hive.serde2.objectinspector.primitive.StringObjectInspector;
+import org.apache.hadoop.hive.serde2.objectinspector.primitive.WritableConstantIntObjectInspector;
 import org.apache.hadoop.hive.serde2.typeinfo.TypeInfo;
 import org.apache.log4j.Logger;
 
 import java.util.*;
 
-/**
- * Similar to Ruby collect, 
- *   return an array with all the values
- */
-
 import org.apache.hadoop.hive.ql.exec.Description;
 
 @Description(name="collect_max",
-    value = "_FUNC_(x, val, n) - Returns an map of the max N elements in the aggregation group "
+    value = "_FUNC_(x, val, n) - Returns an map of the max N numeric values in the aggregation group "
 )
 public class CollectMaxUDAF extends AbstractGenericUDAFResolver {
-  private static final Logger LOG = Logger.getLogger(CollectMaxUDAF.class);
+  public static final Logger LOG = Logger.getLogger(CollectMaxUDAF.class);
   public static int DEFAULT_MAX_VALUES = 20;
 
-
   @Override
   public GenericUDAFEvaluator getEvaluator(TypeInfo[] parameters)
       throws SemanticException {
     return new MapCollectMaxUDAFEvaluator();
   }
 
-
   public static class MapCollectMaxUDAFEvaluator extends GenericUDAFEvaluator {
     // For PARTIAL1 and COMPLETE: ObjectInspectors for original data
     private PrimitiveObjectInspector  inputKeyOI;
-    private PrimitiveObjectInspector inputValOI; /// XXX Support nested values instead of just primitives as values
-    private IntObjectInspector nvOI;
+    private PrimitiveObjectInspector inputValOI; 
     // For PARTIAL2 and FINAL: ObjectInspectors for partial aggregations (list
     // of objs)
-    private StandardMapObjectInspector moi;
     private StandardMapObjectInspector internalMergeOI;
+    private boolean descending = true;
+    private int numValues = DEFAULT_MAX_VALUES;
+
 
+    public MapCollectMaxUDAFEvaluator() {
+    	this(true);
+    }
+
+    public MapCollectMaxUDAFEvaluator(boolean desc) {
+       this.descending = desc;	
+    }
 
-    public static class SortedKeyValue implements Comparable {
-      private String key;
-      private Double value;
+    public class SortedKeyValue implements Comparable {
+      private Object keyObj;
+      private Object valObj;
 
 
-      public SortedKeyValue(String key, Double val) {
-        this.key = key;
-        this.value = val;
+      public SortedKeyValue(Object keyObj, Object valObj ) {
+    	this.keyObj = keyObj;
+        this.valObj = valObj;
       }
 
       @Override
@@ -78,65 +80,62 @@ public class CollectMaxUDAF extends AbstractGenericUDAFResolver {
           return false;
         }
         SortedKeyValue otherKV= (SortedKeyValue)other;
-        if( key.equals(otherKV.key)) {
+        if( getKey().equals(otherKV.getKey())) {
           return true;
         } else {
           return false;
         }
-
       }
 
-      public String getKey() { return key; }
-      public Double getValue() { return value; }
+      public Object getKey() { return (keyObj == null) ? null : inputKeyOI.getPrimitiveJavaObject(keyObj); }
+      public Double getValue() { return (valObj == null ? null : ((Number)inputValOI.getPrimitiveJavaObject(valObj)).doubleValue()); }
+      
+      public String toString() { return getKey() + ":=" + getValue(); }
 
       @Override
       public int compareTo(Object arg1) {
-        SortedKeyValue kv0 = (SortedKeyValue) this;
-        SortedKeyValue kv1 = (SortedKeyValue) arg1;
-
-        if( kv0.value != kv1.value ) {
-          if(kv0.value > kv1.value)  {
-            return -1;
-          } else  {
-            if( kv0.value < kv1.value ) {
-              return 1;
-            }
-          }
-          return kv0.key.compareTo(kv1.key);
+        SortedKeyValue otherKV = (SortedKeyValue) arg1;
+        int cmp = compareKV( otherKV);
+        return ( descending ? cmp : -1*cmp);
+      }
+      
+      public int compareKV(SortedKeyValue otherKV) {
+        
+        double thisNumber = getValue(); 
+        double otherNumber = otherKV.getValue();
+        int sigNum  = (int) Math.signum(otherNumber - thisNumber);
+        if(sigNum !=0 )  {
+            return sigNum;
         } else {
-          return kv0.key.compareTo(kv1.key);
+          return ((Comparable)getKey()).compareTo(otherKV.getKey() );
         }
       }
     }
 
-    static class MapAggBuffer implements AggregationBuffer {
+    class MapAggBuffer implements AggregationBuffer {
       private TreeSet<SortedKeyValue> sortedValues = new TreeSet<SortedKeyValue>();
-      private int numValues = DEFAULT_MAX_VALUES;
-      public void setNumValues(int nv) { numValues = nv; }
 
-      public void addValue(String key, Double value) {
+
+      public void addValue(Object keyObj, Object valObj) {
         if( sortedValues.size() < numValues) {
-          sortedValues.add( new SortedKeyValue( key, value ));
+          SortedKeyValue newValue =  new SortedKeyValue(inputKeyOI.copyObject(keyObj), inputValOI.copyObject(valObj) );
+      	  sortedValues.add( newValue);
         } else {
           SortedKeyValue minValue = sortedValues.last();
-          if( value > minValue.getValue() ) {
+          SortedKeyValue biggerValue =  new SortedKeyValue(inputKeyOI.copyObject(keyObj), inputValOI.copyObject(valObj));
+          int cmp =  biggerValue.compareTo(minValue);
+          if( cmp < 0 ) {
             sortedValues.remove( minValue);
-            sortedValues.add( new SortedKeyValue( key, value));
+            sortedValues.add(biggerValue);
           }
         }
       }
 
-      public void fromMap( Map<Object,Object> fromMap) {
-        for( Object kObj : fromMap.keySet() ) {
-          Object val = fromMap.get(kObj);
-          addValue( (String)kObj, (Double)val);
-        }
-      }
 
-      public Map<String, Double> getValueMap() {
-        LinkedHashMap<String, Double> reverseOrderMap = new LinkedHashMap<String,Double>();
+      public Map getValueMap() {
+        LinkedHashMap<Object, Object> reverseOrderMap = new LinkedHashMap<Object,Object>();
         for( SortedKeyValue kv : sortedValues ) {
-          reverseOrderMap.put( kv.key, kv.value);
+          reverseOrderMap.put( kv.keyObj, kv.valObj);
         }
         return reverseOrderMap;
       }
@@ -149,12 +148,18 @@ public class CollectMaxUDAF extends AbstractGenericUDAFResolver {
     public ObjectInspector init(Mode m, ObjectInspector[] parameters)
         throws HiveException {
       super.init(m, parameters);
-      LOG.info(" CollectMaxUDAF.init() - Mode= " + m.name() );
+      LOG.error(" CollectMaxUDAF.init() - Mode= " + m.name() );
       for(int i=0; i<parameters.length; ++i) {
-        LOG.info(" ObjectInspector[ "+ i + " ] = " + parameters[0]);
+        LOG.error(" ObjectInspector[ "+ i + " ] = " + parameters[0]);
       }
       if(parameters.length > 2) {
-        nvOI = (IntObjectInspector) parameters[2];
+    	  if( parameters[2] instanceof WritableConstantIntObjectInspector ) {
+    		  WritableConstantIntObjectInspector nvOI = (WritableConstantIntObjectInspector) parameters[2];
+    		  numValues = nvOI.getWritableConstantValue().get();
+    		  LOG.info(" Setting number of values to " + numValues); 
+    	  } else {
+    		  throw new HiveException("Number of values must be a constant int.");
+    	  }
       }
 
       // init output object inspectors
@@ -162,36 +167,22 @@ public class CollectMaxUDAF extends AbstractGenericUDAFResolver {
       if (m == Mode.PARTIAL1) {
         inputKeyOI = (PrimitiveObjectInspector) parameters[0];
         inputValOI = (PrimitiveObjectInspector) parameters[1];
-
-        /**
-         return ObjectInspectorFactory.getStandardMapObjectInspector(
-         ObjectInspectorUtils.getStandardObjectInspector(inputKeyOI),
-         ObjectInspectorUtils.getStandardObjectInspector(inputValOI) );
-         **/
+        return ObjectInspectorFactory.getStandardMapObjectInspector(
+        		inputKeyOI,
+        		inputValOI );
       } else {
         if (!(parameters[0] instanceof StandardMapObjectInspector)) {
-          LOG.info(" Not a standard map OjbectInspector " );
-          inputKeyOI = (PrimitiveObjectInspector)  ObjectInspectorUtils
-              .getStandardObjectInspector(parameters[0]);
-          inputValOI = (PrimitiveObjectInspector)  ObjectInspectorUtils
-              .getStandardObjectInspector(parameters[1]);
-          /**
-           return (StandardMapObjectInspector) ObjectInspectorFactory
-           .getStandardMapObjectInspector(inputKeyOI, inputValOI);
-           **/
+          inputKeyOI = (PrimitiveObjectInspector) parameters[0];
+          inputValOI = (PrimitiveObjectInspector) parameters[1];
         } else {
           internalMergeOI = (StandardMapObjectInspector) parameters[0];
           inputKeyOI = (PrimitiveObjectInspector) internalMergeOI.getMapKeyObjectInspector();
           inputValOI = (PrimitiveObjectInspector) internalMergeOI.getMapValueObjectInspector();
-          /**
-           moi =  (StandardMapObjectInspector) ObjectInspectorUtils.getStandardObjectInspector(internalMergeOI);
-           return moi;
-           **/
         }
       }
       return ObjectInspectorFactory.getStandardMapObjectInspector(
-          PrimitiveObjectInspectorFactory.javaStringObjectInspector,
-          PrimitiveObjectInspectorFactory.javaDoubleObjectInspector);
+          inputKeyOI,
+          inputValOI );
     }
 
     @Override
@@ -207,17 +198,12 @@ public class CollectMaxUDAF extends AbstractGenericUDAFResolver {
       Object k = parameters[0];
       Object v = parameters[1];
       if (k == null || v == null) {
-        throw new HiveException("Kay or value is null.  k = " + k + " , v = " + v);
+        throw new HiveException("Key or value is null.  k = " + k + " , v = " + v);
       }
 
       if (k != null) {
         MapAggBuffer myagg = (MapAggBuffer) agg;
 
-        if( parameters.length > 2 ) {
-          Object numValsObj = parameters[2];
-          int nv = nvOI.get( numValsObj);
-          myagg.setNumValues( nv);
-        }
         putIntoSet(k, v, myagg);
       }
     }
@@ -227,18 +213,6 @@ public class CollectMaxUDAF extends AbstractGenericUDAFResolver {
         throws HiveException {
       MapAggBuffer myagg = (MapAggBuffer) agg;
       Map<Object,Object> partialResult = (Map<Object,Object>)  internalMergeOI.getMap(partial);
-      for(Object key : partialResult.keySet()) {
-         StringObjectInspector strInsp = (StringObjectInspector) this.inputKeyOI;
-         String keyCopy = strInsp.getPrimitiveJavaObject(key);
-         if( keyCopy.equals("NumValues"))  {
-            DoubleObjectInspector dblInsp = (DoubleObjectInspector) this.inputValOI;
-            Object val = partialResult.get( key);
-            Double valCopy = dblInsp.get(val);
-            myagg.setNumValues( valCopy.intValue() );
-            partialResult.remove( key);
-            break;
-         }
-      }
       for(Object i : partialResult.keySet()) {
         putIntoSet(i, partialResult.get(i), myagg);
       }
@@ -258,21 +232,14 @@ public class CollectMaxUDAF extends AbstractGenericUDAFResolver {
     }
 
     private void putIntoSet(Object key, Object val, MapAggBuffer myagg) {
-      StringObjectInspector strInsp = (StringObjectInspector) this.inputKeyOI;
-      DoubleObjectInspector dblInsp = (DoubleObjectInspector) this.inputValOI;
-
-      String keyCopy = strInsp.getPrimitiveJavaObject(key);
-      Double valCopy = dblInsp.get(val);
-
-      myagg.addValue(keyCopy, valCopy);
+      myagg.addValue(key, val);
     }
 
     @Override
     public Object terminatePartial(AggregationBuffer agg) throws HiveException {
 
       MapAggBuffer myagg = (MapAggBuffer) agg;
-      Map<String, Double> vals =  myagg.getValueMap();
-      vals.put("NumValues", (double)myagg.numValues);
+      Map<Object, Object> vals =  myagg.getValueMap();
       return vals;
     }
   }
