@@ -17,18 +17,18 @@ package brickhouse.udf.collect;
  **/
 
 
-import java.util.ArrayList;
-import java.util.List;
-
 import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
 import org.apache.hadoop.hive.ql.metadata.HiveException;
 import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
 import org.apache.hadoop.hive.serde2.objectinspector.ListObjectInspector;
 import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
 import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector.Category;
+import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory;
 import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorUtils;
+import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
+import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector.PrimitiveCategory;
+import org.apache.hadoop.hive.serde2.objectinspector.StandardListObjectInspector;
 import org.apache.hadoop.hive.serde2.objectinspector.primitive.IntObjectInspector;
-import org.apache.log4j.Logger;
 
 
 /**
@@ -36,28 +36,22 @@ import org.apache.log4j.Logger;
  *
  */
 public class TruncateArrayUDF extends GenericUDF {
-	private static final Logger LOG = Logger.getLogger( TruncateArrayUDF.class);
 	private ListObjectInspector listInspector;
+	private StandardListObjectInspector returnInspector;
 	private IntObjectInspector intInspector;
 	
 	
-    public List<Object> evaluate( List<Object> array, int numVals) {
-        List truncatedList = new ArrayList();	
-        for(int i=0; i< numVals && i <array.size(); ++i) {
-        	truncatedList.add( listInspector.getListElement( array, i ));
-        }
-        
-        return truncatedList;
-    }
-
-
 	@Override
 	public Object evaluate(DeferredObject[] arg0) throws HiveException {
-		List argList = listInspector.getList(arg0[0].get() );
-		
 		int numVals = intInspector.get( arg0[1].get());
+		Object uninspListObj = arg0[0].get();
+		int listSize = listInspector.getListLength(uninspListObj);
 		
-		return evaluate( argList, numVals);
+		Object truncatedListObj =  returnInspector.create( numVals);
+        for(int i=0; i< numVals && i <listSize; ++i) {
+        	returnInspector.set( truncatedListObj, i,  listInspector.getListElement( uninspListObj, i ));
+        }
+        return truncatedListObj;
 	}
 
 
@@ -70,17 +64,29 @@ public class TruncateArrayUDF extends GenericUDF {
 	@Override
 	public ObjectInspector initialize(ObjectInspector[] arg0)
 			throws UDFArgumentException {
-		ObjectInspector first = ObjectInspectorUtils.getStandardObjectInspector(arg0[0] );
+		ObjectInspector first = arg0[0];
 		if(first.getCategory() == Category.LIST) {
 			listInspector = (ListObjectInspector) first;
 		} else {
-			throw new UDFArgumentException(" Exprecting an array as first argument ");
+			throw new UDFArgumentException(" Expecting an array and an int as arguments ");
 		}
 		
-		//// 
-	    ObjectInspector second = ObjectInspectorUtils.getStandardObjectInspector(arg0[1]);
-	    intInspector= (IntObjectInspector) second;
-		return first;
+	    ObjectInspector second = arg0[1];
+	    if( second.getCategory() == Category.PRIMITIVE) {
+	    	PrimitiveObjectInspector secondPrim = (PrimitiveObjectInspector) second;
+	    	if( secondPrim.getPrimitiveCategory() == PrimitiveCategory.INT ) {
+	    		intInspector= (IntObjectInspector) second;
+	    	} else {
+			  throw new UDFArgumentException(" Expecting an array and an int as arguments ");
+	    	}
+	    } else {
+			throw new UDFArgumentException(" Expecting an array and an int as arguments ");
+	    }
+	    
+	    
+	    returnInspector = ObjectInspectorFactory.getStandardListObjectInspector(
+	    		listInspector.getListElementObjectInspector());
+		return returnInspector;
 	}
 
 }
