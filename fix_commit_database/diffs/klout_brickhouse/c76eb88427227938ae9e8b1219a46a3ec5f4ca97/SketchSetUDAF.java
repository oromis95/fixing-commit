@@ -26,9 +26,11 @@ import org.apache.hadoop.hive.ql.metadata.HiveException;
 import org.apache.hadoop.hive.ql.parse.SemanticException;
 import org.apache.hadoop.hive.ql.udf.generic.AbstractGenericUDAFResolver;
 import org.apache.hadoop.hive.ql.udf.generic.GenericUDAFEvaluator;
+import org.apache.hadoop.hive.serde2.objectinspector.ConstantObjectInspector;
 import org.apache.hadoop.hive.serde2.objectinspector.MapObjectInspector;
 import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
 import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory;
+import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector.Category;
 import org.apache.hadoop.hive.serde2.objectinspector.primitive.LongObjectInspector;
 import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
 import org.apache.hadoop.hive.serde2.objectinspector.primitive.StringObjectInspector;
@@ -54,6 +56,13 @@ public class SketchSetUDAF extends AbstractGenericUDAFResolver {
   @Override
   public GenericUDAFEvaluator getEvaluator(TypeInfo[] parameters)
       throws SemanticException {
+      if( !parameters[0].getTypeName().equals("string") 
+              && !parameters[0].getTypeName().equals("bigint")) {
+          throw new SemanticException("sketch_set UDAF only takes String or longs as values");
+      }
+      if( !parameters[0].getTypeName().equals("int")) {
+          throw new SemanticException("Size of sketch must be an int");
+      }
     return new SketchSetUDAFEvaluator();
   }
 
@@ -64,25 +73,22 @@ public class SketchSetUDAF extends AbstractGenericUDAFResolver {
 	  private MapObjectInspector partialMapOI;
 	  private LongObjectInspector partialMapHashOI;
 	  private StringObjectInspector partialMapStrOI;
+	  private int sketchSetSize = DEFAULT_SKETCH_SET_SIZE;
 
 
     static class SketchSetBuffer implements AggregationBuffer {
-    	private SketchSet sketchSet;
+    	private SketchSet sketchSet = null;
     	
      
-    	public void init() {
+    	public void init(int size) {
     		if( sketchSet == null) {
-    			sketchSet = new SketchSet( DEFAULT_SKETCH_SET_SIZE);
+    			sketchSet = new SketchSet( size);
     		} else {
     			sketchSet.clear();
     		}
     	}
     	public void reset() {
-    		if( sketchSet == null) {
-    			sketchSet = new SketchSet( DEFAULT_SKETCH_SET_SIZE);
-    		} else {
-    			sketchSet.clear();
-    		}
+    	    sketchSet = null;
     	}
     	
     	
@@ -108,13 +114,21 @@ public class SketchSetUDAF extends AbstractGenericUDAFResolver {
         throws HiveException {
       super.init(m, parameters);
       LOG.info(" SketchSetUDAF.init() - Mode= " + m.name() );
-      for(int i=0; i<parameters.length; ++i) {
-        LOG.info(" ObjectInspector[ "+ i + " ] = " + parameters[0]);
-      }
       /// 
       if (m == Mode.PARTIAL1 || m == Mode.COMPLETE) {
     	  //// iterate() gets called.. string is passed in
     	  this.inputStrOI = (StringObjectInspector) parameters[0];
+    	  
+    	  if( parameters.length > 1) {
+    	      //// get the sketch set size from the second parameters
+    	      if(!( parameters[1] instanceof ConstantObjectInspector ) ) {
+    	         throw new HiveException("Sketch Set size must be a constant");
+    	      }
+    	      ConstantObjectInspector sizeOI = (ConstantObjectInspector) parameters[1];
+    	      
+    	      this.sketchSetSize = (Integer) sizeOI.getWritableConstantValue();
+    	  }
+    	  
       } else { /// Mode m == Mode.PARTIAL2 || m == Mode.FINAL
     	   /// merge() gets called ... map is passed in ..
     	  this.partialMapOI = (MapObjectInspector) parameters[0];
@@ -139,7 +153,7 @@ public class SketchSetUDAF extends AbstractGenericUDAFResolver {
     @Override
     public AggregationBuffer getNewAggregationBuffer() throws HiveException {
       SketchSetBuffer buff= new SketchSetBuffer();
-      buff.init();
+      buff.init(sketchSetSize);
       return buff;
     }
 
@@ -162,11 +176,15 @@ public class SketchSetUDAF extends AbstractGenericUDAFResolver {
     	/// Partial is going to be a map of strings and hashes 
         SketchSetBuffer myagg = (SketchSetBuffer) agg;
         
-        Map<Object,Object> partialResult = (Map<Object,Object>)  this.partialMapOI.getMap(partial);
-        for( Map.Entry entry : partialResult.entrySet()) {
-        	Long hash = this.partialMapHashOI.get( entry.getKey());
-        	String item = partialMapStrOI.getPrimitiveJavaObject( entry.getValue());
-        	myagg.addHash(hash, item);
+        if( partial != null) {
+            Map<Object,Object> partialResult = (Map<Object,Object>)  this.partialMapOI.getMap(partial);
+            if( partialResult !=null) {
+                for( Map.Entry entry : partialResult.entrySet()) {
+                    Long hash = this.partialMapHashOI.get( entry.getKey());
+                    String item = partialMapStrOI.getPrimitiveJavaObject( entry.getValue());
+                    myagg.addHash(hash, item);
+                }
+            }
         }
     }
 
