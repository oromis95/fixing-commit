@@ -24,15 +24,23 @@ import org.apache.hadoop.hive.ql.exec.Description;
 import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
 import org.apache.hadoop.hive.ql.metadata.HiveException;
 import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
+import org.apache.hadoop.hive.serde2.objectinspector.ConstantObjectInspector;
 import org.apache.hadoop.hive.serde2.objectinspector.MapObjectInspector;
 import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
 import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector.Category;
 import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory;
+import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
+import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector.PrimitiveCategory;
 import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
 import org.apache.hadoop.hive.serde2.objectinspector.primitive.StringObjectInspector;
+import org.apache.hadoop.hive.serde2.typeinfo.TypeInfo;
+import org.apache.hadoop.hive.serde2.typeinfo.TypeInfoUtils;
+import org.codehaus.jackson.JsonNode;
 import org.codehaus.jackson.JsonProcessingException;
 import org.codehaus.jackson.map.ObjectMapper;
 
+import brickhouse.udf.json.InspectorHandle.InspectorHandleFactory;
+
 /**
  *  Given a JSON String containing a map with values of all the same type,
  *    return a Hive map of key-value pairs 
@@ -44,24 +52,62 @@ value = "_FUNC_(json) - Returns a map of key-value pairs from a JSON string"
 )
 public class JsonMapUDF extends GenericUDF {
 	private StringObjectInspector stringInspector;
+	private InspectorHandle inspHandle;
 
 	@Override
 	public ObjectInspector initialize(ObjectInspector[] arguments)
 			throws UDFArgumentException {
-		if(arguments.length != 1) {
-			throw new UDFArgumentException("Usage : json_map( jsonstring) ");
+		if(arguments.length != 1 && arguments.length != 2) {
+			throw new UDFArgumentException("Usage : json_map( jsonstring, optional typestring ) ");
+		}
+		if(!arguments[0].getCategory().equals( Category.PRIMITIVE)
+		        || ((PrimitiveObjectInspector)arguments[0]).getPrimitiveCategory() != PrimitiveCategory.STRING) {
+			throw new UDFArgumentException("Usage : json_map( jsonstring, optional typestring) ");
 		}
-		if(!arguments[0].getCategory().equals( Category.PRIMITIVE)) {
-			throw new UDFArgumentException("Usage : json_map( jsonstring) ");
+		if(!arguments[1].getCategory().equals( Category.PRIMITIVE)
+		        && ((PrimitiveObjectInspector)arguments[1]).getPrimitiveCategory() != PrimitiveCategory.STRING) {
+			throw new UDFArgumentException("Usage : json_map( jsonstring, optional typestring) ");
 		}
 		
 		stringInspector = (StringObjectInspector) arguments[0];
 		
-		ObjectInspector keyInsp = PrimitiveObjectInspectorFactory.javaStringObjectInspector;
-		ObjectInspector valueInsp = PrimitiveObjectInspectorFactory.javaDoubleObjectInspector; /// XXX Make value type configurable somehow
+		if( arguments.length > 1) {
+		    if(!arguments[1].getCategory().equals( Category.PRIMITIVE)
+		        && ((PrimitiveObjectInspector)arguments[1]).getPrimitiveCategory() != PrimitiveCategory.STRING) {
+		        throw new UDFArgumentException("Usage : json_map( jsonstring, optional typestring) ");
+		    }
+		    if(!(arguments[1] instanceof ConstantObjectInspector)) {
+		        throw new UDFArgumentException("json_map( jsonstring, typestring ) : typestring must be a constant");
+		    }
+		    ConstantObjectInspector constInsp = (ConstantObjectInspector) arguments[1];
+		    String typeStr = (String) constInsp.getWritableConstantValue();
+		    
+		    String[] types = typeStr.split(",");
+		    if( types.length != 2) {
+		        throw new UDFArgumentException(" typestring must be of the form <keytype>,<valuetype>");
+		    }
+		    TypeInfo keyType = TypeInfoUtils.getTypeInfoFromTypeString(types[0]);
+		    TypeInfo valType = TypeInfoUtils.getTypeInfoFromTypeString(types[1]);
+		    
+		    ObjectInspector keyInsp = TypeInfoUtils.getStandardJavaObjectInspectorFromTypeInfo(keyType);
+		    ObjectInspector valInsp = TypeInfoUtils.getStandardJavaObjectInspectorFromTypeInfo(valType);
+		    
+		    MapObjectInspector mapInsp = ObjectInspectorFactory.getStandardMapObjectInspector(keyInsp, valInsp);
+		    
+		    inspHandle = InspectorHandleFactory.GenerateInspectorHandle(mapInsp);
+		    
+		    return inspHandle.getReturnType();
+		    
+		} else {
+		    ObjectInspector keyInsp = PrimitiveObjectInspectorFactory.javaStringObjectInspector;
+		    ObjectInspector valueInsp = PrimitiveObjectInspectorFactory.javaDoubleObjectInspector; /// XXX Make value type configurable somehow
 		
-		MapObjectInspector mapInsp = ObjectInspectorFactory.getStandardMapObjectInspector(keyInsp, valueInsp);
-		return mapInsp;
+		    MapObjectInspector mapInsp = ObjectInspectorFactory.getStandardMapObjectInspector(keyInsp, valueInsp);
+		    
+		    inspHandle = InspectorHandleFactory.GenerateInspectorHandle(mapInsp);
+		    
+		    return inspHandle.getReturnType();
+		}
 	}
 
 	@Override
@@ -69,28 +115,11 @@ public class JsonMapUDF extends GenericUDF {
 		try {
 		    String jsonString =  this.stringInspector.getPrimitiveJavaObject(arguments[0].get());
 			
-		    ObjectMapper om = new ObjectMapper();
-		    Object root = om.readValue(jsonString, Object.class);
-		    Map<String,Object> rootAsMap = om.readValue(jsonString, Map.class);
-		    
-		    Map<String,Double> copyMap = new HashMap<String,Double>();
-		    for(String key : rootAsMap.keySet() ) {
-		    	Object valObj = rootAsMap.get(key);
-		    	if(valObj instanceof Double) {
-		    		copyMap.put( key,(Double)valObj);
-		    	} else if (valObj instanceof Number) {
-		    		
-		    		Number num =(Number)valObj;
-		    		Double newDouble = num.doubleValue();
-		    	
-		    		copyMap.put( key, newDouble);
-		    				
-		    	} else {
-		    		System.err.println(" Don't know how to interpret " + valObj);
-		    	}
-		    	
-		    }
-		    return copyMap;
+		    //// Logic is the same as "from_json"
+            ObjectMapper om = new ObjectMapper();
+            JsonNode jsonNode = om.readTree( jsonString);
+            return inspHandle.parseJson(jsonNode);
+            
 		} catch (JsonProcessingException e) {
 			throw new HiveException(e);
 		} catch (IOException e) {
