@@ -24,14 +24,22 @@ import org.apache.hadoop.hive.ql.exec.Description;
 import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
 import org.apache.hadoop.hive.ql.metadata.HiveException;
 import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
+import org.apache.hadoop.hive.serde2.objectinspector.ConstantObjectInspector;
 import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
 import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector.Category;
+import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
+import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector.PrimitiveCategory;
 import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory;
 import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
 import org.apache.hadoop.hive.serde2.objectinspector.primitive.StringObjectInspector;
+import org.apache.hadoop.hive.serde2.typeinfo.TypeInfo;
+import org.apache.hadoop.hive.serde2.typeinfo.TypeInfoUtils;
+import org.codehaus.jackson.JsonNode;
 import org.codehaus.jackson.JsonProcessingException;
 import org.codehaus.jackson.map.ObjectMapper;
 
+import brickhouse.udf.json.InspectorHandle.InspectorHandleFactory;
+
 /**
  *   UDF to split a JSON array into individual json strings ...
  *
@@ -42,33 +50,26 @@ import org.codehaus.jackson.map.ObjectMapper;
 )
 public class JsonSplitUDF extends GenericUDF {
   private StringObjectInspector stringInspector;
+  private InspectorHandle inspHandle;
 
 
   @Override
   public Object evaluate(DeferredObject[] arguments) throws HiveException {
-    List<String> retJsonArr = new ArrayList<String>();
     try {
       String jsonString = this.stringInspector.getPrimitiveJavaObject(arguments[0].get());
-
+      
+      //// Logic is the same as "from_json"
       ObjectMapper om = new ObjectMapper();
-      Object root = om.readValue(jsonString, Object.class);
-      List<Object> rootAsMap = om.readValue(jsonString, List.class);
-      for( Object jsonObj : rootAsMap) {
-        if (jsonObj != null){
-          String jsonStr = om.writeValueAsString(jsonObj);
-          retJsonArr.add(jsonStr);
-        }
-      }
-
-      return retJsonArr;
+      JsonNode jsonNode = om.readTree( jsonString);
+      return inspHandle.parseJson(jsonNode);
 
 
     } catch( JsonProcessingException jsonProc) {
-      return null;
+      throw new HiveException(jsonProc);
     } catch (IOException e) {
-      return null;
+      throw new HiveException(e);
     } catch (NullPointerException npe){
-      return retJsonArr;
+      return null;
     }
 
   }
@@ -81,18 +82,39 @@ public class JsonSplitUDF extends GenericUDF {
   @Override
   public ObjectInspector initialize(ObjectInspector[] arguments)
       throws UDFArgumentException {
-    if(arguments.length != 1) {
-      throw new UDFArgumentException("Usage : json_split jsonstring) ");
+    if(arguments.length != 1 && arguments.length != 2) {
+      throw new UDFArgumentException("Usage : json_split( jsonstring, optional typestring) ");
     }
-    if(!arguments[0].getCategory().equals( Category.PRIMITIVE)) {
-      throw new UDFArgumentException("Usage : json_split( jsonstring) ");
+    if(!arguments[0].getCategory().equals( Category.PRIMITIVE)
+    		|| ((PrimitiveObjectInspector)arguments[0]).getPrimitiveCategory() != PrimitiveCategory.STRING) {
+      throw new UDFArgumentException("Usage : json_split( jsonstring, optional typestring) ");
     }
     stringInspector = (StringObjectInspector) arguments[0];
-
-    ObjectInspector valInspector = PrimitiveObjectInspectorFactory.javaStringObjectInspector;
-
-    ObjectInspector setInspector = ObjectInspectorFactory.getStandardListObjectInspector(valInspector);
-    return setInspector;
+    
+    if( arguments.length > 1) {
+       if(!arguments[1].getCategory().equals( Category.PRIMITIVE)
+    		|| ((PrimitiveObjectInspector)arguments[0]).getPrimitiveCategory() != PrimitiveCategory.STRING) {
+         throw new UDFArgumentException("Usage : json_split( jsonstring, optional typestring) ");
+       }
+       if( !(arguments[1] instanceof ConstantObjectInspector) ) {
+    	  throw new UDFArgumentException("Usage : json_split( jsonstring, typestring) : typestring must be constant");
+       }
+       ConstantObjectInspector typeInsp = (ConstantObjectInspector) arguments[1];
+       String typeString = (String) typeInsp.getWritableConstantValue();
+       TypeInfo valType = TypeInfoUtils.getTypeInfoFromTypeString(typeString);
+       
+       ObjectInspector valInsp = TypeInfoUtils.getStandardJavaObjectInspectorFromTypeInfo(valType);
+       ObjectInspector setInspector = ObjectInspectorFactory.getStandardListObjectInspector(valInsp);
+       inspHandle = InspectorHandleFactory.GenerateInspectorHandle(setInspector);
+       return inspHandle.getReturnType();
+    	
+    } else {
+        ObjectInspector valInspector = PrimitiveObjectInspectorFactory.javaStringObjectInspector;
+
+        ObjectInspector setInspector = ObjectInspectorFactory.getStandardListObjectInspector(valInspector);
+        inspHandle = InspectorHandleFactory.GenerateInspectorHandle(setInspector);
+        return inspHandle.getReturnType();
+    }
   }
 
 }
