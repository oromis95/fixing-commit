@@ -106,14 +106,16 @@ public interface InspectorHandle {
     		}
     		return ObjectInspectorFactory.getStandardStructObjectInspector(fieldNames, structFieldObjectInspectors);
     	}
-    	
+
     }
-    
+
 	
 	class MapHandle implements InspectorHandle {
 		private InspectorHandle mapValHandle;
 		private StandardMapObjectInspector retInspector;
 
+		public MapHandle() {} // For Kryo Deserialization
+
 		/// for JSON maps (or "objects"), the keys are always string objects
 		///  
 		public MapHandle( MapObjectInspector insp) throws UDFArgumentException {
