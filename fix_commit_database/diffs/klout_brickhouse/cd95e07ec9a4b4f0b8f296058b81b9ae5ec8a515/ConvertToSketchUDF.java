@@ -23,12 +23,18 @@ import org.apache.hadoop.hive.ql.exec.UDF;
 import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
 import org.apache.hadoop.hive.ql.metadata.HiveException;
 import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
+import org.apache.hadoop.hive.serde2.objectinspector.ConstantObjectInspector;
 import org.apache.hadoop.hive.serde2.objectinspector.ListObjectInspector;
 import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
 import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory;
+import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
 import org.apache.hadoop.hive.serde2.objectinspector.StandardListObjectInspector;
+import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector.Category;
+import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector.PrimitiveCategory;
+import org.apache.hadoop.hive.serde2.objectinspector.primitive.IntObjectInspector;
 import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
 import org.apache.hadoop.hive.serde2.objectinspector.primitive.StringObjectInspector;
+import org.apache.hadoop.io.IntWritable;
 import org.apache.log4j.Logger;
 
 import brickhouse.analytics.uniques.SketchSet;
@@ -46,42 +52,27 @@ value = "_FUNC_(x) - Truncate a large array of strings, and return a list of str
 public class ConvertToSketchUDF extends GenericUDF {
 	private static final Logger LOG = Logger.getLogger( ConvertToSketchUDF.class);
 	private ListObjectInspector listInspector;
+	private StringObjectInspector listElemInspector;
 	private StandardListObjectInspector retInspector;
-
-	public List<String> evaluate(List<Object> objList) {
-		SketchSet sketch = new SketchSet();
-	
-		for(Object item : objList) {
-			if(item !=null)
-				sketch.addItem(item.toString());
-			else
-				LOG.warn( "Item in " + objList + " is null");
-		}
-		return sketch.getMinHashItems();
-	}
+	private int sketchSetSize = SketchSetUDAF.DEFAULT_SKETCH_SET_SIZE;
 
 	@Override
 	public Object evaluate(DeferredObject[] arg0) throws HiveException {
 		Object obj = arg0[0].get();
-		if( obj == null) {
+		if(obj == null) {
 			return null;
 		}
 		List oldList = listInspector.getList(obj);
-		List newList = (List) retInspector.create(0);
-		StringObjectInspector strInspector = (StringObjectInspector) listInspector.getListElementObjectInspector();
+		SketchSet sketchSet  = new SketchSet( sketchSetSize);
 		for( Object oldObj : oldList) {
 			if( oldObj == null) {
 				LOG.warn(" Object in uninspected List is null");
 			} else {
-				String newStr = strInspector.getPrimitiveJavaObject(oldObj);
-				if(newStr == null) 
-					LOG.warn(" inspected object is null !!! ");
-				else
-					newList.add( newStr);
-				
+				String newStr = listElemInspector.getPrimitiveJavaObject(oldObj);
+				sketchSet.addItem(newStr);
 			}
 		}
-		return evaluate(newList);
+		return sketchSet.getMinHashItems();
 	}
 
 	@Override
@@ -92,7 +83,28 @@ public class ConvertToSketchUDF extends GenericUDF {
 	@Override
 	public ObjectInspector initialize(ObjectInspector[] arg0)
 			throws UDFArgumentException {
+	    if(arg0.length > 2) {
+	       throw new UDFArgumentException("convert_to_sketch takes an array of strings, and an optional sketch set size.");
+	    }
+	    if(arg0[0].getCategory() != Category.LIST) {
+	       throw new UDFArgumentException("convert_to_sketch takes an array of strings, and an optional sketch set size.");
+	    }
 		listInspector = (ListObjectInspector) arg0[0];
+		if(listInspector.getListElementObjectInspector().getCategory() != Category.PRIMITIVE
+		        || ((PrimitiveObjectInspector)listInspector.getListElementObjectInspector()).getPrimitiveCategory() != PrimitiveCategory.STRING ) {
+	       throw new UDFArgumentException("convert_to_sketch takes an array of strings, and an optional sketch set size.");
+		}
+		listElemInspector = (StringObjectInspector) listInspector.getListElementObjectInspector();
+		
+		if(arg0.length > 1) {
+		   if( !(arg0[1] instanceof IntObjectInspector)
+		           || !(arg0[1] instanceof ConstantObjectInspector)) {
+		      throw new UDFArgumentException("sketch set size must be a constant int value.");
+		   }
+		   IntWritable sizeInt = (IntWritable)((ConstantObjectInspector)arg0[1]).getWritableConstantValue();
+		   sketchSetSize = sizeInt.get();
+		}
+		
 		retInspector =  ObjectInspectorFactory.getStandardListObjectInspector(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
 		return retInspector;
 	}
