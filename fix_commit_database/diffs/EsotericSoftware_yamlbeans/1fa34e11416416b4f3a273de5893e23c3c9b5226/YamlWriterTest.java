@@ -1,13 +1,13 @@
 /*
  * Copyright (c) 2008 Nathan Sweet
- * 
+ *
  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
  * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
  * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
  * is furnished to do so, subject to the following conditions:
- * 
+ *
  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
- * 
+ *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
  * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
@@ -206,6 +206,27 @@ public class YamlWriterTest extends TestCase {
 		roundTrip(test);
 	}
 
+
+	void checkWriterOutput(String example, String expected) throws Exception {
+		StringWriter buffer = new StringWriter();
+		YamlWriter writer = new YamlWriter(buffer, new YamlConfig());
+		writer.write(example);
+		writer.close();
+		assertEquals(expected, buffer.toString());
+	}
+
+	public void testSingleQuotesDoubling () throws Exception {
+		checkWriterOutput("abc", "abc\n");
+		checkWriterOutput("abc def", "abc def\n");
+		// single quotes around output as soon as we have a colon in the input
+		checkWriterOutput("abc: def", "'abc: def'\n");
+		checkWriterOutput("abc: def'ghi", "'abc: def''ghi'\n");
+		checkWriterOutput("abc: def 'ghi", "'abc: def ''ghi'\n");
+		// That was the initial bug:
+		checkWriterOutput("A: 'X'", "'A: ''X'''\n");
+	}
+
+
 	public void testObjectField () throws Exception {
 		ValueHolder object = new ValueHolder();
 		object.value = "XYZ";
