@@ -48,6 +48,7 @@ public class YamlConfig {
 	boolean privateFields;
 	boolean privateConstructors = true;
 	boolean allowDuplicates = true;
+	String tagSuffix;
 
 	public YamlConfig () {
 		scalarSerializers.put(Date.class, new DateSerializer());
@@ -123,6 +124,12 @@ public class YamlConfig {
 		this.privateConstructors = privateConstructors;
 	}
 
+	/** When not null, YAML read into a {@link Map} stores any value tags using key + tagSuffix, and when writing YAML the value
+	 * tags are output. Key tags are not stored in the map. Default is null. */
+	public void setTagSuffix (String tagSuffix) {
+		this.tagSuffix = tagSuffix;
+	}
+
 	static public class WriteConfig {
 		boolean explicitFirstDocument = false;
 		boolean explicitEndDocument = false;
@@ -226,7 +233,6 @@ public class YamlConfig {
 		boolean ignoreUnknownProperties;
 		boolean autoMerge = true;
 		boolean classTags = true;
-		String tagSuffix;
 
 		ReadConfig () {
 		}
@@ -269,11 +275,6 @@ public class YamlConfig {
 		public void setClassTags (boolean classTags) {
 			this.classTags = classTags;
 		}
-
-		/** When not null, YAML read into a {@link Map} stores any tags using key + tagSuffix. Default is null. */
-		public void setTagSuffix (String tagSuffix) {
-			this.tagSuffix = tagSuffix;
-		}
 	}
 
 	static class ConstructorParameters {
