@@ -47,6 +47,7 @@ public class YamlConfig {
 	boolean beanProperties = true;
 	boolean privateFields;
 	boolean privateConstructors = true;
+	boolean allowDuplicates = true;
 
 	public YamlConfig () {
 		scalarSerializers.put(Date.class, new DateSerializer());
@@ -58,6 +59,11 @@ public class YamlConfig {
 		tagToClass.put("tag:yaml.org,2002:float", Float.class);
 	}
 
+	/** Allows duplicate keys in YAML document. Default is true. */
+	public void setAllowDuplicates (boolean allowDuplicates) {
+		this.allowDuplicates = allowDuplicates;
+	}
+
 	/** Allows the specified tag to be used in YAML instead of the full class name. */
 	public void setClassTag (String tag, Class type) {
 		if (tag == null) throw new IllegalArgumentException("tag cannot be null.");
@@ -264,4 +270,4 @@ public class YamlConfig {
 	public static enum WriteClassName {
 		ALWAYS, NEVER, AUTO
 	}
-}
+}
\ No newline at end of file
