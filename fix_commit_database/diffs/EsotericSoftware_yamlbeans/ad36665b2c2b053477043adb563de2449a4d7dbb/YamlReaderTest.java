@@ -16,6 +16,7 @@
 
 package com.esotericsoftware.yamlbeans;
 
+import java.io.StringReader;
 import java.util.List;
 import java.util.Map;
 
@@ -318,6 +319,48 @@ public class YamlReaderTest extends TestCase {
 		}
 	}
 	
+	public void testDuplicateKeysAreNotAllowedIfAllowDuplicateIsSetSo () {
+		String inputWithDuplicates = "a: 1\na: 2\nc: 3";
+		YamlConfig yamlConfig = new YamlConfig();
+		yamlConfig.setAllowDuplicates(false);
+		YamlReader yamlReader = new YamlReader(inputWithDuplicates, yamlConfig);
+		try {
+			yamlReader.read();
+			fail("Duplicates should not have been allowed.");
+		} catch (YamlException e) {
+		}
+
+		String inputWithoutDuplicates = "a: 1\nb: 2\nc: 3";
+		YamlReader yamlReader1 = new YamlReader(inputWithoutDuplicates, yamlConfig);
+		try {
+			yamlReader1.read();
+		} catch (YamlException e) {
+			fail("Duplicates should have been allowed.");
+		}
+	}
+
+	public void testDuplicateKeysAreAllowedIfAllowDuplicateIsSetSo () {
+		String inputWithDuplicates = "a: 1\na: 2\nc: 3";
+		YamlReader yamlReader = new YamlReader(inputWithDuplicates);
+		try {
+			yamlReader.read();
+		} catch (YamlException e) {
+			e.printStackTrace();
+			fail("Duplicates should have been allowed.");
+		}
+
+		String inputWithoutDuplicates = "a: 1\nb: 2\nc: 3";
+		YamlConfig yamlConfig = new YamlConfig();
+		yamlConfig.setAllowDuplicates(false);
+		YamlReader yamlReader1 = new YamlReader(inputWithoutDuplicates, yamlConfig);
+		try {
+			yamlReader1.read();
+		} catch (YamlException e) {
+			e.printStackTrace();
+			fail("Duplicates should have been allowed.");
+		}
+	}
+
 	private static class TypeTagIgnoringReader extends YamlReader {
 
 		public TypeTagIgnoringReader(String yaml) {
@@ -385,4 +428,4 @@ public class YamlReaderTest extends TestCase {
 		assertEquals(1, lake.fish.size());
 		assertEquals("Walleye", lake.fish.get(0).species);
 	}
-}
+}
\ No newline at end of file
