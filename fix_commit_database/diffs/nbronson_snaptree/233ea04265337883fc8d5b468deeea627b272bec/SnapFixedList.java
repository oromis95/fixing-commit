@@ -1,14 +1,9 @@
-/* CCSTM - (c) 2009 Stanford University - PPL */
+/* SnapTree - (c) 2009 Stanford University - PPL */
 
 // SnapFixedList
 package edu.stanford.ppl.concurrent;
 
-import sun.misc.Unsafe;
-
 import java.util.AbstractList;
-import java.util.Arrays;
-import java.util.concurrent.CountDownLatch;
-import java.util.concurrent.atomic.AtomicReference;
 import java.util.concurrent.atomic.AtomicReferenceArray;
 
 /** Implements a concurrent fixed-size <code>List</code> with fast clone and
@@ -24,50 +19,20 @@ public class SnapFixedList<E> extends AbstractList<E> implements Cloneable {
     private static final int BF_MASK = BF - 1;
 
     // Internally this is implemented as an external tree with branching factor
-    // BF.  The leaves of the tree are the elements E.
-
-    private static class Epoch extends ClosableRefCount {
-        Epoch queued;
-        private final CountDownLatch _closed = new CountDownLatch(1);
-
-        Epoch() {
-        }
-
-        Epoch(final Epoch queued) {
-            this.queued = queued;
-        }
-
-        protected void onClose() {
-            queued.queued = new Epoch();
-            _closed.countDown();
-        }
+    // BF.  The leaves of the tree are the elements E.  Nodes are considered to
+    // be unshared if they have the same generation as the root node.
 
-        public void awaitClosed() {
-            boolean interrupted = false;
-            while (true) {
-                try {
-                    _closed.await();
-                    break;
-                }
-                catch (final InterruptedException xx) {
-                    interrupted = true;
-                }
-            }
-            if (interrupted) {
-                Thread.currentThread().interrupt();
-            }
-        }
+    private static class Generation {
     }
 
     private static class Node extends AtomicReferenceArray<Object> {
-        /** This is not volatile, because it is not changed after the initial
-         *  publication of the node.
-         */
-        Epoch epoch;
+        // never modified after initialization of the SnapFixedList, but
+        // convenient to be non-final
+        Generation gen;
 
-        Node(Epoch epoch, int size, Object initialValue) {
+        Node(final Generation gen, final int size, final Object initialValue) {
             super(size);
-            this.epoch = epoch;
+            this.gen = gen;
             if (initialValue != null) {
                 for (int i = 0; i < size; ++i) {
                     lazySet(i, initialValue);
@@ -75,15 +40,25 @@ public class SnapFixedList<E> extends AbstractList<E> implements Cloneable {
             }
         }
 
-        Node(Epoch epoch, Node src) {
+        Node(final Generation gen, final Node src) {
             super(src.length());
-            this.epoch = epoch;
+            this.gen = gen;
             for (int i = 0; i < src.length(); ++i) {
                 lazySet(i, src.get(i));
             }
         }
     }
 
+    private static class COWMgr extends CopyOnWriteManager<Node> {
+        private COWMgr(final Node initialValue) {
+            super(initialValue, 0);
+        }
+
+        protected Node freezeAndClone(final Node value) {
+            return new Node(new Generation(), value);
+        }
+    }
+
     /** 0 if _size == 0, otherwise the smallest positive int such that
      *  (1L << (LOG_BF * _height)) >= _size.
      */
@@ -91,7 +66,7 @@ public class SnapFixedList<E> extends AbstractList<E> implements Cloneable {
 
     private final int _size;
 
-    private final AtomicReference<Node> _rootRef;
+    private CopyOnWriteManager<Node> _rootRef;
 
     public SnapFixedList(final int size) {
         this(size, null);
@@ -102,11 +77,11 @@ public class SnapFixedList<E> extends AbstractList<E> implements Cloneable {
         Node partial = null;
 
         if (size > 0) {
-            // We will insert the epoch into all of the partials (since they
+            // We will insert the gen into all of the partials (since they
             // are used exactly once).  We reuse the fulls, so we will give
-            // them a null Epoch that will cause them to be copied before any
+            // them a null gen that will cause them to be copied before any
             // actual writes.
-            final Epoch epoch = new Epoch();
+            final Generation gen = new Generation();
 
             Object full = element;
 
@@ -122,7 +97,7 @@ public class SnapFixedList<E> extends AbstractList<E> implements Cloneable {
                 Node newP = null;
                 if (partial != null || (levelSize & BF_MASK) != 0) {
                     final int partialBF = ((levelSize - 1) & BF_MASK) + 1;
-                    newP = new Node(epoch, partialBF, full);
+                    newP = new Node(gen, partialBF, full);
                     if (partial != null) {
                         newP.set(partialBF - 1, partial);
                     }
@@ -138,14 +113,14 @@ public class SnapFixedList<E> extends AbstractList<E> implements Cloneable {
                     // we're done
                     if (newP == null) {
                         // top level is a full, which isn't duplicated
-                        newF.epoch = epoch;
+                        newF.gen = gen;
                         partial = newF;
                     }
                     else {
                         // Top level is a partial.  If it uses exactly one
                         // full child, then we can mark that as unshared.
                         if (newP.length() == 2 && newP.get(0) != newP.get(1)) {
-                            ((Node) newP.get(0)).epoch = epoch;
+                            ((Node) newP.get(0)).gen = gen;
                         }
                         partial = newP;
                     }
@@ -161,7 +136,20 @@ public class SnapFixedList<E> extends AbstractList<E> implements Cloneable {
 
         _height = height;
         _size = size;
-        _rootRef = new AtomicReference<Node>(partial);
+        _rootRef = new COWMgr(partial);
+    }
+
+    public SnapFixedList<E> clone() {
+        final SnapFixedList<E> copy;
+        try {
+            copy = (SnapFixedList<E>) super.clone();
+        }
+        catch (final CloneNotSupportedException xx) {
+            throw new Error("unexpected", xx);
+        }
+        // height and size are done properly by the magic Cloneable.clone()
+        copy._rootRef = new COWMgr(_rootRef.frozen());
+        return copy;
     }
 
     @Override
@@ -176,7 +164,7 @@ public class SnapFixedList<E> extends AbstractList<E> implements Cloneable {
             throw new IndexOutOfBoundsException();
         }
 
-        Node cur = _rootRef.get();
+        Node cur = _rootRef.read();
         for (int h = _height - 1; h >= 1; --h) {
             cur = (Node) cur.get((index >> (LOG_BF * h)) & BF_MASK);
         }
@@ -189,66 +177,33 @@ public class SnapFixedList<E> extends AbstractList<E> implements Cloneable {
             throw new IndexOutOfBoundsException();
         }
 
-        while (true) {
-            final Node root = _rootRef.get();
-            final ClosableRefCount t0 = root.epoch.attemptIncr();
-            if (t0 != null) {
-                // entered the current epoch
-                try {
-                    return setImpl(root, index, newValue);
-                }
-                finally {
-                    t0.decr();
-                }
-            }
-
-            final ClosableRefCount t1 = root.epoch.queued.attemptIncr();
-            if (t1 != null) {
-                // Entered the queued epoch.  This guarantees us a seat at the
-                // next table.
-                try {
-                    // This epoch is either closing or closed.  Wait for the latter.
-                    root.epoch.awaitClosed();
-
-                    Node newRoot = _rootRef.get();
-                    if (newRoot == root) {
-                        // no one has yet installed a new root, try to do it
-                        final Node repl = new Node(root.epoch.queued, root);
-                        newRoot = _rootRef.get();
-                        if (newRoot == root) {
-                            _rootRef.compareAndSet(root, repl);
-                            newRoot = _rootRef.get();
-                        }
-                    }
-
-                    assert(newRoot != root && newRoot.epoch == root.epoch.queued);
-
-                    return setImpl(newRoot, index, newValue);
-                }
-                finally {
-                    t1.decr();
-                }
-            }
-
-            // our read of root must be stale, try again
-            assert(_rootRef.get() != root);
+        final Epoch.Ticket ticket = _rootRef.beginMutation();
+        try {
+            return setImpl(_rootRef.mutable(), index, newValue);
+        }
+        finally {
+            ticket.leave(0);
         }
     }
 
     @SuppressWarnings("unchecked")
     private E setImpl(final Node root, final int index, final E newValue) {
-        final Epoch epoch = root.epoch;
+        return (E) mutableLeaf(root, index).getAndSet(index & BF_MASK, newValue);
+    }
+
+    private Node mutableLeaf(final Node root, final int index) {
+        final Generation gen = root.gen;
         Node cur = root;
         for (int h = _height - 1; h >= 1; --h) {
             final int i = (index >> (LOG_BF * h)) & BF_MASK;
             final Node child = (Node) cur.get(i);
-            if (child.epoch == epoch) {
+            if (child.gen == gen) {
                 // easy case
                 cur = child;
                 continue;
             }
 
-            final Node repl = new Node(epoch, child);
+            final Node repl = new Node(gen, child);
 
             // reread before CAS
             Node newChild = (Node) cur.get(i);
@@ -256,9 +211,9 @@ public class SnapFixedList<E> extends AbstractList<E> implements Cloneable {
                 cur.compareAndSet(i, child, repl);
                 newChild = (Node) cur.get(i);
             }
-            assert(newChild.epoch == epoch);
+            assert(newChild.gen == gen);
             cur = newChild;
         }
-        return (E) cur.get(index & BF_MASK);
+        return cur;
     }
 }
