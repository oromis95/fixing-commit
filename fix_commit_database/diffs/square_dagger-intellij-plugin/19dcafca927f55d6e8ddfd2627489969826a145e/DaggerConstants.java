@@ -6,6 +6,9 @@ public interface DaggerConstants {
   String CLASS_LAZY = "dagger.Lazy";
   String CLASS_PROVIDER = "javax.inject.Provider";
   String CLASS_QUALIFIER = "javax.inject.Qualifier";
+  String ATTRIBUTE_TYPE = "type";
+  String SET_TYPE = "SET";
+  String MAP_TYPE = "MAP";
 
   int MAX_USAGES = 100;
 }
