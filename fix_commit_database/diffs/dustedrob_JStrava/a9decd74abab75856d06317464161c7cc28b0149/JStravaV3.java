@@ -27,7 +27,7 @@ public class JStravaV3 implements JStrava {
 
 
     @Override
-    public Athlete findAthlete(Integer id) {
+    public Athlete findAthlete(int id) {
 
         String URL="https://www.strava.com/api/v3/athletes/"+id+"?access_token="+ accessToken;
         String result=getResult(URL);
@@ -39,7 +39,7 @@ public class JStravaV3 implements JStrava {
     }
 
     @Override
-    public List<SegmentEffort> findAthleteKOMs(Integer athleteId) {
+    public List<SegmentEffort> findAthleteKOMs(int athleteId) {
         String URL="https://www.strava.com/api/v3/athletes/"+athleteId+"/koms?access_token="+accessToken;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -49,7 +49,7 @@ public class JStravaV3 implements JStrava {
     }
 
     @Override
-    public List<SegmentEffort> findAthleteKOMs(Integer athleteId,Integer page,Integer per_page) {
+    public List<SegmentEffort> findAthleteKOMs(int athleteId,int page,int per_page) {
         String URL="https://www.strava.com/api/v3/athletes/"+athleteId+"/koms?access_token="+accessToken+"&page="+page+"&per_page="+per_page;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -60,7 +60,21 @@ public class JStravaV3 implements JStrava {
 
     @Override
     public List<Athlete> getCurrentAthleteFriends() {
-        String URL="https://www.strava.com/api/v3/athlete/clubs?access_token="+ accessToken;
+        String URL="https://www.strava.com/api/v3/athlete/friends?access_token="+ accessToken;
+        String result=getResult(URL);
+        Gson gson= new Gson();
+        Athlete[] athletesArray= gson.fromJson(result,Athlete[].class);
+
+
+        List<Athlete>athletes= Arrays.asList(athletesArray);
+
+
+        return athletes;
+    }
+
+    @Override
+    public List<Athlete> getCurrentAthleteFriends(int page, int per_page) {
+        String URL="https://www.strava.com/api/v3/athlete/friends?access_token="+ accessToken+"&page="+page+"&per_page="+per_page;
         String result=getResult(URL);
         Gson gson= new Gson();
         Athlete[] athletesArray= gson.fromJson(result,Athlete[].class);
@@ -72,8 +86,9 @@ public class JStravaV3 implements JStrava {
         return athletes;
     }
 
+
     @Override
-    public List<Athlete> findAthleteFriends(Integer id) {
+    public List<Athlete> findAthleteFriends(int id) {
         String URL="https://www.strava.com/api/v3/athletes/"+id+"/friends?access_token="+ accessToken;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -87,7 +102,7 @@ public class JStravaV3 implements JStrava {
     }
 
     @Override
-    public List<Athlete> findAthleteFriends(Integer id,Integer page, Integer per_page) {
+    public List<Athlete> findAthleteFriends(int id,int page, int per_page) {
         String URL="https://www.strava.com/api/v3/athletes/"+id+"/friends?access_token="+accessToken+"&page="+page+"&per_page="+per_page;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -114,9 +129,23 @@ public class JStravaV3 implements JStrava {
         return athletes;
     }
 
+    @Override
+    public List<Athlete> getCurrentAthleteFollowers(int page, int per_page) {
+        String URL="https://www.strava.com/api/v3/athlete/followers?access_token="+ accessToken+"&page="+page+"&per_page="+per_page;
+        String result=getResult(URL);
+        Gson gson= new Gson();
+        Athlete[] athletesArray= gson.fromJson(result,Athlete[].class);
+
+
+        List<Athlete>athletes= Arrays.asList(athletesArray);
+
+
+        return athletes;
+    }
+
 
     @Override
-    public List<Athlete> findAthleteFollowers(Integer id) {
+    public List<Athlete> findAthleteFollowers(int id) {
         String URL="https://www.strava.com/api/v3/athletes/"+id+"/followers?access_token="+ accessToken;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -130,7 +159,7 @@ public class JStravaV3 implements JStrava {
     }
 
     @Override
-    public List<Athlete> findAthleteFollowers(Integer id,Integer page, Integer per_page) {
+    public List<Athlete> findAthleteFollowers(int id,int page, int per_page) {
         String URL="https://www.strava.com/api/v3/athletes/"+id+"/followers?access_token="+ accessToken+"&page="+page+"&per_page="+per_page;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -145,7 +174,7 @@ public class JStravaV3 implements JStrava {
 
 
     @Override
-    public List<Athlete> findAthleteBothFollowing(Integer id) {
+    public List<Athlete> findAthleteBothFollowing(int id) {
         String URL="https://www.strava.com/api/v3/athletes/"+id+"/both-following?access_token="+ accessToken;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -159,7 +188,7 @@ public class JStravaV3 implements JStrava {
     }
 
     @Override
-    public List<Athlete> findAthleteBothFollowing(Integer id,Integer page, Integer per_page) {
+    public List<Athlete> findAthleteBothFollowing(int id,int page, int per_page) {
         String URL="https://www.strava.com/api/v3/athletes/"+id+"/both-following?access_token="+ accessToken+"&page="+page+"&per_page="+per_page;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -173,10 +202,9 @@ public class JStravaV3 implements JStrava {
     }
 
     @Override
-    public Activity findActivity(Integer id) {
+    public Activity findActivity(int id) {
         String URL="https://www.strava.com/api/v3/activities/"+id+"?access_token="+ accessToken;
         String result=getResult(URL);
-        System.out.println(result);
         Gson gson= new Gson();
         Activity activity= gson.fromJson(result,Activity.class);
 
@@ -184,10 +212,9 @@ public class JStravaV3 implements JStrava {
     }
 
     @Override
-    public Activity findActivity(Integer id,boolean include_all_efforts) {
+    public Activity findActivity(int id,boolean include_all_efforts) {
         String URL="https://www.strava.com/api/v3/activities/"+id+"?access_token="+ accessToken+"&include_all_efforts="+include_all_efforts;
         String result=getResult(URL);
-        System.out.println(result);
         Gson gson= new Gson();
         Activity activity= gson.fromJson(result,Activity.class);
 
@@ -205,7 +232,7 @@ public class JStravaV3 implements JStrava {
     }
 
     @Override
-    public List<Activity> getCurrentAthleteActivities(Integer page,Integer per_page) {
+    public List<Activity> getCurrentAthleteActivities(int page,int per_page) {
         String URL="https://www.strava.com/api/v3/athlete/activities?access_token="+accessToken+"&page="+page+"&per_page="+per_page;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -215,7 +242,7 @@ public class JStravaV3 implements JStrava {
     }
 
     @Override
-    public List<Activity> getCurrentAthleteActivitiesBeforeDate(Long before) {
+    public List<Activity> getCurrentAthleteActivitiesBeforeDate(long before) {
         String URL="https://www.strava.com/api/v3/athlete/activities?access_token="+accessToken+"&before="+before;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -225,7 +252,7 @@ public class JStravaV3 implements JStrava {
     }
 
     @Override
-    public List<Activity> getCurrentAthleteActivitiesAfterDate(Long after) {
+    public List<Activity> getCurrentAthleteActivitiesAfterDate(long after) {
         String URL="https://www.strava.com/api/v3/athlete/activities?access_token="+accessToken+"&after="+after;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -244,7 +271,7 @@ public class JStravaV3 implements JStrava {
         return currentFriendsActivities;
     }
 
-    public List<Activity> getCurrentFriendsActivities(Integer page, Integer per_page) {
+    public List<Activity> getCurrentFriendsActivities(int page, int per_page) {
         String URL="https://www.strava.com/api/v3/activities/following?access_token="+accessToken+"&page="+page+"&per_page="+per_page;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -255,7 +282,7 @@ public class JStravaV3 implements JStrava {
 
 
     @Override
-    public List<LapEffort> findActivityLaps(Integer activityId) {
+    public List<LapEffort> findActivityLaps(int activityId) {
         String URL="https://www.strava.com/api/v3/activities/"+activityId+"/laps"+"?access_token="+accessToken;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -266,7 +293,7 @@ public class JStravaV3 implements JStrava {
 
 
     @Override
-    public List<Comment> findActivityComments(Integer activityId) {
+    public List<Comment> findActivityComments(int activityId) {
 
         String URL="https://www.strava.com/api/v3/activities/"+activityId+"/comments?access_token="+ accessToken;
         String result=getResult(URL);
@@ -281,7 +308,7 @@ public class JStravaV3 implements JStrava {
     }
 
     @Override
-    public List<Comment> findActivityComments(Integer activityId,boolean markdown, Integer page, Integer per_page) {
+    public List<Comment> findActivityComments(int activityId,boolean markdown, int page, int per_page) {
 
         String URL="https://www.strava.com/api/v3/activities/"+activityId+"/comments?access_token="+ accessToken+"&markdown="+markdown+"&page="+page+"&per_page="+per_page;
         String result=getResult(URL);
@@ -297,7 +324,7 @@ public class JStravaV3 implements JStrava {
 
 
     @Override
-    public List<Athlete> findActivityKudos(Integer activityId) {
+    public List<Athlete> findActivityKudos(int activityId) {
         String URL="https://www.strava.com/api/v3/activities/"+activityId+"/kudos?access_token="+ accessToken;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -311,7 +338,7 @@ public class JStravaV3 implements JStrava {
     }
 
     @Override
-    public List<Athlete> findActivityKudos(Integer activityId, Integer page, Integer per_page) {
+    public List<Athlete> findActivityKudos(int activityId, int page, int per_page) {
         String URL="https://www.strava.com/api/v3/activities/"+activityId+"/kudos?access_token="+ accessToken+"&page="+page+"&per_page="+per_page;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -326,7 +353,7 @@ public class JStravaV3 implements JStrava {
 
 
     @Override
-    public List<Athlete> findClubMembers(Integer clubId) {
+    public List<Athlete> findClubMembers(int clubId) {
         String URL="https://www.strava.com/api/v3/clubs/"+clubId+"/members?access_token="+ accessToken;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -340,7 +367,7 @@ public class JStravaV3 implements JStrava {
     }
 
     @Override
-    public List<Athlete> findClubMembers(Integer clubId,Integer page, Integer per_page) {
+    public List<Athlete> findClubMembers(int clubId,int page, int per_page) {
         String URL="https://www.strava.com/api/v3/clubs/"+clubId+"/members?access_token="+ accessToken+"&page="+page+"&per_page="+per_page;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -355,7 +382,7 @@ public class JStravaV3 implements JStrava {
 
 
     @Override
-    public List<Activity> findClubActivities(Integer clubId) {
+    public List<Activity> findClubActivities(int clubId) {
         String URL="https://www.strava.com/api/v3/clubs/"+clubId+"/activities"+"?access_token="+accessToken;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -366,7 +393,7 @@ public class JStravaV3 implements JStrava {
 
 
     @Override
-    public List<Activity> findClubActivities(Integer clubId,Integer page, Integer per_page) {
+    public List<Activity> findClubActivities(int clubId,int page, int per_page) {
         String URL="https://www.strava.com/api/v3/clubs/"+clubId+"/activities"+"?access_token="+accessToken+"&page="+page+"&per_page="+per_page;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -376,7 +403,7 @@ public class JStravaV3 implements JStrava {
     }
 
     @Override
-    public Club findClub(Integer id) {
+    public Club findClub(int id) {
         String URL="https://www.strava.com/api/v3/clubs/"+id+"?access_token="+ accessToken;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -412,7 +439,7 @@ public class JStravaV3 implements JStrava {
     }
 
     @Override
-    public Segment findSegment(Long segmentId) {
+    public Segment findSegment(long segmentId) {
         String URL="https://www.strava.com/api/v3/segments/"+segmentId+"?access_token="+accessToken;
         String result=getResult(URL);
         Gson gson= new Gson();
@@ -436,69 +463,100 @@ public class JStravaV3 implements JStrava {
 
 
 
-    public String getAccessToken() {
-        return accessToken;
-    }
 
-    public void setAccessToken(String accessToken) {
-        this.accessToken = accessToken;
-    }
 
+    @Override
+    public List<Photo> findActivityPhotos(int activityId) {
 
+        String URL="https://www.strava.com/api/v3/activities/"+activityId+"/photos?access_token="+ accessToken;
+        String result=getResult(URL);
+        Gson gson= new Gson();
+        Photo[] photosArray =gson.fromJson(result,Photo[].class);
+        List<Photo>photos= Arrays.asList(photosArray);
+        return photos;
+    }
 
     @Override
-    public SegmentEffort findSegmentEffort(Integer id) {
-        String URL="https://www.strava.com/api/v3/segment_efforts/"+id+"?access_token="+accessToken;
+    public SegmentLeaderBoard findSegmentLeaderBoard(long segmentId) {
+        String URL="https://www.strava.com/api/v3/segments/"+segmentId+"/leaderboard?access_token="+ accessToken;
         String result=getResult(URL);
         Gson gson= new Gson();
-        SegmentEffort segmentEffort= gson.fromJson(result,SegmentEffort.class);
-        return segmentEffort;
+        SegmentLeaderBoard segmentLeaderBoard= gson.fromJson(result,SegmentLeaderBoard.class);
+        return segmentLeaderBoard;
     }
 
+    @Override
+    public SegmentLeaderBoard findSegmentLeaderBoard(long segmentId, HashMap optionalParameters) {
+        String URL="https://www.strava.com/api/v3/segments/"+segmentId+"/leaderboard?access_token="+ accessToken;
+        String result=getResult(URL,optionalParameters);
+        Gson gson= new Gson();
+        SegmentLeaderBoard segmentLeaderBoard= gson.fromJson(result,SegmentLeaderBoard.class);
+        return segmentLeaderBoard;
+    }
+
+
+
 
 
     @Override
     public List<Segment> findSegments(Bound bound) {
-        return null;
-    }
+        String URL="https://www.strava.com/api/v3/segments/explore?access_token="+ accessToken+"&bounds="+bound.toString();
+        String result=getResult(URL);
 
-    @Override
-    public List<Segment> findSegments(Bound bound, String activityType, String minCat, String maxCat) {
-        return null;
-    }
 
+        //////////UGLY HACK TO ALLOW GSON TO PARSE THE JSON STRING AND RETURN A LIST OF SEGMENTS
 
+        String segmentString="\\{\"segments\":";
+        if (result.contains(segmentString))
+        {
+            result=result.replaceFirst(segmentString, "");
+            result=result.substring(0,result.lastIndexOf("}"));
+        }
 
-    @Override
-    public List<Photo> findActivityPhotos(Integer activityId) {
 
-        String URL="https://www.strava.com/api/v3/activities/"+activityId+"/photos?access_token="+ accessToken;
-        String result=getResult(URL);
         Gson gson= new Gson();
-        Photo[] photosArray =gson.fromJson(result,Photo[].class);
-        List<Photo>photos= Arrays.asList(photosArray);
-        return photos;
+        Segment[]segmentsArray= gson.fromJson(result,Segment[].class);
+        List<Segment> segments=Arrays.asList(segmentsArray);
+        return segments;
     }
 
     @Override
-    public SegmentLeaderBoard findSegmentLeaderBoard(Long segmentId) {
-        String URL="https://www.strava.com/api/v3/segments/"+segmentId+"/leaderboard?access_token="+ accessToken;
-        String result=getResult(URL);
+    public List<Segment> findSegments(Bound bound, HashMap optionalParameters) {
+        String URL="https://www.strava.com/api/v3/segments/explore?access_token="+ accessToken+"&bounds="+bound.toString();
+        String result=getResult(URL,optionalParameters);
+
+
+        //////////UGLY HACK TO ALLOW GSON TO PARSE THE JSON STRING AND RETURN A LIST OF SEGMENTS
+
+        String segmentString="\\{\"segments\":";
+        if (result.contains(segmentString))
+        {
+            result=result.replaceFirst(segmentString, "");
+            result=result.substring(0,result.lastIndexOf("}"));
+        }
+
+
         Gson gson= new Gson();
-        SegmentLeaderBoard segmentLeaderBoard= gson.fromJson(result,SegmentLeaderBoard.class);
-        return segmentLeaderBoard;
+        Segment[]segmentsArray= gson.fromJson(result,Segment[].class);
+        List<Segment> segments=Arrays.asList(segmentsArray);
+        return segments;
     }
 
+
+
+
     @Override
-    public SegmentLeaderBoard findSegmentLeaderBoard(Long segmentId, HashMap optionalParameters) {
-        String URL="https://www.strava.com/api/v3/segments/"+segmentId+"/leaderboard?access_token="+ accessToken;
-        String result=getResult(URL,optionalParameters);
+    public SegmentEffort findSegmentEffort(int id) {
+        String URL="https://www.strava.com/api/v3/segment_efforts/"+id+"?access_token="+accessToken;
+        String result=getResult(URL);
         Gson gson= new Gson();
-        SegmentLeaderBoard segmentLeaderBoard= gson.fromJson(result,SegmentLeaderBoard.class);
-        return segmentLeaderBoard;
+        SegmentEffort segmentEffort= gson.fromJson(result,SegmentEffort.class);
+        return segmentEffort;
     }
 
 
+
+
     public JStravaV3(String access_token){
         this.accessToken = access_token;
         String URL="https://www.strava.com/api/v3/athlete"+"?access_token="+accessToken;
@@ -546,6 +604,16 @@ public class JStravaV3 implements JStrava {
 
     }
 
+
+
+    public String getAccessToken() {
+        return accessToken;
+    }
+
+    public void setAccessToken(String accessToken) {
+        this.accessToken = accessToken;
+    }
+
     private String getResult(String URL, HashMap optionalParameters){
         StringBuilder sb= new StringBuilder();
         sb.append(URL);
