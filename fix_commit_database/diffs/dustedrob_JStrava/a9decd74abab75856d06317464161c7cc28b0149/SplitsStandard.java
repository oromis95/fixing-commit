@@ -5,11 +5,11 @@ package org.jstrava.entities;
  */
 public class SplitsStandard {
 
-    private Float distance;
-    private Integer elapsed_time;
-    private Float elevation_difference;
-    private Integer moving_time;
-    private Integer split;
+    private float distance;
+    private int elapsed_time;
+    private float elevation_difference;
+    private int moving_time;
+    private int split;
 
     public SplitsStandard() {
 
@@ -18,46 +18,46 @@ public class SplitsStandard {
 
     @Override
     public String toString() {
-        return split.toString();
+        return ""+split;
     }
 
-    public Float getDistance() {
+    public float getDistance() {
         return distance;
     }
 
-    public void setDistance(Float distance) {
+    public void setDistance(float distance) {
         this.distance = distance;
     }
 
-    public Integer getElapsed_time() {
+    public int getElapsed_time() {
         return elapsed_time;
     }
 
-    public void setElapsed_time(Integer elapsed_time) {
+    public void setElapsed_time(int elapsed_time) {
         this.elapsed_time = elapsed_time;
     }
 
-    public Float getElevation_difference() {
+    public float getElevation_difference() {
         return elevation_difference;
     }
 
-    public void setElevation_difference(Float elevation_difference) {
+    public void setElevation_difference(float elevation_difference) {
         this.elevation_difference = elevation_difference;
     }
 
-    public Integer getMoving_time() {
+    public int getMoving_time() {
         return moving_time;
     }
 
-    public void setMoving_time(Integer moving_time) {
+    public void setMoving_time(int moving_time) {
         this.moving_time = moving_time;
     }
 
-    public Integer getSplit() {
+    public int getSplit() {
         return split;
     }
 
-    public void setSplit(Integer split) {
+    public void setSplit(int split) {
         this.split = split;
     }
 }
