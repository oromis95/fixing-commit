@@ -6,28 +6,28 @@ package org.jstrava.entities;
 public class LapEffort {
 
 
-    private Long id;
-    private Integer resource_state;
+    private long id;
+    private int resource_state;
     private String name;
     private Activity activity;
     private Athlete athlete;
-    private Integer elapsed_time;
-    private Integer moving_time;
+    private int elapsed_time;
+    private int moving_time;
     private String start_date;
     private String start_date_local;
-    private Float distance;
-    private Integer start_index;
-    private Integer end_index;
-    private Float total_elevation_gain;
-    private Float average_speed;
-    private Float max_speed;
-    private Float average_cadence;
-    private Float average_watts;
-    private Float average_heartrate;
-    private Float max_heartrate;
-    private Integer lap_index;
-
-    public LapEffort(Long id) {
+    private float distance;
+    private int start_index;
+    private int end_index;
+    private float total_elevation_gain;
+    private float average_speed;
+    private float max_speed;
+    private float average_cadence;
+    private float average_watts;
+    private float average_heartrate;
+    private float max_heartrate;
+    private int lap_index;
+
+    public LapEffort(long id) {
         this.id = id;
     }
 
@@ -39,19 +39,19 @@ public class LapEffort {
         return name ;
     }
 
-    public Long getId() {
+    public long getId() {
         return id;
     }
 
-    public void setId(Long id) {
+    public void setId(long id) {
         this.id = id;
     }
 
-    public Integer getResource_state() {
+    public int getResource_state() {
         return resource_state;
     }
 
-    public void setResource_state(Integer resource_state) {
+    public void setResource_state(int resource_state) {
         this.resource_state = resource_state;
     }
 
@@ -79,19 +79,19 @@ public class LapEffort {
         this.athlete = athlete;
     }
 
-    public Integer getMoving_time() {
+    public int getMoving_time() {
         return moving_time;
     }
 
-    public void setMoving_time(Integer moving_time) {
+    public void setMoving_time(int moving_time) {
         this.moving_time = moving_time;
     }
 
-    public Integer getElapsed_time() {
+    public int getElapsed_time() {
         return elapsed_time;
     }
 
-    public void setElapsed_time(Integer elapsed_time) {
+    public void setElapsed_time(int elapsed_time) {
         this.elapsed_time = elapsed_time;
     }
 
@@ -111,91 +111,91 @@ public class LapEffort {
         this.start_date_local = start_date_local;
     }
 
-    public Float getDistance() {
+    public float getDistance() {
         return distance;
     }
 
-    public void setDistance(Float distance) {
+    public void setDistance(float distance) {
         this.distance = distance;
     }
 
-    public Integer getStart_index() {
+    public int getStart_index() {
         return start_index;
     }
 
-    public void setStart_index(Integer start_index) {
+    public void setStart_index(int start_index) {
         this.start_index = start_index;
     }
 
-    public Integer getEnd_index() {
+    public int getEnd_index() {
         return end_index;
     }
 
-    public void setEnd_index(Integer end_index) {
+    public void setEnd_index(int end_index) {
         this.end_index = end_index;
     }
 
-    public Float getTotal_elevation_gain() {
+    public float getTotal_elevation_gain() {
         return total_elevation_gain;
     }
 
-    public void setTotal_elevation_gain(Float total_elevation_gain) {
+    public void setTotal_elevation_gain(float total_elevation_gain) {
         this.total_elevation_gain = total_elevation_gain;
     }
 
-    public Float getAverage_speed() {
+    public float getAverage_speed() {
         return average_speed;
     }
 
-    public void setAverage_speed(Float average_speed) {
+    public void setAverage_speed(float average_speed) {
         this.average_speed = average_speed;
     }
 
-    public Float getMax_speed() {
+    public float getMax_speed() {
         return max_speed;
     }
 
-    public void setMax_speed(Float max_speed) {
+    public void setMax_speed(float max_speed) {
         this.max_speed = max_speed;
     }
 
-    public Float getAverage_cadence() {
+    public float getAverage_cadence() {
         return average_cadence;
     }
 
-    public void setAverage_cadence(Float average_cadence) {
+    public void setAverage_cadence(float average_cadence) {
         this.average_cadence = average_cadence;
     }
 
-    public Float getAverage_watts() {
+    public float getAverage_watts() {
         return average_watts;
     }
 
-    public void setAverage_watts(Float average_watts) {
+    public void setAverage_watts(float average_watts) {
         this.average_watts = average_watts;
     }
 
-    public Float getAverage_heartrate() {
+    public float getAverage_heartrate() {
         return average_heartrate;
     }
 
-    public void setAverage_heartrate(Float average_heartrate) {
+    public void setAverage_heartrate(float average_heartrate) {
         this.average_heartrate = average_heartrate;
     }
 
-    public Float getMax_heartrate() {
+    public float getMax_heartrate() {
         return max_heartrate;
     }
 
-    public void setMax_heartrate(Float max_heartrate) {
+    public void setMax_heartrate(float max_heartrate) {
         this.max_heartrate = max_heartrate;
     }
 
-    public Integer getLap_index() {
+    public int getLap_index() {
         return lap_index;
     }
 
-    public void setLap_index(Integer lap_index) {
+    public void setLap_index(int lap_index) {
         this.lap_index = lap_index;
     }
 }
