@@ -10,6 +10,8 @@ import java.io.InputStreamReader;
 import java.net.HttpURLConnection;
 import java.net.URL;
 import java.util.Arrays;
+import java.util.HashMap;
+import java.util.Iterator;
 import java.util.List;
 
 public class JStravaV3 implements JStrava {
@@ -110,6 +112,7 @@ public class JStravaV3 implements JStrava {
     public Activity findActivity(Integer id) {
         String URL="https://www.strava.com/api/v3/activities/"+id+"?access_token="+ accessToken;
         String result=getResult(URL);
+        System.out.println(result);
         Gson gson= new Gson();
         Activity activity= gson.fromJson(result,Activity.class);
 
@@ -159,6 +162,22 @@ public class JStravaV3 implements JStrava {
         return athletes;
     }
 
+    @Override
+    public List<Athlete> findAthleteFriends(Integer id,HashMap optionalParameters) {
+        String URL="https://www.strava.com/api/v3/athletes/"+id+"/friends?access_token="+accessToken;
+        String result=getResult(URL,optionalParameters);
+        Gson gson= new Gson();
+        Athlete[] athletesArray= gson.fromJson(result,Athlete[].class);
+
+
+        List<Athlete>athletes= Arrays.asList(athletesArray);
+
+
+        return athletes;
+    }
+
+
+
     @Override
     public List<Athlete> findAthleteFollowers(Integer id) {
         String URL="https://www.strava.com/api/v3/athletes/"+id+"/followers?access_token="+ accessToken;
@@ -242,6 +261,16 @@ public class JStravaV3 implements JStrava {
         return segmentEffort;
     }
 
+    @Override
+    public List<SegmentEffort> findAthleteKOMs(Integer athleteId) {
+        String URL="https://www.strava.com/api/v3/athletes/"+athleteId+"/koms?access_token="+accessToken;
+        String result=getResult(URL);
+        Gson gson= new Gson();
+        SegmentEffort[] segmentEffortArray=gson.fromJson(result,SegmentEffort[].class);
+        List<SegmentEffort>segmentEfforts= Arrays.asList(segmentEffortArray);
+        return segmentEfforts;
+    }
+
 
     public JStravaV3(String access_token){
         this.accessToken = access_token;
@@ -255,6 +284,8 @@ public class JStravaV3 implements JStrava {
 
 
 
+
+
     private String getResult(String URL){
         StringBuilder sb= new StringBuilder();
 
@@ -287,4 +318,56 @@ public class JStravaV3 implements JStrava {
         return sb.toString();
 
     }
+
+    private String getResult(String URL, HashMap optionalParameters){
+        StringBuilder sb= new StringBuilder();
+        sb.append(URL);
+        try {
+
+            Iterator iterator= optionalParameters.keySet().iterator();
+
+            while(iterator.hasNext())
+            {
+                sb.append("&");
+                String key=(String)iterator.next();
+                sb.append(key);
+                sb.append("=");
+                sb.append(optionalParameters.get(key));
+            }
+
+            URL url = new URL(sb.toString());
+
+            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
+
+            conn.setRequestMethod("GET");
+            conn.setRequestProperty("Accept", "application/json");
+
+            if (conn.getResponseCode() != 200) {
+                throw new RuntimeException("Failed : HTTP error code : "
+                        + conn.getResponseCode());
+            }
+
+
+            BufferedReader br = new BufferedReader(new InputStreamReader(
+                    (conn.getInputStream())));
+
+            String output;
+            sb=new StringBuilder();
+            while ((output = br.readLine()) != null) {
+                sb.append(output);
+            }
+
+            conn.disconnect();
+
+        } catch (IOException e) {
+
+            e.printStackTrace();
+            return null;
+        }
+        return sb.toString();
+
+    }
+
+
+
 }
