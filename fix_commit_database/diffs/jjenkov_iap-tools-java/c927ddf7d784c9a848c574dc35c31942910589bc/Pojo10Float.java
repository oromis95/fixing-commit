@@ -6,7 +6,7 @@ package com.jenkov.iap.ion.pojos;
 public class Pojo10Float {
     public float field0 = 1.1F;
     public float field1 = 12.12F;
-    public float field2 = 123.123F;
+    public float field2 = 123.132F;
     public float field3 = 1234.1234F;
     public float field4 = 12345.12345F;
     public float field5 = -1.1F;
