@@ -96,15 +96,7 @@ public class IonUtil {
     }
 
 
-    //todo remove this ?
-    /*
     public static IIonFieldReader createFieldReader(Field field){
-        return createFieldReader(field, null);
-    }
-    */
-
-    public static IIonFieldReader createFieldReader(Field field, IIonObjectReaderConfigurator configurator){
-
         field.setAccessible(true); //allows access to private fields, and supposedly speeds up reflection...  ?
         Class fieldType = field.getType();
 
@@ -154,9 +146,9 @@ public class IonUtil {
             if(double.class.equals(fieldType.getComponentType())){
                 return new IonFieldReaderArrayDouble(field);
             }
-            return new IonFieldReaderTable(field, configurator);
+            return new IonFieldReaderTable(field);
         } else {
-            return new IonFieldReaderObject(field, configurator);
+            return new IonFieldReaderObject(field);
         }
 
         //todo support object field writer
