@@ -0,0 +1,10 @@
+package com.jenkov.iap.ion.types;
+
+/**
+ * Created by jjenkov on 12-04-2016.
+ */
+public class Key {
+    public byte[] source = null;
+    public int    offset = 0;
+    public int    length = 0;
+}
