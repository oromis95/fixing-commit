@@ -5,17 +5,10 @@ package com.jenkov.iap;
  */
 public class TestPojo {
 
-    public boolean boolean1 = true;     //11
-    public boolean boolean2 = false;    //11
-
-    public short short1 = 12;           //11
-    public int   int1   = 13;           // 9
-    public long  long1  = 14;           //10
-
-    public float  float1  = 14.999F;     //14 (?)
-    public double dbl1    = 9999.9999D;  //16 (?)
-
-    public String str1    = "1234567812345678"; // 24
-
+    public boolean field0 = true;
+    public long    field1 = 1234;
+    public float   field2 = 123.12F;
+    public double  field3 = 123456.1234D;
+    public String  field4 = "abcdefg";
 
 }
