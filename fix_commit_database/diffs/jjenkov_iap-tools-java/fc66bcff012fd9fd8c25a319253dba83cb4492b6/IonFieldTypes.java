@@ -6,27 +6,25 @@ package com.jenkov.iap;
 public class IonFieldTypes {
 
     /* Field type constants */
-    public static final int BYTES         =  0;  //a series of raw bytes
-    public static final int BOOLEAN       =  1;
-    public static final int INT_POS       =  2;
-    public static final int INT_NEG       =  3;
-    public static final int FLOAT         =  4;
-    public static final int UTF_8         =  5;
-
-    public static final int RESERVED_1    =  6;
-    public static final int RESERVED_2    =  7;
-
-    public static final int OBJECT        =  8;
-    public static final int TABLE         =  9;
-    public static final int ARRAY         = 10;
-
-    public static final int COMPLEX_TYPE_ID = 11; //the type of an object - reserved for special IAP object types - none so far.
-
-    public static final int KEY           = 12;   //a sequence of bytes identifying a key or a property name - often UTF-8 encoded field names.
-    public static final int KEY_COMPACT   = 13;   //a sequence of bytes identifying a key or a property name - often UTF-8 encoded field names - 15 bytes or less.
-
-    public static final int REFERENCE     = 14;   //a reference to a field stored in the IAP connection cache.
-    public static final int EXTENDED_TYPE = 15;   //a sequence of bytes identifying a key or a property name - often UTF-8 encoded field names - 15 bytes or less.
+    public static final int BYTES           =  0;  //a series of raw bytes
+    public static final int BOOLEAN         =  1;
+    public static final int INT_POS         =  2;
+    public static final int INT_NEG         =  3;
+    public static final int FLOAT           =  4;
+    public static final int UTF_8           =  5;
+
+    public static final int RESERVED_1      =  6;
+    public static final int RESERVED_2      =  7;
+    public static final int REFERENCE       =  8;   //a reference to a field stored in the IAP connection cache.
+
+    public static final int OBJECT          =  9;
+    public static final int TABLE           = 10;
+    public static final int ARRAY           = 11;
+    public static final int COMPLEX_TYPE_ID = 12;   //the type of an object - reserved for special IAP object types - none so far.
+    public static final int KEY             = 13;   //a sequence of bytes identifying a key or a property name - often UTF-8 encoded field names.
+    public static final int KEY_COMPACT     = 14;   //a sequence of bytes identifying a key or a property name - often UTF-8 encoded field names - 15 bytes or less.
+
+    public static final int EXTENDED_TYPE   = 15;   //a sequence of bytes identifying a key or a property name - often UTF-8 encoded field names - 15 bytes or less.
 
 
     /*
