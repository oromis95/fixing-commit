@@ -1,5 +1,6 @@
 package com.jenkov.iap.write;
 
+import com.jenkov.iap.IonFieldTypes;
 import com.jenkov.iap.TestPojo;
 import org.junit.Test;
 
@@ -10,8 +11,7 @@ import static org.junit.Assert.assertEquals;
  */
 public class IonObjectWriterTest {
 
-    /*
-    IapTypedObjectWriter writer = new IapTypedObjectWriter(TestPojo.class);
+    IonObjectWriter writer = new IonObjectWriter(TestPojo.class);
 
     @Test
     public void test() {
@@ -23,68 +23,85 @@ public class IonObjectWriterTest {
 
         int index = 0;
 
-        assertEquals((IapFieldTypes.OBJECT << 3) | 2, 255 & dest[index++]);  //object field started - 194 = object field type << 3 | 2 (length length)
+        assertEquals((IonFieldTypes.OBJECT << 4) | 2, 255 & dest[index++]);  //object field started - 194 = object field type << 3 | 2 (length length)
         assertEquals(  0, 255 & dest[index++]);  //length of object - MSB
-        assertEquals( 97, 255 & dest[index++]);  //length of object - LSB
-
-        assertEquals((IapFieldTypes.KEY << 3) | 1, 255 & dest[index++]);   //lead byte of key field
-        assertEquals(  8, 255 & dest[index++]);   //length of key field
-        assertEquals( 98, 255 & dest[index++]);   //value of char 1 field
-        assertEquals(111, 255 & dest[index++]);   //value of char 2 field
-        assertEquals(111, 255 & dest[index++]);   //value of char 3 field
-        assertEquals(108, 255 & dest[index++]);   //value of char 4 field
-        assertEquals(101, 255 & dest[index++]);   //value of char 5 field
-        assertEquals( 97, 255 & dest[index++]);  //value of char 6 field
-        assertEquals(110, 255 & dest[index++]);  //value of char 7 field
-        assertEquals( 49, 255 & dest[index++]);  //value of char 8 field
-
-        assertEquals((IapFieldTypes.BOOLEAN << 3) | 1, 255 & dest[index++]);  //lead byte of boolean field
-
-        assertEquals((IapFieldTypes.KEY << 3) | 1, 255 & dest[index++]);   //lead byte of key field
-        assertEquals(  8, 255 & dest[index++]);   //length of key field
-        assertEquals( 98, 255 & dest[index++]);   //value of char 1 field
-        assertEquals(111, 255 & dest[index++]);   //value of char 2 field
-        assertEquals(111, 255 & dest[index++]);   //value of char 3 field
-        assertEquals(108, 255 & dest[index++]);   //value of char 4 field
-        assertEquals(101, 255 & dest[index++]);   //value of char 5 field
-        assertEquals( 97, 255 & dest[index++]);  //value of char 6 field
-        assertEquals(110, 255 & dest[index++]);  //value of char 7 field
-        assertEquals( 50, 255 & dest[index++]);  //value of char 8 field
-
-        assertEquals((IapFieldTypes.BOOLEAN << 3) | 2, 255 & dest[index++]);  //lead byte of boolean field
-
-        assertEquals((IapFieldTypes.KEY_COMPACT << 3) | 6, 255 & dest[index++]);  //lead byte of compact key field
-        assertEquals(115, 255 & dest[index++]);  //value of char 1 field
-        assertEquals(104, 255 & dest[index++]);  //value of char 2 field
-        assertEquals(111, 255 & dest[index++]);  //value of char 3 field
-        assertEquals(114, 255 & dest[index++]);  //value of char 4 field
-        assertEquals(116, 255 & dest[index++]);  //value of char 5 field
-        assertEquals( 49, 255 & dest[index++]);  //value of char 6 field
-
-        assertEquals((IapFieldTypes.SSHORT << 3) | 1, 255 & dest[index++]);  //lead byte of sshort field
-        assertEquals( 12, 255 & dest[index++]);  //value of char 1 field
-
-        assertEquals((IapFieldTypes.KEY_COMPACT << 3) | 4, 255 & dest[index++]);  //value of char 2 field
-        assertEquals(105, 255 & dest[index++]);  //value of char 3 field
-        assertEquals(110, 255 & dest[index++]);  //value of char 4 field
-        assertEquals(116, 255 & dest[index++]);  //value of char 5 field
-        assertEquals( 49, 255 & dest[index++]);  //value of char 5 field
-
-        assertEquals((IapFieldTypes.SINT << 3) | 1, 255 & dest[index++]);  //lead byte of sint field
-        assertEquals( 13, 255 & dest[index++]);  //value of byte 1
-
-        assertEquals((IapFieldTypes.KEY_COMPACT << 3) | 5, 255 & dest[index++]);  //value of char 2 field
-        assertEquals(108, 255 & dest[index++]);  //value of char 3 field
-        assertEquals(111, 255 & dest[index++]);  //value of char 4 field
-        assertEquals(110, 255 & dest[index++]);  //value of char 5 field
-        assertEquals(103, 255 & dest[index++]);  //value of char 5 field
-        assertEquals( 49, 255 & dest[index++]);  //value of char 5 field
-
-        assertEquals((IapFieldTypes.SLONG << 3) | 1, 255 & dest[index++]);  //lead byte of long field
-        assertEquals(  1, 255 & dest[index++]);  //length of long field
-        assertEquals( 14, 255 & dest[index++]);  //value of long field
+        assertEquals( 62, 255 & dest[index++]);  //length of object - LSB
+
+        assertEquals((IonFieldTypes.KEY_COMPACT << 4) | 6, 255 & dest[index++]);   //lead byte of key field
+        assertEquals('f', 255 & dest[index++]);   //value of char 1 field
+        assertEquals('i', 255 & dest[index++]);   //value of char 2 field
+        assertEquals('e', 255 & dest[index++]);   //value of char 3 field
+        assertEquals('l', 255 & dest[index++]);   //value of char 4 field
+        assertEquals('d', 255 & dest[index++]);   //value of char 5 field
+        assertEquals('0', 255 & dest[index++]);  //value of char 6 field
+
+        assertEquals((IonFieldTypes.BOOLEAN << 4) | 1, 255 & dest[index++]);  //lead byte of boolean field
+
+        assertEquals((IonFieldTypes.KEY_COMPACT << 4) | 6, 255 & dest[index++]);   //lead byte of key field
+        assertEquals('f', 255 & dest[index++]);   //value of char 1 field
+        assertEquals('i', 255 & dest[index++]);   //value of char 2 field
+        assertEquals('e', 255 & dest[index++]);   //value of char 3 field
+        assertEquals('l', 255 & dest[index++]);   //value of char 4 field
+        assertEquals('d', 255 & dest[index++]);   //value of char 5 field
+        assertEquals('1', 255 & dest[index++]);  //value of char 6 field
+
+        assertEquals((IonFieldTypes.INT_POS << 4) | 2, 255 & dest[index++]);  //lead byte of boolean field
+        assertEquals( 1234 >> 8 , 255 & dest[index++]);  //value of char 1 field
+        assertEquals( 1234 & 255, 255 & dest[index++]);  //value of char 1 field
+
+        assertEquals((IonFieldTypes.KEY_COMPACT << 4) | 6, 255 & dest[index++]);  //lead byte of compact key field
+        assertEquals('f', 255 & dest[index++]);   //value of char 1 field
+        assertEquals('i', 255 & dest[index++]);   //value of char 2 field
+        assertEquals('e', 255 & dest[index++]);   //value of char 3 field
+        assertEquals('l', 255 & dest[index++]);   //value of char 4 field
+        assertEquals('d', 255 & dest[index++]);   //value of char 5 field
+        assertEquals('2', 255 & dest[index++]);  //value of char 6 field
+
+        int floatBits = Float.floatToIntBits(123.12F);
+        assertEquals((IonFieldTypes.FLOAT << 4) | 4, 255 & dest[index++]);  //lead byte of sshort field
+        assertEquals( 255 & (floatBits >> 24), 255 & dest[index++]);  //value of char 1 field
+        assertEquals( 255 & (floatBits >> 16), 255 & dest[index++]);  //value of char 1 field
+        assertEquals( 255 & (floatBits >>  8), 255 & dest[index++]);  //value of char 1 field
+        assertEquals( 255 & (floatBits)      , 255 & dest[index++]);  //value of char 1 field
+
+        assertEquals((IonFieldTypes.KEY_COMPACT << 4) | 6, 255 & dest[index++]);  //value of char 2 field
+        assertEquals('f', 255 & dest[index++]);   //value of char 1 field
+        assertEquals('i', 255 & dest[index++]);   //value of char 2 field
+        assertEquals('e', 255 & dest[index++]);   //value of char 3 field
+        assertEquals('l', 255 & dest[index++]);   //value of char 4 field
+        assertEquals('d', 255 & dest[index++]);   //value of char 5 field
+        assertEquals('3', 255 & dest[index++]);  //value of char 6 field
+
+        long longBits = Double.doubleToLongBits(123456.1234D);
+        assertEquals((IonFieldTypes.FLOAT << 4) | 8, 255 & dest[index++]);  //lead byte of sint field
+        assertEquals( 255 & (longBits >> 56), 255 & dest[index++]);  //value of char 1 field
+        assertEquals( 255 & (longBits >> 48), 255 & dest[index++]);  //value of char 1 field
+        assertEquals( 255 & (longBits >> 40), 255 & dest[index++]);  //value of char 1 field
+        assertEquals( 255 & (longBits >> 32), 255 & dest[index++]);  //value of char 1 field
+        assertEquals( 255 & (longBits >> 24), 255 & dest[index++]);  //value of char 1 field
+        assertEquals( 255 & (longBits >> 16), 255 & dest[index++]);  //value of char 1 field
+        assertEquals( 255 & (longBits >>  8), 255 & dest[index++]);  //value of char 1 field
+        assertEquals( 255 & (longBits)      , 255 & dest[index++]);  //value of char 1 field
+
+        assertEquals((IonFieldTypes.KEY_COMPACT << 4) | 6, 255 & dest[index++]);  //value of char 2 field
+        assertEquals('f', 255 & dest[index++]);   //value of char 1 field
+        assertEquals('i', 255 & dest[index++]);   //value of char 2 field
+        assertEquals('e', 255 & dest[index++]);   //value of char 3 field
+        assertEquals('l', 255 & dest[index++]);   //value of char 4 field
+        assertEquals('d', 255 & dest[index++]);   //value of char 5 field
+        assertEquals('4', 255 & dest[index++]);  //value of char 6 field
+
+        assertEquals((IonFieldTypes.UTF_8 << 4) | 1, 255 & dest[index++]);  //lead byte of long field
+        assertEquals(  7, 255 & dest[index++]);  //length of long field
+        assertEquals('a', 255 & dest[index++]);  //value of long field
+        assertEquals('b', 255 & dest[index++]);  //value of long field
+        assertEquals('c', 255 & dest[index++]);  //value of long field
+        assertEquals('d', 255 & dest[index++]);  //value of long field
+        assertEquals('e', 255 & dest[index++]);  //value of long field
+        assertEquals('f', 255 & dest[index++]);  //value of long field
+        assertEquals('g', 255 & dest[index++]);  //value of long field
 
 
     }
-    */
+
 }
