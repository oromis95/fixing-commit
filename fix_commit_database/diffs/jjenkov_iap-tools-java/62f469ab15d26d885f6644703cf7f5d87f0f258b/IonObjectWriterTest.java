@@ -9,6 +9,7 @@ import java.util.Calendar;
 import java.util.TimeZone;
 
 import static org.junit.Assert.assertEquals;
+import static org.junit.Assert.assertNotNull;
 
 /**
  * Created by jjenkov on 04-11-2015.
@@ -608,14 +609,14 @@ public class IonObjectWriterTest {
 
 
     @Test
-    public void testFieldAliases() {
-        IonObjectWriter writer = new IonObjectWriter(SmallPojo.class, config -> {
-            if("field0".equals(config.fieldName)){
-                config.alias = "f0";
-            } else if("field1".equals(config.fieldName)){
-                config.alias = "f1";
-            } else if("field2".equals(config.fieldName)){
-                config.include = false;
+    public void testConfigurator() {
+        IonObjectWriter writer = new IonObjectWriter(SmallPojo.class, fieldConfig -> {
+            if("field0".equals(fieldConfig.fieldName)){
+                fieldConfig.alias = "f0";
+            } else if("field1".equals(fieldConfig.fieldName)){
+                fieldConfig.alias = "f1";
+            } else if("field2".equals(fieldConfig.fieldName)){
+                fieldConfig.include = false;
             }
         });
 
@@ -649,4 +650,140 @@ public class IonObjectWriterTest {
 
     }
 
+
+    @Test
+    public void testConfiguratorRecursively() {
+        IonObjectWriter writer = new IonObjectWriter(PojoWithPojo.class, fieldConfig -> {
+
+            assertNotNull(fieldConfig.field);
+
+            if(PojoWithPojo.class.equals(fieldConfig.field.getDeclaringClass())){
+                if("field0".equals(fieldConfig.fieldName)){
+                    fieldConfig.alias = "f0";
+                }
+            }
+            if(Pojo10Int.class.equals(fieldConfig.field.getDeclaringClass())){
+                if("field0".equals(fieldConfig.fieldName)){
+                    fieldConfig.alias = "f0";
+                } else if("field1".equals(fieldConfig.fieldName)){
+                    fieldConfig.alias = "f1";
+                } else if("field2".equals(fieldConfig.fieldName)){
+                    fieldConfig.include = false;
+                }
+            }
+        });
+
+        byte[] dest   = new byte[100 * 1024];
+
+        PojoWithPojo pojo = new PojoWithPojo();
+
+        int bytesWritten = writer.writeObject(pojo, 2, dest, 0);
+
+        System.out.println("bytesWritten = " + bytesWritten);
+
+        int index = 0;
+        assertEquals((IonFieldTypes.OBJECT << 4) | 2, 255 & dest[index++]);
+        assertEquals(   0, 255 & dest[index++]);
+        assertEquals(  79, 255 & dest[index++]);
+
+        assertEquals((IonFieldTypes.KEY_SHORT << 4) | 2, 255 & dest[index++]);
+        assertEquals('f', 255 & dest[index++]);
+        assertEquals('0', 255 & dest[index++]);
+
+        assertEquals((IonFieldTypes.OBJECT << 4) | 2, 255 & dest[index++]);
+        assertEquals(   0, 255 & dest[index++]);
+        assertEquals(  73, 255 & dest[index++]);
+
+        assertEquals((IonFieldTypes.KEY_SHORT << 4) | 2, 255 & dest[index++]);
+        assertEquals('f', 255 & dest[index++]);
+        assertEquals('0', 255 & dest[index++]);
+
+        assertEquals((IonFieldTypes.INT_POS << 4) | 1, 255 & dest[index++]);
+        assertEquals(  0, 255 & dest[index++]);
+
+        assertEquals((IonFieldTypes.KEY_SHORT << 4) | 2, 255 & dest[index++]);
+        assertEquals('f', 255 & dest[index++]);
+        assertEquals('1', 255 & dest[index++]);
+
+        assertEquals((IonFieldTypes.INT_POS << 4) | 1, 255 & dest[index++]);
+        assertEquals(  1, 255 & dest[index++]);
+
+        assertEquals((IonFieldTypes.KEY_SHORT << 4) | 6, 255 & dest[index++]);
+        assertEquals('f', 255 & dest[index++]);
+        assertEquals('i', 255 & dest[index++]);
+        assertEquals('e', 255 & dest[index++]);
+        assertEquals('l', 255 & dest[index++]);
+        assertEquals('d', 255 & dest[index++]);
+        assertEquals('3', 255 & dest[index++]);
+
+        assertEquals((IonFieldTypes.INT_POS << 4) | 1, 255 & dest[index++]);
+        assertEquals(  3, 255 & dest[index++]);
+
+        assertEquals((IonFieldTypes.KEY_SHORT << 4) | 6, 255 & dest[index++]);
+        assertEquals('f', 255 & dest[index++]);
+        assertEquals('i', 255 & dest[index++]);
+        assertEquals('e', 255 & dest[index++]);
+        assertEquals('l', 255 & dest[index++]);
+        assertEquals('d', 255 & dest[index++]);
+        assertEquals('4', 255 & dest[index++]);
+
+        assertEquals((IonFieldTypes.INT_POS << 4) | 1, 255 & dest[index++]);
+        assertEquals(  4, 255 & dest[index++]);
+
+        assertEquals((IonFieldTypes.KEY_SHORT << 4) | 6, 255 & dest[index++]);
+        assertEquals('f', 255 & dest[index++]);
+        assertEquals('i', 255 & dest[index++]);
+        assertEquals('e', 255 & dest[index++]);
+        assertEquals('l', 255 & dest[index++]);
+        assertEquals('d', 255 & dest[index++]);
+        assertEquals('5', 255 & dest[index++]);
+
+        assertEquals((IonFieldTypes.INT_POS << 4) | 1, 255 & dest[index++]);
+        assertEquals(  5, 255 & dest[index++]);
+
+        assertEquals((IonFieldTypes.KEY_SHORT << 4) | 6, 255 & dest[index++]);
+        assertEquals('f', 255 & dest[index++]);
+        assertEquals('i', 255 & dest[index++]);
+        assertEquals('e', 255 & dest[index++]);
+        assertEquals('l', 255 & dest[index++]);
+        assertEquals('d', 255 & dest[index++]);
+        assertEquals('6', 255 & dest[index++]);
+
+        assertEquals((IonFieldTypes.INT_POS << 4) | 1, 255 & dest[index++]);
+        assertEquals(  6, 255 & dest[index++]);
+
+        assertEquals((IonFieldTypes.KEY_SHORT << 4) | 6, 255 & dest[index++]);
+        assertEquals('f', 255 & dest[index++]);
+        assertEquals('i', 255 & dest[index++]);
+        assertEquals('e', 255 & dest[index++]);
+        assertEquals('l', 255 & dest[index++]);
+        assertEquals('d', 255 & dest[index++]);
+        assertEquals('7', 255 & dest[index++]);
+
+        assertEquals((IonFieldTypes.INT_POS << 4) | 1, 255 & dest[index++]);
+        assertEquals(  7, 255 & dest[index++]);
+
+        assertEquals((IonFieldTypes.KEY_SHORT << 4) | 6, 255 & dest[index++]);
+        assertEquals('f', 255 & dest[index++]);
+        assertEquals('i', 255 & dest[index++]);
+        assertEquals('e', 255 & dest[index++]);
+        assertEquals('l', 255 & dest[index++]);
+        assertEquals('d', 255 & dest[index++]);
+        assertEquals('8', 255 & dest[index++]);
+
+        assertEquals((IonFieldTypes.INT_POS << 4) | 1, 255 & dest[index++]);
+        assertEquals(  8, 255 & dest[index++]);
+
+        assertEquals((IonFieldTypes.KEY_SHORT << 4) | 6, 255 & dest[index++]);
+        assertEquals('f', 255 & dest[index++]);
+        assertEquals('i', 255 & dest[index++]);
+        assertEquals('e', 255 & dest[index++]);
+        assertEquals('l', 255 & dest[index++]);
+        assertEquals('d', 255 & dest[index++]);
+        assertEquals('9', 255 & dest[index++]);
+
+        assertEquals((IonFieldTypes.INT_POS << 4) | 1, 255 & dest[index++]);
+        assertEquals(  9, 255 & dest[index++]);
+    }
+
 }
