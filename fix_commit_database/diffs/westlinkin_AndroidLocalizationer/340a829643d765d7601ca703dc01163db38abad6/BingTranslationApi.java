@@ -16,21 +16,23 @@
 
 package language_engine.bing;
 
+import com.google.gson.JsonArray;
 import com.google.gson.JsonObject;
 import com.google.gson.JsonParser;
 import com.intellij.ide.util.PropertiesComponent;
-import com.intellij.openapi.util.io.StreamUtil;
 import data.Key;
 import data.Log;
 import data.StorageDataKey;
 import language_engine.HttpUtils;
 import module.SupportedLanguages;
+import org.apache.commons.lang.StringEscapeUtils;
 import org.apache.http.Header;
 import org.apache.http.NameValuePair;
 import org.apache.http.message.BasicHeader;
 import org.apache.http.message.BasicNameValuePair;
 
-import java.io.InputStream;
+import java.io.UnsupportedEncodingException;
+import java.net.URLEncoder;
 import java.util.ArrayList;
 import java.util.List;
 
@@ -38,9 +40,11 @@ import java.util.List;
  * Created by Wesley Lin on 12/3/14.
  */
 public class BingTranslationApi {
+    private static final String ENCODING = "UTF-8";
+
     private static final String AUTH_URL = "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13/";
 
-    private static final String TRANSLATE_URL = "http://api.microsofttranslator.com/V2/Http.svc/TranslateArray";
+    private static final String TRANSLATE_URL = "http://api.microsofttranslator.com/V2/Ajax.svc/TranslateArray?";
 
     private static List<NameValuePair> getAccessTokenNameValuePair() {
         PropertiesComponent propertiesComponent = PropertiesComponent.getInstance();
@@ -63,6 +67,90 @@ public class BingTranslationApi {
         return null;
     }
 
+    protected static final String PARAM_APP_ID = "appId=",
+            PARAM_TO_LANG = "&to=",
+            PARAM_FROM_LANG = "&from=",
+            PARAM_TEXT_ARRAY = "&texts=";
+
+    /**
+     * using AJAX api now: http://msdn.microsoft.com/en-us/library/ff512402.aspx
+     * @param accessToken
+     * @param querys
+     * @param from
+     * @param to
+     * @return list of String, which is the result
+     */
+    public static List<String> getTranslatedStringArrays2(String accessToken, List<String> querys, SupportedLanguages from, SupportedLanguages to) {
+//        Log.i(accessToken);
+        String url = generateUrl(accessToken, querys, from, to);
+        Header[] headers = new Header[]{
+                new BasicHeader("Authorization", "Bearer " + accessToken),
+                new BasicHeader("Content-Type", "text/plain; charset=" + ENCODING),
+                new BasicHeader("Accept-Charset", ENCODING)
+        };
+
+        String getResult = HttpUtils.doHttpGet(url, headers);
+//        Log.i(getResult);
+        JsonArray jsonArray = new JsonParser().parse(getResult).getAsJsonArray();
+        if (jsonArray == null)
+            return null;
+
+        List<String> result = new ArrayList<String>();
+        for (int i = 0; i < jsonArray.size(); i++) {
+            String translatedText = jsonArray.get(i).getAsJsonObject().get("TranslatedText").getAsString();
+            if (translatedText == null) {
+                result.add("");
+            } else {
+                result.add(StringEscapeUtils.unescapeJava(translatedText));
+            }
+        }
+        return result;
+    }
+
+    private static String generateUrl(String accessToken, List<String> querys, SupportedLanguages from, SupportedLanguages to) {
+        String[] texts = new String[]{};
+        texts = querys.toArray(texts);
+        for (int i = 0; i < texts.length; i++) {
+            texts[i] = StringEscapeUtils.escapeJava(texts[i]);
+        }
+
+        try {
+            final String params =
+                    (accessToken != null ? PARAM_APP_ID + URLEncoder.encode("Bearer " + accessToken, ENCODING) : "")
+                            + PARAM_FROM_LANG + URLEncoder.encode(from.getLanguageCode(), ENCODING)
+                            + PARAM_TO_LANG + URLEncoder.encode(to.getLanguageCode(), ENCODING)
+                            + PARAM_TEXT_ARRAY + URLEncoder.encode(buildStringArrayParam(texts), ENCODING);
+
+            return TRANSLATE_URL + params;
+        } catch (UnsupportedEncodingException e) {
+            e.printStackTrace();
+        }
+        return null;
+    }
+
+    private static String buildStringArrayParam(Object[] values) {
+        StringBuilder targetString = new StringBuilder("[\"");
+        String value;
+        for (Object obj : values) {
+            if (obj != null) {
+                value = obj.toString();
+                if (value.length() != 0) {
+                    if (targetString.length() > 2)
+                        targetString.append(",\"");
+                    targetString.append(value);
+                    targetString.append("\"");
+                }
+            }
+        }
+        targetString.append("]");
+        return targetString.toString();
+    }
+
+
+    /**
+     * @deprecated
+     * using @getTranslatedStringArrays2 now
+     */
     public static List<String> getTranslatedStringArrays(String accessToken, List<String> querys, SupportedLanguages from, SupportedLanguages to) {
         String xmlBodyTop = "<TranslateArrayRequest>\n" +
                 "  <AppId />\n" +
