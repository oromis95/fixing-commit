@@ -40,9 +40,13 @@ public class AndroidString {
         return value;
     }
 
+    public void setValue(String value) {
+        this.value = value;
+    }
+
     @Override
     public String toString() {
-        return "<string name=" +
+        return "<string name=\"" +
                 key +
                 "\">" +
                 value +
