@@ -43,7 +43,7 @@ public class ConvertToOtherLanguages extends AnAction implements MultiSelectDial
     private static final String LOCALIZATION_MSG = "Warning: " +
             "The string resources are translated by %s, " +
             "try keeping your string resources simple, so that the result is more satisfied.";
-    private static final String OVERRIDE_EXITS_STRINGS = "Override the exiting strings";
+    private static final String OVERRIDE_EXITS_STRINGS = "Override the existing strings";
 
     private Project project;
     private List<AndroidString> androidStringsInStringFile = null;
