@@ -31,12 +31,7 @@ public class EntityNMSArmorStand extends EntityArmorStand implements NMSArmorSta
 		setArms(false);
 		setGravity(true);
 		setBasePlate(true);
-		try {
-			ReflectionUtils.callPrivateMethod(EntityArmorStand.class, this, "n", new Class[]{ boolean.class }, new Object[]{ true }); // n() = setMarker()
-		} catch (Exception e) {
-			e.printStackTrace();
-			// It will still work.
-		}
+		// There is no "Marker" tag in v1_8_R1
 		this.parentPiece = parentPiece;
 		try {
 			ReflectionUtils.setPrivateField(EntityArmorStand.class, this, "bg", Integer.MAX_VALUE);
