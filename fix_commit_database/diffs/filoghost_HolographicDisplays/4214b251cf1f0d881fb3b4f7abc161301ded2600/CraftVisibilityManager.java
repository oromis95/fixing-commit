@@ -11,7 +11,6 @@ import org.bukkit.entity.Player;
 import com.gmail.filoghost.holographicdisplays.HolographicDisplays;
 import com.gmail.filoghost.holographicdisplays.api.VisibilityManager;
 import com.gmail.filoghost.holographicdisplays.util.Validator;
-import com.gmail.filoghost.holographicdisplays.util.VersionUtils;
 
 public class CraftVisibilityManager implements VisibilityManager {
 
@@ -39,7 +38,7 @@ public class CraftVisibilityManager implements VisibilityManager {
 			boolean oldVisibleByDefault = this.visibleByDefault;
 			this.visibleByDefault = visibleByDefault;
 			
-			for (Player player : VersionUtils.getOnlinePlayers()) {
+			for (Player player : Bukkit.getOnlinePlayers()) {
 				
 				if (playersVisibilityMap != null && playersVisibilityMap.containsKey(player.getName().toLowerCase())) {
 					// Has a specific value set
