@@ -37,13 +37,9 @@ public class EntityNMSItem extends EntityItem implements NMSItem {
 	
 	@Override
 	public void t_() {
-		// Checks every 20 ticks.
-		if (ticksLived % 20 == 0) {
-			// The item dies without a vehicle.
-			if (this.vehicle == null) {
-				die();
-			}
-		}
+		
+		// So it won't get removed.
+		ticksLived = 0;
 		
 		if (!lockTick) {
 			super.t_();
