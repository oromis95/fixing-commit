@@ -5,8 +5,6 @@ import org.bukkit.command.CommandSender;
 
 public class Strings {
 
-	public static final String NO_SUCH_HOLOGRAM = ChatColor.RED + "A hologram with that name doesn't exist.";
-
 	public static final String BASE_PERM = "holograms.";
 	
 	public static final String TIP_PREFIX = "" + ChatColor.YELLOW + ChatColor.BOLD + "TIP" + Colors.SECONDARY_SHADOW + " ";
@@ -16,6 +14,10 @@ public class Strings {
 		return "" + Colors.PRIMARY_SHADOW + ChatColor.BOLD + "----- " + input + Colors.PRIMARY_SHADOW + ChatColor.BOLD + " -----";
 	}
 	
+	public static String noSuchHologram(String name) {
+		return ChatColor.RED + "Cannot find a hologram named \"" + name + "\".";
+	}
+	
 	public static void sendWarning(CommandSender recipient, String warning) {
 		recipient.sendMessage(ChatColor.RED + "( " + ChatColor.DARK_RED + ChatColor.BOLD + "!" + ChatColor.RED + " ) " + Colors.SECONDARY_SHADOW + warning);
 	}
