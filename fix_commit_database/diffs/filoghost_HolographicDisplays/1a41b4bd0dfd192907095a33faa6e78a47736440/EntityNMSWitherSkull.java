@@ -71,7 +71,7 @@ public class EntityNMSWitherSkull extends EntityWitherSkull implements NMSWither
 	@Override
 	public int getId() {
 		StackTraceElement element = ReflectionUtils.getStackTraceElement(2);
-		if (element.getFileName().equals("EntityTrackerEntry.java") && element.getLineNumber() > 134 && element.getLineNumber() < 144) {
+		if (element != null && element.getFileName() != null && element.getFileName().equals("EntityTrackerEntry.java") && element.getLineNumber() > 134 && element.getLineNumber() < 144) {
 			// Then this method is being called when creating a new packet, we return a fake ID!
 			return -1;
 		}
