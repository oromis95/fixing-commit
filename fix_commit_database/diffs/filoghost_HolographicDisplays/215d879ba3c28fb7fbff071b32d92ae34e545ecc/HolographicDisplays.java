@@ -130,7 +130,7 @@ public class HolographicDisplays extends JavaPlugin {
 				"******************************************************",
 				"     This version of HolographicDisplays can",
 				"     only work on these server versions:",
-				"     1.6.4, from 1.7 to 1.8.3.",
+				"     from 1.6.4 to 1.8.3.",
 				"     The plugin will be disabled.",
 				"******************************************************"
 			);
