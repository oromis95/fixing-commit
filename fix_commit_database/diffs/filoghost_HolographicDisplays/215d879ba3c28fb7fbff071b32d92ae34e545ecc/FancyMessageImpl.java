@@ -9,7 +9,9 @@ import net.minecraft.server.v1_8_R2.IChatBaseComponent;
 import net.minecraft.server.v1_8_R2.PacketPlayOutChat;
 
 import org.bukkit.ChatColor;
-import org.bukkit.craftbukkit.libs.com.google.gson.stream.JsonWriter;
+
+import com.google.gson.stream.JsonWriter;
+
 import org.bukkit.craftbukkit.v1_8_R2.entity.CraftPlayer;
 import org.bukkit.entity.Player;
 
@@ -123,4 +125,46 @@ public class FancyMessageImpl implements FancyMessage {
 		latest.hoverActionData = data;
 	}
 	
+	static class MessagePart {
+
+		public ChatColor color = null;
+		public ChatColor[] styles = null;
+		public String clickActionName = null;
+		public String clickActionData = null;
+		public String hoverActionName = null;
+		public String hoverActionData = null;
+		public final String text;
+		
+		public MessagePart(final String text) {
+			this.text = text;
+		}
+		
+		public JsonWriter writeJson(final JsonWriter json) throws IOException {
+			json.beginObject().name("text").value(text);
+			if (color != null) {
+				json.name("color").value(color.name().toLowerCase());
+			}
+			if (styles != null) {
+				for (final ChatColor style : styles) {
+					json.name(style == ChatColor.UNDERLINE ? "underlined" : style.name().toLowerCase()).value(true);
+				}
+			}
+			if (clickActionName != null && clickActionData != null) {
+				json.name("clickEvent")
+					.beginObject()
+						.name("action").value(clickActionName)
+						.name("value").value(clickActionData)
+					.endObject();
+			}
+			if (hoverActionName != null && hoverActionData != null) {
+				json.name("hoverEvent")
+					.beginObject()
+						.name("action").value(hoverActionName)
+						.name("value").value(hoverActionData)
+					.endObject();
+			}
+			return json.endObject();
+		}
+		
+	}
 }
