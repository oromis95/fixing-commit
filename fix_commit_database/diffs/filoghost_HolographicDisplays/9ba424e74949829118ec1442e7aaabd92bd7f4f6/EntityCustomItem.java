@@ -45,7 +45,7 @@ public class EntityCustomItem extends EntityItem implements CustomItem, BasicEnt
 	public ItemStack getItemStack() {
 		// Dirty method to check if the icon is being picked up
 		StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
-		if (stacktrace.length >= 2 && stacktrace[1].getClassName().contains("EntityInsentient")) {
+		if (stacktrace.length > 2 && stacktrace[2].getClassName().contains("EntityInsentient")) {
 			return null; // Try to pickup this, dear entity ignoring the pickupDelay!
 		}
 		
