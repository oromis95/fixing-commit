@@ -3,7 +3,6 @@ package com.gmail.filoghost.holograms.commands.main.subs;
 import java.util.Arrays;
 import java.util.List;
 
-import org.bukkit.Bukkit;
 import org.bukkit.Location;
 import org.bukkit.command.CommandSender;
 import org.bukkit.entity.Player;
@@ -13,7 +12,6 @@ import com.gmail.filoghost.holograms.commands.CommandValidator;
 import com.gmail.filoghost.holograms.commands.Messages;
 import com.gmail.filoghost.holograms.commands.main.HologramSubCommand;
 import com.gmail.filoghost.holograms.database.HologramDatabase;
-import com.gmail.filoghost.holograms.event.HologramMoveEvent;
 import com.gmail.filoghost.holograms.exception.CommandException;
 import com.gmail.filoghost.holograms.object.CraftHologram;
 import com.gmail.filoghost.holograms.object.HologramManager;
@@ -55,9 +53,6 @@ public class MovehereCommand extends HologramSubCommand {
 		to.setPitch(90);
 		player.teleport(to, TeleportCause.PLUGIN);
 		player.sendMessage(Format.HIGHLIGHT + "You moved the hologram '" + hologram.getName() + "' near to you.");
-		
-		// Call the event.
-		Bukkit.getPluginManager().callEvent(new HologramMoveEvent(hologram));
 	}
 
 	@Override
