@@ -115,6 +115,16 @@ public class EntityCustomItem extends EntityItem implements CustomItem, BasicEnt
 		lockTick = lock;
 	}
 	
+	@Override
+	public void setCustomName(String customName) {
+		// Locks the custom name.
+	}
+	
+	@Override
+	public void setCustomNameVisible(boolean visible) {
+		// Locks the custom name.
+	}
+	
 	@Override
 	public void die() {
 		setLockTick(false);
