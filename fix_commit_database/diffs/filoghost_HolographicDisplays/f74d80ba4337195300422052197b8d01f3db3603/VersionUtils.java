@@ -7,6 +7,17 @@ import org.bukkit.Bukkit;
 
 public class VersionUtils {
 	
+	private static final boolean IS_PAPER_SERVER = Bukkit.getName().equals("Paper");
+	
+	
+	/**
+	 * Paper contains some changes that require
+	 */
+	public static boolean isPaperServer() {
+		return IS_PAPER_SERVER;
+	}
+	
+	
 	/**
 	 * This method uses a regex to get the NMS package part that changes with every update.
 	 * Example: v1_8_R1
