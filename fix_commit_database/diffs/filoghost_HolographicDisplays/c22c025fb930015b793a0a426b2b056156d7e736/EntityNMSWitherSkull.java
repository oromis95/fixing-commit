@@ -11,7 +11,6 @@ import net.minecraft.server.v1_7_R4.NBTTagCompound;
 import net.minecraft.server.v1_7_R4.PacketPlayOutEntityTeleport;
 import net.minecraft.server.v1_7_R4.World;
 import net.minecraft.server.v1_7_R4.EntityPlayer;
-import net.minecraft.server.v1_7_R4.MathHelper;
 
 public class EntityNMSWitherSkull extends EntityWitherSkull implements NMSWitherSkull {
 
@@ -122,14 +121,7 @@ public class EntityNMSWitherSkull extends EntityWitherSkull implements NMSWither
 		super.setPosition(x, y, z);
 
 		// Send a packet near to update the position.
-		PacketPlayOutEntityTeleport teleportPacket = new PacketPlayOutEntityTeleport(
-			getIdNMS(),
-			MathHelper.floor(this.locX * 32.0D),
-			MathHelper.floor(this.locY * 32.0D),
-			MathHelper.floor(this.locZ * 32.0D),
-			(byte) (int) (this.yaw * 256.0F / 360.0F),
-			(byte) (int) (this.pitch * 256.0F / 360.0F)
-		);
+		PacketPlayOutEntityTeleport teleportPacket = new PacketPlayOutEntityTeleport(this);
 
 		for (Object obj : this.world.players) {
 			if (obj instanceof EntityPlayer) {
