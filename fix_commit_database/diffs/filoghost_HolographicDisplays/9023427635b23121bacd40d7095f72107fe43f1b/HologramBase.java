@@ -5,6 +5,7 @@ import org.bukkit.Location;
 import org.bukkit.World;
 
 import com.gmail.filoghost.holograms.utils.Validator;
+import com.gmail.filoghost.holograms.utils.VisibilityManager;
 
 public abstract class HologramBase {
 	
@@ -17,6 +18,8 @@ public abstract class HologramBase {
 	protected int chunkX;
 	protected int chunkZ;
 	
+	protected VisibilityManager visibilityManager;
+	
 	private boolean deleted;
 	
 	protected HologramBase(String name, Location source) {
@@ -81,6 +84,18 @@ public abstract class HologramBase {
 		return bukkitWorld.isChunkLoaded(chunkX, chunkZ);
 	}
 	
+	public void setVisibilityManager(VisibilityManager visibilityManager) {
+		this.visibilityManager = visibilityManager;
+	}
+	
+	public boolean hasVisibilityManager() {
+		return visibilityManager != null;
+	}
+	
+	public VisibilityManager getVisibilityManager() {
+		return visibilityManager;
+	}
+	
 	public final void delete() {
 		deleted = true;
 		onDeleteEvent();		
