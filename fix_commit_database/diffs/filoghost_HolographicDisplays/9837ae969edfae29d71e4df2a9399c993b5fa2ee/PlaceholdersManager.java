@@ -4,12 +4,14 @@ import java.util.Iterator;
 import java.util.Map;
 import java.util.Map.Entry;
 import java.util.Set;
+import java.util.logging.Level;
 import java.util.regex.Matcher;
 import java.util.regex.Pattern;
 
 import org.bukkit.Bukkit;
 import org.bukkit.plugin.Plugin;
 
+import com.gmail.filoghost.holographicdisplays.HolographicDisplays;
 import com.gmail.filoghost.holographicdisplays.api.placeholder.PlaceholderReplacer;
 import com.gmail.filoghost.holographicdisplays.bridge.bungeecord.BungeeServerTracker;
 import com.gmail.filoghost.holographicdisplays.nms.interfaces.entity.NMSNameable;
@@ -48,7 +50,11 @@ public class PlaceholdersManager {
 				
 				for (Placeholder placeholder : PlaceholdersRegister.getPlaceholders()) {
 					if (elapsedTenthsOfSecond % placeholder.getTenthsToRefresh() == 0) {
-						placeholder.update();
+						try {
+							placeholder.update();
+						} catch (Throwable t) {
+							HolographicDisplays.getInstance().getLogger().log(Level.WARNING, "The placeholder " + placeholder.getTextPlaceholder() + " registered by the plugin " + placeholder.getOwner().getName() + " generated an exception while updating. Please contact the author of " + placeholder.getOwner().getName(), t);
+						}
 					}
 				}
 				
