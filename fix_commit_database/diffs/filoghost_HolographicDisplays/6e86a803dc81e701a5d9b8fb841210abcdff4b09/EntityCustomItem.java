@@ -1,5 +1,7 @@
 package com.gmail.filoghost.holograms.nms.v1_7_R4;
 
+import net.minecraft.server.v1_7_R4.NBTTagList;
+import net.minecraft.server.v1_7_R4.NBTTagString;
 import net.minecraft.server.v1_7_R4.EntityHuman;
 import net.minecraft.server.v1_7_R4.EntityItem;
 import net.minecraft.server.v1_7_R4.EntityPlayer;
@@ -11,10 +13,12 @@ import org.bukkit.craftbukkit.v1_7_R4.entity.CraftEntity;
 import org.bukkit.craftbukkit.v1_7_R4.inventory.CraftItemStack;
 import org.bukkit.entity.Player;
 
+import com.gmail.filoghost.holograms.HolographicDisplays;
 import com.gmail.filoghost.holograms.api.FloatingItem;
 import com.gmail.filoghost.holograms.nms.interfaces.BasicEntityNMS;
 import com.gmail.filoghost.holograms.nms.interfaces.CustomItem;
 import com.gmail.filoghost.holograms.object.HologramBase;
+import com.gmail.filoghost.holograms.utils.ItemUtils;
 
 public class EntityCustomItem extends EntityItem implements CustomItem, BasicEntityNMS {
 	
@@ -60,7 +64,12 @@ public class EntityCustomItem extends EntityItem implements CustomItem, BasicEnt
 
 			FloatingItem floatingItemParent = (FloatingItem) parent;
 			if (floatingItemParent.hasPickupHandler()) {
-				floatingItemParent.getPickupHandler().onPickup(floatingItemParent, (Player) human.getBukkitEntity());
+				try {
+					floatingItemParent.getPickupHandler().onPickup(floatingItemParent, (Player) human.getBukkitEntity());
+				} catch (Exception ex) {
+					ex.printStackTrace();
+					HolographicDisplays.getInstance().getLogger().warning("An exception occurred while a player picking up a floating item. It's probably caused by another plugin using Holographic Displays as library.");
+				}
 			}
 			
 			// It is never added to the inventory.
@@ -136,6 +145,20 @@ public class EntityCustomItem extends EntityItem implements CustomItem, BasicEnt
 	@Override
 	public void setItemStackNMS(org.bukkit.inventory.ItemStack stack) {
 		ItemStack newItem = CraftItemStack.asNMSCopy(stack);
+		
+		if (newItem.tag == null) {
+			newItem.tag = new NBTTagCompound();
+		}
+		NBTTagCompound display = newItem.tag.getCompound("display");
+		
+		if (!newItem.tag.hasKey("display")) {
+		newItem.tag.set("display", display);
+		}
+		
+		NBTTagList tagList = new NBTTagList();
+		tagList.add(new NBTTagString(ItemUtils.ANTISTACK_LORE)); // Antistack lore
+		
+		display.set("Lore", tagList);
 		newItem.count = 0;
 		setItemStack(newItem);
 	}
