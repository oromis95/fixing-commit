@@ -71,7 +71,10 @@ public class WrapperPlayClientCustomPayload extends AbstractPacket {
 	 * @return Payload contents as a byte array
 	 */
 	public byte[] getContents() {
-		return getContentsBuffer().array();
+		ByteBuf buffer = getContentsBuffer();
+		byte[] array = new byte[buffer.readableBytes()];
+		buffer.readBytes(array);
+		return array;
 	}
 
 	/**
