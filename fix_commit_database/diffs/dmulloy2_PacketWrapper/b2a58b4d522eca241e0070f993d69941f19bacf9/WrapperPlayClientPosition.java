@@ -5,16 +5,16 @@ import com.comphenix.protocol.events.PacketContainer;
 
 public class WrapperPlayClientPosition extends AbstractPacket {
     public static final PacketType TYPE = PacketType.Play.Client.POSITION;
-    
+
     public WrapperPlayClientPosition() {
         super(new PacketContainer(TYPE), TYPE);
         handle.getModifier().writeDefaults();
     }
-    
+
     public WrapperPlayClientPosition(PacketContainer packet) {
         super(packet, TYPE);
     }
-    
+
     /**
      * Retrieve X.
      * <p>
@@ -24,7 +24,7 @@ public class WrapperPlayClientPosition extends AbstractPacket {
     public double getX() {
         return handle.getDoubles().read(0);
     }
-    
+
     /**
      * Set X.
      * @param value - new value.
@@ -32,25 +32,25 @@ public class WrapperPlayClientPosition extends AbstractPacket {
     public void setX(double value) {
         handle.getDoubles().write(0, value);
     }
-    
+
     /**
      * Retrieve FeetY.
      * <p>
      * Notes: absolute feet position, normally HeadY - 1.62. Used to modify the players bounding box when going up stairs, crouching, etc…
      * @return The current FeetY
      */
-    public double getFeety() {
-        return handle.getDoubles().read(0);
+    public double getY() {
+        return handle.getDoubles().read(1);
     }
-    
+
     /**
      * Set FeetY.
      * @param value - new value.
      */
-    public void setFeety(double value) {
-        handle.getDoubles().write(0, value);
+    public void setY(double value) {
+        handle.getDoubles().write(1, value);
     }
-    
+
     /**
      * Retrieve Z.
      * <p>
@@ -58,17 +58,17 @@ public class WrapperPlayClientPosition extends AbstractPacket {
      * @return The current Z
      */
     public double getZ() {
-        return handle.getDoubles().read(0);
+        return handle.getDoubles().read(2);
     }
-    
+
     /**
      * Set Z.
      * @param value - new value.
      */
     public void setZ(double value) {
-        handle.getDoubles().write(0, value);
+        handle.getDoubles().write(2, value);
     }
-    
+
     /**
      * Retrieve On Ground.
      * <p>
@@ -76,16 +76,15 @@ public class WrapperPlayClientPosition extends AbstractPacket {
      * @return The current On Ground
      */
     public boolean getOnGround() {
-        return handle.getSpecificModifier(boolean.class).read(0);
+        return handle.getBooleans().read(0);
     }
-    
+
     /**
      * Set On Ground.
      * @param value - new value.
      */
     public void setOnGround(boolean value) {
-        handle.getSpecificModifier(boolean.class).write(0,  value);
+        handle.getBooleans().write(0, value);
     }
-    
+
 }
-
