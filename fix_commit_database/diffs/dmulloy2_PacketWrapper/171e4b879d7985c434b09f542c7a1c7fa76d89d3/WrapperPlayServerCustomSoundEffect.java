@@ -167,8 +167,8 @@ public class WrapperPlayServerCustomSoundEffect extends AbstractPacket {
 	 * 
 	 * @return The current Pitch
 	 */
-	public int getPitch() {
-		return handle.getIntegers().read(3);
+	public float getPitch() {
+		return handle.getFloat().read(1);
 	}
 
 	/**
@@ -176,8 +176,7 @@ public class WrapperPlayServerCustomSoundEffect extends AbstractPacket {
 	 * 
 	 * @param value - new value.
 	 */
-	public void setPitch(int value) {
-		handle.getIntegers().write(3, value);
+	public void setPitch(float value) {
+		handle.getFloat().write(1, value);
 	}
-
 }
