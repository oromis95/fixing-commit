@@ -35,26 +35,6 @@ public class WrapperPlayClientResourcePackStatus extends AbstractPacket {
 		super(packet, TYPE);
 	}
 
-	/**
-	 * Retrieve Hash.
-	 * <p>
-	 * Notes: the hash sent in the Resource Pack Send packet.
-	 * 
-	 * @return The current Hash
-	 */
-	public String getHash() {
-		return handle.getStrings().read(0);
-	}
-
-	/**
-	 * Set Hash.
-	 * 
-	 * @param value - new value.
-	 */
-	public void setHash(String value) {
-		handle.getStrings().write(0, value);
-	}
-
 	/**
 	 * Retrieve Result.
 	 * <p>
@@ -75,5 +55,4 @@ public class WrapperPlayClientResourcePackStatus extends AbstractPacket {
 	public void setResult(ResourcePackStatus value) {
 		handle.getResourcePackStatus().write(0, value);
 	}
-
 }
