@@ -18,146 +18,318 @@
  */
 package com.comphenix.packetwrapper;
 
+import org.bukkit.World;
+import org.bukkit.entity.Entity;
+
 import com.comphenix.protocol.PacketType;
+import com.comphenix.protocol.ProtocolLibrary;
 import com.comphenix.protocol.events.PacketContainer;
+import com.comphenix.protocol.events.PacketEvent;
+import com.comphenix.protocol.injector.PacketConstructor;
+import com.comphenix.protocol.reflect.IntEnum;
 
 public class WrapperPlayServerSpawnEntity extends AbstractPacket {
     public static final PacketType TYPE = PacketType.Play.Server.SPAWN_ENTITY;
-    
+
+    private static PacketConstructor entityConstructor;
+
+    /**
+     * Represents the different object types.
+     * 
+     * @author Kristian
+     */
+    public static class ObjectTypes extends IntEnum {
+        public static final int BOAT = 1;
+        public static final int ITEM_STACK = 2;
+        public static final int MINECART = 10;
+        public static final int MINECART_STORAGE = 11;
+        public static final int MINECART_POWERED = 12;
+        public static final int ACTIVATED_TNT = 50;
+        public static final int ENDER_CRYSTAL = 51;
+        public static final int ARROW_PROJECTILE = 60;
+        public static final int SNOWBALL_PROJECTILE = 61;
+        public static final int EGG_PROJECTILE = 62;
+        public static final int FIRE_BALL_GHAST = 63;
+        public static final int FIRE_BALL_BLAZE = 64;
+        public static final int THROWN_ENDERPEARL = 65;
+        public static final int WITHER_SKULL = 66;
+        public static final int FALLING_BLOCK = 70;
+        public static final int ITEM_FRAME = 71;
+        public static final int EYE_OF_ENDER = 72;
+        public static final int THROWN_POTION = 73;
+        public static final int FALLING_DRAGON_EGG = 74;
+        public static final int THROWN_EXP_BOTTLE = 75;
+        public static final int FIREWORK = 76;
+        public static final int FISHING_FLOAT = 90;
+
+        /**
+         * The singleton instance. Can also be retrieved from the parent class.
+         */
+        private static ObjectTypes INSTANCE = new ObjectTypes();
+
+        /**
+         * Retrieve an instance of the object types enum.
+         * @return Object type enum.
+         */
+        public static ObjectTypes getInstance() {
+            return INSTANCE;
+        }
+    }
+
     public WrapperPlayServerSpawnEntity() {
         super(new PacketContainer(TYPE), TYPE);
         handle.getModifier().writeDefaults();
     }
-    
+
     public WrapperPlayServerSpawnEntity(PacketContainer packet) {
         super(packet, TYPE);
     }
-    
+
+    public WrapperPlayServerSpawnEntity(Entity entity, int type, int objectData) {
+        super(fromEntity(entity, type, objectData), TYPE);
+    }
+
+    // Useful constructor
+    private static PacketContainer fromEntity(Entity entity, int type, int objectData) {
+        if (entityConstructor == null)
+            entityConstructor = ProtocolLibrary.getProtocolManager().createPacketConstructor(TYPE, entity, type, objectData);
+        return entityConstructor.createPacket(entity, type, objectData);
+    }
+
     /**
-     * Retrieve Entity ID.
-     * <p>
-     * Notes: entity ID of the object
-     * @return The current Entity ID
-     */
+     * Retrieve entity ID of the Object.
+     * @return The current EID
+    */
     public int getEntityID() {
         return handle.getIntegers().read(0);
     }
-    
+
     /**
-     * Set Entity ID.
-     * @param value - new value.
+     * Retrieve the entity that will be spawned.
+     * @param world - the current world of the entity.
+     * @return The spawned entity.
+     */
+    public Entity getEntity(World world) {
+        return handle.getEntityModifier(world).read(0);
+    }
+
+    /**
+     * Retrieve the entity that will be spawned.
+     * @param event - the packet event.
+     * @return The spawned entity.
      */
-    public void setEntityId(int value) {
+    public Entity getEntity(PacketEvent event) {
+        return getEntity(event.getPlayer().getWorld());
+    }
+
+    /**
+     * Set entity ID of the Object.
+     * @param value - new value.
+    */
+    public void setEntityID(int value) {
         handle.getIntegers().write(0, value);
     }
-    
+
     /**
-     * Retrieve Type.
-     * <p>
-     * Notes: the type of object (See Objects)
+     * Retrieve the type of object. See {@link ObjectTypes}
      * @return The current Type
-     */
+    */
     public int getType() {
         return handle.getIntegers().read(9);
     }
-    
+
     /**
-     * Set Type.
+     * Set the type of object. See {@link ObjectTypes}.
      * @param value - new value.
-     */
+    */
     public void setType(int value) {
         handle.getIntegers().write(9, value);
     }
-    
+
     /**
-     * Retrieve X.
+     * Retrieve the x position of the object.
      * <p>
-     * Notes: x position as a Fixed-Point number
+     * Note that the coordinate is rounded off to the nearest 1/32 of a meter.
      * @return The current X
-     */
-    public int getX() {
-        return handle.getIntegers().read(1);
+    */
+    public double getX() {
+        return handle.getIntegers().read(1) / 32.0D;
     }
-    
+
     /**
-     * Set X.
+     * Set the x position of the object.
      * @param value - new value.
-     */
-    public void setX(int value) {
-        handle.getIntegers().write(1, value);
+    */
+    public void setX(double value) {
+        handle.getIntegers().write(1, (int) Math.floor(value * 32.0D));
     }
-    
+
     /**
-     * Retrieve Y.
+     * Retrieve the y position of the object.
      * <p>
-     * Notes: y position as a Fixed-Point number
-     * @return The current Y
-     */
-    public int getY() {
-        return handle.getIntegers().read(2);
+     * Note that the coordinate is rounded off to the nearest 1/32 of a meter.
+     * @return The current y
+    */
+    public double getY() {
+        return handle.getIntegers().read(2) / 32.0D;
     }
-    
+
     /**
-     * Set Y.
+     * Set the y position of the object.
      * @param value - new value.
-     */
-    public void setY(int value) {
-        handle.getIntegers().write(2, value);
+    */
+    public void setY(double value) {
+        handle.getIntegers().write(2, (int) Math.floor(value * 32.0D));
     }
-    
+
     /**
-     * Retrieve Z.
+     * Retrieve the z position of the object.
      * <p>
-     * Notes: z position as a Fixed-Point number
-     * @return The current Z
-     */
-    public int getZ() {
-        return handle.getIntegers().read(3);
+     * Note that the coordinate is rounded off to the nearest 1/32 of a meter.
+     * @return The current z
+    */
+    public double getZ() {
+        return handle.getIntegers().read(3) / 32.0D;
     }
-    
+
     /**
-     * Set Z.
+     * Set the z position of the object.
      * @param value - new value.
-     */
-    public void setZ(int value) {
-        handle.getIntegers().write(3, value);
+    */
+    public void setZ(double value) {
+        handle.getIntegers().write(3, (int) Math.floor(value * 32.0D));
     }
-    
+
     /**
-     * Retrieve Pitch.
+     * Retrieve the optional speed x.
      * <p>
-     * Notes: the pitch in steps of 2p/256
-     * @return The current Pitch
-     */
-    public int getPitch() {
-        return handle.getIntegers().read(7);
+     * This is ignored if {@link #getObjectData()} is zero.
+     * @return The optional speed x.
+    */
+    public double getOptionalSpeedX() {
+        return handle.getIntegers().read(4) / 8000.0D;
     }
-    
+
     /**
-     * Set Pitch.
+     * Set the optional speed x.
      * @param value - new value.
-     */
-    public void setPitch(int value) {
-        handle.getIntegers().write(7, value);
+    */
+    public void setOptionalSpeedX(double value) {
+        handle.getIntegers().write(4, (int) (value * 8000.0D));
     }
-    
+
     /**
-     * Retrieve Yaw.
+     * Retrieve the optional speed y.
      * <p>
-     * Notes: the yaw in steps of 2p/256
-     * @return The current Yaw
-     */
-    public int getYaw() {
-        return handle.getIntegers().read(8);
+     * This is ignored if {@link #getObjectData()} is zero.
+     * @return The optional speed y.
+    */
+    public double getOptionalSpeedY() {
+        return handle.getIntegers().read(5) / 8000.0D;
     }
-    
+
     /**
-     * Set Yaw.
+     * Set the optional speed y.
      * @param value - new value.
-     */
-    public void setYaw(int value) {
-        handle.getIntegers().write(8, value);
+    */
+    public void setOptionalSpeedY(double value) {
+        handle.getIntegers().write(5, (int) (value * 8000.0D));
+    }
+
+    /**
+     * Retrieve the optional speed z.
+     * <p>
+     * This is ignored if {@link #getObjectData()} is zero.
+     * @return The optional speed z.
+    */
+    public double getOptionalSpeedZ() {
+        return handle.getIntegers().read(6) / 8000.0D;
     }
-    
-}
 
+    /**
+     * Set the optional speed z.
+     * @param value - new value.
+    */
+    public void setOptionalSpeedZ(double value) {
+        handle.getIntegers().write(6, (int) (value * 8000.0D));
+    }
+
+    /**
+     * Retrieve the yaw.
+     * @return The current Yaw
+    */
+    public float getYaw() {
+        return (handle.getIntegers().read(7) * 360.F) / 256.0F;
+    }
+
+    /**
+     * Set the yaw of the object spawned.
+     * @param value - new yaw.
+    */
+    public void setYaw(float value) {
+        handle.getIntegers().write(7, (int) (value * 256.0F / 360.0F));
+    }
+
+    /**
+     * Retrieve the pitch.
+     * @return The current pitch.
+    */
+    public float getPitch() {
+        return (handle.getIntegers().read(8) * 360.F) / 256.0F;
+    }
+
+    /**
+     * Set the pitch.
+     * @param value - new pitch.
+    */
+    public void setPitch(float value) {
+        handle.getIntegers().write(8, (int) (value * 256.0F / 360.0F));
+    }
+
+    /**
+     * Retrieve object data.
+     * <p>
+     * The content depends on the object type:
+     * <table border="1" cellpadding="4">
+     *  <tr>
+     *   <th>Object Type:</th>
+     *   <th>Name:</th>
+     *   <th>Description</th>
+     *  </tr>
+     *  <tr>
+     *   <td>ITEM_FRAME</td>
+     *   <td>Orientation</td>
+     *   <td>0-3: South, West, North, East</td>
+     *  </tr>
+     *  <tr>
+     *   <td>FALLING_BLOCK</td>
+     *   <td>Block Type</td>
+     *   <td>BlockID | (Metadata << 0xC)</td>
+     *  </tr>
+     *  <tr>
+     *   <td>Projectiles</td>
+     *   <td>Entity ID</td>
+     *   <td>The entity ID of the thrower</td>
+     *  </tr>
+     *  <tr>
+     *   <td>Splash Potions</td>
+     *   <td>Data Value</td>
+     *   <td>Potion data value.</td>
+     *  </tr>
+     * </table>
+     * @return The current object Data
+    */
+    public int getObjectData() {
+        return handle.getIntegers().read(10);
+    }
+
+    /**
+     * Set object Data.
+     * <p>
+     * The content depends on the object type. See {@link #getObjectData()} for more information.
+     * @param value - new object data.
+    */
+    public void setObjectData(int value) {
+        handle.getIntegers().write(10, value);
+    }
+}
