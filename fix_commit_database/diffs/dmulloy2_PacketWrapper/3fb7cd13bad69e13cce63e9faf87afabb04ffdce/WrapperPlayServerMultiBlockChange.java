@@ -2,6 +2,7 @@ package com.comphenix.packetwrapper;
 
 import com.comphenix.protocol.PacketType;
 import com.comphenix.protocol.events.PacketContainer;
+import com.comphenix.protocol.wrappers.ChunkCoordIntPair;
 
 public class WrapperPlayServerMultiBlockChange extends AbstractPacket {
     public static final PacketType TYPE = PacketType.Play.Server.MULTI_BLOCK_CHANGE;
