@@ -2,6 +2,7 @@ package com.comphenix.packetwrapper;
 
 import com.comphenix.protocol.PacketType;
 import com.comphenix.protocol.events.PacketContainer;
+import com.comphenix.protocol.wrappers.EnumWrappers.ClientCommand;
 
 public class WrapperPlayClientClientCommand extends AbstractPacket {
     public static final PacketType TYPE = PacketType.Play.Client.CLIENT_COMMAND;
@@ -21,16 +22,16 @@ public class WrapperPlayClientClientCommand extends AbstractPacket {
      * Notes: see below
      * @return The current Action ID
      */
-    public int getActionId() {
-        return (int) handle.getSpecificModifier(Enum.class).read(0);
+    public ClientCommand getAction() {
+        return handle.getClientCommands().read(0);
     }
     
     /**
      * Set Action ID.
      * @param value - new value.
      */
-    public void setActionId(int value) {
-        handle.getSpecificModifier(Enum.class).write(0, (Enum<?>) value);
+    public void setAction(ClientCommand value) {
+        handle.getClientCommands().write(0, value);
     }
     
 }
