@@ -1,7 +1,10 @@
 package com.comphenix.packetwrapper;
 
+import org.bukkit.inventory.ItemStack;
+
 import com.comphenix.protocol.PacketType;
 import com.comphenix.protocol.events.PacketContainer;
+import com.comphenix.protocol.wrappers.BlockPosition;
 
 public class WrapperPlayClientBlockPlace extends AbstractPacket {
     public static final PacketType TYPE = PacketType.Play.Client.BLOCK_PLACE;
@@ -22,7 +25,7 @@ public class WrapperPlayClientBlockPlace extends AbstractPacket {
      * @return The current Location
      */
     public BlockPosition getLocation() {
-        return handle.getBlockPositions().read(0);
+        return handle.getBlockPositionModifier().read(0);
     }
     
     /**
@@ -30,7 +33,7 @@ public class WrapperPlayClientBlockPlace extends AbstractPacket {
      * @param value - new value.
      */
     public void setLocation(BlockPosition value) {
-        handle.getBlockPositions().write(0, value);
+        handle.getBlockPositionModifier().write(0, value);
     }
     
     /**
@@ -39,16 +42,16 @@ public class WrapperPlayClientBlockPlace extends AbstractPacket {
      * Notes: the offset to use for block/item placement (see below)
      * @return The current Direction
      */
-    public byte getDirection() {
-        return (byte) handle.getIntegers().read(0);
+    public int getFace() {
+        return handle.getIntegers().read(0);
     }
     
     /**
      * Set Direction.
      * @param value - new value.
      */
-    public void setDirection(byte value) {
-        handle.getIntegers().write(0, (int) value);
+    public void setFace(int value) {
+        handle.getIntegers().write(0, value);
     }
     
     /**
@@ -73,48 +76,48 @@ public class WrapperPlayClientBlockPlace extends AbstractPacket {
      * Notes: the position of the crosshair on the block
      * @return The current Cursor position X
      */
-    public byte getCursorPositionX() {
-        return (byte) handle.getFloat().read(0);
+    public float getCursorPositionX() {
+        return handle.getFloat().read(0);
     }
     
     /**
      * Set Cursor position X.
      * @param value - new value.
      */
-    public void setCursorPositionX(byte value) {
-        handle.getFloat().write(0, (float) value);
+    public void setCursorPositionX(float value) {
+        handle.getFloat().write(0, value);
     }
     
     /**
      * Retrieve Cursor position Y.
      * @return The current Cursor position Y
      */
-    public byte getCursorPositionY() {
-        return (byte) handle.getFloat().read(1);
+    public float getCursorPositionY() {
+        return handle.getFloat().read(1);
     }
     
     /**
      * Set Cursor position Y.
      * @param value - new value.
      */
-    public void setCursorPositionY(byte value) {
-        handle.getFloat().write(1, (float) value);
+    public void setCursorPositionY(float value) {
+        handle.getFloat().write(1, value);
     }
     
     /**
      * Retrieve Cursor position Z.
      * @return The current Cursor position Z
      */
-    public byte getCursorPositionZ() {
-        return (byte) handle.getFloat().read(2);
+    public float getCursorPositionZ() {
+        return handle.getFloat().read(2);
     }
     
     /**
      * Set Cursor position Z.
      * @param value - new value.
      */
-    public void setCursorPositionZ(byte value) {
-        handle.getFloat().write(2, (float) value);
+    public void setCursorPositionZ(float value) {
+        handle.getFloat().write(2, value);
     }
     
 }
