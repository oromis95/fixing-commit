@@ -2,6 +2,7 @@ package com.comphenix.packetwrapper;
 
 import com.comphenix.protocol.PacketType;
 import com.comphenix.protocol.events.PacketContainer;
+import com.comphenix.protocol.wrappers.WrappedGameProfile;
 
 public class WrapperLoginClientStart extends AbstractPacket {
     public static final PacketType TYPE = PacketType.Login.Client.START;
