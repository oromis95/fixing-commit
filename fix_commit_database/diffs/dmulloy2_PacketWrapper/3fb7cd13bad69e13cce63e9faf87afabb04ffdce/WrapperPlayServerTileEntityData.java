@@ -2,6 +2,8 @@ package com.comphenix.packetwrapper;
 
 import com.comphenix.protocol.PacketType;
 import com.comphenix.protocol.events.PacketContainer;
+import com.comphenix.protocol.wrappers.BlockPosition;
+import com.comphenix.protocol.wrappers.nbt.NbtBase;
 
 public class WrapperPlayServerTileEntityData extends AbstractPacket {
     public static final PacketType TYPE = PacketType.Play.Server.TILE_ENTITY_DATA;
