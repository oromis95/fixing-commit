@@ -21,16 +21,16 @@ public class WrapperPlayClientFlying extends AbstractPacket {
      * Notes: true if the client is on the ground, False otherwise
      * @return The current On Ground
      */
-    public bool getOnGround() {
-        return (bool) handle.getSpecificModifier(boolean.class).read(0);
+    public boolean getOnGround() {
+        return handle.getSpecificModifier(boolean.class).read(0);
     }
     
     /**
      * Set On Ground.
      * @param value - new value.
      */
-    public void setOnGround(bool value) {
-        handle.getSpecificModifier(boolean.class).write(0, (boolean) value);
+    public void setOnGround(boolean value) {
+        handle.getSpecificModifier(boolean.class).write(0, value);
     }
     
 }
