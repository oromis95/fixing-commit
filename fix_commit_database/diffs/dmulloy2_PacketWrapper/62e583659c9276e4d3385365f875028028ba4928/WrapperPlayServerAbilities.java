@@ -42,27 +42,27 @@ public class WrapperPlayServerAbilities extends AbstractPacket {
     }
 
     public boolean isFlying() {
-    	return handle.getBooleans().read(0);
+    	return handle.getBooleans().read(1);
     }
 
     public void setFlying(boolean value) {
-    	handle.getBooleans().write(0, value);
+    	handle.getBooleans().write(1, value);
     }
 
     public boolean canFly() {
-    	return handle.getBooleans().read(0);
+    	return handle.getBooleans().read(2);
     }
 
     public void setCanFly(boolean value) {
-    	handle.getBooleans().write(0, value);
+    	handle.getBooleans().write(2, value);
     }
 
     public boolean canInstantlyBuild() {
-    	return handle.getBooleans().read(0);
+    	return handle.getBooleans().read(3);
     }
 
     public void setCanInstantlyBuild(boolean value) {
-    	handle.getBooleans().write(0, value);
+    	handle.getBooleans().write(3, value);
     }
 
     public float getFlyingSpeed() {
@@ -74,11 +74,11 @@ public class WrapperPlayServerAbilities extends AbstractPacket {
     }
 
     public float getWalkingSpeed() {
-    	return handle.getFloat().read(0);
+    	return handle.getFloat().read(1);
     }
 
     public void setWalkingSpeed(float value) {
-    	handle.getFloat().write(0, value);
+    	handle.getFloat().write(1, value);
     }
 }
 
