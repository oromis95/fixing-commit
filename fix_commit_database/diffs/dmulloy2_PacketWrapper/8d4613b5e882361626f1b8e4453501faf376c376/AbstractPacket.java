@@ -72,7 +72,10 @@ public abstract class AbstractPacket {
 	 * Simulate receiving the current packet from the given sender.
 	 * @param sender - the sender.
 	 * @throws RuntimeException If the packet cannot be received.
+	 * @deprecated Misspelled. recieve -> receive
+	 * @see #receivePacket(Player)
 	 */
+	@Deprecated
 	public void recievePacket(Player sender) {
 		try {
 			ProtocolLibrary.getProtocolManager().recieveClientPacket(sender, getHandle());
@@ -80,4 +83,17 @@ public abstract class AbstractPacket {
 			throw new RuntimeException("Cannot recieve packet.", e);
 		}
 	}
+
+	/**
+	 * Simulate receiving the current packet from the given sender.
+	 * @param sender - the sender.
+	 * @throws RuntimeException if the packet cannot be received.
+	 */
+    public void receivePacket(Player sender) {
+        try {
+            ProtocolLibrary.getProtocolManager().recieveClientPacket(sender, getHandle());
+        } catch (Exception e) {
+            throw new RuntimeException("Cannot recieve packet.", e);
+        }
+    }
 }
