@@ -58,7 +58,7 @@ public class WrapperPlayClientLook extends AbstractPacket {
      * @return The current Pitch
      */
     public float getPitch() {
-        return handle.getFloat().read(0);
+        return handle.getFloat().read(1);
     }
     
     /**
@@ -66,7 +66,7 @@ public class WrapperPlayClientLook extends AbstractPacket {
      * @param value - new value.
      */
     public void setPitch(float value) {
-        handle.getFloat().write(0, value);
+        handle.getFloat().write(1, value);
     }
     
     /**
@@ -76,7 +76,7 @@ public class WrapperPlayClientLook extends AbstractPacket {
      * @return The current On Ground
      */
     public boolean getOnGround() {
-        return handle.getSpecificModifier(boolean.class).read(0);
+        return handle.getBooleans().read(0);
     }
     
     /**
@@ -84,7 +84,7 @@ public class WrapperPlayClientLook extends AbstractPacket {
      * @param value - new value.
      */
     public void setOnGround(boolean value) {
-        handle.getSpecificModifier(boolean.class).write(0, value);
+        handle.getBooleans().write(0, value);
     }
     
 }
