@@ -2,6 +2,7 @@ package com.comphenix.packetwrapper;
 
 import com.comphenix.protocol.PacketType;
 import com.comphenix.protocol.events.PacketContainer;
+import com.comphenix.protocol.wrappers.EnumWrappers.CombatEventType;
 
 public class WrapperPlayServerCombatEvent extends AbstractPacket {
     public static final PacketType TYPE = PacketType.Play.Server.COMBAT_EVENT;
@@ -21,107 +22,19 @@ public class WrapperPlayServerCombatEvent extends AbstractPacket {
      * Notes: 0 ENTER_COMBAT, 1 END_COMBAT, 2 ENTITY_DEAD
      * @return The current Event
      */
-    public int getEvent() {
-        return (int) handle.getSpecificModifier(Enum.class).read(0);
+    public CombatEventType getEvent() {
+        return handle.getCombatEvents().read(0);
     }
     
     /**
      * Set Event.
      * @param value - new value.
      */
-    public void setEvent(int value) {
-        handle.getSpecificModifier(Enum.class).write(0, (Enum<?>) value);
+    public void setEvent(CombatEventType value) {
+       handle.getCombatEvents().write(0, value);
     }
-    
-    /**
-     * Retrieve Duration.
-     * <p>
-     * Notes: only for END_COMBAT
-     * @return The current Duration
-     */
-    public int getDuration() {
-        return (int) handle.getSpecificModifier(Enum.class).read(0);
-    }
-    
-    /**
-     * Set Duration.
-     * @param value - new value.
-     */
-    public void setDuration(int value) {
-        handle.getSpecificModifier(Enum.class).write(0, (Enum<?>) value);
-    }
-    
-    /**
-     * Retrieve Entity ID.
-     * <p>
-     * Notes: only for END_COMBAT
-     * @return The current Entity ID
-     */
-    public int getEntityId() {
-        return handle.getIntegers().read(2);
-    }
-    
-    /**
-     * Set Entity ID.
-     * @param value - new value.
-     */
-    public void setEntityId(int value) {
-        handle.getIntegers().write(2, value);
-    }
-    
-    /**
-     * Retrieve Player ID.
-     * <p>
-     * Notes: only for ENTITY_DEAD
-     * @return The current Player ID
-     */
-    public int getPlayerId() {
-        return handle.getIntegers().read(1);
-    }
-    
-    /**
-     * Set Player ID.
-     * @param value - new value.
-     */
-    public void setPlayerId(int value) {
-        handle.getIntegers().write(1, value);
-    }
-    
-    /**
-     * Retrieve Entity ID.
-     * <p>
-     * Notes: only for ENTITY_DEAD
-     * @return The current Entity ID
-     */
-    public int getEntityId() {
-        return (int) handle.getSpecificModifier(Enum.class).read(0);
-    }
-    
-    /**
-     * Set Entity ID.
-     * @param value - new value.
-     */
-    public void setEntityId(int value) {
-        handle.getSpecificModifier(Enum.class).write(0, (Enum<?>) value);
-    }
-    
-    /**
-     * Retrieve Message.
-     * <p>
-     * Notes: only for ENTITY_DEAD
-     * @return The current Message
-     */
-    public String getMessage() {
-        return (String) handle.getIntegers().read(0);
-    }
-    
-    /**
-     * Set Message.
-     * @param value - new value.
-     */
-    public void setMessage(String value) {
-        handle.getIntegers().write(0, (int) value);
-    }
-    
+
+    // The fields in this class depend on the CombatEventType
+    // TODO: Figure out a solution
 }
 
