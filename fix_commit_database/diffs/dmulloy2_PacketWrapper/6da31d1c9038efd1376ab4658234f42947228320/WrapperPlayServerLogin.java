@@ -4,6 +4,8 @@ import org.bukkit.WorldType;
 
 import com.comphenix.protocol.PacketType;
 import com.comphenix.protocol.events.PacketContainer;
+import com.comphenix.protocol.wrappers.EnumWrappers.Difficulty;
+import com.comphenix.protocol.wrappers.EnumWrappers.NativeGameMode;
 
 public class WrapperPlayServerLogin extends AbstractPacket {
     public static final PacketType TYPE = PacketType.Play.Server.LOGIN;
@@ -41,16 +43,16 @@ public class WrapperPlayServerLogin extends AbstractPacket {
      * Notes: 0: survival, 1: creative, 2: adventure. Bit 3 (0x8) is the hardcore flag
      * @return The current Gamemode
      */
-    public byte getGamemode() {
-        return (byte) handle.getSpecificModifier(Enum.class).read(0);
+    public NativeGameMode getGamemode() {
+        return handle.getGameModes().read(0);
     }
     
     /**
      * Set Gamemode.
      * @param value - new value.
      */
-    public void setGamemode(byte value) {
-        handle.getSpecificModifier(Enum.class).write(0, (Enum<?>) value);
+    public void setGamemode(NativeGameMode value) {
+        handle.getGameModes().write(0, value);
     }
     
     /**
@@ -59,16 +61,16 @@ public class WrapperPlayServerLogin extends AbstractPacket {
      * Notes: -1: nether, 0: overworld, 1: end
      * @return The current Dimension
      */
-    public byte getDimension() {
-        return (byte) handle.getSpecificModifier(boolean.class).read(0);
+    public int getDimension() {
+        return handle.getIntegers().read(0);
     }
     
     /**
      * Set Dimension.
      * @param value - new value.
      */
-    public void setDimension(byte value) {
-        handle.getSpecificModifier(boolean.class).write(0, (boolean) value);
+    public void setDimension(int value) {
+        handle.getIntegers().write(0, value);
     }
     
     /**
@@ -77,16 +79,16 @@ public class WrapperPlayServerLogin extends AbstractPacket {
      * Notes: 0 thru 3 for Peaceful, Easy, Normal, Hard
      * @return The current Difficulty
      */
-    public byte getDifficulty() {
-        return (byte) handle.getIntegers().read(1);
+    public Difficulty getDifficulty() {
+        return handle.getDifficulties().read(0);
     }
     
     /**
      * Set Difficulty.
      * @param value - new value.
      */
-    public void setDifficulty(byte value) {
-        handle.getIntegers().write(1, (int) value);
+    public void setDifficulty(Difficulty value) {
+        handle.getDifficulties().write(0, value);
     }
     
     /**
@@ -95,16 +97,16 @@ public class WrapperPlayServerLogin extends AbstractPacket {
      * Notes: used by the client to draw the player list
      * @return The current Max Players
      */
-    public byte getMaxPlayers() {
-        return (byte) handle.getSpecificModifier(Enum.class).read(0);
+    public int getMaxPlayers() {
+        return handle.getIntegers().read(1);
     }
     
     /**
      * Set Max Players.
      * @param value - new value.
      */
-    public void setMaxPlayers(byte value) {
-        handle.getSpecificModifier(Enum.class).write(0, (Enum<?>) value);
+    public void setMaxPlayers(int value) {
+        handle.getIntegers().write(0, value);
     }
     
     /**
@@ -113,16 +115,16 @@ public class WrapperPlayServerLogin extends AbstractPacket {
      * Notes: default, flat, largeBiomes, amplified, default_1_1
      * @return The current Level Type
      */
-    public String getLevelType() {
-        return (String) handle.getIntegers().read(2);
+    public WorldType getLevelType() {
+        return handle.getWorldTypeModifier().read(0);
     }
     
     /**
      * Set Level Type.
      * @param value - new value.
      */
-    public void setLevelType(String value) {
-        handle.getIntegers().write(2, (int) value);
+    public void setLevelType(WorldType value) {
+        handle.getWorldTypeModifier().write(0, value);
     }
     
     /**
@@ -130,7 +132,7 @@ public class WrapperPlayServerLogin extends AbstractPacket {
      * @return The current Reduced Debug Info
      */
     public boolean getReducedDebugInfo() {
-        return (boolean) handle.getWorldTypeModifier().read(0);
+        return handle.getBooleans().read(0);
     }
     
     /**
@@ -138,7 +140,7 @@ public class WrapperPlayServerLogin extends AbstractPacket {
      * @param value - new value.
      */
     public void setReducedDebugInfo(boolean value) {
-        handle.getWorldTypeModifier().write(0, (WorldType) value);
+        handle.getBooleans().write(0, value);
     }
     
 }
