@@ -21,16 +21,16 @@ public class WrapperPlayServerCraftProgressBar extends AbstractPacket {
      * Notes: the id of the window.
      * @return The current Window ID
      */
-    public byte getWindowId() {
-        return (byte) handle.getIntegers().read(0);
+    public int getWindowId() {
+        return handle.getIntegers().read(0);
     }
     
     /**
      * Set Window ID.
      * @param value - new value.
      */
-    public void setWindowId(byte value) {
-        handle.getIntegers().write(0, (int) value);
+    public void setWindowId(int value) {
+        handle.getIntegers().write(0, value);
     }
     
     /**
@@ -39,16 +39,16 @@ public class WrapperPlayServerCraftProgressBar extends AbstractPacket {
      * Notes: which property should be updated.
      * @return The current Property
      */
-    public short getProperty() {
-        return (short) handle.getIntegers().read(1);
+    public int getProperty() {
+        return handle.getIntegers().read(1);
     }
     
     /**
      * Set Property.
      * @param value - new value.
      */
-    public void setProperty(short value) {
-        handle.getIntegers().write(1, (int) value);
+    public void setProperty(int value) {
+        handle.getIntegers().write(1, value);
     }
     
     /**
@@ -57,16 +57,16 @@ public class WrapperPlayServerCraftProgressBar extends AbstractPacket {
      * Notes: the new value for the property.
      * @return The current Value
      */
-    public short getValue() {
-        return (short) handle.getIntegers().read(2);
+    public int getValue() {
+        return handle.getIntegers().read(2);
     }
     
     /**
      * Set Value.
      * @param value - new value.
      */
-    public void setValue(short value) {
-        handle.getIntegers().write(2, (int) value);
+    public void setValue(int value) {
+        handle.getIntegers().write(2, value);
     }
     
 }
