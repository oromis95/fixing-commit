@@ -41,9 +41,8 @@ public abstract class AbstractPacket {
 		if (handle == null)
 			throw new IllegalArgumentException("Packet handle cannot be NULL.");
 		if (!Objects.equal(handle.getType(), type))
-			throw new IllegalArgumentException(
-					handle.getHandle() + " is not a packet of type " + type);
-		
+			throw new IllegalArgumentException(handle.getHandle() + " is not a packet of type " + type);
+
 		this.handle = handle;
 	}
 
@@ -54,7 +53,7 @@ public abstract class AbstractPacket {
 	public PacketContainer getHandle() {
 		return handle;
 	}
-	
+
 	/**
 	 * Send the current packet to the given receiver.
 	 * @param receiver - the receiver.
@@ -67,7 +66,7 @@ public abstract class AbstractPacket {
 			throw new RuntimeException("Cannot send packet.", e);
 		}
 	}
-	
+
 	/**
 	 * Simulate receiving the current packet from the given sender.
 	 * @param sender - the sender.
@@ -89,11 +88,11 @@ public abstract class AbstractPacket {
 	 * @param sender - the sender.
 	 * @throws RuntimeException if the packet cannot be received.
 	 */
-    public void receivePacket(Player sender) {
-        try {
-            ProtocolLibrary.getProtocolManager().recieveClientPacket(sender, getHandle());
-        } catch (Exception e) {
-            throw new RuntimeException("Cannot recieve packet.", e);
-        }
-    }
+	public void receivePacket(Player sender) {
+		try {
+			ProtocolLibrary.getProtocolManager().recieveClientPacket(sender, getHandle());
+		} catch (Exception e) {
+			throw new RuntimeException("Cannot recieve packet.", e);
+		}
+	}
 }
