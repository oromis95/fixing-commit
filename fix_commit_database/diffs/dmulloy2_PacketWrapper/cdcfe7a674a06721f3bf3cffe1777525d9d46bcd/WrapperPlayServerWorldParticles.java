@@ -2,21 +2,36 @@ package com.comphenix.packetwrapper;
 
 import com.comphenix.protocol.PacketType;
 import com.comphenix.protocol.events.PacketContainer;
+import com.comphenix.protocol.wrappers.EnumWrappers.Particle;
 
 public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public static final PacketType TYPE = PacketType.Play.Server.WORLD_PARTICLES;
-    
+
     public WrapperPlayServerWorldParticles() {
         super(new PacketContainer(TYPE), TYPE);
         handle.getModifier().writeDefaults();
     }
-    
+
     public WrapperPlayServerWorldParticles(PacketContainer packet) {
         super(packet, TYPE);
     }
-    
-    // TODO: Particle id
-    
+
+    /**
+     * Retrieve Particle type.
+     * @return The current Particle type
+     */
+    public Particle getParticleType() {
+        return handle.getParticles().read(0);
+    }
+
+    /**
+     * Set Particle type.
+     * @param value - new value.
+     */
+    public void setParticleType(Particle value) {
+        handle.getParticles().write(0, value);
+    }
+
     /**
      * Retrieve Long Distance.
      * <p>
@@ -26,7 +41,7 @@ public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public boolean getLongDistance() {
         return handle.getSpecificModifier(boolean.class).read(0);
     }
-    
+
     /**
      * Set Long Distance.
      * @param value - new value.
@@ -34,7 +49,7 @@ public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public void setLongDistance(boolean value) {
         handle.getSpecificModifier(boolean.class).write(0, value);
     }
-    
+
     /**
      * Retrieve X.
      * <p>
@@ -44,7 +59,7 @@ public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public float getX() {
         return handle.getFloat().read(0);
     }
-    
+
     /**
      * Set X.
      * @param value - new value.
@@ -52,7 +67,7 @@ public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public void setX(float value) {
         handle.getFloat().write(0, value);
     }
-    
+
     /**
      * Retrieve Y.
      * <p>
@@ -62,7 +77,7 @@ public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public float getY() {
         return handle.getFloat().read(1);
     }
-    
+
     /**
      * Set Y.
      * @param value - new value.
@@ -70,7 +85,7 @@ public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public void setY(float value) {
         handle.getFloat().write(1, value);
     }
-    
+
     /**
      * Retrieve Z.
      * <p>
@@ -80,7 +95,7 @@ public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public float getZ() {
         return handle.getFloat().read(2);
     }
-    
+
     /**
      * Set Z.
      * @param value - new value.
@@ -88,7 +103,7 @@ public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public void setZ(float value) {
         handle.getFloat().write(2, value);
     }
-    
+
     /**
      * Retrieve Offset X.
      * <p>
@@ -98,7 +113,7 @@ public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public float getOffsetX() {
         return handle.getFloat().read(3);
     }
-    
+
     /**
      * Set Offset X.
      * @param value - new value.
@@ -106,7 +121,7 @@ public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public void setOffsetX(float value) {
         handle.getFloat().write(3, value);
     }
-    
+
     /**
      * Retrieve Offset Y.
      * <p>
@@ -116,7 +131,7 @@ public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public float getOffsetY() {
         return handle.getFloat().read(4);
     }
-    
+
     /**
      * Set Offset Y.
      * @param value - new value.
@@ -124,7 +139,7 @@ public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public void setOffsetY(float value) {
         handle.getFloat().write(4, value);
     }
-    
+
     /**
      * Retrieve Offset Z.
      * <p>
@@ -134,7 +149,7 @@ public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public float getOffsetZ() {
         return handle.getFloat().read(5);
     }
-    
+
     /**
      * Set Offset Z.
      * @param value - new value.
@@ -142,7 +157,7 @@ public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public void setOffsetZ(float value) {
         handle.getFloat().write(5, value);
     }
-    
+
     /**
      * Retrieve Particle data.
      * <p>
@@ -152,7 +167,7 @@ public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public float getParticleData() {
         return handle.getFloat().read(6);
     }
-    
+
     /**
      * Set Particle data.
      * @param value - new value.
@@ -160,7 +175,7 @@ public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public void setParticleData(float value) {
         handle.getFloat().write(6, value);
     }
-    
+
     /**
      * Retrieve Number of particles.
      * <p>
@@ -170,7 +185,7 @@ public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public int getNumberOfParticles() {
         return handle.getIntegers().read(0);
     }
-    
+
     /**
      * Set Number of particles.
      * @param value - new value.
@@ -178,7 +193,7 @@ public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public void setNumberOfParticles(int value) {
         handle.getIntegers().write(0, value);
     }
-    
+
     /**
      * Retrieve Data.
      * <p>
@@ -188,7 +203,7 @@ public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public int[] getData() {
         return handle.getIntegerArrays().read(0);
     }
-    
+
     /**
      * Set Data.
      * @param value - new value.
@@ -196,6 +211,5 @@ public class WrapperPlayServerWorldParticles extends AbstractPacket {
     public void setData(int[] value) {
         handle.getIntegerArrays().write(0, value);
     }
-    
+
 }
-
