@@ -18,21 +18,27 @@
  */
 package com.comphenix.packetwrapper;
 
+import java.util.Set;
+
 import com.comphenix.protocol.PacketType;
 import com.comphenix.protocol.events.PacketContainer;
+import com.comphenix.protocol.reflect.StructureModifier;
+import com.comphenix.protocol.utility.MinecraftReflection;
+import com.comphenix.protocol.wrappers.BukkitConverters;
+import com.comphenix.protocol.wrappers.EnumWrappers;
 
 public class WrapperPlayServerPosition extends AbstractPacket {
     public static final PacketType TYPE = PacketType.Play.Server.POSITION;
-    
+
     public WrapperPlayServerPosition() {
         super(new PacketContainer(TYPE), TYPE);
         handle.getModifier().writeDefaults();
     }
-    
+
     public WrapperPlayServerPosition(PacketContainer packet) {
         super(packet, TYPE);
     }
-    
+
     /**
      * Retrieve X.
      * <p>
@@ -42,7 +48,7 @@ public class WrapperPlayServerPosition extends AbstractPacket {
     public double getX() {
         return handle.getDoubles().read(0);
     }
-    
+
     /**
      * Set X.
      * @param value - new value.
@@ -50,7 +56,7 @@ public class WrapperPlayServerPosition extends AbstractPacket {
     public void setX(double value) {
         handle.getDoubles().write(0, value);
     }
-    
+
     /**
      * Retrieve Y.
      * <p>
@@ -60,7 +66,7 @@ public class WrapperPlayServerPosition extends AbstractPacket {
     public double getY() {
         return handle.getDoubles().read(1);
     }
-    
+
     /**
      * Set Y.
      * @param value - new value.
@@ -68,7 +74,7 @@ public class WrapperPlayServerPosition extends AbstractPacket {
     public void setY(double value) {
         handle.getDoubles().write(1, value);
     }
-    
+
     /**
      * Retrieve Z.
      * <p>
@@ -78,7 +84,7 @@ public class WrapperPlayServerPosition extends AbstractPacket {
     public double getZ() {
         return handle.getDoubles().read(2);
     }
-    
+
     /**
      * Set Z.
      * @param value - new value.
@@ -86,7 +92,7 @@ public class WrapperPlayServerPosition extends AbstractPacket {
     public void setZ(double value) {
         handle.getDoubles().write(2, value);
     }
-    
+
     /**
      * Retrieve Yaw.
      * <p>
@@ -96,7 +102,7 @@ public class WrapperPlayServerPosition extends AbstractPacket {
     public float getYaw() {
         return handle.getFloat().read(0);
     }
-    
+
     /**
      * Set Yaw.
      * @param value - new value.
@@ -104,7 +110,7 @@ public class WrapperPlayServerPosition extends AbstractPacket {
     public void setYaw(float value) {
         handle.getFloat().write(0, value);
     }
-    
+
     /**
      * Retrieve Pitch.
      * <p>
@@ -114,7 +120,7 @@ public class WrapperPlayServerPosition extends AbstractPacket {
     public float getPitch() {
         return handle.getFloat().read(1);
     }
-    
+
     /**
      * Set Pitch.
      * @param value - new value.
@@ -122,7 +128,28 @@ public class WrapperPlayServerPosition extends AbstractPacket {
     public void setPitch(float value) {
         handle.getFloat().write(1, value);
     }
-    
-    // TODO: Flags
-}
 
+    private static final Class<?> FLAGS_CLASS = MinecraftReflection.getMinecraftClass("EnumPlayerTeleportFlags",
+            "PacketPlayOutPosition$EnumPlayerTeleportFlags");
+
+    public static enum PlayerTeleportFlag {
+        X,
+        Y,
+        Z,
+        Y_ROT,
+        X_ROT;
+    }
+
+    private StructureModifier<Set<PlayerTeleportFlag>> getFlagsModifier() {
+        return handle.getModifier().withType(Set.class, BukkitConverters.getSetConverter(FLAGS_CLASS,
+                EnumWrappers.getGenericConverter(PlayerTeleportFlag.class)));
+    }
+
+    public Set<PlayerTeleportFlag> getFlags() {
+        return getFlagsModifier().read(0);
+    }
+
+    public void setFlags(Set<PlayerTeleportFlag> value) {
+        getFlagsModifier().write(0, value);
+    }
+}
