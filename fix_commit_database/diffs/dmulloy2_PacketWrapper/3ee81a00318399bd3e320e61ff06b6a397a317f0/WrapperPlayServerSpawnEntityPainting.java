@@ -18,22 +18,29 @@
  */
 package com.comphenix.packetwrapper;
 
+import org.bukkit.World;
+import org.bukkit.entity.Entity;
+
 import com.comphenix.protocol.PacketType;
 import com.comphenix.protocol.events.PacketContainer;
+import com.comphenix.protocol.events.PacketEvent;
+import com.comphenix.protocol.reflect.StructureModifier;
+import com.comphenix.protocol.utility.MinecraftReflection;
 import com.comphenix.protocol.wrappers.BlockPosition;
+import com.comphenix.protocol.wrappers.EnumWrappers;
 
 public class WrapperPlayServerSpawnEntityPainting extends AbstractPacket {
     public static final PacketType TYPE = PacketType.Play.Server.SPAWN_ENTITY_PAINTING;
-    
+
     public WrapperPlayServerSpawnEntityPainting() {
         super(new PacketContainer(TYPE), TYPE);
         handle.getModifier().writeDefaults();
     }
-    
+
     public WrapperPlayServerSpawnEntityPainting(PacketContainer packet) {
         super(packet, TYPE);
     }
-    
+
     /**
      * Retrieve Entity ID.
      * <p>
@@ -43,15 +50,33 @@ public class WrapperPlayServerSpawnEntityPainting extends AbstractPacket {
     public int getEntityID() {
         return handle.getIntegers().read(0);
     }
-    
+
     /**
      * Set Entity ID.
      * @param value - new value.
      */
-    public void setEntityId(int value) {
+    public void setEntityID(int value) {
         handle.getIntegers().write(0, value);
     }
-    
+
+    /**
+     * Retrieve the entity of the painting that will be spawned.
+     * @param world - the current world of the entity.
+     * @return The spawned entity.
+     */
+    public Entity getEntity(World world) {
+        return handle.getEntityModifier(world).read(0);
+    }
+
+    /**
+     * Retrieve the entity of the painting that will be spawned.
+     * @param event - the packet event.
+     * @return The spawned entity.
+     */
+    public Entity getEntity(PacketEvent event) {
+        return getEntity(event.getPlayer().getWorld());
+    }
+
     /**
      * Retrieve Title.
      * <p>
@@ -61,7 +86,7 @@ public class WrapperPlayServerSpawnEntityPainting extends AbstractPacket {
     public String getTitle() {
         return handle.getStrings().read(0);
     }
-    
+
     /**
      * Set Title.
      * @param value - new value.
@@ -69,7 +94,7 @@ public class WrapperPlayServerSpawnEntityPainting extends AbstractPacket {
     public void setTitle(String value) {
         handle.getStrings().write(0, value);
     }
-    
+
     /**
      * Retrieve Location.
      * <p>
@@ -79,7 +104,7 @@ public class WrapperPlayServerSpawnEntityPainting extends AbstractPacket {
     public BlockPosition getLocation() {
         return handle.getBlockPositionModifier().read(0);
     }
-    
+
     /**
      * Set Location.
      * @param value - new value.
@@ -87,8 +112,27 @@ public class WrapperPlayServerSpawnEntityPainting extends AbstractPacket {
     public void setLocation(BlockPosition value) {
         handle.getBlockPositionModifier().write(0, value);
     }
-    
-    // TODO: Direction
-    
-}
 
+    private static final Class<?> DIRECTION_CLASS = MinecraftReflection.getMinecraftClass("EnumDirection");
+
+    public static enum Direction {
+        DOWN,
+        UP,
+        NORTH,
+        SOUTH,
+        WEST,
+        EAST;
+    }
+
+    private StructureModifier<Direction> getDirections() {
+        return handle.getModifier().<Direction> withType(DIRECTION_CLASS, EnumWrappers.getGenericConverter(Direction.class));
+    }
+
+    public Direction getDirection() {
+        return getDirections().read(0);
+    }
+
+    public void setDirection(Direction value) {
+        getDirections().write(0, value);
+    }
+}
