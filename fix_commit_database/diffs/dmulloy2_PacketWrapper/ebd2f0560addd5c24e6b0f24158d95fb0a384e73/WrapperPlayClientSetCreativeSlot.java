@@ -23,16 +23,16 @@ public class WrapperPlayClientSetCreativeSlot extends AbstractPacket {
      * Notes: inventory slot
      * @return The current Slot
      */
-    public short getSlot() {
-        return (short) handle.getIntegers().read(0);
+    public int getSlot() {
+        return handle.getIntegers().read(0);
     }
     
     /**
      * Set Slot.
      * @param value - new value.
      */
-    public void setSlot(short value) {
-        handle.getIntegers().write(0, (int) value);
+    public void setSlot(int value) {
+        handle.getIntegers().write(0, value);
     }
     
     /**
