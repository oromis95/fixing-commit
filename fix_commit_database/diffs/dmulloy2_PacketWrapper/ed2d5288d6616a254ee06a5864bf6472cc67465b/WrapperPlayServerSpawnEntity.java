@@ -39,7 +39,7 @@ public class WrapperPlayServerSpawnEntity extends AbstractPacket {
      * Notes: entity ID of the object
      * @return The current Entity ID
      */
-    public int getEntityId() {
+    public int getEntityID() {
         return handle.getIntegers().read(0);
     }
     
