@@ -40,7 +40,7 @@ public class WrapperPlayServerBed extends AbstractPacket {
      * Notes: player ID
      * @return The current Entity ID
      */
-    public int getEntityId() {
+    public int getEntityID() {
         return handle.getIntegers().read(0);
     }
     
