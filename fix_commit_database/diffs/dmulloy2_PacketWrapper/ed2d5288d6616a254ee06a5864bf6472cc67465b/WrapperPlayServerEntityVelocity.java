@@ -41,7 +41,7 @@ public class WrapperPlayServerEntityVelocity extends AbstractPacket {
      * Notes: entity's ID
      * @return The current Entity ID
      */
-    public int getEntityId() {
+    public int getEntityID() {
         return handle.getIntegers().read(0);
     }
     
