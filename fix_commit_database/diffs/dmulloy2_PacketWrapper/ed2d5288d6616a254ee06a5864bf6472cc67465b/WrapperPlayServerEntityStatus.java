@@ -39,7 +39,7 @@ public class WrapperPlayServerEntityStatus extends AbstractPacket {
      * Notes: entity's ID
      * @return The current Entity ID
      */
-    public int getEntityId() {
+    public int getEntityID() {
         return handle.getIntegers().read(0);
     }
     
