@@ -40,7 +40,7 @@ public class WrapperPlayServerSpawnEntityPainting extends AbstractPacket {
      * Notes: entity's ID
      * @return The current Entity ID
      */
-    public int getEntityId() {
+    public int getEntityID() {
         return handle.getIntegers().read(0);
     }
     
