@@ -42,7 +42,7 @@ public class WrapperPlayServerEntityMetadata extends AbstractPacket {
      * Notes: entity's ID
      * @return The current Entity ID
      */
-    public int getEntityId() {
+    public int getEntityID() {
         return handle.getIntegers().read(0);
     }
     
