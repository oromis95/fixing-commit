@@ -106,7 +106,7 @@ public class WrapperPlayServerEntityEffect extends AbstractPacket {
      * @return The current Hide Particles
      */
     public boolean getHideParticles() {
-        return handle.getBooleans().read(0);
+        return handle.getBytes().read(2) == 0;
     }
     
     /**
@@ -114,7 +114,7 @@ public class WrapperPlayServerEntityEffect extends AbstractPacket {
      * @param value - new value.
      */
     public void setHideParticles(boolean value) {
-        handle.getBooleans().write(0, value);
+        handle.getBytes().write(2, (byte) (value ? 0 : 1));
     }
     
 }
