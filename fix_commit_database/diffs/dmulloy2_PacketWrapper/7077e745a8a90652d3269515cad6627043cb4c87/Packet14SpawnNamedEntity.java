@@ -20,6 +20,7 @@ package com.comphenix.packetwrapper;
 import org.bukkit.World;
 import org.bukkit.entity.Entity;
 import org.bukkit.entity.Player;
+import org.bukkit.util.Vector;
 
 import com.comphenix.protocol.ProtocolLibrary;
 import com.comphenix.protocol.events.PacketContainer;
@@ -109,90 +110,114 @@ public class Packet14SpawnNamedEntity extends AbstractPacket {
     }
     
     /**
-     * Retrieve the player X coordinate as an absolute Integer.
+     * Retrieve the position of the spawned entity as a vector.
+     * @return The position as a vector.
+     */
+    public Vector getPosition() {
+    	return new Vector(getX(), getY(), getZ());
+    }
+    
+    /**
+     * Set the position of the spawned entity using a vector.
+     * @param position - the new position.
+     */
+    public void setPosition(Vector position) {
+    	setX(position.getX());
+    	setY(position.getY());
+    	setZ(position.getZ());
+    }
+    
+    /**
+     * Retrieve the x axis of the position.
+     * <p>
+     * Note that the coordinate is rounded off to the nearest 1/32 of a meter.
      * @return The current X
     */
-    public int getX() {
-        return handle.getIntegers().read(1);
+    public double getX() {
+        return handle.getIntegers().read(1) / 32.0D;
     }
     
     /**
-     * Set the player X as an absolute Integer.
+     * Set the x axis of the position.
      * @param value - new value.
     */
-    public void setX(int value) {
-        handle.getIntegers().write(1, value);
+    public void setX(double value) {
+        handle.getIntegers().write(1, (int) Math.floor(value * 32.0D));
     }
     
     /**
-     * Retrieve player Y as an absolute Integer.
-     * @return The current Y
+     * Retrieve the y axis of the position.
+     * <p>
+     * Note that the coordinate is rounded off to the nearest 1/32 of a meter.
+     * @return The current y
     */
-    public int getY() {
-        return handle.getIntegers().read(2);
+    public double getY() {
+        return handle.getIntegers().read(2) / 32.0D;
     }
     
     /**
-     * Set player Y as an absolute Integer.
+     * Set the y axis of the position.
      * @param value - new value.
     */
-    public void setY(int value) {
-        handle.getIntegers().write(2, value);
+    public void setY(double value) {
+        handle.getIntegers().write(2, (int) Math.floor(value * 32.0D));
     }
     
     /**
-     * Retrieve player Z as an absolute Integer.
-     * @return The current Z
+     * Retrieve the z axis of the new position.
+     * <p>
+     * Note that the coordinate is rounded off to the nearest 1/32 of a meter.
+     * @return The current z
     */
-    public int getZ() {
-        return handle.getIntegers().read(3);
+    public double getZ() {
+        return handle.getIntegers().read(3) / 32.0D;
     }
     
     /**
-     * Set player Z as an absolute Integer.
+     * Set the z axis of the new position.
      * @param value - new value.
     */
-    public void setZ(int value) {
-        handle.getIntegers().write(3, value);
+    public void setZ(double value) {
+        handle.getIntegers().write(3, (int) Math.floor(value * 32.0D));
     }
     
     /**
-     * Retrieve player yaw rotation.
+     * Retrieve the yaw of the spawned entity.
      * @return The current Yaw
     */
     public float getYaw() {
-        return (handle.getBytes().read(0).floatValue() * 360.F) / 256.0F;
+        return (handle.getBytes().read(0) * 360.F) / 256.0F;
     }
     
     /**
-     * Set the player yaw rotation.
-     * @param value - new value.
+     * Set the yaw of the spawned entity.
+     * @param value - new yaw.
     */
     public void setYaw(float value) {
         handle.getBytes().write(0, (byte) (value * 256.0F / 360.0F));
     }
     
     /**
-     * Retrieve player pitch rotation.
-     * @return The current Pitch
+     * Retrieve the pitch of the spawned entity.
+     * @return The current pitch
     */
     public float getPitch() {
-    	return (handle.getBytes().read(1).floatValue() * 360.F) / 256.0F;
+        return (handle.getBytes().read(1) * 360.F) / 256.0F;
     }
     
     /**
-     * Set player pitch rotation.
-     * @param value - new value.
+     * Set the pitch of the spawned entity.
+     * @param value - new pitch.
     */
     public void setPitch(float value) {
-    	 handle.getBytes().write(1, (byte) (value * 256.0F / 360.0F));
+        handle.getBytes().write(1, (byte) (value * 256.0F / 360.0F));
     }
     
     /**
      * Retrieve the item the player is currently holding. 
      * <p>
-     * Note that this should be 0 for "no item", unlike -1 used in other packets. A negative value crashes clients..
-     * @return The current Current Item
+     * Note that this should be 0 for "no item", unlike -1 used in other packets. A negative value crashes clients.
+     * @return The current item.
     */
     public short getCurrentItem() {
         return handle.getIntegers().read(4).shortValue();
@@ -213,7 +238,7 @@ public class Packet14SpawnNamedEntity extends AbstractPacket {
      * <p>
      * Note that the 1.3 client crashes on packets with no metadata, but the server can send any metadata 
      * key of 0, 1 or 8 and the client is fine.
-     * @return The current Metadata
+     * @return The current metadata.
     */
     public WrappedDataWatcher getMetadata() {
         return handle.getDataWatcherModifier().read(0);
