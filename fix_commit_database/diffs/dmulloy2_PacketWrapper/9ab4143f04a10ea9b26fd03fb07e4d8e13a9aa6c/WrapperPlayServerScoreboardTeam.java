@@ -23,6 +23,7 @@ import java.util.List;
 
 import com.comphenix.protocol.PacketType;
 import com.comphenix.protocol.events.PacketContainer;
+import com.comphenix.protocol.reflect.IntEnum;
 
 public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
     public static final PacketType TYPE = PacketType.Play.Server.SCOREBOARD_TEAM;
@@ -35,14 +36,32 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
     public WrapperPlayServerScoreboardTeam(PacketContainer packet) {
         super(packet, TYPE);
     }
-    
+
+    /**
+     * Enum containing all known modes.
+     * @author dmulloy2
+     */
+    public static class Mode extends IntEnum {
+        public static final int TEAM_CREATED = 0;
+        public static final int TEAM_REMOVED = 1;
+        public static final int TEAM_UPDATED = 2;
+        public static final int PLAYERS_ADDED = 3;
+        public static final int PLAYERS_REMOVED = 4;
+
+        private static final Mode INSTANCE = new Mode();
+
+        public static Mode getInstance() {
+            return INSTANCE;
+        }
+    }
+
     /**
      * Retrieve Team Name.
      * <p>
      * Notes: a unique name for the team. (Shared with scoreboard).
      * @return The current Team Name
      */
-    public String getTeamName() {
+    public String getName() {
         return handle.getStrings().read(0);
     }
     
@@ -50,43 +69,25 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
      * Set Team Name.
      * @param value - new value.
      */
-    public void setTeamName(String value) {
+    public void setName(String value) {
         handle.getStrings().write(0, value);
     }
-    
-    /**
-     * Retrieve Mode.
-     * <p>
-     * Notes: if 0 then the team is created. If 1 then the team is removed. If 2 the team team information is updated. If 3 then new players are added to the team. If 4 then players are removed from the team.
-     * @return The current Mode
-     */
-    public int getMode() {
-        return handle.getIntegers().read(1);
-    }
-    
-    /**
-     * Set Mode.
-     * @param value - new value.
-     */
-    public void setMode(int value) {
-        handle.getIntegers().write(1, value);
-    }
-    
+
     /**
      * Retrieve Team Display Name.
      * <p>
      * Notes: only if Mode = 0 or 2.
      * @return The current Team Display Name
      */
-    public String getTeamDisplayName() {
+    public String getDisplayName() {
         return handle.getStrings().read(1);
     }
-    
+
     /**
      * Set Team Display Name.
      * @param value - new value.
      */
-    public void setTeamDisplayName(String value) {
+    public void setDisplayName(String value) {
         handle.getStrings().write(1, value);
     }
     
@@ -96,7 +97,7 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
      * Notes: only if Mode = 0 or 2. Displayed before the players' name that are part of this team.
      * @return The current Team Prefix
      */
-    public String getTeamPrefix() {
+    public String getPrefix() {
         return handle.getStrings().read(2);
     }
     
@@ -104,7 +105,7 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
      * Set Team Prefix.
      * @param value - new value.
      */
-    public void setTeamPrefix(String value) {
+    public void setPrefix(String value) {
         handle.getStrings().write(2, value);
     }
     
@@ -114,7 +115,7 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
      * Notes: only if Mode = 0 or 2. Displayed after the players' name that are part of this team.
      * @return The current Team Suffix
      */
-    public String getTeamSuffix() {
+    public String getSuffix() {
         return handle.getStrings().read(3);
     }
     
@@ -122,28 +123,10 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
      * Set Team Suffix.
      * @param value - new value.
      */
-    public void setTeamSuffix(String value) {
+    public void setSuffix(String value) {
         handle.getStrings().write(3, value);
     }
-    
-    /**
-     * Retrieve Friendly fire.
-     * <p>
-     * Notes: only if Mode = 0 or 2; 0 for off, 1 for on, 3 for seeing friendly invisibles
-     * @return The current Friendly fire
-     */
-    public int getFriendlyFire() {
-        return handle.getIntegers().read(1);
-    }
-    
-    /**
-     * Set Friendly fire.
-     * @param value - new value.
-     */
-    public void setFriendlyFire(int value) {
-        handle.getIntegers().write(1, value);
-    }
-    
+
     /**
      * Retrieve Name Tag Visibility.
      * <p>
@@ -161,7 +144,7 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
     public void setNameTagVisibility(String value) {
         handle.getStrings().write(4, value);
     }
-    
+
     /**
      * Retrieve Color.
      * <p>
@@ -169,7 +152,7 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
      * @return The current Color
      */
     public int getColor() {
-        return handle.getIntegers().read(2);
+        return handle.getIntegers().read(0);
     }
     
     /**
@@ -177,27 +160,9 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
      * @param value - new value.
      */
     public void setColor(int value) {
-        handle.getIntegers().write(2, value);
-    }
-    
-    /**
-     * Retrieve Player count.
-     * <p>
-     * Notes: only if Mode = 0 or 3 or 4. Number of players in the array
-     * @return The current Player count
-     */
-    public int getPlayerCount() {
-        return handle.getIntegers().read(0);
-    }
-    
-    /**
-     * Set Player count.
-     * @param value - new value.
-     */
-    public void setPlayerCount(int value) {
         handle.getIntegers().write(0, value);
     }
-    
+
     /**
      * Retrieve Players.
      * <p>
@@ -216,6 +181,48 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
     public void setPlayers(List<String> value) {
         handle.getSpecificModifier(Collection.class).write(0, value);
     }
+
+    /**
+     * Retrieve Mode.
+     * <p>
+     * Notes: if 0 then the team is created. If 1 then the team is removed. If 2 the team team information is updated. If 3 then new players are added to the team. If 4 then players are removed from the team.
+     * @return The current Mode
+     */
+    public int getMode() {
+        return handle.getIntegers().read(1);
+    }
     
-}
+    /**
+     * Set Mode.
+     * @param value - new value.
+     */
+    public void setMode(int value) {
+        handle.getIntegers().write(1, value);
+    }
+
+    /**
+     * Retrieve pack option data. Pack data is calculated as follows.
+     * <pre><code>
+     * int data = 0;
+     * if (team.allowFriendlyFire()) {
+     *     i |= 1;
+     * }
+     * if (team.canSeeFriendlyInvisibles()) {
+     *     i |= 2;
+     * }
+     * </code></pre>
+     * @return The current pack option data
+     */
+    public int getPackOptionData() {
+        return handle.getIntegers().read(2);
+    }
 
+    /**
+     * Set pack option data.
+     * @param value - new value
+     * @see #getPackOptionData()
+     */
+    public void setPackOptionData(int value) {
+        handle.getIntegers().write(2, value);
+    }
+}
\ No newline at end of file
