@@ -81,7 +81,7 @@ public class WrapperPlayClientCustomPayload extends AbstractPacket {
 	/**
 	 * Update payload contents with a Netty buffer
 	 * 
-	 * @param content - new payload content
+	 * @param contents - new payload contents
 	 */
 	public void setContentsBuffer(ByteBuf contents) {
 		if (MinecraftReflection.is(MinecraftReflection.getPacketDataSerializerClass(), contents)) {
