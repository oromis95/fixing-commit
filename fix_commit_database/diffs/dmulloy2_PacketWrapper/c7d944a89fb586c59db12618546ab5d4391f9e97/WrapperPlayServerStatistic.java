@@ -1,20 +1,20 @@
 /**
- * This file is part of PacketWrapper.
- * Copyright (C) 2012-2015 Kristian S. Strangeland
- * Copyright (C) 2015 dmulloy2
+ * PacketWrapper - ProtocolLib wrappers for Minecraft packets
+ * Copyright (C) dmulloy2 <http://dmulloy2.net>
+ * Copyright (C) Kristian S. Strangeland
  *
- * PacketWrapper is free software: you can redistribute it and/or modify
- * it under the terms of the GNU Lesser General Public License as published by
+ * This program is free software: you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
- * PacketWrapper is distributed in the hope that it will be useful,
+ * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
- * You should have received a copy of the GNU Lesser General Public License
- * along with PacketWrapper.  If not, see <http://www.gnu.org/licenses/>.
+ * You should have received a copy of the GNU General Public License
+ * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */
 package com.comphenix.packetwrapper;
 
@@ -25,24 +25,23 @@ import com.comphenix.protocol.events.PacketContainer;
 import com.comphenix.protocol.wrappers.WrappedStatistic;
 
 public class WrapperPlayServerStatistic extends AbstractPacket {
-    public static final PacketType TYPE = PacketType.Play.Server.STATISTIC;
-    
-    public WrapperPlayServerStatistic() {
-        super(new PacketContainer(TYPE), TYPE);
-        handle.getModifier().writeDefaults();
-    }
-    
-    public WrapperPlayServerStatistic(PacketContainer packet) {
-        super(packet, TYPE);
-    }
-    
-    @SuppressWarnings("unchecked")
-    public Map<WrappedStatistic, Integer> getStatistics() {
-        return handle.getSpecificModifier(Map.class).read(0);
-    }
+	public static final PacketType TYPE = PacketType.Play.Server.STATISTIC;
 
-    public void setStatistics(Map<WrappedStatistic, Integer> value) {
-        handle.getSpecificModifier(Map.class).write(0, value);
-    }
-}
+	public WrapperPlayServerStatistic() {
+		super(new PacketContainer(TYPE), TYPE);
+		handle.getModifier().writeDefaults();
+	}
+
+	public WrapperPlayServerStatistic(PacketContainer packet) {
+		super(packet, TYPE);
+	}
 
+	@SuppressWarnings("unchecked")
+	public Map<WrappedStatistic, Integer> getStatistics() {
+		return handle.getSpecificModifier(Map.class).read(0);
+	}
+
+	public void setStatistics(Map<WrappedStatistic, Integer> value) {
+		handle.getSpecificModifier(Map.class).write(0, value);
+	}
+}
