@@ -1,20 +1,20 @@
 /**
- * This file is part of PacketWrapper.
- * Copyright (C) 2012-2015 Kristian S. Strangeland
- * Copyright (C) 2015 dmulloy2
+ * PacketWrapper - ProtocolLib wrappers for Minecraft packets
+ * Copyright (C) dmulloy2 <http://dmulloy2.net>
+ * Copyright (C) Kristian S. Strangeland
  *
- * PacketWrapper is free software: you can redistribute it and/or modify
- * it under the terms of the GNU Lesser General Public License as published by
+ * This program is free software: you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
- * PacketWrapper is distributed in the hope that it will be useful,
+ * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
- * You should have received a copy of the GNU Lesser General Public License
- * along with PacketWrapper.  If not, see <http://www.gnu.org/licenses/>.
+ * You should have received a copy of the GNU General Public License
+ * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */
 package com.comphenix.packetwrapper;
 
@@ -33,6 +33,7 @@ public abstract class AbstractPacket {
 
 	/**
 	 * Constructs a new strongly typed wrapper for the given packet.
+	 * 
 	 * @param handle - handle to the raw packet data.
 	 * @param type - the packet type.
 	 */
@@ -41,13 +42,15 @@ public abstract class AbstractPacket {
 		if (handle == null)
 			throw new IllegalArgumentException("Packet handle cannot be NULL.");
 		if (!Objects.equal(handle.getType(), type))
-			throw new IllegalArgumentException(handle.getHandle() + " is not a packet of type " + type);
+			throw new IllegalArgumentException(handle.getHandle()
+					+ " is not a packet of type " + type);
 
 		this.handle = handle;
 	}
 
 	/**
 	 * Retrieve a handle to the raw packet data.
+	 * 
 	 * @return Raw packet data.
 	 */
 	public PacketContainer getHandle() {
@@ -56,12 +59,14 @@ public abstract class AbstractPacket {
 
 	/**
 	 * Send the current packet to the given receiver.
+	 * 
 	 * @param receiver - the receiver.
 	 * @throws RuntimeException If the packet cannot be sent.
 	 */
 	public void sendPacket(Player receiver) {
 		try {
-			ProtocolLibrary.getProtocolManager().sendServerPacket(receiver, getHandle());
+			ProtocolLibrary.getProtocolManager().sendServerPacket(receiver,
+					getHandle());
 		} catch (InvocationTargetException e) {
 			throw new RuntimeException("Cannot send packet.", e);
 		}
@@ -69,6 +74,7 @@ public abstract class AbstractPacket {
 
 	/**
 	 * Simulate receiving the current packet from the given sender.
+	 * 
 	 * @param sender - the sender.
 	 * @throws RuntimeException If the packet cannot be received.
 	 * @deprecated Misspelled. recieve -> receive
@@ -77,7 +83,8 @@ public abstract class AbstractPacket {
 	@Deprecated
 	public void recievePacket(Player sender) {
 		try {
-			ProtocolLibrary.getProtocolManager().recieveClientPacket(sender, getHandle());
+			ProtocolLibrary.getProtocolManager().recieveClientPacket(sender,
+					getHandle());
 		} catch (Exception e) {
 			throw new RuntimeException("Cannot recieve packet.", e);
 		}
@@ -85,12 +92,14 @@ public abstract class AbstractPacket {
 
 	/**
 	 * Simulate receiving the current packet from the given sender.
+	 * 
 	 * @param sender - the sender.
 	 * @throws RuntimeException if the packet cannot be received.
 	 */
 	public void receivePacket(Player sender) {
 		try {
-			ProtocolLibrary.getProtocolManager().recieveClientPacket(sender, getHandle());
+			ProtocolLibrary.getProtocolManager().recieveClientPacket(sender,
+					getHandle());
 		} catch (Exception e) {
 			throw new RuntimeException("Cannot recieve packet.", e);
 		}
