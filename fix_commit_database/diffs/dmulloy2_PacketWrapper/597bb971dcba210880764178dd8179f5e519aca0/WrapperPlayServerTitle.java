@@ -2,6 +2,7 @@ package com.comphenix.packetwrapper;
 
 import com.comphenix.protocol.PacketType;
 import com.comphenix.protocol.events.PacketContainer;
+import com.comphenix.protocol.wrappers.EnumWrappers.TitleAction;
 import com.comphenix.protocol.wrappers.WrappedChatComponent;
 
 public class WrapperPlayServerTitle extends AbstractPacket {
@@ -20,27 +21,25 @@ public class WrapperPlayServerTitle extends AbstractPacket {
      * Retrieve Action.
      * @return The current Action
      */
-    public int getAction() {
-        return (int) handle.getSpecificModifier(Enum.class).read(0);
+    public TitleAction getAction() {
+        return handle.getTitleActions().read(0);
     }
     
     /**
      * Set Action.
      * @param value - new value.
      */
-    public void setAction(int value) {
-        handle.getSpecificModifier(Enum.class).write(0, (Enum<?>) value);
+    public void setAction(TitleAction value) {
+        handle.getTitleActions().write(0,  value);
     }
-    
-    // Cannot generate getter a
-    // Cannot generate setter a
+
     /**
      * Retrieve 0 (TITLE).
      * <p>
      * Notes: chat
      * @return The current 0 (TITLE)
      */
-    public WrappedChatComponent get0Title() {
+    public WrappedChatComponent getTitle() {
         return handle.getChatComponents().read(0);
     }
     
@@ -48,44 +47,26 @@ public class WrapperPlayServerTitle extends AbstractPacket {
      * Set 0 (TITLE).
      * @param value - new value.
      */
-    public void set0Title(WrappedChatComponent value) {
+    public void setTitle(WrappedChatComponent value) {
         handle.getChatComponents().write(0, value);
     }
     
-    /**
-     * Retrieve 1 (SUBTITLE).
-     * <p>
-     * Notes: chat
-     * @return The current 1 (SUBTITLE)
-     */
-    public text get1Subtitle() {
-        return (text) handle.getSpecificModifier(Enum.class).read(0);
-    }
-    
-    /**
-     * Set 1 (SUBTITLE).
-     * @param value - new value.
-     */
-    public void set1Subtitle(text value) {
-        handle.getSpecificModifier(Enum.class).write(0, (Enum<?>) value);
-    }
-    
     /**
      * Retrieve 2 (TIMES).
      * <p>
      * Notes: int
      * @return The current 2 (TIMES)
      */
-    public fadein get2Times() {
-        return (fadein) handle.getIntegers().read(0);
+    public int getFadeIn() {
+        return handle.getIntegers().read(0);
     }
     
     /**
      * Set 2 (TIMES).
      * @param value - new value.
      */
-    public void set2Times(fadein value) {
-        handle.getIntegers().write(0, (int) value);
+    public void setFadeIn(int value) {
+        handle.getIntegers().write(0, value);
     }
     
     /**
@@ -119,8 +100,5 @@ public class WrapperPlayServerTitle extends AbstractPacket {
     public void setFadeOut(int value) {
         handle.getIntegers().write(2, value);
     }
-    
-    // Cannot generate field 3 (CLEAR)
-    // Cannot generate field 4 (RESET)
 }
 
