@@ -2,6 +2,7 @@ package com.comphenix.packetwrapper;
 
 import com.comphenix.protocol.PacketType;
 import com.comphenix.protocol.events.PacketContainer;
+import com.comphenix.protocol.wrappers.EnumWrappers.Difficulty;
 
 public class WrapperPlayServerServerDifficulty extends AbstractPacket {
     public static final PacketType TYPE = PacketType.Play.Server.SERVER_DIFFICULTY;
@@ -21,16 +22,16 @@ public class WrapperPlayServerServerDifficulty extends AbstractPacket {
      * Notes: 0:PEACEFUL, 1:EASY, 2:NORMAL, 3: HARD
      * @return The current Difficulty
      */
-    public byte getDifficulty() {
-        return (byte) handle.getSpecificModifier(Enum.class).read(0);
+    public Difficulty getDifficulty() {
+        return handle.getDifficulties().read(0);
     }
     
     /**
      * Set Difficulty.
      * @param value - new value.
      */
-    public void setDifficulty(byte value) {
-        handle.getSpecificModifier(Enum.class).write(0, (Enum<?>) value);
+    public void setDifficulty(Difficulty value) {
+        handle.getDifficulties().write(0, value);
     }
     
 }
