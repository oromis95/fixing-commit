@@ -2,7 +2,6 @@ package com.comphenix.packetwrapper;
 
 import com.comphenix.protocol.PacketType;
 import com.comphenix.protocol.events.PacketContainer;
-import com.comphenix.protocol.wrappers.BlockPosition;
 
 public class WrapperPlayServerUpdateAttributes extends AbstractPacket {
     public static final PacketType TYPE = PacketType.Play.Server.UPDATE_ATTRIBUTES;
@@ -33,40 +32,8 @@ public class WrapperPlayServerUpdateAttributes extends AbstractPacket {
     public void setEntityId(int value) {
         handle.getIntegers().write(0, value);
     }
-    
-    /**
-     * Retrieve Count.
-     * <p>
-     * Notes: length of following array
-     * @return The current Count
-     */
-    public List<BlockPosition> getCount() {
-        return handle.getPositionCollectionModifier().read(0);
-    }
-    
-    /**
-     * Set Count.
-     * @param value - new value.
-     */
-    public void setCount(List<BlockPosition> value) {
-        handle.getPositionCollectionModifier().write(0, value);
-    }
-    
-    /**
-     * Retrieve Properties.
-     * @return The current Properties
-     */
-    public List<BlockPosition> getProperties() {
-        return handle.getPositionCollectionModifier().read(0);
-    }
-    
-    /**
-     * Set Properties.
-     * @param value - new value.
-     */
-    public void setProperties(List<BlockPosition> value) {
-        handle.getPositionCollectionModifier().write(0, value);
-    }
+
+    // TODO: attributes
     
 }
 
