@@ -41,7 +41,7 @@ public class WrapperPlayServerWorldEvent extends AbstractPacket {
      * @return The current Location
      */
     public BlockPosition getLocation() {
-        return handle.getBlockPositions().read(0);
+        return handle.getBlockPositionModifier().read(0);
     }
     
     /**
@@ -49,7 +49,7 @@ public class WrapperPlayServerWorldEvent extends AbstractPacket {
      * @param value - new value.
      */
     public void setLocation(BlockPosition value) {
-        handle.getBlockPositions().write(0, value);
+        handle.getBlockPositionModifier().write(0, value);
     }
     
     /**
@@ -77,7 +77,7 @@ public class WrapperPlayServerWorldEvent extends AbstractPacket {
      * @return The current Disable relative volume
      */
     public boolean getDisableRelativeVolume() {
-        return (boolean) handle.getSpecificModifier(boolean.class).read(0);
+        return handle.getSpecificModifier(boolean.class).read(0);
     }
     
     /**
@@ -85,7 +85,7 @@ public class WrapperPlayServerWorldEvent extends AbstractPacket {
      * @param value - new value.
      */
     public void setDisableRelativeVolume(boolean value) {
-        handle.getSpecificModifier(boolean.class).write(0, (boolean) value);
+        handle.getSpecificModifier(boolean.class).write(0, value);
     }
     
 }
