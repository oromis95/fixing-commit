@@ -59,7 +59,7 @@ public class WrapperPlayServerSpawnEntityPainting extends AbstractPacket {
      * @return The current Location
      */
     public BlockPosition getLocation() {
-        return handle.getBlockPositions().read(0);
+        return handle.getBlockPositionModifier().read(0);
     }
     
     /**
@@ -67,26 +67,10 @@ public class WrapperPlayServerSpawnEntityPainting extends AbstractPacket {
      * @param value - new value.
      */
     public void setLocation(BlockPosition value) {
-        handle.getBlockPositions().write(0, value);
+        handle.getBlockPositionModifier().write(0, value);
     }
     
-    /**
-     * Retrieve Direction.
-     * <p>
-     * Notes: direction the painting faces (0 -z, 1 -x, 2 +z, 3 +x)
-     * @return The current Direction
-     */
-    public byte getDirection() {
-        return (byte) handle.getSpecificModifier(Enum.class).read(0);
-    }
-    
-    /**
-     * Set Direction.
-     * @param value - new value.
-     */
-    public void setDirection(byte value) {
-        handle.getSpecificModifier(Enum.class).write(0, (Enum<?>) value);
-    }
+    // TODO: Direction
     
 }
 
