@@ -1,7 +1,5 @@
 package com.comphenix.packetwrapper;
 
-import java.util.Set;
-
 import com.comphenix.protocol.PacketType;
 import com.comphenix.protocol.events.PacketContainer;
 
@@ -107,28 +105,6 @@ public class WrapperPlayServerPosition extends AbstractPacket {
         handle.getFloat().write(1, value);
     }
     
-    /**
-     * Retrieve Flags.
-     * <p>
-     * Notes: x 0x01 Y 0x02 Z 0x04 Y_ROT 0x08 X_ROT 0x10
-     * @return The current Flags
-     */
-    public byte getFlags() {
-        return (byte) handle.getSpecificModifier(Set.class).read(0);
-    }
-    
-    /**
-     * Set Flags.
-     * @param value - new value.
-     */
-    public void setFlags(byte value) {
-        handle.getSpecificModifier(Set.class).write(0, (Set<?>) value);
-    }
-    
-    // Cannot generate field X
-    // Cannot generate field Y
-    // Cannot generate field Z
-    // Cannot generate field Y_ROT
-    // Cannot generate field X_ROT
+    // TODO: Flags
 }
 
