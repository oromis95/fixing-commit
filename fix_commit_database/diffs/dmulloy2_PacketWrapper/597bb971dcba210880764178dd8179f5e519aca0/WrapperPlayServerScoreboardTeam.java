@@ -1,5 +1,8 @@
 package com.comphenix.packetwrapper;
 
+import java.util.Collection;
+import java.util.List;
+
 import com.comphenix.protocol.PacketType;
 import com.comphenix.protocol.events.PacketContainer;
 
@@ -39,16 +42,16 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
      * Notes: if 0 then the team is created. If 1 then the team is removed. If 2 the team team information is updated. If 3 then new players are added to the team. If 4 then players are removed from the team.
      * @return The current Mode
      */
-    public byte getMode() {
-        return (byte) handle.getIntegers().read(1);
+    public int getMode() {
+        return handle.getIntegers().read(1);
     }
     
     /**
      * Set Mode.
      * @param value - new value.
      */
-    public void setMode(byte value) {
-        handle.getIntegers().write(1, (int) value);
+    public void setMode(int value) {
+        handle.getIntegers().write(1, value);
     }
     
     /**
@@ -58,7 +61,7 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
      * @return The current Team Display Name
      */
     public String getTeamDisplayName() {
-        return (String) handle.getIntegers().read(1);
+        return handle.getStrings().read(1);
     }
     
     /**
@@ -66,7 +69,7 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
      * @param value - new value.
      */
     public void setTeamDisplayName(String value) {
-        handle.getIntegers().write(1, (int) value);
+        handle.getStrings().write(1, value);
     }
     
     /**
@@ -76,7 +79,7 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
      * @return The current Team Prefix
      */
     public String getTeamPrefix() {
-        return handle.getStrings().read(1);
+        return handle.getStrings().read(2);
     }
     
     /**
@@ -84,7 +87,7 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
      * @param value - new value.
      */
     public void setTeamPrefix(String value) {
-        handle.getStrings().write(1, value);
+        handle.getStrings().write(2, value);
     }
     
     /**
@@ -94,7 +97,7 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
      * @return The current Team Suffix
      */
     public String getTeamSuffix() {
-        return handle.getStrings().read(2);
+        return handle.getStrings().read(3);
     }
     
     /**
@@ -102,7 +105,7 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
      * @param value - new value.
      */
     public void setTeamSuffix(String value) {
-        handle.getStrings().write(2, value);
+        handle.getStrings().write(3, value);
     }
     
     /**
@@ -111,16 +114,16 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
      * Notes: only if Mode = 0 or 2; 0 for off, 1 for on, 3 for seeing friendly invisibles
      * @return The current Friendly fire
      */
-    public byte getFriendlyFire() {
-        return (byte) handle.getStrings().read(3);
+    public int getFriendlyFire() {
+        return handle.getIntegers().read(1);
     }
     
     /**
      * Set Friendly fire.
      * @param value - new value.
      */
-    public void setFriendlyFire(byte value) {
-        handle.getStrings().write(3, (String) value);
+    public void setFriendlyFire(int value) {
+        handle.getIntegers().write(1, value);
     }
     
     /**
@@ -130,7 +133,7 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
      * @return The current Name Tag Visibility
      */
     public String getNameTagVisibility() {
-        return (String) handle.getIntegers().read(2);
+        return handle.getStrings().read(4);
     }
     
     /**
@@ -138,7 +141,7 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
      * @param value - new value.
      */
     public void setNameTagVisibility(String value) {
-        handle.getIntegers().write(2, (int) value);
+        handle.getStrings().write(4, value);
     }
     
     /**
@@ -147,16 +150,16 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
      * Notes: only if Mode = 0 or 2. Same as Chat colors.
      * @return The current Color
      */
-    public byte getColor() {
-        return (byte) handle.getStrings().read(4);
+    public int getColor() {
+        return handle.getIntegers().read(2);
     }
     
     /**
      * Set Color.
      * @param value - new value.
      */
-    public void setColor(byte value) {
-        handle.getStrings().write(4, (String) value);
+    public void setColor(int value) {
+        handle.getIntegers().write(2, value);
     }
     
     /**
@@ -183,16 +186,17 @@ public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
      * Notes: only if Mode = 0 or 3 or 4. Players to be added/remove from the team. Max 40 characters so may be uuid's later
      * @return The current Players
      */
-    public ofStrings[] getPlayers() {
-        return (ofStrings[]) handle.getIntegers().read(1);
+    @SuppressWarnings("unchecked")
+    public List<String> getPlayers() {
+        return (List<String>) handle.getSpecificModifier(Collection.class).read(0);
     }
     
     /**
      * Set Players.
      * @param value - new value.
      */
-    public void setPlayers(ofStrings[] value) {
-        handle.getIntegers().write(1, (int) value);
+    public void setPlayers(List<String> value) {
+        handle.getSpecificModifier(Collection.class).write(0, value);
     }
     
 }
