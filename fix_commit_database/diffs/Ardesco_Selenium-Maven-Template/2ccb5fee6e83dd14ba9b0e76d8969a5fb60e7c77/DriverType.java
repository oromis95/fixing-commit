@@ -24,6 +24,7 @@ public enum DriverType implements DriverSetup {
         public RemoteWebDriver getWebDriverObject(DesiredCapabilities capabilities) {
             FirefoxOptions options = new FirefoxOptions();
             options.merge(capabilities);
+            options.setHeadless(HEADLESS);
 
             return new FirefoxDriver(options);
         }
@@ -35,6 +36,7 @@ public enum DriverType implements DriverSetup {
 
             ChromeOptions options = new ChromeOptions();
             options.merge(capabilities);
+            options.setHeadless(HEADLESS);
             options.addArguments("--no-default-browser-check");
             options.setExperimentalOption("prefs", chromePreferences);
 
@@ -75,28 +77,12 @@ public enum DriverType implements DriverSetup {
 
             return new OperaDriver(options);
         }
-    },
-    CHROME_HEADLESS {
-        public RemoteWebDriver getWebDriverObject(DesiredCapabilities capabilities) {
-            HashMap<String, Object> chromePreferences = new HashMap<>();
-            chromePreferences.put("profile.password_manager_enabled", false);
+    };
 
-            ChromeOptions options = new ChromeOptions();
-            options.merge(capabilities);
-            options.setHeadless(true);
-            options.addArguments("--no-default-browser-check");
-            options.setExperimentalOption("prefs", chromePreferences);
+    public final static boolean HEADLESS = Boolean.getBoolean("headless");
 
-            return new ChromeDriver(options);
-        }
-    },
-    FIREFOX_HEADLESS {
-        public RemoteWebDriver getWebDriverObject(DesiredCapabilities capabilities) {
-            FirefoxOptions options = new FirefoxOptions();
-            options.merge(capabilities);
-            options.setHeadless(true);
-
-            return new FirefoxDriver(options);
-        }
-    };
+    @Override
+    public String toString() {
+        return super.toString().toLowerCase();
+    }
 }
\ No newline at end of file
