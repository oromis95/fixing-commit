@@ -3,7 +3,7 @@ package com.lazerycode.selenium.config;
 import org.openqa.selenium.Proxy;
 import org.openqa.selenium.WebDriver;
 import org.openqa.selenium.chrome.ChromeDriver;
-import org.openqa.selenium.firefox.MarionetteDriver;
+import org.openqa.selenium.firefox.FirefoxDriver;
 import org.openqa.selenium.ie.InternetExplorerDriver;
 import org.openqa.selenium.opera.OperaDriver;
 import org.openqa.selenium.phantomjs.PhantomJSDriver;
@@ -23,11 +23,12 @@ public enum DriverType implements DriverSetup {
     FIREFOX {
         public DesiredCapabilities getDesiredCapabilities(Proxy proxySettings) {
             DesiredCapabilities capabilities = DesiredCapabilities.firefox();
+            capabilities.setCapability("marionette", true);
             return addProxySettings(capabilities, proxySettings);
         }
 
         public WebDriver getWebDriverObject(DesiredCapabilities capabilities) {
-            return new MarionetteDriver(capabilities);
+            return new FirefoxDriver(capabilities);
         }
     },
     CHROME {
