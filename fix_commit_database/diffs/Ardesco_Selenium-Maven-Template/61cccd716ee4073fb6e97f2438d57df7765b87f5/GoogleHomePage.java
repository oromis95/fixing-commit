@@ -1,13 +1,20 @@
 package com.lazerycode.selenium.page_objects;
 
+import com.lazerycode.selenium.DriverBase;
 import com.lazerycode.selenium.util.Query;
 import org.openqa.selenium.By;
+import org.openqa.selenium.remote.RemoteWebDriver;
 
 public class GoogleHomePage {
 
-    Query searchBar = new Query(By.name("q"));
-    Query googleSearch = new Query(By.name("btnK"));
-    Query imFeelingLucky = new Query(By.name("btnI"));
+    private final RemoteWebDriver driver = DriverBase.getDriver();
+
+    private Query searchBar = new Query(By.name("q"), driver);
+    private Query googleSearch = new Query(By.name("btnK"), driver);
+    private Query imFeelingLucky = new Query(By.name("btnI"), driver);
+
+    public GoogleHomePage() throws Exception {
+    }
 
     public GoogleHomePage enterSearchTerm(String searchTerm) {
         searchBar.findWebElement().clear();
@@ -26,4 +33,4 @@ public class GoogleHomePage {
         imFeelingLucky.findWebElement().click();
     }
 
-}
+}
\ No newline at end of file
