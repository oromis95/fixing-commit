@@ -1,22 +1,22 @@
 package com.lazerycode.selenium;
 
 public enum BrowserType {
-    FIREFOX("firefox"),
-    CHROME("chrome"),
-    IE("ie"),
-    SAFARI("safari"),
-    OPERA("opera"),
-    GHOSTDRIVER("ghostdriver"),
-    REMOTE("remote"),
-    HTMLUNIT("htmlunit");
+  FIREFOX("firefox"),
+  CHROME("chrome"),
+  IE("ie"),
+  SAFARI("safari"),
+  OPERA("opera"),
+  GHOSTDRIVER("ghostdriver"),
+  REMOTE("remote"),
+  HTMLUNIT("htmlunit");
 
-    private final String browser;
+  private final String browser;
 
-    BrowserType(String browser) {
-        this.browser = browser;
-    }
+  BrowserType(String browser) {
+    this.browser = browser;
+  }
 
-    public String getBrowser() {
-        return browser;
-    }
+  public String getBrowser() {
+    return browser;
+  }
 }
