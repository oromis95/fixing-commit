@@ -8,15 +8,22 @@ import io.dropwizard.setup.Bootstrap;
 import io.dropwizard.setup.Environment;
 import io.dropwizard.util.Generics;
 
-public abstract class FlywayBundle<T extends Configuration> implements Bundle, DatabaseConfiguration<T> {
+public abstract class FlywayBundle<T extends Configuration>
+        implements Bundle, DatabaseConfiguration<T>, FlywayConfiguration<T> {
     @Override
     public final void initialize(final Bootstrap<?> bootstrap) {
         final Class<T> klass = Generics.getTypeParameter(getClass(), Configuration.class);
-        bootstrap.addCommand(new DbCommand<T>(this, klass));
+        bootstrap.addCommand(new DbCommand<T>(this, this, klass));
     }
 
     @Override
     public final void run(final Environment environment) {
         // nothing doing
     }
+
+    @Override
+    public FlywayFactory getFlywayFactory(T configuration) {
+        // Default Flyway configuration
+        return new FlywayFactory();
+    }
 }
