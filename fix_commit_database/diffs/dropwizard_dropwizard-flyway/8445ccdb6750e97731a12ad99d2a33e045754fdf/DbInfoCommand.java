@@ -1,5 +1,6 @@
 package com.github.joschi.dropwizard.flyway.cli;
 
+import com.github.joschi.dropwizard.flyway.FlywayConfiguration;
 import io.dropwizard.Configuration;
 import io.dropwizard.db.DatabaseConfiguration;
 import net.sourceforge.argparse4j.inf.Namespace;
@@ -8,8 +9,11 @@ import org.flywaydb.core.Flyway;
 import static org.flywaydb.core.internal.info.MigrationInfoDumper.dumpToAsciiTable;
 
 public class DbInfoCommand<T extends Configuration> extends AbstractFlywayCommand<T> {
-    public DbInfoCommand(final DatabaseConfiguration<T> databaseConfiguration, final Class<T> configurationClass) {
-        super("info", "Prints the details and status information about all the migrations.", databaseConfiguration, configurationClass);
+    public DbInfoCommand(final DatabaseConfiguration<T> databaseConfiguration,
+                         final FlywayConfiguration<T> flywayConfiguration,
+                         final Class<T> configurationClass) {
+        super("info", "Prints the details and status information about all the migrations.",
+                databaseConfiguration, flywayConfiguration, configurationClass);
     }
 
     @Override
