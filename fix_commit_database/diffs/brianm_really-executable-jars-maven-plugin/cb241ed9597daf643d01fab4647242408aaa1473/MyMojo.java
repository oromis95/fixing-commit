@@ -1,3 +1,16 @@
+/*
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ * http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
 package org.skife.waffles;
 
 import org.apache.maven.artifact.Artifact;
@@ -20,11 +33,14 @@ import java.util.ArrayList;
 import java.util.List;
 
 /**
+ * Make an artifact generated by the build really executable. The resulting artifact
+ * can be run directly from the command line (Java must be installed and in the
+ * shell path).
+ *
  * @goal really-executable-jar
  */
 public class MyMojo extends AbstractMojo
 {
-
     /**
      * The Maven project.
      *
@@ -42,21 +58,21 @@ public class MyMojo extends AbstractMojo
     private MavenProjectHelper projectHelper;
 
     /**
-     * The greeting to display.
+     * Java command line arguments to embed. Only used with the default stanza.
      *
      * @parameter
      */
     private String flags = "";
 
     /**
-     * The greeting to display.
+     * Name of the generated binary.
      *
      * @parameter
      */
     private String programFile = null;
 
     /**
-     * Classifier of the artifact to make executable
+     * Specifies the classifier of the artifact that will be made executable.
      *
      * @parameter
      */
@@ -77,6 +93,7 @@ public class MyMojo extends AbstractMojo
     private String programFileType = "sh";
 
     /**
+     * Shell script to add to the jar instead of the default stanza.
      *
      * @parameter
      * @throws MojoExecutionException
@@ -141,7 +158,7 @@ public class MyMojo extends AbstractMojo
     }
 
     private void makeExecutable(File file)
-            throws IOException
+            throws IOException,MojoExecutionException
     {
         getLog().debug("Making " + file.getAbsolutePath() + " executable");
 
@@ -149,25 +166,67 @@ public class MyMojo extends AbstractMojo
         try {
             FileUtils.rename(file, oldJarStorage);
 
-            FileOutputStream out = new FileOutputStream(file);
-            FileInputStream in = new FileInputStream(oldJarStorage);
-            if (scriptFile == null) {
-                out.write(("#!/bin/sh\n\nexec java " + flags + " -jar \"$0\" \"$@\"\n\n").getBytes("ASCII"));
+            FileOutputStream out = null;
+            FileInputStream in = null;
+            InputStream scriptIn = null;
+            try {
+                out = new FileOutputStream(file);
+                in = new FileInputStream(oldJarStorage);
+                if (scriptFile == null) {
+                    out.write(("#!/bin/sh\n\nexec java " + flags + " -jar \"$0\" \"$@\"\n\n").getBytes("ASCII"));
+                }
+                else {
+                    getLog().debug(String.format("Loading file[%s] from jar[%s]", scriptFile, oldJarStorage));
+                    final URLClassLoader loader = new URLClassLoader(new URL[]{oldJarStorage.toURI().toURL()}, null);
+                    scriptIn = loader.getResourceAsStream(scriptFile);
+                    out.write(IOUtil.toString(scriptIn).getBytes("ASCII"));
+                    out.write("\n\n".getBytes("ASCII"));
+                }
+                IOUtil.copy(in, out);
+                out.flush();
+                Runtime.getRuntime().exec("chmod +x " + file.getAbsolutePath());
             }
-            else {
-                getLog().debug(String.format("Loading file[%s] from jar[%s]", scriptFile, oldJarStorage));
-                final URLClassLoader loader = new URLClassLoader(new URL[]{oldJarStorage.toURI().toURL()}, null);
-                final InputStream scriptIn = loader.getResourceAsStream(scriptFile);
-                out.write(IOUtil.toString(scriptIn).getBytes("ASCII"));
-                out.write("\n\n".getBytes("ASCII"));
+            finally {
+                IOException x = null;
+                if (in != null) {
+                    try {
+                        in.close();
+                    }
+                    catch (IOException e) {
+                        if (e != null) {
+                            x = e;
+                        }
+                    }
+                }
+                if (out != null) {
+                    try {
+                        out.close();
+                    }
+                    catch (IOException e) {
+                        if (e != null) {
+                            x = e;
+                        }
+                    }
+                }
+                if (scriptIn != null) {
+                    try {
+                        scriptIn.close();
+                    }
+                    catch (IOException e) {
+                        if (e != null) {
+                            x = e;
+                        }
+                    }
+                }
+                if (x != null) {
+                    throw x;
+                }
             }
-            IOUtil.copy(in, out);
-            in.close();
-            out.close();
-            Runtime.getRuntime().exec("chmod +x " + file.getAbsolutePath());
         }
         finally {
-            oldJarStorage.delete();
+            if (!oldJarStorage.delete()) {
+                throw new MojoExecutionException("FAILURE, could not delete '" + oldJarStorage.getAbsolutePath() + "'");
+            }
         }
     }
 }
