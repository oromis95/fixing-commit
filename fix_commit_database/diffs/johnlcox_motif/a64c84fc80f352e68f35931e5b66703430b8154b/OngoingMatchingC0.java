@@ -20,6 +20,8 @@ import com.leacox.motif.extract.Extractor0;
 import com.leacox.motif.function.Consumer0;
 
 /**
+ * Fluent API for specifying an action to take on a match with 0 parameters.
+ *
  * @author John Leacox
  */
 public final class OngoingMatchingC0<T, U extends T> extends Matching0<T, U> {
@@ -31,6 +33,9 @@ public final class OngoingMatchingC0<T, U extends T> extends Matching0<T, U> {
     this.fluentMatchingC = fluentMatchingC;
   }
 
+  /**
+   * Sets a {@link Consumer0} to execute if this matches.
+   */
   public FluentMatchingC<T> then(Consumer0 consumer) {
     return then(fluentMatchingC, consumer);
   }
