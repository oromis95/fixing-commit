@@ -20,6 +20,8 @@ import com.leacox.motif.extract.Extractor3;
 import com.leacox.motif.function.Function3;
 
 /**
+ * Fluent API for specifying an action to take on a match with 3 parameters.
+ *
  * @author John Leacox
  */
 public final class OngoingMatchingR3<T, U extends T, A, B, C, R> extends Matching3<T, U, A, B, C> {
@@ -31,6 +33,9 @@ public final class OngoingMatchingR3<T, U extends T, A, B, C, R> extends Matchin
     this.fluentMatchingR = fluentMatchingR;
   }
 
+  /**
+   * Sets a {@link Function3} to execute if this matches.
+   */
   public FluentMatchingR<T, R> get(Function3<A, B, C, R> function) {
     return get(fluentMatchingR, function);
   }
