@@ -13,6 +13,7 @@
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
+
 package com.leacox.motif.cases;
 
 import com.leacox.motif.extract.FieldExtractor;
@@ -22,6 +23,8 @@ import java.util.List;
 import java.util.Optional;
 
 /**
+ * Field extractor for extracting a {@link List} with a single element.
+ *
  * @author John Leacox
  */
 final class ListConsHeadFieldExtractor<A> implements FieldExtractor<List<A>> {
