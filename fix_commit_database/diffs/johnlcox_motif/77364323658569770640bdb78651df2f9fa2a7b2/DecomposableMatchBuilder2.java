@@ -13,12 +13,13 @@
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
+
 package com.leacox.motif.extract;
 
 import com.leacox.motif.extract.matchers.Matcher;
+import com.leacox.motif.extract.util.Maps;
 import com.leacox.motif.tuple.Tuple2;
 import com.leacox.motif.tuple.Tuple3;
-import com.leacox.motif.extract.util.Maps;
 
 import java.util.List;
 import java.util.Map;
