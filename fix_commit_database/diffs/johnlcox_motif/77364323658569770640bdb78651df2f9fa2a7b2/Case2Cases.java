@@ -16,6 +16,7 @@
 //
 // Generated by Motif. Do Not Edit!
 //
+
 package com.leacox.motif.cases;
 
 import com.leacox.motif.MatchesAny;
@@ -29,8 +30,7 @@ import com.leacox.motif.extract.matchers.ArgumentMatchers;
 import com.leacox.motif.extract.matchers.Matcher;
 import com.leacox.motif.extract.util.Lists;
 import com.leacox.motif.tuple.Tuple2;
-import java.lang.Class;
-import java.lang.Object;
+
 import java.util.List;
 
 /**
@@ -43,7 +43,8 @@ public final class Case2Cases {
   /**
    * Matches a case class of two elements.
    */
-  public static <T extends Case2<A, B>, A, B> DecomposableMatchBuilder0<T> case2(Class<T> clazz, MatchesExact<A> a, MatchesExact<B> b) {
+  public static <T extends Case2<A, B>, A, B> DecomposableMatchBuilder0<T> case2(
+      Class<T> clazz, MatchesExact<A> a, MatchesExact<B> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.eq(a.t), ArgumentMatchers.eq(b.t));
     return new DecomposableMatchBuilder0<T>(matchers, new Case2FieldExtractor<>(clazz));
   }
@@ -53,7 +54,8 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code b} value is extracted.
    */
-  public static <T extends Case2<A, B>, A, B> DecomposableMatchBuilder1<T, B> case2(Class<T> clazz, MatchesExact<A> a, MatchesAny<B> b) {
+  public static <T extends Case2<A, B>, A, B> DecomposableMatchBuilder1<T, B> case2(
+      Class<T> clazz, MatchesExact<A> a, MatchesAny<B> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.eq(a.t), ArgumentMatchers.any());
     return new DecomposableMatchBuilder1<T, B>(matchers, 1, new Case2FieldExtractor<>(clazz));
   }
@@ -63,9 +65,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code b} value is decomposed to 0.
    */
-  public static <T extends Case2<A, B>, A, B, EB extends B> DecomposableMatchBuilder0<T> case2(Class<T> clazz, MatchesExact<A> a, DecomposableMatchBuilder0<EB> b) {
+  public static <T extends Case2<A, B>, A, B, EB extends B> DecomposableMatchBuilder0<T> case2(
+      Class<T> clazz, MatchesExact<A> a, DecomposableMatchBuilder0<EB> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.eq(a.t), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder1<T, EB>(matchers, 1, new Case2FieldExtractor<>(clazz)).decomposeFirst(b);
+    return new DecomposableMatchBuilder1<T, EB>(matchers, 1, new Case2FieldExtractor<>(clazz))
+        .decomposeFirst(b);
   }
 
   /**
@@ -73,9 +77,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code b} value is decomposed to 1.
    */
-  public static <T extends Case2<A, B>, A, B, EB extends B, B1> DecomposableMatchBuilder1<T, B1> case2(Class<T> clazz, MatchesExact<A> a, DecomposableMatchBuilder1<EB, B1> b) {
+  public static <T extends Case2<A, B>, A, B, EB extends B, B1> DecomposableMatchBuilder1<T, B1> case2(
+      Class<T> clazz, MatchesExact<A> a, DecomposableMatchBuilder1<EB, B1> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.eq(a.t), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder1<T, EB>(matchers, 1, new Case2FieldExtractor<>(clazz)).decomposeFirst(b);
+    return new DecomposableMatchBuilder1<T, EB>(matchers, 1, new Case2FieldExtractor<>(clazz))
+        .decomposeFirst(b);
   }
 
   /**
@@ -83,9 +89,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code b} value is decomposed to 2.
    */
-  public static <T extends Case2<A, B>, A, B, EB extends B, B1, B2> DecomposableMatchBuilder2<T, B1, B2> case2(Class<T> clazz, MatchesExact<A> a, DecomposableMatchBuilder2<EB, B1, B2> b) {
+  public static <T extends Case2<A, B>, A, B, EB extends B, B1, B2> DecomposableMatchBuilder2<T, B1, B2> case2(
+      Class<T> clazz, MatchesExact<A> a, DecomposableMatchBuilder2<EB, B1, B2> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.eq(a.t), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder1<T, EB>(matchers, 1, new Case2FieldExtractor<>(clazz)).decomposeFirst(b);
+    return new DecomposableMatchBuilder1<T, EB>(matchers, 1, new Case2FieldExtractor<>(clazz))
+        .decomposeFirst(b);
   }
 
   /**
@@ -93,9 +101,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code b} value is decomposed to 3.
    */
-  public static <T extends Case2<A, B>, A, B, EB extends B, B1, B2, B3> DecomposableMatchBuilder3<T, B1, B2, B3> case2(Class<T> clazz, MatchesExact<A> a, DecomposableMatchBuilder3<EB, B1, B2, B3> b) {
+  public static <T extends Case2<A, B>, A, B, EB extends B, B1, B2, B3> DecomposableMatchBuilder3<T, B1, B2, B3> case2(
+      Class<T> clazz, MatchesExact<A> a, DecomposableMatchBuilder3<EB, B1, B2, B3> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.eq(a.t), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder1<T, EB>(matchers, 1, new Case2FieldExtractor<>(clazz)).decomposeFirst(b);
+    return new DecomposableMatchBuilder1<T, EB>(matchers, 1, new Case2FieldExtractor<>(clazz))
+        .decomposeFirst(b);
   }
 
   /**
@@ -103,7 +113,8 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is extracted.
    */
-  public static <T extends Case2<A, B>, A, B> DecomposableMatchBuilder1<T, A> case2(Class<T> clazz, MatchesAny<A> a, MatchesExact<B> b) {
+  public static <T extends Case2<A, B>, A, B> DecomposableMatchBuilder1<T, A> case2(
+      Class<T> clazz, MatchesAny<A> a, MatchesExact<B> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.eq(b.t));
     return new DecomposableMatchBuilder1<T, A>(matchers, 0, new Case2FieldExtractor<>(clazz));
   }
@@ -113,9 +124,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is extracted and the {@code b} value is extracted.
    */
-  public static <T extends Case2<A, B>, A, B> DecomposableMatchBuilder2<T, A, B> case2(Class<T> clazz, MatchesAny<A> a, MatchesAny<B> b) {
+  public static <T extends Case2<A, B>, A, B> DecomposableMatchBuilder2<T, A, B> case2(
+      Class<T> clazz, MatchesAny<A> a, MatchesAny<B> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder2<T, A, B>(matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz));
+    return new DecomposableMatchBuilder2<T, A, B>(
+        matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz));
   }
 
   /**
@@ -123,9 +136,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is extracted and the {@code b} value is decomposed to 0.
    */
-  public static <T extends Case2<A, B>, A, B, EB extends B> DecomposableMatchBuilder1<T, A> case2(Class<T> clazz, MatchesAny<A> a, DecomposableMatchBuilder0<EB> b) {
+  public static <T extends Case2<A, B>, A, B, EB extends B> DecomposableMatchBuilder1<T, A> case2(
+      Class<T> clazz, MatchesAny<A> a, DecomposableMatchBuilder0<EB> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder2<T, A, EB>(matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeSecond(b);
+    return new DecomposableMatchBuilder2<T, A, EB>(
+        matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeSecond(b);
   }
 
   /**
@@ -133,9 +148,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is extracted and the {@code b} value is decomposed to 1.
    */
-  public static <T extends Case2<A, B>, A, B, EB extends B, B1> DecomposableMatchBuilder2<T, A, B1> case2(Class<T> clazz, MatchesAny<A> a, DecomposableMatchBuilder1<EB, B1> b) {
+  public static <T extends Case2<A, B>, A, B, EB extends B, B1> DecomposableMatchBuilder2<T, A, B1> case2(
+      Class<T> clazz, MatchesAny<A> a, DecomposableMatchBuilder1<EB, B1> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder2<T, A, EB>(matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeSecond(b);
+    return new DecomposableMatchBuilder2<T, A, EB>(
+        matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeSecond(b);
   }
 
   /**
@@ -143,9 +160,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is extracted and the {@code b} value is decomposed to 2.
    */
-  public static <T extends Case2<A, B>, A, B, EB extends B, B1, B2> DecomposableMatchBuilder3<T, A, B1, B2> case2(Class<T> clazz, MatchesAny<A> a, DecomposableMatchBuilder2<EB, B1, B2> b) {
+  public static <T extends Case2<A, B>, A, B, EB extends B, B1, B2> DecomposableMatchBuilder3<T, A, B1, B2> case2(
+      Class<T> clazz, MatchesAny<A> a, DecomposableMatchBuilder2<EB, B1, B2> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder2<T, A, EB>(matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeSecond(b);
+    return new DecomposableMatchBuilder2<T, A, EB>(
+        matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeSecond(b);
   }
 
   /**
@@ -153,9 +172,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is decomposed to 0.
    */
-  public static <T extends Case2<A, B>, A, B, EA extends A> DecomposableMatchBuilder0<T> case2(Class<T> clazz, DecomposableMatchBuilder0<EA> a, MatchesExact<B> b) {
+  public static <T extends Case2<A, B>, A, B, EA extends A> DecomposableMatchBuilder0<T> case2(
+      Class<T> clazz, DecomposableMatchBuilder0<EA> a, MatchesExact<B> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.eq(b.t));
-    return new DecomposableMatchBuilder1<T, EA>(matchers, 0, new Case2FieldExtractor<>(clazz)).decomposeFirst(a);
+    return new DecomposableMatchBuilder1<T, EA>(matchers, 0, new Case2FieldExtractor<>(clazz))
+        .decomposeFirst(a);
   }
 
   /**
@@ -163,9 +184,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is decomposed to 0 and the {@code b} value is extracted.
    */
-  public static <T extends Case2<A, B>, A, B, EA extends A> DecomposableMatchBuilder1<T, B> case2(Class<T> clazz, DecomposableMatchBuilder0<EA> a, MatchesAny<B> b) {
+  public static <T extends Case2<A, B>, A, B, EA extends A> DecomposableMatchBuilder1<T, B> case2(
+      Class<T> clazz, DecomposableMatchBuilder0<EA> a, MatchesAny<B> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder2<T, EA, B>(matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirst(a);
+    return new DecomposableMatchBuilder2<T, EA, B>(
+        matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirst(a);
   }
 
   /**
@@ -173,9 +196,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is decomposed to 0 and the {@code b} value is decomposed to 0.
    */
-  public static <T extends Case2<A, B>, A, B, EA extends A, EB extends B> DecomposableMatchBuilder0<T> case2(Class<T> clazz, DecomposableMatchBuilder0<EA> a, DecomposableMatchBuilder0<EB> b) {
+  public static <T extends Case2<A, B>, A, B, EA extends A, EB extends B> DecomposableMatchBuilder0<T> case2(
+      Class<T> clazz, DecomposableMatchBuilder0<EA> a, DecomposableMatchBuilder0<EB> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder2<T, EA, EB>(matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirstAndSecond(a, b);
+    return new DecomposableMatchBuilder2<T, EA, EB>(
+        matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirstAndSecond(a, b);
   }
 
   /**
@@ -183,9 +208,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is decomposed to 0 and the {@code b} value is decomposed to 1.
    */
-  public static <T extends Case2<A, B>, A, B, EA extends A, EB extends B, B1> DecomposableMatchBuilder1<T, B1> case2(Class<T> clazz, DecomposableMatchBuilder0<EA> a, DecomposableMatchBuilder1<EB, B1> b) {
+  public static <T extends Case2<A, B>, A, B, EA extends A, EB extends B, B1> DecomposableMatchBuilder1<T, B1> case2(
+      Class<T> clazz, DecomposableMatchBuilder0<EA> a, DecomposableMatchBuilder1<EB, B1> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder2<T, EA, EB>(matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirstAndSecond(a, b);
+    return new DecomposableMatchBuilder2<T, EA, EB>(
+        matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirstAndSecond(a, b);
   }
 
   /**
@@ -193,9 +220,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is decomposed to 0 and the {@code b} value is decomposed to 2.
    */
-  public static <T extends Case2<A, B>, A, B, EA extends A, EB extends B, B1, B2> DecomposableMatchBuilder2<T, B1, B2> case2(Class<T> clazz, DecomposableMatchBuilder0<EA> a, DecomposableMatchBuilder2<EB, B1, B2> b) {
+  public static <T extends Case2<A, B>, A, B, EA extends A, EB extends B, B1, B2> DecomposableMatchBuilder2<T, B1, B2> case2(
+      Class<T> clazz, DecomposableMatchBuilder0<EA> a, DecomposableMatchBuilder2<EB, B1, B2> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder2<T, EA, EB>(matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirstAndSecond(a, b);
+    return new DecomposableMatchBuilder2<T, EA, EB>(
+        matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirstAndSecond(a, b);
   }
 
   /**
@@ -203,9 +232,12 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is decomposed to 0 and the {@code b} value is decomposed to 3.
    */
-  public static <T extends Case2<A, B>, A, B, EA extends A, EB extends B, B1, B2, B3> DecomposableMatchBuilder3<T, B1, B2, B3> case2(Class<T> clazz, DecomposableMatchBuilder0<EA> a, DecomposableMatchBuilder3<EB, B1, B2, B3> b) {
+  public static <T extends Case2<A, B>, A, B, EA extends A, EB extends B, B1, B2, B3> DecomposableMatchBuilder3<T, B1, B2, B3> case2(
+      Class<T> clazz, DecomposableMatchBuilder0<EA> a,
+      DecomposableMatchBuilder3<EB, B1, B2, B3> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder2<T, EA, EB>(matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirstAndSecond(a, b);
+    return new DecomposableMatchBuilder2<T, EA, EB>(
+        matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirstAndSecond(a, b);
   }
 
   /**
@@ -213,9 +245,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is decomposed to 1.
    */
-  public static <T extends Case2<A, B>, A, B, EA extends A, A1> DecomposableMatchBuilder1<T, A1> case2(Class<T> clazz, DecomposableMatchBuilder1<EA, A1> a, MatchesExact<B> b) {
+  public static <T extends Case2<A, B>, A, B, EA extends A, A1> DecomposableMatchBuilder1<T, A1> case2(
+      Class<T> clazz, DecomposableMatchBuilder1<EA, A1> a, MatchesExact<B> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.eq(b.t));
-    return new DecomposableMatchBuilder1<T, EA>(matchers, 0, new Case2FieldExtractor<>(clazz)).decomposeFirst(a);
+    return new DecomposableMatchBuilder1<T, EA>(matchers, 0, new Case2FieldExtractor<>(clazz))
+        .decomposeFirst(a);
   }
 
   /**
@@ -223,9 +257,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is decomposed to 1 and the {@code b} value is extracted.
    */
-  public static <T extends Case2<A, B>, A, B, EA extends A, A1> DecomposableMatchBuilder2<T, A1, B> case2(Class<T> clazz, DecomposableMatchBuilder1<EA, A1> a, MatchesAny<B> b) {
+  public static <T extends Case2<A, B>, A, B, EA extends A, A1> DecomposableMatchBuilder2<T, A1, B> case2(
+      Class<T> clazz, DecomposableMatchBuilder1<EA, A1> a, MatchesAny<B> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder2<T, EA, B>(matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirst(a);
+    return new DecomposableMatchBuilder2<T, EA, B>(
+        matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirst(a);
   }
 
   /**
@@ -233,9 +269,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is decomposed to 1 and the {@code b} value is decomposed to 0.
    */
-  public static <T extends Case2<A, B>, A, B, EA extends A, A1, EB extends B> DecomposableMatchBuilder1<T, A1> case2(Class<T> clazz, DecomposableMatchBuilder1<EA, A1> a, DecomposableMatchBuilder0<EB> b) {
+  public static <T extends Case2<A, B>, A, B, EA extends A, A1, EB extends B> DecomposableMatchBuilder1<T, A1> case2(
+      Class<T> clazz, DecomposableMatchBuilder1<EA, A1> a, DecomposableMatchBuilder0<EB> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder2<T, EA, EB>(matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirstAndSecond(a, b);
+    return new DecomposableMatchBuilder2<T, EA, EB>(
+        matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirstAndSecond(a, b);
   }
 
   /**
@@ -243,9 +281,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is decomposed to 1 and the {@code b} value is decomposed to 1.
    */
-  public static <T extends Case2<A, B>, A, B, EA extends A, A1, EB extends B, B1> DecomposableMatchBuilder2<T, A1, B1> case2(Class<T> clazz, DecomposableMatchBuilder1<EA, A1> a, DecomposableMatchBuilder1<EB, B1> b) {
+  public static <T extends Case2<A, B>, A, B, EA extends A, A1, EB extends B, B1> DecomposableMatchBuilder2<T, A1, B1> case2(
+      Class<T> clazz, DecomposableMatchBuilder1<EA, A1> a, DecomposableMatchBuilder1<EB, B1> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder2<T, EA, EB>(matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirstAndSecond(a, b);
+    return new DecomposableMatchBuilder2<T, EA, EB>(
+        matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirstAndSecond(a, b);
   }
 
   /**
@@ -253,9 +293,12 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is decomposed to 1 and the {@code b} value is decomposed to 2.
    */
-  public static <T extends Case2<A, B>, A, B, EA extends A, A1, EB extends B, B1, B2> DecomposableMatchBuilder3<T, A1, B1, B2> case2(Class<T> clazz, DecomposableMatchBuilder1<EA, A1> a, DecomposableMatchBuilder2<EB, B1, B2> b) {
+  public static <T extends Case2<A, B>, A, B, EA extends A, A1, EB extends B, B1, B2> DecomposableMatchBuilder3<T, A1, B1, B2> case2(
+      Class<T> clazz, DecomposableMatchBuilder1<EA, A1> a,
+      DecomposableMatchBuilder2<EB, B1, B2> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder2<T, EA, EB>(matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirstAndSecond(a, b);
+    return new DecomposableMatchBuilder2<T, EA, EB>(
+        matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirstAndSecond(a, b);
   }
 
   /**
@@ -263,9 +306,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is decomposed to 2.
    */
-  public static <T extends Case2<A, B>, A, B, EA extends A, A1, A2> DecomposableMatchBuilder2<T, A1, A2> case2(Class<T> clazz, DecomposableMatchBuilder2<EA, A1, A2> a, MatchesExact<B> b) {
+  public static <T extends Case2<A, B>, A, B, EA extends A, A1, A2> DecomposableMatchBuilder2<T, A1, A2> case2(
+      Class<T> clazz, DecomposableMatchBuilder2<EA, A1, A2> a, MatchesExact<B> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.eq(b.t));
-    return new DecomposableMatchBuilder1<T, EA>(matchers, 0, new Case2FieldExtractor<>(clazz)).decomposeFirst(a);
+    return new DecomposableMatchBuilder1<T, EA>(matchers, 0, new Case2FieldExtractor<>(clazz))
+        .decomposeFirst(a);
   }
 
   /**
@@ -273,9 +318,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is decomposed to 2 and the {@code b} value is extracted.
    */
-  public static <T extends Case2<A, B>, A, B, EA extends A, A1, A2> DecomposableMatchBuilder3<T, A1, A2, B> case2(Class<T> clazz, DecomposableMatchBuilder2<EA, A1, A2> a, MatchesAny<B> b) {
+  public static <T extends Case2<A, B>, A, B, EA extends A, A1, A2> DecomposableMatchBuilder3<T, A1, A2, B> case2(
+      Class<T> clazz, DecomposableMatchBuilder2<EA, A1, A2> a, MatchesAny<B> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder2<T, EA, B>(matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirst(a);
+    return new DecomposableMatchBuilder2<T, EA, B>(
+        matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirst(a);
   }
 
   /**
@@ -283,9 +330,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is decomposed to 2 and the {@code b} value is decomposed to 0.
    */
-  public static <T extends Case2<A, B>, A, B, EA extends A, A1, A2, EB extends B> DecomposableMatchBuilder2<T, A1, A2> case2(Class<T> clazz, DecomposableMatchBuilder2<EA, A1, A2> a, DecomposableMatchBuilder0<EB> b) {
+  public static <T extends Case2<A, B>, A, B, EA extends A, A1, A2, EB extends B> DecomposableMatchBuilder2<T, A1, A2> case2(
+      Class<T> clazz, DecomposableMatchBuilder2<EA, A1, A2> a, DecomposableMatchBuilder0<EB> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder2<T, EA, EB>(matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirstAndSecond(a, b);
+    return new DecomposableMatchBuilder2<T, EA, EB>(
+        matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirstAndSecond(a, b);
   }
 
   /**
@@ -293,9 +342,12 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is decomposed to 2 and the {@code b} value is decomposed to 1.
    */
-  public static <T extends Case2<A, B>, A, B, EA extends A, A1, A2, EB extends B, B1> DecomposableMatchBuilder3<T, A1, A2, B1> case2(Class<T> clazz, DecomposableMatchBuilder2<EA, A1, A2> a, DecomposableMatchBuilder1<EB, B1> b) {
+  public static <T extends Case2<A, B>, A, B, EA extends A, A1, A2, EB extends B, B1> DecomposableMatchBuilder3<T, A1, A2, B1> case2(
+      Class<T> clazz, DecomposableMatchBuilder2<EA, A1, A2> a,
+      DecomposableMatchBuilder1<EB, B1> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder2<T, EA, EB>(matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirstAndSecond(a, b);
+    return new DecomposableMatchBuilder2<T, EA, EB>(
+        matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirstAndSecond(a, b);
   }
 
   /**
@@ -303,9 +355,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is decomposed to 3.
    */
-  public static <T extends Case2<A, B>, A, B, EA extends A, A1, A2, A3> DecomposableMatchBuilder3<T, A1, A2, A3> case2(Class<T> clazz, DecomposableMatchBuilder3<EA, A1, A2, A3> a, MatchesExact<B> b) {
+  public static <T extends Case2<A, B>, A, B, EA extends A, A1, A2, A3> DecomposableMatchBuilder3<T, A1, A2, A3> case2(
+      Class<T> clazz, DecomposableMatchBuilder3<EA, A1, A2, A3> a, MatchesExact<B> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.eq(b.t));
-    return new DecomposableMatchBuilder1<T, EA>(matchers, 0, new Case2FieldExtractor<>(clazz)).decomposeFirst(a);
+    return new DecomposableMatchBuilder1<T, EA>(matchers, 0, new Case2FieldExtractor<>(clazz))
+        .decomposeFirst(a);
   }
 
   /**
@@ -313,8 +367,11 @@ public final class Case2Cases {
    *
    * <p>If matched, the {@code a} value is decomposed to 3 and the {@code b} value is decomposed to 0.
    */
-  public static <T extends Case2<A, B>, A, B, EA extends A, A1, A2, A3, EB extends B> DecomposableMatchBuilder3<T, A1, A2, A3> case2(Class<T> clazz, DecomposableMatchBuilder3<EA, A1, A2, A3> a, DecomposableMatchBuilder0<EB> b) {
+  public static <T extends Case2<A, B>, A, B, EA extends A, A1, A2, A3, EB extends B> DecomposableMatchBuilder3<T, A1, A2, A3> case2(
+      Class<T> clazz, DecomposableMatchBuilder3<EA, A1, A2, A3> a,
+      DecomposableMatchBuilder0<EB> b) {
     List<Matcher<Object>> matchers = Lists.of(ArgumentMatchers.any(), ArgumentMatchers.any());
-    return new DecomposableMatchBuilder2<T, EA, EB>(matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirstAndSecond(a, b);
+    return new DecomposableMatchBuilder2<T, EA, EB>(
+        matchers, Tuple2.of(0, 1), new Case2FieldExtractor<>(clazz)).decomposeFirstAndSecond(a, b);
   }
 }
