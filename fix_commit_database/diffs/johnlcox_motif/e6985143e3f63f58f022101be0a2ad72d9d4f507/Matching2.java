@@ -14,13 +14,13 @@ import java.util.Optional;
 /**
  * @author John Leacox
  */
-abstract class Matching2<T, A, B> {
-  private final Extractor2<T, A, B> extractor;
+abstract class Matching2<T, U, A, B> {
+  private final Extractor2<U, A, B> extractor;
   private final Matcher<A> toMatchA;
   private final Matcher<B> toMatchB;
 
   Matching2(
-      Extractor2<T, A, B> extractor, Matcher<A> toMatchA, Matcher<B> toMatchB) {
+      Extractor2<U, A, B> extractor, Matcher<A> toMatchA, Matcher<B> toMatchB) {
     this.extractor = extractor;
     this.toMatchA = toMatchA;
     this.toMatchB = toMatchB;
@@ -31,7 +31,11 @@ abstract class Matching2<T, A, B> {
     fluentMatchingR.addPattern(
         Pattern.of(
             t -> {
-              Optional<Tuple2<A, B>> opt = extractor.unapply(t);
+              if (!extractor.getExtractorClass().isAssignableFrom(t.getClass())) {
+                return false;
+              }
+
+              Optional<Tuple2<A, B>> opt = extractor.unapply((U) t);
               if (opt.isPresent()) {
                 Tuple2<A, B> tuple2 = opt.get();
                 return toMatchA.matches(tuple2.first()) && toMatchB.matches(tuple2.second());
@@ -40,7 +44,7 @@ abstract class Matching2<T, A, B> {
               return false;
             },
             t -> {
-              Tuple2<A, B> tuple2 = extractor.unapply(t).get();
+              Tuple2<A, B> tuple2 = extractor.unapply((U) t).get();
               return function.apply(tuple2.first(), tuple2.second());
             }
         )
@@ -53,7 +57,11 @@ abstract class Matching2<T, A, B> {
     fluentMatchingC.addPattern(
         ConsumablePattern.of(
             t -> {
-              Optional<Tuple2<A, B>> opt = extractor.unapply(t);
+              if (!extractor.getExtractorClass().isAssignableFrom(t.getClass())) {
+                return false;
+              }
+
+              Optional<Tuple2<A, B>> opt = extractor.unapply((U) t);
               if (opt.isPresent()) {
                 Tuple2<A, B> tuple2 = opt.get();
                 return toMatchA.matches(tuple2.first()) && toMatchB.matches(tuple2.second());
@@ -62,7 +70,7 @@ abstract class Matching2<T, A, B> {
               return false;
             },
             t -> {
-              Tuple2<A, B> tuple2 = extractor.unapply(t).get();
+              Tuple2<A, B> tuple2 = extractor.unapply((U) t).get();
               consumer.accept(tuple2.first(), tuple2.second());
             }
         )
