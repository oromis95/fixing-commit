@@ -7,4 +7,6 @@ public interface Extractor0<T> {
   //T apply();
 
   boolean unapply(T t);
+
+  Class getExtractorClass();
 }
