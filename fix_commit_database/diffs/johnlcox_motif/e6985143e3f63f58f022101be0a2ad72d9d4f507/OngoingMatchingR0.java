@@ -7,11 +7,11 @@ import java.util.function.Supplier;
 /**
  * @author John Leacox
  */
-public final class OngoingMatchingR0<T, R> extends Matching0<T> {
+public final class OngoingMatchingR0<T, U, R> extends Matching0<T, U> {
   private final FluentMatchingR<T, R> fluentMatchingR;
 
   OngoingMatchingR0(
-      FluentMatchingR<T, R> fluentMatchingR, Extractor0<T> extractor) {
+      FluentMatchingR<T, R> fluentMatchingR, Extractor0<U> extractor) {
     super(extractor);
     this.fluentMatchingR = fluentMatchingR;
   }
