@@ -36,6 +36,11 @@ public final class OptionalCases {
           return Optional.empty();
         }
       }
+
+      @Override
+      public Class getExtractorClass() {
+        return Optional.class;
+      }
     };
 
     return new MatchingExtractor1<>(extractor, a);
@@ -52,6 +57,11 @@ public final class OptionalCases {
       public boolean unapply(Optional<T> t) {
         return !t.isPresent();
       }
+
+      @Override
+      public Class<Optional> getExtractorClass() {
+        return Optional.class;
+      }
     };
 
     return new MatchingExtractor0<>(extractor);
