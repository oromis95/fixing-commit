@@ -1,5 +1,9 @@
 package com.leacox.motif.example.superhero;
 
+import static com.leacox.motif.fluent.FluentMotif.match;
+import static com.leacox.motif.fluent.cases.CaseClassCases.case3;
+import static com.leacox.motif.matchers.ArgumentMatchers.eq;
+
 import java.math.BigDecimal;
 import java.util.ArrayList;
 import java.util.List;
@@ -39,15 +43,13 @@ public class SuperHeroExample {
       Character character, Civilian alterEgo) {
 
     // TODO: Figure out why this doesn't work
-    //return match(character)
-    //.when(case2(Animal.class, eq("Cat"), eq(4)))
-    //.when(case3(SuperHero.class, any(), any(), eq(Optional.of(alterEgo)))).get(
-    //.when(case3(SuperHero.class, eq(""), eq(ImmutableList.of()), eq(Optional.of(alterEgo))))
-    //.get(
-    //    (n, p, a) -> Optional.of(p))
-    //.orElse(Optional.empty())
-    //.getMatch();
-
-    return null;
+    return match(character)
+        //.when(case2(Animal.class, eq("Cat"), eq(4)))
+        //.when(case3(SuperHero.class, any(), any(), eq(Optional.of(alterEgo)))).get(
+        .when(case3(SuperHero.class, eq(""), eq(ImmutableList.of()), eq(Optional.of(alterEgo))))
+        .get(
+            (n, p, a) -> Optional.of(p))
+        .orElse(Optional.empty())
+        .getMatch();
   }
 }
