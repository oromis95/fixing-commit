@@ -8,11 +8,11 @@ import org.hamcrest.Matcher;
 /**
  * @author John Leacox
  */
-public class OngoingMatchingC3<T, A, B, C> extends Matching3<T, A, B, C> {
+public class OngoingMatchingC3<T, U, A, B, C> extends Matching3<T, U, A, B, C> {
   private final FluentMatchingC<T> fluentMatchingC;
 
   OngoingMatchingC3(
-      FluentMatchingC<T> fluentMatchingC, Extractor3<T, A, B, C> extractor, Matcher<A> toMatchA,
+      FluentMatchingC<T> fluentMatchingC, Extractor3<U, A, B, C> extractor, Matcher<A> toMatchA,
       Matcher<B> toMatchB, Matcher<C> toMatchC) {
     super(extractor, toMatchA, toMatchB, toMatchC);
 
