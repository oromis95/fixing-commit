@@ -9,11 +9,11 @@ import java.util.function.Function;
 /**
  * @author John Leacox
  */
-public final class OngoingMatchingR1<T, A, R> extends Matching1<T, A> {
+public final class OngoingMatchingR1<T, U, A, R> extends Matching1<T, U, A> {
   private final FluentMatchingR<T, R> fluentMatchingR;
 
   OngoingMatchingR1(
-      FluentMatchingR<T, R> fluentMatchingR, Extractor1<T, A> extractor, Matcher<A> toMatchA) {
+      FluentMatchingR<T, R> fluentMatchingR, Extractor1<U, A> extractor, Matcher<A> toMatchA) {
     super(extractor, toMatchA);
 
     this.fluentMatchingR = fluentMatchingR;
