@@ -26,19 +26,34 @@ import java.util.stream.IntStream;
 public class FizzBuzzBenchmark {
   @Benchmark
   public void fizzbuzzConditional() {
+
+
+
+
+
+
+
     IntStream.range(0, 101).forEach(
         n -> {
           if (n % (3 * 5) == 0) {
             System.out.println("FizzBuzz");
           } else if (n % 3 == 0) {
             System.out.println("Fizz");
-          } else if (n % 5 == 5) {
+          } else if (n % 5 == 0) {
             System.out.println("Buzz");
           } else {
             System.out.println(n);
           }
         }
     );
+
+
+
+
+
+
+
+
   }
 
   @Benchmark
