@@ -79,9 +79,9 @@ public final class Motif {
    * IntStream.range(0, 101).forEach(
    *     n -> System.out.println(
    *         match(Tuple2.of(n % 3, n % 5))
-   *             .when(tuple2(0, 0)).get(() -> "FizzBuzz")
-   *             .when(tuple2(0, any())).get(y -> "Fizz")
-   *             .when(tuple2(any(), 0)).get(x -> "Buzz")
+   *             .when(tuple2(eq(0), eq(0))).get(() -> "FizzBuzz")
+   *             .when(tuple2(eq(0), any())).get(y -> "Fizz")
+   *             .when(tuple2(any(), eq(0))).get(x -> "Buzz")
    *             .orElse(String.valueOf(n))
    *             .getMatch()
    *     )
