@@ -26,11 +26,16 @@ import java.util.Map;
 import java.util.Objects;
 
 /**
+ * A decomposable match builder that extracts 2 parameters.
+ *
  * @author John Leacox
  */
 public final class DecomposableMatchBuilder2<T, A, B> extends DecomposableMatchBuilder<T> {
   final Tuple2<Integer, Integer> extractedIndexes;
 
+  /**
+   * Creates a new instance of {@link DecomposableMatchBuilder2}.
+   */
   public DecomposableMatchBuilder2(
       List<Matcher<Object>> fieldMatchers, Tuple2<Integer, Integer> extractedIndexes,
       FieldExtractor<T> fieldExtractor) {
@@ -39,11 +44,19 @@ public final class DecomposableMatchBuilder2<T, A, B> extends DecomposableMatchB
     this.extractedIndexes = extractedIndexes;
   }
 
+  /**
+   * Builds a {@link DecomposableMatch2}.
+   */
   public DecomposableMatch2<T, A, B> build() {
     return new DecomposableMatch2<>(fieldMatchers, extractedIndexes, fieldExtractor);
   }
 
-  // Decomposes the first extracted value and drops it down to a match on only the second value.
+  /**
+   * Returns a new {@link DecomposableMatchBuilder1} that further decomposes the first element of
+   * this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements.
+   */
   public DecomposableMatchBuilder1<T, B> decomposeFirst(DecomposableMatchBuilder0<A> a) {
     Objects.requireNonNull(a);
 
@@ -58,6 +71,12 @@ public final class DecomposableMatchBuilder2<T, A, B> extends DecomposableMatchB
         matchers, extractedIndexes.second(), nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder2} that further decomposes the first element of
+   * this match builder.
+   *
+   * <p>The first element is decomposed to 1 element.
+   */
   public <A1> DecomposableMatchBuilder2<T, A1, B> decomposeFirst(
       DecomposableMatchBuilder1<A, A1> a) {
     Objects.requireNonNull(a);
@@ -76,6 +95,12 @@ public final class DecomposableMatchBuilder2<T, A, B> extends DecomposableMatchB
     return new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the first element of
+   * this match builder.
+   *
+   * <p>The first element is decomposed to 2 elements.
+   */
   public <A1, A2> DecomposableMatchBuilder3<T, A1, A2, B> decomposeFirst(
       DecomposableMatchBuilder2<A, A1, A2> a) {
     Objects.requireNonNull(a);
@@ -98,7 +123,8 @@ public final class DecomposableMatchBuilder2<T, A, B> extends DecomposableMatchB
 
   // TODO: Is there a good way to avoid all the duplication and pull all the decomposeFirst methods
   // up to the parent class and let the children generate the correct indexes?
-  //private <U extends DecomposableMatchBuilder<T>, A1, A2> U decomposeFirst(Class<U> clazz, DecomposableMatchBuilder a) {
+  //private <U extends DecomposableMatchBuilder<T>, A1, A2> U decomposeFirst(Class<U> clazz,
+  // DecomposableMatchBuilder a) {
   //  Objects.requireNonNull(a);
   //
   //  Map<Integer, DecomposableMatchBuilder> buildersByIndex =
@@ -109,7 +135,8 @@ public final class DecomposableMatchBuilder2<T, A, B> extends DecomposableMatchB
   //  NestedFieldExtractor<T> nestedFieldExtractor = getNestedFieldExtractor(buildersByIndex);
   //
   //  if (a instanceof DecomposableMatchBuilder0) {
-  //    return clazz.cast(new DecomposableMatchBuilder1<>(matchers, extractedIndexes.second(), nestedFieldExtractor));
+  //    return clazz.cast(new DecomposableMatchBuilder1<>(matchers, extractedIndexes.second(),
+  // nestedFieldExtractor));
   //  } else if (a instanceof DecomposableMatchBuilder1) {
   //    DecomposableMatchBuilder1<A, A1> x = (DecomposableMatchBuilder1<A, A1>) a;
   //
@@ -117,7 +144,8 @@ public final class DecomposableMatchBuilder2<T, A, B> extends DecomposableMatchB
   //        Tuple2.of(
   //            this.extractedIndexes.first() + x.extractedIndex, this.extractedIndexes.second());
   //
-  //    return clazz.cast(new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor));
+  //    return clazz.cast(new DecomposableMatchBuilder2<>(matchers, newIndexes,
+  // nestedFieldExtractor));
   //  } else if (a instanceof DecomposableMatchBuilder2) {
   //    DecomposableMatchBuilder2<A, A1, A2> x = (DecomposableMatchBuilder2<A, A1, A2>) a;
   //
@@ -127,13 +155,19 @@ public final class DecomposableMatchBuilder2<T, A, B> extends DecomposableMatchB
   //            this.extractedIndexes.first() + x.extractedIndexes.second(),
   //            a.fieldMatchers.size() - 1 + this.extractedIndexes.second());
   //
-  //    return clazz.cast(new DecomposableMatchBuilder3<>(matchers, newIndexes, nestedFieldExtractor));
+  //    return clazz.cast(new DecomposableMatchBuilder3<>(matchers, newIndexes,
+  // nestedFieldExtractor));
   //  }
   //
   //  throw new IllegalStateException("Unknown Decomposition pattern");
   //}
 
-  // Decomposes the second extracted value and drops it down to a match on only the first value.
+  /**
+   * Returns a new {@link DecomposableMatchBuilder1} that further decomposes the second element of
+   * this match builder.
+   *
+   * <p>The second element is decomposed to 0 elements.
+   */
   public DecomposableMatchBuilder1<T, A> decomposeSecond(DecomposableMatchBuilder0<B> second) {
     Objects.requireNonNull(second);
 
@@ -148,6 +182,12 @@ public final class DecomposableMatchBuilder2<T, A, B> extends DecomposableMatchB
         matchers, extractedIndexes.first(), nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder2} that further decomposes the second element of
+   * this match builder.
+   *
+   * <p>The second element is decomposed to 1 elements.
+   */
   public <C> DecomposableMatchBuilder2<T, A, C> decomposeSecond(
       DecomposableMatchBuilder1<B, C> second) {
     Objects.requireNonNull(second);
@@ -166,6 +206,12 @@ public final class DecomposableMatchBuilder2<T, A, B> extends DecomposableMatchB
     return new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder2} that further decomposes the second element of
+   * this match builder.
+   *
+   * <p>The second element is decomposed to 2 elements.
+   */
   public <B1, B2> DecomposableMatchBuilder3<T, A, B1, B2> decomposeSecond(
       DecomposableMatchBuilder2<B, B1, B2> b) {
     Objects.requireNonNull(b);
@@ -186,6 +232,13 @@ public final class DecomposableMatchBuilder2<T, A, B> extends DecomposableMatchB
     return new DecomposableMatchBuilder3<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder0} that further decomposes the first and second
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements, and the second element is decomposed to 0
+   * elements.
+   */
   public DecomposableMatchBuilder0<T> decomposeFirstAndSecond(
       DecomposableMatchBuilder0<A> first, DecomposableMatchBuilder0<B> second) {
     Objects.requireNonNull(first);
@@ -201,6 +254,13 @@ public final class DecomposableMatchBuilder2<T, A, B> extends DecomposableMatchB
     return new DecomposableMatchBuilder0<>(matchers, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder1} that further decomposes the first and second
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 1 element, and the second element is decomposed to 0
+   * elements.
+   */
   public <A1> DecomposableMatchBuilder1<T, A1> decomposeFirstAndSecond(
       DecomposableMatchBuilder1<A, A1> first, DecomposableMatchBuilder0<B> second) {
     Objects.requireNonNull(first);
@@ -217,6 +277,13 @@ public final class DecomposableMatchBuilder2<T, A, B> extends DecomposableMatchB
         matchers, extractedIndexes.first() + first.extractedIndex, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder1} that further decomposes the first and second
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements, and the second element is decomposed to 1
+   * element.
+   */
   public <B1> DecomposableMatchBuilder1<T, B1> decomposeFirstAndSecond(
       DecomposableMatchBuilder0<A> first, DecomposableMatchBuilder1<B, B1> second) {
     Objects.requireNonNull(first);
@@ -233,6 +300,13 @@ public final class DecomposableMatchBuilder2<T, A, B> extends DecomposableMatchB
         matchers, extractedIndexes.second() + second.extractedIndex, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder0} that further decomposes the first and second
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 2 elements, and the second element is decomposed to 0
+   * elements.
+   */
   public <A1, A2> DecomposableMatchBuilder2<T, A1, A2> decomposeFirstAndSecond(
       DecomposableMatchBuilder2<A, A1, A2> a, DecomposableMatchBuilder0<B> b) {
     Objects.requireNonNull(a);
@@ -255,6 +329,13 @@ public final class DecomposableMatchBuilder2<T, A, B> extends DecomposableMatchB
     return new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder0} that further decomposes the first and second
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements, and the second element is decomposed to 2
+   * elements.
+   */
   public <B1, B2> DecomposableMatchBuilder2<T, B1, B2> decomposeFirstAndSecond(
       DecomposableMatchBuilder0<A> a, DecomposableMatchBuilder2<B, B1, B2> b) {
     Objects.requireNonNull(a);
@@ -277,6 +358,13 @@ public final class DecomposableMatchBuilder2<T, A, B> extends DecomposableMatchB
     return new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder0} that further decomposes the first and second
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 1 element, and the second element is decomposed to 1
+   * element.
+   */
   public <A1, B1> DecomposableMatchBuilder2<T, A1, B1> decomposeFirstAndSecond(
       DecomposableMatchBuilder1<A, A1> a, DecomposableMatchBuilder1<B, B1> b) {
     Objects.requireNonNull(a);
@@ -299,6 +387,13 @@ public final class DecomposableMatchBuilder2<T, A, B> extends DecomposableMatchB
     return new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder0} that further decomposes the first and second
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 3 elements, and the second element is decomposed to 0
+   * elements.
+   */
   public <A1, A2, A3> DecomposableMatchBuilder3<T, A1, A2, A3> decomposeFirstAndSecond(
       DecomposableMatchBuilder3<A, A1, A2, A3> a, DecomposableMatchBuilder0<B> b) {
     Objects.requireNonNull(a);
@@ -322,6 +417,13 @@ public final class DecomposableMatchBuilder2<T, A, B> extends DecomposableMatchB
     return new DecomposableMatchBuilder3<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder0} that further decomposes the first and second
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements, and the second element is decomposed to 3
+   * elements.
+   */
   public <B1, B2, B3> DecomposableMatchBuilder3<T, B1, B2, B3> decomposeFirstAndSecond(
       DecomposableMatchBuilder0<A> a, DecomposableMatchBuilder3<B, B1, B2, B3> b) {
     Objects.requireNonNull(a);
@@ -345,6 +447,13 @@ public final class DecomposableMatchBuilder2<T, A, B> extends DecomposableMatchB
     return new DecomposableMatchBuilder3<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder0} that further decomposes the first and second
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 2 elements, and the second element is decomposed to 1
+   * element.
+   */
   public <A1, A2, B1> DecomposableMatchBuilder3<T, A1, A2, B1> decomposeFirstAndSecond(
       DecomposableMatchBuilder2<A, A1, A2> a, DecomposableMatchBuilder1<B, B1> b) {
     Objects.requireNonNull(a);
@@ -368,6 +477,13 @@ public final class DecomposableMatchBuilder2<T, A, B> extends DecomposableMatchB
     return new DecomposableMatchBuilder3<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder0} that further decomposes the first and second
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 1 element, and the second element is decomposed to 2
+   * elements.
+   */
   public <A1, B1, B2> DecomposableMatchBuilder3<T, A1, B1, B2> decomposeFirstAndSecond(
       DecomposableMatchBuilder1<A, A1> a, DecomposableMatchBuilder2<B, B1, B2> b) {
     Objects.requireNonNull(a);
