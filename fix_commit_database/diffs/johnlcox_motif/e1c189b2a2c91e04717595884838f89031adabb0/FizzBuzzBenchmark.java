@@ -27,9 +27,9 @@ import java.util.stream.IntStream;
 public class FizzBuzzBenchmark {
   @Benchmark
   public void fizzbuzzConditional() {
-    IntStream.range(0, 100).forEach(
+    IntStream.range(0, 101).forEach(
         n -> {
-          if (n % 3 * 15 == 0) {
+          if (n % 3 * 5 == 0) {
             System.out.println("FizzBuzz");
           } else if (n % 3 == 0) {
             System.out.println("Fizz");
