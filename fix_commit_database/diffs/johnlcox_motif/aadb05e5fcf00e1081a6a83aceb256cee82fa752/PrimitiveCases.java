@@ -37,6 +37,10 @@ public final class PrimitiveCases {
   }
 
   // TODO: Are these cases actually useful or is the when(Object o) method good enough?
+
+  /**
+   * Matches a byte.
+   */
   public static DecomposableMatchBuilder0<Byte> caseByte(byte b) {
     List<Matcher<Object>> matchers = new ArrayList<>();
     matchers.add(eq(b));
@@ -44,6 +48,11 @@ public final class PrimitiveCases {
     return new DecomposableMatchBuilder0<>(matchers, new PrimitiveFieldExtractor<>(Byte.class));
   }
 
+  /**
+   * Matches a byte.
+   *
+   * <p>If matched, the byte value is extracted.
+   */
   public static DecomposableMatchBuilder1<Byte, Byte> caseByte(MatchesAny b) {
     List<Matcher<Object>> matchers = new ArrayList<>();
     matchers.add(any());
@@ -51,6 +60,9 @@ public final class PrimitiveCases {
     return new DecomposableMatchBuilder1<>(matchers, 0, new PrimitiveFieldExtractor<>(Byte.class));
   }
 
+  /**
+   * Matches a short.
+   */
   public static DecomposableMatchBuilder0<Short> caseShort(short s) {
     List<Matcher<Object>> matchers = new ArrayList<>();
     matchers.add(eq(s));
@@ -58,6 +70,11 @@ public final class PrimitiveCases {
     return new DecomposableMatchBuilder0<>(matchers, new PrimitiveFieldExtractor<>(Short.class));
   }
 
+  /**
+   * Matches a short.
+   *
+   * <p>If matched, the short value is extracted.
+   */
   public static DecomposableMatchBuilder1<Short, Short> caseShort(MatchesAny s) {
     List<Matcher<Object>> matchers = new ArrayList<>();
     matchers.add(any());
@@ -66,6 +83,9 @@ public final class PrimitiveCases {
         matchers, 0, new PrimitiveFieldExtractor<>(Short.class));
   }
 
+  /**
+   * Matches a int.
+   */
   public static DecomposableMatchBuilder0<Integer> caseInt(int i) {
     List<Matcher<Object>> matchers = new ArrayList<>();
     matchers.add(eq(i));
@@ -73,6 +93,11 @@ public final class PrimitiveCases {
     return new DecomposableMatchBuilder0<>(matchers, new PrimitiveFieldExtractor<>(Integer.class));
   }
 
+  /**
+   * Matches a int.
+   *
+   * <p>If matched, the int value is extracted.
+   */
   public static DecomposableMatchBuilder1<Integer, Integer> caseInt(MatchesAny i) {
     List<Matcher<Object>> matchers = new ArrayList<>();
     matchers.add(any());
@@ -81,6 +106,9 @@ public final class PrimitiveCases {
         matchers, 0, new PrimitiveFieldExtractor<>(Integer.class));
   }
 
+  /**
+   * Matches a long.
+   */
   public static DecomposableMatchBuilder0<Long> caseLong(long l) {
     List<Matcher<Object>> matchers = new ArrayList<>();
     matchers.add(eq(l));
@@ -88,6 +116,11 @@ public final class PrimitiveCases {
     return new DecomposableMatchBuilder0<>(matchers, new PrimitiveFieldExtractor<>(Long.class));
   }
 
+  /**
+   * Matches a long.
+   *
+   * <p>If matched, the long value is extracted.
+   */
   public static DecomposableMatchBuilder1<Long, Long> caseLong(MatchesAny<Long> l) {
     List<Matcher<Object>> matchers = new ArrayList<>();
     matchers.add(any());
@@ -95,6 +128,9 @@ public final class PrimitiveCases {
     return new DecomposableMatchBuilder1<>(matchers, 0, new PrimitiveFieldExtractor<>(Long.class));
   }
 
+  /**
+   * Matches a float.
+   */
   public static DecomposableMatchBuilder0<Float> caseFloat(float f) {
     List<Matcher<Object>> matchers = new ArrayList<>();
     matchers.add(eq(f));
@@ -102,6 +138,11 @@ public final class PrimitiveCases {
     return new DecomposableMatchBuilder0<>(matchers, new PrimitiveFieldExtractor<>(Float.class));
   }
 
+  /**
+   * Matches a float.
+   *
+   * <p>If matched, the float value is extracted.
+   */
   public static DecomposableMatchBuilder1<Float, Float> caseFloat(MatchesAny f) {
     List<Matcher<Object>> matchers = new ArrayList<>();
     matchers.add(any());
@@ -110,6 +151,9 @@ public final class PrimitiveCases {
         matchers, 0, new PrimitiveFieldExtractor<>(Float.class));
   }
 
+  /**
+   * Matches a double.
+   */
   public static DecomposableMatchBuilder0<Double> caseDouble(double d) {
     List<Matcher<Object>> matchers = new ArrayList<>();
     matchers.add(eq(d));
@@ -117,6 +161,11 @@ public final class PrimitiveCases {
     return new DecomposableMatchBuilder0<>(matchers, new PrimitiveFieldExtractor<>(Double.class));
   }
 
+  /**
+   * Matches a double.
+   *
+   * <p>If matched, the double value is extracted.
+   */
   public static DecomposableMatchBuilder1<Double, Double> caseDouble(MatchesAny d) {
     List<Matcher<Object>> matchers = new ArrayList<>();
     matchers.add(any());
@@ -125,6 +174,9 @@ public final class PrimitiveCases {
         matchers, 0, new PrimitiveFieldExtractor<>(Double.class));
   }
 
+  /**
+   * Matches a char.
+   */
   public static DecomposableMatchBuilder0<Character> caseChar(char c) {
     List<Matcher<Object>> matchers = new ArrayList<>();
     matchers.add(eq(c));
@@ -133,6 +185,11 @@ public final class PrimitiveCases {
         matchers, new PrimitiveFieldExtractor<>(Character.class));
   }
 
+  /**
+   * Matches a char.
+   *
+   * <p>If matched, the char value is extracted.
+   */
   public static DecomposableMatchBuilder1<Character, Character> caseChar(MatchesAny c) {
     List<Matcher<Object>> matchers = new ArrayList<>();
     matchers.add(any());
@@ -141,6 +198,9 @@ public final class PrimitiveCases {
         matchers, 0, new PrimitiveFieldExtractor<>(Character.class));
   }
 
+  /**
+   * Matches a String.
+   */
   public static DecomposableMatchBuilder0<String> caseString(String s) {
     List<Matcher<Object>> matchers = new ArrayList<>();
     matchers.add(eq(s));
@@ -149,6 +209,11 @@ public final class PrimitiveCases {
         matchers, new PrimitiveFieldExtractor<>(String.class));
   }
 
+  /**
+   * Matches a String.
+   *
+   * <p>If matched, the String value is extracted.
+   */
   public static DecomposableMatchBuilder1<String, String> caseString(MatchesAny s) {
     List<Matcher<Object>> matchers = new ArrayList<>();
     matchers.add(any());
@@ -157,6 +222,9 @@ public final class PrimitiveCases {
         matchers, 0, new PrimitiveFieldExtractor<>(String.class));
   }
 
+  /**
+   * Matches a boolean.
+   */
   public static DecomposableMatchBuilder0<Boolean> caseBoolean(boolean b) {
     List<Matcher<Object>> matchers = new ArrayList<>();
     matchers.add(eq(b));
@@ -165,6 +233,11 @@ public final class PrimitiveCases {
         matchers, new PrimitiveFieldExtractor<>(Boolean.class));
   }
 
+  /**
+   * Matches a boolean.
+   *
+   * <p>If matched, the boolean value is extracted.
+   */
   public static DecomposableMatchBuilder1<Boolean, Boolean> caseBoolean(MatchesAny b) {
     List<Matcher<Object>> matchers = new ArrayList<>();
     matchers.add(any());
