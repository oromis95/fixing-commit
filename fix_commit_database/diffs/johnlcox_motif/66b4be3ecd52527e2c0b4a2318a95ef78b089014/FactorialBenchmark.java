@@ -16,10 +16,9 @@
 
 package com.leacox.motif.benchmarks;
 
+import static com.leacox.motif.MatchesAny.any;
 import static com.leacox.motif.MatchesExact.eq;
 import static com.leacox.motif.Motif.match;
-import static com.leacox.motif.cases.PrimitiveCases.caseLong;
-import static com.leacox.motif.MatchesAny.any;
 
 import org.openjdk.jmh.annotations.Benchmark;
 import org.openjdk.jmh.annotations.BenchmarkMode;
