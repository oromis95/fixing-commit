@@ -56,6 +56,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
   }
 
   //region decomposeFirst
+
+  /**
+   * Returns a new {@link DecomposableMatchBuilder2} that further decomposes the first element of
+   * this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements.
+   */
   public DecomposableMatchBuilder2<T, B, C> decomposeFirst(DecomposableMatchBuilder0<A> a) {
     Objects.requireNonNull(a);
 
@@ -74,6 +81,12 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the first element of
+   * this match builder.
+   *
+   * <p>The first element is decomposed to 1 element.
+   */
   public <A1> DecomposableMatchBuilder3<T, A1, B, C> decomposeFirst(
       DecomposableMatchBuilder1<A, A1> a) {
     Objects.requireNonNull(a);
@@ -96,6 +109,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
   //endregion
 
   //region decomposeSecond
+
+  /**
+   * Returns a new {@link DecomposableMatchBuilder2} that further decomposes the second element of
+   * this match builder.
+   *
+   * <p>The second element is decomposed to 0 elements.
+   */
   public DecomposableMatchBuilder2<T, A, C> decomposeSecond(DecomposableMatchBuilder0<B> b) {
     Objects.requireNonNull(b);
 
@@ -114,6 +134,12 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the second element of
+   * this match builder.
+   *
+   * <p>The second element is decomposed to 1 element.
+   */
   public <B1> DecomposableMatchBuilder3<T, A, B1, C> decomposeSecond(
       DecomposableMatchBuilder1<B, B1> b) {
     Objects.requireNonNull(b);
@@ -136,6 +162,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
   //endregion
 
   //region decomposeThird
+
+  /**
+   * Returns a new {@link DecomposableMatchBuilder2} that further decomposes the third element of
+   * this match builder.
+   *
+   * <p>The third element is decomposed to 0 elements.
+   */
   public DecomposableMatchBuilder2<T, A, B> decomposeThird(DecomposableMatchBuilder0<C> c) {
     Objects.requireNonNull(c);
 
@@ -152,6 +185,12 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the third element of
+   * this match builder.
+   *
+   * <p>The third element is decomposed to 1 elements.
+   */
   public <C1> DecomposableMatchBuilder3<T, A, B, C1> decomposeThird(
       DecomposableMatchBuilder1<C, C1> c) {
     Objects.requireNonNull(c);
@@ -174,6 +213,14 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
   //endregion
 
   //region decomposeFirstAndSecond
+
+  /**
+   * Returns a new {@link DecomposableMatchBuilder1} that further decomposes the first and second
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements, and the second element is decomposed to 0
+   * elements.
+   */
   public DecomposableMatchBuilder1<T, C> decomposeFirstAndSecond(
       DecomposableMatchBuilder0<A> a, DecomposableMatchBuilder0<B> b) {
     Objects.requireNonNull(a);
@@ -190,6 +237,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
         matchers, extractedIndexes.third(), nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder2} that further decomposes the first and second
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 1 element, and the second element is decomposed to 0
+   * elements.
+   */
   public <A1> DecomposableMatchBuilder2<T, A1, C> decomposeFirstAndSecond(
       DecomposableMatchBuilder1<A, A1> a, DecomposableMatchBuilder0<B> b) {
     Objects.requireNonNull(a);
@@ -210,6 +264,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder2} that further decomposes the first and second
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements, and the second element is decomposed to 1
+   * element.
+   */
   public <B1> DecomposableMatchBuilder2<T, B1, C> decomposeFirstAndSecond(
       DecomposableMatchBuilder0<A> a, DecomposableMatchBuilder1<B, B1> b) {
     Objects.requireNonNull(a);
@@ -229,6 +290,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the first and second
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements, and the second element is decomposed to 2
+   * elements.
+   */
   public <B1, B2> DecomposableMatchBuilder3<T, B1, B2, C> decomposeFirstAndSecond(
       DecomposableMatchBuilder0<A> a, DecomposableMatchBuilder2<B, B1, B2> b) {
     Objects.requireNonNull(a);
@@ -249,6 +317,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder3<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the first and second
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 1 element, and the second element is decomposed to 1
+   * element.
+   */
   public <A1, B1> DecomposableMatchBuilder3<T, A1, B1, C> decomposeFirstAndSecond(
       DecomposableMatchBuilder1<A, A1> a, DecomposableMatchBuilder1<B, B1> b) {
     Objects.requireNonNull(a);
@@ -269,6 +344,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder3<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the first and second
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 2 elements, and the second element is decomposed to 0
+   * elements.
+   */
   public <A1, A2> DecomposableMatchBuilder3<T, A1, A2, C> decomposeFirstAndSecond(
       DecomposableMatchBuilder2<A, A1, A2> a, DecomposableMatchBuilder0<B> b) {
     Objects.requireNonNull(a);
@@ -291,6 +373,14 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
   //endregion
 
   //region decomposeFirstAndThird
+
+  /**
+   * Returns a new {@link DecomposableMatchBuilder1} that further decomposes the first and third
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements, and the third element is decomposed to 0
+   * elements.
+   */
   public DecomposableMatchBuilder1<T, B> decomposeFirstAndThird(
       DecomposableMatchBuilder0<A> a, DecomposableMatchBuilder0<C> c) {
     Objects.requireNonNull(a);
@@ -307,6 +397,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
         matchers, extractedIndexes.second(), nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder2} that further decomposes the first and third
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements, and the third element is decomposed to 1
+   * element.
+   */
   public <C1> DecomposableMatchBuilder2<T, B, C1> decomposeFirstAndThird(
       DecomposableMatchBuilder0<A> a, DecomposableMatchBuilder1<C, C1> c) {
     Objects.requireNonNull(a);
@@ -326,6 +423,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder2} that further decomposes the first and third
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 1 element, and the third element is decomposed to 0
+   * elements.
+   */
   public <A1> DecomposableMatchBuilder2<T, A1, B> decomposeFirstAndThird(
       DecomposableMatchBuilder1<A, A1> a, DecomposableMatchBuilder0<C> c) {
     Objects.requireNonNull(a);
@@ -346,6 +450,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the first and third
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 1 element, and the third element is decomposed to 1
+   * element.
+   */
   public <A1, C1> DecomposableMatchBuilder3<T, A1, B, C1> decomposeFirstAndThird(
       DecomposableMatchBuilder1<A, A1> a, DecomposableMatchBuilder1<C, C1> c) {
     Objects.requireNonNull(a);
@@ -366,6 +477,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder3<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the first and third
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements, and the third element is decomposed to 2
+   * elements.
+   */
   public <C1, C2> DecomposableMatchBuilder3<T, B, C1, C2> decomposeFirstAndThird(
       DecomposableMatchBuilder0<A> a, DecomposableMatchBuilder2<C, C1, C2> c) {
     Objects.requireNonNull(a);
@@ -386,6 +504,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder3<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the first and third
+   * elements of this match builder.
+   *
+   * <p>The first element is decomposed to 2 elements, and the third element is decomposed to 0
+   * elements.
+   */
   public <A1, A2> DecomposableMatchBuilder3<T, A1, A2, B> decomposeFirstAndThird(
       DecomposableMatchBuilder2<A, A1, A2> a, DecomposableMatchBuilder0<C> c) {
     Objects.requireNonNull(a);
@@ -408,6 +533,14 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
   //endregion
 
   //region decomposeSecondAndThird
+
+  /**
+   * Returns a new {@link DecomposableMatchBuilder1} that further decomposes the second and third
+   * elements of this match builder.
+   *
+   * <p>The second element is decomposed to 0 elements, and the third element is decomposed to 0
+   * elements.
+   */
   public DecomposableMatchBuilder1<T, A> decomposeSecondAndThird(
       DecomposableMatchBuilder0<B> b, DecomposableMatchBuilder0<C> c) {
     Objects.requireNonNull(b);
@@ -424,6 +557,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
         matchers, extractedIndexes.first(), nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder1} that further decomposes the second and third
+   * elements of this match builder.
+   *
+   * <p>The second element is decomposed to 0 elements, and the third element is decomposed to 1
+   * element.
+   */
   public <C1> DecomposableMatchBuilder2<T, A, C1> decomposeSecondAndThird(
       DecomposableMatchBuilder0<B> b, DecomposableMatchBuilder1<C, C1> c) {
     Objects.requireNonNull(b);
@@ -442,6 +582,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the second and third
+   * elements of this match builder.
+   *
+   * <p>The second element is decomposed to 0 elements, and the third element is decomposed to 2
+   * elements.
+   */
   public <C1, C2> DecomposableMatchBuilder3<T, A, C1, C2> decomposeSecondAndThird(
       DecomposableMatchBuilder0<B> b, DecomposableMatchBuilder2<C, C1, C2> c) {
     Objects.requireNonNull(b);
@@ -463,6 +610,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder3<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder2} that further decomposes the second and third
+   * elements of this match builder.
+   *
+   * <p>The second element is decomposed to 1 element, and the third element is decomposed to 0
+   * elements.
+   */
   public <B1> DecomposableMatchBuilder2<T, A, B1> decomposeSecondAndThird(
       DecomposableMatchBuilder1<B, B1> b, DecomposableMatchBuilder0<C> c) {
     Objects.requireNonNull(b);
@@ -483,6 +637,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the second and third
+   * elements of this match builder.
+   *
+   * <p>The second element is decomposed to 1 element, and the third element is decomposed to 1
+   * element.
+   */
   public <B1, C1> DecomposableMatchBuilder3<T, A, B1, C1> decomposeSecondAndThird(
       DecomposableMatchBuilder1<B, B1> b, DecomposableMatchBuilder1<C, C1> c) {
     Objects.requireNonNull(b);
@@ -504,6 +665,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder3<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the second and third
+   * elements of this match builder.
+   *
+   * <p>The second element is decomposed to 2 elements, and the third element is decomposed to 0
+   * elements.
+   */
   public <B1, B2> DecomposableMatchBuilder3<T, A, B1, B2> decomposeSecondAndThird(
       DecomposableMatchBuilder2<B, B1, B2> b, DecomposableMatchBuilder0<C> c) {
     Objects.requireNonNull(b);
@@ -527,6 +695,14 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
   //endregion
 
   //region decomposeFirstAndSecondAndThird
+
+  /**
+   * Returns a new {@link DecomposableMatchBuilder0} that further decomposes the first, second and
+   * third elements of this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements, and the second element is decomposed to 0
+   * elements, and the third element is decomposed to 0 elements.
+   */
   public DecomposableMatchBuilder0<T> decomposeFirstAndSecondAndThird(
       DecomposableMatchBuilder0<A> a, DecomposableMatchBuilder0<B> b,
       DecomposableMatchBuilder0<C> c) {
@@ -545,6 +721,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder0<>(matchers, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder1} that further decomposes the first, second and
+   * third elements of this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements, and the second element is decomposed to 0
+   * elements, and the third element is decomposed to 1 element.
+   */
   public <C1> DecomposableMatchBuilder1<T, C1> decomposeFirstAndSecondAndThird(
       DecomposableMatchBuilder0<A> a, DecomposableMatchBuilder0<B> b,
       DecomposableMatchBuilder1<C, C1> c) {
@@ -564,6 +747,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
         matchers, extractedIndexes.third() + c.extractedIndex, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder2} that further decomposes the first, second and
+   * third elements of this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements, and the second element is decomposed to 0
+   * elements, and the third element is decomposed to 2 elements.
+   */
   public <C1, C2> DecomposableMatchBuilder2<T, C1, C2> decomposeFirstAndSecondAndThird(
       DecomposableMatchBuilder0<A> a, DecomposableMatchBuilder0<B> b,
       DecomposableMatchBuilder2<C, C1, C2> c) {
@@ -586,6 +776,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the first, second and
+   * third elements of this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements, and the second element is decomposed to 0
+   * elements, and the third element is decomposed to 3 elements.
+   */
   public <C1, C2, C3> DecomposableMatchBuilder3<T, C1, C2, C3> decomposeFirstAndSecondAndThird(
       DecomposableMatchBuilder0<A> a, DecomposableMatchBuilder0<B> b,
       DecomposableMatchBuilder3<C, C1, C2, C3> c) {
@@ -609,6 +806,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder3<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder1} that further decomposes the first, second and
+   * third elements of this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements, and the second element is decomposed to 1
+   * element, and the third element is decomposed to 0 elements.
+   */
   public <B1> DecomposableMatchBuilder1<T, B1> decomposeFirstAndSecondAndThird(
       DecomposableMatchBuilder0<A> a, DecomposableMatchBuilder1<B, B1> b,
       DecomposableMatchBuilder0<C> c) {
@@ -628,6 +832,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
         matchers, extractedIndexes.second() + b.extractedIndex, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder2} that further decomposes the first, second and
+   * third elements of this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements, and the second element is decomposed to 1
+   * element, and the third element is decomposed to 1 element.
+   */
   public <B1, C1> DecomposableMatchBuilder2<T, B1, C1> decomposeFirstAndSecondAndThird(
       DecomposableMatchBuilder0<A> a, DecomposableMatchBuilder1<B, B1> b,
       DecomposableMatchBuilder1<C, C1> c) {
@@ -650,6 +861,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the first, second and
+   * third elements of this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements, and the second element is decomposed to 1
+   * element, and the third element is decomposed to 2 elements.
+   */
   public <B1, C1, C2> DecomposableMatchBuilder3<T, B1, C1, C2> decomposeFirstAndSecondAndThird(
       DecomposableMatchBuilder0<A> a, DecomposableMatchBuilder1<B, B1> b,
       DecomposableMatchBuilder2<C, C1, C2> c) {
@@ -673,6 +891,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder3<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder2} that further decomposes the first, second and
+   * third elements of this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements, and the second element is decomposed to 2
+   * elements, and the third element is decomposed to 0 elements.
+   */
   public <B1, B2> DecomposableMatchBuilder2<T, B1, B2> decomposeFirstAndSecondAndThird(
       DecomposableMatchBuilder0<A> a, DecomposableMatchBuilder2<B, B1, B2> b,
       DecomposableMatchBuilder0<C> c) {
@@ -695,6 +920,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the first, second and
+   * third elements of this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements, and the second element is decomposed to 2
+   * elements, and the third element is decomposed to 1 element.
+   */
   public <B1, B2, C1> DecomposableMatchBuilder3<T, B1, B2, C1> decomposeFirstAndSecondAndThird(
       DecomposableMatchBuilder0<A> a, DecomposableMatchBuilder2<B, B1, B2> b,
       DecomposableMatchBuilder1<C, C1> c) {
@@ -718,6 +950,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder3<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the first, second and
+   * third elements of this match builder.
+   *
+   * <p>The first element is decomposed to 0 elements, and the second element is decomposed to 3
+   * elements, and the third element is decomposed to 0 elements.
+   */
   public <B1, B2, B3> DecomposableMatchBuilder3<T, B1, B2, B3> decomposeFirstAndSecondAndThird(
       DecomposableMatchBuilder0<A> a, DecomposableMatchBuilder3<B, B1, B2, B3> b,
       DecomposableMatchBuilder0<C> c) {
@@ -741,6 +980,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder3<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder1} that further decomposes the first, second and
+   * third elements of this match builder.
+   *
+   * <p>The first element is decomposed to 1 element, and the second element is decomposed to 0
+   * elements, and the third element is decomposed to 0 elements.
+   */
   public <A1> DecomposableMatchBuilder1<T, A1> decomposeFirstAndSecondAndThird(
       DecomposableMatchBuilder1<A, A1> a, DecomposableMatchBuilder0<B> b,
       DecomposableMatchBuilder0<C> c) {
@@ -760,6 +1006,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
         matchers, extractedIndexes.first() + a.extractedIndex, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder2} that further decomposes the first, second and
+   * third elements of this match builder.
+   *
+   * <p>The first element is decomposed to 1 element, and the second element is decomposed to 0
+   * elements, and the third element is decomposed to 1 element.
+   */
   public <A1, C1> DecomposableMatchBuilder2<T, A1, C1> decomposeFirstAndSecondAndThird(
       DecomposableMatchBuilder1<A, A1> a, DecomposableMatchBuilder0<B> b,
       DecomposableMatchBuilder1<C, C1> c) {
@@ -782,6 +1035,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the first, second and
+   * third elements of this match builder.
+   *
+   * <p>The first element is decomposed to 1 element, and the second element is decomposed to 0
+   * elements, and the third element is decomposed to 2 elements.
+   */
   public <A1, C1, C2> DecomposableMatchBuilder3<T, A1, C1, C2> decomposeFirstAndSecondAndThird(
       DecomposableMatchBuilder1<A, A1> a, DecomposableMatchBuilder0<B> b,
       DecomposableMatchBuilder2<C, C1, C2> c) {
@@ -805,6 +1065,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder3<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder2} that further decomposes the first, second and
+   * third elements of this match builder.
+   *
+   * <p>The first element is decomposed to 1 element, and the second element is decomposed to 1
+   * element, and the third element is decomposed to 0 elements.
+   */
   public <A1, B1> DecomposableMatchBuilder2<T, A1, B1> decomposeFirstAndSecondAndThird(
       DecomposableMatchBuilder1<A, A1> a, DecomposableMatchBuilder1<B, B1> b,
       DecomposableMatchBuilder0<C> c) {
@@ -827,6 +1094,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the first, second and
+   * third elements of this match builder.
+   *
+   * <p>The first element is decomposed to 1 element, and the second element is decomposed to 1
+   * element, and the third element is decomposed to 1 element.
+   */
   public <A1, B1, C1> DecomposableMatchBuilder3<T, A1, B1, C1> decomposeFirstAndSecondAndThird(
       DecomposableMatchBuilder1<A, A1> a, DecomposableMatchBuilder1<B, B1> b,
       DecomposableMatchBuilder1<C, C1> c) {
@@ -850,6 +1124,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder3<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the first, second and
+   * third elements of this match builder.
+   *
+   * <p>The first element is decomposed to 1 element, and the second element is decomposed to 2
+   * elements, and the third element is decomposed to 0 elements.
+   */
   public <A1, B1, B2> DecomposableMatchBuilder3<T, A1, B1, B2> decomposeFirstAndSecondAndThird(
       DecomposableMatchBuilder1<A, A1> a, DecomposableMatchBuilder2<B, B1, B2> b,
       DecomposableMatchBuilder0<C> c) {
@@ -873,6 +1154,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder3<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder2} that further decomposes the first, second and
+   * third elements of this match builder.
+   *
+   * <p>The first element is decomposed to 2 elements, and the second element is decomposed to 0
+   * elements, and the third element is decomposed to 0 elements.
+   */
   public <A1, A2> DecomposableMatchBuilder2<T, A1, A2> decomposeFirstAndSecondAndThird(
       DecomposableMatchBuilder2<A, A1, A2> a, DecomposableMatchBuilder0<B> b,
       DecomposableMatchBuilder0<C> c) {
@@ -895,6 +1183,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder2<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the first, second and
+   * third elements of this match builder.
+   *
+   * <p>The first element is decomposed to 2 elements, and the second element is decomposed to 0
+   * elements, and the third element is decomposed to 1 element.
+   */
   public <A1, A2, C1> DecomposableMatchBuilder3<T, A1, A2, C1> decomposeFirstAndSecondAndThird(
       DecomposableMatchBuilder2<A, A1, A2> a, DecomposableMatchBuilder0<B> b,
       DecomposableMatchBuilder1<C, C1> c) {
@@ -918,6 +1213,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder3<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the first, second and
+   * third elements of this match builder.
+   *
+   * <p>The first element is decomposed to 2 elements, and the second element is decomposed to 1
+   * element, and the third element is decomposed to 0 elements.
+   */
   public <A1, A2, B1> DecomposableMatchBuilder3<T, A1, A2, B1> decomposeFirstAndSecondAndThird(
       DecomposableMatchBuilder2<A, A1, A2> a, DecomposableMatchBuilder1<B, B1> b,
       DecomposableMatchBuilder0<C> c) {
@@ -941,6 +1243,13 @@ public final class DecomposableMatchBuilder3<T, A, B, C> extends DecomposableMat
     return new DecomposableMatchBuilder3<>(matchers, newIndexes, nestedFieldExtractor);
   }
 
+  /**
+   * Returns a new {@link DecomposableMatchBuilder3} that further decomposes the first, second and
+   * third elements of this match builder.
+   *
+   * <p>The first element is decomposed to 3 elements, and the second element is decomposed to 0
+   * elements, and the third element is decomposed to 0 elements.
+   */
   public <A1, A2, A3> DecomposableMatchBuilder3<T, A1, A2, A3> decomposeFirstAndSecondAndThird(
       DecomposableMatchBuilder3<A, A1, A2, A3> a, DecomposableMatchBuilder0<B> b,
       DecomposableMatchBuilder0<C> c) {
