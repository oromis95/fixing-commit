@@ -220,8 +220,9 @@ public abstract class LazySeq<E> extends AbstractList<E> {
 
 	/**
 	 * Converts this {@link LazySeq} to immutable {@link List}.
-	 *
+	 * <p/>
 	 * Notice that this method will eventually fail at runtime when called on infinite sequence.
+	 *
 	 * @return {@link List} of all elements in this lazy sequence.
 	 */
 	public List<E> toList() {
@@ -327,7 +328,7 @@ public abstract class LazySeq<E> extends AbstractList<E> {
 	}
 
 	private static <E> E maxByComparator(E first, E second, Comparator<? super E> comparator) {
-		return comparator.compare(first, second) >= 0? first : second;
+		return comparator.compare(first, second) >= 0 ? first : second;
 	}
 
 	@Override
