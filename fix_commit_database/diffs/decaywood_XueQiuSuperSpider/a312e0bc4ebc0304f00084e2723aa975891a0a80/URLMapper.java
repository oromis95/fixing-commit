@@ -9,6 +9,7 @@ public enum URLMapper {
     MAIN_PAGE("http://xueqiu.com"),
     COMPREHENSIVE_PAGE("http://xueqiu.com/hq"),
 
+    CUBES_RANK_JSON("http://xueqiu.com/cubes/discover/rank/cube/list.json"),
     MARKET_QUOTATIONS_RANK_JSON("http://xueqiu.com/stock/quote_order.json"),
     SCOPE_STOCK_RANK_JSON("http://xueqiu.com/stock/rank.json"),
     STOCK_TREND_JSON("http://xueqiu.com/stock/forchart/stocklist.json"),
