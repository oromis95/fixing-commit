@@ -1,7 +1,7 @@
 package org.decaywood.entity.trend;
 
 import org.decaywood.utils.DateParser;
-import org.decaywood.utils.StringChecker;
+import org.decaywood.utils.StringUtils;
 
 import java.util.ArrayList;
 import java.util.Date;
@@ -44,7 +44,7 @@ public class StockTrend extends Trend<StockTrend.TrendBlock, StockTrend> {
         private final Date time;
 
         public TrendBlock(String volume, String avg_price, String current, String time) {
-            if(StringChecker.nullOrEmpty(volume, avg_price, current, time))
+            if(StringUtils.nullOrEmpty(volume, avg_price, current, time))
                 throw new IllegalArgumentException();
             this.volume = volume;
             this.avg_price = avg_price;
