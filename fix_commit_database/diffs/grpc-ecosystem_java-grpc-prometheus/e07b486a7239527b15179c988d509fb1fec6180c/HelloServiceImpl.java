@@ -2,10 +2,11 @@
 
 package com.github.dinowernli.grpc.prometheus.testing;
 
+import com.github.dinowernli.proto.grpc.prometheus.HelloProto.HelloRequest;
+import com.github.dinowernli.proto.grpc.prometheus.HelloProto.HelloResponse;
+import com.github.dinowernli.proto.grpc.prometheus.HelloServiceGrpc.HelloService;
+
 import io.grpc.stub.StreamObserver;
-import polyglot.HelloProto.HelloRequest;
-import polyglot.HelloProto.HelloResponse;
-import polyglot.HelloServiceGrpc.HelloService;
 
 public class HelloServiceImpl implements HelloService {
   /**
