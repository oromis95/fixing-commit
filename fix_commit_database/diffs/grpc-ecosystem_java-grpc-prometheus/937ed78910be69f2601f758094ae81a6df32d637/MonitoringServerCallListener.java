@@ -1,6 +1,7 @@
 package me.dinowernli.grpc.prometheus;
 
 import io.grpc.ForwardingServerCallListener;
+import io.grpc.MethodDescriptor.MethodType;
 import io.grpc.ServerCall;
 
 /**
@@ -10,12 +11,14 @@ import io.grpc.ServerCall;
 class MonitoringServerCallListener<R>
     extends ForwardingServerCallListener<R> {
   private final ServerCall.Listener<R> delegate;
+  private final MethodType methodType;
   private final ServerMetrics serverMetrics;
 
   MonitoringServerCallListener(
-      ServerCall.Listener<R> delegate, ServerMetrics serverMetrics) {
+      ServerCall.Listener<R> delegate, ServerMetrics serverMetrics, MethodType methodType) {
     this.delegate = delegate;
     this.serverMetrics = serverMetrics;
+    this.methodType = methodType;
   }
 
   @Override
@@ -25,7 +28,9 @@ class MonitoringServerCallListener<R>
 
   @Override
   public void onMessage(R request) {
-    serverMetrics.recordMessageReceived();
+    if (methodType == MethodType.CLIENT_STREAMING || methodType == MethodType.BIDI_STREAMING) {
+      serverMetrics.recordStreamMessageReceived();
+    }
     super.onMessage(request);
   }
 }
\ No newline at end of file
