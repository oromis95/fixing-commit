@@ -6,6 +6,7 @@ import static com.google.common.truth.Truth.assertThat;
 
 import java.io.IOException;
 import java.util.Enumeration;
+import java.util.Optional;
 
 import org.junit.After;
 import org.junit.Before;
@@ -28,10 +29,10 @@ import io.grpc.stub.StreamObserver;
 import io.grpc.testing.StreamRecorder;
 import io.grpc.testing.TestUtils;
 import io.prometheus.client.Collector.MetricFamilySamples;
+import io.prometheus.client.CollectorRegistry;
 import me.dinowernli.grpc.prometheus.MonitoringServerInterceptor;
 import me.dinowernli.grpc.prometheus.MonitoringServerInterceptor.Configuration;
 import me.dinowernli.grpc.prometheus.testing.HelloServiceImpl;
-import io.prometheus.client.CollectorRegistry;
 
 /**
  * Integrations tests which make sure that if a service is started with a
@@ -50,6 +51,9 @@ public class MonitoringInterceptorIntegrationTest {
       .setRecipient(RECIPIENT)
       .build();
 
+  private static final Configuration CHEAP_METRICS = Configuration.cheapMetricsOnly();
+  private static final Configuration ALL_METRICS = Configuration.allMetrics();
+
   private CollectorRegistry collectorRegistry;
   private Server grpcServer;
   private int grpcPort;
@@ -57,7 +61,6 @@ public class MonitoringInterceptorIntegrationTest {
   @Before
   public void setUp() {
     collectorRegistry = new CollectorRegistry();
-    startGrpcServer();
   }
 
   @After
@@ -67,9 +70,14 @@ public class MonitoringInterceptorIntegrationTest {
 
   @Test
   public void unaryRpcMetrics() throws Throwable {
+    startGrpcServer(CHEAP_METRICS);
     createGrpcBlockingStub().sayHello(REQUEST);
 
-    MetricFamilySamples handled = findRecordedMetric("grpc_server_handled_total");
+    assertThat(findRecordedMetricOrThrow("grpc_server_started_total").samples).hasSize(1);
+    assertThat(findRecordedMetricOrThrow("grpc_server_msg_received_total").samples).isEmpty();
+    assertThat(findRecordedMetricOrThrow("grpc_server_msg_sent_total").samples).isEmpty();
+
+    MetricFamilySamples handled = findRecordedMetricOrThrow("grpc_server_handled_total");
     assertThat(handled.samples).hasSize(1);
     assertThat(handled.samples.get(0).labelValues).containsExactly(
         "UNARY", SERVICE_NAME, UNARY_METHOD_NAME, "OK");
@@ -78,6 +86,7 @@ public class MonitoringInterceptorIntegrationTest {
 
   @Test
   public void clientStreamRpcMetrics() throws Throwable {
+    startGrpcServer(CHEAP_METRICS);
     StreamRecorder<HelloResponse> streamRecorder = StreamRecorder.create();
     StreamObserver<HelloRequest> requestStream =
         createGrpcStub().sayHelloClientStream(streamRecorder);
@@ -88,7 +97,11 @@ public class MonitoringInterceptorIntegrationTest {
     // Not a blocking stub, so we need to wait.
     streamRecorder.awaitCompletion();
 
-    MetricFamilySamples handled = findRecordedMetric("grpc_server_handled_total");
+    assertThat(findRecordedMetricOrThrow("grpc_server_started_total").samples).hasSize(1);
+    assertThat(findRecordedMetricOrThrow("grpc_server_msg_received_total").samples).hasSize(1);
+    assertThat(findRecordedMetricOrThrow("grpc_server_msg_sent_total").samples).isEmpty();
+
+    MetricFamilySamples handled = findRecordedMetricOrThrow("grpc_server_handled_total");
     assertThat(handled.samples).hasSize(1);
     assertThat(handled.samples.get(0).labelValues).containsExactly(
         "CLIENT_STREAMING", SERVICE_NAME, CLIENT_STREAM_METHOD_NAME, "OK");
@@ -97,16 +110,21 @@ public class MonitoringInterceptorIntegrationTest {
 
   @Test
   public void serverStreamRpcMetrics() throws Throwable {
+    startGrpcServer(CHEAP_METRICS);
     ImmutableList<HelloResponse> responses =
         ImmutableList.copyOf(createGrpcBlockingStub().sayHelloServerStream(REQUEST));
 
-    MetricFamilySamples handled = findRecordedMetric("grpc_server_handled_total");
+    assertThat(findRecordedMetricOrThrow("grpc_server_started_total").samples).hasSize(1);
+    assertThat(findRecordedMetricOrThrow("grpc_server_msg_received_total").samples).isEmpty();
+    assertThat(findRecordedMetricOrThrow("grpc_server_msg_sent_total").samples).hasSize(1);
+
+    MetricFamilySamples handled = findRecordedMetricOrThrow("grpc_server_handled_total");
     assertThat(handled.samples).hasSize(1);
     assertThat(handled.samples.get(0).labelValues).containsExactly(
         "SERVER_STREAMING", SERVICE_NAME, SERVER_STREAM_METHOD_NAME, "OK");
     assertThat(handled.samples.get(0).value).isWithin(0).of(1);
 
-    MetricFamilySamples messagesSent = findRecordedMetric("grpc_server_msg_sent_total");
+    MetricFamilySamples messagesSent = findRecordedMetricOrThrow("grpc_server_msg_sent_total");
     assertThat(messagesSent.samples.get(0).labelValues).containsExactly(
         "SERVER_STREAMING", SERVICE_NAME, SERVER_STREAM_METHOD_NAME);
     assertThat(messagesSent.samples.get(0).value).isWithin(0).of(responses.size());
@@ -114,6 +132,7 @@ public class MonitoringInterceptorIntegrationTest {
 
   @Test
   public void bidiStreamRpcMetrics() throws Throwable {
+    startGrpcServer(CHEAP_METRICS);
     StreamRecorder<HelloResponse> streamRecorder = StreamRecorder.create();
     StreamObserver<HelloRequest> requestStream =
         createGrpcStub().sayHelloBidiStream(streamRecorder);
@@ -124,7 +143,11 @@ public class MonitoringInterceptorIntegrationTest {
     // Not a blocking stub, so we need to wait.
     streamRecorder.awaitCompletion();
 
-    MetricFamilySamples handled = findRecordedMetric("grpc_server_handled_total");
+    assertThat(findRecordedMetricOrThrow("grpc_server_started_total").samples).hasSize(1);
+    assertThat(findRecordedMetricOrThrow("grpc_server_msg_received_total").samples).hasSize(1);
+    assertThat(findRecordedMetricOrThrow("grpc_server_msg_sent_total").samples).hasSize(1);
+
+    MetricFamilySamples handled = findRecordedMetricOrThrow("grpc_server_handled_total");
     assertThat(handled.samples).hasSize(1);
     assertThat(handled.samples.get(0).labelValues).containsExactly(
         "BIDI_STREAMING", SERVICE_NAME, BIDI_STREAM_METHOD_NAME, "OK");
@@ -132,15 +155,44 @@ public class MonitoringInterceptorIntegrationTest {
   }
 
   @Test
-  public void doesNotAddHistogramsIfDisabled() throws Throwable {
+  public void noHistogramIfDisabled() throws Throwable {
+    startGrpcServer(CHEAP_METRICS);
+    createGrpcBlockingStub().sayHello(REQUEST);
+    assertThat(findRecordedMetric("grpc_server_handled_latency_seconds").isPresent()).isFalse();
+  }
+
+  @Test
+  public void addsHistogramIfEnabled() throws Throwable {
+    startGrpcServer(ALL_METRICS);
+    createGrpcBlockingStub().sayHello(REQUEST);
+
+    MetricFamilySamples latency = findRecordedMetricOrThrow("grpc_server_handled_latency_seconds");
+    assertThat(latency.samples.size()).isGreaterThan(0);
+  }
+
+  @Test
+  public void recordsMultipleCalls() throws Throwable {
+    startGrpcServer(CHEAP_METRICS);
+
+    createGrpcBlockingStub().sayHello(REQUEST);
+    createGrpcBlockingStub().sayHello(REQUEST);
     createGrpcBlockingStub().sayHello(REQUEST);
 
-    // TODO(dino): Add plumbing for disabling histograms in the test.
+    StreamRecorder<HelloResponse> streamRecorder = StreamRecorder.create();
+    StreamObserver<HelloRequest> requestStream =
+        createGrpcStub().sayHelloBidiStream(streamRecorder);
+    requestStream.onNext(REQUEST);
+    requestStream.onNext(REQUEST);
+    requestStream.onCompleted();
+    streamRecorder.awaitCompletion();
+
+    assertThat(findRecordedMetricOrThrow("grpc_server_started_total").samples).hasSize(2);
+    assertThat(findRecordedMetricOrThrow("grpc_server_handled_total").samples).hasSize(2);
   }
 
-  private void startGrpcServer() {
+  private void startGrpcServer(Configuration monitoringConfig) {
     MonitoringServerInterceptor interceptor = MonitoringServerInterceptor.create(
-        Configuration.cheapMetricsOnly().withCollectorRegistry(collectorRegistry));
+        monitoringConfig.withCollectorRegistry(collectorRegistry));
     grpcPort = TestUtils.pickUnusedPort();
     grpcServer = ServerBuilder.forPort(grpcPort)
         .addService(ServerInterceptors.intercept(
@@ -153,15 +205,23 @@ public class MonitoringInterceptorIntegrationTest {
     }
   }
 
-  private MetricFamilySamples findRecordedMetric(String name) {
+  private Optional<MetricFamilySamples> findRecordedMetric(String name) {
     Enumeration<MetricFamilySamples> samples = collectorRegistry.metricFamilySamples();
     while (samples.hasMoreElements()) {
       MetricFamilySamples sample = samples.nextElement();
       if (sample.name.equals(name)) {
-        return sample;
+        return Optional.of(sample);
       }
     }
-    throw new IllegalArgumentException("Could not find metric with name: " + name);
+    return Optional.empty();
+  }
+
+  private MetricFamilySamples findRecordedMetricOrThrow(String name) {
+    Optional<MetricFamilySamples> result = findRecordedMetric(name);
+    if (!result.isPresent()){
+      throw new IllegalArgumentException("Could not find metric with name: " + name);
+    }
+    return result.get();
   }
 
   private HelloServiceBlockingStub createGrpcBlockingStub() {
