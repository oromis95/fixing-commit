@@ -39,7 +39,7 @@ public class UnknownRecordDecoder extends AbstractRecordDecoder<UnknownRecord> {
 		
 		/**
 		The value 0x05 (Unknown) SHOULD be used to indicate that the type of the payload is
-		unknown. This is similar to the application/octet-stream media type defined by MIME [RFC
+		unknown. This is similar to the "application/octet-stream" media type defined by MIME [RFC
 		2046]. When used, the TYPE_LENGTH field MUST be zero and thus the TYPE field is omitted
 		from the NDEF record. Regarding implementation, it is RECOMMENDED that an NDEF parser
 		receiving an NDEF record of this type, without further context to its use, provides a mechanism
