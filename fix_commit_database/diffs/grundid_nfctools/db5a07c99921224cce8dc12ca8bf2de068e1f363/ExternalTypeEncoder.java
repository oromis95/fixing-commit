@@ -16,6 +16,7 @@
 package org.nfctools.ndef.ext;
 
 import org.nfctools.ndef.NdefConstants;
+import org.nfctools.ndef.NdefEncoderException;
 import org.nfctools.ndef.NdefMessageEncoder;
 import org.nfctools.ndef.NdefRecord;
 import org.nfctools.ndef.Record;
@@ -31,6 +32,14 @@ public class ExternalTypeEncoder implements RecordEncoder {
 	@Override
 	public NdefRecord encodeRecord(Record record, NdefMessageEncoder messageEncoder) {
 		ExternalTypeRecord externalType = (ExternalTypeRecord)record;
+		
+		if(!externalType.hasNamespace()) {
+			throw new NdefEncoderException("Expected namespace", record);
+		}
+		if(!externalType.hasContent()) {
+			throw new NdefEncoderException("Expected content", record);
+		}
+		
 		byte[] type = externalType.getNamespace().getBytes(NdefConstants.DEFAULT_CHARSET);
 		byte[] paylod = externalType.getContent().getBytes(NdefConstants.DEFAULT_CHARSET);
 		return new NdefRecord(NdefConstants.TNF_EXTERNAL_TYPE, type, record.getId(), paylod);
