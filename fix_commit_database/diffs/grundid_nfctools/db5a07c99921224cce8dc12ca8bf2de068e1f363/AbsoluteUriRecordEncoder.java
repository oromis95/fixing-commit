@@ -17,6 +17,7 @@
 package org.nfctools.ndef.auri;
 
 import org.nfctools.ndef.NdefConstants;
+import org.nfctools.ndef.NdefEncoderException;
 import org.nfctools.ndef.NdefMessageEncoder;
 import org.nfctools.ndef.NdefRecord;
 import org.nfctools.ndef.Record;
@@ -38,6 +39,11 @@ public class AbsoluteUriRecordEncoder implements RecordEncoder {
 	@Override
 	public NdefRecord encodeRecord(Record record, NdefMessageEncoder messageEncoder) {
 		AbsoluteUriRecord absoluteUriRecord = (AbsoluteUriRecord)record;
+		
+		if(!absoluteUriRecord.hasUri()) {
+			throw new NdefEncoderException("Expected URI", record);
+		}
+		
 		return new NdefRecord(NdefConstants.TNF_ABSOLUTE_URI, AbsoluteUriRecord.TYPE, absoluteUriRecord.getId(),
 				absoluteUriRecord.getUri().getBytes(NdefConstants.DEFAULT_CHARSET));
 	}
