@@ -22,6 +22,12 @@ import org.nfctools.ndef.NdefRecord;
 import org.nfctools.ndef.Record;
 import org.nfctools.ndef.wkt.encoder.RecordEncoder;
 
+/**
+ * 
+ * @author Thomas Rorvik Skjolberg (skjolber@gmail.com)
+ *
+ */
+
 public class UnknownRecordEncoder implements RecordEncoder {
 
 	@Override
@@ -31,7 +37,9 @@ public class UnknownRecordEncoder implements RecordEncoder {
 
 	@Override
 	public NdefRecord encodeRecord(Record record, NdefMessageEncoder messageEncoder) {
-		UnknownRecord unknownRecord = (UnknownRecord)record;
+		if(record.getClass() != UnknownRecord.class) {
+			throw new IllegalArgumentException("Unexpected Record " + record.getClass().getName());
+		}
 		
 	    return new NdefRecord(NdefConstants.TNF_UNKNOWN, NdefConstants.EMPTY_BYTE_ARRAY, NdefConstants.EMPTY_BYTE_ARRAY, NdefConstants.EMPTY_BYTE_ARRAY);
 
