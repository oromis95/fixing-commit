@@ -1,3 +1,18 @@
+/**
+ * Copyright 2011 Adrian Stabiszewski, as@nfctools.org
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *       http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
 
 package org.nfctools.ndef.unchanged;
 
@@ -7,6 +22,12 @@ import org.nfctools.ndef.NdefRecord;
 import org.nfctools.ndef.Record;
 import org.nfctools.ndef.wkt.encoder.RecordEncoder;
 
+/**
+ * 
+ * @author Thomas Rorvik Skjolberg (skjolber@gmail.com)
+ *
+ */
+
 public class UnchangedRecordEncoder implements RecordEncoder {
 
 	@Override
@@ -16,7 +37,9 @@ public class UnchangedRecordEncoder implements RecordEncoder {
 
 	@Override
 	public NdefRecord encodeRecord(Record record, NdefMessageEncoder messageEncoder) {
-		UnchangedRecord unchangedRecord = (UnchangedRecord)record;
+		if(record.getClass() != UnchangedRecord.class) {
+			throw new IllegalArgumentException("Unexpected Record " + record.getClass().getName());
+		}
 		
 	    return new NdefRecord(NdefConstants.TNF_UNCHANGED, NdefConstants.EMPTY_BYTE_ARRAY, NdefConstants.EMPTY_BYTE_ARRAY, NdefConstants.EMPTY_BYTE_ARRAY);
 
