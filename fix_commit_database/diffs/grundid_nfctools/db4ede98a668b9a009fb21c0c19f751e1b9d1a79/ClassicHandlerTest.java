@@ -38,14 +38,13 @@ public class ClassicHandlerTest {
 
 	@Test
 	public void testIsFormattedWritable() throws Exception {
-
 		ApplicationDirectory applicationDirectory = ReaderWriterCreator.createReadWriter("mfstd1k_ndef.txt",
 				MemoryLayout.CLASSIC_1K).getApplicationDirectory();
-		assertTrue(ClassicHandler.isFormattedWritable(applicationDirectory.openApplication(MfConstants.NDEF_APP_ID)));
-
+		assertTrue(ClassicHandler.isFormattedWritable(applicationDirectory.openApplication(MfConstants.NDEF_APP_ID),
+				MfClassicConstants.TRANSPORT_KEY));
 		ApplicationDirectory applicationDirectory2 = ReaderWriterCreator.createReadWriter("mfstd4k_ndef.txt",
 				MemoryLayout.CLASSIC_4K).getApplicationDirectory();
-
-		assertTrue(ClassicHandler.isFormattedWritable(applicationDirectory2.openApplication(MfConstants.NDEF_APP_ID)));
+		assertTrue(ClassicHandler.isFormattedWritable(applicationDirectory2.openApplication(MfConstants.NDEF_APP_ID),
+				MfClassicConstants.NDEF_KEY));
 	}
 }
