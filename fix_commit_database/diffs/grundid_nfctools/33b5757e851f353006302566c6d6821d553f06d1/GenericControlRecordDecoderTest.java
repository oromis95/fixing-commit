@@ -15,7 +15,9 @@
  */
 package org.nfctools.ndef.decoder;
 
-import static org.junit.Assert.*;
+import static org.junit.Assert.assertEquals;
+import static org.junit.Assert.assertNotNull;
+import static org.junit.Assert.assertTrue;
 
 import org.junit.Test;
 import org.nfctools.ndef.NdefContext;
