@@ -15,7 +15,9 @@
  */
 package org.nfctools.ndef;
 
-import static org.junit.Assert.*;
+import static org.junit.Assert.assertArrayEquals;
+import static org.junit.Assert.assertEquals;
+import static org.junit.Assert.assertTrue;
 
 import org.junit.Test;
 import org.nfctools.utils.NfcUtils;
