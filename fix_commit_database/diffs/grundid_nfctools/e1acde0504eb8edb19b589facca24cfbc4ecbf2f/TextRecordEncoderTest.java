@@ -17,14 +17,15 @@
 
 package org.nfctools.ndef.encoder;
 
-import static org.junit.Assert.*;
+import static org.junit.Assert.assertEquals;
+import static org.junit.Assert.assertTrue;
 
 import java.nio.charset.Charset;
 import java.util.Locale;
 
 import org.junit.Test;
-import org.nfctools.ndef.NdefContext;
 import org.nfctools.ndef.NdefConstants;
+import org.nfctools.ndef.NdefContext;
 import org.nfctools.ndef.NdefMessageEncoder;
 import org.nfctools.ndef.NdefRecord;
 import org.nfctools.ndef.wkt.encoder.TextRecordEncoder;
