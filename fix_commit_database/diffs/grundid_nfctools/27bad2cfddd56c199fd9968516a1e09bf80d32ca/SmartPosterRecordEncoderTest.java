@@ -23,7 +23,9 @@ import java.util.Locale;
 import org.junit.Test;
 import org.nfctools.ndef.NdefContext;
 import org.nfctools.ndef.NdefMessageEncoder;
-import org.nfctools.ndef.wkt.encoder.SmartPosterRecordEncoder;
+import org.nfctools.ndef.decoder.SmartPosterDecoderTest;
+import org.nfctools.ndef.wkt.records.Action;
+import org.nfctools.ndef.wkt.records.ActionRecord;
 import org.nfctools.ndef.wkt.records.SmartPosterRecord;
 import org.nfctools.ndef.wkt.records.TextRecord;
 import org.nfctools.ndef.wkt.records.UriRecord;
@@ -31,18 +33,29 @@ import org.nfctools.utils.NfcUtils;
 
 public class SmartPosterRecordEncoderTest {
 
-	private SmartPosterRecordEncoder encoder = new SmartPosterRecordEncoder();
 	private NdefMessageEncoder messageEncoder = NdefContext.getNdefMessageEncoder();
 
-	private String innerSmartPoster = "D101075402646554657374D101365500736D733"
-			+ "A2B3439313233343536373839303F626F64793D486921253230576965253230676568742532306573253230646972253346";
+	private String innerSmartPoster = "D10245537091010754026465546573745101365500736D733A2B3439313233343536373839"
+			+ "303F626F64793D486921253230576965253230676568742532306573253230646972253346";
 
 	@Test
 	public void testEncode() throws Exception {
 		SmartPosterRecord smartPosterRecord = new SmartPosterRecord();
 		smartPosterRecord.setTitle(new TextRecord("Test", Charset.forName("UTF8"), Locale.GERMAN));
 		smartPosterRecord.setUri(new UriRecord("sms:+491234567890?body=Hi!%20Wie%20geht%20es%20dir%3F"));
-		byte[] payload = encoder.encodePayload(smartPosterRecord, messageEncoder);
+		byte[] payload = messageEncoder.encodeSingle(smartPosterRecord);
+		System.out.println(innerSmartPoster);
+		System.out.println(NfcUtils.convertBinToASCII(payload));
 		assertEquals(innerSmartPoster, NfcUtils.convertBinToASCII(payload));
 	}
+
+	@Test
+	public void testEncode2() throws Exception {
+		SmartPosterRecord smartPosterRecord = new SmartPosterRecord();
+		smartPosterRecord.setTitle(new TextRecord("Title", Charset.forName("UTF8"), Locale.GERMANY));
+		smartPosterRecord.setUri(new UriRecord("http://www.winfuture.de"));
+		smartPosterRecord.setAction(new ActionRecord(Action.DEFAULT_ACTION));
+		byte[] payload = messageEncoder.encodeSingle(smartPosterRecord);
+		assertEquals(SmartPosterDecoderTest.smartPoster, NfcUtils.convertBinToASCII(payload));
+	}
 }
