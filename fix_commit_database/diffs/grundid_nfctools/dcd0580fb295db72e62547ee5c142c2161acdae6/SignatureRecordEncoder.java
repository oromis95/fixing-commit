@@ -32,7 +32,6 @@ public class SignatureRecordEncoder implements WellKnownRecordPayloadEncoder {
 				if(!signatureRecord.hasSignatureType()) {
 					throw new NdefEncoderException("Expected signature type", signatureRecord);
 				}
-				baos.write((1 << 7) | (signatureRecord.getSignatureType().getValue() & 0x7F));
 
 				if(signatureRecord.hasSignature() && signatureRecord.hasSignatureUri()) {
 					throw new NdefEncoderException("Expected signature or signature uri, not both", signatureRecord);
@@ -40,6 +39,8 @@ public class SignatureRecordEncoder implements WellKnownRecordPayloadEncoder {
 					throw new NdefEncoderException("Expected signature or signature uri", signatureRecord);
 				}
 
+				baos.write(((signatureRecord.hasSignatureUri() ? 1 : 0) << 7) | (signatureRecord.getSignatureType().getValue() & 0x7F));
+
 				byte[] signatureOrUri;
 				if(signatureRecord.hasSignature()) {
 					signatureOrUri = signatureRecord.getSignature();
