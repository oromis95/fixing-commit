@@ -35,7 +35,7 @@ import org.apache.commons.lang3.StringUtils;
  * @author Maciej Szewczyszyn
  * 
  * @deprecated use {@link net.kaczmarzyk.spring.data.jpa.domain.Equal Equal}
- *  or {@link net.kaczmarzyk.spring.data.jpa.domain.In In} specifications that handle enums and other data types
+ *  or {@code In} specifications that handle enums and other data types
  */
 @Deprecated
 public class EqualEnum<T> extends PathSpecification<T> {
