@@ -24,7 +24,6 @@ import javax.persistence.criteria.Predicate;
 import javax.persistence.criteria.Root;
 
 import org.springframework.data.jpa.domain.Specification;
-import org.springframework.data.jpa.domain.Specifications;
 
 /**
  * Helper for easier joining lists of specs with {@code AND} operator
@@ -33,7 +32,9 @@ import org.springframework.data.jpa.domain.Specifications;
  */
 public class Conjunction<T> implements Specification<T>, FakeSpecWrapper<T> {
 
-    private Collection<Specification<T>> innerSpecs;
+	private static final long serialVersionUID = 1L;
+	
+	private Collection<Specification<T>> innerSpecs;
 
     
     @SafeVarargs
@@ -62,13 +63,13 @@ public class Conjunction<T> implements Specification<T>, FakeSpecWrapper<T> {
 	@Override
     public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
     	initializeFakes(root, query, cb);
-        Specifications<T> combinedSpecs = null;
+        Specification<T> combinedSpecs = null;
         for (Specification<T> spec : innerSpecs) {
         	if (spec instanceof Fake) {
         		continue;
         	}
             if (combinedSpecs == null) {
-                combinedSpecs = Specifications.where(spec);
+                combinedSpecs = Specification.where(spec);
             } else {
                 combinedSpecs = combinedSpecs.and(spec);
             }
