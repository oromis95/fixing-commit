@@ -36,7 +36,9 @@ import net.kaczmarzyk.spring.data.jpa.utils.QueryContext;
 @Deprecated
 public class DateAfter<T> extends DateSpecification<T> {
 
-    private Date date;
+	private static final long serialVersionUID = 1L;
+	
+	private Date date;
 
     public DateAfter(QueryContext queryContext, String path, String[] args, Converter converter) throws ParseException {
         super(queryContext, path, args, converter);
