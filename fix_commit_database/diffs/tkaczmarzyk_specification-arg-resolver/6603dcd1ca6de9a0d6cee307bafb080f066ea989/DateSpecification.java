@@ -26,7 +26,9 @@ import net.kaczmarzyk.spring.data.jpa.utils.QueryContext;
  */
 abstract class DateSpecification<T> extends PathSpecification<T> {
 
-    protected Converter converter;
+	private static final long serialVersionUID = 1L;
+	
+	protected Converter converter;
 
     protected DateSpecification(QueryContext queryContext, String path, String[] args, Converter converter) throws ParseException {
         super(queryContext, path);
