@@ -32,6 +32,8 @@ import javax.persistence.criteria.Root;
  **/
 public class NotEqual<T> extends PathSpecification<T> {
 
+	private static final long serialVersionUID = 1L;
+	
 	protected String expectedValue;
 	private Converter converter;
 
