@@ -17,14 +17,14 @@ package net.kaczmarzyk.spring.data.jpa;
 
 import java.util.List;
 
-import net.kaczmarzyk.spring.data.jpa.web.SpecificationArgumentResolver;
-
 import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
 import org.springframework.context.annotation.ComponentScan;
 import org.springframework.context.annotation.Configuration;
 import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
 import org.springframework.web.method.support.HandlerMethodArgumentResolver;
-import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
+import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
+
+import net.kaczmarzyk.spring.data.jpa.web.SpecificationArgumentResolver;
 
 /**
  * @author Tomasz Kaczmarzyk
@@ -33,7 +33,7 @@ import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
 @ComponentScan(basePackages="net.kaczmarzyk")
 @EnableJpaRepositories
 @EnableAutoConfiguration
-public class Application extends WebMvcConfigurerAdapter {
+public class Application implements WebMvcConfigurer {
 
     @Override
     public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
