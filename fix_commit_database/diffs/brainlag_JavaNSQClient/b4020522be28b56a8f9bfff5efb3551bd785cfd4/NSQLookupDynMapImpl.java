@@ -63,7 +63,7 @@ public class NSQLookupDynMapImpl implements NSQLookup {
 	
 	public String getHTML(String url) {
 	  URL u;
-	  HttpURLConnection conn;
+	  HttpURLConnection conn = null;
 	  BufferedReader rd = null;
 	  String line;
 	  String result = "";
@@ -82,9 +82,14 @@ public class NSQLookupDynMapImpl implements NSQLookup {
 	    	  try {
 	    		  if (rd != null)
 	    			  rd.close();
-			} catch (Exception e) {
-				log.error("Caught", e);
-			}
+					} catch (Exception e) {
+						log.error("Caught", e);
+					}
+
+					// Release memory and underlying resources on the HttpURLConnection otherwise we may run out of file descriptors and leak memory
+					if (conn != null){
+						conn.disconnect();
+					}
 	      }
 	      return result;
 	   } 
