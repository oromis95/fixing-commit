@@ -10,18 +10,21 @@ public class ProducerConfig implements Serializable
 {
   private final ConnectionConfig connectionConfig;
   private final String exchangeName;
+  private final String routingKey;
   private final String contentType;
   private final String contentEncoding;
   private final boolean persistent;
 
   public ProducerConfig(ConnectionConfig connectionConfig,
                         String exchangeName,
+                        String routingKey,
                         String contentType,
                         String contentEncoding,
                         boolean persistent)
   {
     this.connectionConfig = connectionConfig;
     this.exchangeName = exchangeName;
+    this.routingKey = routingKey;
     this.contentType = contentType;
     this.contentEncoding = contentEncoding;
     this.persistent = persistent;
@@ -37,6 +40,11 @@ public class ProducerConfig implements Serializable
     return exchangeName;
   }
 
+  public String getRoutingKey()
+  {
+    return routingKey;
+  }
+
   public String getContentType()
   {
     return contentType;
@@ -56,6 +64,7 @@ public class ProducerConfig implements Serializable
     ConnectionConfig connectionConfig = ConnectionConfig.getFromStormConfig(stormConfig);
     return new ProducerConfig(connectionConfig,
                               getFromMap("rabbitmq.exchangeName", stormConfig),
+                              getFromMap("rabbitmq.routingKey", stormConfig),
                               getFromMap("rabbitmq.contentType", stormConfig),
                               getFromMap("rabbitmq.contentEncoding", stormConfig),
                               getFromMapAsBoolean("rabbitmq.persistent", stormConfig));
@@ -65,6 +74,7 @@ public class ProducerConfig implements Serializable
     Map<String, Object> map = new HashMap<String, Object>();
     map.putAll(connectionConfig.asMap());
     addToMap("rabbitmq.exchangeName", map, exchangeName);
+    addToMap("rabbitmq.routingKey", map, routingKey);
     addToMap("rabbitmq.contentType", map, contentType);
     addToMap("rabbitmq.contentEncoding", map, contentEncoding);
     addToMap("rabbitmq.persistent", map, persistent);
