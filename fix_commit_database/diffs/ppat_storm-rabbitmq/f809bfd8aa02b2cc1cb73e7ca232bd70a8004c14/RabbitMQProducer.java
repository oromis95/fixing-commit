@@ -1,26 +1,15 @@
 package io.latent.storm.rabbitmq;
 
+import backtype.storm.topology.ReportedFailedException;
+import com.rabbitmq.client.*;
 import io.latent.storm.rabbitmq.config.ConnectionConfig;
-import io.latent.storm.rabbitmq.config.ProducerConfig;
+import org.slf4j.Logger;
+import org.slf4j.LoggerFactory;
 
 import java.io.IOException;
 import java.io.Serializable;
 import java.util.Map;
 
-import org.slf4j.Logger;
-import org.slf4j.LoggerFactory;
-
-import backtype.storm.topology.ReportedFailedException;
-
-import com.rabbitmq.client.AMQP;
-import com.rabbitmq.client.AlreadyClosedException;
-import com.rabbitmq.client.BlockedListener;
-import com.rabbitmq.client.Channel;
-import com.rabbitmq.client.Connection;
-import com.rabbitmq.client.ConnectionFactory;
-import com.rabbitmq.client.ShutdownListener;
-import com.rabbitmq.client.ShutdownSignalException;
-
 public class RabbitMQProducer implements Serializable {
   private final Declarator declarator;
 
@@ -85,7 +74,7 @@ public class RabbitMQProducer implements Serializable {
 
   public void open(final Map config) {
     logger = LoggerFactory.getLogger(RabbitMQProducer.class);
-    connectionConfig = ProducerConfig.getFromStormConfig(config).getConnectionConfig();
+    connectionConfig = ConnectionConfig.getFromStormConfig(config);
     internalOpen();
   }
 
