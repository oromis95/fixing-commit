@@ -104,10 +104,10 @@ class GitHubOAuthService implements OAuthServiceProvider {
           email == null || email.isJsonNull() ? null : email.getAsString(),
           name == null || name.isJsonNull() ? null : name.getAsString(),
           null);
-    } else {
-        throw new IOException(String.format(
-            "Invalid JSON '%s': not a JSON Object", userJson));
     }
+
+    throw new IOException(String.format(
+        "Invalid JSON '%s': not a JSON Object", userJson));
   }
 
   @Override
