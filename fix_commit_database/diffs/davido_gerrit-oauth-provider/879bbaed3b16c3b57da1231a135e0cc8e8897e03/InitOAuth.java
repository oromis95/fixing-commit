@@ -25,10 +25,14 @@ class InitOAuth implements InitStep {
   static final String CLIENT_SECRET = "client-secret";
   static final String LINK_TO_EXISTING_OPENID_ACCOUNT =
       "link-to-existing-openid-accounts";
+  static final String FIX_LEGACY_USER_ID =
+      "fix-legacy-user-id";
   static final String DOMAIN = "domain";
   static final String USE_EMAIL_AS_USERNAME =
       "use-email-as-username";
   static final String ROOT_URL = "root-url";
+  static String FIX_LEGACY_USER_ID_QUESTION =
+      "Fix legacy user id, without oauth provider prefix?";
 
   private final ConsoleUI ui;
   private final Section googleOAuthProviderSection;
@@ -59,18 +63,24 @@ class InitOAuth implements InitStep {
         true, "Use Google OAuth provider for Gerrit login ?");
     if (configureGoogleOAuthProvider) {
       configureOAuth(googleOAuthProviderSection);
+      googleOAuthProviderSection.string(FIX_LEGACY_USER_ID_QUESTION,
+          FIX_LEGACY_USER_ID, "false");
     }
 
     boolean configueGitHubOAuthProvider = ui.yesno(
         true, "Use GitHub OAuth provider for Gerrit login ?");
     if (configueGitHubOAuthProvider) {
       configureOAuth(githubOAuthProviderSection);
+      githubOAuthProviderSection.string(FIX_LEGACY_USER_ID_QUESTION,
+          FIX_LEGACY_USER_ID, "false");
     }
 
     boolean configureBitbucketOAuthProvider = ui.yesno(
         true, "Use Bitbucket OAuth provider for Gerrit login ?");
     if (configureBitbucketOAuthProvider) {
       configureOAuth(bitbucketOAuthProviderSection);
+      bitbucketOAuthProviderSection.string(FIX_LEGACY_USER_ID_QUESTION,
+          FIX_LEGACY_USER_ID, "false");
     }
 
     boolean configureCasOAuthProvider = ui.yesno(
@@ -78,6 +88,8 @@ class InitOAuth implements InitStep {
     if (configureCasOAuthProvider) {
       casOAuthProviderSection.string("CAS Root URL", ROOT_URL, null);
       configureOAuth(casOAuthProviderSection);
+      casOAuthProviderSection.string(FIX_LEGACY_USER_ID_QUESTION,
+          FIX_LEGACY_USER_ID, "false");
     }
   }
 
