@@ -2,6 +2,8 @@ package com.yiistorm.helpers;
 
 import com.intellij.patterns.PlatformPatterns;
 import com.intellij.patterns.PsiElementPattern;
+import com.intellij.patterns.StandardPatterns;
+import com.intellij.patterns.StringPattern;
 import com.intellij.psi.PsiElement;
 import com.jetbrains.php.lang.PhpLanguage;
 import com.jetbrains.php.lang.parser.PhpElementTypes;
@@ -13,17 +15,17 @@ import com.jetbrains.php.lang.psi.elements.StringLiteralExpression;
  * Created by mazx on 05.01.14.
  */
 public class YiiContibutorHelper {
-    public static PsiElementPattern.Capture stringInMethod(String methodName, String className, int superLevels) {
+    public static PsiElementPattern.Capture stringInMethod(String methodName, StringPattern className) {
         return PlatformPatterns.psiElement(PsiElement.class)
-                .withParent(YiiContibutorHelper.methodLiteralExpression(methodName, className, superLevels))
+                .withParent(YiiContibutorHelper.methodLiteralExpression(methodName, className))
                 .withLanguage(PhpLanguage.INSTANCE);
     }
 
-    public static PsiElementPattern.Capture firstStringInMethod(String methodName, String className, int superLevels) {
+    public static PsiElementPattern.Capture firstStringInMethod(String methodName, StringPattern className) {
 
         return PlatformPatterns.psiElement(PsiElement.class)
                 .withParent(
-                        YiiContibutorHelper.methodLiteralExpression(methodName, className, superLevels)
+                        YiiContibutorHelper.methodLiteralExpression(methodName, className)
                                 .insideStarting(
                                         PlatformPatterns.psiElement().withElementType(PhpElementTypes.PARAMETER_LIST)
                                 )
@@ -32,15 +34,16 @@ public class YiiContibutorHelper {
 
     }
 
-    public static PhpElementPattern.Capture<StringLiteralExpression> methodLiteralExpression(String methodName, String className, int superLevels) {
+    public static PhpElementPattern.Capture<StringLiteralExpression> methodLiteralExpression(String methodName,
+                                                                                             StringPattern className) {
         return PhpPatterns.phpLiteralExpression()
                 .withParent(
-                        methodParamsList(methodName, className, superLevels)
+                        methodParamsList(methodName, className)
                 );
     }
 
-    public static PsiElementPattern.Capture<PsiElement> methodParamsList(String methodName, String className,
-                                                                         int superLevels) {
+    public static PsiElementPattern.Capture<PsiElement> methodParamsList(String methodName,
+                                                                         StringPattern className) {
         return PlatformPatterns.psiElement().withElementType(PhpElementTypes.PARAMETER_LIST)
                 .withParent(
                         PlatformPatterns.psiElement()
@@ -49,7 +52,7 @@ public class YiiContibutorHelper {
                                         PhpPatterns.psiElement().withElementType(
                                                 PhpElementTypes.CLASS_METHOD
                                         ).withName(methodName)
-                                                .withSuperParent(superLevels,
+                                                .withParent(
                                                         PhpPatterns.psiElement().withName(
                                                                 className
                                                         ))
@@ -68,11 +71,22 @@ public class YiiContibutorHelper {
                 );
     }
 
+    public static PsiElementPattern.Capture<PsiElement> arrayInMethodWithName(String methodName) {
+        return PlatformPatterns.psiElement().withElementType(PhpElementTypes.ARRAY_CREATION_EXPRESSION)
+                .withParent(
+                        PlatformPatterns.psiElement().withElementType(PhpElementTypes.METHOD_REFERENCE)
+                                .referencing(PhpPatterns.psiElement().withElementType(PhpElementTypes.CLASS_METHOD)
+                                        .withName(methodName)
+                                )
+                );
+    }
+
+
     public static PsiElementPattern.Capture firstStringInYiiMethod(String methodName) {
-        return firstStringInMethod(methodName, "Yii", 5);
+        return firstStringInMethod(methodName, StandardPatterns.string().oneOf("Yii", "YiiBase"));
     }
 
     public static PsiElementPattern.Capture stringInYiiMethod(String methodName) {
-        return stringInMethod(methodName, "Yii", 5);
+        return stringInMethod(methodName, StandardPatterns.string().oneOf("Yii", "YiiBase"));
     }
 }
