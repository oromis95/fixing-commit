@@ -1,10 +1,10 @@
 package com.yiistorm;
 
 import com.intellij.patterns.StandardPatterns;
-import com.intellij.psi.PsiElement;
 import com.intellij.psi.PsiReferenceContributor;
 import com.intellij.psi.PsiReferenceRegistrar;
 import com.jetbrains.php.lang.psi.elements.PhpPsiElement;
+import com.yiistorm.references.YiiPsiReferenceProvider;
 
 public class MyPsiReferenceContributor extends PsiReferenceContributor {
     @Override
