@@ -1,5 +1,7 @@
 package com.rits.tests.cloning.domain;
 
+import java.util.Objects;
+
 /**
  * @author kostantinos.kougios
  *
@@ -29,4 +31,18 @@ public class A
 	{
 		this.name = name;
 	}
+
+	@Override
+	public boolean equals(Object o) {
+		if (this == o) return true;
+		if (o == null || getClass() != o.getClass()) return false;
+		A a = (A) o;
+		return x == a.x &&
+				Objects.equals(name, a.name);
+	}
+
+	@Override
+	public int hashCode() {
+		return Objects.hash(x, name);
+	}
 }
