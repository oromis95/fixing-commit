@@ -32,7 +32,7 @@ public class TestCloner extends TestCase
 
 	@Target(TYPE)
 	@Retention(RUNTIME)
-	static private @interface MyImmutable
+	private @interface MyImmutable
 	{
 
 	}
@@ -42,6 +42,13 @@ public class TestCloner extends TestCase
 	{
 	}
 
+	public void testCalendarTimezone() {
+		TimeZone timeZone = TimeZone.getTimeZone("America/Los_Angeles");
+		Calendar c = Calendar.getInstance(timeZone);
+		Calendar cloned = cloner.deepClone(c);
+		assertEquals(timeZone, cloned.getTimeZone());
+	}
+
 	public void testCloneEnumInMapIssue20()
 	{
 		Map<Integer, TestEnum> m = new HashMap<Integer, TestEnum>();
