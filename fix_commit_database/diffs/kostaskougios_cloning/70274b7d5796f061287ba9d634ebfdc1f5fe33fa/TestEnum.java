@@ -2,14 +2,39 @@ package com.rits.tests.cloning;
 
 public enum TestEnum
 {
-	A("a"), B("b"), C("c");
+	A("a")
+			{
+				public String someMethod()
+				{
+					return "Some String from A";
+				}
+			},
 
-	private String name;
+
+	B("b")
+			{
+				public String someMethod()
+				{
+					return "Some String from B";
+				}
+			},
+
+	C("c")
+			{
+				public String someMethod()
+				{
+					return "Some String from C";
+				}
+			};
+
+	private final String name;
 
 	private TestEnum(String name)
 	{
 		this.name = name;
 	}
 
+	public abstract String someMethod();
+
 	public static Object o = new Object();
 }
\ No newline at end of file
