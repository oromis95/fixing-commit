@@ -13,17 +13,12 @@ public enum Role {
    * A regular user: can add users, change the topic, and remove himself from the group.
    */
   USER("user");
+  private final String roleString;
 
-  private Role(String roleString) {
+  Role(String roleString) {
     this.roleString = roleString;
   }
 
-  private String roleString;
-
-  String getRoleString() {
-    return roleString;
-  }
-
   static Role getRole(String roleString) {
     for (int i = 0; i < Role.values().length; i++) {
       if (Role.values()[i].roleString.equalsIgnoreCase(roleString)) {
@@ -33,4 +28,8 @@ public enum Role {
     throw new IllegalArgumentException("Unknown role string: " + roleString);
   }
 
+  String getRoleString() {
+    return roleString;
+  }
+
 }
