@@ -26,6 +26,11 @@ public class DocumentAdaptor extends ElementAdaptor implements Document {
         return null;
     }
 
+    @Override
+    public short getNodeType() {
+        return DOCUMENT_NODE;
+    }
+
     @Override
     public Element getDocumentElement() {
         return this;
@@ -33,7 +38,7 @@ public class DocumentAdaptor extends ElementAdaptor implements Document {
 
     @Override
     public Element getElementById(String elementId) {
-        return NodeAdaptorFactory.getElement(document.getElementById(elementId));
+        return NodeAdaptors.getElement(document.getElementById(elementId));
     }
 
     @Override
