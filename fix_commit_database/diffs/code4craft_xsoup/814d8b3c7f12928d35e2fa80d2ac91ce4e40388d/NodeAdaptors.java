@@ -11,7 +11,7 @@ import java.util.List;
 /**
  * @author code4crafer@gmail.com
  */
-public class NodeAdaptorFactory {
+public class NodeAdaptors {
 
     public static Node getNode(org.jsoup.nodes.Node node) {
         if (node == null) {
