@@ -4,6 +4,7 @@ import org.jsoup.nodes.Element;
 import org.jsoup.select.Evaluator;
 
 /**
+ * Evaluators in Xsoup.
  * @author code4crafter@gmail.com
  */
 public abstract class XEvaluators {
