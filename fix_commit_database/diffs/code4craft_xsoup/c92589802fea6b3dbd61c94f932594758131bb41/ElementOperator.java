@@ -10,6 +10,8 @@ import java.util.regex.Matcher;
 import java.util.regex.Pattern;
 
 /**
+ * Operate on element to get XPath result.
+ *
  * @author code4crafter@gmail.com
  */
 public abstract class ElementOperator {
