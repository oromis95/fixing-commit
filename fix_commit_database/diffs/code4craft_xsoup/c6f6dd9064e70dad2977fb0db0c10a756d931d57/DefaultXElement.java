@@ -1,7 +1,7 @@
-package us.codecraft.xsoup.evalutor;
+package us.codecraft.xsoup.nodes;
 
 import org.jsoup.nodes.Element;
-import us.codecraft.xsoup.nodes.ElementOperator;
+import us.codecraft.xsoup.evaluator.ElementOperator;
 
 /**
  * XPath result.
