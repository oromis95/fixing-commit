@@ -91,6 +91,7 @@ import tv.porst.swfretools.parser.actions.as2.ActionToggleQuality;
 import tv.porst.swfretools.parser.actions.as2.ActionTrace;
 import tv.porst.swfretools.parser.actions.as2.ActionTry;
 import tv.porst.swfretools.parser.actions.as2.ActionTypeOf;
+import tv.porst.swfretools.parser.actions.as2.ActionUnknown;
 import tv.porst.swfretools.parser.actions.as2.ActionWaitForFrame;
 import tv.porst.swfretools.parser.actions.as2.ActionWaitForFrame2;
 import tv.porst.swfretools.parser.actions.as2.ActionWith;
@@ -620,6 +621,11 @@ public final class AS2CodePrinter {
 				add(sb, "TypeOf");
 			}
 
+			@Override
+			protected void visit(final ActionUnknown instruction) {
+				add(sb, String.format("Unknown %02X", instruction.getActionCode().value()));
+			}
+
 			@Override
 			protected void visit(final ActionWaitForFrame instruction) {
 				add(sb, "WaitForFrame", instruction.getFrame().value());
