@@ -7,119 +7,32 @@ import tv.porst.splib.binaryparser.UINT16;
 import tv.porst.splib.binaryparser.UINT32;
 import tv.porst.splib.binaryparser.UINT8;
 import tv.porst.swfretools.parser.actions.as2.Action;
-import tv.porst.swfretools.parser.structures.ActionList;
-import tv.porst.swfretools.parser.structures.BevelFilter;
-import tv.porst.swfretools.parser.structures.BlurFilter;
-import tv.porst.swfretools.parser.structures.ButtonCondAction;
-import tv.porst.swfretools.parser.structures.ButtonCondActionList;
-import tv.porst.swfretools.parser.structures.ButtonRecord2;
-import tv.porst.swfretools.parser.structures.ButtonRecord2List;
-import tv.porst.swfretools.parser.structures.ClipActionRecord;
-import tv.porst.swfretools.parser.structures.ClipActionRecordList;
-import tv.porst.swfretools.parser.structures.ClipActions;
-import tv.porst.swfretools.parser.structures.ClipEventFlags;
-import tv.porst.swfretools.parser.structures.ColorMatrixFilter;
-import tv.porst.swfretools.parser.structures.ConvolutionFilter;
-import tv.porst.swfretools.parser.structures.CurvedEdgeRecord;
-import tv.porst.swfretools.parser.structures.CxFormWithAlpha;
-import tv.porst.swfretools.parser.structures.DropShadowFilter;
-import tv.porst.swfretools.parser.structures.EndShapeRecord;
-import tv.porst.swfretools.parser.structures.FillStyle;
-import tv.porst.swfretools.parser.structures.FillStyle3;
-import tv.porst.swfretools.parser.structures.FillStyle3Array;
-import tv.porst.swfretools.parser.structures.FillStyle3List;
-import tv.porst.swfretools.parser.structures.FillStyleArray;
-import tv.porst.swfretools.parser.structures.FillStyleList;
-import tv.porst.swfretools.parser.structures.Filter;
-import tv.porst.swfretools.parser.structures.FilterList;
-import tv.porst.swfretools.parser.structures.FocalGradient;
-import tv.porst.swfretools.parser.structures.GlowFilter;
-import tv.porst.swfretools.parser.structures.GlyphEntry;
-import tv.porst.swfretools.parser.structures.GlyphEntryList;
-import tv.porst.swfretools.parser.structures.GradRecord;
-import tv.porst.swfretools.parser.structures.GradRecord3;
-import tv.porst.swfretools.parser.structures.GradRecord3List;
-import tv.porst.swfretools.parser.structures.GradRecordList;
-import tv.porst.swfretools.parser.structures.Gradient;
-import tv.porst.swfretools.parser.structures.Gradient3;
-import tv.porst.swfretools.parser.structures.GradientBevelFilter;
-import tv.porst.swfretools.parser.structures.GradientGlowFilter;
-import tv.porst.swfretools.parser.structures.IGradient;
-import tv.porst.swfretools.parser.structures.INT16List;
-import tv.porst.swfretools.parser.structures.KerningRecord;
-import tv.porst.swfretools.parser.structures.KerningRecordList;
-import tv.porst.swfretools.parser.structures.LineStyle;
-import tv.porst.swfretools.parser.structures.LineStyle3;
-import tv.porst.swfretools.parser.structures.LineStyle3Array;
-import tv.porst.swfretools.parser.structures.LineStyle3List;
-import tv.porst.swfretools.parser.structures.LineStyle4;
-import tv.porst.swfretools.parser.structures.LineStyle4Array;
-import tv.porst.swfretools.parser.structures.LineStyle4List;
-import tv.porst.swfretools.parser.structures.LineStyleArray;
-import tv.porst.swfretools.parser.structures.LineStyleList;
-import tv.porst.swfretools.parser.structures.Matrix;
-import tv.porst.swfretools.parser.structures.MorphFillStyle;
-import tv.porst.swfretools.parser.structures.MorphFillStyleArray;
-import tv.porst.swfretools.parser.structures.MorphFillStyleList;
-import tv.porst.swfretools.parser.structures.MorphGradient;
-import tv.porst.swfretools.parser.structures.MorphGradientRecord;
-import tv.porst.swfretools.parser.structures.MorphGradientRecordList;
-import tv.porst.swfretools.parser.structures.MorphLineStyle;
-import tv.porst.swfretools.parser.structures.MorphLineStyleArray;
-import tv.porst.swfretools.parser.structures.MorphLineStyleList;
-import tv.porst.swfretools.parser.structures.ParsedINTElementList;
-import tv.porst.swfretools.parser.structures.RGB;
-import tv.porst.swfretools.parser.structures.RGBA;
-import tv.porst.swfretools.parser.structures.Rect;
-import tv.porst.swfretools.parser.structures.RectList;
-import tv.porst.swfretools.parser.structures.SWFFile;
-import tv.porst.swfretools.parser.structures.Shape;
-import tv.porst.swfretools.parser.structures.Shape3;
-import tv.porst.swfretools.parser.structures.Shape3List;
-import tv.porst.swfretools.parser.structures.Shape3Record;
-import tv.porst.swfretools.parser.structures.Shape3RecordList;
-import tv.porst.swfretools.parser.structures.Shape4Record;
-import tv.porst.swfretools.parser.structures.ShapeList;
-import tv.porst.swfretools.parser.structures.ShapeRecord;
-import tv.porst.swfretools.parser.structures.ShapeRecordList;
-import tv.porst.swfretools.parser.structures.ShapeWithStyle;
-import tv.porst.swfretools.parser.structures.ShapeWithStyle3;
-import tv.porst.swfretools.parser.structures.ShapeWithStyle4;
-import tv.porst.swfretools.parser.structures.SingleFilterList;
-import tv.porst.swfretools.parser.structures.SoundEnvelope;
-import tv.porst.swfretools.parser.structures.SoundEnvelopeList;
-import tv.porst.swfretools.parser.structures.SoundInfo;
-import tv.porst.swfretools.parser.structures.StraightEdgeRecord;
-import tv.porst.swfretools.parser.structures.StyleChangeRecord;
-import tv.porst.swfretools.parser.structures.StyleChangeRecord3;
-import tv.porst.swfretools.parser.structures.StyleChangeRecord4;
-import tv.porst.swfretools.parser.structures.Symbol;
-import tv.porst.swfretools.parser.structures.TagList;
-import tv.porst.swfretools.parser.structures.TextRecord;
-import tv.porst.swfretools.parser.structures.TextRecord2;
-import tv.porst.swfretools.parser.structures.TextRecord2List;
-import tv.porst.swfretools.parser.structures.TextRecordList;
-import tv.porst.swfretools.parser.structures.UINT16List;
-import tv.porst.swfretools.parser.structures.ZoneData;
-import tv.porst.swfretools.parser.structures.ZoneDataList;
-import tv.porst.swfretools.parser.structures.ZoneRecord;
-import tv.porst.swfretools.parser.structures.ZoneRecordList;
+import tv.porst.swfretools.parser.structures.*;
 import tv.porst.swfretools.parser.tags.CSMTextSettingsTag;
+import tv.porst.swfretools.parser.tags.DefineBinaryDataTag;
 import tv.porst.swfretools.parser.tags.DefineBitsJPEG2Tag;
 import tv.porst.swfretools.parser.tags.DefineBitsJPEG3Tag;
+import tv.porst.swfretools.parser.tags.DefineBitsJPEG4Tag;
 import tv.porst.swfretools.parser.tags.DefineBitsLossless2Tag;
 import tv.porst.swfretools.parser.tags.DefineBitsLosslessTag;
 import tv.porst.swfretools.parser.tags.DefineBitsTag;
 import tv.porst.swfretools.parser.tags.DefineButton2Tag;
+import tv.porst.swfretools.parser.tags.DefineButtonCxformTag;
+import tv.porst.swfretools.parser.tags.DefineButtonSoundTag;
+import tv.porst.swfretools.parser.tags.DefineButtonTag;
 import tv.porst.swfretools.parser.tags.DefineEditTextTag;
 import tv.porst.swfretools.parser.tags.DefineFont2Tag;
 import tv.porst.swfretools.parser.tags.DefineFont3Tag;
+import tv.porst.swfretools.parser.tags.DefineFont4Tag;
 import tv.porst.swfretools.parser.tags.DefineFontAlignZonesTag;
 import tv.porst.swfretools.parser.tags.DefineFontInfo2Tag;
 import tv.porst.swfretools.parser.tags.DefineFontInfoTag;
 import tv.porst.swfretools.parser.tags.DefineFontNameTag;
 import tv.porst.swfretools.parser.tags.DefineFontTag;
+import tv.porst.swfretools.parser.tags.DefineMorphShape2Tag;
 import tv.porst.swfretools.parser.tags.DefineMorphShapeTag;
+import tv.porst.swfretools.parser.tags.DefineScalingGridTag;
+import tv.porst.swfretools.parser.tags.DefineSceneAndFrameLabelDataTag;
 import tv.porst.swfretools.parser.tags.DefineShape2Tag;
 import tv.porst.swfretools.parser.tags.DefineShape3Tag;
 import tv.porst.swfretools.parser.tags.DefineShape4Tag;
@@ -129,25 +42,36 @@ import tv.porst.swfretools.parser.tags.DefineSpriteTag;
 import tv.porst.swfretools.parser.tags.DefineText2Tag;
 import tv.porst.swfretools.parser.tags.DefineTextTag;
 import tv.porst.swfretools.parser.tags.DefineVideoStreamTag;
+import tv.porst.swfretools.parser.tags.DoABCTag;
 import tv.porst.swfretools.parser.tags.DoActionTag;
 import tv.porst.swfretools.parser.tags.DoInitActionTag;
+import tv.porst.swfretools.parser.tags.EnableDebugger2Tag;
+import tv.porst.swfretools.parser.tags.EnableDebuggerTag;
 import tv.porst.swfretools.parser.tags.EndTag;
 import tv.porst.swfretools.parser.tags.ExportAssetsTag;
 import tv.porst.swfretools.parser.tags.FileAttributesTag;
 import tv.porst.swfretools.parser.tags.FrameLabelTag;
+import tv.porst.swfretools.parser.tags.ImportAssets2Tag;
+import tv.porst.swfretools.parser.tags.ImportAssetsTag;
 import tv.porst.swfretools.parser.tags.JPEGTablesTag;
 import tv.porst.swfretools.parser.tags.MetadataTag;
 import tv.porst.swfretools.parser.tags.PlaceObject2Tag;
 import tv.porst.swfretools.parser.tags.PlaceObject3Tag;
 import tv.porst.swfretools.parser.tags.ProtectTag;
 import tv.porst.swfretools.parser.tags.RemoveObject2Tag;
+import tv.porst.swfretools.parser.tags.RemoveObjectTag;
+import tv.porst.swfretools.parser.tags.ScriptLimitsTag;
 import tv.porst.swfretools.parser.tags.SetBackgroundColorTag;
+import tv.porst.swfretools.parser.tags.SetTabIndexTag;
 import tv.porst.swfretools.parser.tags.ShowFrameTag;
 import tv.porst.swfretools.parser.tags.SoundStreamBlockTag;
 import tv.porst.swfretools.parser.tags.SoundStreamHead2Tag;
 import tv.porst.swfretools.parser.tags.SoundStreamHeadTag;
+import tv.porst.swfretools.parser.tags.StartSound2Tag;
 import tv.porst.swfretools.parser.tags.StartSoundTag;
+import tv.porst.swfretools.parser.tags.SymbolClassTag;
 import tv.porst.swfretools.parser.tags.Tag;
+import tv.porst.swfretools.parser.tags.VideoFrameTag;
 
 /**
  * Class that iterates over all elements of a SWF file.
@@ -162,6 +86,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final ActionList actions, final ISWFVisitor visitor) {
 
+		if (actions == null) {
+			return;
+		}
+
 		for (final Action action : actions) {
 			visitor.visit(actions, "Action", action);
 		}
@@ -174,6 +102,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final BevelFilter filter, final ISWFVisitor visitor) {
+
+		if (filter == null) {
+			return;
+		}
+
 		visitor.visit(filter, "ShadowColor", filter.getShadowColor());
 		visitor.visit(filter, "HightlightColor", filter.getHighlightColor());
 		visitor.visit(filter, "BlurX", filter.getBlurX());
@@ -195,6 +128,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final BlurFilter filter, final ISWFVisitor visitor) {
+
+		if (filter == null) {
+			return;
+		}
+
 		visitor.visit(filter, "BlurX", filter.getBlurX());
 		visitor.visit(filter, "BlurY", filter.getBlurY());
 		visitor.visit(filter, "Passes", filter.getPasses());
@@ -208,6 +146,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final ButtonCondAction action, final ISWFVisitor visitor) {
+
+		if (action == null) {
+			return;
+		}
+
 		visitor.visit(action, "CondActionSize", action.getCondActionSize());
 		visitor.visit(action, "CondIdleToOverDown", action.getCondIdleToOverDown());
 		visitor.visit(action, "CondOutDownToIdle", action.getCondOutDownToIdle());
@@ -231,12 +174,42 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final ButtonCondActionList actions, final ISWFVisitor visitor) {
+
+		if (actions == null) {
+			return;
+		}
+
 		for (final ButtonCondAction action : actions) {
 			visitor.visit(actions, "Action", action);
 			visit(action, visitor);
 		}
 	}
 
+	/**
+	 * Visits a ButtonRecord object.
+	 * 
+	 * @param record The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final ButtonRecord record, final ISWFVisitor visitor) {
+
+		if (record == null) {
+			return;
+		}
+
+		visitor.visit(record, "ButtonReserved", record.getButtonReserved());
+		visitor.visit(record, "ButtonHasBlendMode", record.getButtonHasBlendMode());
+		visitor.visit(record, "ButtonHasFilterList", record.getButtonHasFilterList());
+		visitor.visit(record, "ButtonStateHitTest", record.getButtonStateHitTest());
+		visitor.visit(record, "ButtonStateDown", record.getButtonStateDown());
+		visitor.visit(record, "ButtonStateOver", record.getButtonStateOver());
+		visitor.visit(record, "ButtonStateUp", record.getButtonStateUp());
+		visitor.visit(record, "CharacterID", record.getCharacterId());
+		visitor.visit(record, "PlaceDepth", record.getPlaceDepth());
+		visitor.visit(record, "PlaceMatrix", record.getPlaceMatrix());
+		visit(record.getPlaceMatrix(), visitor);
+	}
+
 	/**
 	 * Visits a ButtonRecord2 object.
 	 * 
@@ -244,6 +217,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final ButtonRecord2 record, final ISWFVisitor visitor) {
+
+		if (record == null) {
+			return;
+		}
+
 		visitor.visit(record, "ButtonReserved", record.getButtonReserved());
 		visitor.visit(record, "ButtonHasBlendMode", record.getButtonHasBlendMode());
 		visitor.visit(record, "ButtonHasFilterList", record.getButtonHasFilterList());
@@ -269,12 +247,35 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final ButtonRecord2List list, final ISWFVisitor visitor) {
+
+		if (list == null) {
+			return;
+		}
+
 		for (final ButtonRecord2 character : list) {
 			visitor.visit(list, "Character", character);
 			visit(character, visitor);
 		}
 	}
 
+	/**
+	 * Visits a ButtonRecordList object.
+	 * 
+	 * @param list The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final ButtonRecordList list, final ISWFVisitor visitor) {
+
+		if (list == null) {
+			return;
+		}
+
+		for (final ButtonRecord character : list) {
+			visitor.visit(list, "Character", character);
+			visit(character, visitor);
+		}
+	}
+
 	/**
 	 * Visits a ClipActionRecord object.
 	 * 
@@ -283,6 +284,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final ClipActionRecord clipActionRecord, final ISWFVisitor visitor) {
 
+		if (clipActionRecord == null) {
+			return;
+		}
+
 		visitor.visit(clipActionRecord, "EventFlags", clipActionRecord.getEventFlags());
 		visit(clipActionRecord.getEventFlags(), visitor);
 		visitor.visit(clipActionRecord, "ActionRecordSize", clipActionRecord.getActionRecordSize());
@@ -299,6 +304,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final ClipActionRecordList clipActionRecords, final ISWFVisitor visitor) {
 
+		if (clipActionRecords == null) {
+			return;
+		}
+
 		for (final ClipActionRecord clipActionRecord : clipActionRecords) {
 			visitor.visit(clipActionRecords, "ClipActionRecord", clipActionRecord);
 			visit(clipActionRecord, visitor);
@@ -313,22 +322,23 @@ public final class SWFIterator {
 	 */
 	public static void visit(final ClipActions clipActions, final ISWFVisitor visitor) {
 
-		if (clipActions != null) {
+		if (clipActions == null) {
+			return;
+		}
 
-			visitor.visit(clipActions, "Reserved", clipActions.getReserved());
-			visitor.visit(clipActions, "AllEventFlags", clipActions.getAllEventFlags());
-			visit(clipActions.getAllEventFlags(), visitor);
-			visitor.visit(clipActions, "ClipActionRecords", clipActions.getClipActionRecords());
-			visit(clipActions.getClipActionRecords(), visitor);
+		visitor.visit(clipActions, "Reserved", clipActions.getReserved());
+		visitor.visit(clipActions, "AllEventFlags", clipActions.getAllEventFlags());
+		visit(clipActions.getAllEventFlags(), visitor);
+		visitor.visit(clipActions, "ClipActionRecords", clipActions.getClipActionRecords());
+		visit(clipActions.getClipActionRecords(), visitor);
 
-			final IParsedINTElement endFlag = clipActions.getClipActionEndFlag();
+		final IParsedINTElement endFlag = clipActions.getClipActionEndFlag();
 
-			if (endFlag instanceof UINT16) {
-				visitor.visit(clipActions, "ClipActionEndFlag", (UINT16) clipActions.getClipActionEndFlag());
-			}
-			else  {
-				visitor.visit(clipActions, "ClipActionEndFlag", (UINT32) clipActions.getClipActionEndFlag());
-			}
+		if (endFlag instanceof UINT16) {
+			visitor.visit(clipActions, "ClipActionEndFlag", (UINT16) clipActions.getClipActionEndFlag());
+		}
+		else  {
+			visitor.visit(clipActions, "ClipActionEndFlag", (UINT32) clipActions.getClipActionEndFlag());
 		}
 	}
 
@@ -340,6 +350,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final ClipEventFlags clipEventFlags, final ISWFVisitor visitor) {
 
+		if (clipEventFlags == null) {
+			return;
+		}
+
 		visitor.visit(clipEventFlags, "ClipEventKeyUp", clipEventFlags.getClipEventKeyUp());
 		visitor.visit(clipEventFlags, "ClipEventKeyDown", clipEventFlags.getClipEventKeyDown());
 		visitor.visit(clipEventFlags, "ClipEventMouseUp", clipEventFlags.getClipEventMouseUp());
@@ -370,7 +384,12 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final ColorMatrixFilter filter, final ISWFVisitor visitor) {
-		// TODO
+
+		if (filter == null) {
+			return;
+		}
+
+		visitor.visit(filter, "Matrix", filter.getMatrix());
 	}
 
 	/**
@@ -380,12 +399,16 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final ConvolutionFilter filter, final ISWFVisitor visitor) {
+
+		if (filter == null) {
+			return;
+		}
+
 		visitor.visit(filter, "MatrixX", filter.getMatrixX());
 		visitor.visit(filter, "MatrixY", filter.getMatrixY());
 		visitor.visit(filter, "Divisor", filter.getDivisor());
 		visitor.visit(filter, "Bias", filter.getBias());
-		// TODO
-		// visitor.visit(filter, "Matrix", filter.getMatrix());
+		visitor.visit(filter, "Matrix", filter.getMatrix());
 		visitor.visit(filter, "DefaultColor", filter.getDefaultColor());
 		visit(filter.getDefaultColor(), visitor);
 		visitor.visit(filter, "Reserved", filter.getReserved());
@@ -401,6 +424,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final CSMTextSettingsTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "TextID", tag.getTextId());
 		visitor.visit(tag, "UseFlashType", tag.getUseFlashType());
 		visitor.visit(tag, "GridFit", tag.getGridFit());
@@ -417,6 +444,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final CurvedEdgeRecord curvedEdgeRecord, final ISWFVisitor visitor) {
+
+		if (curvedEdgeRecord == null) {
+			return;
+		}
+
 		visitor.visit(curvedEdgeRecord, "TypeFlag", curvedEdgeRecord.getTypeFlag());
 		visitor.visit(curvedEdgeRecord, "StraightFlag", curvedEdgeRecord.getStraightFlag());
 		visitor.visit(curvedEdgeRecord, "NumBits", curvedEdgeRecord.getNumBits());
@@ -426,6 +458,29 @@ public final class SWFIterator {
 		visitor.visit(curvedEdgeRecord, "AnchorDeltaY", curvedEdgeRecord.getAnchorDeltaY());
 	}
 
+	/**
+	 * Visits a CxForm object.
+	 * 
+	 * @param cxForm The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final CxForm cxForm, final ISWFVisitor visitor) {
+
+		if (cxForm == null) {
+			return;
+		}
+
+		visitor.visit(cxForm, "HasAddTerms", cxForm.getHasAddTerms());
+		visitor.visit(cxForm, "HasMultTerms", cxForm.getHasMultTerms());
+		visitor.visit(cxForm, "NBits", cxForm.getnBits());
+		visitor.visit(cxForm, "RedMultTerm", cxForm.getRedMultTerm());
+		visitor.visit(cxForm, "GreenMultTerm", cxForm.getGreenMultTerm());
+		visitor.visit(cxForm, "BlueMultTerm", cxForm.getBlueMultTerm());
+		visitor.visit(cxForm, "RedAddTerm", cxForm.getRedAddTerm());
+		visitor.visit(cxForm, "GreenAddTerm", cxForm.getGreenAddTerm());
+		visitor.visit(cxForm, "BlueAddTerm", cxForm.getBlueAddTerm());
+	}
+
 	/**
 	 * Visits a CxFormWithAlpha object.
 	 * 
@@ -434,20 +489,38 @@ public final class SWFIterator {
 	 */
 	public static void visit(final CxFormWithAlpha cxFormWithAlpha, final ISWFVisitor visitor) {
 
-		if (cxFormWithAlpha != null) {
+		if (cxFormWithAlpha == null) {
+			return;
+		}
+
+		visitor.visit(cxFormWithAlpha, "HasAddTerms", cxFormWithAlpha.getHasAddTerms());
+		visitor.visit(cxFormWithAlpha, "HasMultTerms", cxFormWithAlpha.getHasMultTerms());
+		visitor.visit(cxFormWithAlpha, "NBits", cxFormWithAlpha.getnBits());
+		visitor.visit(cxFormWithAlpha, "RedMultTerm", cxFormWithAlpha.getRedMultTerm());
+		visitor.visit(cxFormWithAlpha, "GreenMultTerm", cxFormWithAlpha.getGreenMultTerm());
+		visitor.visit(cxFormWithAlpha, "BlueMultTerm", cxFormWithAlpha.getBlueMultTerm());
+		visitor.visit(cxFormWithAlpha, "AlphaMultTerm", cxFormWithAlpha.getAlphaMultTerm());
+		visitor.visit(cxFormWithAlpha, "RedAddTerm", cxFormWithAlpha.getRedAddTerm());
+		visitor.visit(cxFormWithAlpha, "GreenAddTerm", cxFormWithAlpha.getGreenAddTerm());
+		visitor.visit(cxFormWithAlpha, "BlueAddTerm", cxFormWithAlpha.getBlueAddTerm());
+		visitor.visit(cxFormWithAlpha, "AlphaAddTerm", cxFormWithAlpha.getAlphaAddTerm());
+	}
+
+	/**
+	 * Visits a DefineBinaryDataTag object.
+	 * 
+	 * @param tag The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final DefineBinaryDataTag tag, final ISWFVisitor visitor) {
 
-			visitor.visit(cxFormWithAlpha, "HasAddTerms", cxFormWithAlpha.getHasAddTerms());
-			visitor.visit(cxFormWithAlpha, "HasMultTerms", cxFormWithAlpha.getHasMultTerms());
-			visitor.visit(cxFormWithAlpha, "NBits", cxFormWithAlpha.getnBits());
-			visitor.visit(cxFormWithAlpha, "RedMultTerm", cxFormWithAlpha.getRedMultTerm());
-			visitor.visit(cxFormWithAlpha, "GreenMultTerm", cxFormWithAlpha.getGreenMultTerm());
-			visitor.visit(cxFormWithAlpha, "BlueMultTerm", cxFormWithAlpha.getBlueMultTerm());
-			visitor.visit(cxFormWithAlpha, "AlphaMultTerm", cxFormWithAlpha.getAlphaMultTerm());
-			visitor.visit(cxFormWithAlpha, "RedAddTerm", cxFormWithAlpha.getRedAddTerm());
-			visitor.visit(cxFormWithAlpha, "GreenAddTerm", cxFormWithAlpha.getGreenAddTerm());
-			visitor.visit(cxFormWithAlpha, "BlueAddTerm", cxFormWithAlpha.getBlueAddTerm());
-			visitor.visit(cxFormWithAlpha, "AlphaAddTerm", cxFormWithAlpha.getAlphaAddTerm());
+		if (tag == null) {
+			return;
 		}
+
+		visitor.visit(tag, "Tag", tag.getTag());
+		visitor.visit(tag, "Reserved", tag.getReserved());
+		visitor.visit(tag, "Data", tag.getData());
 	}
 
 	/**
@@ -458,6 +531,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineBitsJPEG2Tag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "CharacterID", tag.getCharacterId());
 		visitor.visit(tag, "ImageData", tag.getImageData());
 	}
@@ -470,12 +547,35 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineBitsJPEG3Tag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "CharacterID", tag.getCharacterId());
 		visitor.visit(tag, "AlphaDataOffset", tag.getAlphaDataOffset());
 		visitor.visit(tag, "JPEGData", tag.getJpegData());
 		visitor.visit(tag, "BitmapAlphaData", tag.getBitmapAlphaData());
 	}
 
+	/**
+	 * Visits a DefineBitsJPEG4Tag object.
+	 * 
+	 * @param tag The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final DefineBitsJPEG4Tag tag, final ISWFVisitor visitor) {
+
+		if (tag == null) {
+			return;
+		}
+
+		visitor.visit(tag, "CharacterID", tag.getCharacterId());
+		visitor.visit(tag, "AlphaDataOffset", tag.getAlphaDataOffset());
+		visitor.visit(tag, "CharacterID", tag.getDeblockParam());
+		visitor.visit(tag, "ImageData", tag.getImageData());
+		visitor.visit(tag, "BitmapAlphaData", tag.getBitmapAlphaData());
+	}
+
 	/**
 	 * Visits a DefineBitsLossless2Tag object.
 	 * 
@@ -484,6 +584,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineBitsLossless2Tag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "CharacterID", tag.getCharacterId());
 		visitor.visit(tag, "BitmapFormat", tag.getBitmapFormat());
 		visitor.visit(tag, "BitmapWidth", tag.getBitmapWidth());
@@ -500,6 +604,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineBitsLosslessTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "CharacterID", tag.getCharacterId());
 		visitor.visit(tag, "BitmapFormat", tag.getBitmapFormat());
 		visitor.visit(tag, "BitmapWidth", tag.getBitmapWidth());
@@ -516,6 +624,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineBitsTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "CharacterID", tag.getCharacterId());
 		visitor.visit(tag, "JPEGData", tag.getJpegData());
 	}
@@ -528,6 +640,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineButton2Tag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "ButtonID", tag.getButtonId());
 		visitor.visit(tag, "ReservedFlags", tag.getReservedFlags());
 		visitor.visit(tag, "TrackAsMenu", tag.getTrackAsMenu());
@@ -539,6 +655,67 @@ public final class SWFIterator {
 		visit(tag.getActions(), visitor);
 	}
 
+	/**
+	 * Visits a DefineButtonCxformTag object.
+	 * 
+	 * @param tag The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final DefineButtonCxformTag tag, final ISWFVisitor visitor) {
+
+		if (tag == null) {
+			return;
+		}
+
+		visitor.visit(tag, "ButtonID", tag.getButtonId());
+		visitor.visit(tag, "ButtonColorTransform", tag.getButtonColorTransform());
+		visit(tag.getButtonColorTransform(), visitor);
+	}
+
+	/**
+	 * Visits a DefineButtonSoundTag object.
+	 * 
+	 * @param tag The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final DefineButtonSoundTag tag, final ISWFVisitor visitor) {
+
+		if (tag == null) {
+			return;
+		}
+
+		visitor.visit(tag, "ButtonID", tag.getButtonId());
+		visitor.visit(tag, "ButtonSoundChar0", tag.getButtonSoundChar0());
+		visitor.visit(tag, "ButtonSoundInfo0", tag.getButtonSoundInfo0());
+		visitor.visit(tag, "ButtonSoundChar1", tag.getButtonSoundChar1());
+		visitor.visit(tag, "ButtonSoundInfo1", tag.getButtonSoundInfo1());
+		visitor.visit(tag, "ButtonSoundChar2", tag.getButtonSoundChar2());
+		visitor.visit(tag, "ButtonSoundInfo2", tag.getButtonSoundInfo2());
+		visitor.visit(tag, "ButtonSoundChar3", tag.getButtonSoundChar3());
+		visitor.visit(tag, "ButtonSoundInfo3", tag.getButtonSoundInfo3());
+	}
+
+	/**
+	 * Visits a DefineButtonTag object.
+	 * 
+	 * @param tag The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final DefineButtonTag tag, final ISWFVisitor visitor) {
+
+		if (tag == null) {
+			return;
+		}
+
+		visitor.visit(tag, "ButtonID", tag.getButtonId());
+		visitor.visit(tag, "Characters", tag.getCharacters());
+		visit(tag.getCharacters(), visitor);
+		visitor.visit(tag, "CharacterEndFlag", tag.getCharacterEndFlag());
+		visitor.visit(tag, "Actions", tag.getActions());
+		visit(tag.getActions(), visitor);
+		visitor.visit(tag, "ActionEndFlag", tag.getActionEndFlag());
+	}
+
 	/**
 	 * Visits a DefineEditTextTag object.
 	 * 
@@ -547,6 +724,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineEditTextTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "CharacterID", tag.getCharacterId());
 		visitor.visit(tag, "Bounds", tag.getBounds());
 		visit(tag.getBounds(), visitor);
@@ -589,6 +770,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineFont2Tag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "FontID", tag.getFontId());
 		visitor.visit(tag, "FontFlagsHasLayout", tag.getFontFlagsHasLayout());
 		visitor.visit(tag, "FontFlagsShiftJIS", tag.getFontFlagsShiftJIS());
@@ -637,6 +822,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineFont3Tag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "FontID", tag.getFontId());
 		visitor.visit(tag, "FontFlagsHasLayout", tag.getFontFlagsHasLayout());
 		visitor.visit(tag, "FontFlagsShiftJIS", tag.getFontFlagsShiftJIS());
@@ -677,6 +866,27 @@ public final class SWFIterator {
 		visit(tag.getFontKerningTable(), visitor);
 	}
 
+	/**
+	 * Visits a DefineFont4Tag object.
+	 * 
+	 * @param tag The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final DefineFont4Tag tag, final ISWFVisitor visitor) {
+
+		if (tag == null) {
+			return;
+		}
+
+		visitor.visit(tag, "FontID", tag.getFontId());
+		visitor.visit(tag, "FontFlagsReserved", tag.getFontFlagsReserved());
+		visitor.visit(tag, "FontFlagsHasFontData", tag.getFontFlagsHasFontData());
+		visitor.visit(tag, "FontFlagsItalic", tag.getFontFlagsItalic());
+		visitor.visit(tag, "FontFlagsBold", tag.getFontFlagsBold());
+		visitor.visit(tag, "FontName", tag.getFontName());
+		visitor.visit(tag, "FontData", tag.getFontData());
+	}
+
 	/**
 	 * Visits a DefineFontAlignZonesTag object.
 	 * 
@@ -685,6 +895,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineFontAlignZonesTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "FontID", tag.getFontId());
 		visitor.visit(tag, "CSMTableHint", tag.getCsmTableHint());
 		visitor.visit(tag, "Reserved", tag.getReserved());
@@ -700,6 +914,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineFontInfo2Tag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "FontID", tag.getFontId());
 		visitor.visit(tag, "FontNameLen", tag.getFontNameLen());
 		visitor.visit(tag, "FontName", tag.getFontName());
@@ -723,6 +941,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineFontInfoTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "FontID", tag.getFontId());
 		visitor.visit(tag, "FontNameLen", tag.getFontNameLen());
 		visitor.visit(tag, "FontName", tag.getFontName());
@@ -745,6 +967,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineFontNameTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "FontID", tag.getFontId());
 		visitor.visit(tag, "FontName", tag.getFontName());
 		visitor.visit(tag, "FontCopyright", tag.getFontCopyright());
@@ -758,6 +984,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineFontTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "FontID", tag.getFontId());
 		visitor.visit(tag, "OffsetTable", tag.getOffsetTable());
 		visit(tag.getOffsetTable(), visitor);
@@ -765,6 +995,41 @@ public final class SWFIterator {
 		visit(tag.getGlyphShapeTable(), visitor);
 	}
 
+	/**
+	 * Visits a DefineMorphShape2Tag object.
+	 * 
+	 * @param tag The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final DefineMorphShape2Tag tag, final ISWFVisitor visitor) {
+
+		if (tag == null) {
+			return;
+		}
+
+		visitor.visit(tag, "CharacterID", tag.getCharacterId());
+		visitor.visit(tag, "StartBounds", tag.getStartBounds());
+		visit(tag.getStartBounds(), visitor);
+		visitor.visit(tag, "EndBounds", tag.getEndBounds());
+		visit(tag.getEndBounds(), visitor);
+		visitor.visit(tag, "StartEdgeBounds", tag.getStartEdgeBounds());
+		visit(tag.getStartBounds(), visitor);
+		visitor.visit(tag, "EndEdgeBounds", tag.getEndEdgeBounds());
+		visit(tag.getEndBounds(), visitor);
+		visitor.visit(tag, "Reserved", tag.getReserved());
+		visitor.visit(tag, "UsesNonScalingStrokes", tag.getUsesNonScalingStrokes());
+		visitor.visit(tag, "UsesScalingStrokes", tag.getUsesScalingStrokes());
+		visitor.visit(tag, "Offset", tag.getOffset());
+		visitor.visit(tag, "MorphFillStyles", tag.getMorphFillStyles());
+		visit(tag.getMorphFillStyles(), visitor);
+		visitor.visit(tag, "MorphLineStyles", tag.getMorphLineStyles());
+		visit(tag.getMorphLineStyles(), visitor);
+		visitor.visit(tag, "StartEdges", tag.getStartEdges());
+		visit(tag.getStartEdges(), visitor);
+		visitor.visit(tag, "EndEdges", tag.getEndEdges());
+		visit(tag.getEndEdges(), visitor);
+	}
+
 	/**
 	 * Visits a DefineMorphShapeTag object.
 	 * 
@@ -773,6 +1038,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineMorphShapeTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "CharacterID", tag.getCharacterId());
 		visitor.visit(tag, "StartBounds", tag.getStartBounds());
 		visit(tag.getStartBounds(), visitor);
@@ -789,6 +1058,43 @@ public final class SWFIterator {
 		visit(tag.getEndEdges(), visitor);
 	}
 
+	/**
+	 * Visits a DefineScalingGridTag object.
+	 * 
+	 * @param tag The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final DefineScalingGridTag tag, final ISWFVisitor visitor) {
+
+		if (tag == null) {
+			return;
+		}
+
+		visitor.visit(tag, "CharacterID", tag.getCharacterId());
+		visitor.visit(tag, "Splitter", tag.getSplitter());
+		visit(tag.getSplitter(), visitor);
+	}
+
+	/**
+	 * Visits a DefineSceneAndFrameLabelDataTag object.
+	 * 
+	 * @param tag The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final DefineSceneAndFrameLabelDataTag tag, final ISWFVisitor visitor) {
+
+		if (tag == null) {
+			return;
+		}
+
+		visitor.visit(tag, "SceneCount", tag.getSceneCount());
+		visitor.visit(tag, "SceneNames", tag.getSceneNames());
+		visit(tag.getSceneNames(), visitor);
+		visitor.visit(tag, "FrameLabelCount", tag.getFrameLabelCount());
+		visitor.visit(tag, "FrameLabels", tag.getFrameLabels());
+		visit(tag.getFrameLabels(), visitor);
+	}
+
 	/**
 	 * Visits a DefineShape2Tag object.
 	 * 
@@ -797,6 +1103,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineShape2Tag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "ShapeID", tag.getShapeId());
 		visitor.visit(tag, "ShapeBounds", tag.getShapeBounds());
 		visit(tag.getShapeBounds(), visitor);
@@ -812,6 +1122,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineShape3Tag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "ShapeID", tag.getShapeId());
 		visitor.visit(tag, "ShapeBounds", tag.getShapeBounds());
 		visit(tag.getShapeBounds(), visitor);
@@ -827,6 +1141,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineShape4Tag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "ShapeID", tag.getShapeId());
 		visitor.visit(tag, "ShapeBounds", tag.getShapeBounds());
 		visit(tag.getShapeBounds(), visitor);
@@ -848,6 +1166,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineShapeTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "ShapeID", tag.getShapeId());
 		visitor.visit(tag, "ShapeBounds", tag.getShapeBounds());
 		visit(tag.getShapeBounds(), visitor);
@@ -863,6 +1185,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineSoundTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "SoundID", tag.getSoundId());
 		visitor.visit(tag, "SoundFormat", tag.getSoundFormat());
 		visitor.visit(tag, "SoundRate", tag.getSoundRate());
@@ -880,6 +1206,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineSpriteTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "SpriteID", tag.getSpriteId());
 		visitor.visit(tag, "FrameCount", tag.getFrameCount());
 		visitor.visit(tag, "ControlTags", tag.getControlTags());
@@ -894,6 +1224,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineText2Tag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "CharacterID", tag.getCharacterId());
 		visitor.visit(tag, "TextBounds", tag.getTextBounds());
 		visit(tag.getTextBounds(), visitor);
@@ -914,6 +1248,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineTextTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "CharacterID", tag.getCharacterId());
 		visitor.visit(tag, "TextBounds", tag.getTextBounds());
 		visit(tag.getTextBounds(), visitor);
@@ -934,6 +1272,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DefineVideoStreamTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "CharacterID", tag.getCharacterId());
 		visitor.visit(tag, "NumFrames", tag.getNumFrames());
 		visitor.visit(tag, "Width", tag.getWidth());
@@ -944,6 +1286,23 @@ public final class SWFIterator {
 		visitor.visit(tag, "CodecID", tag.getCodecId());
 	}
 
+	/**
+	 * Visits a DoABC object.
+	 * 
+	 * @param tag The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final DoABCTag tag, final ISWFVisitor visitor) {
+
+		if (tag == null) {
+			return;
+		}
+
+		visitor.visit(tag, "Flags", tag.getFlags());
+		visitor.visit(tag, "Name", tag.getName());
+		visitor.visit(tag, "ABCData", tag.getAbcData());
+	}
+
 	/**
 	 * Visits a DoActionTag object.
 	 * 
@@ -951,7 +1310,13 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final DoActionTag tag, final ISWFVisitor visitor) {
-		// TODO
+
+		if (tag == null) {
+			return;
+		}
+
+		visitor.visit(tag, "Actions", tag.getActions());
+		visit(tag.getActions(), visitor);
 	}
 
 	/**
@@ -962,8 +1327,13 @@ public final class SWFIterator {
 	 */
 	public static void visit(final DoInitActionTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "SpriteID", tag.getSpriteId());
-		// TODO
+		visitor.visit(tag, "Actions", tag.getActions());
+		visit(tag.getActions(), visitor);
 	}
 
 	/**
@@ -973,6 +1343,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final DropShadowFilter filter, final ISWFVisitor visitor) {
+
+		if (filter == null) {
+			return;
+		}
+
 		visitor.visit(filter, "DropShadowColor", filter.getDropShadowColor());
 		visit(filter.getDropShadowColor(), visitor);
 		visitor.visit(filter, "BlurX", filter.getBlurX());
@@ -986,6 +1361,37 @@ public final class SWFIterator {
 		visitor.visit(filter, "Passes", filter.getPasses());
 	}
 
+	/**
+	 * Visits a EnableDebugger2Tag object.
+	 * 
+	 * @param tag The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final EnableDebugger2Tag tag, final ISWFVisitor visitor) {
+
+		if (tag == null) {
+			return;
+		}
+
+		visitor.visit(tag, "Password", tag.getPassword());
+		visitor.visit(tag, "Reserved", tag.getReserved());
+	}
+
+	/**
+	 * Visits a EnableDebuggerTag object.
+	 * 
+	 * @param tag The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final EnableDebuggerTag tag, final ISWFVisitor visitor) {
+
+		if (tag == null) {
+			return;
+		}
+
+		visitor.visit(tag, "Password", tag.getPassword());
+	}
+
 	/**
 	 * Visits an EndShapeRecord object.
 	 * 
@@ -994,6 +1400,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final EndShapeRecord shapeRecord, final ISWFVisitor visitor) {
 
+		if (shapeRecord == null) {
+			return;
+		}
+
 		visitor.visit(shapeRecord, "TypeFlag", shapeRecord.getTypeFlag());
 		visitor.visit(shapeRecord, "EndOfShape", shapeRecord.getEndOfShape());
 	}
@@ -1016,6 +1426,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final ExportAssetsTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "Count", tag.getCount());
 		visitor.visit(tag, "Tags", tag.getAssets());
 
@@ -1032,6 +1446,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final FileAttributesTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "Reserved", tag.getReserved());
 		visitor.visit(tag, "UseDirectBit", tag.getUseDirectBit());
 		visitor.visit(tag, "UseGPU", tag.getUseGPU());
@@ -1050,6 +1468,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final FillStyle fillStyle, final ISWFVisitor visitor) {
 
+		if (fillStyle == null) {
+			return;
+		}
+
 		visitor.visit(fillStyle, "Width", fillStyle.getFillStyleType());
 		visitor.visit(fillStyle, "Color", fillStyle.getColor());
 		visit(fillStyle.getColor(), visitor);
@@ -1079,6 +1501,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final FillStyle3 fillStyle, final ISWFVisitor visitor) {
 
+		if (fillStyle == null) {
+			return;
+		}
+
 		visitor.visit(fillStyle, "Width", fillStyle.getFillStyleType());
 		visitor.visit(fillStyle, "Color", fillStyle.getColor());
 		visit(fillStyle.getColor(), visitor);
@@ -1108,14 +1534,15 @@ public final class SWFIterator {
 	 */
 	public static void visit(final FillStyle3Array fillStyles, final ISWFVisitor visitor) {
 
-		if (fillStyles != null) {
+		if (fillStyles == null) {
+			return;
+		}
 
-			visitor.visit(fillStyles, "FillStyleCount", fillStyles.getFillStyleCount());
-			visitor.visit(fillStyles, "FillStyleCountExtended", fillStyles.getFillStyleCountExtended());
-			visitor.visit(fillStyles, "FillStyles", fillStyles.getFillStyles());
+		visitor.visit(fillStyles, "FillStyleCount", fillStyles.getFillStyleCount());
+		visitor.visit(fillStyles, "FillStyleCountExtended", fillStyles.getFillStyleCountExtended());
+		visitor.visit(fillStyles, "FillStyles", fillStyles.getFillStyles());
 
-			visit(fillStyles.getFillStyles(), visitor);
-		}
+		visit(fillStyles.getFillStyles(), visitor);
 	}
 
 	/**
@@ -1125,6 +1552,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final FillStyle3List fillStyles, final ISWFVisitor visitor) {
+
+		if (fillStyles == null) {
+			return;
+		}
+
 		for (final FillStyle3 fillStyle : fillStyles) {
 			visitor.visit(fillStyles, "FillStyle", fillStyle);
 			visit(fillStyle, visitor);
@@ -1139,14 +1571,15 @@ public final class SWFIterator {
 	 */
 	public static void visit(final FillStyleArray fillStyles, final ISWFVisitor visitor) {
 
-		if (fillStyles != null) {
+		if (fillStyles == null) {
+			return;
+		}
 
-			visitor.visit(fillStyles, "FillStyleCount", fillStyles.getFillStyleCount());
-			visitor.visit(fillStyles, "FillStyleCountExtended", fillStyles.getFillStyleCountExtended());
-			visitor.visit(fillStyles, "FillStyles", fillStyles.getFillStyles());
+		visitor.visit(fillStyles, "FillStyleCount", fillStyles.getFillStyleCount());
+		visitor.visit(fillStyles, "FillStyleCountExtended", fillStyles.getFillStyleCountExtended());
+		visitor.visit(fillStyles, "FillStyles", fillStyles.getFillStyles());
 
-			visit(fillStyles.getFillStyles(), visitor);
-		}
+		visit(fillStyles.getFillStyles(), visitor);
 	}
 
 	/**
@@ -1156,6 +1589,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final FillStyleList fillStyles, final ISWFVisitor visitor) {
+
+		if (fillStyles == null) {
+			return;
+		}
+
 		for (final FillStyle fillStyle : fillStyles) {
 			visitor.visit(fillStyles, "FillStyle", fillStyle);
 			visit(fillStyle, visitor);
@@ -1169,6 +1607,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final Filter filter, final ISWFVisitor visitor) {
+
+		if (filter == null) {
+			return;
+		}
+
 		visitor.visit(filter, "FilterID", filter.getFilterId());
 		visitor.visit(filter, "DropShadowFilter", filter.getDropShadowFilter());
 		visit(filter.getDropShadowFilter(), visitor);
@@ -1196,11 +1639,13 @@ public final class SWFIterator {
 	 */
 	public static void visit(final FilterList filterList, final ISWFVisitor visitor) {
 
-		if (filterList != null) {
-			visitor.visit(filterList, "NumberOfFilters", filterList.getNumberOfFilters());
-			visitor.visit(filterList, "Filters", filterList.getFilters());
-			visit(filterList.getFilters(), visitor);
+		if (filterList == null) {
+			return;
 		}
+
+		visitor.visit(filterList, "NumberOfFilters", filterList.getNumberOfFilters());
+		visitor.visit(filterList, "Filters", filterList.getFilters());
+		visit(filterList.getFilters(), visitor);
 	}
 
 	/**
@@ -1211,14 +1656,49 @@ public final class SWFIterator {
 	 */
 	public static void visit(final FocalGradient gradient, final ISWFVisitor visitor) {
 
-		if (gradient != null) {
+		if (gradient == null) {
+			return;
+		}
+
+		visitor.visit(gradient, "SpreadMode", gradient.getSpreadMode());
+		visitor.visit(gradient, "InterpolationMode", gradient.getInterpolationMode());
+		visitor.visit(gradient, "NumGradients", gradient.getNumGradients());
+		visitor.visit(gradient, "GradientRecords", gradient.getGradientRecords());
+		visit(gradient.getGradientRecords(), visitor);
+		visitor.visit(gradient, "FocalPoint", gradient.getFocalPoint());
+	}
+
+	/**
+	 * Visits a FrameLabel object.
+	 * 
+	 * @param frameLabel The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final FrameLabel frameLabel, final ISWFVisitor visitor) {
+
+		if (frameLabel == null) {
+			return;
+		}
+
+		visitor.visit(frameLabel, "Offset", frameLabel.getOffset());
+		visitor.visit(frameLabel, "Name", frameLabel.getName());
+	}
+
+	/**
+	 * Visits a FrameLabelList object.
+	 * 
+	 * @param frameLabels The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final FrameLabelList frameLabels, final ISWFVisitor visitor) {
+
+		if (frameLabels == null) {
+			return;
+		}
 
-			visitor.visit(gradient, "SpreadMode", gradient.getSpreadMode());
-			visitor.visit(gradient, "InterpolationMode", gradient.getInterpolationMode());
-			visitor.visit(gradient, "NumGradients", gradient.getNumGradients());
-			visitor.visit(gradient, "GradientRecords", gradient.getGradientRecords());
-			visit(gradient.getGradientRecords(), visitor);
-			visitor.visit(gradient, "FocalPoint", gradient.getFocalPoint());
+		for (final FrameLabel frameLabel : frameLabels) {
+			visitor.visit(frameLabels, "FrameLabel", frameLabel);
+			visit(frameLabel, visitor);
 		}
 	}
 
@@ -1230,6 +1710,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final FrameLabelTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "Name", tag.getName());
 		visitor.visit(tag, "AnchorFlag", tag.getNamedAnchorFlag());
 	}
@@ -1241,6 +1725,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final GlowFilter filter, final ISWFVisitor visitor) {
+
+		if (filter == null) {
+			return;
+		}
+
 		visitor.visit(filter, "GlowColor", filter.getGlowColor());
 		visit(filter.getGlowColor(), visitor);
 		visitor.visit(filter, "BlurX", filter.getBlurX());
@@ -1260,6 +1749,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final GlyphEntry glyphEntry, final ISWFVisitor visitor) {
 
+		if (glyphEntry == null) {
+			return;
+		}
+
 		visitor.visit(glyphEntry, "GlyphIndex", glyphEntry.getGlyphIndex());
 		visitor.visit(glyphEntry, "AdvanceIndex", glyphEntry.getAdvanceIndex());
 	}
@@ -1271,6 +1764,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final GlyphEntryList glyphEntries, final ISWFVisitor visitor) {
+
+		if (glyphEntries == null) {
+			return;
+		}
+
 		for (final GlyphEntry glyphEntry : glyphEntries) {
 			visitor.visit(glyphEntries, "GlyphEntry", glyphEntry);
 			visit(glyphEntry, visitor);
@@ -1285,6 +1783,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final Gradient gradient, final ISWFVisitor visitor) {
 
+		if (gradient == null) {
+			return;
+		}
+
 		visitor.visit(gradient, "SpreadMode", gradient.getSpreadMode());
 		visitor.visit(gradient, "InterpolationMode", gradient.getInterpolationMode());
 		visitor.visit(gradient, "NumGradients", gradient.getNumGradients());
@@ -1300,6 +1802,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final Gradient3 gradient, final ISWFVisitor visitor) {
 
+		if (gradient == null) {
+			return;
+		}
+
 		visitor.visit(gradient, "SpreadMode", gradient.getSpreadMode());
 		visitor.visit(gradient, "InterpolationMode", gradient.getInterpolationMode());
 		visitor.visit(gradient, "NumGradients", gradient.getNumGradients());
@@ -1314,8 +1820,14 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final GradientBevelFilter filter, final ISWFVisitor visitor) {
+
+		if (filter == null) {
+			return;
+		}
+
 		visitor.visit(filter, "NumColors", filter.getNumColors());
-		// TODO
+		visitor.visit(filter, "GradientColors", filter.getGradientColors());
+		visitor.visit(filter, "GradientRatio", filter.getGradientRatio());
 		visitor.visit(filter, "BlurX", filter.getBlurX());
 		visitor.visit(filter, "BlurY", filter.getBlurY());
 		visitor.visit(filter, "Angle", filter.getAngle());
@@ -1335,8 +1847,14 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final GradientGlowFilter filter, final ISWFVisitor visitor) {
+
+		if (filter == null) {
+			return;
+		}
+
 		visitor.visit(filter, "NumColors", filter.getNumColors());
-		//TODO
+		visitor.visit(filter, "GradientColors", filter.getGradientColors());
+		visitor.visit(filter, "GradientRatio", filter.getGradientRatio());
 		visitor.visit(filter, "BlurX", filter.getBlurX());
 		visitor.visit(filter, "BlurY", filter.getBlurY());
 		visitor.visit(filter, "Angle", filter.getAngle());
@@ -1357,6 +1875,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final GradRecord gradRecord, final ISWFVisitor visitor) {
 
+		if (gradRecord == null) {
+			return;
+		}
+
 		visitor.visit(gradRecord, "Ratio", gradRecord.getRatio());
 		visitor.visit(gradRecord, "Color", gradRecord.getColor());
 		visit(gradRecord.getColor(), visitor);
@@ -1370,6 +1892,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final GradRecord3 gradRecord, final ISWFVisitor visitor) {
 
+		if (gradRecord == null) {
+			return;
+		}
+
 		visitor.visit(gradRecord, "Ratio", gradRecord.getRatio());
 		visitor.visit(gradRecord, "Color", gradRecord.getColor());
 		visit(gradRecord.getColor(), visitor);
@@ -1383,6 +1909,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final GradRecord3List gradientRecords, final ISWFVisitor visitor) {
 
+		if (gradientRecords == null) {
+			return;
+		}
+
 		for (final GradRecord3 gradRecord : gradientRecords) {
 			visitor.visit(gradientRecords, "GradRecord", gradRecord);
 			visit(gradRecord, visitor);
@@ -1397,12 +1927,54 @@ public final class SWFIterator {
 	 */
 	public static void visit(final GradRecordList gradientRecords, final ISWFVisitor visitor) {
 
+		if (gradientRecords == null) {
+			return;
+		}
+
 		for (final GradRecord gradRecord : gradientRecords) {
 			visitor.visit(gradientRecords, "GradRecord", gradRecord);
 			visit(gradRecord, visitor);
 		}
 	}
 
+	/**
+	 * Visits an ImportAssets2Tag object.
+	 * 
+	 * @param tag The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final ImportAssets2Tag tag, final ISWFVisitor visitor) {
+
+		if (tag == null) {
+			return;
+		}
+
+		visitor.visit(tag, "URL", tag.getUrl());
+		visitor.visit(tag, "Reserved", tag.getReserved());
+		visitor.visit(tag, "Reserved2", tag.getReserved2());
+		visitor.visit(tag, "Count", tag.getCount());
+		visitor.visit(tag, "Symbols", tag.getSymbols());
+		visit(tag.getSymbols(), visitor);
+	}
+
+	/**
+	 * Visits an ImportAssetsTag object.
+	 * 
+	 * @param tag The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final ImportAssetsTag tag, final ISWFVisitor visitor) {
+
+		if (tag == null) {
+			return;
+		}
+
+		visitor.visit(tag, "URL", tag.getUrl());
+		visitor.visit(tag, "Count", tag.getCount());
+		visitor.visit(tag, "Symbols", tag.getSymbols());
+		visit(tag.getSymbols(), visitor);
+	}
+
 	/**
 	 * Visits an INT16List object.
 	 * 
@@ -1410,6 +1982,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final INT16List list, final ISWFVisitor visitor) {
+
+		if (list == null) {
+			return;
+		}
+
 		for (final INT16 element : list) {
 			visitor.visit(list, "Element", element);
 		}
@@ -1423,6 +2000,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final JPEGTablesTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "JPEGData", tag.getJpegData());
 	}
 
@@ -1434,6 +2015,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final KerningRecord kerningRecord, final ISWFVisitor visitor) {
 
+		if (kerningRecord == null) {
+			return;
+		}
+
 		final IParsedINTElement kerningCode1 = kerningRecord.getFontKerningCode1();
 
 		if (kerningCode1 instanceof UINT16) {
@@ -1462,6 +2047,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final KerningRecordList list, final ISWFVisitor visitor) {
+
+		if (list == null) {
+			return;
+		}
+
 		for (final KerningRecord kerningRecord : list) {
 			visitor.visit(list, "KerningRecord", kerningRecord);
 			visit(kerningRecord, visitor);
@@ -1476,6 +2066,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final LineStyle lineStyle, final ISWFVisitor visitor) {
 
+		if (lineStyle == null) {
+			return;
+		}
+
 		visitor.visit(lineStyle, "Width", lineStyle.getWidth());
 		visitor.visit(lineStyle, "Color", lineStyle.getColor());
 		visit(lineStyle.getColor(), visitor);
@@ -1489,6 +2083,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final LineStyle3 lineStyle, final ISWFVisitor visitor) {
 
+		if (lineStyle == null) {
+			return;
+		}
+
 		visitor.visit(lineStyle, "Width", lineStyle.getWidth());
 		visitor.visit(lineStyle, "Color", lineStyle.getColor());
 		visit(lineStyle.getColor(), visitor);
@@ -1502,14 +2100,15 @@ public final class SWFIterator {
 	 */
 	public static void visit(final LineStyle3Array lineStyles, final ISWFVisitor visitor) {
 
-		if (lineStyles != null) {
+		if (lineStyles == null) {
+			return;
+		}
 
-			visitor.visit(lineStyles, "LineStyleCount", lineStyles.getLineStyleCount());
-			visitor.visit(lineStyles, "LineStyleCountExtended", lineStyles.getLineStyleCountExtended());
-			visitor.visit(lineStyles, "LineStyles", lineStyles.getLineStyles());
+		visitor.visit(lineStyles, "LineStyleCount", lineStyles.getLineStyleCount());
+		visitor.visit(lineStyles, "LineStyleCountExtended", lineStyles.getLineStyleCountExtended());
+		visitor.visit(lineStyles, "LineStyles", lineStyles.getLineStyles());
 
-			visit(lineStyles.getLineStyles(), visitor);
-		}
+		visit(lineStyles.getLineStyles(), visitor);
 	}
 
 	/**
@@ -1519,6 +2118,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final LineStyle3List lineStyles, final ISWFVisitor visitor) {
+
+		if (lineStyles == null) {
+			return;
+		}
+
 		for (final LineStyle3 lineStyle : lineStyles) {
 			visitor.visit(lineStyles, "LineStyle", lineStyle);
 			visit(lineStyle, visitor);
@@ -1533,6 +2137,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final LineStyle4 lineStyle, final ISWFVisitor visitor) {
 
+		if (lineStyle == null) {
+			return;
+		}
+
 		visitor.visit(lineStyle, "Width", lineStyle.getWidth());
 		visitor.visit(lineStyle, "Color", lineStyle.getColor());
 		visit(lineStyle.getColor(), visitor);
@@ -1546,6 +2154,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final LineStyle4Array lineStyles, final ISWFVisitor visitor) {
 
+		if (lineStyles == null) {
+			return;
+		}
+
 		visitor.visit(lineStyles, "LineStyleCount", lineStyles.getLineStyleCount());
 		visitor.visit(lineStyles, "LineStyleCountExtended", lineStyles.getLineStyleCountExtended());
 		visitor.visit(lineStyles, "LineStyles", lineStyles.getLineStyles());
@@ -1560,6 +2172,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final LineStyle4List lineStyles, final ISWFVisitor visitor) {
+
+		if (lineStyles == null) {
+			return;
+		}
+
 		for (final LineStyle4 lineStyle : lineStyles) {
 			visitor.visit(lineStyles, "LineStyle", lineStyle);
 			visit(lineStyle, visitor);
@@ -1574,14 +2191,15 @@ public final class SWFIterator {
 	 */
 	public static void visit(final LineStyleArray lineStyles, final ISWFVisitor visitor) {
 
-		if (lineStyles != null) {
+		if (lineStyles == null) {
+			return;
+		}
 
-			visitor.visit(lineStyles, "LineStyleCount", lineStyles.getLineStyleCount());
-			visitor.visit(lineStyles, "LineStyleCountExtended", lineStyles.getLineStyleCountExtended());
-			visitor.visit(lineStyles, "LineStyles", lineStyles.getLineStyles());
+		visitor.visit(lineStyles, "LineStyleCount", lineStyles.getLineStyleCount());
+		visitor.visit(lineStyles, "LineStyleCountExtended", lineStyles.getLineStyleCountExtended());
+		visitor.visit(lineStyles, "LineStyles", lineStyles.getLineStyles());
 
-			visit(lineStyles.getLineStyles(), visitor);
-		}
+		visit(lineStyles.getLineStyles(), visitor);
 	}
 
 	/**
@@ -1591,6 +2209,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final LineStyleList lineStyles, final ISWFVisitor visitor) {
+
+		if (lineStyles == null) {
+			return;
+		}
+
 		for (final LineStyle lineStyle : lineStyles) {
 			visitor.visit(lineStyles, "LineStyle", lineStyle);
 			visit(lineStyle, visitor);
@@ -1605,19 +2228,21 @@ public final class SWFIterator {
 	 */
 	public static void visit(final Matrix matrix, final ISWFVisitor visitor) {
 
-		if (matrix != null) {
-			visitor.visit(matrix, "HasScale", matrix.getHasScale());
-			visitor.visit(matrix, "NScaleBits", matrix.getnScaleBits());
-			visitor.visit(matrix, "ScaleX", matrix.getScaleX());
-			visitor.visit(matrix, "ScaleY", matrix.getScaleY());
-			visitor.visit(matrix, "HasRotate", matrix.getHasRotate());
-			visitor.visit(matrix, "NRotateBits", matrix.getnRotateBits());
-			visitor.visit(matrix, "RotateSkew0", matrix.getRotateSkew0());
-			visitor.visit(matrix, "RotateSkew1", matrix.getRotateSkew1());
-			visitor.visit(matrix, "NTranslateBits", matrix.getnTranslateBits());
-			visitor.visit(matrix, "TranslateX", matrix.getTranslateX());
-			visitor.visit(matrix, "TranslateY", matrix.getTranslateY());
+		if (matrix == null) {
+			return;
 		}
+
+		visitor.visit(matrix, "HasScale", matrix.getHasScale());
+		visitor.visit(matrix, "NScaleBits", matrix.getnScaleBits());
+		visitor.visit(matrix, "ScaleX", matrix.getScaleX());
+		visitor.visit(matrix, "ScaleY", matrix.getScaleY());
+		visitor.visit(matrix, "HasRotate", matrix.getHasRotate());
+		visitor.visit(matrix, "NRotateBits", matrix.getnRotateBits());
+		visitor.visit(matrix, "RotateSkew0", matrix.getRotateSkew0());
+		visitor.visit(matrix, "RotateSkew1", matrix.getRotateSkew1());
+		visitor.visit(matrix, "NTranslateBits", matrix.getnTranslateBits());
+		visitor.visit(matrix, "TranslateX", matrix.getTranslateX());
+		visitor.visit(matrix, "TranslateY", matrix.getTranslateY());
 	}
 
 	/**
@@ -1628,6 +2253,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final MetadataTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "Metadata", tag.getMetadata());
 	}
 
@@ -1639,6 +2268,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final MorphFillStyle fillStyle, final ISWFVisitor visitor) {
 
+		if (fillStyle == null) {
+			return;
+		}
+
 		visitor.visit(fillStyle, "Width", fillStyle.getFillStyleType());
 		visitor.visit(fillStyle, "StartColor", fillStyle.getStartColor());
 		visit(fillStyle.getStartColor(), visitor);
@@ -1665,6 +2298,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final MorphFillStyleArray fillStyles, final ISWFVisitor visitor) {
 
+		if (fillStyles == null) {
+			return;
+		}
+
 		visitor.visit(fillStyles, "FillStyleCount", fillStyles.getFillStyleCount());
 		visitor.visit(fillStyles, "FillStyleCountExtended", fillStyles.getFillStyleCountExtended());
 		visitor.visit(fillStyles, "FillStyles", fillStyles.getFillStyles());
@@ -1679,6 +2316,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final MorphFillStyleList fillStyles, final ISWFVisitor visitor) {
+
+		if (fillStyles == null) {
+			return;
+		}
+
 		for (final MorphFillStyle fillStyle : fillStyles) {
 			visitor.visit(fillStyles, "MorphFillStyle", fillStyle);
 			visit(fillStyle, visitor);
@@ -1693,11 +2335,13 @@ public final class SWFIterator {
 	 */
 	public static void visit(final MorphGradient gradient, final ISWFVisitor visitor) {
 
-		if (gradient != null) {
-			visitor.visit(gradient, "NumGradients", gradient.getNumGradients());
-			visitor.visit(gradient, "GradientRecords", gradient.getGradientRecords());
-			visit(gradient.getGradientRecords(), visitor);
+		if (gradient == null) {
+			return;
 		}
+
+		visitor.visit(gradient, "NumGradients", gradient.getNumGradients());
+		visitor.visit(gradient, "GradientRecords", gradient.getGradientRecords());
+		visit(gradient.getGradientRecords(), visitor);
 	}
 
 	/**
@@ -1707,6 +2351,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final MorphGradientRecord record, final ISWFVisitor visitor) {
+
+		if (record == null) {
+			return;
+		}
+
 		visitor.visit(record, "StartRatio", record.getStartRatio());
 		visitor.visit(record, "StartColor", record.getStartColor());
 		visit(record.getStartColor(), visitor);
@@ -1722,6 +2371,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final MorphGradientRecordList list, final ISWFVisitor visitor) {
+
+		if (list == null) {
+			return;
+		}
+
 		for (final MorphGradientRecord record : list) {
 			visitor.visit(list, "GradientRecord", record);
 			visit(record, visitor);
@@ -1736,6 +2390,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final MorphLineStyle lineStyle, final ISWFVisitor visitor) {
 
+		if (lineStyle == null) {
+			return;
+		}
+
 		visitor.visit(lineStyle, "StartWidth", lineStyle.getStartWidth());
 		visitor.visit(lineStyle, "EndWidth", lineStyle.getEndWidth());
 		visitor.visit(lineStyle, "StartColor", lineStyle.getStartColor());
@@ -1745,54 +2403,122 @@ public final class SWFIterator {
 	}
 
 	/**
-	 * Visits a MorphLineStyleArray object.
+	 * Visits a MorphLineStyle2 object.
 	 * 
-	 * @param lineStyles The visited object.
+	 * @param lineStyle The visited object.
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
-	public static void visit(final MorphLineStyleArray lineStyles, final ISWFVisitor visitor) {
-
-		if (lineStyles != null) {
+	public static void visit(final MorphLineStyle2 lineStyle, final ISWFVisitor visitor) {
 
-			visitor.visit(lineStyles, "LineStyleCount", lineStyles.getLineStyleCount());
-			visitor.visit(lineStyles, "LineStyleCountExtended", lineStyles.getLineStyleCountExtended());
-			visitor.visit(lineStyles, "LineStyles", lineStyles.getLineStyles());
-
-			visit(lineStyles.getLineStyles(), visitor);
+		if (lineStyle == null) {
+			return;
 		}
+
+		visitor.visit(lineStyle, "StartWidth", lineStyle.getStartWidth());
+		visitor.visit(lineStyle, "EndWidth", lineStyle.getEndWidth());
+		visitor.visit(lineStyle, "StartColor", lineStyle.getStartColor());
+		visit(lineStyle.getStartColor(), visitor);
+		visitor.visit(lineStyle, "EndColor", lineStyle.getEndColor());
+		visit(lineStyle.getEndColor(), visitor);
 	}
 
 	/**
-	 * Visits a MorphLineStyleList object.
+	 * Visits a MorphLineStyle2List object.
 	 * 
 	 * @param lineStyles The visited object.
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
-	public static void visit(final MorphLineStyleList lineStyles, final ISWFVisitor visitor) {
-		for (final MorphLineStyle lineStyle : lineStyles) {
+	public static void visit(final MorphLineStyle2List lineStyles, final ISWFVisitor visitor) {
+
+		if (lineStyles == null) {
+			return;
+		}
+
+		for (final MorphLineStyle2 lineStyle : lineStyles) {
 			visitor.visit(lineStyles, "MorphLineStyle", lineStyle);
 			visit(lineStyle, visitor);
 		}
 	}
 
 	/**
-	 * Visits a ParsedINTELementList object.
+	 * Visits a MorphLineStyleArray object.
 	 * 
-	 * @param list The visited object.
+	 * @param lineStyles The visited object.
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
-	public static void visit(final ParsedINTElementList list, final ISWFVisitor visitor) {
-		for (final IParsedINTElement element : list) {
+	public static void visit(final MorphLineStyleArray lineStyles, final ISWFVisitor visitor) {
 
-			if (element instanceof UINT8) {
-				visitor.visit(list, "Element", (UINT8) element);
-			}
-			else if (element instanceof UINT16) {
-				visitor.visit(list, "Element", (UINT16) element);
-			}
-			else if (element instanceof UINT32) {
-				visitor.visit(list, "Element", (UINT32) element);
-			}
+		if (lineStyles == null) {
+			return;
+		}
+
+		visitor.visit(lineStyles, "LineStyleCount", lineStyles.getLineStyleCount());
+		visitor.visit(lineStyles, "LineStyleCountExtended", lineStyles.getLineStyleCountExtended());
+		visitor.visit(lineStyles, "LineStyles", lineStyles.getLineStyles());
+
+		visit(lineStyles.getLineStyles(), visitor);
+	}
+
+	/**
+	 * Visits a MorphLineStyleArray2 object.
+	 * 
+	 * @param lineStyles The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final MorphLineStyleArray2 lineStyles, final ISWFVisitor visitor) {
+
+		if (lineStyles == null) {
+			return;
+		}
+
+		visitor.visit(lineStyles, "LineStyleCount", lineStyles.getLineStyleCount());
+		visitor.visit(lineStyles, "LineStyleCountExtended", lineStyles.getLineStyleCountExtended());
+		visitor.visit(lineStyles, "LineStyles", lineStyles.getLineStyles());
+
+		visit(lineStyles.getLineStyles(), visitor);
+	}
+
+	/**
+	 * Visits a MorphLineStyleList object.
+	 * 
+	 * @param lineStyles The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final MorphLineStyleList lineStyles, final ISWFVisitor visitor) {
+
+		if (lineStyles == null) {
+			return;
+		}
+
+		for (final MorphLineStyle lineStyle : lineStyles) {
+			visitor.visit(lineStyles, "MorphLineStyle", lineStyle);
+			visit(lineStyle, visitor);
+		}
+	}
+
+	/**
+	 * Visits a ParsedINTELementList object.
+	 * 
+	 * @param list The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final ParsedINTElementList list, final ISWFVisitor visitor) {
+
+		if (list == null) {
+			return;
+		}
+
+		for (final IParsedINTElement element : list) {
+
+			if (element instanceof UINT8) {
+				visitor.visit(list, "Element", (UINT8) element);
+			}
+			else if (element instanceof UINT16) {
+				visitor.visit(list, "Element", (UINT16) element);
+			}
+			else if (element instanceof UINT32) {
+				visitor.visit(list, "Element", (UINT32) element);
+			}
 			else if (element instanceof INT32) {
 				visitor.visit(list, "Element", (INT32) element);
 			}
@@ -1807,6 +2533,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final PlaceObject2Tag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "PlaceFlagHasClipAction", tag.getPlaceFlagHasClipActions());
 		visitor.visit(tag, "PlaceFlagHasClipDepth", tag.getPlaceFlagHasClipDepth());
 		visitor.visit(tag, "PlaceFlagHasName", tag.getPlaceFlagHasName());
@@ -1836,6 +2566,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final PlaceObject3Tag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "PlaceFlagHasClipAction", tag.getPlaceFlagHasClipActions());
 		visitor.visit(tag, "PlaceFlagHasClipDepth", tag.getPlaceFlagHasClipDepth());
 		visitor.visit(tag, "PlaceFlagHasName", tag.getPlaceFlagHasName());
@@ -1876,9 +2610,29 @@ public final class SWFIterator {
 	 */
 	public static void visit(final ProtectTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "Data", tag.getData());
 	}
 
+	/**
+	 * Visits a RecordHeader object.
+	 * 
+	 * @param header The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final RecordHeader header, final ISWFVisitor visitor) {
+
+		if (header == null) {
+			return;
+		}
+
+		visitor.visit(header, "TagAndLength", header.getTagAndLength());
+		visitor.visit(header, "ExtraLength", header.getExtraLength());
+	}
+
 	/**
 	 * Visits a Rect object.
 	 * 
@@ -1887,6 +2641,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final Rect rect, final ISWFVisitor visitor) {
 
+		if (rect == null) {
+			return;
+		}
+
 		visitor.visit(rect, "NBits", rect.getnBits());
 		visitor.visit(rect, "XMin", rect.getxMin());
 		visitor.visit(rect, "XMax", rect.getxMax());
@@ -1901,6 +2659,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final RectList list, final ISWFVisitor visitor) {
+
+		if (list == null) {
+			return;
+		}
+
 		for (final Rect element : list) {
 			visitor.visit(list, "Element", element);
 			visit(element, visitor);
@@ -1915,6 +2678,26 @@ public final class SWFIterator {
 	 */
 	public static void visit(final RemoveObject2Tag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
+		visitor.visit(tag, "Dept", tag.getDepth());
+	}
+
+	/**
+	 * Visits a RemoveObjectTag object.
+	 * 
+	 * @param tag The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final RemoveObjectTag tag, final ISWFVisitor visitor) {
+
+		if (tag == null) {
+			return;
+		}
+
+		visitor.visit(tag, "CharacterID", tag.getCharacterId());
 		visitor.visit(tag, "Dept", tag.getDepth());
 	}
 
@@ -1926,11 +2709,13 @@ public final class SWFIterator {
 	 */
 	public static void visit(final RGB value, final ISWFVisitor visitor) {
 
-		if (value != null) {
-			visitor.visit(value, "Red", value.getRed());
-			visitor.visit(value, "Green", value.getGreen());
-			visitor.visit(value, "Blue", value.getBlue());
+		if (value == null) {
+			return;
 		}
+
+		visitor.visit(value, "Red", value.getRed());
+		visitor.visit(value, "Green", value.getGreen());
+		visitor.visit(value, "Blue", value.getBlue());
 	}
 
 	/**
@@ -1941,12 +2726,64 @@ public final class SWFIterator {
 	 */
 	public static void visit(final RGBA value, final ISWFVisitor visitor) {
 
-		if (value != null) {
-			visitor.visit(value, "Red", value.getRed());
-			visitor.visit(value, "Green", value.getGreen());
-			visitor.visit(value, "Blue", value.getBlue());
-			visitor.visit(value, "Alpha", value.getAlpha());
+		if (value == null) {
+			return;
 		}
+
+		visitor.visit(value, "Red", value.getRed());
+		visitor.visit(value, "Green", value.getGreen());
+		visitor.visit(value, "Blue", value.getBlue());
+		visitor.visit(value, "Alpha", value.getAlpha());
+	}
+
+	/**
+	 * Visits a SceneName object.
+	 * 
+	 * @param sceneName The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final SceneName sceneName, final ISWFVisitor visitor) {
+
+		if (sceneName == null) {
+			return;
+		}
+
+		visitor.visit(sceneName, "Offset", sceneName.getOffset());
+		visitor.visit(sceneName, "Name", sceneName.getName());
+	}
+
+	/**
+	 * Visits a SceneNameList object.
+	 * 
+	 * @param sceneNames The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final SceneNameList sceneNames, final ISWFVisitor visitor) {
+
+		if (sceneNames == null) {
+			return;
+		}
+
+		for (final SceneName sceneName : sceneNames) {
+			visitor.visit(sceneNames, "SceneName", sceneName);
+			visit(sceneName, visitor);
+		}
+	}
+
+	/**
+	 * Visits a ScriptLimitsTag object.
+	 * 
+	 * @param tag The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final ScriptLimitsTag tag, final ISWFVisitor visitor) {
+
+		if (tag == null) {
+			return;
+		}
+
+		visitor.visit(tag, "MaxRecursionDepth", tag.getMaxRecursionDepth());
+		visitor.visit(tag, "ScriptTimeoutSeconds", tag.getScriptTimeoutSeconds());
 	}
 
 	/**
@@ -1957,11 +2794,31 @@ public final class SWFIterator {
 	 */
 	public static void visit(final SetBackgroundColorTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "BackgroundColor", tag.getBackgroundColor());
 
 		visit(tag.getBackgroundColor(), visitor);
 	}
 
+	/**
+	 * Visits a SetTabIndexTag object.
+	 * 
+	 * @param tag The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final SetTabIndexTag tag, final ISWFVisitor visitor) {
+
+		if (tag == null) {
+			return;
+		}
+
+		visitor.visit(tag, "DepthColor", tag.getDepth());
+		visitor.visit(tag, "TabIndex", tag.getTabIndex());
+	}
+
 	/**
 	 * Visits a Shape object.
 	 * 
@@ -1969,6 +2826,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final Shape shape, final ISWFVisitor visitor) {
+
+		if (shape == null) {
+			return;
+		}
+
 		visitor.visit(shape, "NumFillBits", shape.getNumFillBits());
 		visitor.visit(shape, "NumLineBits", shape.getNumLineBits());
 		visitor.visit(shape, "ShapeRecord", shape.getShapeRecord());
@@ -1982,6 +2844,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final Shape3 shape, final ISWFVisitor visitor) {
+
+		if (shape == null) {
+			return;
+		}
+
 		visitor.visit(shape, "NumFillBits", shape.getNumFillBits());
 		visitor.visit(shape, "NumLineBits", shape.getNumLineBits());
 		visitor.visit(shape, "ShapeRecord", shape.getShapeRecord());
@@ -1995,6 +2862,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final Shape3List shapeList, final ISWFVisitor visitor) {
+
+		if (shapeList == null) {
+			return;
+		}
+
 		for (final Shape3 shape3 : shapeList) {
 			visitor.visit(shapeList, "Element", shape3);
 			visit(shape3, visitor);
@@ -2008,6 +2880,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final Shape3RecordList shapeRecordList, final ISWFVisitor visitor) {
+
+		if (shapeRecordList == null) {
+			return;
+		}
+
 		for (final Shape3Record shapeRecord : shapeRecordList) {
 
 			if (shapeRecord instanceof CurvedEdgeRecord) {
@@ -2036,6 +2913,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final ShapeList shapeList, final ISWFVisitor visitor) {
+
+		if (shapeList == null) {
+			return;
+		}
+
 		for (final Shape shape : shapeList) {
 			visitor.visit(shapeList, "Element", shape);
 			visit(shape, visitor);
@@ -2049,6 +2931,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final ShapeRecordList shapeRecordList, final ISWFVisitor visitor) {
+
+		if (shapeRecordList == null) {
+			return;
+		}
+
 		for (final ShapeRecord shapeRecord : shapeRecordList) {
 
 			if (shapeRecord instanceof CurvedEdgeRecord) {
@@ -2077,6 +2964,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final ShapeWithStyle shapeWithStyle, final ISWFVisitor visitor) {
+
+		if (shapeWithStyle == null) {
+			return;
+		}
+
 		visitor.visit(shapeWithStyle, "FillStyles", shapeWithStyle.getFillStyles());
 		visit(shapeWithStyle.getFillStyles(), visitor);
 		visitor.visit(shapeWithStyle, "LineStyles", shapeWithStyle.getLineStyles());
@@ -2111,6 +3003,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final ShapeWithStyle3 shapeWithStyle, final ISWFVisitor visitor) {
+
+		if (shapeWithStyle == null) {
+			return;
+		}
+
 		visitor.visit(shapeWithStyle, "FillStyles", shapeWithStyle.getFillStyles());
 		visit(shapeWithStyle.getFillStyles(), visitor);
 		visitor.visit(shapeWithStyle, "LineStyles", shapeWithStyle.getLineStyles());
@@ -2146,6 +3043,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final ShapeWithStyle4 shapeWithStyle, final ISWFVisitor visitor) {
 
+		if (shapeWithStyle == null) {
+			return;
+		}
+
 		visitor.visit(shapeWithStyle, "FillStyles", shapeWithStyle.getFillStyles());
 		visit(shapeWithStyle.getFillStyles(), visitor);
 		visitor.visit(shapeWithStyle, "LineStyles", shapeWithStyle.getLineStyles());
@@ -2190,6 +3091,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final SingleFilterList filters, final ISWFVisitor visitor) {
+
+		if (filters== null) {
+			return;
+		}
+
 		for (final Filter filter : filters) {
 			visitor.visit(filters, "Filter", filter);
 			visit(filter, visitor);
@@ -2204,6 +3110,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final SoundEnvelope envelope, final ISWFVisitor visitor) {
 
+		if (envelope == null) {
+			return;
+		}
+
 		visitor.visit(envelope, "Pos44", envelope.getPos44());
 		visitor.visit(envelope, "LeftLevel", envelope.getLeftLevel());
 		visitor.visit(envelope, "RightLevel", envelope.getRightLevel());
@@ -2216,6 +3126,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final SoundEnvelopeList list, final ISWFVisitor visitor) {
+
+		if (list == null) {
+			return;
+		}
+
 		for (final SoundEnvelope envelope : list) {
 			visitor.visit(list, "SoundEnvelope", envelope);
 			visit(envelope, visitor);
@@ -2230,6 +3145,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final SoundInfo soundInfo, final ISWFVisitor visitor) {
 
+		if (soundInfo == null) {
+			return;
+		}
+
 		visitor.visit(soundInfo, "Reserved", soundInfo.getReserved());
 		visitor.visit(soundInfo, "SyncStop", soundInfo.getSyncStop());
 		visitor.visit(soundInfo, "SyncNoMultiple", soundInfo.getSyncNoMultiple());
@@ -2253,6 +3172,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final SoundStreamBlockTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "StreamSoundData", tag.getStreamSoundData());
 	}
 
@@ -2264,6 +3187,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final SoundStreamHead2Tag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "Reserved", tag.getReserved());
 		visitor.visit(tag, "PlaybackSoundRate", tag.getPlaybackSoundRate());
 		visitor.visit(tag, "PlaybackSoundSize", tag.getPlaybackSoundSize());
@@ -2284,6 +3211,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final SoundStreamHeadTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "Reserved", tag.getReserved());
 		visitor.visit(tag, "PlaybackSoundRate", tag.getPlaybackSoundRate());
 		visitor.visit(tag, "PlaybackSoundSize", tag.getPlaybackSoundSize());
@@ -2296,6 +3227,23 @@ public final class SWFIterator {
 		visitor.visit(tag, "LatencySeek", tag.getLatencySeek());
 	}
 
+	/**
+	 * Visits a StartSound2Tag object.
+	 * 
+	 * @param tag The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final StartSound2Tag tag, final ISWFVisitor visitor) {
+
+		if (tag == null) {
+			return;
+		}
+
+		visitor.visit(tag, "SoundClassName", tag.getSoundClassName());
+		visitor.visit(tag, "SoundFormat", tag.getSoundInfo());
+		visit(tag.getSoundInfo(), visitor);
+	}
+
 	/**
 	 * Visits a StartSoundTag object.
 	 * 
@@ -2304,6 +3252,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final StartSoundTag tag, final ISWFVisitor visitor) {
 
+		if (tag == null) {
+			return;
+		}
+
 		visitor.visit(tag, "SoundID", tag.getSoundId());
 		visitor.visit(tag, "SoundFormat", tag.getSoundInfo());
 		visit(tag.getSoundInfo(), visitor);
@@ -2316,6 +3268,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final StraightEdgeRecord record, final ISWFVisitor visitor) {
+
+		if (record == null) {
+			return;
+		}
+
 		visitor.visit(record, "TypeFlag", record.getTypeFlag());
 		visitor.visit(record, "StraightFlag", record.getStraightFlag());
 		visitor.visit(record, "NumBits", record.getNumBits());
@@ -2332,6 +3289,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final StyleChangeRecord record, final ISWFVisitor visitor) {
+
+		if (record == null) {
+			return;
+		}
+
 		visitor.visit(record, "TypeFlag", record.getTypeFlag());
 		visitor.visit(record, "StateNewStyles", record.getStateNewStyles());
 		visitor.visit(record, "StateLineStyle", record.getStateLineStyle());
@@ -2359,6 +3321,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final StyleChangeRecord3 record, final ISWFVisitor visitor) {
+
+		if (record == null) {
+			return;
+		}
+
 		visitor.visit(record, "TypeFlag", record.getTypeFlag());
 		visitor.visit(record, "StateNewStyles", record.getStateNewStyles());
 		visitor.visit(record, "StateLineStyle", record.getStateLineStyle());
@@ -2386,6 +3353,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final StyleChangeRecord4 record, final ISWFVisitor visitor) {
+
+		if (record == null) {
+			return;
+		}
+
 		visitor.visit(record, "TypeFlag", record.getTypeFlag());
 		visitor.visit(record, "StateNewStyles", record.getStateNewStyles());
 		visitor.visit(record, "StateLineStyle", record.getStateLineStyle());
@@ -2414,6 +3386,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final SWFFile file, final ISWFVisitor visitor) {
 
+		if (file == null) {
+			return;
+		}
+
 		visitor.visit(file);
 
 		for (final Tag tag : file.getTags()) {
@@ -2431,64 +3407,102 @@ public final class SWFIterator {
 	 */
 	public static void visit(final Symbol symbol, final ISWFVisitor visitor) {
 
+		if (symbol == null) {
+			return;
+		}
+
 		visitor.visit(symbol, "Tag", symbol.getTag());
 		visitor.visit(symbol, "Name", symbol.getName());
 	}
 
 	/**
-	 * Visits a Tag object.
+	 * Visits a SymbolClassTag object.
 	 * 
 	 * @param tag The visited object.
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
-	public static void visit(final Tag tag, final ISWFVisitor visitor) {
-		if (tag instanceof FileAttributesTag) {
-			visit((FileAttributesTag) tag, visitor);
+	public static void visit(final SymbolClassTag tag, final ISWFVisitor visitor) {
+
+		if (tag == null) {
+			return;
 		}
-		else if (tag instanceof MetadataTag) {
-			visit((MetadataTag) tag, visitor);
+
+		visitor.visit(tag, "NumSymbols", tag.getNumSymbols());
+		visitor.visit(tag, "Symbols", tag.getSymbols());
+		visit(tag.getSymbols(), visitor);
+	}
+
+	/**
+	 * Visits a SymbolList object.
+	 * 
+	 * @param list The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final SymbolList list, final ISWFVisitor visitor) {
+
+		if (list == null) {
+			return;
 		}
-		else if (tag instanceof SetBackgroundColorTag) {
-			visit((SetBackgroundColorTag) tag, visitor);
+
+		for (final Symbol element : list) {
+			visitor.visit(element, "Symbol", element);
+			visit(element, visitor);
 		}
-		else if (tag instanceof DefineSpriteTag) {
-			visit((DefineSpriteTag) tag, visitor);
+	}
+
+	/**
+	 * Visits a Tag object.
+	 * 
+	 * @param tag The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final Tag tag, final ISWFVisitor visitor) {
+
+		if (tag == null) {
+			return;
 		}
-		else if (tag instanceof DoActionTag) {
-			visit((DoActionTag) tag, visitor);
+
+		visitor.visit(tag, "Header", tag.getHeader());
+		visit(tag.getHeader(), visitor);
+
+		if (tag instanceof CSMTextSettingsTag) {
+			visit((CSMTextSettingsTag) tag, visitor);
 		}
-		else if (tag instanceof FrameLabelTag) {
-			visit((FrameLabelTag) tag, visitor);
+		else if (tag instanceof DefineBinaryDataTag) {
+			visit((DefineBinaryDataTag) tag, visitor);
 		}
-		else if (tag instanceof ShowFrameTag) {
-			visit((ShowFrameTag) tag, visitor);
+		else if (tag instanceof DefineBitsJPEG2Tag) {
+			visit((DefineBitsJPEG2Tag) tag, visitor);
 		}
-		else if (tag instanceof EndTag) {
-			visit((EndTag) tag, visitor);
+		else if (tag instanceof DefineBitsJPEG3Tag) {
+			visit((DefineBitsJPEG3Tag) tag, visitor);
 		}
-		else if (tag instanceof ExportAssetsTag) {
-			visit((ExportAssetsTag) tag, visitor);
+		else if (tag instanceof DefineBitsJPEG4Tag) {
+			visit((DefineBitsJPEG4Tag) tag, visitor);
 		}
-		else if (tag instanceof DefineShapeTag) {
-			visit((DefineShapeTag) tag, visitor);
+		else if (tag instanceof DefineBitsLosslessTag) {
+			visit((DefineBitsLosslessTag) tag, visitor);
 		}
-		else if (tag instanceof DefineShape2Tag) {
-			visit((DefineShape2Tag) tag, visitor);
+		else if (tag instanceof DefineBitsLossless2Tag) {
+			visit((DefineBitsLossless2Tag) tag, visitor);
 		}
-		else if (tag instanceof DefineShape3Tag) {
-			visit((DefineShape3Tag) tag, visitor);
+		else if (tag instanceof DefineBitsTag) {
+			visit((DefineBitsTag) tag, visitor);
 		}
-		else if (tag instanceof DefineShape4Tag) {
-			visit((DefineShape4Tag) tag, visitor);
+		else if (tag instanceof DefineButtonTag) {
+			visit((DefineButtonTag) tag, visitor);
 		}
-		else if (tag instanceof PlaceObject2Tag) {
-			visit((PlaceObject2Tag) tag, visitor);
+		else if (tag instanceof DefineButton2Tag) {
+			visit((DefineButton2Tag) tag, visitor);
 		}
-		else if (tag instanceof PlaceObject3Tag) {
-			visit((PlaceObject3Tag) tag, visitor);
+		else if (tag instanceof DefineButtonCxformTag) {
+			visit((DefineButtonCxformTag) tag, visitor);
 		}
-		else if (tag instanceof RemoveObject2Tag) {
-			visit((RemoveObject2Tag) tag, visitor);
+		else if (tag instanceof DefineButtonSoundTag) {
+			visit((DefineButtonSoundTag) tag, visitor);
+		}
+		else if (tag instanceof DefineEditTextTag) {
+			visit((DefineEditTextTag) tag, visitor);
 		}
 		else if (tag instanceof DefineFontTag) {
 			visit((DefineFontTag) tag, visitor);
@@ -2499,77 +3513,146 @@ public final class SWFIterator {
 		else if (tag instanceof DefineFont3Tag) {
 			visit((DefineFont3Tag) tag, visitor);
 		}
+		else if (tag instanceof DefineFont4Tag) {
+			visit((DefineFont4Tag) tag, visitor);
+		}
 		else if (tag instanceof DefineFontAlignZonesTag) {
 			visit((DefineFontAlignZonesTag) tag, visitor);
 		}
-		else if (tag instanceof DefineEditTextTag) {
-			visit((DefineEditTextTag) tag, visitor);
+		else if (tag instanceof DefineFontInfoTag) {
+			visit((DefineFontInfoTag) tag, visitor);
 		}
-		else if (tag instanceof DefineVideoStreamTag) {
-			visit((DefineVideoStreamTag) tag, visitor);
+		else if (tag instanceof DefineFontInfo2Tag) {
+			visit((DefineFontInfo2Tag) tag, visitor);
 		}
 		else if (tag instanceof DefineFontNameTag) {
 			visit((DefineFontNameTag) tag, visitor);
 		}
+		else if (tag instanceof DefineMorphShapeTag) {
+			visit((DefineMorphShapeTag) tag, visitor);
+		}
+		else if (tag instanceof DefineMorphShape2Tag) {
+			visit((DefineMorphShape2Tag) tag, visitor);
+		}
+		else if (tag instanceof DefineScalingGridTag) {
+			visit((DefineScalingGridTag) tag, visitor);
+		}
+		else if (tag instanceof DefineSceneAndFrameLabelDataTag) {
+			visit((DefineSceneAndFrameLabelDataTag) tag, visitor);
+		}
+		else if (tag instanceof DefineShapeTag) {
+			visit((DefineShapeTag) tag, visitor);
+		}
+		else if (tag instanceof DefineShape2Tag) {
+			visit((DefineShape2Tag) tag, visitor);
+		}
+		else if (tag instanceof DefineShape3Tag) {
+			visit((DefineShape3Tag) tag, visitor);
+		}
+		else if (tag instanceof DefineShape4Tag) {
+			visit((DefineShape4Tag) tag, visitor);
+		}
+		else if (tag instanceof DefineSoundTag) {
+			visit((DefineSoundTag) tag, visitor);
+		}
+		else if (tag instanceof DefineSpriteTag) {
+			visit((DefineSpriteTag) tag, visitor);
+		}
 		else if (tag instanceof DefineTextTag) {
 			visit((DefineTextTag) tag, visitor);
 		}
 		else if (tag instanceof DefineText2Tag) {
 			visit((DefineText2Tag) tag, visitor);
 		}
+		else if (tag instanceof DefineVideoStreamTag) {
+			visit((DefineVideoStreamTag) tag, visitor);
+		}
+		else if (tag instanceof DoABCTag) {
+			visit((DoABCTag) tag, visitor);
+		}
+		else if (tag instanceof DoActionTag) {
+			visit((DoActionTag) tag, visitor);
+		}
 		else if (tag instanceof DoInitActionTag) {
 			visit((DoInitActionTag) tag, visitor);
 		}
-		else if (tag instanceof CSMTextSettingsTag) {
-			visit((CSMTextSettingsTag) tag, visitor);
+		else if (tag instanceof EnableDebuggerTag) {
+			visit((EnableDebuggerTag) tag, visitor);
 		}
-		else if (tag instanceof DefineFontInfoTag) {
-			visit((DefineFontInfoTag) tag, visitor);
+		else if (tag instanceof EnableDebugger2Tag) {
+			visit((EnableDebugger2Tag) tag, visitor);
 		}
-		else if (tag instanceof DefineFontInfo2Tag) {
-			visit((DefineFontInfo2Tag) tag, visitor);
+		else if (tag instanceof EndTag) {
+			visit((EndTag) tag, visitor);
 		}
-		else if (tag instanceof DefineBitsLosslessTag) {
-			visit((DefineBitsLosslessTag) tag, visitor);
+		else if (tag instanceof ExportAssetsTag) {
+			visit((ExportAssetsTag) tag, visitor);
 		}
-		else if (tag instanceof DefineBitsLossless2Tag) {
-			visit((DefineBitsLossless2Tag) tag, visitor);
+		else if (tag instanceof FileAttributesTag) {
+			visit((FileAttributesTag) tag, visitor);
 		}
-		else if (tag instanceof DefineButton2Tag) {
-			visit((DefineButton2Tag) tag, visitor);
+		else if (tag instanceof FrameLabelTag) {
+			visit((FrameLabelTag) tag, visitor);
+		}
+		else if (tag instanceof ImportAssetsTag) {
+			visit((ImportAssetsTag) tag, visitor);
+		}
+		else if (tag instanceof ImportAssets2Tag) {
+			visit((ImportAssets2Tag) tag, visitor);
 		}
 		else if (tag instanceof JPEGTablesTag) {
 			visit((JPEGTablesTag) tag, visitor);
 		}
-		else if (tag instanceof DefineBitsTag) {
-			visit((DefineBitsTag) tag, visitor);
+		else if (tag instanceof MetadataTag) {
+			visit((MetadataTag) tag, visitor);
 		}
-		else if (tag instanceof DefineBitsJPEG2Tag) {
-			visit((DefineBitsJPEG2Tag) tag, visitor);
+		else if (tag instanceof PlaceObject2Tag) {
+			visit((PlaceObject2Tag) tag, visitor);
 		}
-		else if (tag instanceof DefineBitsJPEG3Tag) {
-			visit((DefineBitsJPEG3Tag) tag, visitor);
+		else if (tag instanceof PlaceObject3Tag) {
+			visit((PlaceObject3Tag) tag, visitor);
 		}
 		else if (tag instanceof ProtectTag) {
 			visit((ProtectTag) tag, visitor);
 		}
+		else if (tag instanceof RemoveObjectTag) {
+			visit((RemoveObjectTag) tag, visitor);
+		}
+		else if (tag instanceof RemoveObject2Tag) {
+			visit((RemoveObject2Tag) tag, visitor);
+		}
+		else if (tag instanceof ScriptLimitsTag) {
+			visit((ScriptLimitsTag) tag, visitor);
+		}
+		else if (tag instanceof SetBackgroundColorTag) {
+			visit((SetBackgroundColorTag) tag, visitor);
+		}
+		else if (tag instanceof SetTabIndexTag) {
+			visit((SetTabIndexTag) tag, visitor);
+		}
+		else if (tag instanceof ShowFrameTag) {
+			visit((ShowFrameTag) tag, visitor);
+		}
+		else if (tag instanceof SoundStreamBlockTag) {
+			visit((SoundStreamBlockTag) tag, visitor);
+		}
 		else if (tag instanceof SoundStreamHeadTag) {
 			visit((SoundStreamHeadTag) tag, visitor);
 		}
 		else if (tag instanceof SoundStreamHead2Tag) {
 			visit((SoundStreamHead2Tag) tag, visitor);
 		}
-		else if (tag instanceof DefineMorphShapeTag) {
-			visit((DefineMorphShapeTag) tag, visitor);
-		}
-		else if (tag instanceof DefineSoundTag) {
-			visit((DefineSoundTag) tag, visitor);
-		}
 		else if (tag instanceof StartSoundTag) {
 			visit((StartSoundTag) tag, visitor);
 		}
-		else if (tag instanceof SoundStreamBlockTag) {
-			visit((SoundStreamBlockTag) tag, visitor);
+		else if (tag instanceof StartSound2Tag) {
+			visit((StartSound2Tag) tag, visitor);
+		}
+		else if (tag instanceof SymbolClassTag) {
+			visit((SymbolClassTag) tag, visitor);
+		}
+		else if (tag instanceof VideoFrameTag) {
+			visit((VideoFrameTag) tag, visitor);
 		}
 		else {
 			System.out.println(tag);
@@ -2584,6 +3667,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final TagList tags, final ISWFVisitor visitor) {
+
+		if (tags == null) {
+			return;
+		}
+
 		for (final Tag tag : tags) {
 			visitor.visit(tags, tag);
 
@@ -2599,6 +3687,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final TextRecord textRecord, final ISWFVisitor visitor) {
 
+		if (textRecord == null) {
+			return;
+		}
+
 		visitor.visit(textRecord, "TextRecordType", textRecord.getTextRecordType());
 		visitor.visit(textRecord, "StyleFlagsReserved", textRecord.getStyleFlagsReserved());
 		visitor.visit(textRecord, "StyleFlagsHasFont", textRecord.getStyleFlagsHasFont());
@@ -2624,6 +3716,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final TextRecord2 textRecord, final ISWFVisitor visitor) {
 
+		if (textRecord == null) {
+			return;
+		}
+
 		visitor.visit(textRecord, "TextRecordType", textRecord.getTextRecordType());
 		visitor.visit(textRecord, "StyleFlagsReserved", textRecord.getStyleFlagsReserved());
 		visitor.visit(textRecord, "StyleFlagsHasFont", textRecord.getStyleFlagsHasFont());
@@ -2648,6 +3744,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final TextRecord2List textRecords, final ISWFVisitor visitor) {
+
+		if (textRecords == null) {
+			return;
+		}
+
 		for (final TextRecord2 textRecord : textRecords) {
 			visitor.visit(textRecords, "TextRecord", textRecord);
 			visit(textRecord, visitor);
@@ -2661,6 +3762,11 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final TextRecordList textRecords, final ISWFVisitor visitor) {
+
+		if (textRecords == null) {
+			return;
+		}
+
 		for (final TextRecord textRecord : textRecords) {
 			visitor.visit(textRecords, "TextRecord", textRecord);
 			visit(textRecord, visitor);
@@ -2674,11 +3780,33 @@ public final class SWFIterator {
 	 * @param visitor The visitor that is invoked for all elements of the visited object.
 	 */
 	public static void visit(final UINT16List list, final ISWFVisitor visitor) {
+
+		if (list == null) {
+			return;
+		}
+
 		for (final UINT16 element : list) {
 			visitor.visit(list, "Element", element);
 		}
 	}
 
+	/**
+	 * Visits a VideoFrameTag object.
+	 * 
+	 * @param tag The visited object.
+	 * @param visitor The visitor that is invoked for all elements of the visited object.
+	 */
+	public static void visit(final VideoFrameTag tag, final ISWFVisitor visitor) {
+
+		if (tag == null) {
+			return;
+		}
+
+		visitor.visit(tag, "StreamID", tag.getStreamId());
+		visitor.visit(tag, "FrameNum", tag.getFrameNum());
+		visitor.visit(tag, "VideoData", tag.getVideoData());
+	}
+
 	/**
 	 * Visits a ZoneData object.
 	 * 
@@ -2687,6 +3815,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final ZoneData zoneData, final ISWFVisitor visitor) {
 
+		if (zoneData == null) {
+			return;
+		}
+
 		visitor.visit(zoneData, "AlignmentCoordinate", zoneData.getAlignmentCoordinate());
 		visitor.visit(zoneData, "Range", zoneData.getRange());
 	}
@@ -2699,6 +3831,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final ZoneDataList zoneDataList, final ISWFVisitor visitor) {
 
+		if (zoneDataList == null) {
+			return;
+		}
+
 		for (final ZoneData zoneData : zoneDataList) {
 
 			visitor.visit(zoneDataList, "ZoneData", zoneData);
@@ -2714,6 +3850,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final ZoneRecord zoneRecord, final ISWFVisitor visitor) {
 
+		if (zoneRecord == null) {
+			return;
+		}
+
 		visitor.visit(zoneRecord, "NumZoneData", zoneRecord.getNumZoneData());
 		visitor.visit(zoneRecord, "ZoneData", zoneRecord.getZoneData());
 		visit(zoneRecord.getZoneData(), visitor);
@@ -2730,6 +3870,10 @@ public final class SWFIterator {
 	 */
 	public static void visit(final ZoneRecordList zoneRecordList, final ISWFVisitor visitor) {
 
+		if (zoneRecordList == null) {
+			return;
+		}
+
 		for (final ZoneRecord zoneRecord : zoneRecordList) {
 
 			visitor.visit(zoneRecordList, "ZoneRecord", zoneRecord);
