@@ -658,6 +658,18 @@ public final class AS2CodePrinter {
 		}.visit(instruction);
 	}
 
+	/**
+	 * Generates a printable string that represents all code in a given
+	 * action list.
+	 * 
+	 * @param code The list that contains the ActionScript 2 code.
+	 * 
+	 * @return The generated ActionScript 2 code string.
+	 */
+	public static String getCodeText(final ActionList code) {
+		return getCodeText(code, "");
+	}
+
 	/**
 	 * Generates a printable string that represents all code in a given
 	 * action list.
@@ -667,7 +679,7 @@ public final class AS2CodePrinter {
 	 * 
 	 * @return The generated ActionScript 2 code string.
 	 */
-	private static String getCodeText(final ActionList code, final String padding) {
+	public static String getCodeText(final ActionList code, final String padding) {
 		if (code.size() == 0) {
 			return "";
 		}
@@ -691,16 +703,4 @@ public final class AS2CodePrinter {
 
 		return sb.toString();
 	}
-
-	/**
-	 * Generates a printable string that represents all code in a given
-	 * action list.
-	 * 
-	 * @param code The list that contains the ActionScript 2 code.
-	 * 
-	 * @return The generated ActionScript 2 code string.
-	 */
-	public static String getCodeText(final ActionList code) {
-		return getCodeText(code, "");
-	}
 }
\ No newline at end of file
