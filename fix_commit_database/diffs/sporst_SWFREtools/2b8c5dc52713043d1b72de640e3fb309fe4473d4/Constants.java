@@ -8,5 +8,5 @@ public final class Constants {
 	/**
 	 * Flash Dissector version string.
 	 */
-	public static final String VERSION = "1.0.0";
+	public static final String VERSION = "1.3.0";
 }
