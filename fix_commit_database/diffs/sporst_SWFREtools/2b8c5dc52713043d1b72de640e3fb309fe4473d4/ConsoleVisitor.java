@@ -18,52 +18,102 @@ import tv.porst.splib.binaryparser.UINT32;
 import tv.porst.splib.binaryparser.UINT8;
 import tv.porst.splib.strings.StringHelpers;
 import tv.porst.swfretools.dissector.Constants;
+import tv.porst.swfretools.dissector.gui.main.panels.AS2CodePrinter;
+import tv.porst.swfretools.dissector.gui.main.panels.AS3CodePrinter;
 import tv.porst.swfretools.parser.actions.as2.Action;
 import tv.porst.swfretools.parser.structures.*;
 import tv.porst.swfretools.parser.tags.Tag;
 import tv.porst.swfretools.utils.ISWFVisitor;
 import tv.porst.swfretools.utils.TagNames;
+import tv.porst.swfretools.utils.as3.ActionScript3Resolver;
 
-public class ConsoleVisitor implements ISWFVisitor {
+/**
+ * Visitor class that dumps all elements of a SWF file to stdout.
+ */
+public final class ConsoleVisitor implements ISWFVisitor {
 
+	/**
+	 * File that is processed.
+	 */
 	private final File file;
 
+	/**
+	 * Output target.
+	 */
 	private final PrintStream output = System.out;
 
+	/**
+	 * Stack that keeps track of the element traversal hierarchy.
+	 */
 	private final Stack<Object> stack = new Stack<Object>();
 
+	/**
+	 * Creates a new console visitor object.
+	 * 
+	 * @param file File that is processed.
+	 */
 	public ConsoleVisitor(final File file) {
+
+		if (file == null) {
+			throw new IllegalArgumentException("File argument must not be null.");
+		}
+
 		this.file = file;
 	}
 
+	/**
+	 * Returns the necessary padding before lines depending on the size of the
+	 * traversal stack.
+	 * 
+	 * @return The necessary padding string.
+	 */
 	private String getPadding() {
 		return StringHelpers.repeat("  ", stack.size() - 1);
 	}
 
-	private void handleList(final Object parent, final String name, final ElementList<?> value) {
+	/**
+	 * Processes a file element.
+	 * 
+	 * @param parent Parent element of the list.
+	 * @param name Name of the file element.
+	 * @param value The file element to process.
+	 */
+	private void handleElement(final Object parent, final String name, final IFileElement value) {
 
-		if (value.size() != 0) {
+		if (value != null) {
 
 			updateStack(parent);
 
-			output.printf(getPadding() + "[%08X:%d]: %s (%d elements)\n", value.getBitPosition(), 0, name, value.size());
+			output.printf(getPadding() + "[%08X:%d]: %s\n", value.getBitPosition() / 8, 0, name);
 
 			stack.push(value);
 		}
 	}
 
-	private void handleStructure(final Object parent, final String name, final IFileElement value) {
+	/**
+	 * Processes a list.
+	 * 
+	 * @param parent Parent element of the list.
+	 * @param name Name of the list.
+	 * @param value The list to process.
+	 */
+	private void handleList(final Object parent, final String name, final ElementList<?> value) {
 
-		if (value != null) {
+		if (value.size() != 0) {
 
 			updateStack(parent);
 
-			output.printf(getPadding() + "[%08X:%d]: %s\n", value.getBitPosition(), 0, name);
+			output.printf(getPadding() + "[%08X:%d]: %s (%d elements)\n", value.getBitPosition() / 8, 0, name, value.size());
 
 			stack.push(value);
 		}
 	}
 
+	/**
+	 * Updates the stack with a new parent element.
+	 * 
+	 * @param parent The new parent element.
+	 */
 	private void updateStack(final Object parent) {
 
 		if (stack.contains(parent)) {
@@ -77,41 +127,50 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 	@Override
 	public void visit(final Object parent, final String name, final Action value) {
-		handleStructure(parent, name, value);
+		// Do nothing
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final ActionList value) {
 		handleList(parent, name, value);
+
+		output.println(AS2CodePrinter.getCodeText(value, getPadding()));
+	}
+
+	@Override
+	public void visit(final Object parent, final String name, final AS3Data value) {
+		handleElement(parent, name, value);
+
+		output.println(AS3CodePrinter.getCodeText(ActionScript3Resolver.resolve(value)));
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final AsciiString value) {
 		if (value != null) {
-			output.printf(getPadding() + "[%08X:%d]: %s : %s\n", value.getBitPosition(), 0, name, value.value());
+			output.printf(getPadding() + "[%08X:%d]: %s : %s\n", value.getBitPosition() / 8, 0, name, value.value());
 		}
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final BevelFilter value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final Bits value) {
 		if (value != null) {
-			output.printf(getPadding() + "[%08X:%d]: %s : %d\n", value.getBitPosition(), value.getBitPosition(), name, value.value());
+			output.printf(getPadding() + "[%08X:%d]: %s : %d\n", value.getBitPosition() / 8, value.getBitPosition() % 8, name, value.value());
 		}
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final BlurFilter value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final ButtonCondAction value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -119,9 +178,14 @@ public class ConsoleVisitor implements ISWFVisitor {
 		handleList(parent, name, value);
 	}
 
+	@Override
+	public void visit(final Object parent, final String name, final ButtonRecord value) {
+		handleElement(parent, name, value);
+	}
+
 	@Override
 	public void visit(final Object parent, final String name, final ButtonRecord2 value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -129,16 +193,21 @@ public class ConsoleVisitor implements ISWFVisitor {
 		handleList(parent, name, value);
 	}
 
+	@Override
+	public void visit(final Object parent, final String name, final ButtonRecordList value) {
+		handleList(parent, name, value);
+	}
+
 	@Override
 	public void visit(final Object parent, final String name, final ByteArray value) {
 		if (value != null) {
-			output.printf(getPadding() + "[%08X:%d]: %s : %s\n", value.getBitPosition(), 0, name, "ARRAY");
+			output.printf(getPadding() + "[%08X:%d]: %s : %s\n", value.getBitPosition() / 8, 0, name, "ARRAY");
 		}
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final ClipActionRecord value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -148,57 +217,69 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 	@Override
 	public void visit(final Object parent, final String name, final ClipActions value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final ClipEventFlags value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final ColorMatrixFilter value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final ConvolutionFilter value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final CurvedEdgeRecord value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
+	}
+
+	@Override
+	public void visit(final Object parent, final String name, final CxForm value) {
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final CxFormWithAlpha value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final DropShadowFilter value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
+	}
+
+	@Override
+	public void visit(final Object parent, final String name, final EncodedU32 value) {
+		if (value != null) {
+			output.printf(getPadding() + "[%08X:%d]: %s : %d\n", value.getBitPosition() / 8, 0, name, value.value());
+		}
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final EndShapeRecord value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final FillStyle value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final FillStyle3 value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final FillStyle3Array value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -208,7 +289,7 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 	@Override
 	public void visit(final Object parent, final String name, final FillStyleArray value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -218,60 +299,75 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 	@Override
 	public void visit(final Object parent, final String name, final Filter value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final FilterList value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final Fixed value) {
 		if (value != null) {
-			output.printf(getPadding() + "[%08X:%d]: %s : %d\n", value.getBitPosition(), 0, name, value.value());
+			output.printf(getPadding() + "[%08X:%d]: %s : %d\n", value.getBitPosition() / 8, 0, name, value.value());
 		}
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final Fixed8 value) {
-		output.printf(getPadding() + "[%08X:%d]: %s : %b\n", value.getBitPosition(), 0, name, value.value());
+		output.printf(getPadding() + "[%08X:%d]: %s : %b\n", value.getBitPosition() / 8,  0, name, value.value());
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final Flag value) {
 		if (value != null) {
-			output.printf(getPadding() + "[%08X:%d]: %s : %b\n", value.getBitPosition(), value.getBitPosition(), name, value.value());
+			output.printf(getPadding() + "[%08X:%d]: %s : %b\n", value.getBitPosition() / 8,  value.getBitPosition() % 8,  name, value.value());
 		}
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final Float16 value) {
 		if (value != null) {
-			output.printf(getPadding() + "[%08X:%d]: %s : %f\n", value.getBitPosition(), 0, name, value.value());
+			output.printf(getPadding() + "[%08X:%d]: %s : %f\n", value.getBitPosition() / 8,  0, name, value.value());
 		}
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final Float32 value) {
 		if (value != null) {
-			output.printf(getPadding() + "[%08X:%d]: %s : %f\n", value.getBitPosition(), 0, name, value.value());
+			output.printf(getPadding() + "[%08X:%d]: %s : %f\n", value.getBitPosition() / 8,  0, name, value.value());
 		}
 	}
 
+	@Override
+	public void visit(final Object parent, final String name, final Float32List value) {
+		handleList(parent, name, value);
+	}
+
 	@Override
 	public void visit(final Object parent, final String name, final FocalGradient value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
+	}
+
+	@Override
+	public void visit(final Object parent, final String name, final FrameLabel value) {
+		handleElement(parent, name, value);
+	}
+
+	@Override
+	public void visit(final Object parent, final String name, final FrameLabelList value) {
+		handleList(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final GlowFilter value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final GlyphEntry value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -281,32 +377,32 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 	@Override
 	public void visit(final Object parent, final String name, final Gradient value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final Gradient3 value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final GradientBevelFilter value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final GradientGlowFilter value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final GradRecord value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final GradRecord3 value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -322,7 +418,7 @@ public class ConsoleVisitor implements ISWFVisitor {
 	@Override
 	public void visit(final Object parent, final String name, final INT16 value) {
 		if (value != null) {
-			output.printf(getPadding() + "[%08X:%d]: %s : %d\n", value.getBitPosition(), 0, name, value.value());
+			output.printf(getPadding() + "[%08X:%d]: %s : %d\n", value.getBitPosition() / 8,  0, name, value.value());
 		}
 	}
 
@@ -332,7 +428,7 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 			updateStack(parent);
 
-			output.printf(getPadding() + "[%08X:%d]: %s (%d elements)\n", value.getBitPosition(), 0, name, value.size());
+			output.printf(getPadding() + "[%08X:%d]: %s (%d elements)\n", value.getBitPosition() / 8,  0, name, value.size());
 
 			stack.push(value);
 		}
@@ -341,13 +437,13 @@ public class ConsoleVisitor implements ISWFVisitor {
 	@Override
 	public void visit(final Object parent, final String name, final INT32 value) {
 		if (value != null) {
-			output.printf(getPadding() + "[%08X:%d]: %s : %d\n", value.getBitPosition(), 0, name, value.value());
+			output.printf(getPadding() + "[%08X:%d]: %s : %d\n", value.getBitPosition() / 8,  0, name, value.value());
 		}
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final KerningRecord value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -357,17 +453,17 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 	@Override
 	public void visit(final Object parent, final String name, final LineStyle value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final LineStyle3 value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final LineStyle3Array value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -377,12 +473,12 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 	@Override
 	public void visit(final Object parent, final String name, final LineStyle4 value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final LineStyle4Array value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -392,7 +488,7 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 	@Override
 	public void visit(final Object parent, final String name, final LineStyleArray value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -402,17 +498,17 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 	@Override
 	public void visit(final Object parent, final String name, final Matrix value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final MorphFillStyle value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final MorphFillStyleArray value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -422,12 +518,12 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 	@Override
 	public void visit(final Object parent, final String name, final MorphGradient value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final MorphGradientRecord value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -437,12 +533,27 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 	@Override
 	public void visit(final Object parent, final String name, final MorphLineStyle value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
+	}
+
+	@Override
+	public void visit(final Object parent, final String name, final MorphLineStyle2 value) {
+		handleElement(parent, name, value);
+	}
+
+	@Override
+	public void visit(final Object parent, final String name, final MorphLineStyle2List value) {
+		handleList(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final MorphLineStyleArray value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
+	}
+
+	@Override
+	public void visit(final Object parent, final String name, final MorphLineStyleArray2 value) {
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -456,7 +567,19 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 			updateStack(parent);
 
-			output.printf(getPadding() + "[%08X:%d]: %s (%d elements)\n", value.getBitPosition(), 0, name, value.size());
+			output.printf(getPadding() + "[%08X:%d]: %s (%d elements)\n", value.getBitPosition() / 8,  0, name, value.size());
+
+			stack.push(value);
+		}
+	}
+
+	@Override
+	public void visit(final Object parent, final String name, final RecordHeader value) {
+		if (value != null) {
+
+			updateStack(parent);
+
+			output.printf(getPadding() + "[%08X:%d]: %s (Code: %d Length: %d)\n", value.getBitPosition() / 8, 0, name, value.getTagCode(), value.getLength());
 
 			stack.push(value);
 		}
@@ -491,17 +614,32 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 	@Override
 	public void visit(final Object parent, final String name, final RGBA value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
+	}
+
+	@Override
+	public void visit(final Object parent, final String name, final RGBAList value) {
+		handleList(parent, name, value);
+	}
+
+	@Override
+	public void visit(final Object parent, final String name, final SceneName value) {
+		handleElement(parent, name, value);
+	}
+
+	@Override
+	public void visit(final Object parent, final String name, final SceneNameList value) {
+		handleList(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final Shape value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final Shape3 value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -511,7 +649,7 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 	@Override
 	public void visit(final Object parent, final String name, final Shape3Record value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -526,7 +664,7 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 	@Override
 	public void visit(final Object parent, final String name, final ShapeRecord value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -536,17 +674,17 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 	@Override
 	public void visit(final Object parent, final String name, final ShapeWithStyle value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final ShapeWithStyle3 value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final ShapeWithStyle4 value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -556,7 +694,7 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 	@Override
 	public void visit(final Object parent, final String name, final SoundEnvelope value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -566,35 +704,39 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 	@Override
 	public void visit(final Object parent, final String name, final SoundInfo value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final StraightEdgeRecord value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final StyleChangeRecord value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final StyleChangeRecord3 value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final StyleChangeRecord4 value) {
-		// TODO Auto-generated method stub
+		handleElement(parent, name, value);
+	}
 
+	@Override
+	public void visit(final Object parent, final String name, final Symbol value) {
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final SymbolList value) {
 		updateStack(parent);
 
-		output.printf(getPadding() + "[%08X:%d]: %s (%d tags)\n", value.getBitPosition(), 0, name, value.size());
+		output.printf(getPadding() + "[%08X:%d]: %s (%d tags)\n", value.getBitPosition() / 8,  0, name, value.size());
 
 		stack.push(value);
 	}
@@ -603,19 +745,19 @@ public class ConsoleVisitor implements ISWFVisitor {
 	public void visit(final Object parent, final String name, final TagList value) {
 		updateStack(parent);
 
-		output.printf(getPadding() + "[%08X:%d]: %s (%d tags)\n", value.getBitPosition(), 0, name, value.size());
+		output.printf(getPadding() + "[%08X:%d]: %s (%d tags)\n", value.getBitPosition() / 8,  0, name, value.size());
 
 		stack.push(value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final TextRecord value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final TextRecord2 value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -631,14 +773,14 @@ public class ConsoleVisitor implements ISWFVisitor {
 	@Override
 	public void visit(final Object parent, final String name, final UBits value) {
 		if (value != null) {
-			output.printf(getPadding() + "[%08X:%d]: %s : %d\n", value.getBitPosition(), value.getBitPosition(), name, value.value());
+			output.printf(getPadding() + "[%08X:%d]: %s : %d\n", value.getBitPosition() / 8,  value.getBitPosition() % 8,  name, value.value());
 		}
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final UINT16 value) {
 		if (value != null) {
-			output.printf(getPadding() + "[%08X:%d]: %s : %d\n", value.getBitPosition(), 0, name, value.value());
+			output.printf(getPadding() + "[%08X:%d]: %s : %d\n", value.getBitPosition() / 8,  0, name, value.value());
 		}
 	}
 
@@ -648,7 +790,7 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 			updateStack(parent);
 
-			output.printf(getPadding() + "[%08X:%d]: %s (%d elements)\n", value.getBitPosition(), 0, name, value.size());
+			output.printf(getPadding() + "[%08X:%d]: %s (%d elements)\n", value.getBitPosition() / 8,  0, name, value.size());
 
 			stack.push(value);
 		}
@@ -657,20 +799,25 @@ public class ConsoleVisitor implements ISWFVisitor {
 	@Override
 	public void visit(final Object parent, final String name, final UINT32 value) {
 		if (value != null) {
-			output.printf(getPadding() + "[%08X:%d]: %s : %d\n", value.getBitPosition(), 0, name, value.value());
+			output.printf(getPadding() + "[%08X:%d]: %s : %d\n", value.getBitPosition() / 8,  0, name, value.value());
 		}
 	}
 
 	@Override
 	public void visit(final Object parent, final String name, final UINT8 value) {
 		if (value != null) {
-			output.printf(getPadding() + "[%08X:%d]: %s : %d\n", value.getBitPosition(), 0, name, value.value());
+			output.printf(getPadding() + "[%08X:%d]: %s : %d\n", value.getBitPosition() / 8,  0, name, value.value());
 		}
 	}
 
+	@Override
+	public void visit(final Object parent, final String name, final UINT8List value) {
+		handleList(parent, name, value);
+	}
+
 	@Override
 	public void visit(final Object parent, final String name, final ZoneData value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -680,7 +827,7 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 	@Override
 	public void visit(final Object parent, final String name, final ZoneRecord value) {
-		handleStructure(parent, name, value);
+		handleElement(parent, name, value);
 	}
 
 	@Override
@@ -693,14 +840,15 @@ public class ConsoleVisitor implements ISWFVisitor {
 
 		updateStack(parent);
 
-		output.printf(getPadding() + "[%08X:%d]: %s\n", tag.getHeader().getBitPosition(), 0, TagNames.getPrintableTagName(tag.getHeader().getTagCode()));
+		output.printf(getPadding() + "[%08X:%d]: %s\n", tag.getHeader().getBitPosition() / 8, 0, TagNames.getPrintableTagName(tag.getHeader().getTagCode()));
 
 		stack.push(tag);
 	}
 
 	@Override
 	public void visit(final SWFFile file) {
-		output.println("Flash Dissector " + Constants.VERSION + " (http://www.the-interweb.com)");
+		output.println("Flash Dissector " + Constants.VERSION + " (http://www.github.com/sporst/SWFRETools)");
 		output.println("Dumping Flash file " + this.file.getAbsolutePath());
+		output.println();
 	}
 }
\ No newline at end of file
