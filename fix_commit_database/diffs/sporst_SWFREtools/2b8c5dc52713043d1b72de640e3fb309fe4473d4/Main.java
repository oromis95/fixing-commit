@@ -2,6 +2,7 @@ package tv.porst.swfretools.dissector;
 
 import javax.swing.JFrame;
 
+import tv.porst.swfretools.dissector.console.ConsoleDumper;
 import tv.porst.swfretools.dissector.gui.MainWindow;
 
 /**
@@ -16,6 +17,13 @@ public final class Main {
 	 */
 	public static void main(final String[] args) {
 
+		if (args.length == 2 && args[0].equals("-dump")) {
+
+			ConsoleDumper.dump(args[1]);
+
+			return;
+		}
+
 		final MainWindow window = new MainWindow();
 
 		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
