@@ -1,5 +1,9 @@
 package tv.porst.swfretools.dissector;
 
+import java.io.File;
+import java.util.ArrayList;
+import java.util.List;
+
 import javax.swing.JFrame;
 
 import tv.porst.swfretools.dissector.console.ConsoleDumper;
@@ -11,6 +15,26 @@ import tv.porst.swfretools.utils.Decompressor;
  */
 public final class Main {
 
+	/**
+	 * Determines whether all arguments refer to file names.
+	 * 
+	 * @param args The arguments to check.
+	 * 
+	 * @return True, if all arguments refer to file names. False, otherwise.
+	 */
+	private static boolean isAllFilenames(final String[] args) {
+
+		for (final String arg : args) {
+			final File file = new File(arg);
+
+			if (!file.exists()) {
+				return false;
+			}
+		}
+
+		return true;
+	}
+
 	/**
 	 * Main method of Flash Dissector.
 	 * 
@@ -18,6 +42,8 @@ public final class Main {
 	 */
 	public static void main(final String[] args) {
 
+		final List<String> filesToOpen = new ArrayList<String>();
+
 		if (args.length == 2 && args[0].equals("-dump")) {
 
 			ConsoleDumper.dump(args[1]);
@@ -30,6 +56,11 @@ public final class Main {
 
 			return;
 		}
+		else if (isAllFilenames(args)) {
+			for (final String filename : args) {
+				filesToOpen.add(filename);
+			}
+		}
 		else if (args.length != 0) {
 			System.out.println("Usage: dissector.jar options");
 			System.out.println();
@@ -38,7 +69,7 @@ public final class Main {
 			return;
 		}
 
-		final MainWindow window = new MainWindow();
+		final MainWindow window = new MainWindow(filesToOpen);
 
 		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 		window.setVisible(true);
