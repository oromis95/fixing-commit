@@ -3,9 +3,11 @@ package tv.porst.swfretools.dissector.gui.main.flashtree;
 import javax.swing.JTree;
 import javax.swing.SwingUtilities;
 
+import tv.porst.swfretools.dissector.gui.main.flashtree.menus.FlashAnyNodeMenu;
 import tv.porst.swfretools.dissector.gui.main.flashtree.menus.FlashFileNodeMenu;
 import tv.porst.swfretools.dissector.gui.main.flashtree.nodes.FlashFileNode;
 import tv.porst.swfretools.dissector.gui.main.flashtree.nodes.FlashTreeNode;
+import tv.porst.swfretools.dissector.gui.main.models.LoadedFile;
 
 /**
  * Contains methods for handling clicks on the tree that shows the SWF
@@ -27,8 +29,15 @@ public final class FlashTreeNodeClickHandler {
 			throw new IllegalArgumentException("Node argument must not be null.");
 		}
 
+		final LoadedFile loadedFile = FlashTreeHelpers.getLoadedFile(node);
+
 		if (node instanceof FlashFileNode) {
-			final FlashFileNodeMenu menu = new FlashFileNodeMenu(SwingUtilities.getWindowAncestor(tree), ((FlashFileNode)node).getUserObject().getLoadedFile());
+			final FlashFileNodeMenu menu = new FlashFileNodeMenu(SwingUtilities.getWindowAncestor(tree), loadedFile);
+
+			menu.show(tree, x, y);
+		}
+		else {
+			final FlashAnyNodeMenu menu = new FlashAnyNodeMenu(SwingUtilities.getWindowAncestor(tree), loadedFile, node);
 
 			menu.show(tree, x, y);
 		}
