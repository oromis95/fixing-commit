@@ -35,7 +35,7 @@ public final class ShapeWithStyle4 implements IFileElement {
 	/**
 	 * Shape records.
 	 */
-	private final ShapeRecord shapeRecord;
+	private final Shape4Record shapeRecord;
 
 	/**
 	 * Creates a new ShapeWithStyle object.
@@ -47,7 +47,7 @@ public final class ShapeWithStyle4 implements IFileElement {
 	 * @param shapeRecord Shape records.
 	 */
 	public ShapeWithStyle4(final FillStyle3Array fillStyles, final LineStyle4Array lineStyles,
-			final UBits numFillBits, final UBits numLineBits, final ShapeRecord shapeRecord) {
+			final UBits numFillBits, final UBits numLineBits, final Shape4Record shapeRecord) {
 
 		this.fillStyles = fillStyles;
 		this.lineStyles = lineStyles;
@@ -108,7 +108,7 @@ public final class ShapeWithStyle4 implements IFileElement {
 	 *
 	 * @return The shape records.
 	 */
-	public ShapeRecord getShapeRecord() {
+	public Shape4Record getShapeRecord() {
 		return shapeRecord;
 	}
 }
\ No newline at end of file
