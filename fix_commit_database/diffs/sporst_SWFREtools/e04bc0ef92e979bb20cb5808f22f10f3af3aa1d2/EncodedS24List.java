@@ -2,12 +2,12 @@ package tv.porst.swfretools.parser.actions.as3;
 
 import java.util.List;
 
+import tv.porst.splib.binaryparser.INT24;
 import tv.porst.swfretools.parser.structures.ElementList;
-import tv.porst.swfretools.parser.structures.EncodedS24;
 
-public class EncodedS24List extends ElementList<EncodedS24> {
+public class EncodedS24List extends ElementList<INT24> {
 
-	public EncodedS24List(final List<EncodedS24> elements) {
+	public EncodedS24List(final List<INT24> elements) {
 		super(elements);
 	}
 
