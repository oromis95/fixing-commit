@@ -19,8 +19,8 @@ public class ActionTry extends Action {
 	private final UINT16 trySize;
 	private final UINT16 catchSize;
 	private final UINT16 finallySize;
-	private final AsciiString functionName;
-	private final UINT8 registerCount;
+	private final AsciiString catchName;
+	private final UINT8 catchRegister;
 	private final ActionList tryBody;
 	private final ActionList catchBody;
 	private final ActionList finallyBody;
@@ -28,7 +28,7 @@ public class ActionTry extends Action {
 	public ActionTry(final UINT8 actionCode, final UINT16 length, final UBits reserved,
 			final Flag catchInRegisterFlag, final Flag finallyBlockFlag,
 			final Flag catchBlockFlag, final UINT16 trySize, final UINT16 catchSize,
-			final UINT16 finallySize, final AsciiString functionName, final UINT8 registerCount,
+			final UINT16 finallySize, final AsciiString catchName, final UINT8 catchRegister,
 			final ActionList tryBody, final ActionList catchBody,
 			final ActionList finallyBody) {
 		super(actionCode);
@@ -41,8 +41,8 @@ public class ActionTry extends Action {
 		this.trySize = trySize;
 		this.catchSize = catchSize;
 		this.finallySize = finallySize;
-		this.functionName = functionName;
-		this.registerCount = registerCount;
+		this.catchName = catchName;
+		this.catchRegister = catchRegister;
 		this.tryBody = tryBody;
 		this.catchBody = catchBody;
 		this.finallyBody = finallyBody;
@@ -51,8 +51,8 @@ public class ActionTry extends Action {
 	@Override
 	public int getBitLength() {
 		return SWFParserHelpers.addBitLengths(getActionCode(), getLength(), length, reserved, catchInRegisterFlag,
-				finallyBlockFlag, catchBlockFlag, trySize, catchSize, finallySize, functionName,
-				registerCount, tryBody, catchBody, finallyBody);
+				finallyBlockFlag, catchBlockFlag, trySize, catchSize, finallySize, catchName,
+				catchRegister, tryBody, catchBody, finallyBody);
 	}
 
 	/**
@@ -87,8 +87,8 @@ public class ActionTry extends Action {
 	 *
 	 * @return The
 	 */
-	public UINT16 getCatchSize() {
-		return catchSize;
+	public AsciiString getCatchName() {
+		return catchName;
 	}
 
 	/**
@@ -96,8 +96,8 @@ public class ActionTry extends Action {
 	 *
 	 * @return The
 	 */
-	public Flag getFinallyBlockFlag() {
-		return finallyBlockFlag;
+	public UINT8 getCatchRegister() {
+		return catchRegister;
 	}
 
 	/**
@@ -105,8 +105,8 @@ public class ActionTry extends Action {
 	 *
 	 * @return The
 	 */
-	public ActionList getFinallyBody() {
-		return finallyBody;
+	public UINT16 getCatchSize() {
+		return catchSize;
 	}
 
 	/**
@@ -114,8 +114,8 @@ public class ActionTry extends Action {
 	 *
 	 * @return The
 	 */
-	public UINT16 getFinallySize() {
-		return finallySize;
+	public Flag getFinallyBlockFlag() {
+		return finallyBlockFlag;
 	}
 
 	/**
@@ -123,8 +123,8 @@ public class ActionTry extends Action {
 	 *
 	 * @return The
 	 */
-	public AsciiString getFunctionName() {
-		return functionName;
+	public ActionList getFinallyBody() {
+		return finallyBody;
 	}
 
 	/**
@@ -132,8 +132,8 @@ public class ActionTry extends Action {
 	 *
 	 * @return The
 	 */
-	public UINT16 getLength() {
-		return length;
+	public UINT16 getFinallySize() {
+		return finallySize;
 	}
 
 	/**
@@ -141,8 +141,8 @@ public class ActionTry extends Action {
 	 *
 	 * @return The
 	 */
-	public UINT8 getRegisterCount() {
-		return registerCount;
+	public UINT16 getLength() {
+		return length;
 	}
 
 	/**
