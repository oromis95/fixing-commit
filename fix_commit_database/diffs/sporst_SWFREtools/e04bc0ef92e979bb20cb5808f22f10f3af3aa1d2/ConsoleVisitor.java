@@ -19,104 +19,7 @@ import tv.porst.splib.binaryparser.UINT8;
 import tv.porst.splib.strings.StringHelpers;
 import tv.porst.swfretools.Constants;
 import tv.porst.swfretools.parser.actions.as2.Action;
-import tv.porst.swfretools.parser.structures.ActionList;
-import tv.porst.swfretools.parser.structures.BevelFilter;
-import tv.porst.swfretools.parser.structures.BlurFilter;
-import tv.porst.swfretools.parser.structures.ButtonCondAction;
-import tv.porst.swfretools.parser.structures.ButtonCondActionList;
-import tv.porst.swfretools.parser.structures.ButtonRecord2;
-import tv.porst.swfretools.parser.structures.ButtonRecord2List;
-import tv.porst.swfretools.parser.structures.ByteArray;
-import tv.porst.swfretools.parser.structures.ClipActionRecord;
-import tv.porst.swfretools.parser.structures.ClipActionRecordList;
-import tv.porst.swfretools.parser.structures.ClipActions;
-import tv.porst.swfretools.parser.structures.ClipEventFlags;
-import tv.porst.swfretools.parser.structures.ColorMatrixFilter;
-import tv.porst.swfretools.parser.structures.ConvolutionFilter;
-import tv.porst.swfretools.parser.structures.CurvedEdgeRecord;
-import tv.porst.swfretools.parser.structures.CxFormWithAlpha;
-import tv.porst.swfretools.parser.structures.DropShadowFilter;
-import tv.porst.swfretools.parser.structures.ElementList;
-import tv.porst.swfretools.parser.structures.EndShapeRecord;
-import tv.porst.swfretools.parser.structures.FillStyle;
-import tv.porst.swfretools.parser.structures.FillStyle3;
-import tv.porst.swfretools.parser.structures.FillStyle3Array;
-import tv.porst.swfretools.parser.structures.FillStyle3List;
-import tv.porst.swfretools.parser.structures.FillStyleArray;
-import tv.porst.swfretools.parser.structures.FillStyleList;
-import tv.porst.swfretools.parser.structures.Filter;
-import tv.porst.swfretools.parser.structures.FilterList;
-import tv.porst.swfretools.parser.structures.Fixed;
-import tv.porst.swfretools.parser.structures.Fixed8;
-import tv.porst.swfretools.parser.structures.FocalGradient;
-import tv.porst.swfretools.parser.structures.GlowFilter;
-import tv.porst.swfretools.parser.structures.GlyphEntry;
-import tv.porst.swfretools.parser.structures.GlyphEntryList;
-import tv.porst.swfretools.parser.structures.GradRecord;
-import tv.porst.swfretools.parser.structures.GradRecord3;
-import tv.porst.swfretools.parser.structures.GradRecord3List;
-import tv.porst.swfretools.parser.structures.GradRecordList;
-import tv.porst.swfretools.parser.structures.Gradient;
-import tv.porst.swfretools.parser.structures.Gradient3;
-import tv.porst.swfretools.parser.structures.GradientBevelFilter;
-import tv.porst.swfretools.parser.structures.GradientGlowFilter;
-import tv.porst.swfretools.parser.structures.INT16List;
-import tv.porst.swfretools.parser.structures.KerningRecord;
-import tv.porst.swfretools.parser.structures.KerningRecordList;
-import tv.porst.swfretools.parser.structures.LineStyle;
-import tv.porst.swfretools.parser.structures.LineStyle3;
-import tv.porst.swfretools.parser.structures.LineStyle3Array;
-import tv.porst.swfretools.parser.structures.LineStyle3List;
-import tv.porst.swfretools.parser.structures.LineStyle4;
-import tv.porst.swfretools.parser.structures.LineStyle4Array;
-import tv.porst.swfretools.parser.structures.LineStyle4List;
-import tv.porst.swfretools.parser.structures.LineStyleArray;
-import tv.porst.swfretools.parser.structures.LineStyleList;
-import tv.porst.swfretools.parser.structures.Matrix;
-import tv.porst.swfretools.parser.structures.MorphFillStyle;
-import tv.porst.swfretools.parser.structures.MorphFillStyleArray;
-import tv.porst.swfretools.parser.structures.MorphFillStyleList;
-import tv.porst.swfretools.parser.structures.MorphGradient;
-import tv.porst.swfretools.parser.structures.MorphGradientRecord;
-import tv.porst.swfretools.parser.structures.MorphGradientRecordList;
-import tv.porst.swfretools.parser.structures.MorphLineStyle;
-import tv.porst.swfretools.parser.structures.MorphLineStyleArray;
-import tv.porst.swfretools.parser.structures.MorphLineStyleList;
-import tv.porst.swfretools.parser.structures.ParsedINTElementList;
-import tv.porst.swfretools.parser.structures.RGB;
-import tv.porst.swfretools.parser.structures.RGBA;
-import tv.porst.swfretools.parser.structures.Rect;
-import tv.porst.swfretools.parser.structures.RectList;
-import tv.porst.swfretools.parser.structures.SWFFile;
-import tv.porst.swfretools.parser.structures.Shape;
-import tv.porst.swfretools.parser.structures.Shape3;
-import tv.porst.swfretools.parser.structures.Shape3List;
-import tv.porst.swfretools.parser.structures.Shape3Record;
-import tv.porst.swfretools.parser.structures.Shape3RecordList;
-import tv.porst.swfretools.parser.structures.ShapeList;
-import tv.porst.swfretools.parser.structures.ShapeRecord;
-import tv.porst.swfretools.parser.structures.ShapeRecordList;
-import tv.porst.swfretools.parser.structures.ShapeWithStyle;
-import tv.porst.swfretools.parser.structures.ShapeWithStyle3;
-import tv.porst.swfretools.parser.structures.ShapeWithStyle4;
-import tv.porst.swfretools.parser.structures.SingleFilterList;
-import tv.porst.swfretools.parser.structures.SoundEnvelope;
-import tv.porst.swfretools.parser.structures.SoundEnvelopeList;
-import tv.porst.swfretools.parser.structures.SoundInfo;
-import tv.porst.swfretools.parser.structures.StraightEdgeRecord;
-import tv.porst.swfretools.parser.structures.StyleChangeRecord;
-import tv.porst.swfretools.parser.structures.StyleChangeRecord3;
-import tv.porst.swfretools.parser.structures.SymbolList;
-import tv.porst.swfretools.parser.structures.TagList;
-import tv.porst.swfretools.parser.structures.TextRecord;
-import tv.porst.swfretools.parser.structures.TextRecord2;
-import tv.porst.swfretools.parser.structures.TextRecord2List;
-import tv.porst.swfretools.parser.structures.TextRecordList;
-import tv.porst.swfretools.parser.structures.UINT16List;
-import tv.porst.swfretools.parser.structures.ZoneData;
-import tv.porst.swfretools.parser.structures.ZoneDataList;
-import tv.porst.swfretools.parser.structures.ZoneRecord;
-import tv.porst.swfretools.parser.structures.ZoneRecordList;
+import tv.porst.swfretools.parser.structures.*;
 import tv.porst.swfretools.parser.tags.Tag;
 import tv.porst.swfretools.utils.ISWFVisitor;
 import tv.porst.swfretools.utils.TagNames;
@@ -613,6 +516,12 @@ public class ConsoleVisitor implements ISWFVisitor {
 		handleStructure(parent, name, value);
 	}
 
+	@Override
+	public void visit(final Object parent, final String name, final StyleChangeRecord4 value) {
+		// TODO Auto-generated method stub
+
+	}
+
 	@Override
 	public void visit(final Object parent, final String name, final SymbolList value) {
 		updateStack(parent);
