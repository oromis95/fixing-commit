@@ -1,10 +1,15 @@
 package tv.porst.swfretools.parser.tags;
 
 import static tv.porst.swfretools.parser.SWFParserHelpers.parseUINT16;
+
+import java.util.List;
+
 import tv.porst.splib.binaryparser.UINT16;
 import tv.porst.swfretools.parser.SWFBinaryParser;
 import tv.porst.swfretools.parser.SWFParserException;
+import tv.porst.swfretools.parser.actions.as2.Action;
 import tv.porst.swfretools.parser.actions.as2.ActionRecordParser;
+import tv.porst.swfretools.parser.structures.ActionList;
 import tv.porst.swfretools.parser.structures.RecordHeader;
 
 
@@ -29,8 +34,8 @@ public final class DoInitActionParser {
 
 		final UINT16 spriteId = parseUINT16(parser, 0x00006, "DoInitAction::SpriteId");
 
-		ActionRecordParser.parse(parser, header.getNormalizedLength() - 2, "DoInitAction::Actions");
+		final List<Action> actions = ActionRecordParser.parse(parser, header.getNormalizedLength() - 2, "DoInitAction::Actions");
 
-		return new DoInitActionTag(header, spriteId);
+		return new DoInitActionTag(header, spriteId, new ActionList(actions));
 	}
 }
\ No newline at end of file
