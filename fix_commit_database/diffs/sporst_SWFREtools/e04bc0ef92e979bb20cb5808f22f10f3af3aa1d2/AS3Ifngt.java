@@ -1,14 +1,14 @@
 package tv.porst.swfretools.parser.actions.as3;
 
+import tv.porst.splib.binaryparser.INT24;
 import tv.porst.splib.binaryparser.UINT8;
 import tv.porst.swfretools.parser.SWFParserHelpers;
-import tv.porst.swfretools.parser.structures.EncodedS24;
 
 public class AS3Ifngt extends AS3Instruction {
 
-	private final EncodedS24 offset;
+	private final INT24 offset;
 
-	public AS3Ifngt(final UINT8 opcode, final EncodedS24 offset) {
+	public AS3Ifngt(final UINT8 opcode, final INT24 offset) {
 		super(opcode);
 
 		this.offset = offset;
@@ -24,7 +24,7 @@ public class AS3Ifngt extends AS3Instruction {
 	 *
 	 * @return The
 	 */
-	public EncodedS24 getOffset() {
+	public INT24 getOffset() {
 		return offset;
 	}
 
