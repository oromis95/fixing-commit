@@ -0,0 +1,22 @@
+package tv.porst.swfretools.parser.tags;
+
+import tv.porst.swfretools.parser.structures.RecordHeader;
+
+/**
+ * Represents a DoAction tag.
+ * 
+ * @author sp
+ *
+ */
+public final class DoActionTag extends Tag {
+
+	/**
+	 * Creates a new DoAction tag object.
+	 * 
+	 * @param header Tag header.
+	 */
+	public DoActionTag(final RecordHeader header) {
+		super(header);
+
+	}
+}
\ No newline at end of file
