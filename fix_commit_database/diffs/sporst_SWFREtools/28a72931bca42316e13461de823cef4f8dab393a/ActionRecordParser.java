@@ -1,8 +1,10 @@
 package tv.porst.swfretools.parser.actions;
 
+import java.util.ArrayList;
 import java.util.List;
 
 import tv.porst.splib.binaryparser.BinaryParser;
+import tv.porst.splib.binaryparser.BinaryParserHelpers;
 
 /**
  * Class responsible for parsing actions.
@@ -21,8 +23,10 @@ public final class ActionRecordParser {
 	 * @return The parsed actions.
 	 */
 	public static List<Action> parse(final BinaryParser parser, final long actionRecordSize) {
-		// TODO Auto-generated method stub
-		return null;
+
+		BinaryParserHelpers.readByteArray(parser, (int) actionRecordSize);
+
+		return new ArrayList<Action>();
 	}
 
 }
