@@ -29,16 +29,20 @@ public final class ShapeRecordParser {
 		final UBits first6 = parser.peekBits(6);
 
 		if (first6.value() == 0) {
+			System.out.println(String.format("%X: End", parser.getBytePosition()));
 			return EndShapeRecordParser.parse(parser, fieldName);
 		}
-		else if ((first6.value() & 0x20) == 0x20) {
-			return StyleChangeRecordParser.parse(parser, fillBits, lineBits, fieldName);
-		}
-		else if ((first6.value() & 0x10) == 0x10) {
+		else if ((first6.value() & 0x30) == 0x30) {
+			System.out.println(String.format("%X: Straight", parser.getBytePosition()));
 			return StraightEdgeRecordParser.parse(parser, fieldName);
 		}
-		else {
+		else if ((first6.value() & 0x20) == 0x20) {
+			System.out.println(String.format("%X: Curved", parser.getBytePosition()));
 			return CurvedEdgeRecordParser.parse(parser, fieldName);
 		}
+		else {
+			System.out.println(String.format("%X: Change", parser.getBytePosition()));
+			return StyleChangeRecordParser.parse(parser, fillBits, lineBits, fieldName);
+		}
 	}
 }
\ No newline at end of file
