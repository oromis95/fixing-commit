@@ -1,6 +1,10 @@
 package tv.porst.swfretools.parser.structures;
 
 import static tv.porst.swfretools.parser.SWFParserHelpers.parseUBits;
+
+import java.util.ArrayList;
+import java.util.List;
+
 import tv.porst.splib.binaryparser.UBits;
 import tv.porst.swfretools.parser.SWFBinaryParser;
 import tv.porst.swfretools.parser.SWFParserException;
@@ -27,8 +31,35 @@ public final class ShapeParser {
 
 		final UBits numFillBits = parseUBits(parser, 4, 0x00006, fieldName + "::NumFillBits");
 		final UBits numLineBits = parseUBits(parser, 4, 0x00006, fieldName + "::NumLineBits");
-		final ShapeRecord shapeRecord = ShapeRecordParser.parse(parser, numFillBits, numLineBits, fieldName + "::ShapeRecord");
 
-		return new Shape(numFillBits, numLineBits, shapeRecord);
+		final List<ShapeRecord> shapeRecords = new ArrayList<ShapeRecord>();
+
+		ShapeRecord shapeRecord = null;
+
+		UBits currentNumFillBits = numFillBits;
+		UBits currentNumLineBits = numLineBits;
+
+		do {
+
+			shapeRecord = ShapeRecordParser.parse(parser, currentNumFillBits, currentNumLineBits, fieldName + "::ShapeRecord");
+
+			shapeRecords.add(shapeRecord);
+
+			if (shapeRecord instanceof StyleChangeRecord) {
+
+				if (((StyleChangeRecord) shapeRecord).getNumFillBits() != null) {
+					currentNumFillBits = ((StyleChangeRecord) shapeRecord).getNumFillBits();
+				}
+
+				if (((StyleChangeRecord) shapeRecord).getNumLineBits() != null) {
+					currentNumLineBits = ((StyleChangeRecord) shapeRecord).getNumLineBits();
+				}
+			}
+
+		} while (!(shapeRecord instanceof EndShapeRecord));
+
+		parser.align();
+
+		return new Shape(numFillBits, numLineBits, shapeRecords);
 	}
 }
\ No newline at end of file
