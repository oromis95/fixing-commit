@@ -1,6 +1,10 @@
 package tv.porst.swfretools.parser.structures;
 
 import static tv.porst.swfretools.parser.SWFParserHelpers.parseUBits;
+
+import java.util.ArrayList;
+import java.util.List;
+
 import tv.porst.splib.binaryparser.UBits;
 import tv.porst.swfretools.parser.SWFBinaryParser;
 import tv.porst.swfretools.parser.SWFParserException;
@@ -29,9 +33,35 @@ public final class ShapeWithStyleParser {
 		final LineStyleArray lineStyles = LineStyleArrayParser.parse(parser, fieldName + "::LineStyle");
 		final UBits numFillBits = parseUBits(parser, 4, 0x00006, fieldName + "::NumFillBits");
 		final UBits numLineBits = parseUBits(parser, 4, 0x00006, fieldName + "::NumLineBits");
-		final ShapeRecord shapeRecord = ShapeRecordParser.parse(parser, numFillBits, numLineBits, fieldName + "::ShapeRecord");
 
-		return new ShapeWithStyle(fillStyles, lineStyles, numFillBits, numLineBits, shapeRecord);
+		final List<ShapeRecord> shapeRecords = new ArrayList<ShapeRecord>();
+
+		ShapeRecord shapeRecord = null;
+
+		UBits currentNumFillBits = numFillBits;
+		UBits currentNumLineBits = numLineBits;
+
+		do {
+
+			shapeRecord = ShapeRecordParser.parse(parser, currentNumFillBits, currentNumLineBits, fieldName + "::ShapeRecord");
 
+			shapeRecords.add(shapeRecord);
+
+			if (shapeRecord instanceof StyleChangeRecord) {
+
+				if (((StyleChangeRecord) shapeRecord).getNumFillBits() != null) {
+					currentNumFillBits = ((StyleChangeRecord) shapeRecord).getNumFillBits();
+				}
+
+				if (((StyleChangeRecord) shapeRecord).getNumLineBits() != null) {
+					currentNumLineBits = ((StyleChangeRecord) shapeRecord).getNumLineBits();
+				}
+			}
+
+		} while (!(shapeRecord instanceof EndShapeRecord));
+
+		parser.align();
+
+		return new ShapeWithStyle(fillStyles, lineStyles, numFillBits, numLineBits, shapeRecord);
 	}
 }
\ No newline at end of file
