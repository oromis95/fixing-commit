@@ -48,6 +48,11 @@ public final class DefineTextTag extends Tag {
 	 */
 	private final List<TextRecord> textRecords;
 
+	/**
+	 * End marker of the record.
+	 */
+	private final UINT8 end;
+
 	/**
 	 * Creates a new DefineText tag object.
 	 * 
@@ -58,10 +63,11 @@ public final class DefineTextTag extends Tag {
 	 * @param glyphBits Bits in each glyph index.
 	 * @param advanceBits Bits in each advance value.
 	 * @param textRecords Text records.
+	 * @param end End marker of the record.
 	 */
 	public DefineTextTag(final RecordHeader header, final UINT16 characterId, final Rect textBounds,
 			final Matrix textMatrix, final UINT8 glyphBits, final UINT8 advanceBits,
-			final List<TextRecord> textRecords) {
+			final List<TextRecord> textRecords, final UINT8 end) {
 		super(header);
 
 		this.characterId = characterId;
@@ -70,6 +76,7 @@ public final class DefineTextTag extends Tag {
 		this.glyphBits = glyphBits;
 		this.advanceBits = advanceBits;
 		this.textRecords = new ArrayList<TextRecord>(textRecords);
+		this.end = end;
 	}
 
 	/**
@@ -90,6 +97,15 @@ public final class DefineTextTag extends Tag {
 		return characterId;
 	}
 
+	/**
+	 * Returns the end marker of the record.
+	 *
+	 * @return The end marker of the record.
+	 */
+	public UINT8 getEnd() {
+		return end;
+	}
+
 	/**
 	 * Returns the bits in each glyph index.
 	 * 
