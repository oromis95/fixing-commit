@@ -124,6 +124,8 @@ public abstract class AS2Visitor {
 
 	protected abstract void visit(final ActionOr instruction);
 
+	protected abstract void visit(final ActionPush instruction);
+
 	protected abstract void visit(final ActionSetMember instruction);
 
 	protected abstract void visit(final ActionSetProperty instruction);
@@ -370,6 +372,9 @@ public abstract class AS2Visitor {
 		else if (instruction instanceof ActionOr) {
 			visit((ActionOr) instruction);
 		}
+		else if (instruction instanceof ActionPush) {
+			visit((ActionPush) instruction);
+		}
 		else if (instruction instanceof ActionSetMember) {
 			visit((ActionSetMember) instruction);
 		}
@@ -463,5 +468,8 @@ public abstract class AS2Visitor {
 		else if (instruction instanceof ActionUnknown) {
 			visit((ActionUnknown) instruction);
 		}
+		else {
+			System.out.println("oh");
+		}
 	}
 }
