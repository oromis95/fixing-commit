@@ -4,6 +4,7 @@ import javax.swing.JFrame;
 
 import tv.porst.swfretools.dissector.console.ConsoleDumper;
 import tv.porst.swfretools.dissector.gui.MainWindow;
+import tv.porst.swfretools.utils.Decompressor;
 
 /**
  * Main class of Flash Dissector.
@@ -23,6 +24,19 @@ public final class Main {
 
 			return;
 		}
+		else if (args.length == 4 && args[0].equals("-decompress") && args[2].equals("-o")) {
+
+			Decompressor.decompress(args[1], args[3]);
+
+			return;
+		}
+		else if (args.length != 0) {
+			System.out.println("Usage: dissector.jar options");
+			System.out.println();
+			System.out.println("  -dump [file]                            : Dumps the content of the SWF file");
+			System.out.println("  -decompress [inputfile] -o [outputfile] : Decompresses the input file");
+			return;
+		}
 
 		final MainWindow window = new MainWindow();
 
