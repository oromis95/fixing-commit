@@ -1,11 +1,11 @@
 package tv.porst.swfretools.parser.tags;
 
-import static tv.porst.swfretools.parser.SWFParserHelpers.parseUBits;
 import static tv.porst.swfretools.parser.SWFParserHelpers.parseFlag;
+import static tv.porst.swfretools.parser.SWFParserHelpers.parseUBits;
 import static tv.porst.swfretools.parser.SWFParserHelpers.parseUINT16;
 import static tv.porst.swfretools.parser.SWFParserHelpers.parseUINT8;
-import tv.porst.splib.binaryparser.UBits;
 import tv.porst.splib.binaryparser.Flag;
+import tv.porst.splib.binaryparser.UBits;
 import tv.porst.splib.binaryparser.UINT16;
 import tv.porst.splib.binaryparser.UINT8;
 import tv.porst.swfretools.parser.SWFBinaryParser;
@@ -36,7 +36,7 @@ public final class DefineVideoStreamParser {
 		final UINT16 width = parseUINT16(parser, 0x00006, "DefineVideoStream::Width");
 		final UINT16 height = parseUINT16(parser, 0x00006, "DefineVideoStream::Height");
 		final UBits videoFlagsReserved = parseUBits(parser, 4, 0x00006, "DefineVideoStream::VideoFlagsReserved");
-		final UBits videoFlagsDeblocking = parseUBits(parser, 4, 0x00006, "DefineVideoStream::VideoFlagsDeblocking");
+		final UBits videoFlagsDeblocking = parseUBits(parser, 3, 0x00006, "DefineVideoStream::VideoFlagsDeblocking");
 		final Flag videoFlagsSmoothing = parseFlag(parser, 0x00006, "DefineVideoStream::VideoFlagsSmoothing");
 		final UINT8 codecId = parseUINT8(parser, 0x00006, "DefineVideoStream::CodecID");
 
