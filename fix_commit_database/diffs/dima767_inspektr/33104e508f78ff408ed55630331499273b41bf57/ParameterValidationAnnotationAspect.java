@@ -13,7 +13,7 @@
  *  See the License for the specific language governing permissions and
  *  limitations under the License.
  */
-package org.inspetkr.support.spring;
+package org.inspektr.support.spring;
 
 import java.lang.annotation.Annotation;
 import java.lang.reflect.Method;
