@@ -21,11 +21,12 @@ import java.util.concurrent.Executors;
 import org.inspektr.common.annotation.NotNull;
 import org.inspektr.statistics.StatisticActionContext;
 import org.inspektr.statistics.StatisticManager;
-import org.springframework.beans.factory.DisposableBean;
 
 /**
  * Abstract class to handle the multithreading capability required by most implementations
  * of the StatisticManager.
+ * <p>
+ * NOTE: When used with Spring configuration, the destruction method should be set to "destroy".
  * 
  * @author Scott Battaglia
  * @version $Revision$ $Date$
@@ -33,7 +34,7 @@ import org.springframework.beans.factory.DisposableBean;
  *
  */
 public abstract class AbstractThreadExecutorBasedStatisticManager implements
-		StatisticManager, DisposableBean {
+		StatisticManager {
 
 	/** ExecutorService that has one thread to asynchronously save requests. */
 	@NotNull
