@@ -18,7 +18,6 @@ package org.inspektr.statistics;
 import java.util.Date;
 
 import org.inspektr.statistics.annotation.Statistic.Precision;
-import org.springframework.util.Assert;
 
 /**
  * Represents a statistic to be stored in the database.
@@ -47,11 +46,17 @@ public final class StatisticActionContext {
 		this.serverIpAddress = serverIpAddress;
 		this.applicationCode = applicationCode;
 		
-		Assert.notNull(this.when, "when cannot be null");
-		Assert.notNull(this.what, "what cannot be null.");
-		Assert.notNull(this.requiredPrecision, "requiredPrecision cannot be null");
-		Assert.notNull(this.serverIpAddress, "serverIpAddress is a required field.");
-		Assert.notNull(this.applicationCode, "applicationCode is a required field.");
+		assertNotNull(this.when, "when cannot be null");
+		assertNotNull(this.what, "what cannot be null.");
+		assertNotNull(this.requiredPrecision, "requiredPrecision cannot be null");
+		assertNotNull(this.serverIpAddress, "serverIpAddress is a required field.");
+		assertNotNull(this.applicationCode, "applicationCode is a required field.");
+	}
+	
+	protected void assertNotNull(final Object o, final String message) {
+		if (o == null) {
+			throw new IllegalArgumentException(message);
+		}
 	}
 	
 	public String getWhat() {
