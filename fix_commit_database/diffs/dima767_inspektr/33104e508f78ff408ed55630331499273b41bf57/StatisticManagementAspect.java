@@ -31,7 +31,6 @@ import org.inspektr.common.web.ClientInfo;
 import org.inspektr.statistics.annotation.Statistic;
 import org.inspektr.statistics.spi.StatisticNameResolver;
 import org.inspektr.statistics.spi.support.DefaultStatisticNameResolver;
-import org.springframework.util.StringUtils;
 
 /**
  * A POJO style aspect modularizing management of a statistic data concern.
@@ -78,7 +77,7 @@ public final class StatisticManagementAspect {
     		throw e;
     	} finally {
     		final ClientInfo clientInfo = this.clientInfoResolver.resolveFrom(joinPoint, retVal);
-    		final String appCode = StringUtils.hasText(statistic.applicationCode()) ? statistic.applicationCode() : this.applicationCode;
+    		final String appCode = (statistic.applicationCode() !=null && statistic.applicationCode().length() > 0) ? statistic.applicationCode() : this.applicationCode;
 	    	final StatisticActionContext statisticActionContext = new StatisticActionContext(new Date(), name, statistic.requiredPrecision(), clientInfo.getServerIpAddress(), appCode);
 	    	for (final StatisticManager manager : this.statisticManagers) {
 	    		manager.recalculate(statisticActionContext);
