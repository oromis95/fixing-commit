@@ -23,14 +23,17 @@
 package io.mola.galimatias.canonicalize;
 
 import io.mola.galimatias.GalimatiasParseException;
+import io.mola.galimatias.TestURL;
 import io.mola.galimatias.URL;
-import org.junit.runner.RunWith;
-import org.junit.runners.JUnit4;
 import org.junit.Test;
+import org.junit.experimental.theories.Theories;
+import org.junit.experimental.theories.Theory;
+import org.junit.runner.RunWith;
 
 import static org.fest.assertions.Assertions.assertThat;
+import static org.junit.Assume.assumeNotNull;
 
-@RunWith(JUnit4.class)
+@RunWith(Theories.class)
 public class RFC3986CanonicalizerTest {
 
     @Test
@@ -46,4 +49,15 @@ public class RFC3986CanonicalizerTest {
         }
     }
 
+    @Theory
+    public void idempotence(final @TestURL.TestURLs(dataset = TestURL.DATASETS.WHATWG) TestURL testURL) throws GalimatiasParseException {
+        assumeNotNull(testURL.parsedURL);
+        final URLCanonicalizer canon = new RFC2396Canonicalizer();
+        final URL roundOne = canon.canonicalize(testURL.parsedURL);
+        final URL roundTwo = canon.canonicalize(roundOne);
+        assertThat(roundOne).isEqualTo(roundTwo);
+        final URL reparse = URL.parse(roundTwo.toString());
+        assertThat(reparse).isEqualTo(roundTwo);
+    }
+
 }
