@@ -42,7 +42,7 @@ public class IPv6AddressTest {
 
     private static final String[][] TEST_ADDRESSES = new String[][] {
         new String[] { "fedc:ba98:7654:3210:fedc:ba98:7654:3210" },
-        new String[] { "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210", "fedc:ba98:7654:3210:fedc:ba98:7654:3210" },
+        new String[] { "FEDC:BA98:7654:3210:FEDC:BA98:7654:3211", "fedc:ba98:7654:3210:fedc:ba98:7654:3211" },
         new String[] { "2001:0db8:85a3:0000:0000:8a2e:0370:7334", "2001:db8:85a3::8a2e:370:7334" },
         new String[] { "2001:db8:85a3::8a2e:370:7334", "2001:db8:85a3::8a2e:370:7334" },
         new String[] { "0:0:0:0:0:0:0:1", "::1" },
