@@ -32,29 +32,17 @@ public final class URLParsingSettings {
 
     private static URLParsingSettings DEFAULT = new URLParsingSettings();
 
-    public static enum Standard {
-        RFC_2396,
-        RFC_3986,
-        WHATWG
-    }
-
-    private Standard standard;
 
     private ErrorHandler errorHandler;
 
     private URLParsingSettings() {
-        this(Standard.WHATWG, DefaultErrorHandler.getInstance());
+        this(DefaultErrorHandler.getInstance());
     }
 
-    private URLParsingSettings(final Standard standard, final ErrorHandler errorHandler) {
-        this.standard = standard;
+    private URLParsingSettings(final ErrorHandler errorHandler) {
         this.errorHandler = errorHandler;
     }
 
-    public Standard standard() {
-        return this.standard;
-    }
-
     public ErrorHandler errorHandler() {
         return this.errorHandler;
     }
@@ -63,12 +51,8 @@ public final class URLParsingSettings {
         return DEFAULT;
     }
 
-    public URLParsingSettings withStandard(final Standard standard) {
-        return new URLParsingSettings(standard, this.errorHandler);
-    }
-
     public URLParsingSettings withErrorHandler(final ErrorHandler errorHandler) {
-        return new URLParsingSettings(this.standard, errorHandler);
+        return new URLParsingSettings(errorHandler);
     }
 
 }
