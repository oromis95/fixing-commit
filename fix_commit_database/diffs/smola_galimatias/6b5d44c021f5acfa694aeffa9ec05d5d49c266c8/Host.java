@@ -23,10 +23,17 @@
 package io.mola.galimatias;
 
 import java.io.Serializable;
+import java.net.MalformedURLException;
 
 public abstract class Host implements Serializable {
 
     @Override
     public abstract String toString();
 
+    private static final URLParser DEFAULT_URL_PARSER = new URLParser();
+
+    public static Host parse(final String host) throws MalformedURLException {
+        return DEFAULT_URL_PARSER.parseHost(host);
+    }
+
 }
