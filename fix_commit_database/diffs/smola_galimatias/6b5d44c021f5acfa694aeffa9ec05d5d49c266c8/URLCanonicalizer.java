@@ -20,7 +20,12 @@
  *   DEALINGS IN THE SOFTWARE.
  */
 
-package io.mola.galimatias;
+package io.mola.galimatias.canonicalization;
+
+import io.mola.galimatias.URL;
+
+interface URLCanonicalizer {
+
+    public URL canonicalize(URL url);
 
-public class URLCanonicalizer {
 }
