@@ -41,7 +41,7 @@ import java.util.Locale;
  */
 class URLUtils {
 
-    private static final Charset UTF_8 = Charset.forName("UTF-8");
+    static final Charset UTF_8 = Charset.forName("UTF-8");
 
     private URLUtils() {
 
@@ -100,28 +100,8 @@ class URLUtils {
     static String[] domainToASCII(final String[] domainLabels) throws GalimatiasParseException {
         final List<String> asciiLabels = new ArrayList<String>();
         for (final String domainLabel : domainLabels) {
-            // XXX: The lowercasing is added here as it's the most sane thing to do
-            //      and browsers do it. However, WHATWG URL does not specify this.
-            //      See https://www.w3.org/Bugs/Public/show_bug.cgi?id=24187
-            //
-            // XXX: Fail on spaces.
-            //      https://www.w3.org/Bugs/Public/show_bug.cgi?id=24191
-            //
-            // XXX: See http://src.chromium.org/viewvc/chrome/trunk/src/url/url_canon_host.cc
-            //      DoIDNHost function for the behaviour implemented here.
-            //      See also http://src.chromium.org/viewvc/chrome/trunk/src/url/url_canon_icu.cc
-            //
             try {
-                String asciiLabel = domainLabelToASCII(domainLabel).toLowerCase(Locale.ENGLISH);
-                asciiLabel = percentDecode(asciiLabel);
-                if (asciiLabel.contains("%")) {
-                    throw new GalimatiasParseException("Invalid percent-escaped character");
-                }
-
-
-                if (asciiLabel.contains(" ")) {
-                    throw new GalimatiasParseException("Spaces are illegal in host");
-                }
+                final String asciiLabel = domainLabelToASCII(domainLabel);
                 asciiLabels.add(asciiLabel);
             } catch (IllegalArgumentException ex) {
                 throw new GalimatiasParseException("Could not convert domain to ASCII", -1, ex);
