@@ -5,7 +5,7 @@ package com.tobedevoured.modelcitizen.field;
  * @author Michael Guymon
  *
  */
-public abstract class ConstructorCallBack {
+public abstract class ConstructorCallback {
 
 	public abstract Object createInstance();
 }
