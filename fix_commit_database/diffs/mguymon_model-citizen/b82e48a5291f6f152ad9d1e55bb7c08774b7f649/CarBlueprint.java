@@ -8,9 +8,9 @@ package com.tobedevoured.modelcitizen.blueprint;
  * to you under the Apache License, Version 2.0 (the
  * "License"); you may not use this file except in compliance
  * with the License. You may obtain a copy of the License at
-   *
+ *
  * http://www.apache.org/licenses/LICENSE-2.0
-   *
+ *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@@ -34,32 +34,32 @@ import com.tobedevoured.modelcitizen.model.Car;
 import com.tobedevoured.modelcitizen.model.Driver;
 import com.tobedevoured.modelcitizen.model.Wheel;
 
-@Blueprint( Car.class )
+@Blueprint(Car.class)
 public class CarBlueprint {
 
-	@Default
-	public String make = "car make";
+    @Default
+    public String make = "car make";
 
-	@Default
-	public String manufacturer = "car manufacturer";
+    @Default
+    public String manufacturer = "car manufacturer";
 
-	@Default
-	public float mileage = 100.1f;
+    @Default
+    public float mileage = 100.1f;
 
-	@Default
-	public Map status = new HashMap();
+    @Default
+    public Map status = new HashMap();
 
-	@MappedList(target = Wheel.class, size = 4, force=true)
-	public List<Wheel> wheels;
-	
-	@MappedSet(target = Wheel.class, size = 1)
-	public Set<Wheel> spares;
+    @MappedList(target = Wheel.class, size = 4, force = true)
+    public List<Wheel> wheels;
 
-	@Mapped
-	public Driver driver;
-	
-	@Mapped
-	@Nullable
-	public Driver passenger = null;
+    @MappedSet(target = Wheel.class, size = 1)
+    public Set<Wheel> spares;
+
+    @Mapped
+    public Driver driver;
+
+    @Mapped
+    @Nullable
+    public Driver passenger = null;
 
 }
\ No newline at end of file
