@@ -8,9 +8,9 @@ package com.tobedevoured.modelcitizen.blueprint;
  * to you under the Apache License, Version 2.0 (the
  * "License"); you may not use this file except in compliance
  * with the License. You may obtain a copy of the License at
-   *
+ *
  * http://www.apache.org/licenses/LICENSE-2.0
-   *
+ *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@@ -28,19 +28,19 @@ import com.tobedevoured.modelcitizen.model.Option;
 @Blueprint(Wheel.class)
 public class WheelBlueprint {
 
-	@NewInstance
-	ConstructorCallback constructor = new ConstructorCallback() {
+    @NewInstance
+    ConstructorCallback constructor = new ConstructorCallback() {
 
-		@Override
-		public Object createInstance() {
-			Wheel wheel = new Wheel( "tire name" );
-			return wheel;
-		}
-		
-	};
+        @Override
+        public Object createInstance() {
+            Wheel wheel = new Wheel("tire name");
+            return wheel;
+        }
 
-	@Default
-	public Integer size = 10;
+    };
+
+    @Default
+    public Integer size = 10;
 
     @Default
     public String color = "black";
