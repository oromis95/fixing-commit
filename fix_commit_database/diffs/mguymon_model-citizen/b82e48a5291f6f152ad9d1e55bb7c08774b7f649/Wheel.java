@@ -8,9 +8,9 @@ package com.tobedevoured.modelcitizen.model;
  * to you under the Apache License, Version 2.0 (the
  * "License"); you may not use this file except in compliance
  * with the License. You may obtain a copy of the License at
-   *
+ *
  * http://www.apache.org/licenses/LICENSE-2.0
-   *
+ *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@@ -27,23 +27,23 @@ public class Wheel {
     private List<Option> options;
 
     private List<Option> variants;
-    
-    public Wheel( String name ) {
-    	this.name = name;
+
+    public Wheel(String name) {
+        this.name = name;
     }
-    
+
     public String getName() {
         return name;
     }
-    
+
     public void setName(String name) {
         this.name = name;
     }
-    
+
     public Integer getSize() {
         return size;
     }
-    
+
     public void setSize(Integer size) {
         this.size = size;
     }
@@ -51,9 +51,9 @@ public class Wheel {
     public List<Option> getOptions() {
         return options;
     }
-    
+
     public void setOptions(List<Option> options) {
-        
+
         this.options = options;
     }
 
