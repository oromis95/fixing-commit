@@ -8,9 +8,9 @@ package com.tobedevoured.modelcitizen.model;
  * to you under the Apache License, Version 2.0 (the
  * "License"); you may not use this file except in compliance
  * with the License. You may obtain a copy of the License at
-   *
+ * <p/>
  * http://www.apache.org/licenses/LICENSE-2.0
-   *
+ * <p/>
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@@ -22,9 +22,9 @@ public class Option {
     public String name;
 
     public void setName(String name) {
-    	this.name = name;
+        this.name = name;
     }
-    
+
     public String getName() {
         return name;
     }
