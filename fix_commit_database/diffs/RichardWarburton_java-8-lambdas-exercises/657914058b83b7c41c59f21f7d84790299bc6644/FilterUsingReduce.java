@@ -15,16 +15,26 @@ public class FilterUsingReduce {
         return stream.reduce(initial,
                              (List<I> acc, I x) -> {
                                 if (predicate.test(x)) {
-                                    acc.add(x);
+                                	// We are copying data from acc to new list instance. It is very inefficient,
+                                	// but contract of Stream.reduce method requires that accumulator function does
+                                	// not mutate its arguments.
+                                	// Stream.collect method could be used to implement more efficient mutable reduction,
+                                	// but this exercise asks to use reduce method explicitly.
+                                	List<I> newAcc = new ArrayList<>(acc);
+                                    newAcc.add(x);
+                                    return newAcc;
+                                } else {
+                                	return acc;
                                 }
-                                return acc;
                              },
                              FilterUsingReduce::combineLists);
     }
 
     private static <I> List<I> combineLists(List<I> left, List<I> right) {
-        left.addAll(right);
-        return left;
+    	// We are copying left to new list to avoid mutating it. 
+    	List<I> newLeft = new ArrayList<>(left);
+    	newLeft.addAll(right);
+        return newLeft;
     }
 
 }
