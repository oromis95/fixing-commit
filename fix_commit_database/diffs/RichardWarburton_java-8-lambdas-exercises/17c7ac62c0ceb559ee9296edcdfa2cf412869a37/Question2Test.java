@@ -2,7 +2,7 @@ package com.insightfullogic.java8.exercises.chapter2;
 
 import org.junit.Test;
 
-import java.util.Date;
+import java.util.Calendar;
 
 import static org.junit.Assert.assertEquals;
 
@@ -10,7 +10,11 @@ public class Question2Test {
 
     @Test
     public void exampleInB() {
-        String formatted = Question2.formatter.get().format(new Date(0));
+        Calendar cal = Calendar.getInstance();
+        cal.set(Calendar.YEAR, 1970);
+        cal.set(Calendar.MONTH, Calendar.JANUARY);
+        cal.set(Calendar.DAY_OF_MONTH, 1);
+        String formatted = Question2.formatter.get().format(cal.getTime());
         assertEquals("01-Jan-1970", formatted);
     }
 
