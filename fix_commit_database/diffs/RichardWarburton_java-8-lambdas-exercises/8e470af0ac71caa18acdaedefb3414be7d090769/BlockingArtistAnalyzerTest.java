@@ -1,6 +1,6 @@
 package com.insightfullogic.java8.answers.chapter9;
 
-import com.insightfullogic.java8.exercises.chapter9.BlockingArtistAnalyzer;
+import com.insightfullogic.java8.answers.chapter9.BlockingArtistAnalyzer;
 import org.junit.Test;
 
 import static org.junit.Assert.assertFalse;
@@ -8,7 +8,7 @@ import static org.junit.Assert.assertTrue;
 
 public class BlockingArtistAnalyzerTest {
 
-    private final com.insightfullogic.java8.exercises.chapter9.BlockingArtistAnalyzer analyser = new BlockingArtistAnalyzer(new FakeLookupService()::lookupArtistName);
+    private final com.insightfullogic.java8.answers.chapter9.BlockingArtistAnalyzer analyser = new BlockingArtistAnalyzer(new FakeLookupService()::lookupArtistName);
 
     @Test
     public void largerGroupsAreLarger() {
