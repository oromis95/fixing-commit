@@ -2,7 +2,7 @@ package com.insightfullogic.java8.answers.chapter4;
 
 import com.insightfullogic.java8.examples.chapter1.Artist;
 import com.insightfullogic.java8.examples.chapter1.SampleData;
-import com.insightfullogic.java8.exercises.chapter4.PerformanceFixed;
+import com.insightfullogic.java8.answers.chapter4.PerformanceFixed;
 import org.junit.Test;
 
 import java.util.List;
@@ -17,7 +17,7 @@ public class PerformanceTest {
 
     @Test
     public void findsAllTheBeatles() {
-        com.insightfullogic.java8.exercises.chapter4.PerformanceFixed stub = new PerformanceFixed() {
+        com.insightfullogic.java8.answers.chapter4.PerformanceFixed stub = new PerformanceFixed() {
             @Override
             public String getName() {
                 throw new UnsupportedOperationException();
