@@ -1,6 +1,6 @@
 package com.insightfullogic.java8.answers.chapter5;
 
-import com.insightfullogic.java8.exercises.chapter5.GroupingBy;
+import com.insightfullogic.java8.answers.chapter5.GroupingBy;
 import org.junit.Test;
 
 import java.util.List;
@@ -14,7 +14,7 @@ public class GroupingByTest {
 
     @Test
     public void stringsByLength() {
-        com.insightfullogic.java8.exercises.chapter5.GroupingBy<String, Integer> stringIntegerGroupingBy = new GroupingBy<>(String::length);
+        com.insightfullogic.java8.answers.chapter5.GroupingBy<String, Integer> stringIntegerGroupingBy = new GroupingBy<>(String::length);
         Map<Integer,List<String>> results = Stream.of("a", "b", "cc", "dd")
                                                   .collect(stringIntegerGroupingBy);
 
