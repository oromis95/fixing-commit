@@ -2,7 +2,7 @@ package com.insightfullogic.java8.answers.chapter4;
 
 import com.insightfullogic.java8.examples.chapter1.Artist;
 import com.insightfullogic.java8.examples.chapter1.SampleData;
-import com.insightfullogic.java8.exercises.chapter4.ArtistsFixed;
+import com.insightfullogic.java8.answers.chapter4.ArtistsFixed;
 import org.junit.Assert;
 import org.junit.Test;
 
@@ -14,7 +14,7 @@ import static org.junit.Assert.assertFalse;
 
 public class ArtistsTest {
 
-    private final com.insightfullogic.java8.exercises.chapter4.ArtistsFixed optionalExamples = new ArtistsFixed(SampleData.getThreeArtists());
+    private final com.insightfullogic.java8.answers.chapter4.ArtistsFixed optionalExamples = new ArtistsFixed(SampleData.getThreeArtists());
 
     @Test
     public void indexWithinRange() {
