@@ -2,7 +2,7 @@ package com.insightfullogic.java8.answers.chapter5;
 
 import com.insightfullogic.java8.examples.chapter1.Artist;
 import com.insightfullogic.java8.examples.chapter1.SampleData;
-import com.insightfullogic.java8.exercises.chapter5.LongestName;
+import com.insightfullogic.java8.answers.chapter5.LongestName;
 import org.junit.Test;
 
 import static org.junit.Assert.assertEquals;
@@ -11,7 +11,7 @@ public class LongestNameTest {
 
     @Test
     public void findsLongestNameByReduce() {
-        Artist artist = com.insightfullogic.java8.exercises.chapter5.LongestName.byReduce(SampleData.getThreeArtists());
+        Artist artist = com.insightfullogic.java8.answers.chapter5.LongestName.byReduce(SampleData.getThreeArtists());
         assertEquals(SampleData.johnColtrane, artist);
     }
 
