@@ -1,6 +1,6 @@
 package com.insightfullogic.java8.examples.chapter9;
 
-import com.insightfullogic.java8.examples.music.Album;
+import com.insightfullogic.java8.examples.chapter1.Album;
 
 public interface AlbumLookup {
     Album lookupByName(String albumName);
