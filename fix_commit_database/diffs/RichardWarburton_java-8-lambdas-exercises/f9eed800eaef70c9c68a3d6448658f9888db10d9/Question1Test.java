@@ -0,0 +1,28 @@
+package com.insightfullogic.java8.exercises.chapter3;
+
+import org.junit.Test;
+
+import java.util.stream.Stream;
+
+import static org.junit.Assert.assertEquals;
+
+public class Question1Test {
+
+    @Test
+    public void addsEmptyList() {
+        int result = Question1.addUp(Stream.empty());
+        assertEquals(0, result);
+    }
+
+    @Test
+    public void addsListWithValues() {
+        int result = Question1.addUp(Stream.of(1, 3, -2));
+        assertEquals(2, result);
+    }
+
+    @Test
+    public void extractsNamesAndOriginsOfArtists() {
+
+    }
+
+}
