@@ -1,13 +1,12 @@
 package com.insightfullogic.java8.examples.chapter7;
 
 import com.insightfullogic.java8.examples.chapter4.OrderDomain;
-import com.insightfullogic.java8.examples.music.Album;
+import com.insightfullogic.java8.examples.chapter1.Album;
 import org.junit.Test;
 
 import static java.util.Arrays.asList;
 import static java.util.Collections.emptyList;
 import static org.junit.Assert.assertEquals;
-import static org.mockito.Mockito.mock;
 
 /**
  * Sample code for chapter7 OrderDomain from the
