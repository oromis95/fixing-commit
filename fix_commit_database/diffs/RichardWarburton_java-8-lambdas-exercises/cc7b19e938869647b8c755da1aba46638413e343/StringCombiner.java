@@ -32,7 +32,14 @@ public StringCombiner add(String element) {
 
     // BEGIN merge
 public StringCombiner merge(StringCombiner other) {
-    builder.append(other.builder);
+	if (other.builder.length() > 0) {
+        if (areAtStart()) {
+        	builder.append(prefix);
+        } else {
+        	builder.append(delim);
+        }
+        builder.append(other.builder, prefix.length(), other.builder.length());
+    }
     return this;
 }
     // END merge
