@@ -14,7 +14,7 @@ public class Question2Test {
         cal.set(Calendar.YEAR, 1970);
         cal.set(Calendar.MONTH, Calendar.JANUARY);
         cal.set(Calendar.DAY_OF_MONTH, 1);
-        String formatted = Question2.formatter.get().format(cal.getTime());
+        String formatted = Question2.formatter.get().getFormat().format(cal.getTime());
         assertEquals("01-Jan-1970", formatted);
     }
 
