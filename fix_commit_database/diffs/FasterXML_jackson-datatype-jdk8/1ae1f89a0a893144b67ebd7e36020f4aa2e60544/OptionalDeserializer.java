@@ -94,7 +94,7 @@ final class OptionalDeserializer
         } else {
             refd = _valueDeserializer.deserializeWithType(jp, ctxt, _valueTypeDeserializer);
         }
-        return Optional.of(refd);
+        return Optional.ofNullable(refd);
     }
 
     /* NOTE: usually should not need this method... but for some reason, it is needed here.
@@ -116,6 +116,6 @@ final class OptionalDeserializer
             return deserialize(jp, ctxt);
         }
         // with type deserializer to use here? Looks like we get passed same one?
-        return Optional.of(typeDeserializer.deserializeTypedFromAny(jp, ctxt));
+        return Optional.ofNullable(typeDeserializer.deserializeTypedFromAny(jp, ctxt));
     }
 }
