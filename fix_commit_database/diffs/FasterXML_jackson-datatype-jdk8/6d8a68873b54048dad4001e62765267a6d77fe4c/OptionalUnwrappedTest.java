@@ -25,6 +25,21 @@ public class OptionalUnwrappedTest extends ModuleTestBase
 		public Optional<Child> child = Optional.of(new Child());
 	}
 
+	static class Bean {
+	    public String id;
+	    @JsonUnwrapped(prefix="child")
+	    public Optional<Bean2> bean2;
+
+	    public Bean(String id, Optional<Bean2> bean2) {
+	        this.id = id;
+	        this.bean2 = bean2;
+	    }
+	}
+
+	static class Bean2 {
+	    public String name;
+	}	
+
 	public void testUntypedWithOptionalsNotNulls() throws Exception
 	{
 		final ObjectMapper mapper = mapperWithModule(false);
@@ -32,4 +47,12 @@ public class OptionalUnwrappedTest extends ModuleTestBase
 		String jsonAct = mapper.writeValueAsString(new OptionalParent());
 		assertEquals(jsonExp, jsonAct);
 	}
+
+	// for [datatype-jdk8#20]
+	public void testShouldSerializeUnwrappedOptional() throws Exception {
+         final ObjectMapper mapper = mapperWithModule(false);
+	    
+	    assertEquals("{\"id\":\"foo\"}",
+	            mapper.writeValueAsString(new Bean("foo", Optional.<Bean2>empty())));
+	}
 }
