@@ -4,7 +4,9 @@ import java.io.IOException;
 import java.util.*;
 
 import com.fasterxml.jackson.annotation.JsonCreator;
+import com.fasterxml.jackson.annotation.JsonInclude;
 import com.fasterxml.jackson.annotation.JsonProperty;
+
 import com.fasterxml.jackson.core.type.TypeReference;
 import com.fasterxml.jackson.databind.ObjectMapper;
 
@@ -43,8 +45,22 @@ public class TestOptionalSerializer extends ModuleTestBase
 
     static class OptionalStringBean {
         public Optional<String> value;
+
+        public OptionalStringBean() { }
+        OptionalStringBean(String str) {
+            value = Optional.ofNullable(str);
+        }
     }
 
+    static class OptionalLongBean {
+        public OptionalLong value;
+
+        public OptionalLongBean() { value = OptionalLong.empty(); }
+        OptionalLongBean(long v) {
+            value = OptionalLong.of(v);
+        }
+    }
+    
     // [issue#4]
     static class Issue4Entity {
         private final Optional<String> data;
@@ -70,13 +86,13 @@ public class TestOptionalSerializer extends ModuleTestBase
         }
     }    
 
-    private ObjectMapper mapper;
+    private ObjectMapper MAPPER;
 
     @Override
     protected void setUp() throws Exception
     {
         super.setUp();
-        mapper = mapperWithModule();
+        MAPPER = mapperWithModule();
     }
 
     /*
@@ -97,32 +113,32 @@ public class TestOptionalSerializer extends ModuleTestBase
 
     public void testIntAbsent() throws Exception
     {
-        assertFalse(mapper.readValue(mapper.writeValueAsBytes(OptionalInt.empty()), OptionalInt.class).isPresent());
+        assertFalse(MAPPER.readValue(MAPPER.writeValueAsBytes(OptionalInt.empty()), OptionalInt.class).isPresent());
     }
 
     public void testIntPresent() throws Exception
     {
-        assertEquals(5, mapper.readValue(mapper.writeValueAsBytes(OptionalInt.of(5)), OptionalInt.class).getAsInt());
+        assertEquals(5, MAPPER.readValue(MAPPER.writeValueAsBytes(OptionalInt.of(5)), OptionalInt.class).getAsInt());
     }
 
     public void testLongAbsent() throws Exception
     {
-        assertFalse(mapper.readValue(mapper.writeValueAsBytes(OptionalLong.empty()), OptionalLong.class).isPresent());
+        assertFalse(MAPPER.readValue(MAPPER.writeValueAsBytes(OptionalLong.empty()), OptionalLong.class).isPresent());
     }
 
     public void testLongPresent() throws Exception
     {
-        assertEquals(Long.MAX_VALUE, mapper.readValue(mapper.writeValueAsBytes(OptionalLong.of(Long.MAX_VALUE)), OptionalLong.class).getAsLong());
+        assertEquals(Long.MAX_VALUE, MAPPER.readValue(MAPPER.writeValueAsBytes(OptionalLong.of(Long.MAX_VALUE)), OptionalLong.class).getAsLong());
     }
 
     public void testDoubleAbsent() throws Exception
     {
-        assertFalse(mapper.readValue(mapper.writeValueAsBytes(OptionalInt.empty()), OptionalInt.class).isPresent());
+        assertFalse(MAPPER.readValue(MAPPER.writeValueAsBytes(OptionalInt.empty()), OptionalInt.class).isPresent());
     }
 
     public void testDoublePresent() throws Exception
     {
-        assertEquals(Double.MIN_VALUE, mapper.readValue(mapper.writeValueAsBytes(OptionalDouble.of(Double.MIN_VALUE)), OptionalDouble.class).getAsDouble());
+        assertEquals(Double.MIN_VALUE, MAPPER.readValue(MAPPER.writeValueAsBytes(OptionalDouble.of(Double.MIN_VALUE)), OptionalDouble.class).getAsDouble());
     }
 
     public void testBeanAbsent() throws Exception
@@ -140,9 +156,9 @@ public class TestOptionalSerializer extends ModuleTestBase
     public void testBeanWithCreator() throws Exception
     {
         final Issue4Entity emptyEntity = new Issue4Entity(Optional.empty());
-        final String json = mapper.writeValueAsString(emptyEntity);
+        final String json = MAPPER.writeValueAsString(emptyEntity);
         
-        final Issue4Entity deserialisedEntity = mapper.readValue(json, Issue4Entity.class);
+        final Issue4Entity deserialisedEntity = MAPPER.readValue(json, Issue4Entity.class);
         if (!deserialisedEntity.equals(emptyEntity)) {
             throw new IOException("Entities not equal");
         }
@@ -151,11 +167,50 @@ public class TestOptionalSerializer extends ModuleTestBase
     // [issue#4]
     public void testOptionalStringInBean() throws Exception
     {
-        OptionalStringBean bean = mapper.readValue("{\"value\":\"xyz\"}", OptionalStringBean.class);
+        OptionalStringBean bean = MAPPER.readValue("{\"value\":\"xyz\"}", OptionalStringBean.class);
         assertNotNull(bean.value);
         assertEquals("xyz", bean.value.get());
     }
 
+    // To support [issue#8]
+    public void testExcludeIfOptionalAbsent() throws Exception
+    {
+        ObjectMapper mapper = mapperWithModule()
+                .setSerializationInclusion(JsonInclude.Include.NON_NULL);
+        assertEquals(aposToQuotes("{'value':'foo'}"),
+                mapper.writeValueAsString(new OptionalStringBean("foo")));
+        // absent is not strictly null so
+        assertEquals(aposToQuotes("{'value':null}"),
+                mapper.writeValueAsString(new OptionalStringBean(null)));
+
+        // however:
+        mapper = mapperWithModule()
+                .setSerializationInclusion(JsonInclude.Include.NON_ABSENT);
+        assertEquals(aposToQuotes("{'value':'foo'}"),
+                mapper.writeValueAsString(new OptionalStringBean("foo")));
+        assertEquals(aposToQuotes("{}"),
+                mapper.writeValueAsString(new OptionalStringBean(null)));
+    }
+
+    public void testExcludeIfOptionalLongAbsent() throws Exception
+    {
+        ObjectMapper mapper = mapperWithModule()
+                .setSerializationInclusion(JsonInclude.Include.NON_NULL);
+        assertEquals(aposToQuotes("{'value':123}"),
+                mapper.writeValueAsString(new OptionalLongBean(123L)));
+        // absent is not strictly null so
+        assertEquals(aposToQuotes("{'value':null}"),
+                mapper.writeValueAsString(new OptionalLongBean()));
+
+        // however:
+        mapper = mapperWithModule()
+                .setSerializationInclusion(JsonInclude.Include.NON_ABSENT);
+        assertEquals(aposToQuotes("{'value':456}"),
+                mapper.writeValueAsString(new OptionalLongBean(456L)));
+        assertEquals(aposToQuotes("{}"),
+                mapper.writeValueAsString(new OptionalLongBean()));
+    }
+    
     /*
     /**********************************************************
     /* Helper methods
@@ -164,6 +219,6 @@ public class TestOptionalSerializer extends ModuleTestBase
     
     private <T> Optional<T> roundtrip(Optional<T> obj, TypeReference<Optional<T>> type) throws IOException
     {
-        return mapper.readValue(mapper.writeValueAsBytes(obj), type);
+        return MAPPER.readValue(MAPPER.writeValueAsBytes(obj), type);
     }
 }
