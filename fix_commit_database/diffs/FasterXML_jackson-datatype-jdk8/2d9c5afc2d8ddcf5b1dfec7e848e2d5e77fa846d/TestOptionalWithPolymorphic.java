@@ -9,66 +9,72 @@ import com.fasterxml.jackson.annotation.JsonProperty;
 import com.fasterxml.jackson.annotation.JsonSubTypes;
 import com.fasterxml.jackson.annotation.JsonTypeInfo;
 import com.fasterxml.jackson.databind.ObjectMapper;
-
-public class TestOptionalWithPolymorphic extends ModuleTestBase {
-	static class ContainerA {
-		@JsonProperty
-		private Optional<String> name = Optional.empty();
-		@JsonProperty
-		private Optional<Strategy> strategy = Optional.empty();
-	}
-
-	static class ContainerB {
-		@JsonProperty
-		private Optional<String> name = Optional.empty();
-		@JsonProperty
-		private Strategy strategy = null;
-	}
-
-	@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
-	@JsonSubTypes({ @JsonSubTypes.Type(name = "Foo", value = Foo.class),
-			@JsonSubTypes.Type(name = "Bar", value = Bar.class),
-			@JsonSubTypes.Type(name = "Baz", value = Baz.class) })
-	interface Strategy {
-	}
-
-	static class Foo implements Strategy {
-		@JsonProperty
-		private final int foo;
-
-		@JsonCreator
-		Foo(@JsonProperty("foo") int foo) {
-			this.foo = foo;
-		}
-	}
-
-	static class Bar implements Strategy {
-		@JsonProperty
-		private final boolean bar;
-
-		@JsonCreator
-		Bar(@JsonProperty("bar") boolean bar) {
-			this.bar = bar;
-		}
-	}
-
-	static class Baz implements Strategy {
-		@JsonProperty
-		private final String baz;
-
-		@JsonCreator
-		Baz(@JsonProperty("baz") String baz) {
-			this.baz = baz;
-		}
-	}
-
-	/*
-	 * /**********************************************************************
-	 * /* Test methods
-	 * /**********************************************************************
-	 */
-
-	final ObjectMapper MAPPER = mapperWithModule();
+import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
+
+public class TestOptionalWithPolymorphic extends ModuleTestBase
+{
+    static class ContainerA {
+        @JsonProperty
+        private Optional<String> name = Optional.empty();
+        @JsonProperty
+        private Optional<Strategy> strategy = Optional.empty();
+    }
+
+    static class ContainerB {
+        @JsonProperty
+        private Optional<String> name = Optional.empty();
+        @JsonProperty
+        private Strategy strategy = null;
+    }
+
+    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
+    @JsonSubTypes({ @JsonSubTypes.Type(name = "Foo", value = Foo.class),
+        @JsonSubTypes.Type(name = "Bar", value = Bar.class),
+        @JsonSubTypes.Type(name = "Baz", value = Baz.class) })
+    interface Strategy { }
+
+    static class Foo implements Strategy {
+        @JsonProperty
+        private final int foo;
+
+        @JsonCreator
+        Foo(@JsonProperty("foo") int foo) {
+            this.foo = foo;
+        }
+    }
+
+    static class Bar implements Strategy {
+        @JsonProperty
+        private final boolean bar;
+
+        @JsonCreator
+        Bar(@JsonProperty("bar") boolean bar) {
+            this.bar = bar;
+        }
+    }
+
+    static class Baz implements Strategy {
+        @JsonProperty
+        private final String baz;
+
+        @JsonCreator
+        Baz(@JsonProperty("baz") String baz) {
+            this.baz = baz;
+        }
+    }
+
+    static class AbstractOptional {
+        @JsonDeserialize(contentAs=Integer.class)
+        public Optional<java.io.Serializable> value;
+    }
+
+    /*
+    /**********************************************************
+    /* Test methods
+    /**********************************************************
+     */
+
+    final ObjectMapper MAPPER = mapperWithModule();
 
 	public void testOptionalMapsFoo() throws Exception {
 
@@ -108,6 +114,17 @@ public class TestOptionalWithPolymorphic extends ModuleTestBase {
 		_test(MAPPER, baz);
 	}
 
+	public void testOptionalWithTypeAnnotation13() throws Exception
+	{
+	    AbstractOptional result = MAPPER.readValue("{\"value\" : 5}",
+	            AbstractOptional.class);
+	    assertNotNull(result);
+         assertNotNull(result.value);
+         Object ob = result.value.get();
+         assertEquals(Integer.class, ob.getClass());
+         assertEquals(Integer.valueOf(5), ob);
+	}
+
 	private void _test(ObjectMapper m, Map<String, ?> map) throws Exception {
 		String json = m.writeValueAsString(map);
 
