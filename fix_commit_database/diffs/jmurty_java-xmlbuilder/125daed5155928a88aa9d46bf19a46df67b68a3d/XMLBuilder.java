@@ -329,7 +329,9 @@ public class XMLBuilder {
      *
      * When adding an element to a namespaced document, the new node will be
      * assigned a namespace matching it's qualified name prefix (if any) or
-     * the document's default namespace.
+     * the document's default namespace. NOTE: If the element has a prefix that
+     * does not match any known namespaces, the element will be created
+     * without any namespace.
      *
      * @param name
      * the name of the XML element.
@@ -340,17 +342,10 @@ public class XMLBuilder {
      * @throws IllegalStateException
      * if you attempt to add a child element to an XML node that already
      * contains a text node value.
-     * @throws IllegalArgumentException
-     * if you attempt to add an element with a prefix name where that
-     * prefix is not already a defined namespace in the document.
      */
     public XMLBuilder element(String name) {
         String prefix = getPrefixFromQualifiedName(name);
         String namespaceURI = this.xmlElement.lookupNamespaceURI(prefix);
-        if (prefix != null && prefix.length() > 0 && namespaceURI == null) {
-            throw new IllegalArgumentException(
-                "Prefix '" + prefix + "' does not have a defined namespace");
-        }
         return element(name, namespaceURI);
     }
 
