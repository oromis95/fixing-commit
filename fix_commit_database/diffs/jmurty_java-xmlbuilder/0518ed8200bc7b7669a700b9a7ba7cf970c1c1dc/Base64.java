@@ -4,14 +4,14 @@ package net.iharder.base64;
  * <p>Encodes and decodes to and from Base64 notation.</p>
  * <p>Homepage: <a href="http://iharder.net/base64">http://iharder.net/base64</a>.</p>
  *
- * <p>The <tt>options</tt> parameter, which appears in a few places, is used to pass 
- * several pieces of information to the encoder. In the "higher level" methods such as 
- * encodeBytes( bytes, options ) the options parameter can be used to indicate such 
- * things as first gzipping the bytes before encoding them, not inserting linefeeds 
- * (though that breaks strict Base64 compatibility), and encoding using the URL-safe 
+ * <p>The <tt>options</tt> parameter, which appears in a few places, is used to pass
+ * several pieces of information to the encoder. In the "higher level" methods such as
+ * encodeBytes( bytes, options ) the options parameter can be used to indicate such
+ * things as first gzipping the bytes before encoding them, not inserting linefeeds
+ * (though that breaks strict Base64 compatibility), and encoding using the URL-safe
  * and Ordered dialects.</p>
  *
- * <p>The constants defined in Base64 can be OR-ed together to combine options, so you 
+ * <p>The constants defined in Base64 can be OR-ed together to combine options, so you
  * might make a call like this:</p>
  *
  * <code>String encoded = Base64.encodeBytes( mybytes, Base64.GZIP | Base64.DONT_BREAK_LINES );</code>
@@ -43,24 +43,24 @@ package net.iharder.base64;
  *   Special thanks to Jim Kellerman at <a href="http://www.powerset.com/">http://www.powerset.com/</a>
  *   for contributing the new Base64 dialects.
  *  </li>
- * 
+ *
  *  <li>v2.1 - Cleaned up javadoc comments and unused variables and methods. Added
  *   some convenience methods for reading and writing to and from files.</li>
  *  <li>v2.0.2 - Now specifies UTF-8 encoding in places where the code fails on systems
  *   with other encodings (like EBCDIC).</li>
  *  <li>v2.0.1 - Fixed an error when decoding a single byte, that is, when the
  *   encoded data was a single byte.</li>
- *  <li>v2.0 - I got rid of methods that used booleans to set options. 
+ *  <li>v2.0 - I got rid of methods that used booleans to set options.
  *   Now everything is more consolidated and cleaner. The code now detects
  *   when data that's being decoded is gzip-compressed and will decompress it
  *   automatically. Generally things are cleaner. You'll probably have to
  *   change some method calls that you were making to support the new
  *   options format (<tt>int</tt>s that you "OR" together).</li>
- *  <li>v1.5.1 - Fixed bug when decompressing and decoding to a             
- *   byte[] using <tt>decode( String s, boolean gzipCompressed )</tt>.      
- *   Added the ability to "suspend" encoding in the Output Stream so        
- *   you can turn on and off the encoding if you need to embed base64       
- *   data in an otherwise "normal" stream (like an XML file).</li>  
+ *  <li>v1.5.1 - Fixed bug when decompressing and decoding to a
+ *   byte[] using <tt>decode( String s, boolean gzipCompressed )</tt>.
+ *   Added the ability to "suspend" encoding in the Output Stream so
+ *   you can turn on and off the encoding if you need to embed base64
+ *   data in an otherwise "normal" stream (like an XML file).</li>
  *  <li>v1.5 - Output stream pases on flush() command but doesn't do anything itself.
  *      This helps when using GZIP streams.
  *      Added the ability to GZip-compress objects before encoding them.</li>
@@ -86,73 +86,73 @@ package net.iharder.base64;
  */
 public class Base64
 {
-    
-/* ********  P U B L I C   F I E L D S  ******** */   
-    
-    
+
+/* ********  P U B L I C   F I E L D S  ******** */
+
+
     /** No options specified. Value is zero. */
     public final static int NO_OPTIONS = 0;
-    
+
     /** Specify encoding. */
     public final static int ENCODE = 1;
-    
-    
+
+
     /** Specify decoding. */
     public final static int DECODE = 0;
-    
-    
+
+
     /** Specify that data should be gzip-compressed. */
     public final static int GZIP = 2;
-    
-    
+
+
     /** Don't break lines when encoding (violates strict Base64 specification) */
     public final static int DONT_BREAK_LINES = 8;
-	
-	/** 
+
+	/**
 	 * Encode using Base64-like encoding that is URL- and Filename-safe as described
-	 * in Section 4 of RFC3548: 
+	 * in Section 4 of RFC3548:
 	 * <a href="http://www.faqs.org/rfcs/rfc3548.html">http://www.faqs.org/rfcs/rfc3548.html</a>.
-	 * It is important to note that data encoded this way is <em>not</em> officially valid Base64, 
+	 * It is important to note that data encoded this way is <em>not</em> officially valid Base64,
 	 * or at the very least should not be called Base64 without also specifying that is
 	 * was encoded using the URL- and Filename-safe dialect.
 	 */
 	 public final static int URL_SAFE = 16;
-	 
-	 
+
+
 	 /**
 	  * Encode using the special "ordered" dialect of Base64 described here:
 	  * <a href="http://www.faqs.org/qa/rfcc-1940.html">http://www.faqs.org/qa/rfcc-1940.html</a>.
 	  */
 	 public final static int ORDERED = 32;
-    
-    
-/* ********  P R I V A T E   F I E L D S  ******** */  
-    
-    
+
+
+/* ********  P R I V A T E   F I E L D S  ******** */
+
+
     /** Maximum line length (76) of Base64 output. */
     private final static int MAX_LINE_LENGTH = 76;
-    
-    
+
+
     /** The equals sign (=) as a byte. */
     private final static byte EQUALS_SIGN = (byte)'=';
-    
-    
+
+
     /** The new line character (\n) as a byte. */
     private final static byte NEW_LINE = (byte)'\n';
-    
-    
+
+
     /** Preferred encoding. */
     private final static String PREFERRED_ENCODING = "UTF-8";
-    
-	
+
+
     // I think I end up not using the BAD_ENCODING indicator.
     //private final static byte BAD_ENCODING    = -9; // Indicates error in encoding
     private final static byte WHITE_SPACE_ENC = -5; // Indicates white space in encoding
     private final static byte EQUALS_SIGN_ENC = -1; // Indicates equals sign in encoding
-	
-	
-/* ********  S T A N D A R D   B A S E 6 4   A L P H A B E T  ******** */	
-    
+
+
+/* ********  S T A N D A R D   B A S E 6 4   A L P H A B E T  ******** */
+
     /** The 64 valid Base64 values. */
     //private final static byte[] ALPHABET;
 	/* Host platform me be something funny like EBCDIC, so we hardcode these values. */
@@ -160,23 +160,23 @@ public class Base64
     {
         (byte)'A', (byte)'B', (byte)'C', (byte)'D', (byte)'E', (byte)'F', (byte)'G',
         (byte)'H', (byte)'I', (byte)'J', (byte)'K', (byte)'L', (byte)'M', (byte)'N',
-        (byte)'O', (byte)'P', (byte)'Q', (byte)'R', (byte)'S', (byte)'T', (byte)'U', 
+        (byte)'O', (byte)'P', (byte)'Q', (byte)'R', (byte)'S', (byte)'T', (byte)'U',
         (byte)'V', (byte)'W', (byte)'X', (byte)'Y', (byte)'Z',
         (byte)'a', (byte)'b', (byte)'c', (byte)'d', (byte)'e', (byte)'f', (byte)'g',
         (byte)'h', (byte)'i', (byte)'j', (byte)'k', (byte)'l', (byte)'m', (byte)'n',
-        (byte)'o', (byte)'p', (byte)'q', (byte)'r', (byte)'s', (byte)'t', (byte)'u', 
+        (byte)'o', (byte)'p', (byte)'q', (byte)'r', (byte)'s', (byte)'t', (byte)'u',
         (byte)'v', (byte)'w', (byte)'x', (byte)'y', (byte)'z',
-        (byte)'0', (byte)'1', (byte)'2', (byte)'3', (byte)'4', (byte)'5', 
+        (byte)'0', (byte)'1', (byte)'2', (byte)'3', (byte)'4', (byte)'5',
         (byte)'6', (byte)'7', (byte)'8', (byte)'9', (byte)'+', (byte)'/'
     };
-	
-    
-    /** 
+
+
+    /**
      * Translates a Base64 value to either its 6-bit reconstruction value
      * or a negative number indicating some other meaning.
      **/
     private final static byte[] _STANDARD_DECODABET =
-    {   
+    {
         -9,-9,-9,-9,-9,-9,-9,-9,-9,                 // Decimal  0 -  8
         -5,-5,                                      // Whitespace: Tab and Linefeed
         -9,-9,                                      // Decimal 11 - 12
@@ -209,12 +209,12 @@ public class Base64
         -9,-9,-9,-9,-9,-9,-9,-9,-9,-9,-9,-9,-9,     // Decimal 231 - 243
         -9,-9,-9,-9,-9,-9,-9,-9,-9,-9,-9,-9         // Decimal 244 - 255 */
     };
-	
-	
+
+
 /* ********  U R L   S A F E   B A S E 6 4   A L P H A B E T  ******** */
-	
+
 	/**
-	 * Used in the URL- and Filename-safe dialect described in Section 4 of RFC3548: 
+	 * Used in the URL- and Filename-safe dialect described in Section 4 of RFC3548:
 	 * <a href="http://www.faqs.org/rfcs/rfc3548.html">http://www.faqs.org/rfcs/rfc3548.html</a>.
 	 * Notice that the last two bytes become "hyphen" and "underscore" instead of "plus" and "slash."
 	 */
@@ -222,21 +222,21 @@ public class Base64
     {
       (byte)'A', (byte)'B', (byte)'C', (byte)'D', (byte)'E', (byte)'F', (byte)'G',
       (byte)'H', (byte)'I', (byte)'J', (byte)'K', (byte)'L', (byte)'M', (byte)'N',
-      (byte)'O', (byte)'P', (byte)'Q', (byte)'R', (byte)'S', (byte)'T', (byte)'U', 
+      (byte)'O', (byte)'P', (byte)'Q', (byte)'R', (byte)'S', (byte)'T', (byte)'U',
       (byte)'V', (byte)'W', (byte)'X', (byte)'Y', (byte)'Z',
       (byte)'a', (byte)'b', (byte)'c', (byte)'d', (byte)'e', (byte)'f', (byte)'g',
       (byte)'h', (byte)'i', (byte)'j', (byte)'k', (byte)'l', (byte)'m', (byte)'n',
-      (byte)'o', (byte)'p', (byte)'q', (byte)'r', (byte)'s', (byte)'t', (byte)'u', 
+      (byte)'o', (byte)'p', (byte)'q', (byte)'r', (byte)'s', (byte)'t', (byte)'u',
       (byte)'v', (byte)'w', (byte)'x', (byte)'y', (byte)'z',
-      (byte)'0', (byte)'1', (byte)'2', (byte)'3', (byte)'4', (byte)'5', 
+      (byte)'0', (byte)'1', (byte)'2', (byte)'3', (byte)'4', (byte)'5',
       (byte)'6', (byte)'7', (byte)'8', (byte)'9', (byte)'-', (byte)'_'
     };
-	
+
 	/**
 	 * Used in decoding URL- and Filename-safe dialects of Base64.
 	 */
     private final static byte[] _URL_SAFE_DECODABET =
-    {   
+    {
       -9,-9,-9,-9,-9,-9,-9,-9,-9,                 // Decimal  0 -  8
       -5,-5,                                      // Whitespace: Tab and Linefeed
       -9,-9,                                      // Decimal 11 - 12
@@ -297,12 +297,12 @@ public class Base64
       (byte)'o', (byte)'p', (byte)'q', (byte)'r', (byte)'s', (byte)'t', (byte)'u',
       (byte)'v', (byte)'w', (byte)'x', (byte)'y', (byte)'z'
     };
-	
+
 	/**
 	 * Used in decoding the "ordered" dialect of Base64.
 	 */
     private final static byte[] _ORDERED_DECODABET =
-    {   
+    {
       -9,-9,-9,-9,-9,-9,-9,-9,-9,                 // Decimal  0 -  8
       -5,-5,                                      // Whitespace: Tab and Linefeed
       -9,-9,                                      // Decimal 11 - 12
@@ -340,7 +340,7 @@ public class Base64
         -9,-9,-9,-9,-9,-9,-9,-9,-9,-9,-9,-9         // Decimal 244 - 255 */
     };
 
-	
+
 /* ********  D E T E R M I N E   W H I C H   A L H A B E T  ******** */
 
 
@@ -353,13 +353,17 @@ public class Base64
 	 */
 	private final static byte[] getAlphabet( int options )
 	{
-		if( (options & URL_SAFE) == URL_SAFE ) return _URL_SAFE_ALPHABET;
-		else if( (options & ORDERED) == ORDERED ) return _ORDERED_ALPHABET;
-		else return _STANDARD_ALPHABET;
-		
+		if( (options & URL_SAFE) == URL_SAFE ) {
+            return _URL_SAFE_ALPHABET;
+        } else if( (options & ORDERED) == ORDERED ) {
+            return _ORDERED_ALPHABET;
+        } else {
+            return _STANDARD_ALPHABET;
+        }
+
 	}	// end getAlphabet
-	
-	
+
+
 	/**
 	 * Returns one of the _SOMETHING_DECODABET byte arrays depending on
 	 * the options specified.
@@ -369,17 +373,21 @@ public class Base64
 	 */
 	private final static byte[] getDecodabet( int options )
 	{
-		if( (options & URL_SAFE) == URL_SAFE ) return _URL_SAFE_DECODABET;
-		else if( (options & ORDERED) == ORDERED ) return _ORDERED_DECODABET;
-		else return _STANDARD_DECODABET;
-		
+		if( (options & URL_SAFE) == URL_SAFE ) {
+            return _URL_SAFE_DECODABET;
+        } else if( (options & ORDERED) == ORDERED ) {
+            return _ORDERED_DECODABET;
+        } else {
+            return _STANDARD_DECODABET;
+        }
+
 	}	// end getAlphabet
-        
 
-    
+
+
     /** Defeats instantiation. */
     private Base64(){}
-    
+
 
     /**
      * Encodes or decodes two files from the command line;
@@ -400,10 +408,10 @@ public class Base64
             }   // end if: encode
             else if( flag.equals( "-d" ) ) {
                 Base64.decodeFileToFile( infile, outfile );
-            }   // end else if: decode    
+            }   // end else if: decode
             else {
                 usage( "Unknown flag: " + flag );
-            }   // end else    
+            }   // end else
         }   // end else
     }   // end main
 
@@ -417,11 +425,11 @@ public class Base64
         System.err.println( msg );
         System.err.println( "Usage: java Base64 -e|-d inputfile outputfile" );
     }   // end usage
-    
-    
-/* ********  E N C O D I N G   M E T H O D S  ******** */    
-    
-    
+
+
+/* ********  E N C O D I N G   M E T H O D S  ******** */
+
+
     /**
      * Encodes up to the first three bytes of array <var>threeBytes</var>
      * and returns a four-byte array in Base64 notation.
@@ -443,12 +451,12 @@ public class Base64
         return b4;
     }   // end encode3to4
 
-    
+
     /**
      * <p>Encodes up to three bytes of the array <var>source</var>
      * and writes the resulting four Base64 bytes to <var>destination</var>.
      * The source and destination arrays can be manipulated
-     * anywhere along their length by specifying 
+     * anywhere along their length by specifying
      * <var>srcOffset</var> and <var>destOffset</var>.
      * This method does not check to make sure your arrays
      * are large enough to accomodate <var>srcOffset</var> + 3 for
@@ -467,19 +475,19 @@ public class Base64
      * @return the <var>destination</var> array
      * @since 1.3
      */
-    private static byte[] encode3to4( 
+    private static byte[] encode3to4(
      byte[] source, int srcOffset, int numSigBytes,
      byte[] destination, int destOffset, int options )
     {
-		byte[] ALPHABET = getAlphabet( options ); 
-	
-        //           1         2         3  
+		byte[] ALPHABET = getAlphabet( options );
+
+        //           1         2         3
         // 01234567890123456789012345678901 Bit position
         // --------000000001111111122222222 Array position from threeBytes
         // --------|    ||    ||    ||    | Six bit groups to index ALPHABET
         //          >>18  >>12  >> 6  >> 0  Right shift necessary
         //                0x3f  0x3f  0x3f  Additional AND
-        
+
         // Create buffer with zero-padding if there are only one or two
         // significant bytes passed in the array.
         // We have to shift left 24 in order to flush out the 1's that appear
@@ -496,28 +504,28 @@ public class Base64
                 destination[ destOffset + 2 ] = ALPHABET[ (inBuff >>>  6) & 0x3f ];
                 destination[ destOffset + 3 ] = ALPHABET[ (inBuff       ) & 0x3f ];
                 return destination;
-                
+
             case 2:
                 destination[ destOffset     ] = ALPHABET[ (inBuff >>> 18)        ];
                 destination[ destOffset + 1 ] = ALPHABET[ (inBuff >>> 12) & 0x3f ];
                 destination[ destOffset + 2 ] = ALPHABET[ (inBuff >>>  6) & 0x3f ];
                 destination[ destOffset + 3 ] = EQUALS_SIGN;
                 return destination;
-                
+
             case 1:
                 destination[ destOffset     ] = ALPHABET[ (inBuff >>> 18)        ];
                 destination[ destOffset + 1 ] = ALPHABET[ (inBuff >>> 12) & 0x3f ];
                 destination[ destOffset + 2 ] = EQUALS_SIGN;
                 destination[ destOffset + 3 ] = EQUALS_SIGN;
                 return destination;
-                
+
             default:
                 return destination;
         }   // end switch
     }   // end encode3to4
-    
-    
-    
+
+
+
     /**
      * Serializes an object and returns the Base64-encoded
      * version of that serialized object. If the object
@@ -533,7 +541,7 @@ public class Base64
     {
         return encodeObject( serializableObject, NO_OPTIONS );
     }   // end encodeObject
-    
+
 
 
     /**
@@ -562,30 +570,31 @@ public class Base64
     public static String encodeObject( java.io.Serializable serializableObject, int options )
     {
         // Streams
-        java.io.ByteArrayOutputStream  baos  = null; 
-        java.io.OutputStream           b64os = null; 
-        java.io.ObjectOutputStream     oos   = null; 
+        java.io.ByteArrayOutputStream  baos  = null;
+        java.io.OutputStream           b64os = null;
+        java.io.ObjectOutputStream     oos   = null;
         java.util.zip.GZIPOutputStream gzos  = null;
-        
+
         // Isolate options
         int gzip           = (options & GZIP);
         int dontBreakLines = (options & DONT_BREAK_LINES);
-        
+
         try
         {
             // ObjectOutputStream -> (GZIP) -> Base64 -> ByteArrayOutputStream
             baos  = new java.io.ByteArrayOutputStream();
             b64os = new Base64.OutputStream( baos, ENCODE | options );
-    
+
             // GZip?
             if( gzip == GZIP )
             {
                 gzos = new java.util.zip.GZIPOutputStream( b64os );
                 oos  = new java.io.ObjectOutputStream( gzos );
             }   // end if: gzip
-            else
+ else {
                 oos   = new java.io.ObjectOutputStream( b64os );
-            
+            }
+
             oos.writeObject( serializableObject );
         }   // end try
         catch( java.io.IOException e )
@@ -600,9 +609,9 @@ public class Base64
             try{ b64os.close(); } catch( Exception e ){}
             try{ baos.close();  } catch( Exception e ){}
         }   // end finally
-        
+
         // Return value according to relevant encoding.
-        try 
+        try
         {
             return new String( baos.toByteArray(), PREFERRED_ENCODING );
         }   // end try
@@ -610,10 +619,10 @@ public class Base64
         {
             return new String( baos.toByteArray() );
         }   // end catch
-        
+
     }   // end encode
-    
-    
+
+
 
     /**
      * Encodes a byte array into Base64 notation.
@@ -626,7 +635,7 @@ public class Base64
     {
         return encodeBytes( source, 0, source.length, NO_OPTIONS );
     }   // end encodeBytes
-    
+
 
 
     /**
@@ -650,11 +659,11 @@ public class Base64
      * @since 2.0
      */
     public static String encodeBytes( byte[] source, int options )
-    {   
+    {
         return encodeBytes( source, 0, source.length, options );
     }   // end encodeBytes
-    
-    
+
+
     /**
      * Encodes a byte array into Base64 notation.
      * Does not GZip-compress data.
@@ -668,8 +677,8 @@ public class Base64
     {
         return encodeBytes( source, off, len, NO_OPTIONS );
     }   // end encodeBytes
-    
-    
+
+
 
     /**
      * Encodes a byte array into Base64 notation.
@@ -688,8 +697,7 @@ public class Base64
      * @param source The data to convert
      * @param off Offset in array where conversion should begin
      * @param len Length of data to convert
-     * @param options Specified options
-	 * @param options alphabet type is pulled from this (standard, url-safe, ordered)
+     * @param options Specified options, alphabet type is pulled from this (standard, url-safe, ordered)
      * @see Base64#GZIP
      * @see Base64#DONT_BREAK_LINES
      * @since 2.0
@@ -699,22 +707,22 @@ public class Base64
         // Isolate options
         int dontBreakLines = ( options & DONT_BREAK_LINES );
         int gzip           = ( options & GZIP   );
-        
+
         // Compress?
         if( gzip == GZIP )
         {
             java.io.ByteArrayOutputStream  baos  = null;
             java.util.zip.GZIPOutputStream gzos  = null;
             Base64.OutputStream            b64os = null;
-            
-    
+
+
             try
             {
                 // GZip -> Base64 -> ByteArray
                 baos = new java.io.ByteArrayOutputStream();
                 b64os = new Base64.OutputStream( baos, ENCODE | options );
-                gzos  = new java.util.zip.GZIPOutputStream( b64os ); 
-            
+                gzos  = new java.util.zip.GZIPOutputStream( b64os );
+
                 gzos.write( source, off, len );
                 gzos.close();
             }   // end try
@@ -740,17 +748,17 @@ public class Base64
                 return new String( baos.toByteArray() );
             }   // end catch
         }   // end if: compress
-        
+
         // Else, don't compress. Better not to use streams at all then.
         else
         {
             // Convert option to boolean in way that code likes it.
             boolean breakLines = dontBreakLines == 0;
-            
+
             int    len43   = len * 4 / 3;
             byte[] outBuff = new byte[   ( len43 )                      // Main 4:3
                                        + ( (len % 3) > 0 ? 4 : 0 )      // Account for padding
-                                       + (breakLines ? ( len43 / MAX_LINE_LENGTH ) : 0) ]; // New lines      
+                                       + (breakLines ? ( len43 / MAX_LINE_LENGTH ) : 0) ]; // New lines
             int d = 0;
             int e = 0;
             int len2 = len - 2;
@@ -761,7 +769,7 @@ public class Base64
 
                 lineLength += 4;
                 if( breakLines && lineLength == MAX_LINE_LENGTH )
-                {   
+                {
                     outBuff[e+4] = NEW_LINE;
                     e++;
                     lineLength = 0;
@@ -774,7 +782,7 @@ public class Base64
                 e += 4;
             }   // end if: some padding needed
 
-            
+
             // Return value according to relevant encoding.
             try
             {
@@ -784,34 +792,34 @@ public class Base64
             {
                 return new String( outBuff, 0, e );
             }   // end catch
-            
+
         }   // end else: don't compress
-        
+
     }   // end encodeBytes
-    
 
-    
-    
-    
+
+
+
+
 /* ********  D E C O D I N G   M E T H O D S  ******** */
-    
-    
+
+
     /**
      * Decodes four bytes from array <var>source</var>
      * and writes the resulting bytes (up to three of them)
      * to <var>destination</var>.
      * The source and destination arrays can be manipulated
-     * anywhere along their length by specifying 
+     * anywhere along their length by specifying
      * <var>srcOffset</var> and <var>destOffset</var>.
      * This method does not check to make sure your arrays
      * are large enough to accomodate <var>srcOffset</var> + 4 for
      * the <var>source</var> array or <var>destOffset</var> + 3 for
      * the <var>destination</var> array.
-     * This method returns the actual number of bytes that 
+     * This method returns the actual number of bytes that
      * were converted from the Base64 encoding.
 	 * <p>This is the lowest level of the decoding methods with
 	 * all possible parameters.</p>
-     * 
+     *
      *
      * @param source the array to convert
      * @param srcOffset the index where conversion begins
@@ -823,8 +831,8 @@ public class Base64
      */
     private static int decode4to3( byte[] source, int srcOffset, byte[] destination, int destOffset, int options )
     {
-		byte[] DECODABET = getDecodabet( options ); 
-	
+		byte[] DECODABET = getDecodabet( options );
+
         // Example: Dk==
         if( source[ srcOffset + 2] == EQUALS_SIGN )
         {
@@ -833,11 +841,11 @@ public class Base64
             //              | ( ( DECODABET[ source[ srcOffset + 1] ] << 24 ) >>> 12 );
             int outBuff =   ( ( DECODABET[ source[ srcOffset    ] ] & 0xFF ) << 18 )
                           | ( ( DECODABET[ source[ srcOffset + 1] ] & 0xFF ) << 12 );
-            
+
             destination[ destOffset ] = (byte)( outBuff >>> 16 );
             return 1;
         }
-        
+
         // Example: DkL=
         else if( source[ srcOffset + 3 ] == EQUALS_SIGN )
         {
@@ -848,12 +856,12 @@ public class Base64
             int outBuff =   ( ( DECODABET[ source[ srcOffset     ] ] & 0xFF ) << 18 )
                           | ( ( DECODABET[ source[ srcOffset + 1 ] ] & 0xFF ) << 12 )
                           | ( ( DECODABET[ source[ srcOffset + 2 ] ] & 0xFF ) <<  6 );
-            
+
             destination[ destOffset     ] = (byte)( outBuff >>> 16 );
             destination[ destOffset + 1 ] = (byte)( outBuff >>>  8 );
             return 2;
         }
-        
+
         // Example: DkLE
         else
         {
@@ -868,7 +876,7 @@ public class Base64
                           | ( ( DECODABET[ source[ srcOffset + 2 ] ] & 0xFF ) <<  6)
                           | ( ( DECODABET[ source[ srcOffset + 3 ] ] & 0xFF )      );
 
-            
+
             destination[ destOffset     ] = (byte)( outBuff >> 16 );
             destination[ destOffset + 1 ] = (byte)( outBuff >>  8 );
             destination[ destOffset + 2 ] = (byte)( outBuff       );
@@ -883,10 +891,10 @@ public class Base64
             }   // end catch
         }
     }   // end decodeToBytes
-    
-    
-    
-    
+
+
+
+
     /**
      * Very low-level access to decoding ASCII characters in
      * the form of a byte array. Does not support automatically
@@ -901,11 +909,11 @@ public class Base64
     public static byte[] decode( byte[] source, int off, int len, int options )
     {
 		byte[] DECODABET = getDecodabet( options );
-	
+
         int    len34   = len * 3 / 4;
         byte[] outBuff = new byte[ len34 ]; // Upper limit on size of output
         int    outBuffPosn = 0;
-        
+
         byte[] b4        = new byte[4];
         int    b4Posn    = 0;
         int    i         = 0;
@@ -915,7 +923,7 @@ public class Base64
         {
             sbiCrop = (byte)(source[i] & 0x7f); // Only the low seven bits
             sbiDecode = DECODABET[ sbiCrop ];
-            
+
             if( sbiDecode >= WHITE_SPACE_ENC ) // White space, Equals sign or better
             {
                 if( sbiDecode >= EQUALS_SIGN_ENC )
@@ -925,30 +933,31 @@ public class Base64
                     {
                         outBuffPosn += decode4to3( b4, 0, outBuff, outBuffPosn, options );
                         b4Posn = 0;
-                        
+
                         // If that was the equals sign, break out of 'for' loop
-                        if( sbiCrop == EQUALS_SIGN )
+                        if( sbiCrop == EQUALS_SIGN ) {
                             break;
+                        }
                     }   // end if: quartet built
-                    
+
                 }   // end if: equals sign or better
-                
+
             }   // end if: white space, equals sign or better
             else
             {
                 System.err.println( "Bad Base64 input character at " + i + ": " + source[i] + "(decimal)" );
                 return null;
-            }   // end else: 
+            }   // end else:
         }   // each input character
-                                   
+
         byte[] out = new byte[ outBuffPosn ];
-        System.arraycopy( outBuff, 0, out, 0, outBuffPosn ); 
+        System.arraycopy( outBuff, 0, out, 0, outBuffPosn );
         return out;
     }   // end decode
-    
-    
-	
-	
+
+
+
+
     /**
      * Decodes data from Base64 notation, automatically
      * detecting gzip-compressed data and decompressing it.
@@ -961,8 +970,8 @@ public class Base64
 	{
 		return decode( s, NO_OPTIONS );
 	}
-    
-    
+
+
     /**
      * Decodes data from Base64 notation, automatically
      * detecting gzip-compressed data and decompressing it.
@@ -973,7 +982,7 @@ public class Base64
      * @since 1.4
      */
     public static byte[] decode( String s, int options )
-    {   
+    {
         byte[] bytes;
         try
         {
@@ -984,18 +993,18 @@ public class Base64
             bytes = s.getBytes();
         }   // end catch
 		//</change>
-        
+
         // Decode
         bytes = decode( bytes, 0, bytes.length, options );
-        
-        
+
+
         // Check to see if it's gzip-compressed
         // GZIP Magic Two-Byte Number: 0x8b1f (35615)
         if( bytes != null && bytes.length >= 4 )
         {
-            
-            int head = ((int)bytes[0] & 0xff) | ((bytes[1] << 8) & 0xff00);       
-            if( java.util.zip.GZIPInputStream.GZIP_MAGIC == head ) 
+
+            int head = (bytes[0] & 0xff) | ((bytes[1] << 8) & 0xff00);
+            if( java.util.zip.GZIPInputStream.GZIP_MAGIC == head )
             {
                 java.io.ByteArrayInputStream  bais = null;
                 java.util.zip.GZIPInputStream gzis = null;
@@ -1031,12 +1040,12 @@ public class Base64
 
             }   // end if: gzipped
         }   // end if: bytes.length >= 2
-        
+
         return bytes;
     }   // end decode
 
 
-    
+
 
     /**
      * Attempts to decode Base64 data and deserialize a Java
@@ -1050,16 +1059,16 @@ public class Base64
     {
         // Decode and gunzip if necessary
         byte[] objBytes = decode( encodedObject );
-        
+
         java.io.ByteArrayInputStream  bais = null;
         java.io.ObjectInputStream     ois  = null;
         Object obj = null;
-        
+
         try
         {
             bais = new java.io.ByteArrayInputStream( objBytes );
             ois  = new java.io.ObjectInputStream( bais );
-        
+
             obj = ois.readObject();
         }   // end try
         catch( java.io.IOException e )
@@ -1077,12 +1086,12 @@ public class Base64
             try{ bais.close(); } catch( Exception e ){}
             try{ ois.close();  } catch( Exception e ){}
         }   // end finally
-        
+
         return obj;
     }   // end decodeObject
-    
-    
-    
+
+
+
     /**
      * Convenience method for encoding data to a file.
      *
@@ -1098,25 +1107,25 @@ public class Base64
         Base64.OutputStream bos = null;
         try
         {
-            bos = new Base64.OutputStream( 
+            bos = new Base64.OutputStream(
                       new java.io.FileOutputStream( filename ), Base64.ENCODE );
             bos.write( dataToEncode );
             success = true;
         }   // end try
         catch( java.io.IOException e )
         {
-            
+
             success = false;
         }   // end catch: IOException
         finally
         {
             try{ bos.close(); } catch( Exception e ){}
         }   // end finally
-        
+
         return success;
     }   // end encodeToFile
-    
-    
+
+
     /**
      * Convenience method for decoding data to a file.
      *
@@ -1132,7 +1141,7 @@ public class Base64
         Base64.OutputStream bos = null;
         try
         {
-                bos = new Base64.OutputStream( 
+                bos = new Base64.OutputStream(
                           new java.io.FileOutputStream( filename ), Base64.DECODE );
                 bos.write( dataToDecode.getBytes( PREFERRED_ENCODING ) );
                 success = true;
@@ -1145,13 +1154,13 @@ public class Base64
         {
                 try{ bos.close(); } catch( Exception e ){}
         }   // end finally
-        
+
         return success;
     }   // end decodeToFile
-    
-    
-    
-    
+
+
+
+
     /**
      * Convenience method for reading a base64-encoded
      * file and decoding it.
@@ -1172,7 +1181,7 @@ public class Base64
             byte[] buffer = null;
             int length   = 0;
             int numBytes = 0;
-            
+
             // Check for size of file
             if( file.length() > Integer.MAX_VALUE )
             {
@@ -1180,20 +1189,21 @@ public class Base64
                 return null;
             }   // end if: file too big for int index
             buffer = new byte[ (int)file.length() ];
-            
+
             // Open a stream
-            bis = new Base64.InputStream( 
-                      new java.io.BufferedInputStream( 
+            bis = new Base64.InputStream(
+                      new java.io.BufferedInputStream(
                       new java.io.FileInputStream( file ) ), Base64.DECODE );
-            
+
             // Read until done
-            while( ( numBytes = bis.read( buffer, length, 4096 ) ) >= 0 )
+            while( ( numBytes = bis.read( buffer, length, 4096 ) ) >= 0 ) {
                 length += numBytes;
-            
+            }
+
             // Save in a variable to return
             decodedData = new byte[ length ];
             System.arraycopy( buffer, 0, decodedData, 0, length );
-            
+
         }   // end try
         catch( java.io.IOException e )
         {
@@ -1203,12 +1213,12 @@ public class Base64
         {
             try{ bis.close(); } catch( Exception e) {}
         }   // end finally
-        
+
         return decodedData;
     }   // end decodeFromFile
-    
-    
-    
+
+
+
     /**
      * Convenience method for reading a binary file
      * and base64-encoding it.
@@ -1229,19 +1239,20 @@ public class Base64
             byte[] buffer = new byte[ Math.max((int)(file.length() * 1.4),40) ]; // Need max() for math on small files (v2.2.1)
             int length   = 0;
             int numBytes = 0;
-            
+
             // Open a stream
-            bis = new Base64.InputStream( 
-                      new java.io.BufferedInputStream( 
+            bis = new Base64.InputStream(
+                      new java.io.BufferedInputStream(
                       new java.io.FileInputStream( file ) ), Base64.ENCODE );
-            
+
             // Read until done
-            while( ( numBytes = bis.read( buffer, length, 4096 ) ) >= 0 )
+            while( ( numBytes = bis.read( buffer, length, 4096 ) ) >= 0 ) {
                 length += numBytes;
-            
+            }
+
             // Save in a variable to return
             encodedData = new String( buffer, 0, length, Base64.PREFERRED_ENCODING );
-                
+
         }   // end try
         catch( java.io.IOException e )
         {
@@ -1251,13 +1262,13 @@ public class Base64
         {
             try{ bis.close(); } catch( Exception e) {}
         }   // end finally
-        
+
         return encodedData;
         }   // end encodeFromFile
-    
-    
-    
-    
+
+
+
+
     /**
      * Reads <tt>infile</tt> and encodes it to <tt>outfile</tt>.
      *
@@ -1272,9 +1283,9 @@ public class Base64
         java.io.InputStream in = null;
         java.io.OutputStream out = null;
         try{
-            in  = new Base64.InputStream( 
-                      new java.io.BufferedInputStream( 
-                      new java.io.FileInputStream( infile ) ), 
+            in  = new Base64.InputStream(
+                      new java.io.BufferedInputStream(
+                      new java.io.FileInputStream( infile ) ),
                       Base64.ENCODE );
             out = new java.io.BufferedOutputStream( new java.io.FileOutputStream( outfile ) );
             byte[] buffer = new byte[65536]; // 64K
@@ -1289,12 +1300,12 @@ public class Base64
             try{ in.close();  } catch( Exception exc ){}
             try{ out.close(); } catch( Exception exc ){}
         }   // end finally
-        
+
         return success;
     }   // end encodeFileToFile
-    
-    
-    
+
+
+
     /**
      * Reads <tt>infile</tt> and decodes it to <tt>outfile</tt>.
      *
@@ -1309,9 +1320,9 @@ public class Base64
         java.io.InputStream in = null;
         java.io.OutputStream out = null;
         try{
-            in  = new Base64.InputStream( 
-                      new java.io.BufferedInputStream( 
-                      new java.io.FileInputStream( infile ) ), 
+            in  = new Base64.InputStream(
+                      new java.io.BufferedInputStream(
+                      new java.io.FileInputStream( infile ) ),
                       Base64.DECODE );
             out = new java.io.BufferedOutputStream( new java.io.FileOutputStream( outfile ) );
             byte[] buffer = new byte[65536]; // 64K
@@ -1326,15 +1337,15 @@ public class Base64
             try{ in.close();  } catch( Exception exc ){}
             try{ out.close(); } catch( Exception exc ){}
         }   // end finally
-        
+
         return success;
     }   // end decodeFileToFile
-    
-    
+
+
     /* ********  I N N E R   C L A S S   I N P U T S T R E A M  ******** */
-    
-    
-    
+
+
+
     /**
      * A {@link Base64.InputStream} will read data from another
      * <tt>java.io.InputStream</tt>, given in the constructor,
@@ -1355,8 +1366,8 @@ public class Base64
 		private int     options;        // Record options used to create the stream.
 		private byte[]  alphabet;	    // Local copies to avoid extra method calls
 		private byte[]  decodabet;		// Local copies to avoid extra method calls
-        
-        
+
+
         /**
          * Constructs a {@link Base64.InputStream} in DECODE mode.
          *
@@ -1364,11 +1375,11 @@ public class Base64
          * @since 1.3
          */
         public InputStream( java.io.InputStream in )
-        {   
+        {
             this( in, DECODE );
         }   // end constructor
-        
-        
+
+
         /**
          * Constructs a {@link Base64.InputStream} in
          * either ENCODE or DECODE mode.
@@ -1391,7 +1402,7 @@ public class Base64
          * @since 2.0
          */
         public InputStream( java.io.InputStream in, int options )
-        {   
+        {
             super( in );
             this.breakLines   = (options & DONT_BREAK_LINES) != DONT_BREAK_LINES;
             this.encode       = (options & ENCODE) == ENCODE;
@@ -1403,7 +1414,7 @@ public class Base64
 			this.alphabet     = getAlphabet(options);
 			this.decodabet    = getDecodabet(options);
         }   // end constructor
-        
+
         /**
          * Reads enough of the input stream to convert
          * to/from Base64 and returns the next byte.
@@ -1411,8 +1422,9 @@ public class Base64
          * @return next byte
          * @since 1.3
          */
-        public int read() throws java.io.IOException 
-        { 
+        @Override
+        public int read() throws java.io.IOException
+        {
             // Do we need to get data?
             if( position < 0 )
             {
@@ -1423,26 +1435,27 @@ public class Base64
                     for( int i = 0; i < 3; i++ )
                     {
                         try
-                        { 
+                        {
                             int b = in.read();
-                            
+
                             // If end of stream, b is -1.
                             if( b >= 0 )
                             {
                                 b3[i] = (byte)b;
                                 numBinaryBytes++;
                             }   // end if: not end of stream
-                            
+
                         }   // end try: read
                         catch( java.io.IOException e )
-                        {   
+                        {
                             // Only a problem if we got no data at all.
-                            if( i == 0 )
+                            if( i == 0 ) {
                                 throw e;
-                            
+                            }
+
                         }   // end catch
                     }   // end for: each needed input byte
-                    
+
                     if( numBinaryBytes > 0 )
                     {
                         encode3to4( b3, 0, numBinaryBytes, buffer, 0, options );
@@ -1454,7 +1467,7 @@ public class Base64
                         return -1;
                     }   // end else
                 }   // end if: encoding
-                
+
                 // Else decoding
                 else
                 {
@@ -1466,13 +1479,14 @@ public class Base64
                         int b = 0;
                         do{ b = in.read(); }
                         while( b >= 0 && decodabet[ b & 0x7f ] <= WHITE_SPACE_ENC );
-                        
-                        if( b < 0 )
+
+                        if( b < 0 ) {
                             break; // Reads a -1 if end of stream
-                        
+                        }
+
                         b4[i] = (byte)b;
                     }   // end for: each needed input byte
-                    
+
                     if( i == 4 )
                     {
                         numSigBytes = decode4to3( b4, 0, buffer, 0, options );
@@ -1485,18 +1499,19 @@ public class Base64
                     {
                         // Must have broken out from above.
                         throw new java.io.IOException( "Improperly padded Base64 input." );
-                    }   // end 
-                    
+                    }   // end
+
                 }   // end else: decode
             }   // end else: get data
-            
+
             // Got data?
             if( position >= 0 )
             {
                 // End of relevant data?
-                if( /*!encode &&*/ position >= numSigBytes )
+                if( /*!encode &&*/ position >= numSigBytes ) {
                     return -1;
-                
+                }
+
                 if( encode && breakLines && lineLength >= MAX_LINE_LENGTH )
                 {
                     lineLength = 0;
@@ -1507,26 +1522,27 @@ public class Base64
                     lineLength++;   // This isn't important when decoding
                                     // but throwing an extra "if" seems
                                     // just as wasteful.
-                    
+
                     int b = buffer[ position++ ];
 
-                    if( position >= bufferLength )
+                    if( position >= bufferLength ) {
                         position = -1;
+                    }
 
                     return b & 0xFF; // This is how you "cast" a byte that's
                                      // intended to be unsigned.
                 }   // end else
             }   // end if: position >= 0
-            
+
             // Else error
             else
-            {   
+            {
                 // When JDK1.4 is more accepted, use an assertion here.
                 throw new java.io.IOException( "Error in Base64 code reading stream." );
             }   // end else
         }   // end read
-        
-        
+
+
         /**
          * Calls {@link #read()} repeatedly until the end of stream
          * is reached or <var>len</var> bytes are read.
@@ -1539,6 +1555,7 @@ public class Base64
          * @return bytes read into array or -1 if end of stream is encountered.
          * @since 1.3
          */
+        @Override
         public int read( byte[] dest, int off, int len ) throws java.io.IOException
         {
             int i;
@@ -1546,31 +1563,32 @@ public class Base64
             for( i = 0; i < len; i++ )
             {
                 b = read();
-                
+
                 //if( b < 0 && i == 0 )
                 //    return -1;
-                
-                if( b >= 0 )
+
+                if( b >= 0 ) {
                     dest[off + i] = (byte)b;
-                else if( i == 0 )
+                } else if( i == 0 ) {
                     return -1;
-                else
+                } else {
                     break; // Out of 'for' loop
+                }
             }   // end for: each byte read
             return i;
         }   // end read
-        
+
     }   // end inner class InputStream
-    
-    
-    
-    
-    
-    
+
+
+
+
+
+
     /* ********  I N N E R   C L A S S   O U T P U T S T R E A M  ******** */
-    
-    
-    
+
+
+
     /**
      * A {@link Base64.OutputStream} will write data to another
      * <tt>java.io.OutputStream</tt>, given in the constructor,
@@ -1592,7 +1610,7 @@ public class Base64
 		private int options; // Record for later
 		private byte[]  alphabet;	    // Local copies to avoid extra method calls
 		private byte[]  decodabet;		// Local copies to avoid extra method calls
-        
+
         /**
          * Constructs a {@link Base64.OutputStream} in ENCODE mode.
          *
@@ -1600,11 +1618,11 @@ public class Base64
          * @since 1.3
          */
         public OutputStream( java.io.OutputStream out )
-        {   
+        {
             this( out, ENCODE );
         }   // end constructor
-        
-        
+
+
         /**
          * Constructs a {@link Base64.OutputStream} in
          * either ENCODE or DECODE mode.
@@ -1626,7 +1644,7 @@ public class Base64
          * @since 1.3
          */
         public OutputStream( java.io.OutputStream out, int options )
-        {   
+        {
             super( out );
             this.breakLines   = (options & DONT_BREAK_LINES) != DONT_BREAK_LINES;
             this.encode       = (options & ENCODE) == ENCODE;
@@ -1640,8 +1658,8 @@ public class Base64
 			this.alphabet     = getAlphabet(options);
 			this.decodabet    = getDecodabet(options);
         }   // end constructor
-        
-        
+
+
         /**
          * Writes the byte to the output stream after
          * converting to/from Base64 notation.
@@ -1654,6 +1672,7 @@ public class Base64
          * @param theByte the byte to write
          * @since 1.3
          */
+        @Override
         public void write(int theByte) throws java.io.IOException
         {
             // Encoding suspended?
@@ -1662,7 +1681,7 @@ public class Base64
                 super.out.write( theByte );
                 return;
             }   // end if: supsended
-            
+
             // Encode?
             if( encode )
             {
@@ -1703,11 +1722,11 @@ public class Base64
                 }   // end else: not white space either
             }   // end else: decoding
         }   // end write
-        
-        
-        
+
+
+
         /**
-         * Calls {@link #write(int)} repeatedly until <var>len</var> 
+         * Calls {@link #write(int)} repeatedly until <var>len</var>
          * bytes are written.
          *
          * @param theBytes array from which to read bytes
@@ -1715,6 +1734,7 @@ public class Base64
          * @param len max number of bytes to read into array
          * @since 1.3
          */
+        @Override
         public void write( byte[] theBytes, int off, int len ) throws java.io.IOException
         {
             // Encoding suspended?
@@ -1723,21 +1743,21 @@ public class Base64
                 super.out.write( theBytes, off, len );
                 return;
             }   // end if: supsended
-            
+
             for( int i = 0; i < len; i++ )
             {
                 write( theBytes[ off + i ] );
             }   // end for: each byte written
-            
+
         }   // end write
-        
-        
-        
+
+
+
         /**
          * Method added by PHIL. [Thanks, PHIL. -Rob]
          * This pads the buffer without closing the stream.
          */
-        public void flushBase64() throws java.io.IOException 
+        public void flushBase64() throws java.io.IOException
         {
             if( position > 0 )
             {
@@ -1754,12 +1774,13 @@ public class Base64
 
         }   // end flush
 
-        
-        /** 
-         * Flushes and closes (I think, in the superclass) the stream. 
+
+        /**
+         * Flushes and closes (I think, in the superclass) the stream.
          *
          * @since 1.3
          */
+        @Override
         public void close() throws java.io.IOException
         {
             // 1. Ensure that pending characters are written
@@ -1768,13 +1789,13 @@ public class Base64
             // 2. Actually close the stream
             // Base class both flushes and closes.
             super.close();
-            
+
             buffer = null;
             out    = null;
         }   // end close
-        
-        
-        
+
+
+
         /**
          * Suspends encoding of the stream.
          * May be helpful if you need to embed a piece of
@@ -1782,13 +1803,13 @@ public class Base64
          *
          * @since 1.5.1
          */
-        public void suspendEncoding() throws java.io.IOException 
+        public void suspendEncoding() throws java.io.IOException
         {
             flushBase64();
             this.suspendEncoding = true;
         }   // end suspendEncoding
-        
-        
+
+
         /**
          * Resumes encoding of the stream.
          * May be helpful if you need to embed a piece of
@@ -1800,10 +1821,10 @@ public class Base64
         {
             this.suspendEncoding = false;
         }   // end resumeEncoding
-        
-        
-        
+
+
+
     }   // end inner class OutputStream
-    
-    
+
+
 }   // end class Base64
