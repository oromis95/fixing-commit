@@ -305,6 +305,12 @@ public class TestXmlBuilder extends TestCase {
             fail();
         } catch (XPathExpressionException e) {}
 
+        // Users are prevented from creating elements that reference
+        // an undefined namespace prefix.
+        try {
+            builder.element("undefined-prefix:ElementName");
+            fail("Element cannot be created with prefix that is undefined");
+        } catch (IllegalArgumentException e) {}
     }
 
 }
