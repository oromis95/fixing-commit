@@ -2,10 +2,16 @@ package storm.kafka;
 
 import com.netflix.curator.framework.CuratorFramework;
 import com.netflix.curator.framework.CuratorFrameworkFactory;
+import com.netflix.curator.framework.imps.CuratorFrameworkState;
 import com.netflix.curator.retry.ExponentialBackoffRetry;
+import com.netflix.curator.test.InstanceSpec;
 import com.netflix.curator.test.TestingServer;
+import kafka.server.KafkaConfig;
 import kafka.server.KafkaServerStartable;
+import org.apache.commons.io.FileUtils;
 
+import java.io.File;
+import java.io.IOException;
 import java.util.Properties;
 
 /**
@@ -14,23 +20,20 @@ import java.util.Properties;
  */
 public class KafkaTestBroker {
 
-    private final int port = 49123;
+    private int port;
     private KafkaServerStartable kafka;
     private TestingServer server;
-    private String zookeeperConnectionString;
+    private CuratorFramework zookeeper;
 
     public KafkaTestBroker() {
         try {
             server = new TestingServer();
-            zookeeperConnectionString = server.getConnectString();
+            String zookeeperConnectionString = server.getConnectString();
             ExponentialBackoffRetry retryPolicy = new ExponentialBackoffRetry(1000, 3);
-            CuratorFramework zookeeper = CuratorFrameworkFactory.newClient(zookeeperConnectionString, retryPolicy);
+            zookeeper = CuratorFrameworkFactory.newClient(zookeeperConnectionString, retryPolicy);
             zookeeper.start();
-            Properties p = new Properties();
-            p.setProperty("zookeeper.connect", zookeeperConnectionString);
-            p.setProperty("broker.id", "0");
-            p.setProperty("port", "" + port);
-            kafka.server.KafkaConfig config = new kafka.server.KafkaConfig(p);
+            port = InstanceSpec.getRandomPort();
+            kafka.server.KafkaConfig config = buildKafkaConfig(zookeeperConnectionString);
             kafka = new KafkaServerStartable(config);
             kafka.startup();
         } catch (Exception ex) {
@@ -38,6 +41,19 @@ public class KafkaTestBroker {
         }
     }
 
+    private kafka.server.KafkaConfig buildKafkaConfig(String zookeeperConnectionString) {
+        Properties p = new Properties();
+        p.setProperty("zookeeper.connect", zookeeperConnectionString);
+        p.setProperty("broker.id", "0");
+        p.setProperty("port", "" + port);
+        p.setProperty("log.dirs", getLogDir());
+        return new KafkaConfig(p);
+    }
+
+    private String getLogDir() {
+        return System.getProperty("java.io.tmpdir") + "kafka/logs/kafka-test-" + port;
+    }
+
     public String getBrokerConnectionString() {
         return "localhost:" + port;
     }
@@ -46,8 +62,12 @@ public class KafkaTestBroker {
         return port;
     }
 
-    public void shutdown() {
+    public void shutdown() throws IOException {
         kafka.shutdown();
-        server.stop();
+        if (zookeeper.getState().equals(CuratorFrameworkState.STARTED)) {
+            zookeeper.close();
+        }
+        server.close();
+        FileUtils.deleteQuietly(new File(getLogDir()));
     }
 }
