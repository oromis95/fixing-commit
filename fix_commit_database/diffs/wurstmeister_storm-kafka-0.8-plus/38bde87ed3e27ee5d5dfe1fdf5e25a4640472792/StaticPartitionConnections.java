@@ -23,7 +23,7 @@ public class StaticPartitionConnections {
         int hostIndex = partition / hosts.partitionsPerHost;
         if(!_kafka.containsKey(hostIndex)) {
             HostPort hp = hosts.hosts.get(hostIndex);
-            _kafka.put(hostIndex, new SimpleConsumer(hp.host, hp.port, _config.socketTimeoutMs, _config.bufferSizeBytes));
+            _kafka.put(hostIndex, new SimpleConsumer(hp.host, hp.port, _config.socketTimeoutMs, _config.bufferSizeBytes, kafka.api.OffsetRequest.DefaultClientId()));
 
         }
         return _kafka.get(hostIndex);
