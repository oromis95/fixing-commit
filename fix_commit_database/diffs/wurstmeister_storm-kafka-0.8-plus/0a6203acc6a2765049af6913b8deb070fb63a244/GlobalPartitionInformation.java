@@ -64,4 +64,30 @@ public class GlobalPartitionInformation implements Iterable<Partition>, Serializ
             }
         };
     }
+
+    @Override
+    public int hashCode() {
+        final int prime = 31;
+        int result = 1;
+        result = prime * result
+                + ((partitionMap == null) ? 0 : partitionMap.hashCode());
+        return result;
+    }
+
+    @Override
+    public boolean equals(Object obj) {
+        if (this == obj)
+            return true;
+        if (obj == null)
+            return false;
+        if (getClass() != obj.getClass())
+            return false;
+        GlobalPartitionInformation other = (GlobalPartitionInformation) obj;
+        if (partitionMap == null) {
+            if (other.partitionMap != null)
+                return false;
+        } else if (!partitionMap.equals(other.partitionMap))
+            return false;
+        return true;
+    }
 }
