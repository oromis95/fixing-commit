@@ -1,4 +1,4 @@
-package io.leangen.graphql.generator.mapping.common;
+package io.leangen.graphql.generator.mapping.strategy;
 
 import java.lang.reflect.AnnotatedType;
 import java.util.Map;
@@ -6,10 +6,10 @@ import java.util.Map;
 import io.leangen.geantyref.GenericTypeReflector;
 
 /**
- * @author Bojan Tomic (kaqqao)
+ * A scalar mapping strategy that treats all {@link Map}s as scalars
  */
-public class MapScalarAdapter extends ObjectScalarAdapter {
-
+public class MapScalarStrategy implements ScalarMappingStrategy {
+    
     @Override
     public boolean supports(AnnotatedType type) {
         return GenericTypeReflector.isSuperType(Map.class, type.getType());
