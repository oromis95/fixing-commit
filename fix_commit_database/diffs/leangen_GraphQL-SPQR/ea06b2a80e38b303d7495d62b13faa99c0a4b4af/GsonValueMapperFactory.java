@@ -7,6 +7,7 @@ import com.google.gson.TypeAdapterFactory;
 import net.dongliu.gson.GsonJava8TypeAdapterFactory;
 
 import java.lang.reflect.Type;
+import java.util.Objects;
 import java.util.Set;
 
 import io.leangen.geantyref.GenericTypeReflector;
@@ -22,28 +23,24 @@ import io.leangen.graphql.util.ClassUtils;
  */
 public class GsonValueMapperFactory implements ValueMapperFactory<GsonValueMapper> {
 
-    private final String basePackage;
+    private final String[] basePackages;
     private final FieldNamingStrategy fieldNamingStrategy;
     private final TypeInfoGenerator typeInfoGenerator;
     private final Configurer configurer;
 
     public GsonValueMapperFactory() {
-        this(null);
+        this(new String[0]);
     }
 
-    public GsonValueMapperFactory(String basePackage) {
-        this(basePackage, new DefaultTypeInfoGenerator());
+    public GsonValueMapperFactory(String... basePackages) {
+        this(basePackages, new DefaultTypeInfoGenerator(), new GsonFieldNamingStrategy(), new AbstractClassAdapterConfigurer());
     }
 
-    public GsonValueMapperFactory(String basePackage, TypeInfoGenerator typeInfoGenerator) {
-        this(basePackage, typeInfoGenerator, new GsonFieldNamingStrategy(), new AbstractClassAdapterConfigurer());
-    }
-
-    public GsonValueMapperFactory(String basePackage, TypeInfoGenerator typeInfoGenerator, FieldNamingStrategy fieldNamingStrategy, Configurer configurer) {
-        this.basePackage = basePackage;
-        this.fieldNamingStrategy = fieldNamingStrategy;
-        this.typeInfoGenerator = typeInfoGenerator;
-        this.configurer = configurer;
+    private GsonValueMapperFactory(String[] basePackages, TypeInfoGenerator typeInfoGenerator, FieldNamingStrategy fieldNamingStrategy, Configurer configurer) {
+        this.basePackages = basePackages;
+        this.fieldNamingStrategy = Objects.requireNonNull(fieldNamingStrategy);
+        this.typeInfoGenerator = Objects.requireNonNull(typeInfoGenerator);
+        this.configurer = Objects.requireNonNull(configurer);
     }
 
     @Override
@@ -51,21 +48,25 @@ public class GsonValueMapperFactory implements ValueMapperFactory<GsonValueMappe
         return new GsonValueMapper(initBuilder(this.fieldNamingStrategy, abstractTypes, this.configurer, environment).create());
     }
 
+    public static Builder builder() {
+        return new Builder();
+    }
+
     private GsonBuilder initBuilder(FieldNamingStrategy fieldNamingStrategy, Set<Type> abstractTypes, Configurer configurer, GlobalEnvironment environment) {
         GsonBuilder gsonBuilder = new GsonBuilder()
                 .setFieldNamingStrategy(fieldNamingStrategy)
                 .registerTypeAdapterFactory(new GsonJava8TypeAdapterFactory());
-        return configurer.configure(gsonBuilder, abstractTypes, basePackage, this.typeInfoGenerator, environment);
+        return configurer.configure(gsonBuilder, abstractTypes, basePackages, this.typeInfoGenerator, environment);
     }
 
     public static class AbstractClassAdapterConfigurer implements Configurer {
 
         @Override
-        public GsonBuilder configure(GsonBuilder gsonBuilder, Set<Type> abstractTypes, String basePackage, TypeInfoGenerator infoGenerator, GlobalEnvironment environment) {
+        public GsonBuilder configure(GsonBuilder gsonBuilder, Set<Type> abstractTypes, String[] basePackages, TypeInfoGenerator infoGenerator, GlobalEnvironment environment) {
             abstractTypes.stream()
                     .map(ClassUtils::getRawType)
                     .distinct()
-                    .map(abstractType -> adapterFor(abstractType, basePackage, infoGenerator))
+                    .map(abstractType -> adapterFor(abstractType, basePackages, infoGenerator))
                     .forEach(gsonBuilder::registerTypeAdapterFactory);
 
             if (environment != null && !environment.getInputConverters().isEmpty()) {
@@ -76,10 +77,10 @@ public class GsonValueMapperFactory implements ValueMapperFactory<GsonValueMappe
         }
 
         @SuppressWarnings("unchecked")
-        private TypeAdapterFactory adapterFor(Class superClass, String basePackage, TypeInfoGenerator infoGen) {
+        private TypeAdapterFactory adapterFor(Class superClass, String[] basePackages, TypeInfoGenerator infoGen) {
             RuntimeTypeAdapterFactory adapterFactory = RuntimeTypeAdapterFactory.of(superClass, ValueMapper.TYPE_METADATA_FIELD_NAME);
 
-            ClassUtils.findImplementations(superClass, basePackage).stream()
+            ClassUtils.findImplementations(superClass, basePackages).stream()
                     .filter(impl -> !ClassUtils.isAbstract(impl))
                     .forEach(impl -> adapterFactory.registerSubtype(impl, infoGen.generateTypeName(GenericTypeReflector.annotate(impl))));
 
@@ -89,11 +90,43 @@ public class GsonValueMapperFactory implements ValueMapperFactory<GsonValueMappe
 
     @FunctionalInterface
     public interface Configurer {
-        GsonBuilder configure(GsonBuilder gsonBuilder, Set<Type> abstractTypes, String basePackage, TypeInfoGenerator infoGenerator, GlobalEnvironment environment);
+        GsonBuilder configure(GsonBuilder gsonBuilder, Set<Type> abstractTypes, String[] basePackages, TypeInfoGenerator infoGenerator, GlobalEnvironment environment);
     }
 
     @Override
     public String toString() {
         return this.getClass().getSimpleName() + " with " + typeInfoGenerator.getClass().getSimpleName();
     }
+
+    public static class Builder {
+
+        private String[] basePackages;
+        private FieldNamingStrategy fieldNamingStrategy = new GsonFieldNamingStrategy();
+        private TypeInfoGenerator typeInfoGenerator = new DefaultTypeInfoGenerator();
+        private Configurer configurer = new AbstractClassAdapterConfigurer();
+
+        public Builder withBasePackages(String... basePackages) {
+            this.basePackages = basePackages;
+            return this;
+        }
+
+        public Builder withFieldNamingStrategy(FieldNamingStrategy fieldNamingStrategy) {
+            this.fieldNamingStrategy = fieldNamingStrategy;
+            return this;
+        }
+
+        public Builder withTypeInfoGenerator(TypeInfoGenerator typeInfoGenerator) {
+            this.typeInfoGenerator = typeInfoGenerator;
+            return this;
+        }
+
+        public Builder withConfigurer(Configurer configurer) {
+            this.configurer = configurer;
+            return this;
+        }
+
+        public GsonValueMapperFactory build() {
+            return new GsonValueMapperFactory(basePackages, typeInfoGenerator, fieldNamingStrategy, configurer);
+        }
+    }
 }
