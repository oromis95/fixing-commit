@@ -1,14 +1,17 @@
 package io.leangen.graphql.metadata.strategy.query;
 
 import io.leangen.graphql.metadata.QueryResolver;
+import io.leangen.graphql.query.execution.MethodInvoker;
+import io.leangen.graphql.query.execution.SingletonMethodInvoker;
+import io.leangen.graphql.util.ClassUtils;
 
 import java.lang.reflect.AnnotatedType;
 import java.lang.reflect.Member;
 import java.lang.reflect.Method;
-import java.lang.reflect.Type;
+import java.util.Arrays;
 import java.util.Collection;
-import java.util.function.BiFunction;
 import java.util.function.Predicate;
+import java.util.stream.Collectors;
 
 /**
  * Created by bojan.tomic on 6/10/16.
@@ -28,35 +31,47 @@ public class PublicResolverExtractor implements ResolverExtractor {
     }
 
     @Override
-    public Collection<QueryResolver> extractQueryResolvers(Object querySourceBean, AnnotatedType type) {
-        return extractQueryResolvers(querySourceBean, type, acceptAll);
+    public Collection<QueryResolver> extractQueryResolvers(Object querySourceBean, AnnotatedType beanType) {
+        return extractQueryResolvers(querySourceBean, beanType, acceptAll);
     }
 
     @Override
-    public Collection<QueryResolver> extractMutationResolvers(Object querySourceBean, AnnotatedType type) {
-        return extractMutationResolvers(querySourceBean, type, acceptAll);
+    public Collection<QueryResolver> extractMutationResolvers(Object querySourceBean, AnnotatedType beanType) {
+        return extractMutationResolvers(querySourceBean, beanType, acceptAll);
     }
 
     @Override
-    public Collection<QueryResolver> extractQueryResolvers(Object querySourceBean, AnnotatedType type, Predicate<Member>... filters) {
-//		return extractResolvers(querySourceBean, type, queryNameGenerator::generateQueryName, this::isQuery, filters);
-        return null;
+    public Collection<QueryResolver> extractQueryResolvers(Object querySourceBean, AnnotatedType beanType, Predicate<Member>... filters) {
+        Class<?> rawType = ClassUtils.getRawType(beanType.getType());
+        return Arrays.stream(rawType.getMethods())
+                .filter(method -> rawType.getPackage().equals(method.getDeclaringClass().getPackage()))
+                .filter(this::isQuery)
+                .filter(Arrays.stream(filters).reduce(Predicate::and).orElse(acceptAll))
+                .map(method -> new QueryResolver(
+                        queryNameGenerator.generateQueryName(method, beanType),
+                        queryNameGenerator.generateQueryName(method, beanType),
+                        false,
+                        querySourceBean == null ? new MethodInvoker(method, beanType) : new SingletonMethodInvoker(querySourceBean, method, beanType),
+                        argumentExtractor.extractResolverArguments(method, beanType)
+                ))
+                .collect(Collectors.toList());
     }
 
     @Override
-    public Collection<QueryResolver> extractMutationResolvers(Object querySourceBean, AnnotatedType type, Predicate<Member>... filters) {
-//		return extractResolvers(querySourceBean, type, queryNameGenerator::generateMutationName, this::isMutation, filters);
-        return null;
-    }
-
-    protected Collection<QueryResolver> extractResolvers(Object querySourceBean, Type type, BiFunction<Method, Type, String> queryNamer,
-                                                         Predicate<Method> mandatoryFilter, Predicate<Method>... extraFilters) {
-//		return Arrays.stream(ClassUtils.getRawType(type).getMethods())
-//				.filter(method -> ClassUtils.getRawType(type).getPackage().equals(method.getDeclaringClass().getPackage()))
-//				.filter(mandatoryFilter)
-//				.filter(Arrays.stream(extraFilters).reduce(Predicate::and).orElse(acceptAll))
-//				.collect(toResolvers(querySourceBean, type, queryNamer, Method::getName, argumentExtractor));
-        return null;
+    public Collection<QueryResolver> extractMutationResolvers(Object querySourceBean, AnnotatedType beanType, Predicate<Member>... filters) {
+        Class<?> rawType = ClassUtils.getRawType(beanType.getType());
+        return Arrays.stream(rawType.getMethods())
+                .filter(method -> rawType.getPackage().equals(method.getDeclaringClass().getPackage()))
+                .filter(this::isMutation)
+                .filter(Arrays.stream(filters).reduce(Predicate::and).orElse(acceptAll))
+                .map(method -> new QueryResolver(
+                        queryNameGenerator.generateMutationName(method, beanType),
+                        queryNameGenerator.generateMutationName(method, beanType),
+                        false,
+                        querySourceBean == null ? new MethodInvoker(method, beanType) : new SingletonMethodInvoker(querySourceBean, method, beanType),
+                        argumentExtractor.extractResolverArguments(method, beanType)
+                ))
+                .collect(Collectors.toList());
     }
 
     protected boolean isQuery(Method method) {
