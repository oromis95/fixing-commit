@@ -8,6 +8,7 @@ import java.lang.annotation.Target;
 /**
  * Created by bojan.tomic on 3/16/16.
  */
+@GraphQLIgnore
 @Retention(RetentionPolicy.RUNTIME)
 @Target(ElementType.PARAMETER)
 public @interface GraphQLContext {
