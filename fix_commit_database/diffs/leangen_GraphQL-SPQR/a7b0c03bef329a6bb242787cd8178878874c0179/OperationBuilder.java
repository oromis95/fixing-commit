@@ -1,5 +1,6 @@
 package io.leangen.graphql.metadata.strategy.query;
 
+import java.lang.reflect.Type;
 import java.util.List;
 
 import io.leangen.graphql.metadata.Operation;
@@ -10,6 +11,6 @@ import io.leangen.graphql.metadata.Resolver;
  */
 public interface OperationBuilder {
 
-    Operation buildQuery(List<Resolver> resolvers);
-    Operation buildMutation(List<Resolver> resolvers);
+    Operation buildQuery(Type context, List<Resolver> resolvers);
+    Operation buildMutation(Type context, List<Resolver> resolvers);
 }
