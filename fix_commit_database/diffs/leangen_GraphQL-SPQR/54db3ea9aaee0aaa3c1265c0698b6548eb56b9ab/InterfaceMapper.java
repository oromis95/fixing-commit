@@ -1,12 +1,5 @@
 package io.leangen.graphql.generator.mapping.common;
 
-import java.lang.reflect.AnnotatedType;
-import java.lang.reflect.Type;
-import java.util.List;
-import java.util.Objects;
-import java.util.Optional;
-import java.util.Set;
-
 import graphql.schema.GraphQLFieldDefinition;
 import graphql.schema.GraphQLInputObjectType;
 import graphql.schema.GraphQLInterfaceType;
@@ -19,6 +12,11 @@ import io.leangen.graphql.generator.types.MappedGraphQLInterfaceType;
 import io.leangen.graphql.util.ClassUtils;
 import io.leangen.graphql.util.Utils;
 
+import java.lang.reflect.AnnotatedType;
+import java.util.List;
+import java.util.Objects;
+import java.util.Optional;
+
 import static graphql.schema.GraphQLInterfaceType.newInterface;
 
 /**
@@ -35,7 +33,7 @@ public class InterfaceMapper extends CachingMapper<GraphQLInterfaceType, GraphQL
     }
 
     @Override
-    public GraphQLInterfaceType toGraphQLType(String typeName, AnnotatedType javaType, Set<Type> abstractTypes, OperationMapper operationMapper, BuildContext buildContext) {
+    public GraphQLInterfaceType toGraphQLType(String typeName, AnnotatedType javaType, OperationMapper operationMapper, BuildContext buildContext) {
         GraphQLInterfaceType.Builder typeBuilder = newInterface()
                 .name(typeName)
                 .description(buildContext.typeInfoGenerator.generateTypeDescription(javaType));
@@ -54,7 +52,7 @@ public class InterfaceMapper extends CachingMapper<GraphQLInterfaceType, GraphQL
                     scanPackages = buildContext.basePackages;
                 }
                 ClassUtils.findImplementations(javaType, scanPackages).forEach(impl ->
-                        getImplementingType(impl, abstractTypes, operationMapper, buildContext)
+                        getImplementingType(impl, operationMapper, buildContext)
                                 .ifPresent(implType -> buildContext.typeRepository.registerDiscoveredCovariantType(type.getName(), impl, implType)));
 
             }
@@ -63,8 +61,8 @@ public class InterfaceMapper extends CachingMapper<GraphQLInterfaceType, GraphQL
     }
 
     @Override
-    public GraphQLInputObjectType toGraphQLInputType(String typeName, AnnotatedType javaType, Set<Type> abstractTypes, OperationMapper operationMapper, BuildContext buildContext) {
-        return objectTypeMapper.toGraphQLInputType(typeName, javaType, abstractTypes, operationMapper, buildContext);
+    public GraphQLInputObjectType toGraphQLInputType(String typeName, AnnotatedType javaType, OperationMapper operationMapper, BuildContext buildContext) {
+        return objectTypeMapper.toGraphQLInputType(typeName, javaType, operationMapper, buildContext);
     }
     
     @Override
@@ -72,11 +70,11 @@ public class InterfaceMapper extends CachingMapper<GraphQLInterfaceType, GraphQL
         return interfaceStrategy.supports(type);
     }
 
-    private Optional<GraphQLObjectType> getImplementingType(AnnotatedType implType, Set<Type> abstractTypes, OperationMapper operationMapper, BuildContext buildContext) {
+    private Optional<GraphQLObjectType> getImplementingType(AnnotatedType implType, OperationMapper operationMapper, BuildContext buildContext) {
         return Optional.of(implType)
                 .filter(impl -> !ClassUtils.isMissingTypeParameters(impl.getType()))
                 .filter(impl -> !interfaceStrategy.supports(impl))
-                .map(impl -> operationMapper.toGraphQLType(impl, abstractTypes, buildContext))
+                .map(impl -> operationMapper.toGraphQLType(impl, buildContext))
                 .filter(impl -> impl instanceof GraphQLObjectType)
                 .map(impl -> (GraphQLObjectType) impl);
     }
