@@ -1,9 +1,5 @@
 package io.leangen.graphql.generator.mapping.core;
 
-import java.lang.reflect.AnnotatedType;
-import java.util.Set;
-import java.util.concurrent.CompletableFuture;
-
 import graphql.schema.GraphQLInputType;
 import io.leangen.geantyref.GenericTypeReflector;
 import io.leangen.graphql.generator.BuildContext;
@@ -11,10 +7,13 @@ import io.leangen.graphql.generator.OperationMapper;
 import io.leangen.graphql.generator.mapping.common.AbstractTypeSubstitutingMapper;
 import io.leangen.graphql.util.ClassUtils;
 
+import java.lang.reflect.AnnotatedType;
+import java.util.concurrent.CompletableFuture;
+
 public class CompletableFutureMapper extends AbstractTypeSubstitutingMapper {
 
     @Override
-    public GraphQLInputType toGraphQLInputType(AnnotatedType javaType, Set abstractTypes, OperationMapper operationMapper, BuildContext buildContext) {
+    public GraphQLInputType toGraphQLInputType(AnnotatedType javaType, OperationMapper operationMapper, BuildContext buildContext) {
         throw new UnsupportedOperationException(CompletableFuture.class.getSimpleName() + " can not be used as an input type");
     }
 
