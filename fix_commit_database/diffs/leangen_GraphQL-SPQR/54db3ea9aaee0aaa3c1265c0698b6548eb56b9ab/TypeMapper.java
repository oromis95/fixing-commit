@@ -1,14 +1,12 @@
 package io.leangen.graphql.generator.mapping;
 
-import java.lang.reflect.AnnotatedType;
-import java.lang.reflect.Type;
-import java.util.Set;
-
 import graphql.schema.GraphQLInputType;
 import graphql.schema.GraphQLOutputType;
 import io.leangen.graphql.generator.BuildContext;
 import io.leangen.graphql.generator.OperationMapper;
 
+import java.lang.reflect.AnnotatedType;
+
 /**
  * A {@code TypeMapper} is used to map annotated Java types to a GraphQL input or output types, modeled by
  * {@link GraphQLOutputType} and {@link GraphQLInputType} respectively.
@@ -17,8 +15,8 @@ import io.leangen.graphql.generator.OperationMapper;
  */
 public interface TypeMapper {
 
-    GraphQLOutputType toGraphQLType(AnnotatedType javaType, Set<Type> abstractTypes, OperationMapper operationMapper, BuildContext buildContext);
-    GraphQLInputType toGraphQLInputType(AnnotatedType javaType, Set<Type> abstractTypes, OperationMapper operationMapper, BuildContext buildContext);
+    GraphQLOutputType toGraphQLType(AnnotatedType javaType, OperationMapper operationMapper, BuildContext buildContext);
+    GraphQLInputType toGraphQLInputType(AnnotatedType javaType, OperationMapper operationMapper, BuildContext buildContext);
 
     boolean supports(AnnotatedType type);
 }
