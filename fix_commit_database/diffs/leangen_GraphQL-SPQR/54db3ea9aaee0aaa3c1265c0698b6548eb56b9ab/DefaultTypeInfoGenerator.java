@@ -3,13 +3,24 @@ package io.leangen.graphql.metadata.strategy.type;
 import io.leangen.graphql.annotations.types.GraphQLInterface;
 import io.leangen.graphql.annotations.types.GraphQLType;
 import io.leangen.graphql.annotations.types.GraphQLUnion;
+import io.leangen.graphql.generator.BuildContext;
+import io.leangen.graphql.generator.exceptions.TypeMappingException;
 import io.leangen.graphql.util.ClassUtils;
+import io.leangen.graphql.util.Scalars;
 import io.leangen.graphql.util.Utils;
 
+import java.lang.reflect.AnnotatedArrayType;
 import java.lang.reflect.AnnotatedParameterizedType;
 import java.lang.reflect.AnnotatedType;
+import java.lang.reflect.AnnotatedTypeVariable;
+import java.lang.reflect.AnnotatedWildcardType;
+import java.lang.reflect.Type;
 import java.util.Arrays;
+import java.util.Collections;
+import java.util.HashSet;
 import java.util.Optional;
+import java.util.Set;
+import java.util.stream.Collectors;
 
 /**
  * @author Bojan Tomic (kaqqao)
@@ -53,6 +64,14 @@ public class DefaultTypeInfoGenerator implements TypeInfoGenerator {
                 .orElse(Utils.emptyArray());
     }
 
+    @Override
+    public Set<Type> findAbstractTypes(AnnotatedType rootType, BuildContext buildContext) {
+        return findAbstract(rootType, buildContext).stream()
+                //ignore built-in types by default as Jackson & Gson *should* be able to deal with them on their own
+                .filter(type -> !ClassUtils.getRawType(type).getPackage().getName().startsWith("java."))
+                .collect(Collectors.toSet());
+    }
+
     @SuppressWarnings("unchecked")
     private String generateSimpleName(AnnotatedType type) {
         Optional<String>[] names = new Optional[]{
@@ -66,6 +85,46 @@ public class DefaultTypeInfoGenerator implements TypeInfoGenerator {
         return getFirstNonEmptyOrDefault(names, ClassUtils.getRawType(type.getType()).getSimpleName());
     }
 
+    @SuppressWarnings("WeakerAccess")
+    protected Set<Type> findAbstract(AnnotatedType javaType, BuildContext buildContext) {
+        javaType = buildContext.globalEnvironment.getMappableInputType(javaType);
+        if (Scalars.isScalar(javaType.getType())) {
+            return Collections.emptySet();
+        }
+        if (javaType instanceof AnnotatedParameterizedType) {
+            Set<Type> abstractTypes = Arrays.stream(((AnnotatedParameterizedType) javaType).getAnnotatedActualTypeArguments())
+                    .flatMap(arg -> findAbstract(arg, buildContext).stream())
+                    .collect(Collectors.toSet());
+            abstractTypes.addAll(findAbstractInner(javaType, buildContext));
+            return abstractTypes;
+        }
+        if (javaType instanceof AnnotatedArrayType) {
+            return findAbstract(((AnnotatedArrayType) javaType).getAnnotatedGenericComponentType(), buildContext);
+        }
+        if (javaType instanceof AnnotatedWildcardType || javaType instanceof AnnotatedTypeVariable) {
+            throw new TypeMappingException(javaType.getType());
+        }
+        return findAbstractInner(javaType, buildContext);
+    }
+
+    private Set<Type> findAbstractInner(AnnotatedType javaType, BuildContext buildContext) {
+        if (buildContext.abstractComponentTypes.get(javaType.getType()) != null) {
+            return buildContext.abstractComponentTypes.get(javaType.getType());
+        }
+        if (buildContext.abstractComponentTypes.containsKey(javaType.getType())) {
+            return Collections.emptySet();
+        }
+        buildContext.abstractComponentTypes.put(javaType.getType(), null);
+        Set<Type> abstractTypes = new HashSet<>();
+        if (ClassUtils.isAbstract(javaType)) {
+            abstractTypes.add(javaType.getType());
+        }
+        buildContext.inputFieldStrategy.getInputFields(javaType)
+                .forEach(childQuery -> abstractTypes.addAll(findAbstract(childQuery.getJavaType(), buildContext)));
+        buildContext.abstractComponentTypes.put(javaType.getType(), abstractTypes);
+        return abstractTypes;
+    }
+
     @SuppressWarnings({"OptionalGetWithoutIsPresent", "ConstantConditions"})
     private String getFirstNonEmptyOrDefault(Optional<String>[] optionals, String defaultValue) {
         return Arrays.stream(optionals)
