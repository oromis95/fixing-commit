@@ -1,8 +1,5 @@
 package io.leangen.graphql.generator.mapping.core;
 
-import java.lang.reflect.AnnotatedType;
-import java.util.Set;
-
 import graphql.execution.DataFetcherResult;
 import graphql.schema.GraphQLInputType;
 import io.leangen.geantyref.GenericTypeReflector;
@@ -11,10 +8,12 @@ import io.leangen.graphql.generator.OperationMapper;
 import io.leangen.graphql.generator.mapping.common.AbstractTypeSubstitutingMapper;
 import io.leangen.graphql.util.ClassUtils;
 
+import java.lang.reflect.AnnotatedType;
+
 public class DataFetcherResultMapper extends AbstractTypeSubstitutingMapper {
 
     @Override
-    public GraphQLInputType toGraphQLInputType(AnnotatedType javaType, Set abstractTypes, OperationMapper operationMapper, BuildContext buildContext) {
+    public GraphQLInputType toGraphQLInputType(AnnotatedType javaType, OperationMapper operationMapper, BuildContext buildContext) {
         throw new UnsupportedOperationException(DataFetcherResult.class.getSimpleName() + " can not be used as an input type");
     }
 
