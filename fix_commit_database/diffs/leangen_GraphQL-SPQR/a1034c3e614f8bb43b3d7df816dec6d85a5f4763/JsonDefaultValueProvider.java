@@ -3,6 +3,8 @@ package io.leangen.graphql.generator.mapping.strategy;
 import io.leangen.graphql.metadata.strategy.type.DefaultTypeInfoGenerator;
 import io.leangen.graphql.metadata.strategy.value.ValueMapper;
 import io.leangen.graphql.util.Defaults;
+import org.slf4j.Logger;
+import org.slf4j.LoggerFactory;
 
 import java.lang.reflect.AnnotatedElement;
 import java.lang.reflect.AnnotatedType;
@@ -14,6 +16,8 @@ public class JsonDefaultValueProvider implements DefaultValueProvider {
 
     private final ValueMapper valueMapper;
 
+    private static final Logger log = LoggerFactory.getLogger(JsonDefaultValueProvider.class);
+
     public JsonDefaultValueProvider() {
         this.valueMapper =  Defaults.valueMapperFactory(new DefaultTypeInfoGenerator())
                 .getValueMapper();
@@ -21,10 +25,6 @@ public class JsonDefaultValueProvider implements DefaultValueProvider {
 
     @Override
     public Object getDefaultValue(AnnotatedElement targetElement, AnnotatedType type, Object initialValue) {
-        if (initialValue == null || (type.getType().equals(String.class) && initialValue instanceof String && !((String) initialValue).startsWith("\""))) {
-            return initialValue;
-        } else {
-            return valueMapper.fromString((String) initialValue, type);
-        }
+        return valueMapper.fromString((String) initialValue, type);
     }
 }
