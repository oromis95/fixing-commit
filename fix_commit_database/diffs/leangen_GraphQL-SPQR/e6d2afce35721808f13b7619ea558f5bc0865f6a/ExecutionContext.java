@@ -1,8 +1,12 @@
 package io.leangen.graphql.query;
 
+import java.lang.reflect.AnnotatedParameterizedType;
 import java.lang.reflect.AnnotatedType;
+import java.util.Arrays;
 
 import graphql.relay.Relay;
+import io.leangen.geantyref.GenericTypeReflector;
+import io.leangen.geantyref.TypeFactory;
 import io.leangen.graphql.generator.TypeRepository;
 import io.leangen.graphql.generator.mapping.ConverterRepository;
 import io.leangen.graphql.generator.mapping.InputConverter;
@@ -10,9 +14,6 @@ import io.leangen.graphql.generator.mapping.OutputConverter;
 import io.leangen.graphql.generator.proxy.ProxyFactory;
 import io.leangen.graphql.metadata.strategy.input.InputDeserializer;
 
-/**
- * Created by bojan.tomic on 5/14/16.
- */
 public class ExecutionContext {
 
     public final Relay relay;
@@ -43,4 +44,22 @@ public class ExecutionContext {
         InputConverter inputConverter = this.converters.getInputConverter(type);
         return inputConverter == null ? input : inputConverter.convertInput(input, type, this);
     }
+
+    public AnnotatedType getMappableType(AnnotatedType type) {
+        InputConverter converter = this.converters.getInputConverter(type);
+        if (converter != null) {
+            return getMappableType(converter.getSubstituteType(type));
+        }
+        if (type.getType() instanceof Class) {
+            return type;
+        }
+        if (type instanceof AnnotatedParameterizedType) {
+            AnnotatedParameterizedType parameterizedType = (AnnotatedParameterizedType) type;
+            AnnotatedType[] arguments = Arrays.stream(parameterizedType.getAnnotatedActualTypeArguments())
+                    .map(this::getMappableType)
+                    .toArray(AnnotatedType[]::new);
+            return TypeFactory.parameterizedAnnotatedClass(GenericTypeReflector.erase(type.getType()), type.getAnnotations(), arguments);
+        }
+        throw new IllegalArgumentException("Can not deserialize type: " + type.getType().getTypeName());
+    }
 }
