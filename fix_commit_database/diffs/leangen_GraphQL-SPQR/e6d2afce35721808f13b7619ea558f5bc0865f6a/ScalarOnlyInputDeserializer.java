@@ -2,9 +2,6 @@ package io.leangen.graphql.metadata.strategy.input;
 
 import java.lang.reflect.AnnotatedType;
 
-/**
- * Created by bojan.tomic on 5/25/16.
- */
 public class ScalarOnlyInputDeserializer implements InputDeserializer {
 
     @Override
@@ -14,4 +11,12 @@ public class ScalarOnlyInputDeserializer implements InputDeserializer {
         }
         throw new IllegalArgumentException("Deserialization failed");
     }
+
+    @Override
+    public <T> T deserializeString(String json, AnnotatedType type) {
+        if (type.getType() == String.class) {
+            return (T) json;
+        }
+        throw new IllegalArgumentException("Deserialization failed");
+    }
 }
