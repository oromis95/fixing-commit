@@ -1,8 +1,10 @@
 package io.leangen.graphql.generator.types;
 
 import java.lang.reflect.AnnotatedType;
+import java.util.Objects;
 
 import graphql.schema.GraphQLInterfaceType;
+import io.leangen.geantyref.GenericTypeReflector;
 
 /**
  * @author Bojan Tomic (kaqqao)
@@ -22,9 +24,18 @@ public class MappedGraphQLInterfaceType extends GraphQLInterfaceType implements
     }
 
     @Override
-    public boolean equals(Object obj) {
-        return obj == this || (obj instanceof GraphQLInterfaceType &&
-                ((GraphQLInterfaceType) obj).getName().contentEquals(getName()) &&
-                ((GraphQLInterfaceType) obj).getFieldDefinitions().equals(getFieldDefinitions()));
+    public boolean equals(Object that) {
+        if (that == this) return true;
+        boolean baseEquals = that instanceof GraphQLInterfaceType &&
+                ((GraphQLInterfaceType) that).getName().equals(getName()) &&
+                ((GraphQLInterfaceType) that).getFieldDefinitions().equals(getFieldDefinitions());
+
+        return baseEquals && (!(that instanceof MappedGraphQLInterfaceType) ||
+                GenericTypeReflector.equals(((MappedGraphQLInterfaceType) that).getJavaType(), this.getJavaType()));
+    }
+
+    @Override
+    public int hashCode() {
+        return Objects.hash(getName(), getFieldDefinitions(), javaType);
     }
 }
