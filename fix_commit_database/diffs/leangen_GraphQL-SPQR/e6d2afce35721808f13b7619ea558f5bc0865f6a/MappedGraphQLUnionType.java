@@ -1,8 +1,10 @@
 package io.leangen.graphql.generator.types;
 
 import java.lang.reflect.AnnotatedType;
+import java.util.Objects;
 
 import graphql.schema.GraphQLUnionType;
+import io.leangen.geantyref.GenericTypeReflector;
 
 /**
  * @author Bojan Tomic (kaqqao)
@@ -23,8 +25,16 @@ public class MappedGraphQLUnionType extends GraphQLUnionType implements MappedGr
 
     @Override
     public boolean equals(Object that) {
-        return that == this || (that instanceof GraphQLUnionType &&
+        if (that == this) return true;
+        boolean baseEquals = that instanceof GraphQLUnionType &&
                 ((GraphQLUnionType) that).getName().equals(this.getName()) &&
-                ((GraphQLUnionType) that).getTypes().equals(this.getTypes()));
+                ((GraphQLUnionType) that).getTypes().equals(this.getTypes());
+        return baseEquals && (!(that instanceof MappedGraphQLUnionType) ||
+                GenericTypeReflector.equals(((MappedGraphQLUnionType) that).getJavaType(), this.getJavaType()));
+    }
+
+    @Override
+    public int hashCode() {
+        return Objects.hash(getName(), getTypes(), javaType);
     }
 }
