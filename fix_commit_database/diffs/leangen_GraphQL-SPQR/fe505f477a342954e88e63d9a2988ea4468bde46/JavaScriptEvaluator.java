@@ -1,14 +1,16 @@
 package io.leangen.graphql.execution.complexity;
 
-import javax.script.Bindings;
-import javax.script.ScriptEngine;
-import javax.script.ScriptEngineManager;
-import javax.script.ScriptException;
-
 import graphql.schema.GraphQLEnumType;
 import graphql.schema.GraphQLScalarType;
 import graphql.schema.GraphQLType;
+import io.leangen.graphql.metadata.Resolver;
 import io.leangen.graphql.util.GraphQLUtils;
+import io.leangen.graphql.util.Utils;
+
+import javax.script.Bindings;
+import javax.script.ScriptEngine;
+import javax.script.ScriptEngineManager;
+import java.util.Map;
 
 public class JavaScriptEvaluator implements ComplexityFunction {
     
@@ -21,21 +23,40 @@ public class JavaScriptEvaluator implements ComplexityFunction {
 
     @Override
     public int getComplexity(ResolvedField node, int childScore) {
-        String expression = node.getResolver().getComplexityExpression();
-        if (expression == null) {
-            GraphQLType fieldType = GraphQLUtils.unwrap(node.getFieldDefinition().getType());
+        Resolver resolver = node.getResolver();
+        if (resolver == null || Utils.isEmpty(resolver.getComplexityExpression())) {
+            GraphQLType fieldType = node.getFieldType();
             if (fieldType instanceof GraphQLScalarType || fieldType instanceof GraphQLEnumType) {
                 return 1;
             }
+            if (GraphQLUtils.isRelayConnectionType(fieldType)) {
+                Integer pageSize = getPageSize(node.getArguments());
+                if (pageSize != null) {
+                    return pageSize * childScore;
+                }
+            }
             return 1 + childScore;
         }
         Bindings bindings = engine.createBindings();
         bindings.putAll(node.getArguments());
         bindings.put("childScore", childScore);
         try {
-            return ((Number) engine.eval(expression, bindings)).intValue();
-        } catch (ScriptException e) {
-            throw new IllegalArgumentException(e);
+            return ((Number) engine.eval(resolver.getComplexityExpression(), bindings)).intValue();
+        } catch (Exception e) {
+            throw new IllegalArgumentException(String.format("Complexity expression \"%s\" on field %s could not be evaluated",
+                    resolver.getComplexityExpression(), node.getName()), e);
+        }
+    }
+
+    private Integer getPageSize(Map<String, Object> arguments) {
+        Object size = arguments.get("first");
+        if (size instanceof Integer) {
+            return (Integer) size;
+        }
+        size = arguments.get("last");
+        if (size instanceof Integer) {
+            return (Integer) size;
         }
+        return null;
     }
 }
