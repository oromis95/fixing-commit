@@ -1,10 +1,9 @@
 package io.leangen.graphql.support;
 
+import io.leangen.graphql.execution.complexity.ComplexityLimitExceededException;
 import org.hamcrest.BaseMatcher;
 import org.hamcrest.Description;
 
-import io.leangen.graphql.execution.complexity.ComplexityLimitExceededException;
-
 public class Matchers {
     
     public static ComplexityMatcher complexityScore(int expectedComplexity) {
@@ -13,7 +12,6 @@ public class Matchers {
 
     private static class ComplexityMatcher extends BaseMatcher<ComplexityLimitExceededException> {
 
-        private int actualComplexity;
         private final int expectedComplexity;
 
         ComplexityMatcher(int expectedComplexity) {
@@ -22,14 +20,13 @@ public class Matchers {
 
         @Override
         public boolean matches(Object exception) {
-            actualComplexity = ((ComplexityLimitExceededException) exception).getComplexity();
-            return actualComplexity == expectedComplexity;
+            return ((ComplexityLimitExceededException) exception).getComplexity() == expectedComplexity;
         }
 
         @Override
         public void describeTo(Description description) {
-            description.appendText(String.format(
-                    "the complexity score of %d, but found %d", expectedComplexity, actualComplexity));
+            description.appendText(ComplexityLimitExceededException.class.getSimpleName() +
+                    " with the complexity score of " + expectedComplexity);
         }
     }
 }
