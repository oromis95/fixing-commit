@@ -1,21 +1,36 @@
 package io.leangen.graphql;
 
-import org.junit.Rule;
-import org.junit.Test;
-import org.junit.rules.ExpectedException;
-
+import graphql.ExecutionResult;
 import graphql.GraphQL;
+import graphql.GraphQLError;
 import graphql.schema.GraphQLSchema;
+import io.leangen.geantyref.GenericTypeReflector;
 import io.leangen.geantyref.TypeToken;
+import io.leangen.graphql.RelayTest.BookService;
 import io.leangen.graphql.annotations.GraphQLArgument;
 import io.leangen.graphql.annotations.GraphQLComplexity;
+import io.leangen.graphql.annotations.GraphQLMutation;
+import io.leangen.graphql.annotations.GraphQLNonNull;
 import io.leangen.graphql.annotations.GraphQLQuery;
+import io.leangen.graphql.annotations.GraphQLSubscription;
 import io.leangen.graphql.annotations.types.GraphQLInterface;
 import io.leangen.graphql.domain.Education;
 import io.leangen.graphql.execution.complexity.ComplexityLimitExceededException;
+import io.leangen.graphql.execution.relay.Page;
+import io.leangen.graphql.execution.relay.generic.PageFactory;
 import io.leangen.graphql.services.UserService;
+import io.leangen.graphql.util.GraphQLUtils;
+import org.junit.Test;
+import org.reactivestreams.Publisher;
+
+import java.lang.reflect.AnnotatedType;
+import java.util.Collections;
+import java.util.List;
 
 import static io.leangen.graphql.support.Matchers.complexityScore;
+import static org.junit.Assert.assertEquals;
+import static org.junit.Assert.assertThat;
+import static org.junit.Assert.assertTrue;
 
 public class ComplexityTest {
 
@@ -62,40 +77,92 @@ public class ComplexityTest {
             "    }" +
             "  }" +
             "}";
-    
-    @Rule
-    public ExpectedException thrown = ExpectedException.none();
+
+    private static final String pagedQuery = "{pets(first:10, after:\"20\") {" +
+            "   pageInfo {" +
+            "       hasNextPage" +
+            "   }," +
+            "   edges {" +
+            "       cursor, node {" +
+            "           sound" +
+            "           owner {" +
+            "               name" +
+            "           }" +
+            "}}}}";
+
+    private static final String simpleMutation = "mutation AddPet {" +
+            "   addPet(pet: {_type_: \"Cat\"}) {" +
+            "       sound" +
+            "       owner {" +
+            "           name" +
+            "       }" +
+            "       ... on Cat {" +
+            "           clawLength" +
+            "       }" +
+            "   }" +
+            "}";
+
+    private static final String subscription = "subscription NewArrivals {" +
+            "   newPets {" +
+            "       sound" +
+            "       owner {" +
+            "           name" +
+            "       }" +
+            "   }" +
+            "}";
 
     @Test
     public void fragmentComplexityTest() {
-        GraphQLSchema schema = new TestSchemaGenerator()
-                .withOperationsFromSingleton(new UserService<Education>(), new TypeToken<UserService<Education>>(){}.getAnnotatedType())
-                .generate();
+        testComplexity(new UserService<Education>(), new TypeToken<UserService<Education>>(){}.getAnnotatedType(), fragmentQuery, 11, 32);
+    }
 
-        GraphQL exe = GraphQLRuntime.newGraphQL(schema)
-                .maximumQueryComplexity(11)
-                .build();
+    @Test
+    public void branchingComplexityTest() {
+        testComplexity(new PetService(), branchingQuery, 5, 6);
+    }
 
-        thrown.expect(ComplexityLimitExceededException.class);
-        thrown.expect(complexityScore(32));
-        exe.execute(fragmentQuery);
+    @Test
+    public void connectionComplexityTest() {
+        testComplexity(new PagedPetService(), pagedQuery, 50, 80);
     }
-    
+
     @Test
-    public void branchingComplexityTest() {
+    public void introspectionComplexityTest() {
+        //introspection query complexity should be independent of the schema (service)
+        testComplexity(new PetService(), GraphQLUtils.FULL_INTROSPECTION_QUERY, 108, 109);
+        testComplexity(new BookService(), GraphQLUtils.FULL_INTROSPECTION_QUERY, 108, 109);
+    }
+
+    @Test
+    public void mutationComplexityTest() {
+        testComplexity(new PetService(), simpleMutation, 4, 6);
+    }
+
+    @Test
+    public void subscriptionComplexityTest() {
+        testComplexity(new PetService(), subscription, 4, 5);
+    }
+
+    private void testComplexity(Object service, String operation, int maxComplexity, int expectedComplexity) {
+        testComplexity(service, GenericTypeReflector.annotate(service.getClass()), operation, maxComplexity, expectedComplexity);
+    }
+
+    private void testComplexity(Object service, AnnotatedType serviceType, String operation, int maxComplexity, int expectedComplexity) {
         GraphQLSchema schema = new TestSchemaGenerator()
-                .withOperationsFromSingleton(new PetService())
+                .withOperationsFromSingleton(service, serviceType)
                 .generate();
 
         GraphQL exe = GraphQLRuntime.newGraphQL(schema)
-                .maximumQueryComplexity(5)
+                .maximumQueryComplexity(maxComplexity)
                 .build();
 
-        thrown.expect(ComplexityLimitExceededException.class);
-        thrown.expect(complexityScore(6));
-        exe.execute(branchingQuery);
+        ExecutionResult res = exe.execute(operation);
+        assertEquals(1, res.getErrors().size());
+        GraphQLError error = res.getErrors().get(0);
+        assertTrue(error instanceof ComplexityLimitExceededException);
+        assertThat((ComplexityLimitExceededException) error, complexityScore(expectedComplexity));
     }
-    
+
     @GraphQLInterface(name = "Pet", implementationAutoDiscovery = true)
     public interface Pet {
         String getSound();
@@ -103,6 +170,7 @@ public class ComplexityTest {
     }
     
     public static class Cat implements Pet {
+
         @Override
         public String getSound() {
             return "meow";
@@ -154,9 +222,30 @@ public class ComplexityTest {
     }
     
     public static class PetService {
+
         @GraphQLQuery(name = "pet")
-        public Pet findPet(@GraphQLArgument(name = "cat") boolean cat) {
+        public @GraphQLNonNull Pet findPet(@GraphQLArgument(name = "cat") boolean cat) {
             return cat ? new Cat() : new Dog();
         }
+
+        @GraphQLMutation
+        @GraphQLComplexity("2 + childScore")
+        public @GraphQLNonNull List<@GraphQLNonNull Pet> addPet(@GraphQLArgument(name = "pet") Pet pet) {
+            return Collections.singletonList(pet);
+        }
+
+        @GraphQLSubscription
+        @GraphQLComplexity("2 + childScore")
+        public Publisher<Pet> newPets() {
+            return null;
+        }
+    }
+
+    public static class PagedPetService {
+
+        @GraphQLQuery(name = "pets")
+        public Page<Pet> findPets(@GraphQLArgument(name = "first") int first, @GraphQLArgument(name = "after") String after) {
+            return PageFactory.createPage(Collections.emptyList(), PageFactory.offsetBasedCursorProvider(0L), false, false);
+        }
     }
 }
