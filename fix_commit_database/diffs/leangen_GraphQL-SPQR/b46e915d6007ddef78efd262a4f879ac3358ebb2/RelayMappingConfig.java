@@ -1,7 +1,8 @@
 package io.leangen.graphql.generator;
 
 public class RelayMappingConfig {
-    
+
+    public boolean inferNodeInterface = true;
     public boolean relayCompliantMutations;
     public String wrapperFieldName;
     public String wrapperFieldDescription;
