@@ -1,11 +1,11 @@
 package io.leangen.graphql.metadata.execution;
 
+import io.leangen.graphql.util.ClassUtils;
+
 import java.lang.reflect.AnnotatedType;
 import java.lang.reflect.Field;
 import java.lang.reflect.Parameter;
 
-import io.leangen.graphql.util.ClassUtils;
-
 /**
  * Created by bojan.tomic on 7/20/16.
  */
@@ -46,11 +46,4 @@ public class FieldAccessor extends Executable {
     public Parameter[] getParameters() {
         return new Parameter[0];
     }
-
-    @Override
-    public String toString() {
-        return ((Field) delegate).getDeclaringClass().getName() + "#"
-                + ((Field) delegate).getName()
-                + " -> " + getReturnType().toString();
-    }
 }
