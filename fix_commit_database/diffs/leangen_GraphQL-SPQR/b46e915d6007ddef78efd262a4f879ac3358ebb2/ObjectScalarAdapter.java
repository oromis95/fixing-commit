@@ -29,13 +29,11 @@ public class ObjectScalarAdapter extends CachingMapper<GraphQLScalarType, GraphQ
 
     @Override
     public GraphQLScalarType toGraphQLType(String typeName, AnnotatedType javaType, OperationMapper operationMapper, BuildContext buildContext) {
-        buildContext.knownInputTypes.add(typeName);
         return Scalars.graphQLObjectScalar(typeName);
     }
     
     @Override
     public GraphQLScalarType toGraphQLInputType(String typeName, AnnotatedType javaType, OperationMapper operationMapper, BuildContext buildContext) {
-        buildContext.knownTypes.add(typeName);
         return toGraphQLType(typeName, javaType, operationMapper, buildContext);
     }
 
