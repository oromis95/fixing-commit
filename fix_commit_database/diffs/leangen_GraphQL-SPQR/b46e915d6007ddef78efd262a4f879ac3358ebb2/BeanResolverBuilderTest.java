@@ -1,14 +1,13 @@
-package io.leangen.graphql.model;
-
-import org.junit.Test;
-
-import java.util.ArrayList;
-import java.util.List;
+package io.leangen.graphql;
 
 import io.leangen.geantyref.GenericTypeReflector;
 import io.leangen.graphql.metadata.Resolver;
 import io.leangen.graphql.metadata.strategy.query.PublicResolverBuilder;
 import io.leangen.graphql.util.Utils;
+import org.junit.Test;
+
+import java.util.ArrayList;
+import java.util.List;
 
 import static org.junit.Assert.assertEquals;
 import static org.junit.Assert.assertTrue;
@@ -34,6 +33,7 @@ public class BeanResolverBuilderTest {
         assertTrue(resolvers.stream().anyMatch(resolver -> resolver.getOperationName().equals("getUserHandle")));
     }
     
+    @SuppressWarnings("WeakerAccess")
     public static class NicknameService {
         
         public String getNickname(String name) {
