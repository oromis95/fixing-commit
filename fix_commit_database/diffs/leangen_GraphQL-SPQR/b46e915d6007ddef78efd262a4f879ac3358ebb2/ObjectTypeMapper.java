@@ -55,7 +55,6 @@ public class ObjectTypeMapper extends CachingMapper<GraphQLObjectType, GraphQLIn
 
         GraphQLObjectType type = new MappedGraphQLObjectType(typeBuilder.build(), javaType);
         interfaces.forEach(inter -> buildContext.typeRepository.registerCovariantType(inter.getName(), javaType, type));
-        buildContext.typeRepository.registerObjectType(type);
         return type;
     }
 
@@ -103,7 +102,7 @@ public class ObjectTypeMapper extends CachingMapper<GraphQLObjectType, GraphQLIn
                                                     List<GraphQLFieldDefinition> fields, BuildContext buildContext, OperationMapper operationMapper) {
 
         List<GraphQLOutputType> interfaces = new ArrayList<>();
-        if (fields.stream().anyMatch(GraphQLUtils::isRelayId)) {
+        if (buildContext.relayMappingConfig.inferNodeInterface && fields.stream().anyMatch(GraphQLUtils::isRelayId)) {
             interfaces.add(buildContext.node);
         }
         buildContext.interfaceStrategy.getInterfaces(javaType).forEach(
