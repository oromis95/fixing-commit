@@ -2,6 +2,8 @@ package io.leangen.graphql;
 
 import ch.qos.logback.classic.Level;
 import ch.qos.logback.classic.spi.ILoggingEvent;
+import com.fasterxml.jackson.annotation.JsonCreator;
+import com.fasterxml.jackson.annotation.JsonProperty;
 import graphql.ExecutionResult;
 import graphql.GraphQL;
 import graphql.Scalars;
@@ -15,8 +17,17 @@ import graphql.schema.GraphQLObjectType;
 import graphql.schema.GraphQLSchema;
 import io.leangen.geantyref.TypeToken;
 import io.leangen.graphql.annotations.GraphQLArgument;
+import io.leangen.graphql.annotations.GraphQLId;
 import io.leangen.graphql.annotations.GraphQLQuery;
+import io.leangen.graphql.annotations.GraphQLUnion;
+import io.leangen.graphql.domain.Cat;
+import io.leangen.graphql.domain.Character;
+import io.leangen.graphql.domain.Dog;
 import io.leangen.graphql.domain.Education;
+import io.leangen.graphql.domain.Human;
+import io.leangen.graphql.domain.Pet;
+import io.leangen.graphql.domain.Robot;
+import io.leangen.graphql.domain.Street;
 import io.leangen.graphql.domain.User;
 import io.leangen.graphql.execution.relay.Connection;
 import io.leangen.graphql.execution.relay.Page;
@@ -24,8 +35,11 @@ import io.leangen.graphql.execution.relay.generic.PageFactory;
 import io.leangen.graphql.generator.OperationMapper;
 import io.leangen.graphql.generator.mapping.common.MapToListTypeAdapter;
 import io.leangen.graphql.generator.mapping.strategy.ObjectScalarStrategy;
+import io.leangen.graphql.metadata.exceptions.TypeMappingException;
+import io.leangen.graphql.metadata.strategy.query.PublicResolverBuilder;
 import io.leangen.graphql.services.UserService;
 import io.leangen.graphql.support.TestLog;
+import io.leangen.graphql.util.GraphQLUtils;
 import io.leangen.graphql.util.Urls;
 import org.junit.Test;
 
@@ -34,13 +48,14 @@ import java.util.Arrays;
 import java.util.Collections;
 import java.util.Iterator;
 import java.util.List;
+import java.util.UUID;
 
 import static io.leangen.graphql.support.QueryResultAssertions.assertValueAtPathEquals;
 import static org.hamcrest.core.Is.is;
 import static org.hamcrest.core.StringContains.containsString;
 import static org.junit.Assert.assertEquals;
 import static org.junit.Assert.assertFalse;
-import static org.junit.Assert.assertNotEquals;
+import static org.junit.Assert.assertNotNull;
 import static org.junit.Assert.assertThat;
 import static org.junit.Assert.assertTrue;
 
@@ -99,7 +114,7 @@ public class RelayTest {
     }
 
     @Test
-    public void relayMutationTest() {
+    public void testRelayMutations() {
         GraphQLSchema schema = new TestSchemaGenerator()
                 .withOperationsFromSingleton(new UserService<Education>(), new TypeToken<UserService<Education>>(){}.getAnnotatedType())
                 .withTypeAdapters(new MapToListTypeAdapter<>(new ObjectScalarStrategy()))
@@ -131,7 +146,7 @@ public class RelayTest {
 
         GraphQLFieldDefinition totalCount = schema.getObjectType("BookConnection")
                 .getFieldDefinition("totalCount");
-        assertNotEquals(null, totalCount);
+        assertNotNull(totalCount);
         assertEquals(Scalars.GraphQLLong, totalCount.getType());
         GraphQL exe = GraphQLRuntime.newGraphQL(schema).build();
 
@@ -158,13 +173,13 @@ public class RelayTest {
         GraphQLObjectType bookConnection = schema.getObjectType("BookConnection");
         assertEquals(3, bookConnection.getFieldDefinitions().size());
         GraphQLFieldDefinition totalCount = bookConnection.getFieldDefinition("totalCount");
-        assertNotEquals(null, totalCount);
+        assertNotNull(totalCount);
         assertEquals(Scalars.GraphQLLong, totalCount.getType());
 
         GraphQLObjectType bookEdge = schema.getObjectType("BookEdge");
         assertEquals(3, bookEdge.getFieldDefinitions().size());
         GraphQLFieldDefinition color = bookEdge.getFieldDefinition("color");
-        assertNotEquals(null, color);
+        assertNotNull(color);
         assertTrue(color.getType() instanceof GraphQLEnumType);
 
         GraphQL exe = GraphQLRuntime.newGraphQL(schema).build();
@@ -196,6 +211,93 @@ public class RelayTest {
         }
     }
 
+    @Test(expected = TypeMappingException.class)
+    public void testInvalidPaginationArguments() {
+        new TestSchemaGenerator()
+                .withOperationsFromSingleton(new InvalidPagingService())
+                .generate();
+    }
+
+    @Test
+    public void testDirectNodeQuery() {
+        GraphQLSchema schema = new GraphQLSchemaGenerator()
+                .withOperationsFromSingletons(new BookService(), new DescriptorService())
+                .generate();
+
+        assertNotNull(schema.getQueryType().getFieldDefinition("node"));
+        assertTrue(GraphQLUtils.isRelayId(((GraphQLObjectType)schema.getType("Descriptor")).getFieldDefinition("id")));
+        assertTrue(GraphQLUtils.isRelayId((schema.getQueryType().getFieldDefinition("descriptor").getArgument("id"))));
+
+        GraphQL exe = GraphQL.newGraphQL(schema).build();
+        ExecutionResult result = exe.execute("{node(id: \"Qm9vazprZXds\") {id}}");
+        assertTrue(result.getErrors().isEmpty());
+        result = exe.execute("{node(id: \"Qm9vazp7InRpdGxlIjoiVGhlIGtleSBib29rIiwiaWQiOiI3NzcifQ==\") {id ... on Descriptor {text}}}");
+        assertTrue(result.getErrors().isEmpty());
+    }
+
+    @Test
+    public void testPolymorphicNodeQuery() {
+        GraphQLSchema schema = new TestSchemaGenerator()
+                .withResolverBuilders(new PublicResolverBuilder())
+                .withOperationsFromSingletons(new PolymorphicPrimaryResolverService())
+                .generate();
+
+        assertNotNull(schema.getQueryType().getFieldDefinition("node"));
+
+        GraphQL exe = GraphQL.newGraphQL(schema).build();
+        ExecutionResult result = exe.execute("{node(id: \"Q2F0OjY2Ng==\") {id}}");
+        assertTrue(result.getErrors().isEmpty());
+        result = exe.execute("{node(id: \"Um9ib3Q6NjY2\") {id ... on Robot {name}}}");
+        assertTrue(result.getErrors().isEmpty());
+    }
+
+    @Test
+    public void testPartialUnionNodeQuery() {
+        GraphQLSchema schema = new TestSchemaGenerator()
+                .withResolverBuilders(new PublicResolverBuilder())
+                .withOperationsFromSingletons(new PartialUnionPrimaryResolverService())
+                .generate();
+
+        assertNotNull(schema.getQueryType().getFieldDefinition("node"));
+
+        GraphQL exe = GraphQL.newGraphQL(schema).build();
+        ExecutionResult result = exe.execute("{node(id: \"Um9ib3Q6NjY2\") {id ... on Robot {name}}}");
+        assertTrue(result.getErrors().isEmpty());
+    }
+
+    @Test
+    public void testDirectNodeQueryPriority() {
+        GraphQLSchema schema = new TestSchemaGenerator()
+                .withResolverBuilders(new PublicResolverBuilder())
+                .withOperationsFromSingletons(new DirectAndPolymorphicPrimaryResolverService())
+                .generate();
+
+        assertNotNull(schema.getQueryType().getFieldDefinition("node"));
+
+        GraphQL exe = GraphQL.newGraphQL(schema).build();
+        ExecutionResult result = exe.execute("{node(id: \"Q2F0Ojg=\") {id ... on Cat {name}}}");
+        assertTrue(result.getErrors().isEmpty());
+        assertValueAtPathEquals("Correct", result, "node.name");
+        result = exe.execute("{node(id: \"RG9nOjk=\") {id ... on Dog {sound}}}");
+        assertTrue(result.getErrors().isEmpty());
+        assertValueAtPathEquals("Correct", result, "node.sound");
+    }
+
+    @Test
+    public void testExplicitNodeQuery() {
+        GraphQLSchema schema = new TestSchemaGenerator()
+                .withResolverBuilders(new PublicResolverBuilder())
+                .withOperationsFromSingletons(new ExplicitNodeService())
+                .generate();
+
+        assertNotNull(schema.getQueryType().getFieldDefinition("node"));
+
+        GraphQL exe = GraphQL.newGraphQL(schema).build();
+        ExecutionResult result = exe.execute("{node(id: \"Q2F0Ojg=\") {id ... on Cat {name}}}");
+        assertTrue(result.getErrors().isEmpty());
+        assertValueAtPathEquals("Correct", result, "node.name");
+    }
+
     private void testPagedQuery(String query) {
         GraphQLSchema schema = new TestSchemaGenerator()
                 .withOperationsFromSingleton(new BookService())
@@ -210,7 +312,8 @@ public class RelayTest {
         private String title;
         private String isbn;
 
-        Book(String title, String isbn) {
+        @JsonCreator
+        Book(@JsonProperty("title") String title, @JsonProperty("id") String isbn) {
             this.title = title;
             this.isbn = isbn;
         }
@@ -219,11 +322,32 @@ public class RelayTest {
             return title;
         }
 
-        public String getIsbn() {
+        @GraphQLQuery(name = "id")
+        public @GraphQLId(relayId = true) String getIsbn() {
             return isbn;
         }
     }
 
+    public static class Descriptor {
+        private Book book;
+        private String text;
+
+        @JsonCreator
+        Descriptor(@JsonProperty("id") Book book, @JsonProperty("text") String text) {
+            this.book = book;
+            this.text = text;
+        }
+
+        @GraphQLQuery(name = "id")
+        public @GraphQLId(relayId = true) Book getBook() {
+            return book;
+        }
+
+        public String getText() {
+            return text;
+        }
+    }
+
     public static class BookService {
         @GraphQLQuery(name = "books")
         public Page<Book> getBooks(@GraphQLArgument(name = "first") int first, @GraphQLArgument(name = "after") String after) {
@@ -236,6 +360,76 @@ public class RelayTest {
         public Page<Book> getEmpty(@GraphQLArgument(name = "first") int first, @GraphQLArgument(name = "after") String after) {
             return PageFactory.createOffsetBasedPage(Collections.emptyList(), 100, 10);
         }
+
+        @GraphQLQuery
+        public Book book(@GraphQLId(relayId = true) String isbn) {
+            return new Book("Node Book", isbn);
+        }
+    }
+
+    public static class DescriptorService {
+
+        @GraphQLQuery
+        public Descriptor descriptor(@GraphQLId(relayId = true) Book book) {
+            return new Descriptor(book, "An imaginative book description");
+        }
+
+        @GraphQLQuery
+        public Descriptor random() {
+            return new Descriptor(null, UUID.randomUUID().toString());
+        }
+    }
+
+    public static class PolymorphicPrimaryResolverService {
+
+        public Pet pet(@GraphQLId(relayId = true) int id) {
+            return id % 2 == 0 ? new Cat() : new Dog();
+        }
+
+        public Character character(@GraphQLId(relayId = true) int id) {
+            return id % 2 == 0 ? new Robot("X3R0", "Zero") : new Human("Jack Alman", "Jackal");
+        }
+    }
+
+    public static class PartialUnionPrimaryResolverService {
+
+        public @GraphQLUnion(name = "Character") Human character(@GraphQLId(relayId = true) int id, String name) {
+            return new Human("Jack Alman", "Jackal");
+        }
+
+        public @GraphQLUnion(name = "Character") Robot character(@GraphQLId(relayId = true) int id) {
+            return new Robot("X3R0", "Zero");
+        }
+    }
+
+    public static class DirectAndPolymorphicPrimaryResolverService {
+
+        public Pet pet(@GraphQLId(relayId = true) int id) {
+            return id % 2 == 0 ? new Cat("Wrong") : new Dog("Correct");
+        }
+
+        public Cat cat(@GraphQLId(relayId = true) int id) {
+            return new Cat("Correct");
+        }
+    }
+
+    public static class ExplicitNodeService {
+
+        public Cat node(@GraphQLId(relayId = true) int id) {
+            return new Cat("Correct");
+        }
+
+        public Cat cat(@GraphQLId(relayId = true) int id) {
+            return new Cat("Wrong");
+        }
+    }
+
+    private static class InvalidPagingService {
+
+        @GraphQLQuery(name = "streets")
+        public Page<Street> streets(@GraphQLArgument(name = "first") String first) {
+            return null;
+        }
     }
 
     public static class ExtendedPageBookService {
