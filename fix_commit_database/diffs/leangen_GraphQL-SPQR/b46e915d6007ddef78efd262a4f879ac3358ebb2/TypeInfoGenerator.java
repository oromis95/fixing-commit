@@ -1,10 +1,6 @@
 package io.leangen.graphql.metadata.strategy.type;
 
-import io.leangen.graphql.generator.BuildContext;
-
 import java.lang.reflect.AnnotatedType;
-import java.lang.reflect.Type;
-import java.util.Set;
 
 /**
  * @author Bojan Tomic (kaqqao)
@@ -20,8 +16,6 @@ public interface TypeInfoGenerator {
 
     String[] getFieldOrder(AnnotatedType type);
 
-    Set<Type> findAbstractTypes(AnnotatedType rootType, BuildContext buildContext);
-
     default String generateInputTypeName(AnnotatedType type) {
         return generateTypeName(type) + INPUT_SUFFIX;
     }
