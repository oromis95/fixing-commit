@@ -1,9 +1,10 @@
 package io.leangen.graphql.generator;
 
-import java.lang.reflect.AnnotatedType;
-
 import graphql.schema.GraphQLObjectType;
 import graphql.schema.GraphQLOutputType;
+import graphql.schema.GraphQLTypeReference;
+
+import java.lang.reflect.AnnotatedType;
 
 /**
  * @author Bojan Tomic (kaqqao)
@@ -13,6 +14,9 @@ public class MappedType {
     public final GraphQLOutputType graphQLType;
 
     MappedType(AnnotatedType javaType, GraphQLOutputType graphQLType) {
+        if (!(graphQLType instanceof GraphQLTypeReference || graphQLType instanceof GraphQLObjectType)) {
+            throw new IllegalArgumentException("Implementation types can only be object types or references to object types");
+        }
         this.javaType = javaType;
         this.graphQLType = graphQLType;
     }
