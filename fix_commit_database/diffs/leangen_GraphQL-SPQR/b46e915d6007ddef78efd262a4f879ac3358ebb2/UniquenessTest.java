@@ -8,6 +8,9 @@ import graphql.schema.GraphQLList;
 import graphql.schema.GraphQLSchema;
 import graphql.schema.GraphQLType;
 import io.leangen.graphql.annotations.GraphQLEnumValue;
+import io.leangen.graphql.annotations.GraphQLId;
+import io.leangen.graphql.annotations.GraphQLScalar;
+import io.leangen.graphql.domain.Address;
 import io.leangen.graphql.domain.SimpleUser;
 import io.leangen.graphql.execution.relay.Page;
 import io.leangen.graphql.generator.mapping.common.MapToListTypeAdapter;
@@ -19,6 +22,7 @@ import org.junit.Test;
 import java.util.EnumMap;
 import java.util.List;
 import java.util.Map;
+import java.util.UUID;
 import java.util.function.Function;
 import java.util.stream.Collectors;
 
@@ -29,6 +33,27 @@ public class UniquenessTest {
 
     private static final String thisPackage = UniquenessTest.class.getPackage().getName();
 
+    @Test
+    public void testRelayIdUniqueness() {
+        GraphQLSchema schema = schemaFor(new RelayIdService());
+        testRootQueryTypeUniqueness(schema);
+        testRootQueryArgumentTypeUniqueness(schema);
+    }
+
+    @Test
+    public void testScalarUniqueness() {
+        GraphQLSchema schema = schemaFor(new ScalarService());
+        testRootQueryTypeUniqueness(schema);
+        testRootQueryArgumentTypeUniqueness(schema);
+    }
+
+    @Test
+    public void testObjectScalarUniqueness() {
+        GraphQLSchema schema = schemaFor(new ObjectScalarService());
+        testRootQueryTypeUniqueness(schema);
+        testRootQueryArgumentTypeUniqueness(schema);
+    }
+
     @Test
     public void testPageUniqueness() {
         testRootQueryTypeUniqueness(schemaFor(new PagingService()));
@@ -100,6 +125,39 @@ public class UniquenessTest {
         assertTrue(inputTypes.stream().allMatch(type -> inputTypes.get(0) == type));
     }
 
+    public static class RelayIdService {
+
+        public @GraphQLId(relayId = true) String getId(@GraphQLId(relayId = true) String in) {
+            return in;
+        }
+
+        public @GraphQLId(relayId = true) String getIdAgain(@GraphQLId(relayId = true) String in) {
+            return in;
+        }
+    }
+
+    public static class ScalarService {
+
+        public UUID getUuid(UUID in) {
+            return in;
+        }
+
+        public UUID getUuidAgain(UUID in) {
+            return in;
+        }
+    }
+
+    public static class ObjectScalarService {
+
+        public @GraphQLScalar Address getAddress(@GraphQLScalar Address in) {
+            return in;
+        }
+
+        public @GraphQLScalar Address getAddressAgain(@GraphQLScalar Address in) {
+            return in;
+        }
+    }
+
     public static class EnumService {
 
         enum BLACK_OR_WHITE {
