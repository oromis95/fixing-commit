@@ -16,39 +16,39 @@ import java.lang.reflect.AnnotatedType;
  * @author Bojan Tomic (kaqqao)
  */
 public abstract class CachingMapper<O extends GraphQLOutputType, I extends GraphQLInputType> implements TypeMapper {
-    
+
     @Override
     public GraphQLOutputType toGraphQLType(AnnotatedType javaType, OperationMapper operationMapper, BuildContext buildContext) {
         String typeName = getTypeName(javaType, buildContext);
-        if (buildContext.knownTypes.contains(typeName)) {
+        if (buildContext.typeCache.contains(typeName)) {
             return new GraphQLTypeReference(typeName);
         }
-        buildContext.knownTypes.add(typeName);
+        buildContext.typeCache.register(typeName);
         return toGraphQLType(typeName, javaType, operationMapper, buildContext);
     }
 
     @Override
     public GraphQLInputType toGraphQLInputType(AnnotatedType javaType, OperationMapper operationMapper, BuildContext buildContext) {
         String typeName = getInputTypeName(javaType, buildContext);
-        if (buildContext.knownInputTypes.contains(typeName)) {
+        if (buildContext.typeCache.contains(typeName)) {
             return new GraphQLTypeReference(typeName);
         }
-        buildContext.knownInputTypes.add(typeName);
+        buildContext.typeCache.register(typeName);
         return toGraphQLInputType(typeName, javaType, operationMapper, buildContext);
     }
-    
+
     protected abstract O toGraphQLType(String typeName, AnnotatedType javaType, OperationMapper operationMapper, BuildContext buildContext);
 
     protected abstract I toGraphQLInputType(String typeName, AnnotatedType javaType, OperationMapper operationMapper, BuildContext buildContext);
-    
+
     protected String getTypeName(AnnotatedType type, BuildContext buildContext) {
         return getTypeName(type, getTypeArguments(0), buildContext.typeInfoGenerator);
     }
-    
+
     protected String getInputTypeName(AnnotatedType type, BuildContext buildContext) {
         return getTypeName(type, getTypeArguments(1), buildContext.typeInfoGenerator);
     }
-    
+
     private String getTypeName(AnnotatedType javaType, AnnotatedType graphQLType, TypeInfoGenerator typeInfoGenerator) {
         if (GenericTypeReflector.isSuperType(GraphQLScalarType.class, graphQLType.getType())) {
             return typeInfoGenerator.generateScalarTypeName(javaType);
@@ -58,7 +58,7 @@ public abstract class CachingMapper<O extends GraphQLOutputType, I extends Graph
         }
         return typeInfoGenerator.generateTypeName(javaType);
     }
-    
+
     private AnnotatedType getTypeArguments(int index) {
         return GenericTypeReflector.getTypeParameter(getClass().getAnnotatedSuperclass(), CachingMapper.class.getTypeParameters()[index]);
     }
