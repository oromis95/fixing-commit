@@ -1,12 +1,12 @@
 package io.leangen.graphql.metadata.execution;
 
+import io.leangen.graphql.util.ClassUtils;
+
 import java.lang.reflect.AnnotatedType;
 import java.lang.reflect.InvocationTargetException;
 import java.lang.reflect.Method;
 import java.lang.reflect.Parameter;
 
-import io.leangen.graphql.util.ClassUtils;
-
 /**
  * Created by bojan.tomic on 7/20/16.
  */
@@ -54,11 +54,4 @@ public class MethodInvoker extends Executable {
     public Parameter[] getParameters() {
         return ((Method) delegate).getParameters();
     }
-
-    @Override
-    public String toString() {
-        return ((Method) delegate).getDeclaringClass().getName() + "#"
-                + ((Method) delegate).getName()
-                + " -> " + returnType.toString();
-    }
 }
