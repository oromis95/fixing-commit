@@ -12,10 +12,10 @@ import io.leangen.graphql.util.ClassUtils;
 public class BeanOperationNameGenerator extends AnnotatedOperationNameGenerator {
 
     @Override
-    public String generateQueryName(Method queryMethod, AnnotatedType declaringType) {
+    public String generateQueryName(Method queryMethod, AnnotatedType declaringType, Object instance) {
         if (ClassUtils.isGetter(queryMethod)) {
             try {
-                return super.generateQueryName(queryMethod, declaringType);
+                return super.generateQueryName(queryMethod, declaringType, instance);
             } catch (IllegalArgumentException e) {
                 return ClassUtils.getFieldNameFromGetter(queryMethod);
             }
@@ -24,19 +24,19 @@ public class BeanOperationNameGenerator extends AnnotatedOperationNameGenerator
     }
 
     @Override
-    public String generateQueryName(Field queryField, AnnotatedType declaringType) {
+    public String generateQueryName(Field queryField, AnnotatedType declaringType, Object instance) {
         try {
-            return super.generateQueryName(queryField, declaringType);
+            return super.generateQueryName(queryField, declaringType, instance);
         } catch (IllegalArgumentException e) {
             return queryField.getName();
         }
     }
 
     @Override
-    public String generateMutationName(Method mutationMethod, AnnotatedType declaringType) {
+    public String generateMutationName(Method mutationMethod, AnnotatedType declaringType, Object instance) {
         if (ClassUtils.isSetter(mutationMethod)) {
             try {
-                return super.generateMutationName(mutationMethod, declaringType);
+                return super.generateMutationName(mutationMethod, declaringType, instance);
             } catch (IllegalArgumentException e) {
                 return ClassUtils.getFieldNameFromSetter(mutationMethod);
             }
