@@ -10,17 +10,17 @@ import java.lang.reflect.Method;
 public class MethodOperationNameGenerator implements OperationNameGenerator {
 
     @Override
-    public String generateQueryName(Method queryMethod, AnnotatedType type) {
+    public String generateQueryName(Method queryMethod, AnnotatedType type, Object instance) {
         return queryMethod.getName();
     }
 
     @Override
-    public String generateQueryName(Field queryField, AnnotatedType declaringType) {
+    public String generateQueryName(Field queryField, AnnotatedType declaringType, Object instance) {
         return queryField.getName();
     }
 
     @Override
-    public String generateMutationName(Method mutationMethod, AnnotatedType declaringType) {
+    public String generateMutationName(Method mutationMethod, AnnotatedType declaringType, Object instance) {
         return mutationMethod.getName();
     }
 }
