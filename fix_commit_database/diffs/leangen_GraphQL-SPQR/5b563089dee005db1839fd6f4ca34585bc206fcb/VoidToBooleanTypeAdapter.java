@@ -2,8 +2,6 @@ package io.leangen.graphql.generator.mapping.common;
 
 import io.leangen.graphql.generator.mapping.AbstractTypeAdapter;
 
-import java.lang.reflect.AnnotatedType;
-
 /**
  * @author Bojan Tomic (kaqqao)
  */
@@ -18,9 +16,4 @@ public class VoidToBooleanTypeAdapter extends AbstractTypeAdapter<Void, Boolean>
     public Void convertInput(Boolean substitute) {
         throw new UnsupportedOperationException("Void used as input");
     }
-
-    @Override
-    public boolean supports(AnnotatedType type) {
-        return super.supports(type);
-    }
 }
