@@ -3,29 +3,44 @@ package io.leangen.graphql;
 import org.junit.Test;
 
 import java.util.ArrayList;
+import java.util.Arrays;
 import java.util.Collections;
+import java.util.Iterator;
 import java.util.List;
 
+import ch.qos.logback.classic.Level;
+import ch.qos.logback.classic.spi.ILoggingEvent;
 import graphql.ExecutionResult;
 import graphql.GraphQL;
 import graphql.Scalars;
+import graphql.relay.ConnectionCursor;
+import graphql.relay.DefaultEdge;
+import graphql.relay.Edge;
+import graphql.relay.PageInfo;
 import graphql.schema.GraphQLFieldDefinition;
 import graphql.schema.GraphQLSchema;
 import io.leangen.geantyref.TypeToken;
 import io.leangen.graphql.annotations.GraphQLArgument;
 import io.leangen.graphql.annotations.GraphQLQuery;
 import io.leangen.graphql.domain.Education;
-import io.leangen.graphql.domain.ExtendedPage;
 import io.leangen.graphql.domain.User;
+import io.leangen.graphql.execution.relay.Connection;
 import io.leangen.graphql.execution.relay.Page;
 import io.leangen.graphql.execution.relay.generic.PageFactory;
+import io.leangen.graphql.generator.OperationMapper;
 import io.leangen.graphql.generator.mapping.common.MapToListTypeAdapter;
 import io.leangen.graphql.generator.mapping.strategy.ObjectScalarStrategy;
 import io.leangen.graphql.services.UserService;
+import io.leangen.graphql.support.TestLog;
+import io.leangen.graphql.util.Urls;
 
+import static io.leangen.graphql.support.QueryResultAssertions.assertValueAtPathEquals;
+import static org.hamcrest.core.Is.is;
+import static org.hamcrest.core.StringContains.containsString;
 import static org.junit.Assert.assertEquals;
 import static org.junit.Assert.assertFalse;
 import static org.junit.Assert.assertNotEquals;
+import static org.junit.Assert.assertThat;
 import static org.junit.Assert.assertTrue;
 
 /**
@@ -52,7 +67,7 @@ public class RelayTest {
             "           addresses(type:\"secret\") {" +
             "               types" +
             "}}}}}}}";
-    
+
     private static final String relayMapInputMutation = "mutation M {" +
             "upMe (input: {" +
             "       clientMutationId: \"123\"," +
@@ -73,7 +88,7 @@ public class RelayTest {
             "       cursor, node {" +
             "           title" +
             "}}}}";
-    
+
     @Test
     public void testOffsetBasedPageCreation() {
         List<User<String>> users = new UserService<String>().getUsersById(1);
@@ -110,10 +125,10 @@ public class RelayTest {
     @Test
     public void testExtendedPageMapping() {
         GraphQLSchema schema = new TestSchemaGenerator()
-                .withOperationsFromSingleton(new BookService())
+                .withOperationsFromSingleton(new ExtendedPageBookService())
                 .generate();
 
-        GraphQLFieldDefinition totalCount = schema.getObjectType("Book2Connection")
+        GraphQLFieldDefinition totalCount = schema.getObjectType("BookConnection")
                 .getFieldDefinition("totalCount");
         assertNotEquals(null, totalCount);
         assertEquals(Scalars.GraphQLLong, totalCount.getType());
@@ -129,6 +144,47 @@ public class RelayTest {
                 "           title" +
                 "}}}}");
         assertTrue(result.getErrors().isEmpty());
+        assertValueAtPathEquals(100L, result, "extended.totalCount");
+        assertValueAtPathEquals("Tesseract", result, "extended.edges.0.node.title");
+    }
+
+    @Test
+    public void testExtendedConnectionMapping() {
+        GraphQLSchema schema = new TestSchemaGenerator()
+                .withOperationsFromSingleton(new ExtendedEdgeBookService())
+                .generate();
+
+        GraphQLFieldDefinition totalCount = schema.getObjectType("BookConnection")
+                .getFieldDefinition("totalCount");
+        assertNotEquals(null, totalCount);
+        assertEquals(Scalars.GraphQLLong, totalCount.getType());
+        GraphQL exe = GraphQLRuntime.newGraphQL(schema).build();
+
+        ExecutionResult result = exe.execute("{extended(first:10, after:\"20\") {" +
+                "   totalCount" +
+                "   pageInfo {" +
+                "       hasNextPage" +
+                "   }," +
+                "   edges {" +
+                "       color, cursor, node {" +
+                "           title" +
+                "}}}}");
+        assertTrue(result.getErrors().isEmpty());
+        assertValueAtPathEquals(100L, result, "extended.totalCount");
+        assertValueAtPathEquals("Tesseract", result, "extended.edges.0.node.title");
+    }
+
+    @Test
+    public void testConflictingConnections() {
+        try (TestLog log = new TestLog(OperationMapper.class)) {
+            new TestSchemaGenerator()
+                    .withOperationsFromSingleton(new ConflictingBookService())
+                    .generate();
+            assertEquals(1, log.getEvents().size());
+            ILoggingEvent event = log.getEvents().get(0);
+            assertThat(event.getLevel(), is(Level.WARN));
+            assertThat(event.getMessage(), containsString(Urls.Errors.NON_UNIQUE_TYPE_NAME));
+        }
     }
 
     private void testPagedQuery(String query) {
@@ -171,12 +227,113 @@ public class RelayTest {
         public Page<Book> getEmpty(@GraphQLArgument(name = "first") int first, @GraphQLArgument(name = "after") String after) {
             return PageFactory.createOffsetBasedPage(Collections.emptyList(), 100, 10);
         }
+    }
 
+    public class ExtendedPageBookService {
         @GraphQLQuery(name = "extended")
         public ExtendedPage<Book> getExtended(@GraphQLArgument(name = "first") int first, @GraphQLArgument(name = "after") String after) {
             List<Book> books = new ArrayList<>();
             books.add(new Book("Tesseract", "x123"));
-            return new ExtendedPage<>(books, 100, 10);
+            long count = 100L;
+            long offset = Long.parseLong(after);
+            return PageFactory.createOffsetBasedPage(books, count, offset, (edges, info) -> new ExtendedPage<>(edges, info, count));
+        }
+    }
+
+    public class ExtendedEdgeBookService {
+        @GraphQLQuery(name = "extended")
+        public ExtendedConnection<ExtendedEdge<Book>> getExtended(@GraphQLArgument(name = "first") int first, @GraphQLArgument(name = "after") String after) {
+            List<Book> books = new ArrayList<>();
+            books.add(new Book("Tesseract", "x123"));
+            long count = 100L;
+            long offset = Long.parseLong(after);
+            Iterator<ExtendedEdge.COLOR> colors = Arrays.asList(ExtendedEdge.COLOR.WHITE, ExtendedEdge.COLOR.BLACK).iterator();
+            return PageFactory.createOffsetBasedConnection(books, count, offset,
+                    (node, cursor) -> new ExtendedEdge<>(node, cursor, colors.next()), (edges, info) -> new ExtendedConnection<>(edges, info, count));
+        }
+    }
+
+    public class ConflictingBookService {
+        @GraphQLQuery(name = "empty")
+        public Page<Book> getEmpty(@GraphQLArgument(name = "first") int first, @GraphQLArgument(name = "after") String after) {
+            return null;
+        }
+
+        @GraphQLQuery(name = "extended")
+        public ExtendedPage<Book> getExtended(@GraphQLArgument(name = "first") int first, @GraphQLArgument(name = "after") String after) {
+            return null;
+        }
+    }
+
+    public static class ExtendedEdge<T> extends DefaultEdge<T> {
+
+        private final COLOR color;
+
+        ExtendedEdge(T node, ConnectionCursor cursor, COLOR color) {
+            super(node, cursor);
+            this.color = color;
+        }
+
+        public COLOR getColor() {
+            return color;
+        }
+
+        public enum COLOR {
+            BLACK, WHITE
+        }
+    }
+
+    public static class ExtendedPage<N> implements Page<N> {
+
+        private final List<Edge<N>> edges;
+        private final PageInfo pageInfo;
+        private final long totalCount;
+
+        ExtendedPage(List<Edge<N>> edges, PageInfo pageInfo, long totalCount) {
+            this.edges = edges;
+            this.pageInfo = pageInfo;
+            this.totalCount = totalCount;
+        }
+
+        @Override
+        public List<Edge<N>> getEdges() {
+            return edges;
+        }
+
+        @Override
+        public PageInfo getPageInfo() {
+            return pageInfo;
+        }
+
+        public long getTotalCount() {
+            return totalCount;
+        }
+    }
+
+    public static class ExtendedConnection<E extends Edge> implements Connection<E> {
+
+        private final List<E> edges;
+        private final PageInfo pageInfo;
+        private final long totalCount;
+
+        ExtendedConnection(List<E> edges, PageInfo pageInfo, long count) {
+            this.edges = edges;
+            this.pageInfo = pageInfo;
+            this.totalCount = count;
+        }
+
+        @Override
+        public List<E> getEdges() {
+            return edges;
+        }
+
+        @Override
+        public PageInfo getPageInfo() {
+            return pageInfo;
+        }
+
+        public long getTotalCount() {
+            return totalCount;
         }
     }
 }
