@@ -6,6 +6,8 @@ import java.lang.reflect.Member;
 import java.lang.reflect.Parameter;
 import java.lang.reflect.Type;
 
+import io.leangen.graphql.util.Urls;
+
 /**
  * Thrown from the mapping process when the type of the object to be mapped can not be determined.
  * Commonly occurs when type information was lost due to type erasure or dynamic proxying.
@@ -31,13 +33,13 @@ public class TypeMappingException extends IllegalArgumentException {
     public TypeMappingException(Member fieldOrMethod, AnnotatedType declaringType, Exception cause) {
         super("The type of member \"" + fieldOrMethod.getName() + "\" belonging to " + declaringType.getType().getTypeName() +
                 " is missing generic type parameters and can not be mapped." +
-                " For details and possible solutions see https://github.com/leangen/graphql-spqr/wiki/Errors#ambiguous-member-type", cause);
+                " For details and possible solutions see " + Urls.Errors.AMBIGUOUS_MEMBER_TYPE, cause);
     }
 
     public TypeMappingException(Executable executable, Parameter parameter, Exception cause) {
         super("Parameter \"" + parameter.getName() + "\" of method \"" + executable.getName() +
                 "\" is missing generic type parameters and can not be mapped." +
-                " For details and possible solutions see https://github.com/leangen/graphql-spqr/wiki/Errors#ambiguous-method-parameter-type", cause);
+                " For details and possible solutions see " + Urls.Errors.AMBIGUOUS_PARAMETER_TYPE, cause);
     }
 
     public TypeMappingException(Type superType, Type subType) {
