@@ -1,14 +1,10 @@
 package io.leangen.graphql.generator.mapping;
 
-import java.lang.reflect.AnnotatedArrayType;
-import java.lang.reflect.AnnotatedParameterizedType;
 import java.lang.reflect.AnnotatedType;
-import java.util.Arrays;
 import java.util.Collections;
 import java.util.List;
 
-import io.leangen.geantyref.GenericTypeReflector;
-import io.leangen.geantyref.TypeFactory;
+import io.leangen.graphql.util.ClassUtils;
 
 /**
  * @author Bojan Tomic (kaqqao)
@@ -37,24 +33,11 @@ public class ConverterRepository {
         return (OutputConverter<T, S>) outputConverters.stream().filter(conv -> conv.supports(outputType)).findFirst().orElse(null);
     }
 
-    public AnnotatedType getMappableType(AnnotatedType type) {
+    public AnnotatedType getMappableInputType(AnnotatedType type) {
         InputConverter converter = this.getInputConverter(type);
         if (converter != null) {
-            return getMappableType(converter.getSubstituteType(type));
+            return getMappableInputType(converter.getSubstituteType(type));
         }
-        if (type.getType() instanceof Class) {
-            return type;
-        }
-        if (type instanceof AnnotatedParameterizedType) {
-            AnnotatedParameterizedType parameterizedType = (AnnotatedParameterizedType) type;
-            AnnotatedType[] arguments = Arrays.stream(parameterizedType.getAnnotatedActualTypeArguments())
-                    .map(this::getMappableType)
-                    .toArray(AnnotatedType[]::new);
-            return TypeFactory.parameterizedAnnotatedClass(GenericTypeReflector.erase(type.getType()), type.getAnnotations(), arguments);
-        }
-        if (type instanceof AnnotatedArrayType) {
-            return TypeFactory.arrayOf(getMappableType(((AnnotatedArrayType) type).getAnnotatedGenericComponentType()), type.getAnnotations());
-        }
-        throw new IllegalArgumentException("Can not find the mappable type for: " + type.getType().getTypeName());
+        return ClassUtils.transformType(type, this::getMappableInputType);
     }
 }
