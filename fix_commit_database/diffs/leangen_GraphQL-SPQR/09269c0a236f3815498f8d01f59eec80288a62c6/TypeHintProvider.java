@@ -1,12 +1,10 @@
 package io.leangen.graphql.generator.proxy;
 
-import graphql.relay.Relay;
-import graphql.schema.DataFetchingEnvironment;
-import io.leangen.graphql.metadata.QueryResolver;
+import graphql.TypeResolutionEnvironment;
 
 /**
  * Created by bojan.tomic on 5/7/16.
  */
 public interface TypeHintProvider {
-    String getGraphQLTypeHint(Object result, DataFetchingEnvironment env, Relay relay, QueryResolver resolver);
+    String getGraphQLTypeHint(Object result, TypeResolutionEnvironment env);
 }
