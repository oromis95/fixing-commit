@@ -1,6 +1,9 @@
 package io.leangen.graphql.generator.mapping.common;
 
+import java.lang.reflect.AnnotatedType;
+
 import io.leangen.graphql.generator.mapping.AbstractTypeAdapter;
+import io.leangen.graphql.query.ExecutionContext;
 
 /**
  * @author Bojan Tomic (kaqqao)
@@ -8,12 +11,12 @@ import io.leangen.graphql.generator.mapping.AbstractTypeAdapter;
 public class VoidToBooleanTypeAdapter extends AbstractTypeAdapter<Void, Boolean> {
 
     @Override
-    public Boolean convertOutput(Void original) {
+    public Boolean convertOutput(Void original, AnnotatedType type, ExecutionContext executionContext) {
         return true;
     }
 
     @Override
-    public Void convertInput(Boolean substitute) {
+    public Void convertInput(Boolean substitute, AnnotatedType type, ExecutionContext executionContext) {
         throw new UnsupportedOperationException("Void used as input");
     }
 }
