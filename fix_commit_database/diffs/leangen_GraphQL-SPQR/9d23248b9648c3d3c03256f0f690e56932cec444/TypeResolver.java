@@ -2,12 +2,12 @@ package io.leangen.graphql.execution;
 
 import graphql.schema.GraphQLObjectType;
 import io.leangen.graphql.generator.TypeRepository;
-import io.leangen.graphql.metadata.strategy.type.TypeMetaDataGenerator;
+import io.leangen.graphql.metadata.strategy.type.TypeInfoGenerator;
 
 /**
  * @author Bojan Tomic (kaqqao)
  */
 public interface TypeResolver {
 
-    GraphQLObjectType resolveType(TypeRepository typeRepository, TypeMetaDataGenerator typeMetaDataGenerator, Object result);
+    GraphQLObjectType resolveType(TypeRepository typeRepository, TypeInfoGenerator typeInfoGenerator, Object result);
 }
