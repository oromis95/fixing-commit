@@ -2,16 +2,16 @@ package io.leangen.graphql.generator;
 
 import java.lang.reflect.AnnotatedType;
 
-import graphql.schema.GraphQLOutputType;
+import graphql.schema.GraphQLObjectType;
 
 /**
  * @author Bojan Tomic (kaqqao)
  */
 public class MappedType {
     public AnnotatedType javaType;
-    public GraphQLOutputType graphQLType;
+    public GraphQLObjectType graphQLType;
 
-    public MappedType(AnnotatedType javaType, GraphQLOutputType graphQLType) {
+    public MappedType(AnnotatedType javaType, GraphQLObjectType graphQLType) {
         this.javaType = javaType;
         this.graphQLType = graphQLType;
     }
