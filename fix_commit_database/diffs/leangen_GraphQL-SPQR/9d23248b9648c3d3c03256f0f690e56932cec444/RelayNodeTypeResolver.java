@@ -1,13 +1,13 @@
 package io.leangen.graphql.generator;
 
-import io.leangen.graphql.metadata.strategy.type.TypeMetaDataGenerator;
+import io.leangen.graphql.metadata.strategy.type.TypeInfoGenerator;
 
 /**
  * @author Bojan Tomic (kaqqao)
  */
 public class RelayNodeTypeResolver extends HintedTypeResolver {
 
-    public RelayNodeTypeResolver(TypeRepository typeRepository, TypeMetaDataGenerator typeMetaDataGenerator) {
-        super("node", typeRepository, typeMetaDataGenerator);
+    public RelayNodeTypeResolver(TypeRepository typeRepository, TypeInfoGenerator typeInfoGenerator) {
+        super("node", typeRepository, typeInfoGenerator);
     }
 }
