@@ -36,6 +36,7 @@ public class AnnotatedResolverBuilder extends FilteredResolverBuilder {
         this.transformer = transformer;
         this.operationNameGenerator = new DelegatingOperationNameGenerator(new AnnotatedOperationNameGenerator(), new MethodOperationNameGenerator());
         this.argumentBuilder = new AnnotatedArgumentBuilder(transformer);
+        withDefaultFilters();
     }
 
     @Override
@@ -50,7 +51,6 @@ public class AnnotatedResolverBuilder extends FilteredResolverBuilder {
 
     private Collection<Resolver> buildQueryResolvers(Object querySourceBean, AnnotatedType beanType, List<Predicate<Member>> filters) {
         Stream<Resolver> methodInvokers = ClassUtils.getAnnotatedMethods(ClassUtils.getRawType(beanType.getType()), GraphQLQuery.class).stream()
-                .filter(REAL_ONLY)
                 .filter(filters.stream().reduce(Predicate::and).orElse(ACCEPT_ALL))
                 .map(method -> new Resolver(
                         operationNameGenerator.generateQueryName(method, beanType, querySourceBean),
@@ -62,7 +62,6 @@ public class AnnotatedResolverBuilder extends FilteredResolverBuilder {
                         method.isAnnotationPresent(GraphQLComplexity.class) ? method.getAnnotation(GraphQLComplexity.class).value() : null
                 ));
         Stream<Resolver> fieldAccessors = ClassUtils.getAnnotatedFields(ClassUtils.getRawType(beanType.getType()), GraphQLQuery.class).stream()
-                .filter(REAL_ONLY)
                 .filter(filters.stream().reduce(Predicate::and).orElse(ACCEPT_ALL))
                 .map(field -> new Resolver(
                         operationNameGenerator.generateQueryName(field, beanType, querySourceBean),
@@ -78,7 +77,6 @@ public class AnnotatedResolverBuilder extends FilteredResolverBuilder {
 
     private Collection<Resolver> buildMutationResolvers(Object querySourceBean, AnnotatedType beanType, List<Predicate<Member>> filters) {
         return ClassUtils.getAnnotatedMethods(ClassUtils.getRawType(beanType.getType()), GraphQLMutation.class).stream()
-                .filter(REAL_ONLY)
                 .filter(filters.stream().reduce(Predicate::and).orElse(ACCEPT_ALL))
                 .map(method -> new Resolver(
                         operationNameGenerator.generateMutationName(method, beanType, querySourceBean),
@@ -94,7 +92,6 @@ public class AnnotatedResolverBuilder extends FilteredResolverBuilder {
     @Override
     public Collection<Resolver> buildSubscriptionResolvers(Object querySourceBean, AnnotatedType beanType) {
         return ClassUtils.getAnnotatedMethods(ClassUtils.getRawType(beanType.getType()), GraphQLSubscription.class).stream()
-                .filter(REAL_ONLY)
                 .filter(filters.stream().reduce(Predicate::and).orElse(ACCEPT_ALL))
                 .map(method -> new Resolver(
                         operationNameGenerator.generateSubscriptionName(method, beanType, querySourceBean),
