@@ -1,6 +1,9 @@
 package io.leangen.graphql.metadata;
 
 import java.lang.reflect.AnnotatedType;
+import java.util.Objects;
+
+import io.leangen.geantyref.GenericTypeReflector;
 
 public class InputField {
 
@@ -25,4 +28,22 @@ public class InputField {
     public AnnotatedType getJavaType() {
         return javaType;
     }
+
+    @Override
+    public int hashCode() {
+        return Objects.hash(name, description) ^ GenericTypeReflector.hashCode(javaType);
+    }
+
+    @Override
+    public boolean equals(Object other) {
+        if (other == this) {
+            return true;
+        }
+        if (!(other instanceof InputField)) {
+            return false;
+        }
+        InputField that = (InputField) other;
+        return Objects.equals(this.name, that.name) && Objects.equals(this.description, that.description)
+                && GenericTypeReflector.equals(this.javaType, that.javaType);
+    }
 }
