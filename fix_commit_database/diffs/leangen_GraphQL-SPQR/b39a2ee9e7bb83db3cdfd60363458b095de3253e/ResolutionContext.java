@@ -21,19 +21,17 @@ public class ResolutionContext {
     public final Object source;
     public final Object context;
     public final ValueMapper valueMapper;
-    public final ConnectionRequest connectionRequest;
     public final GlobalContext globalContext;
     public final List<Field> fields;
     public final GraphQLOutputType fieldType;
     public final GraphQLType parentType;
     public final GraphQLSchema graphQLSchema;
 
-    public ResolutionContext(DataFetchingEnvironment env, ConnectionRequest connectionRequest, ValueMapper valueMapper, GlobalContext globalContext) {
+    public ResolutionContext(DataFetchingEnvironment env, ValueMapper valueMapper, GlobalContext globalContext) {
         
         this.source = env.getSource();
         this.context = env.getContext();
         this.valueMapper = valueMapper;
-        this.connectionRequest = connectionRequest;
         this.globalContext = globalContext;
         this.fields = env.getFields();
         this.fieldType = env.getFieldType();
@@ -43,12 +41,18 @@ public class ResolutionContext {
 
     @SuppressWarnings("unchecked")
     public Object convertOutput(Object output, AnnotatedType type) {
+        if (output == null) {
+            return null;
+        }
         OutputConverter outputConverter = this.globalContext.converters.getOutputConverter(type);
         return outputConverter == null ? output : outputConverter.convertOutput(output, type, this);
     }
 
     @SuppressWarnings("unchecked")
     public Object convertInput(Object input, AnnotatedType type, ResolutionContext resolutionContext) {
+        if (input == null) {
+            return null;
+        }
         InputConverter inputConverter = this.globalContext.converters.getInputConverter(type);
         return inputConverter == null ? input : inputConverter.convertInput(input, type, resolutionContext);
     }
