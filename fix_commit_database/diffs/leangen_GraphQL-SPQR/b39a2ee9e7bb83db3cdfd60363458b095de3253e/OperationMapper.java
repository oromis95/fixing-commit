@@ -127,7 +127,8 @@ public class OperationMapper {
                 .name(operation.getName())
                 .description(operation.getName())
                 .type(type);
-        operation.getArguments()
+        operation.getArguments().stream()
+                .filter(argument -> !argument.isRootContext())
                 .forEach(argument -> queryBuilder.argument(toGraphQLArgument(argument, abstractTypes, buildContext)));
         if (type.getName() != null && !type.getName().equals("Connection") && type.getName().endsWith("Connection")) {
             queryBuilder.argument(buildContext.relay.getConnectionFieldArguments());
