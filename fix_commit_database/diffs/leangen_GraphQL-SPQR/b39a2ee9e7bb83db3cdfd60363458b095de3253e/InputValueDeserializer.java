@@ -12,6 +12,9 @@ public class InputValueDeserializer implements ArgumentInjector {
     
     @Override
     public Object getArgumentValue(Object input, AnnotatedType type, ResolutionContext resolutionContext) {
+        if (input == null) {
+            return null;
+        }
         AnnotatedType argValueType = resolutionContext.getMappableType(type);
         return resolutionContext.valueMapper.fromInput(input, argValueType);
     }
