@@ -2,10 +2,8 @@ package io.leangen.graphql.metadata.strategy.query;
 
 import java.lang.reflect.AnnotatedType;
 import java.lang.reflect.Method;
-import java.lang.reflect.Parameter;
 import java.util.List;
 
-import io.leangen.graphql.execution.ConnectionRequest;
 import io.leangen.graphql.metadata.OperationArgument;
 
 /**
@@ -14,8 +12,4 @@ import io.leangen.graphql.metadata.OperationArgument;
 public interface ResolverArgumentBuilder {
 
     List<OperationArgument> buildResolverArguments(Method resolverMethod, AnnotatedType enclosingType);
-
-    default boolean isExplicitRelayConnectionArgument(Parameter parameter) {
-        return ConnectionRequest.class.isAssignableFrom(parameter.getType());
-    }
 }
