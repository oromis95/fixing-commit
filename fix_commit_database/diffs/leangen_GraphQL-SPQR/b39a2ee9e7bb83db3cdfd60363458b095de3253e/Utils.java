@@ -1,6 +1,8 @@
 package io.leangen.graphql.util;
 
+import java.util.Arrays;
 import java.util.Optional;
+import java.util.stream.Stream;
 
 /**
  * A collection of utility methods
@@ -15,4 +17,9 @@ public class Utils {
     public static boolean notEmpty(String string) {
         return string != null && !string.isEmpty();
     }
+    
+    @SafeVarargs
+    public static <T> Stream<T> concat(Stream<T>... streams) {
+        return Arrays.stream(streams).reduce(Stream::concat).orElse(Stream.empty());
+    }
 }
