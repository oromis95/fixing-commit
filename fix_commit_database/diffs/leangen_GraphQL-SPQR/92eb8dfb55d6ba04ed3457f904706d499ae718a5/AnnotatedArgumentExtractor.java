@@ -1,5 +1,11 @@
 package io.leangen.graphql.metadata.strategy.query;
 
+import java.lang.reflect.AnnotatedType;
+import java.lang.reflect.Method;
+import java.lang.reflect.Parameter;
+import java.util.ArrayList;
+import java.util.List;
+
 import io.leangen.graphql.annotations.GraphQLArgument;
 import io.leangen.graphql.annotations.GraphQLContext;
 import io.leangen.graphql.annotations.GraphQLResolverSource;
@@ -7,12 +13,6 @@ import io.leangen.graphql.annotations.RelayConnectionRequest;
 import io.leangen.graphql.metadata.QueryArgument;
 import io.leangen.graphql.util.ClassUtils;
 
-import java.lang.reflect.AnnotatedType;
-import java.lang.reflect.Method;
-import java.lang.reflect.Parameter;
-import java.util.ArrayList;
-import java.util.List;
-
 /**
  * Created by bojan.tomic on 7/17/16.
  */
@@ -27,7 +27,7 @@ public class AnnotatedArgumentExtractor implements QueryResolverArgumentExtracto
             GraphQLArgument meta = parameter.getAnnotation(GraphQLArgument.class);
             ClassUtils.checkIfResolvable(parameterTypes[i], resolverMethod); //checks if the type is resolvable
             queryArguments.add(new QueryArgument(
-                    parameterTypes[i],
+                    ClassUtils.stripBounds(parameterTypes[i]),
                     meta != null && !meta.name().isEmpty() ? meta.name() : parameter.getName(),
                     meta != null ? meta.description() : null,
                     meta != null && meta.required(),
