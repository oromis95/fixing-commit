@@ -1,6 +1,11 @@
 package io.leangen.graphql.generator;
 
+import java.util.HashSet;
+import java.util.Set;
+
 import graphql.relay.Relay;
+import graphql.schema.TypeResolver;
+import io.leangen.graphql.generator.mapping.ConverterRepository;
 import io.leangen.graphql.generator.mapping.TypeMapperRepository;
 import io.leangen.graphql.generator.proxy.ProxyFactory;
 import io.leangen.graphql.generator.strategy.AbstractTypeGenerationStrategy;
@@ -8,11 +13,8 @@ import io.leangen.graphql.generator.strategy.FlatTypeGenerationStrategy;
 import io.leangen.graphql.metadata.strategy.input.GsonInputDeserializer;
 import io.leangen.graphql.query.DefaultIdTypeMapper;
 import io.leangen.graphql.query.ExecutionContext;
+import io.leangen.graphql.query.HintedTypeResolver;
 import io.leangen.graphql.query.IdTypeMapper;
-import io.leangen.graphql.generator.mapping.ConverterRepository;
-
-import java.util.HashSet;
-import java.util.Set;
 
 /**
  * Created by bojan.tomic on 3/30/16.
@@ -27,13 +29,17 @@ public class BuildContext {
     public final IdTypeMapper idTypeMapper;
     public final TypeMapperRepository typeMappers;
     public final Relay relay;
+    public final TypeResolver typeResolver;
 
     public final Set<String> inputsInProgress = new HashSet<>();
 
-    public enum TypeGenerationMode {
-        FLAT
-    }
-
+    /**
+     *
+     * @param queryRepository Repository that can be used to fetch all known (singleton and domain) queries
+     * @param typeMappers Repository of all registered {@link io.leangen.graphql.generator.mapping.TypeMapper}s
+     * @param converters Repository of all registered {@link io.leangen.graphql.generator.mapping.InputConverter}s
+     *                   and {@link io.leangen.graphql.generator.mapping.OutputConverter}s
+     */
     public BuildContext(QueryRepository queryRepository, TypeMapperRepository typeMappers, ConverterRepository converters) {
         this.typeStrategy = new FlatTypeGenerationStrategy(queryRepository);
         this.queryRepository = queryRepository;
@@ -42,6 +48,7 @@ public class BuildContext {
         this.typeMappers = typeMappers;
         this.proxyFactory = new ProxyFactory();
         this.relay = new Relay();
+        this.typeResolver = new HintedTypeResolver(this.typeRepository);
         this.executionContext = new ExecutionContext(relay, typeRepository, proxyFactory, idTypeMapper, new GsonInputDeserializer(), converters);
     }
 }
