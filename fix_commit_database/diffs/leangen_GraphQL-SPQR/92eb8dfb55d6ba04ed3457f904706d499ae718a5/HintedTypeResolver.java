@@ -1,12 +1,13 @@
 package io.leangen.graphql.query;
 
+import java.util.List;
+
 import graphql.schema.GraphQLObjectType;
+import graphql.schema.GraphQLOutputType;
 import graphql.schema.TypeResolver;
 import io.leangen.graphql.generator.TypeRepository;
 import io.leangen.graphql.generator.proxy.RelayNodeProxy;
 
-import java.util.List;
-
 /**
  * Created by bojan.tomic on 5/7/16.
  */
@@ -20,10 +21,10 @@ public class HintedTypeResolver implements TypeResolver {
 
     @Override
     public GraphQLObjectType getType(Object object) {
-        List<GraphQLObjectType> candidates = typeRepository.getOutputTypes(object.getClass());
+        List<GraphQLOutputType> candidates = typeRepository.getOutputTypes(object.getClass());
         if (candidates.size() == 1) {
-            return candidates.get(0);
+            return (GraphQLObjectType) candidates.get(0);
         }
-        return typeRepository.getOutputType(((RelayNodeProxy) object).getGraphQLTypeHint());
+        return (GraphQLObjectType) typeRepository.getOutputType(((RelayNodeProxy) object).getGraphQLTypeHint());
     }
 }
