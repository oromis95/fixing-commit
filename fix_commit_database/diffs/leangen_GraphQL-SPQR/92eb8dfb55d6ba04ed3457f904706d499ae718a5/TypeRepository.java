@@ -9,7 +9,7 @@ import java.util.Optional;
 import java.util.stream.Collectors;
 
 import graphql.schema.GraphQLInputType;
-import graphql.schema.GraphQLObjectType;
+import graphql.schema.GraphQLOutputType;
 import io.leangen.geantyref.GenericTypeReflector;
 import io.leangen.graphql.metadata.DomainType;
 
@@ -18,11 +18,11 @@ import io.leangen.graphql.metadata.DomainType;
  */
 public class TypeRepository {
 
-    private Map<String, GraphQLObjectType> outputTypesByName = new HashMap<>();
-    private Map<Type, List<GraphQLObjectType>> outputTypesByClass = new HashMap<>();
+    private Map<String, GraphQLOutputType> outputTypesByName = new HashMap<>();
+    private Map<Type, List<GraphQLOutputType>> outputTypesByClass = new HashMap<>();
     private Map<Type, GraphQLInputType> inputTypesByClass = new HashMap<>();
 
-    public void registerType(DomainType domainType, GraphQLObjectType type) {
+    public void registerType(DomainType domainType, GraphQLOutputType type) {
         this.outputTypesByName.put(type.getName(), type);
         this.outputTypesByClass.putIfAbsent(domainType.getJavaType().getType(), new ArrayList<>());
         this.outputTypesByClass.get(domainType.getJavaType().getType()).add(type);
@@ -32,7 +32,7 @@ public class TypeRepository {
         this.inputTypesByClass.put(javaType, type);
     }
 
-    public GraphQLObjectType getOutputType(String name) {
+    public GraphQLOutputType getOutputType(String name) {
         return outputTypesByName.get(name);
     }
 
@@ -40,7 +40,7 @@ public class TypeRepository {
         return Optional.ofNullable(inputTypesByClass.get(javaType));
     }
 
-    public List<GraphQLObjectType> getOutputTypes(Type javaType) {
+    public List<GraphQLOutputType> getOutputTypes(Type javaType) {
         return this.outputTypesByClass.entrySet().stream()
                 .filter(entry -> GenericTypeReflector.isSuperType(entry.getKey(), javaType))
                 .flatMap(entry -> entry.getValue().stream())
