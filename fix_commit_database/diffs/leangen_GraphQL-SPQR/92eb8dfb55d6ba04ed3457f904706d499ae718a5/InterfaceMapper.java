@@ -0,0 +1,46 @@
+package io.leangen.graphql.generator.mapping.common;
+
+import java.lang.reflect.AnnotatedType;
+
+import graphql.schema.GraphQLInterfaceType;
+import graphql.schema.GraphQLOutputType;
+import io.leangen.graphql.generator.BuildContext;
+import io.leangen.graphql.generator.QueryGenerator;
+import io.leangen.graphql.generator.strategy.AbstractTypeGenerationStrategy;
+import io.leangen.graphql.metadata.DomainType;
+import io.leangen.graphql.util.ClassUtils;
+
+import static graphql.schema.GraphQLInterfaceType.newInterface;
+
+/**
+ * @author Bojan Tomic (kaqqao)
+ */
+public class InterfaceMapper extends ObjectTypeMapper {
+
+    @Override
+    public GraphQLOutputType toGraphQLType(AnnotatedType javaType, BuildContext buildContext, QueryGenerator queryGenerator) {
+        DomainType domainType = new DomainType(javaType);
+
+        AbstractTypeGenerationStrategy.Entry typeEntry = buildContext.typeStrategy.get(domainType);
+        if (typeEntry.type.isPresent()) {
+            return typeEntry.type.get();
+        }
+
+        GraphQLInterfaceType.Builder typeBuilder = newInterface()
+                .name(typeEntry.name)
+                .description(domainType.getDescription());
+
+        buildContext.queryRepository.getChildQueries(domainType.getJavaType())
+                .forEach(childQuery -> typeBuilder.field(queryGenerator.toGraphQLQuery(childQuery, typeEntry.name, buildContext)));
+
+        typeBuilder.typeResolver(buildContext.typeResolver);
+        GraphQLInterfaceType type = typeBuilder.build();
+        buildContext.typeRepository.registerType(domainType, type);
+        return type;
+    }
+
+    @Override
+    public boolean supports(AnnotatedType type) {
+        return ClassUtils.getRawType(type.getType()).isInterface();
+    }
+}
