@@ -1,10 +1,9 @@
 package io.leangen.graphql.metadata.strategy.query;
 
-import java.io.Serializable;
-import java.lang.annotation.Annotation;
 import java.lang.reflect.AnnotatedType;
 import java.lang.reflect.Type;
 import java.util.ArrayList;
+import java.util.Arrays;
 import java.util.List;
 import java.util.Map;
 import java.util.Objects;
@@ -13,6 +12,7 @@ import java.util.stream.Collectors;
 
 import io.leangen.geantyref.GenericTypeReflector;
 import io.leangen.graphql.annotations.GraphQLUnion;
+import io.leangen.graphql.generator.exceptions.TypeMappingException;
 import io.leangen.graphql.generator.union.Union;
 import io.leangen.graphql.metadata.Operation;
 import io.leangen.graphql.metadata.OperationArgument;
@@ -20,19 +20,46 @@ import io.leangen.graphql.metadata.OperationArgumentDefaultValue;
 import io.leangen.graphql.metadata.Resolver;
 import io.leangen.graphql.util.ClassUtils;
 
-import static java.util.Arrays.stream;
-
 /**
  * @author Bojan Tomic (kaqqao)
  */
 public class DefaultOperationBuilder implements OperationBuilder {
 
+    private final TypeInference typeInference;
+
+    /**
+     * @param typeInference Controls automatic type inference if multiple resolver methods for the same operation return different types,
+     * or if different resolver methods specify arguments of the same name but of different types.
+     * <p>The inference process selects the most specific common super type of all the detected types.</p>
+     * <p>This feature is off by default as it can lead to surprising results when used unconsciously.</p>
+     *
+     * <p><b>Example:</b></p>
+     *
+     * <pre>
+     * {@code
+     * @GraphQLQuery(name = "numbers")
+     * public ArrayList<Long> getLongs(String paramOne) {...}
+     *
+     * @GraphQLQuery(name = "numbers")
+     * public LinkedList<Double> getDoubles(String paramTwo) {...}
+     * }
+     * </pre>
+     *
+     * The situation shown above would cause an exception without type inference enabled.
+     * With type inference, the return type would be treated as {@code AbstractList<Number>} as that
+     * is the most specific common super type of the two encountered types.
+     */
+
+    public DefaultOperationBuilder(TypeInference typeInference) {
+        this.typeInference = typeInference;
+    }
+
     @Override
     public Operation buildQuery(List<Resolver> resolvers) {
         String name = resolveName(resolvers);
         AnnotatedType javaType = resolveJavaType(name, resolvers);
-        List<Type> contextTypes = resolveContextTypes(resolvers);
-        List<OperationArgument> arguments = collectArguments(resolvers);
+        List<Type> contextTypes = resolveContextTypes(name, resolvers);
+        List<OperationArgument> arguments = collectArguments(name, resolvers);
         boolean batched = isBatched(resolvers);
         return new Operation(name, javaType, contextTypes, arguments, resolvers, batched);
     }
@@ -46,7 +73,7 @@ public class DefaultOperationBuilder implements OperationBuilder {
         return resolvers.get(0).getOperationName();
     }
 
-    protected AnnotatedType resolveJavaType(String queryName, List<Resolver> resolvers) {
+    protected AnnotatedType resolveJavaType(String operationName, List<Resolver> resolvers) {
         List<AnnotatedType> returnTypes = resolvers.stream()
                 .map(Resolver::getReturnType)
                 .collect(Collectors.toList());
@@ -55,39 +82,29 @@ public class DefaultOperationBuilder implements OperationBuilder {
             return unionize(returnTypes.toArray(new AnnotatedType[returnTypes.size()]));
         }
 
-        AnnotatedType mostSpecificSuperType = ClassUtils.getCommonSuperType(returnTypes);
-        if (returnTypes.stream().noneMatch(type -> type.getType().equals(Object.class))
-                && (mostSpecificSuperType.getType().equals(Object.class)
-                || mostSpecificSuperType.getType().equals(Cloneable.class)
-                || mostSpecificSuperType.getType().equals(Serializable.class))) {
-            throw new IllegalArgumentException("Resolvers for query " + queryName + " do not return compatible types, or the types were lost to erasure");
-        }
-        Annotation[] aggregatedAnnotations = resolvers.stream()
-                .flatMap(resolver -> stream(resolver.getReturnType().getAnnotations()))
-                .distinct()
-                .toArray(Annotation[]::new);
-        return GenericTypeReflector.replaceAnnotations(mostSpecificSuperType, aggregatedAnnotations);
+        return resolveJavaType(returnTypes, "Multiple methods detected for operation \"" + operationName + "\" with different return types.");
     }
 
-    protected List<Type> resolveContextTypes(List<Resolver> resolvers) {
+    protected List<Type> resolveContextTypes(String operationName, List<Resolver> resolvers) {
         Set<Type> sourceTypes = resolvers.get(0).getSourceTypes();
         boolean allSame = resolvers.stream().map(Resolver::getSourceTypes)
                 .allMatch(types -> types.size() == sourceTypes.size() && types.containsAll(sourceTypes));
         if (!allSame) {
-            throw new IllegalStateException("Not all resolvers expect the same source types");
+            throw new IllegalStateException("Not all resolver methods for operation \"" + operationName + "\" expect the same source types");
         }
         return new ArrayList<>(sourceTypes);
     }
 
     //TODO do annotations or overloading decide what arg is required? should that decision be externalized?
-    protected List<OperationArgument> collectArguments(List<Resolver> resolvers) {
+    protected List<OperationArgument> collectArguments(String operationName, List<Resolver> resolvers) {
         Map<String, List<OperationArgument>> argumentsByName = resolvers.stream()
                 .flatMap(resolver -> resolver.getArguments().stream()) // merge all known args for this query
                 .collect(Collectors.groupingBy(OperationArgument::getName));
 
+        String errorPrefixTemplate = "Argument %s of operation \"" + operationName + "\" has different types in different resolver methods.";
         return argumentsByName.keySet().stream()
                 .map(argName -> new OperationArgument(
-                        ClassUtils.getCommonSuperType(argumentsByName.get(argName).stream().map(OperationArgument::getJavaType).collect(Collectors.toList())),
+                        resolveJavaType(argumentsByName.get(argName).stream().map(OperationArgument::getJavaType).collect(Collectors.toList()), String.format(errorPrefixTemplate, argName)),
                         argName,
                         argumentsByName.get(argName).stream().map(OperationArgument::getDescription).filter(Objects::nonNull).findFirst().orElse(""),
 //						argumentsByName.get(argName).size() == resolvers.size() || argumentsByName.get(argName).stream().anyMatch(OperationArgument::isRequired),
@@ -105,4 +122,36 @@ public class DefaultOperationBuilder implements OperationBuilder {
     protected AnnotatedType unionize(AnnotatedType[] types) {
         return Union.unionize(types);
     }
+
+    private AnnotatedType resolveJavaType(List<AnnotatedType> types, String errorPrefix) {
+        errorPrefix = errorPrefix + " Types found: " + Arrays.toString(types.stream().map(type -> type.getType().getTypeName()).toArray()) + ". ";
+        if (!typeInference.inferTypes && !types.stream().map(AnnotatedType::getType).allMatch(type -> type.equals(types.get(0).getType()))) {
+            throw new TypeMappingException(errorPrefix + "If this is intentional, and you wish GraphQL SPQR to infer the most " +
+                    "common super type automatically, see https://github.com/leangen/graphql-spqr/wiki/Errors#operation-with-multiple-resolver-methods-with-different-types");
+        }
+        try {
+            return ClassUtils.getCommonSuperType(types, typeInference.allowObject ? GenericTypeReflector.annotate(Object.class) : null);
+        } catch (TypeMappingException e) {
+            throw new TypeMappingException(errorPrefix, e);
+        }
+    }
+
+    /**
+     * <p>{@code NONE} - No type inference. Results in a {@link TypeMappingException} if multiple different types are encountered.</p>
+     * <p>{@code LIMITED} - Automatically infer the common super type. Results in a {@link TypeMappingException} if no common ancestors except
+     * {@link Object}, {@link java.io.Serializable}, {@link Cloneable}, {@link Comparable} or {@link java.lang.annotation.Annotation} are found.</p>
+     * <p>{@code UNRESTRICTED} - Automatically infer the common super type. Results in {@link Object} if no common ancestors are found.</p>
+     */
+    public enum TypeInference {
+        
+        NONE(false, false), LIMITED(true, false), UNLIMITED(true, true);
+        
+        public final boolean inferTypes;
+        public final boolean allowObject;
+
+        TypeInference(boolean inferTypes, boolean allowObject) {
+            this.inferTypes = inferTypes;
+            this.allowObject = allowObject;
+        }
+    }
 }
