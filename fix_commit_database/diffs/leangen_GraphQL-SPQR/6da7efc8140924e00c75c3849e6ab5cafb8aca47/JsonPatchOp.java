@@ -0,0 +1,10 @@
+package io.leangen.graphql.domain;
+
+public enum JsonPatchOp {
+    add,
+    remove,
+    replace,
+    copy,
+    move,
+    test
+}
