@@ -3,8 +3,10 @@ package io.leangen.graphql.generator.mapping.common;
 import java.lang.reflect.AnnotatedType;
 import java.util.OptionalInt;
 
+import io.leangen.graphql.execution.GlobalEnvironment;
 import io.leangen.graphql.execution.ResolutionEnvironment;
 import io.leangen.graphql.generator.mapping.AbstractTypeAdapter;
+import io.leangen.graphql.metadata.strategy.value.ValueMapper;
 
 public class OptionalIntAdapter extends AbstractTypeAdapter<OptionalInt, Integer> {
     
@@ -14,7 +16,7 @@ public class OptionalIntAdapter extends AbstractTypeAdapter<OptionalInt, Integer
     }
 
     @Override
-    public OptionalInt convertInput(Integer substitute, AnnotatedType type, ResolutionEnvironment resolutionEnvironment) {
+    public OptionalInt convertInput(Integer substitute, AnnotatedType type, GlobalEnvironment environment, ValueMapper valueMapper) {
         return substitute == null ? OptionalInt.empty() : OptionalInt.of(substitute);
     }
 }
