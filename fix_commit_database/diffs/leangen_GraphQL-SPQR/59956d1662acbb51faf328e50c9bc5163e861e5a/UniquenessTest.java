@@ -14,6 +14,8 @@ import graphql.schema.GraphQLSchema;
 import graphql.schema.GraphQLType;
 import io.leangen.graphql.domain.SimpleUser;
 import io.leangen.graphql.execution.relay.Page;
+import io.leangen.graphql.generator.mapping.common.MapToListTypeAdapter;
+import io.leangen.graphql.generator.mapping.strategy.ObjectScalarStrategy;
 import io.leangen.graphql.metadata.strategy.query.PublicResolverBuilder;
 
 import static org.junit.Assert.assertEquals;
@@ -37,7 +39,11 @@ public class UniquenessTest {
 
     @Test
     public void testMapUniqueness() {
-        GraphQLSchema schema = schemaFor(new MapService());
+        GraphQLSchema schema = new TestSchemaGenerator()
+                .withTypeAdapters(new MapToListTypeAdapter<>(new ObjectScalarStrategy()))
+                .withDefaults()
+                .withOperationsFromSingleton(new MapService(), new PublicResolverBuilder(thisPackage))
+                .generate();
         testRootQueryTypeUniqueness(schema, type -> ((GraphQLList) type).getWrappedType());
         testRootQueryArgumentTypeUniqueness(schema, type -> ((GraphQLList) type).getWrappedType());
     }
