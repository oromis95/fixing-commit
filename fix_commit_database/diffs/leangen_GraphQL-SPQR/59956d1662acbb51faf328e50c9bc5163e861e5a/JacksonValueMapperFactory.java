@@ -1,10 +1,11 @@
 package io.leangen.graphql.metadata.strategy.value.jackson;
 
+import com.fasterxml.jackson.core.Version;
+import com.fasterxml.jackson.databind.Module;
 import com.fasterxml.jackson.databind.ObjectMapper;
 import com.fasterxml.jackson.databind.jsontype.NamedType;
 
 import java.lang.reflect.Type;
-import java.util.Collections;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
@@ -12,6 +13,7 @@ import java.util.Set;
 import java.util.stream.Collectors;
 
 import io.leangen.geantyref.GenericTypeReflector;
+import io.leangen.graphql.execution.GlobalEnvironment;
 import io.leangen.graphql.metadata.strategy.type.DefaultTypeInfoGenerator;
 import io.leangen.graphql.metadata.strategy.type.TypeInfoGenerator;
 import io.leangen.graphql.metadata.strategy.value.ValueMapperFactory;
@@ -25,7 +27,6 @@ public class JacksonValueMapperFactory implements ValueMapperFactory<JacksonValu
     private final String basePackage;
     private final Configurer configurer;
     private final TypeInfoGenerator typeInfoGenerator;
-    private final JacksonValueMapper defaultValueMapper;
 
     public JacksonValueMapperFactory() {
         this(null);
@@ -34,7 +35,7 @@ public class JacksonValueMapperFactory implements ValueMapperFactory<JacksonValu
     public JacksonValueMapperFactory(String basePackage) {
         this(basePackage, new DefaultTypeInfoGenerator());
     }
-    
+
     public JacksonValueMapperFactory(String basePackage, TypeInfoGenerator typeInfoGenerator) {
         this(basePackage, typeInfoGenerator, new AbstractClassAdapterConfigurer());
     }
@@ -43,26 +44,25 @@ public class JacksonValueMapperFactory implements ValueMapperFactory<JacksonValu
         this.basePackage = basePackage;
         this.configurer = configurer;
         this.typeInfoGenerator = typeInfoGenerator;
-        this.defaultValueMapper = new JacksonValueMapper(
-                this.configurer.configure(new ObjectMapper(), Collections.emptySet(), basePackage, typeInfoGenerator));
     }
 
     @Override
-    public JacksonValueMapper getValueMapper(Set<Type> abstractTypes) {
-        if (abstractTypes.isEmpty()) {
-            return this.defaultValueMapper;
-        }
-        ObjectMapper objectMapper = this.configurer.configure(new ObjectMapper(), abstractTypes, basePackage, this.typeInfoGenerator);
+    public JacksonValueMapper getValueMapper(Set<Type> abstractTypes, GlobalEnvironment environment) {
+        ObjectMapper objectMapper = this.configurer.configure(new ObjectMapper(), abstractTypes, basePackage, this.typeInfoGenerator, environment);
         return new JacksonValueMapper(objectMapper);
     }
 
     public static class AbstractClassAdapterConfigurer implements Configurer {
 
         @Override
-        public ObjectMapper configure(ObjectMapper objectMapper, Set<Type> abstractTypes, String basePackage, TypeInfoGenerator metaDataGen) {
-            return objectMapper
+        public ObjectMapper configure(ObjectMapper objectMapper, Set<Type> abstractTypes, String basePackage, TypeInfoGenerator metaDataGen, GlobalEnvironment environment) {
+            ObjectMapper mapper = objectMapper
                     .findAndRegisterModules()
                     .setAnnotationIntrospector(new AnnotationIntrospector(collectSubtypes(abstractTypes, basePackage, metaDataGen)));
+            if (environment != null && !environment.getInputConverters().isEmpty()) {
+                mapper.registerModule(getDeserializersModule(environment));
+            }
+            return mapper;
         }
 
         private Map<Type, List<NamedType>> collectSubtypes(Set<Type> abstractTypes, String basePackage, TypeInfoGenerator metaDataGen) {
@@ -80,11 +80,30 @@ public class JacksonValueMapperFactory implements ValueMapperFactory<JacksonValu
             }
             return types;
         }
+
+        private Module getDeserializersModule(GlobalEnvironment environment) {
+            return new Module() {
+                @Override
+                public String getModuleName() {
+                    return "graphql-spqr-deserializers";
+                }
+
+                @Override
+                public Version version() {
+                    return Version.unknownVersion();
+                }
+
+                @Override
+                public void setupModule(SetupContext setupContext) {
+                    setupContext.addDeserializers(new ConvertingDeserializers(environment));
+                }
+            };
+        }
     }
 
     @FunctionalInterface
     public interface Configurer {
-        ObjectMapper configure(ObjectMapper objectMapper, Set<Type> abstractTypes, String basePackage, TypeInfoGenerator metaDataGen);
+        ObjectMapper configure(ObjectMapper objectMapper, Set<Type> abstractTypes, String basePackage, TypeInfoGenerator metaDataGen, GlobalEnvironment environment);
     }
 
     @Override
