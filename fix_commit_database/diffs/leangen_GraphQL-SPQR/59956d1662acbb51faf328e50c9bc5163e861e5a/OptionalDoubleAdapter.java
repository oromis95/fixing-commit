@@ -3,8 +3,10 @@ package io.leangen.graphql.generator.mapping.common;
 import java.lang.reflect.AnnotatedType;
 import java.util.OptionalDouble;
 
+import io.leangen.graphql.execution.GlobalEnvironment;
 import io.leangen.graphql.execution.ResolutionEnvironment;
 import io.leangen.graphql.generator.mapping.AbstractTypeAdapter;
+import io.leangen.graphql.metadata.strategy.value.ValueMapper;
 
 public class OptionalDoubleAdapter extends AbstractTypeAdapter<OptionalDouble, Double> {
     
@@ -14,7 +16,7 @@ public class OptionalDoubleAdapter extends AbstractTypeAdapter<OptionalDouble, D
     }
 
     @Override
-    public OptionalDouble convertInput(Double substitute, AnnotatedType type, ResolutionEnvironment resolutionEnvironment) {
+    public OptionalDouble convertInput(Double substitute, AnnotatedType type, GlobalEnvironment environment, ValueMapper valueMapper) {
         return substitute == null ? OptionalDouble.empty() : OptionalDouble.of(substitute);
     }
 }
