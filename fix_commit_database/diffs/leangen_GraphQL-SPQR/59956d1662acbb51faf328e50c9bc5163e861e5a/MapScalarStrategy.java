@@ -7,7 +7,10 @@ import io.leangen.geantyref.GenericTypeReflector;
 
 /**
  * A scalar mapping strategy that treats all {@link Map}s as scalars
+ *
+ * @deprecated The behavior of this strategy is now included in the {@link DefaultScalarStrategy}
  */
+@Deprecated
 public class MapScalarStrategy implements ScalarMappingStrategy {
     
     @Override
