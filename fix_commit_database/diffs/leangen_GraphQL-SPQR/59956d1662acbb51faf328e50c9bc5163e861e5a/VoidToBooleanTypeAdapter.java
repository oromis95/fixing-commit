@@ -2,8 +2,10 @@ package io.leangen.graphql.generator.mapping.common;
 
 import java.lang.reflect.AnnotatedType;
 
+import io.leangen.graphql.execution.GlobalEnvironment;
 import io.leangen.graphql.execution.ResolutionEnvironment;
 import io.leangen.graphql.generator.mapping.AbstractTypeAdapter;
+import io.leangen.graphql.metadata.strategy.value.ValueMapper;
 
 /**
  * @author Bojan Tomic (kaqqao)
@@ -16,7 +18,7 @@ public class VoidToBooleanTypeAdapter extends AbstractTypeAdapter<Void, Boolean>
     }
 
     @Override
-    public Void convertInput(Boolean substitute, AnnotatedType type, ResolutionEnvironment resolutionEnvironment) {
+    public Void convertInput(Boolean substitute, AnnotatedType type, GlobalEnvironment environment, ValueMapper valueMapper) {
         throw new UnsupportedOperationException("Void used as input");
     }
 }
