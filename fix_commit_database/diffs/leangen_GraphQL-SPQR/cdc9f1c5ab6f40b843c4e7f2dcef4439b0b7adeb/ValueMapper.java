@@ -5,6 +5,8 @@ import java.lang.reflect.Type;
 
 public interface ValueMapper {
 
+    String TYPE_METADATA_FIELD_NAME = "@type";
+    
     default <T> T fromInput(Object graphQlInput, AnnotatedType type) {
         return fromInput(graphQlInput, graphQlInput.getClass(), type);
     }
