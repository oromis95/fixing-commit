@@ -6,8 +6,9 @@ import java.lang.annotation.RetentionPolicy;
 import java.lang.annotation.Target;
 
 /**
- * Created by bojan.tomic on 7/17/16.
+ * Marks the parameters representing the root context
  */
+@GraphQLIgnore
 @Retention(RetentionPolicy.RUNTIME)
 @Target(ElementType.PARAMETER)
 public @interface GraphQLRootContext {
