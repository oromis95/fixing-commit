@@ -1,6 +1,9 @@
 package io.leangen.graphql.util;
 
 public final class Urls {
+
+    public static final String ISSUES = "https://github.com/leangen/graphql-spqr/issues";
+
     public static final class Errors {
         public static final String DYNAMIC_PROXIES = "https://github.com/leangen/graphql-spqr/wiki/Errors#dynamic-proxies";
         public static final String TOP_LEVEL_GENERICS = "https://github.com/leangen/graphql-spqr/wiki/Errors#generic-top-level-singletons";
