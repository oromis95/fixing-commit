@@ -5,6 +5,7 @@ import io.leangen.graphql.annotations.GraphQLComplexity;
 import io.leangen.graphql.metadata.Resolver;
 import io.leangen.graphql.metadata.execution.MethodInvoker;
 import io.leangen.graphql.metadata.execution.SingletonMethodInvoker;
+import io.leangen.graphql.metadata.strategy.InclusionStrategy;
 import io.leangen.graphql.metadata.strategy.type.DefaultTypeTransformer;
 import io.leangen.graphql.metadata.strategy.type.TypeTransformer;
 import io.leangen.graphql.util.ClassUtils;
@@ -68,26 +69,27 @@ public class PublicResolverBuilder extends FilteredResolverBuilder {
     }
 
     @Override
-    public Collection<Resolver> buildQueryResolvers(Object querySourceBean, AnnotatedType beanType) {
-        return buildQueryResolvers(querySourceBean, beanType, getFilters());
+    public Collection<Resolver> buildQueryResolvers(Object querySourceBean, AnnotatedType beanType, InclusionStrategy inclusionStrategy) {
+        return buildQueryResolvers(querySourceBean, beanType, inclusionStrategy, getFilters());
     }
 
     @Override
-    public Collection<Resolver> buildMutationResolvers(Object querySourceBean, AnnotatedType beanType) {
-        return buildMutationResolvers(querySourceBean, beanType, getFilters());
+    public Collection<Resolver> buildMutationResolvers(Object querySourceBean, AnnotatedType beanType, InclusionStrategy inclusionStrategy) {
+        return buildMutationResolvers(querySourceBean, beanType, inclusionStrategy, getFilters());
     }
 
     @Override
-    public Collection<Resolver> buildSubscriptionResolvers(Object querySourceBean, AnnotatedType beanType) {
-        return buildSubscriptionResolvers(querySourceBean, beanType, getFilters());
+    public Collection<Resolver> buildSubscriptionResolvers(Object querySourceBean, AnnotatedType beanType, InclusionStrategy inclusionStrategy) {
+        return buildSubscriptionResolvers(querySourceBean, beanType, inclusionStrategy, getFilters());
     }
 
-    private Collection<Resolver> buildQueryResolvers(Object querySourceBean, AnnotatedType beanType, List<Predicate<Member>> filters) {
+    private Collection<Resolver> buildQueryResolvers(Object querySourceBean, AnnotatedType beanType, InclusionStrategy inclusionStrategy, List<Predicate<Member>> filters) {
         Class<?> rawType = ClassUtils.getRawType(beanType.getType());
         if (rawType.isArray() || rawType.isPrimitive()) return Collections.emptyList();
         return Arrays.stream(rawType.getMethods())
                 .filter(method -> isPackageAcceptable(method, rawType))
                 .filter(this::isQuery)
+                .filter(method -> inclusionStrategy.includeOperation(method, getReturnType(method, beanType)))
                 .filter(filters.stream().reduce(Predicate::and).orElse(ACCEPT_ALL))
                 .map(method -> new Resolver(
                         operationNameGenerator.generateQueryName(method, beanType, querySourceBean),
@@ -96,18 +98,19 @@ public class PublicResolverBuilder extends FilteredResolverBuilder {
                         method.isAnnotationPresent(Batched.class),
                         querySourceBean == null ? new MethodInvoker(method, beanType) : new SingletonMethodInvoker(querySourceBean, method, beanType),
                         getReturnType(method, beanType),
-                        argumentBuilder.buildResolverArguments(method, beanType),
+                        argumentBuilder.buildResolverArguments(method, beanType, inclusionStrategy),
                         method.isAnnotationPresent(GraphQLComplexity.class) ? method.getAnnotation(GraphQLComplexity.class).value() : null
                 ))
                 .collect(Collectors.toList());
     }
 
-    private Collection<Resolver> buildMutationResolvers(Object querySourceBean, AnnotatedType beanType, List<Predicate<Member>> filters) {
+    private Collection<Resolver> buildMutationResolvers(Object querySourceBean, AnnotatedType beanType, InclusionStrategy inclusionStrategy, List<Predicate<Member>> filters) {
         Class<?> rawType = ClassUtils.getRawType(beanType.getType());
         if (rawType.isArray()|| rawType.isPrimitive()) return Collections.emptyList();
         return Arrays.stream(rawType.getMethods())
                 .filter(method -> isPackageAcceptable(method, rawType))
                 .filter(this::isMutation)
+                .filter(method -> inclusionStrategy.includeOperation(method, getReturnType(method, beanType)))
                 .filter(filters.stream().reduce(Predicate::and).orElse(ACCEPT_ALL))
                 .map(method -> new Resolver(
                         operationNameGenerator.generateMutationName(method, beanType, querySourceBean),
@@ -116,18 +119,19 @@ public class PublicResolverBuilder extends FilteredResolverBuilder {
                         false,
                         querySourceBean == null ? new MethodInvoker(method, beanType) : new SingletonMethodInvoker(querySourceBean, method, beanType),
                         getReturnType(method, beanType),
-                        argumentBuilder.buildResolverArguments(method, beanType),
+                        argumentBuilder.buildResolverArguments(method, beanType, inclusionStrategy),
                         method.isAnnotationPresent(GraphQLComplexity.class) ? method.getAnnotation(GraphQLComplexity.class).value() : null
                 ))
                 .collect(Collectors.toList());
     }
 
-    private Collection<Resolver> buildSubscriptionResolvers(Object querySourceBean, AnnotatedType beanType, List<Predicate<Member>> filters) {
+    private Collection<Resolver> buildSubscriptionResolvers(Object querySourceBean, AnnotatedType beanType, InclusionStrategy inclusionStrategy, List<Predicate<Member>> filters) {
         Class<?> rawType = ClassUtils.getRawType(beanType.getType());
         if (rawType.isArray()|| rawType.isPrimitive()) return Collections.emptyList();
         return Arrays.stream(rawType.getMethods())
                 .filter(method -> isPackageAcceptable(method, rawType))
                 .filter(this::isSubscription)
+                .filter(method -> inclusionStrategy.includeOperation(method, getReturnType(method, beanType)))
                 .filter(filters.stream().reduce(Predicate::and).orElse(ACCEPT_ALL))
                 .map(method -> new Resolver(
                         operationNameGenerator.generateSubscriptionName(method, beanType, querySourceBean),
@@ -136,7 +140,7 @@ public class PublicResolverBuilder extends FilteredResolverBuilder {
                         false,
                         querySourceBean == null ? new MethodInvoker(method, beanType) : new SingletonMethodInvoker(querySourceBean, method, beanType),
                         getReturnType(method, beanType),
-                        argumentBuilder.buildResolverArguments(method, beanType),
+                        argumentBuilder.buildResolverArguments(method, beanType, inclusionStrategy),
                         method.isAnnotationPresent(GraphQLComplexity.class) ? method.getAnnotation(GraphQLComplexity.class).value() : null
                 ))
                 .collect(Collectors.toList());
@@ -156,8 +160,8 @@ public class PublicResolverBuilder extends FilteredResolverBuilder {
 
     protected boolean isPackageAcceptable(Method method, Class<?> beanType) {
         String[] basePackages = new String[0];
-        if (Utils.arrayNotEmpty(this.basePackages)) {
-            basePackages = this.basePackages;
+        if (Utils.isArrayNotEmpty(this.basePackages)) {
+            basePackages = Arrays.stream(this.basePackages).filter(Utils::isNotEmpty).toArray(String[]::new);
         } else if (beanType.getPackage() != null) {
             basePackages = new String[] {beanType.getPackage().getName()};
         }
