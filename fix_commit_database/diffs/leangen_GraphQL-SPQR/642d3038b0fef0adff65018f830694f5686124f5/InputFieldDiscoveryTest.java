@@ -1,17 +1,21 @@
 package io.leangen.graphql;
 
-import org.junit.Test;
-
-import java.util.Set;
-
+import com.fasterxml.jackson.annotation.JsonCreator;
 import io.leangen.geantyref.GenericTypeReflector;
+import io.leangen.graphql.annotations.GraphQLIgnore;
 import io.leangen.graphql.annotations.GraphQLInputField;
 import io.leangen.graphql.annotations.GraphQLQuery;
 import io.leangen.graphql.metadata.InputField;
+import io.leangen.graphql.metadata.strategy.DefaultInclusionStrategy;
+import io.leangen.graphql.metadata.strategy.InclusionStrategy;
+import io.leangen.graphql.metadata.strategy.value.InputFieldDiscoveryStrategy;
 import io.leangen.graphql.metadata.strategy.value.gson.GsonValueMapper;
 import io.leangen.graphql.metadata.strategy.value.gson.GsonValueMapperFactory;
 import io.leangen.graphql.metadata.strategy.value.jackson.JacksonValueMapper;
 import io.leangen.graphql.metadata.strategy.value.jackson.JacksonValueMapperFactory;
+import org.junit.Test;
+
+import java.util.Set;
 
 import static org.junit.Assert.assertEquals;
 import static org.junit.Assert.assertTrue;
@@ -21,93 +25,110 @@ public class InputFieldDiscoveryTest {
     private JacksonValueMapper jackson = new JacksonValueMapperFactory().getValueMapper();
     private GsonValueMapper gson = new GsonValueMapperFactory().getValueMapper();
 
+    private InclusionStrategy inclusionStrategy = new DefaultInclusionStrategy("io.leangen");
+
     private static final String[] expectedDefaultFieldNames = {"field1", "field2", "field3"};
     private static final String[] expectedExplicitFieldNames = {"aaa", "bbb", "ccc"};
     
     @Test
     public void basicFieldsTest() {
-        assertFieldsNameEqual(FieldsOnly.class, expectedDefaultFieldNames);
+        assertFieldNamesEqual(FieldsOnly.class, expectedDefaultFieldNames);
     }
 
     @Test
     public void basicGettersTest() {
-        assertFieldsNameEqual(GettersOnly.class, expectedDefaultFieldNames);
+        assertFieldNamesEqual(GettersOnly.class, expectedDefaultFieldNames);
     }
 
     @Test
     public void basicSettersTest() {
-        assertFieldsNameEqual(SettersOnly.class, expectedDefaultFieldNames);
+        assertFieldNamesEqual(SettersOnly.class, expectedDefaultFieldNames);
     }
 
     @Test
     public void explicitFieldsTest() {
-        assertFieldsNameEqual(ExplicitFields.class, expectedExplicitFieldNames);
+        assertFieldNamesEqual(ExplicitFields.class, expectedExplicitFieldNames);
     }
 
     @Test
     public void explicitGettersTest() {
-        assertFieldsNameEqual(ExplicitGetters.class, expectedExplicitFieldNames);
+        assertFieldNamesEqual(ExplicitGetters.class, expectedExplicitFieldNames);
     }
 
     @Test
     public void explicitSettersTest() {
-        assertFieldsNameEqual(ExplicitSetters.class, expectedExplicitFieldNames);
+        assertFieldNamesEqual(ExplicitSetters.class, expectedExplicitFieldNames);
     }
     
     @Test
     public void queryFieldsTest() {
-        assertFieldsNameEqual(QueryFields.class, expectedExplicitFieldNames);
+        assertFieldNamesEqual(QueryFields.class, expectedExplicitFieldNames);
     }
 
     @Test
     public void queryGettersTest() {
-        assertFieldsNameEqual(QueryGetters.class, expectedExplicitFieldNames);
+        assertFieldNamesEqual(QueryGetters.class, expectedExplicitFieldNames);
     }
 
     @Test
     public void querySettersTest() {
-        assertFieldsNameEqual(QuerySetters.class, expectedExplicitFieldNames);
+        assertFieldNamesEqual(QuerySetters.class, expectedExplicitFieldNames);
     }
 
     @Test
     public void mixedFieldsTest() {
-        assertFieldsNameEqual(MixedFieldsWin.class, expectedExplicitFieldNames);
+        assertFieldNamesEqual(MixedFieldsWin.class, expectedExplicitFieldNames);
     }
     
     @Test
     public void mixedGettersTest() {
-        assertFieldsNameEqual(MixedGettersWin.class, expectedExplicitFieldNames);
+        assertFieldNamesEqual(MixedGettersWin.class, expectedExplicitFieldNames);
     }
 
     @Test
     public void mixedSettersTest() {
-        assertFieldsNameEqual(MixedSettersWin.class, expectedExplicitFieldNames);
+        assertFieldNamesEqual(MixedSettersWin.class, expectedExplicitFieldNames);
     }
 
     @Test
     public void conflictingGettersTest() {
-        assertFieldsNameEqual(ConflictingGettersWin.class, expectedExplicitFieldNames);
+        assertFieldNamesEqual(ConflictingGettersWin.class, expectedExplicitFieldNames);
     }
 
     @Test
     public void conflictingSettersTest() {
-        assertFieldsNameEqual(ConflictingSettersWin.class, expectedExplicitFieldNames);
+        assertFieldNamesEqual(ConflictingSettersWin.class, expectedExplicitFieldNames);
     }
 
     @Test
     public void allConflictingSettersTest() {
-        assertFieldsNameEqual(AllConflictingSettersWin.class, expectedExplicitFieldNames);
+        assertFieldNamesEqual(AllConflictingSettersWin.class, expectedExplicitFieldNames);
+    }
+
+    @Test
+    public void hiddenSettersTest() {
+        assertFieldNamesEqual(HiddenSetters.class, "field1", "field3");
+    }
+
+    @Test
+    public void hiddenCtorParamsTest() {
+        assertFieldNamesEqual(jackson, HiddenCtorParams.class, "field1", "field3");
     }
 
-    private void assertFieldsNameEqual(Class typeToScan, String... fieldNames) {
-        Set<InputField> jFields = jackson.getInputFields(GenericTypeReflector.annotate(typeToScan));
-        Set<InputField> gFields = gson.getInputFields(GenericTypeReflector.annotate(typeToScan));
+    private void assertFieldNamesEqual(Class typeToScan, String... fieldNames) {
+        Set<InputField> jFields = assertFieldNamesEqual(jackson, typeToScan, fieldNames);
+        Set<InputField> gFields = assertFieldNamesEqual(gson, typeToScan, fieldNames);
 
-        assertEquals(jFields.size(), fieldNames.length);
         assertEquals(jFields, gFields);
+    }
+
+    private Set<InputField> assertFieldNamesEqual(InputFieldDiscoveryStrategy mapper, Class typeToScan, String... fieldNames) {
+        Set<InputField> fields = mapper.getInputFields(GenericTypeReflector.annotate(typeToScan), inclusionStrategy);
+        assertEquals(fieldNames.length, fields.size());
         for (String fieldName : fieldNames) {
-            assertTrue(jFields.stream().anyMatch(input -> input.getName().equals(fieldName)));
+            assertTrue(fields.stream().anyMatch(input -> input.getName().equals(fieldName)));
         }
+        return fields;
     }
 
     private class FieldsOnly {
@@ -412,4 +433,51 @@ public class InputFieldDiscoveryTest {
             this.field3 = field3;
         }
     }
+
+    private class HiddenSetters {
+        private String field1;
+        private int field2;
+        private Object field3;
+
+        public String getField1() {
+            return field1;
+        }
+
+        public void setField1(String field1) {
+            this.field1 = field1;
+        }
+
+        @GraphQLQuery(name = "ignored")
+        @GraphQLInputField(name = "ignored")
+        public int getField2() {
+            return field2;
+        }
+
+        @GraphQLIgnore
+        @GraphQLInputField(name = "ignored")
+        public void setField2(int field2) {
+            this.field2 = field2;
+        }
+
+        public Object getField3() {
+            return field3;
+        }
+
+        public void setField3(Object field3) {
+            this.field3 = field3;
+        }
+    }
+
+    public static class HiddenCtorParams {
+        private String field1;
+        private int field2;
+        private Object field3;
+
+        @JsonCreator
+        public HiddenCtorParams(String field1, @GraphQLIgnore int field2, Object field3) {
+            this.field1 = field1;
+            this.field2 = field2;
+            this.field3 = field3;
+        }
+    }
 }
