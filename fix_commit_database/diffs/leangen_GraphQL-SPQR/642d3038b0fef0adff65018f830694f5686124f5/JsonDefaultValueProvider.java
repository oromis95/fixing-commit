@@ -1,13 +1,13 @@
 package io.leangen.graphql.generator.mapping.strategy;
 
-import java.lang.reflect.AnnotatedType;
-import java.lang.reflect.Parameter;
-
 import io.leangen.graphql.metadata.OperationArgumentDefaultValue;
 import io.leangen.graphql.metadata.strategy.type.DefaultTypeInfoGenerator;
 import io.leangen.graphql.metadata.strategy.value.ValueMapper;
 import io.leangen.graphql.util.Defaults;
 
+import java.lang.reflect.AnnotatedType;
+import java.lang.reflect.Parameter;
+
 /**
  * @author Bojan Tomic (kaqqao)
  */
@@ -16,7 +16,7 @@ public class JsonDefaultValueProvider implements DefaultValueProvider {
     private final ValueMapper valueMapper;
 
     public JsonDefaultValueProvider() {
-        this.valueMapper =  Defaults.valueMapperFactory(null, new DefaultTypeInfoGenerator())
+        this.valueMapper =  Defaults.valueMapperFactory(new DefaultTypeInfoGenerator())
                 .getValueMapper();
     }
 
