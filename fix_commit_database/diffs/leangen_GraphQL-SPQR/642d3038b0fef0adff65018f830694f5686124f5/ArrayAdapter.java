@@ -1,17 +1,17 @@
 package io.leangen.graphql.generator.mapping.common;
 
-import java.lang.reflect.AnnotatedArrayType;
-import java.lang.reflect.AnnotatedType;
-import java.lang.reflect.Array;
-import java.util.List;
-import java.util.stream.IntStream;
-
 import io.leangen.geantyref.GenericTypeReflector;
 import io.leangen.geantyref.TypeFactory;
 import io.leangen.graphql.execution.ResolutionEnvironment;
 import io.leangen.graphql.generator.mapping.OutputConverter;
 import io.leangen.graphql.util.ClassUtils;
 
+import java.lang.reflect.AnnotatedArrayType;
+import java.lang.reflect.AnnotatedType;
+import java.lang.reflect.Array;
+import java.util.List;
+import java.util.stream.IntStream;
+
 /**
  * @author Bojan Tomic (kaqqao)
  */
@@ -22,7 +22,7 @@ public class ArrayAdapter extends AbstractTypeSubstitutingMapper implements Outp
         AnnotatedType component = ((AnnotatedArrayType) original).getAnnotatedGenericComponentType();
         Class<?> raw = ClassUtils.getRawType(component.getType());
         if (raw.isPrimitive()) {
-            component = GenericTypeReflector.annotate(ClassUtils.box(raw), component.getAnnotations());
+            component = GenericTypeReflector.annotate(GenericTypeReflector.box(raw), component.getAnnotations());
         }
         return TypeFactory.parameterizedAnnotatedClass(List.class, original.getAnnotations(), component);
     }
