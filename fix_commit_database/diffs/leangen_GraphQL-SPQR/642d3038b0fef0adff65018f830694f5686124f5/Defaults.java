@@ -4,12 +4,15 @@ import io.leangen.graphql.metadata.strategy.type.TypeInfoGenerator;
 import io.leangen.graphql.metadata.strategy.value.ValueMapperFactory;
 import io.leangen.graphql.metadata.strategy.value.gson.GsonValueMapperFactory;
 import io.leangen.graphql.metadata.strategy.value.jackson.JacksonValueMapperFactory;
-import io.leangen.graphql.metadata.strategy.value.simple.ScalarOnlyValueMapperFactory;
 
 public class Defaults {
 
+    private static final IllegalStateException noJsonLib = new IllegalStateException(
+            "No JSON deserialization library found on classpath. A compatible version of either Jackson or Gson "
+                    + "must be available or a custom ValueMapperFactory must be provided");
+
     private enum JsonLib {
-        JACKSON("com.fasterxml.jackson.databind.ObjectMapper"), GSON("com.google.gson.Gson"), SCALAR("java.lang.Object");
+        JACKSON("com.fasterxml.jackson.databind.ObjectMapper"), GSON("com.google.gson.Gson");
 
         public final String requiredClass;
 
@@ -25,20 +28,18 @@ public class Defaults {
                 return jsonLib;
             } catch (ClassNotFoundException ge) {/*no-op*/}
         }
-        throw new IllegalStateException("No JSON deserialization library found on classpath");
+        throw noJsonLib;
     }
 
-    public static ValueMapperFactory valueMapperFactory(String[] basePackages, TypeInfoGenerator typeInfoGenerator) {
+    public static ValueMapperFactory valueMapperFactory(TypeInfoGenerator typeInfoGenerator) {
         switch (jsonLibrary()) {
             case GSON: return GsonValueMapperFactory.builder()
-                    .withBasePackages(basePackages)
                     .withTypeInfoGenerator(typeInfoGenerator)
                     .build();
             case JACKSON: return JacksonValueMapperFactory.builder()
-                    .withBasePackages(basePackages)
                     .withTypeInfoGenerator(typeInfoGenerator)
                     .build();
-            default: return new ScalarOnlyValueMapperFactory();
+            default: throw noJsonLib;
         }
     }
 }
