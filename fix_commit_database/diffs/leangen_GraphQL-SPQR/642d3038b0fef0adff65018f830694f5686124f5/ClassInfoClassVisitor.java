@@ -46,6 +46,7 @@
 
 package io.leangen.graphql.util.classpath;
 
+import org.objectweb.asm.AnnotationVisitor;
 import org.objectweb.asm.ClassVisitor;
 import org.objectweb.asm.FieldVisitor;
 import org.objectweb.asm.MethodVisitor;
@@ -173,6 +174,11 @@ class ClassInfoClassVisitor extends ClassVisitor {
                 signature, exceptions);
     }
 
+    @Override
+    public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
+        return currentClass.visitAnnotation(desc, visible);
+    }
+
     /**
      * Get the location (the jar file, zip file or directory) containing
      * the classes processed by this visitor.
