@@ -3,6 +3,10 @@ package io.leangen.graphql.metadata.strategy.value.gson;
 import com.google.gson.FieldNamingPolicy;
 import com.google.gson.FieldNamingStrategy;
 import com.google.gson.annotations.SerializedName;
+import io.leangen.graphql.annotations.GraphQLInputField;
+import io.leangen.graphql.annotations.GraphQLQuery;
+import io.leangen.graphql.util.ClassUtils;
+import io.leangen.graphql.util.Utils;
 
 import java.lang.reflect.AnnotatedElement;
 import java.lang.reflect.Field;
@@ -11,11 +15,6 @@ import java.util.List;
 import java.util.Optional;
 import java.util.stream.Stream;
 
-import io.leangen.graphql.annotations.GraphQLInputField;
-import io.leangen.graphql.annotations.GraphQLQuery;
-import io.leangen.graphql.util.ClassUtils;
-import io.leangen.graphql.util.Utils;
-
 public class GsonFieldNamingStrategy implements FieldNamingStrategy {
 
     private FieldNamingStrategy fallback;
@@ -45,18 +44,14 @@ public class GsonFieldNamingStrategy implements FieldNamingStrategy {
                 .map(element -> Optional.ofNullable(element.getAnnotation(SerializedName.class))
                         .map(SerializedName::value));
         return Utils.concat(explicit, queryImplicit, gsonExplicit)
-                .map(opt -> opt.filter(Utils::notEmpty))
+                .map(opt -> opt.filter(Utils::isNotEmpty))
                 .reduce(Utils::or).orElse(Optional.empty());
     }
 
     private List<AnnotatedElement> getNamedCandidates(Field field) {
         List<AnnotatedElement> propertyElements = new ArrayList<>(3);
-        try {
-            propertyElements.add(ClassUtils.findSetter(field.getDeclaringClass(), field.getName(), field.getType()));
-        } catch (NoSuchMethodException e) {/*no-op*/}
-        try {
-            propertyElements.add(ClassUtils.findGetter(field.getDeclaringClass(), field.getName()));
-        } catch (NoSuchMethodException e) {/*no-op*/}
+        ClassUtils.findSetter(field.getDeclaringClass(), field.getName(), field.getType()).ifPresent(propertyElements::add);
+        ClassUtils.findGetter(field.getDeclaringClass(), field.getName()).ifPresent(propertyElements::add);
         propertyElements.add(field);
         return propertyElements;
     }
