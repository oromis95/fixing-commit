@@ -1,19 +1,19 @@
 package io.leangen.graphql.metadata.strategy.value;
 
-import java.lang.reflect.Type;
-import java.util.Collections;
-import java.util.Set;
-
 import io.leangen.graphql.execution.GlobalEnvironment;
 
+import java.util.Collections;
+import java.util.List;
+import java.util.Map;
+
 /**
  * @author Bojan Tomic (kaqqao)
  */
 public interface ValueMapperFactory<T extends ValueMapper> {
     
     default T getValueMapper() {
-        return getValueMapper(Collections.emptySet(), null);
+        return getValueMapper(Collections.emptyMap(), null);
     }
     
-    T getValueMapper(Set<Type> abstractTypes, GlobalEnvironment environment);
+    T getValueMapper(Map<Class, List<Class>> concreteSubTypes, GlobalEnvironment environment);
 }
