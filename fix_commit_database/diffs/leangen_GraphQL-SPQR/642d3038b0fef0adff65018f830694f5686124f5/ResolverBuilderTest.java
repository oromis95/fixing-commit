@@ -1,45 +1,59 @@
 package io.leangen.graphql;
 
-import org.junit.Test;
-
-import java.io.Serializable;
-import java.lang.reflect.AnnotatedType;
-import java.lang.reflect.Field;
-import java.lang.reflect.Method;
-import java.math.BigInteger;
-import java.util.Collection;
-
 import graphql.schema.GraphQLSchema;
 import io.leangen.geantyref.GenericTypeReflector;
 import io.leangen.geantyref.TypeToken;
 import io.leangen.graphql.annotations.GraphQLArgument;
 import io.leangen.graphql.annotations.GraphQLIgnore;
+import io.leangen.graphql.annotations.GraphQLQuery;
 import io.leangen.graphql.domain.Person;
 import io.leangen.graphql.metadata.Resolver;
+import io.leangen.graphql.metadata.strategy.DefaultInclusionStrategy;
+import io.leangen.graphql.metadata.strategy.InclusionStrategy;
+import io.leangen.graphql.metadata.strategy.query.AnnotatedResolverBuilder;
 import io.leangen.graphql.metadata.strategy.query.BeanResolverBuilder;
 import io.leangen.graphql.metadata.strategy.query.OperationNameGenerator;
 import io.leangen.graphql.metadata.strategy.query.PublicResolverBuilder;
+import io.leangen.graphql.metadata.strategy.query.ResolverBuilder;
+import org.junit.Test;
+
+import java.io.Serializable;
+import java.lang.reflect.AnnotatedType;
+import java.lang.reflect.Field;
+import java.lang.reflect.Method;
+import java.math.BigInteger;
+import java.util.ArrayList;
+import java.util.Collection;
 
 import static org.junit.Assert.assertEquals;
 
 public class ResolverBuilderTest {
 
     private static final String BASE_PACKAGE = "io.leangen";
+    private static final InclusionStrategy INCLUSION_STRATEGY = new DefaultInclusionStrategy(BASE_PACKAGE);
 
     @Test
     public void bridgeMethodTest() {
-        Collection<Resolver> resolvers = new PublicResolverBuilder(BASE_PACKAGE)
-                .buildQueryResolvers(new BaseServiceImpl<Number, String>(), new TypeToken<BaseServiceImpl<Number, String>>(){}.getAnnotatedType());
+        Collection<Resolver> resolvers = new PublicResolverBuilder(BASE_PACKAGE) .buildQueryResolvers(
+                new BaseServiceImpl<Number, String>(), new TypeToken<BaseServiceImpl<Number, String>>(){}.getAnnotatedType(), INCLUSION_STRATEGY);
         assertEquals(1, resolvers.size());
         assertEquals(resolvers.iterator().next().getReturnType().getType(), Number.class);
     }
 
     @Test
     public void explicitIgnoreTest() {
-        Collection<Resolver> resolvers = new BeanResolverBuilder(BASE_PACKAGE)
-                .buildQueryResolvers(new Ignorables(), GenericTypeReflector.annotate(Ignorables.class));
-        assertEquals(1, resolvers.size());
-        assertEquals("notIgnored", resolvers.iterator().next().getOperationName());
+        for(Collection<Resolver> resolvers : resolvers(new IgnoredMethods(), new BeanResolverBuilder(BASE_PACKAGE), new AnnotatedResolverBuilder())) {
+            assertEquals(1, resolvers.size());
+            assertEquals("notIgnored", resolvers.iterator().next().getOperationName());
+        }
+    }
+
+    @Test
+    public void fieldIgnoreTest() {
+        for(Collection<Resolver> resolvers : resolvers(new IgnoredFields(), new BeanResolverBuilder(BASE_PACKAGE), new AnnotatedResolverBuilder())) {
+            assertEquals(1, resolvers.size());
+            assertEquals("notIgnored", resolvers.iterator().next().getOperationName());
+        }
     }
 
     @Test
@@ -75,7 +89,16 @@ public class ResolverBuilderTest {
         assertEquals("Two_findOne", schema.getQueryType().getFieldDefinitions().get(1).getName());
         assertEquals("BigDecimal", schema.getQueryType().getFieldDefinitions().get(1).getType().getName());
     }
-    
+
+    private Collection<Collection<Resolver>> resolvers(Object bean, ResolverBuilder... builders) {
+        Collection<Collection<Resolver>> resolvers = new ArrayList<>(builders.length);
+        for (ResolverBuilder builder : builders) {
+            resolvers.add(builder
+                    .buildQueryResolvers(bean, GenericTypeReflector.annotate(bean.getClass()), INCLUSION_STRATEGY));
+        }
+        return resolvers;
+    }
+
     private interface BaseService<T, ID> {
 
         T findOne(@GraphQLArgument(name = "id") ID id);
@@ -111,15 +134,29 @@ public class ResolverBuilderTest {
         }
     }
 
-    private static class Ignorables {
+    private static class IgnoredMethods {
 
+        @GraphQLQuery(name = "notIgnored")
         public String getNotIgnored() {
             return null;
         }
 
         @GraphQLIgnore
+        @GraphQLQuery(name = "ignored")
         public String getIgnored() {
             return null;
         }
     }
+
+    private static class IgnoredFields {
+
+        @GraphQLIgnore
+        @GraphQLQuery(name = "ignored")
+        public String ignored;
+
+        @GraphQLQuery(name = "notIgnored")
+        public String getNotIgnored() {
+            return null;
+        }
+    }
 }
