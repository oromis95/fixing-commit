@@ -1,5 +1,11 @@
 package io.leangen.graphql.generator;
 
+import io.leangen.graphql.metadata.Operation;
+import io.leangen.graphql.metadata.Resolver;
+import io.leangen.graphql.metadata.strategy.InclusionStrategy;
+import io.leangen.graphql.metadata.strategy.query.OperationBuilder;
+import io.leangen.graphql.metadata.strategy.query.ResolverBuilder;
+
 import java.lang.reflect.AnnotatedType;
 import java.lang.reflect.Type;
 import java.util.AbstractMap;
@@ -14,11 +20,6 @@ import java.util.function.BiFunction;
 import java.util.function.Function;
 import java.util.stream.Collectors;
 
-import io.leangen.graphql.metadata.Operation;
-import io.leangen.graphql.metadata.Resolver;
-import io.leangen.graphql.metadata.strategy.query.OperationBuilder;
-import io.leangen.graphql.metadata.strategy.query.ResolverBuilder;
-
 public class OperationRepository {
 
     private final Set<Operation> queries;
@@ -26,10 +27,12 @@ public class OperationRepository {
     private final Set<Operation> subscriptions;
     private final OperationSourceRepository operationSourceRepository;
     private final OperationBuilder operationBuilder;
+    private final InclusionStrategy inclusionStrategy;
 
-    public OperationRepository(OperationSourceRepository operationSourceRepository, OperationBuilder operationBuilder) {
+    public OperationRepository(OperationSourceRepository operationSourceRepository, OperationBuilder operationBuilder, InclusionStrategy inclusionStrategy) {
         this.operationSourceRepository = operationSourceRepository;
         this.operationBuilder = operationBuilder;
+        this.inclusionStrategy = inclusionStrategy;
         List<Resolver> resolvers = buildQueryResolvers(operationSourceRepository.getOperationSources());
         List<Resolver> mutationResolvers = buildMutationResolvers(operationSourceRepository.getOperationSources());
         List<Resolver> subscriptionResolvers = buildSubscriptionResolvers(operationSourceRepository.getOperationSources());
@@ -103,7 +106,7 @@ public class OperationRepository {
         return subscriptions;
     }
 
-    public Set<Operation> getNestedQueries(AnnotatedType domainType) {
+    private Set<Operation> getNestedQueries(AnnotatedType domainType) {
         OperationSource domainSource = operationSourceRepository.nestedSourceForType(domainType);
         return buildNestedQueries(domainSource);
     }
@@ -121,7 +124,7 @@ public class OperationRepository {
         return children.values();
     }
 
-    public Set<Operation> getEmbeddableQueries(Type domainType) {
+    private Set<Operation> getEmbeddableQueries(Type domainType) {
         return getAllQueries().stream()
                 .map(Operation::unbatch)
                 .filter(query -> query.isEmbeddableForType(domainType))
@@ -134,17 +137,17 @@ public class OperationRepository {
 
     private List<Resolver> buildQueryResolvers(Collection<OperationSource> operationSources) {
         return buildResolvers(operationSources, ((operationSource, builder) ->
-                builder.buildQueryResolvers(operationSource.getServiceSingleton(), operationSource.getJavaType())));
+                builder.buildQueryResolvers(operationSource.getServiceSingleton(), operationSource.getJavaType(), inclusionStrategy)));
     }
 
     private List<Resolver> buildMutationResolvers(Collection<OperationSource> operationSources) {
         return buildResolvers(operationSources, ((operationSource, builder) ->
-                builder.buildMutationResolvers(operationSource.getServiceSingleton(), operationSource.getJavaType())));
+                builder.buildMutationResolvers(operationSource.getServiceSingleton(), operationSource.getJavaType(), inclusionStrategy)));
     }
 
     private List<Resolver> buildSubscriptionResolvers(Collection<OperationSource> operationSources) {
         return buildResolvers(operationSources, ((operationSource, builder) ->
-                builder.buildSubscriptionResolvers(operationSource.getServiceSingleton(), operationSource.getJavaType())));
+                builder.buildSubscriptionResolvers(operationSource.getServiceSingleton(), operationSource.getJavaType(), inclusionStrategy)));
     }
 
     private List<Resolver> buildResolvers(Collection<OperationSource> operationSources, BiFunction<OperationSource, ResolverBuilder, Collection<Resolver>> building) {
