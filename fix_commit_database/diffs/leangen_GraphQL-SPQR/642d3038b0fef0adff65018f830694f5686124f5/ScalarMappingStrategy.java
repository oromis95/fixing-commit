@@ -1,8 +0,0 @@
-package io.leangen.graphql.generator.mapping.strategy;
-
-import java.lang.reflect.AnnotatedType;
-
-public interface ScalarMappingStrategy {
-
-    boolean supports(AnnotatedType type);
-}
