@@ -1,9 +1,6 @@
 package io.leangen.graphql;
 
-import org.junit.Test;
-import org.junit.runner.RunWith;
-import org.junit.runners.Parameterized;
-
+import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
 import graphql.ExecutionResult;
 import graphql.GraphQL;
 import graphql.schema.GraphQLSchema;
@@ -13,6 +10,9 @@ import io.leangen.graphql.annotations.GraphQLQuery;
 import io.leangen.graphql.metadata.strategy.value.ValueMapperFactory;
 import io.leangen.graphql.metadata.strategy.value.gson.GsonValueMapperFactory;
 import io.leangen.graphql.metadata.strategy.value.jackson.JacksonValueMapperFactory;
+import org.junit.Test;
+import org.junit.runner.RunWith;
+import org.junit.runners.Parameterized;
 
 import static io.leangen.graphql.support.QueryResultAssertions.assertValueAtPathEquals;
 import static org.junit.Assert.assertTrue;
@@ -29,9 +29,10 @@ public class PolymorphicJsonTest {
     }
     
     @Test
-    public void test() {
+    public void testPolymorphicInput() {
         GraphQLSchema schema = new TestSchemaGenerator()
                 .withValueMapperFactory(valueMapperFactory)
+                .withAbstractInputTypeResolution()
                 .withOperationsFromSingleton(new Operations())
                 .generate();
 
@@ -39,14 +40,23 @@ public class PolymorphicJsonTest {
         ExecutionResult result = exe.execute("{" +
                 "test (container: {" +
                 "       item: \"yay\"," +
-                "       _type_:\"Child\"}) {" +
-                "   _type_," +
+                "       _type_: Child}) {" +
                 "   item}}");
         assertTrue(result.getErrors().isEmpty());
-        assertValueAtPathEquals("Child", result, "test._type_");
-        assertValueAtPathEquals("yay", result, "test.item");
+        assertValueAtPathEquals("yayChild", result, "test.item");
     }
-    
+
+    @Test
+    public void testExplicitDeserializableType() {
+        GraphQLSchema schema = new TestSchemaGenerator()
+                .withOperationsFromSingleton(new Service())
+                .generate();
+        GraphQL exe = GraphQL.newGraphQL(schema).build();
+        ExecutionResult result = exe.execute("{ item (in: { item: {}})}");
+        assertTrue(result.getErrors().toString(), result.getErrors().isEmpty());
+        assertValueAtPathEquals("Concrete", result, "item");
+    }
+
     public static abstract class Parent<T> {
         String item;
         
@@ -64,7 +74,7 @@ public class PolymorphicJsonTest {
         @Override
         @GraphQLQuery(name = "item")
         public String getItem() {
-            return item;
+            return item + getClass().getSimpleName();
         }
 
         public void setItem(String item) {
@@ -79,4 +89,29 @@ public class PolymorphicJsonTest {
             return container;
         }
     }
+
+    public static class Service {
+
+        @GraphQLQuery
+        public String item(Wrapper in) {
+            return in.item.getItem();
+        }
+    }
+
+    public static class Wrapper {
+        @JsonDeserialize(as = Concrete.class)
+        public Abstract<String> item;
+    }
+
+    public interface Abstract<T> {
+        T getItem();
+    }
+
+    public static class Concrete<T> implements Abstract<T> {
+
+        @Override
+        public T getItem() {
+            return (T) getClass().getSimpleName();
+        }
+    }
 }
