@@ -6,18 +6,25 @@ import graphql.schema.GraphQLType;
 import graphql.schema.TypeResolver;
 import io.leangen.graphql.execution.GlobalEnvironment;
 import io.leangen.graphql.generator.mapping.TypeMapperRepository;
+import io.leangen.graphql.generator.mapping.strategy.AbstractInputHandler;
+import io.leangen.graphql.generator.mapping.strategy.ImplementationDiscoveryStrategy;
 import io.leangen.graphql.generator.mapping.strategy.InterfaceMappingStrategy;
+import io.leangen.graphql.metadata.strategy.InclusionStrategy;
 import io.leangen.graphql.metadata.strategy.type.TypeInfoGenerator;
 import io.leangen.graphql.metadata.strategy.value.InputFieldDiscoveryStrategy;
+import io.leangen.graphql.metadata.strategy.value.ScalarDeserializationStrategy;
 import io.leangen.graphql.metadata.strategy.value.ValueMapper;
 import io.leangen.graphql.metadata.strategy.value.ValueMapperFactory;
 import io.leangen.graphql.util.ClassUtils;
 import io.leangen.graphql.util.GraphQLUtils;
 
 import java.lang.reflect.AnnotatedType;
-import java.lang.reflect.Type;
+import java.util.List;
+import java.util.Map;
 import java.util.Set;
+import java.util.function.Function;
 import java.util.stream.Collectors;
+import java.util.stream.Stream;
 
 @SuppressWarnings("WeakerAccess")
 public class BuildContext {
@@ -34,6 +41,10 @@ public class BuildContext {
     public final String[] basePackages;
     public final ValueMapperFactory valueMapperFactory;
     public final InputFieldDiscoveryStrategy inputFieldStrategy;
+    public final InclusionStrategy inclusionStrategy;
+    public final ScalarDeserializationStrategy scalarStrategy;
+    public final AbstractInputHandler abstractInputHandler;
+    public final ImplementationDiscoveryStrategy implDiscoveryStrategy;
     public final TypeInfoGenerator typeInfoGenerator;
     public final RelayMappingConfig relayMappingConfig;
 
@@ -41,24 +52,25 @@ public class BuildContext {
 
     /**
      * The shared context accessible throughout the schema generation process
-     *
+     * @param basePackages The base (root) package of the entire project
+     * @param environment The globally shared environment
      * @param operationRepository Repository that can be used to fetch all known (singleton and domain) queries
      * @param typeMappers Repository of all registered {@link io.leangen.graphql.generator.mapping.TypeMapper}s
-     * @param environment The globally shared environment
-     * @param interfaceStrategy The strategy deciding what Java type gets mapped to a GraphQL interface
-     * @param basePackages The base (root) package of the entire project
+     * @param valueMapperFactory The factory used to produce {@link ValueMapper} instances
      * @param typeInfoGenerator Generates type name/description
-     * @param valueMapperFactory The factory used to produce {@link io.leangen.graphql.metadata.strategy.value.ValueMapper} instances
+     * @param interfaceStrategy The strategy deciding what Java type gets mapped to a GraphQL interface
+     * @param scalarStrategy The strategy deciding how abstract Java types are discovered
+     * @param abstractInputHandler The strategy deciding what Java type gets mapped to a GraphQL interface
      * @param inputFieldStrategy The strategy deciding how GraphQL input fields are discovered from Java types
-     * @param knownTypes The cache of known type names
      * @param relayMappingConfig Relay specific configuration
+     * @param knownTypes The cache of known type names
      */
-    public BuildContext(OperationRepository operationRepository, TypeMapperRepository typeMappers,
-                        GlobalEnvironment environment,
-                        InterfaceMappingStrategy interfaceStrategy, String[] basePackages,
-                        TypeInfoGenerator typeInfoGenerator, ValueMapperFactory valueMapperFactory,
-                        InputFieldDiscoveryStrategy inputFieldStrategy, Set<GraphQLType> knownTypes,
-                        RelayMappingConfig relayMappingConfig) {
+    public BuildContext(String[] basePackages, GlobalEnvironment environment, OperationRepository operationRepository,
+                        TypeMapperRepository typeMappers, ValueMapperFactory valueMapperFactory,
+                        TypeInfoGenerator typeInfoGenerator, InterfaceMappingStrategy interfaceStrategy,
+                        ScalarDeserializationStrategy scalarStrategy, AbstractInputHandler abstractInputHandler,
+                        InputFieldDiscoveryStrategy inputFieldStrategy, InclusionStrategy inclusionStrategy,
+                        RelayMappingConfig relayMappingConfig, Set<GraphQLType> knownTypes, ImplementationDiscoveryStrategy implementationStrategy) {
         this.operationRepository = operationRepository;
         this.typeRepository = environment.typeRepository;
         this.typeCache = new TypeCache(knownTypes);
@@ -74,20 +86,23 @@ public class BuildContext {
         this.basePackages = basePackages;
         this.valueMapperFactory = valueMapperFactory;
         this.inputFieldStrategy = inputFieldStrategy;
+        this.inclusionStrategy = inclusionStrategy;
+        this.scalarStrategy = scalarStrategy;
+        this.implDiscoveryStrategy = implementationStrategy;
+        this.abstractInputHandler = abstractInputHandler;
         this.globalEnvironment = environment;
         this.relayMappingConfig = relayMappingConfig;
         this.validator = new Validator(environment, typeMappers, knownTypes);
     }
 
-    ValueMapper createValueMapper(Set<Type> abstractTypes) {
-        return valueMapperFactory.getValueMapper(abstractTypes, globalEnvironment);
-    }
-
-    Set<Type> findAbstractTypes(AnnotatedType rootType) {
-        return typeCache.findAbstract(rootType, this).stream()
-                //ignore built-in types by default as Jackson & Gson *should* be able to deal with them on their own
-                .filter(type -> !ClassUtils.isSubPackage(ClassUtils.getRawType(type).getPackage(), "java."))
-                .collect(Collectors.toSet());
+    ValueMapper createValueMapper(Stream<AnnotatedType> inputTypes) {
+        List<Class> abstractTypes = inputTypes
+                .flatMap(input -> abstractInputHandler.findConstituentAbstractTypes(input, this).stream().map(ClassUtils::getRawType))
+                .distinct()
+                .collect(Collectors.toList());
+        Map<Class, List<Class>> concreteSubTypes = abstractTypes.stream()
+                .collect(Collectors.toMap(Function.identity(), abs -> abstractInputHandler.findConcreteSubTypes(abs, this)));
+        return valueMapperFactory.getValueMapper(concreteSubTypes, globalEnvironment);
     }
 
     void resolveTypeReferences() {
