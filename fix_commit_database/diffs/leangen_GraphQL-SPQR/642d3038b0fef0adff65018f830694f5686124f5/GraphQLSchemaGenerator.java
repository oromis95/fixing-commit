@@ -51,11 +51,16 @@ import io.leangen.graphql.generator.mapping.common.VoidToBooleanTypeAdapter;
 import io.leangen.graphql.generator.mapping.core.CompletableFutureMapper;
 import io.leangen.graphql.generator.mapping.core.DataFetcherResultMapper;
 import io.leangen.graphql.generator.mapping.core.PublisherMapper;
+import io.leangen.graphql.generator.mapping.strategy.AbstractInputHandler;
 import io.leangen.graphql.generator.mapping.strategy.AnnotatedInterfaceStrategy;
-import io.leangen.graphql.generator.mapping.strategy.DefaultScalarStrategy;
+import io.leangen.graphql.generator.mapping.strategy.AutoDiscoveryAbstractInputHandler;
+import io.leangen.graphql.generator.mapping.strategy.DefaultImplementationDiscoveryStrategy;
+import io.leangen.graphql.generator.mapping.strategy.ImplementationDiscoveryStrategy;
 import io.leangen.graphql.generator.mapping.strategy.InterfaceMappingStrategy;
-import io.leangen.graphql.generator.mapping.strategy.ScalarMappingStrategy;
+import io.leangen.graphql.generator.mapping.strategy.NoOpAbstractInputHandler;
 import io.leangen.graphql.metadata.exceptions.TypeMappingException;
+import io.leangen.graphql.metadata.strategy.DefaultInclusionStrategy;
+import io.leangen.graphql.metadata.strategy.InclusionStrategy;
 import io.leangen.graphql.metadata.strategy.query.AnnotatedResolverBuilder;
 import io.leangen.graphql.metadata.strategy.query.BeanResolverBuilder;
 import io.leangen.graphql.metadata.strategy.query.DefaultOperationBuilder;
@@ -66,6 +71,7 @@ import io.leangen.graphql.metadata.strategy.type.DefaultTypeTransformer;
 import io.leangen.graphql.metadata.strategy.type.TypeInfoGenerator;
 import io.leangen.graphql.metadata.strategy.type.TypeTransformer;
 import io.leangen.graphql.metadata.strategy.value.InputFieldDiscoveryStrategy;
+import io.leangen.graphql.metadata.strategy.value.ScalarDeserializationStrategy;
 import io.leangen.graphql.metadata.strategy.value.ValueMapper;
 import io.leangen.graphql.metadata.strategy.value.ValueMapperFactory;
 import io.leangen.graphql.util.ClassUtils;
@@ -82,6 +88,7 @@ import java.util.Collections;
 import java.util.HashSet;
 import java.util.LinkedHashSet;
 import java.util.List;
+import java.util.Map;
 import java.util.Set;
 import java.util.function.Predicate;
 import java.util.stream.Collectors;
@@ -132,10 +139,13 @@ import static java.util.Collections.addAll;
 public class GraphQLSchemaGenerator {
 
     private InterfaceMappingStrategy interfaceStrategy = new AnnotatedInterfaceStrategy(true);
-    private ScalarMappingStrategy scalarStrategy = new DefaultScalarStrategy();
+    private ScalarDeserializationStrategy scalarStrategy;
+    private AbstractInputHandler abstractInputHandler = new NoOpAbstractInputHandler();
     private OperationBuilder operationBuilder = new DefaultOperationBuilder(DefaultOperationBuilder.TypeInference.NONE);
     private ValueMapperFactory<?> valueMapperFactory;
     private InputFieldDiscoveryStrategy inputFieldStrategy;
+    private InclusionStrategy inclusionStrategy;
+    private ImplementationDiscoveryStrategy implDiscoveryStrategy = new DefaultImplementationDiscoveryStrategy();
     private TypeInfoGenerator typeInfoGenerator = new DefaultTypeInfoGenerator();
     private TypeTransformer typeTransformer = new DefaultTypeTransformer(false, false);
     private GlobalEnvironment environment;
@@ -420,13 +430,13 @@ public class GraphQLSchemaGenerator {
         return this;
     }
 
-    public GraphQLSchemaGenerator withInterfaceMappingStrategy(InterfaceMappingStrategy interfaceStrategy) {
-        this.interfaceStrategy = interfaceStrategy;
+    public GraphQLSchemaGenerator withAbstractInputTypeResolution() {
+        this.abstractInputHandler = new AutoDiscoveryAbstractInputHandler();
         return this;
     }
 
-    public GraphQLSchemaGenerator withScalarMappingStrategy(ScalarMappingStrategy scalarStrategy) {
-        this.scalarStrategy = scalarStrategy;
+    public GraphQLSchemaGenerator withAbstractInputHandler(AbstractInputHandler abstractInputHandler) {
+        this.abstractInputHandler = abstractInputHandler;
         return this;
     }
 
@@ -440,6 +450,46 @@ public class GraphQLSchemaGenerator {
         return this;
     }
 
+    public GraphQLSchemaGenerator withTypeInfoGenerator(TypeInfoGenerator typeInfoGenerator) {
+        this.typeInfoGenerator = typeInfoGenerator;
+        return this;
+    }
+
+    public GraphQLSchemaGenerator withValueMapperFactory(ValueMapperFactory<?> valueMapperFactory) {
+        this.valueMapperFactory = valueMapperFactory;
+        return this;
+    }
+
+    public GraphQLSchemaGenerator withInputFieldDiscoveryStrategy(InputFieldDiscoveryStrategy inputFieldStrategy) {
+        this.inputFieldStrategy = inputFieldStrategy;
+        return this;
+    }
+
+    public GraphQLSchemaGenerator withInterfaceMappingStrategy(InterfaceMappingStrategy interfaceStrategy) {
+        this.interfaceStrategy = interfaceStrategy;
+        return this;
+    }
+
+    public GraphQLSchemaGenerator withScalarDeserializationStrategy(ScalarDeserializationStrategy scalarStrategy) {
+        this.scalarStrategy = scalarStrategy;
+        return this;
+    }
+
+    public GraphQLSchemaGenerator withInclusionStrategy(InclusionStrategy inclusionStrategy) {
+        this.inclusionStrategy = inclusionStrategy;
+        return this;
+    }
+
+    public GraphQLSchemaGenerator withImplementationDiscoveryStrategy(ImplementationDiscoveryStrategy implDiscoveryStrategy) {
+        this.implDiscoveryStrategy = implDiscoveryStrategy;
+        return this;
+    }
+
+    public GraphQLSchemaGenerator withTypeTransformer(TypeTransformer transformer) {
+        this.typeTransformer = transformer;
+        return this;
+    }
+
     /**
      * Registers custom {@link TypeMapper}s to be used for mapping Java type to GraphQL types.
      * <p><b>Ordering of mappers is strictly important as the first {@link TypeMapper} that supports the given Java type
@@ -455,6 +505,16 @@ public class GraphQLSchemaGenerator {
         return this;
     }
 
+    /**
+     * Registers custom {@link TypeMapper}s to be used for mapping Java type to GraphQL types.
+     * <p><b>Ordering of mappers is strictly important as the first {@link TypeMapper} that supports the given Java type
+     * will be used for mapping it.</b></p>
+     * <p>See {@link TypeMapper#supports(AnnotatedType)}</p>
+     *
+     * @param provider Provides the customized list of TypeMappers to use
+     *
+     * @return This {@link GraphQLSchemaGenerator} instance, to allow method chaining
+     */
     public GraphQLSchemaGenerator withTypeMappers(ExtensionProvider<TypeMapper> provider) {
         this.typeMapperProviders.add(provider);
         return this;
@@ -468,7 +528,6 @@ public class GraphQLSchemaGenerator {
      * <p><b>Ordering of converters is strictly important as the first {@link InputConverter} that supports the given Java type
      * will be used for converting it.</b></p>
      * <p>See {@link InputConverter#supports(AnnotatedType)}</p>
-     * <p>See {@link #withDefaults()}</p>
      *
      * @param inputConverters Custom input converters to register with the builder
      *
@@ -492,7 +551,6 @@ public class GraphQLSchemaGenerator {
      * <p><b>Ordering of converters is strictly important as the first {@link OutputConverter} that supports the given Java type
      * will be used for converting it.</b></p>
      * <p>See {@link OutputConverter#supports(AnnotatedType)}</p>
-     * <p>See {@link #withDefaults()}</p>
      *
      * @param outputConverters Custom output converters to register with the builder
      *
@@ -521,16 +579,15 @@ public class GraphQLSchemaGenerator {
      * will be used to map/convert it.</b></p>
      * <p>See {@link #withDefaultMappers()}</p>
      * <p>See {@link #withDefaultConverters()}</p>
-     * <p>See {@link #withDefaults()}</p>
      *
      * @param typeAdapters Custom type adapters to register with the builder
      *
      * @return This {@link GraphQLSchemaGenerator} instance, to allow method chaining
      */
     public GraphQLSchemaGenerator withTypeAdapters(AbstractTypeAdapter<?,?>... typeAdapters) {
-        withInputConverters((InputConverter<?, ?>[]) typeAdapters);
-        withOutputConverters((OutputConverter<?, ?>[]) typeAdapters);
-        return withTypeMappers((TypeMapper[]) typeAdapters);
+        withInputConverters((conf, defaults) -> defaults.insert(0, typeAdapters));
+        withOutputConverters((conf, defaults) -> defaults.insert(0, typeAdapters));
+        return withTypeMappers((conf, defaults) -> defaults.insertAfter(ScalarMapper.class, typeAdapters));
     }
 
     public GraphQLSchemaGenerator withArgumentInjectors(ArgumentInjector... argumentInjectors) {
@@ -543,26 +600,6 @@ public class GraphQLSchemaGenerator {
         return this;
     }
 
-    public GraphQLSchemaGenerator withTypeInfoGenerator(TypeInfoGenerator typeInfoGenerator) {
-        this.typeInfoGenerator = typeInfoGenerator;
-        return this;
-    }
-
-    public GraphQLSchemaGenerator withValueMapperFactory(ValueMapperFactory<?> valueMapperFactory) {
-        this.valueMapperFactory = valueMapperFactory;
-        return this;
-    }
-
-    public GraphQLSchemaGenerator withInputFieldDiscoveryStrategy(InputFieldDiscoveryStrategy inputFieldStrategy) {
-        this.inputFieldStrategy = inputFieldStrategy;
-        return this;
-    }
-
-    public GraphQLSchemaGenerator withTypeTransformer(TypeTransformer transformer) {
-        this.typeTransformer = transformer;
-        return this;
-    }
-
     public GraphQLSchemaGenerator withAdditionalTypes(Collection<GraphQLType> additionalTypes) {
         additionalTypes.stream()
                 .filter(type -> !isInternalType(type))
@@ -629,7 +666,10 @@ public class GraphQLSchemaGenerator {
      * <p>See {@link #withDefaultMappers()} and {@link #withDefaultConverters()}</p>
      *
      * @return This {@link GraphQLSchemaGenerator} instance, to allow method chaining
+     *
+     * @deprecated No longer needed, as {@code withXYZ(ExtensionProvider)} methods provide a better alternative
      */
+    @Deprecated
     public GraphQLSchemaGenerator withDefaults() {
         return withDefaultResolverBuilders()
                 .withDefaultNestedResolverBuilders()
@@ -701,6 +741,20 @@ public class GraphQLSchemaGenerator {
             throw new IllegalStateException("At least one top-level operation source must be registered");
         }
 
+        if (inclusionStrategy == null) {
+            inclusionStrategy = new DefaultInclusionStrategy(basePackages);
+        }
+        ValueMapperFactory<?> internalValueMapperFactory = valueMapperFactory != null
+                ? valueMapperFactory
+                : Defaults.valueMapperFactory(typeInfoGenerator);
+        if (scalarStrategy == null) {
+            if (internalValueMapperFactory instanceof ScalarDeserializationStrategy) {
+                scalarStrategy = (ScalarDeserializationStrategy) internalValueMapperFactory;
+            } else {
+                scalarStrategy = (ScalarDeserializationStrategy) Defaults.valueMapperFactory(typeInfoGenerator);
+            }
+        }
+
         if (resolverBuilderProviders.isEmpty()) {
             resolverBuilderProviders.add(defaultConfig());
         }
@@ -728,7 +782,7 @@ public class GraphQLSchemaGenerator {
         if (typeMapperProviders.isEmpty()) {
             typeMapperProviders.add(defaultConfig());
         }
-        ObjectTypeMapper objectTypeMapper = new ObjectTypeMapper(true);
+        ObjectTypeMapper objectTypeMapper = new ObjectTypeMapper();
         EnumMapper enumMapper = new EnumMapper(respectJavaDeprecation);
         List<TypeMapper> defaultMappers = Arrays.asList(
                 new NonNullMapper(), new IdAdapter(), new ScalarMapper(), new CompletableFutureMapper(),
@@ -784,16 +838,13 @@ public class GraphQLSchemaGenerator {
         }
 
         environment = new GlobalEnvironment(new Relay(), new TypeRepository(additionalTypes), new ConverterRepository(inputConverters, outputConverters), new ArgumentInjectorRepository(argumentInjectors));
-        if (valueMapperFactory == null) {
-            valueMapperFactory = Defaults.valueMapperFactory(basePackages, typeInfoGenerator);
-        }
-        valueMapperFactory = new MemoizedValueMapperFactory<>(environment, valueMapperFactory);
+        valueMapperFactory = new MemoizedValueMapperFactory<>(environment, internalValueMapperFactory);
         if (inputFieldStrategy == null) {
             ValueMapper def = valueMapperFactory.getValueMapper();
             if (def instanceof InputFieldDiscoveryStrategy) {
                 inputFieldStrategy = (InputFieldDiscoveryStrategy) def;
             } else {
-                inputFieldStrategy = (InputFieldDiscoveryStrategy) Defaults.valueMapperFactory(basePackages, typeInfoGenerator).getValueMapper();
+                inputFieldStrategy = (InputFieldDiscoveryStrategy) Defaults.valueMapperFactory(typeInfoGenerator).getValueMapper();
             }
         }
     }
@@ -809,11 +860,9 @@ public class GraphQLSchemaGenerator {
         init();
 
         BuildContext buildContext = new BuildContext(
-                new OperationRepository(operationSourceRepository, operationBuilder),
-                new TypeMapperRepository(typeMappers),
-                environment,
-                interfaceStrategy, basePackages, typeInfoGenerator, valueMapperFactory,
-                inputFieldStrategy, additionalTypes, relayMappingConfig);
+                basePackages, environment, new OperationRepository(operationSourceRepository, operationBuilder, inclusionStrategy),
+                new TypeMapperRepository(typeMappers), valueMapperFactory, typeInfoGenerator, interfaceStrategy, scalarStrategy,
+                abstractInputHandler, inputFieldStrategy, inclusionStrategy, relayMappingConfig, additionalTypes, implDiscoveryStrategy);
         OperationMapper operationMapper = new OperationMapper(buildContext);
 
         GraphQLSchema.Builder builder = GraphQLSchema.newSchema()
@@ -895,13 +944,13 @@ public class GraphQLSchemaGenerator {
 
     public static class Configuration {
         public final InterfaceMappingStrategy interfaceMappingStrategy;
-        public final ScalarMappingStrategy scalarMappingStrategy;
+        public final ScalarDeserializationStrategy scalarDeserializationStrategy;
         public final TypeTransformer typeTransformer;
         public final String[] basePackages;
 
-        public Configuration(InterfaceMappingStrategy interfaceMappingStrategy, ScalarMappingStrategy scalarMappingStrategy, TypeTransformer typeTransformer, String[] basePackages) {
+        public Configuration(InterfaceMappingStrategy interfaceMappingStrategy, ScalarDeserializationStrategy scalarDeserializationStrategy, TypeTransformer typeTransformer, String[] basePackages) {
             this.interfaceMappingStrategy = interfaceMappingStrategy;
-            this.scalarMappingStrategy = scalarMappingStrategy;
+            this.scalarDeserializationStrategy = scalarDeserializationStrategy;
             this.typeTransformer = typeTransformer;
             this.basePackages = basePackages;
         }
@@ -980,22 +1029,23 @@ public class GraphQLSchemaGenerator {
         }
     }
 
+    //TODO Remove MemoizedValueMapperFactory
     private static class MemoizedValueMapperFactory<T extends ValueMapper> implements ValueMapperFactory<T> {
 
         private final T defaultValueMapper;
         private final ValueMapperFactory<T> delegate;
 
         public MemoizedValueMapperFactory(GlobalEnvironment environment, ValueMapperFactory<T> delegate) {
-            this.defaultValueMapper = delegate.getValueMapper(Collections.emptySet(), environment);
+            this.defaultValueMapper = delegate.getValueMapper(Collections.emptyMap(), environment);
             this.delegate = delegate;
         }
 
         @Override
-        public T getValueMapper(Set<Type> abstractTypes, GlobalEnvironment environment) {
-            if (abstractTypes.isEmpty()) {
+        public T getValueMapper(Map<Class, List<Class>> concreteSubTypes, GlobalEnvironment environment) {
+            if (concreteSubTypes.isEmpty() || concreteSubTypes.values().stream().allMatch(List::isEmpty)) {
                 return this.defaultValueMapper;
             }
-            return delegate.getValueMapper(abstractTypes, environment);
+            return delegate.getValueMapper(concreteSubTypes, environment);
         }
     }
 }
