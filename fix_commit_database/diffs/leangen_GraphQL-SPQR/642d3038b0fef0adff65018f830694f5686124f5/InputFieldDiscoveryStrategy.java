@@ -1,11 +1,12 @@
 package io.leangen.graphql.metadata.strategy.value;
 
+import io.leangen.graphql.metadata.InputField;
+import io.leangen.graphql.metadata.strategy.InclusionStrategy;
+
 import java.lang.reflect.AnnotatedType;
 import java.util.Set;
 
-import io.leangen.graphql.metadata.InputField;
-
 public interface InputFieldDiscoveryStrategy {
     
-    Set<InputField> getInputFields(AnnotatedType type);
+    Set<InputField> getInputFields(AnnotatedType type, InclusionStrategy inclusionStrategy);
 }
