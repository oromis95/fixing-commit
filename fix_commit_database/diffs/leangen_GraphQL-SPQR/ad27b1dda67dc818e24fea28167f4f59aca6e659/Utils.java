@@ -2,6 +2,8 @@ package io.leangen.graphql.util;
 
 import java.lang.reflect.Array;
 import java.util.Arrays;
+import java.util.Collections;
+import java.util.List;
 import java.util.Optional;
 import java.util.function.Supplier;
 import java.util.stream.Stream;
@@ -80,4 +82,8 @@ public class Utils {
         }
         return value;
     }
+
+    public static <T> List<T> singletonList(T element) {
+        return element == null ? Collections.emptyList() : Collections.singletonList(element);
+    }
 }
