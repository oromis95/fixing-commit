@@ -1,7 +1,9 @@
 package io.leangen.graphql.metadata;
 
+import io.leangen.graphql.util.ClassUtils;
 import io.leangen.graphql.util.Utils;
 
+import java.lang.reflect.AnnotatedElement;
 import java.lang.reflect.AnnotatedType;
 import java.util.Objects;
 
@@ -9,14 +11,14 @@ public class InputField {
 
     private final String name;
     private final String description;
-    private final AnnotatedType javaType;
+    private final TypedElement typedElement;
     private final AnnotatedType deserializableType;
     private final Object defaultValue;
 
-    public InputField(String name, String description, AnnotatedType javaType, AnnotatedType deserializableType, Object defaultValue) {
+    public InputField(String name, String description, AnnotatedType javaType, AnnotatedType deserializableType, Object defaultValue, AnnotatedElement element) {
         this.name = Utils.requireNonEmpty(name);
         this.description = description;
-        this.javaType = Objects.requireNonNull(javaType);
+        this.typedElement = new TypedElement(Objects.requireNonNull(javaType), element);
         this.deserializableType = deserializableType != null ? deserializableType : javaType;
         this.defaultValue = defaultValue;
     }
@@ -30,7 +32,7 @@ public class InputField {
     }
 
     public AnnotatedType getJavaType() {
-        return javaType;
+        return typedElement.getJavaType();
     }
 
     public AnnotatedType getDeserializableType() {
@@ -40,4 +42,13 @@ public class InputField {
     public Object getDefaultValue() {
         return defaultValue;
     }
+
+    public TypedElement getTypedElement() {
+        return typedElement;
+    }
+
+    @Override
+    public String toString() {
+        return String.format("Input field '%s' of type %s bound to [%s]", name, ClassUtils.toString(getJavaType()), ClassUtils.toString(typedElement.getElement()));
+    }
 }
