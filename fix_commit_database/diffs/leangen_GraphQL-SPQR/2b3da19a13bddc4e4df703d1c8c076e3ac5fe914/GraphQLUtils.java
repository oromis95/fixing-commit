@@ -1,7 +1,9 @@
 package io.leangen.graphql.util;
 
+import graphql.relay.Relay;
 import graphql.schema.GraphQLArgument;
 import graphql.schema.GraphQLFieldDefinition;
+import graphql.schema.GraphQLInterfaceType;
 import graphql.schema.GraphQLNonNull;
 import graphql.schema.GraphQLType;
 import io.leangen.graphql.annotations.GraphQLId;
@@ -24,6 +26,14 @@ public class GraphQLUtils {
         return type.equals(Scalars.RelayId);
     }
 
+    public static boolean isRelayNodeInterface(GraphQLType node) {
+        return node instanceof GraphQLInterfaceType
+                && node.getName().equals(Relay.NODE)
+                && ((GraphQLInterfaceType) node).getFieldDefinitions().size() == 1
+                && ((GraphQLInterfaceType) node).getFieldDefinition("id") != null
+                && isRelayId(((GraphQLInterfaceType) node).getFieldDefinition("id"));
+    }
+
     public static GraphQLType unwrapNonNull(GraphQLType type) {
         if (type instanceof GraphQLNonNull) {
             return unwrapNonNull(((GraphQLNonNull) type).getWrappedType());
