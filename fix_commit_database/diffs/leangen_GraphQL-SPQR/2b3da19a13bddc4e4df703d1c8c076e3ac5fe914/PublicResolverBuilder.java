@@ -10,10 +10,12 @@ import java.util.List;
 import java.util.function.Predicate;
 import java.util.stream.Collectors;
 
+import graphql.execution.batched.Batched;
 import io.leangen.graphql.metadata.Resolver;
 import io.leangen.graphql.metadata.execution.MethodInvoker;
 import io.leangen.graphql.metadata.execution.SingletonMethodInvoker;
 import io.leangen.graphql.util.ClassUtils;
+import io.leangen.graphql.util.Utils;
 
 /**
  * Created by bojan.tomic on 6/10/16.
@@ -21,7 +23,8 @@ import io.leangen.graphql.util.ClassUtils;
 public class PublicResolverBuilder extends FilteredResolverBuilder {
 
     @SuppressWarnings("WeakerAccess")
-    public PublicResolverBuilder() {
+    public PublicResolverBuilder(String basePackage) {
+        this.basePackage = basePackage;
         this.operationNameGenerator = new MethodOperationNameGenerator();
         this.argumentExtractor = new AnnotatedArgumentBuilder();
     }
@@ -38,14 +41,15 @@ public class PublicResolverBuilder extends FilteredResolverBuilder {
 
     private Collection<Resolver> buildQueryResolvers(Object querySourceBean, AnnotatedType beanType, List<Predicate<Member>> filters) {
         Class<?> rawType = ClassUtils.getRawType(beanType.getType());
-        if (rawType.isArray()) return Collections.emptyList();
+        if (rawType.isArray() || rawType.isPrimitive()) return Collections.emptyList();
         return Arrays.stream(rawType.getMethods())
-                .filter(method -> rawType.getPackage().equals(method.getDeclaringClass().getPackage()))
+                .filter(method -> isPackageAcceptable(method, rawType))
                 .filter(this::isQuery)
                 .filter(filters.stream().reduce(Predicate::and).orElse(acceptAll))
                 .map(method -> new Resolver(
                         operationNameGenerator.generateQueryName(method, beanType),
                         operationNameGenerator.generateQueryName(method, beanType),
+                        method.isAnnotationPresent(Batched.class),
                         querySourceBean == null ? new MethodInvoker(method, beanType) : new SingletonMethodInvoker(querySourceBean, method, beanType),
                         argumentExtractor.buildResolverArguments(method, beanType)
                 ))
@@ -54,14 +58,15 @@ public class PublicResolverBuilder extends FilteredResolverBuilder {
 
     private Collection<Resolver> buildMutationResolvers(Object querySourceBean, AnnotatedType beanType, List<Predicate<Member>> filters) {
         Class<?> rawType = ClassUtils.getRawType(beanType.getType());
-        if (rawType.isArray()) return Collections.emptyList();
+        if (rawType.isArray()|| rawType.isPrimitive()) return Collections.emptyList();
         return Arrays.stream(rawType.getMethods())
-                .filter(method -> rawType.getPackage().equals(method.getDeclaringClass().getPackage()))
+                .filter(method -> isPackageAcceptable(method, rawType))
                 .filter(this::isMutation)
                 .filter(filters.stream().reduce(Predicate::and).orElse(acceptAll))
                 .map(method -> new Resolver(
                         operationNameGenerator.generateMutationName(method, beanType),
                         operationNameGenerator.generateMutationName(method, beanType),
+                        method.isAnnotationPresent(Batched.class),
                         querySourceBean == null ? new MethodInvoker(method, beanType) : new SingletonMethodInvoker(querySourceBean, method, beanType),
                         argumentExtractor.buildResolverArguments(method, beanType)
                 ))
@@ -75,4 +80,9 @@ public class PublicResolverBuilder extends FilteredResolverBuilder {
     protected boolean isMutation(Method method) {
         return method.getReturnType() == void.class;
     }
+
+    private boolean isPackageAcceptable(Method method, Class<?> beanType) {
+        String basePackage = Utils.notEmpty(this.basePackage) ? this.basePackage : beanType.getPackage().getName();
+        return method.getDeclaringClass().equals(beanType) || method.getDeclaringClass().getPackage().getName().startsWith(basePackage);
+    }
 }
