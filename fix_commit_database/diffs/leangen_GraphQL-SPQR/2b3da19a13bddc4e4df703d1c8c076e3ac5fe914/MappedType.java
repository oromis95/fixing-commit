@@ -3,16 +3,21 @@ package io.leangen.graphql.generator;
 import java.lang.reflect.AnnotatedType;
 
 import graphql.schema.GraphQLObjectType;
+import graphql.schema.GraphQLOutputType;
 
 /**
  * @author Bojan Tomic (kaqqao)
  */
 public class MappedType {
-    public AnnotatedType javaType;
-    public GraphQLObjectType graphQLType;
+    public final AnnotatedType javaType;
+    public final GraphQLOutputType graphQLType;
 
-    public MappedType(AnnotatedType javaType, GraphQLObjectType graphQLType) {
+    public MappedType(AnnotatedType javaType, GraphQLOutputType graphQLType) {
         this.javaType = javaType;
         this.graphQLType = graphQLType;
     }
+    
+    public GraphQLObjectType getAsObjectType() {
+        return (GraphQLObjectType) graphQLType;
+    }
 }
