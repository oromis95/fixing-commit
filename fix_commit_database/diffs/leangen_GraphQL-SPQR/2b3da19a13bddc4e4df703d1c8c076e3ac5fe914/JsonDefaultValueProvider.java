@@ -5,6 +5,7 @@ import java.lang.reflect.Parameter;
 
 import io.leangen.graphql.metadata.OperationArgumentDefaultValue;
 import io.leangen.graphql.metadata.strategy.type.DefaultTypeInfoGenerator;
+import io.leangen.graphql.metadata.strategy.value.ValueMapper;
 import io.leangen.graphql.util.Defaults;
 
 /**
@@ -12,14 +13,19 @@ import io.leangen.graphql.util.Defaults;
  */
 public class JsonDefaultValueProvider implements DefaultValueProvider {
 
+    private final ValueMapper valueMapper;
+
+    public JsonDefaultValueProvider() {
+        this.valueMapper =  Defaults.valueMapperFactory(null, new DefaultTypeInfoGenerator())
+                .getValueMapper();
+    }
+
     @Override
     public OperationArgumentDefaultValue getDefaultValue(Parameter parameter, AnnotatedType parameterType, OperationArgumentDefaultValue initialValue) {
         if (initialValue.isEmpty()) {
             return initialValue;
         } else {
-            return new OperationArgumentDefaultValue(
-                    Defaults.valueMapperFactory(new DefaultTypeInfoGenerator())
-                            .getValueMapper().fromString(initialValue.get(), parameterType));
+            return new OperationArgumentDefaultValue(valueMapper.fromString(initialValue.get(), parameterType));
         }
     }
 }
