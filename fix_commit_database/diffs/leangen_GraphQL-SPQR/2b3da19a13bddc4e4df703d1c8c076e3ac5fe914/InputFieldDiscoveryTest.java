@@ -8,7 +8,6 @@ import io.leangen.geantyref.GenericTypeReflector;
 import io.leangen.graphql.annotations.GraphQLInputField;
 import io.leangen.graphql.annotations.GraphQLQuery;
 import io.leangen.graphql.metadata.InputField;
-import io.leangen.graphql.metadata.strategy.type.DefaultTypeInfoGenerator;
 import io.leangen.graphql.metadata.strategy.value.gson.GsonValueMapper;
 import io.leangen.graphql.metadata.strategy.value.gson.GsonValueMapperFactory;
 import io.leangen.graphql.metadata.strategy.value.jackson.JacksonValueMapper;
@@ -19,10 +18,8 @@ import static org.junit.Assert.assertTrue;
 
 public class InputFieldDiscoveryTest {
 
-    private JacksonValueMapper jackson = new JacksonValueMapperFactory(new DefaultTypeInfoGenerator())
-            .getValueMapper();
-    private GsonValueMapper gson = new GsonValueMapperFactory(new DefaultTypeInfoGenerator())
-            .getValueMapper();
+    private JacksonValueMapper jackson = new JacksonValueMapperFactory().getValueMapper();
+    private GsonValueMapper gson = new GsonValueMapperFactory().getValueMapper();
 
     private static final String[] expectedDefaultFieldNames = {"field1", "field2", "field3"};
     private static final String[] expectedExplicitFieldNames = {"aaa", "bbb", "ccc"};
