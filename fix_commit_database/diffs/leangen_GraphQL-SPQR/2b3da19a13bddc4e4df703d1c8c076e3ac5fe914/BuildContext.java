@@ -4,8 +4,13 @@ import java.lang.reflect.Type;
 import java.util.HashMap;
 import java.util.Map;
 import java.util.Set;
+import java.util.stream.Collectors;
 
 import graphql.relay.Relay;
+import graphql.schema.GraphQLInputType;
+import graphql.schema.GraphQLInterfaceType;
+import graphql.schema.GraphQLOutputType;
+import graphql.schema.GraphQLType;
 import graphql.schema.TypeResolver;
 import io.leangen.graphql.execution.GlobalEnvironment;
 import io.leangen.graphql.generator.mapping.ArgumentInjectorRepository;
@@ -15,6 +20,7 @@ import io.leangen.graphql.generator.mapping.strategy.InterfaceMappingStrategy;
 import io.leangen.graphql.metadata.strategy.type.TypeInfoGenerator;
 import io.leangen.graphql.metadata.strategy.value.InputFieldDiscoveryStrategy;
 import io.leangen.graphql.metadata.strategy.value.ValueMapperFactory;
+import io.leangen.graphql.util.GraphQLUtils;
 
 @SuppressWarnings("WeakerAccess")
 public class BuildContext {
@@ -24,8 +30,10 @@ public class BuildContext {
     public final TypeRepository typeRepository;
     public final TypeMapperRepository typeMappers;
     public final Relay relay;
+    public final GraphQLInterfaceType node; //Node interface, as defined by the Relay GraphQL spec
     public final TypeResolver typeResolver;
     public final InterfaceMappingStrategy interfaceStrategy;
+    public final String basePackage;
     public final ValueMapperFactory valueMapperFactory;
     public final InputFieldDiscoveryStrategy inputFieldStrategy;
     public final TypeInfoGenerator typeInfoGenerator;
@@ -44,21 +52,33 @@ public class BuildContext {
      */
     public BuildContext(OperationRepository operationRepository, TypeMapperRepository typeMappers,
                         ConverterRepository converters, ArgumentInjectorRepository inputProviders,
-                        InterfaceMappingStrategy interfaceStrategy, TypeInfoGenerator typeInfoGenerator,
-                        ValueMapperFactory valueMapperFactory, InputFieldDiscoveryStrategy inputFieldStrategy,
-                        Set<String> knownTypes, Set<String> knownInputTypes, RelayMappingConfig relayMappingConfig) {
+                        InterfaceMappingStrategy interfaceStrategy, String basePackage,
+                        TypeInfoGenerator typeInfoGenerator, ValueMapperFactory valueMapperFactory,
+                        InputFieldDiscoveryStrategy inputFieldStrategy, Set<GraphQLType> knownTypes,
+                        RelayMappingConfig relayMappingConfig) {
         this.operationRepository = operationRepository;
-        this.typeRepository = new TypeRepository();
+        this.typeRepository = new TypeRepository(knownTypes);
         this.typeMappers = typeMappers;
         this.typeInfoGenerator = typeInfoGenerator;
         this.relay = new Relay();
+        this.node = knownTypes.stream()
+                .filter(GraphQLUtils::isRelayNodeInterface)
+                .findFirst().map(type -> (GraphQLInterfaceType) type)
+                .orElse(relay.nodeInterface(new RelayNodeTypeResolver(this.typeRepository, typeInfoGenerator)));
         this.typeResolver = new DelegatingTypeResolver(this.typeRepository, typeInfoGenerator);
         this.interfaceStrategy = interfaceStrategy;
+        this.basePackage = basePackage;
         this.valueMapperFactory = valueMapperFactory;
         this.inputFieldStrategy = inputFieldStrategy;
         this.globalEnvironment = new GlobalEnvironment(relay, typeRepository, converters, inputProviders);
-        this.knownTypes = knownTypes;
-        this.knownInputTypes = knownInputTypes;
+        this.knownTypes = knownTypes.stream()
+                .filter(type -> type instanceof GraphQLOutputType)
+                .map(GraphQLType::getName)
+                .collect(Collectors.toSet());
+        this.knownInputTypes = knownTypes.stream()
+                .filter(type -> type instanceof GraphQLInputType)
+                .map(GraphQLType::getName)
+                .collect(Collectors.toSet());
         this.relayMappingConfig = relayMappingConfig;
     }
 }
