@@ -9,6 +9,7 @@ import java.math.BigDecimal;
 import java.math.BigInteger;
 import java.util.ArrayList;
 import java.util.Arrays;
+import java.util.Collection;
 import java.util.HashMap;
 import java.util.HashSet;
 import java.util.List;
@@ -214,6 +215,9 @@ interface PropertySetter {
 
 class PropertySetters {
 
+    static final Field[] EMPTY_FIELDS = new Field[0];
+    static final Method[] EMPTY_METHODS = new Method[0];
+
     // final Log log = LogFactory.getLog(getClass());
     final Class<?> clazz;
     final Map<String, PropertySetter> map;
@@ -221,9 +225,9 @@ class PropertySetters {
     public PropertySetters(Class<?> clazz) {
         this.clazz = clazz;
         Map<String, PropertySetter> map = new HashMap<String, PropertySetter>();
-        Method[] ms = clazz.getDeclaredMethods();
-        for (Method m : ms) {
-            String propertyName = getPropertyName(m);
+        Map<String, Method> methods = getAllMethods(clazz);
+        for (String propertyName : methods.keySet()) {
+            Method m = methods.get(propertyName);
             if (propertyName != null) {
                 m.setAccessible(true);
                 Type type = m.getGenericParameterTypes()[0];
@@ -242,9 +246,9 @@ class PropertySetters {
                 });
             }
         }
-        Field[] fs = clazz.getDeclaredFields();
-        for (Field f : fs) {
-            String propertyName = f.getName();
+        Map<String, Field> fields = getAllFields(clazz);
+        for (String propertyName : fields.keySet()) {
+            Field f = fields.get(propertyName);
             if (! map.containsKey(propertyName)) {
                 f.setAccessible(true);
                 Type type = f.getGenericType();
@@ -266,6 +270,33 @@ class PropertySetters {
         this.map = map;
     }
 
+    Map<String, Method> getAllMethods(Class<?> clazz) {
+        Map<String, Method> methods = new HashMap<String, Method>();
+        while (clazz != null) {
+            for (Method m : clazz.getDeclaredMethods()) {
+                String propertyName = getPropertyName(m);
+                if (! methods.containsKey(propertyName)) {
+                    methods.put(propertyName, m);
+                }
+            }
+            clazz = clazz.getSuperclass();
+        }
+        return methods;
+    }
+
+    Map<String, Field> getAllFields(Class<?> clazz) {
+        Map<String, Field> fields = new HashMap<String, Field>();
+        while (clazz != null) {
+            for (Field f : clazz.getDeclaredFields()) {
+                if (! fields.containsKey(f.getName())) {
+                    fields.put(f.getName(), f);
+                }
+            }
+            clazz = clazz.getSuperclass();
+        }
+        return fields;
+    }
+
     /**
      * Get raw type of property, e.g. String, List, JavaBean, etc.
      */
@@ -303,10 +334,10 @@ class PropertySetters {
     String getPropertyName(Method m) {
         String name = m.getName();
         if (name.startsWith("set") && (name.length() >= 4)
-                && m.getReturnType().equals(Void.class)
+                && m.getReturnType().equals(void.class)
                 && (m.getParameterTypes().length == 1)
         ) {
-            return Character.toLowerCase(name.charAt(4)) + name.substring(4);
+            return Character.toLowerCase(name.charAt(3)) + name.substring(4);
         }
         return null;
     }
