@@ -1,6 +1,18 @@
 package com.itranswarp.jsonstream.validator;
 
+/**
+ * Validate property values.
+ * 
+ * @author Michael Liao
+ */
 public interface Validator<T> {
 
-	void validate(T obj, String path, String name);
+    /**
+     * Validate a property value. Throws ValidateException If validation failed.
+     * 
+     * @param propValue The property value.
+     * @param path The path of the JSON document. e.g. "Group.Users[0].Address".
+     * @param name The property name of the JSON document. e.g. "zipcode".
+     */
+	void validate(T propValue, String path, String name);
 }
