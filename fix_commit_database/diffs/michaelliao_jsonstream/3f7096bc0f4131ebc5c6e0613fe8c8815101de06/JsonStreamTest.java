@@ -2,6 +2,10 @@ package com.itranswarp.jsonstream;
 
 import static org.junit.Assert.*;
 
+import java.util.List;
+import java.util.Map;
+
+import org.junit.Assert;
 import org.junit.Test;
 
 public class JsonStreamTest {
@@ -12,6 +16,37 @@ public class JsonStreamTest {
         return new JsonStream(s);
     }
 
+    @Test
+    public void testParseSingleStringOk() throws Exception {
+        assertEquals("", prepareJsonStream("\"\"").parse());
+        assertEquals(" ", prepareJsonStream("\" \"").parse());
+        assertEquals("A\'BC", prepareJsonStream("\"A\'BC\"").parse());
+        assertEquals("\r\n\t\b\f\\\"/$", prepareJsonStream("\"\\r\\n\\t\\b\\f\\\\\\\"\\/$\"").parse());
+        assertEquals("English中文", prepareJsonStream("\"English中文\"").parse());
+        assertEquals("English中文", prepareJsonStream("\"English\\u4E2d\\u6587\"").parse());
+    }
+
+    @Test
+    public void testParseSingleStringFailed() throws Exception {
+        String[] INVALID_STRINGS = {
+                "\"abc\\\"def", // missing end "
+                "\"abc \\a \"", // invalid \a
+                "\"bb \n   \"", // invalid \n
+                "\"bb \r   \"", // invalid \r
+                "\" \\uF0Fp\"", // invalid unicode: F0Fp
+                "\"  \\\'  \""  // invalid \'
+        };
+        for (String s : INVALID_STRINGS) {
+            try {
+                prepareJsonStream(s).parse();
+                fail("Not caught ParseException: " + s);
+            }
+            catch (JsonParseException e) {
+                // ok!
+            }
+        }
+    }
+
     @Test
     public void testParseSingleBoolean() throws Exception {
         assertEquals(Boolean.TRUE, prepareJsonStream("true").parse());
@@ -32,7 +67,7 @@ public class JsonStreamTest {
     }
 
     @Test
-    public void testParseSingleLong() throws Exception {
+    public void testParseSingleLongOk() throws Exception {
         String[] tests = {
                 "0", "00", "000", "-0", "-00",
                 "1", "01", "-1", "-01",
@@ -48,6 +83,39 @@ public class JsonStreamTest {
         }
     }
 
+    @Test
+    public void testParseSingleLongFailed() throws Exception {
+        String[] tests = {
+                "0", "00", "000", "-0", "-00",
+                "1", "01", "-1", "-01",
+                "100", "1020", "-100", "-1020",
+                "999000999", "999000999000", "-999000999", "-999000999000",
+                "9007199254740991", "-9007199254740991"
+        };
+        String[] pres = {
+                "", " ", "-", "+", "  }", "  ] ", "\n.", "e"
+        };
+        String[] ends = {
+                "", " ", "-", "+", "  }", "  ] ", "\n.", "e"
+        };
+        for (String s : tests) {
+            for (String pre : pres) {
+                for (String end : ends) {
+                    if ("".equals(pre.trim() + end.trim()) || "-".equals(pre.trim() + end.trim())) {
+                        continue;
+                    }
+                    try {
+                        prepareJsonStream(pre + s + end).parse();
+                        fail("Not caught JsonParseException when parse: " + pre + s + end + ".");
+                    }
+                    catch (JsonParseException e) {
+                        // ok
+                    }
+                }
+            }
+        }
+    }
+
     @Test
     public void testParseSingleDouble() throws Exception {
         String[] tests = {
@@ -91,11 +159,10 @@ public class JsonStreamTest {
         for (String s : tests) {
             for (String pre : pres) {
                 for (String end : ends) {
-                    if ("".equals(pre.trim() + end.trim())) {
+                    if ("".equals(pre.trim() + end.trim()) || "-".equals(pre.trim() + end.trim())) {
                         continue;
                     }
                     try {
-                        System.out.println(pre+s+end);
                         prepareJsonStream(pre + s + end).parse();
                         fail("Not caught JsonParseException when parse: " + pre + s + end + ".");
                     }
@@ -107,4 +174,33 @@ public class JsonStreamTest {
         }
     }
 
+    @Test
+    public void testParseEmptyArrayAndObjectOk() throws Exception {
+        String[] tests = {
+                "[]", "  []", "\n \r[] \t \n", "  \n  []\t \r\r \n",
+                " [ ] ", " [    ] ", "\r \n[\n ]\t \t", " [  \n]\t ", " \n[ \t \n ]\t \n"
+        };
+        Object[] expecteds = {};
+        for (String s : tests) {
+            assertArrayEquals(expecteds, ((List<?>) prepareJsonStream(s).parse()).toArray());
+            Map<?, ?> map = (Map<?, ?>) prepareJsonStream(s.replace('[', '{').replace(']', '}')).parse();
+            assertTrue(map.isEmpty());
+        }
+    }
+
+    @Test
+    public void testParseNonEmptyArrayOk() throws Exception {
+        String[] tests = {
+                "[\"TEST\",true,1,false,2.5,null,\"END\"]",
+                "[ \"TEST\", true, 1, false , 2.5 ,null, \"END\" ]",
+                " [ \"TEST\", \ntrue, 1,\tfalse , 2.5 ,null, \"END\" ] ",
+                "\n[\n  \"TEST\", true, 1, false , 2.5 ,null, \"END\"\r]\n\t",
+                "\r[\t\"TEST\",\n \n \r true,    1,    false \n, 2.5 ,\tnull, \"END\"\n ]\t"
+        };
+        Object[] expecteds = {"TEST", true, 1L, false, 2.5, null, "END"};
+        for (String s : tests) {
+            assertArrayEquals(expecteds, ((List<?>) prepareJsonStream(s).parse()).toArray());
+        }
+    }
+
 }
