@@ -1,6 +1,8 @@
 package at.favre.tools.uberadb.util;
 
 import java.io.BufferedReader;
+import java.io.File;
+import java.io.FileFilter;
 import java.io.InputStreamReader;
 import java.util.Arrays;
 
@@ -30,19 +32,13 @@ public class CmdUtil {
     }
 
     public static boolean canRunCmd(String[] cmd) {
-        try {
-            ProcessBuilder pb = new ProcessBuilder(cmd);
-            pb.redirectErrorStream(true);
-            Process process = pb.start();
-            try (BufferedReader inStreamReader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
-                while ((inStreamReader.readLine()) != null) {
-                }
-            }
-            process.waitFor();
-        } catch (Exception e) {
+        Result result = runCmd(cmd);
+
+        if (result.exception != null) {
             return false;
+        } else {
+            return true;
         }
-        return true;
     }
 
     public static <T> T[] concat(T[] first, T[] second) {
@@ -51,6 +47,50 @@ public class CmdUtil {
         return result;
     }
 
+    public static File checkAndGetFromPATHEnvVar(final String matchesExecutable) {
+        String[] pathParts = System.getenv("PATH").split(";");
+        for (String pathPart : pathParts) {
+            File pathFile = new File(pathPart);
+
+            if (pathFile.isFile() && pathFile.getName().toLowerCase().contains(matchesExecutable)) {
+                return pathFile;
+            } else if (pathFile.isDirectory()) {
+                File[] matchedFiles = pathFile.listFiles(new FileFilter() {
+                    @Override
+                    public boolean accept(File pathname) {
+                        return pathname.getName().toLowerCase().contains(matchesExecutable);
+                    }
+                });
+
+                if (matchedFiles != null) {
+                    for (File matchedFile : matchedFiles) {
+                        if (CmdUtil.canRunCmd(new String[]{matchedFile.getAbsolutePath()})) {
+                            return matchedFile;
+                        }
+                    }
+                }
+            }
+        }
+        return null;
+    }
+
+    public static OS getOsType() {
+        String osName = System.getProperty("os.name").toLowerCase();
+
+        if (osName.contains("win")) {
+            return OS.WIN;
+        }
+        if (osName.contains("win")) {
+            return OS.MAC;
+        }
+
+        return OS._NIX;
+    }
+
+    public enum OS {
+        WIN, MAC, _NIX;
+    }
+
     public static class Result {
         public final Exception exception;
         public final String out;
