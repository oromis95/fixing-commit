@@ -23,6 +23,7 @@ public class AdbToolTest {
     public TemporaryFolder temporaryFolder = new TemporaryFolder();
     private MockAdbCmdProvider adbMockCmdProviderMultiDevices;
     private MockAdbCmdProvider adbMockCmdProviderSingleDevice;
+    private MockAdbLocationFinder mockAdbLocationFinder;
     private List<String> installedPackages;
     private List<AdbDevice> adbDevices;
     private File apks;
@@ -36,6 +37,7 @@ public class AdbToolTest {
                 new AdbDevice("emulator-5154", AdbDevice.Status.OK, "Android_SDK_built_for_x86", "sdk_google_phone_x86", true));
         adbMockCmdProviderMultiDevices = new MockAdbCmdProvider(adbDevices, installedPackages, true);
         adbMockCmdProviderSingleDevice = new MockAdbCmdProvider(Collections.singletonList(adbDevices.get(0)), installedPackages, true);
+        mockAdbLocationFinder = new MockAdbLocationFinder();
         apks = new File(getClass().getClassLoader().getResource("apks").toURI().getPath());
     }
 
@@ -47,14 +49,14 @@ public class AdbToolTest {
     @Test
     public void testSimpleUninstallMultiDevices() throws Exception {
         Arg arg = new Arg(new String[]{"com.example.*"}, null, null, null, null, 0, false, false, false, false, false, true, false, false, false, Arg.Mode.UNINSTALL);
-        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
+        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices, mockAdbLocationFinder);
         check(result, adbMockCmdProviderMultiDevices.installedCount() * adbMockCmdProviderMultiDevices.deviceCount(), 0, adbMockCmdProviderMultiDevices.deviceCount());
     }
 
     @Test
     public void testSimpleInstallMultiDevices() throws Exception {
         Arg arg = new Arg(new String[]{apks.getAbsolutePath()}, null, null, null, null, 0, false, false, false, false, false, true, false, false, false, Arg.Mode.INSTALL);
-        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
+        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices, mockAdbLocationFinder);
         assertNotNull(result);
         check(result, apks.listFiles().length * adbDevices.size(), 0, adbMockCmdProviderMultiDevices.deviceCount());
     }
@@ -63,7 +65,7 @@ public class AdbToolTest {
     public void testSimpleInstallMultiDevicesMultipleFiles() throws Exception {
         File[] files = apks.listFiles();
         Arg arg = new Arg(new String[]{files[0].getAbsolutePath(), files[1].getAbsolutePath()}, null, null, null, null, 0, false, false, false, false, false, true, true, false, false, Arg.Mode.INSTALL);
-        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
+        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices, mockAdbLocationFinder);
         assertNotNull(result);
         check(result, 2 * adbDevices.size(), 0, adbMockCmdProviderMultiDevices.deviceCount());
     }
@@ -71,7 +73,7 @@ public class AdbToolTest {
     @Test
     public void testSimpleInstallMultiDevicesGrant() throws Exception {
         Arg arg = new Arg(new String[]{apks.getAbsolutePath()}, null, null, null, null, 0, false, false, false, false, false, true, true, false, false, Arg.Mode.INSTALL);
-        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
+        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices, mockAdbLocationFinder);
         assertNotNull(result);
         check(result, apks.listFiles().length * adbDevices.size(), 0, adbMockCmdProviderMultiDevices.deviceCount());
     }
@@ -79,98 +81,98 @@ public class AdbToolTest {
     @Test
     public void testSimpleUninstallOneDevice() throws Exception {
         Arg arg = new Arg(new String[]{"com.example.*"}, null, null, null, null, 0, false, false, false, false, false, true, false, false, false, Arg.Mode.UNINSTALL);
-        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderSingleDevice);
+        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderSingleDevice, mockAdbLocationFinder);
         check(result, installedPackages.size(), 0, adbMockCmdProviderSingleDevice.deviceCount());
     }
 
     @Test
     public void testSimpleUninstallOneDeviceTwoPackages() throws Exception {
         Arg arg = new Arg(new String[]{"com.example.app1", "com.example.app2"}, null, null, null, null, 0, false, false, false, false, false, true, false, false, false, Arg.Mode.UNINSTALL);
-        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderSingleDevice);
+        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderSingleDevice, mockAdbLocationFinder);
         check(result, 2, 0, adbMockCmdProviderSingleDevice.deviceCount());
     }
 
     @Test
     public void testSimpleInstallOneDevice() throws Exception {
         Arg arg = new Arg(new String[]{apks.getAbsolutePath()}, null, null, null, null, 0, false, false, false, false, false, true, false, false, false, Arg.Mode.INSTALL);
-        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderSingleDevice);
+        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderSingleDevice, new AdbLocationFinderImpl());
         check(result, apks.listFiles().length, 0, adbMockCmdProviderSingleDevice.deviceCount());
     }
 
     @Test
     public void testSimpleInstallMultiDevicesSingleApk() throws Exception {
         Arg arg = new Arg(new String[]{apks.listFiles()[0].getAbsolutePath()}, null, null, null, null, 0, false, false, false, false, false, true, false, false, false, Arg.Mode.INSTALL);
-        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
+        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices, mockAdbLocationFinder);
         check(result, adbMockCmdProviderMultiDevices.deviceCount(), 0, adbMockCmdProviderMultiDevices.deviceCount());
     }
 
     @Test
     public void testSimpleUninstallOneDeviceWithFail() throws Exception {
         Arg arg = new Arg(new String[]{"com.example.*"}, null, null, null, null, 0, false, false, false, false, false, true, false, false, false, Arg.Mode.UNINSTALL);
-        Commons.ActionResult result = AdbTool.execute(arg, new MockAdbCmdProvider(Collections.singletonList(adbDevices.get(0)), installedPackages, false));
+        Commons.ActionResult result = AdbTool.execute(arg, new MockAdbCmdProvider(Collections.singletonList(adbDevices.get(0)), installedPackages, false), mockAdbLocationFinder);
         check(result, 0, installedPackages.size(), 1);
     }
 
     @Test
     public void testSimpleInstallOneDeviceWithFail() throws Exception {
         Arg arg = new Arg(new String[]{apks.getAbsolutePath()}, null, null, null, null, 0, false, false, false, false, false, true, false, false, false, Arg.Mode.INSTALL);
-        Commons.ActionResult result = AdbTool.execute(arg, new MockAdbCmdProvider(Collections.singletonList(adbDevices.get(0)), installedPackages, false));
+        Commons.ActionResult result = AdbTool.execute(arg, new MockAdbCmdProvider(Collections.singletonList(adbDevices.get(0)), installedPackages, false), mockAdbLocationFinder);
         check(result, 0, apks.listFiles().length, 1);
     }
 
     @Test
     public void testSimpleUninstallMultiDevicesSelectSpecific() throws Exception {
         Arg arg = new Arg(new String[]{"com.example.*"}, null, adbDevices.get(0).serial, null, null, 0, false, false, false, false, false, true, false, false, false, Arg.Mode.UNINSTALL);
-        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
+        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices, mockAdbLocationFinder);
         check(result, adbMockCmdProviderMultiDevices.installedCount(), 0, 1);
     }
 
     @Test
     public void testSimpleUninstallMultiDevicesDryRun() throws Exception {
         Arg arg = new Arg(new String[]{"com.example.*"}, null, null, null, null, 0, true, false, false, false, false, true, false, false, false, Arg.Mode.UNINSTALL);
-        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
+        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices, mockAdbLocationFinder);
         check(result, 0, 0, adbMockCmdProviderMultiDevices.deviceCount());
     }
 
     @Test
     public void testSimpleUninstallMultiDevicesSkipEmu() throws Exception {
         Arg arg = new Arg(new String[]{"com.example.*"}, null, null, null, null, 0, false, true, false, false, false, true, false, false, false, Arg.Mode.UNINSTALL);
-        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
+        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices, mockAdbLocationFinder);
         check(result, adbMockCmdProviderMultiDevices.installedCount() * (adbMockCmdProviderMultiDevices.deviceCount() - 1), 0, adbMockCmdProviderMultiDevices.deviceCount() - 1);
     }
 
     @Test
     public void testSimpleInstallMultiDevicesSelectSpecific() throws Exception {
         Arg arg = new Arg(new String[]{apks.getAbsolutePath()}, null, adbDevices.get(0).serial, null, null, 0, false, false, false, false, false, true, false, false, false, Arg.Mode.INSTALL);
-        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
+        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices, mockAdbLocationFinder);
         check(result, apks.listFiles().length, 0, 1);
     }
 
     @Test
     public void testSimpleInstallMultiDevicesDryRun() throws Exception {
         Arg arg = new Arg(new String[]{apks.getAbsolutePath()}, null, null, null, null, 0, true, false, false, false, false, true, false, false, false, Arg.Mode.INSTALL);
-        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
+        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices, mockAdbLocationFinder);
         check(result, 0, 0, adbMockCmdProviderMultiDevices.deviceCount());
     }
 
     @Test
     public void testSimpleInstallMultiDevicesSkipEmu() throws Exception {
         Arg arg = new Arg(new String[]{apks.getAbsolutePath()}, null, null, null, null, 0, false, true, false, false, false, true, false, false, false, Arg.Mode.INSTALL);
-        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
+        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices, mockAdbLocationFinder);
         check(result, apks.listFiles().length * (adbDevices.size() - 1), 0, adbMockCmdProviderMultiDevices.deviceCount() - 1);
     }
 
     @Test
     public void testSimpleBugReport() throws Exception {
         Arg arg = new Arg(new String[]{temporaryFolder.newFolder().getAbsolutePath()}, null, null, null, null, 0, false, false, false, false, false, false, false, false, false, Arg.Mode.BUGREPORT);
-        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
+        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices, mockAdbLocationFinder);
         check(result, 0, 0, adbMockCmdProviderMultiDevices.deviceCount());
     }
 
     @Test
     public void testSimpleBugReportSingleDevice() throws Exception {
         Arg arg = new Arg(new String[]{temporaryFolder.newFolder().getAbsolutePath()}, null, null, null, null, 0, false, false, false, false, false, false, false, false, false, Arg.Mode.BUGREPORT);
-        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderSingleDevice);
+        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderSingleDevice, mockAdbLocationFinder);
         check(result, 0, 0, adbMockCmdProviderSingleDevice.deviceCount());
     }
 
