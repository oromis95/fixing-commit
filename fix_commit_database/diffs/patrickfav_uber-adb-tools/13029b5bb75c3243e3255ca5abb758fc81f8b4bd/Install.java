@@ -38,36 +38,48 @@ public class Install {
         }
     }
 
-    private static String[] createInstallCmd(AdbDevice device, String absolutPath, Arg arguments) {
-        if (!arguments.keepData) {
-            return new String[]{"-s", device.serial, "install", absolutPath};
-        } else {
-            return new String[]{"-s", device.serial, "install", "-r", absolutPath};
+    private static String[] createInstallCmd(AdbDevice device, String absolutePath, Arg arguments) {
+        List<String> cmdList = new ArrayList<>();
+        cmdList.add("-s");
+        cmdList.add(device.serial);
+        cmdList.add("install");
+
+        if (arguments.keepData) {
+            cmdList.add("-r");
+        }
+        if (arguments.grantPermissions) {
+            cmdList.add("-g");
         }
+        cmdList.add(absolutePath);
+
+        return cmdList.toArray(new String[cmdList.size()]);
     }
 
     public static List<File> getFilesToInstall(Arg arguments) {
         List<File> installFiles = new ArrayList<>();
 
-        File apkFileRef = new File(arguments.mainArgument);
-        if (!apkFileRef.exists()) {
-            throw new IllegalArgumentException("could not find " + arguments.mainArgument + " for install");
-        }
-        if (apkFileRef.isFile()) {
-            if (apkFileRef.getName().toLowerCase().endsWith(".apk")) {
-                installFiles.add(apkFileRef);
+        for (String dir : arguments.mainArgument) {
+            File apkFileRef = new File(dir);
+            if (!apkFileRef.exists()) {
+                throw new IllegalArgumentException("could not find " + dir + " for install");
             }
-        } else if (apkFileRef.isDirectory()) {
-            for (File apkFile : apkFileRef.listFiles()) {
-                if (apkFile.getName().toLowerCase().endsWith(".apk")) {
-                    installFiles.add(apkFile);
+            if (apkFileRef.isFile()) {
+                if (apkFileRef.getName().toLowerCase().endsWith(".apk")) {
+                    installFiles.add(apkFileRef);
+                }
+            } else if (apkFileRef.isDirectory()) {
+                for (File apkFile : apkFileRef.listFiles()) {
+                    if (apkFile.getName().toLowerCase().endsWith(".apk")) {
+                        installFiles.add(apkFile);
+                    }
                 }
             }
-        }
 
-        if (installFiles.isEmpty()) {
-            throw new IllegalArgumentException("could not find any apk files in " + arguments.mainArgument + " to install");
+            if (installFiles.isEmpty()) {
+                throw new IllegalArgumentException("could not find any apk files in " + arguments.mainArgument + " to install");
+            }
         }
+
         return installFiles;
     }
 }
