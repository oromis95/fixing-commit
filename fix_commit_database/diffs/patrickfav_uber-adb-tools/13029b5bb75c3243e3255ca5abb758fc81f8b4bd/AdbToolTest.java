@@ -46,14 +46,31 @@ public class AdbToolTest {
 
     @Test
     public void testSimpleUninstallMultiDevices() throws Exception {
-        Arg arg = new Arg("com.example.*", null, null, null, false, false, false, false, false, true, Arg.Mode.UNINSTALL);
+        Arg arg = new Arg(new String[]{"com.example.*"}, null, null, null, null, false, false, false, false, false, true, false, false, Arg.Mode.UNINSTALL);
         Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
         check(result, adbMockCmdProviderMultiDevices.installedCount() * adbMockCmdProviderMultiDevices.deviceCount(), 0, adbMockCmdProviderMultiDevices.deviceCount());
     }
 
     @Test
     public void testSimpleInstallMultiDevices() throws Exception {
-        Arg arg = new Arg(apks.getAbsolutePath(), null, null, null, false, false, false, false, false, true, Arg.Mode.INSTALL);
+        Arg arg = new Arg(new String[]{apks.getAbsolutePath()}, null, null, null, null, false, false, false, false, false, true, false, false, Arg.Mode.INSTALL);
+        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
+        assertNotNull(result);
+        check(result, apks.listFiles().length * adbDevices.size(), 0, adbMockCmdProviderMultiDevices.deviceCount());
+    }
+
+    @Test
+    public void testSimpleInstallMultiDevicesMultipleFiles() throws Exception {
+        File[] files = apks.listFiles();
+        Arg arg = new Arg(new String[]{files[0].getAbsolutePath(), files[1].getAbsolutePath()}, null, null, null, null, false, false, false, false, false, true, true, false, Arg.Mode.INSTALL);
+        Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
+        assertNotNull(result);
+        check(result, 2 * adbDevices.size(), 0, adbMockCmdProviderMultiDevices.deviceCount());
+    }
+
+    @Test
+    public void testSimpleInstallMultiDevicesGrant() throws Exception {
+        Arg arg = new Arg(new String[]{apks.getAbsolutePath()}, null, null, null, null, false, false, false, false, false, true, true, false, Arg.Mode.INSTALL);
         Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
         assertNotNull(result);
         check(result, apks.listFiles().length * adbDevices.size(), 0, adbMockCmdProviderMultiDevices.deviceCount());
@@ -61,98 +78,98 @@ public class AdbToolTest {
 
     @Test
     public void testSimpleUninstallOneDevice() throws Exception {
-        Arg arg = new Arg("com.example.*", null, null, null, false, false, false, false, false, true, Arg.Mode.UNINSTALL);
+        Arg arg = new Arg(new String[]{"com.example.*"}, null, null, null, null, false, false, false, false, false, true, false, false, Arg.Mode.UNINSTALL);
         Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderSingleDevice);
         check(result, installedPackages.size(), 0, adbMockCmdProviderSingleDevice.deviceCount());
     }
 
     @Test
     public void testSimpleUninstallOneDeviceTwoPackages() throws Exception {
-        Arg arg = new Arg("com.example.app1,com.example.app2", null, null, null, false, false, false, false, false, true, Arg.Mode.UNINSTALL);
+        Arg arg = new Arg(new String[]{"com.example.app1", "com.example.app2"}, null, null, null, null, false, false, false, false, false, true, false, false, Arg.Mode.UNINSTALL);
         Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderSingleDevice);
         check(result, 2, 0, adbMockCmdProviderSingleDevice.deviceCount());
     }
 
     @Test
     public void testSimpleInstallOneDevice() throws Exception {
-        Arg arg = new Arg(apks.getAbsolutePath(), null, null, null, false, false, false, false, false, true, Arg.Mode.INSTALL);
+        Arg arg = new Arg(new String[]{apks.getAbsolutePath()}, null, null, null, null, false, false, false, false, false, true, false, false, Arg.Mode.INSTALL);
         Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderSingleDevice);
         check(result, apks.listFiles().length, 0, adbMockCmdProviderSingleDevice.deviceCount());
     }
 
     @Test
     public void testSimpleInstallMultiDevicesSingleApk() throws Exception {
-        Arg arg = new Arg(apks.listFiles()[0].getAbsolutePath(), null, null, null, false, false, false, false, false, true, Arg.Mode.INSTALL);
+        Arg arg = new Arg(new String[]{apks.listFiles()[0].getAbsolutePath()}, null, null, null, null, false, false, false, false, false, true, false, false, Arg.Mode.INSTALL);
         Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
         check(result, adbMockCmdProviderMultiDevices.deviceCount(), 0, adbMockCmdProviderMultiDevices.deviceCount());
     }
 
     @Test
     public void testSimpleUninstallOneDeviceWithFail() throws Exception {
-        Arg arg = new Arg("com.example.*", null, null, null, false, false, false, false, false, true, Arg.Mode.UNINSTALL);
+        Arg arg = new Arg(new String[]{"com.example.*"}, null, null, null, null, false, false, false, false, false, true, false, false, Arg.Mode.UNINSTALL);
         Commons.ActionResult result = AdbTool.execute(arg, new MockAdbCmdProvider(Collections.singletonList(adbDevices.get(0)), installedPackages, false));
         check(result, 0, installedPackages.size(), 1);
     }
 
     @Test
     public void testSimpleInstallOneDeviceWithFail() throws Exception {
-        Arg arg = new Arg(apks.getAbsolutePath(), null, null, null, false, false, false, false, false, true, Arg.Mode.INSTALL);
+        Arg arg = new Arg(new String[]{apks.getAbsolutePath()}, null, null, null, null, false, false, false, false, false, true, false, false, Arg.Mode.INSTALL);
         Commons.ActionResult result = AdbTool.execute(arg, new MockAdbCmdProvider(Collections.singletonList(adbDevices.get(0)), installedPackages, false));
         check(result, 0, apks.listFiles().length, 1);
     }
 
     @Test
     public void testSimpleUninstallMultiDevicesSelectSpecific() throws Exception {
-        Arg arg = new Arg("com.example.*", null, adbDevices.get(0).serial, null, false, false, false, false, false, true, Arg.Mode.UNINSTALL);
+        Arg arg = new Arg(new String[]{"com.example.*"}, null, adbDevices.get(0).serial, null, null, false, false, false, false, false, true, false, false, Arg.Mode.UNINSTALL);
         Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
         check(result, adbMockCmdProviderMultiDevices.installedCount(), 0, 1);
     }
 
     @Test
     public void testSimpleUninstallMultiDevicesDryRun() throws Exception {
-        Arg arg = new Arg("com.example.*", null, null, null, true, false, false, false, false, true, Arg.Mode.UNINSTALL);
+        Arg arg = new Arg(new String[]{"com.example.*"}, null, null, null, null, true, false, false, false, false, true, false, false, Arg.Mode.UNINSTALL);
         Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
         check(result, 0, 0, adbMockCmdProviderMultiDevices.deviceCount());
     }
 
     @Test
     public void testSimpleUninstallMultiDevicesSkipEmu() throws Exception {
-        Arg arg = new Arg("com.example.*", null, null, null, false, true, false, false, false, true, Arg.Mode.UNINSTALL);
+        Arg arg = new Arg(new String[]{"com.example.*"}, null, null, null, null, false, true, false, false, false, true, false, false, Arg.Mode.UNINSTALL);
         Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
         check(result, adbMockCmdProviderMultiDevices.installedCount() * (adbMockCmdProviderMultiDevices.deviceCount() - 1), 0, adbMockCmdProviderMultiDevices.deviceCount() - 1);
     }
 
     @Test
     public void testSimpleInstallMultiDevicesSelectSpecific() throws Exception {
-        Arg arg = new Arg(apks.getAbsolutePath(), null, adbDevices.get(0).serial, null, false, false, false, false, false, true, Arg.Mode.INSTALL);
+        Arg arg = new Arg(new String[]{apks.getAbsolutePath()}, null, adbDevices.get(0).serial, null, null, false, false, false, false, false, true, false, false, Arg.Mode.INSTALL);
         Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
         check(result, apks.listFiles().length, 0, 1);
     }
 
     @Test
     public void testSimpleInstallMultiDevicesDryRun() throws Exception {
-        Arg arg = new Arg(apks.getAbsolutePath(), null, null, null, true, false, false, false, false, true, Arg.Mode.INSTALL);
+        Arg arg = new Arg(new String[]{apks.getAbsolutePath()}, null, null, null, null, true, false, false, false, false, true, false, false, Arg.Mode.INSTALL);
         Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
         check(result, 0, 0, adbMockCmdProviderMultiDevices.deviceCount());
     }
 
     @Test
     public void testSimpleInstallMultiDevicesSkipEmu() throws Exception {
-        Arg arg = new Arg(apks.getAbsolutePath(), null, null, null, false, true, false, false, false, true, Arg.Mode.INSTALL);
+        Arg arg = new Arg(new String[]{apks.getAbsolutePath()}, null, null, null, null, false, true, false, false, false, true, false, false, Arg.Mode.INSTALL);
         Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
         check(result, apks.listFiles().length * (adbDevices.size() - 1), 0, adbMockCmdProviderMultiDevices.deviceCount() - 1);
     }
 
     @Test
     public void testSimpleBugReport() throws Exception {
-        Arg arg = new Arg(temporaryFolder.newFolder().getAbsolutePath(), null, null, null, false, false, false, false, false, false, Arg.Mode.BUGREPORT);
+        Arg arg = new Arg(new String[]{temporaryFolder.newFolder().getAbsolutePath()}, null, null, null, null, false, false, false, false, false, false, false, false, Arg.Mode.BUGREPORT);
         Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderMultiDevices);
         check(result, 0, 0, adbMockCmdProviderMultiDevices.deviceCount());
     }
 
     @Test
     public void testSimpleBugReportSingleDevice() throws Exception {
-        Arg arg = new Arg(temporaryFolder.newFolder().getAbsolutePath(), null, null, null, false, false, false, false, false, false, Arg.Mode.BUGREPORT);
+        Arg arg = new Arg(new String[]{temporaryFolder.newFolder().getAbsolutePath()}, null, null, null, null, false, false, false, false, false, false, false, false, Arg.Mode.BUGREPORT);
         Commons.ActionResult result = AdbTool.execute(arg, adbMockCmdProviderSingleDevice);
         check(result, 0, 0, adbMockCmdProviderSingleDevice.deviceCount());
     }
