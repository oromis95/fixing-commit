@@ -5,6 +5,7 @@ import at.favre.tools.uberadb.CmdProvider;
 import at.favre.tools.uberadb.parser.AdbDevice;
 import at.favre.tools.uberadb.parser.InstalledPackagesParser;
 import at.favre.tools.uberadb.ui.Arg;
+import at.favre.tools.uberadb.util.FileUtil;
 
 import java.io.File;
 import java.util.ArrayList;
@@ -13,7 +14,7 @@ import java.util.List;
 public class Install {
     public static void execute(AdbLocationFinder.LocationResult adbLocation, Arg arguments, CmdProvider cmdProvider, List<File> installFiles, boolean preview, Commons.ActionResult actionResult, AdbDevice device) {
         for (File installFile : installFiles) {
-            String installStatus = "\t" + installFile.getName();
+            String installStatus = "\t" + installFile.getName() + "\n\t\tchecksum: " + FileUtil.createChecksum(installFile, "SHA-256") + " (sha256)\n";
 
             if (!arguments.dryRun) {
                 if (!preview) {
@@ -21,17 +22,17 @@ public class Install {
                             installFile.getAbsolutePath(), arguments), cmdProvider, adbLocation);
 
                     if (InstalledPackagesParser.wasSuccessfulInstalled(installCmdResult.out)) {
-                        installStatus += "\tSuccess";
+                        installStatus += "\t\tSuccess";
                         actionResult.successCount++;
                     } else {
-                        installStatus += "\tFail " + InstalledPackagesParser.parseShortenedInstallStatus(installCmdResult.out);
+                        installStatus += "\t\tFail " + InstalledPackagesParser.parseShortenedInstallStatus(installCmdResult.out);
                         actionResult.failureCount++;
                     }
                 } else {
                     actionResult.successCount++;
                 }
             } else {
-                installStatus += "\tskip";
+                installStatus += "\t\tskip";
             }
             Commons.log(installStatus, arguments);
         }
