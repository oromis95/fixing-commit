@@ -30,7 +30,7 @@ public class CmdUtil {
                 File[] matchedFiles = pathFile.listFiles(new FileFilter() {
                     @Override
                     public boolean accept(File pathname) {
-                        return getFileNameWithoutExtension(pathname).toLowerCase().equals(matchesExecutable);
+                        return FileUtil.getFileNameWithoutExtension(pathname).toLowerCase().equals(matchesExecutable);
                     }
                 });
 
@@ -46,15 +46,6 @@ public class CmdUtil {
         return null;
     }
 
-    public static String getFileNameWithoutExtension(File file) {
-        String fileName = file.getName();
-        int pos = fileName.lastIndexOf(".");
-        if (pos > 0) {
-            fileName = fileName.substring(0, pos);
-        }
-        return fileName;
-    }
-
     public static OS getOsType() {
         String osName = System.getProperty("os.name").toLowerCase();
 
@@ -80,4 +71,8 @@ public class CmdUtil {
         }
         return sb.toString().trim();
     }
+
+    public static String jarVersion() {
+        return CmdUtil.class.getPackage().getImplementationVersion();
+    }
 }
