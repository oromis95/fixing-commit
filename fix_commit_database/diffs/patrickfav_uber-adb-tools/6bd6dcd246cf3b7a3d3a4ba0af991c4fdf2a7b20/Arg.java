@@ -2,8 +2,9 @@ package at.favre.tools.uberadb.ui;
 
 
 public class Arg {
-    public String filterString;
-    public String apkInstallFile;
+    public enum Mode {INSTALL, UNINSTALL, BUGREPORT}
+
+    public String mainArgument;
 
     public String adbPath;
     public String device;
@@ -15,14 +16,14 @@ public class Arg {
     public boolean debug = false;
     public boolean force = false;
 
+    public Mode mode;
+
     public Arg() {
     }
 
-    public Arg(String filterString, String apkInstallFile, String adbPath, String device, boolean dryRun, boolean skipEmulators, boolean keepData,
-               boolean quiet, boolean debug, boolean force) {
+    public Arg(String mainArgument, String adbPath, String device, boolean dryRun, boolean skipEmulators, boolean keepData, boolean quiet, boolean debug, boolean force, Mode mode) {
+        this.mainArgument = mainArgument;
         this.adbPath = adbPath;
-        this.apkInstallFile = apkInstallFile;
-        this.filterString = filterString;
         this.device = device;
         this.dryRun = dryRun;
         this.skipEmulators = skipEmulators;
@@ -30,6 +31,7 @@ public class Arg {
         this.quiet = quiet;
         this.debug = debug;
         this.force = force;
+        this.mode = mode;
     }
 
     @Override
@@ -45,18 +47,16 @@ public class Arg {
         if (quiet != arg.quiet) return false;
         if (debug != arg.debug) return false;
         if (force != arg.force) return false;
-        if (filterString != null ? !filterString.equals(arg.filterString) : arg.filterString != null) return false;
-        if (apkInstallFile != null ? !apkInstallFile.equals(arg.apkInstallFile) : arg.apkInstallFile != null)
-            return false;
+        if (mainArgument != null ? !mainArgument.equals(arg.mainArgument) : arg.mainArgument != null) return false;
         if (adbPath != null ? !adbPath.equals(arg.adbPath) : arg.adbPath != null) return false;
-        return device != null ? device.equals(arg.device) : arg.device == null;
+        if (device != null ? !device.equals(arg.device) : arg.device != null) return false;
+        return mode == arg.mode;
 
     }
 
     @Override
     public int hashCode() {
-        int result = filterString != null ? filterString.hashCode() : 0;
-        result = 31 * result + (apkInstallFile != null ? apkInstallFile.hashCode() : 0);
+        int result = mainArgument != null ? mainArgument.hashCode() : 0;
         result = 31 * result + (adbPath != null ? adbPath.hashCode() : 0);
         result = 31 * result + (device != null ? device.hashCode() : 0);
         result = 31 * result + (dryRun ? 1 : 0);
@@ -65,22 +65,7 @@ public class Arg {
         result = 31 * result + (quiet ? 1 : 0);
         result = 31 * result + (debug ? 1 : 0);
         result = 31 * result + (force ? 1 : 0);
+        result = 31 * result + (mode != null ? mode.hashCode() : 0);
         return result;
     }
-
-    @Override
-    public String toString() {
-        return "Arg{" +
-                "filterString='" + filterString + '\'' +
-                ", apkInstallFile='" + apkInstallFile + '\'' +
-                ", adbPath='" + adbPath + '\'' +
-                ", device='" + device + '\'' +
-                ", dryRun=" + dryRun +
-                ", skipEmulators=" + skipEmulators +
-                ", keepData=" + keepData +
-                ", quiet=" + quiet +
-                ", debug=" + debug +
-                ", force=" + force +
-                '}';
-    }
 }
