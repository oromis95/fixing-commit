@@ -4,9 +4,9 @@ import org.apache.commons.cli.*;
 
 public class CLIParser {
 
-    static final String ARG_UNINSTALL = "uninstall";
-    static final String ARG_INSTALL = "install";
-    static final String ARG_BUGREPORT = "bugreport";
+    static final String ARG_INSTALL = "i";
+    static final String ARG_UNINSTALL = "u";
+    static final String ARG_BUGREPORT = "b";
 
     static final String ARG_DEVICE_SERIAL = "s";
 
@@ -46,8 +46,12 @@ public class CLIParser {
             }
 
             if (commandLine.hasOption("reportDebugIntent")) {
-                String reportFilter = commandLine.getArgs()[0];
-                String reportIntent = commandLine.getArgs()[1];
+                String[] reportArgs = commandLine.getOptionValues("reportDebugIntent");
+
+                if (reportArgs.length < 2) {
+                    throw new IllegalArgumentException("must provide filter and intent argument eg. 'com.google.* \"-a intent.at\"");
+                }
+                argument.reportFilterIntent = reportArgs;
             }
 
             if (mainArgCount != 1) {
@@ -83,21 +87,21 @@ public class CLIParser {
     private static Options setupOptions() {
         Options options = new Options();
 
-        Option mainInstall = Option.builder(ARG_INSTALL).argName("apk file/folder").desc("Provide path to an apk file or folder containing apk files and the tool tries to install all of them to all connected devices (if not a specfic device is selected).").hasArg().build();
-        Option mainUninstall = Option.builder(ARG_UNINSTALL).argName("package name").hasArg(true).desc("Filter string that has to be a package name or part of it containing wildcards '*' for uninstalling. Can be multiple filter Strings comma separated. Example: 'com.android.*' or 'com.android.*,com.google.*'.").build();
-        Option mainBugReport = Option.builder(ARG_BUGREPORT).argName("package name").optionalArg(true).hasArg(true).desc("Filter string that has to be a package name or part of it containing wildcards '*' to match the apps to create the bug report for. Can be multiple filter Strings comma separated. Example: 'com.android.*' or 'com.android.*,com.google.*'.").build();
+        Option mainInstall = Option.builder(ARG_INSTALL).longOpt("install").argName("apk file/folder").desc("Provide path to an apk file or folder containing apk files and the tool tries to install all of them to all connected devices (if not a specfic device is selected).").hasArg().build();
+        Option mainUninstall = Option.builder(ARG_UNINSTALL).longOpt("uninstall").argName("package name").hasArg(true).desc("Filter string that has to be a package name or part of it containing wildcards '*' for uninstalling. Can be multiple filter Strings comma separated. Example: 'com.android.*' or 'com.android.*,com.google.*'.").build();
+        Option mainBugReport = Option.builder(ARG_BUGREPORT).longOpt("bugreport").argName("out folder").hasArg().optionalArg(true).desc("Creates a generic bug report (including eg. logcat and screenshot) from all connected devices and zips it to the folder given as arg. If no folder is given trys to zips it in the location of the .jar.").build();
 
-        Option adbPathOpt = Option.builder("adbPath").argName("path").hasArg(true).desc("Full path to adb executable. If this is omitted the tool tries to find adb in PATH env variable.").build();
-        Option deviceOpt = Option.builder(ARG_DEVICE_SERIAL).argName("device serial").hasArg(true).desc("If this is set, will only use given device. Default is all connected devices. Device id is the same that is given by 'adb devices'").build();
-        Option reportFilter = Option.builder("reportDebugIntent").argName("package and intent").numberOfArgs(2).hasArg(true).desc("").build();
+        Option adbPathOpt = Option.builder().longOpt("adbPath").argName("path").hasArg(true).desc("Full path to adb executable. If this is omitted the tool tries to find adb in PATH env variable.").build();
+        Option deviceOpt = Option.builder(ARG_DEVICE_SERIAL).longOpt("serial").argName("device serial").hasArg(true).desc("If this is set, will only use given device. Default is all connected devices. Device id is the same that is given by 'adb devices'").build();
+        Option reportFilter = Option.builder().longOpt("reportDebugIntent").argName("filter-package> <intent").hasArgs().valueSeparator(' ').desc("").build();
 
-        Option dryRunOpt = Option.builder("dryRun").hasArg(false).desc("Use this to see what would be installed/uninstalled on what devices with the given params. Will not install/uninstall anything.").build();
-        Option skipEmuOpt = Option.builder("skipEmulators").hasArg(false).desc("Skips device emulators for install/uninstall.").build();
-        Option keepDataOpt = Option.builder("keepData").hasArg(false).desc("Only for uninstall: Uses the '-k' param on 'adb uninstall' to keep data and caches of the app.").build();
-        Option upgradeOpt = Option.builder("upgrade").hasArg(false).desc("Only for install: Uses the '-r' param on 'adb install' for trying to reinstall the app and keeping its data.").build();
-        Option quietOpt = Option.builder("quiet").hasArg(false).desc("Prints less output.").build();
-        Option debugOpt = Option.builder("debug").hasArg(false).desc("Prints additional info for debugging.").build();
-        Option forceOpt = Option.builder("force").hasArg(false).desc("If this flag is set all matched apps will be installed/uninstalled without any further warning. Otherwise a user input is necessary.").build();
+        Option dryRunOpt = Option.builder().longOpt("dryRun").hasArg(false).desc("Use this to see what would be installed/uninstalled on what devices with the given params. Will not install/uninstall anything.").build();
+        Option skipEmuOpt = Option.builder().longOpt("skipEmulators").hasArg(false).desc("Skips device emulators for install/uninstall.").build();
+        Option keepDataOpt = Option.builder().longOpt("keepData").hasArg(false).desc("Only for uninstall: Uses the '-k' param on 'adb uninstall' to keep data and caches of the app.").build();
+        Option upgradeOpt = Option.builder().longOpt("upgrade").hasArg(false).desc("Only for install: Uses the '-r' param on 'adb install' for trying to reinstall the app and keeping its data.").build();
+        Option quietOpt = Option.builder().longOpt("quiet").hasArg(false).desc("Prints less output.").build();
+        Option debugOpt = Option.builder().longOpt("debug").hasArg(false).desc("Prints additional info for debugging.").build();
+        Option forceOpt = Option.builder().longOpt("force").hasArg(false).desc("If this flag is set all matched apps will be installed/uninstalled without any further warning. Otherwise a user input is necessary.").build();
 
         Option help = Option.builder("h").longOpt("help").desc("Prints docs").build();
         Option version = Option.builder("v").longOpt("version").desc("Prints current version.").build();
@@ -109,7 +113,7 @@ public class CLIParser {
         options.addOptionGroup(mainArgs);
 
         options.addOption(adbPathOpt).addOption(deviceOpt).addOption(dryRunOpt).addOption(skipEmuOpt).addOption(keepDataOpt)
-                .addOption(quietOpt).addOption(debugOpt).addOption(forceOpt).addOption(upgradeOpt);
+                .addOption(quietOpt).addOption(debugOpt).addOption(forceOpt).addOption(upgradeOpt).addOption(reportFilter);
 
         return options;
     }
@@ -118,6 +122,6 @@ public class CLIParser {
         HelpFormatter help = new HelpFormatter();
         help.setWidth(110);
         help.setLeftPadding(4);
-        help.printHelp("-" + ARG_INSTALL + " <apk file/folder> | -" + ARG_UNINSTALL + " <package filter> | -" + ARG_BUGREPORT + " <package filter> | --help", "Version: " + CLIParser.class.getPackage().getImplementationVersion(), options, "", false);
+        help.printHelp("-" + ARG_INSTALL + " <apk file/folder> | -" + ARG_UNINSTALL + " <package filter> | -" + ARG_BUGREPORT + " <out folder> | --help", "Version: " + CLIParser.class.getPackage().getImplementationVersion(), options, "", false);
     }
 }
