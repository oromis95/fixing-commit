@@ -87,6 +87,11 @@ public class ClassPathUtils
         }
     }
 
+    public static String getNullSafePackageName(final Class<?> clazz)
+    {
+        return clazz.getPackage() != null ? clazz.getPackage().getName() : "";
+    }
+
     /**
      * Get the actual classpath resource location for a class
      *
