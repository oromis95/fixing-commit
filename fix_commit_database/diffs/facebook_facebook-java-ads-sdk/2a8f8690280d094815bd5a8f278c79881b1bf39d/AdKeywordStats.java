@@ -24,70 +24,73 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AdKeywordStats extends APINode {
+  @SerializedName("actions")
+  private List<AdsActionStats> mActions = null;
+  @SerializedName("clicks")
+  private Long mClicks = null;
+  @SerializedName("cost_per_total_action")
+  private Double mCostPerTotalAction = null;
+  @SerializedName("cost_per_unique_click")
+  private Double mCostPerUniqueClick = null;
+  @SerializedName("cpc")
+  private Double mCpc = null;
+  @SerializedName("cpm")
+  private Double mCpm = null;
+  @SerializedName("cpp")
+  private Double mCpp = null;
+  @SerializedName("ctr")
+  private Double mCtr = null;
+  @SerializedName("frequency")
+  private Double mFrequency = null;
   @SerializedName("id")
   private String mId = null;
-  @SerializedName("name")
-  private String mName = null;
   @SerializedName("impressions")
   private Long mImpressions = null;
-  @SerializedName("unique_impressions")
-  private Long mUniqueImpressions = null;
+  @SerializedName("name")
+  private String mName = null;
   @SerializedName("reach")
   private Long mReach = null;
-  @SerializedName("clicks")
-  private Long mClicks = null;
-  @SerializedName("unique_clicks")
-  private Long mUniqueClicks = null;
+  @SerializedName("spend")
+  private Double mSpend = null;
   @SerializedName("total_actions")
   private Long mTotalActions = null;
   @SerializedName("total_unique_actions")
   private Long mTotalUniqueActions = null;
-  @SerializedName("actions")
-  private List<AdsActionStats> mActions = null;
   @SerializedName("unique_actions")
   private List<AdsActionStats> mUniqueActions = null;
-  @SerializedName("spend")
-  private Double mSpend = null;
-  @SerializedName("ctr")
-  private Double mCtr = null;
+  @SerializedName("unique_clicks")
+  private Long mUniqueClicks = null;
   @SerializedName("unique_ctr")
   private Double mUniqueCtr = null;
-  @SerializedName("cpm")
-  private Double mCpm = null;
-  @SerializedName("cpp")
-  private Double mCpp = null;
-  @SerializedName("cpc")
-  private Double mCpc = null;
-  @SerializedName("cost_per_total_action")
-  private Double mCostPerTotalAction = null;
-  @SerializedName("cost_per_unique_click")
-  private Double mCostPerUniqueClick = null;
-  @SerializedName("frequency")
-  private Double mFrequency = null;
+  @SerializedName("unique_impressions")
+  private Long mUniqueImpressions = null;
   protected static Gson gson = null;
 
   public AdKeywordStats() {
@@ -105,22 +108,23 @@ public class AdKeywordStats extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    adKeywordStats.mContext = context;
+    adKeywordStats.context = context;
     adKeywordStats.rawValue = json;
     return adKeywordStats;
   }
 
-  public static APINodeList<AdKeywordStats> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AdKeywordStats> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AdKeywordStats> adKeywordStatss = new APINodeList<AdKeywordStats>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -133,10 +137,11 @@ public class AdKeywordStats extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            adKeywordStatss.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            adKeywordStatss.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -147,7 +152,20 @@ public class AdKeywordStats extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            adKeywordStatss.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  adKeywordStatss.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              adKeywordStatss.add(loadJSON(obj.toString(), context));
+            }
           }
           return adKeywordStatss;
         } else if (obj.has("images")) {
@@ -158,24 +176,54 @@ public class AdKeywordStats extends APINode {
           }
           return adKeywordStatss;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              adKeywordStatss.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return adKeywordStatss;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          adKeywordStatss.clear();
           adKeywordStatss.add(loadJSON(json, context));
           return adKeywordStatss;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -184,193 +232,193 @@ public class AdKeywordStats extends APINode {
   }
 
 
-  public String getFieldId() {
-    return mId;
+  public List<AdsActionStats> getFieldActions() {
+    return mActions;
   }
 
-  public AdKeywordStats setFieldId(String value) {
-    this.mId = value;
+  public AdKeywordStats setFieldActions(List<AdsActionStats> value) {
+    this.mActions = value;
     return this;
   }
 
-  public String getFieldName() {
-    return mName;
+  public AdKeywordStats setFieldActions(String value) {
+    Type type = new TypeToken<List<AdsActionStats>>(){}.getType();
+    this.mActions = AdsActionStats.getGson().fromJson(value, type);
+    return this;
+  }
+  public Long getFieldClicks() {
+    return mClicks;
   }
 
-  public AdKeywordStats setFieldName(String value) {
-    this.mName = value;
+  public AdKeywordStats setFieldClicks(Long value) {
+    this.mClicks = value;
     return this;
   }
 
-  public Long getFieldImpressions() {
-    return mImpressions;
+  public Double getFieldCostPerTotalAction() {
+    return mCostPerTotalAction;
   }
 
-  public AdKeywordStats setFieldImpressions(Long value) {
-    this.mImpressions = value;
+  public AdKeywordStats setFieldCostPerTotalAction(Double value) {
+    this.mCostPerTotalAction = value;
     return this;
   }
 
-  public Long getFieldUniqueImpressions() {
-    return mUniqueImpressions;
+  public Double getFieldCostPerUniqueClick() {
+    return mCostPerUniqueClick;
   }
 
-  public AdKeywordStats setFieldUniqueImpressions(Long value) {
-    this.mUniqueImpressions = value;
+  public AdKeywordStats setFieldCostPerUniqueClick(Double value) {
+    this.mCostPerUniqueClick = value;
     return this;
   }
 
-  public Long getFieldReach() {
-    return mReach;
+  public Double getFieldCpc() {
+    return mCpc;
   }
 
-  public AdKeywordStats setFieldReach(Long value) {
-    this.mReach = value;
+  public AdKeywordStats setFieldCpc(Double value) {
+    this.mCpc = value;
     return this;
   }
 
-  public Long getFieldClicks() {
-    return mClicks;
+  public Double getFieldCpm() {
+    return mCpm;
   }
 
-  public AdKeywordStats setFieldClicks(Long value) {
-    this.mClicks = value;
+  public AdKeywordStats setFieldCpm(Double value) {
+    this.mCpm = value;
     return this;
   }
 
-  public Long getFieldUniqueClicks() {
-    return mUniqueClicks;
+  public Double getFieldCpp() {
+    return mCpp;
   }
 
-  public AdKeywordStats setFieldUniqueClicks(Long value) {
-    this.mUniqueClicks = value;
+  public AdKeywordStats setFieldCpp(Double value) {
+    this.mCpp = value;
     return this;
   }
 
-  public Long getFieldTotalActions() {
-    return mTotalActions;
+  public Double getFieldCtr() {
+    return mCtr;
   }
 
-  public AdKeywordStats setFieldTotalActions(Long value) {
-    this.mTotalActions = value;
+  public AdKeywordStats setFieldCtr(Double value) {
+    this.mCtr = value;
     return this;
   }
 
-  public Long getFieldTotalUniqueActions() {
-    return mTotalUniqueActions;
+  public Double getFieldFrequency() {
+    return mFrequency;
   }
 
-  public AdKeywordStats setFieldTotalUniqueActions(Long value) {
-    this.mTotalUniqueActions = value;
+  public AdKeywordStats setFieldFrequency(Double value) {
+    this.mFrequency = value;
     return this;
   }
 
-  public List<AdsActionStats> getFieldActions() {
-    return mActions;
+  public String getFieldId() {
+    return mId;
   }
 
-  public AdKeywordStats setFieldActions(List<AdsActionStats> value) {
-    this.mActions = value;
+  public AdKeywordStats setFieldId(String value) {
+    this.mId = value;
     return this;
   }
 
-  public AdKeywordStats setFieldActions(String value) {
-    Type type = new TypeToken<List<AdsActionStats>>(){}.getType();
-    this.mActions = AdsActionStats.getGson().fromJson(value, type);
-    return this;
-  }
-  public List<AdsActionStats> getFieldUniqueActions() {
-    return mUniqueActions;
+  public Long getFieldImpressions() {
+    return mImpressions;
   }
 
-  public AdKeywordStats setFieldUniqueActions(List<AdsActionStats> value) {
-    this.mUniqueActions = value;
+  public AdKeywordStats setFieldImpressions(Long value) {
+    this.mImpressions = value;
     return this;
   }
 
-  public AdKeywordStats setFieldUniqueActions(String value) {
-    Type type = new TypeToken<List<AdsActionStats>>(){}.getType();
-    this.mUniqueActions = AdsActionStats.getGson().fromJson(value, type);
-    return this;
-  }
-  public Double getFieldSpend() {
-    return mSpend;
+  public String getFieldName() {
+    return mName;
   }
 
-  public AdKeywordStats setFieldSpend(Double value) {
-    this.mSpend = value;
+  public AdKeywordStats setFieldName(String value) {
+    this.mName = value;
     return this;
   }
 
-  public Double getFieldCtr() {
-    return mCtr;
+  public Long getFieldReach() {
+    return mReach;
   }
 
-  public AdKeywordStats setFieldCtr(Double value) {
-    this.mCtr = value;
+  public AdKeywordStats setFieldReach(Long value) {
+    this.mReach = value;
     return this;
   }
 
-  public Double getFieldUniqueCtr() {
-    return mUniqueCtr;
+  public Double getFieldSpend() {
+    return mSpend;
   }
 
-  public AdKeywordStats setFieldUniqueCtr(Double value) {
-    this.mUniqueCtr = value;
+  public AdKeywordStats setFieldSpend(Double value) {
+    this.mSpend = value;
     return this;
   }
 
-  public Double getFieldCpm() {
-    return mCpm;
+  public Long getFieldTotalActions() {
+    return mTotalActions;
   }
 
-  public AdKeywordStats setFieldCpm(Double value) {
-    this.mCpm = value;
+  public AdKeywordStats setFieldTotalActions(Long value) {
+    this.mTotalActions = value;
     return this;
   }
 
-  public Double getFieldCpp() {
-    return mCpp;
+  public Long getFieldTotalUniqueActions() {
+    return mTotalUniqueActions;
   }
 
-  public AdKeywordStats setFieldCpp(Double value) {
-    this.mCpp = value;
+  public AdKeywordStats setFieldTotalUniqueActions(Long value) {
+    this.mTotalUniqueActions = value;
     return this;
   }
 
-  public Double getFieldCpc() {
-    return mCpc;
+  public List<AdsActionStats> getFieldUniqueActions() {
+    return mUniqueActions;
   }
 
-  public AdKeywordStats setFieldCpc(Double value) {
-    this.mCpc = value;
+  public AdKeywordStats setFieldUniqueActions(List<AdsActionStats> value) {
+    this.mUniqueActions = value;
     return this;
   }
 
-  public Double getFieldCostPerTotalAction() {
-    return mCostPerTotalAction;
+  public AdKeywordStats setFieldUniqueActions(String value) {
+    Type type = new TypeToken<List<AdsActionStats>>(){}.getType();
+    this.mUniqueActions = AdsActionStats.getGson().fromJson(value, type);
+    return this;
+  }
+  public Long getFieldUniqueClicks() {
+    return mUniqueClicks;
   }
 
-  public AdKeywordStats setFieldCostPerTotalAction(Double value) {
-    this.mCostPerTotalAction = value;
+  public AdKeywordStats setFieldUniqueClicks(Long value) {
+    this.mUniqueClicks = value;
     return this;
   }
 
-  public Double getFieldCostPerUniqueClick() {
-    return mCostPerUniqueClick;
+  public Double getFieldUniqueCtr() {
+    return mUniqueCtr;
   }
 
-  public AdKeywordStats setFieldCostPerUniqueClick(Double value) {
-    this.mCostPerUniqueClick = value;
+  public AdKeywordStats setFieldUniqueCtr(Double value) {
+    this.mUniqueCtr = value;
     return this;
   }
 
-  public Double getFieldFrequency() {
-    return mFrequency;
+  public Long getFieldUniqueImpressions() {
+    return mUniqueImpressions;
   }
 
-  public AdKeywordStats setFieldFrequency(Double value) {
-    this.mFrequency = value;
+  public AdKeywordStats setFieldUniqueImpressions(Long value) {
+    this.mUniqueImpressions = value;
     return this;
   }
 
@@ -391,34 +439,34 @@ public class AdKeywordStats extends APINode {
   }
 
   public AdKeywordStats copyFrom(AdKeywordStats instance) {
+    this.mActions = instance.mActions;
+    this.mClicks = instance.mClicks;
+    this.mCostPerTotalAction = instance.mCostPerTotalAction;
+    this.mCostPerUniqueClick = instance.mCostPerUniqueClick;
+    this.mCpc = instance.mCpc;
+    this.mCpm = instance.mCpm;
+    this.mCpp = instance.mCpp;
+    this.mCtr = instance.mCtr;
+    this.mFrequency = instance.mFrequency;
     this.mId = instance.mId;
-    this.mName = instance.mName;
     this.mImpressions = instance.mImpressions;
-    this.mUniqueImpressions = instance.mUniqueImpressions;
+    this.mName = instance.mName;
     this.mReach = instance.mReach;
-    this.mClicks = instance.mClicks;
-    this.mUniqueClicks = instance.mUniqueClicks;
+    this.mSpend = instance.mSpend;
     this.mTotalActions = instance.mTotalActions;
     this.mTotalUniqueActions = instance.mTotalUniqueActions;
-    this.mActions = instance.mActions;
     this.mUniqueActions = instance.mUniqueActions;
-    this.mSpend = instance.mSpend;
-    this.mCtr = instance.mCtr;
+    this.mUniqueClicks = instance.mUniqueClicks;
     this.mUniqueCtr = instance.mUniqueCtr;
-    this.mCpm = instance.mCpm;
-    this.mCpp = instance.mCpp;
-    this.mCpc = instance.mCpc;
-    this.mCostPerTotalAction = instance.mCostPerTotalAction;
-    this.mCostPerUniqueClick = instance.mCostPerUniqueClick;
-    this.mFrequency = instance.mFrequency;
-    this.mContext = instance.mContext;
+    this.mUniqueImpressions = instance.mUniqueImpressions;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AdKeywordStats> getParser() {
     return new APIRequest.ResponseParser<AdKeywordStats>() {
-      public APINodeList<AdKeywordStats> parseResponse(String response, APIContext context, APIRequest<AdKeywordStats> request) {
+      public APINodeList<AdKeywordStats> parseResponse(String response, APIContext context, APIRequest<AdKeywordStats> request) throws MalformedResponseException {
         return AdKeywordStats.parseResponse(response, context, request);
       }
     };
