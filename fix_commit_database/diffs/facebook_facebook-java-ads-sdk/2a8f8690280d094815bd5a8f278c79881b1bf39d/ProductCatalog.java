@@ -24,36 +24,39 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class ProductCatalog extends APINode {
-  @SerializedName("id")
-  private String mId = null;
   @SerializedName("business")
   private Business mBusiness = null;
   @SerializedName("feed_count")
   private Long mFeedCount = null;
+  @SerializedName("id")
+  private String mId = null;
   @SerializedName("name")
   private String mName = null;
   @SerializedName("product_count")
@@ -69,11 +72,11 @@ public class ProductCatalog extends APINode {
 
   public ProductCatalog(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public ProductCatalog fetch() throws APIException{
-    ProductCatalog newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    ProductCatalog newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -90,8 +93,17 @@ public class ProductCatalog extends APINode {
     return productCatalog;
   }
 
+  public static APINodeList<ProductCatalog> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<ProductCatalog>)(
+      new APIRequest<ProductCatalog>(context, "", "/", "GET", ProductCatalog.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -106,22 +118,23 @@ public class ProductCatalog extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    productCatalog.mContext = context;
+    productCatalog.context = context;
     productCatalog.rawValue = json;
     return productCatalog;
   }
 
-  public static APINodeList<ProductCatalog> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<ProductCatalog> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<ProductCatalog> productCatalogs = new APINodeList<ProductCatalog>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -134,10 +147,11 @@ public class ProductCatalog extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            productCatalogs.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            productCatalogs.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -148,7 +162,20 @@ public class ProductCatalog extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            productCatalogs.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  productCatalogs.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              productCatalogs.add(loadJSON(obj.toString(), context));
+            }
           }
           return productCatalogs;
         } else if (obj.has("images")) {
@@ -159,24 +186,54 @@ public class ProductCatalog extends APINode {
           }
           return productCatalogs;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              productCatalogs.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return productCatalogs;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          productCatalogs.clear();
           productCatalogs.add(loadJSON(json, context));
           return productCatalogs;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -184,70 +241,70 @@ public class ProductCatalog extends APINode {
     return getGson().toJson(this);
   }
 
-  public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetAgencies getAgencies() {
+    return new APIRequestGetAgencies(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestUpdate update() {
-    return new APIRequestUpdate(this.getPrefixedId().toString(), mContext);
+  public APIRequestDeleteExternalEventSources deleteExternalEventSources() {
+    return new APIRequestDeleteExternalEventSources(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestDelete delete() {
-    return new APIRequestDelete(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetExternalEventSources getExternalEventSources() {
+    return new APIRequestGetExternalEventSources(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetAgencies getAgencies() {
-    return new APIRequestGetAgencies(this.getPrefixedId().toString(), mContext);
+  public APIRequestCreateExternalEventSource createExternalEventSource() {
+    return new APIRequestCreateExternalEventSource(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetExternalEventSources getExternalEventSources() {
-    return new APIRequestGetExternalEventSources(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetProductFeeds getProductFeeds() {
+    return new APIRequestGetProductFeeds(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetProducts getProducts() {
-    return new APIRequestGetProducts(this.getPrefixedId().toString(), mContext);
+  public APIRequestCreateProductFeed createProductFeed() {
+    return new APIRequestCreateProductFeed(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetProductGroups getProductGroups() {
-    return new APIRequestGetProductGroups(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetProductGroups(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetProductFeeds getProductFeeds() {
-    return new APIRequestGetProductFeeds(this.getPrefixedId().toString(), mContext);
+  public APIRequestCreateProductGroup createProductGroup() {
+    return new APIRequestCreateProductGroup(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetProductSets getProductSets() {
-    return new APIRequestGetProductSets(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetProductSets(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestCreateExternalEventSource createExternalEventSource() {
-    return new APIRequestCreateExternalEventSource(this.getPrefixedId().toString(), mContext);
+  public APIRequestCreateProductSet createProductSet() {
+    return new APIRequestCreateProductSet(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestCreateProductGroup createProductGroup() {
-    return new APIRequestCreateProductGroup(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetProducts getProducts() {
+    return new APIRequestGetProducts(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestCreateProductFeed createProductFeed() {
-    return new APIRequestCreateProductFeed(this.getPrefixedId().toString(), mContext);
+  public APIRequestCreateProduct createProduct() {
+    return new APIRequestCreateProduct(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestCreateProductSet createProductSet() {
-    return new APIRequestCreateProductSet(this.getPrefixedId().toString(), mContext);
+  public APIRequestDelete delete() {
+    return new APIRequestDelete(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestDeleteExternalEventSources deleteExternalEventSources() {
-    return new APIRequestDeleteExternalEventSources(this.getPrefixedId().toString(), mContext);
+  public APIRequestGet get() {
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
   }
 
-
-  public String getFieldId() {
-    return mId;
+  public APIRequestUpdate update() {
+    return new APIRequestUpdate(this.getPrefixedId().toString(), context);
   }
 
+
   public Business getFieldBusiness() {
     if (mBusiness != null) {
-      mBusiness.mContext = getContext();
+      mBusiness.context = getContext();
     }
     return mBusiness;
   }
@@ -256,6 +313,10 @@ public class ProductCatalog extends APINode {
     return mFeedCount;
   }
 
+  public String getFieldId() {
+    return mId;
+  }
+
   public String getFieldName() {
     return mName;
   }
@@ -266,11 +327,11 @@ public class ProductCatalog extends APINode {
 
 
 
-  public static class APIRequestGet extends APIRequest<ProductCatalog> {
+  public static class APIRequestGetAgencies extends APIRequest<Business> {
 
-    ProductCatalog lastResponse = null;
+    APINodeList<Business> lastResponse = null;
     @Override
-    public ProductCatalog getLastResponse() {
+    public APINodeList<Business> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
@@ -278,1876 +339,2408 @@ public class ProductCatalog extends APINode {
 
     public static final String[] FIELDS = {
       "id",
-      "business",
-      "feed_count",
       "name",
-      "product_count",
+      "primary_page",
     };
 
     @Override
-    public ProductCatalog parseResponse(String response) throws APIException {
-      return ProductCatalog.parseResponse(response, getContext(), this).head();
+    public APINodeList<Business> parseResponse(String response) throws APIException {
+      return Business.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public ProductCatalog execute() throws APIException {
+    public APINodeList<Business> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public ProductCatalog execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<Business> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGet(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetAgencies(String nodeId, APIContext context) {
+      super(context, nodeId, "/agencies", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGet setParam(String param, Object value) {
+    @Override
+    public APIRequestGetAgencies setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGet setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetAgencies setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGet requestAllFields () {
+    public APIRequestGetAgencies requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGet requestAllFields (boolean value) {
+    public APIRequestGetAgencies requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetAgencies requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGet requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetAgencies requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestField (String field) {
+    @Override
+    public APIRequestGetAgencies requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGet requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetAgencies requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGet requestIdField () {
+    public APIRequestGetAgencies requestIdField () {
       return this.requestIdField(true);
     }
-    public APIRequestGet requestIdField (boolean value) {
+    public APIRequestGetAgencies requestIdField (boolean value) {
       this.requestField("id", value);
       return this;
     }
-    public APIRequestGet requestBusinessField () {
-      return this.requestBusinessField(true);
-    }
-    public APIRequestGet requestBusinessField (boolean value) {
-      this.requestField("business", value);
-      return this;
-    }
-    public APIRequestGet requestFeedCountField () {
-      return this.requestFeedCountField(true);
-    }
-    public APIRequestGet requestFeedCountField (boolean value) {
-      this.requestField("feed_count", value);
-      return this;
-    }
-    public APIRequestGet requestNameField () {
+    public APIRequestGetAgencies requestNameField () {
       return this.requestNameField(true);
     }
-    public APIRequestGet requestNameField (boolean value) {
+    public APIRequestGetAgencies requestNameField (boolean value) {
       this.requestField("name", value);
       return this;
     }
-    public APIRequestGet requestProductCountField () {
-      return this.requestProductCountField(true);
+    public APIRequestGetAgencies requestPrimaryPageField () {
+      return this.requestPrimaryPageField(true);
     }
-    public APIRequestGet requestProductCountField (boolean value) {
-      this.requestField("product_count", value);
+    public APIRequestGetAgencies requestPrimaryPageField (boolean value) {
+      this.requestField("primary_page", value);
       return this;
     }
-
   }
 
-  public static class APIRequestUpdate extends APIRequest<APINode> {
+  public static class APIRequestDeleteExternalEventSources extends APIRequest<ExternalEventSource> {
 
-    APINode lastResponse = null;
+    APINodeList<ExternalEventSource> lastResponse = null;
     @Override
-    public APINode getLastResponse() {
+    public APINodeList<ExternalEventSource> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
+      "external_event_sources",
       "id",
-      "name",
     };
 
     public static final String[] FIELDS = {
     };
 
     @Override
-    public APINode parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this).head();
+    public APINodeList<ExternalEventSource> parseResponse(String response) throws APIException {
+      return ExternalEventSource.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINode execute() throws APIException {
+    public APINodeList<ExternalEventSource> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<ExternalEventSource> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestUpdate(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "POST", Arrays.asList(PARAMS));
+    public APIRequestDeleteExternalEventSources(String nodeId, APIContext context) {
+      super(context, nodeId, "/external_event_sources", "DELETE", Arrays.asList(PARAMS));
     }
 
-    public APIRequestUpdate setParam(String param, Object value) {
+    @Override
+    public APIRequestDeleteExternalEventSources setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestUpdate setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestDeleteExternalEventSources setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestUpdate setId (String id) {
-      this.setParam("id", id);
+    public APIRequestDeleteExternalEventSources setExternalEventSources (List<String> externalEventSources) {
+      this.setParam("external_event_sources", externalEventSources);
       return this;
     }
-
-
-    public APIRequestUpdate setName (String name) {
-      this.setParam("name", name);
+    public APIRequestDeleteExternalEventSources setExternalEventSources (String externalEventSources) {
+      this.setParam("external_event_sources", externalEventSources);
       return this;
     }
 
+    public APIRequestDeleteExternalEventSources setId (String id) {
+      this.setParam("id", id);
+      return this;
+    }
 
-    public APIRequestUpdate requestAllFields () {
+    public APIRequestDeleteExternalEventSources requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestUpdate requestAllFields (boolean value) {
+    public APIRequestDeleteExternalEventSources requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestUpdate requestFields (List<String> fields) {
+    @Override
+    public APIRequestDeleteExternalEventSources requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestUpdate requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestDeleteExternalEventSources requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestUpdate requestField (String field) {
+    @Override
+    public APIRequestDeleteExternalEventSources requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestUpdate requestField (String field, boolean value) {
+    @Override
+    public APIRequestDeleteExternalEventSources requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
   }
 
-  public static class APIRequestDelete extends APIRequest<APINode> {
+  public static class APIRequestGetExternalEventSources extends APIRequest<ExternalEventSource> {
 
-    APINode lastResponse = null;
+    APINodeList<ExternalEventSource> lastResponse = null;
     @Override
-    public APINode getLastResponse() {
+    public APINodeList<ExternalEventSource> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "id",
     };
 
     public static final String[] FIELDS = {
+      "id",
+      "source_type",
     };
 
     @Override
-    public APINode parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this).head();
+    public APINodeList<ExternalEventSource> parseResponse(String response) throws APIException {
+      return ExternalEventSource.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINode execute() throws APIException {
+    public APINodeList<ExternalEventSource> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<ExternalEventSource> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestDelete(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "DELETE", Arrays.asList(PARAMS));
+    public APIRequestGetExternalEventSources(String nodeId, APIContext context) {
+      super(context, nodeId, "/external_event_sources", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestDelete setParam(String param, Object value) {
+    @Override
+    public APIRequestGetExternalEventSources setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestDelete setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetExternalEventSources setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestDelete setId (String id) {
-      this.setParam("id", id);
-      return this;
-    }
-
-
-    public APIRequestDelete requestAllFields () {
+    public APIRequestGetExternalEventSources requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestDelete requestAllFields (boolean value) {
+    public APIRequestGetExternalEventSources requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestDelete requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetExternalEventSources requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestDelete requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetExternalEventSources requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestDelete requestField (String field) {
+    @Override
+    public APIRequestGetExternalEventSources requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestDelete requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetExternalEventSources requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
+    public APIRequestGetExternalEventSources requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGetExternalEventSources requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
+    public APIRequestGetExternalEventSources requestSourceTypeField () {
+      return this.requestSourceTypeField(true);
+    }
+    public APIRequestGetExternalEventSources requestSourceTypeField (boolean value) {
+      this.requestField("source_type", value);
+      return this;
+    }
   }
 
-  public static class APIRequestGetAgencies extends APIRequest<Business> {
+  public static class APIRequestCreateExternalEventSource extends APIRequest<ExternalEventSource> {
 
-    APINodeList<Business> lastResponse = null;
+    ExternalEventSource lastResponse = null;
     @Override
-    public APINodeList<Business> getLastResponse() {
+    public ExternalEventSource getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
+      "external_event_sources",
+      "id",
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "name",
-      "primary_page",
     };
 
     @Override
-    public APINodeList<Business> parseResponse(String response) throws APIException {
-      return Business.parseResponse(response, getContext(), this);
+    public ExternalEventSource parseResponse(String response) throws APIException {
+      return ExternalEventSource.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public APINodeList<Business> execute() throws APIException {
+    public ExternalEventSource execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<Business> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public ExternalEventSource execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetAgencies(String nodeId, APIContext context) {
-      super(context, nodeId, "/agencies", "GET", Arrays.asList(PARAMS));
+    public APIRequestCreateExternalEventSource(String nodeId, APIContext context) {
+      super(context, nodeId, "/external_event_sources", "POST", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetAgencies setParam(String param, Object value) {
+    @Override
+    public APIRequestCreateExternalEventSource setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetAgencies setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestCreateExternalEventSource setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetAgencies requestAllFields () {
+    public APIRequestCreateExternalEventSource setExternalEventSources (List<String> externalEventSources) {
+      this.setParam("external_event_sources", externalEventSources);
+      return this;
+    }
+    public APIRequestCreateExternalEventSource setExternalEventSources (String externalEventSources) {
+      this.setParam("external_event_sources", externalEventSources);
+      return this;
+    }
+
+    public APIRequestCreateExternalEventSource setId (String id) {
+      this.setParam("id", id);
+      return this;
+    }
+
+    public APIRequestCreateExternalEventSource requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetAgencies requestAllFields (boolean value) {
+    public APIRequestCreateExternalEventSource requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetAgencies requestFields (List<String> fields) {
+    @Override
+    public APIRequestCreateExternalEventSource requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetAgencies requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestCreateExternalEventSource requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetAgencies requestField (String field) {
+    @Override
+    public APIRequestCreateExternalEventSource requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetAgencies requestField (String field, boolean value) {
+    @Override
+    public APIRequestCreateExternalEventSource requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetAgencies requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGetAgencies requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
-    }
-    public APIRequestGetAgencies requestNameField () {
-      return this.requestNameField(true);
-    }
-    public APIRequestGetAgencies requestNameField (boolean value) {
-      this.requestField("name", value);
-      return this;
-    }
-    public APIRequestGetAgencies requestPrimaryPageField () {
-      return this.requestPrimaryPageField(true);
-    }
-    public APIRequestGetAgencies requestPrimaryPageField (boolean value) {
-      this.requestField("primary_page", value);
-      return this;
-    }
-
   }
 
-  public static class APIRequestGetExternalEventSources extends APIRequest<ExternalEventSource> {
+  public static class APIRequestGetProductFeeds extends APIRequest<ProductFeed> {
 
-    APINodeList<ExternalEventSource> lastResponse = null;
+    APINodeList<ProductFeed> lastResponse = null;
     @Override
-    public APINodeList<ExternalEventSource> getLastResponse() {
+    public APINodeList<ProductFeed> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
     };
 
     public static final String[] FIELDS = {
+      "country",
+      "created_time",
+      "default_currency",
+      "deletion_enabled",
+      "delimiter",
+      "encoding",
+      "file_name",
       "id",
-      "source_type",
+      "latest_upload",
+      "name",
+      "product_count",
+      "quoted_fields_mode",
+      "schedule",
     };
 
     @Override
-    public APINodeList<ExternalEventSource> parseResponse(String response) throws APIException {
-      return ExternalEventSource.parseResponse(response, getContext(), this);
+    public APINodeList<ProductFeed> parseResponse(String response) throws APIException {
+      return ProductFeed.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINodeList<ExternalEventSource> execute() throws APIException {
+    public APINodeList<ProductFeed> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<ExternalEventSource> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<ProductFeed> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetExternalEventSources(String nodeId, APIContext context) {
-      super(context, nodeId, "/external_event_sources", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetProductFeeds(String nodeId, APIContext context) {
+      super(context, nodeId, "/product_feeds", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetExternalEventSources setParam(String param, Object value) {
+    @Override
+    public APIRequestGetProductFeeds setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetExternalEventSources setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetProductFeeds setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetExternalEventSources requestAllFields () {
+    public APIRequestGetProductFeeds requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetExternalEventSources requestAllFields (boolean value) {
+    public APIRequestGetProductFeeds requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetExternalEventSources requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetProductFeeds requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetExternalEventSources requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetProductFeeds requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetExternalEventSources requestField (String field) {
+    @Override
+    public APIRequestGetProductFeeds requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetExternalEventSources requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetProductFeeds requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetExternalEventSources requestIdField () {
-      return this.requestIdField(true);
+    public APIRequestGetProductFeeds requestCountryField () {
+      return this.requestCountryField(true);
     }
-    public APIRequestGetExternalEventSources requestIdField (boolean value) {
-      this.requestField("id", value);
+    public APIRequestGetProductFeeds requestCountryField (boolean value) {
+      this.requestField("country", value);
       return this;
     }
-    public APIRequestGetExternalEventSources requestSourceTypeField () {
-      return this.requestSourceTypeField(true);
+    public APIRequestGetProductFeeds requestCreatedTimeField () {
+      return this.requestCreatedTimeField(true);
     }
-    public APIRequestGetExternalEventSources requestSourceTypeField (boolean value) {
-      this.requestField("source_type", value);
+    public APIRequestGetProductFeeds requestCreatedTimeField (boolean value) {
+      this.requestField("created_time", value);
       return this;
     }
-
-  }
-
-  public static class APIRequestGetProducts extends APIRequest<ProductItem> {
-
-    APINodeList<ProductItem> lastResponse = null;
-    @Override
-    public APINodeList<ProductItem> getLastResponse() {
-      return lastResponse;
+    public APIRequestGetProductFeeds requestDefaultCurrencyField () {
+      return this.requestDefaultCurrencyField(true);
     }
-    public static final String[] PARAMS = {
-      "filter",
-    };
-
-    public static final String[] FIELDS = {
-      "id",
-      "additional_image_urls",
-      "applinks",
-      "age_group",
-      "availability",
-      "brand",
-      "category",
-      "color",
-      "commerce_insights",
-      "condition",
-      "custom_data",
-      "description",
-      "expiration_date",
-      "gender",
-      "gtin",
-      "image_url",
-      "material",
-      "manufacturer_part_number",
-      "name",
-      "ordering_index",
-      "pattern",
-      "price",
-      "product_type",
-      "retailer_id",
-      "retailer_product_group_id",
-      "review_rejection_reasons",
-      "review_status",
-      "sale_price",
-      "sale_price_start_date",
-      "sale_price_end_date",
-      "shipping_weight_value",
-      "shipping_weight_unit",
-      "size",
-      "start_date",
-      "url",
-      "visibility",
-      "product_feed",
-    };
-
-    @Override
-    public APINodeList<ProductItem> parseResponse(String response) throws APIException {
-      return ProductItem.parseResponse(response, getContext(), this);
+    public APIRequestGetProductFeeds requestDefaultCurrencyField (boolean value) {
+      this.requestField("default_currency", value);
+      return this;
     }
-
-    @Override
-    public APINodeList<ProductItem> execute() throws APIException {
-      return execute(new HashMap<String, Object>());
+    public APIRequestGetProductFeeds requestDeletionEnabledField () {
+      return this.requestDeletionEnabledField(true);
     }
-
-    @Override
-    public APINodeList<ProductItem> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
+    public APIRequestGetProductFeeds requestDeletionEnabledField (boolean value) {
+      this.requestField("deletion_enabled", value);
+      return this;
     }
-
-    public APIRequestGetProducts(String nodeId, APIContext context) {
-      super(context, nodeId, "/products", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetProductFeeds requestDelimiterField () {
+      return this.requestDelimiterField(true);
     }
-
-    public APIRequestGetProducts setParam(String param, Object value) {
-      setParamInternal(param, value);
+    public APIRequestGetProductFeeds requestDelimiterField (boolean value) {
+      this.requestField("delimiter", value);
       return this;
     }
-
-    public APIRequestGetProducts setParams(Map<String, Object> params) {
-      setParamsInternal(params);
+    public APIRequestGetProductFeeds requestEncodingField () {
+      return this.requestEncodingField(true);
+    }
+    public APIRequestGetProductFeeds requestEncodingField (boolean value) {
+      this.requestField("encoding", value);
       return this;
     }
-
-
-    public APIRequestGetProducts setFilter (String filter) {
-      this.setParam("filter", filter);
+    public APIRequestGetProductFeeds requestFileNameField () {
+      return this.requestFileNameField(true);
+    }
+    public APIRequestGetProductFeeds requestFileNameField (boolean value) {
+      this.requestField("file_name", value);
       return this;
     }
-
-
-    public APIRequestGetProducts requestAllFields () {
-      return this.requestAllFields(true);
+    public APIRequestGetProductFeeds requestIdField () {
+      return this.requestIdField(true);
     }
-
-    public APIRequestGetProducts requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
+    public APIRequestGetProductFeeds requestIdField (boolean value) {
+      this.requestField("id", value);
       return this;
     }
-
-    public APIRequestGetProducts requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
+    public APIRequestGetProductFeeds requestLatestUploadField () {
+      return this.requestLatestUploadField(true);
     }
-
-    public APIRequestGetProducts requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+    public APIRequestGetProductFeeds requestLatestUploadField (boolean value) {
+      this.requestField("latest_upload", value);
       return this;
     }
+    public APIRequestGetProductFeeds requestNameField () {
+      return this.requestNameField(true);
+    }
+    public APIRequestGetProductFeeds requestNameField (boolean value) {
+      this.requestField("name", value);
+      return this;
+    }
+    public APIRequestGetProductFeeds requestProductCountField () {
+      return this.requestProductCountField(true);
+    }
+    public APIRequestGetProductFeeds requestProductCountField (boolean value) {
+      this.requestField("product_count", value);
+      return this;
+    }
+    public APIRequestGetProductFeeds requestQuotedFieldsModeField () {
+      return this.requestQuotedFieldsModeField(true);
+    }
+    public APIRequestGetProductFeeds requestQuotedFieldsModeField (boolean value) {
+      this.requestField("quoted_fields_mode", value);
+      return this;
+    }
+    public APIRequestGetProductFeeds requestScheduleField () {
+      return this.requestScheduleField(true);
+    }
+    public APIRequestGetProductFeeds requestScheduleField (boolean value) {
+      this.requestField("schedule", value);
+      return this;
+    }
+  }
 
-    public APIRequestGetProducts requestField (String field) {
+  public static class APIRequestCreateProductFeed extends APIRequest<ProductFeed> {
+
+    ProductFeed lastResponse = null;
+    @Override
+    public ProductFeed getLastResponse() {
+      return lastResponse;
+    }
+    public static final String[] PARAMS = {
+      "country",
+      "default_currency",
+      "deletion_enabled",
+      "delimiter",
+      "encoding",
+      "file_name",
+      "id",
+      "name",
+      "quoted_fields_mode",
+      "schedule",
+    };
+
+    public static final String[] FIELDS = {
+    };
+
+    @Override
+    public ProductFeed parseResponse(String response) throws APIException {
+      return ProductFeed.parseResponse(response, getContext(), this).head();
+    }
+
+    @Override
+    public ProductFeed execute() throws APIException {
+      return execute(new HashMap<String, Object>());
+    }
+
+    @Override
+    public ProductFeed execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
+    }
+
+    public APIRequestCreateProductFeed(String nodeId, APIContext context) {
+      super(context, nodeId, "/product_feeds", "POST", Arrays.asList(PARAMS));
+    }
+
+    @Override
+    public APIRequestCreateProductFeed setParam(String param, Object value) {
+      setParamInternal(param, value);
+      return this;
+    }
+
+    @Override
+    public APIRequestCreateProductFeed setParams(Map<String, Object> params) {
+      setParamsInternal(params);
+      return this;
+    }
+
+
+    public APIRequestCreateProductFeed setCountry (String country) {
+      this.setParam("country", country);
+      return this;
+    }
+
+    public APIRequestCreateProductFeed setDefaultCurrency (String defaultCurrency) {
+      this.setParam("default_currency", defaultCurrency);
+      return this;
+    }
+
+    public APIRequestCreateProductFeed setDeletionEnabled (Boolean deletionEnabled) {
+      this.setParam("deletion_enabled", deletionEnabled);
+      return this;
+    }
+    public APIRequestCreateProductFeed setDeletionEnabled (String deletionEnabled) {
+      this.setParam("deletion_enabled", deletionEnabled);
+      return this;
+    }
+
+    public APIRequestCreateProductFeed setDelimiter (ProductFeed.EnumDelimiter delimiter) {
+      this.setParam("delimiter", delimiter);
+      return this;
+    }
+    public APIRequestCreateProductFeed setDelimiter (String delimiter) {
+      this.setParam("delimiter", delimiter);
+      return this;
+    }
+
+    public APIRequestCreateProductFeed setEncoding (ProductFeed.EnumEncoding encoding) {
+      this.setParam("encoding", encoding);
+      return this;
+    }
+    public APIRequestCreateProductFeed setEncoding (String encoding) {
+      this.setParam("encoding", encoding);
+      return this;
+    }
+
+    public APIRequestCreateProductFeed setFileName (String fileName) {
+      this.setParam("file_name", fileName);
+      return this;
+    }
+
+    public APIRequestCreateProductFeed setId (String id) {
+      this.setParam("id", id);
+      return this;
+    }
+
+    public APIRequestCreateProductFeed setName (String name) {
+      this.setParam("name", name);
+      return this;
+    }
+
+    public APIRequestCreateProductFeed setQuotedFieldsMode (ProductFeed.EnumQuotedFieldsMode quotedFieldsMode) {
+      this.setParam("quoted_fields_mode", quotedFieldsMode);
+      return this;
+    }
+    public APIRequestCreateProductFeed setQuotedFieldsMode (String quotedFieldsMode) {
+      this.setParam("quoted_fields_mode", quotedFieldsMode);
+      return this;
+    }
+
+    public APIRequestCreateProductFeed setSchedule (String schedule) {
+      this.setParam("schedule", schedule);
+      return this;
+    }
+
+    public APIRequestCreateProductFeed requestAllFields () {
+      return this.requestAllFields(true);
+    }
+
+    public APIRequestCreateProductFeed requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestCreateProductFeed requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
+    }
+
+    @Override
+    public APIRequestCreateProductFeed requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestCreateProductFeed requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetProducts requestField (String field, boolean value) {
+    @Override
+    public APIRequestCreateProductFeed requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetProducts requestIdField () {
-      return this.requestIdField(true);
+  }
+
+  public static class APIRequestGetProductGroups extends APIRequest<ProductGroup> {
+
+    APINodeList<ProductGroup> lastResponse = null;
+    @Override
+    public APINodeList<ProductGroup> getLastResponse() {
+      return lastResponse;
     }
-    public APIRequestGetProducts requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
+    public static final String[] PARAMS = {
+    };
+
+    public static final String[] FIELDS = {
+      "id",
+      "retailer_id",
+      "variants",
+    };
+
+    @Override
+    public APINodeList<ProductGroup> parseResponse(String response) throws APIException {
+      return ProductGroup.parseResponse(response, getContext(), this);
     }
-    public APIRequestGetProducts requestAdditionalImageUrlsField () {
-      return this.requestAdditionalImageUrlsField(true);
+
+    @Override
+    public APINodeList<ProductGroup> execute() throws APIException {
+      return execute(new HashMap<String, Object>());
     }
-    public APIRequestGetProducts requestAdditionalImageUrlsField (boolean value) {
-      this.requestField("additional_image_urls", value);
+
+    @Override
+    public APINodeList<ProductGroup> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
+    }
+
+    public APIRequestGetProductGroups(String nodeId, APIContext context) {
+      super(context, nodeId, "/product_groups", "GET", Arrays.asList(PARAMS));
+    }
+
+    @Override
+    public APIRequestGetProductGroups setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
-    public APIRequestGetProducts requestApplinksField () {
-      return this.requestApplinksField(true);
+
+    @Override
+    public APIRequestGetProductGroups setParams(Map<String, Object> params) {
+      setParamsInternal(params);
+      return this;
     }
-    public APIRequestGetProducts requestApplinksField (boolean value) {
-      this.requestField("applinks", value);
+
+
+    public APIRequestGetProductGroups requestAllFields () {
+      return this.requestAllFields(true);
+    }
+
+    public APIRequestGetProductGroups requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetProducts requestAgeGroupField () {
-      return this.requestAgeGroupField(true);
+
+    @Override
+    public APIRequestGetProductGroups requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
-    public APIRequestGetProducts requestAgeGroupField (boolean value) {
-      this.requestField("age_group", value);
+
+    @Override
+    public APIRequestGetProductGroups requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetProducts requestAvailabilityField () {
-      return this.requestAvailabilityField(true);
+
+    @Override
+    public APIRequestGetProductGroups requestField (String field) {
+      this.requestField(field, true);
+      return this;
     }
-    public APIRequestGetProducts requestAvailabilityField (boolean value) {
-      this.requestField("availability", value);
+
+    @Override
+    public APIRequestGetProductGroups requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
-    public APIRequestGetProducts requestBrandField () {
-      return this.requestBrandField(true);
+
+    public APIRequestGetProductGroups requestIdField () {
+      return this.requestIdField(true);
     }
-    public APIRequestGetProducts requestBrandField (boolean value) {
-      this.requestField("brand", value);
+    public APIRequestGetProductGroups requestIdField (boolean value) {
+      this.requestField("id", value);
       return this;
     }
-    public APIRequestGetProducts requestCategoryField () {
-      return this.requestCategoryField(true);
+    public APIRequestGetProductGroups requestRetailerIdField () {
+      return this.requestRetailerIdField(true);
     }
-    public APIRequestGetProducts requestCategoryField (boolean value) {
-      this.requestField("category", value);
+    public APIRequestGetProductGroups requestRetailerIdField (boolean value) {
+      this.requestField("retailer_id", value);
       return this;
     }
-    public APIRequestGetProducts requestColorField () {
-      return this.requestColorField(true);
+    public APIRequestGetProductGroups requestVariantsField () {
+      return this.requestVariantsField(true);
     }
-    public APIRequestGetProducts requestColorField (boolean value) {
-      this.requestField("color", value);
+    public APIRequestGetProductGroups requestVariantsField (boolean value) {
+      this.requestField("variants", value);
       return this;
     }
-    public APIRequestGetProducts requestCommerceInsightsField () {
-      return this.requestCommerceInsightsField(true);
+  }
+
+  public static class APIRequestCreateProductGroup extends APIRequest<ProductGroup> {
+
+    ProductGroup lastResponse = null;
+    @Override
+    public ProductGroup getLastResponse() {
+      return lastResponse;
     }
-    public APIRequestGetProducts requestCommerceInsightsField (boolean value) {
-      this.requestField("commerce_insights", value);
+    public static final String[] PARAMS = {
+      "id",
+      "retailer_id",
+      "variants",
+    };
+
+    public static final String[] FIELDS = {
+    };
+
+    @Override
+    public ProductGroup parseResponse(String response) throws APIException {
+      return ProductGroup.parseResponse(response, getContext(), this).head();
+    }
+
+    @Override
+    public ProductGroup execute() throws APIException {
+      return execute(new HashMap<String, Object>());
+    }
+
+    @Override
+    public ProductGroup execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
+    }
+
+    public APIRequestCreateProductGroup(String nodeId, APIContext context) {
+      super(context, nodeId, "/product_groups", "POST", Arrays.asList(PARAMS));
+    }
+
+    @Override
+    public APIRequestCreateProductGroup setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
-    public APIRequestGetProducts requestConditionField () {
-      return this.requestConditionField(true);
+
+    @Override
+    public APIRequestCreateProductGroup setParams(Map<String, Object> params) {
+      setParamsInternal(params);
+      return this;
     }
-    public APIRequestGetProducts requestConditionField (boolean value) {
-      this.requestField("condition", value);
+
+
+    public APIRequestCreateProductGroup setId (String id) {
+      this.setParam("id", id);
+      return this;
+    }
+
+    public APIRequestCreateProductGroup setRetailerId (String retailerId) {
+      this.setParam("retailer_id", retailerId);
       return this;
     }
-    public APIRequestGetProducts requestCustomDataField () {
-      return this.requestCustomDataField(true);
+
+    public APIRequestCreateProductGroup setVariants (List<Object> variants) {
+      this.setParam("variants", variants);
+      return this;
     }
-    public APIRequestGetProducts requestCustomDataField (boolean value) {
-      this.requestField("custom_data", value);
+    public APIRequestCreateProductGroup setVariants (String variants) {
+      this.setParam("variants", variants);
       return this;
     }
-    public APIRequestGetProducts requestDescriptionField () {
-      return this.requestDescriptionField(true);
+
+    public APIRequestCreateProductGroup requestAllFields () {
+      return this.requestAllFields(true);
     }
-    public APIRequestGetProducts requestDescriptionField (boolean value) {
-      this.requestField("description", value);
+
+    public APIRequestCreateProductGroup requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetProducts requestExpirationDateField () {
-      return this.requestExpirationDateField(true);
+
+    @Override
+    public APIRequestCreateProductGroup requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
-    public APIRequestGetProducts requestExpirationDateField (boolean value) {
-      this.requestField("expiration_date", value);
+
+    @Override
+    public APIRequestCreateProductGroup requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetProducts requestGenderField () {
-      return this.requestGenderField(true);
+
+    @Override
+    public APIRequestCreateProductGroup requestField (String field) {
+      this.requestField(field, true);
+      return this;
     }
-    public APIRequestGetProducts requestGenderField (boolean value) {
-      this.requestField("gender", value);
+
+    @Override
+    public APIRequestCreateProductGroup requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
-    public APIRequestGetProducts requestGtinField () {
-      return this.requestGtinField(true);
+
+  }
+
+  public static class APIRequestGetProductSets extends APIRequest<ProductSet> {
+
+    APINodeList<ProductSet> lastResponse = null;
+    @Override
+    public APINodeList<ProductSet> getLastResponse() {
+      return lastResponse;
     }
-    public APIRequestGetProducts requestGtinField (boolean value) {
-      this.requestField("gtin", value);
-      return this;
+    public static final String[] PARAMS = {
+      "ancestor_id",
+      "has_children",
+      "parent_id",
+    };
+
+    public static final String[] FIELDS = {
+      "filter",
+      "id",
+      "name",
+      "product_catalog",
+      "product_count",
+    };
+
+    @Override
+    public APINodeList<ProductSet> parseResponse(String response) throws APIException {
+      return ProductSet.parseResponse(response, getContext(), this);
     }
-    public APIRequestGetProducts requestImageUrlField () {
-      return this.requestImageUrlField(true);
+
+    @Override
+    public APINodeList<ProductSet> execute() throws APIException {
+      return execute(new HashMap<String, Object>());
     }
-    public APIRequestGetProducts requestImageUrlField (boolean value) {
-      this.requestField("image_url", value);
-      return this;
+
+    @Override
+    public APINodeList<ProductSet> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
     }
-    public APIRequestGetProducts requestMaterialField () {
-      return this.requestMaterialField(true);
+
+    public APIRequestGetProductSets(String nodeId, APIContext context) {
+      super(context, nodeId, "/product_sets", "GET", Arrays.asList(PARAMS));
     }
-    public APIRequestGetProducts requestMaterialField (boolean value) {
-      this.requestField("material", value);
+
+    @Override
+    public APIRequestGetProductSets setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
-    public APIRequestGetProducts requestManufacturerPartNumberField () {
-      return this.requestManufacturerPartNumberField(true);
-    }
-    public APIRequestGetProducts requestManufacturerPartNumberField (boolean value) {
-      this.requestField("manufacturer_part_number", value);
+
+    @Override
+    public APIRequestGetProductSets setParams(Map<String, Object> params) {
+      setParamsInternal(params);
       return this;
     }
-    public APIRequestGetProducts requestNameField () {
-      return this.requestNameField(true);
+
+
+    public APIRequestGetProductSets setAncestorId (String ancestorId) {
+      this.setParam("ancestor_id", ancestorId);
+      return this;
     }
-    public APIRequestGetProducts requestNameField (boolean value) {
-      this.requestField("name", value);
+
+    public APIRequestGetProductSets setHasChildren (Boolean hasChildren) {
+      this.setParam("has_children", hasChildren);
       return this;
     }
-    public APIRequestGetProducts requestOrderingIndexField () {
-      return this.requestOrderingIndexField(true);
+    public APIRequestGetProductSets setHasChildren (String hasChildren) {
+      this.setParam("has_children", hasChildren);
+      return this;
     }
-    public APIRequestGetProducts requestOrderingIndexField (boolean value) {
-      this.requestField("ordering_index", value);
+
+    public APIRequestGetProductSets setParentId (String parentId) {
+      this.setParam("parent_id", parentId);
       return this;
     }
-    public APIRequestGetProducts requestPatternField () {
-      return this.requestPatternField(true);
+
+    public APIRequestGetProductSets requestAllFields () {
+      return this.requestAllFields(true);
     }
-    public APIRequestGetProducts requestPatternField (boolean value) {
-      this.requestField("pattern", value);
+
+    public APIRequestGetProductSets requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetProducts requestPriceField () {
-      return this.requestPriceField(true);
+
+    @Override
+    public APIRequestGetProductSets requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
-    public APIRequestGetProducts requestPriceField (boolean value) {
-      this.requestField("price", value);
+
+    @Override
+    public APIRequestGetProductSets requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetProducts requestProductTypeField () {
-      return this.requestProductTypeField(true);
+
+    @Override
+    public APIRequestGetProductSets requestField (String field) {
+      this.requestField(field, true);
+      return this;
     }
-    public APIRequestGetProducts requestProductTypeField (boolean value) {
-      this.requestField("product_type", value);
+
+    @Override
+    public APIRequestGetProductSets requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
-    public APIRequestGetProducts requestRetailerIdField () {
-      return this.requestRetailerIdField(true);
+
+    public APIRequestGetProductSets requestFilterField () {
+      return this.requestFilterField(true);
     }
-    public APIRequestGetProducts requestRetailerIdField (boolean value) {
-      this.requestField("retailer_id", value);
+    public APIRequestGetProductSets requestFilterField (boolean value) {
+      this.requestField("filter", value);
       return this;
     }
-    public APIRequestGetProducts requestRetailerProductGroupIdField () {
-      return this.requestRetailerProductGroupIdField(true);
+    public APIRequestGetProductSets requestIdField () {
+      return this.requestIdField(true);
     }
-    public APIRequestGetProducts requestRetailerProductGroupIdField (boolean value) {
-      this.requestField("retailer_product_group_id", value);
+    public APIRequestGetProductSets requestIdField (boolean value) {
+      this.requestField("id", value);
       return this;
     }
-    public APIRequestGetProducts requestReviewRejectionReasonsField () {
-      return this.requestReviewRejectionReasonsField(true);
+    public APIRequestGetProductSets requestNameField () {
+      return this.requestNameField(true);
     }
-    public APIRequestGetProducts requestReviewRejectionReasonsField (boolean value) {
-      this.requestField("review_rejection_reasons", value);
+    public APIRequestGetProductSets requestNameField (boolean value) {
+      this.requestField("name", value);
       return this;
     }
-    public APIRequestGetProducts requestReviewStatusField () {
-      return this.requestReviewStatusField(true);
+    public APIRequestGetProductSets requestProductCatalogField () {
+      return this.requestProductCatalogField(true);
     }
-    public APIRequestGetProducts requestReviewStatusField (boolean value) {
-      this.requestField("review_status", value);
+    public APIRequestGetProductSets requestProductCatalogField (boolean value) {
+      this.requestField("product_catalog", value);
       return this;
     }
-    public APIRequestGetProducts requestSalePriceField () {
-      return this.requestSalePriceField(true);
+    public APIRequestGetProductSets requestProductCountField () {
+      return this.requestProductCountField(true);
     }
-    public APIRequestGetProducts requestSalePriceField (boolean value) {
-      this.requestField("sale_price", value);
+    public APIRequestGetProductSets requestProductCountField (boolean value) {
+      this.requestField("product_count", value);
       return this;
     }
-    public APIRequestGetProducts requestSalePriceStartDateField () {
-      return this.requestSalePriceStartDateField(true);
+  }
+
+  public static class APIRequestCreateProductSet extends APIRequest<ProductSet> {
+
+    ProductSet lastResponse = null;
+    @Override
+    public ProductSet getLastResponse() {
+      return lastResponse;
     }
-    public APIRequestGetProducts requestSalePriceStartDateField (boolean value) {
-      this.requestField("sale_price_start_date", value);
-      return this;
+    public static final String[] PARAMS = {
+      "filter",
+      "id",
+      "name",
+    };
+
+    public static final String[] FIELDS = {
+    };
+
+    @Override
+    public ProductSet parseResponse(String response) throws APIException {
+      return ProductSet.parseResponse(response, getContext(), this).head();
     }
-    public APIRequestGetProducts requestSalePriceEndDateField () {
-      return this.requestSalePriceEndDateField(true);
+
+    @Override
+    public ProductSet execute() throws APIException {
+      return execute(new HashMap<String, Object>());
     }
-    public APIRequestGetProducts requestSalePriceEndDateField (boolean value) {
-      this.requestField("sale_price_end_date", value);
-      return this;
+
+    @Override
+    public ProductSet execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
     }
-    public APIRequestGetProducts requestShippingWeightValueField () {
-      return this.requestShippingWeightValueField(true);
+
+    public APIRequestCreateProductSet(String nodeId, APIContext context) {
+      super(context, nodeId, "/product_sets", "POST", Arrays.asList(PARAMS));
     }
-    public APIRequestGetProducts requestShippingWeightValueField (boolean value) {
-      this.requestField("shipping_weight_value", value);
+
+    @Override
+    public APIRequestCreateProductSet setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
-    public APIRequestGetProducts requestShippingWeightUnitField () {
-      return this.requestShippingWeightUnitField(true);
-    }
-    public APIRequestGetProducts requestShippingWeightUnitField (boolean value) {
-      this.requestField("shipping_weight_unit", value);
+
+    @Override
+    public APIRequestCreateProductSet setParams(Map<String, Object> params) {
+      setParamsInternal(params);
       return this;
     }
-    public APIRequestGetProducts requestSizeField () {
-      return this.requestSizeField(true);
+
+
+    public APIRequestCreateProductSet setFilter (Object filter) {
+      this.setParam("filter", filter);
+      return this;
     }
-    public APIRequestGetProducts requestSizeField (boolean value) {
-      this.requestField("size", value);
+    public APIRequestCreateProductSet setFilter (String filter) {
+      this.setParam("filter", filter);
       return this;
     }
-    public APIRequestGetProducts requestStartDateField () {
-      return this.requestStartDateField(true);
+
+    public APIRequestCreateProductSet setId (String id) {
+      this.setParam("id", id);
+      return this;
     }
-    public APIRequestGetProducts requestStartDateField (boolean value) {
-      this.requestField("start_date", value);
+
+    public APIRequestCreateProductSet setName (String name) {
+      this.setParam("name", name);
       return this;
     }
-    public APIRequestGetProducts requestUrlField () {
-      return this.requestUrlField(true);
+
+    public APIRequestCreateProductSet requestAllFields () {
+      return this.requestAllFields(true);
     }
-    public APIRequestGetProducts requestUrlField (boolean value) {
-      this.requestField("url", value);
+
+    public APIRequestCreateProductSet requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetProducts requestVisibilityField () {
-      return this.requestVisibilityField(true);
+
+    @Override
+    public APIRequestCreateProductSet requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
-    public APIRequestGetProducts requestVisibilityField (boolean value) {
-      this.requestField("visibility", value);
+
+    @Override
+    public APIRequestCreateProductSet requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetProducts requestProductFeedField () {
-      return this.requestProductFeedField(true);
+
+    @Override
+    public APIRequestCreateProductSet requestField (String field) {
+      this.requestField(field, true);
+      return this;
     }
-    public APIRequestGetProducts requestProductFeedField (boolean value) {
-      this.requestField("product_feed", value);
+
+    @Override
+    public APIRequestCreateProductSet requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
 
   }
 
-  public static class APIRequestGetProductGroups extends APIRequest<ProductGroup> {
+  public static class APIRequestGetProducts extends APIRequest<ProductItem> {
 
-    APINodeList<ProductGroup> lastResponse = null;
+    APINodeList<ProductItem> lastResponse = null;
     @Override
-    public APINodeList<ProductGroup> getLastResponse() {
+    public APINodeList<ProductItem> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
+      "filter",
     };
 
     public static final String[] FIELDS = {
+      "additional_image_urls",
+      "age_group",
+      "applinks",
+      "availability",
+      "brand",
+      "category",
+      "color",
+      "commerce_insights",
+      "condition",
+      "custom_data",
+      "custom_label_0",
+      "custom_label_1",
+      "custom_label_2",
+      "custom_label_3",
+      "custom_label_4",
+      "description",
+      "expiration_date",
+      "gender",
+      "gtin",
       "id",
+      "image_url",
+      "manufacturer_part_number",
+      "material",
+      "name",
+      "ordering_index",
+      "pattern",
+      "price",
+      "product_feed",
+      "product_type",
       "retailer_id",
-      "variants",
+      "retailer_product_group_id",
+      "review_rejection_reasons",
+      "review_status",
+      "sale_price",
+      "sale_price_end_date",
+      "sale_price_start_date",
+      "shipping_weight_unit",
+      "shipping_weight_value",
+      "size",
+      "start_date",
+      "url",
+      "visibility",
     };
 
     @Override
-    public APINodeList<ProductGroup> parseResponse(String response) throws APIException {
-      return ProductGroup.parseResponse(response, getContext(), this);
+    public APINodeList<ProductItem> parseResponse(String response) throws APIException {
+      return ProductItem.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINodeList<ProductGroup> execute() throws APIException {
+    public APINodeList<ProductItem> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<ProductGroup> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<ProductItem> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetProductGroups(String nodeId, APIContext context) {
-      super(context, nodeId, "/product_groups", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetProducts(String nodeId, APIContext context) {
+      super(context, nodeId, "/products", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetProductGroups setParam(String param, Object value) {
+    @Override
+    public APIRequestGetProducts setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetProductGroups setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetProducts setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetProductGroups requestAllFields () {
+    public APIRequestGetProducts setFilter (Object filter) {
+      this.setParam("filter", filter);
+      return this;
+    }
+    public APIRequestGetProducts setFilter (String filter) {
+      this.setParam("filter", filter);
+      return this;
+    }
+
+    public APIRequestGetProducts requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetProductGroups requestAllFields (boolean value) {
+    public APIRequestGetProducts requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetProductGroups requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetProducts requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetProductGroups requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetProducts requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetProductGroups requestField (String field) {
+    @Override
+    public APIRequestGetProducts requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetProductGroups requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetProducts requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetProductGroups requestIdField () {
-      return this.requestIdField(true);
+    public APIRequestGetProducts requestAdditionalImageUrlsField () {
+      return this.requestAdditionalImageUrlsField(true);
     }
-    public APIRequestGetProductGroups requestIdField (boolean value) {
-      this.requestField("id", value);
+    public APIRequestGetProducts requestAdditionalImageUrlsField (boolean value) {
+      this.requestField("additional_image_urls", value);
       return this;
     }
-    public APIRequestGetProductGroups requestRetailerIdField () {
-      return this.requestRetailerIdField(true);
+    public APIRequestGetProducts requestAgeGroupField () {
+      return this.requestAgeGroupField(true);
     }
-    public APIRequestGetProductGroups requestRetailerIdField (boolean value) {
-      this.requestField("retailer_id", value);
+    public APIRequestGetProducts requestAgeGroupField (boolean value) {
+      this.requestField("age_group", value);
       return this;
     }
-    public APIRequestGetProductGroups requestVariantsField () {
-      return this.requestVariantsField(true);
+    public APIRequestGetProducts requestApplinksField () {
+      return this.requestApplinksField(true);
     }
-    public APIRequestGetProductGroups requestVariantsField (boolean value) {
-      this.requestField("variants", value);
+    public APIRequestGetProducts requestApplinksField (boolean value) {
+      this.requestField("applinks", value);
       return this;
     }
-
-  }
-
-  public static class APIRequestGetProductFeeds extends APIRequest<ProductFeed> {
-
-    APINodeList<ProductFeed> lastResponse = null;
-    @Override
-    public APINodeList<ProductFeed> getLastResponse() {
-      return lastResponse;
+    public APIRequestGetProducts requestAvailabilityField () {
+      return this.requestAvailabilityField(true);
     }
-    public static final String[] PARAMS = {
-    };
-
-    public static final String[] FIELDS = {
-      "id",
-      "country",
-      "created_time",
-      "deletion_enabled",
-      "delimiter",
-      "encoding",
-      "file_name",
-      "latest_upload",
-      "name",
-      "product_count",
-      "quoted_fields",
-      "schedule",
-    };
-
-    @Override
-    public APINodeList<ProductFeed> parseResponse(String response) throws APIException {
-      return ProductFeed.parseResponse(response, getContext(), this);
+    public APIRequestGetProducts requestAvailabilityField (boolean value) {
+      this.requestField("availability", value);
+      return this;
     }
-
-    @Override
-    public APINodeList<ProductFeed> execute() throws APIException {
-      return execute(new HashMap<String, Object>());
+    public APIRequestGetProducts requestBrandField () {
+      return this.requestBrandField(true);
+    }
+    public APIRequestGetProducts requestBrandField (boolean value) {
+      this.requestField("brand", value);
+      return this;
+    }
+    public APIRequestGetProducts requestCategoryField () {
+      return this.requestCategoryField(true);
+    }
+    public APIRequestGetProducts requestCategoryField (boolean value) {
+      this.requestField("category", value);
+      return this;
+    }
+    public APIRequestGetProducts requestColorField () {
+      return this.requestColorField(true);
+    }
+    public APIRequestGetProducts requestColorField (boolean value) {
+      this.requestField("color", value);
+      return this;
+    }
+    public APIRequestGetProducts requestCommerceInsightsField () {
+      return this.requestCommerceInsightsField(true);
+    }
+    public APIRequestGetProducts requestCommerceInsightsField (boolean value) {
+      this.requestField("commerce_insights", value);
+      return this;
+    }
+    public APIRequestGetProducts requestConditionField () {
+      return this.requestConditionField(true);
+    }
+    public APIRequestGetProducts requestConditionField (boolean value) {
+      this.requestField("condition", value);
+      return this;
+    }
+    public APIRequestGetProducts requestCustomDataField () {
+      return this.requestCustomDataField(true);
+    }
+    public APIRequestGetProducts requestCustomDataField (boolean value) {
+      this.requestField("custom_data", value);
+      return this;
+    }
+    public APIRequestGetProducts requestCustomLabel0Field () {
+      return this.requestCustomLabel0Field(true);
+    }
+    public APIRequestGetProducts requestCustomLabel0Field (boolean value) {
+      this.requestField("custom_label_0", value);
+      return this;
+    }
+    public APIRequestGetProducts requestCustomLabel1Field () {
+      return this.requestCustomLabel1Field(true);
+    }
+    public APIRequestGetProducts requestCustomLabel1Field (boolean value) {
+      this.requestField("custom_label_1", value);
+      return this;
     }
-
-    @Override
-    public APINodeList<ProductFeed> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
+    public APIRequestGetProducts requestCustomLabel2Field () {
+      return this.requestCustomLabel2Field(true);
     }
-
-    public APIRequestGetProductFeeds(String nodeId, APIContext context) {
-      super(context, nodeId, "/product_feeds", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetProducts requestCustomLabel2Field (boolean value) {
+      this.requestField("custom_label_2", value);
+      return this;
     }
-
-    public APIRequestGetProductFeeds setParam(String param, Object value) {
-      setParamInternal(param, value);
+    public APIRequestGetProducts requestCustomLabel3Field () {
+      return this.requestCustomLabel3Field(true);
+    }
+    public APIRequestGetProducts requestCustomLabel3Field (boolean value) {
+      this.requestField("custom_label_3", value);
       return this;
     }
-
-    public APIRequestGetProductFeeds setParams(Map<String, Object> params) {
-      setParamsInternal(params);
+    public APIRequestGetProducts requestCustomLabel4Field () {
+      return this.requestCustomLabel4Field(true);
+    }
+    public APIRequestGetProducts requestCustomLabel4Field (boolean value) {
+      this.requestField("custom_label_4", value);
       return this;
     }
-
-
-    public APIRequestGetProductFeeds requestAllFields () {
-      return this.requestAllFields(true);
+    public APIRequestGetProducts requestDescriptionField () {
+      return this.requestDescriptionField(true);
     }
-
-    public APIRequestGetProductFeeds requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
+    public APIRequestGetProducts requestDescriptionField (boolean value) {
+      this.requestField("description", value);
       return this;
     }
-
-    public APIRequestGetProductFeeds requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
+    public APIRequestGetProducts requestExpirationDateField () {
+      return this.requestExpirationDateField(true);
     }
-
-    public APIRequestGetProductFeeds requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+    public APIRequestGetProducts requestExpirationDateField (boolean value) {
+      this.requestField("expiration_date", value);
       return this;
     }
-
-    public APIRequestGetProductFeeds requestField (String field) {
-      this.requestField(field, true);
+    public APIRequestGetProducts requestGenderField () {
+      return this.requestGenderField(true);
+    }
+    public APIRequestGetProducts requestGenderField (boolean value) {
+      this.requestField("gender", value);
       return this;
     }
-
-    public APIRequestGetProductFeeds requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
+    public APIRequestGetProducts requestGtinField () {
+      return this.requestGtinField(true);
+    }
+    public APIRequestGetProducts requestGtinField (boolean value) {
+      this.requestField("gtin", value);
       return this;
     }
-
-    public APIRequestGetProductFeeds requestIdField () {
+    public APIRequestGetProducts requestIdField () {
       return this.requestIdField(true);
     }
-    public APIRequestGetProductFeeds requestIdField (boolean value) {
+    public APIRequestGetProducts requestIdField (boolean value) {
       this.requestField("id", value);
       return this;
     }
-    public APIRequestGetProductFeeds requestCountryField () {
-      return this.requestCountryField(true);
+    public APIRequestGetProducts requestImageUrlField () {
+      return this.requestImageUrlField(true);
     }
-    public APIRequestGetProductFeeds requestCountryField (boolean value) {
-      this.requestField("country", value);
+    public APIRequestGetProducts requestImageUrlField (boolean value) {
+      this.requestField("image_url", value);
       return this;
     }
-    public APIRequestGetProductFeeds requestCreatedTimeField () {
-      return this.requestCreatedTimeField(true);
+    public APIRequestGetProducts requestManufacturerPartNumberField () {
+      return this.requestManufacturerPartNumberField(true);
     }
-    public APIRequestGetProductFeeds requestCreatedTimeField (boolean value) {
-      this.requestField("created_time", value);
+    public APIRequestGetProducts requestManufacturerPartNumberField (boolean value) {
+      this.requestField("manufacturer_part_number", value);
       return this;
     }
-    public APIRequestGetProductFeeds requestDeletionEnabledField () {
-      return this.requestDeletionEnabledField(true);
+    public APIRequestGetProducts requestMaterialField () {
+      return this.requestMaterialField(true);
     }
-    public APIRequestGetProductFeeds requestDeletionEnabledField (boolean value) {
-      this.requestField("deletion_enabled", value);
+    public APIRequestGetProducts requestMaterialField (boolean value) {
+      this.requestField("material", value);
       return this;
     }
-    public APIRequestGetProductFeeds requestDelimiterField () {
-      return this.requestDelimiterField(true);
+    public APIRequestGetProducts requestNameField () {
+      return this.requestNameField(true);
     }
-    public APIRequestGetProductFeeds requestDelimiterField (boolean value) {
-      this.requestField("delimiter", value);
+    public APIRequestGetProducts requestNameField (boolean value) {
+      this.requestField("name", value);
       return this;
     }
-    public APIRequestGetProductFeeds requestEncodingField () {
-      return this.requestEncodingField(true);
+    public APIRequestGetProducts requestOrderingIndexField () {
+      return this.requestOrderingIndexField(true);
     }
-    public APIRequestGetProductFeeds requestEncodingField (boolean value) {
-      this.requestField("encoding", value);
+    public APIRequestGetProducts requestOrderingIndexField (boolean value) {
+      this.requestField("ordering_index", value);
       return this;
     }
-    public APIRequestGetProductFeeds requestFileNameField () {
-      return this.requestFileNameField(true);
+    public APIRequestGetProducts requestPatternField () {
+      return this.requestPatternField(true);
     }
-    public APIRequestGetProductFeeds requestFileNameField (boolean value) {
-      this.requestField("file_name", value);
+    public APIRequestGetProducts requestPatternField (boolean value) {
+      this.requestField("pattern", value);
       return this;
     }
-    public APIRequestGetProductFeeds requestLatestUploadField () {
-      return this.requestLatestUploadField(true);
+    public APIRequestGetProducts requestPriceField () {
+      return this.requestPriceField(true);
     }
-    public APIRequestGetProductFeeds requestLatestUploadField (boolean value) {
-      this.requestField("latest_upload", value);
+    public APIRequestGetProducts requestPriceField (boolean value) {
+      this.requestField("price", value);
       return this;
     }
-    public APIRequestGetProductFeeds requestNameField () {
-      return this.requestNameField(true);
+    public APIRequestGetProducts requestProductFeedField () {
+      return this.requestProductFeedField(true);
     }
-    public APIRequestGetProductFeeds requestNameField (boolean value) {
-      this.requestField("name", value);
+    public APIRequestGetProducts requestProductFeedField (boolean value) {
+      this.requestField("product_feed", value);
       return this;
     }
-    public APIRequestGetProductFeeds requestProductCountField () {
-      return this.requestProductCountField(true);
+    public APIRequestGetProducts requestProductTypeField () {
+      return this.requestProductTypeField(true);
     }
-    public APIRequestGetProductFeeds requestProductCountField (boolean value) {
-      this.requestField("product_count", value);
+    public APIRequestGetProducts requestProductTypeField (boolean value) {
+      this.requestField("product_type", value);
       return this;
     }
-    public APIRequestGetProductFeeds requestQuotedFieldsField () {
-      return this.requestQuotedFieldsField(true);
+    public APIRequestGetProducts requestRetailerIdField () {
+      return this.requestRetailerIdField(true);
     }
-    public APIRequestGetProductFeeds requestQuotedFieldsField (boolean value) {
-      this.requestField("quoted_fields", value);
+    public APIRequestGetProducts requestRetailerIdField (boolean value) {
+      this.requestField("retailer_id", value);
       return this;
     }
-    public APIRequestGetProductFeeds requestScheduleField () {
-      return this.requestScheduleField(true);
+    public APIRequestGetProducts requestRetailerProductGroupIdField () {
+      return this.requestRetailerProductGroupIdField(true);
     }
-    public APIRequestGetProductFeeds requestScheduleField (boolean value) {
-      this.requestField("schedule", value);
+    public APIRequestGetProducts requestRetailerProductGroupIdField (boolean value) {
+      this.requestField("retailer_product_group_id", value);
       return this;
     }
-
-  }
-
-  public static class APIRequestGetProductSets extends APIRequest<ProductSet> {
-
-    APINodeList<ProductSet> lastResponse = null;
-    @Override
-    public APINodeList<ProductSet> getLastResponse() {
-      return lastResponse;
+    public APIRequestGetProducts requestReviewRejectionReasonsField () {
+      return this.requestReviewRejectionReasonsField(true);
     }
-    public static final String[] PARAMS = {
-    };
-
-    public static final String[] FIELDS = {
-      "id",
-      "name",
-      "filter",
-      "product_count",
-    };
-
-    @Override
-    public APINodeList<ProductSet> parseResponse(String response) throws APIException {
-      return ProductSet.parseResponse(response, getContext(), this);
+    public APIRequestGetProducts requestReviewRejectionReasonsField (boolean value) {
+      this.requestField("review_rejection_reasons", value);
+      return this;
     }
-
-    @Override
-    public APINodeList<ProductSet> execute() throws APIException {
-      return execute(new HashMap<String, Object>());
+    public APIRequestGetProducts requestReviewStatusField () {
+      return this.requestReviewStatusField(true);
     }
-
-    @Override
-    public APINodeList<ProductSet> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
+    public APIRequestGetProducts requestReviewStatusField (boolean value) {
+      this.requestField("review_status", value);
+      return this;
     }
-
-    public APIRequestGetProductSets(String nodeId, APIContext context) {
-      super(context, nodeId, "/product_sets", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetProducts requestSalePriceField () {
+      return this.requestSalePriceField(true);
     }
-
-    public APIRequestGetProductSets setParam(String param, Object value) {
-      setParamInternal(param, value);
+    public APIRequestGetProducts requestSalePriceField (boolean value) {
+      this.requestField("sale_price", value);
       return this;
     }
-
-    public APIRequestGetProductSets setParams(Map<String, Object> params) {
-      setParamsInternal(params);
+    public APIRequestGetProducts requestSalePriceEndDateField () {
+      return this.requestSalePriceEndDateField(true);
+    }
+    public APIRequestGetProducts requestSalePriceEndDateField (boolean value) {
+      this.requestField("sale_price_end_date", value);
       return this;
     }
-
-
-    public APIRequestGetProductSets requestAllFields () {
-      return this.requestAllFields(true);
+    public APIRequestGetProducts requestSalePriceStartDateField () {
+      return this.requestSalePriceStartDateField(true);
     }
-
-    public APIRequestGetProductSets requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
+    public APIRequestGetProducts requestSalePriceStartDateField (boolean value) {
+      this.requestField("sale_price_start_date", value);
       return this;
     }
-
-    public APIRequestGetProductSets requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
+    public APIRequestGetProducts requestShippingWeightUnitField () {
+      return this.requestShippingWeightUnitField(true);
     }
-
-    public APIRequestGetProductSets requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+    public APIRequestGetProducts requestShippingWeightUnitField (boolean value) {
+      this.requestField("shipping_weight_unit", value);
       return this;
     }
-
-    public APIRequestGetProductSets requestField (String field) {
-      this.requestField(field, true);
-      return this;
+    public APIRequestGetProducts requestShippingWeightValueField () {
+      return this.requestShippingWeightValueField(true);
     }
-
-    public APIRequestGetProductSets requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
+    public APIRequestGetProducts requestShippingWeightValueField (boolean value) {
+      this.requestField("shipping_weight_value", value);
       return this;
     }
-
-    public APIRequestGetProductSets requestIdField () {
-      return this.requestIdField(true);
+    public APIRequestGetProducts requestSizeField () {
+      return this.requestSizeField(true);
     }
-    public APIRequestGetProductSets requestIdField (boolean value) {
-      this.requestField("id", value);
+    public APIRequestGetProducts requestSizeField (boolean value) {
+      this.requestField("size", value);
       return this;
     }
-    public APIRequestGetProductSets requestNameField () {
-      return this.requestNameField(true);
+    public APIRequestGetProducts requestStartDateField () {
+      return this.requestStartDateField(true);
     }
-    public APIRequestGetProductSets requestNameField (boolean value) {
-      this.requestField("name", value);
+    public APIRequestGetProducts requestStartDateField (boolean value) {
+      this.requestField("start_date", value);
       return this;
     }
-    public APIRequestGetProductSets requestFilterField () {
-      return this.requestFilterField(true);
+    public APIRequestGetProducts requestUrlField () {
+      return this.requestUrlField(true);
     }
-    public APIRequestGetProductSets requestFilterField (boolean value) {
-      this.requestField("filter", value);
+    public APIRequestGetProducts requestUrlField (boolean value) {
+      this.requestField("url", value);
       return this;
     }
-    public APIRequestGetProductSets requestProductCountField () {
-      return this.requestProductCountField(true);
+    public APIRequestGetProducts requestVisibilityField () {
+      return this.requestVisibilityField(true);
     }
-    public APIRequestGetProductSets requestProductCountField (boolean value) {
-      this.requestField("product_count", value);
+    public APIRequestGetProducts requestVisibilityField (boolean value) {
+      this.requestField("visibility", value);
       return this;
     }
-
   }
 
-  public static class APIRequestCreateExternalEventSource extends APIRequest<ExternalEventSource> {
+  public static class APIRequestCreateProduct extends APIRequest<ProductItem> {
 
-    ExternalEventSource lastResponse = null;
+    ProductItem lastResponse = null;
     @Override
-    public ExternalEventSource getLastResponse() {
+    public ProductItem getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
+      "additional_image_urls",
+      "android_app_name",
+      "android_class",
+      "android_package",
+      "android_url",
+      "availability",
+      "brand",
+      "category",
+      "checkout_url",
+      "color",
+      "condition",
+      "currency",
+      "custom_data",
+      "custom_label_0",
+      "custom_label_1",
+      "custom_label_2",
+      "custom_label_3",
+      "custom_label_4",
+      "description",
+      "expiration_date",
+      "gender",
+      "gtin",
       "id",
-      "external_event_sources",
+      "image_url",
+      "inventory",
+      "ios_app_name",
+      "ios_app_store_id",
+      "ios_url",
+      "ipad_app_name",
+      "ipad_app_store_id",
+      "ipad_url",
+      "iphone_app_name",
+      "iphone_app_store_id",
+      "iphone_url",
+      "manufacturer_part_number",
+      "name",
+      "ordering_index",
+      "pattern",
+      "price",
+      "product_type",
+      "retailer_id",
+      "retailer_product_group_id",
+      "sale_price",
+      "sale_price_end_date",
+      "sale_price_start_date",
+      "size",
+      "start_date",
+      "url",
+      "visibility",
+      "windows_phone_app_id",
+      "windows_phone_app_name",
+      "windows_phone_url",
     };
 
     public static final String[] FIELDS = {
     };
 
     @Override
-    public ExternalEventSource parseResponse(String response) throws APIException {
-      return ExternalEventSource.parseResponse(response, getContext(), this).head();
+    public ProductItem parseResponse(String response) throws APIException {
+      return ProductItem.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public ExternalEventSource execute() throws APIException {
+    public ProductItem execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public ExternalEventSource execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public ProductItem execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestCreateExternalEventSource(String nodeId, APIContext context) {
-      super(context, nodeId, "/external_event_sources", "POST", Arrays.asList(PARAMS));
+    public APIRequestCreateProduct(String nodeId, APIContext context) {
+      super(context, nodeId, "/products", "POST", Arrays.asList(PARAMS));
     }
 
-    public APIRequestCreateExternalEventSource setParam(String param, Object value) {
+    @Override
+    public APIRequestCreateProduct setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestCreateExternalEventSource setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestCreateProduct setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestCreateExternalEventSource setId (String id) {
-      this.setParam("id", id);
+    public APIRequestCreateProduct setAdditionalImageUrls (List<String> additionalImageUrls) {
+      this.setParam("additional_image_urls", additionalImageUrls);
+      return this;
+    }
+    public APIRequestCreateProduct setAdditionalImageUrls (String additionalImageUrls) {
+      this.setParam("additional_image_urls", additionalImageUrls);
       return this;
     }
 
+    public APIRequestCreateProduct setAndroidAppName (String androidAppName) {
+      this.setParam("android_app_name", androidAppName);
+      return this;
+    }
 
-    public APIRequestCreateExternalEventSource setExternalEventSources (List<String> externalEventSources) {
-      this.setParam("external_event_sources", externalEventSources);
+    public APIRequestCreateProduct setAndroidClass (String androidClass) {
+      this.setParam("android_class", androidClass);
       return this;
     }
 
-    public APIRequestCreateExternalEventSource setExternalEventSources (String externalEventSources) {
-      this.setParam("external_event_sources", externalEventSources);
+    public APIRequestCreateProduct setAndroidPackage (String androidPackage) {
+      this.setParam("android_package", androidPackage);
       return this;
     }
 
-    public APIRequestCreateExternalEventSource requestAllFields () {
-      return this.requestAllFields(true);
+    public APIRequestCreateProduct setAndroidUrl (String androidUrl) {
+      this.setParam("android_url", androidUrl);
+      return this;
     }
 
-    public APIRequestCreateExternalEventSource requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
+    public APIRequestCreateProduct setAvailability (ProductItem.EnumAvailability availability) {
+      this.setParam("availability", availability);
+      return this;
+    }
+    public APIRequestCreateProduct setAvailability (String availability) {
+      this.setParam("availability", availability);
       return this;
     }
 
-    public APIRequestCreateExternalEventSource requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
+    public APIRequestCreateProduct setBrand (String brand) {
+      this.setParam("brand", brand);
+      return this;
     }
 
-    public APIRequestCreateExternalEventSource requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+    public APIRequestCreateProduct setCategory (String category) {
+      this.setParam("category", category);
       return this;
     }
 
-    public APIRequestCreateExternalEventSource requestField (String field) {
-      this.requestField(field, true);
+    public APIRequestCreateProduct setCheckoutUrl (String checkoutUrl) {
+      this.setParam("checkout_url", checkoutUrl);
       return this;
     }
 
-    public APIRequestCreateExternalEventSource requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
+    public APIRequestCreateProduct setColor (String color) {
+      this.setParam("color", color);
       return this;
     }
 
+    public APIRequestCreateProduct setCondition (ProductItem.EnumCondition condition) {
+      this.setParam("condition", condition);
+      return this;
+    }
+    public APIRequestCreateProduct setCondition (String condition) {
+      this.setParam("condition", condition);
+      return this;
+    }
 
-  }
+    public APIRequestCreateProduct setCurrency (String currency) {
+      this.setParam("currency", currency);
+      return this;
+    }
 
-  public static class APIRequestCreateProductGroup extends APIRequest<ProductGroup> {
+    public APIRequestCreateProduct setCustomData (Map<String, String> customData) {
+      this.setParam("custom_data", customData);
+      return this;
+    }
+    public APIRequestCreateProduct setCustomData (String customData) {
+      this.setParam("custom_data", customData);
+      return this;
+    }
 
-    ProductGroup lastResponse = null;
-    @Override
-    public ProductGroup getLastResponse() {
-      return lastResponse;
+    public APIRequestCreateProduct setCustomLabel0 (String customLabel0) {
+      this.setParam("custom_label_0", customLabel0);
+      return this;
     }
-    public static final String[] PARAMS = {
-      "id",
-      "retailer_id",
-      "variants",
-    };
 
-    public static final String[] FIELDS = {
-    };
+    public APIRequestCreateProduct setCustomLabel1 (String customLabel1) {
+      this.setParam("custom_label_1", customLabel1);
+      return this;
+    }
 
-    @Override
-    public ProductGroup parseResponse(String response) throws APIException {
-      return ProductGroup.parseResponse(response, getContext(), this).head();
+    public APIRequestCreateProduct setCustomLabel2 (String customLabel2) {
+      this.setParam("custom_label_2", customLabel2);
+      return this;
     }
 
-    @Override
-    public ProductGroup execute() throws APIException {
-      return execute(new HashMap<String, Object>());
+    public APIRequestCreateProduct setCustomLabel3 (String customLabel3) {
+      this.setParam("custom_label_3", customLabel3);
+      return this;
     }
 
-    @Override
-    public ProductGroup execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
+    public APIRequestCreateProduct setCustomLabel4 (String customLabel4) {
+      this.setParam("custom_label_4", customLabel4);
+      return this;
     }
 
-    public APIRequestCreateProductGroup(String nodeId, APIContext context) {
-      super(context, nodeId, "/product_groups", "POST", Arrays.asList(PARAMS));
+    public APIRequestCreateProduct setDescription (String description) {
+      this.setParam("description", description);
+      return this;
     }
 
-    public APIRequestCreateProductGroup setParam(String param, Object value) {
-      setParamInternal(param, value);
+    public APIRequestCreateProduct setExpirationDate (String expirationDate) {
+      this.setParam("expiration_date", expirationDate);
       return this;
     }
 
-    public APIRequestCreateProductGroup setParams(Map<String, Object> params) {
-      setParamsInternal(params);
+    public APIRequestCreateProduct setGender (ProductItem.EnumGender gender) {
+      this.setParam("gender", gender);
+      return this;
+    }
+    public APIRequestCreateProduct setGender (String gender) {
+      this.setParam("gender", gender);
       return this;
     }
 
+    public APIRequestCreateProduct setGtin (String gtin) {
+      this.setParam("gtin", gtin);
+      return this;
+    }
 
-    public APIRequestCreateProductGroup setId (String id) {
+    public APIRequestCreateProduct setId (String id) {
       this.setParam("id", id);
       return this;
     }
 
+    public APIRequestCreateProduct setImageUrl (String imageUrl) {
+      this.setParam("image_url", imageUrl);
+      return this;
+    }
 
-    public APIRequestCreateProductGroup setRetailerId (String retailerId) {
-      this.setParam("retailer_id", retailerId);
+    public APIRequestCreateProduct setInventory (Long inventory) {
+      this.setParam("inventory", inventory);
+      return this;
+    }
+    public APIRequestCreateProduct setInventory (String inventory) {
+      this.setParam("inventory", inventory);
       return this;
     }
 
+    public APIRequestCreateProduct setIosAppName (String iosAppName) {
+      this.setParam("ios_app_name", iosAppName);
+      return this;
+    }
 
-    public APIRequestCreateProductGroup setVariants (List<Object> variants) {
-      this.setParam("variants", variants);
+    public APIRequestCreateProduct setIosAppStoreId (Long iosAppStoreId) {
+      this.setParam("ios_app_store_id", iosAppStoreId);
+      return this;
+    }
+    public APIRequestCreateProduct setIosAppStoreId (String iosAppStoreId) {
+      this.setParam("ios_app_store_id", iosAppStoreId);
       return this;
     }
 
-    public APIRequestCreateProductGroup setVariants (String variants) {
-      this.setParam("variants", variants);
+    public APIRequestCreateProduct setIosUrl (String iosUrl) {
+      this.setParam("ios_url", iosUrl);
       return this;
     }
 
-    public APIRequestCreateProductGroup requestAllFields () {
-      return this.requestAllFields(true);
+    public APIRequestCreateProduct setIpadAppName (String ipadAppName) {
+      this.setParam("ipad_app_name", ipadAppName);
+      return this;
     }
 
-    public APIRequestCreateProductGroup requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
+    public APIRequestCreateProduct setIpadAppStoreId (Long ipadAppStoreId) {
+      this.setParam("ipad_app_store_id", ipadAppStoreId);
+      return this;
+    }
+    public APIRequestCreateProduct setIpadAppStoreId (String ipadAppStoreId) {
+      this.setParam("ipad_app_store_id", ipadAppStoreId);
       return this;
     }
 
-    public APIRequestCreateProductGroup requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
+    public APIRequestCreateProduct setIpadUrl (String ipadUrl) {
+      this.setParam("ipad_url", ipadUrl);
+      return this;
     }
 
-    public APIRequestCreateProductGroup requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+    public APIRequestCreateProduct setIphoneAppName (String iphoneAppName) {
+      this.setParam("iphone_app_name", iphoneAppName);
       return this;
     }
 
-    public APIRequestCreateProductGroup requestField (String field) {
-      this.requestField(field, true);
+    public APIRequestCreateProduct setIphoneAppStoreId (Long iphoneAppStoreId) {
+      this.setParam("iphone_app_store_id", iphoneAppStoreId);
+      return this;
+    }
+    public APIRequestCreateProduct setIphoneAppStoreId (String iphoneAppStoreId) {
+      this.setParam("iphone_app_store_id", iphoneAppStoreId);
       return this;
     }
 
-    public APIRequestCreateProductGroup requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
+    public APIRequestCreateProduct setIphoneUrl (String iphoneUrl) {
+      this.setParam("iphone_url", iphoneUrl);
       return this;
     }
 
+    public APIRequestCreateProduct setManufacturerPartNumber (String manufacturerPartNumber) {
+      this.setParam("manufacturer_part_number", manufacturerPartNumber);
+      return this;
+    }
 
-  }
+    public APIRequestCreateProduct setName (String name) {
+      this.setParam("name", name);
+      return this;
+    }
 
-  public static class APIRequestCreateProductFeed extends APIRequest<ProductFeed> {
+    public APIRequestCreateProduct setOrderingIndex (Long orderingIndex) {
+      this.setParam("ordering_index", orderingIndex);
+      return this;
+    }
+    public APIRequestCreateProduct setOrderingIndex (String orderingIndex) {
+      this.setParam("ordering_index", orderingIndex);
+      return this;
+    }
 
-    ProductFeed lastResponse = null;
-    @Override
-    public ProductFeed getLastResponse() {
-      return lastResponse;
+    public APIRequestCreateProduct setPattern (String pattern) {
+      this.setParam("pattern", pattern);
+      return this;
     }
-    public static final String[] PARAMS = {
-      "id",
-      "country",
-      "deletion_enabled",
-      "encoding",
-      "delimiter",
-      "default_currency",
-      "file_name",
-      "name",
-      "schedule",
-      "quoted_fields",
-    };
 
-    public static final String[] FIELDS = {
-    };
+    public APIRequestCreateProduct setPrice (Long price) {
+      this.setParam("price", price);
+      return this;
+    }
+    public APIRequestCreateProduct setPrice (String price) {
+      this.setParam("price", price);
+      return this;
+    }
 
-    @Override
-    public ProductFeed parseResponse(String response) throws APIException {
-      return ProductFeed.parseResponse(response, getContext(), this).head();
+    public APIRequestCreateProduct setProductType (String productType) {
+      this.setParam("product_type", productType);
+      return this;
     }
 
-    @Override
-    public ProductFeed execute() throws APIException {
-      return execute(new HashMap<String, Object>());
+    public APIRequestCreateProduct setRetailerId (String retailerId) {
+      this.setParam("retailer_id", retailerId);
+      return this;
     }
 
-    @Override
-    public ProductFeed execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
+    public APIRequestCreateProduct setRetailerProductGroupId (String retailerProductGroupId) {
+      this.setParam("retailer_product_group_id", retailerProductGroupId);
+      return this;
     }
 
-    public APIRequestCreateProductFeed(String nodeId, APIContext context) {
-      super(context, nodeId, "/product_feeds", "POST", Arrays.asList(PARAMS));
+    public APIRequestCreateProduct setSalePrice (Long salePrice) {
+      this.setParam("sale_price", salePrice);
+      return this;
+    }
+    public APIRequestCreateProduct setSalePrice (String salePrice) {
+      this.setParam("sale_price", salePrice);
+      return this;
     }
 
-    public APIRequestCreateProductFeed setParam(String param, Object value) {
-      setParamInternal(param, value);
+    public APIRequestCreateProduct setSalePriceEndDate (String salePriceEndDate) {
+      this.setParam("sale_price_end_date", salePriceEndDate);
       return this;
     }
 
-    public APIRequestCreateProductFeed setParams(Map<String, Object> params) {
-      setParamsInternal(params);
+    public APIRequestCreateProduct setSalePriceStartDate (String salePriceStartDate) {
+      this.setParam("sale_price_start_date", salePriceStartDate);
       return this;
     }
 
+    public APIRequestCreateProduct setSize (String size) {
+      this.setParam("size", size);
+      return this;
+    }
 
-    public APIRequestCreateProductFeed setId (String id) {
-      this.setParam("id", id);
+    public APIRequestCreateProduct setStartDate (String startDate) {
+      this.setParam("start_date", startDate);
       return this;
     }
 
+    public APIRequestCreateProduct setUrl (String url) {
+      this.setParam("url", url);
+      return this;
+    }
 
-    public APIRequestCreateProductFeed setCountry (String country) {
-      this.setParam("country", country);
+    public APIRequestCreateProduct setVisibility (ProductItem.EnumVisibility visibility) {
+      this.setParam("visibility", visibility);
+      return this;
+    }
+    public APIRequestCreateProduct setVisibility (String visibility) {
+      this.setParam("visibility", visibility);
       return this;
     }
 
+    public APIRequestCreateProduct setWindowsPhoneAppId (Long windowsPhoneAppId) {
+      this.setParam("windows_phone_app_id", windowsPhoneAppId);
+      return this;
+    }
+    public APIRequestCreateProduct setWindowsPhoneAppId (String windowsPhoneAppId) {
+      this.setParam("windows_phone_app_id", windowsPhoneAppId);
+      return this;
+    }
 
-    public APIRequestCreateProductFeed setDeletionEnabled (Boolean deletionEnabled) {
-      this.setParam("deletion_enabled", deletionEnabled);
+    public APIRequestCreateProduct setWindowsPhoneAppName (String windowsPhoneAppName) {
+      this.setParam("windows_phone_app_name", windowsPhoneAppName);
       return this;
     }
 
-    public APIRequestCreateProductFeed setDeletionEnabled (String deletionEnabled) {
-      this.setParam("deletion_enabled", deletionEnabled);
+    public APIRequestCreateProduct setWindowsPhoneUrl (String windowsPhoneUrl) {
+      this.setParam("windows_phone_url", windowsPhoneUrl);
+      return this;
+    }
+
+    public APIRequestCreateProduct requestAllFields () {
+      return this.requestAllFields(true);
+    }
+
+    public APIRequestCreateProduct requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
 
-    public APIRequestCreateProductFeed setEncoding (EnumEncoding encoding) {
-      this.setParam("encoding", encoding);
-      return this;
+    @Override
+    public APIRequestCreateProduct requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
 
-    public APIRequestCreateProductFeed setEncoding (String encoding) {
-      this.setParam("encoding", encoding);
+    @Override
+    public APIRequestCreateProduct requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
 
-    public APIRequestCreateProductFeed setDelimiter (EnumDelimiter delimiter) {
-      this.setParam("delimiter", delimiter);
+    @Override
+    public APIRequestCreateProduct requestField (String field) {
+      this.requestField(field, true);
       return this;
     }
 
-    public APIRequestCreateProductFeed setDelimiter (String delimiter) {
-      this.setParam("delimiter", delimiter);
+    @Override
+    public APIRequestCreateProduct requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestCreateProductFeed setDefaultCurrency (String defaultCurrency) {
-      this.setParam("default_currency", defaultCurrency);
-      return this;
+  }
+
+  public static class APIRequestDelete extends APIRequest<APINode> {
+
+    APINode lastResponse = null;
+    @Override
+    public APINode getLastResponse() {
+      return lastResponse;
     }
+    public static final String[] PARAMS = {
+      "id",
+    };
 
+    public static final String[] FIELDS = {
+    };
 
-    public APIRequestCreateProductFeed setFileName (String fileName) {
-      this.setParam("file_name", fileName);
-      return this;
+    @Override
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
     }
 
+    @Override
+    public APINode execute() throws APIException {
+      return execute(new HashMap<String, Object>());
+    }
 
-    public APIRequestCreateProductFeed setName (String name) {
-      this.setParam("name", name);
-      return this;
+    @Override
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
     }
 
+    public APIRequestDelete(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "DELETE", Arrays.asList(PARAMS));
+    }
 
-    public APIRequestCreateProductFeed setSchedule (String schedule) {
-      this.setParam("schedule", schedule);
+    @Override
+    public APIRequestDelete setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
 
-
-    public APIRequestCreateProductFeed setQuotedFields (Boolean quotedFields) {
-      this.setParam("quoted_fields", quotedFields);
+    @Override
+    public APIRequestDelete setParams(Map<String, Object> params) {
+      setParamsInternal(params);
       return this;
     }
 
-    public APIRequestCreateProductFeed setQuotedFields (String quotedFields) {
-      this.setParam("quoted_fields", quotedFields);
+
+    public APIRequestDelete setId (String id) {
+      this.setParam("id", id);
       return this;
     }
 
-    public APIRequestCreateProductFeed requestAllFields () {
+    public APIRequestDelete requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestCreateProductFeed requestAllFields (boolean value) {
+    public APIRequestDelete requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestCreateProductFeed requestFields (List<String> fields) {
+    @Override
+    public APIRequestDelete requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestCreateProductFeed requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestDelete requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestCreateProductFeed requestField (String field) {
+    @Override
+    public APIRequestDelete requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestCreateProductFeed requestField (String field, boolean value) {
+    @Override
+    public APIRequestDelete requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
   }
 
-  public static class APIRequestCreateProductSet extends APIRequest<ProductSet> {
+  public static class APIRequestGet extends APIRequest<ProductCatalog> {
 
-    ProductSet lastResponse = null;
+    ProductCatalog lastResponse = null;
     @Override
-    public ProductSet getLastResponse() {
+    public ProductCatalog getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "filter",
-      "id",
-      "name",
     };
 
     public static final String[] FIELDS = {
+      "business",
+      "feed_count",
+      "id",
+      "name",
+      "product_count",
     };
 
     @Override
-    public ProductSet parseResponse(String response) throws APIException {
-      return ProductSet.parseResponse(response, getContext(), this).head();
+    public ProductCatalog parseResponse(String response) throws APIException {
+      return ProductCatalog.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public ProductSet execute() throws APIException {
+    public ProductCatalog execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public ProductSet execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public ProductCatalog execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestCreateProductSet(String nodeId, APIContext context) {
-      super(context, nodeId, "/product_sets", "POST", Arrays.asList(PARAMS));
+    public APIRequestGet(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestCreateProductSet setParam(String param, Object value) {
+    @Override
+    public APIRequestGet setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestCreateProductSet setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGet setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestCreateProductSet setFilter (String filter) {
-      this.setParam("filter", filter);
-      return this;
-    }
-
-
-    public APIRequestCreateProductSet setId (String id) {
-      this.setParam("id", id);
-      return this;
-    }
-
-
-    public APIRequestCreateProductSet setName (String name) {
-      this.setParam("name", name);
-      return this;
-    }
-
-
-    public APIRequestCreateProductSet requestAllFields () {
+    public APIRequestGet requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestCreateProductSet requestAllFields (boolean value) {
+    public APIRequestGet requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestCreateProductSet requestFields (List<String> fields) {
+    @Override
+    public APIRequestGet requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestCreateProductSet requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGet requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestCreateProductSet requestField (String field) {
+    @Override
+    public APIRequestGet requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestCreateProductSet requestField (String field, boolean value) {
+    @Override
+    public APIRequestGet requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
+    public APIRequestGet requestBusinessField () {
+      return this.requestBusinessField(true);
+    }
+    public APIRequestGet requestBusinessField (boolean value) {
+      this.requestField("business", value);
+      return this;
+    }
+    public APIRequestGet requestFeedCountField () {
+      return this.requestFeedCountField(true);
+    }
+    public APIRequestGet requestFeedCountField (boolean value) {
+      this.requestField("feed_count", value);
+      return this;
+    }
+    public APIRequestGet requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGet requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
+    public APIRequestGet requestNameField () {
+      return this.requestNameField(true);
+    }
+    public APIRequestGet requestNameField (boolean value) {
+      this.requestField("name", value);
+      return this;
+    }
+    public APIRequestGet requestProductCountField () {
+      return this.requestProductCountField(true);
+    }
+    public APIRequestGet requestProductCountField (boolean value) {
+      this.requestField("product_count", value);
+      return this;
+    }
   }
 
-  public static class APIRequestDeleteExternalEventSources extends APIRequest<ExternalEventSource> {
+  public static class APIRequestUpdate extends APIRequest<APINode> {
 
-    APINodeList<ExternalEventSource> lastResponse = null;
+    APINode lastResponse = null;
     @Override
-    public APINodeList<ExternalEventSource> getLastResponse() {
+    public APINode getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
       "id",
-      "external_event_sources",
+      "name",
     };
 
     public static final String[] FIELDS = {
     };
 
     @Override
-    public APINodeList<ExternalEventSource> parseResponse(String response) throws APIException {
-      return ExternalEventSource.parseResponse(response, getContext(), this);
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public APINodeList<ExternalEventSource> execute() throws APIException {
+    public APINode execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<ExternalEventSource> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestDeleteExternalEventSources(String nodeId, APIContext context) {
-      super(context, nodeId, "/external_event_sources", "DELETE", Arrays.asList(PARAMS));
+    public APIRequestUpdate(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "POST", Arrays.asList(PARAMS));
     }
 
-    public APIRequestDeleteExternalEventSources setParam(String param, Object value) {
+    @Override
+    public APIRequestUpdate setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestDeleteExternalEventSources setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestUpdate setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestDeleteExternalEventSources setId (String id) {
+    public APIRequestUpdate setId (String id) {
       this.setParam("id", id);
       return this;
     }
 
-
-    public APIRequestDeleteExternalEventSources setExternalEventSources (List<String> externalEventSources) {
-      this.setParam("external_event_sources", externalEventSources);
-      return this;
-    }
-
-    public APIRequestDeleteExternalEventSources setExternalEventSources (String externalEventSources) {
-      this.setParam("external_event_sources", externalEventSources);
+    public APIRequestUpdate setName (String name) {
+      this.setParam("name", name);
       return this;
     }
 
-    public APIRequestDeleteExternalEventSources requestAllFields () {
+    public APIRequestUpdate requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestDeleteExternalEventSources requestAllFields (boolean value) {
+    public APIRequestUpdate requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestDeleteExternalEventSources requestFields (List<String> fields) {
+    @Override
+    public APIRequestUpdate requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestDeleteExternalEventSources requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestUpdate requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestDeleteExternalEventSources requestField (String field) {
+    @Override
+    public APIRequestUpdate requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestDeleteExternalEventSources requestField (String field, boolean value) {
+    @Override
+    public APIRequestUpdate requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
-  }
-
-  public static enum EnumEncoding {
-    @SerializedName("AUTODETECT")
-    VALUE_AUTODETECT("AUTODETECT"),
-    @SerializedName("LATIN1")
-    VALUE_LATIN1("LATIN1"),
-    @SerializedName("UTF8")
-    VALUE_UTF8("UTF8"),
-    @SerializedName("UTF16LE")
-    VALUE_UTF16LE("UTF16LE"),
-    @SerializedName("UTF16BE")
-    VALUE_UTF16BE("UTF16BE"),
-    @SerializedName("UTF32LE")
-    VALUE_UTF32LE("UTF32LE"),
-    @SerializedName("UTF32BE")
-    VALUE_UTF32BE("UTF32BE"),
-    NULL(null);
-
-    private String value;
-
-    private EnumEncoding(String value) {
-      this.value = value;
-    }
-
-    @Override
-    public String toString() {
-      return value;
-    }
   }
-  public static enum EnumDelimiter {
-    @SerializedName("AUTODETECT")
-    VALUE_AUTODETECT("AUTODETECT"),
-    @SerializedName("BAR")
-    VALUE_BAR("BAR"),
-    @SerializedName("COMMA")
-    VALUE_COMMA("COMMA"),
-    @SerializedName("TAB")
-    VALUE_TAB("TAB"),
-    @SerializedName("TILDE")
-    VALUE_TILDE("TILDE"),
-    NULL(null);
 
-    private String value;
-
-    private EnumDelimiter(String value) {
-      this.value = value;
-    }
-
-    @Override
-    public String toString() {
-      return value;
-    }
-  }
 
   synchronized /*package*/ static Gson getGson() {
     if (gson != null) {
@@ -2163,19 +2756,19 @@ public class ProductCatalog extends APINode {
   }
 
   public ProductCatalog copyFrom(ProductCatalog instance) {
-    this.mId = instance.mId;
     this.mBusiness = instance.mBusiness;
     this.mFeedCount = instance.mFeedCount;
+    this.mId = instance.mId;
     this.mName = instance.mName;
     this.mProductCount = instance.mProductCount;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<ProductCatalog> getParser() {
     return new APIRequest.ResponseParser<ProductCatalog>() {
-      public APINodeList<ProductCatalog> parseResponse(String response, APIContext context, APIRequest<ProductCatalog> request) {
+      public APINodeList<ProductCatalog> parseResponse(String response, APIContext context, APIRequest<ProductCatalog> request) throws MalformedResponseException {
         return ProductCatalog.parseResponse(response, context, request);
       }
     };
