@@ -22,8 +22,11 @@
  */
 package com.facebook.ads.sdk;
 
+import com.google.gson.JsonObject;
+
 public interface APIResponse {
   public String getRawResponse();
+  public JsonObject getRawResponseAsJsonObject();
   public APINode head();
   public APIException getException();
 }
