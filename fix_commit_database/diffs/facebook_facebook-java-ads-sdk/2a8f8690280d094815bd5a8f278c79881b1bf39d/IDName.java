@@ -24,29 +24,32 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class IDName extends APINode {
   @SerializedName("id")
   private String mId = null;
@@ -61,85 +64,130 @@ public class IDName extends APINode {
     return getFieldId().toString();
   }
   public static IDName loadJSON(String json, APIContext context) {
-    IDName IDName = getGson().fromJson(json, IDName.class);
+    IDName idName = getGson().fromJson(json, IDName.class);
     if (context.isDebug()) {
       JsonParser parser = new JsonParser();
       JsonElement o1 = parser.parse(json);
-      JsonElement o2 = parser.parse(IDName.toString());
+      JsonElement o2 = parser.parse(idName.toString());
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    IDName.mContext = context;
-    IDName.rawValue = json;
-    return IDName;
+    idName.context = context;
+    idName.rawValue = json;
+    return idName;
   }
 
-  public static APINodeList<IDName> parseResponse(String json, APIContext context, APIRequest request) {
-    APINodeList<IDName> IDNames = new APINodeList<IDName>(request, json);
+  public static APINodeList<IDName> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
+    APINodeList<IDName> idNames = new APINodeList<IDName>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
         // First, check if it's a pure JSON Array
         arr = result.getAsJsonArray();
         for (int i = 0; i < arr.size(); i++) {
-          IDNames.add(loadJSON(arr.get(i).getAsJsonObject().toString(), context));
+          idNames.add(loadJSON(arr.get(i).getAsJsonObject().toString(), context));
         };
-        return IDNames;
+        return idNames;
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            IDNames.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            idNames.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
             arr = obj.get("data").getAsJsonArray();
             for (int i = 0; i < arr.size(); i++) {
-              IDNames.add(loadJSON(arr.get(i).getAsJsonObject().toString(), context));
+              idNames.add(loadJSON(arr.get(i).getAsJsonObject().toString(), context));
             };
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            IDNames.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  idNames.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              idNames.add(loadJSON(obj.toString(), context));
+            }
           }
-          return IDNames;
+          return idNames;
         } else if (obj.has("images")) {
           // Fourth, check if it's a map of image objects
           obj = obj.get("images").getAsJsonObject();
           for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
-              IDNames.add(loadJSON(entry.getValue().toString(), context));
+              idNames.add(loadJSON(entry.getValue().toString(), context));
           }
-          return IDNames;
+          return idNames;
         } else {
-          // Fifth, check if it's pure JsonObject
-          IDNames.add(loadJSON(json, context));
-          return IDNames;
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              idNames.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return idNames;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          idNames.clear();
+          idNames.add(loadJSON(json, context));
+          return idNames;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -185,14 +233,14 @@ public class IDName extends APINode {
   public IDName copyFrom(IDName instance) {
     this.mId = instance.mId;
     this.mName = instance.mName;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<IDName> getParser() {
     return new APIRequest.ResponseParser<IDName>() {
-      public APINodeList<IDName> parseResponse(String response, APIContext context, APIRequest<IDName> request) {
+      public APINodeList<IDName> parseResponse(String response, APIContext context, APIRequest<IDName> request) throws MalformedResponseException {
         return IDName.parseResponse(response, context, request);
       }
     };
