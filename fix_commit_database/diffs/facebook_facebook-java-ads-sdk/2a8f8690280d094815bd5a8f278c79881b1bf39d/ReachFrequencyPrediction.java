@@ -24,88 +24,99 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class ReachFrequencyPrediction extends APINode {
-  @SerializedName("id")
-  private String mId = null;
+  @SerializedName("account_id")
+  private Long mAccountId = null;
+  @SerializedName("campaign_group_id")
+  private Long mCampaignGroupId = null;
   @SerializedName("campaign_id")
   private String mCampaignId = null;
-  @SerializedName("account_id")
-  private String mAccountId = null;
-  @SerializedName("time_created")
-  private String mTimeCreated = null;
-  @SerializedName("frequency_cap")
-  private Long mFrequencyCap = null;
+  @SerializedName("campaign_time_start")
+  private String mCampaignTimeStart = null;
+  @SerializedName("campaign_time_stop")
+  private String mCampaignTimeStop = null;
+  @SerializedName("curve_budget_reach")
+  private String mCurveBudgetReach = null;
+  @SerializedName("destination_id")
+  private String mDestinationId = null;
   @SerializedName("expiration_time")
   private String mExpirationTime = null;
-  @SerializedName("external_reach")
-  private Long mExternalReach = null;
   @SerializedName("external_budget")
-  private String mExternalBudget = null;
+  private Long mExternalBudget = null;
   @SerializedName("external_impression")
   private Long mExternalImpression = null;
-  @SerializedName("external_maximum_reach")
-  private Long mExternalMaximumReach = null;
+  @SerializedName("external_maximum_budget")
+  private Long mExternalMaximumBudget = null;
   @SerializedName("external_maximum_impression")
   private Long mExternalMaximumImpression = null;
-  @SerializedName("external_maximum_budget")
-  private String mExternalMaximumBudget = null;
-  @SerializedName("target_spec")
-  private String mTargetSpec = null;
-  @SerializedName("target_audience_size")
-  private Long mTargetAudienceSize = null;
+  @SerializedName("external_maximum_reach")
+  private Long mExternalMaximumReach = null;
+  @SerializedName("external_minimum_budget")
+  private Long mExternalMinimumBudget = null;
+  @SerializedName("external_minimum_impression")
+  private Long mExternalMinimumImpression = null;
+  @SerializedName("external_minimum_reach")
+  private Long mExternalMinimumReach = null;
+  @SerializedName("external_reach")
+  private Long mExternalReach = null;
+  @SerializedName("frequency_cap")
+  private Long mFrequencyCap = null;
+  @SerializedName("grp_dmas_audience_size")
+  private Double mGrpDmasAudienceSize = null;
+  @SerializedName("holdout_percentage")
+  private Long mHoldoutPercentage = null;
+  @SerializedName("id")
+  private String mId = null;
+  @SerializedName("instagram_destination_id")
+  private String mInstagramDestinationId = null;
+  @SerializedName("interval_frequency_cap_reset_period")
+  private Long mIntervalFrequencyCapResetPeriod = null;
+  @SerializedName("name")
+  private String mName = null;
   @SerializedName("prediction_mode")
   private Long mPredictionMode = null;
   @SerializedName("prediction_progress")
   private Long mPredictionProgress = null;
-  @SerializedName("time_updated")
-  private String mTimeUpdated = null;
-  @SerializedName("status")
-  private Long mStatus = null;
-  @SerializedName("campaign_time_start")
-  private String mCampaignTimeStart = null;
-  @SerializedName("campaign_time_stop")
-  private String mCampaignTimeStop = null;
-  @SerializedName("external_minimum_budget")
-  private String mExternalMinimumBudget = null;
-  @SerializedName("external_minimum_reach")
-  private Long mExternalMinimumReach = null;
-  @SerializedName("external_minimum_impression")
-  private Long mExternalMinimumImpression = null;
   @SerializedName("reservation_status")
   private Long mReservationStatus = null;
+  @SerializedName("status")
+  private Long mStatus = null;
   @SerializedName("story_event_type")
   private Long mStoryEventType = null;
-  @SerializedName("curve_budget_reach")
-  private String mCurveBudgetReach = null;
-  @SerializedName("holdout_percentage")
-  private Long mHoldoutPercentage = null;
-  @SerializedName("campaign_group_id")
-  private Long mCampaignGroupId = null;
-  @SerializedName("name")
-  private String mName = null;
+  @SerializedName("target_audience_size")
+  private Long mTargetAudienceSize = null;
+  @SerializedName("target_spec")
+  private String mTargetSpec = null;
+  @SerializedName("time_created")
+  private String mTimeCreated = null;
+  @SerializedName("time_updated")
+  private String mTimeUpdated = null;
   protected static Gson gson = null;
 
   ReachFrequencyPrediction() {
@@ -117,11 +128,11 @@ public class ReachFrequencyPrediction extends APINode {
 
   public ReachFrequencyPrediction(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public ReachFrequencyPrediction fetch() throws APIException{
-    ReachFrequencyPrediction newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    ReachFrequencyPrediction newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -138,8 +149,17 @@ public class ReachFrequencyPrediction extends APINode {
     return reachFrequencyPrediction;
   }
 
+  public static APINodeList<ReachFrequencyPrediction> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<ReachFrequencyPrediction>)(
+      new APIRequest<ReachFrequencyPrediction>(context, "", "/", "GET", ReachFrequencyPrediction.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -154,22 +174,23 @@ public class ReachFrequencyPrediction extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    reachFrequencyPrediction.mContext = context;
+    reachFrequencyPrediction.context = context;
     reachFrequencyPrediction.rawValue = json;
     return reachFrequencyPrediction;
   }
 
-  public static APINodeList<ReachFrequencyPrediction> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<ReachFrequencyPrediction> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<ReachFrequencyPrediction> reachFrequencyPredictions = new APINodeList<ReachFrequencyPrediction>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -182,10 +203,11 @@ public class ReachFrequencyPrediction extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            reachFrequencyPredictions.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            reachFrequencyPredictions.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -196,7 +218,20 @@ public class ReachFrequencyPrediction extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            reachFrequencyPredictions.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  reachFrequencyPredictions.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              reachFrequencyPredictions.add(loadJSON(obj.toString(), context));
+            }
           }
           return reachFrequencyPredictions;
         } else if (obj.has("images")) {
@@ -207,24 +242,54 @@ public class ReachFrequencyPrediction extends APINode {
           }
           return reachFrequencyPredictions;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              reachFrequencyPredictions.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return reachFrequencyPredictions;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          reachFrequencyPredictions.clear();
           reachFrequencyPredictions.add(loadJSON(json, context));
           return reachFrequencyPredictions;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -233,39 +298,43 @@ public class ReachFrequencyPrediction extends APINode {
   }
 
   public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
   }
 
 
-  public String getFieldId() {
-    return mId;
+  public Long getFieldAccountId() {
+    return mAccountId;
+  }
+
+  public Long getFieldCampaignGroupId() {
+    return mCampaignGroupId;
   }
 
   public String getFieldCampaignId() {
     return mCampaignId;
   }
 
-  public String getFieldAccountId() {
-    return mAccountId;
+  public String getFieldCampaignTimeStart() {
+    return mCampaignTimeStart;
   }
 
-  public String getFieldTimeCreated() {
-    return mTimeCreated;
+  public String getFieldCampaignTimeStop() {
+    return mCampaignTimeStop;
   }
 
-  public Long getFieldFrequencyCap() {
-    return mFrequencyCap;
+  public String getFieldCurveBudgetReach() {
+    return mCurveBudgetReach;
   }
 
-  public String getFieldExpirationTime() {
-    return mExpirationTime;
+  public String getFieldDestinationId() {
+    return mDestinationId;
   }
 
-  public Long getFieldExternalReach() {
-    return mExternalReach;
+  public String getFieldExpirationTime() {
+    return mExpirationTime;
   }
 
-  public String getFieldExternalBudget() {
+  public Long getFieldExternalBudget() {
     return mExternalBudget;
   }
 
@@ -273,84 +342,96 @@ public class ReachFrequencyPrediction extends APINode {
     return mExternalImpression;
   }
 
-  public Long getFieldExternalMaximumReach() {
-    return mExternalMaximumReach;
+  public Long getFieldExternalMaximumBudget() {
+    return mExternalMaximumBudget;
   }
 
   public Long getFieldExternalMaximumImpression() {
     return mExternalMaximumImpression;
   }
 
-  public String getFieldExternalMaximumBudget() {
-    return mExternalMaximumBudget;
+  public Long getFieldExternalMaximumReach() {
+    return mExternalMaximumReach;
   }
 
-  public String getFieldTargetSpec() {
-    return mTargetSpec;
+  public Long getFieldExternalMinimumBudget() {
+    return mExternalMinimumBudget;
   }
 
-  public Long getFieldTargetAudienceSize() {
-    return mTargetAudienceSize;
+  public Long getFieldExternalMinimumImpression() {
+    return mExternalMinimumImpression;
   }
 
-  public Long getFieldPredictionMode() {
-    return mPredictionMode;
+  public Long getFieldExternalMinimumReach() {
+    return mExternalMinimumReach;
   }
 
-  public Long getFieldPredictionProgress() {
-    return mPredictionProgress;
+  public Long getFieldExternalReach() {
+    return mExternalReach;
   }
 
-  public String getFieldTimeUpdated() {
-    return mTimeUpdated;
+  public Long getFieldFrequencyCap() {
+    return mFrequencyCap;
   }
 
-  public Long getFieldStatus() {
-    return mStatus;
+  public Double getFieldGrpDmasAudienceSize() {
+    return mGrpDmasAudienceSize;
   }
 
-  public String getFieldCampaignTimeStart() {
-    return mCampaignTimeStart;
+  public Long getFieldHoldoutPercentage() {
+    return mHoldoutPercentage;
   }
 
-  public String getFieldCampaignTimeStop() {
-    return mCampaignTimeStop;
+  public String getFieldId() {
+    return mId;
   }
 
-  public String getFieldExternalMinimumBudget() {
-    return mExternalMinimumBudget;
+  public String getFieldInstagramDestinationId() {
+    return mInstagramDestinationId;
   }
 
-  public Long getFieldExternalMinimumReach() {
-    return mExternalMinimumReach;
+  public Long getFieldIntervalFrequencyCapResetPeriod() {
+    return mIntervalFrequencyCapResetPeriod;
   }
 
-  public Long getFieldExternalMinimumImpression() {
-    return mExternalMinimumImpression;
+  public String getFieldName() {
+    return mName;
+  }
+
+  public Long getFieldPredictionMode() {
+    return mPredictionMode;
+  }
+
+  public Long getFieldPredictionProgress() {
+    return mPredictionProgress;
   }
 
   public Long getFieldReservationStatus() {
     return mReservationStatus;
   }
 
+  public Long getFieldStatus() {
+    return mStatus;
+  }
+
   public Long getFieldStoryEventType() {
     return mStoryEventType;
   }
 
-  public String getFieldCurveBudgetReach() {
-    return mCurveBudgetReach;
+  public Long getFieldTargetAudienceSize() {
+    return mTargetAudienceSize;
   }
 
-  public Long getFieldHoldoutPercentage() {
-    return mHoldoutPercentage;
+  public String getFieldTargetSpec() {
+    return mTargetSpec;
   }
 
-  public Long getFieldCampaignGroupId() {
-    return mCampaignGroupId;
+  public String getFieldTimeCreated() {
+    return mTimeCreated;
   }
 
-  public String getFieldName() {
-    return mName;
+  public String getFieldTimeUpdated() {
+    return mTimeUpdated;
   }
 
 
@@ -366,35 +447,39 @@ public class ReachFrequencyPrediction extends APINode {
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "campaign_id",
       "account_id",
-      "time_created",
-      "frequency_cap",
+      "campaign_group_id",
+      "campaign_id",
+      "campaign_time_start",
+      "campaign_time_stop",
+      "curve_budget_reach",
+      "destination_id",
       "expiration_time",
-      "external_reach",
       "external_budget",
       "external_impression",
-      "external_maximum_reach",
-      "external_maximum_impression",
       "external_maximum_budget",
-      "target_spec",
-      "target_audience_size",
-      "prediction_mode",
-      "prediction_progress",
-      "time_updated",
-      "status",
-      "campaign_time_start",
-      "campaign_time_stop",
+      "external_maximum_impression",
+      "external_maximum_reach",
       "external_minimum_budget",
-      "external_minimum_reach",
       "external_minimum_impression",
-      "reservation_status",
-      "story_event_type",
-      "curve_budget_reach",
+      "external_minimum_reach",
+      "external_reach",
+      "frequency_cap",
+      "grp_dmas_audience_size",
       "holdout_percentage",
-      "campaign_group_id",
+      "id",
+      "instagram_destination_id",
+      "interval_frequency_cap_reset_period",
       "name",
+      "prediction_mode",
+      "prediction_progress",
+      "reservation_status",
+      "status",
+      "story_event_type",
+      "target_audience_size",
+      "target_spec",
+      "time_created",
+      "time_updated",
     };
 
     @Override
@@ -409,7 +494,7 @@ public class ReachFrequencyPrediction extends APINode {
 
     @Override
     public ReachFrequencyPrediction execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -417,11 +502,13 @@ public class ReachFrequencyPrediction extends APINode {
       super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestGet setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestGet setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
@@ -439,10 +526,12 @@ public class ReachFrequencyPrediction extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGet requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestGet requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -450,21 +539,30 @@ public class ReachFrequencyPrediction extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGet requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGet requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGet requestIdField () {
-      return this.requestIdField(true);
+    public APIRequestGet requestAccountIdField () {
+      return this.requestAccountIdField(true);
     }
-    public APIRequestGet requestIdField (boolean value) {
-      this.requestField("id", value);
+    public APIRequestGet requestAccountIdField (boolean value) {
+      this.requestField("account_id", value);
+      return this;
+    }
+    public APIRequestGet requestCampaignGroupIdField () {
+      return this.requestCampaignGroupIdField(true);
+    }
+    public APIRequestGet requestCampaignGroupIdField (boolean value) {
+      this.requestField("campaign_group_id", value);
       return this;
     }
     public APIRequestGet requestCampaignIdField () {
@@ -474,25 +572,32 @@ public class ReachFrequencyPrediction extends APINode {
       this.requestField("campaign_id", value);
       return this;
     }
-    public APIRequestGet requestAccountIdField () {
-      return this.requestAccountIdField(true);
+    public APIRequestGet requestCampaignTimeStartField () {
+      return this.requestCampaignTimeStartField(true);
     }
-    public APIRequestGet requestAccountIdField (boolean value) {
-      this.requestField("account_id", value);
+    public APIRequestGet requestCampaignTimeStartField (boolean value) {
+      this.requestField("campaign_time_start", value);
       return this;
     }
-    public APIRequestGet requestTimeCreatedField () {
-      return this.requestTimeCreatedField(true);
+    public APIRequestGet requestCampaignTimeStopField () {
+      return this.requestCampaignTimeStopField(true);
     }
-    public APIRequestGet requestTimeCreatedField (boolean value) {
-      this.requestField("time_created", value);
+    public APIRequestGet requestCampaignTimeStopField (boolean value) {
+      this.requestField("campaign_time_stop", value);
       return this;
     }
-    public APIRequestGet requestFrequencyCapField () {
-      return this.requestFrequencyCapField(true);
+    public APIRequestGet requestCurveBudgetReachField () {
+      return this.requestCurveBudgetReachField(true);
     }
-    public APIRequestGet requestFrequencyCapField (boolean value) {
-      this.requestField("frequency_cap", value);
+    public APIRequestGet requestCurveBudgetReachField (boolean value) {
+      this.requestField("curve_budget_reach", value);
+      return this;
+    }
+    public APIRequestGet requestDestinationIdField () {
+      return this.requestDestinationIdField(true);
+    }
+    public APIRequestGet requestDestinationIdField (boolean value) {
+      this.requestField("destination_id", value);
       return this;
     }
     public APIRequestGet requestExpirationTimeField () {
@@ -502,13 +607,6 @@ public class ReachFrequencyPrediction extends APINode {
       this.requestField("expiration_time", value);
       return this;
     }
-    public APIRequestGet requestExternalReachField () {
-      return this.requestExternalReachField(true);
-    }
-    public APIRequestGet requestExternalReachField (boolean value) {
-      this.requestField("external_reach", value);
-      return this;
-    }
     public APIRequestGet requestExternalBudgetField () {
       return this.requestExternalBudgetField(true);
     }
@@ -523,11 +621,11 @@ public class ReachFrequencyPrediction extends APINode {
       this.requestField("external_impression", value);
       return this;
     }
-    public APIRequestGet requestExternalMaximumReachField () {
-      return this.requestExternalMaximumReachField(true);
+    public APIRequestGet requestExternalMaximumBudgetField () {
+      return this.requestExternalMaximumBudgetField(true);
     }
-    public APIRequestGet requestExternalMaximumReachField (boolean value) {
-      this.requestField("external_maximum_reach", value);
+    public APIRequestGet requestExternalMaximumBudgetField (boolean value) {
+      this.requestField("external_maximum_budget", value);
       return this;
     }
     public APIRequestGet requestExternalMaximumImpressionField () {
@@ -537,88 +635,102 @@ public class ReachFrequencyPrediction extends APINode {
       this.requestField("external_maximum_impression", value);
       return this;
     }
-    public APIRequestGet requestExternalMaximumBudgetField () {
-      return this.requestExternalMaximumBudgetField(true);
+    public APIRequestGet requestExternalMaximumReachField () {
+      return this.requestExternalMaximumReachField(true);
     }
-    public APIRequestGet requestExternalMaximumBudgetField (boolean value) {
-      this.requestField("external_maximum_budget", value);
+    public APIRequestGet requestExternalMaximumReachField (boolean value) {
+      this.requestField("external_maximum_reach", value);
       return this;
     }
-    public APIRequestGet requestTargetSpecField () {
-      return this.requestTargetSpecField(true);
+    public APIRequestGet requestExternalMinimumBudgetField () {
+      return this.requestExternalMinimumBudgetField(true);
     }
-    public APIRequestGet requestTargetSpecField (boolean value) {
-      this.requestField("target_spec", value);
+    public APIRequestGet requestExternalMinimumBudgetField (boolean value) {
+      this.requestField("external_minimum_budget", value);
       return this;
     }
-    public APIRequestGet requestTargetAudienceSizeField () {
-      return this.requestTargetAudienceSizeField(true);
+    public APIRequestGet requestExternalMinimumImpressionField () {
+      return this.requestExternalMinimumImpressionField(true);
     }
-    public APIRequestGet requestTargetAudienceSizeField (boolean value) {
-      this.requestField("target_audience_size", value);
+    public APIRequestGet requestExternalMinimumImpressionField (boolean value) {
+      this.requestField("external_minimum_impression", value);
       return this;
     }
-    public APIRequestGet requestPredictionModeField () {
-      return this.requestPredictionModeField(true);
+    public APIRequestGet requestExternalMinimumReachField () {
+      return this.requestExternalMinimumReachField(true);
     }
-    public APIRequestGet requestPredictionModeField (boolean value) {
-      this.requestField("prediction_mode", value);
+    public APIRequestGet requestExternalMinimumReachField (boolean value) {
+      this.requestField("external_minimum_reach", value);
       return this;
     }
-    public APIRequestGet requestPredictionProgressField () {
-      return this.requestPredictionProgressField(true);
+    public APIRequestGet requestExternalReachField () {
+      return this.requestExternalReachField(true);
     }
-    public APIRequestGet requestPredictionProgressField (boolean value) {
-      this.requestField("prediction_progress", value);
+    public APIRequestGet requestExternalReachField (boolean value) {
+      this.requestField("external_reach", value);
       return this;
     }
-    public APIRequestGet requestTimeUpdatedField () {
-      return this.requestTimeUpdatedField(true);
+    public APIRequestGet requestFrequencyCapField () {
+      return this.requestFrequencyCapField(true);
     }
-    public APIRequestGet requestTimeUpdatedField (boolean value) {
-      this.requestField("time_updated", value);
+    public APIRequestGet requestFrequencyCapField (boolean value) {
+      this.requestField("frequency_cap", value);
       return this;
     }
-    public APIRequestGet requestStatusField () {
-      return this.requestStatusField(true);
+    public APIRequestGet requestGrpDmasAudienceSizeField () {
+      return this.requestGrpDmasAudienceSizeField(true);
     }
-    public APIRequestGet requestStatusField (boolean value) {
-      this.requestField("status", value);
+    public APIRequestGet requestGrpDmasAudienceSizeField (boolean value) {
+      this.requestField("grp_dmas_audience_size", value);
       return this;
     }
-    public APIRequestGet requestCampaignTimeStartField () {
-      return this.requestCampaignTimeStartField(true);
+    public APIRequestGet requestHoldoutPercentageField () {
+      return this.requestHoldoutPercentageField(true);
     }
-    public APIRequestGet requestCampaignTimeStartField (boolean value) {
-      this.requestField("campaign_time_start", value);
+    public APIRequestGet requestHoldoutPercentageField (boolean value) {
+      this.requestField("holdout_percentage", value);
       return this;
     }
-    public APIRequestGet requestCampaignTimeStopField () {
-      return this.requestCampaignTimeStopField(true);
+    public APIRequestGet requestIdField () {
+      return this.requestIdField(true);
     }
-    public APIRequestGet requestCampaignTimeStopField (boolean value) {
-      this.requestField("campaign_time_stop", value);
+    public APIRequestGet requestIdField (boolean value) {
+      this.requestField("id", value);
       return this;
     }
-    public APIRequestGet requestExternalMinimumBudgetField () {
-      return this.requestExternalMinimumBudgetField(true);
+    public APIRequestGet requestInstagramDestinationIdField () {
+      return this.requestInstagramDestinationIdField(true);
     }
-    public APIRequestGet requestExternalMinimumBudgetField (boolean value) {
-      this.requestField("external_minimum_budget", value);
+    public APIRequestGet requestInstagramDestinationIdField (boolean value) {
+      this.requestField("instagram_destination_id", value);
       return this;
     }
-    public APIRequestGet requestExternalMinimumReachField () {
-      return this.requestExternalMinimumReachField(true);
+    public APIRequestGet requestIntervalFrequencyCapResetPeriodField () {
+      return this.requestIntervalFrequencyCapResetPeriodField(true);
     }
-    public APIRequestGet requestExternalMinimumReachField (boolean value) {
-      this.requestField("external_minimum_reach", value);
+    public APIRequestGet requestIntervalFrequencyCapResetPeriodField (boolean value) {
+      this.requestField("interval_frequency_cap_reset_period", value);
       return this;
     }
-    public APIRequestGet requestExternalMinimumImpressionField () {
-      return this.requestExternalMinimumImpressionField(true);
+    public APIRequestGet requestNameField () {
+      return this.requestNameField(true);
     }
-    public APIRequestGet requestExternalMinimumImpressionField (boolean value) {
-      this.requestField("external_minimum_impression", value);
+    public APIRequestGet requestNameField (boolean value) {
+      this.requestField("name", value);
+      return this;
+    }
+    public APIRequestGet requestPredictionModeField () {
+      return this.requestPredictionModeField(true);
+    }
+    public APIRequestGet requestPredictionModeField (boolean value) {
+      this.requestField("prediction_mode", value);
+      return this;
+    }
+    public APIRequestGet requestPredictionProgressField () {
+      return this.requestPredictionProgressField(true);
+    }
+    public APIRequestGet requestPredictionProgressField (boolean value) {
+      this.requestField("prediction_progress", value);
       return this;
     }
     public APIRequestGet requestReservationStatusField () {
@@ -628,6 +740,13 @@ public class ReachFrequencyPrediction extends APINode {
       this.requestField("reservation_status", value);
       return this;
     }
+    public APIRequestGet requestStatusField () {
+      return this.requestStatusField(true);
+    }
+    public APIRequestGet requestStatusField (boolean value) {
+      this.requestField("status", value);
+      return this;
+    }
     public APIRequestGet requestStoryEventTypeField () {
       return this.requestStoryEventTypeField(true);
     }
@@ -635,35 +754,59 @@ public class ReachFrequencyPrediction extends APINode {
       this.requestField("story_event_type", value);
       return this;
     }
-    public APIRequestGet requestCurveBudgetReachField () {
-      return this.requestCurveBudgetReachField(true);
+    public APIRequestGet requestTargetAudienceSizeField () {
+      return this.requestTargetAudienceSizeField(true);
     }
-    public APIRequestGet requestCurveBudgetReachField (boolean value) {
-      this.requestField("curve_budget_reach", value);
+    public APIRequestGet requestTargetAudienceSizeField (boolean value) {
+      this.requestField("target_audience_size", value);
       return this;
     }
-    public APIRequestGet requestHoldoutPercentageField () {
-      return this.requestHoldoutPercentageField(true);
+    public APIRequestGet requestTargetSpecField () {
+      return this.requestTargetSpecField(true);
     }
-    public APIRequestGet requestHoldoutPercentageField (boolean value) {
-      this.requestField("holdout_percentage", value);
+    public APIRequestGet requestTargetSpecField (boolean value) {
+      this.requestField("target_spec", value);
       return this;
     }
-    public APIRequestGet requestCampaignGroupIdField () {
-      return this.requestCampaignGroupIdField(true);
+    public APIRequestGet requestTimeCreatedField () {
+      return this.requestTimeCreatedField(true);
     }
-    public APIRequestGet requestCampaignGroupIdField (boolean value) {
-      this.requestField("campaign_group_id", value);
+    public APIRequestGet requestTimeCreatedField (boolean value) {
+      this.requestField("time_created", value);
       return this;
     }
-    public APIRequestGet requestNameField () {
-      return this.requestNameField(true);
+    public APIRequestGet requestTimeUpdatedField () {
+      return this.requestTimeUpdatedField(true);
     }
-    public APIRequestGet requestNameField (boolean value) {
-      this.requestField("name", value);
+    public APIRequestGet requestTimeUpdatedField (boolean value) {
+      this.requestField("time_updated", value);
       return this;
     }
+  }
 
+  public static enum EnumStatus {
+      @SerializedName("EXPIRED")
+      VALUE_EXPIRED("EXPIRED"),
+      @SerializedName("DRAFT")
+      VALUE_DRAFT("DRAFT"),
+      @SerializedName("PENDING")
+      VALUE_PENDING("PENDING"),
+      @SerializedName("ACTIVE")
+      VALUE_ACTIVE("ACTIVE"),
+      @SerializedName("COMPLETED")
+      VALUE_COMPLETED("COMPLETED"),
+      NULL(null);
+
+      private String value;
+
+      private EnumStatus(String value) {
+        this.value = value;
+      }
+
+      @Override
+      public String toString() {
+        return value;
+      }
   }
 
 
@@ -681,43 +824,47 @@ public class ReachFrequencyPrediction extends APINode {
   }
 
   public ReachFrequencyPrediction copyFrom(ReachFrequencyPrediction instance) {
-    this.mId = instance.mId;
-    this.mCampaignId = instance.mCampaignId;
     this.mAccountId = instance.mAccountId;
-    this.mTimeCreated = instance.mTimeCreated;
-    this.mFrequencyCap = instance.mFrequencyCap;
+    this.mCampaignGroupId = instance.mCampaignGroupId;
+    this.mCampaignId = instance.mCampaignId;
+    this.mCampaignTimeStart = instance.mCampaignTimeStart;
+    this.mCampaignTimeStop = instance.mCampaignTimeStop;
+    this.mCurveBudgetReach = instance.mCurveBudgetReach;
+    this.mDestinationId = instance.mDestinationId;
     this.mExpirationTime = instance.mExpirationTime;
-    this.mExternalReach = instance.mExternalReach;
     this.mExternalBudget = instance.mExternalBudget;
     this.mExternalImpression = instance.mExternalImpression;
-    this.mExternalMaximumReach = instance.mExternalMaximumReach;
-    this.mExternalMaximumImpression = instance.mExternalMaximumImpression;
     this.mExternalMaximumBudget = instance.mExternalMaximumBudget;
-    this.mTargetSpec = instance.mTargetSpec;
-    this.mTargetAudienceSize = instance.mTargetAudienceSize;
-    this.mPredictionMode = instance.mPredictionMode;
-    this.mPredictionProgress = instance.mPredictionProgress;
-    this.mTimeUpdated = instance.mTimeUpdated;
-    this.mStatus = instance.mStatus;
-    this.mCampaignTimeStart = instance.mCampaignTimeStart;
-    this.mCampaignTimeStop = instance.mCampaignTimeStop;
+    this.mExternalMaximumImpression = instance.mExternalMaximumImpression;
+    this.mExternalMaximumReach = instance.mExternalMaximumReach;
     this.mExternalMinimumBudget = instance.mExternalMinimumBudget;
-    this.mExternalMinimumReach = instance.mExternalMinimumReach;
     this.mExternalMinimumImpression = instance.mExternalMinimumImpression;
-    this.mReservationStatus = instance.mReservationStatus;
-    this.mStoryEventType = instance.mStoryEventType;
-    this.mCurveBudgetReach = instance.mCurveBudgetReach;
+    this.mExternalMinimumReach = instance.mExternalMinimumReach;
+    this.mExternalReach = instance.mExternalReach;
+    this.mFrequencyCap = instance.mFrequencyCap;
+    this.mGrpDmasAudienceSize = instance.mGrpDmasAudienceSize;
     this.mHoldoutPercentage = instance.mHoldoutPercentage;
-    this.mCampaignGroupId = instance.mCampaignGroupId;
+    this.mId = instance.mId;
+    this.mInstagramDestinationId = instance.mInstagramDestinationId;
+    this.mIntervalFrequencyCapResetPeriod = instance.mIntervalFrequencyCapResetPeriod;
     this.mName = instance.mName;
-    this.mContext = instance.mContext;
+    this.mPredictionMode = instance.mPredictionMode;
+    this.mPredictionProgress = instance.mPredictionProgress;
+    this.mReservationStatus = instance.mReservationStatus;
+    this.mStatus = instance.mStatus;
+    this.mStoryEventType = instance.mStoryEventType;
+    this.mTargetAudienceSize = instance.mTargetAudienceSize;
+    this.mTargetSpec = instance.mTargetSpec;
+    this.mTimeCreated = instance.mTimeCreated;
+    this.mTimeUpdated = instance.mTimeUpdated;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<ReachFrequencyPrediction> getParser() {
     return new APIRequest.ResponseParser<ReachFrequencyPrediction>() {
-      public APINodeList<ReachFrequencyPrediction> parseResponse(String response, APIContext context, APIRequest<ReachFrequencyPrediction> request) {
+      public APINodeList<ReachFrequencyPrediction> parseResponse(String response, APIContext context, APIRequest<ReachFrequencyPrediction> request) throws MalformedResponseException {
         return ReachFrequencyPrediction.parseResponse(response, context, request);
       }
     };
