@@ -24,44 +24,51 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AdCreativeLinkDataChildAttachment extends APINode {
-  @SerializedName("link")
-  private String mLink = null;
-  @SerializedName("picture")
-  private String mPicture = null;
-  @SerializedName("image_hash")
-  private String mImageHash = null;
+  @SerializedName("call_to_action")
+  private AdCreativeLinkDataCallToAction mCallToAction = null;
+  @SerializedName("description")
+  private String mDescription = null;
   @SerializedName("image_crops")
   private AdsImageCrops mImageCrops = null;
+  @SerializedName("image_hash")
+  private String mImageHash = null;
+  @SerializedName("link")
+  private String mLink = null;
   @SerializedName("name")
   private String mName = null;
-  @SerializedName("description")
-  private String mDescription = null;
-  @SerializedName("call_to_action")
-  private AdCreativeLinkDataCallToAction mCallToAction = null;
+  @SerializedName("picture")
+  private String mPicture = null;
+  @SerializedName("place_data")
+  private Object mPlaceData = null;
+  @SerializedName("static_card")
+  private Boolean mStaticCard = null;
   @SerializedName("video_id")
   private String mVideoId = null;
   protected static Gson gson = null;
@@ -81,22 +88,23 @@ public class AdCreativeLinkDataChildAttachment extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    adCreativeLinkDataChildAttachment.mContext = context;
+    adCreativeLinkDataChildAttachment.context = context;
     adCreativeLinkDataChildAttachment.rawValue = json;
     return adCreativeLinkDataChildAttachment;
   }
 
-  public static APINodeList<AdCreativeLinkDataChildAttachment> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AdCreativeLinkDataChildAttachment> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AdCreativeLinkDataChildAttachment> adCreativeLinkDataChildAttachments = new APINodeList<AdCreativeLinkDataChildAttachment>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -109,10 +117,11 @@ public class AdCreativeLinkDataChildAttachment extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            adCreativeLinkDataChildAttachments.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            adCreativeLinkDataChildAttachments.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -123,7 +132,20 @@ public class AdCreativeLinkDataChildAttachment extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            adCreativeLinkDataChildAttachments.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  adCreativeLinkDataChildAttachments.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              adCreativeLinkDataChildAttachments.add(loadJSON(obj.toString(), context));
+            }
           }
           return adCreativeLinkDataChildAttachments;
         } else if (obj.has("images")) {
@@ -134,24 +156,54 @@ public class AdCreativeLinkDataChildAttachment extends APINode {
           }
           return adCreativeLinkDataChildAttachments;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              adCreativeLinkDataChildAttachments.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return adCreativeLinkDataChildAttachments;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          adCreativeLinkDataChildAttachments.clear();
           adCreativeLinkDataChildAttachments.add(loadJSON(json, context));
           return adCreativeLinkDataChildAttachments;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -160,30 +212,26 @@ public class AdCreativeLinkDataChildAttachment extends APINode {
   }
 
 
-  public String getFieldLink() {
-    return mLink;
+  public AdCreativeLinkDataCallToAction getFieldCallToAction() {
+    return mCallToAction;
   }
 
-  public AdCreativeLinkDataChildAttachment setFieldLink(String value) {
-    this.mLink = value;
+  public AdCreativeLinkDataChildAttachment setFieldCallToAction(AdCreativeLinkDataCallToAction value) {
+    this.mCallToAction = value;
     return this;
   }
 
-  public String getFieldPicture() {
-    return mPicture;
-  }
-
-  public AdCreativeLinkDataChildAttachment setFieldPicture(String value) {
-    this.mPicture = value;
+  public AdCreativeLinkDataChildAttachment setFieldCallToAction(String value) {
+    Type type = new TypeToken<AdCreativeLinkDataCallToAction>(){}.getType();
+    this.mCallToAction = AdCreativeLinkDataCallToAction.getGson().fromJson(value, type);
     return this;
   }
-
-  public String getFieldImageHash() {
-    return mImageHash;
+  public String getFieldDescription() {
+    return mDescription;
   }
 
-  public AdCreativeLinkDataChildAttachment setFieldImageHash(String value) {
-    this.mImageHash = value;
+  public AdCreativeLinkDataChildAttachment setFieldDescription(String value) {
+    this.mDescription = value;
     return this;
   }
 
@@ -201,6 +249,24 @@ public class AdCreativeLinkDataChildAttachment extends APINode {
     this.mImageCrops = AdsImageCrops.getGson().fromJson(value, type);
     return this;
   }
+  public String getFieldImageHash() {
+    return mImageHash;
+  }
+
+  public AdCreativeLinkDataChildAttachment setFieldImageHash(String value) {
+    this.mImageHash = value;
+    return this;
+  }
+
+  public String getFieldLink() {
+    return mLink;
+  }
+
+  public AdCreativeLinkDataChildAttachment setFieldLink(String value) {
+    this.mLink = value;
+    return this;
+  }
+
   public String getFieldName() {
     return mName;
   }
@@ -210,29 +276,33 @@ public class AdCreativeLinkDataChildAttachment extends APINode {
     return this;
   }
 
-  public String getFieldDescription() {
-    return mDescription;
+  public String getFieldPicture() {
+    return mPicture;
   }
 
-  public AdCreativeLinkDataChildAttachment setFieldDescription(String value) {
-    this.mDescription = value;
+  public AdCreativeLinkDataChildAttachment setFieldPicture(String value) {
+    this.mPicture = value;
     return this;
   }
 
-  public AdCreativeLinkDataCallToAction getFieldCallToAction() {
-    return mCallToAction;
+  public Object getFieldPlaceData() {
+    return mPlaceData;
   }
 
-  public AdCreativeLinkDataChildAttachment setFieldCallToAction(AdCreativeLinkDataCallToAction value) {
-    this.mCallToAction = value;
+  public AdCreativeLinkDataChildAttachment setFieldPlaceData(Object value) {
+    this.mPlaceData = value;
     return this;
   }
 
-  public AdCreativeLinkDataChildAttachment setFieldCallToAction(String value) {
-    Type type = new TypeToken<AdCreativeLinkDataCallToAction>(){}.getType();
-    this.mCallToAction = AdCreativeLinkDataCallToAction.getGson().fromJson(value, type);
+  public Boolean getFieldStaticCard() {
+    return mStaticCard;
+  }
+
+  public AdCreativeLinkDataChildAttachment setFieldStaticCard(Boolean value) {
+    this.mStaticCard = value;
     return this;
   }
+
   public String getFieldVideoId() {
     return mVideoId;
   }
@@ -259,22 +329,24 @@ public class AdCreativeLinkDataChildAttachment extends APINode {
   }
 
   public AdCreativeLinkDataChildAttachment copyFrom(AdCreativeLinkDataChildAttachment instance) {
-    this.mLink = instance.mLink;
-    this.mPicture = instance.mPicture;
-    this.mImageHash = instance.mImageHash;
+    this.mCallToAction = instance.mCallToAction;
+    this.mDescription = instance.mDescription;
     this.mImageCrops = instance.mImageCrops;
+    this.mImageHash = instance.mImageHash;
+    this.mLink = instance.mLink;
     this.mName = instance.mName;
-    this.mDescription = instance.mDescription;
-    this.mCallToAction = instance.mCallToAction;
+    this.mPicture = instance.mPicture;
+    this.mPlaceData = instance.mPlaceData;
+    this.mStaticCard = instance.mStaticCard;
     this.mVideoId = instance.mVideoId;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AdCreativeLinkDataChildAttachment> getParser() {
     return new APIRequest.ResponseParser<AdCreativeLinkDataChildAttachment>() {
-      public APINodeList<AdCreativeLinkDataChildAttachment> parseResponse(String response, APIContext context, APIRequest<AdCreativeLinkDataChildAttachment> request) {
+      public APINodeList<AdCreativeLinkDataChildAttachment> parseResponse(String response, APIContext context, APIRequest<AdCreativeLinkDataChildAttachment> request) throws MalformedResponseException {
         return AdCreativeLinkDataChildAttachment.parseResponse(response, context, request);
       }
     };
