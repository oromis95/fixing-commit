@@ -23,27 +23,42 @@
 
 package com.facebook.ads.sdk;
 
-import com.google.gson.*;
-import com.google.gson.annotations.SerializedName;
-
+import java.io.File;
 import java.lang.reflect.Modifier;
+import java.lang.reflect.Type;
 import java.util.Arrays;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
+import com.google.gson.JsonObject;
+import com.google.gson.JsonArray;
+import com.google.gson.annotations.SerializedName;
+import com.google.gson.reflect.TypeToken;
+import com.google.gson.Gson;
+import com.google.gson.GsonBuilder;
+import com.google.gson.JsonElement;
+import com.google.gson.JsonParser;
+
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class Ad extends APINode {
-  @SerializedName("id")
-  private String mId = null;
   @SerializedName("account_id")
   private String mAccountId = null;
-  @SerializedName("adset")
-  private AdSet mAdset = null;
-  @SerializedName("campaign")
-  private Campaign mCampaign = null;
+  @SerializedName("ad_review_feedback")
+  private AdgroupReviewFeedback mAdReviewFeedback = null;
   @SerializedName("adlabels")
   private List<AdLabel> mAdlabels = null;
+  @SerializedName("adset")
+  private AdSet mAdset = null;
   @SerializedName("adset_id")
   private String mAdsetId = null;
   @SerializedName("bid_amount")
@@ -52,6 +67,10 @@ public class Ad extends APINode {
   private Map<String, Long> mBidInfo = null;
   @SerializedName("bid_type")
   private EnumBidType mBidType = null;
+  @SerializedName("campaign")
+  private Campaign mCampaign = null;
+  @SerializedName("campaign_id")
+  private String mCampaignId = null;
   @SerializedName("configured_status")
   private EnumConfiguredStatus mConfiguredStatus = null;
   @SerializedName("conversion_specs")
@@ -62,18 +81,20 @@ public class Ad extends APINode {
   private AdCreative mCreative = null;
   @SerializedName("effective_status")
   private EnumEffectiveStatus mEffectiveStatus = null;
+  @SerializedName("id")
+  private String mId = null;
   @SerializedName("last_updated_by_app_id")
   private String mLastUpdatedByAppId = null;
   @SerializedName("name")
   private String mName = null;
+  @SerializedName("recommendations")
+  private List<AdRecommendation> mRecommendations = null;
+  @SerializedName("status")
+  private EnumStatus mStatus = null;
   @SerializedName("tracking_specs")
   private List<ConversionActionQuery> mTrackingSpecs = null;
   @SerializedName("updated_time")
   private String mUpdatedTime = null;
-  @SerializedName("campaign_id")
-  private String mCampaignId = null;
-  @SerializedName("ad_review_feedback")
-  private AdgroupReviewFeedback mAdReviewFeedback = null;
   protected static Gson gson = null;
 
   Ad() {
@@ -85,11 +106,11 @@ public class Ad extends APINode {
 
   public Ad(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public Ad fetch() throws APIException{
-    Ad newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    Ad newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -106,8 +127,17 @@ public class Ad extends APINode {
     return ad;
   }
 
+  public static APINodeList<Ad> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<Ad>)(
+      new APIRequest<Ad>(context, "", "/", "GET", Ad.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -122,22 +152,23 @@ public class Ad extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    ad.mContext = context;
+    ad.context = context;
     ad.rawValue = json;
     return ad;
   }
 
-  public static APINodeList<Ad> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<Ad> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<Ad> ads = new APINodeList<Ad>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -150,10 +181,11 @@ public class Ad extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            ads.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            ads.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -164,7 +196,20 @@ public class Ad extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            ads.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  ads.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              ads.add(loadJSON(obj.toString(), context));
+            }
           }
           return ads;
         } else if (obj.has("images")) {
@@ -175,24 +220,54 @@ public class Ad extends APINode {
           }
           return ads;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              ads.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return ads;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          ads.clear();
           ads.add(loadJSON(json, context));
           return ads;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -200,75 +275,75 @@ public class Ad extends APINode {
     return getGson().toJson(this);
   }
 
-  public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
-  }
-
-  public APIRequestUpdate update() {
-    return new APIRequestUpdate(this.getPrefixedId().toString(), mContext);
-  }
-
-  public APIRequestDelete delete() {
-    return new APIRequestDelete(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetAdCreatives getAdCreatives() {
+    return new APIRequestGetAdCreatives(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetLeads getLeads() {
-    return new APIRequestGetLeads(this.getPrefixedId().toString(), mContext);
+  public APIRequestDeleteAdLabels deleteAdLabels() {
+    return new APIRequestDeleteAdLabels(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetAdCreatives getAdCreatives() {
-    return new APIRequestGetAdCreatives(this.getPrefixedId().toString(), mContext);
+  public APIRequestCreateAdLabel createAdLabel() {
+    return new APIRequestCreateAdLabel(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetInsights getInsights() {
-    return new APIRequestGetInsights(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetInsights(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetInsightsAsync getInsightsAsync() {
-    return new APIRequestGetInsightsAsync(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetInsightsAsync(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetKeywordStats getKeywordStats() {
-    return new APIRequestGetKeywordStats(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetKeywordStats(this.getPrefixedId().toString(), context);
+  }
+
+  public APIRequestGetLeads getLeads() {
+    return new APIRequestGetLeads(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetPreviews getPreviews() {
-    return new APIRequestGetPreviews(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetPreviews(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetReachEstimate getReachEstimate() {
-    return new APIRequestGetReachEstimate(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetReachEstimate(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetTargetingSentenceLines getTargetingSentenceLines() {
-    return new APIRequestGetTargetingSentenceLines(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetTargetingSentenceLines(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestDeleteAdLabels deleteAdLabels() {
-    return new APIRequestDeleteAdLabels(this.getPrefixedId().toString(), mContext);
+  public APIRequestDelete delete() {
+    return new APIRequestDelete(this.getPrefixedId().toString(), context);
   }
 
+  public APIRequestGet get() {
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
+  }
 
-  public String getFieldId() {
-    return mId;
+  public APIRequestUpdate update() {
+    return new APIRequestUpdate(this.getPrefixedId().toString(), context);
   }
 
+
   public String getFieldAccountId() {
     return mAccountId;
   }
 
-  public AdSet getFieldAdset() {
-    return mAdset;
-  }
-
-  public Campaign getFieldCampaign() {
-    return mCampaign;
+  public AdgroupReviewFeedback getFieldAdReviewFeedback() {
+    return mAdReviewFeedback;
   }
 
   public List<AdLabel> getFieldAdlabels() {
     return mAdlabels;
   }
 
+  public AdSet getFieldAdset() {
+    return mAdset;
+  }
+
   public String getFieldAdsetId() {
     return mAdsetId;
   }
@@ -285,6 +360,14 @@ public class Ad extends APINode {
     return mBidType;
   }
 
+  public Campaign getFieldCampaign() {
+    return mCampaign;
+  }
+
+  public String getFieldCampaignId() {
+    return mCampaignId;
+  }
+
   public EnumConfiguredStatus getFieldConfiguredStatus() {
     return mConfiguredStatus;
   }
@@ -299,7 +382,7 @@ public class Ad extends APINode {
 
   public AdCreative getFieldCreative() {
     if (mCreative != null) {
-      mCreative.mContext = getContext();
+      mCreative.context = getContext();
     }
     return mCreative;
   }
@@ -308,6 +391,10 @@ public class Ad extends APINode {
     return mEffectiveStatus;
   }
 
+  public String getFieldId() {
+    return mId;
+  }
+
   public String getFieldLastUpdatedByAppId() {
     return mLastUpdatedByAppId;
   }
@@ -316,2572 +403,2600 @@ public class Ad extends APINode {
     return mName;
   }
 
-  public List<ConversionActionQuery> getFieldTrackingSpecs() {
-    return mTrackingSpecs;
+  public List<AdRecommendation> getFieldRecommendations() {
+    return mRecommendations;
   }
 
-  public String getFieldUpdatedTime() {
-    return mUpdatedTime;
+  public EnumStatus getFieldStatus() {
+    return mStatus;
   }
 
-  public String getFieldCampaignId() {
-    return mCampaignId;
+  public List<ConversionActionQuery> getFieldTrackingSpecs() {
+    return mTrackingSpecs;
   }
 
-  public AdgroupReviewFeedback getFieldAdReviewFeedback() {
-    return mAdReviewFeedback;
+  public String getFieldUpdatedTime() {
+    return mUpdatedTime;
   }
 
 
 
-  public static class APIRequestGet extends APIRequest<Ad> {
+  public static class APIRequestGetAdCreatives extends APIRequest<AdCreative> {
 
-    Ad lastResponse = null;
+    APINodeList<AdCreative> lastResponse = null;
     @Override
-    public Ad getLastResponse() {
+    public APINodeList<AdCreative> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "account_id",
-      "adset",
-      "campaign",
+      "actor_id",
+      "actor_image_hash",
+      "actor_image_url",
+      "actor_name",
       "adlabels",
-      "adset_id",
-      "bid_amount",
-      "bid_info",
-      "bid_type",
-      "configured_status",
-      "conversion_specs",
-      "created_time",
-      "creative",
-      "effective_status",
-      "last_updated_by_app_id",
+      "applink_treatment",
+      "body",
+      "call_to_action_type",
+      "id",
+      "image_crops",
+      "image_hash",
+      "image_url",
+      "instagram_actor_id",
+      "instagram_permalink_url",
+      "instagram_story_id",
+      "link_og_id",
+      "link_url",
       "name",
-      "tracking_specs",
-      "updated_time",
-      "campaign_id",
-      "ad_review_feedback",
+      "object_id",
+      "object_story_id",
+      "object_story_spec",
+      "object_type",
+      "object_url",
+      "platform_customizations",
+      "product_set_id",
+      "run_status",
+      "template_url",
+      "thumbnail_url",
+      "title",
+      "url_tags",
     };
 
     @Override
-    public Ad parseResponse(String response) throws APIException {
-      return Ad.parseResponse(response, getContext(), this).head();
+    public APINodeList<AdCreative> parseResponse(String response) throws APIException {
+      return AdCreative.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public Ad execute() throws APIException {
+    public APINodeList<AdCreative> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public Ad execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<AdCreative> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGet(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetAdCreatives(String nodeId, APIContext context) {
+      super(context, nodeId, "/adcreatives", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGet setParam(String param, Object value) {
+    @Override
+    public APIRequestGetAdCreatives setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGet setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetAdCreatives setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGet requestAllFields () {
+    public APIRequestGetAdCreatives requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGet requestAllFields (boolean value) {
+    public APIRequestGetAdCreatives requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetAdCreatives requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGet requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetAdCreatives requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestField (String field) {
+    @Override
+    public APIRequestGetAdCreatives requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGet requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetAdCreatives requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGet requestIdField () {
-      return this.requestIdField(true);
+    public APIRequestGetAdCreatives requestActorIdField () {
+      return this.requestActorIdField(true);
     }
-    public APIRequestGet requestIdField (boolean value) {
-      this.requestField("id", value);
+    public APIRequestGetAdCreatives requestActorIdField (boolean value) {
+      this.requestField("actor_id", value);
       return this;
     }
-    public APIRequestGet requestAccountIdField () {
-      return this.requestAccountIdField(true);
+    public APIRequestGetAdCreatives requestActorImageHashField () {
+      return this.requestActorImageHashField(true);
     }
-    public APIRequestGet requestAccountIdField (boolean value) {
-      this.requestField("account_id", value);
+    public APIRequestGetAdCreatives requestActorImageHashField (boolean value) {
+      this.requestField("actor_image_hash", value);
       return this;
     }
-    public APIRequestGet requestAdsetField () {
-      return this.requestAdsetField(true);
+    public APIRequestGetAdCreatives requestActorImageUrlField () {
+      return this.requestActorImageUrlField(true);
     }
-    public APIRequestGet requestAdsetField (boolean value) {
-      this.requestField("adset", value);
+    public APIRequestGetAdCreatives requestActorImageUrlField (boolean value) {
+      this.requestField("actor_image_url", value);
       return this;
     }
-    public APIRequestGet requestCampaignField () {
-      return this.requestCampaignField(true);
+    public APIRequestGetAdCreatives requestActorNameField () {
+      return this.requestActorNameField(true);
     }
-    public APIRequestGet requestCampaignField (boolean value) {
-      this.requestField("campaign", value);
+    public APIRequestGetAdCreatives requestActorNameField (boolean value) {
+      this.requestField("actor_name", value);
       return this;
     }
-    public APIRequestGet requestAdlabelsField () {
+    public APIRequestGetAdCreatives requestAdlabelsField () {
       return this.requestAdlabelsField(true);
     }
-    public APIRequestGet requestAdlabelsField (boolean value) {
+    public APIRequestGetAdCreatives requestAdlabelsField (boolean value) {
       this.requestField("adlabels", value);
       return this;
     }
-    public APIRequestGet requestAdsetIdField () {
-      return this.requestAdsetIdField(true);
+    public APIRequestGetAdCreatives requestApplinkTreatmentField () {
+      return this.requestApplinkTreatmentField(true);
     }
-    public APIRequestGet requestAdsetIdField (boolean value) {
-      this.requestField("adset_id", value);
+    public APIRequestGetAdCreatives requestApplinkTreatmentField (boolean value) {
+      this.requestField("applink_treatment", value);
       return this;
     }
-    public APIRequestGet requestBidAmountField () {
-      return this.requestBidAmountField(true);
+    public APIRequestGetAdCreatives requestBodyField () {
+      return this.requestBodyField(true);
     }
-    public APIRequestGet requestBidAmountField (boolean value) {
-      this.requestField("bid_amount", value);
+    public APIRequestGetAdCreatives requestBodyField (boolean value) {
+      this.requestField("body", value);
       return this;
     }
-    public APIRequestGet requestBidInfoField () {
-      return this.requestBidInfoField(true);
+    public APIRequestGetAdCreatives requestCallToActionTypeField () {
+      return this.requestCallToActionTypeField(true);
     }
-    public APIRequestGet requestBidInfoField (boolean value) {
-      this.requestField("bid_info", value);
+    public APIRequestGetAdCreatives requestCallToActionTypeField (boolean value) {
+      this.requestField("call_to_action_type", value);
       return this;
     }
-    public APIRequestGet requestBidTypeField () {
-      return this.requestBidTypeField(true);
+    public APIRequestGetAdCreatives requestIdField () {
+      return this.requestIdField(true);
     }
-    public APIRequestGet requestBidTypeField (boolean value) {
-      this.requestField("bid_type", value);
+    public APIRequestGetAdCreatives requestIdField (boolean value) {
+      this.requestField("id", value);
       return this;
     }
-    public APIRequestGet requestConfiguredStatusField () {
-      return this.requestConfiguredStatusField(true);
+    public APIRequestGetAdCreatives requestImageCropsField () {
+      return this.requestImageCropsField(true);
     }
-    public APIRequestGet requestConfiguredStatusField (boolean value) {
-      this.requestField("configured_status", value);
+    public APIRequestGetAdCreatives requestImageCropsField (boolean value) {
+      this.requestField("image_crops", value);
       return this;
     }
-    public APIRequestGet requestConversionSpecsField () {
-      return this.requestConversionSpecsField(true);
+    public APIRequestGetAdCreatives requestImageHashField () {
+      return this.requestImageHashField(true);
     }
-    public APIRequestGet requestConversionSpecsField (boolean value) {
-      this.requestField("conversion_specs", value);
+    public APIRequestGetAdCreatives requestImageHashField (boolean value) {
+      this.requestField("image_hash", value);
       return this;
     }
-    public APIRequestGet requestCreatedTimeField () {
-      return this.requestCreatedTimeField(true);
+    public APIRequestGetAdCreatives requestImageUrlField () {
+      return this.requestImageUrlField(true);
     }
-    public APIRequestGet requestCreatedTimeField (boolean value) {
-      this.requestField("created_time", value);
+    public APIRequestGetAdCreatives requestImageUrlField (boolean value) {
+      this.requestField("image_url", value);
       return this;
     }
-    public APIRequestGet requestCreativeField () {
-      return this.requestCreativeField(true);
+    public APIRequestGetAdCreatives requestInstagramActorIdField () {
+      return this.requestInstagramActorIdField(true);
     }
-    public APIRequestGet requestCreativeField (boolean value) {
-      this.requestField("creative", value);
+    public APIRequestGetAdCreatives requestInstagramActorIdField (boolean value) {
+      this.requestField("instagram_actor_id", value);
       return this;
     }
-    public APIRequestGet requestEffectiveStatusField () {
-      return this.requestEffectiveStatusField(true);
+    public APIRequestGetAdCreatives requestInstagramPermalinkUrlField () {
+      return this.requestInstagramPermalinkUrlField(true);
     }
-    public APIRequestGet requestEffectiveStatusField (boolean value) {
-      this.requestField("effective_status", value);
+    public APIRequestGetAdCreatives requestInstagramPermalinkUrlField (boolean value) {
+      this.requestField("instagram_permalink_url", value);
       return this;
     }
-    public APIRequestGet requestLastUpdatedByAppIdField () {
-      return this.requestLastUpdatedByAppIdField(true);
+    public APIRequestGetAdCreatives requestInstagramStoryIdField () {
+      return this.requestInstagramStoryIdField(true);
     }
-    public APIRequestGet requestLastUpdatedByAppIdField (boolean value) {
-      this.requestField("last_updated_by_app_id", value);
+    public APIRequestGetAdCreatives requestInstagramStoryIdField (boolean value) {
+      this.requestField("instagram_story_id", value);
       return this;
     }
-    public APIRequestGet requestNameField () {
+    public APIRequestGetAdCreatives requestLinkOgIdField () {
+      return this.requestLinkOgIdField(true);
+    }
+    public APIRequestGetAdCreatives requestLinkOgIdField (boolean value) {
+      this.requestField("link_og_id", value);
+      return this;
+    }
+    public APIRequestGetAdCreatives requestLinkUrlField () {
+      return this.requestLinkUrlField(true);
+    }
+    public APIRequestGetAdCreatives requestLinkUrlField (boolean value) {
+      this.requestField("link_url", value);
+      return this;
+    }
+    public APIRequestGetAdCreatives requestNameField () {
       return this.requestNameField(true);
     }
-    public APIRequestGet requestNameField (boolean value) {
+    public APIRequestGetAdCreatives requestNameField (boolean value) {
       this.requestField("name", value);
       return this;
     }
-    public APIRequestGet requestTrackingSpecsField () {
-      return this.requestTrackingSpecsField(true);
+    public APIRequestGetAdCreatives requestObjectIdField () {
+      return this.requestObjectIdField(true);
     }
-    public APIRequestGet requestTrackingSpecsField (boolean value) {
-      this.requestField("tracking_specs", value);
+    public APIRequestGetAdCreatives requestObjectIdField (boolean value) {
+      this.requestField("object_id", value);
       return this;
     }
-    public APIRequestGet requestUpdatedTimeField () {
-      return this.requestUpdatedTimeField(true);
+    public APIRequestGetAdCreatives requestObjectStoryIdField () {
+      return this.requestObjectStoryIdField(true);
     }
-    public APIRequestGet requestUpdatedTimeField (boolean value) {
-      this.requestField("updated_time", value);
+    public APIRequestGetAdCreatives requestObjectStoryIdField (boolean value) {
+      this.requestField("object_story_id", value);
       return this;
     }
-    public APIRequestGet requestCampaignIdField () {
-      return this.requestCampaignIdField(true);
+    public APIRequestGetAdCreatives requestObjectStorySpecField () {
+      return this.requestObjectStorySpecField(true);
     }
-    public APIRequestGet requestCampaignIdField (boolean value) {
-      this.requestField("campaign_id", value);
+    public APIRequestGetAdCreatives requestObjectStorySpecField (boolean value) {
+      this.requestField("object_story_spec", value);
       return this;
     }
-    public APIRequestGet requestAdReviewFeedbackField () {
-      return this.requestAdReviewFeedbackField(true);
+    public APIRequestGetAdCreatives requestObjectTypeField () {
+      return this.requestObjectTypeField(true);
     }
-    public APIRequestGet requestAdReviewFeedbackField (boolean value) {
-      this.requestField("ad_review_feedback", value);
+    public APIRequestGetAdCreatives requestObjectTypeField (boolean value) {
+      this.requestField("object_type", value);
+      return this;
+    }
+    public APIRequestGetAdCreatives requestObjectUrlField () {
+      return this.requestObjectUrlField(true);
+    }
+    public APIRequestGetAdCreatives requestObjectUrlField (boolean value) {
+      this.requestField("object_url", value);
+      return this;
+    }
+    public APIRequestGetAdCreatives requestPlatformCustomizationsField () {
+      return this.requestPlatformCustomizationsField(true);
+    }
+    public APIRequestGetAdCreatives requestPlatformCustomizationsField (boolean value) {
+      this.requestField("platform_customizations", value);
+      return this;
+    }
+    public APIRequestGetAdCreatives requestProductSetIdField () {
+      return this.requestProductSetIdField(true);
+    }
+    public APIRequestGetAdCreatives requestProductSetIdField (boolean value) {
+      this.requestField("product_set_id", value);
+      return this;
+    }
+    public APIRequestGetAdCreatives requestRunStatusField () {
+      return this.requestRunStatusField(true);
+    }
+    public APIRequestGetAdCreatives requestRunStatusField (boolean value) {
+      this.requestField("run_status", value);
+      return this;
+    }
+    public APIRequestGetAdCreatives requestTemplateUrlField () {
+      return this.requestTemplateUrlField(true);
+    }
+    public APIRequestGetAdCreatives requestTemplateUrlField (boolean value) {
+      this.requestField("template_url", value);
+      return this;
+    }
+    public APIRequestGetAdCreatives requestThumbnailUrlField () {
+      return this.requestThumbnailUrlField(true);
+    }
+    public APIRequestGetAdCreatives requestThumbnailUrlField (boolean value) {
+      this.requestField("thumbnail_url", value);
+      return this;
+    }
+    public APIRequestGetAdCreatives requestTitleField () {
+      return this.requestTitleField(true);
+    }
+    public APIRequestGetAdCreatives requestTitleField (boolean value) {
+      this.requestField("title", value);
+      return this;
+    }
+    public APIRequestGetAdCreatives requestUrlTagsField () {
+      return this.requestUrlTagsField(true);
+    }
+    public APIRequestGetAdCreatives requestUrlTagsField (boolean value) {
+      this.requestField("url_tags", value);
       return this;
     }
-
   }
 
-  public static class APIRequestUpdate extends APIRequest<APINode> {
+  public static class APIRequestDeleteAdLabels extends APIRequest<APINode> {
 
-    APINode lastResponse = null;
+    APINodeList<APINode> lastResponse = null;
     @Override
-    public APINode getLastResponse() {
+    public APINodeList<APINode> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "id",
-      "adset_id",
-      "campaign_group_id",
-      "creative",
-      "name",
-      "status",
-      "tracking_specs",
-      "display_sequence",
-      "execution_options",
       "adlabels",
-      "bid_amount",
-      "redownload",
+      "execution_options",
+      "id",
     };
 
     public static final String[] FIELDS = {
     };
 
     @Override
-    public APINode parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this).head();
+    public APINodeList<APINode> parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINode execute() throws APIException {
+    public APINodeList<APINode> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestUpdate(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "POST", Arrays.asList(PARAMS));
+    public APIRequestDeleteAdLabels(String nodeId, APIContext context) {
+      super(context, nodeId, "/adlabels", "DELETE", Arrays.asList(PARAMS));
     }
 
-    public APIRequestUpdate setParam(String param, Object value) {
+    @Override
+    public APIRequestDeleteAdLabels setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestUpdate setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestDeleteAdLabels setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestUpdate setId (String id) {
-      this.setParam("id", id);
+    public APIRequestDeleteAdLabels setAdlabels (List<Object> adlabels) {
+      this.setParam("adlabels", adlabels);
       return this;
     }
-
-
-    public APIRequestUpdate setAdsetId (Long adsetId) {
-      this.setParam("adset_id", adsetId);
+    public APIRequestDeleteAdLabels setAdlabels (String adlabels) {
+      this.setParam("adlabels", adlabels);
       return this;
     }
 
-    public APIRequestUpdate setAdsetId (String adsetId) {
-      this.setParam("adset_id", adsetId);
+    public APIRequestDeleteAdLabels setExecutionOptions (List<AdLabel.EnumExecutionOptions> executionOptions) {
+      this.setParam("execution_options", executionOptions);
       return this;
     }
-
-    public APIRequestUpdate setCampaignGroupId (Long campaignGroupId) {
-      this.setParam("campaign_group_id", campaignGroupId);
+    public APIRequestDeleteAdLabels setExecutionOptions (String executionOptions) {
+      this.setParam("execution_options", executionOptions);
       return this;
     }
 
-    public APIRequestUpdate setCampaignGroupId (String campaignGroupId) {
-      this.setParam("campaign_group_id", campaignGroupId);
+    public APIRequestDeleteAdLabels setId (String id) {
+      this.setParam("id", id);
       return this;
     }
 
-    public APIRequestUpdate setCreative (AdCreative creative) {
-      this.setParam("creative", creative);
-      return this;
+    public APIRequestDeleteAdLabels requestAllFields () {
+      return this.requestAllFields(true);
     }
 
-    public APIRequestUpdate setCreative (String creative) {
-      this.setParam("creative", creative);
+    public APIRequestDeleteAdLabels requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
 
-    public APIRequestUpdate setName (String name) {
-      this.setParam("name", name);
-      return this;
+    @Override
+    public APIRequestDeleteAdLabels requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
 
-
-    public APIRequestUpdate setStatus (EnumStatus status) {
-      this.setParam("status", status);
+    @Override
+    public APIRequestDeleteAdLabels requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
 
-    public APIRequestUpdate setStatus (String status) {
-      this.setParam("status", status);
+    @Override
+    public APIRequestDeleteAdLabels requestField (String field) {
+      this.requestField(field, true);
       return this;
     }
 
-    public APIRequestUpdate setTrackingSpecs (Object trackingSpecs) {
-      this.setParam("tracking_specs", trackingSpecs);
+    @Override
+    public APIRequestDeleteAdLabels requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestUpdate setTrackingSpecs (String trackingSpecs) {
-      this.setParam("tracking_specs", trackingSpecs);
-      return this;
-    }
+  }
 
-    public APIRequestUpdate setDisplaySequence (Long displaySequence) {
-      this.setParam("display_sequence", displaySequence);
-      return this;
-    }
+  public static class APIRequestCreateAdLabel extends APIRequest<APINode> {
 
-    public APIRequestUpdate setDisplaySequence (String displaySequence) {
-      this.setParam("display_sequence", displaySequence);
-      return this;
+    APINode lastResponse = null;
+    @Override
+    public APINode getLastResponse() {
+      return lastResponse;
     }
+    public static final String[] PARAMS = {
+      "adlabels",
+      "execution_options",
+      "id",
+    };
 
-    public APIRequestUpdate setExecutionOptions (List<EnumUpdateExecutionOptions> executionOptions) {
-      this.setParam("execution_options", executionOptions);
-      return this;
+    public static final String[] FIELDS = {
+    };
+
+    @Override
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
     }
 
-    public APIRequestUpdate setExecutionOptions (String executionOptions) {
-      this.setParam("execution_options", executionOptions);
-      return this;
+    @Override
+    public APINode execute() throws APIException {
+      return execute(new HashMap<String, Object>());
     }
 
-    public APIRequestUpdate setAdlabels (List<Object> adlabels) {
-      this.setParam("adlabels", adlabels);
-      return this;
+    @Override
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
     }
 
-    public APIRequestUpdate setAdlabels (String adlabels) {
-      this.setParam("adlabels", adlabels);
+    public APIRequestCreateAdLabel(String nodeId, APIContext context) {
+      super(context, nodeId, "/adlabels", "POST", Arrays.asList(PARAMS));
+    }
+
+    @Override
+    public APIRequestCreateAdLabel setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestUpdate setBidAmount (Long bidAmount) {
-      this.setParam("bid_amount", bidAmount);
+    @Override
+    public APIRequestCreateAdLabel setParams(Map<String, Object> params) {
+      setParamsInternal(params);
       return this;
     }
 
-    public APIRequestUpdate setBidAmount (String bidAmount) {
-      this.setParam("bid_amount", bidAmount);
+
+    public APIRequestCreateAdLabel setAdlabels (List<Object> adlabels) {
+      this.setParam("adlabels", adlabels);
+      return this;
+    }
+    public APIRequestCreateAdLabel setAdlabels (String adlabels) {
+      this.setParam("adlabels", adlabels);
       return this;
     }
 
-    public APIRequestUpdate setRedownload (Boolean redownload) {
-      this.setParam("redownload", redownload);
+    public APIRequestCreateAdLabel setExecutionOptions (List<AdLabel.EnumExecutionOptions> executionOptions) {
+      this.setParam("execution_options", executionOptions);
+      return this;
+    }
+    public APIRequestCreateAdLabel setExecutionOptions (String executionOptions) {
+      this.setParam("execution_options", executionOptions);
       return this;
     }
 
-    public APIRequestUpdate setRedownload (String redownload) {
-      this.setParam("redownload", redownload);
+    public APIRequestCreateAdLabel setId (String id) {
+      this.setParam("id", id);
       return this;
     }
 
-    public APIRequestUpdate requestAllFields () {
+    public APIRequestCreateAdLabel requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestUpdate requestAllFields (boolean value) {
+    public APIRequestCreateAdLabel requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestUpdate requestFields (List<String> fields) {
+    @Override
+    public APIRequestCreateAdLabel requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestUpdate requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestCreateAdLabel requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestUpdate requestField (String field) {
+    @Override
+    public APIRequestCreateAdLabel requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestUpdate requestField (String field, boolean value) {
+    @Override
+    public APIRequestCreateAdLabel requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
   }
 
-  public static class APIRequestDelete extends APIRequest<APINode> {
+  public static class APIRequestGetInsights extends APIRequest<AdsInsights> {
 
-    APINode lastResponse = null;
+    APINodeList<AdsInsights> lastResponse = null;
     @Override
-    public APINode getLastResponse() {
+    public APINodeList<AdsInsights> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "id",
+      "action_attribution_windows",
+      "action_breakdowns",
+      "action_report_time",
+      "breakdowns",
+      "date_preset",
+      "default_summary",
+      "fields",
+      "filtering",
+      "level",
+      "product_id_limit",
+      "sort",
+      "summary",
+      "summary_action_breakdowns",
+      "time_increment",
+      "time_range",
+      "time_ranges",
     };
 
     public static final String[] FIELDS = {
     };
 
     @Override
-    public APINode parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this).head();
+    public APINodeList<AdsInsights> parseResponse(String response) throws APIException {
+      return AdsInsights.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINode execute() throws APIException {
+    public APINodeList<AdsInsights> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<AdsInsights> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestDelete(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "DELETE", Arrays.asList(PARAMS));
+    public APIRequestGetInsights(String nodeId, APIContext context) {
+      super(context, nodeId, "/insights", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestDelete setParam(String param, Object value) {
+    @Override
+    public APIRequestGetInsights setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestDelete setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetInsights setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestDelete setId (String id) {
-      this.setParam("id", id);
+    public APIRequestGetInsights setActionAttributionWindows (List<AdsInsights.EnumActionAttributionWindows> actionAttributionWindows) {
+      this.setParam("action_attribution_windows", actionAttributionWindows);
+      return this;
+    }
+    public APIRequestGetInsights setActionAttributionWindows (String actionAttributionWindows) {
+      this.setParam("action_attribution_windows", actionAttributionWindows);
       return this;
     }
 
+    public APIRequestGetInsights setActionBreakdowns (List<AdsInsights.EnumActionBreakdowns> actionBreakdowns) {
+      this.setParam("action_breakdowns", actionBreakdowns);
+      return this;
+    }
+    public APIRequestGetInsights setActionBreakdowns (String actionBreakdowns) {
+      this.setParam("action_breakdowns", actionBreakdowns);
+      return this;
+    }
 
-    public APIRequestDelete requestAllFields () {
+    public APIRequestGetInsights setActionReportTime (AdsInsights.EnumActionReportTime actionReportTime) {
+      this.setParam("action_report_time", actionReportTime);
+      return this;
+    }
+    public APIRequestGetInsights setActionReportTime (String actionReportTime) {
+      this.setParam("action_report_time", actionReportTime);
+      return this;
+    }
+
+    public APIRequestGetInsights setBreakdowns (List<AdsInsights.EnumBreakdowns> breakdowns) {
+      this.setParam("breakdowns", breakdowns);
+      return this;
+    }
+    public APIRequestGetInsights setBreakdowns (String breakdowns) {
+      this.setParam("breakdowns", breakdowns);
+      return this;
+    }
+
+    public APIRequestGetInsights setDatePreset (AdsInsights.EnumDatePreset datePreset) {
+      this.setParam("date_preset", datePreset);
+      return this;
+    }
+    public APIRequestGetInsights setDatePreset (String datePreset) {
+      this.setParam("date_preset", datePreset);
+      return this;
+    }
+
+    public APIRequestGetInsights setDefaultSummary (Boolean defaultSummary) {
+      this.setParam("default_summary", defaultSummary);
+      return this;
+    }
+    public APIRequestGetInsights setDefaultSummary (String defaultSummary) {
+      this.setParam("default_summary", defaultSummary);
+      return this;
+    }
+
+    public APIRequestGetInsights setFields (List<AdsInsights.EnumSummary> fields) {
+      this.setParam("fields", fields);
+      return this;
+    }
+    public APIRequestGetInsights setFields (String fields) {
+      this.setParam("fields", fields);
+      return this;
+    }
+
+    public APIRequestGetInsights setFiltering (List<Object> filtering) {
+      this.setParam("filtering", filtering);
+      return this;
+    }
+    public APIRequestGetInsights setFiltering (String filtering) {
+      this.setParam("filtering", filtering);
+      return this;
+    }
+
+    public APIRequestGetInsights setLevel (AdsInsights.EnumLevel level) {
+      this.setParam("level", level);
+      return this;
+    }
+    public APIRequestGetInsights setLevel (String level) {
+      this.setParam("level", level);
+      return this;
+    }
+
+    public APIRequestGetInsights setProductIdLimit (Long productIdLimit) {
+      this.setParam("product_id_limit", productIdLimit);
+      return this;
+    }
+    public APIRequestGetInsights setProductIdLimit (String productIdLimit) {
+      this.setParam("product_id_limit", productIdLimit);
+      return this;
+    }
+
+    public APIRequestGetInsights setSort (List<String> sort) {
+      this.setParam("sort", sort);
+      return this;
+    }
+    public APIRequestGetInsights setSort (String sort) {
+      this.setParam("sort", sort);
+      return this;
+    }
+
+    public APIRequestGetInsights setSummary (List<AdsInsights.EnumSummary> summary) {
+      this.setParam("summary", summary);
+      return this;
+    }
+    public APIRequestGetInsights setSummary (String summary) {
+      this.setParam("summary", summary);
+      return this;
+    }
+
+    public APIRequestGetInsights setSummaryActionBreakdowns (List<AdsInsights.EnumSummaryActionBreakdowns> summaryActionBreakdowns) {
+      this.setParam("summary_action_breakdowns", summaryActionBreakdowns);
+      return this;
+    }
+    public APIRequestGetInsights setSummaryActionBreakdowns (String summaryActionBreakdowns) {
+      this.setParam("summary_action_breakdowns", summaryActionBreakdowns);
+      return this;
+    }
+
+    public APIRequestGetInsights setTimeIncrement (String timeIncrement) {
+      this.setParam("time_increment", timeIncrement);
+      return this;
+    }
+
+    public APIRequestGetInsights setTimeRange (Map<String, String> timeRange) {
+      this.setParam("time_range", timeRange);
+      return this;
+    }
+    public APIRequestGetInsights setTimeRange (String timeRange) {
+      this.setParam("time_range", timeRange);
+      return this;
+    }
+
+    public APIRequestGetInsights setTimeRanges (List<Map<String, String>> timeRanges) {
+      this.setParam("time_ranges", timeRanges);
+      return this;
+    }
+    public APIRequestGetInsights setTimeRanges (String timeRanges) {
+      this.setParam("time_ranges", timeRanges);
+      return this;
+    }
+
+    public APIRequestGetInsights requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestDelete requestAllFields (boolean value) {
+    public APIRequestGetInsights requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestDelete requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetInsights requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestDelete requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetInsights requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestDelete requestField (String field) {
+    @Override
+    public APIRequestGetInsights requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestDelete requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetInsights requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
   }
 
-  public static class APIRequestGetLeads extends APIRequest<APINode> {
+  public static class APIRequestGetInsightsAsync extends APIRequest<AdReportRun> {
 
-    APINodeList<APINode> lastResponse = null;
+    APINodeList<AdReportRun> lastResponse = null;
     @Override
-    public APINodeList<APINode> getLastResponse() {
+    public APINodeList<AdReportRun> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
+      "action_attribution_windows",
+      "action_breakdowns",
+      "action_report_time",
+      "breakdowns",
+      "date_preset",
+      "default_summary",
+      "fields",
+      "filtering",
+      "level",
+      "product_id_limit",
+      "sort",
+      "summary",
+      "summary_action_breakdowns",
+      "time_increment",
+      "time_range",
+      "time_ranges",
     };
 
     public static final String[] FIELDS = {
     };
 
     @Override
-    public APINodeList<APINode> parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this);
+    public APINodeList<AdReportRun> parseResponse(String response) throws APIException {
+      return AdReportRun.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINodeList<APINode> execute() throws APIException {
+    public APINodeList<AdReportRun> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<AdReportRun> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetLeads(String nodeId, APIContext context) {
-      super(context, nodeId, "/leads", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetInsightsAsync(String nodeId, APIContext context) {
+      super(context, nodeId, "/insights", "POST", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetLeads setParam(String param, Object value) {
+    @Override
+    public APIRequestGetInsightsAsync setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetLeads setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetInsightsAsync setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetLeads requestAllFields () {
-      return this.requestAllFields(true);
+    public APIRequestGetInsightsAsync setActionAttributionWindows (List<AdsInsights.EnumActionAttributionWindows> actionAttributionWindows) {
+      this.setParam("action_attribution_windows", actionAttributionWindows);
+      return this;
     }
-
-    public APIRequestGetLeads requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
+    public APIRequestGetInsightsAsync setActionAttributionWindows (String actionAttributionWindows) {
+      this.setParam("action_attribution_windows", actionAttributionWindows);
       return this;
     }
 
-    public APIRequestGetLeads requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
+    public APIRequestGetInsightsAsync setActionBreakdowns (List<AdsInsights.EnumActionBreakdowns> actionBreakdowns) {
+      this.setParam("action_breakdowns", actionBreakdowns);
+      return this;
     }
-
-    public APIRequestGetLeads requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+    public APIRequestGetInsightsAsync setActionBreakdowns (String actionBreakdowns) {
+      this.setParam("action_breakdowns", actionBreakdowns);
       return this;
     }
 
-    public APIRequestGetLeads requestField (String field) {
-      this.requestField(field, true);
+    public APIRequestGetInsightsAsync setActionReportTime (AdsInsights.EnumActionReportTime actionReportTime) {
+      this.setParam("action_report_time", actionReportTime);
       return this;
     }
-
-    public APIRequestGetLeads requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
+    public APIRequestGetInsightsAsync setActionReportTime (String actionReportTime) {
+      this.setParam("action_report_time", actionReportTime);
       return this;
     }
 
+    public APIRequestGetInsightsAsync setBreakdowns (List<AdsInsights.EnumBreakdowns> breakdowns) {
+      this.setParam("breakdowns", breakdowns);
+      return this;
+    }
+    public APIRequestGetInsightsAsync setBreakdowns (String breakdowns) {
+      this.setParam("breakdowns", breakdowns);
+      return this;
+    }
 
-  }
-
-  public static class APIRequestGetAdCreatives extends APIRequest<AdCreative> {
+    public APIRequestGetInsightsAsync setDatePreset (AdsInsights.EnumDatePreset datePreset) {
+      this.setParam("date_preset", datePreset);
+      return this;
+    }
+    public APIRequestGetInsightsAsync setDatePreset (String datePreset) {
+      this.setParam("date_preset", datePreset);
+      return this;
+    }
 
-    APINodeList<AdCreative> lastResponse = null;
-    @Override
-    public APINodeList<AdCreative> getLastResponse() {
-      return lastResponse;
+    public APIRequestGetInsightsAsync setDefaultSummary (Boolean defaultSummary) {
+      this.setParam("default_summary", defaultSummary);
+      return this;
+    }
+    public APIRequestGetInsightsAsync setDefaultSummary (String defaultSummary) {
+      this.setParam("default_summary", defaultSummary);
+      return this;
     }
-    public static final String[] PARAMS = {
-    };
 
-    public static final String[] FIELDS = {
-      "id",
-      "actor_id",
-      "actor_image_hash",
-      "actor_image_url",
-      "actor_name",
-      "adlabels",
-      "body",
-      "call_to_action_type",
-      "image_crops",
-      "image_hash",
-      "image_url",
-      "instagram_actor_id",
-      "instagram_permalink_url",
-      "link_og_id",
-      "link_url",
-      "name",
-      "object_id",
-      "object_url",
-      "object_story_id",
-      "object_story_spec",
-      "object_type",
-      "product_set_id",
-      "run_status",
-      "template_url",
-      "thumbnail_url",
-      "title",
-      "url_tags",
-      "applink_treatment",
-    };
+    public APIRequestGetInsightsAsync setFields (List<AdsInsights.EnumSummary> fields) {
+      this.setParam("fields", fields);
+      return this;
+    }
+    public APIRequestGetInsightsAsync setFields (String fields) {
+      this.setParam("fields", fields);
+      return this;
+    }
 
-    @Override
-    public APINodeList<AdCreative> parseResponse(String response) throws APIException {
-      return AdCreative.parseResponse(response, getContext(), this);
+    public APIRequestGetInsightsAsync setFiltering (List<Object> filtering) {
+      this.setParam("filtering", filtering);
+      return this;
+    }
+    public APIRequestGetInsightsAsync setFiltering (String filtering) {
+      this.setParam("filtering", filtering);
+      return this;
     }
 
-    @Override
-    public APINodeList<AdCreative> execute() throws APIException {
-      return execute(new HashMap<String, Object>());
+    public APIRequestGetInsightsAsync setLevel (AdsInsights.EnumLevel level) {
+      this.setParam("level", level);
+      return this;
+    }
+    public APIRequestGetInsightsAsync setLevel (String level) {
+      this.setParam("level", level);
+      return this;
     }
 
-    @Override
-    public APINodeList<AdCreative> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
+    public APIRequestGetInsightsAsync setProductIdLimit (Long productIdLimit) {
+      this.setParam("product_id_limit", productIdLimit);
+      return this;
+    }
+    public APIRequestGetInsightsAsync setProductIdLimit (String productIdLimit) {
+      this.setParam("product_id_limit", productIdLimit);
+      return this;
     }
 
-    public APIRequestGetAdCreatives(String nodeId, APIContext context) {
-      super(context, nodeId, "/adcreatives", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetInsightsAsync setSort (List<String> sort) {
+      this.setParam("sort", sort);
+      return this;
+    }
+    public APIRequestGetInsightsAsync setSort (String sort) {
+      this.setParam("sort", sort);
+      return this;
     }
 
-    public APIRequestGetAdCreatives setParam(String param, Object value) {
-      setParamInternal(param, value);
+    public APIRequestGetInsightsAsync setSummary (List<AdsInsights.EnumSummary> summary) {
+      this.setParam("summary", summary);
+      return this;
+    }
+    public APIRequestGetInsightsAsync setSummary (String summary) {
+      this.setParam("summary", summary);
       return this;
     }
 
-    public APIRequestGetAdCreatives setParams(Map<String, Object> params) {
-      setParamsInternal(params);
+    public APIRequestGetInsightsAsync setSummaryActionBreakdowns (List<AdsInsights.EnumSummaryActionBreakdowns> summaryActionBreakdowns) {
+      this.setParam("summary_action_breakdowns", summaryActionBreakdowns);
+      return this;
+    }
+    public APIRequestGetInsightsAsync setSummaryActionBreakdowns (String summaryActionBreakdowns) {
+      this.setParam("summary_action_breakdowns", summaryActionBreakdowns);
       return this;
     }
 
+    public APIRequestGetInsightsAsync setTimeIncrement (String timeIncrement) {
+      this.setParam("time_increment", timeIncrement);
+      return this;
+    }
 
-    public APIRequestGetAdCreatives requestAllFields () {
+    public APIRequestGetInsightsAsync setTimeRange (Map<String, String> timeRange) {
+      this.setParam("time_range", timeRange);
+      return this;
+    }
+    public APIRequestGetInsightsAsync setTimeRange (String timeRange) {
+      this.setParam("time_range", timeRange);
+      return this;
+    }
+
+    public APIRequestGetInsightsAsync setTimeRanges (List<Map<String, String>> timeRanges) {
+      this.setParam("time_ranges", timeRanges);
+      return this;
+    }
+    public APIRequestGetInsightsAsync setTimeRanges (String timeRanges) {
+      this.setParam("time_ranges", timeRanges);
+      return this;
+    }
+
+    public APIRequestGetInsightsAsync requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetAdCreatives requestAllFields (boolean value) {
+    public APIRequestGetInsightsAsync requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetAdCreatives requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetInsightsAsync requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetAdCreatives requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetInsightsAsync requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetAdCreatives requestField (String field) {
+    @Override
+    public APIRequestGetInsightsAsync requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetAdCreatives requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetInsightsAsync requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetAdCreatives requestIdField () {
-      return this.requestIdField(true);
+  }
+
+  public static class APIRequestGetKeywordStats extends APIRequest<AdKeywordStats> {
+
+    APINodeList<AdKeywordStats> lastResponse = null;
+    @Override
+    public APINodeList<AdKeywordStats> getLastResponse() {
+      return lastResponse;
     }
-    public APIRequestGetAdCreatives requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
+    public static final String[] PARAMS = {
+      "date",
+    };
+
+    public static final String[] FIELDS = {
+      "actions",
+      "clicks",
+      "cost_per_total_action",
+      "cost_per_unique_click",
+      "cpc",
+      "cpm",
+      "cpp",
+      "ctr",
+      "frequency",
+      "id",
+      "impressions",
+      "name",
+      "reach",
+      "spend",
+      "total_actions",
+      "total_unique_actions",
+      "unique_actions",
+      "unique_clicks",
+      "unique_ctr",
+      "unique_impressions",
+    };
+
+    @Override
+    public APINodeList<AdKeywordStats> parseResponse(String response) throws APIException {
+      return AdKeywordStats.parseResponse(response, getContext(), this);
     }
-    public APIRequestGetAdCreatives requestActorIdField () {
-      return this.requestActorIdField(true);
+
+    @Override
+    public APINodeList<AdKeywordStats> execute() throws APIException {
+      return execute(new HashMap<String, Object>());
     }
-    public APIRequestGetAdCreatives requestActorIdField (boolean value) {
-      this.requestField("actor_id", value);
-      return this;
+
+    @Override
+    public APINodeList<AdKeywordStats> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
     }
-    public APIRequestGetAdCreatives requestActorImageHashField () {
-      return this.requestActorImageHashField(true);
+
+    public APIRequestGetKeywordStats(String nodeId, APIContext context) {
+      super(context, nodeId, "/keywordstats", "GET", Arrays.asList(PARAMS));
     }
-    public APIRequestGetAdCreatives requestActorImageHashField (boolean value) {
-      this.requestField("actor_image_hash", value);
+
+    @Override
+    public APIRequestGetKeywordStats setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
-    public APIRequestGetAdCreatives requestActorImageUrlField () {
-      return this.requestActorImageUrlField(true);
-    }
-    public APIRequestGetAdCreatives requestActorImageUrlField (boolean value) {
-      this.requestField("actor_image_url", value);
+
+    @Override
+    public APIRequestGetKeywordStats setParams(Map<String, Object> params) {
+      setParamsInternal(params);
       return this;
     }
-    public APIRequestGetAdCreatives requestActorNameField () {
-      return this.requestActorNameField(true);
-    }
-    public APIRequestGetAdCreatives requestActorNameField (boolean value) {
-      this.requestField("actor_name", value);
+
+
+    public APIRequestGetKeywordStats setDate (String date) {
+      this.setParam("date", date);
       return this;
     }
-    public APIRequestGetAdCreatives requestAdlabelsField () {
-      return this.requestAdlabelsField(true);
+
+    public APIRequestGetKeywordStats requestAllFields () {
+      return this.requestAllFields(true);
     }
-    public APIRequestGetAdCreatives requestAdlabelsField (boolean value) {
-      this.requestField("adlabels", value);
+
+    public APIRequestGetKeywordStats requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetAdCreatives requestBodyField () {
-      return this.requestBodyField(true);
+
+    @Override
+    public APIRequestGetKeywordStats requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
-    public APIRequestGetAdCreatives requestBodyField (boolean value) {
-      this.requestField("body", value);
+
+    @Override
+    public APIRequestGetKeywordStats requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetAdCreatives requestCallToActionTypeField () {
-      return this.requestCallToActionTypeField(true);
+
+    @Override
+    public APIRequestGetKeywordStats requestField (String field) {
+      this.requestField(field, true);
+      return this;
     }
-    public APIRequestGetAdCreatives requestCallToActionTypeField (boolean value) {
-      this.requestField("call_to_action_type", value);
+
+    @Override
+    public APIRequestGetKeywordStats requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
-    public APIRequestGetAdCreatives requestImageCropsField () {
-      return this.requestImageCropsField(true);
+
+    public APIRequestGetKeywordStats requestActionsField () {
+      return this.requestActionsField(true);
     }
-    public APIRequestGetAdCreatives requestImageCropsField (boolean value) {
-      this.requestField("image_crops", value);
+    public APIRequestGetKeywordStats requestActionsField (boolean value) {
+      this.requestField("actions", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestImageHashField () {
-      return this.requestImageHashField(true);
+    public APIRequestGetKeywordStats requestClicksField () {
+      return this.requestClicksField(true);
     }
-    public APIRequestGetAdCreatives requestImageHashField (boolean value) {
-      this.requestField("image_hash", value);
+    public APIRequestGetKeywordStats requestClicksField (boolean value) {
+      this.requestField("clicks", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestImageUrlField () {
-      return this.requestImageUrlField(true);
+    public APIRequestGetKeywordStats requestCostPerTotalActionField () {
+      return this.requestCostPerTotalActionField(true);
     }
-    public APIRequestGetAdCreatives requestImageUrlField (boolean value) {
-      this.requestField("image_url", value);
+    public APIRequestGetKeywordStats requestCostPerTotalActionField (boolean value) {
+      this.requestField("cost_per_total_action", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestInstagramActorIdField () {
-      return this.requestInstagramActorIdField(true);
+    public APIRequestGetKeywordStats requestCostPerUniqueClickField () {
+      return this.requestCostPerUniqueClickField(true);
     }
-    public APIRequestGetAdCreatives requestInstagramActorIdField (boolean value) {
-      this.requestField("instagram_actor_id", value);
+    public APIRequestGetKeywordStats requestCostPerUniqueClickField (boolean value) {
+      this.requestField("cost_per_unique_click", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestInstagramPermalinkUrlField () {
-      return this.requestInstagramPermalinkUrlField(true);
+    public APIRequestGetKeywordStats requestCpcField () {
+      return this.requestCpcField(true);
     }
-    public APIRequestGetAdCreatives requestInstagramPermalinkUrlField (boolean value) {
-      this.requestField("instagram_permalink_url", value);
+    public APIRequestGetKeywordStats requestCpcField (boolean value) {
+      this.requestField("cpc", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestLinkOgIdField () {
-      return this.requestLinkOgIdField(true);
+    public APIRequestGetKeywordStats requestCpmField () {
+      return this.requestCpmField(true);
     }
-    public APIRequestGetAdCreatives requestLinkOgIdField (boolean value) {
-      this.requestField("link_og_id", value);
+    public APIRequestGetKeywordStats requestCpmField (boolean value) {
+      this.requestField("cpm", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestLinkUrlField () {
-      return this.requestLinkUrlField(true);
+    public APIRequestGetKeywordStats requestCppField () {
+      return this.requestCppField(true);
     }
-    public APIRequestGetAdCreatives requestLinkUrlField (boolean value) {
-      this.requestField("link_url", value);
+    public APIRequestGetKeywordStats requestCppField (boolean value) {
+      this.requestField("cpp", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestNameField () {
-      return this.requestNameField(true);
+    public APIRequestGetKeywordStats requestCtrField () {
+      return this.requestCtrField(true);
     }
-    public APIRequestGetAdCreatives requestNameField (boolean value) {
-      this.requestField("name", value);
+    public APIRequestGetKeywordStats requestCtrField (boolean value) {
+      this.requestField("ctr", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestObjectIdField () {
-      return this.requestObjectIdField(true);
+    public APIRequestGetKeywordStats requestFrequencyField () {
+      return this.requestFrequencyField(true);
     }
-    public APIRequestGetAdCreatives requestObjectIdField (boolean value) {
-      this.requestField("object_id", value);
+    public APIRequestGetKeywordStats requestFrequencyField (boolean value) {
+      this.requestField("frequency", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestObjectUrlField () {
-      return this.requestObjectUrlField(true);
+    public APIRequestGetKeywordStats requestIdField () {
+      return this.requestIdField(true);
     }
-    public APIRequestGetAdCreatives requestObjectUrlField (boolean value) {
-      this.requestField("object_url", value);
+    public APIRequestGetKeywordStats requestIdField (boolean value) {
+      this.requestField("id", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestObjectStoryIdField () {
-      return this.requestObjectStoryIdField(true);
+    public APIRequestGetKeywordStats requestImpressionsField () {
+      return this.requestImpressionsField(true);
     }
-    public APIRequestGetAdCreatives requestObjectStoryIdField (boolean value) {
-      this.requestField("object_story_id", value);
+    public APIRequestGetKeywordStats requestImpressionsField (boolean value) {
+      this.requestField("impressions", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestObjectStorySpecField () {
-      return this.requestObjectStorySpecField(true);
+    public APIRequestGetKeywordStats requestNameField () {
+      return this.requestNameField(true);
     }
-    public APIRequestGetAdCreatives requestObjectStorySpecField (boolean value) {
-      this.requestField("object_story_spec", value);
+    public APIRequestGetKeywordStats requestNameField (boolean value) {
+      this.requestField("name", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestObjectTypeField () {
-      return this.requestObjectTypeField(true);
+    public APIRequestGetKeywordStats requestReachField () {
+      return this.requestReachField(true);
     }
-    public APIRequestGetAdCreatives requestObjectTypeField (boolean value) {
-      this.requestField("object_type", value);
+    public APIRequestGetKeywordStats requestReachField (boolean value) {
+      this.requestField("reach", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestProductSetIdField () {
-      return this.requestProductSetIdField(true);
+    public APIRequestGetKeywordStats requestSpendField () {
+      return this.requestSpendField(true);
     }
-    public APIRequestGetAdCreatives requestProductSetIdField (boolean value) {
-      this.requestField("product_set_id", value);
+    public APIRequestGetKeywordStats requestSpendField (boolean value) {
+      this.requestField("spend", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestRunStatusField () {
-      return this.requestRunStatusField(true);
+    public APIRequestGetKeywordStats requestTotalActionsField () {
+      return this.requestTotalActionsField(true);
     }
-    public APIRequestGetAdCreatives requestRunStatusField (boolean value) {
-      this.requestField("run_status", value);
+    public APIRequestGetKeywordStats requestTotalActionsField (boolean value) {
+      this.requestField("total_actions", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestTemplateUrlField () {
-      return this.requestTemplateUrlField(true);
+    public APIRequestGetKeywordStats requestTotalUniqueActionsField () {
+      return this.requestTotalUniqueActionsField(true);
     }
-    public APIRequestGetAdCreatives requestTemplateUrlField (boolean value) {
-      this.requestField("template_url", value);
+    public APIRequestGetKeywordStats requestTotalUniqueActionsField (boolean value) {
+      this.requestField("total_unique_actions", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestThumbnailUrlField () {
-      return this.requestThumbnailUrlField(true);
+    public APIRequestGetKeywordStats requestUniqueActionsField () {
+      return this.requestUniqueActionsField(true);
     }
-    public APIRequestGetAdCreatives requestThumbnailUrlField (boolean value) {
-      this.requestField("thumbnail_url", value);
+    public APIRequestGetKeywordStats requestUniqueActionsField (boolean value) {
+      this.requestField("unique_actions", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestTitleField () {
-      return this.requestTitleField(true);
+    public APIRequestGetKeywordStats requestUniqueClicksField () {
+      return this.requestUniqueClicksField(true);
     }
-    public APIRequestGetAdCreatives requestTitleField (boolean value) {
-      this.requestField("title", value);
+    public APIRequestGetKeywordStats requestUniqueClicksField (boolean value) {
+      this.requestField("unique_clicks", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestUrlTagsField () {
-      return this.requestUrlTagsField(true);
+    public APIRequestGetKeywordStats requestUniqueCtrField () {
+      return this.requestUniqueCtrField(true);
     }
-    public APIRequestGetAdCreatives requestUrlTagsField (boolean value) {
-      this.requestField("url_tags", value);
+    public APIRequestGetKeywordStats requestUniqueCtrField (boolean value) {
+      this.requestField("unique_ctr", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestApplinkTreatmentField () {
-      return this.requestApplinkTreatmentField(true);
+    public APIRequestGetKeywordStats requestUniqueImpressionsField () {
+      return this.requestUniqueImpressionsField(true);
     }
-    public APIRequestGetAdCreatives requestApplinkTreatmentField (boolean value) {
-      this.requestField("applink_treatment", value);
+    public APIRequestGetKeywordStats requestUniqueImpressionsField (boolean value) {
+      this.requestField("unique_impressions", value);
       return this;
     }
-
   }
 
-  public abstract static class APIRequestGetInsightsBase<T extends APINode> extends APIRequest<T> {
+  public static class APIRequestGetLeads extends APIRequest<Lead> {
 
-    APINodeList<T> lastResponse = null;
+    APINodeList<Lead> lastResponse = null;
     @Override
-    public APINodeList<T> getLastResponse() {
+    public APINodeList<Lead> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "default_summary",
-      "fields",
-      "filtering",
-      "summary",
-      "sort",
-      "action_attribution_windows",
-      "action_breakdowns",
-      "action_report_time",
-      "breakdowns",
-      "date_preset",
-      "level",
-      "product_id_limit",
-      "summary_action_breakdowns",
-      "time_increment",
-      "time_range",
-      "time_ranges",
     };
 
     public static final String[] FIELDS = {
+      "ad_id",
+      "ad_name",
+      "adset_id",
+      "adset_name",
+      "campaign_id",
+      "campaign_name",
+      "created_time",
+      "custom_disclaimer_responses",
+      "field_data",
+      "form_id",
+      "id",
+      "is_organic",
+      "post",
     };
 
-    public APIRequestGetInsightsBase(String nodeId, APIContext context, String method) {
-      super(context, nodeId, "/insights", method, Arrays.asList(PARAMS));
-    }
-
-    public APIRequestGetInsightsBase setParam(String param, Object value) {
-      setParamInternal(param, value);
-      return this;
-    }
-
-    public APIRequestGetInsightsBase setParams(Map<String, Object> params) {
-      setParamsInternal(params);
-      return this;
-    }
-
-
-    public APIRequestGetInsightsBase setDefaultSummary (Boolean defaultSummary) {
-      this.setParam("default_summary", defaultSummary);
-      return this;
-    }
-
-    public APIRequestGetInsightsBase setDefaultSummary (String defaultSummary) {
-      this.setParam("default_summary", defaultSummary);
-      return this;
-    }
-
-    public APIRequestGetInsightsBase setFields (List<EnumFields> fields) {
-      this.setParam("fields", fields);
-      return this;
-    }
-
-    public APIRequestGetInsightsBase setFields (String fields) {
-      this.setParam("fields", fields);
-      return this;
-    }
-
-    public APIRequestGetInsightsBase setFiltering (List<Object> filtering) {
-      this.setParam("filtering", filtering);
-      return this;
-    }
-
-    public APIRequestGetInsightsBase setFiltering (String filtering) {
-      this.setParam("filtering", filtering);
-      return this;
-    }
-
-    public APIRequestGetInsightsBase setSummary (List<EnumFields> summary) {
-      this.setParam("summary", summary);
-      return this;
-    }
-
-    public APIRequestGetInsightsBase setSummary (String summary) {
-      this.setParam("summary", summary);
-      return this;
-    }
-
-    public APIRequestGetInsightsBase setSort (List<String> sort) {
-      this.setParam("sort", sort);
-      return this;
-    }
-
-    public APIRequestGetInsightsBase setSort (String sort) {
-      this.setParam("sort", sort);
-      return this;
+    @Override
+    public APINodeList<Lead> parseResponse(String response) throws APIException {
+      return Lead.parseResponse(response, getContext(), this);
     }
 
-    public APIRequestGetInsightsBase setActionAttributionWindows (List<EnumActionAttributionWindows> actionAttributionWindows) {
-      this.setParam("action_attribution_windows", actionAttributionWindows);
-      return this;
+    @Override
+    public APINodeList<Lead> execute() throws APIException {
+      return execute(new HashMap<String, Object>());
     }
 
-    public APIRequestGetInsightsBase setActionAttributionWindows (String actionAttributionWindows) {
-      this.setParam("action_attribution_windows", actionAttributionWindows);
-      return this;
+    @Override
+    public APINodeList<Lead> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
     }
 
-    public APIRequestGetInsightsBase setActionBreakdowns (List<EnumActionBreakdowns> actionBreakdowns) {
-      this.setParam("action_breakdowns", actionBreakdowns);
-      return this;
+    public APIRequestGetLeads(String nodeId, APIContext context) {
+      super(context, nodeId, "/leads", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetInsightsBase setActionBreakdowns (String actionBreakdowns) {
-      this.setParam("action_breakdowns", actionBreakdowns);
+    @Override
+    public APIRequestGetLeads setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetInsightsBase setActionReportTime (EnumActionReportTime actionReportTime) {
-      this.setParam("action_report_time", actionReportTime);
+    @Override
+    public APIRequestGetLeads setParams(Map<String, Object> params) {
+      setParamsInternal(params);
       return this;
     }
 
-    public APIRequestGetInsightsBase setActionReportTime (String actionReportTime) {
-      this.setParam("action_report_time", actionReportTime);
-      return this;
-    }
 
-    public APIRequestGetInsightsBase setBreakdowns (List<EnumBreakdowns> breakdowns) {
-      this.setParam("breakdowns", breakdowns);
-      return this;
+    public APIRequestGetLeads requestAllFields () {
+      return this.requestAllFields(true);
     }
 
-    public APIRequestGetInsightsBase setBreakdowns (String breakdowns) {
-      this.setParam("breakdowns", breakdowns);
+    public APIRequestGetLeads requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
 
-    public APIRequestGetInsightsBase setDatePreset (EnumDatePreset datePreset) {
-      this.setParam("date_preset", datePreset);
-      return this;
+    @Override
+    public APIRequestGetLeads requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
 
-    public APIRequestGetInsightsBase setDatePreset (String datePreset) {
-      this.setParam("date_preset", datePreset);
+    @Override
+    public APIRequestGetLeads requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
 
-    public APIRequestGetInsightsBase setLevel (EnumLevel level) {
-      this.setParam("level", level);
+    @Override
+    public APIRequestGetLeads requestField (String field) {
+      this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetInsightsBase setLevel (String level) {
-      this.setParam("level", level);
+    @Override
+    public APIRequestGetLeads requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetInsightsBase setProductIdLimit (Long productIdLimit) {
-      this.setParam("product_id_limit", productIdLimit);
-      return this;
+    public APIRequestGetLeads requestAdIdField () {
+      return this.requestAdIdField(true);
     }
-
-    public APIRequestGetInsightsBase setProductIdLimit (String productIdLimit) {
-      this.setParam("product_id_limit", productIdLimit);
+    public APIRequestGetLeads requestAdIdField (boolean value) {
+      this.requestField("ad_id", value);
       return this;
     }
-
-    public APIRequestGetInsightsBase setSummaryActionBreakdowns (List<EnumSummaryActionBreakdowns> summaryActionBreakdowns) {
-      this.setParam("summary_action_breakdowns", summaryActionBreakdowns);
-      return this;
+    public APIRequestGetLeads requestAdNameField () {
+      return this.requestAdNameField(true);
     }
-
-    public APIRequestGetInsightsBase setSummaryActionBreakdowns (String summaryActionBreakdowns) {
-      this.setParam("summary_action_breakdowns", summaryActionBreakdowns);
+    public APIRequestGetLeads requestAdNameField (boolean value) {
+      this.requestField("ad_name", value);
       return this;
     }
-
-    public APIRequestGetInsightsBase setTimeIncrement (String timeIncrement) {
-      this.setParam("time_increment", timeIncrement);
-      return this;
+    public APIRequestGetLeads requestAdsetIdField () {
+      return this.requestAdsetIdField(true);
     }
-
-
-    public APIRequestGetInsightsBase setTimeRange (String timeRange) {
-      this.setParam("time_range", timeRange);
+    public APIRequestGetLeads requestAdsetIdField (boolean value) {
+      this.requestField("adset_id", value);
       return this;
     }
-
-
-    public APIRequestGetInsightsBase setTimeRanges (List<String> timeRanges) {
-      this.setParam("time_ranges", timeRanges);
+    public APIRequestGetLeads requestAdsetNameField () {
+      return this.requestAdsetNameField(true);
+    }
+    public APIRequestGetLeads requestAdsetNameField (boolean value) {
+      this.requestField("adset_name", value);
       return this;
     }
-
-    public APIRequestGetInsightsBase setTimeRanges (String timeRanges) {
-      this.setParam("time_ranges", timeRanges);
+    public APIRequestGetLeads requestCampaignIdField () {
+      return this.requestCampaignIdField(true);
+    }
+    public APIRequestGetLeads requestCampaignIdField (boolean value) {
+      this.requestField("campaign_id", value);
       return this;
     }
-
-    public APIRequestGetInsightsBase requestAllFields () {
-      return this.requestAllFields(true);
+    public APIRequestGetLeads requestCampaignNameField () {
+      return this.requestCampaignNameField(true);
     }
-
-    public APIRequestGetInsightsBase requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
+    public APIRequestGetLeads requestCampaignNameField (boolean value) {
+      this.requestField("campaign_name", value);
       return this;
     }
-
-    public APIRequestGetInsightsBase requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
+    public APIRequestGetLeads requestCreatedTimeField () {
+      return this.requestCreatedTimeField(true);
     }
-
-    public APIRequestGetInsightsBase requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+    public APIRequestGetLeads requestCreatedTimeField (boolean value) {
+      this.requestField("created_time", value);
       return this;
     }
-
-    public APIRequestGetInsightsBase requestField (String field) {
-      this.requestField(field, true);
+    public APIRequestGetLeads requestCustomDisclaimerResponsesField () {
+      return this.requestCustomDisclaimerResponsesField(true);
+    }
+    public APIRequestGetLeads requestCustomDisclaimerResponsesField (boolean value) {
+      this.requestField("custom_disclaimer_responses", value);
       return this;
     }
-
-    public APIRequestGetInsightsBase requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
+    public APIRequestGetLeads requestFieldDataField () {
+      return this.requestFieldDataField(true);
+    }
+    public APIRequestGetLeads requestFieldDataField (boolean value) {
+      this.requestField("field_data", value);
       return this;
     }
-
-
-  }
-
-  public static class APIRequestGetInsights extends APIRequestGetInsightsBase<AdsInsights> {
-    @Override
-    public APINodeList<AdsInsights> parseResponse(String response) throws APIException {
-      return AdsInsights.parseResponse(response, getContext(), this);
+    public APIRequestGetLeads requestFormIdField () {
+      return this.requestFormIdField(true);
     }
-
-    @Override
-    public APINodeList<AdsInsights> execute() throws APIException {
-      return execute(new HashMap<String, Object>());
+    public APIRequestGetLeads requestFormIdField (boolean value) {
+      this.requestField("form_id", value);
+      return this;
     }
-
-    @Override
-    public APINodeList<AdsInsights> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
+    public APIRequestGetLeads requestIdField () {
+      return this.requestIdField(true);
     }
-
-    public APIRequestGetInsights(String nodeId, APIContext context) {
-      super(nodeId, context, "GET");
+    public APIRequestGetLeads requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
     }
-  }
-
-  public static class APIRequestGetInsightsAsync extends APIRequestGetInsightsBase<AdReportRun> {
-    @Override
-    public APINodeList<AdReportRun> parseResponse(String response) throws APIException {
-      return AdReportRun.parseResponse(response, getContext(), this);
+    public APIRequestGetLeads requestIsOrganicField () {
+      return this.requestIsOrganicField(true);
     }
-
-    @Override
-    public APINodeList<AdReportRun> execute() throws APIException {
-      return execute(new HashMap<String, Object>());
+    public APIRequestGetLeads requestIsOrganicField (boolean value) {
+      this.requestField("is_organic", value);
+      return this;
     }
-
-    @Override
-    public APINodeList<AdReportRun> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
+    public APIRequestGetLeads requestPostField () {
+      return this.requestPostField(true);
     }
-
-    public APIRequestGetInsightsAsync(String nodeId, APIContext context) {
-      super(nodeId, context, "POST");
+    public APIRequestGetLeads requestPostField (boolean value) {
+      this.requestField("post", value);
+      return this;
     }
   }
 
-  public static class APIRequestGetKeywordStats extends APIRequest<AdKeywordStats> {
+  public static class APIRequestGetPreviews extends APIRequest<AdPreview> {
 
-    APINodeList<AdKeywordStats> lastResponse = null;
+    APINodeList<AdPreview> lastResponse = null;
     @Override
-    public APINodeList<AdKeywordStats> getLastResponse() {
+    public APINodeList<AdPreview> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "date",
+      "ad_format",
+      "height",
+      "locale",
+      "post",
+      "product_item_ids",
+      "width",
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "name",
-      "impressions",
-      "unique_impressions",
-      "reach",
-      "clicks",
-      "unique_clicks",
-      "total_actions",
-      "total_unique_actions",
-      "actions",
-      "unique_actions",
-      "spend",
-      "ctr",
-      "unique_ctr",
-      "cpm",
-      "cpp",
-      "cpc",
-      "cost_per_total_action",
-      "cost_per_unique_click",
-      "frequency",
+      "body",
     };
 
     @Override
-    public APINodeList<AdKeywordStats> parseResponse(String response) throws APIException {
-      return AdKeywordStats.parseResponse(response, getContext(), this);
+    public APINodeList<AdPreview> parseResponse(String response) throws APIException {
+      return AdPreview.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINodeList<AdKeywordStats> execute() throws APIException {
+    public APINodeList<AdPreview> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<AdKeywordStats> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<AdPreview> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetKeywordStats(String nodeId, APIContext context) {
-      super(context, nodeId, "/keywordstats", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetPreviews(String nodeId, APIContext context) {
+      super(context, nodeId, "/previews", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetKeywordStats setParam(String param, Object value) {
+    @Override
+    public APIRequestGetPreviews setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetKeywordStats setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetPreviews setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetKeywordStats setDate (String date) {
-      this.setParam("date", date);
+    public APIRequestGetPreviews setAdFormat (AdPreview.EnumAdFormat adFormat) {
+      this.setParam("ad_format", adFormat);
+      return this;
+    }
+    public APIRequestGetPreviews setAdFormat (String adFormat) {
+      this.setParam("ad_format", adFormat);
+      return this;
+    }
+
+    public APIRequestGetPreviews setHeight (Long height) {
+      this.setParam("height", height);
+      return this;
+    }
+    public APIRequestGetPreviews setHeight (String height) {
+      this.setParam("height", height);
       return this;
     }
 
+    public APIRequestGetPreviews setLocale (String locale) {
+      this.setParam("locale", locale);
+      return this;
+    }
 
-    public APIRequestGetKeywordStats requestAllFields () {
+    public APIRequestGetPreviews setPost (Object post) {
+      this.setParam("post", post);
+      return this;
+    }
+    public APIRequestGetPreviews setPost (String post) {
+      this.setParam("post", post);
+      return this;
+    }
+
+    public APIRequestGetPreviews setProductItemIds (List<Long> productItemIds) {
+      this.setParam("product_item_ids", productItemIds);
+      return this;
+    }
+    public APIRequestGetPreviews setProductItemIds (String productItemIds) {
+      this.setParam("product_item_ids", productItemIds);
+      return this;
+    }
+
+    public APIRequestGetPreviews setWidth (Long width) {
+      this.setParam("width", width);
+      return this;
+    }
+    public APIRequestGetPreviews setWidth (String width) {
+      this.setParam("width", width);
+      return this;
+    }
+
+    public APIRequestGetPreviews requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetKeywordStats requestAllFields (boolean value) {
+    public APIRequestGetPreviews requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetKeywordStats requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetPreviews requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetKeywordStats requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetPreviews requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetKeywordStats requestField (String field) {
+    @Override
+    public APIRequestGetPreviews requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetKeywordStats requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetPreviews requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetKeywordStats requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGetKeywordStats requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
-    }
-    public APIRequestGetKeywordStats requestNameField () {
-      return this.requestNameField(true);
-    }
-    public APIRequestGetKeywordStats requestNameField (boolean value) {
-      this.requestField("name", value);
-      return this;
-    }
-    public APIRequestGetKeywordStats requestImpressionsField () {
-      return this.requestImpressionsField(true);
-    }
-    public APIRequestGetKeywordStats requestImpressionsField (boolean value) {
-      this.requestField("impressions", value);
-      return this;
-    }
-    public APIRequestGetKeywordStats requestUniqueImpressionsField () {
-      return this.requestUniqueImpressionsField(true);
-    }
-    public APIRequestGetKeywordStats requestUniqueImpressionsField (boolean value) {
-      this.requestField("unique_impressions", value);
-      return this;
-    }
-    public APIRequestGetKeywordStats requestReachField () {
-      return this.requestReachField(true);
+    public APIRequestGetPreviews requestBodyField () {
+      return this.requestBodyField(true);
     }
-    public APIRequestGetKeywordStats requestReachField (boolean value) {
-      this.requestField("reach", value);
+    public APIRequestGetPreviews requestBodyField (boolean value) {
+      this.requestField("body", value);
       return this;
     }
-    public APIRequestGetKeywordStats requestClicksField () {
-      return this.requestClicksField(true);
+  }
+
+  public static class APIRequestGetReachEstimate extends APIRequest<ReachEstimate> {
+
+    APINodeList<ReachEstimate> lastResponse = null;
+    @Override
+    public APINodeList<ReachEstimate> getLastResponse() {
+      return lastResponse;
     }
-    public APIRequestGetKeywordStats requestClicksField (boolean value) {
-      this.requestField("clicks", value);
-      return this;
+    public static final String[] PARAMS = {
+      "currency",
+      "daily_budget",
+      "optimize_for",
+    };
+
+    public static final String[] FIELDS = {
+      "bid_estimations",
+      "estimate_ready",
+      "unsupported",
+      "users",
+    };
+
+    @Override
+    public APINodeList<ReachEstimate> parseResponse(String response) throws APIException {
+      return ReachEstimate.parseResponse(response, getContext(), this);
     }
-    public APIRequestGetKeywordStats requestUniqueClicksField () {
-      return this.requestUniqueClicksField(true);
+
+    @Override
+    public APINodeList<ReachEstimate> execute() throws APIException {
+      return execute(new HashMap<String, Object>());
     }
-    public APIRequestGetKeywordStats requestUniqueClicksField (boolean value) {
-      this.requestField("unique_clicks", value);
-      return this;
+
+    @Override
+    public APINodeList<ReachEstimate> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
     }
-    public APIRequestGetKeywordStats requestTotalActionsField () {
-      return this.requestTotalActionsField(true);
+
+    public APIRequestGetReachEstimate(String nodeId, APIContext context) {
+      super(context, nodeId, "/reachestimate", "GET", Arrays.asList(PARAMS));
     }
-    public APIRequestGetKeywordStats requestTotalActionsField (boolean value) {
-      this.requestField("total_actions", value);
+
+    @Override
+    public APIRequestGetReachEstimate setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
-    public APIRequestGetKeywordStats requestTotalUniqueActionsField () {
-      return this.requestTotalUniqueActionsField(true);
-    }
-    public APIRequestGetKeywordStats requestTotalUniqueActionsField (boolean value) {
-      this.requestField("total_unique_actions", value);
+
+    @Override
+    public APIRequestGetReachEstimate setParams(Map<String, Object> params) {
+      setParamsInternal(params);
       return this;
     }
-    public APIRequestGetKeywordStats requestActionsField () {
-      return this.requestActionsField(true);
-    }
-    public APIRequestGetKeywordStats requestActionsField (boolean value) {
-      this.requestField("actions", value);
+
+
+    public APIRequestGetReachEstimate setCurrency (String currency) {
+      this.setParam("currency", currency);
       return this;
     }
-    public APIRequestGetKeywordStats requestUniqueActionsField () {
-      return this.requestUniqueActionsField(true);
-    }
-    public APIRequestGetKeywordStats requestUniqueActionsField (boolean value) {
-      this.requestField("unique_actions", value);
+
+    public APIRequestGetReachEstimate setDailyBudget (Double dailyBudget) {
+      this.setParam("daily_budget", dailyBudget);
       return this;
     }
-    public APIRequestGetKeywordStats requestSpendField () {
-      return this.requestSpendField(true);
-    }
-    public APIRequestGetKeywordStats requestSpendField (boolean value) {
-      this.requestField("spend", value);
+    public APIRequestGetReachEstimate setDailyBudget (String dailyBudget) {
+      this.setParam("daily_budget", dailyBudget);
       return this;
     }
-    public APIRequestGetKeywordStats requestCtrField () {
-      return this.requestCtrField(true);
+
+    public APIRequestGetReachEstimate setOptimizeFor (ReachEstimate.EnumOptimizeFor optimizeFor) {
+      this.setParam("optimize_for", optimizeFor);
+      return this;
     }
-    public APIRequestGetKeywordStats requestCtrField (boolean value) {
-      this.requestField("ctr", value);
+    public APIRequestGetReachEstimate setOptimizeFor (String optimizeFor) {
+      this.setParam("optimize_for", optimizeFor);
       return this;
     }
-    public APIRequestGetKeywordStats requestUniqueCtrField () {
-      return this.requestUniqueCtrField(true);
+
+    public APIRequestGetReachEstimate requestAllFields () {
+      return this.requestAllFields(true);
     }
-    public APIRequestGetKeywordStats requestUniqueCtrField (boolean value) {
-      this.requestField("unique_ctr", value);
+
+    public APIRequestGetReachEstimate requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetKeywordStats requestCpmField () {
-      return this.requestCpmField(true);
+
+    @Override
+    public APIRequestGetReachEstimate requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
-    public APIRequestGetKeywordStats requestCpmField (boolean value) {
-      this.requestField("cpm", value);
+
+    @Override
+    public APIRequestGetReachEstimate requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetKeywordStats requestCppField () {
-      return this.requestCppField(true);
+
+    @Override
+    public APIRequestGetReachEstimate requestField (String field) {
+      this.requestField(field, true);
+      return this;
     }
-    public APIRequestGetKeywordStats requestCppField (boolean value) {
-      this.requestField("cpp", value);
+
+    @Override
+    public APIRequestGetReachEstimate requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
-    public APIRequestGetKeywordStats requestCpcField () {
-      return this.requestCpcField(true);
+
+    public APIRequestGetReachEstimate requestBidEstimationsField () {
+      return this.requestBidEstimationsField(true);
     }
-    public APIRequestGetKeywordStats requestCpcField (boolean value) {
-      this.requestField("cpc", value);
+    public APIRequestGetReachEstimate requestBidEstimationsField (boolean value) {
+      this.requestField("bid_estimations", value);
       return this;
     }
-    public APIRequestGetKeywordStats requestCostPerTotalActionField () {
-      return this.requestCostPerTotalActionField(true);
+    public APIRequestGetReachEstimate requestEstimateReadyField () {
+      return this.requestEstimateReadyField(true);
     }
-    public APIRequestGetKeywordStats requestCostPerTotalActionField (boolean value) {
-      this.requestField("cost_per_total_action", value);
+    public APIRequestGetReachEstimate requestEstimateReadyField (boolean value) {
+      this.requestField("estimate_ready", value);
       return this;
     }
-    public APIRequestGetKeywordStats requestCostPerUniqueClickField () {
-      return this.requestCostPerUniqueClickField(true);
+    public APIRequestGetReachEstimate requestUnsupportedField () {
+      return this.requestUnsupportedField(true);
     }
-    public APIRequestGetKeywordStats requestCostPerUniqueClickField (boolean value) {
-      this.requestField("cost_per_unique_click", value);
+    public APIRequestGetReachEstimate requestUnsupportedField (boolean value) {
+      this.requestField("unsupported", value);
       return this;
     }
-    public APIRequestGetKeywordStats requestFrequencyField () {
-      return this.requestFrequencyField(true);
+    public APIRequestGetReachEstimate requestUsersField () {
+      return this.requestUsersField(true);
     }
-    public APIRequestGetKeywordStats requestFrequencyField (boolean value) {
-      this.requestField("frequency", value);
+    public APIRequestGetReachEstimate requestUsersField (boolean value) {
+      this.requestField("users", value);
       return this;
     }
-
   }
 
-  public static class APIRequestGetPreviews extends APIRequest<AdPreview> {
+  public static class APIRequestGetTargetingSentenceLines extends APIRequest<TargetingSentenceLine> {
 
-    APINodeList<AdPreview> lastResponse = null;
+    APINodeList<TargetingSentenceLine> lastResponse = null;
     @Override
-    public APINodeList<AdPreview> getLastResponse() {
+    public APINodeList<TargetingSentenceLine> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "ad_format",
-      "post",
-      "height",
-      "width",
-      "product_item_ids",
-      "locale",
     };
 
     public static final String[] FIELDS = {
-      "body",
+      "id",
+      "params",
+      "targetingsentencelines",
     };
 
     @Override
-    public APINodeList<AdPreview> parseResponse(String response) throws APIException {
-      return AdPreview.parseResponse(response, getContext(), this);
+    public APINodeList<TargetingSentenceLine> parseResponse(String response) throws APIException {
+      return TargetingSentenceLine.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINodeList<AdPreview> execute() throws APIException {
+    public APINodeList<TargetingSentenceLine> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<AdPreview> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<TargetingSentenceLine> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetPreviews(String nodeId, APIContext context) {
-      super(context, nodeId, "/previews", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetTargetingSentenceLines(String nodeId, APIContext context) {
+      super(context, nodeId, "/targetingsentencelines", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetPreviews setParam(String param, Object value) {
+    @Override
+    public APIRequestGetTargetingSentenceLines setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetPreviews setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetTargetingSentenceLines setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetPreviews setAdFormat (EnumAdFormat adFormat) {
-      this.setParam("ad_format", adFormat);
-      return this;
+    public APIRequestGetTargetingSentenceLines requestAllFields () {
+      return this.requestAllFields(true);
     }
 
-    public APIRequestGetPreviews setAdFormat (String adFormat) {
-      this.setParam("ad_format", adFormat);
+    public APIRequestGetTargetingSentenceLines requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
 
-    public APIRequestGetPreviews setPost (Object post) {
-      this.setParam("post", post);
-      return this;
+    @Override
+    public APIRequestGetTargetingSentenceLines requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
 
-    public APIRequestGetPreviews setPost (String post) {
-      this.setParam("post", post);
+    @Override
+    public APIRequestGetTargetingSentenceLines requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
 
-    public APIRequestGetPreviews setHeight (Long height) {
-      this.setParam("height", height);
+    @Override
+    public APIRequestGetTargetingSentenceLines requestField (String field) {
+      this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetPreviews setHeight (String height) {
-      this.setParam("height", height);
+    @Override
+    public APIRequestGetTargetingSentenceLines requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetPreviews setWidth (Long width) {
-      this.setParam("width", width);
+    public APIRequestGetTargetingSentenceLines requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGetTargetingSentenceLines requestIdField (boolean value) {
+      this.requestField("id", value);
       return this;
     }
-
-    public APIRequestGetPreviews setWidth (String width) {
-      this.setParam("width", width);
+    public APIRequestGetTargetingSentenceLines requestParamsField () {
+      return this.requestParamsField(true);
+    }
+    public APIRequestGetTargetingSentenceLines requestParamsField (boolean value) {
+      this.requestField("params", value);
       return this;
     }
-
-    public APIRequestGetPreviews setProductItemIds (List<String> productItemIds) {
-      this.setParam("product_item_ids", productItemIds);
+    public APIRequestGetTargetingSentenceLines requestTargetingsentencelinesField () {
+      return this.requestTargetingsentencelinesField(true);
+    }
+    public APIRequestGetTargetingSentenceLines requestTargetingsentencelinesField (boolean value) {
+      this.requestField("targetingsentencelines", value);
       return this;
     }
+  }
 
-    public APIRequestGetPreviews setProductItemIds (String productItemIds) {
-      this.setParam("product_item_ids", productItemIds);
+  public static class APIRequestDelete extends APIRequest<APINode> {
+
+    APINode lastResponse = null;
+    @Override
+    public APINode getLastResponse() {
+      return lastResponse;
+    }
+    public static final String[] PARAMS = {
+    };
+
+    public static final String[] FIELDS = {
+    };
+
+    @Override
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
+    }
+
+    @Override
+    public APINode execute() throws APIException {
+      return execute(new HashMap<String, Object>());
+    }
+
+    @Override
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
+    }
+
+    public APIRequestDelete(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "DELETE", Arrays.asList(PARAMS));
+    }
+
+    @Override
+    public APIRequestDelete setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetPreviews setLocale (String locale) {
-      this.setParam("locale", locale);
+    @Override
+    public APIRequestDelete setParams(Map<String, Object> params) {
+      setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetPreviews requestAllFields () {
+    public APIRequestDelete requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetPreviews requestAllFields (boolean value) {
+    public APIRequestDelete requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetPreviews requestFields (List<String> fields) {
+    @Override
+    public APIRequestDelete requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetPreviews requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestDelete requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetPreviews requestField (String field) {
+    @Override
+    public APIRequestDelete requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetPreviews requestField (String field, boolean value) {
+    @Override
+    public APIRequestDelete requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetPreviews requestBodyField () {
-      return this.requestBodyField(true);
-    }
-    public APIRequestGetPreviews requestBodyField (boolean value) {
-      this.requestField("body", value);
-      return this;
-    }
-
   }
 
-  public static class APIRequestGetReachEstimate extends APIRequest<ReachEstimate> {
+  public static class APIRequestGet extends APIRequest<Ad> {
 
-    APINodeList<ReachEstimate> lastResponse = null;
+    Ad lastResponse = null;
     @Override
-    public APINodeList<ReachEstimate> getLastResponse() {
+    public Ad getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "targeting_spec",
-      "currency",
-      "optimize_for",
-      "daily_budget",
-      "creative_action_spec",
-      "concepts",
-      "caller_id",
     };
 
     public static final String[] FIELDS = {
-      "users",
-      "bid_estimations",
-      "estimate_ready",
-      "unsupported",
+      "account_id",
+      "ad_review_feedback",
+      "adlabels",
+      "adset",
+      "adset_id",
+      "bid_amount",
+      "bid_info",
+      "bid_type",
+      "campaign",
+      "campaign_id",
+      "configured_status",
+      "conversion_specs",
+      "created_time",
+      "creative",
+      "effective_status",
+      "id",
+      "last_updated_by_app_id",
+      "name",
+      "recommendations",
+      "status",
+      "tracking_specs",
+      "updated_time",
     };
 
     @Override
-    public APINodeList<ReachEstimate> parseResponse(String response) throws APIException {
-      return ReachEstimate.parseResponse(response, getContext(), this);
+    public Ad parseResponse(String response) throws APIException {
+      return Ad.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public APINodeList<ReachEstimate> execute() throws APIException {
+    public Ad execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<ReachEstimate> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public Ad execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetReachEstimate(String nodeId, APIContext context) {
-      super(context, nodeId, "/reachestimate", "GET", Arrays.asList(PARAMS));
+    public APIRequestGet(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetReachEstimate setParam(String param, Object value) {
+    @Override
+    public APIRequestGet setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetReachEstimate setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGet setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetReachEstimate setTargetingSpec (String targetingSpec) {
-      this.setParam("targeting_spec", targetingSpec);
-      return this;
+    public APIRequestGet requestAllFields () {
+      return this.requestAllFields(true);
     }
 
-
-    public APIRequestGetReachEstimate setCurrency (String currency) {
-      this.setParam("currency", currency);
+    public APIRequestGet requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
 
+    @Override
+    public APIRequestGet requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
+    }
 
-    public APIRequestGetReachEstimate setOptimizeFor (EnumOptimizeFor optimizeFor) {
-      this.setParam("optimize_for", optimizeFor);
+    @Override
+    public APIRequestGet requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
 
-    public APIRequestGetReachEstimate setOptimizeFor (String optimizeFor) {
-      this.setParam("optimize_for", optimizeFor);
+    @Override
+    public APIRequestGet requestField (String field) {
+      this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetReachEstimate setDailyBudget (Double dailyBudget) {
-      this.setParam("daily_budget", dailyBudget);
+    @Override
+    public APIRequestGet requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetReachEstimate setDailyBudget (String dailyBudget) {
-      this.setParam("daily_budget", dailyBudget);
+    public APIRequestGet requestAccountIdField () {
+      return this.requestAccountIdField(true);
+    }
+    public APIRequestGet requestAccountIdField (boolean value) {
+      this.requestField("account_id", value);
       return this;
     }
-
-    public APIRequestGetReachEstimate setCreativeActionSpec (String creativeActionSpec) {
-      this.setParam("creative_action_spec", creativeActionSpec);
+    public APIRequestGet requestAdReviewFeedbackField () {
+      return this.requestAdReviewFeedbackField(true);
+    }
+    public APIRequestGet requestAdReviewFeedbackField (boolean value) {
+      this.requestField("ad_review_feedback", value);
       return this;
     }
-
-
-    public APIRequestGetReachEstimate setConcepts (String concepts) {
-      this.setParam("concepts", concepts);
+    public APIRequestGet requestAdlabelsField () {
+      return this.requestAdlabelsField(true);
+    }
+    public APIRequestGet requestAdlabelsField (boolean value) {
+      this.requestField("adlabels", value);
       return this;
     }
-
-
-    public APIRequestGetReachEstimate setCallerId (String callerId) {
-      this.setParam("caller_id", callerId);
+    public APIRequestGet requestAdsetField () {
+      return this.requestAdsetField(true);
+    }
+    public APIRequestGet requestAdsetField (boolean value) {
+      this.requestField("adset", value);
       return this;
     }
-
-
-    public APIRequestGetReachEstimate requestAllFields () {
-      return this.requestAllFields(true);
+    public APIRequestGet requestAdsetIdField () {
+      return this.requestAdsetIdField(true);
     }
-
-    public APIRequestGetReachEstimate requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
+    public APIRequestGet requestAdsetIdField (boolean value) {
+      this.requestField("adset_id", value);
       return this;
     }
-
-    public APIRequestGetReachEstimate requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
+    public APIRequestGet requestBidAmountField () {
+      return this.requestBidAmountField(true);
     }
-
-    public APIRequestGetReachEstimate requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+    public APIRequestGet requestBidAmountField (boolean value) {
+      this.requestField("bid_amount", value);
       return this;
     }
-
-    public APIRequestGetReachEstimate requestField (String field) {
-      this.requestField(field, true);
+    public APIRequestGet requestBidInfoField () {
+      return this.requestBidInfoField(true);
+    }
+    public APIRequestGet requestBidInfoField (boolean value) {
+      this.requestField("bid_info", value);
       return this;
     }
-
-    public APIRequestGetReachEstimate requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
+    public APIRequestGet requestBidTypeField () {
+      return this.requestBidTypeField(true);
+    }
+    public APIRequestGet requestBidTypeField (boolean value) {
+      this.requestField("bid_type", value);
       return this;
     }
-
-    public APIRequestGetReachEstimate requestUsersField () {
-      return this.requestUsersField(true);
+    public APIRequestGet requestCampaignField () {
+      return this.requestCampaignField(true);
     }
-    public APIRequestGetReachEstimate requestUsersField (boolean value) {
-      this.requestField("users", value);
+    public APIRequestGet requestCampaignField (boolean value) {
+      this.requestField("campaign", value);
       return this;
     }
-    public APIRequestGetReachEstimate requestBidEstimationsField () {
-      return this.requestBidEstimationsField(true);
+    public APIRequestGet requestCampaignIdField () {
+      return this.requestCampaignIdField(true);
     }
-    public APIRequestGetReachEstimate requestBidEstimationsField (boolean value) {
-      this.requestField("bid_estimations", value);
+    public APIRequestGet requestCampaignIdField (boolean value) {
+      this.requestField("campaign_id", value);
       return this;
     }
-    public APIRequestGetReachEstimate requestEstimateReadyField () {
-      return this.requestEstimateReadyField(true);
+    public APIRequestGet requestConfiguredStatusField () {
+      return this.requestConfiguredStatusField(true);
+    }
+    public APIRequestGet requestConfiguredStatusField (boolean value) {
+      this.requestField("configured_status", value);
+      return this;
+    }
+    public APIRequestGet requestConversionSpecsField () {
+      return this.requestConversionSpecsField(true);
+    }
+    public APIRequestGet requestConversionSpecsField (boolean value) {
+      this.requestField("conversion_specs", value);
+      return this;
+    }
+    public APIRequestGet requestCreatedTimeField () {
+      return this.requestCreatedTimeField(true);
+    }
+    public APIRequestGet requestCreatedTimeField (boolean value) {
+      this.requestField("created_time", value);
+      return this;
+    }
+    public APIRequestGet requestCreativeField () {
+      return this.requestCreativeField(true);
+    }
+    public APIRequestGet requestCreativeField (boolean value) {
+      this.requestField("creative", value);
+      return this;
+    }
+    public APIRequestGet requestEffectiveStatusField () {
+      return this.requestEffectiveStatusField(true);
+    }
+    public APIRequestGet requestEffectiveStatusField (boolean value) {
+      this.requestField("effective_status", value);
+      return this;
+    }
+    public APIRequestGet requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGet requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
+    public APIRequestGet requestLastUpdatedByAppIdField () {
+      return this.requestLastUpdatedByAppIdField(true);
+    }
+    public APIRequestGet requestLastUpdatedByAppIdField (boolean value) {
+      this.requestField("last_updated_by_app_id", value);
+      return this;
+    }
+    public APIRequestGet requestNameField () {
+      return this.requestNameField(true);
+    }
+    public APIRequestGet requestNameField (boolean value) {
+      this.requestField("name", value);
+      return this;
+    }
+    public APIRequestGet requestRecommendationsField () {
+      return this.requestRecommendationsField(true);
+    }
+    public APIRequestGet requestRecommendationsField (boolean value) {
+      this.requestField("recommendations", value);
+      return this;
+    }
+    public APIRequestGet requestStatusField () {
+      return this.requestStatusField(true);
+    }
+    public APIRequestGet requestStatusField (boolean value) {
+      this.requestField("status", value);
+      return this;
     }
-    public APIRequestGetReachEstimate requestEstimateReadyField (boolean value) {
-      this.requestField("estimate_ready", value);
+    public APIRequestGet requestTrackingSpecsField () {
+      return this.requestTrackingSpecsField(true);
+    }
+    public APIRequestGet requestTrackingSpecsField (boolean value) {
+      this.requestField("tracking_specs", value);
       return this;
     }
-    public APIRequestGetReachEstimate requestUnsupportedField () {
-      return this.requestUnsupportedField(true);
+    public APIRequestGet requestUpdatedTimeField () {
+      return this.requestUpdatedTimeField(true);
     }
-    public APIRequestGetReachEstimate requestUnsupportedField (boolean value) {
-      this.requestField("unsupported", value);
+    public APIRequestGet requestUpdatedTimeField (boolean value) {
+      this.requestField("updated_time", value);
       return this;
     }
-
   }
 
-  public static class APIRequestGetTargetingSentenceLines extends APIRequest<TargetingSentenceLine> {
+  public static class APIRequestUpdate extends APIRequest<APINode> {
 
-    APINodeList<TargetingSentenceLine> lastResponse = null;
+    APINode lastResponse = null;
     @Override
-    public APINodeList<TargetingSentenceLine> getLastResponse() {
+    public APINode getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
+      "adlabels",
+      "adset_id",
+      "bid_amount",
+      "creative",
+      "display_sequence",
+      "execution_options",
+      "name",
+      "redownload",
+      "status",
+      "tracking_specs",
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "params",
-      "targetingsentencelines",
     };
 
     @Override
-    public APINodeList<TargetingSentenceLine> parseResponse(String response) throws APIException {
-      return TargetingSentenceLine.parseResponse(response, getContext(), this);
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public APINodeList<TargetingSentenceLine> execute() throws APIException {
+    public APINode execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<TargetingSentenceLine> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetTargetingSentenceLines(String nodeId, APIContext context) {
-      super(context, nodeId, "/targetingsentencelines", "GET", Arrays.asList(PARAMS));
+    public APIRequestUpdate(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "POST", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetTargetingSentenceLines setParam(String param, Object value) {
+    @Override
+    public APIRequestUpdate setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetTargetingSentenceLines setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestUpdate setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetTargetingSentenceLines requestAllFields () {
-      return this.requestAllFields(true);
-    }
-
-    public APIRequestGetTargetingSentenceLines requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
+    public APIRequestUpdate setAdlabels (List<Object> adlabels) {
+      this.setParam("adlabels", adlabels);
       return this;
     }
-
-    public APIRequestGetTargetingSentenceLines requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
-    }
-
-    public APIRequestGetTargetingSentenceLines requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+    public APIRequestUpdate setAdlabels (String adlabels) {
+      this.setParam("adlabels", adlabels);
       return this;
     }
 
-    public APIRequestGetTargetingSentenceLines requestField (String field) {
-      this.requestField(field, true);
+    public APIRequestUpdate setAdsetId (Long adsetId) {
+      this.setParam("adset_id", adsetId);
       return this;
     }
-
-    public APIRequestGetTargetingSentenceLines requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
+    public APIRequestUpdate setAdsetId (String adsetId) {
+      this.setParam("adset_id", adsetId);
       return this;
     }
 
-    public APIRequestGetTargetingSentenceLines requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGetTargetingSentenceLines requestIdField (boolean value) {
-      this.requestField("id", value);
+    public APIRequestUpdate setBidAmount (Long bidAmount) {
+      this.setParam("bid_amount", bidAmount);
       return this;
     }
-    public APIRequestGetTargetingSentenceLines requestParamsField () {
-      return this.requestParamsField(true);
-    }
-    public APIRequestGetTargetingSentenceLines requestParamsField (boolean value) {
-      this.requestField("params", value);
+    public APIRequestUpdate setBidAmount (String bidAmount) {
+      this.setParam("bid_amount", bidAmount);
       return this;
     }
-    public APIRequestGetTargetingSentenceLines requestTargetingsentencelinesField () {
-      return this.requestTargetingsentencelinesField(true);
-    }
-    public APIRequestGetTargetingSentenceLines requestTargetingsentencelinesField (boolean value) {
-      this.requestField("targetingsentencelines", value);
+
+    public APIRequestUpdate setCreative (AdCreative creative) {
+      this.setParam("creative", creative);
       return this;
     }
-
-  }
-
-  public static class APIRequestDeleteAdLabels extends APIRequest<APINode> {
-
-    APINodeList<APINode> lastResponse = null;
-    @Override
-    public APINodeList<APINode> getLastResponse() {
-      return lastResponse;
+    public APIRequestUpdate setCreative (String creative) {
+      this.setParam("creative", creative);
+      return this;
     }
-    public static final String[] PARAMS = {
-      "id",
-      "adlabels",
-      "execution_options",
-    };
-
-    public static final String[] FIELDS = {
-    };
 
-    @Override
-    public APINodeList<APINode> parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this);
+    public APIRequestUpdate setDisplaySequence (Long displaySequence) {
+      this.setParam("display_sequence", displaySequence);
+      return this;
     }
-
-    @Override
-    public APINodeList<APINode> execute() throws APIException {
-      return execute(new HashMap<String, Object>());
+    public APIRequestUpdate setDisplaySequence (String displaySequence) {
+      this.setParam("display_sequence", displaySequence);
+      return this;
     }
 
-    @Override
-    public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
+    public APIRequestUpdate setExecutionOptions (List<Ad.EnumExecutionOptions> executionOptions) {
+      this.setParam("execution_options", executionOptions);
+      return this;
     }
-
-    public APIRequestDeleteAdLabels(String nodeId, APIContext context) {
-      super(context, nodeId, "/adlabels", "DELETE", Arrays.asList(PARAMS));
+    public APIRequestUpdate setExecutionOptions (String executionOptions) {
+      this.setParam("execution_options", executionOptions);
+      return this;
     }
 
-    public APIRequestDeleteAdLabels setParam(String param, Object value) {
-      setParamInternal(param, value);
+    public APIRequestUpdate setName (String name) {
+      this.setParam("name", name);
       return this;
     }
 
-    public APIRequestDeleteAdLabels setParams(Map<String, Object> params) {
-      setParamsInternal(params);
+    public APIRequestUpdate setRedownload (Boolean redownload) {
+      this.setParam("redownload", redownload);
       return this;
     }
-
-
-    public APIRequestDeleteAdLabels setId (String id) {
-      this.setParam("id", id);
+    public APIRequestUpdate setRedownload (String redownload) {
+      this.setParam("redownload", redownload);
       return this;
     }
 
-
-    public APIRequestDeleteAdLabels setAdlabels (List<Object> adlabels) {
-      this.setParam("adlabels", adlabels);
+    public APIRequestUpdate setStatus (Ad.EnumStatus status) {
+      this.setParam("status", status);
       return this;
     }
-
-    public APIRequestDeleteAdLabels setAdlabels (String adlabels) {
-      this.setParam("adlabels", adlabels);
+    public APIRequestUpdate setStatus (String status) {
+      this.setParam("status", status);
       return this;
     }
 
-    public APIRequestDeleteAdLabels setExecutionOptions (List<EnumDeleteAdLabelsExecutionOptions> executionOptions) {
-      this.setParam("execution_options", executionOptions);
+    public APIRequestUpdate setTrackingSpecs (Object trackingSpecs) {
+      this.setParam("tracking_specs", trackingSpecs);
       return this;
     }
-
-    public APIRequestDeleteAdLabels setExecutionOptions (String executionOptions) {
-      this.setParam("execution_options", executionOptions);
+    public APIRequestUpdate setTrackingSpecs (String trackingSpecs) {
+      this.setParam("tracking_specs", trackingSpecs);
       return this;
     }
 
-    public APIRequestDeleteAdLabels requestAllFields () {
+    public APIRequestUpdate requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestDeleteAdLabels requestAllFields (boolean value) {
+    public APIRequestUpdate requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestDeleteAdLabels requestFields (List<String> fields) {
+    @Override
+    public APIRequestUpdate requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestDeleteAdLabels requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestUpdate requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestDeleteAdLabels requestField (String field) {
+    @Override
+    public APIRequestUpdate requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestDeleteAdLabels requestField (String field, boolean value) {
+    @Override
+    public APIRequestUpdate requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
   }
 
-  public static enum EnumStatus {
-    @SerializedName("ACTIVE")
-    VALUE_ACTIVE("ACTIVE"),
-    @SerializedName("PAUSED")
-    VALUE_PAUSED("PAUSED"),
-    @SerializedName("ARCHIVED")
-    VALUE_ARCHIVED("ARCHIVED"),
-    @SerializedName("DELETED")
-    VALUE_DELETED("DELETED"),
-    NULL(null);
-
-    private String value;
-
-    private EnumStatus(String value) {
-      this.value = value;
-    }
-
-    @Override
-    public String toString() {
-      return value;
-    }
-  }
-  public static enum EnumUpdateExecutionOptions {
-    @SerializedName("VALIDATE_ONLY")
-    VALUE_VALIDATE_ONLY("VALIDATE_ONLY"),
-    @SerializedName("INCLUDE_WARNINGS")
-    VALUE_INCLUDE_WARNINGS("INCLUDE_WARNINGS"),
-    NULL(null);
-
-    private String value;
-
-    private EnumUpdateExecutionOptions(String value) {
-      this.value = value;
-    }
-
-    @Override
-    public String toString() {
-      return value;
-    }
-  }
-  public static enum EnumFields {
-    @SerializedName("frequency_value")
-    VALUE_FREQUENCY_VALUE("frequency_value"),
-    @SerializedName("age")
-    VALUE_AGE("age"),
-    @SerializedName("country")
-    VALUE_COUNTRY("country"),
-    @SerializedName("gender")
-    VALUE_GENDER("gender"),
-    @SerializedName("impression_device")
-    VALUE_IMPRESSION_DEVICE("impression_device"),
-    @SerializedName("place_page_id")
-    VALUE_PLACE_PAGE_ID("place_page_id"),
-    @SerializedName("placement")
-    VALUE_PLACEMENT("placement"),
-    @SerializedName("product_id")
-    VALUE_PRODUCT_ID("product_id"),
-    @SerializedName("region")
-    VALUE_REGION("region"),
-    @SerializedName("hourly_stats_aggregated_by_advertiser_time_zone")
-    VALUE_HOURLY_STATS_AGGREGATED_BY_ADVERTISER_TIME_ZONE("hourly_stats_aggregated_by_advertiser_time_zone"),
-    @SerializedName("hourly_stats_aggregated_by_audience_time_zone")
-    VALUE_HOURLY_STATS_AGGREGATED_BY_AUDIENCE_TIME_ZONE("hourly_stats_aggregated_by_audience_time_zone"),
-    @SerializedName("account_id")
-    VALUE_ACCOUNT_ID("account_id"),
-    @SerializedName("account_name")
-    VALUE_ACCOUNT_NAME("account_name"),
-    @SerializedName("action_values")
-    VALUE_ACTION_VALUES("action_values"),
-    @SerializedName("actions")
-    VALUE_ACTIONS("actions"),
-    @SerializedName("unique_actions")
-    VALUE_UNIQUE_ACTIONS("unique_actions"),
-    @SerializedName("app_store_clicks")
-    VALUE_APP_STORE_CLICKS("app_store_clicks"),
-    @SerializedName("buying_type")
-    VALUE_BUYING_TYPE("buying_type"),
-    @SerializedName("call_to_action_clicks")
-    VALUE_CALL_TO_ACTION_CLICKS("call_to_action_clicks"),
-    @SerializedName("card_views")
-    VALUE_CARD_VIEWS("card_views"),
-    @SerializedName("unique_clicks")
-    VALUE_UNIQUE_CLICKS("unique_clicks"),
-    @SerializedName("cost_per_action_type")
-    VALUE_COST_PER_ACTION_TYPE("cost_per_action_type"),
-    @SerializedName("cost_per_unique_action_type")
-    VALUE_COST_PER_UNIQUE_ACTION_TYPE("cost_per_unique_action_type"),
-    @SerializedName("cost_per_inline_post_engagement")
-    VALUE_COST_PER_INLINE_POST_ENGAGEMENT("cost_per_inline_post_engagement"),
-    @SerializedName("cost_per_inline_link_click")
-    VALUE_COST_PER_INLINE_LINK_CLICK("cost_per_inline_link_click"),
-    @SerializedName("cost_per_total_action")
-    VALUE_COST_PER_TOTAL_ACTION("cost_per_total_action"),
-    @SerializedName("cost_per_10_sec_video_view")
-    VALUE_COST_PER_10_SEC_VIDEO_VIEW("cost_per_10_sec_video_view"),
-    @SerializedName("cost_per_unique_click")
-    VALUE_COST_PER_UNIQUE_CLICK("cost_per_unique_click"),
-    @SerializedName("cpm")
-    VALUE_CPM("cpm"),
-    @SerializedName("cpp")
-    VALUE_CPP("cpp"),
-    @SerializedName("ctr")
-    VALUE_CTR("ctr"),
-    @SerializedName("unique_ctr")
-    VALUE_UNIQUE_CTR("unique_ctr"),
-    @SerializedName("unique_link_clicks_ctr")
-    VALUE_UNIQUE_LINK_CLICKS_CTR("unique_link_clicks_ctr"),
-    @SerializedName("date_start")
-    VALUE_DATE_START("date_start"),
-    @SerializedName("date_stop")
-    VALUE_DATE_STOP("date_stop"),
-    @SerializedName("deeplink_clicks")
-    VALUE_DEEPLINK_CLICKS("deeplink_clicks"),
-    @SerializedName("frequency")
-    VALUE_FREQUENCY("frequency"),
-    @SerializedName("impressions")
-    VALUE_IMPRESSIONS("impressions"),
-    @SerializedName("unique_impressions")
-    VALUE_UNIQUE_IMPRESSIONS("unique_impressions"),
-    @SerializedName("inline_link_clicks")
-    VALUE_INLINE_LINK_CLICKS("inline_link_clicks"),
-    @SerializedName("inline_post_engagement")
-    VALUE_INLINE_POST_ENGAGEMENT("inline_post_engagement"),
-    @SerializedName("newsfeed_avg_position")
-    VALUE_NEWSFEED_AVG_POSITION("newsfeed_avg_position"),
-    @SerializedName("newsfeed_clicks")
-    VALUE_NEWSFEED_CLICKS("newsfeed_clicks"),
-    @SerializedName("newsfeed_impressions")
-    VALUE_NEWSFEED_IMPRESSIONS("newsfeed_impressions"),
-    @SerializedName("reach")
-    VALUE_REACH("reach"),
-    @SerializedName("relevance_score")
-    VALUE_RELEVANCE_SCORE("relevance_score"),
-    @SerializedName("social_clicks")
-    VALUE_SOCIAL_CLICKS("social_clicks"),
-    @SerializedName("unique_social_clicks")
-    VALUE_UNIQUE_SOCIAL_CLICKS("unique_social_clicks"),
-    @SerializedName("social_impressions")
-    VALUE_SOCIAL_IMPRESSIONS("social_impressions"),
-    @SerializedName("unique_social_impressions")
-    VALUE_UNIQUE_SOCIAL_IMPRESSIONS("unique_social_impressions"),
-    @SerializedName("social_reach")
-    VALUE_SOCIAL_REACH("social_reach"),
-    @SerializedName("social_spend")
-    VALUE_SOCIAL_SPEND("social_spend"),
-    @SerializedName("spend")
-    VALUE_SPEND("spend"),
-    @SerializedName("total_action_value")
-    VALUE_TOTAL_ACTION_VALUE("total_action_value"),
-    @SerializedName("total_actions")
-    VALUE_TOTAL_ACTIONS("total_actions"),
-    @SerializedName("total_unique_actions")
-    VALUE_TOTAL_UNIQUE_ACTIONS("total_unique_actions"),
-    @SerializedName("video_avg_pct_watched_actions")
-    VALUE_VIDEO_AVG_PCT_WATCHED_ACTIONS("video_avg_pct_watched_actions"),
-    @SerializedName("video_avg_sec_watched_actions")
-    VALUE_VIDEO_AVG_SEC_WATCHED_ACTIONS("video_avg_sec_watched_actions"),
-    @SerializedName("video_complete_watched_actions")
-    VALUE_VIDEO_COMPLETE_WATCHED_ACTIONS("video_complete_watched_actions"),
-    @SerializedName("video_p25_watched_actions")
-    VALUE_VIDEO_P25_WATCHED_ACTIONS("video_p25_watched_actions"),
-    @SerializedName("video_p50_watched_actions")
-    VALUE_VIDEO_P50_WATCHED_ACTIONS("video_p50_watched_actions"),
-    @SerializedName("video_p75_watched_actions")
-    VALUE_VIDEO_P75_WATCHED_ACTIONS("video_p75_watched_actions"),
-    @SerializedName("video_p95_watched_actions")
-    VALUE_VIDEO_P95_WATCHED_ACTIONS("video_p95_watched_actions"),
-    @SerializedName("video_p100_watched_actions")
-    VALUE_VIDEO_P100_WATCHED_ACTIONS("video_p100_watched_actions"),
-    @SerializedName("video_10_sec_watched_actions")
-    VALUE_VIDEO_10_SEC_WATCHED_ACTIONS("video_10_sec_watched_actions"),
-    @SerializedName("video_15_sec_watched_actions")
-    VALUE_VIDEO_15_SEC_WATCHED_ACTIONS("video_15_sec_watched_actions"),
-    @SerializedName("video_30_sec_watched_actions")
-    VALUE_VIDEO_30_SEC_WATCHED_ACTIONS("video_30_sec_watched_actions"),
-    @SerializedName("website_clicks")
-    VALUE_WEBSITE_CLICKS("website_clicks"),
-    @SerializedName("website_ctr")
-    VALUE_WEBSITE_CTR("website_ctr"),
-    @SerializedName("ad_id")
-    VALUE_AD_ID("ad_id"),
-    @SerializedName("ad_name")
-    VALUE_AD_NAME("ad_name"),
-    @SerializedName("adset_id")
-    VALUE_ADSET_ID("adset_id"),
-    @SerializedName("adset_name")
-    VALUE_ADSET_NAME("adset_name"),
-    @SerializedName("campaign_id")
-    VALUE_CAMPAIGN_ID("campaign_id"),
-    @SerializedName("campaign_name")
-    VALUE_CAMPAIGN_NAME("campaign_name"),
-    NULL(null);
-
-    private String value;
-
-    private EnumFields(String value) {
-      this.value = value;
-    }
-
-    @Override
-    public String toString() {
-      return value;
-    }
-  }
-  public static enum EnumActionAttributionWindows {
-    @SerializedName("1d_view")
-    VALUE_1D_VIEW("1d_view"),
-    @SerializedName("7d_view")
-    VALUE_7D_VIEW("7d_view"),
-    @SerializedName("28d_view")
-    VALUE_28D_VIEW("28d_view"),
-    @SerializedName("1d_click")
-    VALUE_1D_CLICK("1d_click"),
-    @SerializedName("7d_click")
-    VALUE_7D_CLICK("7d_click"),
-    @SerializedName("28d_click")
-    VALUE_28D_CLICK("28d_click"),
-    @SerializedName("default")
-    VALUE_DEFAULT("default"),
-    NULL(null);
-
-    private String value;
-
-    private EnumActionAttributionWindows(String value) {
-      this.value = value;
-    }
-
-    @Override
-    public String toString() {
-      return value;
-    }
-  }
-  public static enum EnumActionBreakdowns {
-    @SerializedName("action_carousel_card_id")
-    VALUE_ACTION_CAROUSEL_CARD_ID("action_carousel_card_id"),
-    @SerializedName("action_carousel_card_name")
-    VALUE_ACTION_CAROUSEL_CARD_NAME("action_carousel_card_name"),
-    @SerializedName("action_destination")
-    VALUE_ACTION_DESTINATION("action_destination"),
-    @SerializedName("action_device")
-    VALUE_ACTION_DEVICE("action_device"),
-    @SerializedName("action_target_id")
-    VALUE_ACTION_TARGET_ID("action_target_id"),
-    @SerializedName("action_type")
-    VALUE_ACTION_TYPE("action_type"),
-    @SerializedName("action_video_type")
-    VALUE_ACTION_VIDEO_TYPE("action_video_type"),
-    NULL(null);
-
-    private String value;
-
-    private EnumActionBreakdowns(String value) {
-      this.value = value;
-    }
-
-    @Override
-    public String toString() {
-      return value;
-    }
-  }
-  public static enum EnumActionReportTime {
-    @SerializedName("impression")
-    VALUE_IMPRESSION("impression"),
-    @SerializedName("conversion")
-    VALUE_CONVERSION("conversion"),
-    NULL(null);
-
-    private String value;
-
-    private EnumActionReportTime(String value) {
-      this.value = value;
-    }
-
-    @Override
-    public String toString() {
-      return value;
-    }
-  }
-  public static enum EnumBreakdowns {
-    @SerializedName("age")
-    VALUE_AGE("age"),
-    @SerializedName("country")
-    VALUE_COUNTRY("country"),
-    @SerializedName("gender")
-    VALUE_GENDER("gender"),
-    @SerializedName("frequency_value")
-    VALUE_FREQUENCY_VALUE("frequency_value"),
-    @SerializedName("hourly_stats_aggregated_by_advertiser_time_zone")
-    VALUE_HOURLY_STATS_AGGREGATED_BY_ADVERTISER_TIME_ZONE("hourly_stats_aggregated_by_advertiser_time_zone"),
-    @SerializedName("hourly_stats_aggregated_by_audience_time_zone")
-    VALUE_HOURLY_STATS_AGGREGATED_BY_AUDIENCE_TIME_ZONE("hourly_stats_aggregated_by_audience_time_zone"),
-    @SerializedName("impression_device")
-    VALUE_IMPRESSION_DEVICE("impression_device"),
-    @SerializedName("place_page_id")
-    VALUE_PLACE_PAGE_ID("place_page_id"),
-    @SerializedName("placement")
-    VALUE_PLACEMENT("placement"),
-    @SerializedName("placement_merge_rhc")
-    VALUE_PLACEMENT_MERGE_RHC("placement_merge_rhc"),
-    @SerializedName("product_id")
-    VALUE_PRODUCT_ID("product_id"),
-    @SerializedName("region")
-    VALUE_REGION("region"),
-    NULL(null);
+  public static enum EnumBidType {
+      @SerializedName("CPC")
+      VALUE_CPC("CPC"),
+      @SerializedName("CPM")
+      VALUE_CPM("CPM"),
+      @SerializedName("MULTI_PREMIUM")
+      VALUE_MULTI_PREMIUM("MULTI_PREMIUM"),
+      @SerializedName("ABSOLUTE_OCPM")
+      VALUE_ABSOLUTE_OCPM("ABSOLUTE_OCPM"),
+      @SerializedName("CPA")
+      VALUE_CPA("CPA"),
+      NULL(null);
+
+      private String value;
+
+      private EnumBidType(String value) {
+        this.value = value;
+      }
 
-    private String value;
+      @Override
+      public String toString() {
+        return value;
+      }
+  }
 
-    private EnumBreakdowns(String value) {
-      this.value = value;
-    }
+  public static enum EnumConfiguredStatus {
+      @SerializedName("ACTIVE")
+      VALUE_ACTIVE("ACTIVE"),
+      @SerializedName("PAUSED")
+      VALUE_PAUSED("PAUSED"),
+      @SerializedName("DELETED")
+      VALUE_DELETED("DELETED"),
+      @SerializedName("ARCHIVED")
+      VALUE_ARCHIVED("ARCHIVED"),
+      NULL(null);
+
+      private String value;
+
+      private EnumConfiguredStatus(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
-  }
-  public static enum EnumDatePreset {
-    @SerializedName("today")
-    VALUE_TODAY("today"),
-    @SerializedName("yesterday")
-    VALUE_YESTERDAY("yesterday"),
-    @SerializedName("last_3_days")
-    VALUE_LAST_3_DAYS("last_3_days"),
-    @SerializedName("this_week")
-    VALUE_THIS_WEEK("this_week"),
-    @SerializedName("last_week")
-    VALUE_LAST_WEEK("last_week"),
-    @SerializedName("last_7_days")
-    VALUE_LAST_7_DAYS("last_7_days"),
-    @SerializedName("last_14_days")
-    VALUE_LAST_14_DAYS("last_14_days"),
-    @SerializedName("last_28_days")
-    VALUE_LAST_28_DAYS("last_28_days"),
-    @SerializedName("last_30_days")
-    VALUE_LAST_30_DAYS("last_30_days"),
-    @SerializedName("last_90_days")
-    VALUE_LAST_90_DAYS("last_90_days"),
-    @SerializedName("this_month")
-    VALUE_THIS_MONTH("this_month"),
-    @SerializedName("last_month")
-    VALUE_LAST_MONTH("last_month"),
-    @SerializedName("this_quarter")
-    VALUE_THIS_QUARTER("this_quarter"),
-    @SerializedName("last_3_months")
-    VALUE_LAST_3_MONTHS("last_3_months"),
-    @SerializedName("lifetime")
-    VALUE_LIFETIME("lifetime"),
-    NULL(null);
-
-    private String value;
-
-    private EnumDatePreset(String value) {
-      this.value = value;
-    }
-
-    @Override
-    public String toString() {
-      return value;
-    }
-  }
-  public static enum EnumLevel {
-    @SerializedName("ad")
-    VALUE_AD("ad"),
-    @SerializedName("adset")
-    VALUE_ADSET("adset"),
-    @SerializedName("campaign")
-    VALUE_CAMPAIGN("campaign"),
-    NULL(null);
-
-    private String value;
-
-    private EnumLevel(String value) {
-      this.value = value;
-    }
-
-    @Override
-    public String toString() {
-      return value;
-    }
-  }
-  public static enum EnumSummaryActionBreakdowns {
-    @SerializedName("action_carousel_card_id")
-    VALUE_ACTION_CAROUSEL_CARD_ID("action_carousel_card_id"),
-    @SerializedName("action_carousel_card_name")
-    VALUE_ACTION_CAROUSEL_CARD_NAME("action_carousel_card_name"),
-    @SerializedName("action_destination")
-    VALUE_ACTION_DESTINATION("action_destination"),
-    @SerializedName("action_device")
-    VALUE_ACTION_DEVICE("action_device"),
-    @SerializedName("action_target_id")
-    VALUE_ACTION_TARGET_ID("action_target_id"),
-    @SerializedName("action_type")
-    VALUE_ACTION_TYPE("action_type"),
-    @SerializedName("action_video_type")
-    VALUE_ACTION_VIDEO_TYPE("action_video_type"),
-    NULL(null);
-
-    private String value;
-
-    private EnumSummaryActionBreakdowns(String value) {
-      this.value = value;
-    }
-
-    @Override
-    public String toString() {
-      return value;
-    }
-  }
-  public static enum EnumAdFormat {
-    @SerializedName("RIGHT_COLUMN_STANDARD")
-    VALUE_RIGHT_COLUMN_STANDARD("RIGHT_COLUMN_STANDARD"),
-    @SerializedName("DESKTOP_FEED_STANDARD")
-    VALUE_DESKTOP_FEED_STANDARD("DESKTOP_FEED_STANDARD"),
-    @SerializedName("MOBILE_FEED_STANDARD")
-    VALUE_MOBILE_FEED_STANDARD("MOBILE_FEED_STANDARD"),
-    @SerializedName("MOBILE_FEED_BASIC")
-    VALUE_MOBILE_FEED_BASIC("MOBILE_FEED_BASIC"),
-    @SerializedName("MOBILE_INTERSTITIAL")
-    VALUE_MOBILE_INTERSTITIAL("MOBILE_INTERSTITIAL"),
-    @SerializedName("MOBILE_BANNER")
-    VALUE_MOBILE_BANNER("MOBILE_BANNER"),
-    @SerializedName("MOBILE_NATIVE")
-    VALUE_MOBILE_NATIVE("MOBILE_NATIVE"),
-    @SerializedName("INSTAGRAM_STANDARD")
-    VALUE_INSTAGRAM_STANDARD("INSTAGRAM_STANDARD"),
-    @SerializedName("LIVERAIL_VIDEO")
-    VALUE_LIVERAIL_VIDEO("LIVERAIL_VIDEO"),
-    NULL(null);
-
-    private String value;
-
-    private EnumAdFormat(String value) {
-      this.value = value;
-    }
-
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
-  public static enum EnumOptimizeFor {
-    @SerializedName("NONE")
-    VALUE_NONE("NONE"),
-    @SerializedName("APP_INSTALLS")
-    VALUE_APP_INSTALLS("APP_INSTALLS"),
-    @SerializedName("BRAND_AWARENESS")
-    VALUE_BRAND_AWARENESS("BRAND_AWARENESS"),
-    @SerializedName("CLICKS")
-    VALUE_CLICKS("CLICKS"),
-    @SerializedName("ENGAGED_USERS")
-    VALUE_ENGAGED_USERS("ENGAGED_USERS"),
-    @SerializedName("EXTERNAL")
-    VALUE_EXTERNAL("EXTERNAL"),
-    @SerializedName("EVENT_RESPONSES")
-    VALUE_EVENT_RESPONSES("EVENT_RESPONSES"),
-    @SerializedName("IMPRESSIONS")
-    VALUE_IMPRESSIONS("IMPRESSIONS"),
-    @SerializedName("LEAD_GENERATION")
-    VALUE_LEAD_GENERATION("LEAD_GENERATION"),
-    @SerializedName("LINK_CLICKS")
-    VALUE_LINK_CLICKS("LINK_CLICKS"),
-    @SerializedName("OFFER_CLAIMS")
-    VALUE_OFFER_CLAIMS("OFFER_CLAIMS"),
-    @SerializedName("OFFSITE_CONVERSIONS")
-    VALUE_OFFSITE_CONVERSIONS("OFFSITE_CONVERSIONS"),
-    @SerializedName("PAGE_ENGAGEMENT")
-    VALUE_PAGE_ENGAGEMENT("PAGE_ENGAGEMENT"),
-    @SerializedName("PAGE_LIKES")
-    VALUE_PAGE_LIKES("PAGE_LIKES"),
-    @SerializedName("POST_ENGAGEMENT")
-    VALUE_POST_ENGAGEMENT("POST_ENGAGEMENT"),
-    @SerializedName("REACH")
-    VALUE_REACH("REACH"),
-    @SerializedName("SOCIAL_IMPRESSIONS")
-    VALUE_SOCIAL_IMPRESSIONS("SOCIAL_IMPRESSIONS"),
-    @SerializedName("VIDEO_VIEWS")
-    VALUE_VIDEO_VIEWS("VIDEO_VIEWS"),
-    NULL(null);
-
-    private String value;
-
-    private EnumOptimizeFor(String value) {
-      this.value = value;
-    }
-
-    @Override
-    public String toString() {
-      return value;
-    }
-  }
-  public static enum EnumDeleteAdLabelsExecutionOptions {
-    @SerializedName("VALIDATE_ONLY")
-    VALUE_VALIDATE_ONLY("VALIDATE_ONLY"),
-    @SerializedName("INCLUDE_WARNINGS")
-    VALUE_INCLUDE_WARNINGS("INCLUDE_WARNINGS"),
-    NULL(null);
 
-    private String value;
+  public static enum EnumEffectiveStatus {
+      @SerializedName("ACTIVE")
+      VALUE_ACTIVE("ACTIVE"),
+      @SerializedName("PAUSED")
+      VALUE_PAUSED("PAUSED"),
+      @SerializedName("DELETED")
+      VALUE_DELETED("DELETED"),
+      @SerializedName("PENDING_REVIEW")
+      VALUE_PENDING_REVIEW("PENDING_REVIEW"),
+      @SerializedName("DISAPPROVED")
+      VALUE_DISAPPROVED("DISAPPROVED"),
+      @SerializedName("PREAPPROVED")
+      VALUE_PREAPPROVED("PREAPPROVED"),
+      @SerializedName("PENDING_BILLING_INFO")
+      VALUE_PENDING_BILLING_INFO("PENDING_BILLING_INFO"),
+      @SerializedName("CAMPAIGN_PAUSED")
+      VALUE_CAMPAIGN_PAUSED("CAMPAIGN_PAUSED"),
+      @SerializedName("ARCHIVED")
+      VALUE_ARCHIVED("ARCHIVED"),
+      @SerializedName("ADSET_PAUSED")
+      VALUE_ADSET_PAUSED("ADSET_PAUSED"),
+      NULL(null);
+
+      private String value;
+
+      private EnumEffectiveStatus(String value) {
+        this.value = value;
+      }
 
-    private EnumDeleteAdLabelsExecutionOptions(String value) {
-      this.value = value;
-    }
-
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
-  public static enum EnumBidType {
-    @SerializedName("CPC")
-    VALUE_CPC("CPC"),
-    @SerializedName("CPM")
-    VALUE_CPM("CPM"),
-    @SerializedName("MULTI_PREMIUM")
-    VALUE_MULTI_PREMIUM("MULTI_PREMIUM"),
-    @SerializedName("ABSOLUTE_OCPM")
-    VALUE_ABSOLUTE_OCPM("ABSOLUTE_OCPM"),
-    @SerializedName("CPA")
-    VALUE_CPA("CPA"),
-    NULL(null);
 
-    private String value;
+  public static enum EnumStatus {
+      @SerializedName("ACTIVE")
+      VALUE_ACTIVE("ACTIVE"),
+      @SerializedName("PAUSED")
+      VALUE_PAUSED("PAUSED"),
+      @SerializedName("DELETED")
+      VALUE_DELETED("DELETED"),
+      @SerializedName("ARCHIVED")
+      VALUE_ARCHIVED("ARCHIVED"),
+      NULL(null);
+
+      private String value;
+
+      private EnumStatus(String value) {
+        this.value = value;
+      }
 
-    private EnumBidType(String value) {
-      this.value = value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
+  }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+  public static enum EnumDatePreset {
+      @SerializedName("today")
+      VALUE_TODAY("today"),
+      @SerializedName("yesterday")
+      VALUE_YESTERDAY("yesterday"),
+      @SerializedName("last_3_days")
+      VALUE_LAST_3_DAYS("last_3_days"),
+      @SerializedName("this_week")
+      VALUE_THIS_WEEK("this_week"),
+      @SerializedName("last_week")
+      VALUE_LAST_WEEK("last_week"),
+      @SerializedName("last_7_days")
+      VALUE_LAST_7_DAYS("last_7_days"),
+      @SerializedName("last_14_days")
+      VALUE_LAST_14_DAYS("last_14_days"),
+      @SerializedName("last_28_days")
+      VALUE_LAST_28_DAYS("last_28_days"),
+      @SerializedName("last_30_days")
+      VALUE_LAST_30_DAYS("last_30_days"),
+      @SerializedName("last_90_days")
+      VALUE_LAST_90_DAYS("last_90_days"),
+      @SerializedName("this_month")
+      VALUE_THIS_MONTH("this_month"),
+      @SerializedName("last_month")
+      VALUE_LAST_MONTH("last_month"),
+      @SerializedName("this_quarter")
+      VALUE_THIS_QUARTER("this_quarter"),
+      @SerializedName("last_3_months")
+      VALUE_LAST_3_MONTHS("last_3_months"),
+      @SerializedName("lifetime")
+      VALUE_LIFETIME("lifetime"),
+      NULL(null);
+
+      private String value;
+
+      private EnumDatePreset(String value) {
+        this.value = value;
+      }
+
+      @Override
+      public String toString() {
+        return value;
+      }
   }
-  public static enum EnumConfiguredStatus {
-    @SerializedName("ACTIVE")
-    VALUE_ACTIVE("ACTIVE"),
-    @SerializedName("PAUSED")
-    VALUE_PAUSED("PAUSED"),
-    @SerializedName("DELETED")
-    VALUE_DELETED("DELETED"),
-    @SerializedName("ARCHIVED")
-    VALUE_ARCHIVED("ARCHIVED"),
-    NULL(null);
 
-    private String value;
+  public static enum EnumExecutionOptions {
+      @SerializedName("VALIDATE_ONLY")
+      VALUE_VALIDATE_ONLY("VALIDATE_ONLY"),
+      @SerializedName("SYNCHRONOUS_AD_REVIEW")
+      VALUE_SYNCHRONOUS_AD_REVIEW("SYNCHRONOUS_AD_REVIEW"),
+      @SerializedName("INCLUDE_RECOMMENDATIONS")
+      VALUE_INCLUDE_RECOMMENDATIONS("INCLUDE_RECOMMENDATIONS"),
+      NULL(null);
 
-    private EnumConfiguredStatus(String value) {
-      this.value = value;
-    }
+      private String value;
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      private EnumExecutionOptions(String value) {
+        this.value = value;
+      }
+
+      @Override
+      public String toString() {
+        return value;
+      }
   }
-  public static enum EnumEffectiveStatus {
-    @SerializedName("ACTIVE")
-    VALUE_ACTIVE("ACTIVE"),
-    @SerializedName("PAUSED")
-    VALUE_PAUSED("PAUSED"),
-    @SerializedName("DELETED")
-    VALUE_DELETED("DELETED"),
-    @SerializedName("PENDING_REVIEW")
-    VALUE_PENDING_REVIEW("PENDING_REVIEW"),
-    @SerializedName("DISAPPROVED")
-    VALUE_DISAPPROVED("DISAPPROVED"),
-    @SerializedName("PREAPPROVED")
-    VALUE_PREAPPROVED("PREAPPROVED"),
-    @SerializedName("PENDING_BILLING_INFO")
-    VALUE_PENDING_BILLING_INFO("PENDING_BILLING_INFO"),
-    @SerializedName("CAMPAIGN_PAUSED")
-    VALUE_CAMPAIGN_PAUSED("CAMPAIGN_PAUSED"),
-    @SerializedName("ARCHIVED")
-    VALUE_ARCHIVED("ARCHIVED"),
-    @SerializedName("ADSET_PAUSED")
-    VALUE_ADSET_PAUSED("ADSET_PAUSED"),
-    NULL(null);
 
-    private String value;
+  public static enum EnumOperator {
+      @SerializedName("ALL")
+      VALUE_ALL("ALL"),
+      @SerializedName("ANY")
+      VALUE_ANY("ANY"),
+      NULL(null);
 
-    private EnumEffectiveStatus(String value) {
-      this.value = value;
-    }
+      private String value;
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      private EnumOperator(String value) {
+        this.value = value;
+      }
+
+      @Override
+      public String toString() {
+        return value;
+      }
   }
 
+
   synchronized /*package*/ static Gson getGson() {
     if (gson != null) {
       return gson;
@@ -2896,34 +3011,36 @@ public class Ad extends APINode {
   }
 
   public Ad copyFrom(Ad instance) {
-    this.mId = instance.mId;
     this.mAccountId = instance.mAccountId;
-    this.mAdset = instance.mAdset;
-    this.mCampaign = instance.mCampaign;
+    this.mAdReviewFeedback = instance.mAdReviewFeedback;
     this.mAdlabels = instance.mAdlabels;
+    this.mAdset = instance.mAdset;
     this.mAdsetId = instance.mAdsetId;
     this.mBidAmount = instance.mBidAmount;
     this.mBidInfo = instance.mBidInfo;
     this.mBidType = instance.mBidType;
+    this.mCampaign = instance.mCampaign;
+    this.mCampaignId = instance.mCampaignId;
     this.mConfiguredStatus = instance.mConfiguredStatus;
     this.mConversionSpecs = instance.mConversionSpecs;
     this.mCreatedTime = instance.mCreatedTime;
     this.mCreative = instance.mCreative;
     this.mEffectiveStatus = instance.mEffectiveStatus;
+    this.mId = instance.mId;
     this.mLastUpdatedByAppId = instance.mLastUpdatedByAppId;
     this.mName = instance.mName;
+    this.mRecommendations = instance.mRecommendations;
+    this.mStatus = instance.mStatus;
     this.mTrackingSpecs = instance.mTrackingSpecs;
     this.mUpdatedTime = instance.mUpdatedTime;
-    this.mCampaignId = instance.mCampaignId;
-    this.mAdReviewFeedback = instance.mAdReviewFeedback;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<Ad> getParser() {
     return new APIRequest.ResponseParser<Ad>() {
-      public APINodeList<Ad> parseResponse(String response, APIContext context, APIRequest<Ad> request) {
+      public APINodeList<Ad> parseResponse(String response, APIContext context, APIRequest<Ad> request) throws MalformedResponseException {
         return Ad.parseResponse(response, context, request);
       }
     };
