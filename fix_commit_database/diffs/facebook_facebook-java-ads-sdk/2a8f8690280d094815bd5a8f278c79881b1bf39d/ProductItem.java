@@ -24,40 +24,41 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class ProductItem extends APINode {
-  @SerializedName("id")
-  private String mId = null;
   @SerializedName("additional_image_urls")
   private List<String> mAdditionalImageUrls = null;
-  @SerializedName("applinks")
-  private AppLinks mApplinks = null;
   @SerializedName("age_group")
   private EnumAgeGroup mAgeGroup = null;
+  @SerializedName("applinks")
+  private AppLinks mApplinks = null;
   @SerializedName("availability")
-  private EnumProductItemAvailability mAvailability = null;
+  private EnumAvailability mAvailability = null;
   @SerializedName("brand")
   private String mBrand = null;
   @SerializedName("category")
@@ -67,23 +68,35 @@ public class ProductItem extends APINode {
   @SerializedName("commerce_insights")
   private ProductItemCommerceInsights mCommerceInsights = null;
   @SerializedName("condition")
-  private EnumProductItemCondition mCondition = null;
+  private EnumCondition mCondition = null;
   @SerializedName("custom_data")
   private List<Object> mCustomData = null;
+  @SerializedName("custom_label_0")
+  private String mCustomLabel0 = null;
+  @SerializedName("custom_label_1")
+  private String mCustomLabel1 = null;
+  @SerializedName("custom_label_2")
+  private String mCustomLabel2 = null;
+  @SerializedName("custom_label_3")
+  private String mCustomLabel3 = null;
+  @SerializedName("custom_label_4")
+  private String mCustomLabel4 = null;
   @SerializedName("description")
   private String mDescription = null;
   @SerializedName("expiration_date")
   private String mExpirationDate = null;
   @SerializedName("gender")
-  private EnumProductItemGender mGender = null;
+  private EnumGender mGender = null;
   @SerializedName("gtin")
   private String mGtin = null;
+  @SerializedName("id")
+  private String mId = null;
   @SerializedName("image_url")
   private String mImageUrl = null;
-  @SerializedName("material")
-  private String mMaterial = null;
   @SerializedName("manufacturer_part_number")
   private String mManufacturerPartNumber = null;
+  @SerializedName("material")
+  private String mMaterial = null;
   @SerializedName("name")
   private String mName = null;
   @SerializedName("ordering_index")
@@ -92,6 +105,8 @@ public class ProductItem extends APINode {
   private String mPattern = null;
   @SerializedName("price")
   private String mPrice = null;
+  @SerializedName("product_feed")
+  private ProductFeed mProductFeed = null;
   @SerializedName("product_type")
   private String mProductType = null;
   @SerializedName("retailer_id")
@@ -104,14 +119,14 @@ public class ProductItem extends APINode {
   private EnumReviewStatus mReviewStatus = null;
   @SerializedName("sale_price")
   private String mSalePrice = null;
-  @SerializedName("sale_price_start_date")
-  private String mSalePriceStartDate = null;
   @SerializedName("sale_price_end_date")
   private String mSalePriceEndDate = null;
-  @SerializedName("shipping_weight_value")
-  private Double mShippingWeightValue = null;
+  @SerializedName("sale_price_start_date")
+  private String mSalePriceStartDate = null;
   @SerializedName("shipping_weight_unit")
   private EnumShippingWeightUnit mShippingWeightUnit = null;
+  @SerializedName("shipping_weight_value")
+  private Double mShippingWeightValue = null;
   @SerializedName("size")
   private String mSize = null;
   @SerializedName("start_date")
@@ -119,9 +134,7 @@ public class ProductItem extends APINode {
   @SerializedName("url")
   private String mUrl = null;
   @SerializedName("visibility")
-  private EnumProductItemVisibility mVisibility = null;
-  @SerializedName("product_feed")
-  private ProductFeed mProductFeed = null;
+  private EnumVisibility mVisibility = null;
   protected static Gson gson = null;
 
   ProductItem() {
@@ -133,11 +146,11 @@ public class ProductItem extends APINode {
 
   public ProductItem(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public ProductItem fetch() throws APIException{
-    ProductItem newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    ProductItem newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -154,8 +167,17 @@ public class ProductItem extends APINode {
     return productItem;
   }
 
+  public static APINodeList<ProductItem> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<ProductItem>)(
+      new APIRequest<ProductItem>(context, "", "/", "GET", ProductItem.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -170,22 +192,23 @@ public class ProductItem extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    productItem.mContext = context;
+    productItem.context = context;
     productItem.rawValue = json;
     return productItem;
   }
 
-  public static APINodeList<ProductItem> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<ProductItem> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<ProductItem> productItems = new APINodeList<ProductItem>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -198,10 +221,11 @@ public class ProductItem extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            productItems.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            productItems.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -212,7 +236,20 @@ public class ProductItem extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            productItems.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  productItems.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              productItems.add(loadJSON(obj.toString(), context));
+            }
           }
           return productItems;
         } else if (obj.has("images")) {
@@ -223,24 +260,54 @@ public class ProductItem extends APINode {
           }
           return productItems;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              productItems.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return productItems;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          productItems.clear();
           productItems.add(loadJSON(json, context));
           return productItems;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -248,39 +315,39 @@ public class ProductItem extends APINode {
     return getGson().toJson(this);
   }
 
-  public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetProductSets getProductSets() {
+    return new APIRequestGetProductSets(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestUpdate update() {
-    return new APIRequestUpdate(this.getPrefixedId().toString(), mContext);
+  public APIRequestDelete delete() {
+    return new APIRequestDelete(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetProductSets getProductSets() {
-    return new APIRequestGetProductSets(this.getPrefixedId().toString(), mContext);
+  public APIRequestGet get() {
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
   }
 
-
-  public String getFieldId() {
-    return mId;
+  public APIRequestUpdate update() {
+    return new APIRequestUpdate(this.getPrefixedId().toString(), context);
   }
 
+
   public List<String> getFieldAdditionalImageUrls() {
     return mAdditionalImageUrls;
   }
 
+  public EnumAgeGroup getFieldAgeGroup() {
+    return mAgeGroup;
+  }
+
   public AppLinks getFieldApplinks() {
     if (mApplinks != null) {
-      mApplinks.mContext = getContext();
+      mApplinks.context = getContext();
     }
     return mApplinks;
   }
 
-  public EnumAgeGroup getFieldAgeGroup() {
-    return mAgeGroup;
-  }
-
-  public EnumProductItemAvailability getFieldAvailability() {
+  public EnumAvailability getFieldAvailability() {
     return mAvailability;
   }
 
@@ -300,7 +367,7 @@ public class ProductItem extends APINode {
     return mCommerceInsights;
   }
 
-  public EnumProductItemCondition getFieldCondition() {
+  public EnumCondition getFieldCondition() {
     return mCondition;
   }
 
@@ -308,6 +375,26 @@ public class ProductItem extends APINode {
     return mCustomData;
   }
 
+  public String getFieldCustomLabel0() {
+    return mCustomLabel0;
+  }
+
+  public String getFieldCustomLabel1() {
+    return mCustomLabel1;
+  }
+
+  public String getFieldCustomLabel2() {
+    return mCustomLabel2;
+  }
+
+  public String getFieldCustomLabel3() {
+    return mCustomLabel3;
+  }
+
+  public String getFieldCustomLabel4() {
+    return mCustomLabel4;
+  }
+
   public String getFieldDescription() {
     return mDescription;
   }
@@ -316,7 +403,7 @@ public class ProductItem extends APINode {
     return mExpirationDate;
   }
 
-  public EnumProductItemGender getFieldGender() {
+  public EnumGender getFieldGender() {
     return mGender;
   }
 
@@ -324,18 +411,22 @@ public class ProductItem extends APINode {
     return mGtin;
   }
 
-  public String getFieldImageUrl() {
-    return mImageUrl;
+  public String getFieldId() {
+    return mId;
   }
 
-  public String getFieldMaterial() {
-    return mMaterial;
+  public String getFieldImageUrl() {
+    return mImageUrl;
   }
 
   public String getFieldManufacturerPartNumber() {
     return mManufacturerPartNumber;
   }
 
+  public String getFieldMaterial() {
+    return mMaterial;
+  }
+
   public String getFieldName() {
     return mName;
   }
@@ -352,6 +443,13 @@ public class ProductItem extends APINode {
     return mPrice;
   }
 
+  public ProductFeed getFieldProductFeed() {
+    if (mProductFeed != null) {
+      mProductFeed.context = getContext();
+    }
+    return mProductFeed;
+  }
+
   public String getFieldProductType() {
     return mProductType;
   }
@@ -376,22 +474,22 @@ public class ProductItem extends APINode {
     return mSalePrice;
   }
 
-  public String getFieldSalePriceStartDate() {
-    return mSalePriceStartDate;
-  }
-
   public String getFieldSalePriceEndDate() {
     return mSalePriceEndDate;
   }
 
-  public Double getFieldShippingWeightValue() {
-    return mShippingWeightValue;
+  public String getFieldSalePriceStartDate() {
+    return mSalePriceStartDate;
   }
 
   public EnumShippingWeightUnit getFieldShippingWeightUnit() {
     return mShippingWeightUnit;
   }
 
+  public Double getFieldShippingWeightValue() {
+    return mShippingWeightValue;
+  }
+
   public String getFieldSize() {
     return mSize;
   }
@@ -404,186 +502,397 @@ public class ProductItem extends APINode {
     return mUrl;
   }
 
-  public EnumProductItemVisibility getFieldVisibility() {
+  public EnumVisibility getFieldVisibility() {
     return mVisibility;
   }
 
-  public ProductFeed getFieldProductFeed() {
-    if (mProductFeed != null) {
-      mProductFeed.mContext = getContext();
-    }
-    return mProductFeed;
-  }
-
 
 
-  public static class APIRequestGet extends APIRequest<ProductItem> {
+  public static class APIRequestGetProductSets extends APIRequest<ProductSet> {
 
-    ProductItem lastResponse = null;
+    APINodeList<ProductSet> lastResponse = null;
     @Override
-    public ProductItem getLastResponse() {
+    public APINodeList<ProductSet> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
     };
 
     public static final String[] FIELDS = {
+      "filter",
       "id",
-      "additional_image_urls",
-      "applinks",
-      "age_group",
-      "availability",
-      "brand",
-      "category",
-      "color",
-      "commerce_insights",
-      "condition",
-      "custom_data",
-      "description",
-      "expiration_date",
-      "gender",
-      "gtin",
-      "image_url",
-      "material",
-      "manufacturer_part_number",
       "name",
-      "ordering_index",
-      "pattern",
-      "price",
-      "product_type",
-      "retailer_id",
-      "retailer_product_group_id",
-      "review_rejection_reasons",
-      "review_status",
-      "sale_price",
-      "sale_price_start_date",
-      "sale_price_end_date",
-      "shipping_weight_value",
-      "shipping_weight_unit",
-      "size",
-      "start_date",
-      "url",
-      "visibility",
-      "product_feed",
+      "product_catalog",
+      "product_count",
     };
 
     @Override
-    public ProductItem parseResponse(String response) throws APIException {
-      return ProductItem.parseResponse(response, getContext(), this).head();
+    public APINodeList<ProductSet> parseResponse(String response) throws APIException {
+      return ProductSet.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public ProductItem execute() throws APIException {
+    public APINodeList<ProductSet> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public ProductItem execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<ProductSet> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGet(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetProductSets(String nodeId, APIContext context) {
+      super(context, nodeId, "/product_sets", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGet setParam(String param, Object value) {
+    @Override
+    public APIRequestGetProductSets setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGet setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetProductSets setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGet requestAllFields () {
+    public APIRequestGetProductSets requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGet requestAllFields (boolean value) {
+    public APIRequestGetProductSets requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetProductSets requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGet requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetProductSets requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestField (String field) {
+    @Override
+    public APIRequestGetProductSets requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGet requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetProductSets requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGet requestIdField () {
+    public APIRequestGetProductSets requestFilterField () {
+      return this.requestFilterField(true);
+    }
+    public APIRequestGetProductSets requestFilterField (boolean value) {
+      this.requestField("filter", value);
+      return this;
+    }
+    public APIRequestGetProductSets requestIdField () {
       return this.requestIdField(true);
     }
-    public APIRequestGet requestIdField (boolean value) {
+    public APIRequestGetProductSets requestIdField (boolean value) {
       this.requestField("id", value);
       return this;
     }
-    public APIRequestGet requestAdditionalImageUrlsField () {
-      return this.requestAdditionalImageUrlsField(true);
+    public APIRequestGetProductSets requestNameField () {
+      return this.requestNameField(true);
     }
-    public APIRequestGet requestAdditionalImageUrlsField (boolean value) {
-      this.requestField("additional_image_urls", value);
+    public APIRequestGetProductSets requestNameField (boolean value) {
+      this.requestField("name", value);
       return this;
     }
-    public APIRequestGet requestApplinksField () {
-      return this.requestApplinksField(true);
+    public APIRequestGetProductSets requestProductCatalogField () {
+      return this.requestProductCatalogField(true);
     }
-    public APIRequestGet requestApplinksField (boolean value) {
-      this.requestField("applinks", value);
+    public APIRequestGetProductSets requestProductCatalogField (boolean value) {
+      this.requestField("product_catalog", value);
       return this;
     }
-    public APIRequestGet requestAgeGroupField () {
-      return this.requestAgeGroupField(true);
+    public APIRequestGetProductSets requestProductCountField () {
+      return this.requestProductCountField(true);
     }
-    public APIRequestGet requestAgeGroupField (boolean value) {
-      this.requestField("age_group", value);
+    public APIRequestGetProductSets requestProductCountField (boolean value) {
+      this.requestField("product_count", value);
       return this;
     }
-    public APIRequestGet requestAvailabilityField () {
-      return this.requestAvailabilityField(true);
+  }
+
+  public static class APIRequestDelete extends APIRequest<APINode> {
+
+    APINode lastResponse = null;
+    @Override
+    public APINode getLastResponse() {
+      return lastResponse;
     }
-    public APIRequestGet requestAvailabilityField (boolean value) {
-      this.requestField("availability", value);
-      return this;
+    public static final String[] PARAMS = {
+      "id",
+    };
+
+    public static final String[] FIELDS = {
+    };
+
+    @Override
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
     }
-    public APIRequestGet requestBrandField () {
-      return this.requestBrandField(true);
+
+    @Override
+    public APINode execute() throws APIException {
+      return execute(new HashMap<String, Object>());
     }
-    public APIRequestGet requestBrandField (boolean value) {
-      this.requestField("brand", value);
-      return this;
+
+    @Override
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
     }
-    public APIRequestGet requestCategoryField () {
-      return this.requestCategoryField(true);
+
+    public APIRequestDelete(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "DELETE", Arrays.asList(PARAMS));
     }
-    public APIRequestGet requestCategoryField (boolean value) {
-      this.requestField("category", value);
+
+    @Override
+    public APIRequestDelete setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
-    public APIRequestGet requestColorField () {
-      return this.requestColorField(true);
-    }
-    public APIRequestGet requestColorField (boolean value) {
-      this.requestField("color", value);
+
+    @Override
+    public APIRequestDelete setParams(Map<String, Object> params) {
+      setParamsInternal(params);
+      return this;
+    }
+
+
+    public APIRequestDelete setId (String id) {
+      this.setParam("id", id);
+      return this;
+    }
+
+    public APIRequestDelete requestAllFields () {
+      return this.requestAllFields(true);
+    }
+
+    public APIRequestDelete requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestDelete requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
+    }
+
+    @Override
+    public APIRequestDelete requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestDelete requestField (String field) {
+      this.requestField(field, true);
+      return this;
+    }
+
+    @Override
+    public APIRequestDelete requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
+      return this;
+    }
+
+  }
+
+  public static class APIRequestGet extends APIRequest<ProductItem> {
+
+    ProductItem lastResponse = null;
+    @Override
+    public ProductItem getLastResponse() {
+      return lastResponse;
+    }
+    public static final String[] PARAMS = {
+    };
+
+    public static final String[] FIELDS = {
+      "additional_image_urls",
+      "age_group",
+      "applinks",
+      "availability",
+      "brand",
+      "category",
+      "color",
+      "commerce_insights",
+      "condition",
+      "custom_data",
+      "custom_label_0",
+      "custom_label_1",
+      "custom_label_2",
+      "custom_label_3",
+      "custom_label_4",
+      "description",
+      "expiration_date",
+      "gender",
+      "gtin",
+      "id",
+      "image_url",
+      "manufacturer_part_number",
+      "material",
+      "name",
+      "ordering_index",
+      "pattern",
+      "price",
+      "product_feed",
+      "product_type",
+      "retailer_id",
+      "retailer_product_group_id",
+      "review_rejection_reasons",
+      "review_status",
+      "sale_price",
+      "sale_price_end_date",
+      "sale_price_start_date",
+      "shipping_weight_unit",
+      "shipping_weight_value",
+      "size",
+      "start_date",
+      "url",
+      "visibility",
+    };
+
+    @Override
+    public ProductItem parseResponse(String response) throws APIException {
+      return ProductItem.parseResponse(response, getContext(), this).head();
+    }
+
+    @Override
+    public ProductItem execute() throws APIException {
+      return execute(new HashMap<String, Object>());
+    }
+
+    @Override
+    public ProductItem execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
+    }
+
+    public APIRequestGet(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
+    }
+
+    @Override
+    public APIRequestGet setParam(String param, Object value) {
+      setParamInternal(param, value);
+      return this;
+    }
+
+    @Override
+    public APIRequestGet setParams(Map<String, Object> params) {
+      setParamsInternal(params);
+      return this;
+    }
+
+
+    public APIRequestGet requestAllFields () {
+      return this.requestAllFields(true);
+    }
+
+    public APIRequestGet requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestGet requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
+    }
+
+    @Override
+    public APIRequestGet requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestGet requestField (String field) {
+      this.requestField(field, true);
+      return this;
+    }
+
+    @Override
+    public APIRequestGet requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
+      return this;
+    }
+
+    public APIRequestGet requestAdditionalImageUrlsField () {
+      return this.requestAdditionalImageUrlsField(true);
+    }
+    public APIRequestGet requestAdditionalImageUrlsField (boolean value) {
+      this.requestField("additional_image_urls", value);
+      return this;
+    }
+    public APIRequestGet requestAgeGroupField () {
+      return this.requestAgeGroupField(true);
+    }
+    public APIRequestGet requestAgeGroupField (boolean value) {
+      this.requestField("age_group", value);
+      return this;
+    }
+    public APIRequestGet requestApplinksField () {
+      return this.requestApplinksField(true);
+    }
+    public APIRequestGet requestApplinksField (boolean value) {
+      this.requestField("applinks", value);
+      return this;
+    }
+    public APIRequestGet requestAvailabilityField () {
+      return this.requestAvailabilityField(true);
+    }
+    public APIRequestGet requestAvailabilityField (boolean value) {
+      this.requestField("availability", value);
+      return this;
+    }
+    public APIRequestGet requestBrandField () {
+      return this.requestBrandField(true);
+    }
+    public APIRequestGet requestBrandField (boolean value) {
+      this.requestField("brand", value);
+      return this;
+    }
+    public APIRequestGet requestCategoryField () {
+      return this.requestCategoryField(true);
+    }
+    public APIRequestGet requestCategoryField (boolean value) {
+      this.requestField("category", value);
+      return this;
+    }
+    public APIRequestGet requestColorField () {
+      return this.requestColorField(true);
+    }
+    public APIRequestGet requestColorField (boolean value) {
+      this.requestField("color", value);
       return this;
     }
     public APIRequestGet requestCommerceInsightsField () {
@@ -607,6 +916,41 @@ public class ProductItem extends APINode {
       this.requestField("custom_data", value);
       return this;
     }
+    public APIRequestGet requestCustomLabel0Field () {
+      return this.requestCustomLabel0Field(true);
+    }
+    public APIRequestGet requestCustomLabel0Field (boolean value) {
+      this.requestField("custom_label_0", value);
+      return this;
+    }
+    public APIRequestGet requestCustomLabel1Field () {
+      return this.requestCustomLabel1Field(true);
+    }
+    public APIRequestGet requestCustomLabel1Field (boolean value) {
+      this.requestField("custom_label_1", value);
+      return this;
+    }
+    public APIRequestGet requestCustomLabel2Field () {
+      return this.requestCustomLabel2Field(true);
+    }
+    public APIRequestGet requestCustomLabel2Field (boolean value) {
+      this.requestField("custom_label_2", value);
+      return this;
+    }
+    public APIRequestGet requestCustomLabel3Field () {
+      return this.requestCustomLabel3Field(true);
+    }
+    public APIRequestGet requestCustomLabel3Field (boolean value) {
+      this.requestField("custom_label_3", value);
+      return this;
+    }
+    public APIRequestGet requestCustomLabel4Field () {
+      return this.requestCustomLabel4Field(true);
+    }
+    public APIRequestGet requestCustomLabel4Field (boolean value) {
+      this.requestField("custom_label_4", value);
+      return this;
+    }
     public APIRequestGet requestDescriptionField () {
       return this.requestDescriptionField(true);
     }
@@ -635,6 +979,13 @@ public class ProductItem extends APINode {
       this.requestField("gtin", value);
       return this;
     }
+    public APIRequestGet requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGet requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
     public APIRequestGet requestImageUrlField () {
       return this.requestImageUrlField(true);
     }
@@ -642,13 +993,6 @@ public class ProductItem extends APINode {
       this.requestField("image_url", value);
       return this;
     }
-    public APIRequestGet requestMaterialField () {
-      return this.requestMaterialField(true);
-    }
-    public APIRequestGet requestMaterialField (boolean value) {
-      this.requestField("material", value);
-      return this;
-    }
     public APIRequestGet requestManufacturerPartNumberField () {
       return this.requestManufacturerPartNumberField(true);
     }
@@ -656,6 +1000,13 @@ public class ProductItem extends APINode {
       this.requestField("manufacturer_part_number", value);
       return this;
     }
+    public APIRequestGet requestMaterialField () {
+      return this.requestMaterialField(true);
+    }
+    public APIRequestGet requestMaterialField (boolean value) {
+      this.requestField("material", value);
+      return this;
+    }
     public APIRequestGet requestNameField () {
       return this.requestNameField(true);
     }
@@ -684,6 +1035,13 @@ public class ProductItem extends APINode {
       this.requestField("price", value);
       return this;
     }
+    public APIRequestGet requestProductFeedField () {
+      return this.requestProductFeedField(true);
+    }
+    public APIRequestGet requestProductFeedField (boolean value) {
+      this.requestField("product_feed", value);
+      return this;
+    }
     public APIRequestGet requestProductTypeField () {
       return this.requestProductTypeField(true);
     }
@@ -726,13 +1084,6 @@ public class ProductItem extends APINode {
       this.requestField("sale_price", value);
       return this;
     }
-    public APIRequestGet requestSalePriceStartDateField () {
-      return this.requestSalePriceStartDateField(true);
-    }
-    public APIRequestGet requestSalePriceStartDateField (boolean value) {
-      this.requestField("sale_price_start_date", value);
-      return this;
-    }
     public APIRequestGet requestSalePriceEndDateField () {
       return this.requestSalePriceEndDateField(true);
     }
@@ -740,11 +1091,11 @@ public class ProductItem extends APINode {
       this.requestField("sale_price_end_date", value);
       return this;
     }
-    public APIRequestGet requestShippingWeightValueField () {
-      return this.requestShippingWeightValueField(true);
+    public APIRequestGet requestSalePriceStartDateField () {
+      return this.requestSalePriceStartDateField(true);
     }
-    public APIRequestGet requestShippingWeightValueField (boolean value) {
-      this.requestField("shipping_weight_value", value);
+    public APIRequestGet requestSalePriceStartDateField (boolean value) {
+      this.requestField("sale_price_start_date", value);
       return this;
     }
     public APIRequestGet requestShippingWeightUnitField () {
@@ -754,6 +1105,13 @@ public class ProductItem extends APINode {
       this.requestField("shipping_weight_unit", value);
       return this;
     }
+    public APIRequestGet requestShippingWeightValueField () {
+      return this.requestShippingWeightValueField(true);
+    }
+    public APIRequestGet requestShippingWeightValueField (boolean value) {
+      this.requestField("shipping_weight_value", value);
+      return this;
+    }
     public APIRequestGet requestSizeField () {
       return this.requestSizeField(true);
     }
@@ -782,14 +1140,6 @@ public class ProductItem extends APINode {
       this.requestField("visibility", value);
       return this;
     }
-    public APIRequestGet requestProductFeedField () {
-      return this.requestProductFeedField(true);
-    }
-    public APIRequestGet requestProductFeedField (boolean value) {
-      this.requestField("product_feed", value);
-      return this;
-    }
-
   }
 
   public static class APIRequestUpdate extends APIRequest<APINode> {
@@ -800,50 +1150,56 @@ public class ProductItem extends APINode {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "availability",
-      "category",
-      "currency",
-      "condition",
-      "description",
-      "image_url",
-      "name",
-      "price",
-      "product_type",
-      "url",
-      "visibility",
       "additional_image_urls",
+      "android_app_name",
+      "android_class",
+      "android_package",
+      "android_url",
+      "availability",
       "brand",
+      "category",
       "checkout_url",
       "color",
+      "condition",
+      "currency",
       "custom_data",
+      "custom_label_0",
+      "custom_label_1",
+      "custom_label_2",
+      "custom_label_3",
+      "custom_label_4",
+      "description",
       "expiration_date",
       "gender",
       "gtin",
+      "id",
+      "image_url",
+      "inventory",
+      "ios_app_name",
+      "ios_app_store_id",
+      "ios_url",
+      "ipad_app_name",
+      "ipad_app_store_id",
+      "ipad_url",
+      "iphone_app_name",
+      "iphone_app_store_id",
+      "iphone_url",
       "manufacturer_part_number",
+      "name",
       "ordering_index",
       "pattern",
+      "price",
+      "product_type",
       "sale_price",
       "sale_price_end_date",
       "sale_price_start_date",
       "size",
       "start_date",
-      "ios_url",
-      "ios_app_store_id",
-      "ios_app_name",
-      "iphone_url",
-      "iphone_app_store_id",
-      "iphone_app_name",
-      "ipad_url",
-      "ipad_app_store_id",
-      "ipad_app_name",
-      "android_url",
-      "android_package",
-      "android_class",
-      "android_app_name",
-      "windows_phone_url",
+      "url",
+      "visibility",
       "windows_phone_app_id",
       "windows_phone_app_name",
-      "id",
+      "windows_phone_url",
     };
 
     public static final String[] FIELDS = {
@@ -861,7 +1217,7 @@ public class ProductItem extends APINode {
 
     @Override
     public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -869,134 +1225,127 @@ public class ProductItem extends APINode {
       super(context, nodeId, "/", "POST", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestUpdate setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestUpdate setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestUpdate setAvailability (EnumUpdateAvailability availability) {
-      this.setParam("availability", availability);
+    public APIRequestUpdate setAdditionalImageUrls (List<String> additionalImageUrls) {
+      this.setParam("additional_image_urls", additionalImageUrls);
       return this;
     }
-
-    public APIRequestUpdate setAvailability (String availability) {
-      this.setParam("availability", availability);
+    public APIRequestUpdate setAdditionalImageUrls (String additionalImageUrls) {
+      this.setParam("additional_image_urls", additionalImageUrls);
       return this;
     }
 
-    public APIRequestUpdate setCategory (String category) {
-      this.setParam("category", category);
+    public APIRequestUpdate setAndroidAppName (String androidAppName) {
+      this.setParam("android_app_name", androidAppName);
       return this;
     }
 
-
-    public APIRequestUpdate setCurrency (String currency) {
-      this.setParam("currency", currency);
+    public APIRequestUpdate setAndroidClass (String androidClass) {
+      this.setParam("android_class", androidClass);
       return this;
     }
 
-
-    public APIRequestUpdate setCondition (EnumUpdateCondition condition) {
-      this.setParam("condition", condition);
+    public APIRequestUpdate setAndroidPackage (String androidPackage) {
+      this.setParam("android_package", androidPackage);
       return this;
     }
 
-    public APIRequestUpdate setCondition (String condition) {
-      this.setParam("condition", condition);
+    public APIRequestUpdate setAndroidUrl (String androidUrl) {
+      this.setParam("android_url", androidUrl);
       return this;
     }
 
-    public APIRequestUpdate setDescription (String description) {
-      this.setParam("description", description);
+    public APIRequestUpdate setAvailability (ProductItem.EnumAvailability availability) {
+      this.setParam("availability", availability);
       return this;
     }
-
-
-    public APIRequestUpdate setImageUrl (String imageUrl) {
-      this.setParam("image_url", imageUrl);
+    public APIRequestUpdate setAvailability (String availability) {
+      this.setParam("availability", availability);
       return this;
     }
 
-
-    public APIRequestUpdate setName (String name) {
-      this.setParam("name", name);
+    public APIRequestUpdate setBrand (String brand) {
+      this.setParam("brand", brand);
       return this;
     }
 
-
-    public APIRequestUpdate setPrice (Long price) {
-      this.setParam("price", price);
+    public APIRequestUpdate setCategory (String category) {
+      this.setParam("category", category);
       return this;
     }
 
-    public APIRequestUpdate setPrice (String price) {
-      this.setParam("price", price);
+    public APIRequestUpdate setCheckoutUrl (String checkoutUrl) {
+      this.setParam("checkout_url", checkoutUrl);
       return this;
     }
 
-    public APIRequestUpdate setProductType (String productType) {
-      this.setParam("product_type", productType);
+    public APIRequestUpdate setColor (String color) {
+      this.setParam("color", color);
       return this;
     }
 
-
-    public APIRequestUpdate setUrl (String url) {
-      this.setParam("url", url);
+    public APIRequestUpdate setCondition (ProductItem.EnumCondition condition) {
+      this.setParam("condition", condition);
       return this;
     }
-
-
-    public APIRequestUpdate setVisibility (EnumUpdateVisibility visibility) {
-      this.setParam("visibility", visibility);
+    public APIRequestUpdate setCondition (String condition) {
+      this.setParam("condition", condition);
       return this;
     }
 
-    public APIRequestUpdate setVisibility (String visibility) {
-      this.setParam("visibility", visibility);
+    public APIRequestUpdate setCurrency (String currency) {
+      this.setParam("currency", currency);
       return this;
     }
 
-    public APIRequestUpdate setAdditionalImageUrls (List<String> additionalImageUrls) {
-      this.setParam("additional_image_urls", additionalImageUrls);
+    public APIRequestUpdate setCustomData (Map<String, String> customData) {
+      this.setParam("custom_data", customData);
       return this;
     }
-
-    public APIRequestUpdate setAdditionalImageUrls (String additionalImageUrls) {
-      this.setParam("additional_image_urls", additionalImageUrls);
+    public APIRequestUpdate setCustomData (String customData) {
+      this.setParam("custom_data", customData);
       return this;
     }
 
-    public APIRequestUpdate setBrand (String brand) {
-      this.setParam("brand", brand);
+    public APIRequestUpdate setCustomLabel0 (String customLabel0) {
+      this.setParam("custom_label_0", customLabel0);
       return this;
     }
 
-
-    public APIRequestUpdate setCheckoutUrl (String checkoutUrl) {
-      this.setParam("checkout_url", checkoutUrl);
+    public APIRequestUpdate setCustomLabel1 (String customLabel1) {
+      this.setParam("custom_label_1", customLabel1);
       return this;
     }
 
-
-    public APIRequestUpdate setColor (String color) {
-      this.setParam("color", color);
+    public APIRequestUpdate setCustomLabel2 (String customLabel2) {
+      this.setParam("custom_label_2", customLabel2);
       return this;
     }
 
+    public APIRequestUpdate setCustomLabel3 (String customLabel3) {
+      this.setParam("custom_label_3", customLabel3);
+      return this;
+    }
 
-    public APIRequestUpdate setCustomData (Map<String, String> customData) {
-      this.setParam("custom_data", customData);
+    public APIRequestUpdate setCustomLabel4 (String customLabel4) {
+      this.setParam("custom_label_4", customLabel4);
       return this;
     }
 
-    public APIRequestUpdate setCustomData (String customData) {
-      this.setParam("custom_data", customData);
+    public APIRequestUpdate setDescription (String description) {
+      this.setParam("description", description);
       return this;
     }
 
@@ -1005,12 +1354,10 @@ public class ProductItem extends APINode {
       return this;
     }
 
-
-    public APIRequestUpdate setGender (EnumUpdateGender gender) {
+    public APIRequestUpdate setGender (ProductItem.EnumGender gender) {
       this.setParam("gender", gender);
       return this;
     }
-
     public APIRequestUpdate setGender (String gender) {
       this.setParam("gender", gender);
       return this;
@@ -1021,164 +1368,167 @@ public class ProductItem extends APINode {
       return this;
     }
 
-
-    public APIRequestUpdate setManufacturerPartNumber (String manufacturerPartNumber) {
-      this.setParam("manufacturer_part_number", manufacturerPartNumber);
+    public APIRequestUpdate setId (String id) {
+      this.setParam("id", id);
       return this;
     }
 
-
-    public APIRequestUpdate setOrderingIndex (Long orderingIndex) {
-      this.setParam("ordering_index", orderingIndex);
+    public APIRequestUpdate setImageUrl (String imageUrl) {
+      this.setParam("image_url", imageUrl);
       return this;
     }
 
-    public APIRequestUpdate setOrderingIndex (String orderingIndex) {
-      this.setParam("ordering_index", orderingIndex);
+    public APIRequestUpdate setInventory (Long inventory) {
+      this.setParam("inventory", inventory);
       return this;
     }
-
-    public APIRequestUpdate setPattern (String pattern) {
-      this.setParam("pattern", pattern);
+    public APIRequestUpdate setInventory (String inventory) {
+      this.setParam("inventory", inventory);
       return this;
     }
 
-
-    public APIRequestUpdate setSalePrice (Long salePrice) {
-      this.setParam("sale_price", salePrice);
+    public APIRequestUpdate setIosAppName (String iosAppName) {
+      this.setParam("ios_app_name", iosAppName);
       return this;
     }
 
-    public APIRequestUpdate setSalePrice (String salePrice) {
-      this.setParam("sale_price", salePrice);
+    public APIRequestUpdate setIosAppStoreId (Long iosAppStoreId) {
+      this.setParam("ios_app_store_id", iosAppStoreId);
       return this;
     }
-
-    public APIRequestUpdate setSalePriceEndDate (String salePriceEndDate) {
-      this.setParam("sale_price_end_date", salePriceEndDate);
+    public APIRequestUpdate setIosAppStoreId (String iosAppStoreId) {
+      this.setParam("ios_app_store_id", iosAppStoreId);
       return this;
     }
 
-
-    public APIRequestUpdate setSalePriceStartDate (String salePriceStartDate) {
-      this.setParam("sale_price_start_date", salePriceStartDate);
+    public APIRequestUpdate setIosUrl (String iosUrl) {
+      this.setParam("ios_url", iosUrl);
       return this;
     }
 
-
-    public APIRequestUpdate setSize (String size) {
-      this.setParam("size", size);
+    public APIRequestUpdate setIpadAppName (String ipadAppName) {
+      this.setParam("ipad_app_name", ipadAppName);
       return this;
     }
 
-
-    public APIRequestUpdate setStartDate (String startDate) {
-      this.setParam("start_date", startDate);
+    public APIRequestUpdate setIpadAppStoreId (Long ipadAppStoreId) {
+      this.setParam("ipad_app_store_id", ipadAppStoreId);
+      return this;
+    }
+    public APIRequestUpdate setIpadAppStoreId (String ipadAppStoreId) {
+      this.setParam("ipad_app_store_id", ipadAppStoreId);
       return this;
     }
 
-
-    public APIRequestUpdate setIosUrl (String iosUrl) {
-      this.setParam("ios_url", iosUrl);
+    public APIRequestUpdate setIpadUrl (String ipadUrl) {
+      this.setParam("ipad_url", ipadUrl);
       return this;
     }
 
+    public APIRequestUpdate setIphoneAppName (String iphoneAppName) {
+      this.setParam("iphone_app_name", iphoneAppName);
+      return this;
+    }
 
-    public APIRequestUpdate setIosAppStoreId (Long iosAppStoreId) {
-      this.setParam("ios_app_store_id", iosAppStoreId);
+    public APIRequestUpdate setIphoneAppStoreId (Long iphoneAppStoreId) {
+      this.setParam("iphone_app_store_id", iphoneAppStoreId);
+      return this;
+    }
+    public APIRequestUpdate setIphoneAppStoreId (String iphoneAppStoreId) {
+      this.setParam("iphone_app_store_id", iphoneAppStoreId);
       return this;
     }
 
-    public APIRequestUpdate setIosAppStoreId (String iosAppStoreId) {
-      this.setParam("ios_app_store_id", iosAppStoreId);
+    public APIRequestUpdate setIphoneUrl (String iphoneUrl) {
+      this.setParam("iphone_url", iphoneUrl);
       return this;
     }
 
-    public APIRequestUpdate setIosAppName (String iosAppName) {
-      this.setParam("ios_app_name", iosAppName);
+    public APIRequestUpdate setManufacturerPartNumber (String manufacturerPartNumber) {
+      this.setParam("manufacturer_part_number", manufacturerPartNumber);
       return this;
     }
 
+    public APIRequestUpdate setName (String name) {
+      this.setParam("name", name);
+      return this;
+    }
 
-    public APIRequestUpdate setIphoneUrl (String iphoneUrl) {
-      this.setParam("iphone_url", iphoneUrl);
+    public APIRequestUpdate setOrderingIndex (Long orderingIndex) {
+      this.setParam("ordering_index", orderingIndex);
+      return this;
+    }
+    public APIRequestUpdate setOrderingIndex (String orderingIndex) {
+      this.setParam("ordering_index", orderingIndex);
       return this;
     }
 
+    public APIRequestUpdate setPattern (String pattern) {
+      this.setParam("pattern", pattern);
+      return this;
+    }
 
-    public APIRequestUpdate setIphoneAppStoreId (Long iphoneAppStoreId) {
-      this.setParam("iphone_app_store_id", iphoneAppStoreId);
+    public APIRequestUpdate setPrice (Long price) {
+      this.setParam("price", price);
       return this;
     }
-
-    public APIRequestUpdate setIphoneAppStoreId (String iphoneAppStoreId) {
-      this.setParam("iphone_app_store_id", iphoneAppStoreId);
+    public APIRequestUpdate setPrice (String price) {
+      this.setParam("price", price);
       return this;
     }
 
-    public APIRequestUpdate setIphoneAppName (String iphoneAppName) {
-      this.setParam("iphone_app_name", iphoneAppName);
+    public APIRequestUpdate setProductType (String productType) {
+      this.setParam("product_type", productType);
       return this;
     }
 
-
-    public APIRequestUpdate setIpadUrl (String ipadUrl) {
-      this.setParam("ipad_url", ipadUrl);
+    public APIRequestUpdate setSalePrice (Long salePrice) {
+      this.setParam("sale_price", salePrice);
       return this;
     }
-
-
-    public APIRequestUpdate setIpadAppStoreId (Long ipadAppStoreId) {
-      this.setParam("ipad_app_store_id", ipadAppStoreId);
+    public APIRequestUpdate setSalePrice (String salePrice) {
+      this.setParam("sale_price", salePrice);
       return this;
     }
 
-    public APIRequestUpdate setIpadAppStoreId (String ipadAppStoreId) {
-      this.setParam("ipad_app_store_id", ipadAppStoreId);
+    public APIRequestUpdate setSalePriceEndDate (String salePriceEndDate) {
+      this.setParam("sale_price_end_date", salePriceEndDate);
       return this;
     }
 
-    public APIRequestUpdate setIpadAppName (String ipadAppName) {
-      this.setParam("ipad_app_name", ipadAppName);
+    public APIRequestUpdate setSalePriceStartDate (String salePriceStartDate) {
+      this.setParam("sale_price_start_date", salePriceStartDate);
       return this;
     }
 
-
-    public APIRequestUpdate setAndroidUrl (String androidUrl) {
-      this.setParam("android_url", androidUrl);
+    public APIRequestUpdate setSize (String size) {
+      this.setParam("size", size);
       return this;
     }
 
-
-    public APIRequestUpdate setAndroidPackage (String androidPackage) {
-      this.setParam("android_package", androidPackage);
+    public APIRequestUpdate setStartDate (String startDate) {
+      this.setParam("start_date", startDate);
       return this;
     }
 
-
-    public APIRequestUpdate setAndroidClass (String androidClass) {
-      this.setParam("android_class", androidClass);
+    public APIRequestUpdate setUrl (String url) {
+      this.setParam("url", url);
       return this;
     }
 
-
-    public APIRequestUpdate setAndroidAppName (String androidAppName) {
-      this.setParam("android_app_name", androidAppName);
+    public APIRequestUpdate setVisibility (ProductItem.EnumVisibility visibility) {
+      this.setParam("visibility", visibility);
       return this;
     }
-
-
-    public APIRequestUpdate setWindowsPhoneUrl (String windowsPhoneUrl) {
-      this.setParam("windows_phone_url", windowsPhoneUrl);
+    public APIRequestUpdate setVisibility (String visibility) {
+      this.setParam("visibility", visibility);
       return this;
     }
 
-
     public APIRequestUpdate setWindowsPhoneAppId (Long windowsPhoneAppId) {
       this.setParam("windows_phone_app_id", windowsPhoneAppId);
       return this;
     }
-
     public APIRequestUpdate setWindowsPhoneAppId (String windowsPhoneAppId) {
       this.setParam("windows_phone_app_id", windowsPhoneAppId);
       return this;
@@ -1189,13 +1539,11 @@ public class ProductItem extends APINode {
       return this;
     }
 
-
-    public APIRequestUpdate setId (String id) {
-      this.setParam("id", id);
+    public APIRequestUpdate setWindowsPhoneUrl (String windowsPhoneUrl) {
+      this.setParam("windows_phone_url", windowsPhoneUrl);
       return this;
     }
 
-
     public APIRequestUpdate requestAllFields () {
       return this.requestAllFields(true);
     }
@@ -1207,10 +1555,12 @@ public class ProductItem extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestUpdate requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestUpdate requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -1218,556 +1568,476 @@ public class ProductItem extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestUpdate requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestUpdate requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
   }
 
-  public static class APIRequestGetProductSets extends APIRequest<ProductSet> {
-
-    APINodeList<ProductSet> lastResponse = null;
-    @Override
-    public APINodeList<ProductSet> getLastResponse() {
-      return lastResponse;
-    }
-    public static final String[] PARAMS = {
-    };
-
-    public static final String[] FIELDS = {
-      "id",
-      "name",
-      "filter",
-      "product_count",
-    };
-
-    @Override
-    public APINodeList<ProductSet> parseResponse(String response) throws APIException {
-      return ProductSet.parseResponse(response, getContext(), this);
-    }
-
-    @Override
-    public APINodeList<ProductSet> execute() throws APIException {
-      return execute(new HashMap<String, Object>());
-    }
-
-    @Override
-    public APINodeList<ProductSet> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
-    }
-
-    public APIRequestGetProductSets(String nodeId, APIContext context) {
-      super(context, nodeId, "/product_sets", "GET", Arrays.asList(PARAMS));
-    }
-
-    public APIRequestGetProductSets setParam(String param, Object value) {
-      setParamInternal(param, value);
-      return this;
-    }
-
-    public APIRequestGetProductSets setParams(Map<String, Object> params) {
-      setParamsInternal(params);
-      return this;
-    }
-
-
-    public APIRequestGetProductSets requestAllFields () {
-      return this.requestAllFields(true);
-    }
-
-    public APIRequestGetProductSets requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
+  public static enum EnumAgeGroup {
+      @SerializedName("kids")
+      VALUE_KIDS("kids"),
+      @SerializedName("adult")
+      VALUE_ADULT("adult"),
+      @SerializedName("infant")
+      VALUE_INFANT("infant"),
+      @SerializedName("toddler")
+      VALUE_TODDLER("toddler"),
+      @SerializedName("newborn")
+      VALUE_NEWBORN("newborn"),
+      NULL(null);
+
+      private String value;
+
+      private EnumAgeGroup(String value) {
+        this.value = value;
       }
-      return this;
-    }
-
-    public APIRequestGetProductSets requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
-    }
 
-    public APIRequestGetProductSets requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
+      @Override
+      public String toString() {
+        return value;
       }
-      return this;
-    }
-
-    public APIRequestGetProductSets requestField (String field) {
-      this.requestField(field, true);
-      return this;
-    }
-
-    public APIRequestGetProductSets requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
-      return this;
-    }
-
-    public APIRequestGetProductSets requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGetProductSets requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
-    }
-    public APIRequestGetProductSets requestNameField () {
-      return this.requestNameField(true);
-    }
-    public APIRequestGetProductSets requestNameField (boolean value) {
-      this.requestField("name", value);
-      return this;
-    }
-    public APIRequestGetProductSets requestFilterField () {
-      return this.requestFilterField(true);
-    }
-    public APIRequestGetProductSets requestFilterField (boolean value) {
-      this.requestField("filter", value);
-      return this;
-    }
-    public APIRequestGetProductSets requestProductCountField () {
-      return this.requestProductCountField(true);
-    }
-    public APIRequestGetProductSets requestProductCountField (boolean value) {
-      this.requestField("product_count", value);
-      return this;
-    }
-
   }
 
-  public static enum EnumUpdateAvailability {
-    @SerializedName("IN_STOCK")
-    VALUE_IN_STOCK("IN_STOCK"),
-    @SerializedName("OUT_OF_STOCK")
-    VALUE_OUT_OF_STOCK("OUT_OF_STOCK"),
-    @SerializedName("PREORDER")
-    VALUE_PREORDER("PREORDER"),
-    @SerializedName("AVAILABLE_FOR_ORDER")
-    VALUE_AVAILABLE_FOR_ORDER("AVAILABLE_FOR_ORDER"),
-    @SerializedName("DISCONTINUED")
-    VALUE_DISCONTINUED("DISCONTINUED"),
-    NULL(null);
-
-    private String value;
-
-    private EnumUpdateAvailability(String value) {
-      this.value = value;
-    }
-
-    @Override
-    public String toString() {
-      return value;
-    }
-  }
-  public static enum EnumUpdateCondition {
-    @SerializedName("PC_NEW")
-    VALUE_PC_NEW("PC_NEW"),
-    @SerializedName("PC_REFURBISHED")
-    VALUE_PC_REFURBISHED("PC_REFURBISHED"),
-    @SerializedName("PC_USED")
-    VALUE_PC_USED("PC_USED"),
-    NULL(null);
+  public static enum EnumAvailability {
+      @SerializedName("in stock")
+      VALUE_IN_STOCK("in stock"),
+      @SerializedName("out of stock")
+      VALUE_OUT_OF_STOCK("out of stock"),
+      @SerializedName("preorder")
+      VALUE_PREORDER("preorder"),
+      @SerializedName("available for order")
+      VALUE_AVAILABLE_FOR_ORDER("available for order"),
+      @SerializedName("discontinued")
+      VALUE_DISCONTINUED("discontinued"),
+      NULL(null);
 
-    private String value;
+      private String value;
 
-    private EnumUpdateCondition(String value) {
-      this.value = value;
-    }
+      private EnumAvailability(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
-  public static enum EnumUpdateVisibility {
-    @SerializedName("LEGACY_PUBLIC")
-    VALUE_LEGACY_PUBLIC("LEGACY_PUBLIC"),
-    @SerializedName("STAGING")
-    VALUE_STAGING("STAGING"),
-    @SerializedName("PUBLISHED")
-    VALUE_PUBLISHED("PUBLISHED"),
-    NULL(null);
 
-    private String value;
+  public static enum EnumCondition {
+      @SerializedName("new")
+      VALUE_NEW("new"),
+      @SerializedName("refurbished")
+      VALUE_REFURBISHED("refurbished"),
+      @SerializedName("used")
+      VALUE_USED("used"),
+      NULL(null);
 
-    private EnumUpdateVisibility(String value) {
-      this.value = value;
-    }
-
-    @Override
-    public String toString() {
-      return value;
-    }
-  }
-  public static enum EnumUpdateGender {
-    @SerializedName("FEMALE")
-    VALUE_FEMALE("FEMALE"),
-    @SerializedName("MALE")
-    VALUE_MALE("MALE"),
-    @SerializedName("UNISEX")
-    VALUE_UNISEX("UNISEX"),
-    NULL(null);
-
-    private String value;
+      private String value;
 
-    private EnumUpdateGender(String value) {
-      this.value = value;
-    }
+      private EnumCondition(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
-  public static enum EnumAgeGroup {
-    @SerializedName("kids")
-    VALUE_KIDS("kids"),
-    @SerializedName("adult")
-    VALUE_ADULT("adult"),
-    @SerializedName("infant")
-    VALUE_INFANT("infant"),
-    @SerializedName("toddler")
-    VALUE_TODDLER("toddler"),
-    @SerializedName("newborn")
-    VALUE_NEWBORN("newborn"),
-    @SerializedName("")
-    VALUE_(""),
-    NULL(null);
-
-    private String value;
-
-    private EnumAgeGroup(String value) {
-      this.value = value;
-    }
 
-    @Override
-    public String toString() {
-      return value;
-    }
-  }
-  public static enum EnumProductItemAvailability {
-    @SerializedName("in stock")
-    VALUE_IN_STOCK("in stock"),
-    @SerializedName("out of stock")
-    VALUE_OUT_OF_STOCK("out of stock"),
-    @SerializedName("preorder")
-    VALUE_PREORDER("preorder"),
-    @SerializedName("available for order")
-    VALUE_AVAILABLE_FOR_ORDER("available for order"),
-    @SerializedName("discontinued")
-    VALUE_DISCONTINUED("discontinued"),
-    NULL(null);
+  public static enum EnumGender {
+      @SerializedName("female")
+      VALUE_FEMALE("female"),
+      @SerializedName("male")
+      VALUE_MALE("male"),
+      @SerializedName("unisex")
+      VALUE_UNISEX("unisex"),
+      NULL(null);
 
-    private String value;
+      private String value;
 
-    private EnumProductItemAvailability(String value) {
-      this.value = value;
-    }
+      private EnumGender(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
-  public static enum EnumProductItemCondition {
-    @SerializedName("new")
-    VALUE_NEW("new"),
-    @SerializedName("refurbished")
-    VALUE_REFURBISHED("refurbished"),
-    @SerializedName("used")
-    VALUE_USED("used"),
-    @SerializedName("")
-    VALUE_(""),
-    NULL(null);
-
-    private String value;
 
-    private EnumProductItemCondition(String value) {
-      this.value = value;
-    }
+  public static enum EnumReviewStatus {
+      @SerializedName("pending")
+      VALUE_PENDING("pending"),
+      @SerializedName("rejected")
+      VALUE_REJECTED("rejected"),
+      @SerializedName("approved")
+      VALUE_APPROVED("approved"),
+      NULL(null);
+
+      private String value;
+
+      private EnumReviewStatus(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
-  public static enum EnumProductItemGender {
-    @SerializedName("female")
-    VALUE_FEMALE("female"),
-    @SerializedName("male")
-    VALUE_MALE("male"),
-    @SerializedName("unisex")
-    VALUE_UNISEX("unisex"),
-    @SerializedName("")
-    VALUE_(""),
-    NULL(null);
 
-    private String value;
-
-    private EnumProductItemGender(String value) {
-      this.value = value;
-    }
+  public static enum EnumShippingWeightUnit {
+      @SerializedName("lb")
+      VALUE_LB("lb"),
+      @SerializedName("oz")
+      VALUE_OZ("oz"),
+      @SerializedName("g")
+      VALUE_G("g"),
+      @SerializedName("kg")
+      VALUE_KG("kg"),
+      NULL(null);
+
+      private String value;
+
+      private EnumShippingWeightUnit(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
-  public static enum EnumReviewRejectionReasons {
-    @SerializedName("UNKNOWN")
-    VALUE_UNKNOWN("UNKNOWN"),
-    @SerializedName("IRREGULAR_APP_INSTALL")
-    VALUE_IRREGULAR_APP_INSTALL("IRREGULAR_APP_INSTALL"),
-    @SerializedName("TEXT_OVERLAY")
-    VALUE_TEXT_OVERLAY("TEXT_OVERLAY"),
-    @SerializedName("ADULT_CONTENT")
-    VALUE_ADULT_CONTENT("ADULT_CONTENT"),
-    @SerializedName("ADULT_HEALTH")
-    VALUE_ADULT_HEALTH("ADULT_HEALTH"),
-    @SerializedName("ALCOHOL")
-    VALUE_ALCOHOL("ALCOHOL"),
-    @SerializedName("ANIMATED_IMAGE")
-    VALUE_ANIMATED_IMAGE("ANIMATED_IMAGE"),
-    @SerializedName("BEFORE_AND_AFTER")
-    VALUE_BEFORE_AND_AFTER("BEFORE_AND_AFTER"),
-    @SerializedName("CASUAL_DATING")
-    VALUE_CASUAL_DATING("CASUAL_DATING"),
-    @SerializedName("DATING")
-    VALUE_DATING("DATING"),
-    @SerializedName("FACEBOOK_REFERENCE")
-    VALUE_FACEBOOK_REFERENCE("FACEBOOK_REFERENCE"),
-    @SerializedName("FINANCIAL")
-    VALUE_FINANCIAL("FINANCIAL"),
-    @SerializedName("GAMBLING")
-    VALUE_GAMBLING("GAMBLING"),
-    @SerializedName("IDEALIZED_BODY")
-    VALUE_IDEALIZED_BODY("IDEALIZED_BODY"),
-    @SerializedName("LANGUAGE")
-    VALUE_LANGUAGE("LANGUAGE"),
-    @SerializedName("LANDING_PAGE_FAIL")
-    VALUE_LANDING_PAGE_FAIL("LANDING_PAGE_FAIL"),
-    @SerializedName("SEXUAL")
-    VALUE_SEXUAL("SEXUAL"),
-    @SerializedName("TEST")
-    VALUE_TEST("TEST"),
-    @SerializedName("TOBACCO_SALE")
-    VALUE_TOBACCO_SALE("TOBACCO_SALE"),
-    @SerializedName("TRAPPING")
-    VALUE_TRAPPING("TRAPPING"),
-    @SerializedName("UNSUBSTANTIATED_CLAIM")
-    VALUE_UNSUBSTANTIATED_CLAIM("UNSUBSTANTIATED_CLAIM"),
-    @SerializedName("OTHER")
-    VALUE_OTHER("OTHER"),
-    @SerializedName("WEAPON_SALE")
-    VALUE_WEAPON_SALE("WEAPON_SALE"),
-    @SerializedName("WORK_FROM_HOME")
-    VALUE_WORK_FROM_HOME("WORK_FROM_HOME"),
-    @SerializedName("CASH_ADVANCE")
-    VALUE_CASH_ADVANCE("CASH_ADVANCE"),
-    @SerializedName("SHOCK_AND_SCARE")
-    VALUE_SHOCK_AND_SCARE("SHOCK_AND_SCARE"),
-    @SerializedName("SPY_CAMERA")
-    VALUE_SPY_CAMERA("SPY_CAMERA"),
-    @SerializedName("BAD_HEALTH_PRODUCT")
-    VALUE_BAD_HEALTH_PRODUCT("BAD_HEALTH_PRODUCT"),
-    @SerializedName("GRAMMAR")
-    VALUE_GRAMMAR("GRAMMAR"),
-    @SerializedName("ILLEGAL")
-    VALUE_ILLEGAL("ILLEGAL"),
-    @SerializedName("MISUSE_OF_LIKE")
-    VALUE_MISUSE_OF_LIKE("MISUSE_OF_LIKE"),
-    @SerializedName("NON_EXISTENT_FUNCTIONALITY")
-    VALUE_NON_EXISTENT_FUNCTIONALITY("NON_EXISTENT_FUNCTIONALITY"),
-    @SerializedName("ONLINE_PHARMACY")
-    VALUE_ONLINE_PHARMACY("ONLINE_PHARMACY"),
-    @SerializedName("PENNY_AUCTION")
-    VALUE_PENNY_AUCTION("PENNY_AUCTION"),
-    @SerializedName("PORN")
-    VALUE_PORN("PORN"),
-    @SerializedName("COPYRIGHT")
-    VALUE_COPYRIGHT("COPYRIGHT"),
-    @SerializedName("TRADEMARK")
-    VALUE_TRADEMARK("TRADEMARK"),
-    @SerializedName("COUNTERFEIT")
-    VALUE_COUNTERFEIT("COUNTERFEIT"),
-    @SerializedName("SYSTEM_ISSUE")
-    VALUE_SYSTEM_ISSUE("SYSTEM_ISSUE"),
-    @SerializedName("Q_BLURRY_PIXELATED")
-    VALUE_Q_BLURRY_PIXELATED("Q_BLURRY_PIXELATED"),
-    @SerializedName("Q_BORDERLINE_SEXUAL")
-    VALUE_Q_BORDERLINE_SEXUAL("Q_BORDERLINE_SEXUAL"),
-    @SerializedName("Q_BORDER_BACKGROUND")
-    VALUE_Q_BORDER_BACKGROUND("Q_BORDER_BACKGROUND"),
-    @SerializedName("Q_GRAMMAR_CAPITALIZATION")
-    VALUE_Q_GRAMMAR_CAPITALIZATION("Q_GRAMMAR_CAPITALIZATION"),
-    @SerializedName("Q_IRRELEVANT_IMAGE_COPY")
-    VALUE_Q_IRRELEVANT_IMAGE_COPY("Q_IRRELEVANT_IMAGE_COPY"),
-    @SerializedName("Q_MISLEADING")
-    VALUE_Q_MISLEADING("Q_MISLEADING"),
-    @SerializedName("Q_MULTIPLE_IMAGES")
-    VALUE_Q_MULTIPLE_IMAGES("Q_MULTIPLE_IMAGES"),
-    @SerializedName("Q_HOT_BUTTON")
-    VALUE_Q_HOT_BUTTON("Q_HOT_BUTTON"),
-    @SerializedName("Q_ZOOM_IN_BODY_PARTS")
-    VALUE_Q_ZOOM_IN_BODY_PARTS("Q_ZOOM_IN_BODY_PARTS"),
-    @SerializedName("Q_ZOOM_IN_FOOD")
-    VALUE_Q_ZOOM_IN_FOOD("Q_ZOOM_IN_FOOD"),
-    @SerializedName("QUALITY_LOW")
-    VALUE_QUALITY_LOW("QUALITY_LOW"),
-    @SerializedName("LEAD_AD_FROM_AGGREGATOR")
-    VALUE_LEAD_AD_FROM_AGGREGATOR("LEAD_AD_FROM_AGGREGATOR"),
-    @SerializedName("UNSUITABLE_QUESTION")
-    VALUE_UNSUITABLE_QUESTION("UNSUITABLE_QUESTION"),
-    @SerializedName("FACEBOOK_WORD_MANIPULATED")
-    VALUE_FACEBOOK_WORD_MANIPULATED("FACEBOOK_WORD_MANIPULATED"),
-    @SerializedName("FACEBOOK_ICONS")
-    VALUE_FACEBOOK_ICONS("FACEBOOK_ICONS"),
-    @SerializedName("FACEBOOK_PAGE_LOOKALIKE")
-    VALUE_FACEBOOK_PAGE_LOOKALIKE("FACEBOOK_PAGE_LOOKALIKE"),
-    @SerializedName("FACEBOOK_LOGO_FOCUS")
-    VALUE_FACEBOOK_LOGO_FOCUS("FACEBOOK_LOGO_FOCUS"),
-    @SerializedName("FACEBOOK_LOGO_OVERLAP")
-    VALUE_FACEBOOK_LOGO_OVERLAP("FACEBOOK_LOGO_OVERLAP"),
-    @SerializedName("FACEBOOK_LOGO")
-    VALUE_FACEBOOK_LOGO("FACEBOOK_LOGO"),
-    @SerializedName("FACEBOOK_LOGO_THUMBS_UP")
-    VALUE_FACEBOOK_LOGO_THUMBS_UP("FACEBOOK_LOGO_THUMBS_UP"),
-    @SerializedName("FACEBOOK_SCREENSHOT_PROD")
-    VALUE_FACEBOOK_SCREENSHOT_PROD("FACEBOOK_SCREENSHOT_PROD"),
-    @SerializedName("FACEBOOK_WORDMARK")
-    VALUE_FACEBOOK_WORDMARK("FACEBOOK_WORDMARK"),
-    @SerializedName("FACEBOOK_ZUCKPIC")
-    VALUE_FACEBOOK_ZUCKPIC("FACEBOOK_ZUCKPIC"),
-    @SerializedName("HIGHLIGHTED_PAIN_POINTS")
-    VALUE_HIGHLIGHTED_PAIN_POINTS("HIGHLIGHTED_PAIN_POINTS"),
-    @SerializedName("PERFECT_BODY")
-    VALUE_PERFECT_BODY("PERFECT_BODY"),
-    @SerializedName("SCALES")
-    VALUE_SCALES("SCALES"),
-    @SerializedName("TAPE_MEASURE")
-    VALUE_TAPE_MEASURE("TAPE_MEASURE"),
-    @SerializedName("UNDESIRABLE_BODY")
-    VALUE_UNDESIRABLE_BODY("UNDESIRABLE_BODY"),
-    @SerializedName("ZOOM_BODY_PART")
-    VALUE_ZOOM_BODY_PART("ZOOM_BODY_PART"),
-    @SerializedName("HARRASSMENT")
-    VALUE_HARRASSMENT("HARRASSMENT"),
-    @SerializedName("USER_ATTRIBUTES_CALLOUT")
-    VALUE_USER_ATTRIBUTES_CALLOUT("USER_ATTRIBUTES_CALLOUT"),
-    @SerializedName("USER_FINANICAL_CALLOUT")
-    VALUE_USER_FINANICAL_CALLOUT("USER_FINANICAL_CALLOUT"),
-    @SerializedName("USER_HEALTH_ATTRIBUTES")
-    VALUE_USER_HEALTH_ATTRIBUTES("USER_HEALTH_ATTRIBUTES"),
-    @SerializedName("USER_WEIGHT_ATTRIBUTES")
-    VALUE_USER_WEIGHT_ATTRIBUTES("USER_WEIGHT_ATTRIBUTES"),
-    @SerializedName("PROFANITY")
-    VALUE_PROFANITY("PROFANITY"),
-    @SerializedName("FAKE_FORM_ELEMENTS")
-    VALUE_FAKE_FORM_ELEMENTS("FAKE_FORM_ELEMENTS"),
-    @SerializedName("FAKE_NOTIFICATIONS")
-    VALUE_FAKE_NOTIFICATIONS("FAKE_NOTIFICATIONS"),
-    @SerializedName("MOUSE_CURSOR")
-    VALUE_MOUSE_CURSOR("MOUSE_CURSOR"),
-    @SerializedName("PLAY_BUTTON")
-    VALUE_PLAY_BUTTON("PLAY_BUTTON"),
-    @SerializedName("QR_CODES")
-    VALUE_QR_CODES("QR_CODES"),
-    @SerializedName("EXCESSIVE_SKIN")
-    VALUE_EXCESSIVE_SKIN("EXCESSIVE_SKIN"),
-    @SerializedName("INDIRECT_NUDITY")
-    VALUE_INDIRECT_NUDITY("INDIRECT_NUDITY"),
-    @SerializedName("INDIRECT_SEXUAL_ACT")
-    VALUE_INDIRECT_SEXUAL_ACT("INDIRECT_SEXUAL_ACT"),
-    @SerializedName("SEXUAL_OTHER")
-    VALUE_SEXUAL_OTHER("SEXUAL_OTHER"),
-    @SerializedName("ZOOM_SEXUAL_IMAGE")
-    VALUE_ZOOM_SEXUAL_IMAGE("ZOOM_SEXUAL_IMAGE"),
-    NULL(null);
-
-    private String value;
-
-    private EnumReviewRejectionReasons(String value) {
-      this.value = value;
-    }
 
-    @Override
-    public String toString() {
-      return value;
-    }
-  }
-  public static enum EnumReviewStatus {
-    @SerializedName("NO_REVIEW")
-    VALUE_NO_REVIEW("NO_REVIEW"),
-    @SerializedName("PENDING")
-    VALUE_PENDING("PENDING"),
-    @SerializedName("REJECTED")
-    VALUE_REJECTED("REJECTED"),
-    @SerializedName("APPROVED")
-    VALUE_APPROVED("APPROVED"),
-    NULL(null);
+  public static enum EnumVisibility {
+      @SerializedName("staging")
+      VALUE_STAGING("staging"),
+      @SerializedName("published")
+      VALUE_PUBLISHED("published"),
+      NULL(null);
 
-    private String value;
+      private String value;
 
-    private EnumReviewStatus(String value) {
-      this.value = value;
-    }
+      private EnumVisibility(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
-  public static enum EnumShippingWeightUnit {
-    @SerializedName("lb")
-    VALUE_LB("lb"),
-    @SerializedName("oz")
-    VALUE_OZ("oz"),
-    @SerializedName("g")
-    VALUE_G("g"),
-    @SerializedName("kg")
-    VALUE_KG("kg"),
-    @SerializedName("")
-    VALUE_(""),
-    NULL(null);
-
-    private String value;
 
-    private EnumShippingWeightUnit(String value) {
-      this.value = value;
-    }
+  public static enum EnumReviewRejectionReasons {
+      @SerializedName("UNKNOWN")
+      VALUE_UNKNOWN("UNKNOWN"),
+      @SerializedName("IRREGULAR_APP_INSTALL")
+      VALUE_IRREGULAR_APP_INSTALL("IRREGULAR_APP_INSTALL"),
+      @SerializedName("TEXT_OVERLAY")
+      VALUE_TEXT_OVERLAY("TEXT_OVERLAY"),
+      @SerializedName("ADULT_CONTENT")
+      VALUE_ADULT_CONTENT("ADULT_CONTENT"),
+      @SerializedName("ADULT_HEALTH")
+      VALUE_ADULT_HEALTH("ADULT_HEALTH"),
+      @SerializedName("ALCOHOL")
+      VALUE_ALCOHOL("ALCOHOL"),
+      @SerializedName("ANIMATED_IMAGE")
+      VALUE_ANIMATED_IMAGE("ANIMATED_IMAGE"),
+      @SerializedName("BEFORE_AND_AFTER")
+      VALUE_BEFORE_AND_AFTER("BEFORE_AND_AFTER"),
+      @SerializedName("CASUAL_DATING")
+      VALUE_CASUAL_DATING("CASUAL_DATING"),
+      @SerializedName("DATING")
+      VALUE_DATING("DATING"),
+      @SerializedName("FACEBOOK_REFERENCE")
+      VALUE_FACEBOOK_REFERENCE("FACEBOOK_REFERENCE"),
+      @SerializedName("FINANCIAL")
+      VALUE_FINANCIAL("FINANCIAL"),
+      @SerializedName("GAMBLING")
+      VALUE_GAMBLING("GAMBLING"),
+      @SerializedName("IDEALIZED_BODY")
+      VALUE_IDEALIZED_BODY("IDEALIZED_BODY"),
+      @SerializedName("LANGUAGE")
+      VALUE_LANGUAGE("LANGUAGE"),
+      @SerializedName("LANDING_PAGE_FAIL")
+      VALUE_LANDING_PAGE_FAIL("LANDING_PAGE_FAIL"),
+      @SerializedName("SEXUAL")
+      VALUE_SEXUAL("SEXUAL"),
+      @SerializedName("TEST")
+      VALUE_TEST("TEST"),
+      @SerializedName("TOBACCO_SALE")
+      VALUE_TOBACCO_SALE("TOBACCO_SALE"),
+      @SerializedName("TRAPPING")
+      VALUE_TRAPPING("TRAPPING"),
+      @SerializedName("UNSUBSTANTIATED_CLAIM")
+      VALUE_UNSUBSTANTIATED_CLAIM("UNSUBSTANTIATED_CLAIM"),
+      @SerializedName("OTHER")
+      VALUE_OTHER("OTHER"),
+      @SerializedName("WEAPON_SALE")
+      VALUE_WEAPON_SALE("WEAPON_SALE"),
+      @SerializedName("WORK_FROM_HOME")
+      VALUE_WORK_FROM_HOME("WORK_FROM_HOME"),
+      @SerializedName("CASH_ADVANCE")
+      VALUE_CASH_ADVANCE("CASH_ADVANCE"),
+      @SerializedName("SHOCK_AND_SCARE")
+      VALUE_SHOCK_AND_SCARE("SHOCK_AND_SCARE"),
+      @SerializedName("SPY_CAMERA")
+      VALUE_SPY_CAMERA("SPY_CAMERA"),
+      @SerializedName("BAD_HEALTH_PRODUCT")
+      VALUE_BAD_HEALTH_PRODUCT("BAD_HEALTH_PRODUCT"),
+      @SerializedName("GRAMMAR")
+      VALUE_GRAMMAR("GRAMMAR"),
+      @SerializedName("ILLEGAL")
+      VALUE_ILLEGAL("ILLEGAL"),
+      @SerializedName("MISUSE_OF_LIKE")
+      VALUE_MISUSE_OF_LIKE("MISUSE_OF_LIKE"),
+      @SerializedName("NON_EXISTENT_FUNCTIONALITY")
+      VALUE_NON_EXISTENT_FUNCTIONALITY("NON_EXISTENT_FUNCTIONALITY"),
+      @SerializedName("ONLINE_PHARMACY")
+      VALUE_ONLINE_PHARMACY("ONLINE_PHARMACY"),
+      @SerializedName("PENNY_AUCTION")
+      VALUE_PENNY_AUCTION("PENNY_AUCTION"),
+      @SerializedName("PORN")
+      VALUE_PORN("PORN"),
+      @SerializedName("COPYRIGHT")
+      VALUE_COPYRIGHT("COPYRIGHT"),
+      @SerializedName("TRADEMARK")
+      VALUE_TRADEMARK("TRADEMARK"),
+      @SerializedName("COUNTERFEIT")
+      VALUE_COUNTERFEIT("COUNTERFEIT"),
+      @SerializedName("SYSTEM_ISSUE")
+      VALUE_SYSTEM_ISSUE("SYSTEM_ISSUE"),
+      @SerializedName("Q_BLURRY_PIXELATED")
+      VALUE_Q_BLURRY_PIXELATED("Q_BLURRY_PIXELATED"),
+      @SerializedName("Q_BORDERLINE_SEXUAL")
+      VALUE_Q_BORDERLINE_SEXUAL("Q_BORDERLINE_SEXUAL"),
+      @SerializedName("Q_BORDER_BACKGROUND")
+      VALUE_Q_BORDER_BACKGROUND("Q_BORDER_BACKGROUND"),
+      @SerializedName("Q_GRAMMAR_CAPITALIZATION")
+      VALUE_Q_GRAMMAR_CAPITALIZATION("Q_GRAMMAR_CAPITALIZATION"),
+      @SerializedName("Q_IRRELEVANT_IMAGE_COPY")
+      VALUE_Q_IRRELEVANT_IMAGE_COPY("Q_IRRELEVANT_IMAGE_COPY"),
+      @SerializedName("Q_MISLEADING")
+      VALUE_Q_MISLEADING("Q_MISLEADING"),
+      @SerializedName("Q_MULTIPLE_IMAGES")
+      VALUE_Q_MULTIPLE_IMAGES("Q_MULTIPLE_IMAGES"),
+      @SerializedName("Q_HOT_BUTTON")
+      VALUE_Q_HOT_BUTTON("Q_HOT_BUTTON"),
+      @SerializedName("Q_ZOOM_IN_BODY_PARTS")
+      VALUE_Q_ZOOM_IN_BODY_PARTS("Q_ZOOM_IN_BODY_PARTS"),
+      @SerializedName("Q_ZOOM_IN_FOOD")
+      VALUE_Q_ZOOM_IN_FOOD("Q_ZOOM_IN_FOOD"),
+      @SerializedName("QUALITY_LOW")
+      VALUE_QUALITY_LOW("QUALITY_LOW"),
+      @SerializedName("LEAD_AD_FROM_AGGREGATOR")
+      VALUE_LEAD_AD_FROM_AGGREGATOR("LEAD_AD_FROM_AGGREGATOR"),
+      @SerializedName("UNSUITABLE_QUESTION")
+      VALUE_UNSUITABLE_QUESTION("UNSUITABLE_QUESTION"),
+      @SerializedName("FRAUD_ASSOCIATED")
+      VALUE_FRAUD_ASSOCIATED("FRAUD_ASSOCIATED"),
+      @SerializedName("MYSTERY_IMAGE")
+      VALUE_MYSTERY_IMAGE("MYSTERY_IMAGE"),
+      @SerializedName("APP_SCAM")
+      VALUE_APP_SCAM("APP_SCAM"),
+      @SerializedName("TEXT_PENALTY_HIGH")
+      VALUE_TEXT_PENALTY_HIGH("TEXT_PENALTY_HIGH"),
+      @SerializedName("TEXT_PENALTY_MEDIUM")
+      VALUE_TEXT_PENALTY_MEDIUM("TEXT_PENALTY_MEDIUM"),
+      @SerializedName("TEXT_PENALTY_LOW")
+      VALUE_TEXT_PENALTY_LOW("TEXT_PENALTY_LOW"),
+      @SerializedName("BAD_SUBSCRIPTION")
+      VALUE_BAD_SUBSCRIPTION("BAD_SUBSCRIPTION"),
+      @SerializedName("FACEBOOK_WORD_MANIPULATED")
+      VALUE_FACEBOOK_WORD_MANIPULATED("FACEBOOK_WORD_MANIPULATED"),
+      @SerializedName("FACEBOOK_ICONS")
+      VALUE_FACEBOOK_ICONS("FACEBOOK_ICONS"),
+      @SerializedName("FACEBOOK_PAGE_LOOKALIKE")
+      VALUE_FACEBOOK_PAGE_LOOKALIKE("FACEBOOK_PAGE_LOOKALIKE"),
+      @SerializedName("FACEBOOK_LOGO_FOCUS")
+      VALUE_FACEBOOK_LOGO_FOCUS("FACEBOOK_LOGO_FOCUS"),
+      @SerializedName("FACEBOOK_LOGO_OVERLAP")
+      VALUE_FACEBOOK_LOGO_OVERLAP("FACEBOOK_LOGO_OVERLAP"),
+      @SerializedName("FACEBOOK_LOGO")
+      VALUE_FACEBOOK_LOGO("FACEBOOK_LOGO"),
+      @SerializedName("FACEBOOK_LOGO_THUMBS_UP")
+      VALUE_FACEBOOK_LOGO_THUMBS_UP("FACEBOOK_LOGO_THUMBS_UP"),
+      @SerializedName("FACEBOOK_SCREENSHOT_PROD")
+      VALUE_FACEBOOK_SCREENSHOT_PROD("FACEBOOK_SCREENSHOT_PROD"),
+      @SerializedName("FACEBOOK_WORDMARK")
+      VALUE_FACEBOOK_WORDMARK("FACEBOOK_WORDMARK"),
+      @SerializedName("FACEBOOK_ZUCKPIC")
+      VALUE_FACEBOOK_ZUCKPIC("FACEBOOK_ZUCKPIC"),
+      @SerializedName("HIGHLIGHTED_PAIN_POINTS")
+      VALUE_HIGHLIGHTED_PAIN_POINTS("HIGHLIGHTED_PAIN_POINTS"),
+      @SerializedName("PERFECT_BODY")
+      VALUE_PERFECT_BODY("PERFECT_BODY"),
+      @SerializedName("SCALES")
+      VALUE_SCALES("SCALES"),
+      @SerializedName("TAPE_MEASURE")
+      VALUE_TAPE_MEASURE("TAPE_MEASURE"),
+      @SerializedName("UNDESIRABLE_BODY")
+      VALUE_UNDESIRABLE_BODY("UNDESIRABLE_BODY"),
+      @SerializedName("ZOOM_BODY_PART")
+      VALUE_ZOOM_BODY_PART("ZOOM_BODY_PART"),
+      @SerializedName("HARRASSMENT")
+      VALUE_HARRASSMENT("HARRASSMENT"),
+      @SerializedName("USER_ATTRIBUTES_CALLOUT")
+      VALUE_USER_ATTRIBUTES_CALLOUT("USER_ATTRIBUTES_CALLOUT"),
+      @SerializedName("USER_FINANICAL_CALLOUT")
+      VALUE_USER_FINANICAL_CALLOUT("USER_FINANICAL_CALLOUT"),
+      @SerializedName("USER_HEALTH_ATTRIBUTES")
+      VALUE_USER_HEALTH_ATTRIBUTES("USER_HEALTH_ATTRIBUTES"),
+      @SerializedName("USER_WEIGHT_ATTRIBUTES")
+      VALUE_USER_WEIGHT_ATTRIBUTES("USER_WEIGHT_ATTRIBUTES"),
+      @SerializedName("PROFANITY")
+      VALUE_PROFANITY("PROFANITY"),
+      @SerializedName("FAKE_FORM_ELEMENTS")
+      VALUE_FAKE_FORM_ELEMENTS("FAKE_FORM_ELEMENTS"),
+      @SerializedName("FAKE_NOTIFICATIONS")
+      VALUE_FAKE_NOTIFICATIONS("FAKE_NOTIFICATIONS"),
+      @SerializedName("MOUSE_CURSOR")
+      VALUE_MOUSE_CURSOR("MOUSE_CURSOR"),
+      @SerializedName("PLAY_BUTTON")
+      VALUE_PLAY_BUTTON("PLAY_BUTTON"),
+      @SerializedName("QR_CODES")
+      VALUE_QR_CODES("QR_CODES"),
+      @SerializedName("EXCESSIVE_SKIN")
+      VALUE_EXCESSIVE_SKIN("EXCESSIVE_SKIN"),
+      @SerializedName("INDIRECT_NUDITY")
+      VALUE_INDIRECT_NUDITY("INDIRECT_NUDITY"),
+      @SerializedName("INDIRECT_SEXUAL_ACT")
+      VALUE_INDIRECT_SEXUAL_ACT("INDIRECT_SEXUAL_ACT"),
+      @SerializedName("SEXUAL_OTHER")
+      VALUE_SEXUAL_OTHER("SEXUAL_OTHER"),
+      @SerializedName("ZOOM_SEXUAL_IMAGE")
+      VALUE_ZOOM_SEXUAL_IMAGE("ZOOM_SEXUAL_IMAGE"),
+      @SerializedName("BREASTENLARGEMENT")
+      VALUE_BREASTENLARGEMENT("BREASTENLARGEMENT"),
+      @SerializedName("GENITALSURGERY")
+      VALUE_GENITALSURGERY("GENITALSURGERY"),
+      @SerializedName("LIBIDO")
+      VALUE_LIBIDO("LIBIDO"),
+      @SerializedName("NUDITY_NOTPORN")
+      VALUE_NUDITY_NOTPORN("NUDITY_NOTPORN"),
+      @SerializedName("PHEROMONE")
+      VALUE_PHEROMONE("PHEROMONE"),
+      @SerializedName("SCHEME_HOTGIRLPAGE")
+      VALUE_SCHEME_HOTGIRLPAGE("SCHEME_HOTGIRLPAGE"),
+      @SerializedName("SERVICES_SEDUCTION")
+      VALUE_SERVICES_SEDUCTION("SERVICES_SEDUCTION"),
+      @SerializedName("SEXPUBLICATIONS")
+      VALUE_SEXPUBLICATIONS("SEXPUBLICATIONS"),
+      @SerializedName("SEXTOYS")
+      VALUE_SEXTOYS("SEXTOYS"),
+      @SerializedName("SEXUALPLEASURE")
+      VALUE_SEXUALPLEASURE("SEXUALPLEASURE"),
+      @SerializedName("STRIPCLUBS")
+      VALUE_STRIPCLUBS("STRIPCLUBS"),
+      @SerializedName("MENTION_BOTOX")
+      VALUE_MENTION_BOTOX("MENTION_BOTOX"),
+      @SerializedName("MENTION_DIETPRODUCT")
+      VALUE_MENTION_DIETPRODUCT("MENTION_DIETPRODUCT"),
+      @SerializedName("MENTION_LASERS")
+      VALUE_MENTION_LASERS("MENTION_LASERS"),
+      @SerializedName("MENTION_SEXUALHEALTH")
+      VALUE_MENTION_SEXUALHEALTH("MENTION_SEXUALHEALTH"),
+      @SerializedName("MENTION_SUPPLEMENT")
+      VALUE_MENTION_SUPPLEMENT("MENTION_SUPPLEMENT"),
+      @SerializedName("MENTION_SURGERY")
+      VALUE_MENTION_SURGERY("MENTION_SURGERY"),
+      @SerializedName("MENTION_BRAND_ALCOHOL")
+      VALUE_MENTION_BRAND_ALCOHOL("MENTION_BRAND_ALCOHOL"),
+      @SerializedName("MENTION_CONSUMPTION_ALCOHOL")
+      VALUE_MENTION_CONSUMPTION_ALCOHOL("MENTION_CONSUMPTION_ALCOHOL"),
+      @SerializedName("MENTION_SALES_ALCOHOL")
+      VALUE_MENTION_SALES_ALCOHOL("MENTION_SALES_ALCOHOL"),
+      @SerializedName("SPONSORSHIP")
+      VALUE_SPONSORSHIP("SPONSORSHIP"),
+      @SerializedName("MENTION_SALES_BADHEALTHPRODUCT")
+      VALUE_MENTION_SALES_BADHEALTHPRODUCT("MENTION_SALES_BADHEALTHPRODUCT"),
+      @SerializedName("UNCLEAR_CANCELLATION_SUBSCRIPTION")
+      VALUE_UNCLEAR_CANCELLATION_SUBSCRIPTION("UNCLEAR_CANCELLATION_SUBSCRIPTION"),
+      @SerializedName("UNCLEAR_PRICING_SUBSCRIPTION")
+      VALUE_UNCLEAR_PRICING_SUBSCRIPTION("UNCLEAR_PRICING_SUBSCRIPTION"),
+      @SerializedName("BA_HAIRLOSS")
+      VALUE_BA_HAIRLOSS("BA_HAIRLOSS"),
+      @SerializedName("BA_MEDICAL")
+      VALUE_BA_MEDICAL("BA_MEDICAL"),
+      @SerializedName("BA_SKIN")
+      VALUE_BA_SKIN("BA_SKIN"),
+      @SerializedName("BA_TEETH")
+      VALUE_BA_TEETH("BA_TEETH"),
+      @SerializedName("BA_WEIGHTLOSS")
+      VALUE_BA_WEIGHTLOSS("BA_WEIGHTLOSS"),
+      @SerializedName("GIBBERISH")
+      VALUE_GIBBERISH("GIBBERISH"),
+      @SerializedName("RANDOMCHARACTERS")
+      VALUE_RANDOMCHARACTERS("RANDOMCHARACTERS"),
+      @SerializedName("DRUGS_ILLEGAL")
+      VALUE_DRUGS_ILLEGAL("DRUGS_ILLEGAL"),
+      @SerializedName("HUMAN_TRAFFICKING")
+      VALUE_HUMAN_TRAFFICKING("HUMAN_TRAFFICKING"),
+      @SerializedName("SERVICES_ESCORT")
+      VALUE_SERVICES_ESCORT("SERVICES_ESCORT"),
+      @SerializedName("IMAGE_ANIMALCRUELTY")
+      VALUE_IMAGE_ANIMALCRUELTY("IMAGE_ANIMALCRUELTY"),
+      @SerializedName("IMAGE_MEDICAL")
+      VALUE_IMAGE_MEDICAL("IMAGE_MEDICAL"),
+      @SerializedName("IMAGE_OFFENSIVEGESTURE")
+      VALUE_IMAGE_OFFENSIVEGESTURE("IMAGE_OFFENSIVEGESTURE"),
+      @SerializedName("IMAGE_VEHICLECOLLISION")
+      VALUE_IMAGE_VEHICLECOLLISION("IMAGE_VEHICLECOLLISION"),
+      @SerializedName("IMAGE_WEAPONATUSER")
+      VALUE_IMAGE_WEAPONATUSER("IMAGE_WEAPONATUSER"),
+      @SerializedName("MENTION_IMAGE_VIOLENCE_GORE")
+      VALUE_MENTION_IMAGE_VIOLENCE_GORE("MENTION_IMAGE_VIOLENCE_GORE"),
+      @SerializedName("MENTION_ACCESSORY_CONSUMPTION_TOBACCO")
+      VALUE_MENTION_ACCESSORY_CONSUMPTION_TOBACCO("MENTION_ACCESSORY_CONSUMPTION_TOBACCO"),
+      @SerializedName("MENTION_BRAND_TOBACCO")
+      VALUE_MENTION_BRAND_TOBACCO("MENTION_BRAND_TOBACCO"),
+      @SerializedName("FALSENOTIFICATION")
+      VALUE_FALSENOTIFICATION("FALSENOTIFICATION"),
+      @SerializedName("IMPOSSIBLECURES")
+      VALUE_IMPOSSIBLECURES("IMPOSSIBLECURES"),
+      @SerializedName("MENTION_NUMERIC_CLAIM")
+      VALUE_MENTION_NUMERIC_CLAIM("MENTION_NUMERIC_CLAIM"),
+      @SerializedName("MENTION_TRICKSTIPS")
+      VALUE_MENTION_TRICKSTIPS("MENTION_TRICKSTIPS"),
+      @SerializedName("SPECIFICINDIVIDUALCLAIM")
+      VALUE_SPECIFICINDIVIDUALCLAIM("SPECIFICINDIVIDUALCLAIM"),
+      @SerializedName("UNREALISTIC_EBOOKPROMISE")
+      VALUE_UNREALISTIC_EBOOKPROMISE("UNREALISTIC_EBOOKPROMISE"),
+      @SerializedName("VIDEOSEXUAL")
+      VALUE_VIDEOSEXUAL("VIDEOSEXUAL"),
+      @SerializedName("VIDEOSCHOCKANDSCARE")
+      VALUE_VIDEOSCHOCKANDSCARE("VIDEOSCHOCKANDSCARE"),
+      @SerializedName("VIDEOLANGUAGE")
+      VALUE_VIDEOLANGUAGE("VIDEOLANGUAGE"),
+      @SerializedName("NOT_DATING_PARTNER")
+      VALUE_NOT_DATING_PARTNER("NOT_DATING_PARTNER"),
+      NULL(null);
+
+      private String value;
+
+      private EnumReviewRejectionReasons(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
-  public static enum EnumProductItemVisibility {
-    @SerializedName("LEGACY_PUBLIC")
-    VALUE_LEGACY_PUBLIC("LEGACY_PUBLIC"),
-    @SerializedName("STAGING")
-    VALUE_STAGING("STAGING"),
-    @SerializedName("PUBLISHED")
-    VALUE_PUBLISHED("PUBLISHED"),
-    NULL(null);
-
-    private String value;
-
-    private EnumProductItemVisibility(String value) {
-      this.value = value;
-    }
 
-    @Override
-    public String toString() {
-      return value;
-    }
-  }
 
   synchronized /*package*/ static Gson getGson() {
     if (gson != null) {
@@ -1783,10 +2053,9 @@ public class ProductItem extends APINode {
   }
 
   public ProductItem copyFrom(ProductItem instance) {
-    this.mId = instance.mId;
     this.mAdditionalImageUrls = instance.mAdditionalImageUrls;
-    this.mApplinks = instance.mApplinks;
     this.mAgeGroup = instance.mAgeGroup;
+    this.mApplinks = instance.mApplinks;
     this.mAvailability = instance.mAvailability;
     this.mBrand = instance.mBrand;
     this.mCategory = instance.mCategory;
@@ -1794,40 +2063,46 @@ public class ProductItem extends APINode {
     this.mCommerceInsights = instance.mCommerceInsights;
     this.mCondition = instance.mCondition;
     this.mCustomData = instance.mCustomData;
+    this.mCustomLabel0 = instance.mCustomLabel0;
+    this.mCustomLabel1 = instance.mCustomLabel1;
+    this.mCustomLabel2 = instance.mCustomLabel2;
+    this.mCustomLabel3 = instance.mCustomLabel3;
+    this.mCustomLabel4 = instance.mCustomLabel4;
     this.mDescription = instance.mDescription;
     this.mExpirationDate = instance.mExpirationDate;
     this.mGender = instance.mGender;
     this.mGtin = instance.mGtin;
+    this.mId = instance.mId;
     this.mImageUrl = instance.mImageUrl;
-    this.mMaterial = instance.mMaterial;
     this.mManufacturerPartNumber = instance.mManufacturerPartNumber;
+    this.mMaterial = instance.mMaterial;
     this.mName = instance.mName;
     this.mOrderingIndex = instance.mOrderingIndex;
     this.mPattern = instance.mPattern;
     this.mPrice = instance.mPrice;
+    this.mProductFeed = instance.mProductFeed;
     this.mProductType = instance.mProductType;
     this.mRetailerId = instance.mRetailerId;
     this.mRetailerProductGroupId = instance.mRetailerProductGroupId;
     this.mReviewRejectionReasons = instance.mReviewRejectionReasons;
     this.mReviewStatus = instance.mReviewStatus;
     this.mSalePrice = instance.mSalePrice;
-    this.mSalePriceStartDate = instance.mSalePriceStartDate;
     this.mSalePriceEndDate = instance.mSalePriceEndDate;
-    this.mShippingWeightValue = instance.mShippingWeightValue;
+    this.mSalePriceStartDate = instance.mSalePriceStartDate;
     this.mShippingWeightUnit = instance.mShippingWeightUnit;
+    this.mShippingWeightValue = instance.mShippingWeightValue;
     this.mSize = instance.mSize;
     this.mStartDate = instance.mStartDate;
     this.mUrl = instance.mUrl;
     this.mVisibility = instance.mVisibility;
-    this.mProductFeed = instance.mProductFeed;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<ProductItem> getParser() {
     return new APIRequest.ResponseParser<ProductItem>() {
-      public APINodeList<ProductItem> parseResponse(String response, APIContext context, APIRequest<ProductItem> request) {
+      public APINodeList<ProductItem> parseResponse(String response, APIContext context, APIRequest<ProductItem> request) throws MalformedResponseException {
         return ProductItem.parseResponse(response, context, request);
       }
     };
