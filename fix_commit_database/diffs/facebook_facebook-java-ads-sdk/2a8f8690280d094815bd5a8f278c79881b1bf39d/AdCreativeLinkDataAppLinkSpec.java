@@ -24,29 +24,32 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AdCreativeLinkDataAppLinkSpec extends APINode {
   @SerializedName("android")
   private List<AndroidAppLink> mAndroid = null;
@@ -73,22 +76,23 @@ public class AdCreativeLinkDataAppLinkSpec extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    adCreativeLinkDataAppLinkSpec.mContext = context;
+    adCreativeLinkDataAppLinkSpec.context = context;
     adCreativeLinkDataAppLinkSpec.rawValue = json;
     return adCreativeLinkDataAppLinkSpec;
   }
 
-  public static APINodeList<AdCreativeLinkDataAppLinkSpec> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AdCreativeLinkDataAppLinkSpec> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AdCreativeLinkDataAppLinkSpec> adCreativeLinkDataAppLinkSpecs = new APINodeList<AdCreativeLinkDataAppLinkSpec>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -101,10 +105,11 @@ public class AdCreativeLinkDataAppLinkSpec extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            adCreativeLinkDataAppLinkSpecs.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            adCreativeLinkDataAppLinkSpecs.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -115,7 +120,20 @@ public class AdCreativeLinkDataAppLinkSpec extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            adCreativeLinkDataAppLinkSpecs.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  adCreativeLinkDataAppLinkSpecs.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              adCreativeLinkDataAppLinkSpecs.add(loadJSON(obj.toString(), context));
+            }
           }
           return adCreativeLinkDataAppLinkSpecs;
         } else if (obj.has("images")) {
@@ -126,24 +144,54 @@ public class AdCreativeLinkDataAppLinkSpec extends APINode {
           }
           return adCreativeLinkDataAppLinkSpecs;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              adCreativeLinkDataAppLinkSpecs.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return adCreativeLinkDataAppLinkSpecs;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          adCreativeLinkDataAppLinkSpecs.clear();
           adCreativeLinkDataAppLinkSpecs.add(loadJSON(json, context));
           return adCreativeLinkDataAppLinkSpecs;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -229,14 +277,14 @@ public class AdCreativeLinkDataAppLinkSpec extends APINode {
     this.mIos = instance.mIos;
     this.mIpad = instance.mIpad;
     this.mIphone = instance.mIphone;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AdCreativeLinkDataAppLinkSpec> getParser() {
     return new APIRequest.ResponseParser<AdCreativeLinkDataAppLinkSpec>() {
-      public APINodeList<AdCreativeLinkDataAppLinkSpec> parseResponse(String response, APIContext context, APIRequest<AdCreativeLinkDataAppLinkSpec> request) {
+      public APINodeList<AdCreativeLinkDataAppLinkSpec> parseResponse(String response, APIContext context, APIRequest<AdCreativeLinkDataAppLinkSpec> request) throws MalformedResponseException {
         return AdCreativeLinkDataAppLinkSpec.parseResponse(response, context, request);
       }
     };
