@@ -24,40 +24,43 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AdsImageCrops extends APINode {
-  @SerializedName("191x100")
-  private List<JsonArray> m191x100 = null;
+  @SerializedName("100x100")
+  private List<JsonArray> m100x100 = null;
   @SerializedName("100x72")
   private List<JsonArray> m100x72 = null;
+  @SerializedName("191x100")
+  private List<JsonArray> m191x100 = null;
   @SerializedName("400x150")
   private List<JsonArray> m400x150 = null;
   @SerializedName("600x360")
   private List<JsonArray> m600x360 = null;
-  @SerializedName("100x100")
-  private List<JsonArray> m100x100 = null;
   protected static Gson gson = null;
 
   public AdsImageCrops() {
@@ -75,22 +78,23 @@ public class AdsImageCrops extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    adsImageCrops.mContext = context;
+    adsImageCrops.context = context;
     adsImageCrops.rawValue = json;
     return adsImageCrops;
   }
 
-  public static APINodeList<AdsImageCrops> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AdsImageCrops> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AdsImageCrops> adsImageCropss = new APINodeList<AdsImageCrops>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -103,10 +107,11 @@ public class AdsImageCrops extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            adsImageCropss.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            adsImageCropss.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -117,7 +122,20 @@ public class AdsImageCrops extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            adsImageCropss.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  adsImageCropss.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              adsImageCropss.add(loadJSON(obj.toString(), context));
+            }
           }
           return adsImageCropss;
         } else if (obj.has("images")) {
@@ -128,24 +146,54 @@ public class AdsImageCrops extends APINode {
           }
           return adsImageCropss;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              adsImageCropss.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return adsImageCropss;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          adsImageCropss.clear();
           adsImageCropss.add(loadJSON(json, context));
           return adsImageCropss;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -154,12 +202,12 @@ public class AdsImageCrops extends APINode {
   }
 
 
-  public List<JsonArray> getField191x100() {
-    return m191x100;
+  public List<JsonArray> getField100x100() {
+    return m100x100;
   }
 
-  public AdsImageCrops setField191x100(List<JsonArray> value) {
-    this.m191x100 = value;
+  public AdsImageCrops setField100x100(List<JsonArray> value) {
+    this.m100x100 = value;
     return this;
   }
 
@@ -172,6 +220,15 @@ public class AdsImageCrops extends APINode {
     return this;
   }
 
+  public List<JsonArray> getField191x100() {
+    return m191x100;
+  }
+
+  public AdsImageCrops setField191x100(List<JsonArray> value) {
+    this.m191x100 = value;
+    return this;
+  }
+
   public List<JsonArray> getField400x150() {
     return m400x150;
   }
@@ -190,15 +247,6 @@ public class AdsImageCrops extends APINode {
     return this;
   }
 
-  public List<JsonArray> getField100x100() {
-    return m100x100;
-  }
-
-  public AdsImageCrops setField100x100(List<JsonArray> value) {
-    this.m100x100 = value;
-    return this;
-  }
-
 
 
 
@@ -216,19 +264,19 @@ public class AdsImageCrops extends APINode {
   }
 
   public AdsImageCrops copyFrom(AdsImageCrops instance) {
-    this.m191x100 = instance.m191x100;
+    this.m100x100 = instance.m100x100;
     this.m100x72 = instance.m100x72;
+    this.m191x100 = instance.m191x100;
     this.m400x150 = instance.m400x150;
     this.m600x360 = instance.m600x360;
-    this.m100x100 = instance.m100x100;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AdsImageCrops> getParser() {
     return new APIRequest.ResponseParser<AdsImageCrops>() {
-      public APINodeList<AdsImageCrops> parseResponse(String response, APIContext context, APIRequest<AdsImageCrops> request) {
+      public APINodeList<AdsImageCrops> parseResponse(String response, APIContext context, APIRequest<AdsImageCrops> request) throws MalformedResponseException {
         return AdsImageCrops.parseResponse(response, context, request);
       }
     };
