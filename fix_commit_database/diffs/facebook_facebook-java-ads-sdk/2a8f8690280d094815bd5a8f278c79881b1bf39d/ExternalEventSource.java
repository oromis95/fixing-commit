@@ -24,29 +24,32 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class ExternalEventSource extends APINode {
   @SerializedName("id")
   private String mId = null;
@@ -63,11 +66,11 @@ public class ExternalEventSource extends APINode {
 
   public ExternalEventSource(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public ExternalEventSource fetch() throws APIException{
-    ExternalEventSource newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    ExternalEventSource newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -84,8 +87,17 @@ public class ExternalEventSource extends APINode {
     return externalEventSource;
   }
 
+  public static APINodeList<ExternalEventSource> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<ExternalEventSource>)(
+      new APIRequest<ExternalEventSource>(context, "", "/", "GET", ExternalEventSource.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -100,22 +112,23 @@ public class ExternalEventSource extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    externalEventSource.mContext = context;
+    externalEventSource.context = context;
     externalEventSource.rawValue = json;
     return externalEventSource;
   }
 
-  public static APINodeList<ExternalEventSource> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<ExternalEventSource> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<ExternalEventSource> externalEventSources = new APINodeList<ExternalEventSource>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -128,10 +141,11 @@ public class ExternalEventSource extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            externalEventSources.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            externalEventSources.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -142,7 +156,20 @@ public class ExternalEventSource extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            externalEventSources.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  externalEventSources.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              externalEventSources.add(loadJSON(obj.toString(), context));
+            }
           }
           return externalEventSources;
         } else if (obj.has("images")) {
@@ -153,24 +180,54 @@ public class ExternalEventSource extends APINode {
           }
           return externalEventSources;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              externalEventSources.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return externalEventSources;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          externalEventSources.clear();
           externalEventSources.add(loadJSON(json, context));
           return externalEventSources;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -179,7 +236,7 @@ public class ExternalEventSource extends APINode {
   }
 
   public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
   }
 
 
@@ -220,7 +277,7 @@ public class ExternalEventSource extends APINode {
 
     @Override
     public ExternalEventSource execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -228,11 +285,13 @@ public class ExternalEventSource extends APINode {
       super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestGet setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestGet setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
@@ -250,10 +309,12 @@ public class ExternalEventSource extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGet requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestGet requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -261,11 +322,13 @@ public class ExternalEventSource extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGet requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGet requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
@@ -285,28 +348,28 @@ public class ExternalEventSource extends APINode {
       this.requestField("source_type", value);
       return this;
     }
-
   }
 
   public static enum EnumSourceType {
-    @SerializedName("APP")
-    VALUE_APP("APP"),
-    @SerializedName("PIXEL")
-    VALUE_PIXEL("PIXEL"),
-    NULL(null);
+      @SerializedName("APP")
+      VALUE_APP("APP"),
+      @SerializedName("PIXEL")
+      VALUE_PIXEL("PIXEL"),
+      NULL(null);
 
-    private String value;
+      private String value;
 
-    private EnumSourceType(String value) {
-      this.value = value;
-    }
+      private EnumSourceType(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
 
+
   synchronized /*package*/ static Gson getGson() {
     if (gson != null) {
       return gson;
@@ -323,14 +386,14 @@ public class ExternalEventSource extends APINode {
   public ExternalEventSource copyFrom(ExternalEventSource instance) {
     this.mId = instance.mId;
     this.mSourceType = instance.mSourceType;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<ExternalEventSource> getParser() {
     return new APIRequest.ResponseParser<ExternalEventSource>() {
-      public APINodeList<ExternalEventSource> parseResponse(String response, APIContext context, APIRequest<ExternalEventSource> request) {
+      public APINodeList<ExternalEventSource> parseResponse(String response, APIContext context, APIRequest<ExternalEventSource> request) throws MalformedResponseException {
         return ExternalEventSource.parseResponse(response, context, request);
       }
     };
