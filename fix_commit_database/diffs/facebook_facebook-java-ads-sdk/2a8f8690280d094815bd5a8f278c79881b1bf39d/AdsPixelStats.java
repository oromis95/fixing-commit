@@ -24,34 +24,37 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AdsPixelStats extends APINode {
-  @SerializedName("value")
-  private String mValue = null;
   @SerializedName("count")
   private Long mCount = null;
+  @SerializedName("value")
+  private String mValue = null;
   protected static Gson gson = null;
 
   public AdsPixelStats() {
@@ -69,22 +72,23 @@ public class AdsPixelStats extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    adsPixelStats.mContext = context;
+    adsPixelStats.context = context;
     adsPixelStats.rawValue = json;
     return adsPixelStats;
   }
 
-  public static APINodeList<AdsPixelStats> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AdsPixelStats> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AdsPixelStats> adsPixelStatss = new APINodeList<AdsPixelStats>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -97,10 +101,11 @@ public class AdsPixelStats extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            adsPixelStatss.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            adsPixelStatss.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -111,7 +116,20 @@ public class AdsPixelStats extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            adsPixelStatss.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  adsPixelStatss.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              adsPixelStatss.add(loadJSON(obj.toString(), context));
+            }
           }
           return adsPixelStatss;
         } else if (obj.has("images")) {
@@ -122,24 +140,54 @@ public class AdsPixelStats extends APINode {
           }
           return adsPixelStatss;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              adsPixelStatss.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return adsPixelStatss;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          adsPixelStatss.clear();
           adsPixelStatss.add(loadJSON(json, context));
           return adsPixelStatss;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -148,21 +196,21 @@ public class AdsPixelStats extends APINode {
   }
 
 
-  public String getFieldValue() {
-    return mValue;
+  public Long getFieldCount() {
+    return mCount;
   }
 
-  public AdsPixelStats setFieldValue(String value) {
-    this.mValue = value;
+  public AdsPixelStats setFieldCount(Long value) {
+    this.mCount = value;
     return this;
   }
 
-  public Long getFieldCount() {
-    return mCount;
+  public String getFieldValue() {
+    return mValue;
   }
 
-  public AdsPixelStats setFieldCount(Long value) {
-    this.mCount = value;
+  public AdsPixelStats setFieldValue(String value) {
+    this.mValue = value;
     return this;
   }
 
@@ -183,16 +231,16 @@ public class AdsPixelStats extends APINode {
   }
 
   public AdsPixelStats copyFrom(AdsPixelStats instance) {
-    this.mValue = instance.mValue;
     this.mCount = instance.mCount;
-    this.mContext = instance.mContext;
+    this.mValue = instance.mValue;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AdsPixelStats> getParser() {
     return new APIRequest.ResponseParser<AdsPixelStats>() {
-      public APINodeList<AdsPixelStats> parseResponse(String response, APIContext context, APIRequest<AdsPixelStats> request) {
+      public APINodeList<AdsPixelStats> parseResponse(String response, APIContext context, APIRequest<AdsPixelStats> request) throws MalformedResponseException {
         return AdsPixelStats.parseResponse(response, context, request);
       }
     };
