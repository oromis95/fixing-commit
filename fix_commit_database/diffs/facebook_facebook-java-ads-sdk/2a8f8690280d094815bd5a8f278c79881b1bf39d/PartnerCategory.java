@@ -24,52 +24,55 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class PartnerCategory extends APINode {
-  @SerializedName("id")
-  private String mId = null;
-  @SerializedName("name")
-  private String mName = null;
+  @SerializedName("approximate_count")
+  private Long mApproximateCount = null;
   @SerializedName("country")
   private String mCountry = null;
   @SerializedName("description")
   private String mDescription = null;
   @SerializedName("details")
   private String mDetails = null;
-  @SerializedName("source")
-  private String mSource = null;
-  @SerializedName("parent_category")
-  private String mParentCategory = null;
-  @SerializedName("targeting_type")
-  private String mTargetingType = null;
+  @SerializedName("id")
+  private String mId = null;
   @SerializedName("is_private")
   private Boolean mIsPrivate = null;
-  @SerializedName("approximate_count")
-  private Long mApproximateCount = null;
+  @SerializedName("name")
+  private String mName = null;
+  @SerializedName("parent_category")
+  private String mParentCategory = null;
+  @SerializedName("source")
+  private String mSource = null;
   @SerializedName("status")
   private String mStatus = null;
+  @SerializedName("targeting_type")
+  private String mTargetingType = null;
   protected static Gson gson = null;
 
   public PartnerCategory() {
@@ -87,22 +90,23 @@ public class PartnerCategory extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    partnerCategory.mContext = context;
+    partnerCategory.context = context;
     partnerCategory.rawValue = json;
     return partnerCategory;
   }
 
-  public static APINodeList<PartnerCategory> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<PartnerCategory> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<PartnerCategory> partnerCategorys = new APINodeList<PartnerCategory>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -115,10 +119,11 @@ public class PartnerCategory extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            partnerCategorys.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            partnerCategorys.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -129,7 +134,20 @@ public class PartnerCategory extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            partnerCategorys.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  partnerCategorys.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              partnerCategorys.add(loadJSON(obj.toString(), context));
+            }
           }
           return partnerCategorys;
         } else if (obj.has("images")) {
@@ -140,24 +158,54 @@ public class PartnerCategory extends APINode {
           }
           return partnerCategorys;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              partnerCategorys.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return partnerCategorys;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          partnerCategorys.clear();
           partnerCategorys.add(loadJSON(json, context));
           return partnerCategorys;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -166,21 +214,12 @@ public class PartnerCategory extends APINode {
   }
 
 
-  public String getFieldId() {
-    return mId;
-  }
-
-  public PartnerCategory setFieldId(String value) {
-    this.mId = value;
-    return this;
-  }
-
-  public String getFieldName() {
-    return mName;
+  public Long getFieldApproximateCount() {
+    return mApproximateCount;
   }
 
-  public PartnerCategory setFieldName(String value) {
-    this.mName = value;
+  public PartnerCategory setFieldApproximateCount(Long value) {
+    this.mApproximateCount = value;
     return this;
   }
 
@@ -211,48 +250,48 @@ public class PartnerCategory extends APINode {
     return this;
   }
 
-  public String getFieldSource() {
-    return mSource;
+  public String getFieldId() {
+    return mId;
   }
 
-  public PartnerCategory setFieldSource(String value) {
-    this.mSource = value;
+  public PartnerCategory setFieldId(String value) {
+    this.mId = value;
     return this;
   }
 
-  public String getFieldParentCategory() {
-    return mParentCategory;
+  public Boolean getFieldIsPrivate() {
+    return mIsPrivate;
   }
 
-  public PartnerCategory setFieldParentCategory(String value) {
-    this.mParentCategory = value;
+  public PartnerCategory setFieldIsPrivate(Boolean value) {
+    this.mIsPrivate = value;
     return this;
   }
 
-  public String getFieldTargetingType() {
-    return mTargetingType;
+  public String getFieldName() {
+    return mName;
   }
 
-  public PartnerCategory setFieldTargetingType(String value) {
-    this.mTargetingType = value;
+  public PartnerCategory setFieldName(String value) {
+    this.mName = value;
     return this;
   }
 
-  public Boolean getFieldIsPrivate() {
-    return mIsPrivate;
+  public String getFieldParentCategory() {
+    return mParentCategory;
   }
 
-  public PartnerCategory setFieldIsPrivate(Boolean value) {
-    this.mIsPrivate = value;
+  public PartnerCategory setFieldParentCategory(String value) {
+    this.mParentCategory = value;
     return this;
   }
 
-  public Long getFieldApproximateCount() {
-    return mApproximateCount;
+  public String getFieldSource() {
+    return mSource;
   }
 
-  public PartnerCategory setFieldApproximateCount(Long value) {
-    this.mApproximateCount = value;
+  public PartnerCategory setFieldSource(String value) {
+    this.mSource = value;
     return this;
   }
 
@@ -265,6 +304,15 @@ public class PartnerCategory extends APINode {
     return this;
   }
 
+  public String getFieldTargetingType() {
+    return mTargetingType;
+  }
+
+  public PartnerCategory setFieldTargetingType(String value) {
+    this.mTargetingType = value;
+    return this;
+  }
+
 
 
 
@@ -282,25 +330,25 @@ public class PartnerCategory extends APINode {
   }
 
   public PartnerCategory copyFrom(PartnerCategory instance) {
-    this.mId = instance.mId;
-    this.mName = instance.mName;
+    this.mApproximateCount = instance.mApproximateCount;
     this.mCountry = instance.mCountry;
     this.mDescription = instance.mDescription;
     this.mDetails = instance.mDetails;
-    this.mSource = instance.mSource;
-    this.mParentCategory = instance.mParentCategory;
-    this.mTargetingType = instance.mTargetingType;
+    this.mId = instance.mId;
     this.mIsPrivate = instance.mIsPrivate;
-    this.mApproximateCount = instance.mApproximateCount;
+    this.mName = instance.mName;
+    this.mParentCategory = instance.mParentCategory;
+    this.mSource = instance.mSource;
     this.mStatus = instance.mStatus;
-    this.mContext = instance.mContext;
+    this.mTargetingType = instance.mTargetingType;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<PartnerCategory> getParser() {
     return new APIRequest.ResponseParser<PartnerCategory>() {
-      public APINodeList<PartnerCategory> parseResponse(String response, APIContext context, APIRequest<PartnerCategory> request) {
+      public APINodeList<PartnerCategory> parseResponse(String response, APIContext context, APIRequest<PartnerCategory> request) throws MalformedResponseException {
         return PartnerCategory.parseResponse(response, context, request);
       }
     };
