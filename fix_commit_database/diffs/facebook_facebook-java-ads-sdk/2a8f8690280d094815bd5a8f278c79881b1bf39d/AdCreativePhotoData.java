@@ -24,36 +24,41 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AdCreativePhotoData extends APINode {
+  @SerializedName("branded_content_sponsor_page_id")
+  private String mBrandedContentSponsorPageId = null;
   @SerializedName("caption")
   private String mCaption = null;
-  @SerializedName("url")
-  private String mUrl = null;
   @SerializedName("image_hash")
   private String mImageHash = null;
+  @SerializedName("url")
+  private String mUrl = null;
   protected static Gson gson = null;
 
   public AdCreativePhotoData() {
@@ -71,22 +76,23 @@ public class AdCreativePhotoData extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    adCreativePhotoData.mContext = context;
+    adCreativePhotoData.context = context;
     adCreativePhotoData.rawValue = json;
     return adCreativePhotoData;
   }
 
-  public static APINodeList<AdCreativePhotoData> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AdCreativePhotoData> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AdCreativePhotoData> adCreativePhotoDatas = new APINodeList<AdCreativePhotoData>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -99,10 +105,11 @@ public class AdCreativePhotoData extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            adCreativePhotoDatas.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            adCreativePhotoDatas.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -113,7 +120,20 @@ public class AdCreativePhotoData extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            adCreativePhotoDatas.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  adCreativePhotoDatas.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              adCreativePhotoDatas.add(loadJSON(obj.toString(), context));
+            }
           }
           return adCreativePhotoDatas;
         } else if (obj.has("images")) {
@@ -124,24 +144,54 @@ public class AdCreativePhotoData extends APINode {
           }
           return adCreativePhotoDatas;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              adCreativePhotoDatas.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return adCreativePhotoDatas;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          adCreativePhotoDatas.clear();
           adCreativePhotoDatas.add(loadJSON(json, context));
           return adCreativePhotoDatas;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -150,21 +200,21 @@ public class AdCreativePhotoData extends APINode {
   }
 
 
-  public String getFieldCaption() {
-    return mCaption;
+  public String getFieldBrandedContentSponsorPageId() {
+    return mBrandedContentSponsorPageId;
   }
 
-  public AdCreativePhotoData setFieldCaption(String value) {
-    this.mCaption = value;
+  public AdCreativePhotoData setFieldBrandedContentSponsorPageId(String value) {
+    this.mBrandedContentSponsorPageId = value;
     return this;
   }
 
-  public String getFieldUrl() {
-    return mUrl;
+  public String getFieldCaption() {
+    return mCaption;
   }
 
-  public AdCreativePhotoData setFieldUrl(String value) {
-    this.mUrl = value;
+  public AdCreativePhotoData setFieldCaption(String value) {
+    this.mCaption = value;
     return this;
   }
 
@@ -177,6 +227,15 @@ public class AdCreativePhotoData extends APINode {
     return this;
   }
 
+  public String getFieldUrl() {
+    return mUrl;
+  }
+
+  public AdCreativePhotoData setFieldUrl(String value) {
+    this.mUrl = value;
+    return this;
+  }
+
 
 
 
@@ -194,17 +253,18 @@ public class AdCreativePhotoData extends APINode {
   }
 
   public AdCreativePhotoData copyFrom(AdCreativePhotoData instance) {
+    this.mBrandedContentSponsorPageId = instance.mBrandedContentSponsorPageId;
     this.mCaption = instance.mCaption;
-    this.mUrl = instance.mUrl;
     this.mImageHash = instance.mImageHash;
-    this.mContext = instance.mContext;
+    this.mUrl = instance.mUrl;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AdCreativePhotoData> getParser() {
     return new APIRequest.ResponseParser<AdCreativePhotoData>() {
-      public APINodeList<AdCreativePhotoData> parseResponse(String response, APIContext context, APIRequest<AdCreativePhotoData> request) {
+      public APINodeList<AdCreativePhotoData> parseResponse(String response, APIContext context, APIRequest<AdCreativePhotoData> request) throws MalformedResponseException {
         return AdCreativePhotoData.parseResponse(response, context, request);
       }
     };
