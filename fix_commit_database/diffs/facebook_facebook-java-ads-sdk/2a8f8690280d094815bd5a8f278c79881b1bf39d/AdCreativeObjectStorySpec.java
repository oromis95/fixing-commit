@@ -24,44 +24,47 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AdCreativeObjectStorySpec extends APINode {
-  @SerializedName("page_id")
-  private String mPageId = null;
   @SerializedName("instagram_actor_id")
   private String mInstagramActorId = null;
-  @SerializedName("photo_data")
-  private AdCreativePhotoData mPhotoData = null;
-  @SerializedName("text_data")
-  private AdCreativeTextData mTextData = null;
   @SerializedName("link_data")
   private AdCreativeLinkData mLinkData = null;
-  @SerializedName("template_data")
-  private AdCreativeLinkData mTemplateData = null;
   @SerializedName("offer_data")
   private AdCreativeOfferData mOfferData = null;
+  @SerializedName("page_id")
+  private String mPageId = null;
+  @SerializedName("photo_data")
+  private AdCreativePhotoData mPhotoData = null;
+  @SerializedName("template_data")
+  private AdCreativeLinkData mTemplateData = null;
+  @SerializedName("text_data")
+  private AdCreativeTextData mTextData = null;
   @SerializedName("video_data")
   private AdCreativeVideoData mVideoData = null;
   protected static Gson gson = null;
@@ -81,22 +84,23 @@ public class AdCreativeObjectStorySpec extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    adCreativeObjectStorySpec.mContext = context;
+    adCreativeObjectStorySpec.context = context;
     adCreativeObjectStorySpec.rawValue = json;
     return adCreativeObjectStorySpec;
   }
 
-  public static APINodeList<AdCreativeObjectStorySpec> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AdCreativeObjectStorySpec> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AdCreativeObjectStorySpec> adCreativeObjectStorySpecs = new APINodeList<AdCreativeObjectStorySpec>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -109,10 +113,11 @@ public class AdCreativeObjectStorySpec extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            adCreativeObjectStorySpecs.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            adCreativeObjectStorySpecs.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -123,7 +128,20 @@ public class AdCreativeObjectStorySpec extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            adCreativeObjectStorySpecs.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  adCreativeObjectStorySpecs.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              adCreativeObjectStorySpecs.add(loadJSON(obj.toString(), context));
+            }
           }
           return adCreativeObjectStorySpecs;
         } else if (obj.has("images")) {
@@ -134,24 +152,54 @@ public class AdCreativeObjectStorySpec extends APINode {
           }
           return adCreativeObjectStorySpecs;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              adCreativeObjectStorySpecs.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return adCreativeObjectStorySpecs;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          adCreativeObjectStorySpecs.clear();
           adCreativeObjectStorySpecs.add(loadJSON(json, context));
           return adCreativeObjectStorySpecs;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -160,15 +208,6 @@ public class AdCreativeObjectStorySpec extends APINode {
   }
 
 
-  public String getFieldPageId() {
-    return mPageId;
-  }
-
-  public AdCreativeObjectStorySpec setFieldPageId(String value) {
-    this.mPageId = value;
-    return this;
-  }
-
   public String getFieldInstagramActorId() {
     return mInstagramActorId;
   }
@@ -178,46 +217,55 @@ public class AdCreativeObjectStorySpec extends APINode {
     return this;
   }
 
-  public AdCreativePhotoData getFieldPhotoData() {
-    return mPhotoData;
+  public AdCreativeLinkData getFieldLinkData() {
+    return mLinkData;
   }
 
-  public AdCreativeObjectStorySpec setFieldPhotoData(AdCreativePhotoData value) {
-    this.mPhotoData = value;
+  public AdCreativeObjectStorySpec setFieldLinkData(AdCreativeLinkData value) {
+    this.mLinkData = value;
     return this;
   }
 
-  public AdCreativeObjectStorySpec setFieldPhotoData(String value) {
-    Type type = new TypeToken<AdCreativePhotoData>(){}.getType();
-    this.mPhotoData = AdCreativePhotoData.getGson().fromJson(value, type);
+  public AdCreativeObjectStorySpec setFieldLinkData(String value) {
+    Type type = new TypeToken<AdCreativeLinkData>(){}.getType();
+    this.mLinkData = AdCreativeLinkData.getGson().fromJson(value, type);
     return this;
   }
-  public AdCreativeTextData getFieldTextData() {
-    return mTextData;
+  public AdCreativeOfferData getFieldOfferData() {
+    return mOfferData;
   }
 
-  public AdCreativeObjectStorySpec setFieldTextData(AdCreativeTextData value) {
-    this.mTextData = value;
+  public AdCreativeObjectStorySpec setFieldOfferData(AdCreativeOfferData value) {
+    this.mOfferData = value;
     return this;
   }
 
-  public AdCreativeObjectStorySpec setFieldTextData(String value) {
-    Type type = new TypeToken<AdCreativeTextData>(){}.getType();
-    this.mTextData = AdCreativeTextData.getGson().fromJson(value, type);
+  public AdCreativeObjectStorySpec setFieldOfferData(String value) {
+    Type type = new TypeToken<AdCreativeOfferData>(){}.getType();
+    this.mOfferData = AdCreativeOfferData.getGson().fromJson(value, type);
     return this;
   }
-  public AdCreativeLinkData getFieldLinkData() {
-    return mLinkData;
+  public String getFieldPageId() {
+    return mPageId;
   }
 
-  public AdCreativeObjectStorySpec setFieldLinkData(AdCreativeLinkData value) {
-    this.mLinkData = value;
+  public AdCreativeObjectStorySpec setFieldPageId(String value) {
+    this.mPageId = value;
     return this;
   }
 
-  public AdCreativeObjectStorySpec setFieldLinkData(String value) {
-    Type type = new TypeToken<AdCreativeLinkData>(){}.getType();
-    this.mLinkData = AdCreativeLinkData.getGson().fromJson(value, type);
+  public AdCreativePhotoData getFieldPhotoData() {
+    return mPhotoData;
+  }
+
+  public AdCreativeObjectStorySpec setFieldPhotoData(AdCreativePhotoData value) {
+    this.mPhotoData = value;
+    return this;
+  }
+
+  public AdCreativeObjectStorySpec setFieldPhotoData(String value) {
+    Type type = new TypeToken<AdCreativePhotoData>(){}.getType();
+    this.mPhotoData = AdCreativePhotoData.getGson().fromJson(value, type);
     return this;
   }
   public AdCreativeLinkData getFieldTemplateData() {
@@ -234,18 +282,18 @@ public class AdCreativeObjectStorySpec extends APINode {
     this.mTemplateData = AdCreativeLinkData.getGson().fromJson(value, type);
     return this;
   }
-  public AdCreativeOfferData getFieldOfferData() {
-    return mOfferData;
+  public AdCreativeTextData getFieldTextData() {
+    return mTextData;
   }
 
-  public AdCreativeObjectStorySpec setFieldOfferData(AdCreativeOfferData value) {
-    this.mOfferData = value;
+  public AdCreativeObjectStorySpec setFieldTextData(AdCreativeTextData value) {
+    this.mTextData = value;
     return this;
   }
 
-  public AdCreativeObjectStorySpec setFieldOfferData(String value) {
-    Type type = new TypeToken<AdCreativeOfferData>(){}.getType();
-    this.mOfferData = AdCreativeOfferData.getGson().fromJson(value, type);
+  public AdCreativeObjectStorySpec setFieldTextData(String value) {
+    Type type = new TypeToken<AdCreativeTextData>(){}.getType();
+    this.mTextData = AdCreativeTextData.getGson().fromJson(value, type);
     return this;
   }
   public AdCreativeVideoData getFieldVideoData() {
@@ -279,22 +327,22 @@ public class AdCreativeObjectStorySpec extends APINode {
   }
 
   public AdCreativeObjectStorySpec copyFrom(AdCreativeObjectStorySpec instance) {
-    this.mPageId = instance.mPageId;
     this.mInstagramActorId = instance.mInstagramActorId;
-    this.mPhotoData = instance.mPhotoData;
-    this.mTextData = instance.mTextData;
     this.mLinkData = instance.mLinkData;
-    this.mTemplateData = instance.mTemplateData;
     this.mOfferData = instance.mOfferData;
+    this.mPageId = instance.mPageId;
+    this.mPhotoData = instance.mPhotoData;
+    this.mTemplateData = instance.mTemplateData;
+    this.mTextData = instance.mTextData;
     this.mVideoData = instance.mVideoData;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AdCreativeObjectStorySpec> getParser() {
     return new APIRequest.ResponseParser<AdCreativeObjectStorySpec>() {
-      public APINodeList<AdCreativeObjectStorySpec> parseResponse(String response, APIContext context, APIRequest<AdCreativeObjectStorySpec> request) {
+      public APINodeList<AdCreativeObjectStorySpec> parseResponse(String response, APIContext context, APIRequest<AdCreativeObjectStorySpec> request) throws MalformedResponseException {
         return AdCreativeObjectStorySpec.parseResponse(response, context, request);
       }
     };
