@@ -24,42 +24,45 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class OffsitePixel extends APINode {
-  @SerializedName("id")
-  private String mId = null;
-  @SerializedName("name")
-  private String mName = null;
-  @SerializedName("tag")
-  private String mTag = null;
   @SerializedName("creator")
   private String mCreator = null;
+  @SerializedName("id")
+  private String mId = null;
   @SerializedName("js_pixel")
   private String mJsPixel = null;
   @SerializedName("last_firing_time")
   private String mLastFiringTime = null;
+  @SerializedName("name")
+  private String mName = null;
+  @SerializedName("tag")
+  private String mTag = null;
   protected static Gson gson = null;
 
   OffsitePixel() {
@@ -71,11 +74,11 @@ public class OffsitePixel extends APINode {
 
   public OffsitePixel(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public OffsitePixel fetch() throws APIException{
-    OffsitePixel newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    OffsitePixel newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -92,8 +95,17 @@ public class OffsitePixel extends APINode {
     return offsitePixel;
   }
 
+  public static APINodeList<OffsitePixel> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<OffsitePixel>)(
+      new APIRequest<OffsitePixel>(context, "", "/", "GET", OffsitePixel.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -108,22 +120,23 @@ public class OffsitePixel extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    offsitePixel.mContext = context;
+    offsitePixel.context = context;
     offsitePixel.rawValue = json;
     return offsitePixel;
   }
 
-  public static APINodeList<OffsitePixel> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<OffsitePixel> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<OffsitePixel> offsitePixels = new APINodeList<OffsitePixel>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -136,10 +149,11 @@ public class OffsitePixel extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            offsitePixels.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            offsitePixels.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -150,7 +164,20 @@ public class OffsitePixel extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            offsitePixels.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  offsitePixels.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              offsitePixels.add(loadJSON(obj.toString(), context));
+            }
           }
           return offsitePixels;
         } else if (obj.has("images")) {
@@ -161,24 +188,54 @@ public class OffsitePixel extends APINode {
           }
           return offsitePixels;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              offsitePixels.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return offsitePixels;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          offsitePixels.clear();
           offsitePixels.add(loadJSON(json, context));
           return offsitePixels;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -186,47 +243,39 @@ public class OffsitePixel extends APINode {
     return getGson().toJson(this);
   }
 
-  public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
-  }
-
-  public APIRequestUpdate update() {
-    return new APIRequestUpdate(this.getPrefixedId().toString(), mContext);
-  }
-
-  public APIRequestDelete delete() {
-    return new APIRequestDelete(this.getPrefixedId().toString(), mContext);
+  public APIRequestDeleteAdAccounts deleteAdAccounts() {
+    return new APIRequestDeleteAdAccounts(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetAdAccounts getAdAccounts() {
-    return new APIRequestGetAdAccounts(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetAdAccounts(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestCreateAdAccount createAdAccount() {
-    return new APIRequestCreateAdAccount(this.getPrefixedId().toString(), mContext);
+    return new APIRequestCreateAdAccount(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestDeleteAdAccounts deleteAdAccounts() {
-    return new APIRequestDeleteAdAccounts(this.getPrefixedId().toString(), mContext);
+  public APIRequestDelete delete() {
+    return new APIRequestDelete(this.getPrefixedId().toString(), context);
   }
 
-
-  public String getFieldId() {
-    return mId;
+  public APIRequestGet get() {
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
   }
 
-  public String getFieldName() {
-    return mName;
+  public APIRequestUpdate update() {
+    return new APIRequestUpdate(this.getPrefixedId().toString(), context);
   }
 
-  public String getFieldTag() {
-    return mTag;
-  }
 
   public String getFieldCreator() {
     return mCreator;
   }
 
+  public String getFieldId() {
+    return mId;
+  }
+
   public String getFieldJsPixel() {
     return mJsPixel;
   }
@@ -235,479 +284,243 @@ public class OffsitePixel extends APINode {
     return mLastFiringTime;
   }
 
+  public String getFieldName() {
+    return mName;
+  }
+
+  public String getFieldTag() {
+    return mTag;
+  }
+
 
 
-  public static class APIRequestGet extends APIRequest<OffsitePixel> {
+  public static class APIRequestDeleteAdAccounts extends APIRequest<AdAccount> {
 
-    OffsitePixel lastResponse = null;
+    APINodeList<AdAccount> lastResponse = null;
     @Override
-    public OffsitePixel getLastResponse() {
+    public APINodeList<AdAccount> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
+      "adaccounts",
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "name",
-      "tag",
-      "creator",
-      "js_pixel",
-      "last_firing_time",
     };
 
     @Override
-    public OffsitePixel parseResponse(String response) throws APIException {
-      return OffsitePixel.parseResponse(response, getContext(), this).head();
+    public APINodeList<AdAccount> parseResponse(String response) throws APIException {
+      return AdAccount.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public OffsitePixel execute() throws APIException {
+    public APINodeList<AdAccount> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public OffsitePixel execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<AdAccount> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGet(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
+    public APIRequestDeleteAdAccounts(String nodeId, APIContext context) {
+      super(context, nodeId, "/adaccounts", "DELETE", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGet setParam(String param, Object value) {
+    @Override
+    public APIRequestDeleteAdAccounts setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGet setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestDeleteAdAccounts setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGet requestAllFields () {
+    public APIRequestDeleteAdAccounts setAdaccounts (List<String> adaccounts) {
+      this.setParam("adaccounts", adaccounts);
+      return this;
+    }
+    public APIRequestDeleteAdAccounts setAdaccounts (String adaccounts) {
+      this.setParam("adaccounts", adaccounts);
+      return this;
+    }
+
+    public APIRequestDeleteAdAccounts requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGet requestAllFields (boolean value) {
+    public APIRequestDeleteAdAccounts requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestFields (List<String> fields) {
+    @Override
+    public APIRequestDeleteAdAccounts requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGet requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestDeleteAdAccounts requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestField (String field) {
+    @Override
+    public APIRequestDeleteAdAccounts requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGet requestField (String field, boolean value) {
+    @Override
+    public APIRequestDeleteAdAccounts requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGet requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGet requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
-    }
-    public APIRequestGet requestNameField () {
-      return this.requestNameField(true);
-    }
-    public APIRequestGet requestNameField (boolean value) {
-      this.requestField("name", value);
-      return this;
-    }
-    public APIRequestGet requestTagField () {
-      return this.requestTagField(true);
-    }
-    public APIRequestGet requestTagField (boolean value) {
-      this.requestField("tag", value);
-      return this;
-    }
-    public APIRequestGet requestCreatorField () {
-      return this.requestCreatorField(true);
-    }
-    public APIRequestGet requestCreatorField (boolean value) {
-      this.requestField("creator", value);
-      return this;
-    }
-    public APIRequestGet requestJsPixelField () {
-      return this.requestJsPixelField(true);
-    }
-    public APIRequestGet requestJsPixelField (boolean value) {
-      this.requestField("js_pixel", value);
-      return this;
-    }
-    public APIRequestGet requestLastFiringTimeField () {
-      return this.requestLastFiringTimeField(true);
-    }
-    public APIRequestGet requestLastFiringTimeField (boolean value) {
-      this.requestField("last_firing_time", value);
-      return this;
-    }
-
   }
 
-  public static class APIRequestUpdate extends APIRequest<APINode> {
+  public static class APIRequestGetAdAccounts extends APIRequest<AdAccount> {
 
-    APINode lastResponse = null;
+    APINodeList<AdAccount> lastResponse = null;
     @Override
-    public APINode getLastResponse() {
+    public APINodeList<AdAccount> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "id",
-      "name",
-      "tag",
-      "app_id_for_app_event_firing",
-      "app_event",
     };
 
     public static final String[] FIELDS = {
+      "account_groups",
+      "account_id",
+      "account_status",
+      "age",
+      "agency_client_declaration",
+      "amount_spent",
+      "balance",
+      "business",
+      "business_city",
+      "business_country_code",
+      "business_name",
+      "business_state",
+      "business_street",
+      "business_street2",
+      "business_zip",
+      "capabilities",
+      "created_time",
+      "currency",
+      "disable_reason",
+      "end_advertiser",
+      "end_advertiser_name",
+      "failed_delivery_checks",
+      "funding_source",
+      "funding_source_details",
+      "has_migrated_permissions",
+      "id",
+      "io_number",
+      "is_notifications_enabled",
+      "is_personal",
+      "is_prepay_account",
+      "is_tax_id_required",
+      "last_used_time",
+      "line_numbers",
+      "media_agency",
+      "min_campaign_group_spend_cap",
+      "min_daily_budget",
+      "name",
+      "offsite_pixels_tos_accepted",
+      "owner",
+      "owner_business",
+      "partner",
+      "rf_spec",
+      "spend_cap",
+      "tax_id",
+      "tax_id_status",
+      "tax_id_type",
+      "timezone_id",
+      "timezone_name",
+      "timezone_offset_hours_utc",
+      "tos_accepted",
+      "user_role",
     };
 
     @Override
-    public APINode parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this).head();
+    public APINodeList<AdAccount> parseResponse(String response) throws APIException {
+      return AdAccount.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINode execute() throws APIException {
+    public APINodeList<AdAccount> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<AdAccount> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestUpdate(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "POST", Arrays.asList(PARAMS));
+    public APIRequestGetAdAccounts(String nodeId, APIContext context) {
+      super(context, nodeId, "/adaccounts", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestUpdate setParam(String param, Object value) {
+    @Override
+    public APIRequestGetAdAccounts setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestUpdate setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetAdAccounts setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestUpdate setId (String id) {
-      this.setParam("id", id);
-      return this;
+    public APIRequestGetAdAccounts requestAllFields () {
+      return this.requestAllFields(true);
     }
 
-
-    public APIRequestUpdate setName (String name) {
-      this.setParam("name", name);
+    public APIRequestGetAdAccounts requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
 
+    @Override
+    public APIRequestGetAdAccounts requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
+    }
 
-    public APIRequestUpdate setTag (EnumTag tag) {
-      this.setParam("tag", tag);
+    @Override
+    public APIRequestGetAdAccounts requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
 
-    public APIRequestUpdate setTag (String tag) {
-      this.setParam("tag", tag);
-      return this;
-    }
-
-    public APIRequestUpdate setAppIdForAppEventFiring (Long appIdForAppEventFiring) {
-      this.setParam("app_id_for_app_event_firing", appIdForAppEventFiring);
-      return this;
-    }
-
-    public APIRequestUpdate setAppIdForAppEventFiring (String appIdForAppEventFiring) {
-      this.setParam("app_id_for_app_event_firing", appIdForAppEventFiring);
-      return this;
-    }
-
-    public APIRequestUpdate setAppEvent (String appEvent) {
-      this.setParam("app_event", appEvent);
-      return this;
-    }
-
-
-    public APIRequestUpdate requestAllFields () {
-      return this.requestAllFields(true);
-    }
-
-    public APIRequestUpdate requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
-      return this;
-    }
-
-    public APIRequestUpdate requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
-    }
-
-    public APIRequestUpdate requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
-      return this;
-    }
-
-    public APIRequestUpdate requestField (String field) {
-      this.requestField(field, true);
-      return this;
-    }
-
-    public APIRequestUpdate requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
-      return this;
-    }
-
-
-  }
-
-  public static class APIRequestDelete extends APIRequest<APINode> {
-
-    APINode lastResponse = null;
     @Override
-    public APINode getLastResponse() {
-      return lastResponse;
-    }
-    public static final String[] PARAMS = {
-      "id",
-    };
-
-    public static final String[] FIELDS = {
-    };
-
-    @Override
-    public APINode parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this).head();
-    }
-
-    @Override
-    public APINode execute() throws APIException {
-      return execute(new HashMap<String, Object>());
-    }
-
-    @Override
-    public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
-    }
-
-    public APIRequestDelete(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "DELETE", Arrays.asList(PARAMS));
-    }
-
-    public APIRequestDelete setParam(String param, Object value) {
-      setParamInternal(param, value);
-      return this;
-    }
-
-    public APIRequestDelete setParams(Map<String, Object> params) {
-      setParamsInternal(params);
-      return this;
-    }
-
-
-    public APIRequestDelete setId (String id) {
-      this.setParam("id", id);
-      return this;
-    }
-
-
-    public APIRequestDelete requestAllFields () {
-      return this.requestAllFields(true);
-    }
-
-    public APIRequestDelete requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
-      return this;
-    }
-
-    public APIRequestDelete requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
-    }
-
-    public APIRequestDelete requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
-      return this;
-    }
-
-    public APIRequestDelete requestField (String field) {
-      this.requestField(field, true);
-      return this;
-    }
-
-    public APIRequestDelete requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
-      return this;
-    }
-
-
-  }
-
-  public static class APIRequestGetAdAccounts extends APIRequest<AdAccount> {
-
-    APINodeList<AdAccount> lastResponse = null;
-    @Override
-    public APINodeList<AdAccount> getLastResponse() {
-      return lastResponse;
-    }
-    public static final String[] PARAMS = {
-    };
-
-    public static final String[] FIELDS = {
-      "id",
-      "account_groups",
-      "account_id",
-      "account_status",
-      "age",
-      "agency_client_declaration",
-      "business_city",
-      "business_country_code",
-      "business_name",
-      "business_state",
-      "business_street",
-      "business_street2",
-      "business_zip",
-      "capabilities",
-      "created_time",
-      "currency",
-      "disable_reason",
-      "end_advertiser",
-      "end_advertiser_name",
-      "failed_delivery_checks",
-      "funding_source",
-      "funding_source_details",
-      "has_migrated_permissions",
-      "io_number",
-      "is_notifications_enabled",
-      "is_personal",
-      "is_prepay_account",
-      "is_tax_id_required",
-      "line_numbers",
-      "media_agency",
-      "min_campaign_group_spend_cap",
-      "min_daily_budget",
-      "name",
-      "owner",
-      "offsite_pixels_tos_accepted",
-      "partner",
-      "tax_id",
-      "tax_id_status",
-      "tax_id_type",
-      "timezone_id",
-      "timezone_name",
-      "timezone_offset_hours_utc",
-      "rf_spec",
-      "tos_accepted",
-      "user_role",
-      "vertical_name",
-      "amount_spent",
-      "spend_cap",
-      "balance",
-      "business",
-      "owner_business",
-      "last_used_time",
-      "asset_score",
-    };
-
-    @Override
-    public APINodeList<AdAccount> parseResponse(String response) throws APIException {
-      return AdAccount.parseResponse(response, getContext(), this);
-    }
-
-    @Override
-    public APINodeList<AdAccount> execute() throws APIException {
-      return execute(new HashMap<String, Object>());
-    }
-
-    @Override
-    public APINodeList<AdAccount> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
-    }
-
-    public APIRequestGetAdAccounts(String nodeId, APIContext context) {
-      super(context, nodeId, "/adaccounts", "GET", Arrays.asList(PARAMS));
-    }
-
-    public APIRequestGetAdAccounts setParam(String param, Object value) {
-      setParamInternal(param, value);
-      return this;
-    }
-
-    public APIRequestGetAdAccounts setParams(Map<String, Object> params) {
-      setParamsInternal(params);
-      return this;
-    }
-
-
-    public APIRequestGetAdAccounts requestAllFields () {
-      return this.requestAllFields(true);
-    }
-
-    public APIRequestGetAdAccounts requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
-      return this;
-    }
-
-    public APIRequestGetAdAccounts requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
-    }
-
-    public APIRequestGetAdAccounts requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
-      return this;
-    }
-
     public APIRequestGetAdAccounts requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGetAdAccounts requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetAdAccounts requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGetAdAccounts requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
-    }
     public APIRequestGetAdAccounts requestAccountGroupsField () {
       return this.requestAccountGroupsField(true);
     }
@@ -743,6 +556,27 @@ public class OffsitePixel extends APINode {
       this.requestField("agency_client_declaration", value);
       return this;
     }
+    public APIRequestGetAdAccounts requestAmountSpentField () {
+      return this.requestAmountSpentField(true);
+    }
+    public APIRequestGetAdAccounts requestAmountSpentField (boolean value) {
+      this.requestField("amount_spent", value);
+      return this;
+    }
+    public APIRequestGetAdAccounts requestBalanceField () {
+      return this.requestBalanceField(true);
+    }
+    public APIRequestGetAdAccounts requestBalanceField (boolean value) {
+      this.requestField("balance", value);
+      return this;
+    }
+    public APIRequestGetAdAccounts requestBusinessField () {
+      return this.requestBusinessField(true);
+    }
+    public APIRequestGetAdAccounts requestBusinessField (boolean value) {
+      this.requestField("business", value);
+      return this;
+    }
     public APIRequestGetAdAccounts requestBusinessCityField () {
       return this.requestBusinessCityField(true);
     }
@@ -862,6 +696,13 @@ public class OffsitePixel extends APINode {
       this.requestField("has_migrated_permissions", value);
       return this;
     }
+    public APIRequestGetAdAccounts requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGetAdAccounts requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
     public APIRequestGetAdAccounts requestIoNumberField () {
       return this.requestIoNumberField(true);
     }
@@ -897,6 +738,13 @@ public class OffsitePixel extends APINode {
       this.requestField("is_tax_id_required", value);
       return this;
     }
+    public APIRequestGetAdAccounts requestLastUsedTimeField () {
+      return this.requestLastUsedTimeField(true);
+    }
+    public APIRequestGetAdAccounts requestLastUsedTimeField (boolean value) {
+      this.requestField("last_used_time", value);
+      return this;
+    }
     public APIRequestGetAdAccounts requestLineNumbersField () {
       return this.requestLineNumbersField(true);
     }
@@ -932,6 +780,13 @@ public class OffsitePixel extends APINode {
       this.requestField("name", value);
       return this;
     }
+    public APIRequestGetAdAccounts requestOffsitePixelsTosAcceptedField () {
+      return this.requestOffsitePixelsTosAcceptedField(true);
+    }
+    public APIRequestGetAdAccounts requestOffsitePixelsTosAcceptedField (boolean value) {
+      this.requestField("offsite_pixels_tos_accepted", value);
+      return this;
+    }
     public APIRequestGetAdAccounts requestOwnerField () {
       return this.requestOwnerField(true);
     }
@@ -939,11 +794,11 @@ public class OffsitePixel extends APINode {
       this.requestField("owner", value);
       return this;
     }
-    public APIRequestGetAdAccounts requestOffsitePixelsTosAcceptedField () {
-      return this.requestOffsitePixelsTosAcceptedField(true);
+    public APIRequestGetAdAccounts requestOwnerBusinessField () {
+      return this.requestOwnerBusinessField(true);
     }
-    public APIRequestGetAdAccounts requestOffsitePixelsTosAcceptedField (boolean value) {
-      this.requestField("offsite_pixels_tos_accepted", value);
+    public APIRequestGetAdAccounts requestOwnerBusinessField (boolean value) {
+      this.requestField("owner_business", value);
       return this;
     }
     public APIRequestGetAdAccounts requestPartnerField () {
@@ -953,6 +808,20 @@ public class OffsitePixel extends APINode {
       this.requestField("partner", value);
       return this;
     }
+    public APIRequestGetAdAccounts requestRfSpecField () {
+      return this.requestRfSpecField(true);
+    }
+    public APIRequestGetAdAccounts requestRfSpecField (boolean value) {
+      this.requestField("rf_spec", value);
+      return this;
+    }
+    public APIRequestGetAdAccounts requestSpendCapField () {
+      return this.requestSpendCapField(true);
+    }
+    public APIRequestGetAdAccounts requestSpendCapField (boolean value) {
+      this.requestField("spend_cap", value);
+      return this;
+    }
     public APIRequestGetAdAccounts requestTaxIdField () {
       return this.requestTaxIdField(true);
     }
@@ -995,13 +864,6 @@ public class OffsitePixel extends APINode {
       this.requestField("timezone_offset_hours_utc", value);
       return this;
     }
-    public APIRequestGetAdAccounts requestRfSpecField () {
-      return this.requestRfSpecField(true);
-    }
-    public APIRequestGetAdAccounts requestRfSpecField (boolean value) {
-      this.requestField("rf_spec", value);
-      return this;
-    }
     public APIRequestGetAdAccounts requestTosAcceptedField () {
       return this.requestTosAcceptedField(true);
     }
@@ -1016,272 +878,474 @@ public class OffsitePixel extends APINode {
       this.requestField("user_role", value);
       return this;
     }
-    public APIRequestGetAdAccounts requestVerticalNameField () {
-      return this.requestVerticalNameField(true);
+  }
+
+  public static class APIRequestCreateAdAccount extends APIRequest<AdAccount> {
+
+    AdAccount lastResponse = null;
+    @Override
+    public AdAccount getLastResponse() {
+      return lastResponse;
+    }
+    public static final String[] PARAMS = {
+      "adaccounts",
+    };
+
+    public static final String[] FIELDS = {
+    };
+
+    @Override
+    public AdAccount parseResponse(String response) throws APIException {
+      return AdAccount.parseResponse(response, getContext(), this).head();
+    }
+
+    @Override
+    public AdAccount execute() throws APIException {
+      return execute(new HashMap<String, Object>());
+    }
+
+    @Override
+    public AdAccount execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
+    }
+
+    public APIRequestCreateAdAccount(String nodeId, APIContext context) {
+      super(context, nodeId, "/adaccounts", "POST", Arrays.asList(PARAMS));
+    }
+
+    @Override
+    public APIRequestCreateAdAccount setParam(String param, Object value) {
+      setParamInternal(param, value);
+      return this;
+    }
+
+    @Override
+    public APIRequestCreateAdAccount setParams(Map<String, Object> params) {
+      setParamsInternal(params);
+      return this;
+    }
+
+
+    public APIRequestCreateAdAccount setAdaccounts (List<String> adaccounts) {
+      this.setParam("adaccounts", adaccounts);
+      return this;
     }
-    public APIRequestGetAdAccounts requestVerticalNameField (boolean value) {
-      this.requestField("vertical_name", value);
+    public APIRequestCreateAdAccount setAdaccounts (String adaccounts) {
+      this.setParam("adaccounts", adaccounts);
       return this;
     }
-    public APIRequestGetAdAccounts requestAmountSpentField () {
-      return this.requestAmountSpentField(true);
+
+    public APIRequestCreateAdAccount requestAllFields () {
+      return this.requestAllFields(true);
     }
-    public APIRequestGetAdAccounts requestAmountSpentField (boolean value) {
-      this.requestField("amount_spent", value);
+
+    public APIRequestCreateAdAccount requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetAdAccounts requestSpendCapField () {
-      return this.requestSpendCapField(true);
+
+    @Override
+    public APIRequestCreateAdAccount requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
+    }
+
+    @Override
+    public APIRequestCreateAdAccount requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestCreateAdAccount requestField (String field) {
+      this.requestField(field, true);
+      return this;
+    }
+
+    @Override
+    public APIRequestCreateAdAccount requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
+      return this;
+    }
+
+  }
+
+  public static class APIRequestDelete extends APIRequest<APINode> {
+
+    APINode lastResponse = null;
+    @Override
+    public APINode getLastResponse() {
+      return lastResponse;
+    }
+    public static final String[] PARAMS = {
+      "id",
+    };
+
+    public static final String[] FIELDS = {
+    };
+
+    @Override
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
+    }
+
+    @Override
+    public APINode execute() throws APIException {
+      return execute(new HashMap<String, Object>());
     }
-    public APIRequestGetAdAccounts requestSpendCapField (boolean value) {
-      this.requestField("spend_cap", value);
-      return this;
+
+    @Override
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
     }
-    public APIRequestGetAdAccounts requestBalanceField () {
-      return this.requestBalanceField(true);
+
+    public APIRequestDelete(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "DELETE", Arrays.asList(PARAMS));
     }
-    public APIRequestGetAdAccounts requestBalanceField (boolean value) {
-      this.requestField("balance", value);
+
+    @Override
+    public APIRequestDelete setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
-    public APIRequestGetAdAccounts requestBusinessField () {
-      return this.requestBusinessField(true);
+
+    @Override
+    public APIRequestDelete setParams(Map<String, Object> params) {
+      setParamsInternal(params);
+      return this;
     }
-    public APIRequestGetAdAccounts requestBusinessField (boolean value) {
-      this.requestField("business", value);
+
+
+    public APIRequestDelete setId (String id) {
+      this.setParam("id", id);
       return this;
     }
-    public APIRequestGetAdAccounts requestOwnerBusinessField () {
-      return this.requestOwnerBusinessField(true);
+
+    public APIRequestDelete requestAllFields () {
+      return this.requestAllFields(true);
     }
-    public APIRequestGetAdAccounts requestOwnerBusinessField (boolean value) {
-      this.requestField("owner_business", value);
+
+    public APIRequestDelete requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetAdAccounts requestLastUsedTimeField () {
-      return this.requestLastUsedTimeField(true);
+
+    @Override
+    public APIRequestDelete requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
-    public APIRequestGetAdAccounts requestLastUsedTimeField (boolean value) {
-      this.requestField("last_used_time", value);
+
+    @Override
+    public APIRequestDelete requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetAdAccounts requestAssetScoreField () {
-      return this.requestAssetScoreField(true);
+
+    @Override
+    public APIRequestDelete requestField (String field) {
+      this.requestField(field, true);
+      return this;
     }
-    public APIRequestGetAdAccounts requestAssetScoreField (boolean value) {
-      this.requestField("asset_score", value);
+
+    @Override
+    public APIRequestDelete requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
 
   }
 
-  public static class APIRequestCreateAdAccount extends APIRequest<AdAccount> {
+  public static class APIRequestGet extends APIRequest<OffsitePixel> {
 
-    AdAccount lastResponse = null;
+    OffsitePixel lastResponse = null;
     @Override
-    public AdAccount getLastResponse() {
+    public OffsitePixel getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "adaccounts",
     };
 
     public static final String[] FIELDS = {
+      "creator",
+      "id",
+      "js_pixel",
+      "last_firing_time",
+      "name",
+      "tag",
     };
 
     @Override
-    public AdAccount parseResponse(String response) throws APIException {
-      return AdAccount.parseResponse(response, getContext(), this).head();
+    public OffsitePixel parseResponse(String response) throws APIException {
+      return OffsitePixel.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public AdAccount execute() throws APIException {
+    public OffsitePixel execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public AdAccount execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public OffsitePixel execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestCreateAdAccount(String nodeId, APIContext context) {
-      super(context, nodeId, "/adaccounts", "POST", Arrays.asList(PARAMS));
+    public APIRequestGet(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestCreateAdAccount setParam(String param, Object value) {
+    @Override
+    public APIRequestGet setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestCreateAdAccount setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGet setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestCreateAdAccount setAdaccounts (List<String> adaccounts) {
-      this.setParam("adaccounts", adaccounts);
-      return this;
-    }
-
-    public APIRequestCreateAdAccount setAdaccounts (String adaccounts) {
-      this.setParam("adaccounts", adaccounts);
-      return this;
-    }
-
-    public APIRequestCreateAdAccount requestAllFields () {
+    public APIRequestGet requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestCreateAdAccount requestAllFields (boolean value) {
+    public APIRequestGet requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestCreateAdAccount requestFields (List<String> fields) {
+    @Override
+    public APIRequestGet requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestCreateAdAccount requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGet requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestCreateAdAccount requestField (String field) {
+    @Override
+    public APIRequestGet requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestCreateAdAccount requestField (String field, boolean value) {
+    @Override
+    public APIRequestGet requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
+    public APIRequestGet requestCreatorField () {
+      return this.requestCreatorField(true);
+    }
+    public APIRequestGet requestCreatorField (boolean value) {
+      this.requestField("creator", value);
+      return this;
+    }
+    public APIRequestGet requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGet requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
+    public APIRequestGet requestJsPixelField () {
+      return this.requestJsPixelField(true);
+    }
+    public APIRequestGet requestJsPixelField (boolean value) {
+      this.requestField("js_pixel", value);
+      return this;
+    }
+    public APIRequestGet requestLastFiringTimeField () {
+      return this.requestLastFiringTimeField(true);
+    }
+    public APIRequestGet requestLastFiringTimeField (boolean value) {
+      this.requestField("last_firing_time", value);
+      return this;
+    }
+    public APIRequestGet requestNameField () {
+      return this.requestNameField(true);
+    }
+    public APIRequestGet requestNameField (boolean value) {
+      this.requestField("name", value);
+      return this;
+    }
+    public APIRequestGet requestTagField () {
+      return this.requestTagField(true);
+    }
+    public APIRequestGet requestTagField (boolean value) {
+      this.requestField("tag", value);
+      return this;
+    }
   }
 
-  public static class APIRequestDeleteAdAccounts extends APIRequest<AdAccount> {
+  public static class APIRequestUpdate extends APIRequest<APINode> {
 
-    APINodeList<AdAccount> lastResponse = null;
+    APINode lastResponse = null;
     @Override
-    public APINodeList<AdAccount> getLastResponse() {
+    public APINode getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "adaccounts",
+      "app_event",
+      "app_id_for_app_event_firing",
+      "id",
+      "name",
+      "tag",
     };
 
     public static final String[] FIELDS = {
     };
 
     @Override
-    public APINodeList<AdAccount> parseResponse(String response) throws APIException {
-      return AdAccount.parseResponse(response, getContext(), this);
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public APINodeList<AdAccount> execute() throws APIException {
+    public APINode execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<AdAccount> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestDeleteAdAccounts(String nodeId, APIContext context) {
-      super(context, nodeId, "/adaccounts", "DELETE", Arrays.asList(PARAMS));
+    public APIRequestUpdate(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "POST", Arrays.asList(PARAMS));
     }
 
-    public APIRequestDeleteAdAccounts setParam(String param, Object value) {
+    @Override
+    public APIRequestUpdate setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestDeleteAdAccounts setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestUpdate setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestDeleteAdAccounts setAdaccounts (List<String> adaccounts) {
-      this.setParam("adaccounts", adaccounts);
+    public APIRequestUpdate setAppEvent (String appEvent) {
+      this.setParam("app_event", appEvent);
       return this;
     }
 
-    public APIRequestDeleteAdAccounts setAdaccounts (String adaccounts) {
-      this.setParam("adaccounts", adaccounts);
+    public APIRequestUpdate setAppIdForAppEventFiring (Long appIdForAppEventFiring) {
+      this.setParam("app_id_for_app_event_firing", appIdForAppEventFiring);
+      return this;
+    }
+    public APIRequestUpdate setAppIdForAppEventFiring (String appIdForAppEventFiring) {
+      this.setParam("app_id_for_app_event_firing", appIdForAppEventFiring);
       return this;
     }
 
-    public APIRequestDeleteAdAccounts requestAllFields () {
+    public APIRequestUpdate setId (String id) {
+      this.setParam("id", id);
+      return this;
+    }
+
+    public APIRequestUpdate setName (String name) {
+      this.setParam("name", name);
+      return this;
+    }
+
+    public APIRequestUpdate setTag (OffsitePixel.EnumTag tag) {
+      this.setParam("tag", tag);
+      return this;
+    }
+    public APIRequestUpdate setTag (String tag) {
+      this.setParam("tag", tag);
+      return this;
+    }
+
+    public APIRequestUpdate requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestDeleteAdAccounts requestAllFields (boolean value) {
+    public APIRequestUpdate requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestDeleteAdAccounts requestFields (List<String> fields) {
+    @Override
+    public APIRequestUpdate requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestDeleteAdAccounts requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestUpdate requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestDeleteAdAccounts requestField (String field) {
+    @Override
+    public APIRequestUpdate requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestDeleteAdAccounts requestField (String field, boolean value) {
+    @Override
+    public APIRequestUpdate requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
   }
 
   public static enum EnumTag {
-    @SerializedName("CHECKOUT")
-    VALUE_CHECKOUT("CHECKOUT"),
-    @SerializedName("REGISTRATION")
-    VALUE_REGISTRATION("REGISTRATION"),
-    @SerializedName("LEAD")
-    VALUE_LEAD("LEAD"),
-    @SerializedName("KEY_PAGE_VIEW")
-    VALUE_KEY_PAGE_VIEW("KEY_PAGE_VIEW"),
-    @SerializedName("ADD_TO_CART")
-    VALUE_ADD_TO_CART("ADD_TO_CART"),
-    @SerializedName("OTHER")
-    VALUE_OTHER("OTHER"),
-    NULL(null);
-
-    private String value;
-
-    private EnumTag(String value) {
-      this.value = value;
-    }
+      @SerializedName("CHECKOUT")
+      VALUE_CHECKOUT("CHECKOUT"),
+      @SerializedName("REGISTRATION")
+      VALUE_REGISTRATION("REGISTRATION"),
+      @SerializedName("LEAD")
+      VALUE_LEAD("LEAD"),
+      @SerializedName("KEY_PAGE_VIEW")
+      VALUE_KEY_PAGE_VIEW("KEY_PAGE_VIEW"),
+      @SerializedName("ADD_TO_CART")
+      VALUE_ADD_TO_CART("ADD_TO_CART"),
+      @SerializedName("OTHER")
+      VALUE_OTHER("OTHER"),
+      NULL(null);
+
+      private String value;
+
+      private EnumTag(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
 
+
   synchronized /*package*/ static Gson getGson() {
     if (gson != null) {
       return gson;
@@ -1296,20 +1360,20 @@ public class OffsitePixel extends APINode {
   }
 
   public OffsitePixel copyFrom(OffsitePixel instance) {
-    this.mId = instance.mId;
-    this.mName = instance.mName;
-    this.mTag = instance.mTag;
     this.mCreator = instance.mCreator;
+    this.mId = instance.mId;
     this.mJsPixel = instance.mJsPixel;
     this.mLastFiringTime = instance.mLastFiringTime;
-    this.mContext = instance.mContext;
+    this.mName = instance.mName;
+    this.mTag = instance.mTag;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<OffsitePixel> getParser() {
     return new APIRequest.ResponseParser<OffsitePixel>() {
-      public APINodeList<OffsitePixel> parseResponse(String response, APIContext context, APIRequest<OffsitePixel> request) {
+      public APINodeList<OffsitePixel> parseResponse(String response, APIContext context, APIRequest<OffsitePixel> request) throws MalformedResponseException {
         return OffsitePixel.parseResponse(response, context, request);
       }
     };
