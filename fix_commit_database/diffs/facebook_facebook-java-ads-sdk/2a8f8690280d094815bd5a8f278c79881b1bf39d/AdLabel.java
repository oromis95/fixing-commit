@@ -24,38 +24,41 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AdLabel extends APINode {
-  @SerializedName("id")
-  private String mId = null;
   @SerializedName("account")
   private AdAccount mAccount = null;
-  @SerializedName("name")
-  private String mName = null;
   @SerializedName("created_time")
   private String mCreatedTime = null;
+  @SerializedName("id")
+  private String mId = null;
+  @SerializedName("name")
+  private String mName = null;
   @SerializedName("updated_time")
   private String mUpdatedTime = null;
   protected static Gson gson = null;
@@ -69,11 +72,11 @@ public class AdLabel extends APINode {
 
   public AdLabel(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public AdLabel fetch() throws APIException{
-    AdLabel newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    AdLabel newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -90,8 +93,17 @@ public class AdLabel extends APINode {
     return adLabel;
   }
 
+  public static APINodeList<AdLabel> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<AdLabel>)(
+      new APIRequest<AdLabel>(context, "", "/", "GET", AdLabel.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -106,22 +118,23 @@ public class AdLabel extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    adLabel.mContext = context;
+    adLabel.context = context;
     adLabel.rawValue = json;
     return adLabel;
   }
 
-  public static APINodeList<AdLabel> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AdLabel> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AdLabel> adLabels = new APINodeList<AdLabel>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -134,10 +147,11 @@ public class AdLabel extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            adLabels.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            adLabels.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -148,7 +162,20 @@ public class AdLabel extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            adLabels.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  adLabels.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              adLabels.add(loadJSON(obj.toString(), context));
+            }
           }
           return adLabels;
         } else if (obj.has("images")) {
@@ -159,24 +186,54 @@ public class AdLabel extends APINode {
           }
           return adLabels;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              adLabels.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return adLabels;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          adLabels.clear();
           adLabels.add(loadJSON(json, context));
           return adLabels;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -184,356 +241,382 @@ public class AdLabel extends APINode {
     return getGson().toJson(this);
   }
 
-  public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
-  }
-
-  public APIRequestUpdate update() {
-    return new APIRequestUpdate(this.getPrefixedId().toString(), mContext);
-  }
-
-  public APIRequestDelete delete() {
-    return new APIRequestDelete(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetAdCreatives getAdCreatives() {
+    return new APIRequestGetAdCreatives(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetAds getAds() {
-    return new APIRequestGetAds(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetAds(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetAdSets getAdSets() {
-    return new APIRequestGetAdSets(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetAdSets(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetCampaigns getCampaigns() {
-    return new APIRequestGetCampaigns(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetCampaigns(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetAdCreatives getAdCreatives() {
-    return new APIRequestGetAdCreatives(this.getPrefixedId().toString(), mContext);
+  public APIRequestDelete delete() {
+    return new APIRequestDelete(this.getPrefixedId().toString(), context);
   }
 
+  public APIRequestGet get() {
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
+  }
 
-  public String getFieldId() {
-    return mId;
+  public APIRequestUpdate update() {
+    return new APIRequestUpdate(this.getPrefixedId().toString(), context);
   }
 
+
   public AdAccount getFieldAccount() {
     if (mAccount != null) {
-      mAccount.mContext = getContext();
+      mAccount.context = getContext();
     }
     return mAccount;
   }
 
-  public String getFieldName() {
-    return mName;
-  }
-
   public String getFieldCreatedTime() {
     return mCreatedTime;
   }
 
+  public String getFieldId() {
+    return mId;
+  }
+
+  public String getFieldName() {
+    return mName;
+  }
+
   public String getFieldUpdatedTime() {
     return mUpdatedTime;
   }
 
 
 
-  public static class APIRequestGet extends APIRequest<AdLabel> {
+  public static class APIRequestGetAdCreatives extends APIRequest<AdCreative> {
 
-    AdLabel lastResponse = null;
+    APINodeList<AdCreative> lastResponse = null;
     @Override
-    public AdLabel getLastResponse() {
+    public APINodeList<AdCreative> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
     };
 
     public static final String[] FIELDS = {
+      "actor_id",
+      "actor_image_hash",
+      "actor_image_url",
+      "actor_name",
+      "adlabels",
+      "applink_treatment",
+      "body",
+      "call_to_action_type",
       "id",
-      "account",
+      "image_crops",
+      "image_hash",
+      "image_url",
+      "instagram_actor_id",
+      "instagram_permalink_url",
+      "instagram_story_id",
+      "link_og_id",
+      "link_url",
       "name",
-      "created_time",
-      "updated_time",
+      "object_id",
+      "object_story_id",
+      "object_story_spec",
+      "object_type",
+      "object_url",
+      "platform_customizations",
+      "product_set_id",
+      "run_status",
+      "template_url",
+      "thumbnail_url",
+      "title",
+      "url_tags",
     };
 
     @Override
-    public AdLabel parseResponse(String response) throws APIException {
-      return AdLabel.parseResponse(response, getContext(), this).head();
+    public APINodeList<AdCreative> parseResponse(String response) throws APIException {
+      return AdCreative.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public AdLabel execute() throws APIException {
+    public APINodeList<AdCreative> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public AdLabel execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<AdCreative> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGet(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetAdCreatives(String nodeId, APIContext context) {
+      super(context, nodeId, "/adcreatives", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGet setParam(String param, Object value) {
+    @Override
+    public APIRequestGetAdCreatives setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGet setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetAdCreatives setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGet requestAllFields () {
+    public APIRequestGetAdCreatives requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGet requestAllFields (boolean value) {
+    public APIRequestGetAdCreatives requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetAdCreatives requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGet requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetAdCreatives requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestField (String field) {
+    @Override
+    public APIRequestGetAdCreatives requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGet requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetAdCreatives requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGet requestIdField () {
-      return this.requestIdField(true);
+    public APIRequestGetAdCreatives requestActorIdField () {
+      return this.requestActorIdField(true);
     }
-    public APIRequestGet requestIdField (boolean value) {
-      this.requestField("id", value);
+    public APIRequestGetAdCreatives requestActorIdField (boolean value) {
+      this.requestField("actor_id", value);
       return this;
     }
-    public APIRequestGet requestAccountField () {
-      return this.requestAccountField(true);
+    public APIRequestGetAdCreatives requestActorImageHashField () {
+      return this.requestActorImageHashField(true);
     }
-    public APIRequestGet requestAccountField (boolean value) {
-      this.requestField("account", value);
+    public APIRequestGetAdCreatives requestActorImageHashField (boolean value) {
+      this.requestField("actor_image_hash", value);
       return this;
     }
-    public APIRequestGet requestNameField () {
-      return this.requestNameField(true);
+    public APIRequestGetAdCreatives requestActorImageUrlField () {
+      return this.requestActorImageUrlField(true);
     }
-    public APIRequestGet requestNameField (boolean value) {
-      this.requestField("name", value);
+    public APIRequestGetAdCreatives requestActorImageUrlField (boolean value) {
+      this.requestField("actor_image_url", value);
       return this;
     }
-    public APIRequestGet requestCreatedTimeField () {
-      return this.requestCreatedTimeField(true);
+    public APIRequestGetAdCreatives requestActorNameField () {
+      return this.requestActorNameField(true);
     }
-    public APIRequestGet requestCreatedTimeField (boolean value) {
-      this.requestField("created_time", value);
+    public APIRequestGetAdCreatives requestActorNameField (boolean value) {
+      this.requestField("actor_name", value);
       return this;
     }
-    public APIRequestGet requestUpdatedTimeField () {
-      return this.requestUpdatedTimeField(true);
+    public APIRequestGetAdCreatives requestAdlabelsField () {
+      return this.requestAdlabelsField(true);
     }
-    public APIRequestGet requestUpdatedTimeField (boolean value) {
-      this.requestField("updated_time", value);
+    public APIRequestGetAdCreatives requestAdlabelsField (boolean value) {
+      this.requestField("adlabels", value);
       return this;
     }
-
-  }
-
-  public static class APIRequestUpdate extends APIRequest<APINode> {
-
-    APINode lastResponse = null;
-    @Override
-    public APINode getLastResponse() {
-      return lastResponse;
+    public APIRequestGetAdCreatives requestApplinkTreatmentField () {
+      return this.requestApplinkTreatmentField(true);
     }
-    public static final String[] PARAMS = {
-      "id",
-      "name",
-    };
-
-    public static final String[] FIELDS = {
-    };
-
-    @Override
-    public APINode parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this).head();
+    public APIRequestGetAdCreatives requestApplinkTreatmentField (boolean value) {
+      this.requestField("applink_treatment", value);
+      return this;
     }
-
-    @Override
-    public APINode execute() throws APIException {
-      return execute(new HashMap<String, Object>());
+    public APIRequestGetAdCreatives requestBodyField () {
+      return this.requestBodyField(true);
     }
-
-    @Override
-    public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
+    public APIRequestGetAdCreatives requestBodyField (boolean value) {
+      this.requestField("body", value);
+      return this;
     }
-
-    public APIRequestUpdate(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "POST", Arrays.asList(PARAMS));
+    public APIRequestGetAdCreatives requestCallToActionTypeField () {
+      return this.requestCallToActionTypeField(true);
     }
-
-    public APIRequestUpdate setParam(String param, Object value) {
-      setParamInternal(param, value);
+    public APIRequestGetAdCreatives requestCallToActionTypeField (boolean value) {
+      this.requestField("call_to_action_type", value);
       return this;
     }
-
-    public APIRequestUpdate setParams(Map<String, Object> params) {
-      setParamsInternal(params);
-      return this;
+    public APIRequestGetAdCreatives requestIdField () {
+      return this.requestIdField(true);
     }
-
-
-    public APIRequestUpdate setId (String id) {
-      this.setParam("id", id);
+    public APIRequestGetAdCreatives requestIdField (boolean value) {
+      this.requestField("id", value);
       return this;
     }
-
-
-    public APIRequestUpdate setName (String name) {
-      this.setParam("name", name);
+    public APIRequestGetAdCreatives requestImageCropsField () {
+      return this.requestImageCropsField(true);
+    }
+    public APIRequestGetAdCreatives requestImageCropsField (boolean value) {
+      this.requestField("image_crops", value);
       return this;
     }
-
-
-    public APIRequestUpdate requestAllFields () {
-      return this.requestAllFields(true);
+    public APIRequestGetAdCreatives requestImageHashField () {
+      return this.requestImageHashField(true);
     }
-
-    public APIRequestUpdate requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
+    public APIRequestGetAdCreatives requestImageHashField (boolean value) {
+      this.requestField("image_hash", value);
       return this;
     }
-
-    public APIRequestUpdate requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
+    public APIRequestGetAdCreatives requestImageUrlField () {
+      return this.requestImageUrlField(true);
     }
-
-    public APIRequestUpdate requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+    public APIRequestGetAdCreatives requestImageUrlField (boolean value) {
+      this.requestField("image_url", value);
       return this;
     }
-
-    public APIRequestUpdate requestField (String field) {
-      this.requestField(field, true);
+    public APIRequestGetAdCreatives requestInstagramActorIdField () {
+      return this.requestInstagramActorIdField(true);
+    }
+    public APIRequestGetAdCreatives requestInstagramActorIdField (boolean value) {
+      this.requestField("instagram_actor_id", value);
       return this;
     }
-
-    public APIRequestUpdate requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
+    public APIRequestGetAdCreatives requestInstagramPermalinkUrlField () {
+      return this.requestInstagramPermalinkUrlField(true);
+    }
+    public APIRequestGetAdCreatives requestInstagramPermalinkUrlField (boolean value) {
+      this.requestField("instagram_permalink_url", value);
       return this;
     }
-
-
-  }
-
-  public static class APIRequestDelete extends APIRequest<APINode> {
-
-    APINode lastResponse = null;
-    @Override
-    public APINode getLastResponse() {
-      return lastResponse;
+    public APIRequestGetAdCreatives requestInstagramStoryIdField () {
+      return this.requestInstagramStoryIdField(true);
     }
-    public static final String[] PARAMS = {
-      "id",
-    };
-
-    public static final String[] FIELDS = {
-    };
-
-    @Override
-    public APINode parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this).head();
+    public APIRequestGetAdCreatives requestInstagramStoryIdField (boolean value) {
+      this.requestField("instagram_story_id", value);
+      return this;
     }
-
-    @Override
-    public APINode execute() throws APIException {
-      return execute(new HashMap<String, Object>());
+    public APIRequestGetAdCreatives requestLinkOgIdField () {
+      return this.requestLinkOgIdField(true);
     }
-
-    @Override
-    public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
+    public APIRequestGetAdCreatives requestLinkOgIdField (boolean value) {
+      this.requestField("link_og_id", value);
+      return this;
     }
-
-    public APIRequestDelete(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "DELETE", Arrays.asList(PARAMS));
+    public APIRequestGetAdCreatives requestLinkUrlField () {
+      return this.requestLinkUrlField(true);
     }
-
-    public APIRequestDelete setParam(String param, Object value) {
-      setParamInternal(param, value);
+    public APIRequestGetAdCreatives requestLinkUrlField (boolean value) {
+      this.requestField("link_url", value);
       return this;
     }
-
-    public APIRequestDelete setParams(Map<String, Object> params) {
-      setParamsInternal(params);
+    public APIRequestGetAdCreatives requestNameField () {
+      return this.requestNameField(true);
+    }
+    public APIRequestGetAdCreatives requestNameField (boolean value) {
+      this.requestField("name", value);
       return this;
     }
-
-
-    public APIRequestDelete setId (String id) {
-      this.setParam("id", id);
+    public APIRequestGetAdCreatives requestObjectIdField () {
+      return this.requestObjectIdField(true);
+    }
+    public APIRequestGetAdCreatives requestObjectIdField (boolean value) {
+      this.requestField("object_id", value);
       return this;
     }
-
-
-    public APIRequestDelete requestAllFields () {
-      return this.requestAllFields(true);
+    public APIRequestGetAdCreatives requestObjectStoryIdField () {
+      return this.requestObjectStoryIdField(true);
     }
-
-    public APIRequestDelete requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
+    public APIRequestGetAdCreatives requestObjectStoryIdField (boolean value) {
+      this.requestField("object_story_id", value);
       return this;
     }
-
-    public APIRequestDelete requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
+    public APIRequestGetAdCreatives requestObjectStorySpecField () {
+      return this.requestObjectStorySpecField(true);
     }
-
-    public APIRequestDelete requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+    public APIRequestGetAdCreatives requestObjectStorySpecField (boolean value) {
+      this.requestField("object_story_spec", value);
       return this;
     }
-
-    public APIRequestDelete requestField (String field) {
-      this.requestField(field, true);
+    public APIRequestGetAdCreatives requestObjectTypeField () {
+      return this.requestObjectTypeField(true);
+    }
+    public APIRequestGetAdCreatives requestObjectTypeField (boolean value) {
+      this.requestField("object_type", value);
       return this;
     }
-
-    public APIRequestDelete requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
+    public APIRequestGetAdCreatives requestObjectUrlField () {
+      return this.requestObjectUrlField(true);
+    }
+    public APIRequestGetAdCreatives requestObjectUrlField (boolean value) {
+      this.requestField("object_url", value);
+      return this;
+    }
+    public APIRequestGetAdCreatives requestPlatformCustomizationsField () {
+      return this.requestPlatformCustomizationsField(true);
+    }
+    public APIRequestGetAdCreatives requestPlatformCustomizationsField (boolean value) {
+      this.requestField("platform_customizations", value);
+      return this;
+    }
+    public APIRequestGetAdCreatives requestProductSetIdField () {
+      return this.requestProductSetIdField(true);
+    }
+    public APIRequestGetAdCreatives requestProductSetIdField (boolean value) {
+      this.requestField("product_set_id", value);
+      return this;
+    }
+    public APIRequestGetAdCreatives requestRunStatusField () {
+      return this.requestRunStatusField(true);
+    }
+    public APIRequestGetAdCreatives requestRunStatusField (boolean value) {
+      this.requestField("run_status", value);
+      return this;
+    }
+    public APIRequestGetAdCreatives requestTemplateUrlField () {
+      return this.requestTemplateUrlField(true);
+    }
+    public APIRequestGetAdCreatives requestTemplateUrlField (boolean value) {
+      this.requestField("template_url", value);
+      return this;
+    }
+    public APIRequestGetAdCreatives requestThumbnailUrlField () {
+      return this.requestThumbnailUrlField(true);
+    }
+    public APIRequestGetAdCreatives requestThumbnailUrlField (boolean value) {
+      this.requestField("thumbnail_url", value);
+      return this;
+    }
+    public APIRequestGetAdCreatives requestTitleField () {
+      return this.requestTitleField(true);
+    }
+    public APIRequestGetAdCreatives requestTitleField (boolean value) {
+      this.requestField("title", value);
+      return this;
+    }
+    public APIRequestGetAdCreatives requestUrlTagsField () {
+      return this.requestUrlTagsField(true);
+    }
+    public APIRequestGetAdCreatives requestUrlTagsField (boolean value) {
+      this.requestField("url_tags", value);
       return this;
     }
-
-
   }
 
   public static class APIRequestGetAds extends APIRequest<Ad> {
@@ -547,26 +630,28 @@ public class AdLabel extends APINode {
     };
 
     public static final String[] FIELDS = {
-      "id",
       "account_id",
-      "adset",
-      "campaign",
+      "ad_review_feedback",
       "adlabels",
+      "adset",
       "adset_id",
       "bid_amount",
       "bid_info",
       "bid_type",
+      "campaign",
+      "campaign_id",
       "configured_status",
       "conversion_specs",
       "created_time",
       "creative",
       "effective_status",
+      "id",
       "last_updated_by_app_id",
       "name",
+      "recommendations",
+      "status",
       "tracking_specs",
       "updated_time",
-      "campaign_id",
-      "ad_review_feedback",
     };
 
     @Override
@@ -581,7 +666,7 @@ public class AdLabel extends APINode {
 
     @Override
     public APINodeList<Ad> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -589,11 +674,13 @@ public class AdLabel extends APINode {
       super(context, nodeId, "/ads", "GET", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestGetAds setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestGetAds setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
@@ -611,10 +698,12 @@ public class AdLabel extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetAds requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestGetAds requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -622,23 +711,18 @@ public class AdLabel extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetAds requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGetAds requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetAds requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGetAds requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
-    }
     public APIRequestGetAds requestAccountIdField () {
       return this.requestAccountIdField(true);
     }
@@ -646,18 +730,11 @@ public class AdLabel extends APINode {
       this.requestField("account_id", value);
       return this;
     }
-    public APIRequestGetAds requestAdsetField () {
-      return this.requestAdsetField(true);
-    }
-    public APIRequestGetAds requestAdsetField (boolean value) {
-      this.requestField("adset", value);
-      return this;
-    }
-    public APIRequestGetAds requestCampaignField () {
-      return this.requestCampaignField(true);
+    public APIRequestGetAds requestAdReviewFeedbackField () {
+      return this.requestAdReviewFeedbackField(true);
     }
-    public APIRequestGetAds requestCampaignField (boolean value) {
-      this.requestField("campaign", value);
+    public APIRequestGetAds requestAdReviewFeedbackField (boolean value) {
+      this.requestField("ad_review_feedback", value);
       return this;
     }
     public APIRequestGetAds requestAdlabelsField () {
@@ -667,6 +744,13 @@ public class AdLabel extends APINode {
       this.requestField("adlabels", value);
       return this;
     }
+    public APIRequestGetAds requestAdsetField () {
+      return this.requestAdsetField(true);
+    }
+    public APIRequestGetAds requestAdsetField (boolean value) {
+      this.requestField("adset", value);
+      return this;
+    }
     public APIRequestGetAds requestAdsetIdField () {
       return this.requestAdsetIdField(true);
     }
@@ -695,6 +779,20 @@ public class AdLabel extends APINode {
       this.requestField("bid_type", value);
       return this;
     }
+    public APIRequestGetAds requestCampaignField () {
+      return this.requestCampaignField(true);
+    }
+    public APIRequestGetAds requestCampaignField (boolean value) {
+      this.requestField("campaign", value);
+      return this;
+    }
+    public APIRequestGetAds requestCampaignIdField () {
+      return this.requestCampaignIdField(true);
+    }
+    public APIRequestGetAds requestCampaignIdField (boolean value) {
+      this.requestField("campaign_id", value);
+      return this;
+    }
     public APIRequestGetAds requestConfiguredStatusField () {
       return this.requestConfiguredStatusField(true);
     }
@@ -730,6 +828,13 @@ public class AdLabel extends APINode {
       this.requestField("effective_status", value);
       return this;
     }
+    public APIRequestGetAds requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGetAds requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
     public APIRequestGetAds requestLastUpdatedByAppIdField () {
       return this.requestLastUpdatedByAppIdField(true);
     }
@@ -744,6 +849,20 @@ public class AdLabel extends APINode {
       this.requestField("name", value);
       return this;
     }
+    public APIRequestGetAds requestRecommendationsField () {
+      return this.requestRecommendationsField(true);
+    }
+    public APIRequestGetAds requestRecommendationsField (boolean value) {
+      this.requestField("recommendations", value);
+      return this;
+    }
+    public APIRequestGetAds requestStatusField () {
+      return this.requestStatusField(true);
+    }
+    public APIRequestGetAds requestStatusField (boolean value) {
+      this.requestField("status", value);
+      return this;
+    }
     public APIRequestGetAds requestTrackingSpecsField () {
       return this.requestTrackingSpecsField(true);
     }
@@ -758,21 +877,6 @@ public class AdLabel extends APINode {
       this.requestField("updated_time", value);
       return this;
     }
-    public APIRequestGetAds requestCampaignIdField () {
-      return this.requestCampaignIdField(true);
-    }
-    public APIRequestGetAds requestCampaignIdField (boolean value) {
-      this.requestField("campaign_id", value);
-      return this;
-    }
-    public APIRequestGetAds requestAdReviewFeedbackField () {
-      return this.requestAdReviewFeedbackField(true);
-    }
-    public APIRequestGetAds requestAdReviewFeedbackField (boolean value) {
-      this.requestField("ad_review_feedback", value);
-      return this;
-    }
-
   }
 
   public static class APIRequestGetAdSets extends APIRequest<AdSet> {
@@ -786,40 +890,41 @@ public class AdLabel extends APINode {
     };
 
     public static final String[] FIELDS = {
+      "account_id",
       "adlabels",
       "adset_schedule",
-      "id",
-      "account_id",
       "bid_amount",
       "bid_info",
       "billing_event",
+      "budget_remaining",
       "campaign",
       "campaign_id",
       "configured_status",
       "created_time",
       "creative_sequence",
+      "daily_budget",
       "effective_status",
       "end_time",
       "frequency_cap",
       "frequency_cap_reset_period",
       "frequency_control_specs",
+      "id",
       "is_autobid",
+      "lifetime_budget",
       "lifetime_frequency_cap",
       "lifetime_imps",
       "name",
       "optimization_goal",
-      "product_ad_behavior",
+      "pacing_type",
       "promoted_object",
+      "recommendations",
       "rf_prediction_id",
       "rtb_flag",
       "start_time",
+      "status",
       "targeting",
       "updated_time",
       "use_new_app_click",
-      "pacing_type",
-      "budget_remaining",
-      "daily_budget",
-      "lifetime_budget",
     };
 
     @Override
@@ -834,7 +939,7 @@ public class AdLabel extends APINode {
 
     @Override
     public APINodeList<AdSet> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -842,11 +947,13 @@ public class AdLabel extends APINode {
       super(context, nodeId, "/adsets", "GET", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestGetAdSets setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestGetAdSets setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
@@ -864,10 +971,12 @@ public class AdLabel extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetAdSets requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestGetAdSets requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -875,16 +984,25 @@ public class AdLabel extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetAdSets requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGetAdSets requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
+    public APIRequestGetAdSets requestAccountIdField () {
+      return this.requestAccountIdField(true);
+    }
+    public APIRequestGetAdSets requestAccountIdField (boolean value) {
+      this.requestField("account_id", value);
+      return this;
+    }
     public APIRequestGetAdSets requestAdlabelsField () {
       return this.requestAdlabelsField(true);
     }
@@ -899,20 +1017,6 @@ public class AdLabel extends APINode {
       this.requestField("adset_schedule", value);
       return this;
     }
-    public APIRequestGetAdSets requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGetAdSets requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
-    }
-    public APIRequestGetAdSets requestAccountIdField () {
-      return this.requestAccountIdField(true);
-    }
-    public APIRequestGetAdSets requestAccountIdField (boolean value) {
-      this.requestField("account_id", value);
-      return this;
-    }
     public APIRequestGetAdSets requestBidAmountField () {
       return this.requestBidAmountField(true);
     }
@@ -934,6 +1038,13 @@ public class AdLabel extends APINode {
       this.requestField("billing_event", value);
       return this;
     }
+    public APIRequestGetAdSets requestBudgetRemainingField () {
+      return this.requestBudgetRemainingField(true);
+    }
+    public APIRequestGetAdSets requestBudgetRemainingField (boolean value) {
+      this.requestField("budget_remaining", value);
+      return this;
+    }
     public APIRequestGetAdSets requestCampaignField () {
       return this.requestCampaignField(true);
     }
@@ -969,6 +1080,13 @@ public class AdLabel extends APINode {
       this.requestField("creative_sequence", value);
       return this;
     }
+    public APIRequestGetAdSets requestDailyBudgetField () {
+      return this.requestDailyBudgetField(true);
+    }
+    public APIRequestGetAdSets requestDailyBudgetField (boolean value) {
+      this.requestField("daily_budget", value);
+      return this;
+    }
     public APIRequestGetAdSets requestEffectiveStatusField () {
       return this.requestEffectiveStatusField(true);
     }
@@ -1004,6 +1122,13 @@ public class AdLabel extends APINode {
       this.requestField("frequency_control_specs", value);
       return this;
     }
+    public APIRequestGetAdSets requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGetAdSets requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
     public APIRequestGetAdSets requestIsAutobidField () {
       return this.requestIsAutobidField(true);
     }
@@ -1011,6 +1136,13 @@ public class AdLabel extends APINode {
       this.requestField("is_autobid", value);
       return this;
     }
+    public APIRequestGetAdSets requestLifetimeBudgetField () {
+      return this.requestLifetimeBudgetField(true);
+    }
+    public APIRequestGetAdSets requestLifetimeBudgetField (boolean value) {
+      this.requestField("lifetime_budget", value);
+      return this;
+    }
     public APIRequestGetAdSets requestLifetimeFrequencyCapField () {
       return this.requestLifetimeFrequencyCapField(true);
     }
@@ -1039,11 +1171,11 @@ public class AdLabel extends APINode {
       this.requestField("optimization_goal", value);
       return this;
     }
-    public APIRequestGetAdSets requestProductAdBehaviorField () {
-      return this.requestProductAdBehaviorField(true);
+    public APIRequestGetAdSets requestPacingTypeField () {
+      return this.requestPacingTypeField(true);
     }
-    public APIRequestGetAdSets requestProductAdBehaviorField (boolean value) {
-      this.requestField("product_ad_behavior", value);
+    public APIRequestGetAdSets requestPacingTypeField (boolean value) {
+      this.requestField("pacing_type", value);
       return this;
     }
     public APIRequestGetAdSets requestPromotedObjectField () {
@@ -1053,6 +1185,13 @@ public class AdLabel extends APINode {
       this.requestField("promoted_object", value);
       return this;
     }
+    public APIRequestGetAdSets requestRecommendationsField () {
+      return this.requestRecommendationsField(true);
+    }
+    public APIRequestGetAdSets requestRecommendationsField (boolean value) {
+      this.requestField("recommendations", value);
+      return this;
+    }
     public APIRequestGetAdSets requestRfPredictionIdField () {
       return this.requestRfPredictionIdField(true);
     }
@@ -1074,6 +1213,13 @@ public class AdLabel extends APINode {
       this.requestField("start_time", value);
       return this;
     }
+    public APIRequestGetAdSets requestStatusField () {
+      return this.requestStatusField(true);
+    }
+    public APIRequestGetAdSets requestStatusField (boolean value) {
+      this.requestField("status", value);
+      return this;
+    }
     public APIRequestGetAdSets requestTargetingField () {
       return this.requestTargetingField(true);
     }
@@ -1095,35 +1241,6 @@ public class AdLabel extends APINode {
       this.requestField("use_new_app_click", value);
       return this;
     }
-    public APIRequestGetAdSets requestPacingTypeField () {
-      return this.requestPacingTypeField(true);
-    }
-    public APIRequestGetAdSets requestPacingTypeField (boolean value) {
-      this.requestField("pacing_type", value);
-      return this;
-    }
-    public APIRequestGetAdSets requestBudgetRemainingField () {
-      return this.requestBudgetRemainingField(true);
-    }
-    public APIRequestGetAdSets requestBudgetRemainingField (boolean value) {
-      this.requestField("budget_remaining", value);
-      return this;
-    }
-    public APIRequestGetAdSets requestDailyBudgetField () {
-      return this.requestDailyBudgetField(true);
-    }
-    public APIRequestGetAdSets requestDailyBudgetField (boolean value) {
-      this.requestField("daily_budget", value);
-      return this;
-    }
-    public APIRequestGetAdSets requestLifetimeBudgetField () {
-      return this.requestLifetimeBudgetField(true);
-    }
-    public APIRequestGetAdSets requestLifetimeBudgetField (boolean value) {
-      this.requestField("lifetime_budget", value);
-      return this;
-    }
-
   }
 
   public static class APIRequestGetCampaigns extends APIRequest<Campaign> {
@@ -1137,20 +1254,22 @@ public class AdLabel extends APINode {
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "adlabels",
       "account_id",
+      "adlabels",
       "buying_type",
       "can_use_spend_cap",
       "configured_status",
       "created_time",
       "effective_status",
+      "id",
       "name",
       "objective",
+      "recommendations",
+      "spend_cap",
       "start_time",
+      "status",
       "stop_time",
       "updated_time",
-      "spend_cap",
     };
 
     @Override
@@ -1165,7 +1284,7 @@ public class AdLabel extends APINode {
 
     @Override
     public APINodeList<Campaign> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -1173,11 +1292,13 @@ public class AdLabel extends APINode {
       super(context, nodeId, "/campaigns", "GET", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestGetCampaigns setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestGetCampaigns setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
@@ -1195,10 +1316,12 @@ public class AdLabel extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetCampaigns requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestGetCampaigns requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -1206,21 +1329,23 @@ public class AdLabel extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetCampaigns requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGetCampaigns requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetCampaigns requestIdField () {
-      return this.requestIdField(true);
+    public APIRequestGetCampaigns requestAccountIdField () {
+      return this.requestAccountIdField(true);
     }
-    public APIRequestGetCampaigns requestIdField (boolean value) {
-      this.requestField("id", value);
+    public APIRequestGetCampaigns requestAccountIdField (boolean value) {
+      this.requestField("account_id", value);
       return this;
     }
     public APIRequestGetCampaigns requestAdlabelsField () {
@@ -1230,13 +1355,6 @@ public class AdLabel extends APINode {
       this.requestField("adlabels", value);
       return this;
     }
-    public APIRequestGetCampaigns requestAccountIdField () {
-      return this.requestAccountIdField(true);
-    }
-    public APIRequestGetCampaigns requestAccountIdField (boolean value) {
-      this.requestField("account_id", value);
-      return this;
-    }
     public APIRequestGetCampaigns requestBuyingTypeField () {
       return this.requestBuyingTypeField(true);
     }
@@ -1272,6 +1390,13 @@ public class AdLabel extends APINode {
       this.requestField("effective_status", value);
       return this;
     }
+    public APIRequestGetCampaigns requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGetCampaigns requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
     public APIRequestGetCampaigns requestNameField () {
       return this.requestNameField(true);
     }
@@ -1286,6 +1411,20 @@ public class AdLabel extends APINode {
       this.requestField("objective", value);
       return this;
     }
+    public APIRequestGetCampaigns requestRecommendationsField () {
+      return this.requestRecommendationsField(true);
+    }
+    public APIRequestGetCampaigns requestRecommendationsField (boolean value) {
+      this.requestField("recommendations", value);
+      return this;
+    }
+    public APIRequestGetCampaigns requestSpendCapField () {
+      return this.requestSpendCapField(true);
+    }
+    public APIRequestGetCampaigns requestSpendCapField (boolean value) {
+      this.requestField("spend_cap", value);
+      return this;
+    }
     public APIRequestGetCampaigns requestStartTimeField () {
       return this.requestStartTimeField(true);
     }
@@ -1293,6 +1432,13 @@ public class AdLabel extends APINode {
       this.requestField("start_time", value);
       return this;
     }
+    public APIRequestGetCampaigns requestStatusField () {
+      return this.requestStatusField(true);
+    }
+    public APIRequestGetCampaigns requestStatusField (boolean value) {
+      this.requestField("status", value);
+      return this;
+    }
     public APIRequestGetCampaigns requestStopTimeField () {
       return this.requestStopTimeField(true);
     }
@@ -1307,319 +1453,339 @@ public class AdLabel extends APINode {
       this.requestField("updated_time", value);
       return this;
     }
-    public APIRequestGetCampaigns requestSpendCapField () {
-      return this.requestSpendCapField(true);
-    }
-    public APIRequestGetCampaigns requestSpendCapField (boolean value) {
-      this.requestField("spend_cap", value);
-      return this;
-    }
-
   }
 
-  public static class APIRequestGetAdCreatives extends APIRequest<AdCreative> {
+  public static class APIRequestDelete extends APIRequest<APINode> {
 
-    APINodeList<AdCreative> lastResponse = null;
+    APINode lastResponse = null;
     @Override
-    public APINodeList<AdCreative> getLastResponse() {
+    public APINode getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
+      "id",
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "actor_id",
-      "actor_image_hash",
-      "actor_image_url",
-      "actor_name",
-      "adlabels",
-      "body",
-      "call_to_action_type",
-      "image_crops",
-      "image_hash",
-      "image_url",
-      "instagram_actor_id",
-      "instagram_permalink_url",
-      "link_og_id",
-      "link_url",
-      "name",
-      "object_id",
-      "object_url",
-      "object_story_id",
-      "object_story_spec",
-      "object_type",
-      "product_set_id",
-      "run_status",
-      "template_url",
-      "thumbnail_url",
-      "title",
-      "url_tags",
-      "applink_treatment",
     };
 
     @Override
-    public APINodeList<AdCreative> parseResponse(String response) throws APIException {
-      return AdCreative.parseResponse(response, getContext(), this);
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public APINodeList<AdCreative> execute() throws APIException {
+    public APINode execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<AdCreative> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetAdCreatives(String nodeId, APIContext context) {
-      super(context, nodeId, "/adcreatives", "GET", Arrays.asList(PARAMS));
+    public APIRequestDelete(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "DELETE", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetAdCreatives setParam(String param, Object value) {
+    @Override
+    public APIRequestDelete setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetAdCreatives setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestDelete setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetAdCreatives requestAllFields () {
+    public APIRequestDelete setId (String id) {
+      this.setParam("id", id);
+      return this;
+    }
+
+    public APIRequestDelete requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetAdCreatives requestAllFields (boolean value) {
+    public APIRequestDelete requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetAdCreatives requestFields (List<String> fields) {
+    @Override
+    public APIRequestDelete requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetAdCreatives requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestDelete requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetAdCreatives requestField (String field) {
+    @Override
+    public APIRequestDelete requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetAdCreatives requestField (String field, boolean value) {
+    @Override
+    public APIRequestDelete requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetAdCreatives requestIdField () {
-      return this.requestIdField(true);
+  }
+
+  public static class APIRequestGet extends APIRequest<AdLabel> {
+
+    AdLabel lastResponse = null;
+    @Override
+    public AdLabel getLastResponse() {
+      return lastResponse;
     }
-    public APIRequestGetAdCreatives requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
+    public static final String[] PARAMS = {
+    };
+
+    public static final String[] FIELDS = {
+      "account",
+      "created_time",
+      "id",
+      "name",
+      "updated_time",
+    };
+
+    @Override
+    public AdLabel parseResponse(String response) throws APIException {
+      return AdLabel.parseResponse(response, getContext(), this).head();
     }
-    public APIRequestGetAdCreatives requestActorIdField () {
-      return this.requestActorIdField(true);
+
+    @Override
+    public AdLabel execute() throws APIException {
+      return execute(new HashMap<String, Object>());
     }
-    public APIRequestGetAdCreatives requestActorIdField (boolean value) {
-      this.requestField("actor_id", value);
-      return this;
-    }
-    public APIRequestGetAdCreatives requestActorImageHashField () {
-      return this.requestActorImageHashField(true);
-    }
-    public APIRequestGetAdCreatives requestActorImageHashField (boolean value) {
-      this.requestField("actor_image_hash", value);
-      return this;
-    }
-    public APIRequestGetAdCreatives requestActorImageUrlField () {
-      return this.requestActorImageUrlField(true);
-    }
-    public APIRequestGetAdCreatives requestActorImageUrlField (boolean value) {
-      this.requestField("actor_image_url", value);
-      return this;
-    }
-    public APIRequestGetAdCreatives requestActorNameField () {
-      return this.requestActorNameField(true);
-    }
-    public APIRequestGetAdCreatives requestActorNameField (boolean value) {
-      this.requestField("actor_name", value);
-      return this;
-    }
-    public APIRequestGetAdCreatives requestAdlabelsField () {
-      return this.requestAdlabelsField(true);
-    }
-    public APIRequestGetAdCreatives requestAdlabelsField (boolean value) {
-      this.requestField("adlabels", value);
-      return this;
+
+    @Override
+    public AdLabel execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
     }
-    public APIRequestGetAdCreatives requestBodyField () {
-      return this.requestBodyField(true);
+
+    public APIRequestGet(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
-    public APIRequestGetAdCreatives requestBodyField (boolean value) {
-      this.requestField("body", value);
+
+    @Override
+    public APIRequestGet setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
-    public APIRequestGetAdCreatives requestCallToActionTypeField () {
-      return this.requestCallToActionTypeField(true);
-    }
-    public APIRequestGetAdCreatives requestCallToActionTypeField (boolean value) {
-      this.requestField("call_to_action_type", value);
+
+    @Override
+    public APIRequestGet setParams(Map<String, Object> params) {
+      setParamsInternal(params);
       return this;
     }
-    public APIRequestGetAdCreatives requestImageCropsField () {
-      return this.requestImageCropsField(true);
+
+
+    public APIRequestGet requestAllFields () {
+      return this.requestAllFields(true);
     }
-    public APIRequestGetAdCreatives requestImageCropsField (boolean value) {
-      this.requestField("image_crops", value);
+
+    public APIRequestGet requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetAdCreatives requestImageHashField () {
-      return this.requestImageHashField(true);
+
+    @Override
+    public APIRequestGet requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
-    public APIRequestGetAdCreatives requestImageHashField (boolean value) {
-      this.requestField("image_hash", value);
+
+    @Override
+    public APIRequestGet requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetAdCreatives requestImageUrlField () {
-      return this.requestImageUrlField(true);
-    }
-    public APIRequestGetAdCreatives requestImageUrlField (boolean value) {
-      this.requestField("image_url", value);
+
+    @Override
+    public APIRequestGet requestField (String field) {
+      this.requestField(field, true);
       return this;
     }
-    public APIRequestGetAdCreatives requestInstagramActorIdField () {
-      return this.requestInstagramActorIdField(true);
-    }
-    public APIRequestGetAdCreatives requestInstagramActorIdField (boolean value) {
-      this.requestField("instagram_actor_id", value);
+
+    @Override
+    public APIRequestGet requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
-    public APIRequestGetAdCreatives requestInstagramPermalinkUrlField () {
-      return this.requestInstagramPermalinkUrlField(true);
+
+    public APIRequestGet requestAccountField () {
+      return this.requestAccountField(true);
     }
-    public APIRequestGetAdCreatives requestInstagramPermalinkUrlField (boolean value) {
-      this.requestField("instagram_permalink_url", value);
+    public APIRequestGet requestAccountField (boolean value) {
+      this.requestField("account", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestLinkOgIdField () {
-      return this.requestLinkOgIdField(true);
+    public APIRequestGet requestCreatedTimeField () {
+      return this.requestCreatedTimeField(true);
     }
-    public APIRequestGetAdCreatives requestLinkOgIdField (boolean value) {
-      this.requestField("link_og_id", value);
+    public APIRequestGet requestCreatedTimeField (boolean value) {
+      this.requestField("created_time", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestLinkUrlField () {
-      return this.requestLinkUrlField(true);
+    public APIRequestGet requestIdField () {
+      return this.requestIdField(true);
     }
-    public APIRequestGetAdCreatives requestLinkUrlField (boolean value) {
-      this.requestField("link_url", value);
+    public APIRequestGet requestIdField (boolean value) {
+      this.requestField("id", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestNameField () {
+    public APIRequestGet requestNameField () {
       return this.requestNameField(true);
     }
-    public APIRequestGetAdCreatives requestNameField (boolean value) {
+    public APIRequestGet requestNameField (boolean value) {
       this.requestField("name", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestObjectIdField () {
-      return this.requestObjectIdField(true);
-    }
-    public APIRequestGetAdCreatives requestObjectIdField (boolean value) {
-      this.requestField("object_id", value);
-      return this;
-    }
-    public APIRequestGetAdCreatives requestObjectUrlField () {
-      return this.requestObjectUrlField(true);
+    public APIRequestGet requestUpdatedTimeField () {
+      return this.requestUpdatedTimeField(true);
     }
-    public APIRequestGetAdCreatives requestObjectUrlField (boolean value) {
-      this.requestField("object_url", value);
+    public APIRequestGet requestUpdatedTimeField (boolean value) {
+      this.requestField("updated_time", value);
       return this;
     }
-    public APIRequestGetAdCreatives requestObjectStoryIdField () {
-      return this.requestObjectStoryIdField(true);
+  }
+
+  public static class APIRequestUpdate extends APIRequest<APINode> {
+
+    APINode lastResponse = null;
+    @Override
+    public APINode getLastResponse() {
+      return lastResponse;
     }
-    public APIRequestGetAdCreatives requestObjectStoryIdField (boolean value) {
-      this.requestField("object_story_id", value);
-      return this;
+    public static final String[] PARAMS = {
+      "id",
+      "name",
+    };
+
+    public static final String[] FIELDS = {
+    };
+
+    @Override
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
     }
-    public APIRequestGetAdCreatives requestObjectStorySpecField () {
-      return this.requestObjectStorySpecField(true);
+
+    @Override
+    public APINode execute() throws APIException {
+      return execute(new HashMap<String, Object>());
     }
-    public APIRequestGetAdCreatives requestObjectStorySpecField (boolean value) {
-      this.requestField("object_story_spec", value);
-      return this;
+
+    @Override
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
     }
-    public APIRequestGetAdCreatives requestObjectTypeField () {
-      return this.requestObjectTypeField(true);
+
+    public APIRequestUpdate(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "POST", Arrays.asList(PARAMS));
     }
-    public APIRequestGetAdCreatives requestObjectTypeField (boolean value) {
-      this.requestField("object_type", value);
+
+    @Override
+    public APIRequestUpdate setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
-    public APIRequestGetAdCreatives requestProductSetIdField () {
-      return this.requestProductSetIdField(true);
-    }
-    public APIRequestGetAdCreatives requestProductSetIdField (boolean value) {
-      this.requestField("product_set_id", value);
+
+    @Override
+    public APIRequestUpdate setParams(Map<String, Object> params) {
+      setParamsInternal(params);
       return this;
     }
-    public APIRequestGetAdCreatives requestRunStatusField () {
-      return this.requestRunStatusField(true);
-    }
-    public APIRequestGetAdCreatives requestRunStatusField (boolean value) {
-      this.requestField("run_status", value);
+
+
+    public APIRequestUpdate setId (String id) {
+      this.setParam("id", id);
       return this;
     }
-    public APIRequestGetAdCreatives requestTemplateUrlField () {
-      return this.requestTemplateUrlField(true);
-    }
-    public APIRequestGetAdCreatives requestTemplateUrlField (boolean value) {
-      this.requestField("template_url", value);
+
+    public APIRequestUpdate setName (String name) {
+      this.setParam("name", name);
       return this;
     }
-    public APIRequestGetAdCreatives requestThumbnailUrlField () {
-      return this.requestThumbnailUrlField(true);
+
+    public APIRequestUpdate requestAllFields () {
+      return this.requestAllFields(true);
     }
-    public APIRequestGetAdCreatives requestThumbnailUrlField (boolean value) {
-      this.requestField("thumbnail_url", value);
+
+    public APIRequestUpdate requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetAdCreatives requestTitleField () {
-      return this.requestTitleField(true);
+
+    @Override
+    public APIRequestUpdate requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
-    public APIRequestGetAdCreatives requestTitleField (boolean value) {
-      this.requestField("title", value);
+
+    @Override
+    public APIRequestUpdate requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetAdCreatives requestUrlTagsField () {
-      return this.requestUrlTagsField(true);
-    }
-    public APIRequestGetAdCreatives requestUrlTagsField (boolean value) {
-      this.requestField("url_tags", value);
+
+    @Override
+    public APIRequestUpdate requestField (String field) {
+      this.requestField(field, true);
       return this;
     }
-    public APIRequestGetAdCreatives requestApplinkTreatmentField () {
-      return this.requestApplinkTreatmentField(true);
-    }
-    public APIRequestGetAdCreatives requestApplinkTreatmentField (boolean value) {
-      this.requestField("applink_treatment", value);
+
+    @Override
+    public APIRequestUpdate requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
 
   }
 
+  public static enum EnumExecutionOptions {
+      @SerializedName("VALIDATE_ONLY")
+      VALUE_VALIDATE_ONLY("VALIDATE_ONLY"),
+      @SerializedName("SYNCHRONOUS_AD_REVIEW")
+      VALUE_SYNCHRONOUS_AD_REVIEW("SYNCHRONOUS_AD_REVIEW"),
+      @SerializedName("INCLUDE_RECOMMENDATIONS")
+      VALUE_INCLUDE_RECOMMENDATIONS("INCLUDE_RECOMMENDATIONS"),
+      NULL(null);
+
+      private String value;
+
+      private EnumExecutionOptions(String value) {
+        this.value = value;
+      }
+
+      @Override
+      public String toString() {
+        return value;
+      }
+  }
+
 
   synchronized /*package*/ static Gson getGson() {
     if (gson != null) {
@@ -1635,19 +1801,19 @@ public class AdLabel extends APINode {
   }
 
   public AdLabel copyFrom(AdLabel instance) {
-    this.mId = instance.mId;
     this.mAccount = instance.mAccount;
-    this.mName = instance.mName;
     this.mCreatedTime = instance.mCreatedTime;
+    this.mId = instance.mId;
+    this.mName = instance.mName;
     this.mUpdatedTime = instance.mUpdatedTime;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AdLabel> getParser() {
     return new APIRequest.ResponseParser<AdLabel>() {
-      public APINodeList<AdLabel> parseResponse(String response, APIContext context, APIRequest<AdLabel> request) {
+      public APINodeList<AdLabel> parseResponse(String response, APIContext context, APIRequest<AdLabel> request) throws MalformedResponseException {
         return AdLabel.parseResponse(response, context, request);
       }
     };
