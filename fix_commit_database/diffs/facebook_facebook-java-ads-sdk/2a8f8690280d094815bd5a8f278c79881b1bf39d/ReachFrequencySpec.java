@@ -24,40 +24,43 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class ReachFrequencySpec extends APINode {
-  @SerializedName("min_reach_limits")
-  private Map<String, String> mMinReachLimits = null;
   @SerializedName("countries")
   private List<String> mCountries = null;
-  @SerializedName("min_campaign_duration")
-  private Map<String, String> mMinCampaignDuration = null;
   @SerializedName("max_campaign_duration")
-  private Map<String, String> mMaxCampaignDuration = null;
+  private Object mMaxCampaignDuration = null;
   @SerializedName("max_days_to_finish")
-  private Map<String, String> mMaxDaysToFinish = null;
+  private Object mMaxDaysToFinish = null;
+  @SerializedName("min_campaign_duration")
+  private Object mMinCampaignDuration = null;
+  @SerializedName("min_reach_limits")
+  private Object mMinReachLimits = null;
   protected static Gson gson = null;
 
   public ReachFrequencySpec() {
@@ -75,22 +78,23 @@ public class ReachFrequencySpec extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    reachFrequencySpec.mContext = context;
+    reachFrequencySpec.context = context;
     reachFrequencySpec.rawValue = json;
     return reachFrequencySpec;
   }
 
-  public static APINodeList<ReachFrequencySpec> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<ReachFrequencySpec> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<ReachFrequencySpec> reachFrequencySpecs = new APINodeList<ReachFrequencySpec>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -103,10 +107,11 @@ public class ReachFrequencySpec extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            reachFrequencySpecs.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            reachFrequencySpecs.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -117,7 +122,20 @@ public class ReachFrequencySpec extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            reachFrequencySpecs.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  reachFrequencySpecs.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              reachFrequencySpecs.add(loadJSON(obj.toString(), context));
+            }
           }
           return reachFrequencySpecs;
         } else if (obj.has("images")) {
@@ -128,24 +146,54 @@ public class ReachFrequencySpec extends APINode {
           }
           return reachFrequencySpecs;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              reachFrequencySpecs.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return reachFrequencySpecs;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          reachFrequencySpecs.clear();
           reachFrequencySpecs.add(loadJSON(json, context));
           return reachFrequencySpecs;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -154,48 +202,48 @@ public class ReachFrequencySpec extends APINode {
   }
 
 
-  public Map<String, String> getFieldMinReachLimits() {
-    return mMinReachLimits;
+  public List<String> getFieldCountries() {
+    return mCountries;
   }
 
-  public ReachFrequencySpec setFieldMinReachLimits(Map<String, String> value) {
-    this.mMinReachLimits = value;
+  public ReachFrequencySpec setFieldCountries(List<String> value) {
+    this.mCountries = value;
     return this;
   }
 
-  public List<String> getFieldCountries() {
-    return mCountries;
+  public Object getFieldMaxCampaignDuration() {
+    return mMaxCampaignDuration;
   }
 
-  public ReachFrequencySpec setFieldCountries(List<String> value) {
-    this.mCountries = value;
+  public ReachFrequencySpec setFieldMaxCampaignDuration(Object value) {
+    this.mMaxCampaignDuration = value;
     return this;
   }
 
-  public Map<String, String> getFieldMinCampaignDuration() {
-    return mMinCampaignDuration;
+  public Object getFieldMaxDaysToFinish() {
+    return mMaxDaysToFinish;
   }
 
-  public ReachFrequencySpec setFieldMinCampaignDuration(Map<String, String> value) {
-    this.mMinCampaignDuration = value;
+  public ReachFrequencySpec setFieldMaxDaysToFinish(Object value) {
+    this.mMaxDaysToFinish = value;
     return this;
   }
 
-  public Map<String, String> getFieldMaxCampaignDuration() {
-    return mMaxCampaignDuration;
+  public Object getFieldMinCampaignDuration() {
+    return mMinCampaignDuration;
   }
 
-  public ReachFrequencySpec setFieldMaxCampaignDuration(Map<String, String> value) {
-    this.mMaxCampaignDuration = value;
+  public ReachFrequencySpec setFieldMinCampaignDuration(Object value) {
+    this.mMinCampaignDuration = value;
     return this;
   }
 
-  public Map<String, String> getFieldMaxDaysToFinish() {
-    return mMaxDaysToFinish;
+  public Object getFieldMinReachLimits() {
+    return mMinReachLimits;
   }
 
-  public ReachFrequencySpec setFieldMaxDaysToFinish(Map<String, String> value) {
-    this.mMaxDaysToFinish = value;
+  public ReachFrequencySpec setFieldMinReachLimits(Object value) {
+    this.mMinReachLimits = value;
     return this;
   }
 
@@ -216,19 +264,19 @@ public class ReachFrequencySpec extends APINode {
   }
 
   public ReachFrequencySpec copyFrom(ReachFrequencySpec instance) {
-    this.mMinReachLimits = instance.mMinReachLimits;
     this.mCountries = instance.mCountries;
-    this.mMinCampaignDuration = instance.mMinCampaignDuration;
     this.mMaxCampaignDuration = instance.mMaxCampaignDuration;
     this.mMaxDaysToFinish = instance.mMaxDaysToFinish;
-    this.mContext = instance.mContext;
+    this.mMinCampaignDuration = instance.mMinCampaignDuration;
+    this.mMinReachLimits = instance.mMinReachLimits;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<ReachFrequencySpec> getParser() {
     return new APIRequest.ResponseParser<ReachFrequencySpec>() {
-      public APINodeList<ReachFrequencySpec> parseResponse(String response, APIContext context, APIRequest<ReachFrequencySpec> request) {
+      public APINodeList<ReachFrequencySpec> parseResponse(String response, APIContext context, APIRequest<ReachFrequencySpec> request) throws MalformedResponseException {
         return ReachFrequencySpec.parseResponse(response, context, request);
       }
     };
