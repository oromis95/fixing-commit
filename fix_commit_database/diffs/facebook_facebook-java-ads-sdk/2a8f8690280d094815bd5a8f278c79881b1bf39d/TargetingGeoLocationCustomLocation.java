@@ -24,50 +24,53 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class TargetingGeoLocationCustomLocation extends APINode {
-  @SerializedName("key")
-  private String mKey = null;
-  @SerializedName("name")
-  private String mName = null;
   @SerializedName("address_string")
   private String mAddressString = null;
+  @SerializedName("country")
+  private String mCountry = null;
   @SerializedName("distance_unit")
   private String mDistanceUnit = null;
+  @SerializedName("key")
+  private String mKey = null;
   @SerializedName("latitude")
   private Double mLatitude = null;
   @SerializedName("longitude")
   private Double mLongitude = null;
-  @SerializedName("radius")
-  private Double mRadius = null;
+  @SerializedName("name")
+  private String mName = null;
   @SerializedName("primary_city_id")
   private Long mPrimaryCityId = null;
+  @SerializedName("radius")
+  private Double mRadius = null;
   @SerializedName("region_id")
   private Long mRegionId = null;
-  @SerializedName("country")
-  private String mCountry = null;
   protected static Gson gson = null;
 
   public TargetingGeoLocationCustomLocation() {
@@ -85,22 +88,23 @@ public class TargetingGeoLocationCustomLocation extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    targetingGeoLocationCustomLocation.mContext = context;
+    targetingGeoLocationCustomLocation.context = context;
     targetingGeoLocationCustomLocation.rawValue = json;
     return targetingGeoLocationCustomLocation;
   }
 
-  public static APINodeList<TargetingGeoLocationCustomLocation> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<TargetingGeoLocationCustomLocation> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<TargetingGeoLocationCustomLocation> targetingGeoLocationCustomLocations = new APINodeList<TargetingGeoLocationCustomLocation>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -113,10 +117,11 @@ public class TargetingGeoLocationCustomLocation extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            targetingGeoLocationCustomLocations.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            targetingGeoLocationCustomLocations.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -127,7 +132,20 @@ public class TargetingGeoLocationCustomLocation extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            targetingGeoLocationCustomLocations.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  targetingGeoLocationCustomLocations.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              targetingGeoLocationCustomLocations.add(loadJSON(obj.toString(), context));
+            }
           }
           return targetingGeoLocationCustomLocations;
         } else if (obj.has("images")) {
@@ -138,24 +156,54 @@ public class TargetingGeoLocationCustomLocation extends APINode {
           }
           return targetingGeoLocationCustomLocations;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              targetingGeoLocationCustomLocations.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return targetingGeoLocationCustomLocations;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          targetingGeoLocationCustomLocations.clear();
           targetingGeoLocationCustomLocations.add(loadJSON(json, context));
           return targetingGeoLocationCustomLocations;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -164,39 +212,39 @@ public class TargetingGeoLocationCustomLocation extends APINode {
   }
 
 
-  public String getFieldKey() {
-    return mKey;
+  public String getFieldAddressString() {
+    return mAddressString;
   }
 
-  public TargetingGeoLocationCustomLocation setFieldKey(String value) {
-    this.mKey = value;
+  public TargetingGeoLocationCustomLocation setFieldAddressString(String value) {
+    this.mAddressString = value;
     return this;
   }
 
-  public String getFieldName() {
-    return mName;
+  public String getFieldCountry() {
+    return mCountry;
   }
 
-  public TargetingGeoLocationCustomLocation setFieldName(String value) {
-    this.mName = value;
+  public TargetingGeoLocationCustomLocation setFieldCountry(String value) {
+    this.mCountry = value;
     return this;
   }
 
-  public String getFieldAddressString() {
-    return mAddressString;
+  public String getFieldDistanceUnit() {
+    return mDistanceUnit;
   }
 
-  public TargetingGeoLocationCustomLocation setFieldAddressString(String value) {
-    this.mAddressString = value;
+  public TargetingGeoLocationCustomLocation setFieldDistanceUnit(String value) {
+    this.mDistanceUnit = value;
     return this;
   }
 
-  public String getFieldDistanceUnit() {
-    return mDistanceUnit;
+  public String getFieldKey() {
+    return mKey;
   }
 
-  public TargetingGeoLocationCustomLocation setFieldDistanceUnit(String value) {
-    this.mDistanceUnit = value;
+  public TargetingGeoLocationCustomLocation setFieldKey(String value) {
+    this.mKey = value;
     return this;
   }
 
@@ -218,12 +266,12 @@ public class TargetingGeoLocationCustomLocation extends APINode {
     return this;
   }
 
-  public Double getFieldRadius() {
-    return mRadius;
+  public String getFieldName() {
+    return mName;
   }
 
-  public TargetingGeoLocationCustomLocation setFieldRadius(Double value) {
-    this.mRadius = value;
+  public TargetingGeoLocationCustomLocation setFieldName(String value) {
+    this.mName = value;
     return this;
   }
 
@@ -236,21 +284,21 @@ public class TargetingGeoLocationCustomLocation extends APINode {
     return this;
   }
 
-  public Long getFieldRegionId() {
-    return mRegionId;
+  public Double getFieldRadius() {
+    return mRadius;
   }
 
-  public TargetingGeoLocationCustomLocation setFieldRegionId(Long value) {
-    this.mRegionId = value;
+  public TargetingGeoLocationCustomLocation setFieldRadius(Double value) {
+    this.mRadius = value;
     return this;
   }
 
-  public String getFieldCountry() {
-    return mCountry;
+  public Long getFieldRegionId() {
+    return mRegionId;
   }
 
-  public TargetingGeoLocationCustomLocation setFieldCountry(String value) {
-    this.mCountry = value;
+  public TargetingGeoLocationCustomLocation setFieldRegionId(Long value) {
+    this.mRegionId = value;
     return this;
   }
 
@@ -271,24 +319,24 @@ public class TargetingGeoLocationCustomLocation extends APINode {
   }
 
   public TargetingGeoLocationCustomLocation copyFrom(TargetingGeoLocationCustomLocation instance) {
-    this.mKey = instance.mKey;
-    this.mName = instance.mName;
     this.mAddressString = instance.mAddressString;
+    this.mCountry = instance.mCountry;
     this.mDistanceUnit = instance.mDistanceUnit;
+    this.mKey = instance.mKey;
     this.mLatitude = instance.mLatitude;
     this.mLongitude = instance.mLongitude;
-    this.mRadius = instance.mRadius;
+    this.mName = instance.mName;
     this.mPrimaryCityId = instance.mPrimaryCityId;
+    this.mRadius = instance.mRadius;
     this.mRegionId = instance.mRegionId;
-    this.mCountry = instance.mCountry;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<TargetingGeoLocationCustomLocation> getParser() {
     return new APIRequest.ResponseParser<TargetingGeoLocationCustomLocation>() {
-      public APINodeList<TargetingGeoLocationCustomLocation> parseResponse(String response, APIContext context, APIRequest<TargetingGeoLocationCustomLocation> request) {
+      public APINodeList<TargetingGeoLocationCustomLocation> parseResponse(String response, APIContext context, APIRequest<TargetingGeoLocationCustomLocation> request) throws MalformedResponseException {
         return TargetingGeoLocationCustomLocation.parseResponse(response, context, request);
       }
     };
