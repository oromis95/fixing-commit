@@ -24,36 +24,39 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class BroadTargetingCategories extends APINode {
+  @SerializedName("category_description")
+  private String mCategoryDescription = null;
   @SerializedName("id")
   private String mId = null;
   @SerializedName("name")
   private String mName = null;
-  @SerializedName("category_description")
-  private String mCategoryDescription = null;
   @SerializedName("parent_category")
   private String mParentCategory = null;
   @SerializedName("path")
@@ -87,22 +90,23 @@ public class BroadTargetingCategories extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    broadTargetingCategories.mContext = context;
+    broadTargetingCategories.context = context;
     broadTargetingCategories.rawValue = json;
     return broadTargetingCategories;
   }
 
-  public static APINodeList<BroadTargetingCategories> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<BroadTargetingCategories> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<BroadTargetingCategories> broadTargetingCategoriess = new APINodeList<BroadTargetingCategories>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -115,10 +119,11 @@ public class BroadTargetingCategories extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            broadTargetingCategoriess.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            broadTargetingCategoriess.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -129,7 +134,20 @@ public class BroadTargetingCategories extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            broadTargetingCategoriess.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  broadTargetingCategoriess.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              broadTargetingCategoriess.add(loadJSON(obj.toString(), context));
+            }
           }
           return broadTargetingCategoriess;
         } else if (obj.has("images")) {
@@ -140,24 +158,54 @@ public class BroadTargetingCategories extends APINode {
           }
           return broadTargetingCategoriess;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              broadTargetingCategoriess.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return broadTargetingCategoriess;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          broadTargetingCategoriess.clear();
           broadTargetingCategoriess.add(loadJSON(json, context));
           return broadTargetingCategoriess;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -166,6 +214,15 @@ public class BroadTargetingCategories extends APINode {
   }
 
 
+  public String getFieldCategoryDescription() {
+    return mCategoryDescription;
+  }
+
+  public BroadTargetingCategories setFieldCategoryDescription(String value) {
+    this.mCategoryDescription = value;
+    return this;
+  }
+
   public String getFieldId() {
     return mId;
   }
@@ -184,15 +241,6 @@ public class BroadTargetingCategories extends APINode {
     return this;
   }
 
-  public String getFieldCategoryDescription() {
-    return mCategoryDescription;
-  }
-
-  public BroadTargetingCategories setFieldCategoryDescription(String value) {
-    this.mCategoryDescription = value;
-    return this;
-  }
-
   public String getFieldParentCategory() {
     return mParentCategory;
   }
@@ -282,9 +330,9 @@ public class BroadTargetingCategories extends APINode {
   }
 
   public BroadTargetingCategories copyFrom(BroadTargetingCategories instance) {
+    this.mCategoryDescription = instance.mCategoryDescription;
     this.mId = instance.mId;
     this.mName = instance.mName;
-    this.mCategoryDescription = instance.mCategoryDescription;
     this.mParentCategory = instance.mParentCategory;
     this.mPath = instance.mPath;
     this.mSize = instance.mSize;
@@ -293,14 +341,14 @@ public class BroadTargetingCategories extends APINode {
     this.mTypeName = instance.mTypeName;
     this.mUntranslatedName = instance.mUntranslatedName;
     this.mUntranslatedParentName = instance.mUntranslatedParentName;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<BroadTargetingCategories> getParser() {
     return new APIRequest.ResponseParser<BroadTargetingCategories>() {
-      public APINodeList<BroadTargetingCategories> parseResponse(String response, APIContext context, APIRequest<BroadTargetingCategories> request) {
+      public APINodeList<BroadTargetingCategories> parseResponse(String response, APIContext context, APIRequest<BroadTargetingCategories> request) throws MalformedResponseException {
         return BroadTargetingCategories.parseResponse(response, context, request);
       }
     };
