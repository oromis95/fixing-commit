@@ -24,32 +24,33 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class CustomAudience extends APINode {
-  @SerializedName("id")
-  private String mId = null;
   @SerializedName("account_id")
   private String mAccountId = null;
   @SerializedName("approximate_count")
@@ -62,10 +63,14 @@ public class CustomAudience extends APINode {
   private String mDescription = null;
   @SerializedName("excluded_custom_audiences")
   private List<CustomAudience> mExcludedCustomAudiences = null;
-  @SerializedName("included_custom_audiences")
-  private List<CustomAudience> mIncludedCustomAudiences = null;
   @SerializedName("external_event_source")
   private AdsPixel mExternalEventSource = null;
+  @SerializedName("id")
+  private String mId = null;
+  @SerializedName("included_custom_audiences")
+  private List<CustomAudience> mIncludedCustomAudiences = null;
+  @SerializedName("last_used_time")
+  private String mLastUsedTime = null;
   @SerializedName("lookalike_audience_ids")
   private List<String> mLookalikeAudienceIds = null;
   @SerializedName("lookalike_spec")
@@ -76,22 +81,24 @@ public class CustomAudience extends APINode {
   private CustomAudienceStatus mOperationStatus = null;
   @SerializedName("opt_out_link")
   private String mOptOutLink = null;
+  @SerializedName("owner_business")
+  private Business mOwnerBusiness = null;
   @SerializedName("permission_for_actions")
   private CustomAudiencePermission mPermissionForActions = null;
   @SerializedName("pixel_id")
   private String mPixelId = null;
-  @SerializedName("rule")
-  private String mRule = null;
   @SerializedName("retention_days")
   private Long mRetentionDays = null;
+  @SerializedName("rule")
+  private String mRule = null;
   @SerializedName("subtype")
   private String mSubtype = null;
+  @SerializedName("time_content_updated")
+  private Long mTimeContentUpdated = null;
   @SerializedName("time_created")
   private Long mTimeCreated = null;
   @SerializedName("time_updated")
   private Long mTimeUpdated = null;
-  @SerializedName("time_content_updated")
-  private Long mTimeContentUpdated = null;
   protected static Gson gson = null;
 
   CustomAudience() {
@@ -103,11 +110,11 @@ public class CustomAudience extends APINode {
 
   public CustomAudience(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public CustomAudience fetch() throws APIException{
-    CustomAudience newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    CustomAudience newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -124,8 +131,17 @@ public class CustomAudience extends APINode {
     return customAudience;
   }
 
+  public static APINodeList<CustomAudience> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<CustomAudience>)(
+      new APIRequest<CustomAudience>(context, "", "/", "GET", CustomAudience.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -140,22 +156,23 @@ public class CustomAudience extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    customAudience.mContext = context;
+    customAudience.context = context;
     customAudience.rawValue = json;
     return customAudience;
   }
 
-  public static APINodeList<CustomAudience> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<CustomAudience> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<CustomAudience> customAudiences = new APINodeList<CustomAudience>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -168,10 +185,11 @@ public class CustomAudience extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            customAudiences.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            customAudiences.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -182,7 +200,20 @@ public class CustomAudience extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            customAudiences.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  customAudiences.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              customAudiences.add(loadJSON(obj.toString(), context));
+            }
           }
           return customAudiences;
         } else if (obj.has("images")) {
@@ -193,24 +224,54 @@ public class CustomAudience extends APINode {
           }
           return customAudiences;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              customAudiences.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return customAudiences;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          customAudiences.clear();
           customAudiences.add(loadJSON(json, context));
           return customAudiences;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -218,67 +279,51 @@ public class CustomAudience extends APINode {
     return getGson().toJson(this);
   }
 
-  public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
-  }
-
-  public APIRequestUpdate update() {
-    return new APIRequestUpdate(this.getPrefixedId().toString(), mContext);
-  }
-
-  public APIRequestDelete delete() {
-    return new APIRequestDelete(this.getPrefixedId().toString(), mContext);
+  public APIRequestDeleteAdAccounts deleteAdAccounts() {
+    return new APIRequestDeleteAdAccounts(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetCapabilities getCapabilities() {
-    return new APIRequestGetCapabilities(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetAdAccounts getAdAccounts() {
+    return new APIRequestGetAdAccounts(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetPrefills getPrefills() {
-    return new APIRequestGetPrefills(this.getPrefixedId().toString(), mContext);
+  public APIRequestCreateAdAccount createAdAccount() {
+    return new APIRequestCreateAdAccount(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetAds getAds() {
-    return new APIRequestGetAds(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetAds(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetAdAccounts getAdAccounts() {
-    return new APIRequestGetAdAccounts(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetPrefills getPrefills() {
+    return new APIRequestGetPrefills(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetSessions getSessions() {
-    return new APIRequestGetSessions(this.getPrefixedId().toString(), mContext);
-  }
-
-  public APIRequestCreateCapabilitie createCapabilitie() {
-    return new APIRequestCreateCapabilitie(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetSessions(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestCreateAdAccount createAdAccount() {
-    return new APIRequestCreateAdAccount(this.getPrefixedId().toString(), mContext);
+  public APIRequestDeleteUsers deleteUsers() {
+    return new APIRequestDeleteUsers(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestDeleteCapabilities deleteCapabilities() {
-    return new APIRequestDeleteCapabilities(this.getPrefixedId().toString(), mContext);
+  public APIRequestCreateUser createUser() {
+    return new APIRequestCreateUser(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestDeleteAdAccounts deleteAdAccounts() {
-    return new APIRequestDeleteAdAccounts(this.getPrefixedId().toString(), mContext);
+  public APIRequestDelete delete() {
+    return new APIRequestDelete(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestCreateUser createUser() {
-    return new APIRequestCreateUser(this.getPrefixedId().toString(), mContext);
+  public APIRequestGet get() {
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestDeleteUsers deleteUsers() {
-    return new APIRequestDeleteUsers(this.getPrefixedId().toString(), mContext);
+  public APIRequestUpdate update() {
+    return new APIRequestUpdate(this.getPrefixedId().toString(), context);
   }
 
 
-  public String getFieldId() {
-    return mId;
-  }
-
   public String getFieldAccountId() {
     return mAccountId;
   }
@@ -303,17 +348,25 @@ public class CustomAudience extends APINode {
     return mExcludedCustomAudiences;
   }
 
-  public List<CustomAudience> getFieldIncludedCustomAudiences() {
-    return mIncludedCustomAudiences;
-  }
-
   public AdsPixel getFieldExternalEventSource() {
     if (mExternalEventSource != null) {
-      mExternalEventSource.mContext = getContext();
+      mExternalEventSource.context = getContext();
     }
     return mExternalEventSource;
   }
 
+  public String getFieldId() {
+    return mId;
+  }
+
+  public List<CustomAudience> getFieldIncludedCustomAudiences() {
+    return mIncludedCustomAudiences;
+  }
+
+  public String getFieldLastUsedTime() {
+    return mLastUsedTime;
+  }
+
   public List<String> getFieldLookalikeAudienceIds() {
     return mLookalikeAudienceIds;
   }
@@ -334,6 +387,13 @@ public class CustomAudience extends APINode {
     return mOptOutLink;
   }
 
+  public Business getFieldOwnerBusiness() {
+    if (mOwnerBusiness != null) {
+      mOwnerBusiness.context = getContext();
+    }
+    return mOwnerBusiness;
+  }
+
   public CustomAudiencePermission getFieldPermissionForActions() {
     return mPermissionForActions;
   }
@@ -342,18 +402,22 @@ public class CustomAudience extends APINode {
     return mPixelId;
   }
 
-  public String getFieldRule() {
-    return mRule;
-  }
-
   public Long getFieldRetentionDays() {
     return mRetentionDays;
   }
 
+  public String getFieldRule() {
+    return mRule;
+  }
+
   public String getFieldSubtype() {
     return mSubtype;
   }
 
+  public Long getFieldTimeContentUpdated() {
+    return mTimeContentUpdated;
+  }
+
   public Long getFieldTimeCreated() {
     return mTimeCreated;
   }
@@ -362,703 +426,320 @@ public class CustomAudience extends APINode {
     return mTimeUpdated;
   }
 
-  public Long getFieldTimeContentUpdated() {
-    return mTimeContentUpdated;
-  }
-
 
 
-  public static class APIRequestGet extends APIRequest<CustomAudience> {
+  public static class APIRequestDeleteAdAccounts extends APIRequest<CustomAudienceAdAccount> {
 
-    CustomAudience lastResponse = null;
+    APINodeList<CustomAudienceAdAccount> lastResponse = null;
     @Override
-    public CustomAudience getLastResponse() {
+    public APINodeList<CustomAudienceAdAccount> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
+      "adaccounts",
+      "id",
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "account_id",
-      "approximate_count",
-      "data_source",
-      "delivery_status",
-      "description",
-      "excluded_custom_audiences",
-      "included_custom_audiences",
-      "external_event_source",
-      "lookalike_audience_ids",
-      "lookalike_spec",
-      "name",
-      "operation_status",
-      "opt_out_link",
-      "permission_for_actions",
-      "pixel_id",
-      "rule",
-      "retention_days",
-      "subtype",
-      "time_created",
-      "time_updated",
-      "time_content_updated",
     };
 
     @Override
-    public CustomAudience parseResponse(String response) throws APIException {
-      return CustomAudience.parseResponse(response, getContext(), this).head();
+    public APINodeList<CustomAudienceAdAccount> parseResponse(String response) throws APIException {
+      return CustomAudienceAdAccount.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public CustomAudience execute() throws APIException {
+    public APINodeList<CustomAudienceAdAccount> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public CustomAudience execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<CustomAudienceAdAccount> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGet(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
+    public APIRequestDeleteAdAccounts(String nodeId, APIContext context) {
+      super(context, nodeId, "/adaccounts", "DELETE", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGet setParam(String param, Object value) {
+    @Override
+    public APIRequestDeleteAdAccounts setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGet setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestDeleteAdAccounts setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGet requestAllFields () {
+    public APIRequestDeleteAdAccounts setAdaccounts (List<String> adaccounts) {
+      this.setParam("adaccounts", adaccounts);
+      return this;
+    }
+    public APIRequestDeleteAdAccounts setAdaccounts (String adaccounts) {
+      this.setParam("adaccounts", adaccounts);
+      return this;
+    }
+
+    public APIRequestDeleteAdAccounts setId (String id) {
+      this.setParam("id", id);
+      return this;
+    }
+
+    public APIRequestDeleteAdAccounts requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGet requestAllFields (boolean value) {
+    public APIRequestDeleteAdAccounts requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestFields (List<String> fields) {
+    @Override
+    public APIRequestDeleteAdAccounts requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGet requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestDeleteAdAccounts requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestField (String field) {
+    @Override
+    public APIRequestDeleteAdAccounts requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGet requestField (String field, boolean value) {
+    @Override
+    public APIRequestDeleteAdAccounts requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGet requestIdField () {
-      return this.requestIdField(true);
+  }
+
+  public static class APIRequestGetAdAccounts extends APIRequest<CustomAudienceAdAccount> {
+
+    APINodeList<CustomAudienceAdAccount> lastResponse = null;
+    @Override
+    public APINodeList<CustomAudienceAdAccount> getLastResponse() {
+      return lastResponse;
     }
-    public APIRequestGet requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
+    public static final String[] PARAMS = {
+      "permissions",
+    };
+
+    public static final String[] FIELDS = {
+      "id",
+    };
+
+    @Override
+    public APINodeList<CustomAudienceAdAccount> parseResponse(String response) throws APIException {
+      return CustomAudienceAdAccount.parseResponse(response, getContext(), this);
     }
-    public APIRequestGet requestAccountIdField () {
-      return this.requestAccountIdField(true);
+
+    @Override
+    public APINodeList<CustomAudienceAdAccount> execute() throws APIException {
+      return execute(new HashMap<String, Object>());
     }
-    public APIRequestGet requestAccountIdField (boolean value) {
-      this.requestField("account_id", value);
-      return this;
+
+    @Override
+    public APINodeList<CustomAudienceAdAccount> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
     }
-    public APIRequestGet requestApproximateCountField () {
-      return this.requestApproximateCountField(true);
+
+    public APIRequestGetAdAccounts(String nodeId, APIContext context) {
+      super(context, nodeId, "/adaccounts", "GET", Arrays.asList(PARAMS));
     }
-    public APIRequestGet requestApproximateCountField (boolean value) {
-      this.requestField("approximate_count", value);
+
+    @Override
+    public APIRequestGetAdAccounts setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
-    public APIRequestGet requestDataSourceField () {
-      return this.requestDataSourceField(true);
-    }
-    public APIRequestGet requestDataSourceField (boolean value) {
-      this.requestField("data_source", value);
+
+    @Override
+    public APIRequestGetAdAccounts setParams(Map<String, Object> params) {
+      setParamsInternal(params);
       return this;
     }
-    public APIRequestGet requestDeliveryStatusField () {
-      return this.requestDeliveryStatusField(true);
-    }
-    public APIRequestGet requestDeliveryStatusField (boolean value) {
-      this.requestField("delivery_status", value);
+
+
+    public APIRequestGetAdAccounts setPermissions (String permissions) {
+      this.setParam("permissions", permissions);
       return this;
     }
-    public APIRequestGet requestDescriptionField () {
-      return this.requestDescriptionField(true);
+
+    public APIRequestGetAdAccounts requestAllFields () {
+      return this.requestAllFields(true);
     }
-    public APIRequestGet requestDescriptionField (boolean value) {
-      this.requestField("description", value);
+
+    public APIRequestGetAdAccounts requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGet requestExcludedCustomAudiencesField () {
-      return this.requestExcludedCustomAudiencesField(true);
+
+    @Override
+    public APIRequestGetAdAccounts requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
-    public APIRequestGet requestExcludedCustomAudiencesField (boolean value) {
-      this.requestField("excluded_custom_audiences", value);
-      return this;
-    }
-    public APIRequestGet requestIncludedCustomAudiencesField () {
-      return this.requestIncludedCustomAudiencesField(true);
-    }
-    public APIRequestGet requestIncludedCustomAudiencesField (boolean value) {
-      this.requestField("included_custom_audiences", value);
-      return this;
-    }
-    public APIRequestGet requestExternalEventSourceField () {
-      return this.requestExternalEventSourceField(true);
-    }
-    public APIRequestGet requestExternalEventSourceField (boolean value) {
-      this.requestField("external_event_source", value);
-      return this;
-    }
-    public APIRequestGet requestLookalikeAudienceIdsField () {
-      return this.requestLookalikeAudienceIdsField(true);
-    }
-    public APIRequestGet requestLookalikeAudienceIdsField (boolean value) {
-      this.requestField("lookalike_audience_ids", value);
-      return this;
-    }
-    public APIRequestGet requestLookalikeSpecField () {
-      return this.requestLookalikeSpecField(true);
-    }
-    public APIRequestGet requestLookalikeSpecField (boolean value) {
-      this.requestField("lookalike_spec", value);
-      return this;
-    }
-    public APIRequestGet requestNameField () {
-      return this.requestNameField(true);
-    }
-    public APIRequestGet requestNameField (boolean value) {
-      this.requestField("name", value);
-      return this;
-    }
-    public APIRequestGet requestOperationStatusField () {
-      return this.requestOperationStatusField(true);
-    }
-    public APIRequestGet requestOperationStatusField (boolean value) {
-      this.requestField("operation_status", value);
-      return this;
-    }
-    public APIRequestGet requestOptOutLinkField () {
-      return this.requestOptOutLinkField(true);
-    }
-    public APIRequestGet requestOptOutLinkField (boolean value) {
-      this.requestField("opt_out_link", value);
-      return this;
-    }
-    public APIRequestGet requestPermissionForActionsField () {
-      return this.requestPermissionForActionsField(true);
-    }
-    public APIRequestGet requestPermissionForActionsField (boolean value) {
-      this.requestField("permission_for_actions", value);
-      return this;
-    }
-    public APIRequestGet requestPixelIdField () {
-      return this.requestPixelIdField(true);
-    }
-    public APIRequestGet requestPixelIdField (boolean value) {
-      this.requestField("pixel_id", value);
-      return this;
-    }
-    public APIRequestGet requestRuleField () {
-      return this.requestRuleField(true);
-    }
-    public APIRequestGet requestRuleField (boolean value) {
-      this.requestField("rule", value);
-      return this;
-    }
-    public APIRequestGet requestRetentionDaysField () {
-      return this.requestRetentionDaysField(true);
-    }
-    public APIRequestGet requestRetentionDaysField (boolean value) {
-      this.requestField("retention_days", value);
-      return this;
-    }
-    public APIRequestGet requestSubtypeField () {
-      return this.requestSubtypeField(true);
-    }
-    public APIRequestGet requestSubtypeField (boolean value) {
-      this.requestField("subtype", value);
-      return this;
-    }
-    public APIRequestGet requestTimeCreatedField () {
-      return this.requestTimeCreatedField(true);
-    }
-    public APIRequestGet requestTimeCreatedField (boolean value) {
-      this.requestField("time_created", value);
-      return this;
-    }
-    public APIRequestGet requestTimeUpdatedField () {
-      return this.requestTimeUpdatedField(true);
-    }
-    public APIRequestGet requestTimeUpdatedField (boolean value) {
-      this.requestField("time_updated", value);
-      return this;
-    }
-    public APIRequestGet requestTimeContentUpdatedField () {
-      return this.requestTimeContentUpdatedField(true);
-    }
-    public APIRequestGet requestTimeContentUpdatedField (boolean value) {
-      this.requestField("time_content_updated", value);
-      return this;
-    }
-
-  }
-
-  public static class APIRequestUpdate extends APIRequest<APINode> {
-
-    APINode lastResponse = null;
-    @Override
-    public APINode getLastResponse() {
-      return lastResponse;
-    }
-    public static final String[] PARAMS = {
-      "description",
-      "name",
-      "opt_out_link",
-      "parent_audience_id",
-      "lookalike_spec",
-      "retention_days",
-      "rule",
-      "inclusions",
-      "exclusions",
-    };
-
-    public static final String[] FIELDS = {
-    };
-
-    @Override
-    public APINode parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this).head();
-    }
-
-    @Override
-    public APINode execute() throws APIException {
-      return execute(new HashMap<String, Object>());
-    }
-
-    @Override
-    public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
-    }
-
-    public APIRequestUpdate(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "POST", Arrays.asList(PARAMS));
-    }
-
-    public APIRequestUpdate setParam(String param, Object value) {
-      setParamInternal(param, value);
-      return this;
-    }
-
-    public APIRequestUpdate setParams(Map<String, Object> params) {
-      setParamsInternal(params);
-      return this;
-    }
-
-
-    public APIRequestUpdate setDescription (String description) {
-      this.setParam("description", description);
-      return this;
-    }
-
-
-    public APIRequestUpdate setName (String name) {
-      this.setParam("name", name);
-      return this;
-    }
-
-
-    public APIRequestUpdate setOptOutLink (String optOutLink) {
-      this.setParam("opt_out_link", optOutLink);
-      return this;
-    }
-
-
-    public APIRequestUpdate setParentAudienceId (Long parentAudienceId) {
-      this.setParam("parent_audience_id", parentAudienceId);
-      return this;
-    }
-
-    public APIRequestUpdate setParentAudienceId (String parentAudienceId) {
-      this.setParam("parent_audience_id", parentAudienceId);
-      return this;
-    }
-
-    public APIRequestUpdate setLookalikeSpec (String lookalikeSpec) {
-      this.setParam("lookalike_spec", lookalikeSpec);
-      return this;
-    }
-
-
-    public APIRequestUpdate setRetentionDays (Long retentionDays) {
-      this.setParam("retention_days", retentionDays);
-      return this;
-    }
-
-    public APIRequestUpdate setRetentionDays (String retentionDays) {
-      this.setParam("retention_days", retentionDays);
-      return this;
-    }
-
-    public APIRequestUpdate setRule (String rule) {
-      this.setParam("rule", rule);
-      return this;
-    }
-
-
-    public APIRequestUpdate setInclusions (List<Object> inclusions) {
-      this.setParam("inclusions", inclusions);
-      return this;
-    }
-
-    public APIRequestUpdate setInclusions (String inclusions) {
-      this.setParam("inclusions", inclusions);
-      return this;
-    }
-
-    public APIRequestUpdate setExclusions (List<Object> exclusions) {
-      this.setParam("exclusions", exclusions);
-      return this;
-    }
-
-    public APIRequestUpdate setExclusions (String exclusions) {
-      this.setParam("exclusions", exclusions);
-      return this;
-    }
-
-    public APIRequestUpdate requestAllFields () {
-      return this.requestAllFields(true);
-    }
-
-    public APIRequestUpdate requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
-      return this;
-    }
-
-    public APIRequestUpdate requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
-    }
-
-    public APIRequestUpdate requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
-      return this;
-    }
-
-    public APIRequestUpdate requestField (String field) {
-      this.requestField(field, true);
-      return this;
-    }
-
-    public APIRequestUpdate requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
-      return this;
-    }
-
-
-  }
-
-  public static class APIRequestDelete extends APIRequest<APINode> {
-
-    APINode lastResponse = null;
-    @Override
-    public APINode getLastResponse() {
-      return lastResponse;
-    }
-    public static final String[] PARAMS = {
-      "id",
-    };
-
-    public static final String[] FIELDS = {
-    };
-
-    @Override
-    public APINode parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this).head();
-    }
-
-    @Override
-    public APINode execute() throws APIException {
-      return execute(new HashMap<String, Object>());
-    }
-
-    @Override
-    public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
-    }
-
-    public APIRequestDelete(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "DELETE", Arrays.asList(PARAMS));
-    }
-
-    public APIRequestDelete setParam(String param, Object value) {
-      setParamInternal(param, value);
-      return this;
-    }
-
-    public APIRequestDelete setParams(Map<String, Object> params) {
-      setParamsInternal(params);
-      return this;
-    }
-
-
-    public APIRequestDelete setId (Long id) {
-      this.setParam("id", id);
-      return this;
-    }
-
-    public APIRequestDelete setId (String id) {
-      this.setParam("id", id);
-      return this;
-    }
-
-    public APIRequestDelete requestAllFields () {
-      return this.requestAllFields(true);
-    }
-
-    public APIRequestDelete requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
-      return this;
-    }
-
-    public APIRequestDelete requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
-    }
-
-    public APIRequestDelete requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+
+    @Override
+    public APIRequestGetAdAccounts requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
 
-    public APIRequestDelete requestField (String field) {
+    @Override
+    public APIRequestGetAdAccounts requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestDelete requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetAdAccounts requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
+    public APIRequestGetAdAccounts requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGetAdAccounts requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
   }
 
-  public static class APIRequestGetCapabilities extends APIRequest<CustomAudienceCapabilities> {
+  public static class APIRequestCreateAdAccount extends APIRequest<CustomAudienceAdAccount> {
 
-    APINodeList<CustomAudienceCapabilities> lastResponse = null;
+    CustomAudienceAdAccount lastResponse = null;
     @Override
-    public APINodeList<CustomAudienceCapabilities> getLastResponse() {
+    public CustomAudienceAdAccount getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
+      "adaccounts",
+      "id",
+      "permissions",
+      "replace",
     };
 
     public static final String[] FIELDS = {
-      "capabilities",
     };
 
     @Override
-    public APINodeList<CustomAudienceCapabilities> parseResponse(String response) throws APIException {
-      return CustomAudienceCapabilities.parseResponse(response, getContext(), this);
+    public CustomAudienceAdAccount parseResponse(String response) throws APIException {
+      return CustomAudienceAdAccount.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public APINodeList<CustomAudienceCapabilities> execute() throws APIException {
+    public CustomAudienceAdAccount execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<CustomAudienceCapabilities> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public CustomAudienceAdAccount execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetCapabilities(String nodeId, APIContext context) {
-      super(context, nodeId, "/capabilities", "GET", Arrays.asList(PARAMS));
+    public APIRequestCreateAdAccount(String nodeId, APIContext context) {
+      super(context, nodeId, "/adaccounts", "POST", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetCapabilities setParam(String param, Object value) {
+    @Override
+    public APIRequestCreateAdAccount setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetCapabilities setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestCreateAdAccount setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetCapabilities requestAllFields () {
-      return this.requestAllFields(true);
-    }
-
-    public APIRequestGetCapabilities requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
-      return this;
-    }
-
-    public APIRequestGetCapabilities requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
-    }
-
-    public APIRequestGetCapabilities requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+    public APIRequestCreateAdAccount setAdaccounts (List<String> adaccounts) {
+      this.setParam("adaccounts", adaccounts);
       return this;
     }
-
-    public APIRequestGetCapabilities requestField (String field) {
-      this.requestField(field, true);
+    public APIRequestCreateAdAccount setAdaccounts (String adaccounts) {
+      this.setParam("adaccounts", adaccounts);
       return this;
     }
 
-    public APIRequestGetCapabilities requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
+    public APIRequestCreateAdAccount setId (String id) {
+      this.setParam("id", id);
       return this;
     }
 
-    public APIRequestGetCapabilities requestCapabilitiesField () {
-      return this.requestCapabilitiesField(true);
-    }
-    public APIRequestGetCapabilities requestCapabilitiesField (boolean value) {
-      this.requestField("capabilities", value);
+    public APIRequestCreateAdAccount setPermissions (String permissions) {
+      this.setParam("permissions", permissions);
       return this;
     }
 
-  }
-
-  public static class APIRequestGetPrefills extends APIRequest<CustomAudiencePrefillState> {
-
-    APINodeList<CustomAudiencePrefillState> lastResponse = null;
-    @Override
-    public APINodeList<CustomAudiencePrefillState> getLastResponse() {
-      return lastResponse;
-    }
-    public static final String[] PARAMS = {
-    };
-
-    public static final String[] FIELDS = {
-      "status",
-      "description",
-      "num_added",
-    };
-
-    @Override
-    public APINodeList<CustomAudiencePrefillState> parseResponse(String response) throws APIException {
-      return CustomAudiencePrefillState.parseResponse(response, getContext(), this);
-    }
-
-    @Override
-    public APINodeList<CustomAudiencePrefillState> execute() throws APIException {
-      return execute(new HashMap<String, Object>());
-    }
-
-    @Override
-    public APINodeList<CustomAudiencePrefillState> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
-    }
-
-    public APIRequestGetPrefills(String nodeId, APIContext context) {
-      super(context, nodeId, "/prefills", "GET", Arrays.asList(PARAMS));
-    }
-
-    public APIRequestGetPrefills setParam(String param, Object value) {
-      setParamInternal(param, value);
+    public APIRequestCreateAdAccount setReplace (Boolean replace) {
+      this.setParam("replace", replace);
       return this;
     }
-
-    public APIRequestGetPrefills setParams(Map<String, Object> params) {
-      setParamsInternal(params);
+    public APIRequestCreateAdAccount setReplace (String replace) {
+      this.setParam("replace", replace);
       return this;
     }
 
-
-    public APIRequestGetPrefills requestAllFields () {
+    public APIRequestCreateAdAccount requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetPrefills requestAllFields (boolean value) {
+    public APIRequestCreateAdAccount requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetPrefills requestFields (List<String> fields) {
+    @Override
+    public APIRequestCreateAdAccount requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetPrefills requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestCreateAdAccount requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetPrefills requestField (String field) {
+    @Override
+    public APIRequestCreateAdAccount requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetPrefills requestField (String field, boolean value) {
+    @Override
+    public APIRequestCreateAdAccount requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetPrefills requestStatusField () {
-      return this.requestStatusField(true);
-    }
-    public APIRequestGetPrefills requestStatusField (boolean value) {
-      this.requestField("status", value);
-      return this;
-    }
-    public APIRequestGetPrefills requestDescriptionField () {
-      return this.requestDescriptionField(true);
-    }
-    public APIRequestGetPrefills requestDescriptionField (boolean value) {
-      this.requestField("description", value);
-      return this;
-    }
-    public APIRequestGetPrefills requestNumAddedField () {
-      return this.requestNumAddedField(true);
-    }
-    public APIRequestGetPrefills requestNumAddedField (boolean value) {
-      this.requestField("num_added", value);
-      return this;
-    }
-
   }
 
   public static class APIRequestGetAds extends APIRequest<Ad> {
@@ -1073,26 +754,28 @@ public class CustomAudience extends APINode {
     };
 
     public static final String[] FIELDS = {
-      "id",
       "account_id",
-      "adset",
-      "campaign",
+      "ad_review_feedback",
       "adlabels",
+      "adset",
       "adset_id",
       "bid_amount",
       "bid_info",
       "bid_type",
+      "campaign",
+      "campaign_id",
       "configured_status",
       "conversion_specs",
       "created_time",
       "creative",
       "effective_status",
+      "id",
       "last_updated_by_app_id",
       "name",
+      "recommendations",
+      "status",
       "tracking_specs",
       "updated_time",
-      "campaign_id",
-      "ad_review_feedback",
     };
 
     @Override
@@ -1107,7 +790,7 @@ public class CustomAudience extends APINode {
 
     @Override
     public APINodeList<Ad> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -1115,11 +798,13 @@ public class CustomAudience extends APINode {
       super(context, nodeId, "/ads", "GET", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestGetAds setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestGetAds setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
@@ -1130,7 +815,6 @@ public class CustomAudience extends APINode {
       this.setParam("effective_status", effectiveStatus);
       return this;
     }
-
     public APIRequestGetAds setEffectiveStatus (String effectiveStatus) {
       this.setParam("effective_status", effectiveStatus);
       return this;
@@ -1147,10 +831,12 @@ public class CustomAudience extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetAds requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestGetAds requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -1158,23 +844,18 @@ public class CustomAudience extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetAds requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGetAds requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetAds requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGetAds requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
-    }
     public APIRequestGetAds requestAccountIdField () {
       return this.requestAccountIdField(true);
     }
@@ -1182,18 +863,11 @@ public class CustomAudience extends APINode {
       this.requestField("account_id", value);
       return this;
     }
-    public APIRequestGetAds requestAdsetField () {
-      return this.requestAdsetField(true);
-    }
-    public APIRequestGetAds requestAdsetField (boolean value) {
-      this.requestField("adset", value);
-      return this;
-    }
-    public APIRequestGetAds requestCampaignField () {
-      return this.requestCampaignField(true);
+    public APIRequestGetAds requestAdReviewFeedbackField () {
+      return this.requestAdReviewFeedbackField(true);
     }
-    public APIRequestGetAds requestCampaignField (boolean value) {
-      this.requestField("campaign", value);
+    public APIRequestGetAds requestAdReviewFeedbackField (boolean value) {
+      this.requestField("ad_review_feedback", value);
       return this;
     }
     public APIRequestGetAds requestAdlabelsField () {
@@ -1203,6 +877,13 @@ public class CustomAudience extends APINode {
       this.requestField("adlabels", value);
       return this;
     }
+    public APIRequestGetAds requestAdsetField () {
+      return this.requestAdsetField(true);
+    }
+    public APIRequestGetAds requestAdsetField (boolean value) {
+      this.requestField("adset", value);
+      return this;
+    }
     public APIRequestGetAds requestAdsetIdField () {
       return this.requestAdsetIdField(true);
     }
@@ -1231,6 +912,20 @@ public class CustomAudience extends APINode {
       this.requestField("bid_type", value);
       return this;
     }
+    public APIRequestGetAds requestCampaignField () {
+      return this.requestCampaignField(true);
+    }
+    public APIRequestGetAds requestCampaignField (boolean value) {
+      this.requestField("campaign", value);
+      return this;
+    }
+    public APIRequestGetAds requestCampaignIdField () {
+      return this.requestCampaignIdField(true);
+    }
+    public APIRequestGetAds requestCampaignIdField (boolean value) {
+      this.requestField("campaign_id", value);
+      return this;
+    }
     public APIRequestGetAds requestConfiguredStatusField () {
       return this.requestConfiguredStatusField(true);
     }
@@ -1266,6 +961,13 @@ public class CustomAudience extends APINode {
       this.requestField("effective_status", value);
       return this;
     }
+    public APIRequestGetAds requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGetAds requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
     public APIRequestGetAds requestLastUpdatedByAppIdField () {
       return this.requestLastUpdatedByAppIdField(true);
     }
@@ -1280,6 +982,20 @@ public class CustomAudience extends APINode {
       this.requestField("name", value);
       return this;
     }
+    public APIRequestGetAds requestRecommendationsField () {
+      return this.requestRecommendationsField(true);
+    }
+    public APIRequestGetAds requestRecommendationsField (boolean value) {
+      this.requestField("recommendations", value);
+      return this;
+    }
+    public APIRequestGetAds requestStatusField () {
+      return this.requestStatusField(true);
+    }
+    public APIRequestGetAds requestStatusField (boolean value) {
+      this.requestField("status", value);
+      return this;
+    }
     public APIRequestGetAds requestTrackingSpecsField () {
       return this.requestTrackingSpecsField(true);
     }
@@ -1294,108 +1010,114 @@ public class CustomAudience extends APINode {
       this.requestField("updated_time", value);
       return this;
     }
-    public APIRequestGetAds requestCampaignIdField () {
-      return this.requestCampaignIdField(true);
-    }
-    public APIRequestGetAds requestCampaignIdField (boolean value) {
-      this.requestField("campaign_id", value);
-      return this;
-    }
-    public APIRequestGetAds requestAdReviewFeedbackField () {
-      return this.requestAdReviewFeedbackField(true);
-    }
-    public APIRequestGetAds requestAdReviewFeedbackField (boolean value) {
-      this.requestField("ad_review_feedback", value);
-      return this;
-    }
-
   }
 
-  public static class APIRequestGetAdAccounts extends APIRequest<CustomAudienceAdAccount> {
+  public static class APIRequestGetPrefills extends APIRequest<CustomAudiencePrefillState> {
 
-    APINodeList<CustomAudienceAdAccount> lastResponse = null;
+    APINodeList<CustomAudiencePrefillState> lastResponse = null;
     @Override
-    public APINodeList<CustomAudienceAdAccount> getLastResponse() {
+    public APINodeList<CustomAudiencePrefillState> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
     };
 
     public static final String[] FIELDS = {
-      "id",
+      "description",
+      "num_added",
+      "status",
     };
 
     @Override
-    public APINodeList<CustomAudienceAdAccount> parseResponse(String response) throws APIException {
-      return CustomAudienceAdAccount.parseResponse(response, getContext(), this);
+    public APINodeList<CustomAudiencePrefillState> parseResponse(String response) throws APIException {
+      return CustomAudiencePrefillState.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINodeList<CustomAudienceAdAccount> execute() throws APIException {
+    public APINodeList<CustomAudiencePrefillState> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<CustomAudienceAdAccount> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<CustomAudiencePrefillState> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetAdAccounts(String nodeId, APIContext context) {
-      super(context, nodeId, "/adaccounts", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetPrefills(String nodeId, APIContext context) {
+      super(context, nodeId, "/prefills", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetAdAccounts setParam(String param, Object value) {
+    @Override
+    public APIRequestGetPrefills setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetAdAccounts setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetPrefills setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetAdAccounts requestAllFields () {
+    public APIRequestGetPrefills requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetAdAccounts requestAllFields (boolean value) {
+    public APIRequestGetPrefills requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetAdAccounts requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetPrefills requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetAdAccounts requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetPrefills requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetAdAccounts requestField (String field) {
+    @Override
+    public APIRequestGetPrefills requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetAdAccounts requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetPrefills requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetAdAccounts requestIdField () {
-      return this.requestIdField(true);
+    public APIRequestGetPrefills requestDescriptionField () {
+      return this.requestDescriptionField(true);
     }
-    public APIRequestGetAdAccounts requestIdField (boolean value) {
-      this.requestField("id", value);
+    public APIRequestGetPrefills requestDescriptionField (boolean value) {
+      this.requestField("description", value);
+      return this;
+    }
+    public APIRequestGetPrefills requestNumAddedField () {
+      return this.requestNumAddedField(true);
+    }
+    public APIRequestGetPrefills requestNumAddedField (boolean value) {
+      this.requestField("num_added", value);
+      return this;
+    }
+    public APIRequestGetPrefills requestStatusField () {
+      return this.requestStatusField(true);
+    }
+    public APIRequestGetPrefills requestStatusField (boolean value) {
+      this.requestField("status", value);
       return this;
     }
-
   }
 
   public static class APIRequestGetSessions extends APIRequest<CustomAudienceSession> {
@@ -1410,14 +1132,14 @@ public class CustomAudience extends APINode {
     };
 
     public static final String[] FIELDS = {
-      "session_id",
-      "start_time",
       "end_time",
-      "num_received",
-      "num_matched",
       "num_invalid_entries",
+      "num_matched",
+      "num_received",
       "progress",
+      "session_id",
       "stage",
+      "start_time",
     };
 
     @Override
@@ -1432,7 +1154,7 @@ public class CustomAudience extends APINode {
 
     @Override
     public APINodeList<CustomAudienceSession> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -1440,11 +1162,13 @@ public class CustomAudience extends APINode {
       super(context, nodeId, "/sessions", "GET", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestGetSessions setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestGetSessions setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
@@ -1455,7 +1179,6 @@ public class CustomAudience extends APINode {
       this.setParam("session_id", sessionId);
       return this;
     }
-
     public APIRequestGetSessions setSessionId (String sessionId) {
       this.setParam("session_id", sessionId);
       return this;
@@ -1472,10 +1195,12 @@ public class CustomAudience extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetSessions requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestGetSessions requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -1483,30 +1208,18 @@ public class CustomAudience extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetSessions requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGetSessions requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetSessions requestSessionIdField () {
-      return this.requestSessionIdField(true);
-    }
-    public APIRequestGetSessions requestSessionIdField (boolean value) {
-      this.requestField("session_id", value);
-      return this;
-    }
-    public APIRequestGetSessions requestStartTimeField () {
-      return this.requestStartTimeField(true);
-    }
-    public APIRequestGetSessions requestStartTimeField (boolean value) {
-      this.requestField("start_time", value);
-      return this;
-    }
     public APIRequestGetSessions requestEndTimeField () {
       return this.requestEndTimeField(true);
     }
@@ -1514,11 +1227,11 @@ public class CustomAudience extends APINode {
       this.requestField("end_time", value);
       return this;
     }
-    public APIRequestGetSessions requestNumReceivedField () {
-      return this.requestNumReceivedField(true);
+    public APIRequestGetSessions requestNumInvalidEntriesField () {
+      return this.requestNumInvalidEntriesField(true);
     }
-    public APIRequestGetSessions requestNumReceivedField (boolean value) {
-      this.requestField("num_received", value);
+    public APIRequestGetSessions requestNumInvalidEntriesField (boolean value) {
+      this.requestField("num_invalid_entries", value);
       return this;
     }
     public APIRequestGetSessions requestNumMatchedField () {
@@ -1528,11 +1241,11 @@ public class CustomAudience extends APINode {
       this.requestField("num_matched", value);
       return this;
     }
-    public APIRequestGetSessions requestNumInvalidEntriesField () {
-      return this.requestNumInvalidEntriesField(true);
+    public APIRequestGetSessions requestNumReceivedField () {
+      return this.requestNumReceivedField(true);
     }
-    public APIRequestGetSessions requestNumInvalidEntriesField (boolean value) {
-      this.requestField("num_invalid_entries", value);
+    public APIRequestGetSessions requestNumReceivedField (boolean value) {
+      this.requestField("num_received", value);
       return this;
     }
     public APIRequestGetSessions requestProgressField () {
@@ -1542,6 +1255,13 @@ public class CustomAudience extends APINode {
       this.requestField("progress", value);
       return this;
     }
+    public APIRequestGetSessions requestSessionIdField () {
+      return this.requestSessionIdField(true);
+    }
+    public APIRequestGetSessions requestSessionIdField (boolean value) {
+      this.requestField("session_id", value);
+      return this;
+    }
     public APIRequestGetSessions requestStageField () {
       return this.requestStageField(true);
     }
@@ -1549,405 +1269,606 @@ public class CustomAudience extends APINode {
       this.requestField("stage", value);
       return this;
     }
-
+    public APIRequestGetSessions requestStartTimeField () {
+      return this.requestStartTimeField(true);
+    }
+    public APIRequestGetSessions requestStartTimeField (boolean value) {
+      this.requestField("start_time", value);
+      return this;
+    }
   }
 
-  public static class APIRequestCreateCapabilitie extends APIRequest<CustomAudienceCapabilities> {
+  public static class APIRequestDeleteUsers extends APIRequest<APINode> {
 
-    CustomAudienceCapabilities lastResponse = null;
+    APINodeList<APINode> lastResponse = null;
     @Override
-    public CustomAudienceCapabilities getLastResponse() {
+    public APINodeList<APINode> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
       "id",
-      "accounts_capabilities",
+      "payload",
+      "session",
     };
 
     public static final String[] FIELDS = {
     };
 
     @Override
-    public CustomAudienceCapabilities parseResponse(String response) throws APIException {
-      return CustomAudienceCapabilities.parseResponse(response, getContext(), this).head();
+    public APINodeList<APINode> parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public CustomAudienceCapabilities execute() throws APIException {
+    public APINodeList<APINode> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public CustomAudienceCapabilities execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestCreateCapabilitie(String nodeId, APIContext context) {
-      super(context, nodeId, "/capabilities", "POST", Arrays.asList(PARAMS));
+    public APIRequestDeleteUsers(String nodeId, APIContext context) {
+      super(context, nodeId, "/users", "DELETE", Arrays.asList(PARAMS));
     }
 
-    public APIRequestCreateCapabilitie setParam(String param, Object value) {
+    @Override
+    public APIRequestDeleteUsers setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestCreateCapabilitie setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestDeleteUsers setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestCreateCapabilitie setId (String id) {
+    public APIRequestDeleteUsers setId (String id) {
       this.setParam("id", id);
       return this;
     }
 
-
-    public APIRequestCreateCapabilitie setAccountsCapabilities (String accountsCapabilities) {
-      this.setParam("accounts_capabilities", accountsCapabilities);
+    public APIRequestDeleteUsers setPayload (Object payload) {
+      this.setParam("payload", payload);
+      return this;
+    }
+    public APIRequestDeleteUsers setPayload (String payload) {
+      this.setParam("payload", payload);
       return this;
     }
 
+    public APIRequestDeleteUsers setSession (Object session) {
+      this.setParam("session", session);
+      return this;
+    }
+    public APIRequestDeleteUsers setSession (String session) {
+      this.setParam("session", session);
+      return this;
+    }
 
-    public APIRequestCreateCapabilitie requestAllFields () {
+    public APIRequestDeleteUsers requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestCreateCapabilitie requestAllFields (boolean value) {
+    public APIRequestDeleteUsers requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestCreateCapabilitie requestFields (List<String> fields) {
+    @Override
+    public APIRequestDeleteUsers requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestCreateCapabilitie requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestDeleteUsers requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestCreateCapabilitie requestField (String field) {
+    @Override
+    public APIRequestDeleteUsers requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestCreateCapabilitie requestField (String field, boolean value) {
+    @Override
+    public APIRequestDeleteUsers requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
+  }
+
+  public static class APIRequestCreateUser extends APIRequest<APINode> {
+
+    APINode lastResponse = null;
+    @Override
+    public APINode getLastResponse() {
+      return lastResponse;
+    }
+    public static final String[] PARAMS = {
+      "id",
+      "payload",
+      "session",
+    };
+
+    public static final String[] FIELDS = {
+    };
+
+    @Override
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
+    }
+
+    @Override
+    public APINode execute() throws APIException {
+      return execute(new HashMap<String, Object>());
+    }
+
+    @Override
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
+    }
+
+    public APIRequestCreateUser(String nodeId, APIContext context) {
+      super(context, nodeId, "/users", "POST", Arrays.asList(PARAMS));
+    }
+
+    @Override
+    public APIRequestCreateUser setParam(String param, Object value) {
+      setParamInternal(param, value);
+      return this;
+    }
+
+    @Override
+    public APIRequestCreateUser setParams(Map<String, Object> params) {
+      setParamsInternal(params);
+      return this;
+    }
+
+
+    public APIRequestCreateUser setId (String id) {
+      this.setParam("id", id);
+      return this;
+    }
+
+    public APIRequestCreateUser setPayload (Object payload) {
+      this.setParam("payload", payload);
+      return this;
+    }
+    public APIRequestCreateUser setPayload (String payload) {
+      this.setParam("payload", payload);
+      return this;
+    }
+
+    public APIRequestCreateUser setSession (Object session) {
+      this.setParam("session", session);
+      return this;
+    }
+    public APIRequestCreateUser setSession (String session) {
+      this.setParam("session", session);
+      return this;
+    }
+
+    public APIRequestCreateUser requestAllFields () {
+      return this.requestAllFields(true);
+    }
+
+    public APIRequestCreateUser requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestCreateUser requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
+    }
+
+    @Override
+    public APIRequestCreateUser requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestCreateUser requestField (String field) {
+      this.requestField(field, true);
+      return this;
+    }
+
+    @Override
+    public APIRequestCreateUser requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
+      return this;
+    }
 
   }
 
-  public static class APIRequestCreateAdAccount extends APIRequest<CustomAudienceAdAccount> {
+  public static class APIRequestDelete extends APIRequest<APINode> {
 
-    CustomAudienceAdAccount lastResponse = null;
+    APINode lastResponse = null;
     @Override
-    public CustomAudienceAdAccount getLastResponse() {
+    public APINode getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
       "id",
-      "adaccounts",
-      "replace",
     };
 
     public static final String[] FIELDS = {
     };
 
     @Override
-    public CustomAudienceAdAccount parseResponse(String response) throws APIException {
-      return CustomAudienceAdAccount.parseResponse(response, getContext(), this).head();
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public CustomAudienceAdAccount execute() throws APIException {
+    public APINode execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public CustomAudienceAdAccount execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestCreateAdAccount(String nodeId, APIContext context) {
-      super(context, nodeId, "/adaccounts", "POST", Arrays.asList(PARAMS));
+    public APIRequestDelete(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "DELETE", Arrays.asList(PARAMS));
     }
 
-    public APIRequestCreateAdAccount setParam(String param, Object value) {
+    @Override
+    public APIRequestDelete setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestCreateAdAccount setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestDelete setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestCreateAdAccount setId (String id) {
+    public APIRequestDelete setId (Long id) {
       this.setParam("id", id);
       return this;
     }
-
-
-    public APIRequestCreateAdAccount setAdaccounts (List<String> adaccounts) {
-      this.setParam("adaccounts", adaccounts);
-      return this;
-    }
-
-    public APIRequestCreateAdAccount setAdaccounts (String adaccounts) {
-      this.setParam("adaccounts", adaccounts);
-      return this;
-    }
-
-    public APIRequestCreateAdAccount setReplace (Boolean replace) {
-      this.setParam("replace", replace);
-      return this;
-    }
-
-    public APIRequestCreateAdAccount setReplace (String replace) {
-      this.setParam("replace", replace);
+    public APIRequestDelete setId (String id) {
+      this.setParam("id", id);
       return this;
     }
 
-    public APIRequestCreateAdAccount requestAllFields () {
+    public APIRequestDelete requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestCreateAdAccount requestAllFields (boolean value) {
+    public APIRequestDelete requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestCreateAdAccount requestFields (List<String> fields) {
+    @Override
+    public APIRequestDelete requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestCreateAdAccount requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestDelete requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestCreateAdAccount requestField (String field) {
+    @Override
+    public APIRequestDelete requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestCreateAdAccount requestField (String field, boolean value) {
+    @Override
+    public APIRequestDelete requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
   }
 
-  public static class APIRequestDeleteCapabilities extends APIRequest<CustomAudienceCapabilities> {
+  public static class APIRequestGet extends APIRequest<CustomAudience> {
 
-    APINodeList<CustomAudienceCapabilities> lastResponse = null;
+    CustomAudience lastResponse = null;
     @Override
-    public APINodeList<CustomAudienceCapabilities> getLastResponse() {
+    public CustomAudience getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "id",
-      "adaccounts",
     };
 
     public static final String[] FIELDS = {
+      "account_id",
+      "approximate_count",
+      "data_source",
+      "delivery_status",
+      "description",
+      "excluded_custom_audiences",
+      "external_event_source",
+      "id",
+      "included_custom_audiences",
+      "last_used_time",
+      "lookalike_audience_ids",
+      "lookalike_spec",
+      "name",
+      "operation_status",
+      "opt_out_link",
+      "owner_business",
+      "permission_for_actions",
+      "pixel_id",
+      "retention_days",
+      "rule",
+      "subtype",
+      "time_content_updated",
+      "time_created",
+      "time_updated",
     };
 
     @Override
-    public APINodeList<CustomAudienceCapabilities> parseResponse(String response) throws APIException {
-      return CustomAudienceCapabilities.parseResponse(response, getContext(), this);
+    public CustomAudience parseResponse(String response) throws APIException {
+      return CustomAudience.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public APINodeList<CustomAudienceCapabilities> execute() throws APIException {
+    public CustomAudience execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<CustomAudienceCapabilities> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public CustomAudience execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestDeleteCapabilities(String nodeId, APIContext context) {
-      super(context, nodeId, "/capabilities", "DELETE", Arrays.asList(PARAMS));
+    public APIRequestGet(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestDeleteCapabilities setParam(String param, Object value) {
+    @Override
+    public APIRequestGet setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestDeleteCapabilities setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGet setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestDeleteCapabilities setId (String id) {
-      this.setParam("id", id);
-      return this;
-    }
-
-
-    public APIRequestDeleteCapabilities setAdaccounts (List<String> adaccounts) {
-      this.setParam("adaccounts", adaccounts);
-      return this;
-    }
-
-    public APIRequestDeleteCapabilities setAdaccounts (String adaccounts) {
-      this.setParam("adaccounts", adaccounts);
-      return this;
-    }
-
-    public APIRequestDeleteCapabilities requestAllFields () {
+    public APIRequestGet requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestDeleteCapabilities requestAllFields (boolean value) {
+    public APIRequestGet requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestDeleteCapabilities requestFields (List<String> fields) {
+    @Override
+    public APIRequestGet requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestDeleteCapabilities requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGet requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestDeleteCapabilities requestField (String field) {
+    @Override
+    public APIRequestGet requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestDeleteCapabilities requestField (String field, boolean value) {
+    @Override
+    public APIRequestGet requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
-  }
-
-  public static class APIRequestDeleteAdAccounts extends APIRequest<CustomAudienceAdAccount> {
-
-    APINodeList<CustomAudienceAdAccount> lastResponse = null;
-    @Override
-    public APINodeList<CustomAudienceAdAccount> getLastResponse() {
-      return lastResponse;
+    public APIRequestGet requestAccountIdField () {
+      return this.requestAccountIdField(true);
     }
-    public static final String[] PARAMS = {
-      "id",
-      "adaccounts",
-    };
-
-    public static final String[] FIELDS = {
-    };
-
-    @Override
-    public APINodeList<CustomAudienceAdAccount> parseResponse(String response) throws APIException {
-      return CustomAudienceAdAccount.parseResponse(response, getContext(), this);
+    public APIRequestGet requestAccountIdField (boolean value) {
+      this.requestField("account_id", value);
+      return this;
     }
-
-    @Override
-    public APINodeList<CustomAudienceAdAccount> execute() throws APIException {
-      return execute(new HashMap<String, Object>());
+    public APIRequestGet requestApproximateCountField () {
+      return this.requestApproximateCountField(true);
     }
-
-    @Override
-    public APINodeList<CustomAudienceAdAccount> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
+    public APIRequestGet requestApproximateCountField (boolean value) {
+      this.requestField("approximate_count", value);
+      return this;
     }
-
-    public APIRequestDeleteAdAccounts(String nodeId, APIContext context) {
-      super(context, nodeId, "/adaccounts", "DELETE", Arrays.asList(PARAMS));
+    public APIRequestGet requestDataSourceField () {
+      return this.requestDataSourceField(true);
     }
-
-    public APIRequestDeleteAdAccounts setParam(String param, Object value) {
-      setParamInternal(param, value);
+    public APIRequestGet requestDataSourceField (boolean value) {
+      this.requestField("data_source", value);
+      return this;
+    }
+    public APIRequestGet requestDeliveryStatusField () {
+      return this.requestDeliveryStatusField(true);
+    }
+    public APIRequestGet requestDeliveryStatusField (boolean value) {
+      this.requestField("delivery_status", value);
+      return this;
+    }
+    public APIRequestGet requestDescriptionField () {
+      return this.requestDescriptionField(true);
+    }
+    public APIRequestGet requestDescriptionField (boolean value) {
+      this.requestField("description", value);
+      return this;
+    }
+    public APIRequestGet requestExcludedCustomAudiencesField () {
+      return this.requestExcludedCustomAudiencesField(true);
+    }
+    public APIRequestGet requestExcludedCustomAudiencesField (boolean value) {
+      this.requestField("excluded_custom_audiences", value);
+      return this;
+    }
+    public APIRequestGet requestExternalEventSourceField () {
+      return this.requestExternalEventSourceField(true);
+    }
+    public APIRequestGet requestExternalEventSourceField (boolean value) {
+      this.requestField("external_event_source", value);
+      return this;
+    }
+    public APIRequestGet requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGet requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
+    public APIRequestGet requestIncludedCustomAudiencesField () {
+      return this.requestIncludedCustomAudiencesField(true);
+    }
+    public APIRequestGet requestIncludedCustomAudiencesField (boolean value) {
+      this.requestField("included_custom_audiences", value);
+      return this;
+    }
+    public APIRequestGet requestLastUsedTimeField () {
+      return this.requestLastUsedTimeField(true);
+    }
+    public APIRequestGet requestLastUsedTimeField (boolean value) {
+      this.requestField("last_used_time", value);
+      return this;
+    }
+    public APIRequestGet requestLookalikeAudienceIdsField () {
+      return this.requestLookalikeAudienceIdsField(true);
+    }
+    public APIRequestGet requestLookalikeAudienceIdsField (boolean value) {
+      this.requestField("lookalike_audience_ids", value);
+      return this;
+    }
+    public APIRequestGet requestLookalikeSpecField () {
+      return this.requestLookalikeSpecField(true);
+    }
+    public APIRequestGet requestLookalikeSpecField (boolean value) {
+      this.requestField("lookalike_spec", value);
+      return this;
+    }
+    public APIRequestGet requestNameField () {
+      return this.requestNameField(true);
+    }
+    public APIRequestGet requestNameField (boolean value) {
+      this.requestField("name", value);
+      return this;
+    }
+    public APIRequestGet requestOperationStatusField () {
+      return this.requestOperationStatusField(true);
+    }
+    public APIRequestGet requestOperationStatusField (boolean value) {
+      this.requestField("operation_status", value);
+      return this;
+    }
+    public APIRequestGet requestOptOutLinkField () {
+      return this.requestOptOutLinkField(true);
+    }
+    public APIRequestGet requestOptOutLinkField (boolean value) {
+      this.requestField("opt_out_link", value);
+      return this;
+    }
+    public APIRequestGet requestOwnerBusinessField () {
+      return this.requestOwnerBusinessField(true);
+    }
+    public APIRequestGet requestOwnerBusinessField (boolean value) {
+      this.requestField("owner_business", value);
       return this;
     }
-
-    public APIRequestDeleteAdAccounts setParams(Map<String, Object> params) {
-      setParamsInternal(params);
+    public APIRequestGet requestPermissionForActionsField () {
+      return this.requestPermissionForActionsField(true);
+    }
+    public APIRequestGet requestPermissionForActionsField (boolean value) {
+      this.requestField("permission_for_actions", value);
       return this;
     }
-
-
-    public APIRequestDeleteAdAccounts setId (String id) {
-      this.setParam("id", id);
+    public APIRequestGet requestPixelIdField () {
+      return this.requestPixelIdField(true);
+    }
+    public APIRequestGet requestPixelIdField (boolean value) {
+      this.requestField("pixel_id", value);
       return this;
     }
-
-
-    public APIRequestDeleteAdAccounts setAdaccounts (List<String> adaccounts) {
-      this.setParam("adaccounts", adaccounts);
+    public APIRequestGet requestRetentionDaysField () {
+      return this.requestRetentionDaysField(true);
+    }
+    public APIRequestGet requestRetentionDaysField (boolean value) {
+      this.requestField("retention_days", value);
       return this;
     }
-
-    public APIRequestDeleteAdAccounts setAdaccounts (String adaccounts) {
-      this.setParam("adaccounts", adaccounts);
+    public APIRequestGet requestRuleField () {
+      return this.requestRuleField(true);
+    }
+    public APIRequestGet requestRuleField (boolean value) {
+      this.requestField("rule", value);
       return this;
     }
-
-    public APIRequestDeleteAdAccounts requestAllFields () {
-      return this.requestAllFields(true);
+    public APIRequestGet requestSubtypeField () {
+      return this.requestSubtypeField(true);
     }
-
-    public APIRequestDeleteAdAccounts requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
+    public APIRequestGet requestSubtypeField (boolean value) {
+      this.requestField("subtype", value);
       return this;
     }
-
-    public APIRequestDeleteAdAccounts requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
+    public APIRequestGet requestTimeContentUpdatedField () {
+      return this.requestTimeContentUpdatedField(true);
     }
-
-    public APIRequestDeleteAdAccounts requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+    public APIRequestGet requestTimeContentUpdatedField (boolean value) {
+      this.requestField("time_content_updated", value);
       return this;
     }
-
-    public APIRequestDeleteAdAccounts requestField (String field) {
-      this.requestField(field, true);
+    public APIRequestGet requestTimeCreatedField () {
+      return this.requestTimeCreatedField(true);
+    }
+    public APIRequestGet requestTimeCreatedField (boolean value) {
+      this.requestField("time_created", value);
       return this;
     }
-
-    public APIRequestDeleteAdAccounts requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
+    public APIRequestGet requestTimeUpdatedField () {
+      return this.requestTimeUpdatedField(true);
+    }
+    public APIRequestGet requestTimeUpdatedField (boolean value) {
+      this.requestField("time_updated", value);
       return this;
     }
-
-
   }
 
-  public static class APIRequestCreateUser extends APIRequest<APINode> {
+  public static class APIRequestUpdate extends APIRequest<APINode> {
 
     APINode lastResponse = null;
     @Override
@@ -1955,9 +1876,16 @@ public class CustomAudience extends APINode {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "id",
-      "session",
-      "payload",
+      "content_type",
+      "description",
+      "exclusions",
+      "inclusions",
+      "lookalike_spec",
+      "name",
+      "opt_out_link",
+      "product_set_id",
+      "retention_days",
+      "rule",
     };
 
     public static final String[] FIELDS = {
@@ -1975,192 +1903,248 @@ public class CustomAudience extends APINode {
 
     @Override
     public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestCreateUser(String nodeId, APIContext context) {
-      super(context, nodeId, "/users", "POST", Arrays.asList(PARAMS));
+    public APIRequestUpdate(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "POST", Arrays.asList(PARAMS));
     }
 
-    public APIRequestCreateUser setParam(String param, Object value) {
+    @Override
+    public APIRequestUpdate setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestCreateUser setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestUpdate setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestCreateUser setId (String id) {
-      this.setParam("id", id);
-      return this;
-    }
-
-
-    public APIRequestCreateUser setSession (Object session) {
-      this.setParam("session", session);
-      return this;
-    }
-
-    public APIRequestCreateUser setSession (String session) {
-      this.setParam("session", session);
+    public APIRequestUpdate setContentType (CustomAudience.EnumContentType contentType) {
+      this.setParam("content_type", contentType);
       return this;
     }
-
-    public APIRequestCreateUser setPayload (Object payload) {
-      this.setParam("payload", payload);
+    public APIRequestUpdate setContentType (String contentType) {
+      this.setParam("content_type", contentType);
       return this;
     }
 
-    public APIRequestCreateUser setPayload (String payload) {
-      this.setParam("payload", payload);
+    public APIRequestUpdate setDescription (String description) {
+      this.setParam("description", description);
       return this;
     }
 
-    public APIRequestCreateUser requestAllFields () {
-      return this.requestAllFields(true);
-    }
-
-    public APIRequestCreateUser requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
+    public APIRequestUpdate setExclusions (List<Object> exclusions) {
+      this.setParam("exclusions", exclusions);
       return this;
     }
-
-    public APIRequestCreateUser requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
-    }
-
-    public APIRequestCreateUser requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+    public APIRequestUpdate setExclusions (String exclusions) {
+      this.setParam("exclusions", exclusions);
       return this;
     }
 
-    public APIRequestCreateUser requestField (String field) {
-      this.requestField(field, true);
+    public APIRequestUpdate setInclusions (List<Object> inclusions) {
+      this.setParam("inclusions", inclusions);
       return this;
     }
-
-    public APIRequestCreateUser requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
+    public APIRequestUpdate setInclusions (String inclusions) {
+      this.setParam("inclusions", inclusions);
       return this;
     }
 
-
-  }
-
-  public static class APIRequestDeleteUsers extends APIRequest<APINode> {
-
-    APINodeList<APINode> lastResponse = null;
-    @Override
-    public APINodeList<APINode> getLastResponse() {
-      return lastResponse;
-    }
-    public static final String[] PARAMS = {
-      "id",
-      "session",
-      "payload",
-    };
-
-    public static final String[] FIELDS = {
-    };
-
-    @Override
-    public APINodeList<APINode> parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this);
-    }
-
-    @Override
-    public APINodeList<APINode> execute() throws APIException {
-      return execute(new HashMap<String, Object>());
-    }
-
-    @Override
-    public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
-    }
-
-    public APIRequestDeleteUsers(String nodeId, APIContext context) {
-      super(context, nodeId, "/users", "DELETE", Arrays.asList(PARAMS));
-    }
-
-    public APIRequestDeleteUsers setParam(String param, Object value) {
-      setParamInternal(param, value);
+    public APIRequestUpdate setLookalikeSpec (String lookalikeSpec) {
+      this.setParam("lookalike_spec", lookalikeSpec);
       return this;
     }
 
-    public APIRequestDeleteUsers setParams(Map<String, Object> params) {
-      setParamsInternal(params);
+    public APIRequestUpdate setName (String name) {
+      this.setParam("name", name);
       return this;
     }
 
-
-    public APIRequestDeleteUsers setId (String id) {
-      this.setParam("id", id);
+    public APIRequestUpdate setOptOutLink (String optOutLink) {
+      this.setParam("opt_out_link", optOutLink);
       return this;
     }
 
-
-    public APIRequestDeleteUsers setSession (Object session) {
-      this.setParam("session", session);
+    public APIRequestUpdate setProductSetId (String productSetId) {
+      this.setParam("product_set_id", productSetId);
       return this;
     }
 
-    public APIRequestDeleteUsers setSession (String session) {
-      this.setParam("session", session);
+    public APIRequestUpdate setRetentionDays (Long retentionDays) {
+      this.setParam("retention_days", retentionDays);
       return this;
     }
-
-    public APIRequestDeleteUsers setPayload (Object payload) {
-      this.setParam("payload", payload);
+    public APIRequestUpdate setRetentionDays (String retentionDays) {
+      this.setParam("retention_days", retentionDays);
       return this;
     }
 
-    public APIRequestDeleteUsers setPayload (String payload) {
-      this.setParam("payload", payload);
+    public APIRequestUpdate setRule (String rule) {
+      this.setParam("rule", rule);
       return this;
     }
 
-    public APIRequestDeleteUsers requestAllFields () {
+    public APIRequestUpdate requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestDeleteUsers requestAllFields (boolean value) {
+    public APIRequestUpdate requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestDeleteUsers requestFields (List<String> fields) {
+    @Override
+    public APIRequestUpdate requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestDeleteUsers requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestUpdate requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestDeleteUsers requestField (String field) {
+    @Override
+    public APIRequestUpdate requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestDeleteUsers requestField (String field, boolean value) {
+    @Override
+    public APIRequestUpdate requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
+  }
+
+  public static enum EnumContentType {
+      @SerializedName("HOTEL")
+      VALUE_HOTEL("HOTEL"),
+      @SerializedName("FLIGHT")
+      VALUE_FLIGHT("FLIGHT"),
+      NULL(null);
+
+      private String value;
+
+      private EnumContentType(String value) {
+        this.value = value;
+      }
+
+      @Override
+      public String toString() {
+        return value;
+      }
+  }
+
+  public static enum EnumSubtype {
+      @SerializedName("CUSTOM")
+      VALUE_CUSTOM("CUSTOM"),
+      @SerializedName("WEBSITE")
+      VALUE_WEBSITE("WEBSITE"),
+      @SerializedName("APP")
+      VALUE_APP("APP"),
+      @SerializedName("CLAIM")
+      VALUE_CLAIM("CLAIM"),
+      @SerializedName("PARTNER")
+      VALUE_PARTNER("PARTNER"),
+      @SerializedName("MANAGED")
+      VALUE_MANAGED("MANAGED"),
+      @SerializedName("VIDEO")
+      VALUE_VIDEO("VIDEO"),
+      @SerializedName("LOOKALIKE")
+      VALUE_LOOKALIKE("LOOKALIKE"),
+      @SerializedName("ENGAGEMENT")
+      VALUE_ENGAGEMENT("ENGAGEMENT"),
+      @SerializedName("DATA_SET")
+      VALUE_DATA_SET("DATA_SET"),
+      @SerializedName("BAG_OF_ACCOUNTS")
+      VALUE_BAG_OF_ACCOUNTS("BAG_OF_ACCOUNTS"),
+      NULL(null);
+
+      private String value;
+
+      private EnumSubtype(String value) {
+        this.value = value;
+      }
+
+      @Override
+      public String toString() {
+        return value;
+      }
+  }
+
+  public static enum EnumFields {
+      @SerializedName("id")
+      VALUE_ID("id"),
+      @SerializedName("account_id")
+      VALUE_ACCOUNT_ID("account_id"),
+      @SerializedName("approximate_count")
+      VALUE_APPROXIMATE_COUNT("approximate_count"),
+      @SerializedName("data_source")
+      VALUE_DATA_SOURCE("data_source"),
+      @SerializedName("delivery_status")
+      VALUE_DELIVERY_STATUS("delivery_status"),
+      @SerializedName("description")
+      VALUE_DESCRIPTION("description"),
+      @SerializedName("excluded_custom_audiences")
+      VALUE_EXCLUDED_CUSTOM_AUDIENCES("excluded_custom_audiences"),
+      @SerializedName("external_event_source")
+      VALUE_EXTERNAL_EVENT_SOURCE("external_event_source"),
+      @SerializedName("included_custom_audiences")
+      VALUE_INCLUDED_CUSTOM_AUDIENCES("included_custom_audiences"),
+      @SerializedName("last_used_time")
+      VALUE_LAST_USED_TIME("last_used_time"),
+      @SerializedName("lookalike_audience_ids")
+      VALUE_LOOKALIKE_AUDIENCE_IDS("lookalike_audience_ids"),
+      @SerializedName("lookalike_spec")
+      VALUE_LOOKALIKE_SPEC("lookalike_spec"),
+      @SerializedName("name")
+      VALUE_NAME("name"),
+      @SerializedName("operation_status")
+      VALUE_OPERATION_STATUS("operation_status"),
+      @SerializedName("opt_out_link")
+      VALUE_OPT_OUT_LINK("opt_out_link"),
+      @SerializedName("owner_business")
+      VALUE_OWNER_BUSINESS("owner_business"),
+      @SerializedName("permission_for_actions")
+      VALUE_PERMISSION_FOR_ACTIONS("permission_for_actions"),
+      @SerializedName("pixel_id")
+      VALUE_PIXEL_ID("pixel_id"),
+      @SerializedName("retention_days")
+      VALUE_RETENTION_DAYS("retention_days"),
+      @SerializedName("rule")
+      VALUE_RULE("rule"),
+      @SerializedName("subtype")
+      VALUE_SUBTYPE("subtype"),
+      @SerializedName("time_content_updated")
+      VALUE_TIME_CONTENT_UPDATED("time_content_updated"),
+      @SerializedName("time_created")
+      VALUE_TIME_CREATED("time_created"),
+      @SerializedName("time_updated")
+      VALUE_TIME_UPDATED("time_updated"),
+      NULL(null);
+
+      private String value;
+
+      private EnumFields(String value) {
+        this.value = value;
+      }
 
+      @Override
+      public String toString() {
+        return value;
+      }
   }
 
 
@@ -2178,36 +2162,38 @@ public class CustomAudience extends APINode {
   }
 
   public CustomAudience copyFrom(CustomAudience instance) {
-    this.mId = instance.mId;
     this.mAccountId = instance.mAccountId;
     this.mApproximateCount = instance.mApproximateCount;
     this.mDataSource = instance.mDataSource;
     this.mDeliveryStatus = instance.mDeliveryStatus;
     this.mDescription = instance.mDescription;
     this.mExcludedCustomAudiences = instance.mExcludedCustomAudiences;
-    this.mIncludedCustomAudiences = instance.mIncludedCustomAudiences;
     this.mExternalEventSource = instance.mExternalEventSource;
+    this.mId = instance.mId;
+    this.mIncludedCustomAudiences = instance.mIncludedCustomAudiences;
+    this.mLastUsedTime = instance.mLastUsedTime;
     this.mLookalikeAudienceIds = instance.mLookalikeAudienceIds;
     this.mLookalikeSpec = instance.mLookalikeSpec;
     this.mName = instance.mName;
     this.mOperationStatus = instance.mOperationStatus;
     this.mOptOutLink = instance.mOptOutLink;
+    this.mOwnerBusiness = instance.mOwnerBusiness;
     this.mPermissionForActions = instance.mPermissionForActions;
     this.mPixelId = instance.mPixelId;
-    this.mRule = instance.mRule;
     this.mRetentionDays = instance.mRetentionDays;
+    this.mRule = instance.mRule;
     this.mSubtype = instance.mSubtype;
+    this.mTimeContentUpdated = instance.mTimeContentUpdated;
     this.mTimeCreated = instance.mTimeCreated;
     this.mTimeUpdated = instance.mTimeUpdated;
-    this.mTimeContentUpdated = instance.mTimeContentUpdated;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<CustomAudience> getParser() {
     return new APIRequest.ResponseParser<CustomAudience>() {
-      public APINodeList<CustomAudience> parseResponse(String response, APIContext context, APIRequest<CustomAudience> request) {
+      public APINodeList<CustomAudience> parseResponse(String response, APIContext context, APIRequest<CustomAudience> request) throws MalformedResponseException {
         return CustomAudience.parseResponse(response, context, request);
       }
     };
