@@ -24,36 +24,41 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class ProductSet extends APINode {
+  @SerializedName("filter")
+  private String mFilter = null;
   @SerializedName("id")
   private String mId = null;
   @SerializedName("name")
   private String mName = null;
-  @SerializedName("filter")
-  private String mFilter = null;
+  @SerializedName("product_catalog")
+  private ProductCatalog mProductCatalog = null;
   @SerializedName("product_count")
   private Long mProductCount = null;
   protected static Gson gson = null;
@@ -67,11 +72,11 @@ public class ProductSet extends APINode {
 
   public ProductSet(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public ProductSet fetch() throws APIException{
-    ProductSet newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    ProductSet newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -88,8 +93,17 @@ public class ProductSet extends APINode {
     return productSet;
   }
 
+  public static APINodeList<ProductSet> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<ProductSet>)(
+      new APIRequest<ProductSet>(context, "", "/", "GET", ProductSet.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -104,22 +118,23 @@ public class ProductSet extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    productSet.mContext = context;
+    productSet.context = context;
     productSet.rawValue = json;
     return productSet;
   }
 
-  public static APINodeList<ProductSet> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<ProductSet> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<ProductSet> productSets = new APINodeList<ProductSet>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -132,10 +147,11 @@ public class ProductSet extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            productSets.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            productSets.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -146,7 +162,20 @@ public class ProductSet extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            productSets.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  productSets.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              productSets.add(loadJSON(obj.toString(), context));
+            }
           }
           return productSets;
         } else if (obj.has("images")) {
@@ -157,24 +186,54 @@ public class ProductSet extends APINode {
           }
           return productSets;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              productSets.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return productSets;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          productSets.clear();
           productSets.add(loadJSON(json, context));
           return productSets;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -182,27 +241,31 @@ public class ProductSet extends APINode {
     return getGson().toJson(this);
   }
 
-  public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetProductGroups getProductGroups() {
+    return new APIRequestGetProductGroups(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestUpdate update() {
-    return new APIRequestUpdate(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetProducts getProducts() {
+    return new APIRequestGetProducts(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestDelete delete() {
-    return new APIRequestDelete(this.getPrefixedId().toString(), mContext);
+    return new APIRequestDelete(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetProducts getProducts() {
-    return new APIRequestGetProducts(this.getPrefixedId().toString(), mContext);
+  public APIRequestGet get() {
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetProductGroups getProductGroups() {
-    return new APIRequestGetProductGroups(this.getPrefixedId().toString(), mContext);
+  public APIRequestUpdate update() {
+    return new APIRequestUpdate(this.getPrefixedId().toString(), context);
   }
 
 
+  public String getFieldFilter() {
+    return mFilter;
+  }
+
   public String getFieldId() {
     return mId;
   }
@@ -211,8 +274,11 @@ public class ProductSet extends APINode {
     return mName;
   }
 
-  public String getFieldFilter() {
-    return mFilter;
+  public ProductCatalog getFieldProductCatalog() {
+    if (mProductCatalog != null) {
+      mProductCatalog.context = getContext();
+    }
+    return mProductCatalog;
   }
 
   public Long getFieldProductCount() {
@@ -221,11 +287,11 @@ public class ProductSet extends APINode {
 
 
 
-  public static class APIRequestGet extends APIRequest<ProductSet> {
+  public static class APIRequestGetProductGroups extends APIRequest<ProductGroup> {
 
-    ProductSet lastResponse = null;
+    APINodeList<ProductGroup> lastResponse = null;
     @Override
-    public ProductSet getLastResponse() {
+    public APINodeList<ProductGroup> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
@@ -233,437 +299,252 @@ public class ProductSet extends APINode {
 
     public static final String[] FIELDS = {
       "id",
-      "name",
-      "filter",
-      "product_count",
+      "retailer_id",
+      "variants",
     };
 
     @Override
-    public ProductSet parseResponse(String response) throws APIException {
-      return ProductSet.parseResponse(response, getContext(), this).head();
+    public APINodeList<ProductGroup> parseResponse(String response) throws APIException {
+      return ProductGroup.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public ProductSet execute() throws APIException {
+    public APINodeList<ProductGroup> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public ProductSet execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<ProductGroup> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGet(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetProductGroups(String nodeId, APIContext context) {
+      super(context, nodeId, "/product_groups", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGet setParam(String param, Object value) {
+    @Override
+    public APIRequestGetProductGroups setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGet setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetProductGroups setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGet requestAllFields () {
+    public APIRequestGetProductGroups requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGet requestAllFields (boolean value) {
+    public APIRequestGetProductGroups requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetProductGroups requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGet requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetProductGroups requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestField (String field) {
+    @Override
+    public APIRequestGetProductGroups requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGet requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetProductGroups requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGet requestIdField () {
+    public APIRequestGetProductGroups requestIdField () {
       return this.requestIdField(true);
     }
-    public APIRequestGet requestIdField (boolean value) {
+    public APIRequestGetProductGroups requestIdField (boolean value) {
       this.requestField("id", value);
       return this;
     }
-    public APIRequestGet requestNameField () {
-      return this.requestNameField(true);
-    }
-    public APIRequestGet requestNameField (boolean value) {
-      this.requestField("name", value);
-      return this;
-    }
-    public APIRequestGet requestFilterField () {
-      return this.requestFilterField(true);
+    public APIRequestGetProductGroups requestRetailerIdField () {
+      return this.requestRetailerIdField(true);
     }
-    public APIRequestGet requestFilterField (boolean value) {
-      this.requestField("filter", value);
+    public APIRequestGetProductGroups requestRetailerIdField (boolean value) {
+      this.requestField("retailer_id", value);
       return this;
     }
-    public APIRequestGet requestProductCountField () {
-      return this.requestProductCountField(true);
+    public APIRequestGetProductGroups requestVariantsField () {
+      return this.requestVariantsField(true);
     }
-    public APIRequestGet requestProductCountField (boolean value) {
-      this.requestField("product_count", value);
+    public APIRequestGetProductGroups requestVariantsField (boolean value) {
+      this.requestField("variants", value);
       return this;
     }
-
   }
 
-  public static class APIRequestUpdate extends APIRequest<APINode> {
+  public static class APIRequestGetProducts extends APIRequest<ProductItem> {
 
-    APINode lastResponse = null;
+    APINodeList<ProductItem> lastResponse = null;
     @Override
-    public APINode getLastResponse() {
+    public APINodeList<ProductItem> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "filter",
-      "id",
-      "name",
     };
 
     public static final String[] FIELDS = {
+      "additional_image_urls",
+      "age_group",
+      "applinks",
+      "availability",
+      "brand",
+      "category",
+      "color",
+      "commerce_insights",
+      "condition",
+      "custom_data",
+      "custom_label_0",
+      "custom_label_1",
+      "custom_label_2",
+      "custom_label_3",
+      "custom_label_4",
+      "description",
+      "expiration_date",
+      "gender",
+      "gtin",
+      "id",
+      "image_url",
+      "manufacturer_part_number",
+      "material",
+      "name",
+      "ordering_index",
+      "pattern",
+      "price",
+      "product_feed",
+      "product_type",
+      "retailer_id",
+      "retailer_product_group_id",
+      "review_rejection_reasons",
+      "review_status",
+      "sale_price",
+      "sale_price_end_date",
+      "sale_price_start_date",
+      "shipping_weight_unit",
+      "shipping_weight_value",
+      "size",
+      "start_date",
+      "url",
+      "visibility",
     };
 
     @Override
-    public APINode parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this).head();
+    public APINodeList<ProductItem> parseResponse(String response) throws APIException {
+      return ProductItem.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINode execute() throws APIException {
+    public APINodeList<ProductItem> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<ProductItem> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestUpdate(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "POST", Arrays.asList(PARAMS));
+    public APIRequestGetProducts(String nodeId, APIContext context) {
+      super(context, nodeId, "/products", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestUpdate setParam(String param, Object value) {
+    @Override
+    public APIRequestGetProducts setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestUpdate setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetProducts setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestUpdate setFilter (String filter) {
-      this.setParam("filter", filter);
-      return this;
-    }
-
-
-    public APIRequestUpdate setId (String id) {
-      this.setParam("id", id);
-      return this;
-    }
-
-
-    public APIRequestUpdate setName (String name) {
-      this.setParam("name", name);
-      return this;
-    }
-
-
-    public APIRequestUpdate requestAllFields () {
+    public APIRequestGetProducts requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestUpdate requestAllFields (boolean value) {
+    public APIRequestGetProducts requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestUpdate requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetProducts requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestUpdate requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetProducts requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestUpdate requestField (String field) {
+    @Override
+    public APIRequestGetProducts requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestUpdate requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetProducts requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
-  }
-
-  public static class APIRequestDelete extends APIRequest<APINode> {
-
-    APINode lastResponse = null;
-    @Override
-    public APINode getLastResponse() {
-      return lastResponse;
+    public APIRequestGetProducts requestAdditionalImageUrlsField () {
+      return this.requestAdditionalImageUrlsField(true);
     }
-    public static final String[] PARAMS = {
-      "id",
-    };
-
-    public static final String[] FIELDS = {
-    };
-
-    @Override
-    public APINode parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this).head();
+    public APIRequestGetProducts requestAdditionalImageUrlsField (boolean value) {
+      this.requestField("additional_image_urls", value);
+      return this;
     }
-
-    @Override
-    public APINode execute() throws APIException {
-      return execute(new HashMap<String, Object>());
+    public APIRequestGetProducts requestAgeGroupField () {
+      return this.requestAgeGroupField(true);
     }
-
-    @Override
-    public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
+    public APIRequestGetProducts requestAgeGroupField (boolean value) {
+      this.requestField("age_group", value);
+      return this;
     }
-
-    public APIRequestDelete(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "DELETE", Arrays.asList(PARAMS));
+    public APIRequestGetProducts requestApplinksField () {
+      return this.requestApplinksField(true);
     }
-
-    public APIRequestDelete setParam(String param, Object value) {
-      setParamInternal(param, value);
+    public APIRequestGetProducts requestApplinksField (boolean value) {
+      this.requestField("applinks", value);
       return this;
     }
-
-    public APIRequestDelete setParams(Map<String, Object> params) {
-      setParamsInternal(params);
-      return this;
-    }
-
-
-    public APIRequestDelete setId (String id) {
-      this.setParam("id", id);
-      return this;
-    }
-
-
-    public APIRequestDelete requestAllFields () {
-      return this.requestAllFields(true);
-    }
-
-    public APIRequestDelete requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
-      return this;
-    }
-
-    public APIRequestDelete requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
-    }
-
-    public APIRequestDelete requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
-      return this;
-    }
-
-    public APIRequestDelete requestField (String field) {
-      this.requestField(field, true);
-      return this;
-    }
-
-    public APIRequestDelete requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
-      return this;
-    }
-
-
-  }
-
-  public static class APIRequestGetProducts extends APIRequest<ProductItem> {
-
-    APINodeList<ProductItem> lastResponse = null;
-    @Override
-    public APINodeList<ProductItem> getLastResponse() {
-      return lastResponse;
-    }
-    public static final String[] PARAMS = {
-    };
-
-    public static final String[] FIELDS = {
-      "id",
-      "additional_image_urls",
-      "applinks",
-      "age_group",
-      "availability",
-      "brand",
-      "category",
-      "color",
-      "commerce_insights",
-      "condition",
-      "custom_data",
-      "description",
-      "expiration_date",
-      "gender",
-      "gtin",
-      "image_url",
-      "material",
-      "manufacturer_part_number",
-      "name",
-      "ordering_index",
-      "pattern",
-      "price",
-      "product_type",
-      "retailer_id",
-      "retailer_product_group_id",
-      "review_rejection_reasons",
-      "review_status",
-      "sale_price",
-      "sale_price_start_date",
-      "sale_price_end_date",
-      "shipping_weight_value",
-      "shipping_weight_unit",
-      "size",
-      "start_date",
-      "url",
-      "visibility",
-      "product_feed",
-    };
-
-    @Override
-    public APINodeList<ProductItem> parseResponse(String response) throws APIException {
-      return ProductItem.parseResponse(response, getContext(), this);
-    }
-
-    @Override
-    public APINodeList<ProductItem> execute() throws APIException {
-      return execute(new HashMap<String, Object>());
-    }
-
-    @Override
-    public APINodeList<ProductItem> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
-    }
-
-    public APIRequestGetProducts(String nodeId, APIContext context) {
-      super(context, nodeId, "/products", "GET", Arrays.asList(PARAMS));
-    }
-
-    public APIRequestGetProducts setParam(String param, Object value) {
-      setParamInternal(param, value);
-      return this;
-    }
-
-    public APIRequestGetProducts setParams(Map<String, Object> params) {
-      setParamsInternal(params);
-      return this;
-    }
-
-
-    public APIRequestGetProducts requestAllFields () {
-      return this.requestAllFields(true);
-    }
-
-    public APIRequestGetProducts requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
-      return this;
-    }
-
-    public APIRequestGetProducts requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
-    }
-
-    public APIRequestGetProducts requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
-      return this;
-    }
-
-    public APIRequestGetProducts requestField (String field) {
-      this.requestField(field, true);
-      return this;
-    }
-
-    public APIRequestGetProducts requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
-      return this;
-    }
-
-    public APIRequestGetProducts requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGetProducts requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
-    }
-    public APIRequestGetProducts requestAdditionalImageUrlsField () {
-      return this.requestAdditionalImageUrlsField(true);
-    }
-    public APIRequestGetProducts requestAdditionalImageUrlsField (boolean value) {
-      this.requestField("additional_image_urls", value);
-      return this;
-    }
-    public APIRequestGetProducts requestApplinksField () {
-      return this.requestApplinksField(true);
-    }
-    public APIRequestGetProducts requestApplinksField (boolean value) {
-      this.requestField("applinks", value);
-      return this;
-    }
-    public APIRequestGetProducts requestAgeGroupField () {
-      return this.requestAgeGroupField(true);
-    }
-    public APIRequestGetProducts requestAgeGroupField (boolean value) {
-      this.requestField("age_group", value);
-      return this;
-    }
-    public APIRequestGetProducts requestAvailabilityField () {
-      return this.requestAvailabilityField(true);
-    }
-    public APIRequestGetProducts requestAvailabilityField (boolean value) {
-      this.requestField("availability", value);
+    public APIRequestGetProducts requestAvailabilityField () {
+      return this.requestAvailabilityField(true);
+    }
+    public APIRequestGetProducts requestAvailabilityField (boolean value) {
+      this.requestField("availability", value);
       return this;
     }
     public APIRequestGetProducts requestBrandField () {
@@ -708,6 +589,41 @@ public class ProductSet extends APINode {
       this.requestField("custom_data", value);
       return this;
     }
+    public APIRequestGetProducts requestCustomLabel0Field () {
+      return this.requestCustomLabel0Field(true);
+    }
+    public APIRequestGetProducts requestCustomLabel0Field (boolean value) {
+      this.requestField("custom_label_0", value);
+      return this;
+    }
+    public APIRequestGetProducts requestCustomLabel1Field () {
+      return this.requestCustomLabel1Field(true);
+    }
+    public APIRequestGetProducts requestCustomLabel1Field (boolean value) {
+      this.requestField("custom_label_1", value);
+      return this;
+    }
+    public APIRequestGetProducts requestCustomLabel2Field () {
+      return this.requestCustomLabel2Field(true);
+    }
+    public APIRequestGetProducts requestCustomLabel2Field (boolean value) {
+      this.requestField("custom_label_2", value);
+      return this;
+    }
+    public APIRequestGetProducts requestCustomLabel3Field () {
+      return this.requestCustomLabel3Field(true);
+    }
+    public APIRequestGetProducts requestCustomLabel3Field (boolean value) {
+      this.requestField("custom_label_3", value);
+      return this;
+    }
+    public APIRequestGetProducts requestCustomLabel4Field () {
+      return this.requestCustomLabel4Field(true);
+    }
+    public APIRequestGetProducts requestCustomLabel4Field (boolean value) {
+      this.requestField("custom_label_4", value);
+      return this;
+    }
     public APIRequestGetProducts requestDescriptionField () {
       return this.requestDescriptionField(true);
     }
@@ -736,6 +652,13 @@ public class ProductSet extends APINode {
       this.requestField("gtin", value);
       return this;
     }
+    public APIRequestGetProducts requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGetProducts requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
     public APIRequestGetProducts requestImageUrlField () {
       return this.requestImageUrlField(true);
     }
@@ -743,13 +666,6 @@ public class ProductSet extends APINode {
       this.requestField("image_url", value);
       return this;
     }
-    public APIRequestGetProducts requestMaterialField () {
-      return this.requestMaterialField(true);
-    }
-    public APIRequestGetProducts requestMaterialField (boolean value) {
-      this.requestField("material", value);
-      return this;
-    }
     public APIRequestGetProducts requestManufacturerPartNumberField () {
       return this.requestManufacturerPartNumberField(true);
     }
@@ -757,6 +673,13 @@ public class ProductSet extends APINode {
       this.requestField("manufacturer_part_number", value);
       return this;
     }
+    public APIRequestGetProducts requestMaterialField () {
+      return this.requestMaterialField(true);
+    }
+    public APIRequestGetProducts requestMaterialField (boolean value) {
+      this.requestField("material", value);
+      return this;
+    }
     public APIRequestGetProducts requestNameField () {
       return this.requestNameField(true);
     }
@@ -785,6 +708,13 @@ public class ProductSet extends APINode {
       this.requestField("price", value);
       return this;
     }
+    public APIRequestGetProducts requestProductFeedField () {
+      return this.requestProductFeedField(true);
+    }
+    public APIRequestGetProducts requestProductFeedField (boolean value) {
+      this.requestField("product_feed", value);
+      return this;
+    }
     public APIRequestGetProducts requestProductTypeField () {
       return this.requestProductTypeField(true);
     }
@@ -827,13 +757,6 @@ public class ProductSet extends APINode {
       this.requestField("sale_price", value);
       return this;
     }
-    public APIRequestGetProducts requestSalePriceStartDateField () {
-      return this.requestSalePriceStartDateField(true);
-    }
-    public APIRequestGetProducts requestSalePriceStartDateField (boolean value) {
-      this.requestField("sale_price_start_date", value);
-      return this;
-    }
     public APIRequestGetProducts requestSalePriceEndDateField () {
       return this.requestSalePriceEndDateField(true);
     }
@@ -841,11 +764,11 @@ public class ProductSet extends APINode {
       this.requestField("sale_price_end_date", value);
       return this;
     }
-    public APIRequestGetProducts requestShippingWeightValueField () {
-      return this.requestShippingWeightValueField(true);
+    public APIRequestGetProducts requestSalePriceStartDateField () {
+      return this.requestSalePriceStartDateField(true);
     }
-    public APIRequestGetProducts requestShippingWeightValueField (boolean value) {
-      this.requestField("shipping_weight_value", value);
+    public APIRequestGetProducts requestSalePriceStartDateField (boolean value) {
+      this.requestField("sale_price_start_date", value);
       return this;
     }
     public APIRequestGetProducts requestShippingWeightUnitField () {
@@ -855,6 +778,13 @@ public class ProductSet extends APINode {
       this.requestField("shipping_weight_unit", value);
       return this;
     }
+    public APIRequestGetProducts requestShippingWeightValueField () {
+      return this.requestShippingWeightValueField(true);
+    }
+    public APIRequestGetProducts requestShippingWeightValueField (boolean value) {
+      this.requestField("shipping_weight_value", value);
+      return this;
+    }
     public APIRequestGetProducts requestSizeField () {
       return this.requestSizeField(true);
     }
@@ -883,114 +813,323 @@ public class ProductSet extends APINode {
       this.requestField("visibility", value);
       return this;
     }
-    public APIRequestGetProducts requestProductFeedField () {
-      return this.requestProductFeedField(true);
-    }
-    public APIRequestGetProducts requestProductFeedField (boolean value) {
-      this.requestField("product_feed", value);
+  }
+
+  public static class APIRequestDelete extends APIRequest<APINode> {
+
+    APINode lastResponse = null;
+    @Override
+    public APINode getLastResponse() {
+      return lastResponse;
+    }
+    public static final String[] PARAMS = {
+      "id",
+    };
+
+    public static final String[] FIELDS = {
+    };
+
+    @Override
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
+    }
+
+    @Override
+    public APINode execute() throws APIException {
+      return execute(new HashMap<String, Object>());
+    }
+
+    @Override
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
+    }
+
+    public APIRequestDelete(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "DELETE", Arrays.asList(PARAMS));
+    }
+
+    @Override
+    public APIRequestDelete setParam(String param, Object value) {
+      setParamInternal(param, value);
+      return this;
+    }
+
+    @Override
+    public APIRequestDelete setParams(Map<String, Object> params) {
+      setParamsInternal(params);
+      return this;
+    }
+
+
+    public APIRequestDelete setId (String id) {
+      this.setParam("id", id);
+      return this;
+    }
+
+    public APIRequestDelete requestAllFields () {
+      return this.requestAllFields(true);
+    }
+
+    public APIRequestDelete requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestDelete requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
+    }
+
+    @Override
+    public APIRequestDelete requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestDelete requestField (String field) {
+      this.requestField(field, true);
+      return this;
+    }
+
+    @Override
+    public APIRequestDelete requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
 
   }
 
-  public static class APIRequestGetProductGroups extends APIRequest<ProductGroup> {
+  public static class APIRequestGet extends APIRequest<ProductSet> {
 
-    APINodeList<ProductGroup> lastResponse = null;
+    ProductSet lastResponse = null;
     @Override
-    public APINodeList<ProductGroup> getLastResponse() {
+    public ProductSet getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
     };
 
     public static final String[] FIELDS = {
+      "filter",
       "id",
-      "retailer_id",
-      "variants",
+      "name",
+      "product_catalog",
+      "product_count",
     };
 
     @Override
-    public APINodeList<ProductGroup> parseResponse(String response) throws APIException {
-      return ProductGroup.parseResponse(response, getContext(), this);
+    public ProductSet parseResponse(String response) throws APIException {
+      return ProductSet.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public APINodeList<ProductGroup> execute() throws APIException {
+    public ProductSet execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<ProductGroup> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public ProductSet execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetProductGroups(String nodeId, APIContext context) {
-      super(context, nodeId, "/product_groups", "GET", Arrays.asList(PARAMS));
+    public APIRequestGet(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetProductGroups setParam(String param, Object value) {
+    @Override
+    public APIRequestGet setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetProductGroups setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGet setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetProductGroups requestAllFields () {
+    public APIRequestGet requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetProductGroups requestAllFields (boolean value) {
+    public APIRequestGet requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetProductGroups requestFields (List<String> fields) {
+    @Override
+    public APIRequestGet requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetProductGroups requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGet requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetProductGroups requestField (String field) {
+    @Override
+    public APIRequestGet requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetProductGroups requestField (String field, boolean value) {
+    @Override
+    public APIRequestGet requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetProductGroups requestIdField () {
+    public APIRequestGet requestFilterField () {
+      return this.requestFilterField(true);
+    }
+    public APIRequestGet requestFilterField (boolean value) {
+      this.requestField("filter", value);
+      return this;
+    }
+    public APIRequestGet requestIdField () {
       return this.requestIdField(true);
     }
-    public APIRequestGetProductGroups requestIdField (boolean value) {
+    public APIRequestGet requestIdField (boolean value) {
       this.requestField("id", value);
       return this;
     }
-    public APIRequestGetProductGroups requestRetailerIdField () {
-      return this.requestRetailerIdField(true);
+    public APIRequestGet requestNameField () {
+      return this.requestNameField(true);
     }
-    public APIRequestGetProductGroups requestRetailerIdField (boolean value) {
-      this.requestField("retailer_id", value);
+    public APIRequestGet requestNameField (boolean value) {
+      this.requestField("name", value);
       return this;
     }
-    public APIRequestGetProductGroups requestVariantsField () {
-      return this.requestVariantsField(true);
+    public APIRequestGet requestProductCatalogField () {
+      return this.requestProductCatalogField(true);
     }
-    public APIRequestGetProductGroups requestVariantsField (boolean value) {
-      this.requestField("variants", value);
+    public APIRequestGet requestProductCatalogField (boolean value) {
+      this.requestField("product_catalog", value);
+      return this;
+    }
+    public APIRequestGet requestProductCountField () {
+      return this.requestProductCountField(true);
+    }
+    public APIRequestGet requestProductCountField (boolean value) {
+      this.requestField("product_count", value);
+      return this;
+    }
+  }
+
+  public static class APIRequestUpdate extends APIRequest<APINode> {
+
+    APINode lastResponse = null;
+    @Override
+    public APINode getLastResponse() {
+      return lastResponse;
+    }
+    public static final String[] PARAMS = {
+      "filter",
+      "id",
+      "name",
+    };
+
+    public static final String[] FIELDS = {
+    };
+
+    @Override
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
+    }
+
+    @Override
+    public APINode execute() throws APIException {
+      return execute(new HashMap<String, Object>());
+    }
+
+    @Override
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
+    }
+
+    public APIRequestUpdate(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "POST", Arrays.asList(PARAMS));
+    }
+
+    @Override
+    public APIRequestUpdate setParam(String param, Object value) {
+      setParamInternal(param, value);
+      return this;
+    }
+
+    @Override
+    public APIRequestUpdate setParams(Map<String, Object> params) {
+      setParamsInternal(params);
+      return this;
+    }
+
+
+    public APIRequestUpdate setFilter (Object filter) {
+      this.setParam("filter", filter);
+      return this;
+    }
+    public APIRequestUpdate setFilter (String filter) {
+      this.setParam("filter", filter);
+      return this;
+    }
+
+    public APIRequestUpdate setId (String id) {
+      this.setParam("id", id);
+      return this;
+    }
+
+    public APIRequestUpdate setName (String name) {
+      this.setParam("name", name);
+      return this;
+    }
+
+    public APIRequestUpdate requestAllFields () {
+      return this.requestAllFields(true);
+    }
+
+    public APIRequestUpdate requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestUpdate requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
+    }
+
+    @Override
+    public APIRequestUpdate requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestUpdate requestField (String field) {
+      this.requestField(field, true);
+      return this;
+    }
+
+    @Override
+    public APIRequestUpdate requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
 
@@ -1011,18 +1150,19 @@ public class ProductSet extends APINode {
   }
 
   public ProductSet copyFrom(ProductSet instance) {
+    this.mFilter = instance.mFilter;
     this.mId = instance.mId;
     this.mName = instance.mName;
-    this.mFilter = instance.mFilter;
+    this.mProductCatalog = instance.mProductCatalog;
     this.mProductCount = instance.mProductCount;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<ProductSet> getParser() {
     return new APIRequest.ResponseParser<ProductSet>() {
-      public APINodeList<ProductSet> parseResponse(String response, APIContext context, APIRequest<ProductSet> request) {
+      public APINodeList<ProductSet> parseResponse(String response, APIContext context, APIRequest<ProductSet> request) throws MalformedResponseException {
         return ProductSet.parseResponse(response, context, request);
       }
     };
