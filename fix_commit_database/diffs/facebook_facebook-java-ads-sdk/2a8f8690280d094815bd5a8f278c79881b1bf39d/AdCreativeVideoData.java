@@ -24,44 +24,49 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AdCreativeVideoData extends APINode {
-  @SerializedName("video_id")
-  private String mVideoId = null;
-  @SerializedName("title")
-  private String mTitle = null;
-  @SerializedName("description")
-  private String mDescription = null;
+  @SerializedName("branded_content_sponsor_page_id")
+  private String mBrandedContentSponsorPageId = null;
   @SerializedName("call_to_action")
   private AdCreativeLinkDataCallToAction mCallToAction = null;
-  @SerializedName("image_url")
-  private String mImageUrl = null;
+  @SerializedName("description")
+  private String mDescription = null;
   @SerializedName("image_hash")
   private String mImageHash = null;
+  @SerializedName("image_url")
+  private String mImageUrl = null;
   @SerializedName("targeting")
   private Targeting mTargeting = null;
+  @SerializedName("title")
+  private String mTitle = null;
+  @SerializedName("video_id")
+  private String mVideoId = null;
   protected static Gson gson = null;
 
   public AdCreativeVideoData() {
@@ -79,22 +84,23 @@ public class AdCreativeVideoData extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    adCreativeVideoData.mContext = context;
+    adCreativeVideoData.context = context;
     adCreativeVideoData.rawValue = json;
     return adCreativeVideoData;
   }
 
-  public static APINodeList<AdCreativeVideoData> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AdCreativeVideoData> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AdCreativeVideoData> adCreativeVideoDatas = new APINodeList<AdCreativeVideoData>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -107,10 +113,11 @@ public class AdCreativeVideoData extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            adCreativeVideoDatas.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            adCreativeVideoDatas.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -121,7 +128,20 @@ public class AdCreativeVideoData extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            adCreativeVideoDatas.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  adCreativeVideoDatas.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              adCreativeVideoDatas.add(loadJSON(obj.toString(), context));
+            }
           }
           return adCreativeVideoDatas;
         } else if (obj.has("images")) {
@@ -132,24 +152,54 @@ public class AdCreativeVideoData extends APINode {
           }
           return adCreativeVideoDatas;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              adCreativeVideoDatas.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return adCreativeVideoDatas;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          adCreativeVideoDatas.clear();
           adCreativeVideoDatas.add(loadJSON(json, context));
           return adCreativeVideoDatas;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -158,30 +208,12 @@ public class AdCreativeVideoData extends APINode {
   }
 
 
-  public String getFieldVideoId() {
-    return mVideoId;
-  }
-
-  public AdCreativeVideoData setFieldVideoId(String value) {
-    this.mVideoId = value;
-    return this;
-  }
-
-  public String getFieldTitle() {
-    return mTitle;
+  public String getFieldBrandedContentSponsorPageId() {
+    return mBrandedContentSponsorPageId;
   }
 
-  public AdCreativeVideoData setFieldTitle(String value) {
-    this.mTitle = value;
-    return this;
-  }
-
-  public String getFieldDescription() {
-    return mDescription;
-  }
-
-  public AdCreativeVideoData setFieldDescription(String value) {
-    this.mDescription = value;
+  public AdCreativeVideoData setFieldBrandedContentSponsorPageId(String value) {
+    this.mBrandedContentSponsorPageId = value;
     return this;
   }
 
@@ -199,12 +231,12 @@ public class AdCreativeVideoData extends APINode {
     this.mCallToAction = AdCreativeLinkDataCallToAction.getGson().fromJson(value, type);
     return this;
   }
-  public String getFieldImageUrl() {
-    return mImageUrl;
+  public String getFieldDescription() {
+    return mDescription;
   }
 
-  public AdCreativeVideoData setFieldImageUrl(String value) {
-    this.mImageUrl = value;
+  public AdCreativeVideoData setFieldDescription(String value) {
+    this.mDescription = value;
     return this;
   }
 
@@ -217,6 +249,15 @@ public class AdCreativeVideoData extends APINode {
     return this;
   }
 
+  public String getFieldImageUrl() {
+    return mImageUrl;
+  }
+
+  public AdCreativeVideoData setFieldImageUrl(String value) {
+    this.mImageUrl = value;
+    return this;
+  }
+
   public Targeting getFieldTargeting() {
     return mTargeting;
   }
@@ -231,6 +272,24 @@ public class AdCreativeVideoData extends APINode {
     this.mTargeting = Targeting.getGson().fromJson(value, type);
     return this;
   }
+  public String getFieldTitle() {
+    return mTitle;
+  }
+
+  public AdCreativeVideoData setFieldTitle(String value) {
+    this.mTitle = value;
+    return this;
+  }
+
+  public String getFieldVideoId() {
+    return mVideoId;
+  }
+
+  public AdCreativeVideoData setFieldVideoId(String value) {
+    this.mVideoId = value;
+    return this;
+  }
+
 
 
 
@@ -248,21 +307,22 @@ public class AdCreativeVideoData extends APINode {
   }
 
   public AdCreativeVideoData copyFrom(AdCreativeVideoData instance) {
-    this.mVideoId = instance.mVideoId;
-    this.mTitle = instance.mTitle;
-    this.mDescription = instance.mDescription;
+    this.mBrandedContentSponsorPageId = instance.mBrandedContentSponsorPageId;
     this.mCallToAction = instance.mCallToAction;
-    this.mImageUrl = instance.mImageUrl;
+    this.mDescription = instance.mDescription;
     this.mImageHash = instance.mImageHash;
+    this.mImageUrl = instance.mImageUrl;
     this.mTargeting = instance.mTargeting;
-    this.mContext = instance.mContext;
+    this.mTitle = instance.mTitle;
+    this.mVideoId = instance.mVideoId;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AdCreativeVideoData> getParser() {
     return new APIRequest.ResponseParser<AdCreativeVideoData>() {
-      public APINodeList<AdCreativeVideoData> parseResponse(String response, APIContext context, APIRequest<AdCreativeVideoData> request) {
+      public APINodeList<AdCreativeVideoData> parseResponse(String response, APIContext context, APIRequest<AdCreativeVideoData> request) throws MalformedResponseException {
         return AdCreativeVideoData.parseResponse(response, context, request);
       }
     };
