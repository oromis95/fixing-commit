@@ -24,34 +24,37 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class ProductFeedUpload extends APINode {
-  @SerializedName("id")
-  private String mId = null;
   @SerializedName("end_time")
   private String mEndTime = null;
+  @SerializedName("id")
+  private String mId = null;
   @SerializedName("input_method")
   private EnumInputMethod mInputMethod = null;
   @SerializedName("start_time")
@@ -69,11 +72,11 @@ public class ProductFeedUpload extends APINode {
 
   public ProductFeedUpload(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public ProductFeedUpload fetch() throws APIException{
-    ProductFeedUpload newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    ProductFeedUpload newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -90,8 +93,17 @@ public class ProductFeedUpload extends APINode {
     return productFeedUpload;
   }
 
+  public static APINodeList<ProductFeedUpload> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<ProductFeedUpload>)(
+      new APIRequest<ProductFeedUpload>(context, "", "/", "GET", ProductFeedUpload.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -106,22 +118,23 @@ public class ProductFeedUpload extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    productFeedUpload.mContext = context;
+    productFeedUpload.context = context;
     productFeedUpload.rawValue = json;
     return productFeedUpload;
   }
 
-  public static APINodeList<ProductFeedUpload> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<ProductFeedUpload> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<ProductFeedUpload> productFeedUploads = new APINodeList<ProductFeedUpload>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -134,10 +147,11 @@ public class ProductFeedUpload extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            productFeedUploads.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            productFeedUploads.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -148,7 +162,20 @@ public class ProductFeedUpload extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            productFeedUploads.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  productFeedUploads.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              productFeedUploads.add(loadJSON(obj.toString(), context));
+            }
           }
           return productFeedUploads;
         } else if (obj.has("images")) {
@@ -159,24 +186,54 @@ public class ProductFeedUpload extends APINode {
           }
           return productFeedUploads;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              productFeedUploads.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return productFeedUploads;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          productFeedUploads.clear();
           productFeedUploads.add(loadJSON(json, context));
           return productFeedUploads;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -184,23 +241,23 @@ public class ProductFeedUpload extends APINode {
     return getGson().toJson(this);
   }
 
-  public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
-  }
-
   public APIRequestGetErrors getErrors() {
-    return new APIRequestGetErrors(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetErrors(this.getPrefixedId().toString(), context);
   }
 
-
-  public String getFieldId() {
-    return mId;
+  public APIRequestGet get() {
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
   }
 
+
   public String getFieldEndTime() {
     return mEndTime;
   }
 
+  public String getFieldId() {
+    return mId;
+  }
+
   public EnumInputMethod getFieldInputMethod() {
     return mInputMethod;
   }
@@ -215,279 +272,274 @@ public class ProductFeedUpload extends APINode {
 
 
 
-  public static class APIRequestGet extends APIRequest<ProductFeedUpload> {
+  public static class APIRequestGetErrors extends APIRequest<ProductFeedUploadError> {
 
-    ProductFeedUpload lastResponse = null;
+    APINodeList<ProductFeedUploadError> lastResponse = null;
     @Override
-    public ProductFeedUpload getLastResponse() {
+    public APINodeList<ProductFeedUploadError> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
     };
 
     public static final String[] FIELDS = {
+      "description",
       "id",
-      "end_time",
-      "input_method",
-      "start_time",
-      "url",
+      "severity",
+      "summary",
+      "total_count",
     };
 
     @Override
-    public ProductFeedUpload parseResponse(String response) throws APIException {
-      return ProductFeedUpload.parseResponse(response, getContext(), this).head();
+    public APINodeList<ProductFeedUploadError> parseResponse(String response) throws APIException {
+      return ProductFeedUploadError.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public ProductFeedUpload execute() throws APIException {
+    public APINodeList<ProductFeedUploadError> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public ProductFeedUpload execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<ProductFeedUploadError> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGet(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetErrors(String nodeId, APIContext context) {
+      super(context, nodeId, "/errors", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGet setParam(String param, Object value) {
+    @Override
+    public APIRequestGetErrors setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGet setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetErrors setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGet requestAllFields () {
+    public APIRequestGetErrors requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGet requestAllFields (boolean value) {
+    public APIRequestGetErrors requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetErrors requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGet requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetErrors requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestField (String field) {
+    @Override
+    public APIRequestGetErrors requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGet requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetErrors requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGet requestIdField () {
-      return this.requestIdField(true);
+    public APIRequestGetErrors requestDescriptionField () {
+      return this.requestDescriptionField(true);
     }
-    public APIRequestGet requestIdField (boolean value) {
-      this.requestField("id", value);
+    public APIRequestGetErrors requestDescriptionField (boolean value) {
+      this.requestField("description", value);
       return this;
     }
-    public APIRequestGet requestEndTimeField () {
-      return this.requestEndTimeField(true);
+    public APIRequestGetErrors requestIdField () {
+      return this.requestIdField(true);
     }
-    public APIRequestGet requestEndTimeField (boolean value) {
-      this.requestField("end_time", value);
+    public APIRequestGetErrors requestIdField (boolean value) {
+      this.requestField("id", value);
       return this;
     }
-    public APIRequestGet requestInputMethodField () {
-      return this.requestInputMethodField(true);
+    public APIRequestGetErrors requestSeverityField () {
+      return this.requestSeverityField(true);
     }
-    public APIRequestGet requestInputMethodField (boolean value) {
-      this.requestField("input_method", value);
+    public APIRequestGetErrors requestSeverityField (boolean value) {
+      this.requestField("severity", value);
       return this;
     }
-    public APIRequestGet requestStartTimeField () {
-      return this.requestStartTimeField(true);
+    public APIRequestGetErrors requestSummaryField () {
+      return this.requestSummaryField(true);
     }
-    public APIRequestGet requestStartTimeField (boolean value) {
-      this.requestField("start_time", value);
+    public APIRequestGetErrors requestSummaryField (boolean value) {
+      this.requestField("summary", value);
       return this;
     }
-    public APIRequestGet requestUrlField () {
-      return this.requestUrlField(true);
+    public APIRequestGetErrors requestTotalCountField () {
+      return this.requestTotalCountField(true);
     }
-    public APIRequestGet requestUrlField (boolean value) {
-      this.requestField("url", value);
+    public APIRequestGetErrors requestTotalCountField (boolean value) {
+      this.requestField("total_count", value);
       return this;
     }
-
   }
 
-  public static class APIRequestGetErrors extends APIRequest<ProductFeedUploadError> {
+  public static class APIRequestGet extends APIRequest<ProductFeedUpload> {
 
-    APINodeList<ProductFeedUploadError> lastResponse = null;
+    ProductFeedUpload lastResponse = null;
     @Override
-    public APINodeList<ProductFeedUploadError> getLastResponse() {
+    public ProductFeedUpload getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
     };
 
     public static final String[] FIELDS = {
+      "end_time",
       "id",
-      "summary",
-      "description",
-      "severity",
-      "row_number",
-      "column_number",
-      "total_count",
+      "input_method",
+      "start_time",
+      "url",
     };
 
     @Override
-    public APINodeList<ProductFeedUploadError> parseResponse(String response) throws APIException {
-      return ProductFeedUploadError.parseResponse(response, getContext(), this);
+    public ProductFeedUpload parseResponse(String response) throws APIException {
+      return ProductFeedUpload.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public APINodeList<ProductFeedUploadError> execute() throws APIException {
+    public ProductFeedUpload execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<ProductFeedUploadError> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public ProductFeedUpload execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetErrors(String nodeId, APIContext context) {
-      super(context, nodeId, "/errors", "GET", Arrays.asList(PARAMS));
+    public APIRequestGet(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetErrors setParam(String param, Object value) {
+    @Override
+    public APIRequestGet setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetErrors setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGet setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetErrors requestAllFields () {
+    public APIRequestGet requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetErrors requestAllFields (boolean value) {
+    public APIRequestGet requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetErrors requestFields (List<String> fields) {
+    @Override
+    public APIRequestGet requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetErrors requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGet requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetErrors requestField (String field) {
+    @Override
+    public APIRequestGet requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetErrors requestField (String field, boolean value) {
+    @Override
+    public APIRequestGet requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetErrors requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGetErrors requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
-    }
-    public APIRequestGetErrors requestSummaryField () {
-      return this.requestSummaryField(true);
-    }
-    public APIRequestGetErrors requestSummaryField (boolean value) {
-      this.requestField("summary", value);
-      return this;
-    }
-    public APIRequestGetErrors requestDescriptionField () {
-      return this.requestDescriptionField(true);
+    public APIRequestGet requestEndTimeField () {
+      return this.requestEndTimeField(true);
     }
-    public APIRequestGetErrors requestDescriptionField (boolean value) {
-      this.requestField("description", value);
+    public APIRequestGet requestEndTimeField (boolean value) {
+      this.requestField("end_time", value);
       return this;
     }
-    public APIRequestGetErrors requestSeverityField () {
-      return this.requestSeverityField(true);
+    public APIRequestGet requestIdField () {
+      return this.requestIdField(true);
     }
-    public APIRequestGetErrors requestSeverityField (boolean value) {
-      this.requestField("severity", value);
+    public APIRequestGet requestIdField (boolean value) {
+      this.requestField("id", value);
       return this;
     }
-    public APIRequestGetErrors requestRowNumberField () {
-      return this.requestRowNumberField(true);
+    public APIRequestGet requestInputMethodField () {
+      return this.requestInputMethodField(true);
     }
-    public APIRequestGetErrors requestRowNumberField (boolean value) {
-      this.requestField("row_number", value);
+    public APIRequestGet requestInputMethodField (boolean value) {
+      this.requestField("input_method", value);
       return this;
     }
-    public APIRequestGetErrors requestColumnNumberField () {
-      return this.requestColumnNumberField(true);
+    public APIRequestGet requestStartTimeField () {
+      return this.requestStartTimeField(true);
     }
-    public APIRequestGetErrors requestColumnNumberField (boolean value) {
-      this.requestField("column_number", value);
+    public APIRequestGet requestStartTimeField (boolean value) {
+      this.requestField("start_time", value);
       return this;
     }
-    public APIRequestGetErrors requestTotalCountField () {
-      return this.requestTotalCountField(true);
+    public APIRequestGet requestUrlField () {
+      return this.requestUrlField(true);
     }
-    public APIRequestGetErrors requestTotalCountField (boolean value) {
-      this.requestField("total_count", value);
+    public APIRequestGet requestUrlField (boolean value) {
+      this.requestField("url", value);
       return this;
     }
-
   }
 
   public static enum EnumInputMethod {
-    @SerializedName("Manual Upload")
-    VALUE_MANUAL_UPLOAD("Manual Upload"),
-    @SerializedName("Server Fetch")
-    VALUE_SERVER_FETCH("Server Fetch"),
-    NULL(null);
+      @SerializedName("Manual Upload")
+      VALUE_MANUAL_UPLOAD("Manual Upload"),
+      @SerializedName("Server Fetch")
+      VALUE_SERVER_FETCH("Server Fetch"),
+      NULL(null);
 
-    private String value;
+      private String value;
 
-    private EnumInputMethod(String value) {
-      this.value = value;
-    }
+      private EnumInputMethod(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
 
+
   synchronized /*package*/ static Gson getGson() {
     if (gson != null) {
       return gson;
@@ -502,19 +554,19 @@ public class ProductFeedUpload extends APINode {
   }
 
   public ProductFeedUpload copyFrom(ProductFeedUpload instance) {
-    this.mId = instance.mId;
     this.mEndTime = instance.mEndTime;
+    this.mId = instance.mId;
     this.mInputMethod = instance.mInputMethod;
     this.mStartTime = instance.mStartTime;
     this.mUrl = instance.mUrl;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<ProductFeedUpload> getParser() {
     return new APIRequest.ResponseParser<ProductFeedUpload>() {
-      public APINodeList<ProductFeedUpload> parseResponse(String response, APIContext context, APIRequest<ProductFeedUpload> request) {
+      public APINodeList<ProductFeedUpload> parseResponse(String response, APIContext context, APIRequest<ProductFeedUpload> request) throws MalformedResponseException {
         return ProductFeedUpload.parseResponse(response, context, request);
       }
     };
