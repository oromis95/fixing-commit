@@ -24,48 +24,51 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AppLinks extends APINode {
+  @SerializedName("android")
+  private List<AndroidAppLink> mAndroid = null;
   @SerializedName("id")
   private String mId = null;
   @SerializedName("ios")
   private List<IosAppLink> mIos = null;
-  @SerializedName("iphone")
-  private List<IosAppLink> mIphone = null;
   @SerializedName("ipad")
   private List<IosAppLink> mIpad = null;
-  @SerializedName("android")
-  private List<AndroidAppLink> mAndroid = null;
-  @SerializedName("windows_phone")
-  private List<WindowsPhoneAppLink> mWindowsPhone = null;
+  @SerializedName("iphone")
+  private List<IosAppLink> mIphone = null;
+  @SerializedName("web")
+  private WebAppLink mWeb = null;
   @SerializedName("windows")
   private List<WindowsAppLink> mWindows = null;
+  @SerializedName("windows_phone")
+  private List<WindowsPhoneAppLink> mWindowsPhone = null;
   @SerializedName("windows_universal")
   private List<WindowsAppLink> mWindowsUniversal = null;
-  @SerializedName("web")
-  private WebAppLink mWeb = null;
   protected static Gson gson = null;
 
   AppLinks() {
@@ -77,11 +80,11 @@ public class AppLinks extends APINode {
 
   public AppLinks(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public AppLinks fetch() throws APIException{
-    AppLinks newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    AppLinks newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -98,8 +101,17 @@ public class AppLinks extends APINode {
     return appLinks;
   }
 
+  public static APINodeList<AppLinks> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<AppLinks>)(
+      new APIRequest<AppLinks>(context, "", "/", "GET", AppLinks.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -114,22 +126,23 @@ public class AppLinks extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    appLinks.mContext = context;
+    appLinks.context = context;
     appLinks.rawValue = json;
     return appLinks;
   }
 
-  public static APINodeList<AppLinks> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AppLinks> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AppLinks> appLinkss = new APINodeList<AppLinks>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -142,10 +155,11 @@ public class AppLinks extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            appLinkss.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            appLinkss.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -156,7 +170,20 @@ public class AppLinks extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            appLinkss.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  appLinkss.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              appLinkss.add(loadJSON(obj.toString(), context));
+            }
           }
           return appLinkss;
         } else if (obj.has("images")) {
@@ -167,24 +194,54 @@ public class AppLinks extends APINode {
           }
           return appLinkss;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              appLinkss.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return appLinkss;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          appLinkss.clear();
           appLinkss.add(loadJSON(json, context));
           return appLinkss;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -193,10 +250,14 @@ public class AppLinks extends APINode {
   }
 
   public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
   }
 
 
+  public List<AndroidAppLink> getFieldAndroid() {
+    return mAndroid;
+  }
+
   public String getFieldId() {
     return mId;
   }
@@ -205,32 +266,28 @@ public class AppLinks extends APINode {
     return mIos;
   }
 
-  public List<IosAppLink> getFieldIphone() {
-    return mIphone;
-  }
-
   public List<IosAppLink> getFieldIpad() {
     return mIpad;
   }
 
-  public List<AndroidAppLink> getFieldAndroid() {
-    return mAndroid;
+  public List<IosAppLink> getFieldIphone() {
+    return mIphone;
   }
 
-  public List<WindowsPhoneAppLink> getFieldWindowsPhone() {
-    return mWindowsPhone;
+  public WebAppLink getFieldWeb() {
+    return mWeb;
   }
 
   public List<WindowsAppLink> getFieldWindows() {
     return mWindows;
   }
 
-  public List<WindowsAppLink> getFieldWindowsUniversal() {
-    return mWindowsUniversal;
+  public List<WindowsPhoneAppLink> getFieldWindowsPhone() {
+    return mWindowsPhone;
   }
 
-  public WebAppLink getFieldWeb() {
-    return mWeb;
+  public List<WindowsAppLink> getFieldWindowsUniversal() {
+    return mWindowsUniversal;
   }
 
 
@@ -246,15 +303,15 @@ public class AppLinks extends APINode {
     };
 
     public static final String[] FIELDS = {
+      "android",
       "id",
       "ios",
-      "iphone",
       "ipad",
-      "android",
-      "windows_phone",
+      "iphone",
+      "web",
       "windows",
+      "windows_phone",
       "windows_universal",
-      "web",
     };
 
     @Override
@@ -269,7 +326,7 @@ public class AppLinks extends APINode {
 
     @Override
     public AppLinks execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -277,11 +334,13 @@ public class AppLinks extends APINode {
       super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestGet setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestGet setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
@@ -299,10 +358,12 @@ public class AppLinks extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGet requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestGet requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -310,16 +371,25 @@ public class AppLinks extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGet requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGet requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
+    public APIRequestGet requestAndroidField () {
+      return this.requestAndroidField(true);
+    }
+    public APIRequestGet requestAndroidField (boolean value) {
+      this.requestField("android", value);
+      return this;
+    }
     public APIRequestGet requestIdField () {
       return this.requestIdField(true);
     }
@@ -334,13 +404,6 @@ public class AppLinks extends APINode {
       this.requestField("ios", value);
       return this;
     }
-    public APIRequestGet requestIphoneField () {
-      return this.requestIphoneField(true);
-    }
-    public APIRequestGet requestIphoneField (boolean value) {
-      this.requestField("iphone", value);
-      return this;
-    }
     public APIRequestGet requestIpadField () {
       return this.requestIpadField(true);
     }
@@ -348,18 +411,18 @@ public class AppLinks extends APINode {
       this.requestField("ipad", value);
       return this;
     }
-    public APIRequestGet requestAndroidField () {
-      return this.requestAndroidField(true);
+    public APIRequestGet requestIphoneField () {
+      return this.requestIphoneField(true);
     }
-    public APIRequestGet requestAndroidField (boolean value) {
-      this.requestField("android", value);
+    public APIRequestGet requestIphoneField (boolean value) {
+      this.requestField("iphone", value);
       return this;
     }
-    public APIRequestGet requestWindowsPhoneField () {
-      return this.requestWindowsPhoneField(true);
+    public APIRequestGet requestWebField () {
+      return this.requestWebField(true);
     }
-    public APIRequestGet requestWindowsPhoneField (boolean value) {
-      this.requestField("windows_phone", value);
+    public APIRequestGet requestWebField (boolean value) {
+      this.requestField("web", value);
       return this;
     }
     public APIRequestGet requestWindowsField () {
@@ -369,6 +432,13 @@ public class AppLinks extends APINode {
       this.requestField("windows", value);
       return this;
     }
+    public APIRequestGet requestWindowsPhoneField () {
+      return this.requestWindowsPhoneField(true);
+    }
+    public APIRequestGet requestWindowsPhoneField (boolean value) {
+      this.requestField("windows_phone", value);
+      return this;
+    }
     public APIRequestGet requestWindowsUniversalField () {
       return this.requestWindowsUniversalField(true);
     }
@@ -376,14 +446,6 @@ public class AppLinks extends APINode {
       this.requestField("windows_universal", value);
       return this;
     }
-    public APIRequestGet requestWebField () {
-      return this.requestWebField(true);
-    }
-    public APIRequestGet requestWebField (boolean value) {
-      this.requestField("web", value);
-      return this;
-    }
-
   }
 
 
@@ -401,23 +463,23 @@ public class AppLinks extends APINode {
   }
 
   public AppLinks copyFrom(AppLinks instance) {
+    this.mAndroid = instance.mAndroid;
     this.mId = instance.mId;
     this.mIos = instance.mIos;
-    this.mIphone = instance.mIphone;
     this.mIpad = instance.mIpad;
-    this.mAndroid = instance.mAndroid;
-    this.mWindowsPhone = instance.mWindowsPhone;
+    this.mIphone = instance.mIphone;
+    this.mWeb = instance.mWeb;
     this.mWindows = instance.mWindows;
+    this.mWindowsPhone = instance.mWindowsPhone;
     this.mWindowsUniversal = instance.mWindowsUniversal;
-    this.mWeb = instance.mWeb;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AppLinks> getParser() {
     return new APIRequest.ResponseParser<AppLinks>() {
-      public APINodeList<AppLinks> parseResponse(String response, APIContext context, APIRequest<AppLinks> request) {
+      public APINodeList<AppLinks> parseResponse(String response, APIContext context, APIRequest<AppLinks> request) throws MalformedResponseException {
         return AppLinks.parseResponse(response, context, request);
       }
     };
