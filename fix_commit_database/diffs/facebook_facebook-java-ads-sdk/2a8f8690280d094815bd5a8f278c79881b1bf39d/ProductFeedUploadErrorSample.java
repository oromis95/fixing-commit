@@ -24,29 +24,32 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class ProductFeedUploadErrorSample extends APINode {
   @SerializedName("id")
   private String mId = null;
@@ -65,11 +68,11 @@ public class ProductFeedUploadErrorSample extends APINode {
 
   public ProductFeedUploadErrorSample(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public ProductFeedUploadErrorSample fetch() throws APIException{
-    ProductFeedUploadErrorSample newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    ProductFeedUploadErrorSample newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -86,8 +89,17 @@ public class ProductFeedUploadErrorSample extends APINode {
     return productFeedUploadErrorSample;
   }
 
+  public static APINodeList<ProductFeedUploadErrorSample> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<ProductFeedUploadErrorSample>)(
+      new APIRequest<ProductFeedUploadErrorSample>(context, "", "/", "GET", ProductFeedUploadErrorSample.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -102,22 +114,23 @@ public class ProductFeedUploadErrorSample extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    productFeedUploadErrorSample.mContext = context;
+    productFeedUploadErrorSample.context = context;
     productFeedUploadErrorSample.rawValue = json;
     return productFeedUploadErrorSample;
   }
 
-  public static APINodeList<ProductFeedUploadErrorSample> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<ProductFeedUploadErrorSample> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<ProductFeedUploadErrorSample> productFeedUploadErrorSamples = new APINodeList<ProductFeedUploadErrorSample>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -130,10 +143,11 @@ public class ProductFeedUploadErrorSample extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            productFeedUploadErrorSamples.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            productFeedUploadErrorSamples.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -144,7 +158,20 @@ public class ProductFeedUploadErrorSample extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            productFeedUploadErrorSamples.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  productFeedUploadErrorSamples.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              productFeedUploadErrorSamples.add(loadJSON(obj.toString(), context));
+            }
           }
           return productFeedUploadErrorSamples;
         } else if (obj.has("images")) {
@@ -155,24 +182,54 @@ public class ProductFeedUploadErrorSample extends APINode {
           }
           return productFeedUploadErrorSamples;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              productFeedUploadErrorSamples.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return productFeedUploadErrorSamples;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          productFeedUploadErrorSamples.clear();
           productFeedUploadErrorSamples.add(loadJSON(json, context));
           return productFeedUploadErrorSamples;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -181,7 +238,7 @@ public class ProductFeedUploadErrorSample extends APINode {
   }
 
   public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
   }
 
 
@@ -227,7 +284,7 @@ public class ProductFeedUploadErrorSample extends APINode {
 
     @Override
     public ProductFeedUploadErrorSample execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -235,11 +292,13 @@ public class ProductFeedUploadErrorSample extends APINode {
       super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestGet setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestGet setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
@@ -257,10 +316,12 @@ public class ProductFeedUploadErrorSample extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGet requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestGet requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -268,11 +329,13 @@ public class ProductFeedUploadErrorSample extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGet requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGet requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
@@ -299,7 +362,6 @@ public class ProductFeedUploadErrorSample extends APINode {
       this.requestField("row_number", value);
       return this;
     }
-
   }
 
 
@@ -320,14 +382,14 @@ public class ProductFeedUploadErrorSample extends APINode {
     this.mId = instance.mId;
     this.mRetailerId = instance.mRetailerId;
     this.mRowNumber = instance.mRowNumber;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<ProductFeedUploadErrorSample> getParser() {
     return new APIRequest.ResponseParser<ProductFeedUploadErrorSample>() {
-      public APINodeList<ProductFeedUploadErrorSample> parseResponse(String response, APIContext context, APIRequest<ProductFeedUploadErrorSample> request) {
+      public APINodeList<ProductFeedUploadErrorSample> parseResponse(String response, APIContext context, APIRequest<ProductFeedUploadErrorSample> request) throws MalformedResponseException {
         return ProductFeedUploadErrorSample.parseResponse(response, context, request);
       }
     };
