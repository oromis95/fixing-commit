@@ -24,80 +24,83 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class ConnectionObject extends APINode {
+  @SerializedName("app_installs_tracked")
+  private Boolean mAppInstallsTracked = null;
+  @SerializedName("checkin_capable")
+  private Boolean mCheckinCapable = null;
+  @SerializedName("cpa_access")
+  private Map<String, Boolean> mCpaAccess = null;
+  @SerializedName("event_is_viewer_admin")
+  private Boolean mEventIsViewerAdmin = null;
+  @SerializedName("event_parent_page_id")
+  private String mEventParentPageId = null;
+  @SerializedName("event_parent_page_name")
+  private String mEventParentPageName = null;
+  @SerializedName("event_start_timestamp")
+  private Long mEventStartTimestamp = null;
+  @SerializedName("icon_url")
+  private String mIconUrl = null;
   @SerializedName("id")
   private String mId = null;
+  @SerializedName("is_game")
+  private Boolean mIsGame = null;
+  @SerializedName("logo_url")
+  private String mLogoUrl = null;
   @SerializedName("name")
   private String mName = null;
   @SerializedName("name_with_location_descriptor")
   private String mNameWithLocationDescriptor = null;
-  @SerializedName("url")
-  private String mUrl = null;
-  @SerializedName("type")
-  private Long mType = null;
-  @SerializedName("checkin_capable")
-  private Boolean mCheckinCapable = null;
-  @SerializedName("picture")
-  private String mPicture = null;
-  @SerializedName("tabs")
-  private Map<String, String> mTabs = null;
-  @SerializedName("is_game")
-  private Boolean mIsGame = null;
-  @SerializedName("supported_platforms")
-  private List<Long> mSupportedPlatforms = null;
   @SerializedName("native_app_store_ids")
   private Map<String, String> mNativeAppStoreIds = null;
-  @SerializedName("object_store_urls")
-  private Map<String, String> mObjectStoreUrls = null;
-  @SerializedName("app_installs_tracked")
-  private Boolean mAppInstallsTracked = null;
   @SerializedName("native_app_targeting_ids")
   private Map<String, String> mNativeAppTargetingIds = null;
-  @SerializedName("og_namespace")
-  private String mOgNamespace = null;
+  @SerializedName("object_store_urls")
+  private Map<String, String> mObjectStoreUrls = null;
   @SerializedName("og_actions")
   private List<ConnectionObjectOpenGraphAction> mOgActions = null;
+  @SerializedName("og_namespace")
+  private String mOgNamespace = null;
   @SerializedName("og_objects")
   private List<ConnectionObjectOpenGraphObject> mOgObjects = null;
+  @SerializedName("picture")
+  private String mPicture = null;
+  @SerializedName("supported_platforms")
+  private List<Long> mSupportedPlatforms = null;
+  @SerializedName("tabs")
+  private Map<String, String> mTabs = null;
+  @SerializedName("type")
+  private Long mType = null;
+  @SerializedName("url")
+  private String mUrl = null;
   @SerializedName("website")
   private String mWebsite = null;
-  @SerializedName("logo_url")
-  private String mLogoUrl = null;
-  @SerializedName("icon_url")
-  private String mIconUrl = null;
-  @SerializedName("event_parent_page_id")
-  private String mEventParentPageId = null;
-  @SerializedName("event_parent_page_name")
-  private String mEventParentPageName = null;
-  @SerializedName("event_start_timestamp")
-  private Long mEventStartTimestamp = null;
-  @SerializedName("event_is_viewer_admin")
-  private Boolean mEventIsViewerAdmin = null;
-  @SerializedName("cpa_access")
-  private Map<String, Boolean> mCpaAccess = null;
   protected static Gson gson = null;
 
   public ConnectionObject() {
@@ -115,22 +118,23 @@ public class ConnectionObject extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    connectionObject.mContext = context;
+    connectionObject.context = context;
     connectionObject.rawValue = json;
     return connectionObject;
   }
 
-  public static APINodeList<ConnectionObject> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<ConnectionObject> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<ConnectionObject> connectionObjects = new APINodeList<ConnectionObject>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -143,10 +147,11 @@ public class ConnectionObject extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            connectionObjects.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            connectionObjects.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -157,7 +162,20 @@ public class ConnectionObject extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            connectionObjects.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  connectionObjects.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              connectionObjects.add(loadJSON(obj.toString(), context));
+            }
           }
           return connectionObjects;
         } else if (obj.has("images")) {
@@ -168,24 +186,54 @@ public class ConnectionObject extends APINode {
           }
           return connectionObjects;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              connectionObjects.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return connectionObjects;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          connectionObjects.clear();
           connectionObjects.add(loadJSON(json, context));
           return connectionObjects;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -194,75 +242,84 @@ public class ConnectionObject extends APINode {
   }
 
 
-  public String getFieldId() {
-    return mId;
+  public Boolean getFieldAppInstallsTracked() {
+    return mAppInstallsTracked;
   }
 
-  public ConnectionObject setFieldId(String value) {
-    this.mId = value;
+  public ConnectionObject setFieldAppInstallsTracked(Boolean value) {
+    this.mAppInstallsTracked = value;
     return this;
   }
 
-  public String getFieldName() {
-    return mName;
+  public Boolean getFieldCheckinCapable() {
+    return mCheckinCapable;
   }
 
-  public ConnectionObject setFieldName(String value) {
-    this.mName = value;
+  public ConnectionObject setFieldCheckinCapable(Boolean value) {
+    this.mCheckinCapable = value;
     return this;
   }
 
-  public String getFieldNameWithLocationDescriptor() {
-    return mNameWithLocationDescriptor;
+  public Map<String, Boolean> getFieldCpaAccess() {
+    return mCpaAccess;
   }
 
-  public ConnectionObject setFieldNameWithLocationDescriptor(String value) {
-    this.mNameWithLocationDescriptor = value;
+  public ConnectionObject setFieldCpaAccess(Map<String, Boolean> value) {
+    this.mCpaAccess = value;
     return this;
   }
 
-  public String getFieldUrl() {
-    return mUrl;
+  public Boolean getFieldEventIsViewerAdmin() {
+    return mEventIsViewerAdmin;
   }
 
-  public ConnectionObject setFieldUrl(String value) {
-    this.mUrl = value;
+  public ConnectionObject setFieldEventIsViewerAdmin(Boolean value) {
+    this.mEventIsViewerAdmin = value;
     return this;
   }
 
-  public Long getFieldType() {
-    return mType;
+  public String getFieldEventParentPageId() {
+    return mEventParentPageId;
   }
 
-  public ConnectionObject setFieldType(Long value) {
-    this.mType = value;
+  public ConnectionObject setFieldEventParentPageId(String value) {
+    this.mEventParentPageId = value;
     return this;
   }
 
-  public Boolean getFieldCheckinCapable() {
-    return mCheckinCapable;
+  public String getFieldEventParentPageName() {
+    return mEventParentPageName;
   }
 
-  public ConnectionObject setFieldCheckinCapable(Boolean value) {
-    this.mCheckinCapable = value;
+  public ConnectionObject setFieldEventParentPageName(String value) {
+    this.mEventParentPageName = value;
     return this;
   }
 
-  public String getFieldPicture() {
-    return mPicture;
+  public Long getFieldEventStartTimestamp() {
+    return mEventStartTimestamp;
   }
 
-  public ConnectionObject setFieldPicture(String value) {
-    this.mPicture = value;
+  public ConnectionObject setFieldEventStartTimestamp(Long value) {
+    this.mEventStartTimestamp = value;
     return this;
   }
 
-  public Map<String, String> getFieldTabs() {
-    return mTabs;
+  public String getFieldIconUrl() {
+    return mIconUrl;
   }
 
-  public ConnectionObject setFieldTabs(Map<String, String> value) {
-    this.mTabs = value;
+  public ConnectionObject setFieldIconUrl(String value) {
+    this.mIconUrl = value;
+    return this;
+  }
+
+  public String getFieldId() {
+    return mId;
+  }
+
+  public ConnectionObject setFieldId(String value) {
+    this.mId = value;
     return this;
   }
 
@@ -275,39 +332,39 @@ public class ConnectionObject extends APINode {
     return this;
   }
 
-  public List<Long> getFieldSupportedPlatforms() {
-    return mSupportedPlatforms;
+  public String getFieldLogoUrl() {
+    return mLogoUrl;
   }
 
-  public ConnectionObject setFieldSupportedPlatforms(List<Long> value) {
-    this.mSupportedPlatforms = value;
+  public ConnectionObject setFieldLogoUrl(String value) {
+    this.mLogoUrl = value;
     return this;
   }
 
-  public Map<String, String> getFieldNativeAppStoreIds() {
-    return mNativeAppStoreIds;
+  public String getFieldName() {
+    return mName;
   }
 
-  public ConnectionObject setFieldNativeAppStoreIds(Map<String, String> value) {
-    this.mNativeAppStoreIds = value;
+  public ConnectionObject setFieldName(String value) {
+    this.mName = value;
     return this;
   }
 
-  public Map<String, String> getFieldObjectStoreUrls() {
-    return mObjectStoreUrls;
+  public String getFieldNameWithLocationDescriptor() {
+    return mNameWithLocationDescriptor;
   }
 
-  public ConnectionObject setFieldObjectStoreUrls(Map<String, String> value) {
-    this.mObjectStoreUrls = value;
+  public ConnectionObject setFieldNameWithLocationDescriptor(String value) {
+    this.mNameWithLocationDescriptor = value;
     return this;
   }
 
-  public Boolean getFieldAppInstallsTracked() {
-    return mAppInstallsTracked;
+  public Map<String, String> getFieldNativeAppStoreIds() {
+    return mNativeAppStoreIds;
   }
 
-  public ConnectionObject setFieldAppInstallsTracked(Boolean value) {
-    this.mAppInstallsTracked = value;
+  public ConnectionObject setFieldNativeAppStoreIds(Map<String, String> value) {
+    this.mNativeAppStoreIds = value;
     return this;
   }
 
@@ -320,12 +377,12 @@ public class ConnectionObject extends APINode {
     return this;
   }
 
-  public String getFieldOgNamespace() {
-    return mOgNamespace;
+  public Map<String, String> getFieldObjectStoreUrls() {
+    return mObjectStoreUrls;
   }
 
-  public ConnectionObject setFieldOgNamespace(String value) {
-    this.mOgNamespace = value;
+  public ConnectionObject setFieldObjectStoreUrls(Map<String, String> value) {
+    this.mObjectStoreUrls = value;
     return this;
   }
 
@@ -343,6 +400,15 @@ public class ConnectionObject extends APINode {
     this.mOgActions = ConnectionObjectOpenGraphAction.getGson().fromJson(value, type);
     return this;
   }
+  public String getFieldOgNamespace() {
+    return mOgNamespace;
+  }
+
+  public ConnectionObject setFieldOgNamespace(String value) {
+    this.mOgNamespace = value;
+    return this;
+  }
+
   public List<ConnectionObjectOpenGraphObject> getFieldOgObjects() {
     return mOgObjects;
   }
@@ -357,75 +423,57 @@ public class ConnectionObject extends APINode {
     this.mOgObjects = ConnectionObjectOpenGraphObject.getGson().fromJson(value, type);
     return this;
   }
-  public String getFieldWebsite() {
-    return mWebsite;
-  }
-
-  public ConnectionObject setFieldWebsite(String value) {
-    this.mWebsite = value;
-    return this;
-  }
-
-  public String getFieldLogoUrl() {
-    return mLogoUrl;
-  }
-
-  public ConnectionObject setFieldLogoUrl(String value) {
-    this.mLogoUrl = value;
-    return this;
-  }
-
-  public String getFieldIconUrl() {
-    return mIconUrl;
+  public String getFieldPicture() {
+    return mPicture;
   }
 
-  public ConnectionObject setFieldIconUrl(String value) {
-    this.mIconUrl = value;
+  public ConnectionObject setFieldPicture(String value) {
+    this.mPicture = value;
     return this;
   }
 
-  public String getFieldEventParentPageId() {
-    return mEventParentPageId;
+  public List<Long> getFieldSupportedPlatforms() {
+    return mSupportedPlatforms;
   }
 
-  public ConnectionObject setFieldEventParentPageId(String value) {
-    this.mEventParentPageId = value;
+  public ConnectionObject setFieldSupportedPlatforms(List<Long> value) {
+    this.mSupportedPlatforms = value;
     return this;
   }
 
-  public String getFieldEventParentPageName() {
-    return mEventParentPageName;
+  public Map<String, String> getFieldTabs() {
+    return mTabs;
   }
 
-  public ConnectionObject setFieldEventParentPageName(String value) {
-    this.mEventParentPageName = value;
+  public ConnectionObject setFieldTabs(Map<String, String> value) {
+    this.mTabs = value;
     return this;
   }
 
-  public Long getFieldEventStartTimestamp() {
-    return mEventStartTimestamp;
+  public Long getFieldType() {
+    return mType;
   }
 
-  public ConnectionObject setFieldEventStartTimestamp(Long value) {
-    this.mEventStartTimestamp = value;
+  public ConnectionObject setFieldType(Long value) {
+    this.mType = value;
     return this;
   }
 
-  public Boolean getFieldEventIsViewerAdmin() {
-    return mEventIsViewerAdmin;
+  public String getFieldUrl() {
+    return mUrl;
   }
 
-  public ConnectionObject setFieldEventIsViewerAdmin(Boolean value) {
-    this.mEventIsViewerAdmin = value;
+  public ConnectionObject setFieldUrl(String value) {
+    this.mUrl = value;
     return this;
   }
 
-  public Map<String, Boolean> getFieldCpaAccess() {
-    return mCpaAccess;
+  public String getFieldWebsite() {
+    return mWebsite;
   }
 
-  public ConnectionObject setFieldCpaAccess(Map<String, Boolean> value) {
-    this.mCpaAccess = value;
+  public ConnectionObject setFieldWebsite(String value) {
+    this.mWebsite = value;
     return this;
   }
 
@@ -446,39 +494,39 @@ public class ConnectionObject extends APINode {
   }
 
   public ConnectionObject copyFrom(ConnectionObject instance) {
+    this.mAppInstallsTracked = instance.mAppInstallsTracked;
+    this.mCheckinCapable = instance.mCheckinCapable;
+    this.mCpaAccess = instance.mCpaAccess;
+    this.mEventIsViewerAdmin = instance.mEventIsViewerAdmin;
+    this.mEventParentPageId = instance.mEventParentPageId;
+    this.mEventParentPageName = instance.mEventParentPageName;
+    this.mEventStartTimestamp = instance.mEventStartTimestamp;
+    this.mIconUrl = instance.mIconUrl;
     this.mId = instance.mId;
+    this.mIsGame = instance.mIsGame;
+    this.mLogoUrl = instance.mLogoUrl;
     this.mName = instance.mName;
     this.mNameWithLocationDescriptor = instance.mNameWithLocationDescriptor;
-    this.mUrl = instance.mUrl;
-    this.mType = instance.mType;
-    this.mCheckinCapable = instance.mCheckinCapable;
-    this.mPicture = instance.mPicture;
-    this.mTabs = instance.mTabs;
-    this.mIsGame = instance.mIsGame;
-    this.mSupportedPlatforms = instance.mSupportedPlatforms;
     this.mNativeAppStoreIds = instance.mNativeAppStoreIds;
-    this.mObjectStoreUrls = instance.mObjectStoreUrls;
-    this.mAppInstallsTracked = instance.mAppInstallsTracked;
     this.mNativeAppTargetingIds = instance.mNativeAppTargetingIds;
-    this.mOgNamespace = instance.mOgNamespace;
+    this.mObjectStoreUrls = instance.mObjectStoreUrls;
     this.mOgActions = instance.mOgActions;
+    this.mOgNamespace = instance.mOgNamespace;
     this.mOgObjects = instance.mOgObjects;
+    this.mPicture = instance.mPicture;
+    this.mSupportedPlatforms = instance.mSupportedPlatforms;
+    this.mTabs = instance.mTabs;
+    this.mType = instance.mType;
+    this.mUrl = instance.mUrl;
     this.mWebsite = instance.mWebsite;
-    this.mLogoUrl = instance.mLogoUrl;
-    this.mIconUrl = instance.mIconUrl;
-    this.mEventParentPageId = instance.mEventParentPageId;
-    this.mEventParentPageName = instance.mEventParentPageName;
-    this.mEventStartTimestamp = instance.mEventStartTimestamp;
-    this.mEventIsViewerAdmin = instance.mEventIsViewerAdmin;
-    this.mCpaAccess = instance.mCpaAccess;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<ConnectionObject> getParser() {
     return new APIRequest.ResponseParser<ConnectionObject>() {
-      public APINodeList<ConnectionObject> parseResponse(String response, APIContext context, APIRequest<ConnectionObject> request) {
+      public APINodeList<ConnectionObject> parseResponse(String response, APIContext context, APIRequest<ConnectionObject> request) throws MalformedResponseException {
         return ConnectionObject.parseResponse(response, context, request);
       }
     };
