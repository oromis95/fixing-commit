@@ -24,32 +24,33 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AdCreative extends APINode {
-  @SerializedName("id")
-  private String mId = null;
   @SerializedName("actor_id")
   private String mActorId = null;
   @SerializedName("actor_image_hash")
@@ -60,10 +61,14 @@ public class AdCreative extends APINode {
   private String mActorName = null;
   @SerializedName("adlabels")
   private List<AdLabel> mAdlabels = null;
+  @SerializedName("applink_treatment")
+  private EnumApplinkTreatment mApplinkTreatment = null;
   @SerializedName("body")
   private String mBody = null;
   @SerializedName("call_to_action_type")
   private EnumCallToActionType mCallToActionType = null;
+  @SerializedName("id")
+  private String mId = null;
   @SerializedName("image_crops")
   private AdsImageCrops mImageCrops = null;
   @SerializedName("image_hash")
@@ -74,6 +79,8 @@ public class AdCreative extends APINode {
   private String mInstagramActorId = null;
   @SerializedName("instagram_permalink_url")
   private String mInstagramPermalinkUrl = null;
+  @SerializedName("instagram_story_id")
+  private String mInstagramStoryId = null;
   @SerializedName("link_og_id")
   private String mLinkOgId = null;
   @SerializedName("link_url")
@@ -82,14 +89,16 @@ public class AdCreative extends APINode {
   private String mName = null;
   @SerializedName("object_id")
   private String mObjectId = null;
-  @SerializedName("object_url")
-  private String mObjectUrl = null;
   @SerializedName("object_story_id")
   private String mObjectStoryId = null;
   @SerializedName("object_story_spec")
   private AdCreativeObjectStorySpec mObjectStorySpec = null;
   @SerializedName("object_type")
   private EnumObjectType mObjectType = null;
+  @SerializedName("object_url")
+  private String mObjectUrl = null;
+  @SerializedName("platform_customizations")
+  private Object mPlatformCustomizations = null;
   @SerializedName("product_set_id")
   private String mProductSetId = null;
   @SerializedName("run_status")
@@ -102,8 +111,6 @@ public class AdCreative extends APINode {
   private String mTitle = null;
   @SerializedName("url_tags")
   private String mUrlTags = null;
-  @SerializedName("applink_treatment")
-  private EnumApplinkTreatment mApplinkTreatment = null;
   @SerializedName("creative_id")
   private String mCreativeId = null;
   protected static Gson gson = null;
@@ -118,11 +125,11 @@ public class AdCreative extends APINode {
   public AdCreative(String id, APIContext context) {
     this.mId = id;
     mCreativeId = mId.toString();
-    this.mContext = context;
+    this.context = context;
   }
 
   public AdCreative fetch() throws APIException{
-    AdCreative newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    AdCreative newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     mCreativeId = mId.toString();
     return this;
@@ -140,8 +147,17 @@ public class AdCreative extends APINode {
     return adCreative;
   }
 
+  public static APINodeList<AdCreative> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<AdCreative>)(
+      new APIRequest<AdCreative>(context, "", "/", "GET", AdCreative.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -156,23 +172,24 @@ public class AdCreative extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    adCreative.mContext = context;
+    adCreative.context = context;
     adCreative.rawValue = json;
     adCreative.mCreativeId = adCreative.mId.toString();
     return adCreative;
   }
 
-  public static APINodeList<AdCreative> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AdCreative> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AdCreative> adCreatives = new APINodeList<AdCreative>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -185,10 +202,11 @@ public class AdCreative extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            adCreatives.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            adCreatives.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -199,7 +217,20 @@ public class AdCreative extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            adCreatives.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  adCreatives.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              adCreatives.add(loadJSON(obj.toString(), context));
+            }
           }
           return adCreatives;
         } else if (obj.has("images")) {
@@ -210,24 +241,54 @@ public class AdCreative extends APINode {
           }
           return adCreatives;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              adCreatives.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return adCreatives;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          adCreatives.clear();
           adCreatives.add(loadJSON(json, context));
           return adCreatives;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -235,40 +296,30 @@ public class AdCreative extends APINode {
     return getGson().toJson(this);
   }
 
-  public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
-  }
-
-  public APIRequestUpdate update() {
-    return new APIRequestUpdate(this.getPrefixedId().toString(), mContext);
+  public APIRequestDeleteAdLabels deleteAdLabels() {
+    return new APIRequestDeleteAdLabels(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestDelete delete() {
-    return new APIRequestDelete(this.getPrefixedId().toString(), mContext);
+  public APIRequestCreateAdLabel createAdLabel() {
+    return new APIRequestCreateAdLabel(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetPreviews getPreviews() {
-    return new APIRequestGetPreviews(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetPreviews(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestCreateAdLabel createAdLabel() {
-    return new APIRequestCreateAdLabel(this.getPrefixedId().toString(), mContext);
+  public APIRequestDelete delete() {
+    return new APIRequestDelete(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestDeleteAdLabels deleteAdLabels() {
-    return new APIRequestDeleteAdLabels(this.getPrefixedId().toString(), mContext);
+  public APIRequestGet get() {
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
   }
 
-
-  public String getFieldId() {
-    return mId;
+  public APIRequestUpdate update() {
+    return new APIRequestUpdate(this.getPrefixedId().toString(), context);
   }
 
-  public AdCreative setFieldId(String value) {
-    this.mId = value;
-    this.mCreativeId = this.mId.toString();
-    return this;
-  }
 
   public String getFieldActorId() {
     return mActorId;
@@ -320,6 +371,15 @@ public class AdCreative extends APINode {
     this.mAdlabels = AdLabel.getGson().fromJson(value, type);
     return this;
   }
+  public EnumApplinkTreatment getFieldApplinkTreatment() {
+    return mApplinkTreatment;
+  }
+
+  public AdCreative setFieldApplinkTreatment(EnumApplinkTreatment value) {
+    this.mApplinkTreatment = value;
+    return this;
+  }
+
   public String getFieldBody() {
     return mBody;
   }
@@ -338,6 +398,16 @@ public class AdCreative extends APINode {
     return this;
   }
 
+  public String getFieldId() {
+    return mId;
+  }
+
+  public AdCreative setFieldId(String value) {
+    this.mId = value;
+    this.mCreativeId = this.mId.toString();
+    return this;
+  }
+
   public AdsImageCrops getFieldImageCrops() {
     return mImageCrops;
   }
@@ -388,6 +458,15 @@ public class AdCreative extends APINode {
     return this;
   }
 
+  public String getFieldInstagramStoryId() {
+    return mInstagramStoryId;
+  }
+
+  public AdCreative setFieldInstagramStoryId(String value) {
+    this.mInstagramStoryId = value;
+    return this;
+  }
+
   public String getFieldLinkOgId() {
     return mLinkOgId;
   }
@@ -424,15 +503,6 @@ public class AdCreative extends APINode {
     return this;
   }
 
-  public String getFieldObjectUrl() {
-    return mObjectUrl;
-  }
-
-  public AdCreative setFieldObjectUrl(String value) {
-    this.mObjectUrl = value;
-    return this;
-  }
-
   public String getFieldObjectStoryId() {
     return mObjectStoryId;
   }
@@ -465,6 +535,24 @@ public class AdCreative extends APINode {
     return this;
   }
 
+  public String getFieldObjectUrl() {
+    return mObjectUrl;
+  }
+
+  public AdCreative setFieldObjectUrl(String value) {
+    this.mObjectUrl = value;
+    return this;
+  }
+
+  public Object getFieldPlatformCustomizations() {
+    return mPlatformCustomizations;
+  }
+
+  public AdCreative setFieldPlatformCustomizations(Object value) {
+    this.mPlatformCustomizations = value;
+    return this;
+  }
+
   public String getFieldProductSetId() {
     return mProductSetId;
   }
@@ -519,321 +607,357 @@ public class AdCreative extends APINode {
     return this;
   }
 
-  public EnumApplinkTreatment getFieldApplinkTreatment() {
-    return mApplinkTreatment;
-  }
-
-  public AdCreative setFieldApplinkTreatment(EnumApplinkTreatment value) {
-    this.mApplinkTreatment = value;
-    return this;
-  }
-
 
 
-  public static class APIRequestGet extends APIRequest<AdCreative> {
+  public static class APIRequestDeleteAdLabels extends APIRequest<APINode> {
 
-    AdCreative lastResponse = null;
+    APINodeList<APINode> lastResponse = null;
     @Override
-    public AdCreative getLastResponse() {
+    public APINodeList<APINode> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
+      "adlabels",
+      "id",
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "actor_id",
-      "actor_image_hash",
-      "actor_image_url",
-      "actor_name",
-      "adlabels",
-      "body",
-      "call_to_action_type",
-      "image_crops",
-      "image_hash",
-      "image_url",
-      "instagram_actor_id",
-      "instagram_permalink_url",
-      "link_og_id",
-      "link_url",
-      "name",
-      "object_id",
-      "object_url",
-      "object_story_id",
-      "object_story_spec",
-      "object_type",
-      "product_set_id",
-      "run_status",
-      "template_url",
-      "thumbnail_url",
-      "title",
-      "url_tags",
-      "applink_treatment",
     };
 
     @Override
-    public AdCreative parseResponse(String response) throws APIException {
-      return AdCreative.parseResponse(response, getContext(), this).head();
+    public APINodeList<APINode> parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public AdCreative execute() throws APIException {
+    public APINodeList<APINode> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public AdCreative execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGet(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
+    public APIRequestDeleteAdLabels(String nodeId, APIContext context) {
+      super(context, nodeId, "/adlabels", "DELETE", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGet setParam(String param, Object value) {
+    @Override
+    public APIRequestDeleteAdLabels setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGet setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestDeleteAdLabels setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGet requestAllFields () {
+    public APIRequestDeleteAdLabels setAdlabels (List<Object> adlabels) {
+      this.setParam("adlabels", adlabels);
+      return this;
+    }
+    public APIRequestDeleteAdLabels setAdlabels (String adlabels) {
+      this.setParam("adlabels", adlabels);
+      return this;
+    }
+
+    public APIRequestDeleteAdLabels setId (String id) {
+      this.setParam("id", id);
+      return this;
+    }
+
+    public APIRequestDeleteAdLabels requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGet requestAllFields (boolean value) {
+    public APIRequestDeleteAdLabels requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestFields (List<String> fields) {
+    @Override
+    public APIRequestDeleteAdLabels requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGet requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestDeleteAdLabels requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestField (String field) {
+    @Override
+    public APIRequestDeleteAdLabels requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGet requestField (String field, boolean value) {
+    @Override
+    public APIRequestDeleteAdLabels requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGet requestIdField () {
-      return this.requestIdField(true);
+  }
+
+  public static class APIRequestCreateAdLabel extends APIRequest<APINode> {
+
+    APINode lastResponse = null;
+    @Override
+    public APINode getLastResponse() {
+      return lastResponse;
     }
-    public APIRequestGet requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
+    public static final String[] PARAMS = {
+      "adlabels",
+      "id",
+    };
+
+    public static final String[] FIELDS = {
+    };
+
+    @Override
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
     }
-    public APIRequestGet requestActorIdField () {
-      return this.requestActorIdField(true);
+
+    @Override
+    public APINode execute() throws APIException {
+      return execute(new HashMap<String, Object>());
     }
-    public APIRequestGet requestActorIdField (boolean value) {
-      this.requestField("actor_id", value);
-      return this;
+
+    @Override
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
     }
-    public APIRequestGet requestActorImageHashField () {
-      return this.requestActorImageHashField(true);
+
+    public APIRequestCreateAdLabel(String nodeId, APIContext context) {
+      super(context, nodeId, "/adlabels", "POST", Arrays.asList(PARAMS));
     }
-    public APIRequestGet requestActorImageHashField (boolean value) {
-      this.requestField("actor_image_hash", value);
+
+    @Override
+    public APIRequestCreateAdLabel setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
-    public APIRequestGet requestActorImageUrlField () {
-      return this.requestActorImageUrlField(true);
-    }
-    public APIRequestGet requestActorImageUrlField (boolean value) {
-      this.requestField("actor_image_url", value);
+
+    @Override
+    public APIRequestCreateAdLabel setParams(Map<String, Object> params) {
+      setParamsInternal(params);
       return this;
     }
-    public APIRequestGet requestActorNameField () {
-      return this.requestActorNameField(true);
-    }
-    public APIRequestGet requestActorNameField (boolean value) {
-      this.requestField("actor_name", value);
+
+
+    public APIRequestCreateAdLabel setAdlabels (List<Object> adlabels) {
+      this.setParam("adlabels", adlabels);
       return this;
     }
-    public APIRequestGet requestAdlabelsField () {
-      return this.requestAdlabelsField(true);
-    }
-    public APIRequestGet requestAdlabelsField (boolean value) {
-      this.requestField("adlabels", value);
+    public APIRequestCreateAdLabel setAdlabels (String adlabels) {
+      this.setParam("adlabels", adlabels);
       return this;
     }
-    public APIRequestGet requestBodyField () {
-      return this.requestBodyField(true);
-    }
-    public APIRequestGet requestBodyField (boolean value) {
-      this.requestField("body", value);
+
+    public APIRequestCreateAdLabel setId (String id) {
+      this.setParam("id", id);
       return this;
     }
-    public APIRequestGet requestCallToActionTypeField () {
-      return this.requestCallToActionTypeField(true);
+
+    public APIRequestCreateAdLabel requestAllFields () {
+      return this.requestAllFields(true);
     }
-    public APIRequestGet requestCallToActionTypeField (boolean value) {
-      this.requestField("call_to_action_type", value);
+
+    public APIRequestCreateAdLabel requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGet requestImageCropsField () {
-      return this.requestImageCropsField(true);
+
+    @Override
+    public APIRequestCreateAdLabel requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
-    public APIRequestGet requestImageCropsField (boolean value) {
-      this.requestField("image_crops", value);
+
+    @Override
+    public APIRequestCreateAdLabel requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGet requestImageHashField () {
-      return this.requestImageHashField(true);
-    }
-    public APIRequestGet requestImageHashField (boolean value) {
-      this.requestField("image_hash", value);
+
+    @Override
+    public APIRequestCreateAdLabel requestField (String field) {
+      this.requestField(field, true);
       return this;
     }
-    public APIRequestGet requestImageUrlField () {
-      return this.requestImageUrlField(true);
-    }
-    public APIRequestGet requestImageUrlField (boolean value) {
-      this.requestField("image_url", value);
+
+    @Override
+    public APIRequestCreateAdLabel requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
-    public APIRequestGet requestInstagramActorIdField () {
-      return this.requestInstagramActorIdField(true);
+
+  }
+
+  public static class APIRequestGetPreviews extends APIRequest<AdPreview> {
+
+    APINodeList<AdPreview> lastResponse = null;
+    @Override
+    public APINodeList<AdPreview> getLastResponse() {
+      return lastResponse;
     }
-    public APIRequestGet requestInstagramActorIdField (boolean value) {
-      this.requestField("instagram_actor_id", value);
-      return this;
+    public static final String[] PARAMS = {
+      "ad_format",
+      "height",
+      "locale",
+      "post",
+      "product_item_ids",
+      "width",
+    };
+
+    public static final String[] FIELDS = {
+      "body",
+    };
+
+    @Override
+    public APINodeList<AdPreview> parseResponse(String response) throws APIException {
+      return AdPreview.parseResponse(response, getContext(), this);
     }
-    public APIRequestGet requestInstagramPermalinkUrlField () {
-      return this.requestInstagramPermalinkUrlField(true);
+
+    @Override
+    public APINodeList<AdPreview> execute() throws APIException {
+      return execute(new HashMap<String, Object>());
     }
-    public APIRequestGet requestInstagramPermalinkUrlField (boolean value) {
-      this.requestField("instagram_permalink_url", value);
-      return this;
+
+    @Override
+    public APINodeList<AdPreview> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
     }
-    public APIRequestGet requestLinkOgIdField () {
-      return this.requestLinkOgIdField(true);
+
+    public APIRequestGetPreviews(String nodeId, APIContext context) {
+      super(context, nodeId, "/previews", "GET", Arrays.asList(PARAMS));
     }
-    public APIRequestGet requestLinkOgIdField (boolean value) {
-      this.requestField("link_og_id", value);
+
+    @Override
+    public APIRequestGetPreviews setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
-    public APIRequestGet requestLinkUrlField () {
-      return this.requestLinkUrlField(true);
-    }
-    public APIRequestGet requestLinkUrlField (boolean value) {
-      this.requestField("link_url", value);
+
+    @Override
+    public APIRequestGetPreviews setParams(Map<String, Object> params) {
+      setParamsInternal(params);
       return this;
     }
-    public APIRequestGet requestNameField () {
-      return this.requestNameField(true);
-    }
-    public APIRequestGet requestNameField (boolean value) {
-      this.requestField("name", value);
+
+
+    public APIRequestGetPreviews setAdFormat (AdPreview.EnumAdFormat adFormat) {
+      this.setParam("ad_format", adFormat);
       return this;
     }
-    public APIRequestGet requestObjectIdField () {
-      return this.requestObjectIdField(true);
-    }
-    public APIRequestGet requestObjectIdField (boolean value) {
-      this.requestField("object_id", value);
+    public APIRequestGetPreviews setAdFormat (String adFormat) {
+      this.setParam("ad_format", adFormat);
       return this;
     }
-    public APIRequestGet requestObjectUrlField () {
-      return this.requestObjectUrlField(true);
-    }
-    public APIRequestGet requestObjectUrlField (boolean value) {
-      this.requestField("object_url", value);
+
+    public APIRequestGetPreviews setHeight (Long height) {
+      this.setParam("height", height);
       return this;
     }
-    public APIRequestGet requestObjectStoryIdField () {
-      return this.requestObjectStoryIdField(true);
-    }
-    public APIRequestGet requestObjectStoryIdField (boolean value) {
-      this.requestField("object_story_id", value);
+    public APIRequestGetPreviews setHeight (String height) {
+      this.setParam("height", height);
       return this;
     }
-    public APIRequestGet requestObjectStorySpecField () {
-      return this.requestObjectStorySpecField(true);
-    }
-    public APIRequestGet requestObjectStorySpecField (boolean value) {
-      this.requestField("object_story_spec", value);
+
+    public APIRequestGetPreviews setLocale (String locale) {
+      this.setParam("locale", locale);
       return this;
     }
-    public APIRequestGet requestObjectTypeField () {
-      return this.requestObjectTypeField(true);
-    }
-    public APIRequestGet requestObjectTypeField (boolean value) {
-      this.requestField("object_type", value);
+
+    public APIRequestGetPreviews setPost (Object post) {
+      this.setParam("post", post);
       return this;
     }
-    public APIRequestGet requestProductSetIdField () {
-      return this.requestProductSetIdField(true);
-    }
-    public APIRequestGet requestProductSetIdField (boolean value) {
-      this.requestField("product_set_id", value);
+    public APIRequestGetPreviews setPost (String post) {
+      this.setParam("post", post);
       return this;
     }
-    public APIRequestGet requestRunStatusField () {
-      return this.requestRunStatusField(true);
+
+    public APIRequestGetPreviews setProductItemIds (List<String> productItemIds) {
+      this.setParam("product_item_ids", productItemIds);
+      return this;
     }
-    public APIRequestGet requestRunStatusField (boolean value) {
-      this.requestField("run_status", value);
+    public APIRequestGetPreviews setProductItemIds (String productItemIds) {
+      this.setParam("product_item_ids", productItemIds);
       return this;
     }
-    public APIRequestGet requestTemplateUrlField () {
-      return this.requestTemplateUrlField(true);
+
+    public APIRequestGetPreviews setWidth (Long width) {
+      this.setParam("width", width);
+      return this;
     }
-    public APIRequestGet requestTemplateUrlField (boolean value) {
-      this.requestField("template_url", value);
+    public APIRequestGetPreviews setWidth (String width) {
+      this.setParam("width", width);
       return this;
     }
-    public APIRequestGet requestThumbnailUrlField () {
-      return this.requestThumbnailUrlField(true);
+
+    public APIRequestGetPreviews requestAllFields () {
+      return this.requestAllFields(true);
     }
-    public APIRequestGet requestThumbnailUrlField (boolean value) {
-      this.requestField("thumbnail_url", value);
+
+    public APIRequestGetPreviews requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGet requestTitleField () {
-      return this.requestTitleField(true);
+
+    @Override
+    public APIRequestGetPreviews requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
-    public APIRequestGet requestTitleField (boolean value) {
-      this.requestField("title", value);
+
+    @Override
+    public APIRequestGetPreviews requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGet requestUrlTagsField () {
-      return this.requestUrlTagsField(true);
+
+    @Override
+    public APIRequestGetPreviews requestField (String field) {
+      this.requestField(field, true);
+      return this;
     }
-    public APIRequestGet requestUrlTagsField (boolean value) {
-      this.requestField("url_tags", value);
+
+    @Override
+    public APIRequestGetPreviews requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
-    public APIRequestGet requestApplinkTreatmentField () {
-      return this.requestApplinkTreatmentField(true);
+
+    public APIRequestGetPreviews requestBodyField () {
+      return this.requestBodyField(true);
     }
-    public APIRequestGet requestApplinkTreatmentField (boolean value) {
-      this.requestField("applink_treatment", value);
+    public APIRequestGetPreviews requestBodyField (boolean value) {
+      this.requestField("body", value);
       return this;
     }
-
   }
 
-  public static class APIRequestUpdate extends APIRequest<APINode> {
+  public static class APIRequestDelete extends APIRequest<APINode> {
 
     APINode lastResponse = null;
     @Override
@@ -841,11 +965,11 @@ public class AdCreative extends APINode {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "id",
       "account_id",
+      "adlabels",
+      "id",
       "name",
       "run_status",
-      "adlabels",
     };
 
     public static final String[] FIELDS = {
@@ -863,370 +987,423 @@ public class AdCreative extends APINode {
 
     @Override
     public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestUpdate(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "POST", Arrays.asList(PARAMS));
+    public APIRequestDelete(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "DELETE", Arrays.asList(PARAMS));
     }
 
-    public APIRequestUpdate setParam(String param, Object value) {
+    @Override
+    public APIRequestDelete setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestUpdate setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestDelete setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestUpdate setId (String id) {
-      this.setParam("id", id);
+    public APIRequestDelete setAccountId (String accountId) {
+      this.setParam("account_id", accountId);
       return this;
     }
 
-
-    public APIRequestUpdate setAccountId (String accountId) {
-      this.setParam("account_id", accountId);
+    public APIRequestDelete setAdlabels (List<Object> adlabels) {
+      this.setParam("adlabels", adlabels);
       return this;
     }
-
-
-    public APIRequestUpdate setName (String name) {
-      this.setParam("name", name);
+    public APIRequestDelete setAdlabels (String adlabels) {
+      this.setParam("adlabels", adlabels);
       return this;
     }
 
-
-    public APIRequestUpdate setRunStatus (Long runStatus) {
-      this.setParam("run_status", runStatus);
+    public APIRequestDelete setId (String id) {
+      this.setParam("id", id);
       return this;
     }
 
-    public APIRequestUpdate setRunStatus (String runStatus) {
-      this.setParam("run_status", runStatus);
+    public APIRequestDelete setName (String name) {
+      this.setParam("name", name);
       return this;
     }
 
-    public APIRequestUpdate setAdlabels (List<Object> adlabels) {
-      this.setParam("adlabels", adlabels);
+    public APIRequestDelete setRunStatus (Long runStatus) {
+      this.setParam("run_status", runStatus);
       return this;
     }
-
-    public APIRequestUpdate setAdlabels (String adlabels) {
-      this.setParam("adlabels", adlabels);
+    public APIRequestDelete setRunStatus (String runStatus) {
+      this.setParam("run_status", runStatus);
       return this;
     }
 
-    public APIRequestUpdate requestAllFields () {
+    public APIRequestDelete requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestUpdate requestAllFields (boolean value) {
+    public APIRequestDelete requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestUpdate requestFields (List<String> fields) {
+    @Override
+    public APIRequestDelete requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestUpdate requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestDelete requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestUpdate requestField (String field) {
+    @Override
+    public APIRequestDelete requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestUpdate requestField (String field, boolean value) {
+    @Override
+    public APIRequestDelete requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
   }
 
-  public static class APIRequestDelete extends APIRequest<APINode> {
+  public static class APIRequestGet extends APIRequest<AdCreative> {
 
-    APINode lastResponse = null;
+    AdCreative lastResponse = null;
     @Override
-    public APINode getLastResponse() {
+    public AdCreative getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "id",
-      "account_id",
-      "name",
-      "run_status",
-      "adlabels",
     };
 
     public static final String[] FIELDS = {
+      "actor_id",
+      "actor_image_hash",
+      "actor_image_url",
+      "actor_name",
+      "adlabels",
+      "applink_treatment",
+      "body",
+      "call_to_action_type",
+      "id",
+      "image_crops",
+      "image_hash",
+      "image_url",
+      "instagram_actor_id",
+      "instagram_permalink_url",
+      "instagram_story_id",
+      "link_og_id",
+      "link_url",
+      "name",
+      "object_id",
+      "object_story_id",
+      "object_story_spec",
+      "object_type",
+      "object_url",
+      "platform_customizations",
+      "product_set_id",
+      "run_status",
+      "template_url",
+      "thumbnail_url",
+      "title",
+      "url_tags",
     };
 
     @Override
-    public APINode parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this).head();
+    public AdCreative parseResponse(String response) throws APIException {
+      return AdCreative.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public APINode execute() throws APIException {
+    public AdCreative execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public AdCreative execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestDelete(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "DELETE", Arrays.asList(PARAMS));
+    public APIRequestGet(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestDelete setParam(String param, Object value) {
+    @Override
+    public APIRequestGet setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestDelete setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGet setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestDelete setId (String id) {
-      this.setParam("id", id);
-      return this;
+    public APIRequestGet requestAllFields () {
+      return this.requestAllFields(true);
     }
 
-
-    public APIRequestDelete setAccountId (String accountId) {
-      this.setParam("account_id", accountId);
+    public APIRequestGet requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
 
+    @Override
+    public APIRequestGet requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
+    }
 
-    public APIRequestDelete setName (String name) {
-      this.setParam("name", name);
+    @Override
+    public APIRequestGet requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
 
-
-    public APIRequestDelete setRunStatus (Long runStatus) {
-      this.setParam("run_status", runStatus);
+    @Override
+    public APIRequestGet requestField (String field) {
+      this.requestField(field, true);
       return this;
     }
 
-    public APIRequestDelete setRunStatus (String runStatus) {
-      this.setParam("run_status", runStatus);
+    @Override
+    public APIRequestGet requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestDelete setAdlabels (List<Object> adlabels) {
-      this.setParam("adlabels", adlabels);
+    public APIRequestGet requestActorIdField () {
+      return this.requestActorIdField(true);
+    }
+    public APIRequestGet requestActorIdField (boolean value) {
+      this.requestField("actor_id", value);
       return this;
     }
-
-    public APIRequestDelete setAdlabels (String adlabels) {
-      this.setParam("adlabels", adlabels);
+    public APIRequestGet requestActorImageHashField () {
+      return this.requestActorImageHashField(true);
+    }
+    public APIRequestGet requestActorImageHashField (boolean value) {
+      this.requestField("actor_image_hash", value);
       return this;
     }
-
-    public APIRequestDelete requestAllFields () {
-      return this.requestAllFields(true);
+    public APIRequestGet requestActorImageUrlField () {
+      return this.requestActorImageUrlField(true);
     }
-
-    public APIRequestDelete requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
+    public APIRequestGet requestActorImageUrlField (boolean value) {
+      this.requestField("actor_image_url", value);
       return this;
     }
-
-    public APIRequestDelete requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
+    public APIRequestGet requestActorNameField () {
+      return this.requestActorNameField(true);
     }
-
-    public APIRequestDelete requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+    public APIRequestGet requestActorNameField (boolean value) {
+      this.requestField("actor_name", value);
       return this;
     }
-
-    public APIRequestDelete requestField (String field) {
-      this.requestField(field, true);
+    public APIRequestGet requestAdlabelsField () {
+      return this.requestAdlabelsField(true);
+    }
+    public APIRequestGet requestAdlabelsField (boolean value) {
+      this.requestField("adlabels", value);
       return this;
     }
-
-    public APIRequestDelete requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
+    public APIRequestGet requestApplinkTreatmentField () {
+      return this.requestApplinkTreatmentField(true);
+    }
+    public APIRequestGet requestApplinkTreatmentField (boolean value) {
+      this.requestField("applink_treatment", value);
       return this;
     }
-
-
-  }
-
-  public static class APIRequestGetPreviews extends APIRequest<AdPreview> {
-
-    APINodeList<AdPreview> lastResponse = null;
-    @Override
-    public APINodeList<AdPreview> getLastResponse() {
-      return lastResponse;
+    public APIRequestGet requestBodyField () {
+      return this.requestBodyField(true);
     }
-    public static final String[] PARAMS = {
-      "ad_format",
-      "post",
-      "height",
-      "width",
-      "product_item_ids",
-      "locale",
-    };
-
-    public static final String[] FIELDS = {
-      "body",
-    };
-
-    @Override
-    public APINodeList<AdPreview> parseResponse(String response) throws APIException {
-      return AdPreview.parseResponse(response, getContext(), this);
+    public APIRequestGet requestBodyField (boolean value) {
+      this.requestField("body", value);
+      return this;
     }
-
-    @Override
-    public APINodeList<AdPreview> execute() throws APIException {
-      return execute(new HashMap<String, Object>());
+    public APIRequestGet requestCallToActionTypeField () {
+      return this.requestCallToActionTypeField(true);
     }
-
-    @Override
-    public APINodeList<AdPreview> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
+    public APIRequestGet requestCallToActionTypeField (boolean value) {
+      this.requestField("call_to_action_type", value);
+      return this;
     }
-
-    public APIRequestGetPreviews(String nodeId, APIContext context) {
-      super(context, nodeId, "/previews", "GET", Arrays.asList(PARAMS));
+    public APIRequestGet requestIdField () {
+      return this.requestIdField(true);
     }
-
-    public APIRequestGetPreviews setParam(String param, Object value) {
-      setParamInternal(param, value);
+    public APIRequestGet requestIdField (boolean value) {
+      this.requestField("id", value);
       return this;
     }
-
-    public APIRequestGetPreviews setParams(Map<String, Object> params) {
-      setParamsInternal(params);
+    public APIRequestGet requestImageCropsField () {
+      return this.requestImageCropsField(true);
+    }
+    public APIRequestGet requestImageCropsField (boolean value) {
+      this.requestField("image_crops", value);
       return this;
     }
-
-
-    public APIRequestGetPreviews setAdFormat (EnumAdFormat adFormat) {
-      this.setParam("ad_format", adFormat);
+    public APIRequestGet requestImageHashField () {
+      return this.requestImageHashField(true);
+    }
+    public APIRequestGet requestImageHashField (boolean value) {
+      this.requestField("image_hash", value);
       return this;
     }
-
-    public APIRequestGetPreviews setAdFormat (String adFormat) {
-      this.setParam("ad_format", adFormat);
+    public APIRequestGet requestImageUrlField () {
+      return this.requestImageUrlField(true);
+    }
+    public APIRequestGet requestImageUrlField (boolean value) {
+      this.requestField("image_url", value);
       return this;
     }
-
-    public APIRequestGetPreviews setPost (Object post) {
-      this.setParam("post", post);
+    public APIRequestGet requestInstagramActorIdField () {
+      return this.requestInstagramActorIdField(true);
+    }
+    public APIRequestGet requestInstagramActorIdField (boolean value) {
+      this.requestField("instagram_actor_id", value);
       return this;
     }
-
-    public APIRequestGetPreviews setPost (String post) {
-      this.setParam("post", post);
+    public APIRequestGet requestInstagramPermalinkUrlField () {
+      return this.requestInstagramPermalinkUrlField(true);
+    }
+    public APIRequestGet requestInstagramPermalinkUrlField (boolean value) {
+      this.requestField("instagram_permalink_url", value);
       return this;
     }
-
-    public APIRequestGetPreviews setHeight (Long height) {
-      this.setParam("height", height);
+    public APIRequestGet requestInstagramStoryIdField () {
+      return this.requestInstagramStoryIdField(true);
+    }
+    public APIRequestGet requestInstagramStoryIdField (boolean value) {
+      this.requestField("instagram_story_id", value);
       return this;
     }
-
-    public APIRequestGetPreviews setHeight (String height) {
-      this.setParam("height", height);
+    public APIRequestGet requestLinkOgIdField () {
+      return this.requestLinkOgIdField(true);
+    }
+    public APIRequestGet requestLinkOgIdField (boolean value) {
+      this.requestField("link_og_id", value);
+      return this;
+    }
+    public APIRequestGet requestLinkUrlField () {
+      return this.requestLinkUrlField(true);
+    }
+    public APIRequestGet requestLinkUrlField (boolean value) {
+      this.requestField("link_url", value);
+      return this;
+    }
+    public APIRequestGet requestNameField () {
+      return this.requestNameField(true);
+    }
+    public APIRequestGet requestNameField (boolean value) {
+      this.requestField("name", value);
+      return this;
+    }
+    public APIRequestGet requestObjectIdField () {
+      return this.requestObjectIdField(true);
+    }
+    public APIRequestGet requestObjectIdField (boolean value) {
+      this.requestField("object_id", value);
+      return this;
+    }
+    public APIRequestGet requestObjectStoryIdField () {
+      return this.requestObjectStoryIdField(true);
+    }
+    public APIRequestGet requestObjectStoryIdField (boolean value) {
+      this.requestField("object_story_id", value);
+      return this;
+    }
+    public APIRequestGet requestObjectStorySpecField () {
+      return this.requestObjectStorySpecField(true);
+    }
+    public APIRequestGet requestObjectStorySpecField (boolean value) {
+      this.requestField("object_story_spec", value);
+      return this;
+    }
+    public APIRequestGet requestObjectTypeField () {
+      return this.requestObjectTypeField(true);
+    }
+    public APIRequestGet requestObjectTypeField (boolean value) {
+      this.requestField("object_type", value);
+      return this;
+    }
+    public APIRequestGet requestObjectUrlField () {
+      return this.requestObjectUrlField(true);
+    }
+    public APIRequestGet requestObjectUrlField (boolean value) {
+      this.requestField("object_url", value);
       return this;
     }
-
-    public APIRequestGetPreviews setWidth (Long width) {
-      this.setParam("width", width);
-      return this;
+    public APIRequestGet requestPlatformCustomizationsField () {
+      return this.requestPlatformCustomizationsField(true);
     }
-
-    public APIRequestGetPreviews setWidth (String width) {
-      this.setParam("width", width);
+    public APIRequestGet requestPlatformCustomizationsField (boolean value) {
+      this.requestField("platform_customizations", value);
       return this;
     }
-
-    public APIRequestGetPreviews setProductItemIds (List<String> productItemIds) {
-      this.setParam("product_item_ids", productItemIds);
-      return this;
+    public APIRequestGet requestProductSetIdField () {
+      return this.requestProductSetIdField(true);
     }
-
-    public APIRequestGetPreviews setProductItemIds (String productItemIds) {
-      this.setParam("product_item_ids", productItemIds);
+    public APIRequestGet requestProductSetIdField (boolean value) {
+      this.requestField("product_set_id", value);
       return this;
     }
-
-    public APIRequestGetPreviews setLocale (String locale) {
-      this.setParam("locale", locale);
+    public APIRequestGet requestRunStatusField () {
+      return this.requestRunStatusField(true);
+    }
+    public APIRequestGet requestRunStatusField (boolean value) {
+      this.requestField("run_status", value);
       return this;
     }
-
-
-    public APIRequestGetPreviews requestAllFields () {
-      return this.requestAllFields(true);
+    public APIRequestGet requestTemplateUrlField () {
+      return this.requestTemplateUrlField(true);
     }
-
-    public APIRequestGetPreviews requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
+    public APIRequestGet requestTemplateUrlField (boolean value) {
+      this.requestField("template_url", value);
       return this;
     }
-
-    public APIRequestGetPreviews requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
+    public APIRequestGet requestThumbnailUrlField () {
+      return this.requestThumbnailUrlField(true);
     }
-
-    public APIRequestGetPreviews requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+    public APIRequestGet requestThumbnailUrlField (boolean value) {
+      this.requestField("thumbnail_url", value);
       return this;
     }
-
-    public APIRequestGetPreviews requestField (String field) {
-      this.requestField(field, true);
-      return this;
+    public APIRequestGet requestTitleField () {
+      return this.requestTitleField(true);
     }
-
-    public APIRequestGetPreviews requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
+    public APIRequestGet requestTitleField (boolean value) {
+      this.requestField("title", value);
       return this;
     }
-
-    public APIRequestGetPreviews requestBodyField () {
-      return this.requestBodyField(true);
+    public APIRequestGet requestUrlTagsField () {
+      return this.requestUrlTagsField(true);
     }
-    public APIRequestGetPreviews requestBodyField (boolean value) {
-      this.requestField("body", value);
+    public APIRequestGet requestUrlTagsField (boolean value) {
+      this.requestField("url_tags", value);
       return this;
     }
-
   }
 
-  public static class APIRequestCreateAdLabel extends APIRequest<APINode> {
+  public static class APIRequestUpdate extends APIRequest<APINode> {
 
     APINode lastResponse = null;
     @Override
@@ -1234,8 +1411,11 @@ public class AdCreative extends APINode {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "id",
+      "account_id",
       "adlabels",
+      "id",
+      "name",
+      "run_status",
     };
 
     public static final String[] FIELDS = {
@@ -1253,352 +1433,293 @@ public class AdCreative extends APINode {
 
     @Override
     public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestCreateAdLabel(String nodeId, APIContext context) {
-      super(context, nodeId, "/adlabels", "POST", Arrays.asList(PARAMS));
+    public APIRequestUpdate(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "POST", Arrays.asList(PARAMS));
     }
 
-    public APIRequestCreateAdLabel setParam(String param, Object value) {
+    @Override
+    public APIRequestUpdate setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestCreateAdLabel setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestUpdate setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestCreateAdLabel setId (String id) {
-      this.setParam("id", id);
+    public APIRequestUpdate setAccountId (String accountId) {
+      this.setParam("account_id", accountId);
       return this;
     }
 
-
-    public APIRequestCreateAdLabel setAdlabels (List<Object> adlabels) {
+    public APIRequestUpdate setAdlabels (List<Object> adlabels) {
       this.setParam("adlabels", adlabels);
       return this;
     }
-
-    public APIRequestCreateAdLabel setAdlabels (String adlabels) {
+    public APIRequestUpdate setAdlabels (String adlabels) {
       this.setParam("adlabels", adlabels);
       return this;
     }
 
-    public APIRequestCreateAdLabel requestAllFields () {
-      return this.requestAllFields(true);
-    }
-
-    public APIRequestCreateAdLabel requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
-      return this;
-    }
-
-    public APIRequestCreateAdLabel requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
-    }
-
-    public APIRequestCreateAdLabel requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
-      return this;
-    }
-
-    public APIRequestCreateAdLabel requestField (String field) {
-      this.requestField(field, true);
-      return this;
-    }
-
-    public APIRequestCreateAdLabel requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
-      return this;
-    }
-
-
-  }
-
-  public static class APIRequestDeleteAdLabels extends APIRequest<APINode> {
-
-    APINodeList<APINode> lastResponse = null;
-    @Override
-    public APINodeList<APINode> getLastResponse() {
-      return lastResponse;
-    }
-    public static final String[] PARAMS = {
-      "id",
-      "adlabels",
-    };
-
-    public static final String[] FIELDS = {
-    };
-
-    @Override
-    public APINodeList<APINode> parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this);
-    }
-
-    @Override
-    public APINodeList<APINode> execute() throws APIException {
-      return execute(new HashMap<String, Object>());
-    }
-
-    @Override
-    public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
-    }
-
-    public APIRequestDeleteAdLabels(String nodeId, APIContext context) {
-      super(context, nodeId, "/adlabels", "DELETE", Arrays.asList(PARAMS));
-    }
-
-    public APIRequestDeleteAdLabels setParam(String param, Object value) {
-      setParamInternal(param, value);
-      return this;
-    }
-
-    public APIRequestDeleteAdLabels setParams(Map<String, Object> params) {
-      setParamsInternal(params);
+    public APIRequestUpdate setId (String id) {
+      this.setParam("id", id);
       return this;
     }
 
-
-    public APIRequestDeleteAdLabels setId (String id) {
-      this.setParam("id", id);
+    public APIRequestUpdate setName (String name) {
+      this.setParam("name", name);
       return this;
     }
 
-
-    public APIRequestDeleteAdLabels setAdlabels (List<Object> adlabels) {
-      this.setParam("adlabels", adlabels);
+    public APIRequestUpdate setRunStatus (Long runStatus) {
+      this.setParam("run_status", runStatus);
       return this;
     }
-
-    public APIRequestDeleteAdLabels setAdlabels (String adlabels) {
-      this.setParam("adlabels", adlabels);
+    public APIRequestUpdate setRunStatus (String runStatus) {
+      this.setParam("run_status", runStatus);
       return this;
     }
 
-    public APIRequestDeleteAdLabels requestAllFields () {
+    public APIRequestUpdate requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestDeleteAdLabels requestAllFields (boolean value) {
+    public APIRequestUpdate requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestDeleteAdLabels requestFields (List<String> fields) {
+    @Override
+    public APIRequestUpdate requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestDeleteAdLabels requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestUpdate requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestDeleteAdLabels requestField (String field) {
+    @Override
+    public APIRequestUpdate requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestDeleteAdLabels requestField (String field, boolean value) {
+    @Override
+    public APIRequestUpdate requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
   }
 
-  public static enum EnumAdFormat {
-    @SerializedName("RIGHT_COLUMN_STANDARD")
-    VALUE_RIGHT_COLUMN_STANDARD("RIGHT_COLUMN_STANDARD"),
-    @SerializedName("DESKTOP_FEED_STANDARD")
-    VALUE_DESKTOP_FEED_STANDARD("DESKTOP_FEED_STANDARD"),
-    @SerializedName("MOBILE_FEED_STANDARD")
-    VALUE_MOBILE_FEED_STANDARD("MOBILE_FEED_STANDARD"),
-    @SerializedName("MOBILE_FEED_BASIC")
-    VALUE_MOBILE_FEED_BASIC("MOBILE_FEED_BASIC"),
-    @SerializedName("MOBILE_INTERSTITIAL")
-    VALUE_MOBILE_INTERSTITIAL("MOBILE_INTERSTITIAL"),
-    @SerializedName("MOBILE_BANNER")
-    VALUE_MOBILE_BANNER("MOBILE_BANNER"),
-    @SerializedName("MOBILE_NATIVE")
-    VALUE_MOBILE_NATIVE("MOBILE_NATIVE"),
-    @SerializedName("INSTAGRAM_STANDARD")
-    VALUE_INSTAGRAM_STANDARD("INSTAGRAM_STANDARD"),
-    @SerializedName("LIVERAIL_VIDEO")
-    VALUE_LIVERAIL_VIDEO("LIVERAIL_VIDEO"),
-    NULL(null);
-
-    private String value;
-
-    private EnumAdFormat(String value) {
-      this.value = value;
-    }
+  public static enum EnumApplinkTreatment {
+      @SerializedName("deeplink_with_web_fallback")
+      VALUE_DEEPLINK_WITH_WEB_FALLBACK("deeplink_with_web_fallback"),
+      @SerializedName("deeplink_with_appstore_fallback")
+      VALUE_DEEPLINK_WITH_APPSTORE_FALLBACK("deeplink_with_appstore_fallback"),
+      @SerializedName("web_only")
+      VALUE_WEB_ONLY("web_only"),
+      NULL(null);
+
+      private String value;
+
+      private EnumApplinkTreatment(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
+
   public static enum EnumCallToActionType {
-    @SerializedName("OPEN_LINK")
-    VALUE_OPEN_LINK("OPEN_LINK"),
-    @SerializedName("LIKE_PAGE")
-    VALUE_LIKE_PAGE("LIKE_PAGE"),
-    @SerializedName("SHOP_NOW")
-    VALUE_SHOP_NOW("SHOP_NOW"),
-    @SerializedName("PLAY_GAME")
-    VALUE_PLAY_GAME("PLAY_GAME"),
-    @SerializedName("INSTALL_APP")
-    VALUE_INSTALL_APP("INSTALL_APP"),
-    @SerializedName("USE_APP")
-    VALUE_USE_APP("USE_APP"),
-    @SerializedName("INSTALL_MOBILE_APP")
-    VALUE_INSTALL_MOBILE_APP("INSTALL_MOBILE_APP"),
-    @SerializedName("USE_MOBILE_APP")
-    VALUE_USE_MOBILE_APP("USE_MOBILE_APP"),
-    @SerializedName("BOOK_TRAVEL")
-    VALUE_BOOK_TRAVEL("BOOK_TRAVEL"),
-    @SerializedName("LISTEN_MUSIC")
-    VALUE_LISTEN_MUSIC("LISTEN_MUSIC"),
-    @SerializedName("WATCH_VIDEO")
-    VALUE_WATCH_VIDEO("WATCH_VIDEO"),
-    @SerializedName("LEARN_MORE")
-    VALUE_LEARN_MORE("LEARN_MORE"),
-    @SerializedName("SIGN_UP")
-    VALUE_SIGN_UP("SIGN_UP"),
-    @SerializedName("DOWNLOAD")
-    VALUE_DOWNLOAD("DOWNLOAD"),
-    @SerializedName("WATCH_MORE")
-    VALUE_WATCH_MORE("WATCH_MORE"),
-    @SerializedName("NO_BUTTON")
-    VALUE_NO_BUTTON("NO_BUTTON"),
-    @SerializedName("CALL_NOW")
-    VALUE_CALL_NOW("CALL_NOW"),
-    @SerializedName("BUY_NOW")
-    VALUE_BUY_NOW("BUY_NOW"),
-    @SerializedName("GET_OFFER")
-    VALUE_GET_OFFER("GET_OFFER"),
-    @SerializedName("GET_DIRECTIONS")
-    VALUE_GET_DIRECTIONS("GET_DIRECTIONS"),
-    @SerializedName("MESSAGE_PAGE")
-    VALUE_MESSAGE_PAGE("MESSAGE_PAGE"),
-    @SerializedName("SUBSCRIBE")
-    VALUE_SUBSCRIBE("SUBSCRIBE"),
-    @SerializedName("DONATE_NOW")
-    VALUE_DONATE_NOW("DONATE_NOW"),
-    @SerializedName("GET_QUOTE")
-    VALUE_GET_QUOTE("GET_QUOTE"),
-    @SerializedName("CONTACT_US")
-    VALUE_CONTACT_US("CONTACT_US"),
-    @SerializedName("RECORD_NOW")
-    VALUE_RECORD_NOW("RECORD_NOW"),
-    @SerializedName("OPEN_MOVIES")
-    VALUE_OPEN_MOVIES("OPEN_MOVIES"),
-    NULL(null);
-
-    private String value;
-
-    private EnumCallToActionType(String value) {
-      this.value = value;
-    }
+      @SerializedName("OPEN_LINK")
+      VALUE_OPEN_LINK("OPEN_LINK"),
+      @SerializedName("LIKE_PAGE")
+      VALUE_LIKE_PAGE("LIKE_PAGE"),
+      @SerializedName("SHOP_NOW")
+      VALUE_SHOP_NOW("SHOP_NOW"),
+      @SerializedName("PLAY_GAME")
+      VALUE_PLAY_GAME("PLAY_GAME"),
+      @SerializedName("INSTALL_APP")
+      VALUE_INSTALL_APP("INSTALL_APP"),
+      @SerializedName("USE_APP")
+      VALUE_USE_APP("USE_APP"),
+      @SerializedName("INSTALL_MOBILE_APP")
+      VALUE_INSTALL_MOBILE_APP("INSTALL_MOBILE_APP"),
+      @SerializedName("USE_MOBILE_APP")
+      VALUE_USE_MOBILE_APP("USE_MOBILE_APP"),
+      @SerializedName("BOOK_TRAVEL")
+      VALUE_BOOK_TRAVEL("BOOK_TRAVEL"),
+      @SerializedName("LISTEN_MUSIC")
+      VALUE_LISTEN_MUSIC("LISTEN_MUSIC"),
+      @SerializedName("WATCH_VIDEO")
+      VALUE_WATCH_VIDEO("WATCH_VIDEO"),
+      @SerializedName("LEARN_MORE")
+      VALUE_LEARN_MORE("LEARN_MORE"),
+      @SerializedName("SIGN_UP")
+      VALUE_SIGN_UP("SIGN_UP"),
+      @SerializedName("DOWNLOAD")
+      VALUE_DOWNLOAD("DOWNLOAD"),
+      @SerializedName("WATCH_MORE")
+      VALUE_WATCH_MORE("WATCH_MORE"),
+      @SerializedName("NO_BUTTON")
+      VALUE_NO_BUTTON("NO_BUTTON"),
+      @SerializedName("CALL_NOW")
+      VALUE_CALL_NOW("CALL_NOW"),
+      @SerializedName("BUY_NOW")
+      VALUE_BUY_NOW("BUY_NOW"),
+      @SerializedName("GET_OFFER")
+      VALUE_GET_OFFER("GET_OFFER"),
+      @SerializedName("GET_OFFER_VIEW")
+      VALUE_GET_OFFER_VIEW("GET_OFFER_VIEW"),
+      @SerializedName("GET_DIRECTIONS")
+      VALUE_GET_DIRECTIONS("GET_DIRECTIONS"),
+      @SerializedName("MESSAGE_PAGE")
+      VALUE_MESSAGE_PAGE("MESSAGE_PAGE"),
+      @SerializedName("SUBSCRIBE")
+      VALUE_SUBSCRIBE("SUBSCRIBE"),
+      @SerializedName("SELL_NOW")
+      VALUE_SELL_NOW("SELL_NOW"),
+      @SerializedName("DONATE_NOW")
+      VALUE_DONATE_NOW("DONATE_NOW"),
+      @SerializedName("GET_QUOTE")
+      VALUE_GET_QUOTE("GET_QUOTE"),
+      @SerializedName("CONTACT_US")
+      VALUE_CONTACT_US("CONTACT_US"),
+      @SerializedName("RECORD_NOW")
+      VALUE_RECORD_NOW("RECORD_NOW"),
+      @SerializedName("VOTE_NOW")
+      VALUE_VOTE_NOW("VOTE_NOW"),
+      @SerializedName("OPEN_MOVIES")
+      VALUE_OPEN_MOVIES("OPEN_MOVIES"),
+      NULL(null);
+
+      private String value;
+
+      private EnumCallToActionType(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
+
   public static enum EnumObjectType {
-    @SerializedName("APPLICATION")
-    VALUE_APPLICATION("APPLICATION"),
-    @SerializedName("DOMAIN")
-    VALUE_DOMAIN("DOMAIN"),
-    @SerializedName("EVENT")
-    VALUE_EVENT("EVENT"),
-    @SerializedName("OFFER")
-    VALUE_OFFER("OFFER"),
-    @SerializedName("PAGE")
-    VALUE_PAGE("PAGE"),
-    @SerializedName("PHOTO")
-    VALUE_PHOTO("PHOTO"),
-    @SerializedName("SHARE")
-    VALUE_SHARE("SHARE"),
-    @SerializedName("STATUS")
-    VALUE_STATUS("STATUS"),
-    @SerializedName("STORE_ITEM")
-    VALUE_STORE_ITEM("STORE_ITEM"),
-    @SerializedName("VIDEO")
-    VALUE_VIDEO("VIDEO"),
-    @SerializedName("INVALID")
-    VALUE_INVALID("INVALID"),
-    @SerializedName("ACTION_SPEC")
-    VALUE_ACTION_SPEC("ACTION_SPEC"),
-    @SerializedName("INSTAGRAM_MEDIA")
-    VALUE_INSTAGRAM_MEDIA("INSTAGRAM_MEDIA"),
-    NULL(null);
-
-    private String value;
-
-    private EnumObjectType(String value) {
-      this.value = value;
-    }
+      @SerializedName("APPLICATION")
+      VALUE_APPLICATION("APPLICATION"),
+      @SerializedName("DOMAIN")
+      VALUE_DOMAIN("DOMAIN"),
+      @SerializedName("EVENT")
+      VALUE_EVENT("EVENT"),
+      @SerializedName("OFFER")
+      VALUE_OFFER("OFFER"),
+      @SerializedName("PAGE")
+      VALUE_PAGE("PAGE"),
+      @SerializedName("PHOTO")
+      VALUE_PHOTO("PHOTO"),
+      @SerializedName("SHARE")
+      VALUE_SHARE("SHARE"),
+      @SerializedName("STATUS")
+      VALUE_STATUS("STATUS"),
+      @SerializedName("STORE_ITEM")
+      VALUE_STORE_ITEM("STORE_ITEM"),
+      @SerializedName("VIDEO")
+      VALUE_VIDEO("VIDEO"),
+      @SerializedName("INVALID")
+      VALUE_INVALID("INVALID"),
+      @SerializedName("ACTION_SPEC")
+      VALUE_ACTION_SPEC("ACTION_SPEC"),
+      @SerializedName("INSTAGRAM_MEDIA")
+      VALUE_INSTAGRAM_MEDIA("INSTAGRAM_MEDIA"),
+      NULL(null);
+
+      private String value;
+
+      private EnumObjectType(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
+
   public static enum EnumRunStatus {
-    @SerializedName("ACTIVE")
-    VALUE_ACTIVE("ACTIVE"),
-    @SerializedName("DELETED")
-    VALUE_DELETED("DELETED"),
-    NULL(null);
+      @SerializedName("ACTIVE")
+      VALUE_ACTIVE("ACTIVE"),
+      @SerializedName("DELETED")
+      VALUE_DELETED("DELETED"),
+      NULL(null);
 
-    private String value;
+      private String value;
 
-    private EnumRunStatus(String value) {
-      this.value = value;
-    }
+      private EnumRunStatus(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
-  public static enum EnumApplinkTreatment {
-    @SerializedName("deeplink_with_web_fallback")
-    VALUE_DEEPLINK_WITH_WEB_FALLBACK("deeplink_with_web_fallback"),
-    @SerializedName("deeplink_with_appstore_fallback")
-    VALUE_DEEPLINK_WITH_APPSTORE_FALLBACK("deeplink_with_appstore_fallback"),
-    @SerializedName("web_only")
-    VALUE_WEB_ONLY("web_only"),
-    NULL(null);
 
-    private String value;
+  public static enum EnumDynamicAdVoice {
+      @SerializedName("DYNAMIC")
+      VALUE_DYNAMIC("DYNAMIC"),
+      @SerializedName("STORY_OWNER")
+      VALUE_STORY_OWNER("STORY_OWNER"),
+      NULL(null);
 
-    private EnumApplinkTreatment(String value) {
-      this.value = value;
-    }
+      private String value;
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      private EnumDynamicAdVoice(String value) {
+        this.value = value;
+      }
+
+      @Override
+      public String toString() {
+        return value;
+      }
+  }
+
+  public static enum EnumOperator {
+      @SerializedName("ALL")
+      VALUE_ALL("ALL"),
+      @SerializedName("ANY")
+      VALUE_ANY("ANY"),
+      NULL(null);
+
+      private String value;
+
+      private EnumOperator(String value) {
+        this.value = value;
+      }
+
+      @Override
+      public String toString() {
+        return value;
+      }
   }
 
+
   synchronized /*package*/ static Gson getGson() {
     if (gson != null) {
       return gson;
@@ -1613,43 +1734,45 @@ public class AdCreative extends APINode {
   }
 
   public AdCreative copyFrom(AdCreative instance) {
-    this.mId = instance.mId;
     this.mActorId = instance.mActorId;
     this.mActorImageHash = instance.mActorImageHash;
     this.mActorImageUrl = instance.mActorImageUrl;
     this.mActorName = instance.mActorName;
     this.mAdlabels = instance.mAdlabels;
+    this.mApplinkTreatment = instance.mApplinkTreatment;
     this.mBody = instance.mBody;
     this.mCallToActionType = instance.mCallToActionType;
+    this.mId = instance.mId;
     this.mImageCrops = instance.mImageCrops;
     this.mImageHash = instance.mImageHash;
     this.mImageUrl = instance.mImageUrl;
     this.mInstagramActorId = instance.mInstagramActorId;
     this.mInstagramPermalinkUrl = instance.mInstagramPermalinkUrl;
+    this.mInstagramStoryId = instance.mInstagramStoryId;
     this.mLinkOgId = instance.mLinkOgId;
     this.mLinkUrl = instance.mLinkUrl;
     this.mName = instance.mName;
     this.mObjectId = instance.mObjectId;
-    this.mObjectUrl = instance.mObjectUrl;
     this.mObjectStoryId = instance.mObjectStoryId;
     this.mObjectStorySpec = instance.mObjectStorySpec;
     this.mObjectType = instance.mObjectType;
+    this.mObjectUrl = instance.mObjectUrl;
+    this.mPlatformCustomizations = instance.mPlatformCustomizations;
     this.mProductSetId = instance.mProductSetId;
     this.mRunStatus = instance.mRunStatus;
     this.mTemplateUrl = instance.mTemplateUrl;
     this.mThumbnailUrl = instance.mThumbnailUrl;
     this.mTitle = instance.mTitle;
     this.mUrlTags = instance.mUrlTags;
-    this.mApplinkTreatment = instance.mApplinkTreatment;
     this.mCreativeId = this.mId;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AdCreative> getParser() {
     return new APIRequest.ResponseParser<AdCreative>() {
-      public APINodeList<AdCreative> parseResponse(String response, APIContext context, APIRequest<AdCreative> request) {
+      public APINodeList<AdCreative> parseResponse(String response, APIContext context, APIRequest<AdCreative> request) throws MalformedResponseException {
         return AdCreative.parseResponse(response, context, request);
       }
     };
