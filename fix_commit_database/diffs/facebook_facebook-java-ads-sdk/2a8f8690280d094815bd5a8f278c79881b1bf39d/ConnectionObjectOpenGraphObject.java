@@ -24,34 +24,37 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class ConnectionObjectOpenGraphObject extends APINode {
-  @SerializedName("name")
-  private String mName = null;
   @SerializedName("display_name")
   private String mDisplayName = null;
+  @SerializedName("name")
+  private String mName = null;
   @SerializedName("properties")
   private List<ConnectionObjectOpenGraphObjectProperty> mProperties = null;
   protected static Gson gson = null;
@@ -71,22 +74,23 @@ public class ConnectionObjectOpenGraphObject extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    connectionObjectOpenGraphObject.mContext = context;
+    connectionObjectOpenGraphObject.context = context;
     connectionObjectOpenGraphObject.rawValue = json;
     return connectionObjectOpenGraphObject;
   }
 
-  public static APINodeList<ConnectionObjectOpenGraphObject> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<ConnectionObjectOpenGraphObject> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<ConnectionObjectOpenGraphObject> connectionObjectOpenGraphObjects = new APINodeList<ConnectionObjectOpenGraphObject>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -99,10 +103,11 @@ public class ConnectionObjectOpenGraphObject extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            connectionObjectOpenGraphObjects.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            connectionObjectOpenGraphObjects.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -113,7 +118,20 @@ public class ConnectionObjectOpenGraphObject extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            connectionObjectOpenGraphObjects.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  connectionObjectOpenGraphObjects.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              connectionObjectOpenGraphObjects.add(loadJSON(obj.toString(), context));
+            }
           }
           return connectionObjectOpenGraphObjects;
         } else if (obj.has("images")) {
@@ -124,24 +142,54 @@ public class ConnectionObjectOpenGraphObject extends APINode {
           }
           return connectionObjectOpenGraphObjects;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              connectionObjectOpenGraphObjects.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return connectionObjectOpenGraphObjects;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          connectionObjectOpenGraphObjects.clear();
           connectionObjectOpenGraphObjects.add(loadJSON(json, context));
           return connectionObjectOpenGraphObjects;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -150,21 +198,21 @@ public class ConnectionObjectOpenGraphObject extends APINode {
   }
 
 
-  public String getFieldName() {
-    return mName;
+  public String getFieldDisplayName() {
+    return mDisplayName;
   }
 
-  public ConnectionObjectOpenGraphObject setFieldName(String value) {
-    this.mName = value;
+  public ConnectionObjectOpenGraphObject setFieldDisplayName(String value) {
+    this.mDisplayName = value;
     return this;
   }
 
-  public String getFieldDisplayName() {
-    return mDisplayName;
+  public String getFieldName() {
+    return mName;
   }
 
-  public ConnectionObjectOpenGraphObject setFieldDisplayName(String value) {
-    this.mDisplayName = value;
+  public ConnectionObjectOpenGraphObject setFieldName(String value) {
+    this.mName = value;
     return this;
   }
 
@@ -199,17 +247,17 @@ public class ConnectionObjectOpenGraphObject extends APINode {
   }
 
   public ConnectionObjectOpenGraphObject copyFrom(ConnectionObjectOpenGraphObject instance) {
-    this.mName = instance.mName;
     this.mDisplayName = instance.mDisplayName;
+    this.mName = instance.mName;
     this.mProperties = instance.mProperties;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<ConnectionObjectOpenGraphObject> getParser() {
     return new APIRequest.ResponseParser<ConnectionObjectOpenGraphObject>() {
-      public APINodeList<ConnectionObjectOpenGraphObject> parseResponse(String response, APIContext context, APIRequest<ConnectionObjectOpenGraphObject> request) {
+      public APINodeList<ConnectionObjectOpenGraphObject> parseResponse(String response, APIContext context, APIRequest<ConnectionObjectOpenGraphObject> request) throws MalformedResponseException {
         return ConnectionObjectOpenGraphObject.parseResponse(response, context, request);
       }
     };
