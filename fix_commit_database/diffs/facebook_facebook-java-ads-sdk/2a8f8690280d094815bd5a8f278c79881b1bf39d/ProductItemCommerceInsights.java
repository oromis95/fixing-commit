@@ -24,36 +24,39 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class ProductItemCommerceInsights extends APINode {
+  @SerializedName("message_sends")
+  private Long mMessageSends = null;
   @SerializedName("organic_impressions")
   private Long mOrganicImpressions = null;
   @SerializedName("paid_impressions")
   private Long mPaidImpressions = null;
-  @SerializedName("message_sends")
-  private Long mMessageSends = null;
   protected static Gson gson = null;
 
   public ProductItemCommerceInsights() {
@@ -71,22 +74,23 @@ public class ProductItemCommerceInsights extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    productItemCommerceInsights.mContext = context;
+    productItemCommerceInsights.context = context;
     productItemCommerceInsights.rawValue = json;
     return productItemCommerceInsights;
   }
 
-  public static APINodeList<ProductItemCommerceInsights> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<ProductItemCommerceInsights> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<ProductItemCommerceInsights> productItemCommerceInsightss = new APINodeList<ProductItemCommerceInsights>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -99,10 +103,11 @@ public class ProductItemCommerceInsights extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            productItemCommerceInsightss.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            productItemCommerceInsightss.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -113,7 +118,20 @@ public class ProductItemCommerceInsights extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            productItemCommerceInsightss.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  productItemCommerceInsightss.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              productItemCommerceInsightss.add(loadJSON(obj.toString(), context));
+            }
           }
           return productItemCommerceInsightss;
         } else if (obj.has("images")) {
@@ -124,24 +142,54 @@ public class ProductItemCommerceInsights extends APINode {
           }
           return productItemCommerceInsightss;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              productItemCommerceInsightss.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return productItemCommerceInsightss;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          productItemCommerceInsightss.clear();
           productItemCommerceInsightss.add(loadJSON(json, context));
           return productItemCommerceInsightss;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -150,6 +198,15 @@ public class ProductItemCommerceInsights extends APINode {
   }
 
 
+  public Long getFieldMessageSends() {
+    return mMessageSends;
+  }
+
+  public ProductItemCommerceInsights setFieldMessageSends(Long value) {
+    this.mMessageSends = value;
+    return this;
+  }
+
   public Long getFieldOrganicImpressions() {
     return mOrganicImpressions;
   }
@@ -168,15 +225,6 @@ public class ProductItemCommerceInsights extends APINode {
     return this;
   }
 
-  public Long getFieldMessageSends() {
-    return mMessageSends;
-  }
-
-  public ProductItemCommerceInsights setFieldMessageSends(Long value) {
-    this.mMessageSends = value;
-    return this;
-  }
-
 
 
 
@@ -194,17 +242,17 @@ public class ProductItemCommerceInsights extends APINode {
   }
 
   public ProductItemCommerceInsights copyFrom(ProductItemCommerceInsights instance) {
+    this.mMessageSends = instance.mMessageSends;
     this.mOrganicImpressions = instance.mOrganicImpressions;
     this.mPaidImpressions = instance.mPaidImpressions;
-    this.mMessageSends = instance.mMessageSends;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<ProductItemCommerceInsights> getParser() {
     return new APIRequest.ResponseParser<ProductItemCommerceInsights>() {
-      public APINodeList<ProductItemCommerceInsights> parseResponse(String response, APIContext context, APIRequest<ProductItemCommerceInsights> request) {
+      public APINodeList<ProductItemCommerceInsights> parseResponse(String response, APIContext context, APIRequest<ProductItemCommerceInsights> request) throws MalformedResponseException {
         return ProductItemCommerceInsights.parseResponse(response, context, request);
       }
     };
