@@ -24,36 +24,39 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AdsPixelStatsResult extends APINode {
   @SerializedName("aggregation")
   private EnumAggregation mAggregation = null;
-  @SerializedName("timestamp")
-  private String mTimestamp = null;
   @SerializedName("data")
   private List<AdsPixelStats> mData = null;
+  @SerializedName("timestamp")
+  private String mTimestamp = null;
   protected static Gson gson = null;
 
   public AdsPixelStatsResult() {
@@ -71,22 +74,23 @@ public class AdsPixelStatsResult extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    adsPixelStatsResult.mContext = context;
+    adsPixelStatsResult.context = context;
     adsPixelStatsResult.rawValue = json;
     return adsPixelStatsResult;
   }
 
-  public static APINodeList<AdsPixelStatsResult> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AdsPixelStatsResult> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AdsPixelStatsResult> adsPixelStatsResults = new APINodeList<AdsPixelStatsResult>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -99,10 +103,11 @@ public class AdsPixelStatsResult extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            adsPixelStatsResults.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            adsPixelStatsResults.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -113,7 +118,20 @@ public class AdsPixelStatsResult extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            adsPixelStatsResults.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  adsPixelStatsResults.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              adsPixelStatsResults.add(loadJSON(obj.toString(), context));
+            }
           }
           return adsPixelStatsResults;
         } else if (obj.has("images")) {
@@ -124,24 +142,54 @@ public class AdsPixelStatsResult extends APINode {
           }
           return adsPixelStatsResults;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              adsPixelStatsResults.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return adsPixelStatsResults;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          adsPixelStatsResults.clear();
           adsPixelStatsResults.add(loadJSON(json, context));
           return adsPixelStatsResults;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -159,15 +207,6 @@ public class AdsPixelStatsResult extends APINode {
     return this;
   }
 
-  public String getFieldTimestamp() {
-    return mTimestamp;
-  }
-
-  public AdsPixelStatsResult setFieldTimestamp(String value) {
-    this.mTimestamp = value;
-    return this;
-  }
-
   public List<AdsPixelStats> getFieldData() {
     return mData;
   }
@@ -182,41 +221,51 @@ public class AdsPixelStatsResult extends APINode {
     this.mData = AdsPixelStats.getGson().fromJson(value, type);
     return this;
   }
+  public String getFieldTimestamp() {
+    return mTimestamp;
+  }
+
+  public AdsPixelStatsResult setFieldTimestamp(String value) {
+    this.mTimestamp = value;
+    return this;
+  }
+
 
 
   public static enum EnumAggregation {
-    @SerializedName("browser_type")
-    VALUE_BROWSER_TYPE("browser_type"),
-    @SerializedName("custom_data_field")
-    VALUE_CUSTOM_DATA_FIELD("custom_data_field"),
-    @SerializedName("device_os")
-    VALUE_DEVICE_OS("device_os"),
-    @SerializedName("device_type")
-    VALUE_DEVICE_TYPE("device_type"),
-    @SerializedName("event")
-    VALUE_EVENT("event"),
-    @SerializedName("pixel_fire")
-    VALUE_PIXEL_FIRE("pixel_fire"),
-    @SerializedName("host")
-    VALUE_HOST("host"),
-    @SerializedName("user_match")
-    VALUE_USER_MATCH("user_match"),
-    @SerializedName("url")
-    VALUE_URL("url"),
-    NULL(null);
-
-    private String value;
-
-    private EnumAggregation(String value) {
-      this.value = value;
-    }
+      @SerializedName("browser_type")
+      VALUE_BROWSER_TYPE("browser_type"),
+      @SerializedName("custom_data_field")
+      VALUE_CUSTOM_DATA_FIELD("custom_data_field"),
+      @SerializedName("device_os")
+      VALUE_DEVICE_OS("device_os"),
+      @SerializedName("device_type")
+      VALUE_DEVICE_TYPE("device_type"),
+      @SerializedName("event")
+      VALUE_EVENT("event"),
+      @SerializedName("host")
+      VALUE_HOST("host"),
+      @SerializedName("user_match")
+      VALUE_USER_MATCH("user_match"),
+      @SerializedName("pixel_fire")
+      VALUE_PIXEL_FIRE("pixel_fire"),
+      @SerializedName("url")
+      VALUE_URL("url"),
+      NULL(null);
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      private String value;
+
+      private EnumAggregation(String value) {
+        this.value = value;
+      }
+
+      @Override
+      public String toString() {
+        return value;
+      }
   }
 
+
   synchronized /*package*/ static Gson getGson() {
     if (gson != null) {
       return gson;
@@ -232,16 +281,16 @@ public class AdsPixelStatsResult extends APINode {
 
   public AdsPixelStatsResult copyFrom(AdsPixelStatsResult instance) {
     this.mAggregation = instance.mAggregation;
-    this.mTimestamp = instance.mTimestamp;
     this.mData = instance.mData;
-    this.mContext = instance.mContext;
+    this.mTimestamp = instance.mTimestamp;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AdsPixelStatsResult> getParser() {
     return new APIRequest.ResponseParser<AdsPixelStatsResult>() {
-      public APINodeList<AdsPixelStatsResult> parseResponse(String response, APIContext context, APIRequest<AdsPixelStatsResult> request) {
+      public APINodeList<AdsPixelStatsResult> parseResponse(String response, APIContext context, APIRequest<AdsPixelStatsResult> request) throws MalformedResponseException {
         return AdsPixelStatsResult.parseResponse(response, context, request);
       }
     };
