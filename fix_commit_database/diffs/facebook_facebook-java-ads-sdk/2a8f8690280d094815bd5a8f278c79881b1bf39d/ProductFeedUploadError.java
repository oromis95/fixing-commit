@@ -24,42 +24,41 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class ProductFeedUploadError extends APINode {
-  @SerializedName("id")
-  private String mId = null;
-  @SerializedName("summary")
-  private String mSummary = null;
   @SerializedName("description")
   private String mDescription = null;
+  @SerializedName("id")
+  private String mId = null;
   @SerializedName("severity")
   private EnumSeverity mSeverity = null;
-  @SerializedName("row_number")
-  private Long mRowNumber = null;
-  @SerializedName("column_number")
-  private Long mColumnNumber = null;
+  @SerializedName("summary")
+  private String mSummary = null;
   @SerializedName("total_count")
   private Long mTotalCount = null;
   protected static Gson gson = null;
@@ -73,11 +72,11 @@ public class ProductFeedUploadError extends APINode {
 
   public ProductFeedUploadError(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public ProductFeedUploadError fetch() throws APIException{
-    ProductFeedUploadError newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    ProductFeedUploadError newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -94,8 +93,17 @@ public class ProductFeedUploadError extends APINode {
     return productFeedUploadError;
   }
 
+  public static APINodeList<ProductFeedUploadError> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<ProductFeedUploadError>)(
+      new APIRequest<ProductFeedUploadError>(context, "", "/", "GET", ProductFeedUploadError.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -110,22 +118,23 @@ public class ProductFeedUploadError extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    productFeedUploadError.mContext = context;
+    productFeedUploadError.context = context;
     productFeedUploadError.rawValue = json;
     return productFeedUploadError;
   }
 
-  public static APINodeList<ProductFeedUploadError> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<ProductFeedUploadError> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<ProductFeedUploadError> productFeedUploadErrors = new APINodeList<ProductFeedUploadError>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -138,10 +147,11 @@ public class ProductFeedUploadError extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            productFeedUploadErrors.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            productFeedUploadErrors.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -152,7 +162,20 @@ public class ProductFeedUploadError extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            productFeedUploadErrors.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  productFeedUploadErrors.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              productFeedUploadErrors.add(loadJSON(obj.toString(), context));
+            }
           }
           return productFeedUploadErrors;
         } else if (obj.has("images")) {
@@ -163,24 +186,54 @@ public class ProductFeedUploadError extends APINode {
           }
           return productFeedUploadErrors;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              productFeedUploadErrors.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return productFeedUploadErrors;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          productFeedUploadErrors.clear();
           productFeedUploadErrors.add(loadJSON(json, context));
           return productFeedUploadErrors;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -188,37 +241,29 @@ public class ProductFeedUploadError extends APINode {
     return getGson().toJson(this);
   }
 
-  public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
-  }
-
   public APIRequestGetSamples getSamples() {
-    return new APIRequestGetSamples(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetSamples(this.getPrefixedId().toString(), context);
   }
 
-
-  public String getFieldId() {
-    return mId;
+  public APIRequestGet get() {
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
   }
 
-  public String getFieldSummary() {
-    return mSummary;
-  }
 
   public String getFieldDescription() {
     return mDescription;
   }
 
-  public EnumSeverity getFieldSeverity() {
-    return mSeverity;
+  public String getFieldId() {
+    return mId;
   }
 
-  public Long getFieldRowNumber() {
-    return mRowNumber;
+  public EnumSeverity getFieldSeverity() {
+    return mSeverity;
   }
 
-  public Long getFieldColumnNumber() {
-    return mColumnNumber;
+  public String getFieldSummary() {
+    return mSummary;
   }
 
   public Long getFieldTotalCount() {
@@ -227,11 +272,11 @@ public class ProductFeedUploadError extends APINode {
 
 
 
-  public static class APIRequestGet extends APIRequest<ProductFeedUploadError> {
+  public static class APIRequestGetSamples extends APIRequest<ProductFeedUploadErrorSample> {
 
-    ProductFeedUploadError lastResponse = null;
+    APINodeList<ProductFeedUploadErrorSample> lastResponse = null;
     @Override
-    public ProductFeedUploadError getLastResponse() {
+    public APINodeList<ProductFeedUploadErrorSample> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
@@ -239,251 +284,246 @@ public class ProductFeedUploadError extends APINode {
 
     public static final String[] FIELDS = {
       "id",
-      "summary",
-      "description",
-      "severity",
+      "retailer_id",
       "row_number",
-      "column_number",
-      "total_count",
     };
 
     @Override
-    public ProductFeedUploadError parseResponse(String response) throws APIException {
-      return ProductFeedUploadError.parseResponse(response, getContext(), this).head();
+    public APINodeList<ProductFeedUploadErrorSample> parseResponse(String response) throws APIException {
+      return ProductFeedUploadErrorSample.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public ProductFeedUploadError execute() throws APIException {
+    public APINodeList<ProductFeedUploadErrorSample> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public ProductFeedUploadError execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<ProductFeedUploadErrorSample> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGet(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetSamples(String nodeId, APIContext context) {
+      super(context, nodeId, "/samples", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGet setParam(String param, Object value) {
+    @Override
+    public APIRequestGetSamples setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGet setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetSamples setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGet requestAllFields () {
+    public APIRequestGetSamples requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGet requestAllFields (boolean value) {
+    public APIRequestGetSamples requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetSamples requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGet requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetSamples requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestField (String field) {
+    @Override
+    public APIRequestGetSamples requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGet requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetSamples requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGet requestIdField () {
+    public APIRequestGetSamples requestIdField () {
       return this.requestIdField(true);
     }
-    public APIRequestGet requestIdField (boolean value) {
+    public APIRequestGetSamples requestIdField (boolean value) {
       this.requestField("id", value);
       return this;
     }
-    public APIRequestGet requestSummaryField () {
-      return this.requestSummaryField(true);
-    }
-    public APIRequestGet requestSummaryField (boolean value) {
-      this.requestField("summary", value);
-      return this;
-    }
-    public APIRequestGet requestDescriptionField () {
-      return this.requestDescriptionField(true);
-    }
-    public APIRequestGet requestDescriptionField (boolean value) {
-      this.requestField("description", value);
-      return this;
-    }
-    public APIRequestGet requestSeverityField () {
-      return this.requestSeverityField(true);
+    public APIRequestGetSamples requestRetailerIdField () {
+      return this.requestRetailerIdField(true);
     }
-    public APIRequestGet requestSeverityField (boolean value) {
-      this.requestField("severity", value);
+    public APIRequestGetSamples requestRetailerIdField (boolean value) {
+      this.requestField("retailer_id", value);
       return this;
     }
-    public APIRequestGet requestRowNumberField () {
+    public APIRequestGetSamples requestRowNumberField () {
       return this.requestRowNumberField(true);
     }
-    public APIRequestGet requestRowNumberField (boolean value) {
+    public APIRequestGetSamples requestRowNumberField (boolean value) {
       this.requestField("row_number", value);
       return this;
     }
-    public APIRequestGet requestColumnNumberField () {
-      return this.requestColumnNumberField(true);
-    }
-    public APIRequestGet requestColumnNumberField (boolean value) {
-      this.requestField("column_number", value);
-      return this;
-    }
-    public APIRequestGet requestTotalCountField () {
-      return this.requestTotalCountField(true);
-    }
-    public APIRequestGet requestTotalCountField (boolean value) {
-      this.requestField("total_count", value);
-      return this;
-    }
-
   }
 
-  public static class APIRequestGetSamples extends APIRequest<ProductFeedUploadErrorSample> {
+  public static class APIRequestGet extends APIRequest<ProductFeedUploadError> {
 
-    APINodeList<ProductFeedUploadErrorSample> lastResponse = null;
+    ProductFeedUploadError lastResponse = null;
     @Override
-    public APINodeList<ProductFeedUploadErrorSample> getLastResponse() {
+    public ProductFeedUploadError getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
     };
 
     public static final String[] FIELDS = {
+      "description",
       "id",
-      "retailer_id",
-      "row_number",
+      "severity",
+      "summary",
+      "total_count",
     };
 
     @Override
-    public APINodeList<ProductFeedUploadErrorSample> parseResponse(String response) throws APIException {
-      return ProductFeedUploadErrorSample.parseResponse(response, getContext(), this);
+    public ProductFeedUploadError parseResponse(String response) throws APIException {
+      return ProductFeedUploadError.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public APINodeList<ProductFeedUploadErrorSample> execute() throws APIException {
+    public ProductFeedUploadError execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<ProductFeedUploadErrorSample> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public ProductFeedUploadError execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetSamples(String nodeId, APIContext context) {
-      super(context, nodeId, "/samples", "GET", Arrays.asList(PARAMS));
+    public APIRequestGet(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetSamples setParam(String param, Object value) {
+    @Override
+    public APIRequestGet setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetSamples setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGet setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetSamples requestAllFields () {
+    public APIRequestGet requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetSamples requestAllFields (boolean value) {
+    public APIRequestGet requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetSamples requestFields (List<String> fields) {
+    @Override
+    public APIRequestGet requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetSamples requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGet requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetSamples requestField (String field) {
+    @Override
+    public APIRequestGet requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetSamples requestField (String field, boolean value) {
+    @Override
+    public APIRequestGet requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetSamples requestIdField () {
+    public APIRequestGet requestDescriptionField () {
+      return this.requestDescriptionField(true);
+    }
+    public APIRequestGet requestDescriptionField (boolean value) {
+      this.requestField("description", value);
+      return this;
+    }
+    public APIRequestGet requestIdField () {
       return this.requestIdField(true);
     }
-    public APIRequestGetSamples requestIdField (boolean value) {
+    public APIRequestGet requestIdField (boolean value) {
       this.requestField("id", value);
       return this;
     }
-    public APIRequestGetSamples requestRetailerIdField () {
-      return this.requestRetailerIdField(true);
+    public APIRequestGet requestSeverityField () {
+      return this.requestSeverityField(true);
     }
-    public APIRequestGetSamples requestRetailerIdField (boolean value) {
-      this.requestField("retailer_id", value);
+    public APIRequestGet requestSeverityField (boolean value) {
+      this.requestField("severity", value);
       return this;
     }
-    public APIRequestGetSamples requestRowNumberField () {
-      return this.requestRowNumberField(true);
+    public APIRequestGet requestSummaryField () {
+      return this.requestSummaryField(true);
     }
-    public APIRequestGetSamples requestRowNumberField (boolean value) {
-      this.requestField("row_number", value);
+    public APIRequestGet requestSummaryField (boolean value) {
+      this.requestField("summary", value);
+      return this;
+    }
+    public APIRequestGet requestTotalCountField () {
+      return this.requestTotalCountField(true);
+    }
+    public APIRequestGet requestTotalCountField (boolean value) {
+      this.requestField("total_count", value);
       return this;
     }
-
   }
 
   public static enum EnumSeverity {
-    @SerializedName("fatal")
-    VALUE_FATAL("fatal"),
-    @SerializedName("warning")
-    VALUE_WARNING("warning"),
-    NULL(null);
+      @SerializedName("fatal")
+      VALUE_FATAL("fatal"),
+      @SerializedName("warning")
+      VALUE_WARNING("warning"),
+      NULL(null);
 
-    private String value;
+      private String value;
 
-    private EnumSeverity(String value) {
-      this.value = value;
-    }
+      private EnumSeverity(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
 
+
   synchronized /*package*/ static Gson getGson() {
     if (gson != null) {
       return gson;
@@ -498,21 +538,19 @@ public class ProductFeedUploadError extends APINode {
   }
 
   public ProductFeedUploadError copyFrom(ProductFeedUploadError instance) {
-    this.mId = instance.mId;
-    this.mSummary = instance.mSummary;
     this.mDescription = instance.mDescription;
+    this.mId = instance.mId;
     this.mSeverity = instance.mSeverity;
-    this.mRowNumber = instance.mRowNumber;
-    this.mColumnNumber = instance.mColumnNumber;
+    this.mSummary = instance.mSummary;
     this.mTotalCount = instance.mTotalCount;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<ProductFeedUploadError> getParser() {
     return new APIRequest.ResponseParser<ProductFeedUploadError>() {
-      public APINodeList<ProductFeedUploadError> parseResponse(String response, APIContext context, APIRequest<ProductFeedUploadError> request) {
+      public APINodeList<ProductFeedUploadError> parseResponse(String response, APIContext context, APIRequest<ProductFeedUploadError> request) throws MalformedResponseException {
         return ProductFeedUploadError.parseResponse(response, context, request);
       }
     };
