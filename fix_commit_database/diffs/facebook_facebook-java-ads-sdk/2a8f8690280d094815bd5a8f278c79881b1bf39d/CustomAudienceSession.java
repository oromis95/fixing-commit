@@ -24,46 +24,49 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class CustomAudienceSession extends APINode {
-  @SerializedName("session_id")
-  private String mSessionId = null;
-  @SerializedName("start_time")
-  private String mStartTime = null;
   @SerializedName("end_time")
   private String mEndTime = null;
-  @SerializedName("num_received")
-  private String mNumReceived = null;
-  @SerializedName("num_matched")
-  private String mNumMatched = null;
   @SerializedName("num_invalid_entries")
   private String mNumInvalidEntries = null;
+  @SerializedName("num_matched")
+  private String mNumMatched = null;
+  @SerializedName("num_received")
+  private String mNumReceived = null;
   @SerializedName("progress")
   private String mProgress = null;
+  @SerializedName("session_id")
+  private String mSessionId = null;
   @SerializedName("stage")
   private String mStage = null;
+  @SerializedName("start_time")
+  private String mStartTime = null;
   protected static Gson gson = null;
 
   public CustomAudienceSession() {
@@ -81,22 +84,23 @@ public class CustomAudienceSession extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    customAudienceSession.mContext = context;
+    customAudienceSession.context = context;
     customAudienceSession.rawValue = json;
     return customAudienceSession;
   }
 
-  public static APINodeList<CustomAudienceSession> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<CustomAudienceSession> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<CustomAudienceSession> customAudienceSessions = new APINodeList<CustomAudienceSession>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -109,10 +113,11 @@ public class CustomAudienceSession extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            customAudienceSessions.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            customAudienceSessions.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -123,7 +128,20 @@ public class CustomAudienceSession extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            customAudienceSessions.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  customAudienceSessions.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              customAudienceSessions.add(loadJSON(obj.toString(), context));
+            }
           }
           return customAudienceSessions;
         } else if (obj.has("images")) {
@@ -134,24 +152,54 @@ public class CustomAudienceSession extends APINode {
           }
           return customAudienceSessions;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              customAudienceSessions.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return customAudienceSessions;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          customAudienceSessions.clear();
           customAudienceSessions.add(loadJSON(json, context));
           return customAudienceSessions;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -160,24 +208,6 @@ public class CustomAudienceSession extends APINode {
   }
 
 
-  public String getFieldSessionId() {
-    return mSessionId;
-  }
-
-  public CustomAudienceSession setFieldSessionId(String value) {
-    this.mSessionId = value;
-    return this;
-  }
-
-  public String getFieldStartTime() {
-    return mStartTime;
-  }
-
-  public CustomAudienceSession setFieldStartTime(String value) {
-    this.mStartTime = value;
-    return this;
-  }
-
   public String getFieldEndTime() {
     return mEndTime;
   }
@@ -187,12 +217,12 @@ public class CustomAudienceSession extends APINode {
     return this;
   }
 
-  public String getFieldNumReceived() {
-    return mNumReceived;
+  public String getFieldNumInvalidEntries() {
+    return mNumInvalidEntries;
   }
 
-  public CustomAudienceSession setFieldNumReceived(String value) {
-    this.mNumReceived = value;
+  public CustomAudienceSession setFieldNumInvalidEntries(String value) {
+    this.mNumInvalidEntries = value;
     return this;
   }
 
@@ -205,12 +235,12 @@ public class CustomAudienceSession extends APINode {
     return this;
   }
 
-  public String getFieldNumInvalidEntries() {
-    return mNumInvalidEntries;
+  public String getFieldNumReceived() {
+    return mNumReceived;
   }
 
-  public CustomAudienceSession setFieldNumInvalidEntries(String value) {
-    this.mNumInvalidEntries = value;
+  public CustomAudienceSession setFieldNumReceived(String value) {
+    this.mNumReceived = value;
     return this;
   }
 
@@ -223,6 +253,15 @@ public class CustomAudienceSession extends APINode {
     return this;
   }
 
+  public String getFieldSessionId() {
+    return mSessionId;
+  }
+
+  public CustomAudienceSession setFieldSessionId(String value) {
+    this.mSessionId = value;
+    return this;
+  }
+
   public String getFieldStage() {
     return mStage;
   }
@@ -232,6 +271,15 @@ public class CustomAudienceSession extends APINode {
     return this;
   }
 
+  public String getFieldStartTime() {
+    return mStartTime;
+  }
+
+  public CustomAudienceSession setFieldStartTime(String value) {
+    this.mStartTime = value;
+    return this;
+  }
+
 
 
 
@@ -249,22 +297,22 @@ public class CustomAudienceSession extends APINode {
   }
 
   public CustomAudienceSession copyFrom(CustomAudienceSession instance) {
-    this.mSessionId = instance.mSessionId;
-    this.mStartTime = instance.mStartTime;
     this.mEndTime = instance.mEndTime;
-    this.mNumReceived = instance.mNumReceived;
-    this.mNumMatched = instance.mNumMatched;
     this.mNumInvalidEntries = instance.mNumInvalidEntries;
+    this.mNumMatched = instance.mNumMatched;
+    this.mNumReceived = instance.mNumReceived;
     this.mProgress = instance.mProgress;
+    this.mSessionId = instance.mSessionId;
     this.mStage = instance.mStage;
-    this.mContext = instance.mContext;
+    this.mStartTime = instance.mStartTime;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<CustomAudienceSession> getParser() {
     return new APIRequest.ResponseParser<CustomAudienceSession>() {
-      public APINodeList<CustomAudienceSession> parseResponse(String response, APIContext context, APIRequest<CustomAudienceSession> request) {
+      public APINodeList<CustomAudienceSession> parseResponse(String response, APIContext context, APIRequest<CustomAudienceSession> request) throws MalformedResponseException {
         return CustomAudienceSession.parseResponse(response, context, request);
       }
     };
