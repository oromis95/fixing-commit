@@ -24,29 +24,32 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AdsDataPartner extends APINode {
   @SerializedName("id")
   private String mId = null;
@@ -65,11 +68,11 @@ public class AdsDataPartner extends APINode {
 
   public AdsDataPartner(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public AdsDataPartner fetch() throws APIException{
-    AdsDataPartner newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    AdsDataPartner newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -86,8 +89,17 @@ public class AdsDataPartner extends APINode {
     return adsDataPartner;
   }
 
+  public static APINodeList<AdsDataPartner> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<AdsDataPartner>)(
+      new APIRequest<AdsDataPartner>(context, "", "/", "GET", AdsDataPartner.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -102,22 +114,23 @@ public class AdsDataPartner extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    adsDataPartner.mContext = context;
+    adsDataPartner.context = context;
     adsDataPartner.rawValue = json;
     return adsDataPartner;
   }
 
-  public static APINodeList<AdsDataPartner> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AdsDataPartner> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AdsDataPartner> adsDataPartners = new APINodeList<AdsDataPartner>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -130,10 +143,11 @@ public class AdsDataPartner extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            adsDataPartners.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            adsDataPartners.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -144,7 +158,20 @@ public class AdsDataPartner extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            adsDataPartners.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  adsDataPartners.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              adsDataPartners.add(loadJSON(obj.toString(), context));
+            }
           }
           return adsDataPartners;
         } else if (obj.has("images")) {
@@ -155,24 +182,54 @@ public class AdsDataPartner extends APINode {
           }
           return adsDataPartners;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              adsDataPartners.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return adsDataPartners;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          adsDataPartners.clear();
           adsDataPartners.add(loadJSON(json, context));
           return adsDataPartners;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -181,7 +238,7 @@ public class AdsDataPartner extends APINode {
   }
 
   public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
   }
 
 
@@ -227,7 +284,7 @@ public class AdsDataPartner extends APINode {
 
     @Override
     public AdsDataPartner execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -235,11 +292,13 @@ public class AdsDataPartner extends APINode {
       super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestGet setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestGet setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
@@ -257,10 +316,12 @@ public class AdsDataPartner extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGet requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestGet requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -268,11 +329,13 @@ public class AdsDataPartner extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGet requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGet requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
@@ -299,7 +362,6 @@ public class AdsDataPartner extends APINode {
       this.requestField("rev_share_policies", value);
       return this;
     }
-
   }
 
 
@@ -320,14 +382,14 @@ public class AdsDataPartner extends APINode {
     this.mId = instance.mId;
     this.mName = instance.mName;
     this.mRevSharePolicies = instance.mRevSharePolicies;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AdsDataPartner> getParser() {
     return new APIRequest.ResponseParser<AdsDataPartner>() {
-      public APINodeList<AdsDataPartner> parseResponse(String response, APIContext context, APIRequest<AdsDataPartner> request) {
+      public APINodeList<AdsDataPartner> parseResponse(String response, APIContext context, APIRequest<AdsDataPartner> request) throws MalformedResponseException {
         return AdsDataPartner.parseResponse(response, context, request);
       }
     };
