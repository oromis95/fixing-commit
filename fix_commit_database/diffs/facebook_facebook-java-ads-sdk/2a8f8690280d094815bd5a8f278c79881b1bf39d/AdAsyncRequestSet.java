@@ -24,62 +24,65 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AdAsyncRequestSet extends APINode {
+  @SerializedName("canceled_count")
+  private Long mCanceledCount = null;
+  @SerializedName("created_time")
+  private String mCreatedTime = null;
+  @SerializedName("error_count")
+  private Long mErrorCount = null;
   @SerializedName("id")
   private String mId = null;
-  @SerializedName("owner_id")
-  private String mOwnerId = null;
+  @SerializedName("in_progress_count")
+  private Long mInProgressCount = null;
+  @SerializedName("initial_count")
+  private Long mInitialCount = null;
+  @SerializedName("is_completed")
+  private Boolean mIsCompleted = null;
   @SerializedName("name")
   private String mName = null;
   @SerializedName("notification_mode")
   private EnumNotificationMode mNotificationMode = null;
+  @SerializedName("notification_result")
+  private AdAsyncRequestSetNotificationResult mNotificationResult = null;
+  @SerializedName("notification_status")
+  private EnumNotificationStatus mNotificationStatus = null;
   @SerializedName("notification_uri")
   private String mNotificationUri = null;
-  @SerializedName("total_count")
-  private Long mTotalCount = null;
-  @SerializedName("initial_count")
-  private Long mInitialCount = null;
-  @SerializedName("in_progress_count")
-  private Long mInProgressCount = null;
+  @SerializedName("owner_id")
+  private String mOwnerId = null;
   @SerializedName("success_count")
   private Long mSuccessCount = null;
-  @SerializedName("error_count")
-  private Long mErrorCount = null;
-  @SerializedName("canceled_count")
-  private Long mCanceledCount = null;
-  @SerializedName("is_completed")
-  private Boolean mIsCompleted = null;
-  @SerializedName("created_time")
-  private String mCreatedTime = null;
+  @SerializedName("total_count")
+  private Long mTotalCount = null;
   @SerializedName("updated_time")
   private String mUpdatedTime = null;
-  @SerializedName("notification_status")
-  private EnumNotificationStatus mNotificationStatus = null;
-  @SerializedName("notification_result")
-  private AdAsyncRequestSetNotificationResult mNotificationResult = null;
   protected static Gson gson = null;
 
   AdAsyncRequestSet() {
@@ -91,11 +94,11 @@ public class AdAsyncRequestSet extends APINode {
 
   public AdAsyncRequestSet(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public AdAsyncRequestSet fetch() throws APIException{
-    AdAsyncRequestSet newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    AdAsyncRequestSet newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -112,8 +115,17 @@ public class AdAsyncRequestSet extends APINode {
     return adAsyncRequestSet;
   }
 
+  public static APINodeList<AdAsyncRequestSet> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<AdAsyncRequestSet>)(
+      new APIRequest<AdAsyncRequestSet>(context, "", "/", "GET", AdAsyncRequestSet.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -128,22 +140,23 @@ public class AdAsyncRequestSet extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    adAsyncRequestSet.mContext = context;
+    adAsyncRequestSet.context = context;
     adAsyncRequestSet.rawValue = json;
     return adAsyncRequestSet;
   }
 
-  public static APINodeList<AdAsyncRequestSet> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AdAsyncRequestSet> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AdAsyncRequestSet> adAsyncRequestSets = new APINodeList<AdAsyncRequestSet>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -156,10 +169,11 @@ public class AdAsyncRequestSet extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            adAsyncRequestSets.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            adAsyncRequestSets.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -170,7 +184,20 @@ public class AdAsyncRequestSet extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            adAsyncRequestSets.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  adAsyncRequestSets.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              adAsyncRequestSets.add(loadJSON(obj.toString(), context));
+            }
           }
           return adAsyncRequestSets;
         } else if (obj.has("images")) {
@@ -181,24 +208,54 @@ public class AdAsyncRequestSet extends APINode {
           }
           return adAsyncRequestSets;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              adAsyncRequestSets.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return adAsyncRequestSets;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          adAsyncRequestSets.clear();
           adAsyncRequestSets.add(loadJSON(json, context));
           return adAsyncRequestSets;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -206,21 +263,41 @@ public class AdAsyncRequestSet extends APINode {
     return getGson().toJson(this);
   }
 
+  public APIRequestGetRequests getRequests() {
+    return new APIRequestGetRequests(this.getPrefixedId().toString(), context);
+  }
+
   public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetRequests getRequests() {
-    return new APIRequestGetRequests(this.getPrefixedId().toString(), mContext);
+
+  public Long getFieldCanceledCount() {
+    return mCanceledCount;
   }
 
+  public String getFieldCreatedTime() {
+    return mCreatedTime;
+  }
+
+  public Long getFieldErrorCount() {
+    return mErrorCount;
+  }
 
   public String getFieldId() {
     return mId;
   }
 
-  public String getFieldOwnerId() {
-    return mOwnerId;
+  public Long getFieldInProgressCount() {
+    return mInProgressCount;
+  }
+
+  public Long getFieldInitialCount() {
+    return mInitialCount;
+  }
+
+  public Boolean getFieldIsCompleted() {
+    return mIsCompleted;
   }
 
   public String getFieldName() {
@@ -231,488 +308,447 @@ public class AdAsyncRequestSet extends APINode {
     return mNotificationMode;
   }
 
-  public String getFieldNotificationUri() {
-    return mNotificationUri;
+  public AdAsyncRequestSetNotificationResult getFieldNotificationResult() {
+    return mNotificationResult;
   }
 
-  public Long getFieldTotalCount() {
-    return mTotalCount;
+  public EnumNotificationStatus getFieldNotificationStatus() {
+    return mNotificationStatus;
   }
 
-  public Long getFieldInitialCount() {
-    return mInitialCount;
+  public String getFieldNotificationUri() {
+    return mNotificationUri;
   }
 
-  public Long getFieldInProgressCount() {
-    return mInProgressCount;
+  public String getFieldOwnerId() {
+    return mOwnerId;
   }
 
   public Long getFieldSuccessCount() {
     return mSuccessCount;
   }
 
-  public Long getFieldErrorCount() {
-    return mErrorCount;
-  }
-
-  public Long getFieldCanceledCount() {
-    return mCanceledCount;
-  }
-
-  public Boolean getFieldIsCompleted() {
-    return mIsCompleted;
-  }
-
-  public String getFieldCreatedTime() {
-    return mCreatedTime;
+  public Long getFieldTotalCount() {
+    return mTotalCount;
   }
 
   public String getFieldUpdatedTime() {
     return mUpdatedTime;
   }
 
-  public EnumNotificationStatus getFieldNotificationStatus() {
-    return mNotificationStatus;
-  }
-
-  public AdAsyncRequestSetNotificationResult getFieldNotificationResult() {
-    return mNotificationResult;
-  }
-
 
 
-  public static class APIRequestGet extends APIRequest<AdAsyncRequestSet> {
+  public static class APIRequestGetRequests extends APIRequest<AdAsyncRequest> {
 
-    AdAsyncRequestSet lastResponse = null;
+    APINodeList<AdAsyncRequest> lastResponse = null;
     @Override
-    public AdAsyncRequestSet getLastResponse() {
+    public APINodeList<AdAsyncRequest> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
+      "statuses",
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "owner_id",
-      "name",
-      "notification_mode",
-      "notification_uri",
-      "total_count",
-      "initial_count",
-      "in_progress_count",
-      "success_count",
-      "error_count",
-      "canceled_count",
-      "is_completed",
+      "async_request_set",
       "created_time",
+      "id",
+      "input",
+      "result",
+      "scope_object_id",
+      "status",
       "updated_time",
-      "notification_status",
-      "notification_result",
     };
 
     @Override
-    public AdAsyncRequestSet parseResponse(String response) throws APIException {
-      return AdAsyncRequestSet.parseResponse(response, getContext(), this).head();
+    public APINodeList<AdAsyncRequest> parseResponse(String response) throws APIException {
+      return AdAsyncRequest.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public AdAsyncRequestSet execute() throws APIException {
+    public APINodeList<AdAsyncRequest> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public AdAsyncRequestSet execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<AdAsyncRequest> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGet(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetRequests(String nodeId, APIContext context) {
+      super(context, nodeId, "/requests", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGet setParam(String param, Object value) {
+    @Override
+    public APIRequestGetRequests setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGet setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetRequests setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGet requestAllFields () {
+    public APIRequestGetRequests setStatuses (List<AdAsyncRequest.EnumStatuses> statuses) {
+      this.setParam("statuses", statuses);
+      return this;
+    }
+    public APIRequestGetRequests setStatuses (String statuses) {
+      this.setParam("statuses", statuses);
+      return this;
+    }
+
+    public APIRequestGetRequests requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGet requestAllFields (boolean value) {
+    public APIRequestGetRequests requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetRequests requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGet requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetRequests requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestField (String field) {
+    @Override
+    public APIRequestGetRequests requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGet requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetRequests requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGet requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGet requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
-    }
-    public APIRequestGet requestOwnerIdField () {
-      return this.requestOwnerIdField(true);
+    public APIRequestGetRequests requestAsyncRequestSetField () {
+      return this.requestAsyncRequestSetField(true);
     }
-    public APIRequestGet requestOwnerIdField (boolean value) {
-      this.requestField("owner_id", value);
+    public APIRequestGetRequests requestAsyncRequestSetField (boolean value) {
+      this.requestField("async_request_set", value);
       return this;
     }
-    public APIRequestGet requestNameField () {
-      return this.requestNameField(true);
+    public APIRequestGetRequests requestCreatedTimeField () {
+      return this.requestCreatedTimeField(true);
     }
-    public APIRequestGet requestNameField (boolean value) {
-      this.requestField("name", value);
+    public APIRequestGetRequests requestCreatedTimeField (boolean value) {
+      this.requestField("created_time", value);
       return this;
     }
-    public APIRequestGet requestNotificationModeField () {
-      return this.requestNotificationModeField(true);
+    public APIRequestGetRequests requestIdField () {
+      return this.requestIdField(true);
     }
-    public APIRequestGet requestNotificationModeField (boolean value) {
-      this.requestField("notification_mode", value);
+    public APIRequestGetRequests requestIdField (boolean value) {
+      this.requestField("id", value);
       return this;
     }
-    public APIRequestGet requestNotificationUriField () {
-      return this.requestNotificationUriField(true);
+    public APIRequestGetRequests requestInputField () {
+      return this.requestInputField(true);
     }
-    public APIRequestGet requestNotificationUriField (boolean value) {
-      this.requestField("notification_uri", value);
+    public APIRequestGetRequests requestInputField (boolean value) {
+      this.requestField("input", value);
       return this;
     }
-    public APIRequestGet requestTotalCountField () {
-      return this.requestTotalCountField(true);
+    public APIRequestGetRequests requestResultField () {
+      return this.requestResultField(true);
     }
-    public APIRequestGet requestTotalCountField (boolean value) {
-      this.requestField("total_count", value);
+    public APIRequestGetRequests requestResultField (boolean value) {
+      this.requestField("result", value);
       return this;
     }
-    public APIRequestGet requestInitialCountField () {
-      return this.requestInitialCountField(true);
+    public APIRequestGetRequests requestScopeObjectIdField () {
+      return this.requestScopeObjectIdField(true);
     }
-    public APIRequestGet requestInitialCountField (boolean value) {
-      this.requestField("initial_count", value);
+    public APIRequestGetRequests requestScopeObjectIdField (boolean value) {
+      this.requestField("scope_object_id", value);
       return this;
     }
-    public APIRequestGet requestInProgressCountField () {
-      return this.requestInProgressCountField(true);
+    public APIRequestGetRequests requestStatusField () {
+      return this.requestStatusField(true);
     }
-    public APIRequestGet requestInProgressCountField (boolean value) {
-      this.requestField("in_progress_count", value);
+    public APIRequestGetRequests requestStatusField (boolean value) {
+      this.requestField("status", value);
       return this;
     }
-    public APIRequestGet requestSuccessCountField () {
-      return this.requestSuccessCountField(true);
+    public APIRequestGetRequests requestUpdatedTimeField () {
+      return this.requestUpdatedTimeField(true);
     }
-    public APIRequestGet requestSuccessCountField (boolean value) {
-      this.requestField("success_count", value);
+    public APIRequestGetRequests requestUpdatedTimeField (boolean value) {
+      this.requestField("updated_time", value);
       return this;
     }
-    public APIRequestGet requestErrorCountField () {
-      return this.requestErrorCountField(true);
-    }
-    public APIRequestGet requestErrorCountField (boolean value) {
-      this.requestField("error_count", value);
-      return this;
-    }
-    public APIRequestGet requestCanceledCountField () {
-      return this.requestCanceledCountField(true);
-    }
-    public APIRequestGet requestCanceledCountField (boolean value) {
-      this.requestField("canceled_count", value);
-      return this;
-    }
-    public APIRequestGet requestIsCompletedField () {
-      return this.requestIsCompletedField(true);
-    }
-    public APIRequestGet requestIsCompletedField (boolean value) {
-      this.requestField("is_completed", value);
-      return this;
-    }
-    public APIRequestGet requestCreatedTimeField () {
-      return this.requestCreatedTimeField(true);
-    }
-    public APIRequestGet requestCreatedTimeField (boolean value) {
-      this.requestField("created_time", value);
-      return this;
-    }
-    public APIRequestGet requestUpdatedTimeField () {
-      return this.requestUpdatedTimeField(true);
-    }
-    public APIRequestGet requestUpdatedTimeField (boolean value) {
-      this.requestField("updated_time", value);
-      return this;
-    }
-    public APIRequestGet requestNotificationStatusField () {
-      return this.requestNotificationStatusField(true);
-    }
-    public APIRequestGet requestNotificationStatusField (boolean value) {
-      this.requestField("notification_status", value);
-      return this;
-    }
-    public APIRequestGet requestNotificationResultField () {
-      return this.requestNotificationResultField(true);
-    }
-    public APIRequestGet requestNotificationResultField (boolean value) {
-      this.requestField("notification_result", value);
-      return this;
-    }
-
   }
 
-  public static class APIRequestGetRequests extends APIRequest<AdAsyncRequest> {
+  public static class APIRequestGet extends APIRequest<AdAsyncRequestSet> {
 
-    APINodeList<AdAsyncRequest> lastResponse = null;
+    AdAsyncRequestSet lastResponse = null;
     @Override
-    public APINodeList<AdAsyncRequest> getLastResponse() {
+    public AdAsyncRequestSet getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "statuses",
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "scope_object_id",
-      "status",
-      "result",
-      "input",
-      "async_request_set",
+      "canceled_count",
       "created_time",
+      "error_count",
+      "id",
+      "in_progress_count",
+      "initial_count",
+      "is_completed",
+      "name",
+      "notification_mode",
+      "notification_result",
+      "notification_status",
+      "notification_uri",
+      "owner_id",
+      "success_count",
+      "total_count",
       "updated_time",
     };
 
     @Override
-    public APINodeList<AdAsyncRequest> parseResponse(String response) throws APIException {
-      return AdAsyncRequest.parseResponse(response, getContext(), this);
+    public AdAsyncRequestSet parseResponse(String response) throws APIException {
+      return AdAsyncRequestSet.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public APINodeList<AdAsyncRequest> execute() throws APIException {
+    public AdAsyncRequestSet execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<AdAsyncRequest> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public AdAsyncRequestSet execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetRequests(String nodeId, APIContext context) {
-      super(context, nodeId, "/requests", "GET", Arrays.asList(PARAMS));
+    public APIRequestGet(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetRequests setParam(String param, Object value) {
+    @Override
+    public APIRequestGet setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetRequests setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGet setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetRequests setStatuses (List<EnumStatuses> statuses) {
-      this.setParam("statuses", statuses);
-      return this;
-    }
-
-    public APIRequestGetRequests setStatuses (String statuses) {
-      this.setParam("statuses", statuses);
-      return this;
-    }
-
-    public APIRequestGetRequests requestAllFields () {
+    public APIRequestGet requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetRequests requestAllFields (boolean value) {
+    public APIRequestGet requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetRequests requestFields (List<String> fields) {
+    @Override
+    public APIRequestGet requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetRequests requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGet requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetRequests requestField (String field) {
+    @Override
+    public APIRequestGet requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetRequests requestField (String field, boolean value) {
+    @Override
+    public APIRequestGet requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetRequests requestIdField () {
+    public APIRequestGet requestCanceledCountField () {
+      return this.requestCanceledCountField(true);
+    }
+    public APIRequestGet requestCanceledCountField (boolean value) {
+      this.requestField("canceled_count", value);
+      return this;
+    }
+    public APIRequestGet requestCreatedTimeField () {
+      return this.requestCreatedTimeField(true);
+    }
+    public APIRequestGet requestCreatedTimeField (boolean value) {
+      this.requestField("created_time", value);
+      return this;
+    }
+    public APIRequestGet requestErrorCountField () {
+      return this.requestErrorCountField(true);
+    }
+    public APIRequestGet requestErrorCountField (boolean value) {
+      this.requestField("error_count", value);
+      return this;
+    }
+    public APIRequestGet requestIdField () {
       return this.requestIdField(true);
     }
-    public APIRequestGetRequests requestIdField (boolean value) {
+    public APIRequestGet requestIdField (boolean value) {
       this.requestField("id", value);
       return this;
     }
-    public APIRequestGetRequests requestScopeObjectIdField () {
-      return this.requestScopeObjectIdField(true);
+    public APIRequestGet requestInProgressCountField () {
+      return this.requestInProgressCountField(true);
     }
-    public APIRequestGetRequests requestScopeObjectIdField (boolean value) {
-      this.requestField("scope_object_id", value);
+    public APIRequestGet requestInProgressCountField (boolean value) {
+      this.requestField("in_progress_count", value);
       return this;
     }
-    public APIRequestGetRequests requestStatusField () {
-      return this.requestStatusField(true);
+    public APIRequestGet requestInitialCountField () {
+      return this.requestInitialCountField(true);
     }
-    public APIRequestGetRequests requestStatusField (boolean value) {
-      this.requestField("status", value);
+    public APIRequestGet requestInitialCountField (boolean value) {
+      this.requestField("initial_count", value);
       return this;
     }
-    public APIRequestGetRequests requestResultField () {
-      return this.requestResultField(true);
+    public APIRequestGet requestIsCompletedField () {
+      return this.requestIsCompletedField(true);
     }
-    public APIRequestGetRequests requestResultField (boolean value) {
-      this.requestField("result", value);
+    public APIRequestGet requestIsCompletedField (boolean value) {
+      this.requestField("is_completed", value);
       return this;
     }
-    public APIRequestGetRequests requestInputField () {
-      return this.requestInputField(true);
+    public APIRequestGet requestNameField () {
+      return this.requestNameField(true);
     }
-    public APIRequestGetRequests requestInputField (boolean value) {
-      this.requestField("input", value);
+    public APIRequestGet requestNameField (boolean value) {
+      this.requestField("name", value);
       return this;
     }
-    public APIRequestGetRequests requestAsyncRequestSetField () {
-      return this.requestAsyncRequestSetField(true);
+    public APIRequestGet requestNotificationModeField () {
+      return this.requestNotificationModeField(true);
     }
-    public APIRequestGetRequests requestAsyncRequestSetField (boolean value) {
-      this.requestField("async_request_set", value);
+    public APIRequestGet requestNotificationModeField (boolean value) {
+      this.requestField("notification_mode", value);
       return this;
     }
-    public APIRequestGetRequests requestCreatedTimeField () {
-      return this.requestCreatedTimeField(true);
+    public APIRequestGet requestNotificationResultField () {
+      return this.requestNotificationResultField(true);
     }
-    public APIRequestGetRequests requestCreatedTimeField (boolean value) {
-      this.requestField("created_time", value);
+    public APIRequestGet requestNotificationResultField (boolean value) {
+      this.requestField("notification_result", value);
       return this;
     }
-    public APIRequestGetRequests requestUpdatedTimeField () {
+    public APIRequestGet requestNotificationStatusField () {
+      return this.requestNotificationStatusField(true);
+    }
+    public APIRequestGet requestNotificationStatusField (boolean value) {
+      this.requestField("notification_status", value);
+      return this;
+    }
+    public APIRequestGet requestNotificationUriField () {
+      return this.requestNotificationUriField(true);
+    }
+    public APIRequestGet requestNotificationUriField (boolean value) {
+      this.requestField("notification_uri", value);
+      return this;
+    }
+    public APIRequestGet requestOwnerIdField () {
+      return this.requestOwnerIdField(true);
+    }
+    public APIRequestGet requestOwnerIdField (boolean value) {
+      this.requestField("owner_id", value);
+      return this;
+    }
+    public APIRequestGet requestSuccessCountField () {
+      return this.requestSuccessCountField(true);
+    }
+    public APIRequestGet requestSuccessCountField (boolean value) {
+      this.requestField("success_count", value);
+      return this;
+    }
+    public APIRequestGet requestTotalCountField () {
+      return this.requestTotalCountField(true);
+    }
+    public APIRequestGet requestTotalCountField (boolean value) {
+      this.requestField("total_count", value);
+      return this;
+    }
+    public APIRequestGet requestUpdatedTimeField () {
       return this.requestUpdatedTimeField(true);
     }
-    public APIRequestGetRequests requestUpdatedTimeField (boolean value) {
+    public APIRequestGet requestUpdatedTimeField (boolean value) {
       this.requestField("updated_time", value);
       return this;
     }
-
   }
 
-  public static enum EnumStatuses {
-    @SerializedName("INITIAL")
-    VALUE_INITIAL("INITIAL"),
-    @SerializedName("IN_PROGRESS")
-    VALUE_IN_PROGRESS("IN_PROGRESS"),
-    @SerializedName("SUCCESS")
-    VALUE_SUCCESS("SUCCESS"),
-    @SerializedName("ERROR")
-    VALUE_ERROR("ERROR"),
-    @SerializedName("CANCELED")
-    VALUE_CANCELED("CANCELED"),
-    @SerializedName("PENDING_DEPENDENCY")
-    VALUE_PENDING_DEPENDENCY("PENDING_DEPENDENCY"),
-    @SerializedName("CANCELED_DEPENDENCY")
-    VALUE_CANCELED_DEPENDENCY("CANCELED_DEPENDENCY"),
-    @SerializedName("ERROR_DEPENDENCY")
-    VALUE_ERROR_DEPENDENCY("ERROR_DEPENDENCY"),
-    @SerializedName("ERROR_CONFLICTS")
-    VALUE_ERROR_CONFLICTS("ERROR_CONFLICTS"),
-    NULL(null);
-
-    private String value;
-
-    private EnumStatuses(String value) {
-      this.value = value;
-    }
-
-    @Override
-    public String toString() {
-      return value;
-    }
-  }
   public static enum EnumNotificationMode {
-    @SerializedName("OFF")
-    VALUE_OFF("OFF"),
-    @SerializedName("ON_COMPLETE")
-    VALUE_ON_COMPLETE("ON_COMPLETE"),
-    NULL(null);
+      @SerializedName("OFF")
+      VALUE_OFF("OFF"),
+      @SerializedName("ON_COMPLETE")
+      VALUE_ON_COMPLETE("ON_COMPLETE"),
+      NULL(null);
 
-    private String value;
+      private String value;
 
-    private EnumNotificationMode(String value) {
-      this.value = value;
-    }
+      private EnumNotificationMode(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
-  public static enum EnumNotificationStatus {
-    @SerializedName("NOT_SENT")
-    VALUE_NOT_SENT("NOT_SENT"),
-    @SerializedName("SENDING")
-    VALUE_SENDING("SENDING"),
-    @SerializedName("SENT")
-    VALUE_SENT("SENT"),
-    NULL(null);
-
-    private String value;
 
-    private EnumNotificationStatus(String value) {
-      this.value = value;
-    }
+  public static enum EnumNotificationStatus {
+      @SerializedName("NOT_SENT")
+      VALUE_NOT_SENT("NOT_SENT"),
+      @SerializedName("SENDING")
+      VALUE_SENDING("SENDING"),
+      @SerializedName("SENT")
+      VALUE_SENT("SENT"),
+      NULL(null);
+
+      private String value;
+
+      private EnumNotificationStatus(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
 
+
   synchronized /*package*/ static Gson getGson() {
     if (gson != null) {
       return gson;
@@ -727,30 +763,30 @@ public class AdAsyncRequestSet extends APINode {
   }
 
   public AdAsyncRequestSet copyFrom(AdAsyncRequestSet instance) {
+    this.mCanceledCount = instance.mCanceledCount;
+    this.mCreatedTime = instance.mCreatedTime;
+    this.mErrorCount = instance.mErrorCount;
     this.mId = instance.mId;
-    this.mOwnerId = instance.mOwnerId;
+    this.mInProgressCount = instance.mInProgressCount;
+    this.mInitialCount = instance.mInitialCount;
+    this.mIsCompleted = instance.mIsCompleted;
     this.mName = instance.mName;
     this.mNotificationMode = instance.mNotificationMode;
+    this.mNotificationResult = instance.mNotificationResult;
+    this.mNotificationStatus = instance.mNotificationStatus;
     this.mNotificationUri = instance.mNotificationUri;
-    this.mTotalCount = instance.mTotalCount;
-    this.mInitialCount = instance.mInitialCount;
-    this.mInProgressCount = instance.mInProgressCount;
+    this.mOwnerId = instance.mOwnerId;
     this.mSuccessCount = instance.mSuccessCount;
-    this.mErrorCount = instance.mErrorCount;
-    this.mCanceledCount = instance.mCanceledCount;
-    this.mIsCompleted = instance.mIsCompleted;
-    this.mCreatedTime = instance.mCreatedTime;
+    this.mTotalCount = instance.mTotalCount;
     this.mUpdatedTime = instance.mUpdatedTime;
-    this.mNotificationStatus = instance.mNotificationStatus;
-    this.mNotificationResult = instance.mNotificationResult;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AdAsyncRequestSet> getParser() {
     return new APIRequest.ResponseParser<AdAsyncRequestSet>() {
-      public APINodeList<AdAsyncRequestSet> parseResponse(String response, APIContext context, APIRequest<AdAsyncRequestSet> request) {
+      public APINodeList<AdAsyncRequestSet> parseResponse(String response, APIContext context, APIRequest<AdAsyncRequestSet> request) throws MalformedResponseException {
         return AdAsyncRequestSet.parseResponse(response, context, request);
       }
     };
