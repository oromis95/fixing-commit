@@ -24,30 +24,35 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class TargetingGeoLocationZip extends APINode {
+  @SerializedName("country")
+  private String mCountry = null;
   @SerializedName("key")
   private String mKey = null;
   @SerializedName("name")
@@ -56,8 +61,6 @@ public class TargetingGeoLocationZip extends APINode {
   private Long mPrimaryCityId = null;
   @SerializedName("region_id")
   private Long mRegionId = null;
-  @SerializedName("country")
-  private String mCountry = null;
   protected static Gson gson = null;
 
   public TargetingGeoLocationZip() {
@@ -75,22 +78,23 @@ public class TargetingGeoLocationZip extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    targetingGeoLocationZip.mContext = context;
+    targetingGeoLocationZip.context = context;
     targetingGeoLocationZip.rawValue = json;
     return targetingGeoLocationZip;
   }
 
-  public static APINodeList<TargetingGeoLocationZip> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<TargetingGeoLocationZip> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<TargetingGeoLocationZip> targetingGeoLocationZips = new APINodeList<TargetingGeoLocationZip>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -103,10 +107,11 @@ public class TargetingGeoLocationZip extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            targetingGeoLocationZips.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            targetingGeoLocationZips.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -117,7 +122,20 @@ public class TargetingGeoLocationZip extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            targetingGeoLocationZips.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  targetingGeoLocationZips.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              targetingGeoLocationZips.add(loadJSON(obj.toString(), context));
+            }
           }
           return targetingGeoLocationZips;
         } else if (obj.has("images")) {
@@ -128,24 +146,54 @@ public class TargetingGeoLocationZip extends APINode {
           }
           return targetingGeoLocationZips;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              targetingGeoLocationZips.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return targetingGeoLocationZips;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          targetingGeoLocationZips.clear();
           targetingGeoLocationZips.add(loadJSON(json, context));
           return targetingGeoLocationZips;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -154,6 +202,15 @@ public class TargetingGeoLocationZip extends APINode {
   }
 
 
+  public String getFieldCountry() {
+    return mCountry;
+  }
+
+  public TargetingGeoLocationZip setFieldCountry(String value) {
+    this.mCountry = value;
+    return this;
+  }
+
   public String getFieldKey() {
     return mKey;
   }
@@ -190,15 +247,6 @@ public class TargetingGeoLocationZip extends APINode {
     return this;
   }
 
-  public String getFieldCountry() {
-    return mCountry;
-  }
-
-  public TargetingGeoLocationZip setFieldCountry(String value) {
-    this.mCountry = value;
-    return this;
-  }
-
 
 
 
@@ -216,19 +264,19 @@ public class TargetingGeoLocationZip extends APINode {
   }
 
   public TargetingGeoLocationZip copyFrom(TargetingGeoLocationZip instance) {
+    this.mCountry = instance.mCountry;
     this.mKey = instance.mKey;
     this.mName = instance.mName;
     this.mPrimaryCityId = instance.mPrimaryCityId;
     this.mRegionId = instance.mRegionId;
-    this.mCountry = instance.mCountry;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<TargetingGeoLocationZip> getParser() {
     return new APIRequest.ResponseParser<TargetingGeoLocationZip>() {
-      public APINodeList<TargetingGeoLocationZip> parseResponse(String response, APIContext context, APIRequest<TargetingGeoLocationZip> request) {
+      public APINodeList<TargetingGeoLocationZip> parseResponse(String response, APIContext context, APIRequest<TargetingGeoLocationZip> request) throws MalformedResponseException {
         return TargetingGeoLocationZip.parseResponse(response, context, request);
       }
     };
