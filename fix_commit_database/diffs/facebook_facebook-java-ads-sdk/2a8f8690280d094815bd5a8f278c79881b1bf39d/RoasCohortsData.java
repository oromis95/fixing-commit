@@ -24,40 +24,43 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class RoasCohortsData extends APINode {
-  @SerializedName("cohorts_start")
-  private String mCohortsStart = null;
   @SerializedName("cohorts_end")
   private String mCohortsEnd = null;
+  @SerializedName("cohorts_start")
+  private String mCohortsStart = null;
   @SerializedName("installs")
   private Long mInstalls = null;
-  @SerializedName("spend")
-  private Double mSpend = null;
   @SerializedName("revenue_cohorts")
   private List<RoasCohortsPerCohortIntervalUnit> mRevenueCohorts = null;
+  @SerializedName("spend")
+  private Double mSpend = null;
   protected static Gson gson = null;
 
   public RoasCohortsData() {
@@ -75,22 +78,23 @@ public class RoasCohortsData extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    roasCohortsData.mContext = context;
+    roasCohortsData.context = context;
     roasCohortsData.rawValue = json;
     return roasCohortsData;
   }
 
-  public static APINodeList<RoasCohortsData> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<RoasCohortsData> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<RoasCohortsData> roasCohortsDatas = new APINodeList<RoasCohortsData>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -103,10 +107,11 @@ public class RoasCohortsData extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            roasCohortsDatas.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            roasCohortsDatas.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -117,7 +122,20 @@ public class RoasCohortsData extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            roasCohortsDatas.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  roasCohortsDatas.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              roasCohortsDatas.add(loadJSON(obj.toString(), context));
+            }
           }
           return roasCohortsDatas;
         } else if (obj.has("images")) {
@@ -128,24 +146,54 @@ public class RoasCohortsData extends APINode {
           }
           return roasCohortsDatas;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              roasCohortsDatas.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return roasCohortsDatas;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          roasCohortsDatas.clear();
           roasCohortsDatas.add(loadJSON(json, context));
           return roasCohortsDatas;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -154,15 +202,6 @@ public class RoasCohortsData extends APINode {
   }
 
 
-  public String getFieldCohortsStart() {
-    return mCohortsStart;
-  }
-
-  public RoasCohortsData setFieldCohortsStart(String value) {
-    this.mCohortsStart = value;
-    return this;
-  }
-
   public String getFieldCohortsEnd() {
     return mCohortsEnd;
   }
@@ -172,21 +211,21 @@ public class RoasCohortsData extends APINode {
     return this;
   }
 
-  public Long getFieldInstalls() {
-    return mInstalls;
+  public String getFieldCohortsStart() {
+    return mCohortsStart;
   }
 
-  public RoasCohortsData setFieldInstalls(Long value) {
-    this.mInstalls = value;
+  public RoasCohortsData setFieldCohortsStart(String value) {
+    this.mCohortsStart = value;
     return this;
   }
 
-  public Double getFieldSpend() {
-    return mSpend;
+  public Long getFieldInstalls() {
+    return mInstalls;
   }
 
-  public RoasCohortsData setFieldSpend(Double value) {
-    this.mSpend = value;
+  public RoasCohortsData setFieldInstalls(Long value) {
+    this.mInstalls = value;
     return this;
   }
 
@@ -204,6 +243,15 @@ public class RoasCohortsData extends APINode {
     this.mRevenueCohorts = RoasCohortsPerCohortIntervalUnit.getGson().fromJson(value, type);
     return this;
   }
+  public Double getFieldSpend() {
+    return mSpend;
+  }
+
+  public RoasCohortsData setFieldSpend(Double value) {
+    this.mSpend = value;
+    return this;
+  }
+
 
 
 
@@ -221,19 +269,19 @@ public class RoasCohortsData extends APINode {
   }
 
   public RoasCohortsData copyFrom(RoasCohortsData instance) {
-    this.mCohortsStart = instance.mCohortsStart;
     this.mCohortsEnd = instance.mCohortsEnd;
+    this.mCohortsStart = instance.mCohortsStart;
     this.mInstalls = instance.mInstalls;
-    this.mSpend = instance.mSpend;
     this.mRevenueCohorts = instance.mRevenueCohorts;
-    this.mContext = instance.mContext;
+    this.mSpend = instance.mSpend;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<RoasCohortsData> getParser() {
     return new APIRequest.ResponseParser<RoasCohortsData>() {
-      public APINodeList<RoasCohortsData> parseResponse(String response, APIContext context, APIRequest<RoasCohortsData> request) {
+      public APINodeList<RoasCohortsData> parseResponse(String response, APIContext context, APIRequest<RoasCohortsData> request) throws MalformedResponseException {
         return RoasCohortsData.parseResponse(response, context, request);
       }
     };
