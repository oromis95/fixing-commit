@@ -22,6 +22,8 @@
  */
 package com.facebook.ads.sdk;
 
+import com.google.gson.JsonObject;
+import com.google.gson.JsonParser;
 import java.util.List;
 import java.util.ArrayList;
 import java.util.Map;
@@ -60,6 +62,12 @@ public class APINodeList<T extends APINode> extends ArrayList<T> implements APIR
       return rawValue;
     }
 
+    @Override
+    public JsonObject getRawResponseAsJsonObject() {
+      JsonParser parser = new JsonParser();
+      return parser.parse(rawValue).getAsJsonObject();
+    }
+
     @Override
     public T head() {
       return this.size() > 0 ? this.get(0) : null;
