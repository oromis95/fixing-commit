@@ -24,38 +24,41 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class Business extends APINode {
   @SerializedName("id")
   private String mId = null;
   @SerializedName("name")
   private String mName = null;
-  @SerializedName("primary_page")
-  private Object mPrimaryPage = null;
   @SerializedName("payment_account_id")
   private String mPaymentAccountId = null;
+  @SerializedName("primary_page")
+  private Object mPrimaryPage = null;
   protected static Gson gson = null;
 
   Business() {
@@ -67,11 +70,11 @@ public class Business extends APINode {
 
   public Business(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public Business fetch() throws APIException{
-    Business newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    Business newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -88,8 +91,17 @@ public class Business extends APINode {
     return business;
   }
 
+  public static APINodeList<Business> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<Business>)(
+      new APIRequest<Business>(context, "", "/", "GET", Business.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -104,22 +116,23 @@ public class Business extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    business.mContext = context;
+    business.context = context;
     business.rawValue = json;
     return business;
   }
 
-  public static APINodeList<Business> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<Business> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<Business> businesss = new APINodeList<Business>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -132,10 +145,11 @@ public class Business extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            businesss.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            businesss.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -146,7 +160,20 @@ public class Business extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            businesss.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  businesss.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              businesss.add(loadJSON(obj.toString(), context));
+            }
           }
           return businesss;
         } else if (obj.has("images")) {
@@ -157,24 +184,54 @@ public class Business extends APINode {
           }
           return businesss;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              businesss.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return businesss;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          businesss.clear();
           businesss.add(loadJSON(json, context));
           return businesss;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -182,104 +239,128 @@ public class Business extends APINode {
     return getGson().toJson(this);
   }
 
-  public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
+  public APIRequestCreateAdAccount createAdAccount() {
+    return new APIRequestCreateAdAccount(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetSystemUsers getSystemUsers() {
-    return new APIRequestGetSystemUsers(this.getPrefixedId().toString(), mContext);
+  public APIRequestCreateAdAccounts createAdAccounts() {
+    return new APIRequestCreateAdAccounts(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetProductCatalogs getProductCatalogs() {
-    return new APIRequestGetProductCatalogs(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetAdsPixels getAdsPixels() {
+    return new APIRequestGetAdsPixels(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetAdsPixels getAdsPixels() {
-    return new APIRequestGetAdsPixels(this.getPrefixedId().toString(), mContext);
+  public APIRequestDeleteApps deleteApps() {
+    return new APIRequestDeleteApps(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetAssignedAdAccounts getAssignedAdAccounts() {
-    return new APIRequestGetAssignedAdAccounts(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetAssignedAdAccounts(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetAssignedPages getAssignedPages() {
-    return new APIRequestGetAssignedPages(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetAssignedPages(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetAssignedProductCatalogs getAssignedProductCatalogs() {
-    return new APIRequestGetAssignedProductCatalogs(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetAssignedProductCatalogs(this.getPrefixedId().toString(), context);
+  }
+
+  public APIRequestGetClientAdAccountRequests getClientAdAccountRequests() {
+    return new APIRequestGetClientAdAccountRequests(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetClientAdAccounts getClientAdAccounts() {
-    return new APIRequestGetClientAdAccounts(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetClientAdAccounts(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetClientAdAccountRequests getClientAdAccountRequests() {
-    return new APIRequestGetClientAdAccountRequests(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetClientPageRequests getClientPageRequests() {
+    return new APIRequestGetClientPageRequests(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetClientPages getClientPages() {
-    return new APIRequestGetClientPages(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetClientPages(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetClientPageRequests getClientPageRequests() {
-    return new APIRequestGetClientPageRequests(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetGrpPlans getGrpPlans() {
+    return new APIRequestGetGrpPlans(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetOwnedAdAccounts getOwnedAdAccounts() {
-    return new APIRequestGetOwnedAdAccounts(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetInstagramAccounts getInstagramAccounts() {
+    return new APIRequestGetInstagramAccounts(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetOwnedAdAccountRequests getOwnedAdAccountRequests() {
-    return new APIRequestGetOwnedAdAccountRequests(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetMeasurementReports getMeasurementReports() {
+    return new APIRequestGetMeasurementReports(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetOwnedPages getOwnedPages() {
-    return new APIRequestGetOwnedPages(this.getPrefixedId().toString(), mContext);
+  public APIRequestCreateMeasurementReport createMeasurementReport() {
+    return new APIRequestCreateMeasurementReport(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetOwnedPageRequests getOwnedPageRequests() {
-    return new APIRequestGetOwnedPageRequests(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetOfflineConversionDataSets getOfflineConversionDataSets() {
+    return new APIRequestGetOfflineConversionDataSets(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetOwnedPixels getOwnedPixels() {
-    return new APIRequestGetOwnedPixels(this.getPrefixedId().toString(), mContext);
+  public APIRequestCreateOfflineConversionDataSet createOfflineConversionDataSet() {
+    return new APIRequestCreateOfflineConversionDataSet(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetPicture getPicture() {
-    return new APIRequestGetPicture(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetOwnedAdAccountRequests getOwnedAdAccountRequests() {
+    return new APIRequestGetOwnedAdAccountRequests(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetInstagramAccounts getInstagramAccounts() {
-    return new APIRequestGetInstagramAccounts(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetOwnedAdAccounts getOwnedAdAccounts() {
+    return new APIRequestGetOwnedAdAccounts(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetOwnedInstagramAccounts getOwnedInstagramAccounts() {
-    return new APIRequestGetOwnedInstagramAccounts(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetOwnedInstagramAccounts(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetGrpPlans getGrpPlans() {
-    return new APIRequestGetGrpPlans(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetOwnedPageRequests getOwnedPageRequests() {
+    return new APIRequestGetOwnedPageRequests(this.getPrefixedId().toString(), context);
+  }
+
+  public APIRequestGetOwnedPages getOwnedPages() {
+    return new APIRequestGetOwnedPages(this.getPrefixedId().toString(), context);
+  }
+
+  public APIRequestGetOwnedPixels getOwnedPixels() {
+    return new APIRequestGetOwnedPixels(this.getPrefixedId().toString(), context);
+  }
+
+  public APIRequestGetPicture getPicture() {
+    return new APIRequestGetPicture(this.getPrefixedId().toString(), context);
+  }
+
+  public APIRequestGetProductCatalogs getProductCatalogs() {
+    return new APIRequestGetProductCatalogs(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestCreateProductCatalog createProductCatalog() {
-    return new APIRequestCreateProductCatalog(this.getPrefixedId().toString(), mContext);
+    return new APIRequestCreateProductCatalog(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestCreateAdAccount createAdAccount() {
-    return new APIRequestCreateAdAccount(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetReceivedAudiencePermissions getReceivedAudiencePermissions() {
+    return new APIRequestGetReceivedAudiencePermissions(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestDeleteApps deleteApps() {
-    return new APIRequestDeleteApps(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetSharedAudiencePermissions getSharedAudiencePermissions() {
+    return new APIRequestGetSharedAudiencePermissions(this.getPrefixedId().toString(), context);
+  }
+
+  public APIRequestGetSystemUsers getSystemUsers() {
+    return new APIRequestGetSystemUsers(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestCreateUserPermission createUserPermission() {
-    return new APIRequestCreateUserPermission(this.getPrefixedId().toString(), mContext);
+    return new APIRequestCreateUserPermission(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestCreateAdAccounts createAdAccounts() {
-    return new APIRequestCreateAdAccounts(this.getPrefixedId().toString(), mContext);
+  public APIRequestGet get() {
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
   }
 
 
@@ -291,315 +372,296 @@ public class Business extends APINode {
     return mName;
   }
 
-  public Object getFieldPrimaryPage() {
-    return mPrimaryPage;
-  }
-
   public String getFieldPaymentAccountId() {
     return mPaymentAccountId;
   }
 
+  public Object getFieldPrimaryPage() {
+    return mPrimaryPage;
+  }
+
 
 
-  public static class APIRequestGet extends APIRequest<Business> {
+  public static class APIRequestCreateAdAccount extends APIRequest<APINode> {
 
-    Business lastResponse = null;
+    APINode lastResponse = null;
     @Override
-    public Business getLastResponse() {
+    public APINode getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
+      "currency",
+      "end_advertiser",
+      "funding_id",
+      "id",
+      "invoice",
+      "io",
+      "media_agency",
+      "name",
+      "partner",
+      "po_number",
+      "timezone_id",
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "name",
-      "primary_page",
     };
 
     @Override
-    public Business parseResponse(String response) throws APIException {
-      return Business.parseResponse(response, getContext(), this).head();
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public Business execute() throws APIException {
+    public APINode execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public Business execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGet(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
+    public APIRequestCreateAdAccount(String nodeId, APIContext context) {
+      super(context, nodeId, "/adaccount", "POST", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGet setParam(String param, Object value) {
+    @Override
+    public APIRequestCreateAdAccount setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGet setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestCreateAdAccount setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGet requestAllFields () {
-      return this.requestAllFields(true);
-    }
-
-    public APIRequestGet requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
+    public APIRequestCreateAdAccount setCurrency (String currency) {
+      this.setParam("currency", currency);
       return this;
     }
 
-    public APIRequestGet requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
+    public APIRequestCreateAdAccount setEndAdvertiser (Object endAdvertiser) {
+      this.setParam("end_advertiser", endAdvertiser);
+      return this;
     }
-
-    public APIRequestGet requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+    public APIRequestCreateAdAccount setEndAdvertiser (String endAdvertiser) {
+      this.setParam("end_advertiser", endAdvertiser);
       return this;
     }
 
-    public APIRequestGet requestField (String field) {
-      this.requestField(field, true);
+    public APIRequestCreateAdAccount setFundingId (String fundingId) {
+      this.setParam("funding_id", fundingId);
       return this;
     }
 
-    public APIRequestGet requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
+    public APIRequestCreateAdAccount setId (String id) {
+      this.setParam("id", id);
       return this;
     }
 
-    public APIRequestGet requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGet requestIdField (boolean value) {
-      this.requestField("id", value);
+    public APIRequestCreateAdAccount setInvoice (Boolean invoice) {
+      this.setParam("invoice", invoice);
       return this;
     }
-    public APIRequestGet requestNameField () {
-      return this.requestNameField(true);
-    }
-    public APIRequestGet requestNameField (boolean value) {
-      this.requestField("name", value);
+    public APIRequestCreateAdAccount setInvoice (String invoice) {
+      this.setParam("invoice", invoice);
       return this;
     }
-    public APIRequestGet requestPrimaryPageField () {
-      return this.requestPrimaryPageField(true);
-    }
-    public APIRequestGet requestPrimaryPageField (boolean value) {
-      this.requestField("primary_page", value);
+
+    public APIRequestCreateAdAccount setIo (Boolean io) {
+      this.setParam("io", io);
       return this;
     }
-
-  }
-
-  public static class APIRequestGetSystemUsers extends APIRequest<APINode> {
-
-    APINodeList<APINode> lastResponse = null;
-    @Override
-    public APINodeList<APINode> getLastResponse() {
-      return lastResponse;
+    public APIRequestCreateAdAccount setIo (String io) {
+      this.setParam("io", io);
+      return this;
     }
-    public static final String[] PARAMS = {
-    };
-
-    public static final String[] FIELDS = {
-    };
 
-    @Override
-    public APINodeList<APINode> parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this);
+    public APIRequestCreateAdAccount setMediaAgency (String mediaAgency) {
+      this.setParam("media_agency", mediaAgency);
+      return this;
     }
 
-    @Override
-    public APINodeList<APINode> execute() throws APIException {
-      return execute(new HashMap<String, Object>());
+    public APIRequestCreateAdAccount setName (String name) {
+      this.setParam("name", name);
+      return this;
     }
 
-    @Override
-    public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
+    public APIRequestCreateAdAccount setPartner (String partner) {
+      this.setParam("partner", partner);
+      return this;
     }
 
-    public APIRequestGetSystemUsers(String nodeId, APIContext context) {
-      super(context, nodeId, "/system_users", "GET", Arrays.asList(PARAMS));
+    public APIRequestCreateAdAccount setPoNumber (String poNumber) {
+      this.setParam("po_number", poNumber);
+      return this;
     }
 
-    public APIRequestGetSystemUsers setParam(String param, Object value) {
-      setParamInternal(param, value);
+    public APIRequestCreateAdAccount setTimezoneId (Long timezoneId) {
+      this.setParam("timezone_id", timezoneId);
       return this;
     }
-
-    public APIRequestGetSystemUsers setParams(Map<String, Object> params) {
-      setParamsInternal(params);
+    public APIRequestCreateAdAccount setTimezoneId (String timezoneId) {
+      this.setParam("timezone_id", timezoneId);
       return this;
     }
 
-
-    public APIRequestGetSystemUsers requestAllFields () {
+    public APIRequestCreateAdAccount requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetSystemUsers requestAllFields (boolean value) {
+    public APIRequestCreateAdAccount requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetSystemUsers requestFields (List<String> fields) {
+    @Override
+    public APIRequestCreateAdAccount requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetSystemUsers requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestCreateAdAccount requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetSystemUsers requestField (String field) {
+    @Override
+    public APIRequestCreateAdAccount requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetSystemUsers requestField (String field, boolean value) {
+    @Override
+    public APIRequestCreateAdAccount requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
   }
 
-  public static class APIRequestGetProductCatalogs extends APIRequest<ProductCatalog> {
+  public static class APIRequestCreateAdAccounts extends APIRequest<APINode> {
 
-    APINodeList<ProductCatalog> lastResponse = null;
+    APINode lastResponse = null;
     @Override
-    public APINodeList<ProductCatalog> getLastResponse() {
+    public APINode getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
+      "access_type",
+      "adaccount_id",
+      "id",
+      "permitted_roles",
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "business",
-      "feed_count",
-      "name",
-      "product_count",
     };
 
     @Override
-    public APINodeList<ProductCatalog> parseResponse(String response) throws APIException {
-      return ProductCatalog.parseResponse(response, getContext(), this);
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public APINodeList<ProductCatalog> execute() throws APIException {
+    public APINode execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<ProductCatalog> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetProductCatalogs(String nodeId, APIContext context) {
-      super(context, nodeId, "/product_catalogs", "GET", Arrays.asList(PARAMS));
+    public APIRequestCreateAdAccounts(String nodeId, APIContext context) {
+      super(context, nodeId, "/adaccounts", "POST", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetProductCatalogs setParam(String param, Object value) {
+    @Override
+    public APIRequestCreateAdAccounts setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetProductCatalogs setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestCreateAdAccounts setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetProductCatalogs requestAllFields () {
+    public APIRequestCreateAdAccounts setAccessType (AdAccount.EnumAccessType accessType) {
+      this.setParam("access_type", accessType);
+      return this;
+    }
+    public APIRequestCreateAdAccounts setAccessType (String accessType) {
+      this.setParam("access_type", accessType);
+      return this;
+    }
+
+    public APIRequestCreateAdAccounts setAdaccountId (String adaccountId) {
+      this.setParam("adaccount_id", adaccountId);
+      return this;
+    }
+
+    public APIRequestCreateAdAccounts setId (String id) {
+      this.setParam("id", id);
+      return this;
+    }
+
+    public APIRequestCreateAdAccounts setPermittedRoles (List<AdAccount.EnumPermittedRoles> permittedRoles) {
+      this.setParam("permitted_roles", permittedRoles);
+      return this;
+    }
+    public APIRequestCreateAdAccounts setPermittedRoles (String permittedRoles) {
+      this.setParam("permitted_roles", permittedRoles);
+      return this;
+    }
+
+    public APIRequestCreateAdAccounts requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetProductCatalogs requestAllFields (boolean value) {
+    public APIRequestCreateAdAccounts requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetProductCatalogs requestFields (List<String> fields) {
+    @Override
+    public APIRequestCreateAdAccounts requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetProductCatalogs requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestCreateAdAccounts requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetProductCatalogs requestField (String field) {
+    @Override
+    public APIRequestCreateAdAccounts requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetProductCatalogs requestField (String field, boolean value) {
+    @Override
+    public APIRequestCreateAdAccounts requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetProductCatalogs requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGetProductCatalogs requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
-    }
-    public APIRequestGetProductCatalogs requestBusinessField () {
-      return this.requestBusinessField(true);
-    }
-    public APIRequestGetProductCatalogs requestBusinessField (boolean value) {
-      this.requestField("business", value);
-      return this;
-    }
-    public APIRequestGetProductCatalogs requestFeedCountField () {
-      return this.requestFeedCountField(true);
-    }
-    public APIRequestGetProductCatalogs requestFeedCountField (boolean value) {
-      this.requestField("feed_count", value);
-      return this;
-    }
-    public APIRequestGetProductCatalogs requestNameField () {
-      return this.requestNameField(true);
-    }
-    public APIRequestGetProductCatalogs requestNameField (boolean value) {
-      this.requestField("name", value);
-      return this;
-    }
-    public APIRequestGetProductCatalogs requestProductCountField () {
-      return this.requestProductCountField(true);
-    }
-    public APIRequestGetProductCatalogs requestProductCountField (boolean value) {
-      this.requestField("product_count", value);
-      return this;
-    }
-
   }
 
   public static class APIRequestGetAdsPixels extends APIRequest<AdsPixel> {
@@ -613,13 +675,13 @@ public class Business extends APINode {
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "owner_business",
-      "owner_ad_account",
-      "name",
+      "code",
       "creation_time",
+      "id",
       "last_fired_time",
-      "code",
+      "name",
+      "owner_ad_account",
+      "owner_business",
     };
 
     @Override
@@ -634,7 +696,7 @@ public class Business extends APINode {
 
     @Override
     public APINodeList<AdsPixel> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -642,11 +704,13 @@ public class Business extends APINode {
       super(context, nodeId, "/adspixels", "GET", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestGetAdsPixels setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestGetAdsPixels setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
@@ -664,10 +728,12 @@ public class Business extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetAdsPixels requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestGetAdsPixels requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -675,16 +741,32 @@ public class Business extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetAdsPixels requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGetAdsPixels requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
+    public APIRequestGetAdsPixels requestCodeField () {
+      return this.requestCodeField(true);
+    }
+    public APIRequestGetAdsPixels requestCodeField (boolean value) {
+      this.requestField("code", value);
+      return this;
+    }
+    public APIRequestGetAdsPixels requestCreationTimeField () {
+      return this.requestCreationTimeField(true);
+    }
+    public APIRequestGetAdsPixels requestCreationTimeField (boolean value) {
+      this.requestField("creation_time", value);
+      return this;
+    }
     public APIRequestGetAdsPixels requestIdField () {
       return this.requestIdField(true);
     }
@@ -692,11 +774,18 @@ public class Business extends APINode {
       this.requestField("id", value);
       return this;
     }
-    public APIRequestGetAdsPixels requestOwnerBusinessField () {
-      return this.requestOwnerBusinessField(true);
+    public APIRequestGetAdsPixels requestLastFiredTimeField () {
+      return this.requestLastFiredTimeField(true);
     }
-    public APIRequestGetAdsPixels requestOwnerBusinessField (boolean value) {
-      this.requestField("owner_business", value);
+    public APIRequestGetAdsPixels requestLastFiredTimeField (boolean value) {
+      this.requestField("last_fired_time", value);
+      return this;
+    }
+    public APIRequestGetAdsPixels requestNameField () {
+      return this.requestNameField(true);
+    }
+    public APIRequestGetAdsPixels requestNameField (boolean value) {
+      this.requestField("name", value);
       return this;
     }
     public APIRequestGetAdsPixels requestOwnerAdAccountField () {
@@ -706,32 +795,110 @@ public class Business extends APINode {
       this.requestField("owner_ad_account", value);
       return this;
     }
-    public APIRequestGetAdsPixels requestNameField () {
-      return this.requestNameField(true);
+    public APIRequestGetAdsPixels requestOwnerBusinessField () {
+      return this.requestOwnerBusinessField(true);
     }
-    public APIRequestGetAdsPixels requestNameField (boolean value) {
-      this.requestField("name", value);
+    public APIRequestGetAdsPixels requestOwnerBusinessField (boolean value) {
+      this.requestField("owner_business", value);
       return this;
     }
-    public APIRequestGetAdsPixels requestCreationTimeField () {
-      return this.requestCreationTimeField(true);
+  }
+
+  public static class APIRequestDeleteApps extends APIRequest<APINode> {
+
+    APINodeList<APINode> lastResponse = null;
+    @Override
+    public APINodeList<APINode> getLastResponse() {
+      return lastResponse;
     }
-    public APIRequestGetAdsPixels requestCreationTimeField (boolean value) {
-      this.requestField("creation_time", value);
+    public static final String[] PARAMS = {
+      "app_id",
+      "id",
+    };
+
+    public static final String[] FIELDS = {
+    };
+
+    @Override
+    public APINodeList<APINode> parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this);
+    }
+
+    @Override
+    public APINodeList<APINode> execute() throws APIException {
+      return execute(new HashMap<String, Object>());
+    }
+
+    @Override
+    public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
+    }
+
+    public APIRequestDeleteApps(String nodeId, APIContext context) {
+      super(context, nodeId, "/apps", "DELETE", Arrays.asList(PARAMS));
+    }
+
+    @Override
+    public APIRequestDeleteApps setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
-    public APIRequestGetAdsPixels requestLastFiredTimeField () {
-      return this.requestLastFiredTimeField(true);
+
+    @Override
+    public APIRequestDeleteApps setParams(Map<String, Object> params) {
+      setParamsInternal(params);
+      return this;
     }
-    public APIRequestGetAdsPixels requestLastFiredTimeField (boolean value) {
-      this.requestField("last_fired_time", value);
+
+
+    public APIRequestDeleteApps setAppId (Long appId) {
+      this.setParam("app_id", appId);
       return this;
     }
-    public APIRequestGetAdsPixels requestCodeField () {
-      return this.requestCodeField(true);
+    public APIRequestDeleteApps setAppId (String appId) {
+      this.setParam("app_id", appId);
+      return this;
     }
-    public APIRequestGetAdsPixels requestCodeField (boolean value) {
-      this.requestField("code", value);
+
+    public APIRequestDeleteApps setId (String id) {
+      this.setParam("id", id);
+      return this;
+    }
+
+    public APIRequestDeleteApps requestAllFields () {
+      return this.requestAllFields(true);
+    }
+
+    public APIRequestDeleteApps requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestDeleteApps requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
+    }
+
+    @Override
+    public APIRequestDeleteApps requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestDeleteApps requestField (String field) {
+      this.requestField(field, true);
+      return this;
+    }
+
+    @Override
+    public APIRequestDeleteApps requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
 
@@ -745,17 +912,19 @@ public class Business extends APINode {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "user_id",
       "email",
+      "user_id",
     };
 
     public static final String[] FIELDS = {
-      "id",
       "account_groups",
       "account_id",
       "account_status",
       "age",
       "agency_client_declaration",
+      "amount_spent",
+      "balance",
+      "business",
       "business_city",
       "business_country_code",
       "business_name",
@@ -773,36 +942,32 @@ public class Business extends APINode {
       "funding_source",
       "funding_source_details",
       "has_migrated_permissions",
+      "id",
       "io_number",
       "is_notifications_enabled",
       "is_personal",
       "is_prepay_account",
       "is_tax_id_required",
+      "last_used_time",
       "line_numbers",
       "media_agency",
       "min_campaign_group_spend_cap",
       "min_daily_budget",
       "name",
-      "owner",
       "offsite_pixels_tos_accepted",
+      "owner",
+      "owner_business",
       "partner",
+      "rf_spec",
+      "spend_cap",
       "tax_id",
       "tax_id_status",
       "tax_id_type",
       "timezone_id",
       "timezone_name",
       "timezone_offset_hours_utc",
-      "rf_spec",
       "tos_accepted",
       "user_role",
-      "vertical_name",
-      "amount_spent",
-      "spend_cap",
-      "balance",
-      "business",
-      "owner_business",
-      "last_used_time",
-      "asset_score",
     };
 
     @Override
@@ -817,7 +982,7 @@ public class Business extends APINode {
 
     @Override
     public APINodeList<AdAccount> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -825,33 +990,33 @@ public class Business extends APINode {
       super(context, nodeId, "/assigned_ad_accounts", "GET", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestGetAssignedAdAccounts setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestGetAssignedAdAccounts setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetAssignedAdAccounts setUserId (Long userId) {
-      this.setParam("user_id", userId);
+    public APIRequestGetAssignedAdAccounts setEmail (String email) {
+      this.setParam("email", email);
       return this;
     }
 
-    public APIRequestGetAssignedAdAccounts setUserId (String userId) {
+    public APIRequestGetAssignedAdAccounts setUserId (Long userId) {
       this.setParam("user_id", userId);
       return this;
     }
-
-    public APIRequestGetAssignedAdAccounts setEmail (String email) {
-      this.setParam("email", email);
+    public APIRequestGetAssignedAdAccounts setUserId (String userId) {
+      this.setParam("user_id", userId);
       return this;
     }
 
-
     public APIRequestGetAssignedAdAccounts requestAllFields () {
       return this.requestAllFields(true);
     }
@@ -863,10 +1028,12 @@ public class Business extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetAssignedAdAccounts requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestGetAssignedAdAccounts requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -874,23 +1041,18 @@ public class Business extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetAssignedAdAccounts requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGetAssignedAdAccounts requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetAssignedAdAccounts requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGetAssignedAdAccounts requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
-    }
     public APIRequestGetAssignedAdAccounts requestAccountGroupsField () {
       return this.requestAccountGroupsField(true);
     }
@@ -926,6 +1088,27 @@ public class Business extends APINode {
       this.requestField("agency_client_declaration", value);
       return this;
     }
+    public APIRequestGetAssignedAdAccounts requestAmountSpentField () {
+      return this.requestAmountSpentField(true);
+    }
+    public APIRequestGetAssignedAdAccounts requestAmountSpentField (boolean value) {
+      this.requestField("amount_spent", value);
+      return this;
+    }
+    public APIRequestGetAssignedAdAccounts requestBalanceField () {
+      return this.requestBalanceField(true);
+    }
+    public APIRequestGetAssignedAdAccounts requestBalanceField (boolean value) {
+      this.requestField("balance", value);
+      return this;
+    }
+    public APIRequestGetAssignedAdAccounts requestBusinessField () {
+      return this.requestBusinessField(true);
+    }
+    public APIRequestGetAssignedAdAccounts requestBusinessField (boolean value) {
+      this.requestField("business", value);
+      return this;
+    }
     public APIRequestGetAssignedAdAccounts requestBusinessCityField () {
       return this.requestBusinessCityField(true);
     }
@@ -1045,6 +1228,13 @@ public class Business extends APINode {
       this.requestField("has_migrated_permissions", value);
       return this;
     }
+    public APIRequestGetAssignedAdAccounts requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGetAssignedAdAccounts requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
     public APIRequestGetAssignedAdAccounts requestIoNumberField () {
       return this.requestIoNumberField(true);
     }
@@ -1080,6 +1270,13 @@ public class Business extends APINode {
       this.requestField("is_tax_id_required", value);
       return this;
     }
+    public APIRequestGetAssignedAdAccounts requestLastUsedTimeField () {
+      return this.requestLastUsedTimeField(true);
+    }
+    public APIRequestGetAssignedAdAccounts requestLastUsedTimeField (boolean value) {
+      this.requestField("last_used_time", value);
+      return this;
+    }
     public APIRequestGetAssignedAdAccounts requestLineNumbersField () {
       return this.requestLineNumbersField(true);
     }
@@ -1115,6 +1312,13 @@ public class Business extends APINode {
       this.requestField("name", value);
       return this;
     }
+    public APIRequestGetAssignedAdAccounts requestOffsitePixelsTosAcceptedField () {
+      return this.requestOffsitePixelsTosAcceptedField(true);
+    }
+    public APIRequestGetAssignedAdAccounts requestOffsitePixelsTosAcceptedField (boolean value) {
+      this.requestField("offsite_pixels_tos_accepted", value);
+      return this;
+    }
     public APIRequestGetAssignedAdAccounts requestOwnerField () {
       return this.requestOwnerField(true);
     }
@@ -1122,11 +1326,11 @@ public class Business extends APINode {
       this.requestField("owner", value);
       return this;
     }
-    public APIRequestGetAssignedAdAccounts requestOffsitePixelsTosAcceptedField () {
-      return this.requestOffsitePixelsTosAcceptedField(true);
+    public APIRequestGetAssignedAdAccounts requestOwnerBusinessField () {
+      return this.requestOwnerBusinessField(true);
     }
-    public APIRequestGetAssignedAdAccounts requestOffsitePixelsTosAcceptedField (boolean value) {
-      this.requestField("offsite_pixels_tos_accepted", value);
+    public APIRequestGetAssignedAdAccounts requestOwnerBusinessField (boolean value) {
+      this.requestField("owner_business", value);
       return this;
     }
     public APIRequestGetAssignedAdAccounts requestPartnerField () {
@@ -1136,6 +1340,20 @@ public class Business extends APINode {
       this.requestField("partner", value);
       return this;
     }
+    public APIRequestGetAssignedAdAccounts requestRfSpecField () {
+      return this.requestRfSpecField(true);
+    }
+    public APIRequestGetAssignedAdAccounts requestRfSpecField (boolean value) {
+      this.requestField("rf_spec", value);
+      return this;
+    }
+    public APIRequestGetAssignedAdAccounts requestSpendCapField () {
+      return this.requestSpendCapField(true);
+    }
+    public APIRequestGetAssignedAdAccounts requestSpendCapField (boolean value) {
+      this.requestField("spend_cap", value);
+      return this;
+    }
     public APIRequestGetAssignedAdAccounts requestTaxIdField () {
       return this.requestTaxIdField(true);
     }
@@ -1178,13 +1396,6 @@ public class Business extends APINode {
       this.requestField("timezone_offset_hours_utc", value);
       return this;
     }
-    public APIRequestGetAssignedAdAccounts requestRfSpecField () {
-      return this.requestRfSpecField(true);
-    }
-    public APIRequestGetAssignedAdAccounts requestRfSpecField (boolean value) {
-      this.requestField("rf_spec", value);
-      return this;
-    }
     public APIRequestGetAssignedAdAccounts requestTosAcceptedField () {
       return this.requestTosAcceptedField(true);
     }
@@ -1199,63 +1410,6 @@ public class Business extends APINode {
       this.requestField("user_role", value);
       return this;
     }
-    public APIRequestGetAssignedAdAccounts requestVerticalNameField () {
-      return this.requestVerticalNameField(true);
-    }
-    public APIRequestGetAssignedAdAccounts requestVerticalNameField (boolean value) {
-      this.requestField("vertical_name", value);
-      return this;
-    }
-    public APIRequestGetAssignedAdAccounts requestAmountSpentField () {
-      return this.requestAmountSpentField(true);
-    }
-    public APIRequestGetAssignedAdAccounts requestAmountSpentField (boolean value) {
-      this.requestField("amount_spent", value);
-      return this;
-    }
-    public APIRequestGetAssignedAdAccounts requestSpendCapField () {
-      return this.requestSpendCapField(true);
-    }
-    public APIRequestGetAssignedAdAccounts requestSpendCapField (boolean value) {
-      this.requestField("spend_cap", value);
-      return this;
-    }
-    public APIRequestGetAssignedAdAccounts requestBalanceField () {
-      return this.requestBalanceField(true);
-    }
-    public APIRequestGetAssignedAdAccounts requestBalanceField (boolean value) {
-      this.requestField("balance", value);
-      return this;
-    }
-    public APIRequestGetAssignedAdAccounts requestBusinessField () {
-      return this.requestBusinessField(true);
-    }
-    public APIRequestGetAssignedAdAccounts requestBusinessField (boolean value) {
-      this.requestField("business", value);
-      return this;
-    }
-    public APIRequestGetAssignedAdAccounts requestOwnerBusinessField () {
-      return this.requestOwnerBusinessField(true);
-    }
-    public APIRequestGetAssignedAdAccounts requestOwnerBusinessField (boolean value) {
-      this.requestField("owner_business", value);
-      return this;
-    }
-    public APIRequestGetAssignedAdAccounts requestLastUsedTimeField () {
-      return this.requestLastUsedTimeField(true);
-    }
-    public APIRequestGetAssignedAdAccounts requestLastUsedTimeField (boolean value) {
-      this.requestField("last_used_time", value);
-      return this;
-    }
-    public APIRequestGetAssignedAdAccounts requestAssetScoreField () {
-      return this.requestAssetScoreField(true);
-    }
-    public APIRequestGetAssignedAdAccounts requestAssetScoreField (boolean value) {
-      this.requestField("asset_score", value);
-      return this;
-    }
-
   }
 
   public static class APIRequestGetAssignedPages extends APIRequest<APINode> {
@@ -1266,8 +1420,8 @@ public class Business extends APINode {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "user_id",
       "email",
+      "user_id",
     };
 
     public static final String[] FIELDS = {
@@ -1285,7 +1439,7 @@ public class Business extends APINode {
 
     @Override
     public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -1293,33 +1447,33 @@ public class Business extends APINode {
       super(context, nodeId, "/assigned_pages", "GET", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestGetAssignedPages setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestGetAssignedPages setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetAssignedPages setUserId (Long userId) {
-      this.setParam("user_id", userId);
+    public APIRequestGetAssignedPages setEmail (String email) {
+      this.setParam("email", email);
       return this;
     }
 
-    public APIRequestGetAssignedPages setUserId (String userId) {
+    public APIRequestGetAssignedPages setUserId (Long userId) {
       this.setParam("user_id", userId);
       return this;
     }
-
-    public APIRequestGetAssignedPages setEmail (String email) {
-      this.setParam("email", email);
+    public APIRequestGetAssignedPages setUserId (String userId) {
+      this.setParam("user_id", userId);
       return this;
     }
 
-
     public APIRequestGetAssignedPages requestAllFields () {
       return this.requestAllFields(true);
     }
@@ -1331,10 +1485,12 @@ public class Business extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetAssignedPages requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestGetAssignedPages requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -1342,17 +1498,18 @@ public class Business extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetAssignedPages requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGetAssignedPages requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
   }
 
   public static class APIRequestGetAssignedProductCatalogs extends APIRequest<ProductCatalog> {
@@ -1363,14 +1520,14 @@ public class Business extends APINode {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "user_id",
       "email",
+      "user_id",
     };
 
     public static final String[] FIELDS = {
-      "id",
       "business",
       "feed_count",
+      "id",
       "name",
       "product_count",
     };
@@ -1387,7 +1544,7 @@ public class Business extends APINode {
 
     @Override
     public APINodeList<ProductCatalog> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -1395,33 +1552,33 @@ public class Business extends APINode {
       super(context, nodeId, "/assigned_product_catalogs", "GET", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestGetAssignedProductCatalogs setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestGetAssignedProductCatalogs setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetAssignedProductCatalogs setUserId (Long userId) {
-      this.setParam("user_id", userId);
+    public APIRequestGetAssignedProductCatalogs setEmail (String email) {
+      this.setParam("email", email);
       return this;
     }
 
-    public APIRequestGetAssignedProductCatalogs setUserId (String userId) {
+    public APIRequestGetAssignedProductCatalogs setUserId (Long userId) {
       this.setParam("user_id", userId);
       return this;
     }
-
-    public APIRequestGetAssignedProductCatalogs setEmail (String email) {
-      this.setParam("email", email);
+    public APIRequestGetAssignedProductCatalogs setUserId (String userId) {
+      this.setParam("user_id", userId);
       return this;
     }
 
-
     public APIRequestGetAssignedProductCatalogs requestAllFields () {
       return this.requestAllFields(true);
     }
@@ -1433,10 +1590,12 @@ public class Business extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetAssignedProductCatalogs requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestGetAssignedProductCatalogs requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -1444,23 +1603,18 @@ public class Business extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetAssignedProductCatalogs requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGetAssignedProductCatalogs requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetAssignedProductCatalogs requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGetAssignedProductCatalogs requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
-    }
     public APIRequestGetAssignedProductCatalogs requestBusinessField () {
       return this.requestBusinessField(true);
     }
@@ -1475,6 +1629,13 @@ public class Business extends APINode {
       this.requestField("feed_count", value);
       return this;
     }
+    public APIRequestGetAssignedProductCatalogs requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGetAssignedProductCatalogs requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
     public APIRequestGetAssignedProductCatalogs requestNameField () {
       return this.requestNameField(true);
     }
@@ -1489,73 +1650,178 @@ public class Business extends APINode {
       this.requestField("product_count", value);
       return this;
     }
-
   }
 
-  public static class APIRequestGetClientAdAccounts extends APIRequest<AdAccount> {
+  public static class APIRequestGetClientAdAccountRequests extends APIRequest<BusinessAdAccountRequest> {
 
-    APINodeList<AdAccount> lastResponse = null;
+    APINodeList<BusinessAdAccountRequest> lastResponse = null;
     @Override
-    public APINodeList<AdAccount> getLastResponse() {
+    public APINodeList<BusinessAdAccountRequest> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
     };
 
     public static final String[] FIELDS = {
+      "ad_account",
       "id",
-      "account_groups",
-      "account_id",
-      "account_status",
-      "age",
-      "agency_client_declaration",
-      "business_city",
-      "business_country_code",
-      "business_name",
-      "business_state",
-      "business_street",
-      "business_street2",
-      "business_zip",
-      "capabilities",
-      "created_time",
-      "currency",
-      "disable_reason",
-      "end_advertiser",
-      "end_advertiser_name",
-      "failed_delivery_checks",
-      "funding_source",
-      "funding_source_details",
-      "has_migrated_permissions",
-      "io_number",
-      "is_notifications_enabled",
-      "is_personal",
-      "is_prepay_account",
-      "is_tax_id_required",
-      "line_numbers",
-      "media_agency",
-      "min_campaign_group_spend_cap",
-      "min_daily_budget",
-      "name",
-      "owner",
-      "offsite_pixels_tos_accepted",
-      "partner",
-      "tax_id",
-      "tax_id_status",
-      "tax_id_type",
-      "timezone_id",
-      "timezone_name",
-      "timezone_offset_hours_utc",
-      "rf_spec",
-      "tos_accepted",
-      "user_role",
-      "vertical_name",
-      "amount_spent",
-      "spend_cap",
-      "balance",
-      "business",
-      "owner_business",
-      "last_used_time",
-      "asset_score",
+      "permitted_roles",
+    };
+
+    @Override
+    public APINodeList<BusinessAdAccountRequest> parseResponse(String response) throws APIException {
+      return BusinessAdAccountRequest.parseResponse(response, getContext(), this);
+    }
+
+    @Override
+    public APINodeList<BusinessAdAccountRequest> execute() throws APIException {
+      return execute(new HashMap<String, Object>());
+    }
+
+    @Override
+    public APINodeList<BusinessAdAccountRequest> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
+    }
+
+    public APIRequestGetClientAdAccountRequests(String nodeId, APIContext context) {
+      super(context, nodeId, "/client_ad_account_requests", "GET", Arrays.asList(PARAMS));
+    }
+
+    @Override
+    public APIRequestGetClientAdAccountRequests setParam(String param, Object value) {
+      setParamInternal(param, value);
+      return this;
+    }
+
+    @Override
+    public APIRequestGetClientAdAccountRequests setParams(Map<String, Object> params) {
+      setParamsInternal(params);
+      return this;
+    }
+
+
+    public APIRequestGetClientAdAccountRequests requestAllFields () {
+      return this.requestAllFields(true);
+    }
+
+    public APIRequestGetClientAdAccountRequests requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestGetClientAdAccountRequests requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
+    }
+
+    @Override
+    public APIRequestGetClientAdAccountRequests requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestGetClientAdAccountRequests requestField (String field) {
+      this.requestField(field, true);
+      return this;
+    }
+
+    @Override
+    public APIRequestGetClientAdAccountRequests requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
+      return this;
+    }
+
+    public APIRequestGetClientAdAccountRequests requestAdAccountField () {
+      return this.requestAdAccountField(true);
+    }
+    public APIRequestGetClientAdAccountRequests requestAdAccountField (boolean value) {
+      this.requestField("ad_account", value);
+      return this;
+    }
+    public APIRequestGetClientAdAccountRequests requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGetClientAdAccountRequests requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
+    public APIRequestGetClientAdAccountRequests requestPermittedRolesField () {
+      return this.requestPermittedRolesField(true);
+    }
+    public APIRequestGetClientAdAccountRequests requestPermittedRolesField (boolean value) {
+      this.requestField("permitted_roles", value);
+      return this;
+    }
+  }
+
+  public static class APIRequestGetClientAdAccounts extends APIRequest<AdAccount> {
+
+    APINodeList<AdAccount> lastResponse = null;
+    @Override
+    public APINodeList<AdAccount> getLastResponse() {
+      return lastResponse;
+    }
+    public static final String[] PARAMS = {
+    };
+
+    public static final String[] FIELDS = {
+      "account_groups",
+      "account_id",
+      "account_status",
+      "age",
+      "agency_client_declaration",
+      "amount_spent",
+      "balance",
+      "business",
+      "business_city",
+      "business_country_code",
+      "business_name",
+      "business_state",
+      "business_street",
+      "business_street2",
+      "business_zip",
+      "capabilities",
+      "created_time",
+      "currency",
+      "disable_reason",
+      "end_advertiser",
+      "end_advertiser_name",
+      "failed_delivery_checks",
+      "funding_source",
+      "funding_source_details",
+      "has_migrated_permissions",
+      "id",
+      "io_number",
+      "is_notifications_enabled",
+      "is_personal",
+      "is_prepay_account",
+      "is_tax_id_required",
+      "last_used_time",
+      "line_numbers",
+      "media_agency",
+      "min_campaign_group_spend_cap",
+      "min_daily_budget",
+      "name",
+      "offsite_pixels_tos_accepted",
+      "owner",
+      "owner_business",
+      "partner",
+      "rf_spec",
+      "spend_cap",
+      "tax_id",
+      "tax_id_status",
+      "tax_id_type",
+      "timezone_id",
+      "timezone_name",
+      "timezone_offset_hours_utc",
+      "tos_accepted",
+      "user_role",
     };
 
     @Override
@@ -1570,7 +1836,7 @@ public class Business extends APINode {
 
     @Override
     public APINodeList<AdAccount> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -1578,11 +1844,13 @@ public class Business extends APINode {
       super(context, nodeId, "/client_ad_accounts", "GET", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestGetClientAdAccounts setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestGetClientAdAccounts setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
@@ -1600,10 +1868,12 @@ public class Business extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetClientAdAccounts requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestGetClientAdAccounts requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -1611,23 +1881,18 @@ public class Business extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetClientAdAccounts requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGetClientAdAccounts requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetClientAdAccounts requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGetClientAdAccounts requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
-    }
     public APIRequestGetClientAdAccounts requestAccountGroupsField () {
       return this.requestAccountGroupsField(true);
     }
@@ -1663,6 +1928,27 @@ public class Business extends APINode {
       this.requestField("agency_client_declaration", value);
       return this;
     }
+    public APIRequestGetClientAdAccounts requestAmountSpentField () {
+      return this.requestAmountSpentField(true);
+    }
+    public APIRequestGetClientAdAccounts requestAmountSpentField (boolean value) {
+      this.requestField("amount_spent", value);
+      return this;
+    }
+    public APIRequestGetClientAdAccounts requestBalanceField () {
+      return this.requestBalanceField(true);
+    }
+    public APIRequestGetClientAdAccounts requestBalanceField (boolean value) {
+      this.requestField("balance", value);
+      return this;
+    }
+    public APIRequestGetClientAdAccounts requestBusinessField () {
+      return this.requestBusinessField(true);
+    }
+    public APIRequestGetClientAdAccounts requestBusinessField (boolean value) {
+      this.requestField("business", value);
+      return this;
+    }
     public APIRequestGetClientAdAccounts requestBusinessCityField () {
       return this.requestBusinessCityField(true);
     }
@@ -1782,6 +2068,13 @@ public class Business extends APINode {
       this.requestField("has_migrated_permissions", value);
       return this;
     }
+    public APIRequestGetClientAdAccounts requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGetClientAdAccounts requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
     public APIRequestGetClientAdAccounts requestIoNumberField () {
       return this.requestIoNumberField(true);
     }
@@ -1817,6 +2110,13 @@ public class Business extends APINode {
       this.requestField("is_tax_id_required", value);
       return this;
     }
+    public APIRequestGetClientAdAccounts requestLastUsedTimeField () {
+      return this.requestLastUsedTimeField(true);
+    }
+    public APIRequestGetClientAdAccounts requestLastUsedTimeField (boolean value) {
+      this.requestField("last_used_time", value);
+      return this;
+    }
     public APIRequestGetClientAdAccounts requestLineNumbersField () {
       return this.requestLineNumbersField(true);
     }
@@ -1852,6 +2152,13 @@ public class Business extends APINode {
       this.requestField("name", value);
       return this;
     }
+    public APIRequestGetClientAdAccounts requestOffsitePixelsTosAcceptedField () {
+      return this.requestOffsitePixelsTosAcceptedField(true);
+    }
+    public APIRequestGetClientAdAccounts requestOffsitePixelsTosAcceptedField (boolean value) {
+      this.requestField("offsite_pixels_tos_accepted", value);
+      return this;
+    }
     public APIRequestGetClientAdAccounts requestOwnerField () {
       return this.requestOwnerField(true);
     }
@@ -1859,11 +2166,11 @@ public class Business extends APINode {
       this.requestField("owner", value);
       return this;
     }
-    public APIRequestGetClientAdAccounts requestOffsitePixelsTosAcceptedField () {
-      return this.requestOffsitePixelsTosAcceptedField(true);
+    public APIRequestGetClientAdAccounts requestOwnerBusinessField () {
+      return this.requestOwnerBusinessField(true);
     }
-    public APIRequestGetClientAdAccounts requestOffsitePixelsTosAcceptedField (boolean value) {
-      this.requestField("offsite_pixels_tos_accepted", value);
+    public APIRequestGetClientAdAccounts requestOwnerBusinessField (boolean value) {
+      this.requestField("owner_business", value);
       return this;
     }
     public APIRequestGetClientAdAccounts requestPartnerField () {
@@ -1873,6 +2180,20 @@ public class Business extends APINode {
       this.requestField("partner", value);
       return this;
     }
+    public APIRequestGetClientAdAccounts requestRfSpecField () {
+      return this.requestRfSpecField(true);
+    }
+    public APIRequestGetClientAdAccounts requestRfSpecField (boolean value) {
+      this.requestField("rf_spec", value);
+      return this;
+    }
+    public APIRequestGetClientAdAccounts requestSpendCapField () {
+      return this.requestSpendCapField(true);
+    }
+    public APIRequestGetClientAdAccounts requestSpendCapField (boolean value) {
+      this.requestField("spend_cap", value);
+      return this;
+    }
     public APIRequestGetClientAdAccounts requestTaxIdField () {
       return this.requestTaxIdField(true);
     }
@@ -1915,13 +2236,6 @@ public class Business extends APINode {
       this.requestField("timezone_offset_hours_utc", value);
       return this;
     }
-    public APIRequestGetClientAdAccounts requestRfSpecField () {
-      return this.requestRfSpecField(true);
-    }
-    public APIRequestGetClientAdAccounts requestRfSpecField (boolean value) {
-      this.requestField("rf_spec", value);
-      return this;
-    }
     public APIRequestGetClientAdAccounts requestTosAcceptedField () {
       return this.requestTosAcceptedField(true);
     }
@@ -1936,166 +2250,106 @@ public class Business extends APINode {
       this.requestField("user_role", value);
       return this;
     }
-    public APIRequestGetClientAdAccounts requestVerticalNameField () {
-      return this.requestVerticalNameField(true);
-    }
-    public APIRequestGetClientAdAccounts requestVerticalNameField (boolean value) {
-      this.requestField("vertical_name", value);
-      return this;
-    }
-    public APIRequestGetClientAdAccounts requestAmountSpentField () {
-      return this.requestAmountSpentField(true);
-    }
-    public APIRequestGetClientAdAccounts requestAmountSpentField (boolean value) {
-      this.requestField("amount_spent", value);
-      return this;
-    }
-    public APIRequestGetClientAdAccounts requestSpendCapField () {
-      return this.requestSpendCapField(true);
-    }
-    public APIRequestGetClientAdAccounts requestSpendCapField (boolean value) {
-      this.requestField("spend_cap", value);
-      return this;
-    }
-    public APIRequestGetClientAdAccounts requestBalanceField () {
-      return this.requestBalanceField(true);
-    }
-    public APIRequestGetClientAdAccounts requestBalanceField (boolean value) {
-      this.requestField("balance", value);
-      return this;
-    }
-    public APIRequestGetClientAdAccounts requestBusinessField () {
-      return this.requestBusinessField(true);
-    }
-    public APIRequestGetClientAdAccounts requestBusinessField (boolean value) {
-      this.requestField("business", value);
-      return this;
-    }
-    public APIRequestGetClientAdAccounts requestOwnerBusinessField () {
-      return this.requestOwnerBusinessField(true);
-    }
-    public APIRequestGetClientAdAccounts requestOwnerBusinessField (boolean value) {
-      this.requestField("owner_business", value);
-      return this;
-    }
-    public APIRequestGetClientAdAccounts requestLastUsedTimeField () {
-      return this.requestLastUsedTimeField(true);
-    }
-    public APIRequestGetClientAdAccounts requestLastUsedTimeField (boolean value) {
-      this.requestField("last_used_time", value);
-      return this;
-    }
-    public APIRequestGetClientAdAccounts requestAssetScoreField () {
-      return this.requestAssetScoreField(true);
-    }
-    public APIRequestGetClientAdAccounts requestAssetScoreField (boolean value) {
-      this.requestField("asset_score", value);
-      return this;
-    }
-
-  }
-
-  public static class APIRequestGetClientAdAccountRequests extends APIRequest<BusinessAdAccountRequest> {
-
-    APINodeList<BusinessAdAccountRequest> lastResponse = null;
-    @Override
-    public APINodeList<BusinessAdAccountRequest> getLastResponse() {
-      return lastResponse;
+  }
+
+  public static class APIRequestGetClientPageRequests extends APIRequest<BusinessPageRequest> {
+
+    APINodeList<BusinessPageRequest> lastResponse = null;
+    @Override
+    public APINodeList<BusinessPageRequest> getLastResponse() {
+      return lastResponse;
     }
     public static final String[] PARAMS = {
     };
 
     public static final String[] FIELDS = {
       "id",
-      "ad_account",
-      "permitted_roles",
+      "page",
     };
 
     @Override
-    public APINodeList<BusinessAdAccountRequest> parseResponse(String response) throws APIException {
-      return BusinessAdAccountRequest.parseResponse(response, getContext(), this);
+    public APINodeList<BusinessPageRequest> parseResponse(String response) throws APIException {
+      return BusinessPageRequest.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINodeList<BusinessAdAccountRequest> execute() throws APIException {
+    public APINodeList<BusinessPageRequest> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<BusinessAdAccountRequest> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<BusinessPageRequest> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetClientAdAccountRequests(String nodeId, APIContext context) {
-      super(context, nodeId, "/client_ad_account_requests", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetClientPageRequests(String nodeId, APIContext context) {
+      super(context, nodeId, "/client_page_requests", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetClientAdAccountRequests setParam(String param, Object value) {
+    @Override
+    public APIRequestGetClientPageRequests setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetClientAdAccountRequests setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetClientPageRequests setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetClientAdAccountRequests requestAllFields () {
+    public APIRequestGetClientPageRequests requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetClientAdAccountRequests requestAllFields (boolean value) {
+    public APIRequestGetClientPageRequests requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetClientAdAccountRequests requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetClientPageRequests requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetClientAdAccountRequests requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetClientPageRequests requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetClientAdAccountRequests requestField (String field) {
+    @Override
+    public APIRequestGetClientPageRequests requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetClientAdAccountRequests requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetClientPageRequests requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetClientAdAccountRequests requestIdField () {
+    public APIRequestGetClientPageRequests requestIdField () {
       return this.requestIdField(true);
     }
-    public APIRequestGetClientAdAccountRequests requestIdField (boolean value) {
+    public APIRequestGetClientPageRequests requestIdField (boolean value) {
       this.requestField("id", value);
       return this;
     }
-    public APIRequestGetClientAdAccountRequests requestAdAccountField () {
-      return this.requestAdAccountField(true);
-    }
-    public APIRequestGetClientAdAccountRequests requestAdAccountField (boolean value) {
-      this.requestField("ad_account", value);
-      return this;
-    }
-    public APIRequestGetClientAdAccountRequests requestPermittedRolesField () {
-      return this.requestPermittedRolesField(true);
+    public APIRequestGetClientPageRequests requestPageField () {
+      return this.requestPageField(true);
     }
-    public APIRequestGetClientAdAccountRequests requestPermittedRolesField (boolean value) {
-      this.requestField("permitted_roles", value);
+    public APIRequestGetClientPageRequests requestPageField (boolean value) {
+      this.requestField("page", value);
       return this;
     }
-
   }
 
   public static class APIRequestGetClientPages extends APIRequest<APINode> {
@@ -2123,7 +2377,7 @@ public class Business extends APINode {
 
     @Override
     public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -2131,11 +2385,13 @@ public class Business extends APINode {
       super(context, nodeId, "/client_pages", "GET", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestGetClientPages setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestGetClientPages setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
@@ -2153,10 +2409,12 @@ public class Business extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetClientPages requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestGetClientPages requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -2164,721 +2422,463 @@ public class Business extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGetClientPages requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGetClientPages requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
   }
 
-  public static class APIRequestGetClientPageRequests extends APIRequest<BusinessPageRequest> {
+  public static class APIRequestGetGrpPlans extends APIRequest<ReachFrequencyPrediction> {
 
-    APINodeList<BusinessPageRequest> lastResponse = null;
+    APINodeList<ReachFrequencyPrediction> lastResponse = null;
     @Override
-    public APINodeList<BusinessPageRequest> getLastResponse() {
+    public APINodeList<ReachFrequencyPrediction> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
+      "status",
     };
 
     public static final String[] FIELDS = {
+      "account_id",
+      "campaign_group_id",
+      "campaign_id",
+      "campaign_time_start",
+      "campaign_time_stop",
+      "curve_budget_reach",
+      "destination_id",
+      "expiration_time",
+      "external_budget",
+      "external_impression",
+      "external_maximum_budget",
+      "external_maximum_impression",
+      "external_maximum_reach",
+      "external_minimum_budget",
+      "external_minimum_impression",
+      "external_minimum_reach",
+      "external_reach",
+      "frequency_cap",
+      "grp_dmas_audience_size",
+      "holdout_percentage",
       "id",
-      "page",
+      "instagram_destination_id",
+      "interval_frequency_cap_reset_period",
+      "name",
+      "prediction_mode",
+      "prediction_progress",
+      "reservation_status",
+      "status",
+      "story_event_type",
+      "target_audience_size",
+      "target_spec",
+      "time_created",
+      "time_updated",
     };
 
     @Override
-    public APINodeList<BusinessPageRequest> parseResponse(String response) throws APIException {
-      return BusinessPageRequest.parseResponse(response, getContext(), this);
+    public APINodeList<ReachFrequencyPrediction> parseResponse(String response) throws APIException {
+      return ReachFrequencyPrediction.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINodeList<BusinessPageRequest> execute() throws APIException {
+    public APINodeList<ReachFrequencyPrediction> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<BusinessPageRequest> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<ReachFrequencyPrediction> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetClientPageRequests(String nodeId, APIContext context) {
-      super(context, nodeId, "/client_page_requests", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetGrpPlans(String nodeId, APIContext context) {
+      super(context, nodeId, "/grp_plans", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetClientPageRequests setParam(String param, Object value) {
+    @Override
+    public APIRequestGetGrpPlans setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetClientPageRequests setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetGrpPlans setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetClientPageRequests requestAllFields () {
+    public APIRequestGetGrpPlans setStatus (ReachFrequencyPrediction.EnumStatus status) {
+      this.setParam("status", status);
+      return this;
+    }
+    public APIRequestGetGrpPlans setStatus (String status) {
+      this.setParam("status", status);
+      return this;
+    }
+
+    public APIRequestGetGrpPlans requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetClientPageRequests requestAllFields (boolean value) {
+    public APIRequestGetGrpPlans requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetClientPageRequests requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetGrpPlans requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetClientPageRequests requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetGrpPlans requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetClientPageRequests requestField (String field) {
+    @Override
+    public APIRequestGetGrpPlans requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetClientPageRequests requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetGrpPlans requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetClientPageRequests requestIdField () {
-      return this.requestIdField(true);
+    public APIRequestGetGrpPlans requestAccountIdField () {
+      return this.requestAccountIdField(true);
     }
-    public APIRequestGetClientPageRequests requestIdField (boolean value) {
-      this.requestField("id", value);
+    public APIRequestGetGrpPlans requestAccountIdField (boolean value) {
+      this.requestField("account_id", value);
       return this;
     }
-    public APIRequestGetClientPageRequests requestPageField () {
-      return this.requestPageField(true);
+    public APIRequestGetGrpPlans requestCampaignGroupIdField () {
+      return this.requestCampaignGroupIdField(true);
     }
-    public APIRequestGetClientPageRequests requestPageField (boolean value) {
-      this.requestField("page", value);
+    public APIRequestGetGrpPlans requestCampaignGroupIdField (boolean value) {
+      this.requestField("campaign_group_id", value);
       return this;
     }
-
-  }
-
-  public static class APIRequestGetOwnedAdAccounts extends APIRequest<AdAccount> {
-
-    APINodeList<AdAccount> lastResponse = null;
-    @Override
-    public APINodeList<AdAccount> getLastResponse() {
-      return lastResponse;
+    public APIRequestGetGrpPlans requestCampaignIdField () {
+      return this.requestCampaignIdField(true);
     }
-    public static final String[] PARAMS = {
-    };
-
-    public static final String[] FIELDS = {
-      "id",
-      "account_groups",
-      "account_id",
-      "account_status",
-      "age",
-      "agency_client_declaration",
-      "business_city",
-      "business_country_code",
-      "business_name",
-      "business_state",
-      "business_street",
-      "business_street2",
-      "business_zip",
-      "capabilities",
-      "created_time",
-      "currency",
-      "disable_reason",
-      "end_advertiser",
-      "end_advertiser_name",
-      "failed_delivery_checks",
-      "funding_source",
-      "funding_source_details",
-      "has_migrated_permissions",
-      "io_number",
-      "is_notifications_enabled",
-      "is_personal",
-      "is_prepay_account",
-      "is_tax_id_required",
-      "line_numbers",
-      "media_agency",
-      "min_campaign_group_spend_cap",
-      "min_daily_budget",
-      "name",
-      "owner",
-      "offsite_pixels_tos_accepted",
-      "partner",
-      "tax_id",
-      "tax_id_status",
-      "tax_id_type",
-      "timezone_id",
-      "timezone_name",
-      "timezone_offset_hours_utc",
-      "rf_spec",
-      "tos_accepted",
-      "user_role",
-      "vertical_name",
-      "amount_spent",
-      "spend_cap",
-      "balance",
-      "business",
-      "owner_business",
-      "last_used_time",
-      "asset_score",
-    };
-
-    @Override
-    public APINodeList<AdAccount> parseResponse(String response) throws APIException {
-      return AdAccount.parseResponse(response, getContext(), this);
+    public APIRequestGetGrpPlans requestCampaignIdField (boolean value) {
+      this.requestField("campaign_id", value);
+      return this;
     }
-
-    @Override
-    public APINodeList<AdAccount> execute() throws APIException {
-      return execute(new HashMap<String, Object>());
+    public APIRequestGetGrpPlans requestCampaignTimeStartField () {
+      return this.requestCampaignTimeStartField(true);
     }
-
-    @Override
-    public APINodeList<AdAccount> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
+    public APIRequestGetGrpPlans requestCampaignTimeStartField (boolean value) {
+      this.requestField("campaign_time_start", value);
+      return this;
     }
-
-    public APIRequestGetOwnedAdAccounts(String nodeId, APIContext context) {
-      super(context, nodeId, "/owned_ad_accounts", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetGrpPlans requestCampaignTimeStopField () {
+      return this.requestCampaignTimeStopField(true);
     }
-
-    public APIRequestGetOwnedAdAccounts setParam(String param, Object value) {
-      setParamInternal(param, value);
+    public APIRequestGetGrpPlans requestCampaignTimeStopField (boolean value) {
+      this.requestField("campaign_time_stop", value);
       return this;
     }
-
-    public APIRequestGetOwnedAdAccounts setParams(Map<String, Object> params) {
-      setParamsInternal(params);
+    public APIRequestGetGrpPlans requestCurveBudgetReachField () {
+      return this.requestCurveBudgetReachField(true);
+    }
+    public APIRequestGetGrpPlans requestCurveBudgetReachField (boolean value) {
+      this.requestField("curve_budget_reach", value);
       return this;
     }
-
-
-    public APIRequestGetOwnedAdAccounts requestAllFields () {
-      return this.requestAllFields(true);
+    public APIRequestGetGrpPlans requestDestinationIdField () {
+      return this.requestDestinationIdField(true);
     }
-
-    public APIRequestGetOwnedAdAccounts requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
+    public APIRequestGetGrpPlans requestDestinationIdField (boolean value) {
+      this.requestField("destination_id", value);
       return this;
     }
-
-    public APIRequestGetOwnedAdAccounts requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
+    public APIRequestGetGrpPlans requestExpirationTimeField () {
+      return this.requestExpirationTimeField(true);
     }
-
-    public APIRequestGetOwnedAdAccounts requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+    public APIRequestGetGrpPlans requestExpirationTimeField (boolean value) {
+      this.requestField("expiration_time", value);
       return this;
     }
-
-    public APIRequestGetOwnedAdAccounts requestField (String field) {
-      this.requestField(field, true);
-      return this;
+    public APIRequestGetGrpPlans requestExternalBudgetField () {
+      return this.requestExternalBudgetField(true);
     }
-
-    public APIRequestGetOwnedAdAccounts requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
+    public APIRequestGetGrpPlans requestExternalBudgetField (boolean value) {
+      this.requestField("external_budget", value);
       return this;
     }
-
-    public APIRequestGetOwnedAdAccounts requestIdField () {
-      return this.requestIdField(true);
+    public APIRequestGetGrpPlans requestExternalImpressionField () {
+      return this.requestExternalImpressionField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestIdField (boolean value) {
-      this.requestField("id", value);
+    public APIRequestGetGrpPlans requestExternalImpressionField (boolean value) {
+      this.requestField("external_impression", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestAccountGroupsField () {
-      return this.requestAccountGroupsField(true);
+    public APIRequestGetGrpPlans requestExternalMaximumBudgetField () {
+      return this.requestExternalMaximumBudgetField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestAccountGroupsField (boolean value) {
-      this.requestField("account_groups", value);
+    public APIRequestGetGrpPlans requestExternalMaximumBudgetField (boolean value) {
+      this.requestField("external_maximum_budget", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestAccountIdField () {
-      return this.requestAccountIdField(true);
+    public APIRequestGetGrpPlans requestExternalMaximumImpressionField () {
+      return this.requestExternalMaximumImpressionField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestAccountIdField (boolean value) {
-      this.requestField("account_id", value);
+    public APIRequestGetGrpPlans requestExternalMaximumImpressionField (boolean value) {
+      this.requestField("external_maximum_impression", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestAccountStatusField () {
-      return this.requestAccountStatusField(true);
+    public APIRequestGetGrpPlans requestExternalMaximumReachField () {
+      return this.requestExternalMaximumReachField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestAccountStatusField (boolean value) {
-      this.requestField("account_status", value);
+    public APIRequestGetGrpPlans requestExternalMaximumReachField (boolean value) {
+      this.requestField("external_maximum_reach", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestAgeField () {
-      return this.requestAgeField(true);
+    public APIRequestGetGrpPlans requestExternalMinimumBudgetField () {
+      return this.requestExternalMinimumBudgetField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestAgeField (boolean value) {
-      this.requestField("age", value);
+    public APIRequestGetGrpPlans requestExternalMinimumBudgetField (boolean value) {
+      this.requestField("external_minimum_budget", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestAgencyClientDeclarationField () {
-      return this.requestAgencyClientDeclarationField(true);
+    public APIRequestGetGrpPlans requestExternalMinimumImpressionField () {
+      return this.requestExternalMinimumImpressionField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestAgencyClientDeclarationField (boolean value) {
-      this.requestField("agency_client_declaration", value);
+    public APIRequestGetGrpPlans requestExternalMinimumImpressionField (boolean value) {
+      this.requestField("external_minimum_impression", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestBusinessCityField () {
-      return this.requestBusinessCityField(true);
+    public APIRequestGetGrpPlans requestExternalMinimumReachField () {
+      return this.requestExternalMinimumReachField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestBusinessCityField (boolean value) {
-      this.requestField("business_city", value);
+    public APIRequestGetGrpPlans requestExternalMinimumReachField (boolean value) {
+      this.requestField("external_minimum_reach", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestBusinessCountryCodeField () {
-      return this.requestBusinessCountryCodeField(true);
+    public APIRequestGetGrpPlans requestExternalReachField () {
+      return this.requestExternalReachField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestBusinessCountryCodeField (boolean value) {
-      this.requestField("business_country_code", value);
+    public APIRequestGetGrpPlans requestExternalReachField (boolean value) {
+      this.requestField("external_reach", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestBusinessNameField () {
-      return this.requestBusinessNameField(true);
+    public APIRequestGetGrpPlans requestFrequencyCapField () {
+      return this.requestFrequencyCapField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestBusinessNameField (boolean value) {
-      this.requestField("business_name", value);
+    public APIRequestGetGrpPlans requestFrequencyCapField (boolean value) {
+      this.requestField("frequency_cap", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestBusinessStateField () {
-      return this.requestBusinessStateField(true);
+    public APIRequestGetGrpPlans requestGrpDmasAudienceSizeField () {
+      return this.requestGrpDmasAudienceSizeField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestBusinessStateField (boolean value) {
-      this.requestField("business_state", value);
+    public APIRequestGetGrpPlans requestGrpDmasAudienceSizeField (boolean value) {
+      this.requestField("grp_dmas_audience_size", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestBusinessStreetField () {
-      return this.requestBusinessStreetField(true);
+    public APIRequestGetGrpPlans requestHoldoutPercentageField () {
+      return this.requestHoldoutPercentageField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestBusinessStreetField (boolean value) {
-      this.requestField("business_street", value);
+    public APIRequestGetGrpPlans requestHoldoutPercentageField (boolean value) {
+      this.requestField("holdout_percentage", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestBusinessStreet2Field () {
-      return this.requestBusinessStreet2Field(true);
+    public APIRequestGetGrpPlans requestIdField () {
+      return this.requestIdField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestBusinessStreet2Field (boolean value) {
-      this.requestField("business_street2", value);
+    public APIRequestGetGrpPlans requestIdField (boolean value) {
+      this.requestField("id", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestBusinessZipField () {
-      return this.requestBusinessZipField(true);
+    public APIRequestGetGrpPlans requestInstagramDestinationIdField () {
+      return this.requestInstagramDestinationIdField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestBusinessZipField (boolean value) {
-      this.requestField("business_zip", value);
+    public APIRequestGetGrpPlans requestInstagramDestinationIdField (boolean value) {
+      this.requestField("instagram_destination_id", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestCapabilitiesField () {
-      return this.requestCapabilitiesField(true);
+    public APIRequestGetGrpPlans requestIntervalFrequencyCapResetPeriodField () {
+      return this.requestIntervalFrequencyCapResetPeriodField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestCapabilitiesField (boolean value) {
-      this.requestField("capabilities", value);
+    public APIRequestGetGrpPlans requestIntervalFrequencyCapResetPeriodField (boolean value) {
+      this.requestField("interval_frequency_cap_reset_period", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestCreatedTimeField () {
-      return this.requestCreatedTimeField(true);
+    public APIRequestGetGrpPlans requestNameField () {
+      return this.requestNameField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestCreatedTimeField (boolean value) {
-      this.requestField("created_time", value);
+    public APIRequestGetGrpPlans requestNameField (boolean value) {
+      this.requestField("name", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestCurrencyField () {
-      return this.requestCurrencyField(true);
+    public APIRequestGetGrpPlans requestPredictionModeField () {
+      return this.requestPredictionModeField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestCurrencyField (boolean value) {
-      this.requestField("currency", value);
+    public APIRequestGetGrpPlans requestPredictionModeField (boolean value) {
+      this.requestField("prediction_mode", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestDisableReasonField () {
-      return this.requestDisableReasonField(true);
+    public APIRequestGetGrpPlans requestPredictionProgressField () {
+      return this.requestPredictionProgressField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestDisableReasonField (boolean value) {
-      this.requestField("disable_reason", value);
+    public APIRequestGetGrpPlans requestPredictionProgressField (boolean value) {
+      this.requestField("prediction_progress", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestEndAdvertiserField () {
-      return this.requestEndAdvertiserField(true);
+    public APIRequestGetGrpPlans requestReservationStatusField () {
+      return this.requestReservationStatusField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestEndAdvertiserField (boolean value) {
-      this.requestField("end_advertiser", value);
+    public APIRequestGetGrpPlans requestReservationStatusField (boolean value) {
+      this.requestField("reservation_status", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestEndAdvertiserNameField () {
-      return this.requestEndAdvertiserNameField(true);
+    public APIRequestGetGrpPlans requestStatusField () {
+      return this.requestStatusField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestEndAdvertiserNameField (boolean value) {
-      this.requestField("end_advertiser_name", value);
+    public APIRequestGetGrpPlans requestStatusField (boolean value) {
+      this.requestField("status", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestFailedDeliveryChecksField () {
-      return this.requestFailedDeliveryChecksField(true);
+    public APIRequestGetGrpPlans requestStoryEventTypeField () {
+      return this.requestStoryEventTypeField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestFailedDeliveryChecksField (boolean value) {
-      this.requestField("failed_delivery_checks", value);
+    public APIRequestGetGrpPlans requestStoryEventTypeField (boolean value) {
+      this.requestField("story_event_type", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestFundingSourceField () {
-      return this.requestFundingSourceField(true);
+    public APIRequestGetGrpPlans requestTargetAudienceSizeField () {
+      return this.requestTargetAudienceSizeField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestFundingSourceField (boolean value) {
-      this.requestField("funding_source", value);
+    public APIRequestGetGrpPlans requestTargetAudienceSizeField (boolean value) {
+      this.requestField("target_audience_size", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestFundingSourceDetailsField () {
-      return this.requestFundingSourceDetailsField(true);
+    public APIRequestGetGrpPlans requestTargetSpecField () {
+      return this.requestTargetSpecField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestFundingSourceDetailsField (boolean value) {
-      this.requestField("funding_source_details", value);
+    public APIRequestGetGrpPlans requestTargetSpecField (boolean value) {
+      this.requestField("target_spec", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestHasMigratedPermissionsField () {
-      return this.requestHasMigratedPermissionsField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestHasMigratedPermissionsField (boolean value) {
-      this.requestField("has_migrated_permissions", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestIoNumberField () {
-      return this.requestIoNumberField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestIoNumberField (boolean value) {
-      this.requestField("io_number", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestIsNotificationsEnabledField () {
-      return this.requestIsNotificationsEnabledField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestIsNotificationsEnabledField (boolean value) {
-      this.requestField("is_notifications_enabled", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestIsPersonalField () {
-      return this.requestIsPersonalField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestIsPersonalField (boolean value) {
-      this.requestField("is_personal", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestIsPrepayAccountField () {
-      return this.requestIsPrepayAccountField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestIsPrepayAccountField (boolean value) {
-      this.requestField("is_prepay_account", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestIsTaxIdRequiredField () {
-      return this.requestIsTaxIdRequiredField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestIsTaxIdRequiredField (boolean value) {
-      this.requestField("is_tax_id_required", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestLineNumbersField () {
-      return this.requestLineNumbersField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestLineNumbersField (boolean value) {
-      this.requestField("line_numbers", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestMediaAgencyField () {
-      return this.requestMediaAgencyField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestMediaAgencyField (boolean value) {
-      this.requestField("media_agency", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestMinCampaignGroupSpendCapField () {
-      return this.requestMinCampaignGroupSpendCapField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestMinCampaignGroupSpendCapField (boolean value) {
-      this.requestField("min_campaign_group_spend_cap", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestMinDailyBudgetField () {
-      return this.requestMinDailyBudgetField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestMinDailyBudgetField (boolean value) {
-      this.requestField("min_daily_budget", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestNameField () {
-      return this.requestNameField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestNameField (boolean value) {
-      this.requestField("name", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestOwnerField () {
-      return this.requestOwnerField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestOwnerField (boolean value) {
-      this.requestField("owner", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestOffsitePixelsTosAcceptedField () {
-      return this.requestOffsitePixelsTosAcceptedField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestOffsitePixelsTosAcceptedField (boolean value) {
-      this.requestField("offsite_pixels_tos_accepted", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestPartnerField () {
-      return this.requestPartnerField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestPartnerField (boolean value) {
-      this.requestField("partner", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestTaxIdField () {
-      return this.requestTaxIdField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestTaxIdField (boolean value) {
-      this.requestField("tax_id", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestTaxIdStatusField () {
-      return this.requestTaxIdStatusField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestTaxIdStatusField (boolean value) {
-      this.requestField("tax_id_status", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestTaxIdTypeField () {
-      return this.requestTaxIdTypeField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestTaxIdTypeField (boolean value) {
-      this.requestField("tax_id_type", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestTimezoneIdField () {
-      return this.requestTimezoneIdField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestTimezoneIdField (boolean value) {
-      this.requestField("timezone_id", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestTimezoneNameField () {
-      return this.requestTimezoneNameField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestTimezoneNameField (boolean value) {
-      this.requestField("timezone_name", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestTimezoneOffsetHoursUtcField () {
-      return this.requestTimezoneOffsetHoursUtcField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestTimezoneOffsetHoursUtcField (boolean value) {
-      this.requestField("timezone_offset_hours_utc", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestRfSpecField () {
-      return this.requestRfSpecField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestRfSpecField (boolean value) {
-      this.requestField("rf_spec", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestTosAcceptedField () {
-      return this.requestTosAcceptedField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestTosAcceptedField (boolean value) {
-      this.requestField("tos_accepted", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestUserRoleField () {
-      return this.requestUserRoleField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestUserRoleField (boolean value) {
-      this.requestField("user_role", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestVerticalNameField () {
-      return this.requestVerticalNameField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestVerticalNameField (boolean value) {
-      this.requestField("vertical_name", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestAmountSpentField () {
-      return this.requestAmountSpentField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestAmountSpentField (boolean value) {
-      this.requestField("amount_spent", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestSpendCapField () {
-      return this.requestSpendCapField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestSpendCapField (boolean value) {
-      this.requestField("spend_cap", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestBalanceField () {
-      return this.requestBalanceField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestBalanceField (boolean value) {
-      this.requestField("balance", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestBusinessField () {
-      return this.requestBusinessField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestBusinessField (boolean value) {
-      this.requestField("business", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestOwnerBusinessField () {
-      return this.requestOwnerBusinessField(true);
-    }
-    public APIRequestGetOwnedAdAccounts requestOwnerBusinessField (boolean value) {
-      this.requestField("owner_business", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccounts requestLastUsedTimeField () {
-      return this.requestLastUsedTimeField(true);
+    public APIRequestGetGrpPlans requestTimeCreatedField () {
+      return this.requestTimeCreatedField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestLastUsedTimeField (boolean value) {
-      this.requestField("last_used_time", value);
+    public APIRequestGetGrpPlans requestTimeCreatedField (boolean value) {
+      this.requestField("time_created", value);
       return this;
     }
-    public APIRequestGetOwnedAdAccounts requestAssetScoreField () {
-      return this.requestAssetScoreField(true);
+    public APIRequestGetGrpPlans requestTimeUpdatedField () {
+      return this.requestTimeUpdatedField(true);
     }
-    public APIRequestGetOwnedAdAccounts requestAssetScoreField (boolean value) {
-      this.requestField("asset_score", value);
+    public APIRequestGetGrpPlans requestTimeUpdatedField (boolean value) {
+      this.requestField("time_updated", value);
       return this;
     }
-
   }
 
-  public static class APIRequestGetOwnedAdAccountRequests extends APIRequest<BusinessAdAccountRequest> {
+  public static class APIRequestGetInstagramAccounts extends APIRequest<APINode> {
 
-    APINodeList<BusinessAdAccountRequest> lastResponse = null;
+    APINodeList<APINode> lastResponse = null;
     @Override
-    public APINodeList<BusinessAdAccountRequest> getLastResponse() {
+    public APINodeList<APINode> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "ad_account",
-      "permitted_roles",
     };
 
     @Override
-    public APINodeList<BusinessAdAccountRequest> parseResponse(String response) throws APIException {
-      return BusinessAdAccountRequest.parseResponse(response, getContext(), this);
+    public APINodeList<APINode> parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINodeList<BusinessAdAccountRequest> execute() throws APIException {
+    public APINodeList<APINode> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<BusinessAdAccountRequest> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetOwnedAdAccountRequests(String nodeId, APIContext context) {
-      super(context, nodeId, "/owned_ad_account_requests", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetInstagramAccounts(String nodeId, APIContext context) {
+      super(context, nodeId, "/instagram_accounts", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetOwnedAdAccountRequests setParam(String param, Object value) {
+    @Override
+    public APIRequestGetInstagramAccounts setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetOwnedAdAccountRequests setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetInstagramAccounts setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetOwnedAdAccountRequests requestAllFields () {
+    public APIRequestGetInstagramAccounts requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetOwnedAdAccountRequests requestAllFields (boolean value) {
+    public APIRequestGetInstagramAccounts requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetOwnedAdAccountRequests requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetInstagramAccounts requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetOwnedAdAccountRequests requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetInstagramAccounts requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetOwnedAdAccountRequests requestField (String field) {
+    @Override
+    public APIRequestGetInstagramAccounts requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetOwnedAdAccountRequests requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetInstagramAccounts requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetOwnedAdAccountRequests requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGetOwnedAdAccountRequests requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccountRequests requestAdAccountField () {
-      return this.requestAdAccountField(true);
-    }
-    public APIRequestGetOwnedAdAccountRequests requestAdAccountField (boolean value) {
-      this.requestField("ad_account", value);
-      return this;
-    }
-    public APIRequestGetOwnedAdAccountRequests requestPermittedRolesField () {
-      return this.requestPermittedRolesField(true);
-    }
-    public APIRequestGetOwnedAdAccountRequests requestPermittedRolesField (boolean value) {
-      this.requestField("permitted_roles", value);
-      return this;
-    }
-
   }
 
-  public static class APIRequestGetOwnedPages extends APIRequest<APINode> {
+  public static class APIRequestGetMeasurementReports extends APIRequest<APINode> {
 
     APINodeList<APINode> lastResponse = null;
     @Override
@@ -2886,6 +2886,8 @@ public class Business extends APINode {
       return lastResponse;
     }
     public static final String[] PARAMS = {
+      "filters",
+      "report_type",
     };
 
     public static final String[] FIELDS = {
@@ -2903,557 +2905,1158 @@ public class Business extends APINode {
 
     @Override
     public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetOwnedPages(String nodeId, APIContext context) {
-      super(context, nodeId, "/owned_pages", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetMeasurementReports(String nodeId, APIContext context) {
+      super(context, nodeId, "/measurement_reports", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetOwnedPages setParam(String param, Object value) {
+    @Override
+    public APIRequestGetMeasurementReports setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetOwnedPages setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetMeasurementReports setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetOwnedPages requestAllFields () {
+    public APIRequestGetMeasurementReports setFilters (List<Object> filters) {
+      this.setParam("filters", filters);
+      return this;
+    }
+    public APIRequestGetMeasurementReports setFilters (String filters) {
+      this.setParam("filters", filters);
+      return this;
+    }
+
+    public APIRequestGetMeasurementReports setReportType (EnumReportType reportType) {
+      this.setParam("report_type", reportType);
+      return this;
+    }
+    public APIRequestGetMeasurementReports setReportType (String reportType) {
+      this.setParam("report_type", reportType);
+      return this;
+    }
+
+    public APIRequestGetMeasurementReports requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetOwnedPages requestAllFields (boolean value) {
+    public APIRequestGetMeasurementReports requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetOwnedPages requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetMeasurementReports requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetOwnedPages requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetMeasurementReports requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetOwnedPages requestField (String field) {
+    @Override
+    public APIRequestGetMeasurementReports requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetOwnedPages requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetMeasurementReports requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
   }
 
-  public static class APIRequestGetOwnedPageRequests extends APIRequest<BusinessPageRequest> {
+  public static class APIRequestCreateMeasurementReport extends APIRequest<APINode> {
 
-    APINodeList<BusinessPageRequest> lastResponse = null;
+    APINode lastResponse = null;
     @Override
-    public APINodeList<BusinessPageRequest> getLastResponse() {
+    public APINode getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
+      "id",
+      "metadata",
+      "report_type",
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "page",
     };
 
     @Override
-    public APINodeList<BusinessPageRequest> parseResponse(String response) throws APIException {
-      return BusinessPageRequest.parseResponse(response, getContext(), this);
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public APINodeList<BusinessPageRequest> execute() throws APIException {
+    public APINode execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<BusinessPageRequest> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetOwnedPageRequests(String nodeId, APIContext context) {
-      super(context, nodeId, "/owned_page_requests", "GET", Arrays.asList(PARAMS));
+    public APIRequestCreateMeasurementReport(String nodeId, APIContext context) {
+      super(context, nodeId, "/measurement_reports", "POST", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetOwnedPageRequests setParam(String param, Object value) {
+    @Override
+    public APIRequestCreateMeasurementReport setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetOwnedPageRequests setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestCreateMeasurementReport setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetOwnedPageRequests requestAllFields () {
+    public APIRequestCreateMeasurementReport setId (Object id) {
+      this.setParam("id", id);
+      return this;
+    }
+    public APIRequestCreateMeasurementReport setId (String id) {
+      this.setParam("id", id);
+      return this;
+    }
+
+    public APIRequestCreateMeasurementReport setMetadata (String metadata) {
+      this.setParam("metadata", metadata);
+      return this;
+    }
+
+    public APIRequestCreateMeasurementReport setReportType (EnumReportType reportType) {
+      this.setParam("report_type", reportType);
+      return this;
+    }
+    public APIRequestCreateMeasurementReport setReportType (String reportType) {
+      this.setParam("report_type", reportType);
+      return this;
+    }
+
+    public APIRequestCreateMeasurementReport requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetOwnedPageRequests requestAllFields (boolean value) {
+    public APIRequestCreateMeasurementReport requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetOwnedPageRequests requestFields (List<String> fields) {
+    @Override
+    public APIRequestCreateMeasurementReport requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetOwnedPageRequests requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestCreateMeasurementReport requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetOwnedPageRequests requestField (String field) {
+    @Override
+    public APIRequestCreateMeasurementReport requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetOwnedPageRequests requestField (String field, boolean value) {
+    @Override
+    public APIRequestCreateMeasurementReport requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetOwnedPageRequests requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGetOwnedPageRequests requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
-    }
-    public APIRequestGetOwnedPageRequests requestPageField () {
-      return this.requestPageField(true);
-    }
-    public APIRequestGetOwnedPageRequests requestPageField (boolean value) {
-      this.requestField("page", value);
-      return this;
-    }
-
   }
 
-  public static class APIRequestGetOwnedPixels extends APIRequest<AdsPixel> {
+  public static class APIRequestGetOfflineConversionDataSets extends APIRequest<APINode> {
 
-    APINodeList<AdsPixel> lastResponse = null;
+    APINodeList<APINode> lastResponse = null;
     @Override
-    public APINodeList<AdsPixel> getLastResponse() {
+    public APINodeList<APINode> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "owner_business",
-      "owner_ad_account",
-      "name",
-      "creation_time",
-      "last_fired_time",
-      "code",
     };
 
     @Override
-    public APINodeList<AdsPixel> parseResponse(String response) throws APIException {
-      return AdsPixel.parseResponse(response, getContext(), this);
+    public APINodeList<APINode> parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINodeList<AdsPixel> execute() throws APIException {
+    public APINodeList<APINode> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<AdsPixel> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetOwnedPixels(String nodeId, APIContext context) {
-      super(context, nodeId, "/owned_pixels", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetOfflineConversionDataSets(String nodeId, APIContext context) {
+      super(context, nodeId, "/offline_conversion_data_sets", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetOwnedPixels setParam(String param, Object value) {
+    @Override
+    public APIRequestGetOfflineConversionDataSets setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetOwnedPixels setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetOfflineConversionDataSets setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetOwnedPixels requestAllFields () {
+    public APIRequestGetOfflineConversionDataSets requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetOwnedPixels requestAllFields (boolean value) {
+    public APIRequestGetOfflineConversionDataSets requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetOwnedPixels requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetOfflineConversionDataSets requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetOwnedPixels requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetOfflineConversionDataSets requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetOwnedPixels requestField (String field) {
+    @Override
+    public APIRequestGetOfflineConversionDataSets requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetOwnedPixels requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetOfflineConversionDataSets requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetOwnedPixels requestIdField () {
-      return this.requestIdField(true);
+  }
+
+  public static class APIRequestCreateOfflineConversionDataSet extends APIRequest<APINode> {
+
+    APINode lastResponse = null;
+    @Override
+    public APINode getLastResponse() {
+      return lastResponse;
     }
-    public APIRequestGetOwnedPixels requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
+    public static final String[] PARAMS = {
+      "description",
+      "name",
+    };
+
+    public static final String[] FIELDS = {
+    };
+
+    @Override
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
     }
-    public APIRequestGetOwnedPixels requestOwnerBusinessField () {
-      return this.requestOwnerBusinessField(true);
+
+    @Override
+    public APINode execute() throws APIException {
+      return execute(new HashMap<String, Object>());
     }
-    public APIRequestGetOwnedPixels requestOwnerBusinessField (boolean value) {
-      this.requestField("owner_business", value);
-      return this;
+
+    @Override
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
     }
-    public APIRequestGetOwnedPixels requestOwnerAdAccountField () {
-      return this.requestOwnerAdAccountField(true);
+
+    public APIRequestCreateOfflineConversionDataSet(String nodeId, APIContext context) {
+      super(context, nodeId, "/offline_conversion_data_sets", "POST", Arrays.asList(PARAMS));
     }
-    public APIRequestGetOwnedPixels requestOwnerAdAccountField (boolean value) {
-      this.requestField("owner_ad_account", value);
+
+    @Override
+    public APIRequestCreateOfflineConversionDataSet setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
-    public APIRequestGetOwnedPixels requestNameField () {
-      return this.requestNameField(true);
+
+    @Override
+    public APIRequestCreateOfflineConversionDataSet setParams(Map<String, Object> params) {
+      setParamsInternal(params);
+      return this;
     }
-    public APIRequestGetOwnedPixels requestNameField (boolean value) {
-      this.requestField("name", value);
+
+
+    public APIRequestCreateOfflineConversionDataSet setDescription (String description) {
+      this.setParam("description", description);
       return this;
     }
-    public APIRequestGetOwnedPixels requestCreationTimeField () {
-      return this.requestCreationTimeField(true);
+
+    public APIRequestCreateOfflineConversionDataSet setName (String name) {
+      this.setParam("name", name);
+      return this;
     }
-    public APIRequestGetOwnedPixels requestCreationTimeField (boolean value) {
-      this.requestField("creation_time", value);
+
+    public APIRequestCreateOfflineConversionDataSet requestAllFields () {
+      return this.requestAllFields(true);
+    }
+
+    public APIRequestCreateOfflineConversionDataSet requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetOwnedPixels requestLastFiredTimeField () {
-      return this.requestLastFiredTimeField(true);
+
+    @Override
+    public APIRequestCreateOfflineConversionDataSet requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
-    public APIRequestGetOwnedPixels requestLastFiredTimeField (boolean value) {
-      this.requestField("last_fired_time", value);
+
+    @Override
+    public APIRequestCreateOfflineConversionDataSet requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetOwnedPixels requestCodeField () {
-      return this.requestCodeField(true);
+
+    @Override
+    public APIRequestCreateOfflineConversionDataSet requestField (String field) {
+      this.requestField(field, true);
+      return this;
     }
-    public APIRequestGetOwnedPixels requestCodeField (boolean value) {
-      this.requestField("code", value);
+
+    @Override
+    public APIRequestCreateOfflineConversionDataSet requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
 
   }
 
-  public static class APIRequestGetPicture extends APIRequest<ProfilePictureSource> {
+  public static class APIRequestGetOwnedAdAccountRequests extends APIRequest<BusinessAdAccountRequest> {
 
-    APINodeList<ProfilePictureSource> lastResponse = null;
+    APINodeList<BusinessAdAccountRequest> lastResponse = null;
     @Override
-    public APINodeList<ProfilePictureSource> getLastResponse() {
+    public APINodeList<BusinessAdAccountRequest> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "height",
-      "width",
-      "type",
-      "redirect",
     };
 
     public static final String[] FIELDS = {
-      "height",
-      "is_silhouette",
-      "url",
-      "width",
-      "left",
-      "top",
-      "right",
-      "bottom",
+      "ad_account",
+      "id",
+      "permitted_roles",
     };
 
     @Override
-    public APINodeList<ProfilePictureSource> parseResponse(String response) throws APIException {
-      return ProfilePictureSource.parseResponse(response, getContext(), this);
+    public APINodeList<BusinessAdAccountRequest> parseResponse(String response) throws APIException {
+      return BusinessAdAccountRequest.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINodeList<ProfilePictureSource> execute() throws APIException {
+    public APINodeList<BusinessAdAccountRequest> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<ProfilePictureSource> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<BusinessAdAccountRequest> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetPicture(String nodeId, APIContext context) {
-      super(context, nodeId, "/picture", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetOwnedAdAccountRequests(String nodeId, APIContext context) {
+      super(context, nodeId, "/owned_ad_account_requests", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetPicture setParam(String param, Object value) {
+    @Override
+    public APIRequestGetOwnedAdAccountRequests setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetPicture setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetOwnedAdAccountRequests setParams(Map<String, Object> params) {
+      setParamsInternal(params);
+      return this;
+    }
+
+
+    public APIRequestGetOwnedAdAccountRequests requestAllFields () {
+      return this.requestAllFields(true);
+    }
+
+    public APIRequestGetOwnedAdAccountRequests requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestGetOwnedAdAccountRequests requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
+    }
+
+    @Override
+    public APIRequestGetOwnedAdAccountRequests requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestGetOwnedAdAccountRequests requestField (String field) {
+      this.requestField(field, true);
+      return this;
+    }
+
+    @Override
+    public APIRequestGetOwnedAdAccountRequests requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
+      return this;
+    }
+
+    public APIRequestGetOwnedAdAccountRequests requestAdAccountField () {
+      return this.requestAdAccountField(true);
+    }
+    public APIRequestGetOwnedAdAccountRequests requestAdAccountField (boolean value) {
+      this.requestField("ad_account", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccountRequests requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGetOwnedAdAccountRequests requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccountRequests requestPermittedRolesField () {
+      return this.requestPermittedRolesField(true);
+    }
+    public APIRequestGetOwnedAdAccountRequests requestPermittedRolesField (boolean value) {
+      this.requestField("permitted_roles", value);
+      return this;
+    }
+  }
+
+  public static class APIRequestGetOwnedAdAccounts extends APIRequest<AdAccount> {
+
+    APINodeList<AdAccount> lastResponse = null;
+    @Override
+    public APINodeList<AdAccount> getLastResponse() {
+      return lastResponse;
+    }
+    public static final String[] PARAMS = {
+    };
+
+    public static final String[] FIELDS = {
+      "account_groups",
+      "account_id",
+      "account_status",
+      "age",
+      "agency_client_declaration",
+      "amount_spent",
+      "balance",
+      "business",
+      "business_city",
+      "business_country_code",
+      "business_name",
+      "business_state",
+      "business_street",
+      "business_street2",
+      "business_zip",
+      "capabilities",
+      "created_time",
+      "currency",
+      "disable_reason",
+      "end_advertiser",
+      "end_advertiser_name",
+      "failed_delivery_checks",
+      "funding_source",
+      "funding_source_details",
+      "has_migrated_permissions",
+      "id",
+      "io_number",
+      "is_notifications_enabled",
+      "is_personal",
+      "is_prepay_account",
+      "is_tax_id_required",
+      "last_used_time",
+      "line_numbers",
+      "media_agency",
+      "min_campaign_group_spend_cap",
+      "min_daily_budget",
+      "name",
+      "offsite_pixels_tos_accepted",
+      "owner",
+      "owner_business",
+      "partner",
+      "rf_spec",
+      "spend_cap",
+      "tax_id",
+      "tax_id_status",
+      "tax_id_type",
+      "timezone_id",
+      "timezone_name",
+      "timezone_offset_hours_utc",
+      "tos_accepted",
+      "user_role",
+    };
+
+    @Override
+    public APINodeList<AdAccount> parseResponse(String response) throws APIException {
+      return AdAccount.parseResponse(response, getContext(), this);
+    }
+
+    @Override
+    public APINodeList<AdAccount> execute() throws APIException {
+      return execute(new HashMap<String, Object>());
+    }
+
+    @Override
+    public APINodeList<AdAccount> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
+    }
+
+    public APIRequestGetOwnedAdAccounts(String nodeId, APIContext context) {
+      super(context, nodeId, "/owned_ad_accounts", "GET", Arrays.asList(PARAMS));
+    }
+
+    @Override
+    public APIRequestGetOwnedAdAccounts setParam(String param, Object value) {
+      setParamInternal(param, value);
+      return this;
+    }
+
+    @Override
+    public APIRequestGetOwnedAdAccounts setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetPicture setHeight (Long height) {
-      this.setParam("height", height);
-      return this;
+    public APIRequestGetOwnedAdAccounts requestAllFields () {
+      return this.requestAllFields(true);
+    }
+
+    public APIRequestGetOwnedAdAccounts requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestGetOwnedAdAccounts requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
+    }
+
+    @Override
+    public APIRequestGetOwnedAdAccounts requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestGetOwnedAdAccounts requestField (String field) {
+      this.requestField(field, true);
+      return this;
+    }
+
+    @Override
+    public APIRequestGetOwnedAdAccounts requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
+      return this;
+    }
+
+    public APIRequestGetOwnedAdAccounts requestAccountGroupsField () {
+      return this.requestAccountGroupsField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestAccountGroupsField (boolean value) {
+      this.requestField("account_groups", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestAccountIdField () {
+      return this.requestAccountIdField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestAccountIdField (boolean value) {
+      this.requestField("account_id", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestAccountStatusField () {
+      return this.requestAccountStatusField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestAccountStatusField (boolean value) {
+      this.requestField("account_status", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestAgeField () {
+      return this.requestAgeField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestAgeField (boolean value) {
+      this.requestField("age", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestAgencyClientDeclarationField () {
+      return this.requestAgencyClientDeclarationField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestAgencyClientDeclarationField (boolean value) {
+      this.requestField("agency_client_declaration", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestAmountSpentField () {
+      return this.requestAmountSpentField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestAmountSpentField (boolean value) {
+      this.requestField("amount_spent", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestBalanceField () {
+      return this.requestBalanceField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestBalanceField (boolean value) {
+      this.requestField("balance", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestBusinessField () {
+      return this.requestBusinessField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestBusinessField (boolean value) {
+      this.requestField("business", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestBusinessCityField () {
+      return this.requestBusinessCityField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestBusinessCityField (boolean value) {
+      this.requestField("business_city", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestBusinessCountryCodeField () {
+      return this.requestBusinessCountryCodeField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestBusinessCountryCodeField (boolean value) {
+      this.requestField("business_country_code", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestBusinessNameField () {
+      return this.requestBusinessNameField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestBusinessNameField (boolean value) {
+      this.requestField("business_name", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestBusinessStateField () {
+      return this.requestBusinessStateField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestBusinessStateField (boolean value) {
+      this.requestField("business_state", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestBusinessStreetField () {
+      return this.requestBusinessStreetField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestBusinessStreetField (boolean value) {
+      this.requestField("business_street", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestBusinessStreet2Field () {
+      return this.requestBusinessStreet2Field(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestBusinessStreet2Field (boolean value) {
+      this.requestField("business_street2", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestBusinessZipField () {
+      return this.requestBusinessZipField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestBusinessZipField (boolean value) {
+      this.requestField("business_zip", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestCapabilitiesField () {
+      return this.requestCapabilitiesField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestCapabilitiesField (boolean value) {
+      this.requestField("capabilities", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestCreatedTimeField () {
+      return this.requestCreatedTimeField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestCreatedTimeField (boolean value) {
+      this.requestField("created_time", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestCurrencyField () {
+      return this.requestCurrencyField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestCurrencyField (boolean value) {
+      this.requestField("currency", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestDisableReasonField () {
+      return this.requestDisableReasonField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestDisableReasonField (boolean value) {
+      this.requestField("disable_reason", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestEndAdvertiserField () {
+      return this.requestEndAdvertiserField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestEndAdvertiserField (boolean value) {
+      this.requestField("end_advertiser", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestEndAdvertiserNameField () {
+      return this.requestEndAdvertiserNameField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestEndAdvertiserNameField (boolean value) {
+      this.requestField("end_advertiser_name", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestFailedDeliveryChecksField () {
+      return this.requestFailedDeliveryChecksField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestFailedDeliveryChecksField (boolean value) {
+      this.requestField("failed_delivery_checks", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestFundingSourceField () {
+      return this.requestFundingSourceField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestFundingSourceField (boolean value) {
+      this.requestField("funding_source", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestFundingSourceDetailsField () {
+      return this.requestFundingSourceDetailsField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestFundingSourceDetailsField (boolean value) {
+      this.requestField("funding_source_details", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestHasMigratedPermissionsField () {
+      return this.requestHasMigratedPermissionsField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestHasMigratedPermissionsField (boolean value) {
+      this.requestField("has_migrated_permissions", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestIoNumberField () {
+      return this.requestIoNumberField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestIoNumberField (boolean value) {
+      this.requestField("io_number", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestIsNotificationsEnabledField () {
+      return this.requestIsNotificationsEnabledField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestIsNotificationsEnabledField (boolean value) {
+      this.requestField("is_notifications_enabled", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestIsPersonalField () {
+      return this.requestIsPersonalField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestIsPersonalField (boolean value) {
+      this.requestField("is_personal", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestIsPrepayAccountField () {
+      return this.requestIsPrepayAccountField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestIsPrepayAccountField (boolean value) {
+      this.requestField("is_prepay_account", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestIsTaxIdRequiredField () {
+      return this.requestIsTaxIdRequiredField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestIsTaxIdRequiredField (boolean value) {
+      this.requestField("is_tax_id_required", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestLastUsedTimeField () {
+      return this.requestLastUsedTimeField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestLastUsedTimeField (boolean value) {
+      this.requestField("last_used_time", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestLineNumbersField () {
+      return this.requestLineNumbersField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestLineNumbersField (boolean value) {
+      this.requestField("line_numbers", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestMediaAgencyField () {
+      return this.requestMediaAgencyField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestMediaAgencyField (boolean value) {
+      this.requestField("media_agency", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestMinCampaignGroupSpendCapField () {
+      return this.requestMinCampaignGroupSpendCapField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestMinCampaignGroupSpendCapField (boolean value) {
+      this.requestField("min_campaign_group_spend_cap", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestMinDailyBudgetField () {
+      return this.requestMinDailyBudgetField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestMinDailyBudgetField (boolean value) {
+      this.requestField("min_daily_budget", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestNameField () {
+      return this.requestNameField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestNameField (boolean value) {
+      this.requestField("name", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestOffsitePixelsTosAcceptedField () {
+      return this.requestOffsitePixelsTosAcceptedField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestOffsitePixelsTosAcceptedField (boolean value) {
+      this.requestField("offsite_pixels_tos_accepted", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestOwnerField () {
+      return this.requestOwnerField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestOwnerField (boolean value) {
+      this.requestField("owner", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestOwnerBusinessField () {
+      return this.requestOwnerBusinessField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestOwnerBusinessField (boolean value) {
+      this.requestField("owner_business", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestPartnerField () {
+      return this.requestPartnerField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestPartnerField (boolean value) {
+      this.requestField("partner", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestRfSpecField () {
+      return this.requestRfSpecField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestRfSpecField (boolean value) {
+      this.requestField("rf_spec", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestSpendCapField () {
+      return this.requestSpendCapField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestSpendCapField (boolean value) {
+      this.requestField("spend_cap", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestTaxIdField () {
+      return this.requestTaxIdField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestTaxIdField (boolean value) {
+      this.requestField("tax_id", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestTaxIdStatusField () {
+      return this.requestTaxIdStatusField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestTaxIdStatusField (boolean value) {
+      this.requestField("tax_id_status", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestTaxIdTypeField () {
+      return this.requestTaxIdTypeField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestTaxIdTypeField (boolean value) {
+      this.requestField("tax_id_type", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestTimezoneIdField () {
+      return this.requestTimezoneIdField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestTimezoneIdField (boolean value) {
+      this.requestField("timezone_id", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestTimezoneNameField () {
+      return this.requestTimezoneNameField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestTimezoneNameField (boolean value) {
+      this.requestField("timezone_name", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestTimezoneOffsetHoursUtcField () {
+      return this.requestTimezoneOffsetHoursUtcField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestTimezoneOffsetHoursUtcField (boolean value) {
+      this.requestField("timezone_offset_hours_utc", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestTosAcceptedField () {
+      return this.requestTosAcceptedField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestTosAcceptedField (boolean value) {
+      this.requestField("tos_accepted", value);
+      return this;
+    }
+    public APIRequestGetOwnedAdAccounts requestUserRoleField () {
+      return this.requestUserRoleField(true);
+    }
+    public APIRequestGetOwnedAdAccounts requestUserRoleField (boolean value) {
+      this.requestField("user_role", value);
+      return this;
+    }
+  }
+
+  public static class APIRequestGetOwnedInstagramAccounts extends APIRequest<APINode> {
+
+    APINodeList<APINode> lastResponse = null;
+    @Override
+    public APINodeList<APINode> getLastResponse() {
+      return lastResponse;
     }
+    public static final String[] PARAMS = {
+    };
 
-    public APIRequestGetPicture setHeight (String height) {
-      this.setParam("height", height);
-      return this;
-    }
+    public static final String[] FIELDS = {
+    };
 
-    public APIRequestGetPicture setWidth (Long width) {
-      this.setParam("width", width);
-      return this;
+    @Override
+    public APINodeList<APINode> parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this);
     }
 
-    public APIRequestGetPicture setWidth (String width) {
-      this.setParam("width", width);
-      return this;
+    @Override
+    public APINodeList<APINode> execute() throws APIException {
+      return execute(new HashMap<String, Object>());
     }
 
-    public APIRequestGetPicture setType (EnumType type) {
-      this.setParam("type", type);
-      return this;
+    @Override
+    public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
     }
 
-    public APIRequestGetPicture setType (String type) {
-      this.setParam("type", type);
-      return this;
+    public APIRequestGetOwnedInstagramAccounts(String nodeId, APIContext context) {
+      super(context, nodeId, "/owned_instagram_accounts", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetPicture setRedirect (Boolean redirect) {
-      this.setParam("redirect", redirect);
+    @Override
+    public APIRequestGetOwnedInstagramAccounts setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetPicture setRedirect (String redirect) {
-      this.setParam("redirect", redirect);
+    @Override
+    public APIRequestGetOwnedInstagramAccounts setParams(Map<String, Object> params) {
+      setParamsInternal(params);
       return this;
     }
 
-    public APIRequestGetPicture requestAllFields () {
+
+    public APIRequestGetOwnedInstagramAccounts requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetPicture requestAllFields (boolean value) {
+    public APIRequestGetOwnedInstagramAccounts requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetPicture requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetOwnedInstagramAccounts requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetPicture requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetOwnedInstagramAccounts requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetPicture requestField (String field) {
+    @Override
+    public APIRequestGetOwnedInstagramAccounts requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetPicture requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetOwnedInstagramAccounts requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetPicture requestHeightField () {
-      return this.requestHeightField(true);
-    }
-    public APIRequestGetPicture requestHeightField (boolean value) {
-      this.requestField("height", value);
-      return this;
-    }
-    public APIRequestGetPicture requestIsSilhouetteField () {
-      return this.requestIsSilhouetteField(true);
-    }
-    public APIRequestGetPicture requestIsSilhouetteField (boolean value) {
-      this.requestField("is_silhouette", value);
-      return this;
-    }
-    public APIRequestGetPicture requestUrlField () {
-      return this.requestUrlField(true);
-    }
-    public APIRequestGetPicture requestUrlField (boolean value) {
-      this.requestField("url", value);
-      return this;
-    }
-    public APIRequestGetPicture requestWidthField () {
-      return this.requestWidthField(true);
-    }
-    public APIRequestGetPicture requestWidthField (boolean value) {
-      this.requestField("width", value);
-      return this;
-    }
-    public APIRequestGetPicture requestLeftField () {
-      return this.requestLeftField(true);
-    }
-    public APIRequestGetPicture requestLeftField (boolean value) {
-      this.requestField("left", value);
-      return this;
-    }
-    public APIRequestGetPicture requestTopField () {
-      return this.requestTopField(true);
-    }
-    public APIRequestGetPicture requestTopField (boolean value) {
-      this.requestField("top", value);
-      return this;
-    }
-    public APIRequestGetPicture requestRightField () {
-      return this.requestRightField(true);
-    }
-    public APIRequestGetPicture requestRightField (boolean value) {
-      this.requestField("right", value);
-      return this;
-    }
-    public APIRequestGetPicture requestBottomField () {
-      return this.requestBottomField(true);
-    }
-    public APIRequestGetPicture requestBottomField (boolean value) {
-      this.requestField("bottom", value);
-      return this;
-    }
-
   }
 
-  public static class APIRequestGetInstagramAccounts extends APIRequest<APINode> {
+  public static class APIRequestGetOwnedPageRequests extends APIRequest<BusinessPageRequest> {
 
-    APINodeList<APINode> lastResponse = null;
+    APINodeList<BusinessPageRequest> lastResponse = null;
     @Override
-    public APINodeList<APINode> getLastResponse() {
+    public APINodeList<BusinessPageRequest> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
     };
 
     public static final String[] FIELDS = {
+      "id",
+      "page",
     };
 
     @Override
-    public APINodeList<APINode> parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this);
+    public APINodeList<BusinessPageRequest> parseResponse(String response) throws APIException {
+      return BusinessPageRequest.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINodeList<APINode> execute() throws APIException {
+    public APINodeList<BusinessPageRequest> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<BusinessPageRequest> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetInstagramAccounts(String nodeId, APIContext context) {
-      super(context, nodeId, "/instagram_accounts", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetOwnedPageRequests(String nodeId, APIContext context) {
+      super(context, nodeId, "/owned_page_requests", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetInstagramAccounts setParam(String param, Object value) {
+    @Override
+    public APIRequestGetOwnedPageRequests setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetInstagramAccounts setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetOwnedPageRequests setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetInstagramAccounts requestAllFields () {
+    public APIRequestGetOwnedPageRequests requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetInstagramAccounts requestAllFields (boolean value) {
+    public APIRequestGetOwnedPageRequests requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetInstagramAccounts requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetOwnedPageRequests requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetInstagramAccounts requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetOwnedPageRequests requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetInstagramAccounts requestField (String field) {
+    @Override
+    public APIRequestGetOwnedPageRequests requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetInstagramAccounts requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetOwnedPageRequests requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
+    public APIRequestGetOwnedPageRequests requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGetOwnedPageRequests requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
+    public APIRequestGetOwnedPageRequests requestPageField () {
+      return this.requestPageField(true);
+    }
+    public APIRequestGetOwnedPageRequests requestPageField (boolean value) {
+      this.requestField("page", value);
+      return this;
+    }
   }
 
-  public static class APIRequestGetOwnedInstagramAccounts extends APIRequest<APINode> {
+  public static class APIRequestGetOwnedPages extends APIRequest<APINode> {
 
     APINodeList<APINode> lastResponse = null;
     @Override
@@ -3478,380 +4081,515 @@ public class Business extends APINode {
 
     @Override
     public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetOwnedInstagramAccounts(String nodeId, APIContext context) {
-      super(context, nodeId, "/owned_instagram_accounts", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetOwnedPages(String nodeId, APIContext context) {
+      super(context, nodeId, "/owned_pages", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetOwnedInstagramAccounts setParam(String param, Object value) {
+    @Override
+    public APIRequestGetOwnedPages setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetOwnedInstagramAccounts setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetOwnedPages setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetOwnedInstagramAccounts requestAllFields () {
+    public APIRequestGetOwnedPages requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetOwnedInstagramAccounts requestAllFields (boolean value) {
+    public APIRequestGetOwnedPages requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetOwnedInstagramAccounts requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetOwnedPages requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetOwnedInstagramAccounts requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetOwnedPages requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetOwnedInstagramAccounts requestField (String field) {
+    @Override
+    public APIRequestGetOwnedPages requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetOwnedInstagramAccounts requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetOwnedPages requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
   }
 
-  public static class APIRequestGetGrpPlans extends APIRequest<ReachFrequencyPrediction> {
+  public static class APIRequestGetOwnedPixels extends APIRequest<AdsPixel> {
 
-    APINodeList<ReachFrequencyPrediction> lastResponse = null;
+    APINodeList<AdsPixel> lastResponse = null;
     @Override
-    public APINodeList<ReachFrequencyPrediction> getLastResponse() {
+    public APINodeList<AdsPixel> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "status",
     };
 
     public static final String[] FIELDS = {
+      "code",
+      "creation_time",
       "id",
-      "campaign_id",
-      "account_id",
-      "time_created",
-      "frequency_cap",
-      "expiration_time",
-      "external_reach",
-      "external_budget",
-      "external_impression",
-      "external_maximum_reach",
-      "external_maximum_impression",
-      "external_maximum_budget",
-      "target_spec",
-      "target_audience_size",
-      "prediction_mode",
-      "prediction_progress",
-      "time_updated",
-      "status",
-      "campaign_time_start",
-      "campaign_time_stop",
-      "external_minimum_budget",
-      "external_minimum_reach",
-      "external_minimum_impression",
-      "reservation_status",
-      "story_event_type",
-      "curve_budget_reach",
-      "holdout_percentage",
-      "campaign_group_id",
+      "last_fired_time",
       "name",
+      "owner_ad_account",
+      "owner_business",
     };
 
     @Override
-    public APINodeList<ReachFrequencyPrediction> parseResponse(String response) throws APIException {
-      return ReachFrequencyPrediction.parseResponse(response, getContext(), this);
+    public APINodeList<AdsPixel> parseResponse(String response) throws APIException {
+      return AdsPixel.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINodeList<ReachFrequencyPrediction> execute() throws APIException {
+    public APINodeList<AdsPixel> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<ReachFrequencyPrediction> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<AdsPixel> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetGrpPlans(String nodeId, APIContext context) {
-      super(context, nodeId, "/grp_plans", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetOwnedPixels(String nodeId, APIContext context) {
+      super(context, nodeId, "/owned_pixels", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetGrpPlans setParam(String param, Object value) {
+    @Override
+    public APIRequestGetOwnedPixels setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetGrpPlans setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetOwnedPixels setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetGrpPlans setStatus (EnumStatus status) {
-      this.setParam("status", status);
-      return this;
-    }
-
-    public APIRequestGetGrpPlans setStatus (String status) {
-      this.setParam("status", status);
-      return this;
-    }
-
-    public APIRequestGetGrpPlans requestAllFields () {
+    public APIRequestGetOwnedPixels requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetGrpPlans requestAllFields (boolean value) {
+    public APIRequestGetOwnedPixels requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetGrpPlans requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetOwnedPixels requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetGrpPlans requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetOwnedPixels requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetGrpPlans requestField (String field) {
+    @Override
+    public APIRequestGetOwnedPixels requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetGrpPlans requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetOwnedPixels requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetGrpPlans requestIdField () {
+    public APIRequestGetOwnedPixels requestCodeField () {
+      return this.requestCodeField(true);
+    }
+    public APIRequestGetOwnedPixels requestCodeField (boolean value) {
+      this.requestField("code", value);
+      return this;
+    }
+    public APIRequestGetOwnedPixels requestCreationTimeField () {
+      return this.requestCreationTimeField(true);
+    }
+    public APIRequestGetOwnedPixels requestCreationTimeField (boolean value) {
+      this.requestField("creation_time", value);
+      return this;
+    }
+    public APIRequestGetOwnedPixels requestIdField () {
       return this.requestIdField(true);
     }
-    public APIRequestGetGrpPlans requestIdField (boolean value) {
+    public APIRequestGetOwnedPixels requestIdField (boolean value) {
       this.requestField("id", value);
       return this;
     }
-    public APIRequestGetGrpPlans requestCampaignIdField () {
-      return this.requestCampaignIdField(true);
+    public APIRequestGetOwnedPixels requestLastFiredTimeField () {
+      return this.requestLastFiredTimeField(true);
     }
-    public APIRequestGetGrpPlans requestCampaignIdField (boolean value) {
-      this.requestField("campaign_id", value);
+    public APIRequestGetOwnedPixels requestLastFiredTimeField (boolean value) {
+      this.requestField("last_fired_time", value);
       return this;
     }
-    public APIRequestGetGrpPlans requestAccountIdField () {
-      return this.requestAccountIdField(true);
+    public APIRequestGetOwnedPixels requestNameField () {
+      return this.requestNameField(true);
     }
-    public APIRequestGetGrpPlans requestAccountIdField (boolean value) {
-      this.requestField("account_id", value);
+    public APIRequestGetOwnedPixels requestNameField (boolean value) {
+      this.requestField("name", value);
       return this;
     }
-    public APIRequestGetGrpPlans requestTimeCreatedField () {
-      return this.requestTimeCreatedField(true);
+    public APIRequestGetOwnedPixels requestOwnerAdAccountField () {
+      return this.requestOwnerAdAccountField(true);
     }
-    public APIRequestGetGrpPlans requestTimeCreatedField (boolean value) {
-      this.requestField("time_created", value);
+    public APIRequestGetOwnedPixels requestOwnerAdAccountField (boolean value) {
+      this.requestField("owner_ad_account", value);
       return this;
     }
-    public APIRequestGetGrpPlans requestFrequencyCapField () {
-      return this.requestFrequencyCapField(true);
+    public APIRequestGetOwnedPixels requestOwnerBusinessField () {
+      return this.requestOwnerBusinessField(true);
     }
-    public APIRequestGetGrpPlans requestFrequencyCapField (boolean value) {
-      this.requestField("frequency_cap", value);
+    public APIRequestGetOwnedPixels requestOwnerBusinessField (boolean value) {
+      this.requestField("owner_business", value);
       return this;
     }
-    public APIRequestGetGrpPlans requestExpirationTimeField () {
-      return this.requestExpirationTimeField(true);
+  }
+
+  public static class APIRequestGetPicture extends APIRequest<ProfilePictureSource> {
+
+    APINodeList<ProfilePictureSource> lastResponse = null;
+    @Override
+    public APINodeList<ProfilePictureSource> getLastResponse() {
+      return lastResponse;
     }
-    public APIRequestGetGrpPlans requestExpirationTimeField (boolean value) {
-      this.requestField("expiration_time", value);
+    public static final String[] PARAMS = {
+      "height",
+      "redirect",
+      "type",
+      "width",
+    };
+
+    public static final String[] FIELDS = {
+      "bottom",
+      "height",
+      "is_silhouette",
+      "left",
+      "right",
+      "top",
+      "url",
+      "width",
+    };
+
+    @Override
+    public APINodeList<ProfilePictureSource> parseResponse(String response) throws APIException {
+      return ProfilePictureSource.parseResponse(response, getContext(), this);
+    }
+
+    @Override
+    public APINodeList<ProfilePictureSource> execute() throws APIException {
+      return execute(new HashMap<String, Object>());
+    }
+
+    @Override
+    public APINodeList<ProfilePictureSource> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
+    }
+
+    public APIRequestGetPicture(String nodeId, APIContext context) {
+      super(context, nodeId, "/picture", "GET", Arrays.asList(PARAMS));
+    }
+
+    @Override
+    public APIRequestGetPicture setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
-    public APIRequestGetGrpPlans requestExternalReachField () {
-      return this.requestExternalReachField(true);
+
+    @Override
+    public APIRequestGetPicture setParams(Map<String, Object> params) {
+      setParamsInternal(params);
+      return this;
     }
-    public APIRequestGetGrpPlans requestExternalReachField (boolean value) {
-      this.requestField("external_reach", value);
+
+
+    public APIRequestGetPicture setHeight (Long height) {
+      this.setParam("height", height);
       return this;
     }
-    public APIRequestGetGrpPlans requestExternalBudgetField () {
-      return this.requestExternalBudgetField(true);
+    public APIRequestGetPicture setHeight (String height) {
+      this.setParam("height", height);
+      return this;
     }
-    public APIRequestGetGrpPlans requestExternalBudgetField (boolean value) {
-      this.requestField("external_budget", value);
+
+    public APIRequestGetPicture setRedirect (Boolean redirect) {
+      this.setParam("redirect", redirect);
       return this;
     }
-    public APIRequestGetGrpPlans requestExternalImpressionField () {
-      return this.requestExternalImpressionField(true);
+    public APIRequestGetPicture setRedirect (String redirect) {
+      this.setParam("redirect", redirect);
+      return this;
     }
-    public APIRequestGetGrpPlans requestExternalImpressionField (boolean value) {
-      this.requestField("external_impression", value);
+
+    public APIRequestGetPicture setType (ProfilePictureSource.EnumType type) {
+      this.setParam("type", type);
       return this;
     }
-    public APIRequestGetGrpPlans requestExternalMaximumReachField () {
-      return this.requestExternalMaximumReachField(true);
+    public APIRequestGetPicture setType (String type) {
+      this.setParam("type", type);
+      return this;
     }
-    public APIRequestGetGrpPlans requestExternalMaximumReachField (boolean value) {
-      this.requestField("external_maximum_reach", value);
+
+    public APIRequestGetPicture setWidth (Long width) {
+      this.setParam("width", width);
       return this;
     }
-    public APIRequestGetGrpPlans requestExternalMaximumImpressionField () {
-      return this.requestExternalMaximumImpressionField(true);
+    public APIRequestGetPicture setWidth (String width) {
+      this.setParam("width", width);
+      return this;
     }
-    public APIRequestGetGrpPlans requestExternalMaximumImpressionField (boolean value) {
-      this.requestField("external_maximum_impression", value);
+
+    public APIRequestGetPicture requestAllFields () {
+      return this.requestAllFields(true);
+    }
+
+    public APIRequestGetPicture requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetGrpPlans requestExternalMaximumBudgetField () {
-      return this.requestExternalMaximumBudgetField(true);
+
+    @Override
+    public APIRequestGetPicture requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
-    public APIRequestGetGrpPlans requestExternalMaximumBudgetField (boolean value) {
-      this.requestField("external_maximum_budget", value);
+
+    @Override
+    public APIRequestGetPicture requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetGrpPlans requestTargetSpecField () {
-      return this.requestTargetSpecField(true);
+
+    @Override
+    public APIRequestGetPicture requestField (String field) {
+      this.requestField(field, true);
+      return this;
     }
-    public APIRequestGetGrpPlans requestTargetSpecField (boolean value) {
-      this.requestField("target_spec", value);
+
+    @Override
+    public APIRequestGetPicture requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
-    public APIRequestGetGrpPlans requestTargetAudienceSizeField () {
-      return this.requestTargetAudienceSizeField(true);
+
+    public APIRequestGetPicture requestBottomField () {
+      return this.requestBottomField(true);
     }
-    public APIRequestGetGrpPlans requestTargetAudienceSizeField (boolean value) {
-      this.requestField("target_audience_size", value);
+    public APIRequestGetPicture requestBottomField (boolean value) {
+      this.requestField("bottom", value);
       return this;
     }
-    public APIRequestGetGrpPlans requestPredictionModeField () {
-      return this.requestPredictionModeField(true);
+    public APIRequestGetPicture requestHeightField () {
+      return this.requestHeightField(true);
     }
-    public APIRequestGetGrpPlans requestPredictionModeField (boolean value) {
-      this.requestField("prediction_mode", value);
+    public APIRequestGetPicture requestHeightField (boolean value) {
+      this.requestField("height", value);
+      return this;
+    }
+    public APIRequestGetPicture requestIsSilhouetteField () {
+      return this.requestIsSilhouetteField(true);
+    }
+    public APIRequestGetPicture requestIsSilhouetteField (boolean value) {
+      this.requestField("is_silhouette", value);
+      return this;
+    }
+    public APIRequestGetPicture requestLeftField () {
+      return this.requestLeftField(true);
+    }
+    public APIRequestGetPicture requestLeftField (boolean value) {
+      this.requestField("left", value);
       return this;
     }
-    public APIRequestGetGrpPlans requestPredictionProgressField () {
-      return this.requestPredictionProgressField(true);
+    public APIRequestGetPicture requestRightField () {
+      return this.requestRightField(true);
     }
-    public APIRequestGetGrpPlans requestPredictionProgressField (boolean value) {
-      this.requestField("prediction_progress", value);
+    public APIRequestGetPicture requestRightField (boolean value) {
+      this.requestField("right", value);
       return this;
     }
-    public APIRequestGetGrpPlans requestTimeUpdatedField () {
-      return this.requestTimeUpdatedField(true);
+    public APIRequestGetPicture requestTopField () {
+      return this.requestTopField(true);
     }
-    public APIRequestGetGrpPlans requestTimeUpdatedField (boolean value) {
-      this.requestField("time_updated", value);
+    public APIRequestGetPicture requestTopField (boolean value) {
+      this.requestField("top", value);
       return this;
     }
-    public APIRequestGetGrpPlans requestStatusField () {
-      return this.requestStatusField(true);
+    public APIRequestGetPicture requestUrlField () {
+      return this.requestUrlField(true);
     }
-    public APIRequestGetGrpPlans requestStatusField (boolean value) {
-      this.requestField("status", value);
+    public APIRequestGetPicture requestUrlField (boolean value) {
+      this.requestField("url", value);
       return this;
     }
-    public APIRequestGetGrpPlans requestCampaignTimeStartField () {
-      return this.requestCampaignTimeStartField(true);
+    public APIRequestGetPicture requestWidthField () {
+      return this.requestWidthField(true);
     }
-    public APIRequestGetGrpPlans requestCampaignTimeStartField (boolean value) {
-      this.requestField("campaign_time_start", value);
+    public APIRequestGetPicture requestWidthField (boolean value) {
+      this.requestField("width", value);
       return this;
     }
-    public APIRequestGetGrpPlans requestCampaignTimeStopField () {
-      return this.requestCampaignTimeStopField(true);
+  }
+
+  public static class APIRequestGetProductCatalogs extends APIRequest<ProductCatalog> {
+
+    APINodeList<ProductCatalog> lastResponse = null;
+    @Override
+    public APINodeList<ProductCatalog> getLastResponse() {
+      return lastResponse;
     }
-    public APIRequestGetGrpPlans requestCampaignTimeStopField (boolean value) {
-      this.requestField("campaign_time_stop", value);
-      return this;
+    public static final String[] PARAMS = {
+    };
+
+    public static final String[] FIELDS = {
+      "business",
+      "feed_count",
+      "id",
+      "name",
+      "product_count",
+    };
+
+    @Override
+    public APINodeList<ProductCatalog> parseResponse(String response) throws APIException {
+      return ProductCatalog.parseResponse(response, getContext(), this);
     }
-    public APIRequestGetGrpPlans requestExternalMinimumBudgetField () {
-      return this.requestExternalMinimumBudgetField(true);
+
+    @Override
+    public APINodeList<ProductCatalog> execute() throws APIException {
+      return execute(new HashMap<String, Object>());
     }
-    public APIRequestGetGrpPlans requestExternalMinimumBudgetField (boolean value) {
-      this.requestField("external_minimum_budget", value);
-      return this;
+
+    @Override
+    public APINodeList<ProductCatalog> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
     }
-    public APIRequestGetGrpPlans requestExternalMinimumReachField () {
-      return this.requestExternalMinimumReachField(true);
+
+    public APIRequestGetProductCatalogs(String nodeId, APIContext context) {
+      super(context, nodeId, "/product_catalogs", "GET", Arrays.asList(PARAMS));
     }
-    public APIRequestGetGrpPlans requestExternalMinimumReachField (boolean value) {
-      this.requestField("external_minimum_reach", value);
+
+    @Override
+    public APIRequestGetProductCatalogs setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
-    public APIRequestGetGrpPlans requestExternalMinimumImpressionField () {
-      return this.requestExternalMinimumImpressionField(true);
+
+    @Override
+    public APIRequestGetProductCatalogs setParams(Map<String, Object> params) {
+      setParamsInternal(params);
+      return this;
     }
-    public APIRequestGetGrpPlans requestExternalMinimumImpressionField (boolean value) {
-      this.requestField("external_minimum_impression", value);
+
+
+    public APIRequestGetProductCatalogs requestAllFields () {
+      return this.requestAllFields(true);
+    }
+
+    public APIRequestGetProductCatalogs requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetGrpPlans requestReservationStatusField () {
-      return this.requestReservationStatusField(true);
+
+    @Override
+    public APIRequestGetProductCatalogs requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
-    public APIRequestGetGrpPlans requestReservationStatusField (boolean value) {
-      this.requestField("reservation_status", value);
+
+    @Override
+    public APIRequestGetProductCatalogs requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetGrpPlans requestStoryEventTypeField () {
-      return this.requestStoryEventTypeField(true);
+
+    @Override
+    public APIRequestGetProductCatalogs requestField (String field) {
+      this.requestField(field, true);
+      return this;
     }
-    public APIRequestGetGrpPlans requestStoryEventTypeField (boolean value) {
-      this.requestField("story_event_type", value);
+
+    @Override
+    public APIRequestGetProductCatalogs requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
-    public APIRequestGetGrpPlans requestCurveBudgetReachField () {
-      return this.requestCurveBudgetReachField(true);
+
+    public APIRequestGetProductCatalogs requestBusinessField () {
+      return this.requestBusinessField(true);
     }
-    public APIRequestGetGrpPlans requestCurveBudgetReachField (boolean value) {
-      this.requestField("curve_budget_reach", value);
+    public APIRequestGetProductCatalogs requestBusinessField (boolean value) {
+      this.requestField("business", value);
       return this;
     }
-    public APIRequestGetGrpPlans requestHoldoutPercentageField () {
-      return this.requestHoldoutPercentageField(true);
+    public APIRequestGetProductCatalogs requestFeedCountField () {
+      return this.requestFeedCountField(true);
     }
-    public APIRequestGetGrpPlans requestHoldoutPercentageField (boolean value) {
-      this.requestField("holdout_percentage", value);
+    public APIRequestGetProductCatalogs requestFeedCountField (boolean value) {
+      this.requestField("feed_count", value);
       return this;
     }
-    public APIRequestGetGrpPlans requestCampaignGroupIdField () {
-      return this.requestCampaignGroupIdField(true);
+    public APIRequestGetProductCatalogs requestIdField () {
+      return this.requestIdField(true);
     }
-    public APIRequestGetGrpPlans requestCampaignGroupIdField (boolean value) {
-      this.requestField("campaign_group_id", value);
+    public APIRequestGetProductCatalogs requestIdField (boolean value) {
+      this.requestField("id", value);
       return this;
     }
-    public APIRequestGetGrpPlans requestNameField () {
+    public APIRequestGetProductCatalogs requestNameField () {
       return this.requestNameField(true);
     }
-    public APIRequestGetGrpPlans requestNameField (boolean value) {
+    public APIRequestGetProductCatalogs requestNameField (boolean value) {
       this.requestField("name", value);
       return this;
     }
-
+    public APIRequestGetProductCatalogs requestProductCountField () {
+      return this.requestProductCountField(true);
+    }
+    public APIRequestGetProductCatalogs requestProductCountField (boolean value) {
+      this.requestField("product_count", value);
+      return this;
+    }
   }
 
   public static class APIRequestCreateProductCatalog extends APIRequest<ProductCatalog> {
@@ -3881,7 +4619,7 @@ public class Business extends APINode {
 
     @Override
     public ProductCatalog execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -3889,11 +4627,13 @@ public class Business extends APINode {
       super(context, nodeId, "/product_catalogs", "POST", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestCreateProductCatalog setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestCreateProductCatalog setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
@@ -3905,13 +4645,11 @@ public class Business extends APINode {
       return this;
     }
 
-
     public APIRequestCreateProductCatalog setName (String name) {
       this.setParam("name", name);
       return this;
     }
 
-
     public APIRequestCreateProductCatalog requestAllFields () {
       return this.requestAllFields(true);
     }
@@ -3923,10 +4661,12 @@ public class Business extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestCreateProductCatalog requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestCreateProductCatalog requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -3934,188 +4674,201 @@ public class Business extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestCreateProductCatalog requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestCreateProductCatalog requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
   }
 
-  public static class APIRequestCreateAdAccount extends APIRequest<APINode> {
+  public static class APIRequestGetReceivedAudiencePermissions extends APIRequest<APINode> {
 
-    APINode lastResponse = null;
+    APINodeList<APINode> lastResponse = null;
     @Override
-    public APINode getLastResponse() {
+    public APINodeList<APINode> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "id",
-      "name",
-      "currency",
-      "timezone_id",
-      "end_advertiser",
-      "funding_id",
-      "media_agency",
-      "partner",
-      "invoice",
-      "po_number",
-      "io",
+      "partner_id",
     };
 
     public static final String[] FIELDS = {
     };
 
     @Override
-    public APINode parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this).head();
+    public APINodeList<APINode> parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINode execute() throws APIException {
+    public APINodeList<APINode> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestCreateAdAccount(String nodeId, APIContext context) {
-      super(context, nodeId, "/adaccount", "POST", Arrays.asList(PARAMS));
+    public APIRequestGetReceivedAudiencePermissions(String nodeId, APIContext context) {
+      super(context, nodeId, "/received_audience_permissions", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestCreateAdAccount setParam(String param, Object value) {
+    @Override
+    public APIRequestGetReceivedAudiencePermissions setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestCreateAdAccount setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetReceivedAudiencePermissions setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestCreateAdAccount setId (String id) {
-      this.setParam("id", id);
+    public APIRequestGetReceivedAudiencePermissions setPartnerId (String partnerId) {
+      this.setParam("partner_id", partnerId);
       return this;
     }
 
-
-    public APIRequestCreateAdAccount setName (String name) {
-      this.setParam("name", name);
-      return this;
+    public APIRequestGetReceivedAudiencePermissions requestAllFields () {
+      return this.requestAllFields(true);
     }
 
-
-    public APIRequestCreateAdAccount setCurrency (String currency) {
-      this.setParam("currency", currency);
+    public APIRequestGetReceivedAudiencePermissions requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
 
-
-    public APIRequestCreateAdAccount setTimezoneId (Long timezoneId) {
-      this.setParam("timezone_id", timezoneId);
-      return this;
+    @Override
+    public APIRequestGetReceivedAudiencePermissions requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
 
-    public APIRequestCreateAdAccount setTimezoneId (String timezoneId) {
-      this.setParam("timezone_id", timezoneId);
+    @Override
+    public APIRequestGetReceivedAudiencePermissions requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
 
-    public APIRequestCreateAdAccount setEndAdvertiser (String endAdvertiser) {
-      this.setParam("end_advertiser", endAdvertiser);
+    @Override
+    public APIRequestGetReceivedAudiencePermissions requestField (String field) {
+      this.requestField(field, true);
       return this;
     }
 
-
-    public APIRequestCreateAdAccount setFundingId (String fundingId) {
-      this.setParam("funding_id", fundingId);
+    @Override
+    public APIRequestGetReceivedAudiencePermissions requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
 
+  }
 
-    public APIRequestCreateAdAccount setMediaAgency (String mediaAgency) {
-      this.setParam("media_agency", mediaAgency);
-      return this;
+  public static class APIRequestGetSharedAudiencePermissions extends APIRequest<APINode> {
+
+    APINodeList<APINode> lastResponse = null;
+    @Override
+    public APINodeList<APINode> getLastResponse() {
+      return lastResponse;
     }
+    public static final String[] PARAMS = {
+      "partner_id",
+    };
 
+    public static final String[] FIELDS = {
+    };
 
-    public APIRequestCreateAdAccount setPartner (String partner) {
-      this.setParam("partner", partner);
-      return this;
+    @Override
+    public APINodeList<APINode> parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this);
     }
 
+    @Override
+    public APINodeList<APINode> execute() throws APIException {
+      return execute(new HashMap<String, Object>());
+    }
 
-    public APIRequestCreateAdAccount setInvoice (Boolean invoice) {
-      this.setParam("invoice", invoice);
-      return this;
+    @Override
+    public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
     }
 
-    public APIRequestCreateAdAccount setInvoice (String invoice) {
-      this.setParam("invoice", invoice);
-      return this;
+    public APIRequestGetSharedAudiencePermissions(String nodeId, APIContext context) {
+      super(context, nodeId, "/shared_audience_permissions", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestCreateAdAccount setPoNumber (String poNumber) {
-      this.setParam("po_number", poNumber);
+    @Override
+    public APIRequestGetSharedAudiencePermissions setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
 
-
-    public APIRequestCreateAdAccount setIo (Boolean io) {
-      this.setParam("io", io);
+    @Override
+    public APIRequestGetSharedAudiencePermissions setParams(Map<String, Object> params) {
+      setParamsInternal(params);
       return this;
     }
 
-    public APIRequestCreateAdAccount setIo (String io) {
-      this.setParam("io", io);
+
+    public APIRequestGetSharedAudiencePermissions setPartnerId (String partnerId) {
+      this.setParam("partner_id", partnerId);
       return this;
     }
 
-    public APIRequestCreateAdAccount requestAllFields () {
+    public APIRequestGetSharedAudiencePermissions requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestCreateAdAccount requestAllFields (boolean value) {
+    public APIRequestGetSharedAudiencePermissions requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestCreateAdAccount requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetSharedAudiencePermissions requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestCreateAdAccount requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetSharedAudiencePermissions requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestCreateAdAccount requestField (String field) {
+    @Override
+    public APIRequestGetSharedAudiencePermissions requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestCreateAdAccount requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetSharedAudiencePermissions requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
   }
 
-  public static class APIRequestDeleteApps extends APIRequest<APINode> {
+  public static class APIRequestGetSystemUsers extends APIRequest<APINode> {
 
     APINodeList<APINode> lastResponse = null;
     @Override
@@ -4123,8 +4876,6 @@ public class Business extends APINode {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "id",
-      "app_id",
     };
 
     public static final String[] FIELDS = {
@@ -4142,74 +4893,63 @@ public class Business extends APINode {
 
     @Override
     public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestDeleteApps(String nodeId, APIContext context) {
-      super(context, nodeId, "/apps", "DELETE", Arrays.asList(PARAMS));
+    public APIRequestGetSystemUsers(String nodeId, APIContext context) {
+      super(context, nodeId, "/system_users", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestDeleteApps setParam(String param, Object value) {
+    @Override
+    public APIRequestGetSystemUsers setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestDeleteApps setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetSystemUsers setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestDeleteApps setId (String id) {
-      this.setParam("id", id);
-      return this;
-    }
-
-
-    public APIRequestDeleteApps setAppId (Long appId) {
-      this.setParam("app_id", appId);
-      return this;
-    }
-
-    public APIRequestDeleteApps setAppId (String appId) {
-      this.setParam("app_id", appId);
-      return this;
-    }
-
-    public APIRequestDeleteApps requestAllFields () {
+    public APIRequestGetSystemUsers requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestDeleteApps requestAllFields (boolean value) {
+    public APIRequestGetSystemUsers requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestDeleteApps requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetSystemUsers requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestDeleteApps requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetSystemUsers requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestDeleteApps requestField (String field) {
+    @Override
+    public APIRequestGetSystemUsers requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestDeleteApps requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetSystemUsers requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
   }
 
   public static class APIRequestCreateUserPermission extends APIRequest<APINode> {
@@ -4220,10 +4960,10 @@ public class Business extends APINode {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "id",
-      "user",
       "email",
+      "id",
       "role",
+      "user",
     };
 
     public static final String[] FIELDS = {
@@ -4241,7 +4981,7 @@ public class Business extends APINode {
 
     @Override
     public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -4249,46 +4989,44 @@ public class Business extends APINode {
       super(context, nodeId, "/userpermissions", "POST", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestCreateUserPermission setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestCreateUserPermission setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestCreateUserPermission setId (String id) {
-      this.setParam("id", id);
+    public APIRequestCreateUserPermission setEmail (String email) {
+      this.setParam("email", email);
       return this;
     }
 
-
-    public APIRequestCreateUserPermission setUser (Long user) {
-      this.setParam("user", user);
+    public APIRequestCreateUserPermission setId (String id) {
+      this.setParam("id", id);
       return this;
     }
 
-    public APIRequestCreateUserPermission setUser (String user) {
-      this.setParam("user", user);
+    public APIRequestCreateUserPermission setRole (EnumRole role) {
+      this.setParam("role", role);
       return this;
     }
-
-    public APIRequestCreateUserPermission setEmail (String email) {
-      this.setParam("email", email);
+    public APIRequestCreateUserPermission setRole (String role) {
+      this.setParam("role", role);
       return this;
     }
 
-
-    public APIRequestCreateUserPermission setRole (EnumRole role) {
-      this.setParam("role", role);
+    public APIRequestCreateUserPermission setUser (Long user) {
+      this.setParam("user", user);
       return this;
     }
-
-    public APIRequestCreateUserPermission setRole (String role) {
-      this.setParam("role", role);
+    public APIRequestCreateUserPermission setUser (String user) {
+      this.setParam("user", user);
       return this;
     }
 
@@ -4303,10 +5041,12 @@ public class Business extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestCreateUserPermission requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestCreateUserPermission requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -4314,257 +5054,259 @@ public class Business extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestCreateUserPermission requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestCreateUserPermission requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
   }
 
-  public static class APIRequestCreateAdAccounts extends APIRequest<APINode> {
+  public static class APIRequestGet extends APIRequest<Business> {
 
-    APINode lastResponse = null;
+    Business lastResponse = null;
     @Override
-    public APINode getLastResponse() {
+    public Business getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "id",
-      "adaccount_id",
-      "access_type",
-      "permitted_roles",
     };
 
     public static final String[] FIELDS = {
+      "id",
+      "name",
+      "primary_page",
     };
 
     @Override
-    public APINode parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this).head();
+    public Business parseResponse(String response) throws APIException {
+      return Business.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public APINode execute() throws APIException {
+    public Business execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public Business execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestCreateAdAccounts(String nodeId, APIContext context) {
-      super(context, nodeId, "/adaccounts", "POST", Arrays.asList(PARAMS));
+    public APIRequestGet(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestCreateAdAccounts setParam(String param, Object value) {
+    @Override
+    public APIRequestGet setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestCreateAdAccounts setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGet setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestCreateAdAccounts setId (String id) {
-      this.setParam("id", id);
-      return this;
-    }
-
-
-    public APIRequestCreateAdAccounts setAdaccountId (String adaccountId) {
-      this.setParam("adaccount_id", adaccountId);
-      return this;
-    }
-
-
-    public APIRequestCreateAdAccounts setAccessType (EnumAccessType accessType) {
-      this.setParam("access_type", accessType);
-      return this;
-    }
-
-    public APIRequestCreateAdAccounts setAccessType (String accessType) {
-      this.setParam("access_type", accessType);
-      return this;
-    }
-
-    public APIRequestCreateAdAccounts setPermittedRoles (List<EnumPermittedRoles> permittedRoles) {
-      this.setParam("permitted_roles", permittedRoles);
-      return this;
-    }
-
-    public APIRequestCreateAdAccounts setPermittedRoles (String permittedRoles) {
-      this.setParam("permitted_roles", permittedRoles);
-      return this;
-    }
-
-    public APIRequestCreateAdAccounts requestAllFields () {
+    public APIRequestGet requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestCreateAdAccounts requestAllFields (boolean value) {
+    public APIRequestGet requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestCreateAdAccounts requestFields (List<String> fields) {
+    @Override
+    public APIRequestGet requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestCreateAdAccounts requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGet requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestCreateAdAccounts requestField (String field) {
+    @Override
+    public APIRequestGet requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestCreateAdAccounts requestField (String field, boolean value) {
+    @Override
+    public APIRequestGet requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
-  }
-
-  public static enum EnumType {
-    @SerializedName("small")
-    VALUE_SMALL("small"),
-    @SerializedName("normal")
-    VALUE_NORMAL("normal"),
-    @SerializedName("album")
-    VALUE_ALBUM("album"),
-    @SerializedName("large")
-    VALUE_LARGE("large"),
-    @SerializedName("square")
-    VALUE_SQUARE("square"),
-    NULL(null);
-
-    private String value;
-
-    private EnumType(String value) {
-      this.value = value;
+    public APIRequestGet requestIdField () {
+      return this.requestIdField(true);
     }
-
-    @Override
-    public String toString() {
-      return value;
+    public APIRequestGet requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
     }
-  }
-  public static enum EnumStatus {
-    @SerializedName("EXPIRED")
-    VALUE_EXPIRED("EXPIRED"),
-    @SerializedName("DRAFT")
-    VALUE_DRAFT("DRAFT"),
-    @SerializedName("PENDING")
-    VALUE_PENDING("PENDING"),
-    @SerializedName("ACTIVE")
-    VALUE_ACTIVE("ACTIVE"),
-    @SerializedName("COMPLETED")
-    VALUE_COMPLETED("COMPLETED"),
-    NULL(null);
-
-    private String value;
-
-    private EnumStatus(String value) {
-      this.value = value;
+    public APIRequestGet requestNameField () {
+      return this.requestNameField(true);
     }
-
-    @Override
-    public String toString() {
-      return value;
+    public APIRequestGet requestNameField (boolean value) {
+      this.requestField("name", value);
+      return this;
     }
-  }
-  public static enum EnumRole {
-    @SerializedName("ADMIN")
-    VALUE_ADMIN("ADMIN"),
-    @SerializedName("EMPLOYEE")
-    VALUE_EMPLOYEE("EMPLOYEE"),
-    @SerializedName("SYSTEM_USER")
-    VALUE_SYSTEM_USER("SYSTEM_USER"),
-    @SerializedName("ADMIN_SYSTEM_USER")
-    VALUE_ADMIN_SYSTEM_USER("ADMIN_SYSTEM_USER"),
-    @SerializedName("INSTAGRAM_ADMIN")
-    VALUE_INSTAGRAM_ADMIN("INSTAGRAM_ADMIN"),
-    @SerializedName("INSTAGRAM_EMPLOYEE")
-    VALUE_INSTAGRAM_EMPLOYEE("INSTAGRAM_EMPLOYEE"),
-    @SerializedName("FB_EMPLOYEE_ACCOUNT_MANAGER")
-    VALUE_FB_EMPLOYEE_ACCOUNT_MANAGER("FB_EMPLOYEE_ACCOUNT_MANAGER"),
-    @SerializedName("FB_EMPLOYEE_SALES_REP")
-    VALUE_FB_EMPLOYEE_SALES_REP("FB_EMPLOYEE_SALES_REP"),
-    NULL(null);
-
-    private String value;
-
-    private EnumRole(String value) {
-      this.value = value;
-    }
-
-    @Override
-    public String toString() {
-      return value;
+    public APIRequestGet requestPrimaryPageField () {
+      return this.requestPrimaryPageField(true);
+    }
+    public APIRequestGet requestPrimaryPageField (boolean value) {
+      this.requestField("primary_page", value);
+      return this;
     }
   }
-  public static enum EnumAccessType {
-    @SerializedName("OWNER")
-    VALUE_OWNER("OWNER"),
-    @SerializedName("AGENCY")
-    VALUE_AGENCY("AGENCY"),
-    NULL(null);
 
-    private String value;
+  public static enum EnumSurveyBusinessType {
+      @SerializedName("AGENCY")
+      VALUE_AGENCY("AGENCY"),
+      @SerializedName("ADVERTISER")
+      VALUE_ADVERTISER("ADVERTISER"),
+      @SerializedName("APP_DEVELOPER")
+      VALUE_APP_DEVELOPER("APP_DEVELOPER"),
+      @SerializedName("PUBLISHER")
+      VALUE_PUBLISHER("PUBLISHER"),
+      NULL(null);
+
+      private String value;
+
+      private EnumSurveyBusinessType(String value) {
+        this.value = value;
+      }
 
-    private EnumAccessType(String value) {
-      this.value = value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
+  }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+  public static enum EnumVertical {
+      @SerializedName("ADVERTISING")
+      VALUE_ADVERTISING("ADVERTISING"),
+      @SerializedName("AUTOMOTIVE")
+      VALUE_AUTOMOTIVE("AUTOMOTIVE"),
+      @SerializedName("CONSUMER_PACKAGED_GOODS")
+      VALUE_CONSUMER_PACKAGED_GOODS("CONSUMER_PACKAGED_GOODS"),
+      @SerializedName("ECOMMERCE")
+      VALUE_ECOMMERCE("ECOMMERCE"),
+      @SerializedName("EDUCATION")
+      VALUE_EDUCATION("EDUCATION"),
+      @SerializedName("ENERGY_AND_UTILITIES")
+      VALUE_ENERGY_AND_UTILITIES("ENERGY_AND_UTILITIES"),
+      @SerializedName("ENTERTAINMENT_AND_MEDIA")
+      VALUE_ENTERTAINMENT_AND_MEDIA("ENTERTAINMENT_AND_MEDIA"),
+      @SerializedName("FINANCIAL_SERVICES")
+      VALUE_FINANCIAL_SERVICES("FINANCIAL_SERVICES"),
+      @SerializedName("GAMING")
+      VALUE_GAMING("GAMING"),
+      @SerializedName("GOVERNMENT_AND_POLITICS")
+      VALUE_GOVERNMENT_AND_POLITICS("GOVERNMENT_AND_POLITICS"),
+      @SerializedName("MARKETING")
+      VALUE_MARKETING("MARKETING"),
+      @SerializedName("ORGANIZATIONS_AND_ASSOCIATIONS")
+      VALUE_ORGANIZATIONS_AND_ASSOCIATIONS("ORGANIZATIONS_AND_ASSOCIATIONS"),
+      @SerializedName("PROFESSIONAL_SERVICES")
+      VALUE_PROFESSIONAL_SERVICES("PROFESSIONAL_SERVICES"),
+      @SerializedName("RETAIL")
+      VALUE_RETAIL("RETAIL"),
+      @SerializedName("TECHNOLOGY")
+      VALUE_TECHNOLOGY("TECHNOLOGY"),
+      @SerializedName("TELECOM")
+      VALUE_TELECOM("TELECOM"),
+      @SerializedName("TRAVEL")
+      VALUE_TRAVEL("TRAVEL"),
+      @SerializedName("NON_PROFIT")
+      VALUE_NON_PROFIT("NON_PROFIT"),
+      @SerializedName("RESTAURANT")
+      VALUE_RESTAURANT("RESTAURANT"),
+      @SerializedName("OTHER")
+      VALUE_OTHER("OTHER"),
+      NULL(null);
+
+      private String value;
+
+      private EnumVertical(String value) {
+        this.value = value;
+      }
+
+      @Override
+      public String toString() {
+        return value;
+      }
   }
-  public static enum EnumPermittedRoles {
-    @SerializedName("ADMIN")
-    VALUE_ADMIN("ADMIN"),
-    @SerializedName("GENERAL_USER")
-    VALUE_GENERAL_USER("GENERAL_USER"),
-    @SerializedName("REPORTS_ONLY")
-    VALUE_REPORTS_ONLY("REPORTS_ONLY"),
-    @SerializedName("INSTAGRAM_ADVERTISER")
-    VALUE_INSTAGRAM_ADVERTISER("INSTAGRAM_ADVERTISER"),
-    @SerializedName("INSTAGRAM_MANAGER")
-    VALUE_INSTAGRAM_MANAGER("INSTAGRAM_MANAGER"),
-    @SerializedName("FB_EMPLOYEE_DSO_ADVERTISER")
-    VALUE_FB_EMPLOYEE_DSO_ADVERTISER("FB_EMPLOYEE_DSO_ADVERTISER"),
-    NULL(null);
 
-    private String value;
+  public static enum EnumReportType {
+      @SerializedName("multi_channel_report")
+      VALUE_MULTI_CHANNEL_REPORT("multi_channel_report"),
+      @SerializedName("video_metrics_report")
+      VALUE_VIDEO_METRICS_REPORT("video_metrics_report"),
+      @SerializedName("fruit_rollup_report")
+      VALUE_FRUIT_ROLLUP_REPORT("fruit_rollup_report"),
+      NULL(null);
 
-    private EnumPermittedRoles(String value) {
-      this.value = value;
-    }
+      private String value;
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      private EnumReportType(String value) {
+        this.value = value;
+      }
+
+      @Override
+      public String toString() {
+        return value;
+      }
+  }
+
+  public static enum EnumRole {
+      @SerializedName("ADMIN")
+      VALUE_ADMIN("ADMIN"),
+      @SerializedName("EMPLOYEE")
+      VALUE_EMPLOYEE("EMPLOYEE"),
+      @SerializedName("SYSTEM_USER")
+      VALUE_SYSTEM_USER("SYSTEM_USER"),
+      @SerializedName("ADMIN_SYSTEM_USER")
+      VALUE_ADMIN_SYSTEM_USER("ADMIN_SYSTEM_USER"),
+      @SerializedName("INSTAGRAM_ADMIN")
+      VALUE_INSTAGRAM_ADMIN("INSTAGRAM_ADMIN"),
+      @SerializedName("INSTAGRAM_EMPLOYEE")
+      VALUE_INSTAGRAM_EMPLOYEE("INSTAGRAM_EMPLOYEE"),
+      @SerializedName("FB_EMPLOYEE_ACCOUNT_MANAGER")
+      VALUE_FB_EMPLOYEE_ACCOUNT_MANAGER("FB_EMPLOYEE_ACCOUNT_MANAGER"),
+      @SerializedName("FB_EMPLOYEE_SALES_REP")
+      VALUE_FB_EMPLOYEE_SALES_REP("FB_EMPLOYEE_SALES_REP"),
+      NULL(null);
+
+      private String value;
+
+      private EnumRole(String value) {
+        this.value = value;
+      }
+
+      @Override
+      public String toString() {
+        return value;
+      }
   }
 
+
   synchronized /*package*/ static Gson getGson() {
     if (gson != null) {
       return gson;
@@ -4581,16 +5323,16 @@ public class Business extends APINode {
   public Business copyFrom(Business instance) {
     this.mId = instance.mId;
     this.mName = instance.mName;
-    this.mPrimaryPage = instance.mPrimaryPage;
     this.mPaymentAccountId = instance.mPaymentAccountId;
-    this.mContext = instance.mContext;
+    this.mPrimaryPage = instance.mPrimaryPage;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<Business> getParser() {
     return new APIRequest.ResponseParser<Business>() {
-      public APINodeList<Business> parseResponse(String response, APIContext context, APIRequest<Business> request) {
+      public APINodeList<Business> parseResponse(String response, APIContext context, APIRequest<Business> request) throws MalformedResponseException {
         return Business.parseResponse(response, context, request);
       }
     };
