@@ -24,44 +24,47 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AdsPixel extends APINode {
-  @SerializedName("id")
-  private String mId = null;
-  @SerializedName("owner_business")
-  private Business mOwnerBusiness = null;
-  @SerializedName("owner_ad_account")
-  private AdAccount mOwnerAdAccount = null;
-  @SerializedName("name")
-  private String mName = null;
+  @SerializedName("code")
+  private String mCode = null;
   @SerializedName("creation_time")
   private String mCreationTime = null;
+  @SerializedName("id")
+  private String mId = null;
   @SerializedName("last_fired_time")
   private String mLastFiredTime = null;
-  @SerializedName("code")
-  private String mCode = null;
+  @SerializedName("name")
+  private String mName = null;
+  @SerializedName("owner_ad_account")
+  private AdAccount mOwnerAdAccount = null;
+  @SerializedName("owner_business")
+  private Business mOwnerBusiness = null;
   protected static Gson gson = null;
 
   AdsPixel() {
@@ -73,11 +76,11 @@ public class AdsPixel extends APINode {
 
   public AdsPixel(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public AdsPixel fetch() throws APIException{
-    AdsPixel newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    AdsPixel newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -94,8 +97,17 @@ public class AdsPixel extends APINode {
     return adsPixel;
   }
 
+  public static APINodeList<AdsPixel> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<AdsPixel>)(
+      new APIRequest<AdsPixel>(context, "", "/", "GET", AdsPixel.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -110,22 +122,23 @@ public class AdsPixel extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    adsPixel.mContext = context;
+    adsPixel.context = context;
     adsPixel.rawValue = json;
     return adsPixel;
   }
 
-  public static APINodeList<AdsPixel> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AdsPixel> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AdsPixel> adsPixels = new APINodeList<AdsPixel>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -138,10 +151,11 @@ public class AdsPixel extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            adsPixels.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            adsPixels.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -152,7 +166,20 @@ public class AdsPixel extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            adsPixels.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  adsPixels.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              adsPixels.add(loadJSON(obj.toString(), context));
+            }
           }
           return adsPixels;
         } else if (obj.has("images")) {
@@ -163,24 +190,54 @@ public class AdsPixel extends APINode {
           }
           return adsPixels;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              adsPixels.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return adsPixels;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          adsPixels.clear();
           adsPixels.add(loadJSON(json, context));
           return adsPixels;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -188,1323 +245,1315 @@ public class AdsPixel extends APINode {
     return getGson().toJson(this);
   }
 
-  public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetAudiences getAudiences() {
+    return new APIRequestGetAudiences(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestUpdate update() {
-    return new APIRequestUpdate(this.getPrefixedId().toString(), mContext);
+  public APIRequestGetSharedAccounts getSharedAccounts() {
+    return new APIRequestGetSharedAccounts(this.getPrefixedId().toString(), context);
+  }
+
+  public APIRequestGetSharedAgencies getSharedAgencies() {
+    return new APIRequestGetSharedAgencies(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestGetStats getStats() {
-    return new APIRequestGetStats(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetStats(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetAudiences getAudiences() {
-    return new APIRequestGetAudiences(this.getPrefixedId().toString(), mContext);
+  public APIRequestGet get() {
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetSharedAccounts getSharedAccounts() {
-    return new APIRequestGetSharedAccounts(this.getPrefixedId().toString(), mContext);
+  public APIRequestUpdate update() {
+    return new APIRequestUpdate(this.getPrefixedId().toString(), context);
   }
 
-  public APIRequestGetSharedAgencies getSharedAgencies() {
-    return new APIRequestGetSharedAgencies(this.getPrefixedId().toString(), mContext);
+
+  public String getFieldCode() {
+    return mCode;
   }
 
+  public String getFieldCreationTime() {
+    return mCreationTime;
+  }
 
   public String getFieldId() {
     return mId;
   }
 
-  public Business getFieldOwnerBusiness() {
-    if (mOwnerBusiness != null) {
-      mOwnerBusiness.mContext = getContext();
-    }
-    return mOwnerBusiness;
-  }
-
-  public AdAccount getFieldOwnerAdAccount() {
-    if (mOwnerAdAccount != null) {
-      mOwnerAdAccount.mContext = getContext();
-    }
-    return mOwnerAdAccount;
+  public String getFieldLastFiredTime() {
+    return mLastFiredTime;
   }
 
   public String getFieldName() {
     return mName;
   }
 
-  public String getFieldCreationTime() {
-    return mCreationTime;
-  }
-
-  public String getFieldLastFiredTime() {
-    return mLastFiredTime;
+  public AdAccount getFieldOwnerAdAccount() {
+    if (mOwnerAdAccount != null) {
+      mOwnerAdAccount.context = getContext();
+    }
+    return mOwnerAdAccount;
   }
 
-  public String getFieldCode() {
-    return mCode;
+  public Business getFieldOwnerBusiness() {
+    if (mOwnerBusiness != null) {
+      mOwnerBusiness.context = getContext();
+    }
+    return mOwnerBusiness;
   }
 
 
 
-  public static class APIRequestGet extends APIRequest<AdsPixel> {
+  public static class APIRequestGetAudiences extends APIRequest<CustomAudience> {
 
-    AdsPixel lastResponse = null;
+    APINodeList<CustomAudience> lastResponse = null;
     @Override
-    public AdsPixel getLastResponse() {
+    public APINodeList<CustomAudience> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
     };
 
     public static final String[] FIELDS = {
+      "account_id",
+      "approximate_count",
+      "data_source",
+      "delivery_status",
+      "description",
+      "excluded_custom_audiences",
+      "external_event_source",
       "id",
-      "owner_business",
-      "owner_ad_account",
+      "included_custom_audiences",
+      "last_used_time",
+      "lookalike_audience_ids",
+      "lookalike_spec",
       "name",
-      "creation_time",
-      "last_fired_time",
-      "code",
+      "operation_status",
+      "opt_out_link",
+      "owner_business",
+      "permission_for_actions",
+      "pixel_id",
+      "retention_days",
+      "rule",
+      "subtype",
+      "time_content_updated",
+      "time_created",
+      "time_updated",
     };
 
     @Override
-    public AdsPixel parseResponse(String response) throws APIException {
-      return AdsPixel.parseResponse(response, getContext(), this).head();
+    public APINodeList<CustomAudience> parseResponse(String response) throws APIException {
+      return CustomAudience.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public AdsPixel execute() throws APIException {
+    public APINodeList<CustomAudience> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public AdsPixel execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<CustomAudience> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGet(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetAudiences(String nodeId, APIContext context) {
+      super(context, nodeId, "/audiences", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGet setParam(String param, Object value) {
+    @Override
+    public APIRequestGetAudiences setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGet setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetAudiences setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGet requestAllFields () {
+    public APIRequestGetAudiences requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGet requestAllFields (boolean value) {
+    public APIRequestGetAudiences requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetAudiences requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGet requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetAudiences requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGet requestField (String field) {
+    @Override
+    public APIRequestGetAudiences requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGet requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetAudiences requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGet requestIdField () {
-      return this.requestIdField(true);
+    public APIRequestGetAudiences requestAccountIdField () {
+      return this.requestAccountIdField(true);
     }
-    public APIRequestGet requestIdField (boolean value) {
-      this.requestField("id", value);
+    public APIRequestGetAudiences requestAccountIdField (boolean value) {
+      this.requestField("account_id", value);
       return this;
     }
-    public APIRequestGet requestOwnerBusinessField () {
-      return this.requestOwnerBusinessField(true);
+    public APIRequestGetAudiences requestApproximateCountField () {
+      return this.requestApproximateCountField(true);
     }
-    public APIRequestGet requestOwnerBusinessField (boolean value) {
-      this.requestField("owner_business", value);
+    public APIRequestGetAudiences requestApproximateCountField (boolean value) {
+      this.requestField("approximate_count", value);
       return this;
     }
-    public APIRequestGet requestOwnerAdAccountField () {
-      return this.requestOwnerAdAccountField(true);
+    public APIRequestGetAudiences requestDataSourceField () {
+      return this.requestDataSourceField(true);
     }
-    public APIRequestGet requestOwnerAdAccountField (boolean value) {
-      this.requestField("owner_ad_account", value);
+    public APIRequestGetAudiences requestDataSourceField (boolean value) {
+      this.requestField("data_source", value);
       return this;
     }
-    public APIRequestGet requestNameField () {
-      return this.requestNameField(true);
+    public APIRequestGetAudiences requestDeliveryStatusField () {
+      return this.requestDeliveryStatusField(true);
     }
-    public APIRequestGet requestNameField (boolean value) {
-      this.requestField("name", value);
+    public APIRequestGetAudiences requestDeliveryStatusField (boolean value) {
+      this.requestField("delivery_status", value);
       return this;
     }
-    public APIRequestGet requestCreationTimeField () {
-      return this.requestCreationTimeField(true);
+    public APIRequestGetAudiences requestDescriptionField () {
+      return this.requestDescriptionField(true);
     }
-    public APIRequestGet requestCreationTimeField (boolean value) {
-      this.requestField("creation_time", value);
+    public APIRequestGetAudiences requestDescriptionField (boolean value) {
+      this.requestField("description", value);
       return this;
     }
-    public APIRequestGet requestLastFiredTimeField () {
-      return this.requestLastFiredTimeField(true);
+    public APIRequestGetAudiences requestExcludedCustomAudiencesField () {
+      return this.requestExcludedCustomAudiencesField(true);
     }
-    public APIRequestGet requestLastFiredTimeField (boolean value) {
-      this.requestField("last_fired_time", value);
+    public APIRequestGetAudiences requestExcludedCustomAudiencesField (boolean value) {
+      this.requestField("excluded_custom_audiences", value);
       return this;
     }
-    public APIRequestGet requestCodeField () {
-      return this.requestCodeField(true);
+    public APIRequestGetAudiences requestExternalEventSourceField () {
+      return this.requestExternalEventSourceField(true);
     }
-    public APIRequestGet requestCodeField (boolean value) {
-      this.requestField("code", value);
+    public APIRequestGetAudiences requestExternalEventSourceField (boolean value) {
+      this.requestField("external_event_source", value);
       return this;
     }
-
-  }
-
-  public static class APIRequestUpdate extends APIRequest<APINode> {
-
-    APINode lastResponse = null;
-    @Override
-    public APINode getLastResponse() {
-      return lastResponse;
+    public APIRequestGetAudiences requestIdField () {
+      return this.requestIdField(true);
     }
-    public static final String[] PARAMS = {
-      "name",
-    };
-
-    public static final String[] FIELDS = {
-    };
-
-    @Override
-    public APINode parseResponse(String response) throws APIException {
-      return APINode.parseResponse(response, getContext(), this).head();
+    public APIRequestGetAudiences requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
     }
-
-    @Override
-    public APINode execute() throws APIException {
-      return execute(new HashMap<String, Object>());
+    public APIRequestGetAudiences requestIncludedCustomAudiencesField () {
+      return this.requestIncludedCustomAudiencesField(true);
     }
-
-    @Override
-    public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
+    public APIRequestGetAudiences requestIncludedCustomAudiencesField (boolean value) {
+      this.requestField("included_custom_audiences", value);
+      return this;
     }
-
-    public APIRequestUpdate(String nodeId, APIContext context) {
-      super(context, nodeId, "/", "POST", Arrays.asList(PARAMS));
+    public APIRequestGetAudiences requestLastUsedTimeField () {
+      return this.requestLastUsedTimeField(true);
     }
-
-    public APIRequestUpdate setParam(String param, Object value) {
-      setParamInternal(param, value);
+    public APIRequestGetAudiences requestLastUsedTimeField (boolean value) {
+      this.requestField("last_used_time", value);
       return this;
     }
-
-    public APIRequestUpdate setParams(Map<String, Object> params) {
-      setParamsInternal(params);
-      return this;
+    public APIRequestGetAudiences requestLookalikeAudienceIdsField () {
+      return this.requestLookalikeAudienceIdsField(true);
     }
-
-
-    public APIRequestUpdate setName (String name) {
-      this.setParam("name", name);
+    public APIRequestGetAudiences requestLookalikeAudienceIdsField (boolean value) {
+      this.requestField("lookalike_audience_ids", value);
       return this;
     }
-
-
-    public APIRequestUpdate requestAllFields () {
-      return this.requestAllFields(true);
+    public APIRequestGetAudiences requestLookalikeSpecField () {
+      return this.requestLookalikeSpecField(true);
     }
-
-    public APIRequestUpdate requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
+    public APIRequestGetAudiences requestLookalikeSpecField (boolean value) {
+      this.requestField("lookalike_spec", value);
       return this;
     }
-
-    public APIRequestUpdate requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
+    public APIRequestGetAudiences requestNameField () {
+      return this.requestNameField(true);
     }
-
-    public APIRequestUpdate requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+    public APIRequestGetAudiences requestNameField (boolean value) {
+      this.requestField("name", value);
       return this;
     }
-
-    public APIRequestUpdate requestField (String field) {
-      this.requestField(field, true);
+    public APIRequestGetAudiences requestOperationStatusField () {
+      return this.requestOperationStatusField(true);
+    }
+    public APIRequestGetAudiences requestOperationStatusField (boolean value) {
+      this.requestField("operation_status", value);
       return this;
     }
-
-    public APIRequestUpdate requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
+    public APIRequestGetAudiences requestOptOutLinkField () {
+      return this.requestOptOutLinkField(true);
+    }
+    public APIRequestGetAudiences requestOptOutLinkField (boolean value) {
+      this.requestField("opt_out_link", value);
+      return this;
+    }
+    public APIRequestGetAudiences requestOwnerBusinessField () {
+      return this.requestOwnerBusinessField(true);
+    }
+    public APIRequestGetAudiences requestOwnerBusinessField (boolean value) {
+      this.requestField("owner_business", value);
+      return this;
+    }
+    public APIRequestGetAudiences requestPermissionForActionsField () {
+      return this.requestPermissionForActionsField(true);
+    }
+    public APIRequestGetAudiences requestPermissionForActionsField (boolean value) {
+      this.requestField("permission_for_actions", value);
+      return this;
+    }
+    public APIRequestGetAudiences requestPixelIdField () {
+      return this.requestPixelIdField(true);
+    }
+    public APIRequestGetAudiences requestPixelIdField (boolean value) {
+      this.requestField("pixel_id", value);
+      return this;
+    }
+    public APIRequestGetAudiences requestRetentionDaysField () {
+      return this.requestRetentionDaysField(true);
+    }
+    public APIRequestGetAudiences requestRetentionDaysField (boolean value) {
+      this.requestField("retention_days", value);
+      return this;
+    }
+    public APIRequestGetAudiences requestRuleField () {
+      return this.requestRuleField(true);
+    }
+    public APIRequestGetAudiences requestRuleField (boolean value) {
+      this.requestField("rule", value);
+      return this;
+    }
+    public APIRequestGetAudiences requestSubtypeField () {
+      return this.requestSubtypeField(true);
+    }
+    public APIRequestGetAudiences requestSubtypeField (boolean value) {
+      this.requestField("subtype", value);
+      return this;
+    }
+    public APIRequestGetAudiences requestTimeContentUpdatedField () {
+      return this.requestTimeContentUpdatedField(true);
+    }
+    public APIRequestGetAudiences requestTimeContentUpdatedField (boolean value) {
+      this.requestField("time_content_updated", value);
+      return this;
+    }
+    public APIRequestGetAudiences requestTimeCreatedField () {
+      return this.requestTimeCreatedField(true);
+    }
+    public APIRequestGetAudiences requestTimeCreatedField (boolean value) {
+      this.requestField("time_created", value);
+      return this;
+    }
+    public APIRequestGetAudiences requestTimeUpdatedField () {
+      return this.requestTimeUpdatedField(true);
+    }
+    public APIRequestGetAudiences requestTimeUpdatedField (boolean value) {
+      this.requestField("time_updated", value);
       return this;
     }
-
-
   }
 
-  public static class APIRequestGetStats extends APIRequest<AdsPixelStatsResult> {
+  public static class APIRequestGetSharedAccounts extends APIRequest<AdAccount> {
 
-    APINodeList<AdsPixelStatsResult> lastResponse = null;
+    APINodeList<AdAccount> lastResponse = null;
     @Override
-    public APINodeList<AdsPixelStatsResult> getLastResponse() {
+    public APINodeList<AdAccount> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "start_time",
-      "end_time",
-      "aggregation",
-      "event",
+      "business",
     };
 
     public static final String[] FIELDS = {
-      "aggregation",
-      "timestamp",
-      "data",
+      "account_groups",
+      "account_id",
+      "account_status",
+      "age",
+      "agency_client_declaration",
+      "amount_spent",
+      "balance",
+      "business",
+      "business_city",
+      "business_country_code",
+      "business_name",
+      "business_state",
+      "business_street",
+      "business_street2",
+      "business_zip",
+      "capabilities",
+      "created_time",
+      "currency",
+      "disable_reason",
+      "end_advertiser",
+      "end_advertiser_name",
+      "failed_delivery_checks",
+      "funding_source",
+      "funding_source_details",
+      "has_migrated_permissions",
+      "id",
+      "io_number",
+      "is_notifications_enabled",
+      "is_personal",
+      "is_prepay_account",
+      "is_tax_id_required",
+      "last_used_time",
+      "line_numbers",
+      "media_agency",
+      "min_campaign_group_spend_cap",
+      "min_daily_budget",
+      "name",
+      "offsite_pixels_tos_accepted",
+      "owner",
+      "owner_business",
+      "partner",
+      "rf_spec",
+      "spend_cap",
+      "tax_id",
+      "tax_id_status",
+      "tax_id_type",
+      "timezone_id",
+      "timezone_name",
+      "timezone_offset_hours_utc",
+      "tos_accepted",
+      "user_role",
     };
 
     @Override
-    public APINodeList<AdsPixelStatsResult> parseResponse(String response) throws APIException {
-      return AdsPixelStatsResult.parseResponse(response, getContext(), this);
+    public APINodeList<AdAccount> parseResponse(String response) throws APIException {
+      return AdAccount.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINodeList<AdsPixelStatsResult> execute() throws APIException {
+    public APINodeList<AdAccount> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<AdsPixelStatsResult> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<AdAccount> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetStats(String nodeId, APIContext context) {
-      super(context, nodeId, "/stats", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetSharedAccounts(String nodeId, APIContext context) {
+      super(context, nodeId, "/shared_accounts", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetStats setParam(String param, Object value) {
+    @Override
+    public APIRequestGetSharedAccounts setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetStats setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetSharedAccounts setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetStats setStartTime (String startTime) {
-      this.setParam("start_time", startTime);
-      return this;
-    }
-
-
-    public APIRequestGetStats setEndTime (String endTime) {
-      this.setParam("end_time", endTime);
-      return this;
-    }
-
-
-    public APIRequestGetStats setAggregation (EnumAggregation aggregation) {
-      this.setParam("aggregation", aggregation);
-      return this;
-    }
-
-    public APIRequestGetStats setAggregation (String aggregation) {
-      this.setParam("aggregation", aggregation);
-      return this;
-    }
-
-    public APIRequestGetStats setEvent (String event) {
-      this.setParam("event", event);
+    public APIRequestGetSharedAccounts setBusiness (String business) {
+      this.setParam("business", business);
       return this;
     }
 
-
-    public APIRequestGetStats requestAllFields () {
+    public APIRequestGetSharedAccounts requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetStats requestAllFields (boolean value) {
+    public APIRequestGetSharedAccounts requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetStats requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetSharedAccounts requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetStats requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetSharedAccounts requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetStats requestField (String field) {
+    @Override
+    public APIRequestGetSharedAccounts requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetStats requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetSharedAccounts requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetStats requestAggregationField () {
-      return this.requestAggregationField(true);
+    public APIRequestGetSharedAccounts requestAccountGroupsField () {
+      return this.requestAccountGroupsField(true);
     }
-    public APIRequestGetStats requestAggregationField (boolean value) {
-      this.requestField("aggregation", value);
+    public APIRequestGetSharedAccounts requestAccountGroupsField (boolean value) {
+      this.requestField("account_groups", value);
       return this;
     }
-    public APIRequestGetStats requestTimestampField () {
-      return this.requestTimestampField(true);
+    public APIRequestGetSharedAccounts requestAccountIdField () {
+      return this.requestAccountIdField(true);
     }
-    public APIRequestGetStats requestTimestampField (boolean value) {
-      this.requestField("timestamp", value);
+    public APIRequestGetSharedAccounts requestAccountIdField (boolean value) {
+      this.requestField("account_id", value);
       return this;
     }
-    public APIRequestGetStats requestDataField () {
-      return this.requestDataField(true);
+    public APIRequestGetSharedAccounts requestAccountStatusField () {
+      return this.requestAccountStatusField(true);
     }
-    public APIRequestGetStats requestDataField (boolean value) {
-      this.requestField("data", value);
+    public APIRequestGetSharedAccounts requestAccountStatusField (boolean value) {
+      this.requestField("account_status", value);
       return this;
     }
-
-  }
-
-  public static class APIRequestGetAudiences extends APIRequest<CustomAudience> {
-
-    APINodeList<CustomAudience> lastResponse = null;
-    @Override
-    public APINodeList<CustomAudience> getLastResponse() {
-      return lastResponse;
+    public APIRequestGetSharedAccounts requestAgeField () {
+      return this.requestAgeField(true);
     }
-    public static final String[] PARAMS = {
-    };
-
-    public static final String[] FIELDS = {
-      "id",
-      "account_id",
-      "approximate_count",
-      "data_source",
-      "delivery_status",
-      "description",
-      "excluded_custom_audiences",
-      "included_custom_audiences",
-      "external_event_source",
-      "lookalike_audience_ids",
-      "lookalike_spec",
-      "name",
-      "operation_status",
-      "opt_out_link",
-      "permission_for_actions",
-      "pixel_id",
-      "rule",
-      "retention_days",
-      "subtype",
-      "time_created",
-      "time_updated",
-      "time_content_updated",
-    };
-
-    @Override
-    public APINodeList<CustomAudience> parseResponse(String response) throws APIException {
-      return CustomAudience.parseResponse(response, getContext(), this);
+    public APIRequestGetSharedAccounts requestAgeField (boolean value) {
+      this.requestField("age", value);
+      return this;
     }
-
-    @Override
-    public APINodeList<CustomAudience> execute() throws APIException {
-      return execute(new HashMap<String, Object>());
+    public APIRequestGetSharedAccounts requestAgencyClientDeclarationField () {
+      return this.requestAgencyClientDeclarationField(true);
     }
-
-    @Override
-    public APINodeList<CustomAudience> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
+    public APIRequestGetSharedAccounts requestAgencyClientDeclarationField (boolean value) {
+      this.requestField("agency_client_declaration", value);
+      return this;
     }
-
-    public APIRequestGetAudiences(String nodeId, APIContext context) {
-      super(context, nodeId, "/audiences", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetSharedAccounts requestAmountSpentField () {
+      return this.requestAmountSpentField(true);
     }
-
-    public APIRequestGetAudiences setParam(String param, Object value) {
-      setParamInternal(param, value);
+    public APIRequestGetSharedAccounts requestAmountSpentField (boolean value) {
+      this.requestField("amount_spent", value);
       return this;
     }
-
-    public APIRequestGetAudiences setParams(Map<String, Object> params) {
-      setParamsInternal(params);
+    public APIRequestGetSharedAccounts requestBalanceField () {
+      return this.requestBalanceField(true);
+    }
+    public APIRequestGetSharedAccounts requestBalanceField (boolean value) {
+      this.requestField("balance", value);
       return this;
     }
-
-
-    public APIRequestGetAudiences requestAllFields () {
-      return this.requestAllFields(true);
+    public APIRequestGetSharedAccounts requestBusinessField () {
+      return this.requestBusinessField(true);
     }
-
-    public APIRequestGetAudiences requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
-      }
+    public APIRequestGetSharedAccounts requestBusinessField (boolean value) {
+      this.requestField("business", value);
       return this;
     }
-
-    public APIRequestGetAudiences requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
+    public APIRequestGetSharedAccounts requestBusinessCityField () {
+      return this.requestBusinessCityField(true);
     }
-
-    public APIRequestGetAudiences requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
-      }
+    public APIRequestGetSharedAccounts requestBusinessCityField (boolean value) {
+      this.requestField("business_city", value);
       return this;
     }
-
-    public APIRequestGetAudiences requestField (String field) {
-      this.requestField(field, true);
-      return this;
+    public APIRequestGetSharedAccounts requestBusinessCountryCodeField () {
+      return this.requestBusinessCountryCodeField(true);
     }
-
-    public APIRequestGetAudiences requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
+    public APIRequestGetSharedAccounts requestBusinessCountryCodeField (boolean value) {
+      this.requestField("business_country_code", value);
       return this;
     }
-
-    public APIRequestGetAudiences requestIdField () {
-      return this.requestIdField(true);
+    public APIRequestGetSharedAccounts requestBusinessNameField () {
+      return this.requestBusinessNameField(true);
     }
-    public APIRequestGetAudiences requestIdField (boolean value) {
-      this.requestField("id", value);
+    public APIRequestGetSharedAccounts requestBusinessNameField (boolean value) {
+      this.requestField("business_name", value);
       return this;
     }
-    public APIRequestGetAudiences requestAccountIdField () {
-      return this.requestAccountIdField(true);
+    public APIRequestGetSharedAccounts requestBusinessStateField () {
+      return this.requestBusinessStateField(true);
     }
-    public APIRequestGetAudiences requestAccountIdField (boolean value) {
-      this.requestField("account_id", value);
+    public APIRequestGetSharedAccounts requestBusinessStateField (boolean value) {
+      this.requestField("business_state", value);
       return this;
     }
-    public APIRequestGetAudiences requestApproximateCountField () {
-      return this.requestApproximateCountField(true);
+    public APIRequestGetSharedAccounts requestBusinessStreetField () {
+      return this.requestBusinessStreetField(true);
     }
-    public APIRequestGetAudiences requestApproximateCountField (boolean value) {
-      this.requestField("approximate_count", value);
+    public APIRequestGetSharedAccounts requestBusinessStreetField (boolean value) {
+      this.requestField("business_street", value);
       return this;
     }
-    public APIRequestGetAudiences requestDataSourceField () {
-      return this.requestDataSourceField(true);
+    public APIRequestGetSharedAccounts requestBusinessStreet2Field () {
+      return this.requestBusinessStreet2Field(true);
     }
-    public APIRequestGetAudiences requestDataSourceField (boolean value) {
-      this.requestField("data_source", value);
+    public APIRequestGetSharedAccounts requestBusinessStreet2Field (boolean value) {
+      this.requestField("business_street2", value);
       return this;
     }
-    public APIRequestGetAudiences requestDeliveryStatusField () {
-      return this.requestDeliveryStatusField(true);
+    public APIRequestGetSharedAccounts requestBusinessZipField () {
+      return this.requestBusinessZipField(true);
     }
-    public APIRequestGetAudiences requestDeliveryStatusField (boolean value) {
-      this.requestField("delivery_status", value);
+    public APIRequestGetSharedAccounts requestBusinessZipField (boolean value) {
+      this.requestField("business_zip", value);
       return this;
     }
-    public APIRequestGetAudiences requestDescriptionField () {
-      return this.requestDescriptionField(true);
+    public APIRequestGetSharedAccounts requestCapabilitiesField () {
+      return this.requestCapabilitiesField(true);
     }
-    public APIRequestGetAudiences requestDescriptionField (boolean value) {
-      this.requestField("description", value);
+    public APIRequestGetSharedAccounts requestCapabilitiesField (boolean value) {
+      this.requestField("capabilities", value);
       return this;
     }
-    public APIRequestGetAudiences requestExcludedCustomAudiencesField () {
-      return this.requestExcludedCustomAudiencesField(true);
+    public APIRequestGetSharedAccounts requestCreatedTimeField () {
+      return this.requestCreatedTimeField(true);
     }
-    public APIRequestGetAudiences requestExcludedCustomAudiencesField (boolean value) {
-      this.requestField("excluded_custom_audiences", value);
+    public APIRequestGetSharedAccounts requestCreatedTimeField (boolean value) {
+      this.requestField("created_time", value);
       return this;
     }
-    public APIRequestGetAudiences requestIncludedCustomAudiencesField () {
-      return this.requestIncludedCustomAudiencesField(true);
+    public APIRequestGetSharedAccounts requestCurrencyField () {
+      return this.requestCurrencyField(true);
     }
-    public APIRequestGetAudiences requestIncludedCustomAudiencesField (boolean value) {
-      this.requestField("included_custom_audiences", value);
+    public APIRequestGetSharedAccounts requestCurrencyField (boolean value) {
+      this.requestField("currency", value);
       return this;
     }
-    public APIRequestGetAudiences requestExternalEventSourceField () {
-      return this.requestExternalEventSourceField(true);
+    public APIRequestGetSharedAccounts requestDisableReasonField () {
+      return this.requestDisableReasonField(true);
     }
-    public APIRequestGetAudiences requestExternalEventSourceField (boolean value) {
-      this.requestField("external_event_source", value);
+    public APIRequestGetSharedAccounts requestDisableReasonField (boolean value) {
+      this.requestField("disable_reason", value);
       return this;
     }
-    public APIRequestGetAudiences requestLookalikeAudienceIdsField () {
-      return this.requestLookalikeAudienceIdsField(true);
+    public APIRequestGetSharedAccounts requestEndAdvertiserField () {
+      return this.requestEndAdvertiserField(true);
     }
-    public APIRequestGetAudiences requestLookalikeAudienceIdsField (boolean value) {
-      this.requestField("lookalike_audience_ids", value);
+    public APIRequestGetSharedAccounts requestEndAdvertiserField (boolean value) {
+      this.requestField("end_advertiser", value);
       return this;
     }
-    public APIRequestGetAudiences requestLookalikeSpecField () {
-      return this.requestLookalikeSpecField(true);
+    public APIRequestGetSharedAccounts requestEndAdvertiserNameField () {
+      return this.requestEndAdvertiserNameField(true);
     }
-    public APIRequestGetAudiences requestLookalikeSpecField (boolean value) {
-      this.requestField("lookalike_spec", value);
+    public APIRequestGetSharedAccounts requestEndAdvertiserNameField (boolean value) {
+      this.requestField("end_advertiser_name", value);
       return this;
     }
-    public APIRequestGetAudiences requestNameField () {
-      return this.requestNameField(true);
+    public APIRequestGetSharedAccounts requestFailedDeliveryChecksField () {
+      return this.requestFailedDeliveryChecksField(true);
     }
-    public APIRequestGetAudiences requestNameField (boolean value) {
-      this.requestField("name", value);
+    public APIRequestGetSharedAccounts requestFailedDeliveryChecksField (boolean value) {
+      this.requestField("failed_delivery_checks", value);
       return this;
     }
-    public APIRequestGetAudiences requestOperationStatusField () {
-      return this.requestOperationStatusField(true);
+    public APIRequestGetSharedAccounts requestFundingSourceField () {
+      return this.requestFundingSourceField(true);
     }
-    public APIRequestGetAudiences requestOperationStatusField (boolean value) {
-      this.requestField("operation_status", value);
+    public APIRequestGetSharedAccounts requestFundingSourceField (boolean value) {
+      this.requestField("funding_source", value);
       return this;
     }
-    public APIRequestGetAudiences requestOptOutLinkField () {
-      return this.requestOptOutLinkField(true);
+    public APIRequestGetSharedAccounts requestFundingSourceDetailsField () {
+      return this.requestFundingSourceDetailsField(true);
     }
-    public APIRequestGetAudiences requestOptOutLinkField (boolean value) {
-      this.requestField("opt_out_link", value);
+    public APIRequestGetSharedAccounts requestFundingSourceDetailsField (boolean value) {
+      this.requestField("funding_source_details", value);
       return this;
     }
-    public APIRequestGetAudiences requestPermissionForActionsField () {
-      return this.requestPermissionForActionsField(true);
+    public APIRequestGetSharedAccounts requestHasMigratedPermissionsField () {
+      return this.requestHasMigratedPermissionsField(true);
     }
-    public APIRequestGetAudiences requestPermissionForActionsField (boolean value) {
-      this.requestField("permission_for_actions", value);
+    public APIRequestGetSharedAccounts requestHasMigratedPermissionsField (boolean value) {
+      this.requestField("has_migrated_permissions", value);
       return this;
     }
-    public APIRequestGetAudiences requestPixelIdField () {
-      return this.requestPixelIdField(true);
+    public APIRequestGetSharedAccounts requestIdField () {
+      return this.requestIdField(true);
     }
-    public APIRequestGetAudiences requestPixelIdField (boolean value) {
-      this.requestField("pixel_id", value);
+    public APIRequestGetSharedAccounts requestIdField (boolean value) {
+      this.requestField("id", value);
       return this;
     }
-    public APIRequestGetAudiences requestRuleField () {
-      return this.requestRuleField(true);
+    public APIRequestGetSharedAccounts requestIoNumberField () {
+      return this.requestIoNumberField(true);
     }
-    public APIRequestGetAudiences requestRuleField (boolean value) {
-      this.requestField("rule", value);
+    public APIRequestGetSharedAccounts requestIoNumberField (boolean value) {
+      this.requestField("io_number", value);
       return this;
     }
-    public APIRequestGetAudiences requestRetentionDaysField () {
-      return this.requestRetentionDaysField(true);
+    public APIRequestGetSharedAccounts requestIsNotificationsEnabledField () {
+      return this.requestIsNotificationsEnabledField(true);
     }
-    public APIRequestGetAudiences requestRetentionDaysField (boolean value) {
-      this.requestField("retention_days", value);
+    public APIRequestGetSharedAccounts requestIsNotificationsEnabledField (boolean value) {
+      this.requestField("is_notifications_enabled", value);
       return this;
     }
-    public APIRequestGetAudiences requestSubtypeField () {
-      return this.requestSubtypeField(true);
-    }
-    public APIRequestGetAudiences requestSubtypeField (boolean value) {
-      this.requestField("subtype", value);
+    public APIRequestGetSharedAccounts requestIsPersonalField () {
+      return this.requestIsPersonalField(true);
+    }
+    public APIRequestGetSharedAccounts requestIsPersonalField (boolean value) {
+      this.requestField("is_personal", value);
       return this;
     }
-    public APIRequestGetAudiences requestTimeCreatedField () {
-      return this.requestTimeCreatedField(true);
+    public APIRequestGetSharedAccounts requestIsPrepayAccountField () {
+      return this.requestIsPrepayAccountField(true);
     }
-    public APIRequestGetAudiences requestTimeCreatedField (boolean value) {
-      this.requestField("time_created", value);
+    public APIRequestGetSharedAccounts requestIsPrepayAccountField (boolean value) {
+      this.requestField("is_prepay_account", value);
       return this;
     }
-    public APIRequestGetAudiences requestTimeUpdatedField () {
-      return this.requestTimeUpdatedField(true);
+    public APIRequestGetSharedAccounts requestIsTaxIdRequiredField () {
+      return this.requestIsTaxIdRequiredField(true);
     }
-    public APIRequestGetAudiences requestTimeUpdatedField (boolean value) {
-      this.requestField("time_updated", value);
+    public APIRequestGetSharedAccounts requestIsTaxIdRequiredField (boolean value) {
+      this.requestField("is_tax_id_required", value);
       return this;
     }
-    public APIRequestGetAudiences requestTimeContentUpdatedField () {
-      return this.requestTimeContentUpdatedField(true);
+    public APIRequestGetSharedAccounts requestLastUsedTimeField () {
+      return this.requestLastUsedTimeField(true);
     }
-    public APIRequestGetAudiences requestTimeContentUpdatedField (boolean value) {
-      this.requestField("time_content_updated", value);
+    public APIRequestGetSharedAccounts requestLastUsedTimeField (boolean value) {
+      this.requestField("last_used_time", value);
+      return this;
+    }
+    public APIRequestGetSharedAccounts requestLineNumbersField () {
+      return this.requestLineNumbersField(true);
+    }
+    public APIRequestGetSharedAccounts requestLineNumbersField (boolean value) {
+      this.requestField("line_numbers", value);
+      return this;
+    }
+    public APIRequestGetSharedAccounts requestMediaAgencyField () {
+      return this.requestMediaAgencyField(true);
+    }
+    public APIRequestGetSharedAccounts requestMediaAgencyField (boolean value) {
+      this.requestField("media_agency", value);
+      return this;
+    }
+    public APIRequestGetSharedAccounts requestMinCampaignGroupSpendCapField () {
+      return this.requestMinCampaignGroupSpendCapField(true);
+    }
+    public APIRequestGetSharedAccounts requestMinCampaignGroupSpendCapField (boolean value) {
+      this.requestField("min_campaign_group_spend_cap", value);
+      return this;
+    }
+    public APIRequestGetSharedAccounts requestMinDailyBudgetField () {
+      return this.requestMinDailyBudgetField(true);
+    }
+    public APIRequestGetSharedAccounts requestMinDailyBudgetField (boolean value) {
+      this.requestField("min_daily_budget", value);
+      return this;
+    }
+    public APIRequestGetSharedAccounts requestNameField () {
+      return this.requestNameField(true);
+    }
+    public APIRequestGetSharedAccounts requestNameField (boolean value) {
+      this.requestField("name", value);
+      return this;
+    }
+    public APIRequestGetSharedAccounts requestOffsitePixelsTosAcceptedField () {
+      return this.requestOffsitePixelsTosAcceptedField(true);
+    }
+    public APIRequestGetSharedAccounts requestOffsitePixelsTosAcceptedField (boolean value) {
+      this.requestField("offsite_pixels_tos_accepted", value);
+      return this;
+    }
+    public APIRequestGetSharedAccounts requestOwnerField () {
+      return this.requestOwnerField(true);
+    }
+    public APIRequestGetSharedAccounts requestOwnerField (boolean value) {
+      this.requestField("owner", value);
+      return this;
+    }
+    public APIRequestGetSharedAccounts requestOwnerBusinessField () {
+      return this.requestOwnerBusinessField(true);
+    }
+    public APIRequestGetSharedAccounts requestOwnerBusinessField (boolean value) {
+      this.requestField("owner_business", value);
+      return this;
+    }
+    public APIRequestGetSharedAccounts requestPartnerField () {
+      return this.requestPartnerField(true);
+    }
+    public APIRequestGetSharedAccounts requestPartnerField (boolean value) {
+      this.requestField("partner", value);
+      return this;
+    }
+    public APIRequestGetSharedAccounts requestRfSpecField () {
+      return this.requestRfSpecField(true);
+    }
+    public APIRequestGetSharedAccounts requestRfSpecField (boolean value) {
+      this.requestField("rf_spec", value);
+      return this;
+    }
+    public APIRequestGetSharedAccounts requestSpendCapField () {
+      return this.requestSpendCapField(true);
+    }
+    public APIRequestGetSharedAccounts requestSpendCapField (boolean value) {
+      this.requestField("spend_cap", value);
+      return this;
+    }
+    public APIRequestGetSharedAccounts requestTaxIdField () {
+      return this.requestTaxIdField(true);
+    }
+    public APIRequestGetSharedAccounts requestTaxIdField (boolean value) {
+      this.requestField("tax_id", value);
+      return this;
+    }
+    public APIRequestGetSharedAccounts requestTaxIdStatusField () {
+      return this.requestTaxIdStatusField(true);
+    }
+    public APIRequestGetSharedAccounts requestTaxIdStatusField (boolean value) {
+      this.requestField("tax_id_status", value);
+      return this;
+    }
+    public APIRequestGetSharedAccounts requestTaxIdTypeField () {
+      return this.requestTaxIdTypeField(true);
+    }
+    public APIRequestGetSharedAccounts requestTaxIdTypeField (boolean value) {
+      this.requestField("tax_id_type", value);
+      return this;
+    }
+    public APIRequestGetSharedAccounts requestTimezoneIdField () {
+      return this.requestTimezoneIdField(true);
+    }
+    public APIRequestGetSharedAccounts requestTimezoneIdField (boolean value) {
+      this.requestField("timezone_id", value);
+      return this;
+    }
+    public APIRequestGetSharedAccounts requestTimezoneNameField () {
+      return this.requestTimezoneNameField(true);
+    }
+    public APIRequestGetSharedAccounts requestTimezoneNameField (boolean value) {
+      this.requestField("timezone_name", value);
+      return this;
+    }
+    public APIRequestGetSharedAccounts requestTimezoneOffsetHoursUtcField () {
+      return this.requestTimezoneOffsetHoursUtcField(true);
+    }
+    public APIRequestGetSharedAccounts requestTimezoneOffsetHoursUtcField (boolean value) {
+      this.requestField("timezone_offset_hours_utc", value);
+      return this;
+    }
+    public APIRequestGetSharedAccounts requestTosAcceptedField () {
+      return this.requestTosAcceptedField(true);
+    }
+    public APIRequestGetSharedAccounts requestTosAcceptedField (boolean value) {
+      this.requestField("tos_accepted", value);
+      return this;
+    }
+    public APIRequestGetSharedAccounts requestUserRoleField () {
+      return this.requestUserRoleField(true);
+    }
+    public APIRequestGetSharedAccounts requestUserRoleField (boolean value) {
+      this.requestField("user_role", value);
       return this;
     }
-
   }
 
-  public static class APIRequestGetSharedAccounts extends APIRequest<AdAccount> {
+  public static class APIRequestGetSharedAgencies extends APIRequest<Business> {
 
-    APINodeList<AdAccount> lastResponse = null;
+    APINodeList<Business> lastResponse = null;
     @Override
-    public APINodeList<AdAccount> getLastResponse() {
+    public APINodeList<Business> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
-      "business",
     };
 
     public static final String[] FIELDS = {
       "id",
-      "account_groups",
-      "account_id",
-      "account_status",
-      "age",
-      "agency_client_declaration",
-      "business_city",
-      "business_country_code",
-      "business_name",
-      "business_state",
-      "business_street",
-      "business_street2",
-      "business_zip",
-      "capabilities",
-      "created_time",
-      "currency",
-      "disable_reason",
-      "end_advertiser",
-      "end_advertiser_name",
-      "failed_delivery_checks",
-      "funding_source",
-      "funding_source_details",
-      "has_migrated_permissions",
-      "io_number",
-      "is_notifications_enabled",
-      "is_personal",
-      "is_prepay_account",
-      "is_tax_id_required",
-      "line_numbers",
-      "media_agency",
-      "min_campaign_group_spend_cap",
-      "min_daily_budget",
       "name",
-      "owner",
-      "offsite_pixels_tos_accepted",
-      "partner",
-      "tax_id",
-      "tax_id_status",
-      "tax_id_type",
-      "timezone_id",
-      "timezone_name",
-      "timezone_offset_hours_utc",
-      "rf_spec",
-      "tos_accepted",
-      "user_role",
-      "vertical_name",
-      "amount_spent",
-      "spend_cap",
-      "balance",
-      "business",
-      "owner_business",
-      "last_used_time",
-      "asset_score",
+      "primary_page",
     };
 
     @Override
-    public APINodeList<AdAccount> parseResponse(String response) throws APIException {
-      return AdAccount.parseResponse(response, getContext(), this);
+    public APINodeList<Business> parseResponse(String response) throws APIException {
+      return Business.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINodeList<AdAccount> execute() throws APIException {
+    public APINodeList<Business> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<AdAccount> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINodeList<Business> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetSharedAccounts(String nodeId, APIContext context) {
-      super(context, nodeId, "/shared_accounts", "GET", Arrays.asList(PARAMS));
+    public APIRequestGetSharedAgencies(String nodeId, APIContext context) {
+      super(context, nodeId, "/shared_agencies", "GET", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetSharedAccounts setParam(String param, Object value) {
+    @Override
+    public APIRequestGetSharedAgencies setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetSharedAccounts setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestGetSharedAgencies setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetSharedAccounts setBusiness (String business) {
-      this.setParam("business", business);
-      return this;
-    }
-
-
-    public APIRequestGetSharedAccounts requestAllFields () {
+    public APIRequestGetSharedAgencies requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetSharedAccounts requestAllFields (boolean value) {
+    public APIRequestGetSharedAgencies requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetSharedAccounts requestFields (List<String> fields) {
+    @Override
+    public APIRequestGetSharedAgencies requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetSharedAccounts requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestGetSharedAgencies requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetSharedAccounts requestField (String field) {
+    @Override
+    public APIRequestGetSharedAgencies requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetSharedAccounts requestField (String field, boolean value) {
+    @Override
+    public APIRequestGetSharedAgencies requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetSharedAccounts requestIdField () {
+    public APIRequestGetSharedAgencies requestIdField () {
       return this.requestIdField(true);
     }
-    public APIRequestGetSharedAccounts requestIdField (boolean value) {
+    public APIRequestGetSharedAgencies requestIdField (boolean value) {
       this.requestField("id", value);
       return this;
     }
-    public APIRequestGetSharedAccounts requestAccountGroupsField () {
-      return this.requestAccountGroupsField(true);
-    }
-    public APIRequestGetSharedAccounts requestAccountGroupsField (boolean value) {
-      this.requestField("account_groups", value);
-      return this;
-    }
-    public APIRequestGetSharedAccounts requestAccountIdField () {
-      return this.requestAccountIdField(true);
-    }
-    public APIRequestGetSharedAccounts requestAccountIdField (boolean value) {
-      this.requestField("account_id", value);
-      return this;
-    }
-    public APIRequestGetSharedAccounts requestAccountStatusField () {
-      return this.requestAccountStatusField(true);
-    }
-    public APIRequestGetSharedAccounts requestAccountStatusField (boolean value) {
-      this.requestField("account_status", value);
-      return this;
-    }
-    public APIRequestGetSharedAccounts requestAgeField () {
-      return this.requestAgeField(true);
-    }
-    public APIRequestGetSharedAccounts requestAgeField (boolean value) {
-      this.requestField("age", value);
-      return this;
-    }
-    public APIRequestGetSharedAccounts requestAgencyClientDeclarationField () {
-      return this.requestAgencyClientDeclarationField(true);
-    }
-    public APIRequestGetSharedAccounts requestAgencyClientDeclarationField (boolean value) {
-      this.requestField("agency_client_declaration", value);
-      return this;
-    }
-    public APIRequestGetSharedAccounts requestBusinessCityField () {
-      return this.requestBusinessCityField(true);
-    }
-    public APIRequestGetSharedAccounts requestBusinessCityField (boolean value) {
-      this.requestField("business_city", value);
-      return this;
-    }
-    public APIRequestGetSharedAccounts requestBusinessCountryCodeField () {
-      return this.requestBusinessCountryCodeField(true);
-    }
-    public APIRequestGetSharedAccounts requestBusinessCountryCodeField (boolean value) {
-      this.requestField("business_country_code", value);
-      return this;
-    }
-    public APIRequestGetSharedAccounts requestBusinessNameField () {
-      return this.requestBusinessNameField(true);
-    }
-    public APIRequestGetSharedAccounts requestBusinessNameField (boolean value) {
-      this.requestField("business_name", value);
-      return this;
-    }
-    public APIRequestGetSharedAccounts requestBusinessStateField () {
-      return this.requestBusinessStateField(true);
-    }
-    public APIRequestGetSharedAccounts requestBusinessStateField (boolean value) {
-      this.requestField("business_state", value);
-      return this;
-    }
-    public APIRequestGetSharedAccounts requestBusinessStreetField () {
-      return this.requestBusinessStreetField(true);
-    }
-    public APIRequestGetSharedAccounts requestBusinessStreetField (boolean value) {
-      this.requestField("business_street", value);
-      return this;
-    }
-    public APIRequestGetSharedAccounts requestBusinessStreet2Field () {
-      return this.requestBusinessStreet2Field(true);
-    }
-    public APIRequestGetSharedAccounts requestBusinessStreet2Field (boolean value) {
-      this.requestField("business_street2", value);
-      return this;
-    }
-    public APIRequestGetSharedAccounts requestBusinessZipField () {
-      return this.requestBusinessZipField(true);
-    }
-    public APIRequestGetSharedAccounts requestBusinessZipField (boolean value) {
-      this.requestField("business_zip", value);
-      return this;
-    }
-    public APIRequestGetSharedAccounts requestCapabilitiesField () {
-      return this.requestCapabilitiesField(true);
-    }
-    public APIRequestGetSharedAccounts requestCapabilitiesField (boolean value) {
-      this.requestField("capabilities", value);
-      return this;
-    }
-    public APIRequestGetSharedAccounts requestCreatedTimeField () {
-      return this.requestCreatedTimeField(true);
-    }
-    public APIRequestGetSharedAccounts requestCreatedTimeField (boolean value) {
-      this.requestField("created_time", value);
-      return this;
-    }
-    public APIRequestGetSharedAccounts requestCurrencyField () {
-      return this.requestCurrencyField(true);
-    }
-    public APIRequestGetSharedAccounts requestCurrencyField (boolean value) {
-      this.requestField("currency", value);
-      return this;
-    }
-    public APIRequestGetSharedAccounts requestDisableReasonField () {
-      return this.requestDisableReasonField(true);
-    }
-    public APIRequestGetSharedAccounts requestDisableReasonField (boolean value) {
-      this.requestField("disable_reason", value);
-      return this;
-    }
-    public APIRequestGetSharedAccounts requestEndAdvertiserField () {
-      return this.requestEndAdvertiserField(true);
-    }
-    public APIRequestGetSharedAccounts requestEndAdvertiserField (boolean value) {
-      this.requestField("end_advertiser", value);
-      return this;
-    }
-    public APIRequestGetSharedAccounts requestEndAdvertiserNameField () {
-      return this.requestEndAdvertiserNameField(true);
-    }
-    public APIRequestGetSharedAccounts requestEndAdvertiserNameField (boolean value) {
-      this.requestField("end_advertiser_name", value);
-      return this;
-    }
-    public APIRequestGetSharedAccounts requestFailedDeliveryChecksField () {
-      return this.requestFailedDeliveryChecksField(true);
-    }
-    public APIRequestGetSharedAccounts requestFailedDeliveryChecksField (boolean value) {
-      this.requestField("failed_delivery_checks", value);
-      return this;
-    }
-    public APIRequestGetSharedAccounts requestFundingSourceField () {
-      return this.requestFundingSourceField(true);
-    }
-    public APIRequestGetSharedAccounts requestFundingSourceField (boolean value) {
-      this.requestField("funding_source", value);
-      return this;
-    }
-    public APIRequestGetSharedAccounts requestFundingSourceDetailsField () {
-      return this.requestFundingSourceDetailsField(true);
+    public APIRequestGetSharedAgencies requestNameField () {
+      return this.requestNameField(true);
     }
-    public APIRequestGetSharedAccounts requestFundingSourceDetailsField (boolean value) {
-      this.requestField("funding_source_details", value);
+    public APIRequestGetSharedAgencies requestNameField (boolean value) {
+      this.requestField("name", value);
       return this;
     }
-    public APIRequestGetSharedAccounts requestHasMigratedPermissionsField () {
-      return this.requestHasMigratedPermissionsField(true);
+    public APIRequestGetSharedAgencies requestPrimaryPageField () {
+      return this.requestPrimaryPageField(true);
     }
-    public APIRequestGetSharedAccounts requestHasMigratedPermissionsField (boolean value) {
-      this.requestField("has_migrated_permissions", value);
+    public APIRequestGetSharedAgencies requestPrimaryPageField (boolean value) {
+      this.requestField("primary_page", value);
       return this;
     }
-    public APIRequestGetSharedAccounts requestIoNumberField () {
-      return this.requestIoNumberField(true);
+  }
+
+  public static class APIRequestGetStats extends APIRequest<AdsPixelStatsResult> {
+
+    APINodeList<AdsPixelStatsResult> lastResponse = null;
+    @Override
+    public APINodeList<AdsPixelStatsResult> getLastResponse() {
+      return lastResponse;
     }
-    public APIRequestGetSharedAccounts requestIoNumberField (boolean value) {
-      this.requestField("io_number", value);
-      return this;
+    public static final String[] PARAMS = {
+      "aggregation",
+      "end_time",
+      "event",
+      "start_time",
+    };
+
+    public static final String[] FIELDS = {
+      "aggregation",
+      "data",
+      "timestamp",
+    };
+
+    @Override
+    public APINodeList<AdsPixelStatsResult> parseResponse(String response) throws APIException {
+      return AdsPixelStatsResult.parseResponse(response, getContext(), this);
     }
-    public APIRequestGetSharedAccounts requestIsNotificationsEnabledField () {
-      return this.requestIsNotificationsEnabledField(true);
+
+    @Override
+    public APINodeList<AdsPixelStatsResult> execute() throws APIException {
+      return execute(new HashMap<String, Object>());
     }
-    public APIRequestGetSharedAccounts requestIsNotificationsEnabledField (boolean value) {
-      this.requestField("is_notifications_enabled", value);
-      return this;
+
+    @Override
+    public APINodeList<AdsPixelStatsResult> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
     }
-    public APIRequestGetSharedAccounts requestIsPersonalField () {
-      return this.requestIsPersonalField(true);
+
+    public APIRequestGetStats(String nodeId, APIContext context) {
+      super(context, nodeId, "/stats", "GET", Arrays.asList(PARAMS));
     }
-    public APIRequestGetSharedAccounts requestIsPersonalField (boolean value) {
-      this.requestField("is_personal", value);
+
+    @Override
+    public APIRequestGetStats setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
-    public APIRequestGetSharedAccounts requestIsPrepayAccountField () {
-      return this.requestIsPrepayAccountField(true);
-    }
-    public APIRequestGetSharedAccounts requestIsPrepayAccountField (boolean value) {
-      this.requestField("is_prepay_account", value);
+
+    @Override
+    public APIRequestGetStats setParams(Map<String, Object> params) {
+      setParamsInternal(params);
       return this;
     }
-    public APIRequestGetSharedAccounts requestIsTaxIdRequiredField () {
-      return this.requestIsTaxIdRequiredField(true);
-    }
-    public APIRequestGetSharedAccounts requestIsTaxIdRequiredField (boolean value) {
-      this.requestField("is_tax_id_required", value);
+
+
+    public APIRequestGetStats setAggregation (AdsPixelStatsResult.EnumAggregation aggregation) {
+      this.setParam("aggregation", aggregation);
       return this;
     }
-    public APIRequestGetSharedAccounts requestLineNumbersField () {
-      return this.requestLineNumbersField(true);
-    }
-    public APIRequestGetSharedAccounts requestLineNumbersField (boolean value) {
-      this.requestField("line_numbers", value);
+    public APIRequestGetStats setAggregation (String aggregation) {
+      this.setParam("aggregation", aggregation);
       return this;
     }
-    public APIRequestGetSharedAccounts requestMediaAgencyField () {
-      return this.requestMediaAgencyField(true);
-    }
-    public APIRequestGetSharedAccounts requestMediaAgencyField (boolean value) {
-      this.requestField("media_agency", value);
+
+    public APIRequestGetStats setEndTime (String endTime) {
+      this.setParam("end_time", endTime);
       return this;
     }
-    public APIRequestGetSharedAccounts requestMinCampaignGroupSpendCapField () {
-      return this.requestMinCampaignGroupSpendCapField(true);
-    }
-    public APIRequestGetSharedAccounts requestMinCampaignGroupSpendCapField (boolean value) {
-      this.requestField("min_campaign_group_spend_cap", value);
+
+    public APIRequestGetStats setEvent (String event) {
+      this.setParam("event", event);
       return this;
     }
-    public APIRequestGetSharedAccounts requestMinDailyBudgetField () {
-      return this.requestMinDailyBudgetField(true);
-    }
-    public APIRequestGetSharedAccounts requestMinDailyBudgetField (boolean value) {
-      this.requestField("min_daily_budget", value);
+
+    public APIRequestGetStats setStartTime (String startTime) {
+      this.setParam("start_time", startTime);
       return this;
     }
-    public APIRequestGetSharedAccounts requestNameField () {
-      return this.requestNameField(true);
+
+    public APIRequestGetStats requestAllFields () {
+      return this.requestAllFields(true);
     }
-    public APIRequestGetSharedAccounts requestNameField (boolean value) {
-      this.requestField("name", value);
+
+    public APIRequestGetStats requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetSharedAccounts requestOwnerField () {
-      return this.requestOwnerField(true);
+
+    @Override
+    public APIRequestGetStats requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
-    public APIRequestGetSharedAccounts requestOwnerField (boolean value) {
-      this.requestField("owner", value);
+
+    @Override
+    public APIRequestGetStats requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetSharedAccounts requestOffsitePixelsTosAcceptedField () {
-      return this.requestOffsitePixelsTosAcceptedField(true);
-    }
-    public APIRequestGetSharedAccounts requestOffsitePixelsTosAcceptedField (boolean value) {
-      this.requestField("offsite_pixels_tos_accepted", value);
+
+    @Override
+    public APIRequestGetStats requestField (String field) {
+      this.requestField(field, true);
       return this;
     }
-    public APIRequestGetSharedAccounts requestPartnerField () {
-      return this.requestPartnerField(true);
-    }
-    public APIRequestGetSharedAccounts requestPartnerField (boolean value) {
-      this.requestField("partner", value);
+
+    @Override
+    public APIRequestGetStats requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
-    public APIRequestGetSharedAccounts requestTaxIdField () {
-      return this.requestTaxIdField(true);
+
+    public APIRequestGetStats requestAggregationField () {
+      return this.requestAggregationField(true);
     }
-    public APIRequestGetSharedAccounts requestTaxIdField (boolean value) {
-      this.requestField("tax_id", value);
+    public APIRequestGetStats requestAggregationField (boolean value) {
+      this.requestField("aggregation", value);
       return this;
     }
-    public APIRequestGetSharedAccounts requestTaxIdStatusField () {
-      return this.requestTaxIdStatusField(true);
+    public APIRequestGetStats requestDataField () {
+      return this.requestDataField(true);
     }
-    public APIRequestGetSharedAccounts requestTaxIdStatusField (boolean value) {
-      this.requestField("tax_id_status", value);
+    public APIRequestGetStats requestDataField (boolean value) {
+      this.requestField("data", value);
       return this;
     }
-    public APIRequestGetSharedAccounts requestTaxIdTypeField () {
-      return this.requestTaxIdTypeField(true);
+    public APIRequestGetStats requestTimestampField () {
+      return this.requestTimestampField(true);
     }
-    public APIRequestGetSharedAccounts requestTaxIdTypeField (boolean value) {
-      this.requestField("tax_id_type", value);
+    public APIRequestGetStats requestTimestampField (boolean value) {
+      this.requestField("timestamp", value);
       return this;
     }
-    public APIRequestGetSharedAccounts requestTimezoneIdField () {
-      return this.requestTimezoneIdField(true);
+  }
+
+  public static class APIRequestGet extends APIRequest<AdsPixel> {
+
+    AdsPixel lastResponse = null;
+    @Override
+    public AdsPixel getLastResponse() {
+      return lastResponse;
     }
-    public APIRequestGetSharedAccounts requestTimezoneIdField (boolean value) {
-      this.requestField("timezone_id", value);
-      return this;
+    public static final String[] PARAMS = {
+    };
+
+    public static final String[] FIELDS = {
+      "code",
+      "creation_time",
+      "id",
+      "last_fired_time",
+      "name",
+      "owner_ad_account",
+      "owner_business",
+    };
+
+    @Override
+    public AdsPixel parseResponse(String response) throws APIException {
+      return AdsPixel.parseResponse(response, getContext(), this).head();
     }
-    public APIRequestGetSharedAccounts requestTimezoneNameField () {
-      return this.requestTimezoneNameField(true);
+
+    @Override
+    public AdsPixel execute() throws APIException {
+      return execute(new HashMap<String, Object>());
     }
-    public APIRequestGetSharedAccounts requestTimezoneNameField (boolean value) {
-      this.requestField("timezone_name", value);
-      return this;
+
+    @Override
+    public AdsPixel execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
     }
-    public APIRequestGetSharedAccounts requestTimezoneOffsetHoursUtcField () {
-      return this.requestTimezoneOffsetHoursUtcField(true);
+
+    public APIRequestGet(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
-    public APIRequestGetSharedAccounts requestTimezoneOffsetHoursUtcField (boolean value) {
-      this.requestField("timezone_offset_hours_utc", value);
+
+    @Override
+    public APIRequestGet setParam(String param, Object value) {
+      setParamInternal(param, value);
       return this;
     }
-    public APIRequestGetSharedAccounts requestRfSpecField () {
-      return this.requestRfSpecField(true);
-    }
-    public APIRequestGetSharedAccounts requestRfSpecField (boolean value) {
-      this.requestField("rf_spec", value);
+
+    @Override
+    public APIRequestGet setParams(Map<String, Object> params) {
+      setParamsInternal(params);
       return this;
     }
-    public APIRequestGetSharedAccounts requestTosAcceptedField () {
-      return this.requestTosAcceptedField(true);
+
+
+    public APIRequestGet requestAllFields () {
+      return this.requestAllFields(true);
     }
-    public APIRequestGetSharedAccounts requestTosAcceptedField (boolean value) {
-      this.requestField("tos_accepted", value);
+
+    public APIRequestGet requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetSharedAccounts requestUserRoleField () {
-      return this.requestUserRoleField(true);
+
+    @Override
+    public APIRequestGet requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
     }
-    public APIRequestGetSharedAccounts requestUserRoleField (boolean value) {
-      this.requestField("user_role", value);
+
+    @Override
+    public APIRequestGet requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
       return this;
     }
-    public APIRequestGetSharedAccounts requestVerticalNameField () {
-      return this.requestVerticalNameField(true);
+
+    @Override
+    public APIRequestGet requestField (String field) {
+      this.requestField(field, true);
+      return this;
     }
-    public APIRequestGetSharedAccounts requestVerticalNameField (boolean value) {
-      this.requestField("vertical_name", value);
+
+    @Override
+    public APIRequestGet requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
       return this;
     }
-    public APIRequestGetSharedAccounts requestAmountSpentField () {
-      return this.requestAmountSpentField(true);
+
+    public APIRequestGet requestCodeField () {
+      return this.requestCodeField(true);
     }
-    public APIRequestGetSharedAccounts requestAmountSpentField (boolean value) {
-      this.requestField("amount_spent", value);
+    public APIRequestGet requestCodeField (boolean value) {
+      this.requestField("code", value);
       return this;
     }
-    public APIRequestGetSharedAccounts requestSpendCapField () {
-      return this.requestSpendCapField(true);
+    public APIRequestGet requestCreationTimeField () {
+      return this.requestCreationTimeField(true);
     }
-    public APIRequestGetSharedAccounts requestSpendCapField (boolean value) {
-      this.requestField("spend_cap", value);
+    public APIRequestGet requestCreationTimeField (boolean value) {
+      this.requestField("creation_time", value);
       return this;
     }
-    public APIRequestGetSharedAccounts requestBalanceField () {
-      return this.requestBalanceField(true);
+    public APIRequestGet requestIdField () {
+      return this.requestIdField(true);
     }
-    public APIRequestGetSharedAccounts requestBalanceField (boolean value) {
-      this.requestField("balance", value);
+    public APIRequestGet requestIdField (boolean value) {
+      this.requestField("id", value);
       return this;
     }
-    public APIRequestGetSharedAccounts requestBusinessField () {
-      return this.requestBusinessField(true);
+    public APIRequestGet requestLastFiredTimeField () {
+      return this.requestLastFiredTimeField(true);
     }
-    public APIRequestGetSharedAccounts requestBusinessField (boolean value) {
-      this.requestField("business", value);
+    public APIRequestGet requestLastFiredTimeField (boolean value) {
+      this.requestField("last_fired_time", value);
       return this;
     }
-    public APIRequestGetSharedAccounts requestOwnerBusinessField () {
-      return this.requestOwnerBusinessField(true);
+    public APIRequestGet requestNameField () {
+      return this.requestNameField(true);
     }
-    public APIRequestGetSharedAccounts requestOwnerBusinessField (boolean value) {
-      this.requestField("owner_business", value);
+    public APIRequestGet requestNameField (boolean value) {
+      this.requestField("name", value);
       return this;
     }
-    public APIRequestGetSharedAccounts requestLastUsedTimeField () {
-      return this.requestLastUsedTimeField(true);
+    public APIRequestGet requestOwnerAdAccountField () {
+      return this.requestOwnerAdAccountField(true);
     }
-    public APIRequestGetSharedAccounts requestLastUsedTimeField (boolean value) {
-      this.requestField("last_used_time", value);
+    public APIRequestGet requestOwnerAdAccountField (boolean value) {
+      this.requestField("owner_ad_account", value);
       return this;
     }
-    public APIRequestGetSharedAccounts requestAssetScoreField () {
-      return this.requestAssetScoreField(true);
+    public APIRequestGet requestOwnerBusinessField () {
+      return this.requestOwnerBusinessField(true);
     }
-    public APIRequestGetSharedAccounts requestAssetScoreField (boolean value) {
-      this.requestField("asset_score", value);
+    public APIRequestGet requestOwnerBusinessField (boolean value) {
+      this.requestField("owner_business", value);
       return this;
     }
-
   }
 
-  public static class APIRequestGetSharedAgencies extends APIRequest<Business> {
+  public static class APIRequestUpdate extends APIRequest<APINode> {
 
-    APINodeList<Business> lastResponse = null;
+    APINode lastResponse = null;
     @Override
-    public APINodeList<Business> getLastResponse() {
+    public APINode getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
+      "name",
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "name",
-      "primary_page",
     };
 
     @Override
-    public APINodeList<Business> parseResponse(String response) throws APIException {
-      return Business.parseResponse(response, getContext(), this);
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
     }
 
     @Override
-    public APINodeList<Business> execute() throws APIException {
+    public APINode execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<Business> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
-    public APIRequestGetSharedAgencies(String nodeId, APIContext context) {
-      super(context, nodeId, "/shared_agencies", "GET", Arrays.asList(PARAMS));
+    public APIRequestUpdate(String nodeId, APIContext context) {
+      super(context, nodeId, "/", "POST", Arrays.asList(PARAMS));
     }
 
-    public APIRequestGetSharedAgencies setParam(String param, Object value) {
+    @Override
+    public APIRequestUpdate setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
-    public APIRequestGetSharedAgencies setParams(Map<String, Object> params) {
+    @Override
+    public APIRequestUpdate setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
     }
 
 
-    public APIRequestGetSharedAgencies requestAllFields () {
+    public APIRequestUpdate setName (String name) {
+      this.setParam("name", name);
+      return this;
+    }
+
+    public APIRequestUpdate requestAllFields () {
       return this.requestAllFields(true);
     }
 
-    public APIRequestGetSharedAgencies requestAllFields (boolean value) {
+    public APIRequestUpdate requestAllFields (boolean value) {
       for (String field : FIELDS) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetSharedAgencies requestFields (List<String> fields) {
+    @Override
+    public APIRequestUpdate requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
-    public APIRequestGetSharedAgencies requestFields (List<String> fields, boolean value) {
+    @Override
+    public APIRequestUpdate requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
       }
       return this;
     }
 
-    public APIRequestGetSharedAgencies requestField (String field) {
+    @Override
+    public APIRequestUpdate requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
-    public APIRequestGetSharedAgencies requestField (String field, boolean value) {
+    @Override
+    public APIRequestUpdate requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGetSharedAgencies requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGetSharedAgencies requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
-    }
-    public APIRequestGetSharedAgencies requestNameField () {
-      return this.requestNameField(true);
-    }
-    public APIRequestGetSharedAgencies requestNameField (boolean value) {
-      this.requestField("name", value);
-      return this;
-    }
-    public APIRequestGetSharedAgencies requestPrimaryPageField () {
-      return this.requestPrimaryPageField(true);
-    }
-    public APIRequestGetSharedAgencies requestPrimaryPageField (boolean value) {
-      this.requestField("primary_page", value);
-      return this;
-    }
-
   }
 
-  public static enum EnumAggregation {
-    @SerializedName("browser_type")
-    VALUE_BROWSER_TYPE("browser_type"),
-    @SerializedName("custom_data_field")
-    VALUE_CUSTOM_DATA_FIELD("custom_data_field"),
-    @SerializedName("device_os")
-    VALUE_DEVICE_OS("device_os"),
-    @SerializedName("device_type")
-    VALUE_DEVICE_TYPE("device_type"),
-    @SerializedName("event")
-    VALUE_EVENT("event"),
-    @SerializedName("pixel_fire")
-    VALUE_PIXEL_FIRE("pixel_fire"),
-    @SerializedName("host")
-    VALUE_HOST("host"),
-    @SerializedName("user_match")
-    VALUE_USER_MATCH("user_match"),
-    @SerializedName("url")
-    VALUE_URL("url"),
-    NULL(null);
-
-    private String value;
-
-    private EnumAggregation(String value) {
-      this.value = value;
-    }
-
-    @Override
-    public String toString() {
-      return value;
-    }
-  }
 
   synchronized /*package*/ static Gson getGson() {
     if (gson != null) {
@@ -1520,21 +1569,21 @@ public class AdsPixel extends APINode {
   }
 
   public AdsPixel copyFrom(AdsPixel instance) {
-    this.mId = instance.mId;
-    this.mOwnerBusiness = instance.mOwnerBusiness;
-    this.mOwnerAdAccount = instance.mOwnerAdAccount;
-    this.mName = instance.mName;
+    this.mCode = instance.mCode;
     this.mCreationTime = instance.mCreationTime;
+    this.mId = instance.mId;
     this.mLastFiredTime = instance.mLastFiredTime;
-    this.mCode = instance.mCode;
-    this.mContext = instance.mContext;
+    this.mName = instance.mName;
+    this.mOwnerAdAccount = instance.mOwnerAdAccount;
+    this.mOwnerBusiness = instance.mOwnerBusiness;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AdsPixel> getParser() {
     return new APIRequest.ResponseParser<AdsPixel>() {
-      public APINodeList<AdsPixel> parseResponse(String response, APIContext context, APIRequest<AdsPixel> request) {
+      public APINodeList<AdsPixel> parseResponse(String response, APIContext context, APIRequest<AdsPixel> request) throws MalformedResponseException {
         return AdsPixel.parseResponse(response, context, request);
       }
     };
