@@ -23,43 +23,60 @@
 
 package com.facebook.ads.sdk;
 
-import com.google.gson.*;
-import com.google.gson.annotations.SerializedName;
-
+import java.io.File;
 import java.lang.reflect.Modifier;
+import java.lang.reflect.Type;
 import java.util.Arrays;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
+import com.google.gson.JsonObject;
+import com.google.gson.JsonArray;
+import com.google.gson.annotations.SerializedName;
+import com.google.gson.reflect.TypeToken;
+import com.google.gson.Gson;
+import com.google.gson.GsonBuilder;
+import com.google.gson.JsonElement;
+import com.google.gson.JsonParser;
+
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AdReportRun extends APINode {
-  @SerializedName(value = "id", alternate = {"report_run_id"})
-  private String mId = null;
-  @SerializedName("async_status")
-  private String mAsyncStatus = null;
-  @SerializedName("async_percent_completion")
-  private Long mAsyncPercentCompletion = null;
   @SerializedName("account_id")
   private String mAccountId = null;
-  @SerializedName("schedule_id")
-  private String mScheduleId = null;
-  @SerializedName("time_ref")
-  private Long mTimeRef = null;
-  @SerializedName("time_completed")
-  private Long mTimeCompleted = null;
+  @SerializedName("async_percent_completion")
+  private Long mAsyncPercentCompletion = null;
+  @SerializedName("async_status")
+  private String mAsyncStatus = null;
+  @SerializedName("date_start")
+  private String mDateStart = null;
+  @SerializedName("date_stop")
+  private String mDateStop = null;
   @SerializedName("emails")
   private List<String> mEmails = null;
   @SerializedName("friendly_name")
   private String mFriendlyName = null;
+  @SerializedName("id")
+  private String mId = null;
   @SerializedName("is_bookmarked")
   private Boolean mIsBookmarked = null;
   @SerializedName("is_running")
   private Boolean mIsRunning = null;
-  @SerializedName("date_start")
-  private String mDateStart = null;
-  @SerializedName("date_stop")
-  private String mDateStop = null;
+  @SerializedName("schedule_id")
+  private String mScheduleId = null;
+  @SerializedName("time_completed")
+  private Long mTimeCompleted = null;
+  @SerializedName("time_ref")
+  private Long mTimeRef = null;
   protected static Gson gson = null;
 
   AdReportRun() {
@@ -71,11 +88,11 @@ public class AdReportRun extends APINode {
 
   public AdReportRun(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public AdReportRun fetch() throws APIException{
-    AdReportRun newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    AdReportRun newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -92,8 +109,17 @@ public class AdReportRun extends APINode {
     return adReportRun;
   }
 
+  public static APINodeList<AdReportRun> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<AdReportRun>)(
+      new APIRequest<AdReportRun>(context, "", "/", "GET", AdReportRun.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -108,22 +134,32 @@ public class AdReportRun extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    adReportRun.mContext = context;
+    adReportRun.context = context;
     adReportRun.rawValue = json;
+    JsonParser parser = new JsonParser();
+    JsonObject o = parser.parse(json).getAsJsonObject();
+    String reportRunId = null;
+    if (o.has("report_run_id")) {
+      reportRunId = o.get("report_run_id").getAsString();
+      if (reportRunId != null) {
+        adReportRun.mId = reportRunId;
+      }
+    }
     return adReportRun;
   }
 
-  public static APINodeList<AdReportRun> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AdReportRun> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AdReportRun> adReportRuns = new APINodeList<AdReportRun>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -136,10 +172,11 @@ public class AdReportRun extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            adReportRuns.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            adReportRuns.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -150,7 +187,20 @@ public class AdReportRun extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            adReportRuns.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  adReportRuns.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              adReportRuns.add(loadJSON(obj.toString(), context));
+            }
           }
           return adReportRuns;
         } else if (obj.has("images")) {
@@ -161,24 +211,54 @@ public class AdReportRun extends APINode {
           }
           return adReportRuns;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              adReportRuns.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return adReportRuns;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          adReportRuns.clear();
           adReportRuns.add(loadJSON(json, context));
           return adReportRuns;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -186,41 +266,33 @@ public class AdReportRun extends APINode {
     return getGson().toJson(this);
   }
 
-  public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
-  }
-
   public APIRequestGetInsights getInsights() {
-    return new APIRequestGetInsights(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGetInsights(this.getPrefixedId().toString(), context);
   }
 
-
-  public String getFieldId() {
-    return mId;
+  public APIRequestGet get() {
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
   }
 
-  public String getFieldAsyncStatus() {
-    return mAsyncStatus;
+
+  public String getFieldAccountId() {
+    return mAccountId;
   }
 
   public Long getFieldAsyncPercentCompletion() {
     return mAsyncPercentCompletion;
   }
 
-  public String getFieldAccountId() {
-    return mAccountId;
-  }
-
-  public String getFieldScheduleId() {
-    return mScheduleId;
+  public String getFieldAsyncStatus() {
+    return mAsyncStatus;
   }
 
-  public Long getFieldTimeRef() {
-    return mTimeRef;
+  public String getFieldDateStart() {
+    return mDateStart;
   }
 
-  public Long getFieldTimeCompleted() {
-    return mTimeCompleted;
+  public String getFieldDateStop() {
+    return mDateStop;
   }
 
   public List<String> getFieldEmails() {
@@ -231,6 +303,10 @@ public class AdReportRun extends APINode {
     return mFriendlyName;
   }
 
+  public String getFieldId() {
+    return mId;
+  }
+
   public Boolean getFieldIsBookmarked() {
     return mIsBookmarked;
   }
@@ -239,16 +315,154 @@ public class AdReportRun extends APINode {
     return mIsRunning;
   }
 
-  public String getFieldDateStart() {
-    return mDateStart;
+  public String getFieldScheduleId() {
+    return mScheduleId;
   }
 
-  public String getFieldDateStop() {
-    return mDateStop;
+  public Long getFieldTimeCompleted() {
+    return mTimeCompleted;
+  }
+
+  public Long getFieldTimeRef() {
+    return mTimeRef;
   }
 
 
 
+  public static class APIRequestGetInsights extends APIRequest<AdsInsights> {
+
+    APINodeList<AdsInsights> lastResponse = null;
+    @Override
+    public APINodeList<AdsInsights> getLastResponse() {
+      return lastResponse;
+    }
+    public static final String[] PARAMS = {
+      "default_summary",
+      "fields",
+      "filtering",
+      "sort",
+      "summary",
+    };
+
+    public static final String[] FIELDS = {
+    };
+
+    @Override
+    public APINodeList<AdsInsights> parseResponse(String response) throws APIException {
+      return AdsInsights.parseResponse(response, getContext(), this);
+    }
+
+    @Override
+    public APINodeList<AdsInsights> execute() throws APIException {
+      return execute(new HashMap<String, Object>());
+    }
+
+    @Override
+    public APINodeList<AdsInsights> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
+    }
+
+    public APIRequestGetInsights(String nodeId, APIContext context) {
+      super(context, nodeId, "/insights", "GET", Arrays.asList(PARAMS));
+    }
+
+    @Override
+    public APIRequestGetInsights setParam(String param, Object value) {
+      setParamInternal(param, value);
+      return this;
+    }
+
+    @Override
+    public APIRequestGetInsights setParams(Map<String, Object> params) {
+      setParamsInternal(params);
+      return this;
+    }
+
+
+    public APIRequestGetInsights setDefaultSummary (Boolean defaultSummary) {
+      this.setParam("default_summary", defaultSummary);
+      return this;
+    }
+    public APIRequestGetInsights setDefaultSummary (String defaultSummary) {
+      this.setParam("default_summary", defaultSummary);
+      return this;
+    }
+
+    public APIRequestGetInsights setFields (List<AdsInsights.EnumSummary> fields) {
+      this.setParam("fields", fields);
+      return this;
+    }
+    public APIRequestGetInsights setFields (String fields) {
+      this.setParam("fields", fields);
+      return this;
+    }
+
+    public APIRequestGetInsights setFiltering (List<Object> filtering) {
+      this.setParam("filtering", filtering);
+      return this;
+    }
+    public APIRequestGetInsights setFiltering (String filtering) {
+      this.setParam("filtering", filtering);
+      return this;
+    }
+
+    public APIRequestGetInsights setSort (List<String> sort) {
+      this.setParam("sort", sort);
+      return this;
+    }
+    public APIRequestGetInsights setSort (String sort) {
+      this.setParam("sort", sort);
+      return this;
+    }
+
+    public APIRequestGetInsights setSummary (List<AdsInsights.EnumSummary> summary) {
+      this.setParam("summary", summary);
+      return this;
+    }
+    public APIRequestGetInsights setSummary (String summary) {
+      this.setParam("summary", summary);
+      return this;
+    }
+
+    public APIRequestGetInsights requestAllFields () {
+      return this.requestAllFields(true);
+    }
+
+    public APIRequestGetInsights requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestGetInsights requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
+    }
+
+    @Override
+    public APIRequestGetInsights requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestGetInsights requestField (String field) {
+      this.requestField(field, true);
+      return this;
+    }
+
+    @Override
+    public APIRequestGetInsights requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
+      return this;
+    }
+
+  }
+
   public static class APIRequestGet extends APIRequest<AdReportRun> {
 
     AdReportRun lastResponse = null;
@@ -260,19 +474,19 @@ public class AdReportRun extends APINode {
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "async_status",
-      "async_percent_completion",
       "account_id",
-      "schedule_id",
-      "time_ref",
-      "time_completed",
+      "async_percent_completion",
+      "async_status",
+      "date_start",
+      "date_stop",
       "emails",
       "friendly_name",
+      "id",
       "is_bookmarked",
       "is_running",
-      "date_start",
-      "date_stop",
+      "schedule_id",
+      "time_completed",
+      "time_ref",
     };
 
     @Override
@@ -287,7 +501,7 @@ public class AdReportRun extends APINode {
 
     @Override
     public AdReportRun execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -295,11 +509,13 @@ public class AdReportRun extends APINode {
       super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestGet setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestGet setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
@@ -317,10 +533,12 @@ public class AdReportRun extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGet requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestGet requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -328,28 +546,23 @@ public class AdReportRun extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGet requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGet requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGet requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGet requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
-    }
-    public APIRequestGet requestAsyncStatusField () {
-      return this.requestAsyncStatusField(true);
+    public APIRequestGet requestAccountIdField () {
+      return this.requestAccountIdField(true);
     }
-    public APIRequestGet requestAsyncStatusField (boolean value) {
-      this.requestField("async_status", value);
+    public APIRequestGet requestAccountIdField (boolean value) {
+      this.requestField("account_id", value);
       return this;
     }
     public APIRequestGet requestAsyncPercentCompletionField () {
@@ -359,32 +572,25 @@ public class AdReportRun extends APINode {
       this.requestField("async_percent_completion", value);
       return this;
     }
-    public APIRequestGet requestAccountIdField () {
-      return this.requestAccountIdField(true);
-    }
-    public APIRequestGet requestAccountIdField (boolean value) {
-      this.requestField("account_id", value);
-      return this;
-    }
-    public APIRequestGet requestScheduleIdField () {
-      return this.requestScheduleIdField(true);
+    public APIRequestGet requestAsyncStatusField () {
+      return this.requestAsyncStatusField(true);
     }
-    public APIRequestGet requestScheduleIdField (boolean value) {
-      this.requestField("schedule_id", value);
+    public APIRequestGet requestAsyncStatusField (boolean value) {
+      this.requestField("async_status", value);
       return this;
     }
-    public APIRequestGet requestTimeRefField () {
-      return this.requestTimeRefField(true);
+    public APIRequestGet requestDateStartField () {
+      return this.requestDateStartField(true);
     }
-    public APIRequestGet requestTimeRefField (boolean value) {
-      this.requestField("time_ref", value);
+    public APIRequestGet requestDateStartField (boolean value) {
+      this.requestField("date_start", value);
       return this;
     }
-    public APIRequestGet requestTimeCompletedField () {
-      return this.requestTimeCompletedField(true);
+    public APIRequestGet requestDateStopField () {
+      return this.requestDateStopField(true);
     }
-    public APIRequestGet requestTimeCompletedField (boolean value) {
-      this.requestField("time_completed", value);
+    public APIRequestGet requestDateStopField (boolean value) {
+      this.requestField("date_stop", value);
       return this;
     }
     public APIRequestGet requestEmailsField () {
@@ -401,6 +607,13 @@ public class AdReportRun extends APINode {
       this.requestField("friendly_name", value);
       return this;
     }
+    public APIRequestGet requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGet requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
     public APIRequestGet requestIsBookmarkedField () {
       return this.requestIsBookmarkedField(true);
     }
@@ -415,322 +628,284 @@ public class AdReportRun extends APINode {
       this.requestField("is_running", value);
       return this;
     }
-    public APIRequestGet requestDateStartField () {
-      return this.requestDateStartField(true);
+    public APIRequestGet requestScheduleIdField () {
+      return this.requestScheduleIdField(true);
     }
-    public APIRequestGet requestDateStartField (boolean value) {
-      this.requestField("date_start", value);
+    public APIRequestGet requestScheduleIdField (boolean value) {
+      this.requestField("schedule_id", value);
       return this;
     }
-    public APIRequestGet requestDateStopField () {
-      return this.requestDateStopField(true);
+    public APIRequestGet requestTimeCompletedField () {
+      return this.requestTimeCompletedField(true);
     }
-    public APIRequestGet requestDateStopField (boolean value) {
-      this.requestField("date_stop", value);
+    public APIRequestGet requestTimeCompletedField (boolean value) {
+      this.requestField("time_completed", value);
       return this;
     }
-
-  }
-
-  public static class APIRequestGetInsights extends APIRequest<AdsInsights> {
-
-    APINodeList<AdsInsights> lastResponse = null;
-    @Override
-    public APINodeList<AdsInsights> getLastResponse() {
-      return lastResponse;
-    }
-    public static final String[] PARAMS = {
-      "default_summary",
-      "fields",
-      "filtering",
-      "summary",
-      "sort",
-    };
-
-    public static final String[] FIELDS = {
-    };
-
-    @Override
-    public APINodeList<AdsInsights> parseResponse(String response) throws APIException {
-      return AdsInsights.parseResponse(response, getContext(), this);
-    }
-
-    @Override
-    public APINodeList<AdsInsights> execute() throws APIException {
-      return execute(new HashMap<String, Object>());
-    }
-
-    @Override
-    public APINodeList<AdsInsights> execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
-      return lastResponse;
-    }
-
-    public APIRequestGetInsights(String nodeId, APIContext context) {
-      super(context, nodeId, "/insights", "GET", Arrays.asList(PARAMS));
+    public APIRequestGet requestTimeRefField () {
+      return this.requestTimeRefField(true);
     }
-
-    public APIRequestGetInsights setParam(String param, Object value) {
-      setParamInternal(param, value);
+    public APIRequestGet requestTimeRefField (boolean value) {
+      this.requestField("time_ref", value);
       return this;
     }
+  }
 
-    public APIRequestGetInsights setParams(Map<String, Object> params) {
-      setParamsInternal(params);
-      return this;
-    }
+  public static enum EnumActionAttributionWindows {
+      @SerializedName("1d_view")
+      VALUE_1D_VIEW("1d_view"),
+      @SerializedName("7d_view")
+      VALUE_7D_VIEW("7d_view"),
+      @SerializedName("28d_view")
+      VALUE_28D_VIEW("28d_view"),
+      @SerializedName("1d_click")
+      VALUE_1D_CLICK("1d_click"),
+      @SerializedName("7d_click")
+      VALUE_7D_CLICK("7d_click"),
+      @SerializedName("28d_click")
+      VALUE_28D_CLICK("28d_click"),
+      @SerializedName("default")
+      VALUE_DEFAULT("default"),
+      NULL(null);
 
+      private String value;
 
-    public APIRequestGetInsights setDefaultSummary (Boolean defaultSummary) {
-      this.setParam("default_summary", defaultSummary);
-      return this;
-    }
+      private EnumActionAttributionWindows(String value) {
+        this.value = value;
+      }
 
-    public APIRequestGetInsights setDefaultSummary (String defaultSummary) {
-      this.setParam("default_summary", defaultSummary);
-      return this;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
+  }
 
-    public APIRequestGetInsights setFields (List<EnumFields> fields) {
-      this.setParam("fields", fields);
-      return this;
-    }
+  public static enum EnumActionBreakdowns {
+      @SerializedName("action_carousel_card_id")
+      VALUE_ACTION_CAROUSEL_CARD_ID("action_carousel_card_id"),
+      @SerializedName("action_carousel_card_name")
+      VALUE_ACTION_CAROUSEL_CARD_NAME("action_carousel_card_name"),
+      @SerializedName("action_destination")
+      VALUE_ACTION_DESTINATION("action_destination"),
+      @SerializedName("action_device")
+      VALUE_ACTION_DEVICE("action_device"),
+      @SerializedName("action_target_id")
+      VALUE_ACTION_TARGET_ID("action_target_id"),
+      @SerializedName("action_type")
+      VALUE_ACTION_TYPE("action_type"),
+      @SerializedName("action_video_type")
+      VALUE_ACTION_VIDEO_TYPE("action_video_type"),
+      NULL(null);
+
+      private String value;
+
+      private EnumActionBreakdowns(String value) {
+        this.value = value;
+      }
 
-    public APIRequestGetInsights setFields (String fields) {
-      this.setParam("fields", fields);
-      return this;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
+  }
 
-    public APIRequestGetInsights setFiltering (List<Object> filtering) {
-      this.setParam("filtering", filtering);
-      return this;
-    }
+  public static enum EnumActionReportTime {
+      @SerializedName("impression")
+      VALUE_IMPRESSION("impression"),
+      @SerializedName("conversion")
+      VALUE_CONVERSION("conversion"),
+      NULL(null);
 
-    public APIRequestGetInsights setFiltering (String filtering) {
-      this.setParam("filtering", filtering);
-      return this;
-    }
+      private String value;
 
-    public APIRequestGetInsights setSummary (List<EnumFields> summary) {
-      this.setParam("summary", summary);
-      return this;
-    }
+      private EnumActionReportTime(String value) {
+        this.value = value;
+      }
 
-    public APIRequestGetInsights setSummary (String summary) {
-      this.setParam("summary", summary);
-      return this;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
+  }
 
-    public APIRequestGetInsights setSort (List<String> sort) {
-      this.setParam("sort", sort);
-      return this;
-    }
+  public static enum EnumBreakdowns {
+      @SerializedName("age")
+      VALUE_AGE("age"),
+      @SerializedName("country")
+      VALUE_COUNTRY("country"),
+      @SerializedName("gender")
+      VALUE_GENDER("gender"),
+      @SerializedName("frequency_value")
+      VALUE_FREQUENCY_VALUE("frequency_value"),
+      @SerializedName("hourly_stats_aggregated_by_advertiser_time_zone")
+      VALUE_HOURLY_STATS_AGGREGATED_BY_ADVERTISER_TIME_ZONE("hourly_stats_aggregated_by_advertiser_time_zone"),
+      @SerializedName("hourly_stats_aggregated_by_audience_time_zone")
+      VALUE_HOURLY_STATS_AGGREGATED_BY_AUDIENCE_TIME_ZONE("hourly_stats_aggregated_by_audience_time_zone"),
+      @SerializedName("impression_device")
+      VALUE_IMPRESSION_DEVICE("impression_device"),
+      @SerializedName("place_page_id")
+      VALUE_PLACE_PAGE_ID("place_page_id"),
+      @SerializedName("placement")
+      VALUE_PLACEMENT("placement"),
+      @SerializedName("placement_merge_rhc")
+      VALUE_PLACEMENT_MERGE_RHC("placement_merge_rhc"),
+      @SerializedName("product_id")
+      VALUE_PRODUCT_ID("product_id"),
+      @SerializedName("region")
+      VALUE_REGION("region"),
+      NULL(null);
+
+      private String value;
+
+      private EnumBreakdowns(String value) {
+        this.value = value;
+      }
 
-    public APIRequestGetInsights setSort (String sort) {
-      this.setParam("sort", sort);
-      return this;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
+  }
 
-    public APIRequestGetInsights requestAllFields () {
-      return this.requestAllFields(true);
-    }
+  public static enum EnumDatePreset {
+      @SerializedName("today")
+      VALUE_TODAY("today"),
+      @SerializedName("yesterday")
+      VALUE_YESTERDAY("yesterday"),
+      @SerializedName("last_3_days")
+      VALUE_LAST_3_DAYS("last_3_days"),
+      @SerializedName("this_week")
+      VALUE_THIS_WEEK("this_week"),
+      @SerializedName("last_week")
+      VALUE_LAST_WEEK("last_week"),
+      @SerializedName("last_7_days")
+      VALUE_LAST_7_DAYS("last_7_days"),
+      @SerializedName("last_14_days")
+      VALUE_LAST_14_DAYS("last_14_days"),
+      @SerializedName("last_28_days")
+      VALUE_LAST_28_DAYS("last_28_days"),
+      @SerializedName("last_30_days")
+      VALUE_LAST_30_DAYS("last_30_days"),
+      @SerializedName("last_90_days")
+      VALUE_LAST_90_DAYS("last_90_days"),
+      @SerializedName("this_month")
+      VALUE_THIS_MONTH("this_month"),
+      @SerializedName("last_month")
+      VALUE_LAST_MONTH("last_month"),
+      @SerializedName("this_quarter")
+      VALUE_THIS_QUARTER("this_quarter"),
+      @SerializedName("last_3_months")
+      VALUE_LAST_3_MONTHS("last_3_months"),
+      @SerializedName("lifetime")
+      VALUE_LIFETIME("lifetime"),
+      NULL(null);
+
+      private String value;
+
+      private EnumDatePreset(String value) {
+        this.value = value;
+      }
 
-    public APIRequestGetInsights requestAllFields (boolean value) {
-      for (String field : FIELDS) {
-        this.requestField(field, value);
+      @Override
+      public String toString() {
+        return value;
       }
-      return this;
-    }
+  }
 
-    public APIRequestGetInsights requestFields (List<String> fields) {
-      return this.requestFields(fields, true);
-    }
+  public static enum EnumSummary {
+      @SerializedName("id")
+      VALUE_ID("id"),
+      @SerializedName("account_id")
+      VALUE_ACCOUNT_ID("account_id"),
+      @SerializedName("async_percent_completion")
+      VALUE_ASYNC_PERCENT_COMPLETION("async_percent_completion"),
+      @SerializedName("async_status")
+      VALUE_ASYNC_STATUS("async_status"),
+      @SerializedName("date_start")
+      VALUE_DATE_START("date_start"),
+      @SerializedName("date_stop")
+      VALUE_DATE_STOP("date_stop"),
+      @SerializedName("emails")
+      VALUE_EMAILS("emails"),
+      @SerializedName("friendly_name")
+      VALUE_FRIENDLY_NAME("friendly_name"),
+      @SerializedName("is_bookmarked")
+      VALUE_IS_BOOKMARKED("is_bookmarked"),
+      @SerializedName("is_running")
+      VALUE_IS_RUNNING("is_running"),
+      @SerializedName("schedule_id")
+      VALUE_SCHEDULE_ID("schedule_id"),
+      @SerializedName("time_completed")
+      VALUE_TIME_COMPLETED("time_completed"),
+      @SerializedName("time_ref")
+      VALUE_TIME_REF("time_ref"),
+      NULL(null);
+
+      private String value;
+
+      private EnumSummary(String value) {
+        this.value = value;
+      }
 
-    public APIRequestGetInsights requestFields (List<String> fields, boolean value) {
-      for (String field : fields) {
-        this.requestField(field, value);
+      @Override
+      public String toString() {
+        return value;
       }
-      return this;
-    }
+  }
 
-    public APIRequestGetInsights requestField (String field) {
-      this.requestField(field, true);
-      return this;
-    }
+  public static enum EnumLevel {
+      @SerializedName("ad")
+      VALUE_AD("ad"),
+      @SerializedName("adset")
+      VALUE_ADSET("adset"),
+      @SerializedName("campaign")
+      VALUE_CAMPAIGN("campaign"),
+      @SerializedName("account")
+      VALUE_ACCOUNT("account"),
+      NULL(null);
 
-    public APIRequestGetInsights requestField (String field, boolean value) {
-      this.requestFieldInternal(field, value);
-      return this;
-    }
+      private String value;
 
+      private EnumLevel(String value) {
+        this.value = value;
+      }
 
+      @Override
+      public String toString() {
+        return value;
+      }
   }
 
-  public static enum EnumFields {
-    @SerializedName("frequency_value")
-    VALUE_FREQUENCY_VALUE("frequency_value"),
-    @SerializedName("age")
-    VALUE_AGE("age"),
-    @SerializedName("country")
-    VALUE_COUNTRY("country"),
-    @SerializedName("gender")
-    VALUE_GENDER("gender"),
-    @SerializedName("impression_device")
-    VALUE_IMPRESSION_DEVICE("impression_device"),
-    @SerializedName("place_page_id")
-    VALUE_PLACE_PAGE_ID("place_page_id"),
-    @SerializedName("placement")
-    VALUE_PLACEMENT("placement"),
-    @SerializedName("product_id")
-    VALUE_PRODUCT_ID("product_id"),
-    @SerializedName("region")
-    VALUE_REGION("region"),
-    @SerializedName("hourly_stats_aggregated_by_advertiser_time_zone")
-    VALUE_HOURLY_STATS_AGGREGATED_BY_ADVERTISER_TIME_ZONE("hourly_stats_aggregated_by_advertiser_time_zone"),
-    @SerializedName("hourly_stats_aggregated_by_audience_time_zone")
-    VALUE_HOURLY_STATS_AGGREGATED_BY_AUDIENCE_TIME_ZONE("hourly_stats_aggregated_by_audience_time_zone"),
-    @SerializedName("account_id")
-    VALUE_ACCOUNT_ID("account_id"),
-    @SerializedName("account_name")
-    VALUE_ACCOUNT_NAME("account_name"),
-    @SerializedName("action_values")
-    VALUE_ACTION_VALUES("action_values"),
-    @SerializedName("actions")
-    VALUE_ACTIONS("actions"),
-    @SerializedName("unique_actions")
-    VALUE_UNIQUE_ACTIONS("unique_actions"),
-    @SerializedName("app_store_clicks")
-    VALUE_APP_STORE_CLICKS("app_store_clicks"),
-    @SerializedName("buying_type")
-    VALUE_BUYING_TYPE("buying_type"),
-    @SerializedName("call_to_action_clicks")
-    VALUE_CALL_TO_ACTION_CLICKS("call_to_action_clicks"),
-    @SerializedName("card_views")
-    VALUE_CARD_VIEWS("card_views"),
-    @SerializedName("unique_clicks")
-    VALUE_UNIQUE_CLICKS("unique_clicks"),
-    @SerializedName("cost_per_action_type")
-    VALUE_COST_PER_ACTION_TYPE("cost_per_action_type"),
-    @SerializedName("cost_per_unique_action_type")
-    VALUE_COST_PER_UNIQUE_ACTION_TYPE("cost_per_unique_action_type"),
-    @SerializedName("cost_per_inline_post_engagement")
-    VALUE_COST_PER_INLINE_POST_ENGAGEMENT("cost_per_inline_post_engagement"),
-    @SerializedName("cost_per_inline_link_click")
-    VALUE_COST_PER_INLINE_LINK_CLICK("cost_per_inline_link_click"),
-    @SerializedName("cost_per_total_action")
-    VALUE_COST_PER_TOTAL_ACTION("cost_per_total_action"),
-    @SerializedName("cost_per_10_sec_video_view")
-    VALUE_COST_PER_10_SEC_VIDEO_VIEW("cost_per_10_sec_video_view"),
-    @SerializedName("cost_per_unique_click")
-    VALUE_COST_PER_UNIQUE_CLICK("cost_per_unique_click"),
-    @SerializedName("cpm")
-    VALUE_CPM("cpm"),
-    @SerializedName("cpp")
-    VALUE_CPP("cpp"),
-    @SerializedName("ctr")
-    VALUE_CTR("ctr"),
-    @SerializedName("unique_ctr")
-    VALUE_UNIQUE_CTR("unique_ctr"),
-    @SerializedName("unique_link_clicks_ctr")
-    VALUE_UNIQUE_LINK_CLICKS_CTR("unique_link_clicks_ctr"),
-    @SerializedName("date_start")
-    VALUE_DATE_START("date_start"),
-    @SerializedName("date_stop")
-    VALUE_DATE_STOP("date_stop"),
-    @SerializedName("deeplink_clicks")
-    VALUE_DEEPLINK_CLICKS("deeplink_clicks"),
-    @SerializedName("frequency")
-    VALUE_FREQUENCY("frequency"),
-    @SerializedName("impressions")
-    VALUE_IMPRESSIONS("impressions"),
-    @SerializedName("unique_impressions")
-    VALUE_UNIQUE_IMPRESSIONS("unique_impressions"),
-    @SerializedName("inline_link_clicks")
-    VALUE_INLINE_LINK_CLICKS("inline_link_clicks"),
-    @SerializedName("inline_post_engagement")
-    VALUE_INLINE_POST_ENGAGEMENT("inline_post_engagement"),
-    @SerializedName("newsfeed_avg_position")
-    VALUE_NEWSFEED_AVG_POSITION("newsfeed_avg_position"),
-    @SerializedName("newsfeed_clicks")
-    VALUE_NEWSFEED_CLICKS("newsfeed_clicks"),
-    @SerializedName("newsfeed_impressions")
-    VALUE_NEWSFEED_IMPRESSIONS("newsfeed_impressions"),
-    @SerializedName("reach")
-    VALUE_REACH("reach"),
-    @SerializedName("relevance_score")
-    VALUE_RELEVANCE_SCORE("relevance_score"),
-    @SerializedName("social_clicks")
-    VALUE_SOCIAL_CLICKS("social_clicks"),
-    @SerializedName("unique_social_clicks")
-    VALUE_UNIQUE_SOCIAL_CLICKS("unique_social_clicks"),
-    @SerializedName("social_impressions")
-    VALUE_SOCIAL_IMPRESSIONS("social_impressions"),
-    @SerializedName("unique_social_impressions")
-    VALUE_UNIQUE_SOCIAL_IMPRESSIONS("unique_social_impressions"),
-    @SerializedName("social_reach")
-    VALUE_SOCIAL_REACH("social_reach"),
-    @SerializedName("social_spend")
-    VALUE_SOCIAL_SPEND("social_spend"),
-    @SerializedName("spend")
-    VALUE_SPEND("spend"),
-    @SerializedName("total_action_value")
-    VALUE_TOTAL_ACTION_VALUE("total_action_value"),
-    @SerializedName("total_actions")
-    VALUE_TOTAL_ACTIONS("total_actions"),
-    @SerializedName("total_unique_actions")
-    VALUE_TOTAL_UNIQUE_ACTIONS("total_unique_actions"),
-    @SerializedName("video_avg_pct_watched_actions")
-    VALUE_VIDEO_AVG_PCT_WATCHED_ACTIONS("video_avg_pct_watched_actions"),
-    @SerializedName("video_avg_sec_watched_actions")
-    VALUE_VIDEO_AVG_SEC_WATCHED_ACTIONS("video_avg_sec_watched_actions"),
-    @SerializedName("video_complete_watched_actions")
-    VALUE_VIDEO_COMPLETE_WATCHED_ACTIONS("video_complete_watched_actions"),
-    @SerializedName("video_p25_watched_actions")
-    VALUE_VIDEO_P25_WATCHED_ACTIONS("video_p25_watched_actions"),
-    @SerializedName("video_p50_watched_actions")
-    VALUE_VIDEO_P50_WATCHED_ACTIONS("video_p50_watched_actions"),
-    @SerializedName("video_p75_watched_actions")
-    VALUE_VIDEO_P75_WATCHED_ACTIONS("video_p75_watched_actions"),
-    @SerializedName("video_p95_watched_actions")
-    VALUE_VIDEO_P95_WATCHED_ACTIONS("video_p95_watched_actions"),
-    @SerializedName("video_p100_watched_actions")
-    VALUE_VIDEO_P100_WATCHED_ACTIONS("video_p100_watched_actions"),
-    @SerializedName("video_10_sec_watched_actions")
-    VALUE_VIDEO_10_SEC_WATCHED_ACTIONS("video_10_sec_watched_actions"),
-    @SerializedName("video_15_sec_watched_actions")
-    VALUE_VIDEO_15_SEC_WATCHED_ACTIONS("video_15_sec_watched_actions"),
-    @SerializedName("video_30_sec_watched_actions")
-    VALUE_VIDEO_30_SEC_WATCHED_ACTIONS("video_30_sec_watched_actions"),
-    @SerializedName("website_clicks")
-    VALUE_WEBSITE_CLICKS("website_clicks"),
-    @SerializedName("website_ctr")
-    VALUE_WEBSITE_CTR("website_ctr"),
-    @SerializedName("ad_id")
-    VALUE_AD_ID("ad_id"),
-    @SerializedName("ad_name")
-    VALUE_AD_NAME("ad_name"),
-    @SerializedName("adset_id")
-    VALUE_ADSET_ID("adset_id"),
-    @SerializedName("adset_name")
-    VALUE_ADSET_NAME("adset_name"),
-    @SerializedName("campaign_id")
-    VALUE_CAMPAIGN_ID("campaign_id"),
-    @SerializedName("campaign_name")
-    VALUE_CAMPAIGN_NAME("campaign_name"),
-    NULL(null);
-
-    private String value;
-
-    private EnumFields(String value) {
-      this.value = value;
-    }
+  public static enum EnumSummaryActionBreakdowns {
+      @SerializedName("action_carousel_card_id")
+      VALUE_ACTION_CAROUSEL_CARD_ID("action_carousel_card_id"),
+      @SerializedName("action_carousel_card_name")
+      VALUE_ACTION_CAROUSEL_CARD_NAME("action_carousel_card_name"),
+      @SerializedName("action_destination")
+      VALUE_ACTION_DESTINATION("action_destination"),
+      @SerializedName("action_device")
+      VALUE_ACTION_DEVICE("action_device"),
+      @SerializedName("action_target_id")
+      VALUE_ACTION_TARGET_ID("action_target_id"),
+      @SerializedName("action_type")
+      VALUE_ACTION_TYPE("action_type"),
+      @SerializedName("action_video_type")
+      VALUE_ACTION_VIDEO_TYPE("action_video_type"),
+      NULL(null);
+
+      private String value;
+
+      private EnumSummaryActionBreakdowns(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
 
+
   synchronized /*package*/ static Gson getGson() {
     if (gson != null) {
       return gson;
@@ -745,27 +920,27 @@ public class AdReportRun extends APINode {
   }
 
   public AdReportRun copyFrom(AdReportRun instance) {
-    this.mId = instance.mId;
-    this.mAsyncStatus = instance.mAsyncStatus;
-    this.mAsyncPercentCompletion = instance.mAsyncPercentCompletion;
     this.mAccountId = instance.mAccountId;
-    this.mScheduleId = instance.mScheduleId;
-    this.mTimeRef = instance.mTimeRef;
-    this.mTimeCompleted = instance.mTimeCompleted;
+    this.mAsyncPercentCompletion = instance.mAsyncPercentCompletion;
+    this.mAsyncStatus = instance.mAsyncStatus;
+    this.mDateStart = instance.mDateStart;
+    this.mDateStop = instance.mDateStop;
     this.mEmails = instance.mEmails;
     this.mFriendlyName = instance.mFriendlyName;
+    this.mId = instance.mId;
     this.mIsBookmarked = instance.mIsBookmarked;
     this.mIsRunning = instance.mIsRunning;
-    this.mDateStart = instance.mDateStart;
-    this.mDateStop = instance.mDateStop;
-    this.mContext = instance.mContext;
+    this.mScheduleId = instance.mScheduleId;
+    this.mTimeCompleted = instance.mTimeCompleted;
+    this.mTimeRef = instance.mTimeRef;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AdReportRun> getParser() {
     return new APIRequest.ResponseParser<AdReportRun>() {
-      public APINodeList<AdReportRun> parseResponse(String response, APIContext context, APIRequest<AdReportRun> request) {
+      public APINodeList<AdReportRun> parseResponse(String response, APIContext context, APIRequest<AdReportRun> request) throws MalformedResponseException {
         return AdReportRun.parseResponse(response, context, request);
       }
     };
