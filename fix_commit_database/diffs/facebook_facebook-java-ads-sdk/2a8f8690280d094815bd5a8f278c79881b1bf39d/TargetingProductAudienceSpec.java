@@ -24,36 +24,39 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class TargetingProductAudienceSpec extends APINode {
-  @SerializedName("product_set_id")
-  private String mProductSetId = null;
-  @SerializedName("inclusions")
-  private List<TargetingProductAudienceSubSpec> mInclusions = null;
   @SerializedName("exclusions")
   private List<TargetingProductAudienceSubSpec> mExclusions = null;
+  @SerializedName("inclusions")
+  private List<TargetingProductAudienceSubSpec> mInclusions = null;
+  @SerializedName("product_set_id")
+  private String mProductSetId = null;
   protected static Gson gson = null;
 
   public TargetingProductAudienceSpec() {
@@ -71,22 +74,23 @@ public class TargetingProductAudienceSpec extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    targetingProductAudienceSpec.mContext = context;
+    targetingProductAudienceSpec.context = context;
     targetingProductAudienceSpec.rawValue = json;
     return targetingProductAudienceSpec;
   }
 
-  public static APINodeList<TargetingProductAudienceSpec> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<TargetingProductAudienceSpec> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<TargetingProductAudienceSpec> targetingProductAudienceSpecs = new APINodeList<TargetingProductAudienceSpec>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -99,10 +103,11 @@ public class TargetingProductAudienceSpec extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            targetingProductAudienceSpecs.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            targetingProductAudienceSpecs.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -113,7 +118,20 @@ public class TargetingProductAudienceSpec extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            targetingProductAudienceSpecs.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  targetingProductAudienceSpecs.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              targetingProductAudienceSpecs.add(loadJSON(obj.toString(), context));
+            }
           }
           return targetingProductAudienceSpecs;
         } else if (obj.has("images")) {
@@ -124,24 +142,54 @@ public class TargetingProductAudienceSpec extends APINode {
           }
           return targetingProductAudienceSpecs;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              targetingProductAudienceSpecs.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return targetingProductAudienceSpecs;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          targetingProductAudienceSpecs.clear();
           targetingProductAudienceSpecs.add(loadJSON(json, context));
           return targetingProductAudienceSpecs;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -150,15 +198,20 @@ public class TargetingProductAudienceSpec extends APINode {
   }
 
 
-  public String getFieldProductSetId() {
-    return mProductSetId;
+  public List<TargetingProductAudienceSubSpec> getFieldExclusions() {
+    return mExclusions;
   }
 
-  public TargetingProductAudienceSpec setFieldProductSetId(String value) {
-    this.mProductSetId = value;
+  public TargetingProductAudienceSpec setFieldExclusions(List<TargetingProductAudienceSubSpec> value) {
+    this.mExclusions = value;
     return this;
   }
 
+  public TargetingProductAudienceSpec setFieldExclusions(String value) {
+    Type type = new TypeToken<List<TargetingProductAudienceSubSpec>>(){}.getType();
+    this.mExclusions = TargetingProductAudienceSubSpec.getGson().fromJson(value, type);
+    return this;
+  }
   public List<TargetingProductAudienceSubSpec> getFieldInclusions() {
     return mInclusions;
   }
@@ -173,20 +226,15 @@ public class TargetingProductAudienceSpec extends APINode {
     this.mInclusions = TargetingProductAudienceSubSpec.getGson().fromJson(value, type);
     return this;
   }
-  public List<TargetingProductAudienceSubSpec> getFieldExclusions() {
-    return mExclusions;
+  public String getFieldProductSetId() {
+    return mProductSetId;
   }
 
-  public TargetingProductAudienceSpec setFieldExclusions(List<TargetingProductAudienceSubSpec> value) {
-    this.mExclusions = value;
+  public TargetingProductAudienceSpec setFieldProductSetId(String value) {
+    this.mProductSetId = value;
     return this;
   }
 
-  public TargetingProductAudienceSpec setFieldExclusions(String value) {
-    Type type = new TypeToken<List<TargetingProductAudienceSubSpec>>(){}.getType();
-    this.mExclusions = TargetingProductAudienceSubSpec.getGson().fromJson(value, type);
-    return this;
-  }
 
 
 
@@ -204,17 +252,17 @@ public class TargetingProductAudienceSpec extends APINode {
   }
 
   public TargetingProductAudienceSpec copyFrom(TargetingProductAudienceSpec instance) {
-    this.mProductSetId = instance.mProductSetId;
-    this.mInclusions = instance.mInclusions;
     this.mExclusions = instance.mExclusions;
-    this.mContext = instance.mContext;
+    this.mInclusions = instance.mInclusions;
+    this.mProductSetId = instance.mProductSetId;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<TargetingProductAudienceSpec> getParser() {
     return new APIRequest.ResponseParser<TargetingProductAudienceSpec>() {
-      public APINodeList<TargetingProductAudienceSpec> parseResponse(String response, APIContext context, APIRequest<TargetingProductAudienceSpec> request) {
+      public APINodeList<TargetingProductAudienceSpec> parseResponse(String response, APIContext context, APIRequest<TargetingProductAudienceSpec> request) throws MalformedResponseException {
         return TargetingProductAudienceSpec.parseResponse(response, context, request);
       }
     };
