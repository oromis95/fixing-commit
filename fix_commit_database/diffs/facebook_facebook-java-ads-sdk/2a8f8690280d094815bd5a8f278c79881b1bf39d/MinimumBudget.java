@@ -24,38 +24,43 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class MinimumBudget extends APINode {
   @SerializedName("currency")
   private String mCurrency = null;
-  @SerializedName("min_daily_budget_imp")
-  private Long mMinDailyBudgetImp = null;
   @SerializedName("min_daily_budget_high_freq")
   private Long mMinDailyBudgetHighFreq = null;
+  @SerializedName("min_daily_budget_imp")
+  private Long mMinDailyBudgetImp = null;
   @SerializedName("min_daily_budget_low_freq")
   private Long mMinDailyBudgetLowFreq = null;
+  @SerializedName("min_daily_budget_video_views")
+  private Long mMinDailyBudgetVideoViews = null;
   protected static Gson gson = null;
 
   public MinimumBudget() {
@@ -73,22 +78,23 @@ public class MinimumBudget extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    minimumBudget.mContext = context;
+    minimumBudget.context = context;
     minimumBudget.rawValue = json;
     return minimumBudget;
   }
 
-  public static APINodeList<MinimumBudget> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<MinimumBudget> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<MinimumBudget> minimumBudgets = new APINodeList<MinimumBudget>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -101,10 +107,11 @@ public class MinimumBudget extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            minimumBudgets.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            minimumBudgets.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -115,7 +122,20 @@ public class MinimumBudget extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            minimumBudgets.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  minimumBudgets.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              minimumBudgets.add(loadJSON(obj.toString(), context));
+            }
           }
           return minimumBudgets;
         } else if (obj.has("images")) {
@@ -126,24 +146,54 @@ public class MinimumBudget extends APINode {
           }
           return minimumBudgets;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              minimumBudgets.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return minimumBudgets;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          minimumBudgets.clear();
           minimumBudgets.add(loadJSON(json, context));
           return minimumBudgets;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -161,21 +211,21 @@ public class MinimumBudget extends APINode {
     return this;
   }
 
-  public Long getFieldMinDailyBudgetImp() {
-    return mMinDailyBudgetImp;
+  public Long getFieldMinDailyBudgetHighFreq() {
+    return mMinDailyBudgetHighFreq;
   }
 
-  public MinimumBudget setFieldMinDailyBudgetImp(Long value) {
-    this.mMinDailyBudgetImp = value;
+  public MinimumBudget setFieldMinDailyBudgetHighFreq(Long value) {
+    this.mMinDailyBudgetHighFreq = value;
     return this;
   }
 
-  public Long getFieldMinDailyBudgetHighFreq() {
-    return mMinDailyBudgetHighFreq;
+  public Long getFieldMinDailyBudgetImp() {
+    return mMinDailyBudgetImp;
   }
 
-  public MinimumBudget setFieldMinDailyBudgetHighFreq(Long value) {
-    this.mMinDailyBudgetHighFreq = value;
+  public MinimumBudget setFieldMinDailyBudgetImp(Long value) {
+    this.mMinDailyBudgetImp = value;
     return this;
   }
 
@@ -188,6 +238,15 @@ public class MinimumBudget extends APINode {
     return this;
   }
 
+  public Long getFieldMinDailyBudgetVideoViews() {
+    return mMinDailyBudgetVideoViews;
+  }
+
+  public MinimumBudget setFieldMinDailyBudgetVideoViews(Long value) {
+    this.mMinDailyBudgetVideoViews = value;
+    return this;
+  }
+
 
 
 
@@ -206,17 +265,18 @@ public class MinimumBudget extends APINode {
 
   public MinimumBudget copyFrom(MinimumBudget instance) {
     this.mCurrency = instance.mCurrency;
-    this.mMinDailyBudgetImp = instance.mMinDailyBudgetImp;
     this.mMinDailyBudgetHighFreq = instance.mMinDailyBudgetHighFreq;
+    this.mMinDailyBudgetImp = instance.mMinDailyBudgetImp;
     this.mMinDailyBudgetLowFreq = instance.mMinDailyBudgetLowFreq;
-    this.mContext = instance.mContext;
+    this.mMinDailyBudgetVideoViews = instance.mMinDailyBudgetVideoViews;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<MinimumBudget> getParser() {
     return new APIRequest.ResponseParser<MinimumBudget>() {
-      public APINodeList<MinimumBudget> parseResponse(String response, APIContext context, APIRequest<MinimumBudget> request) {
+      public APINodeList<MinimumBudget> parseResponse(String response, APIContext context, APIRequest<MinimumBudget> request) throws MalformedResponseException {
         return MinimumBudget.parseResponse(response, context, request);
       }
     };
