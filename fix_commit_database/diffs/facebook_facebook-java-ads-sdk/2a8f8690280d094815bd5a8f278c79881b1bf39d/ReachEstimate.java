@@ -24,38 +24,41 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class ReachEstimate extends APINode {
-  @SerializedName("users")
-  private Long mUsers = null;
   @SerializedName("bid_estimations")
   private JsonArray mBidEstimations = null;
   @SerializedName("estimate_ready")
   private Boolean mEstimateReady = null;
   @SerializedName("unsupported")
   private Boolean mUnsupported = null;
+  @SerializedName("users")
+  private Long mUsers = null;
   protected static Gson gson = null;
 
   public ReachEstimate() {
@@ -73,22 +76,23 @@ public class ReachEstimate extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    reachEstimate.mContext = context;
+    reachEstimate.context = context;
     reachEstimate.rawValue = json;
     return reachEstimate;
   }
 
-  public static APINodeList<ReachEstimate> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<ReachEstimate> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<ReachEstimate> reachEstimates = new APINodeList<ReachEstimate>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -101,10 +105,11 @@ public class ReachEstimate extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            reachEstimates.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            reachEstimates.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -115,7 +120,20 @@ public class ReachEstimate extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            reachEstimates.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  reachEstimates.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              reachEstimates.add(loadJSON(obj.toString(), context));
+            }
           }
           return reachEstimates;
         } else if (obj.has("images")) {
@@ -126,24 +144,54 @@ public class ReachEstimate extends APINode {
           }
           return reachEstimates;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              reachEstimates.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return reachEstimates;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          reachEstimates.clear();
           reachEstimates.add(loadJSON(json, context));
           return reachEstimates;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -152,15 +200,6 @@ public class ReachEstimate extends APINode {
   }
 
 
-  public Long getFieldUsers() {
-    return mUsers;
-  }
-
-  public ReachEstimate setFieldUsers(Long value) {
-    this.mUsers = value;
-    return this;
-  }
-
   public JsonArray getFieldBidEstimations() {
     return mBidEstimations;
   }
@@ -188,8 +227,68 @@ public class ReachEstimate extends APINode {
     return this;
   }
 
+  public Long getFieldUsers() {
+    return mUsers;
+  }
+
+  public ReachEstimate setFieldUsers(Long value) {
+    this.mUsers = value;
+    return this;
+  }
+
 
 
+  public static enum EnumOptimizeFor {
+      @SerializedName("NONE")
+      VALUE_NONE("NONE"),
+      @SerializedName("APP_INSTALLS")
+      VALUE_APP_INSTALLS("APP_INSTALLS"),
+      @SerializedName("BRAND_AWARENESS")
+      VALUE_BRAND_AWARENESS("BRAND_AWARENESS"),
+      @SerializedName("CLICKS")
+      VALUE_CLICKS("CLICKS"),
+      @SerializedName("ENGAGED_USERS")
+      VALUE_ENGAGED_USERS("ENGAGED_USERS"),
+      @SerializedName("EXTERNAL")
+      VALUE_EXTERNAL("EXTERNAL"),
+      @SerializedName("EVENT_RESPONSES")
+      VALUE_EVENT_RESPONSES("EVENT_RESPONSES"),
+      @SerializedName("IMPRESSIONS")
+      VALUE_IMPRESSIONS("IMPRESSIONS"),
+      @SerializedName("LEAD_GENERATION")
+      VALUE_LEAD_GENERATION("LEAD_GENERATION"),
+      @SerializedName("LINK_CLICKS")
+      VALUE_LINK_CLICKS("LINK_CLICKS"),
+      @SerializedName("OFFER_CLAIMS")
+      VALUE_OFFER_CLAIMS("OFFER_CLAIMS"),
+      @SerializedName("OFFSITE_CONVERSIONS")
+      VALUE_OFFSITE_CONVERSIONS("OFFSITE_CONVERSIONS"),
+      @SerializedName("PAGE_ENGAGEMENT")
+      VALUE_PAGE_ENGAGEMENT("PAGE_ENGAGEMENT"),
+      @SerializedName("PAGE_LIKES")
+      VALUE_PAGE_LIKES("PAGE_LIKES"),
+      @SerializedName("POST_ENGAGEMENT")
+      VALUE_POST_ENGAGEMENT("POST_ENGAGEMENT"),
+      @SerializedName("REACH")
+      VALUE_REACH("REACH"),
+      @SerializedName("SOCIAL_IMPRESSIONS")
+      VALUE_SOCIAL_IMPRESSIONS("SOCIAL_IMPRESSIONS"),
+      @SerializedName("VIDEO_VIEWS")
+      VALUE_VIDEO_VIEWS("VIDEO_VIEWS"),
+      NULL(null);
+
+      private String value;
+
+      private EnumOptimizeFor(String value) {
+        this.value = value;
+      }
+
+      @Override
+      public String toString() {
+        return value;
+      }
+  }
+
 
   synchronized /*package*/ static Gson getGson() {
     if (gson != null) {
@@ -205,18 +304,18 @@ public class ReachEstimate extends APINode {
   }
 
   public ReachEstimate copyFrom(ReachEstimate instance) {
-    this.mUsers = instance.mUsers;
     this.mBidEstimations = instance.mBidEstimations;
     this.mEstimateReady = instance.mEstimateReady;
     this.mUnsupported = instance.mUnsupported;
-    this.mContext = instance.mContext;
+    this.mUsers = instance.mUsers;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<ReachEstimate> getParser() {
     return new APIRequest.ResponseParser<ReachEstimate>() {
-      public APINodeList<ReachEstimate> parseResponse(String response, APIContext context, APIRequest<ReachEstimate> request) {
+      public APINodeList<ReachEstimate> parseResponse(String response, APIContext context, APIRequest<ReachEstimate> request) throws MalformedResponseException {
         return ReachEstimate.parseResponse(response, context, request);
       }
     };
