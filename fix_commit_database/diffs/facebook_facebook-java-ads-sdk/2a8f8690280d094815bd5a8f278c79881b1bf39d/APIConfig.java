@@ -23,8 +23,9 @@
 
 package com.facebook.ads.sdk;
 
-class APIConfig {
-  public static final String DEFAULT_API_VERSION = "v2.5";
+public class APIConfig {
+  public static final String DEFAULT_API_VERSION = "v2.6";
   public static final String DEFAULT_API_BASE = "https://graph.facebook.com";
-  public static final String USER_AGENT = "fb-java-ads-api-sdk-v2.5";
+  public static final String DEFAULT_VIDEO_API_BASE = "https://graph-video.facebook.com";
+  public static final String USER_AGENT = "fb-java-ads-api-sdk-v2.6.0";
 };
