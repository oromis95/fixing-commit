@@ -24,46 +24,49 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AdCreativeOfferData extends APINode {
-  @SerializedName("title")
-  private String mTitle = null;
-  @SerializedName("message")
-  private String mMessage = null;
-  @SerializedName("image_url")
-  private String mImageUrl = null;
+  @SerializedName("claim_limit")
+  private Long mClaimLimit = null;
   @SerializedName("coupon_type")
   private String mCouponType = null;
   @SerializedName("expiration_time")
   private String mExpirationTime = null;
-  @SerializedName("reminder_time")
-  private String mReminderTime = null;
-  @SerializedName("claim_limit")
-  private Long mClaimLimit = null;
+  @SerializedName("image_url")
+  private String mImageUrl = null;
+  @SerializedName("message")
+  private String mMessage = null;
   @SerializedName("redemption_link")
   private String mRedemptionLink = null;
+  @SerializedName("reminder_time")
+  private String mReminderTime = null;
+  @SerializedName("title")
+  private String mTitle = null;
   protected static Gson gson = null;
 
   public AdCreativeOfferData() {
@@ -81,22 +84,23 @@ public class AdCreativeOfferData extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    adCreativeOfferData.mContext = context;
+    adCreativeOfferData.context = context;
     adCreativeOfferData.rawValue = json;
     return adCreativeOfferData;
   }
 
-  public static APINodeList<AdCreativeOfferData> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AdCreativeOfferData> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AdCreativeOfferData> adCreativeOfferDatas = new APINodeList<AdCreativeOfferData>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -109,10 +113,11 @@ public class AdCreativeOfferData extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            adCreativeOfferDatas.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            adCreativeOfferDatas.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -123,7 +128,20 @@ public class AdCreativeOfferData extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            adCreativeOfferDatas.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  adCreativeOfferDatas.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              adCreativeOfferDatas.add(loadJSON(obj.toString(), context));
+            }
           }
           return adCreativeOfferDatas;
         } else if (obj.has("images")) {
@@ -134,24 +152,54 @@ public class AdCreativeOfferData extends APINode {
           }
           return adCreativeOfferDatas;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              adCreativeOfferDatas.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return adCreativeOfferDatas;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          adCreativeOfferDatas.clear();
           adCreativeOfferDatas.add(loadJSON(json, context));
           return adCreativeOfferDatas;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -160,21 +208,30 @@ public class AdCreativeOfferData extends APINode {
   }
 
 
-  public String getFieldTitle() {
-    return mTitle;
+  public Long getFieldClaimLimit() {
+    return mClaimLimit;
   }
 
-  public AdCreativeOfferData setFieldTitle(String value) {
-    this.mTitle = value;
+  public AdCreativeOfferData setFieldClaimLimit(Long value) {
+    this.mClaimLimit = value;
     return this;
   }
 
-  public String getFieldMessage() {
-    return mMessage;
+  public String getFieldCouponType() {
+    return mCouponType;
   }
 
-  public AdCreativeOfferData setFieldMessage(String value) {
-    this.mMessage = value;
+  public AdCreativeOfferData setFieldCouponType(String value) {
+    this.mCouponType = value;
+    return this;
+  }
+
+  public String getFieldExpirationTime() {
+    return mExpirationTime;
+  }
+
+  public AdCreativeOfferData setFieldExpirationTime(String value) {
+    this.mExpirationTime = value;
     return this;
   }
 
@@ -187,21 +244,21 @@ public class AdCreativeOfferData extends APINode {
     return this;
   }
 
-  public String getFieldCouponType() {
-    return mCouponType;
+  public String getFieldMessage() {
+    return mMessage;
   }
 
-  public AdCreativeOfferData setFieldCouponType(String value) {
-    this.mCouponType = value;
+  public AdCreativeOfferData setFieldMessage(String value) {
+    this.mMessage = value;
     return this;
   }
 
-  public String getFieldExpirationTime() {
-    return mExpirationTime;
+  public String getFieldRedemptionLink() {
+    return mRedemptionLink;
   }
 
-  public AdCreativeOfferData setFieldExpirationTime(String value) {
-    this.mExpirationTime = value;
+  public AdCreativeOfferData setFieldRedemptionLink(String value) {
+    this.mRedemptionLink = value;
     return this;
   }
 
@@ -214,21 +271,12 @@ public class AdCreativeOfferData extends APINode {
     return this;
   }
 
-  public Long getFieldClaimLimit() {
-    return mClaimLimit;
-  }
-
-  public AdCreativeOfferData setFieldClaimLimit(Long value) {
-    this.mClaimLimit = value;
-    return this;
-  }
-
-  public String getFieldRedemptionLink() {
-    return mRedemptionLink;
+  public String getFieldTitle() {
+    return mTitle;
   }
 
-  public AdCreativeOfferData setFieldRedemptionLink(String value) {
-    this.mRedemptionLink = value;
+  public AdCreativeOfferData setFieldTitle(String value) {
+    this.mTitle = value;
     return this;
   }
 
@@ -249,22 +297,22 @@ public class AdCreativeOfferData extends APINode {
   }
 
   public AdCreativeOfferData copyFrom(AdCreativeOfferData instance) {
-    this.mTitle = instance.mTitle;
-    this.mMessage = instance.mMessage;
-    this.mImageUrl = instance.mImageUrl;
+    this.mClaimLimit = instance.mClaimLimit;
     this.mCouponType = instance.mCouponType;
     this.mExpirationTime = instance.mExpirationTime;
-    this.mReminderTime = instance.mReminderTime;
-    this.mClaimLimit = instance.mClaimLimit;
+    this.mImageUrl = instance.mImageUrl;
+    this.mMessage = instance.mMessage;
     this.mRedemptionLink = instance.mRedemptionLink;
-    this.mContext = instance.mContext;
+    this.mReminderTime = instance.mReminderTime;
+    this.mTitle = instance.mTitle;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AdCreativeOfferData> getParser() {
     return new APIRequest.ResponseParser<AdCreativeOfferData>() {
-      public APINodeList<AdCreativeOfferData> parseResponse(String response, APIContext context, APIRequest<AdCreativeOfferData> request) {
+      public APINodeList<AdCreativeOfferData> parseResponse(String response, APIContext context, APIRequest<AdCreativeOfferData> request) throws MalformedResponseException {
         return AdCreativeOfferData.parseResponse(response, context, request);
       }
     };
