@@ -24,66 +24,71 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AdCreativeLinkData extends APINode {
-  @SerializedName("link")
-  private String mLink = null;
-  @SerializedName("message")
-  private String mMessage = null;
-  @SerializedName("name")
-  private String mName = null;
-  @SerializedName("caption")
-  private String mCaption = null;
+  @SerializedName("additional_image_index")
+  private Long mAdditionalImageIndex = null;
+  @SerializedName("app_link_spec")
+  private AdCreativeLinkDataAppLinkSpec mAppLinkSpec = null;
   @SerializedName("attachment_style")
   private EnumAttachmentStyle mAttachmentStyle = null;
-  @SerializedName("description")
-  private String mDescription = null;
-  @SerializedName("image_hash")
-  private String mImageHash = null;
+  @SerializedName("branded_content_sponsor_page_id")
+  private String mBrandedContentSponsorPageId = null;
   @SerializedName("call_to_action")
   private AdCreativeLinkDataCallToAction mCallToAction = null;
-  @SerializedName("image_crops")
-  private AdsImageCrops mImageCrops = null;
-  @SerializedName("picture")
-  private String mPicture = null;
+  @SerializedName("canvas_enabled")
+  private Boolean mCanvasEnabled = null;
+  @SerializedName("caption")
+  private String mCaption = null;
   @SerializedName("child_attachments")
   private List<AdCreativeLinkDataChildAttachment> mChildAttachments = null;
+  @SerializedName("description")
+  private String mDescription = null;
+  @SerializedName("event_id")
+  private String mEventId = null;
+  @SerializedName("force_single_link")
+  private Boolean mForceSingleLink = null;
+  @SerializedName("image_crops")
+  private AdsImageCrops mImageCrops = null;
+  @SerializedName("image_hash")
+  private String mImageHash = null;
+  @SerializedName("link")
+  private String mLink = null;
+  @SerializedName("message")
+  private String mMessage = null;
   @SerializedName("multi_share_end_card")
   private Boolean mMultiShareEndCard = null;
   @SerializedName("multi_share_optimized")
   private Boolean mMultiShareOptimized = null;
-  @SerializedName("max_product_count")
-  private Long mMaxProductCount = null;
-  @SerializedName("additional_image_index")
-  private Long mAdditionalImageIndex = null;
-  @SerializedName("app_link_spec")
-  private AdCreativeLinkDataAppLinkSpec mAppLinkSpec = null;
-  @SerializedName("event_id")
-  private String mEventId = null;
-  @SerializedName("canvas_enabled")
-  private Boolean mCanvasEnabled = null;
+  @SerializedName("name")
+  private String mName = null;
+  @SerializedName("picture")
+  private String mPicture = null;
   protected static Gson gson = null;
 
   public AdCreativeLinkData() {
@@ -101,22 +106,23 @@ public class AdCreativeLinkData extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    adCreativeLinkData.mContext = context;
+    adCreativeLinkData.context = context;
     adCreativeLinkData.rawValue = json;
     return adCreativeLinkData;
   }
 
-  public static APINodeList<AdCreativeLinkData> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AdCreativeLinkData> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AdCreativeLinkData> adCreativeLinkDatas = new APINodeList<AdCreativeLinkData>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -129,10 +135,11 @@ public class AdCreativeLinkData extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            adCreativeLinkDatas.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            adCreativeLinkDatas.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -143,7 +150,20 @@ public class AdCreativeLinkData extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            adCreativeLinkDatas.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  adCreativeLinkDatas.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              adCreativeLinkDatas.add(loadJSON(obj.toString(), context));
+            }
           }
           return adCreativeLinkDatas;
         } else if (obj.has("images")) {
@@ -154,24 +174,54 @@ public class AdCreativeLinkData extends APINode {
           }
           return adCreativeLinkDatas;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              adCreativeLinkDatas.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return adCreativeLinkDatas;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          adCreativeLinkDatas.clear();
           adCreativeLinkDatas.add(loadJSON(json, context));
           return adCreativeLinkDatas;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -180,30 +230,67 @@ public class AdCreativeLinkData extends APINode {
   }
 
 
-  public String getFieldLink() {
-    return mLink;
+  public Long getFieldAdditionalImageIndex() {
+    return mAdditionalImageIndex;
   }
 
-  public AdCreativeLinkData setFieldLink(String value) {
-    this.mLink = value;
+  public AdCreativeLinkData setFieldAdditionalImageIndex(Long value) {
+    this.mAdditionalImageIndex = value;
     return this;
   }
 
-  public String getFieldMessage() {
-    return mMessage;
+  public AdCreativeLinkDataAppLinkSpec getFieldAppLinkSpec() {
+    return mAppLinkSpec;
   }
 
-  public AdCreativeLinkData setFieldMessage(String value) {
-    this.mMessage = value;
+  public AdCreativeLinkData setFieldAppLinkSpec(AdCreativeLinkDataAppLinkSpec value) {
+    this.mAppLinkSpec = value;
     return this;
   }
 
-  public String getFieldName() {
-    return mName;
+  public AdCreativeLinkData setFieldAppLinkSpec(String value) {
+    Type type = new TypeToken<AdCreativeLinkDataAppLinkSpec>(){}.getType();
+    this.mAppLinkSpec = AdCreativeLinkDataAppLinkSpec.getGson().fromJson(value, type);
+    return this;
+  }
+  public EnumAttachmentStyle getFieldAttachmentStyle() {
+    return mAttachmentStyle;
   }
 
-  public AdCreativeLinkData setFieldName(String value) {
-    this.mName = value;
+  public AdCreativeLinkData setFieldAttachmentStyle(EnumAttachmentStyle value) {
+    this.mAttachmentStyle = value;
+    return this;
+  }
+
+  public String getFieldBrandedContentSponsorPageId() {
+    return mBrandedContentSponsorPageId;
+  }
+
+  public AdCreativeLinkData setFieldBrandedContentSponsorPageId(String value) {
+    this.mBrandedContentSponsorPageId = value;
+    return this;
+  }
+
+  public AdCreativeLinkDataCallToAction getFieldCallToAction() {
+    return mCallToAction;
+  }
+
+  public AdCreativeLinkData setFieldCallToAction(AdCreativeLinkDataCallToAction value) {
+    this.mCallToAction = value;
+    return this;
+  }
+
+  public AdCreativeLinkData setFieldCallToAction(String value) {
+    Type type = new TypeToken<AdCreativeLinkDataCallToAction>(){}.getType();
+    this.mCallToAction = AdCreativeLinkDataCallToAction.getGson().fromJson(value, type);
+    return this;
+  }
+  public Boolean getFieldCanvasEnabled() {
+    return mCanvasEnabled;
+  }
+
+  public AdCreativeLinkData setFieldCanvasEnabled(Boolean value) {
+    this.mCanvasEnabled = value;
     return this;
   }
 
@@ -216,15 +303,20 @@ public class AdCreativeLinkData extends APINode {
     return this;
   }
 
-  public EnumAttachmentStyle getFieldAttachmentStyle() {
-    return mAttachmentStyle;
+  public List<AdCreativeLinkDataChildAttachment> getFieldChildAttachments() {
+    return mChildAttachments;
   }
 
-  public AdCreativeLinkData setFieldAttachmentStyle(EnumAttachmentStyle value) {
-    this.mAttachmentStyle = value;
+  public AdCreativeLinkData setFieldChildAttachments(List<AdCreativeLinkDataChildAttachment> value) {
+    this.mChildAttachments = value;
     return this;
   }
 
+  public AdCreativeLinkData setFieldChildAttachments(String value) {
+    Type type = new TypeToken<List<AdCreativeLinkDataChildAttachment>>(){}.getType();
+    this.mChildAttachments = AdCreativeLinkDataChildAttachment.getGson().fromJson(value, type);
+    return this;
+  }
   public String getFieldDescription() {
     return mDescription;
   }
@@ -234,29 +326,24 @@ public class AdCreativeLinkData extends APINode {
     return this;
   }
 
-  public String getFieldImageHash() {
-    return mImageHash;
+  public String getFieldEventId() {
+    return mEventId;
   }
 
-  public AdCreativeLinkData setFieldImageHash(String value) {
-    this.mImageHash = value;
+  public AdCreativeLinkData setFieldEventId(String value) {
+    this.mEventId = value;
     return this;
   }
 
-  public AdCreativeLinkDataCallToAction getFieldCallToAction() {
-    return mCallToAction;
+  public Boolean getFieldForceSingleLink() {
+    return mForceSingleLink;
   }
 
-  public AdCreativeLinkData setFieldCallToAction(AdCreativeLinkDataCallToAction value) {
-    this.mCallToAction = value;
+  public AdCreativeLinkData setFieldForceSingleLink(Boolean value) {
+    this.mForceSingleLink = value;
     return this;
   }
 
-  public AdCreativeLinkData setFieldCallToAction(String value) {
-    Type type = new TypeToken<AdCreativeLinkDataCallToAction>(){}.getType();
-    this.mCallToAction = AdCreativeLinkDataCallToAction.getGson().fromJson(value, type);
-    return this;
-  }
   public AdsImageCrops getFieldImageCrops() {
     return mImageCrops;
   }
@@ -271,29 +358,33 @@ public class AdCreativeLinkData extends APINode {
     this.mImageCrops = AdsImageCrops.getGson().fromJson(value, type);
     return this;
   }
-  public String getFieldPicture() {
-    return mPicture;
+  public String getFieldImageHash() {
+    return mImageHash;
   }
 
-  public AdCreativeLinkData setFieldPicture(String value) {
-    this.mPicture = value;
+  public AdCreativeLinkData setFieldImageHash(String value) {
+    this.mImageHash = value;
     return this;
   }
 
-  public List<AdCreativeLinkDataChildAttachment> getFieldChildAttachments() {
-    return mChildAttachments;
+  public String getFieldLink() {
+    return mLink;
   }
 
-  public AdCreativeLinkData setFieldChildAttachments(List<AdCreativeLinkDataChildAttachment> value) {
-    this.mChildAttachments = value;
+  public AdCreativeLinkData setFieldLink(String value) {
+    this.mLink = value;
     return this;
   }
 
-  public AdCreativeLinkData setFieldChildAttachments(String value) {
-    Type type = new TypeToken<List<AdCreativeLinkDataChildAttachment>>(){}.getType();
-    this.mChildAttachments = AdCreativeLinkDataChildAttachment.getGson().fromJson(value, type);
+  public String getFieldMessage() {
+    return mMessage;
+  }
+
+  public AdCreativeLinkData setFieldMessage(String value) {
+    this.mMessage = value;
     return this;
   }
+
   public Boolean getFieldMultiShareEndCard() {
     return mMultiShareEndCard;
   }
@@ -312,77 +403,46 @@ public class AdCreativeLinkData extends APINode {
     return this;
   }
 
-  public Long getFieldMaxProductCount() {
-    return mMaxProductCount;
-  }
-
-  public AdCreativeLinkData setFieldMaxProductCount(Long value) {
-    this.mMaxProductCount = value;
-    return this;
-  }
-
-  public Long getFieldAdditionalImageIndex() {
-    return mAdditionalImageIndex;
-  }
-
-  public AdCreativeLinkData setFieldAdditionalImageIndex(Long value) {
-    this.mAdditionalImageIndex = value;
-    return this;
-  }
-
-  public AdCreativeLinkDataAppLinkSpec getFieldAppLinkSpec() {
-    return mAppLinkSpec;
-  }
-
-  public AdCreativeLinkData setFieldAppLinkSpec(AdCreativeLinkDataAppLinkSpec value) {
-    this.mAppLinkSpec = value;
-    return this;
-  }
-
-  public AdCreativeLinkData setFieldAppLinkSpec(String value) {
-    Type type = new TypeToken<AdCreativeLinkDataAppLinkSpec>(){}.getType();
-    this.mAppLinkSpec = AdCreativeLinkDataAppLinkSpec.getGson().fromJson(value, type);
-    return this;
-  }
-  public String getFieldEventId() {
-    return mEventId;
+  public String getFieldName() {
+    return mName;
   }
 
-  public AdCreativeLinkData setFieldEventId(String value) {
-    this.mEventId = value;
+  public AdCreativeLinkData setFieldName(String value) {
+    this.mName = value;
     return this;
   }
 
-  public Boolean getFieldCanvasEnabled() {
-    return mCanvasEnabled;
+  public String getFieldPicture() {
+    return mPicture;
   }
 
-  public AdCreativeLinkData setFieldCanvasEnabled(Boolean value) {
-    this.mCanvasEnabled = value;
+  public AdCreativeLinkData setFieldPicture(String value) {
+    this.mPicture = value;
     return this;
   }
 
 
 
   public static enum EnumAttachmentStyle {
-    @SerializedName("link")
-    VALUE_LINK("link"),
-    @SerializedName("default")
-    VALUE_DEFAULT("default"),
-    NULL(null);
+      @SerializedName("link")
+      VALUE_LINK("link"),
+      @SerializedName("default")
+      VALUE_DEFAULT("default"),
+      NULL(null);
 
-    private String value;
+      private String value;
 
-    private EnumAttachmentStyle(String value) {
-      this.value = value;
-    }
+      private EnumAttachmentStyle(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
 
+
   synchronized /*package*/ static Gson getGson() {
     if (gson != null) {
       return gson;
@@ -397,32 +457,33 @@ public class AdCreativeLinkData extends APINode {
   }
 
   public AdCreativeLinkData copyFrom(AdCreativeLinkData instance) {
-    this.mLink = instance.mLink;
-    this.mMessage = instance.mMessage;
-    this.mName = instance.mName;
-    this.mCaption = instance.mCaption;
+    this.mAdditionalImageIndex = instance.mAdditionalImageIndex;
+    this.mAppLinkSpec = instance.mAppLinkSpec;
     this.mAttachmentStyle = instance.mAttachmentStyle;
-    this.mDescription = instance.mDescription;
-    this.mImageHash = instance.mImageHash;
+    this.mBrandedContentSponsorPageId = instance.mBrandedContentSponsorPageId;
     this.mCallToAction = instance.mCallToAction;
-    this.mImageCrops = instance.mImageCrops;
-    this.mPicture = instance.mPicture;
+    this.mCanvasEnabled = instance.mCanvasEnabled;
+    this.mCaption = instance.mCaption;
     this.mChildAttachments = instance.mChildAttachments;
+    this.mDescription = instance.mDescription;
+    this.mEventId = instance.mEventId;
+    this.mForceSingleLink = instance.mForceSingleLink;
+    this.mImageCrops = instance.mImageCrops;
+    this.mImageHash = instance.mImageHash;
+    this.mLink = instance.mLink;
+    this.mMessage = instance.mMessage;
     this.mMultiShareEndCard = instance.mMultiShareEndCard;
     this.mMultiShareOptimized = instance.mMultiShareOptimized;
-    this.mMaxProductCount = instance.mMaxProductCount;
-    this.mAdditionalImageIndex = instance.mAdditionalImageIndex;
-    this.mAppLinkSpec = instance.mAppLinkSpec;
-    this.mEventId = instance.mEventId;
-    this.mCanvasEnabled = instance.mCanvasEnabled;
-    this.mContext = instance.mContext;
+    this.mName = instance.mName;
+    this.mPicture = instance.mPicture;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AdCreativeLinkData> getParser() {
     return new APIRequest.ResponseParser<AdCreativeLinkData>() {
-      public APINodeList<AdCreativeLinkData> parseResponse(String response, APIContext context, APIRequest<AdCreativeLinkData> request) {
+      public APINodeList<AdCreativeLinkData> parseResponse(String response, APIContext context, APIRequest<AdCreativeLinkData> request) throws MalformedResponseException {
         return AdCreativeLinkData.parseResponse(response, context, request);
       }
     };
