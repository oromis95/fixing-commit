@@ -24,46 +24,49 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class ProfilePictureSource extends APINode {
+  @SerializedName("bottom")
+  private Long mBottom = null;
   @SerializedName("height")
   private Long mHeight = null;
   @SerializedName("is_silhouette")
   private Boolean mIsSilhouette = null;
-  @SerializedName("url")
-  private String mUrl = null;
-  @SerializedName("width")
-  private Long mWidth = null;
   @SerializedName("left")
   private Long mLeft = null;
-  @SerializedName("top")
-  private Long mTop = null;
   @SerializedName("right")
   private Long mRight = null;
-  @SerializedName("bottom")
-  private Long mBottom = null;
+  @SerializedName("top")
+  private Long mTop = null;
+  @SerializedName("url")
+  private String mUrl = null;
+  @SerializedName("width")
+  private Long mWidth = null;
   protected static Gson gson = null;
 
   public ProfilePictureSource() {
@@ -81,22 +84,23 @@ public class ProfilePictureSource extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    profilePictureSource.mContext = context;
+    profilePictureSource.context = context;
     profilePictureSource.rawValue = json;
     return profilePictureSource;
   }
 
-  public static APINodeList<ProfilePictureSource> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<ProfilePictureSource> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<ProfilePictureSource> profilePictureSources = new APINodeList<ProfilePictureSource>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -109,10 +113,11 @@ public class ProfilePictureSource extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            profilePictureSources.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            profilePictureSources.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -123,7 +128,20 @@ public class ProfilePictureSource extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            profilePictureSources.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  profilePictureSources.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              profilePictureSources.add(loadJSON(obj.toString(), context));
+            }
           }
           return profilePictureSources;
         } else if (obj.has("images")) {
@@ -134,24 +152,54 @@ public class ProfilePictureSource extends APINode {
           }
           return profilePictureSources;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              profilePictureSources.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return profilePictureSources;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          profilePictureSources.clear();
           profilePictureSources.add(loadJSON(json, context));
           return profilePictureSources;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -160,6 +208,15 @@ public class ProfilePictureSource extends APINode {
   }
 
 
+  public Long getFieldBottom() {
+    return mBottom;
+  }
+
+  public ProfilePictureSource setFieldBottom(Long value) {
+    this.mBottom = value;
+    return this;
+  }
+
   public Long getFieldHeight() {
     return mHeight;
   }
@@ -178,30 +235,21 @@ public class ProfilePictureSource extends APINode {
     return this;
   }
 
-  public String getFieldUrl() {
-    return mUrl;
-  }
-
-  public ProfilePictureSource setFieldUrl(String value) {
-    this.mUrl = value;
-    return this;
-  }
-
-  public Long getFieldWidth() {
-    return mWidth;
+  public Long getFieldLeft() {
+    return mLeft;
   }
 
-  public ProfilePictureSource setFieldWidth(Long value) {
-    this.mWidth = value;
+  public ProfilePictureSource setFieldLeft(Long value) {
+    this.mLeft = value;
     return this;
   }
 
-  public Long getFieldLeft() {
-    return mLeft;
+  public Long getFieldRight() {
+    return mRight;
   }
 
-  public ProfilePictureSource setFieldLeft(Long value) {
-    this.mLeft = value;
+  public ProfilePictureSource setFieldRight(Long value) {
+    this.mRight = value;
     return this;
   }
 
@@ -214,26 +262,51 @@ public class ProfilePictureSource extends APINode {
     return this;
   }
 
-  public Long getFieldRight() {
-    return mRight;
+  public String getFieldUrl() {
+    return mUrl;
   }
 
-  public ProfilePictureSource setFieldRight(Long value) {
-    this.mRight = value;
+  public ProfilePictureSource setFieldUrl(String value) {
+    this.mUrl = value;
     return this;
   }
 
-  public Long getFieldBottom() {
-    return mBottom;
+  public Long getFieldWidth() {
+    return mWidth;
   }
 
-  public ProfilePictureSource setFieldBottom(Long value) {
-    this.mBottom = value;
+  public ProfilePictureSource setFieldWidth(Long value) {
+    this.mWidth = value;
     return this;
   }
 
 
 
+  public static enum EnumType {
+      @SerializedName("small")
+      VALUE_SMALL("small"),
+      @SerializedName("normal")
+      VALUE_NORMAL("normal"),
+      @SerializedName("album")
+      VALUE_ALBUM("album"),
+      @SerializedName("large")
+      VALUE_LARGE("large"),
+      @SerializedName("square")
+      VALUE_SQUARE("square"),
+      NULL(null);
+
+      private String value;
+
+      private EnumType(String value) {
+        this.value = value;
+      }
+
+      @Override
+      public String toString() {
+        return value;
+      }
+  }
+
 
   synchronized /*package*/ static Gson getGson() {
     if (gson != null) {
@@ -249,22 +322,22 @@ public class ProfilePictureSource extends APINode {
   }
 
   public ProfilePictureSource copyFrom(ProfilePictureSource instance) {
+    this.mBottom = instance.mBottom;
     this.mHeight = instance.mHeight;
     this.mIsSilhouette = instance.mIsSilhouette;
-    this.mUrl = instance.mUrl;
-    this.mWidth = instance.mWidth;
     this.mLeft = instance.mLeft;
-    this.mTop = instance.mTop;
     this.mRight = instance.mRight;
-    this.mBottom = instance.mBottom;
-    this.mContext = instance.mContext;
+    this.mTop = instance.mTop;
+    this.mUrl = instance.mUrl;
+    this.mWidth = instance.mWidth;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<ProfilePictureSource> getParser() {
     return new APIRequest.ResponseParser<ProfilePictureSource>() {
-      public APINodeList<ProfilePictureSource> parseResponse(String response, APIContext context, APIRequest<ProfilePictureSource> request) {
+      public APINodeList<ProfilePictureSource> parseResponse(String response, APIContext context, APIRequest<ProfilePictureSource> request) throws MalformedResponseException {
         return ProfilePictureSource.parseResponse(response, context, request);
       }
     };
