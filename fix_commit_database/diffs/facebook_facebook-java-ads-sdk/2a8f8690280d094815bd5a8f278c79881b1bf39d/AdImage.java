@@ -24,34 +24,33 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AdImage extends APINode {
-  @SerializedName("id")
-  private String mId = null;
-  @SerializedName("name")
-  private String mName = null;
   @SerializedName("account_id")
   private String mAccountId = null;
   @SerializedName("created_time")
@@ -62,6 +61,10 @@ public class AdImage extends APINode {
   private String mHash = null;
   @SerializedName("height")
   private Long mHeight = null;
+  @SerializedName("id")
+  private String mId = null;
+  @SerializedName("name")
+  private String mName = null;
   @SerializedName("original_height")
   private Long mOriginalHeight = null;
   @SerializedName("original_width")
@@ -89,11 +92,11 @@ public class AdImage extends APINode {
 
   public AdImage(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public AdImage fetch() throws APIException{
-    AdImage newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    AdImage newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -110,8 +113,17 @@ public class AdImage extends APINode {
     return adImage;
   }
 
+  public static APINodeList<AdImage> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<AdImage>)(
+      new APIRequest<AdImage>(context, "", "/", "GET", AdImage.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -126,22 +138,23 @@ public class AdImage extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    adImage.mContext = context;
+    adImage.context = context;
     adImage.rawValue = json;
     return adImage;
   }
 
-  public static APINodeList<AdImage> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AdImage> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AdImage> adImages = new APINodeList<AdImage>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -154,10 +167,11 @@ public class AdImage extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            adImages.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            adImages.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -168,7 +182,20 @@ public class AdImage extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            adImages.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  adImages.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              adImages.add(loadJSON(obj.toString(), context));
+            }
           }
           return adImages;
         } else if (obj.has("images")) {
@@ -179,24 +206,54 @@ public class AdImage extends APINode {
           }
           return adImages;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              adImages.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return adImages;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          adImages.clear();
           adImages.add(loadJSON(json, context));
           return adImages;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -205,18 +262,10 @@ public class AdImage extends APINode {
   }
 
   public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
   }
 
 
-  public String getFieldId() {
-    return mId;
-  }
-
-  public String getFieldName() {
-    return mName;
-  }
-
   public String getFieldAccountId() {
     return mAccountId;
   }
@@ -237,6 +286,14 @@ public class AdImage extends APINode {
     return mHeight;
   }
 
+  public String getFieldId() {
+    return mId;
+  }
+
+  public String getFieldName() {
+    return mName;
+  }
+
   public Long getFieldOriginalHeight() {
     return mOriginalHeight;
   }
@@ -282,13 +339,13 @@ public class AdImage extends APINode {
     };
 
     public static final String[] FIELDS = {
-      "id",
-      "name",
       "account_id",
       "created_time",
       "creatives",
       "hash",
       "height",
+      "id",
+      "name",
       "original_height",
       "original_width",
       "permalink_url",
@@ -311,7 +368,7 @@ public class AdImage extends APINode {
 
     @Override
     public AdImage execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -319,11 +376,13 @@ public class AdImage extends APINode {
       super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestGet setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestGet setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
@@ -341,10 +400,12 @@ public class AdImage extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGet requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestGet requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -352,30 +413,18 @@ public class AdImage extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGet requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGet requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-    public APIRequestGet requestIdField () {
-      return this.requestIdField(true);
-    }
-    public APIRequestGet requestIdField (boolean value) {
-      this.requestField("id", value);
-      return this;
-    }
-    public APIRequestGet requestNameField () {
-      return this.requestNameField(true);
-    }
-    public APIRequestGet requestNameField (boolean value) {
-      this.requestField("name", value);
-      return this;
-    }
     public APIRequestGet requestAccountIdField () {
       return this.requestAccountIdField(true);
     }
@@ -411,6 +460,20 @@ public class AdImage extends APINode {
       this.requestField("height", value);
       return this;
     }
+    public APIRequestGet requestIdField () {
+      return this.requestIdField(true);
+    }
+    public APIRequestGet requestIdField (boolean value) {
+      this.requestField("id", value);
+      return this;
+    }
+    public APIRequestGet requestNameField () {
+      return this.requestNameField(true);
+    }
+    public APIRequestGet requestNameField (boolean value) {
+      this.requestField("name", value);
+      return this;
+    }
     public APIRequestGet requestOriginalHeightField () {
       return this.requestOriginalHeightField(true);
     }
@@ -467,28 +530,28 @@ public class AdImage extends APINode {
       this.requestField("width", value);
       return this;
     }
-
   }
 
   public static enum EnumStatus {
-    @SerializedName("ACTIVE")
-    VALUE_ACTIVE("ACTIVE"),
-    @SerializedName("DELETED")
-    VALUE_DELETED("DELETED"),
-    NULL(null);
+      @SerializedName("ACTIVE")
+      VALUE_ACTIVE("ACTIVE"),
+      @SerializedName("DELETED")
+      VALUE_DELETED("DELETED"),
+      NULL(null);
 
-    private String value;
+      private String value;
 
-    private EnumStatus(String value) {
-      this.value = value;
-    }
+      private EnumStatus(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
 
+
   synchronized /*package*/ static Gson getGson() {
     if (gson != null) {
       return gson;
@@ -503,13 +566,13 @@ public class AdImage extends APINode {
   }
 
   public AdImage copyFrom(AdImage instance) {
-    this.mId = instance.mId;
-    this.mName = instance.mName;
     this.mAccountId = instance.mAccountId;
     this.mCreatedTime = instance.mCreatedTime;
     this.mCreatives = instance.mCreatives;
     this.mHash = instance.mHash;
     this.mHeight = instance.mHeight;
+    this.mId = instance.mId;
+    this.mName = instance.mName;
     this.mOriginalHeight = instance.mOriginalHeight;
     this.mOriginalWidth = instance.mOriginalWidth;
     this.mPermalinkUrl = instance.mPermalinkUrl;
@@ -518,14 +581,14 @@ public class AdImage extends APINode {
     this.mUrl = instance.mUrl;
     this.mUrl128 = instance.mUrl128;
     this.mWidth = instance.mWidth;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AdImage> getParser() {
     return new APIRequest.ResponseParser<AdImage>() {
-      public APINodeList<AdImage> parseResponse(String response, APIContext context, APIRequest<AdImage> request) {
+      public APINodeList<AdImage> parseResponse(String response, APIContext context, APIRequest<AdImage> request) throws MalformedResponseException {
         return AdImage.parseResponse(response, context, request);
       }
     };
