@@ -24,40 +24,43 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AdPlacePageSet extends APINode {
+  @SerializedName("account_id")
+  private String mAccountId = null;
   @SerializedName("id")
   private String mId = null;
   @SerializedName("name")
   private String mName = null;
-  @SerializedName("account_id")
-  private String mAccountId = null;
-  @SerializedName("parent_page")
-  private Object mParentPage = null;
   @SerializedName("pages_count")
   private Long mPagesCount = null;
+  @SerializedName("parent_page")
+  private Object mParentPage = null;
   protected static Gson gson = null;
 
   AdPlacePageSet() {
@@ -69,11 +72,11 @@ public class AdPlacePageSet extends APINode {
 
   public AdPlacePageSet(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public AdPlacePageSet fetch() throws APIException{
-    AdPlacePageSet newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    AdPlacePageSet newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -90,8 +93,17 @@ public class AdPlacePageSet extends APINode {
     return adPlacePageSet;
   }
 
+  public static APINodeList<AdPlacePageSet> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<AdPlacePageSet>)(
+      new APIRequest<AdPlacePageSet>(context, "", "/", "GET", AdPlacePageSet.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -106,22 +118,23 @@ public class AdPlacePageSet extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    adPlacePageSet.mContext = context;
+    adPlacePageSet.context = context;
     adPlacePageSet.rawValue = json;
     return adPlacePageSet;
   }
 
-  public static APINodeList<AdPlacePageSet> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AdPlacePageSet> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AdPlacePageSet> adPlacePageSets = new APINodeList<AdPlacePageSet>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -134,10 +147,11 @@ public class AdPlacePageSet extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            adPlacePageSets.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            adPlacePageSets.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -148,7 +162,20 @@ public class AdPlacePageSet extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            adPlacePageSets.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  adPlacePageSets.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              adPlacePageSets.add(loadJSON(obj.toString(), context));
+            }
           }
           return adPlacePageSets;
         } else if (obj.has("images")) {
@@ -159,24 +186,54 @@ public class AdPlacePageSet extends APINode {
           }
           return adPlacePageSets;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              adPlacePageSets.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return adPlacePageSets;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          adPlacePageSets.clear();
           adPlacePageSets.add(loadJSON(json, context));
           return adPlacePageSets;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -185,14 +242,18 @@ public class AdPlacePageSet extends APINode {
   }
 
   public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
   }
 
   public APIRequestUpdate update() {
-    return new APIRequestUpdate(this.getPrefixedId().toString(), mContext);
+    return new APIRequestUpdate(this.getPrefixedId().toString(), context);
   }
 
 
+  public String getFieldAccountId() {
+    return mAccountId;
+  }
+
   public String getFieldId() {
     return mId;
   }
@@ -201,18 +262,14 @@ public class AdPlacePageSet extends APINode {
     return mName;
   }
 
-  public String getFieldAccountId() {
-    return mAccountId;
+  public Long getFieldPagesCount() {
+    return mPagesCount;
   }
 
   public Object getFieldParentPage() {
     return mParentPage;
   }
 
-  public Long getFieldPagesCount() {
-    return mPagesCount;
-  }
-
 
 
   public static class APIRequestGet extends APIRequest<AdPlacePageSet> {
@@ -226,11 +283,11 @@ public class AdPlacePageSet extends APINode {
     };
 
     public static final String[] FIELDS = {
+      "account_id",
       "id",
       "name",
-      "account_id",
-      "parent_page",
       "pages_count",
+      "parent_page",
     };
 
     @Override
@@ -245,7 +302,7 @@ public class AdPlacePageSet extends APINode {
 
     @Override
     public AdPlacePageSet execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -253,11 +310,13 @@ public class AdPlacePageSet extends APINode {
       super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestGet setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestGet setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
@@ -275,10 +334,12 @@ public class AdPlacePageSet extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGet requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestGet requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -286,16 +347,25 @@ public class AdPlacePageSet extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGet requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGet requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
+    public APIRequestGet requestAccountIdField () {
+      return this.requestAccountIdField(true);
+    }
+    public APIRequestGet requestAccountIdField (boolean value) {
+      this.requestField("account_id", value);
+      return this;
+    }
     public APIRequestGet requestIdField () {
       return this.requestIdField(true);
     }
@@ -310,11 +380,11 @@ public class AdPlacePageSet extends APINode {
       this.requestField("name", value);
       return this;
     }
-    public APIRequestGet requestAccountIdField () {
-      return this.requestAccountIdField(true);
+    public APIRequestGet requestPagesCountField () {
+      return this.requestPagesCountField(true);
     }
-    public APIRequestGet requestAccountIdField (boolean value) {
-      this.requestField("account_id", value);
+    public APIRequestGet requestPagesCountField (boolean value) {
+      this.requestField("pages_count", value);
       return this;
     }
     public APIRequestGet requestParentPageField () {
@@ -324,14 +394,6 @@ public class AdPlacePageSet extends APINode {
       this.requestField("parent_page", value);
       return this;
     }
-    public APIRequestGet requestPagesCountField () {
-      return this.requestPagesCountField(true);
-    }
-    public APIRequestGet requestPagesCountField (boolean value) {
-      this.requestField("pages_count", value);
-      return this;
-    }
-
   }
 
   public static class APIRequestUpdate extends APIRequest<APINode> {
@@ -361,7 +423,7 @@ public class AdPlacePageSet extends APINode {
 
     @Override
     public APINode execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -369,11 +431,13 @@ public class AdPlacePageSet extends APINode {
       super(context, nodeId, "/", "POST", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestUpdate setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestUpdate setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
@@ -385,13 +449,11 @@ public class AdPlacePageSet extends APINode {
       return this;
     }
 
-
     public APIRequestUpdate setName (String name) {
       this.setParam("name", name);
       return this;
     }
 
-
     public APIRequestUpdate requestAllFields () {
       return this.requestAllFields(true);
     }
@@ -403,10 +465,12 @@ public class AdPlacePageSet extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestUpdate requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestUpdate requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -414,17 +478,18 @@ public class AdPlacePageSet extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestUpdate requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestUpdate requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
-
   }
 
 
@@ -442,19 +507,19 @@ public class AdPlacePageSet extends APINode {
   }
 
   public AdPlacePageSet copyFrom(AdPlacePageSet instance) {
+    this.mAccountId = instance.mAccountId;
     this.mId = instance.mId;
     this.mName = instance.mName;
-    this.mAccountId = instance.mAccountId;
-    this.mParentPage = instance.mParentPage;
     this.mPagesCount = instance.mPagesCount;
-    this.mContext = instance.mContext;
+    this.mParentPage = instance.mParentPage;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AdPlacePageSet> getParser() {
     return new APIRequest.ResponseParser<AdPlacePageSet>() {
-      public APINodeList<AdPlacePageSet> parseResponse(String response, APIContext context, APIRequest<AdPlacePageSet> request) {
+      public APINodeList<AdPlacePageSet> parseResponse(String response, APIContext context, APIRequest<AdPlacePageSet> request) throws MalformedResponseException {
         return AdPlacePageSet.parseResponse(response, context, request);
       }
     };
