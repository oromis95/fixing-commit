@@ -24,40 +24,47 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class ProductFeedSchedule extends APINode {
   @SerializedName("day_of_month")
   private Long mDayOfMonth = null;
   @SerializedName("day_of_week")
-  private Object mDayOfWeek = null;
+  private EnumDayOfWeek mDayOfWeek = null;
   @SerializedName("hour")
   private Long mHour = null;
   @SerializedName("interval")
   private EnumInterval mInterval = null;
+  @SerializedName("minute")
+  private Long mMinute = null;
   @SerializedName("url")
   private String mUrl = null;
+  @SerializedName("username")
+  private String mUsername = null;
   protected static Gson gson = null;
 
   public ProductFeedSchedule() {
@@ -75,22 +82,23 @@ public class ProductFeedSchedule extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    productFeedSchedule.mContext = context;
+    productFeedSchedule.context = context;
     productFeedSchedule.rawValue = json;
     return productFeedSchedule;
   }
 
-  public static APINodeList<ProductFeedSchedule> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<ProductFeedSchedule> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<ProductFeedSchedule> productFeedSchedules = new APINodeList<ProductFeedSchedule>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -103,10 +111,11 @@ public class ProductFeedSchedule extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            productFeedSchedules.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            productFeedSchedules.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -117,7 +126,20 @@ public class ProductFeedSchedule extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            productFeedSchedules.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  productFeedSchedules.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              productFeedSchedules.add(loadJSON(obj.toString(), context));
+            }
           }
           return productFeedSchedules;
         } else if (obj.has("images")) {
@@ -128,24 +150,54 @@ public class ProductFeedSchedule extends APINode {
           }
           return productFeedSchedules;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              productFeedSchedules.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return productFeedSchedules;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          productFeedSchedules.clear();
           productFeedSchedules.add(loadJSON(json, context));
           return productFeedSchedules;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -163,11 +215,11 @@ public class ProductFeedSchedule extends APINode {
     return this;
   }
 
-  public Object getFieldDayOfWeek() {
+  public EnumDayOfWeek getFieldDayOfWeek() {
     return mDayOfWeek;
   }
 
-  public ProductFeedSchedule setFieldDayOfWeek(Object value) {
+  public ProductFeedSchedule setFieldDayOfWeek(EnumDayOfWeek value) {
     this.mDayOfWeek = value;
     return this;
   }
@@ -190,6 +242,15 @@ public class ProductFeedSchedule extends APINode {
     return this;
   }
 
+  public Long getFieldMinute() {
+    return mMinute;
+  }
+
+  public ProductFeedSchedule setFieldMinute(Long value) {
+    this.mMinute = value;
+    return this;
+  }
+
   public String getFieldUrl() {
     return mUrl;
   }
@@ -199,29 +260,70 @@ public class ProductFeedSchedule extends APINode {
     return this;
   }
 
+  public String getFieldUsername() {
+    return mUsername;
+  }
+
+  public ProductFeedSchedule setFieldUsername(String value) {
+    this.mUsername = value;
+    return this;
+  }
+
+
+
+  public static enum EnumDayOfWeek {
+      @SerializedName("SUNDAY")
+      VALUE_SUNDAY("SUNDAY"),
+      @SerializedName("MONDAY")
+      VALUE_MONDAY("MONDAY"),
+      @SerializedName("TUESDAY")
+      VALUE_TUESDAY("TUESDAY"),
+      @SerializedName("WEDNESDAY")
+      VALUE_WEDNESDAY("WEDNESDAY"),
+      @SerializedName("THURSDAY")
+      VALUE_THURSDAY("THURSDAY"),
+      @SerializedName("FRIDAY")
+      VALUE_FRIDAY("FRIDAY"),
+      @SerializedName("SATURDAY")
+      VALUE_SATURDAY("SATURDAY"),
+      NULL(null);
 
+      private String value;
+
+      private EnumDayOfWeek(String value) {
+        this.value = value;
+      }
+
+      @Override
+      public String toString() {
+        return value;
+      }
+  }
 
   public static enum EnumInterval {
-    @SerializedName("DAILY")
-    VALUE_DAILY("DAILY"),
-    @SerializedName("WEEKLY")
-    VALUE_WEEKLY("WEEKLY"),
-    @SerializedName("MONTHLY")
-    VALUE_MONTHLY("MONTHLY"),
-    NULL(null);
-
-    private String value;
-
-    private EnumInterval(String value) {
-      this.value = value;
-    }
+      @SerializedName("HOURLY")
+      VALUE_HOURLY("HOURLY"),
+      @SerializedName("DAILY")
+      VALUE_DAILY("DAILY"),
+      @SerializedName("WEEKLY")
+      VALUE_WEEKLY("WEEKLY"),
+      @SerializedName("MONTHLY")
+      VALUE_MONTHLY("MONTHLY"),
+      NULL(null);
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      private String value;
+
+      private EnumInterval(String value) {
+        this.value = value;
+      }
+
+      @Override
+      public String toString() {
+        return value;
+      }
   }
 
+
   synchronized /*package*/ static Gson getGson() {
     if (gson != null) {
       return gson;
@@ -240,15 +342,17 @@ public class ProductFeedSchedule extends APINode {
     this.mDayOfWeek = instance.mDayOfWeek;
     this.mHour = instance.mHour;
     this.mInterval = instance.mInterval;
+    this.mMinute = instance.mMinute;
     this.mUrl = instance.mUrl;
-    this.mContext = instance.mContext;
+    this.mUsername = instance.mUsername;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<ProductFeedSchedule> getParser() {
     return new APIRequest.ResponseParser<ProductFeedSchedule>() {
-      public APINodeList<ProductFeedSchedule> parseResponse(String response, APIContext context, APIRequest<ProductFeedSchedule> request) {
+      public APINodeList<ProductFeedSchedule> parseResponse(String response, APIContext context, APIRequest<ProductFeedSchedule> request) throws MalformedResponseException {
         return ProductFeedSchedule.parseResponse(response, context, request);
       }
     };
