@@ -24,44 +24,47 @@
 package com.facebook.ads.sdk;
 
 import java.io.File;
-import java.lang.reflect.Field;
 import java.lang.reflect.Modifier;
 import java.lang.reflect.Type;
-import java.lang.IllegalArgumentException;
 import java.util.Arrays;
-import java.util.ArrayList;
-import java.util.Iterator;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 import com.google.gson.JsonObject;
 import com.google.gson.JsonArray;
-import com.google.gson.JsonParseException;
 import com.google.gson.annotations.SerializedName;
 import com.google.gson.reflect.TypeToken;
-import com.google.gson.FieldNamingStrategy;
 import com.google.gson.Gson;
 import com.google.gson.GsonBuilder;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonParser;
 
+import com.facebook.ads.sdk.APIException.MalformedResponseException;
 
+/**
+ * This class is auto-genereated.
+ *
+ * For any issues or feature requests related to this class, please let us know
+ * on github and we'll fix in our codegen framework. We'll not be able to accept
+ * pull request for this class.
+ *
+ */
 public class AdAsyncRequest extends APINode {
+  @SerializedName("async_request_set")
+  private AdAsyncRequestSet mAsyncRequestSet = null;
+  @SerializedName("created_time")
+  private String mCreatedTime = null;
   @SerializedName("id")
   private String mId = null;
+  @SerializedName("input")
+  private Map<String, String> mInput = null;
+  @SerializedName("result")
+  private Map<String, String> mResult = null;
   @SerializedName("scope_object_id")
   private String mScopeObjectId = null;
   @SerializedName("status")
   private EnumStatus mStatus = null;
-  @SerializedName("result")
-  private Map<String, String> mResult = null;
-  @SerializedName("input")
-  private Map<String, String> mInput = null;
-  @SerializedName("async_request_set")
-  private AdAsyncRequestSet mAsyncRequestSet = null;
-  @SerializedName("created_time")
-  private String mCreatedTime = null;
   @SerializedName("updated_time")
   private String mUpdatedTime = null;
   protected static Gson gson = null;
@@ -75,11 +78,11 @@ public class AdAsyncRequest extends APINode {
 
   public AdAsyncRequest(String id, APIContext context) {
     this.mId = id;
-    this.mContext = context;
+    this.context = context;
   }
 
   public AdAsyncRequest fetch() throws APIException{
-    AdAsyncRequest newInstance = fetchById(this.getPrefixedId().toString(), this.mContext);
+    AdAsyncRequest newInstance = fetchById(this.getPrefixedId().toString(), this.context);
     this.copyFrom(newInstance);
     return this;
   }
@@ -96,8 +99,17 @@ public class AdAsyncRequest extends APINode {
     return adAsyncRequest;
   }
 
+  public static APINodeList<AdAsyncRequest> fetchByIds(List<String> ids, List<String> fields, APIContext context) throws APIException {
+    return (APINodeList<AdAsyncRequest>)(
+      new APIRequest<AdAsyncRequest>(context, "", "/", "GET", AdAsyncRequest.getParser())
+        .setParam("ids", String.join(",", ids))
+        .requestFields(fields)
+        .execute()
+    );
+  }
+
   private String getPrefixedId() {
-    return mId.toString();
+    return getId();
   }
 
   public String getId() {
@@ -112,22 +124,23 @@ public class AdAsyncRequest extends APINode {
       if (o1.getAsJsonObject().get("__fb_trace_id__") != null) {
         o2.getAsJsonObject().add("__fb_trace_id__", o1.getAsJsonObject().get("__fb_trace_id__"));
       }
-      if(!o1.equals(o2)) {
+      if (!o1.equals(o2)) {
         context.log("[Warning] When parsing response, object is not consistent with JSON:");
         context.log("[JSON]" + o1);
         context.log("[Object]" + o2);
       };
     }
-    adAsyncRequest.mContext = context;
+    adAsyncRequest.context = context;
     adAsyncRequest.rawValue = json;
     return adAsyncRequest;
   }
 
-  public static APINodeList<AdAsyncRequest> parseResponse(String json, APIContext context, APIRequest request) {
+  public static APINodeList<AdAsyncRequest> parseResponse(String json, APIContext context, APIRequest request) throws MalformedResponseException {
     APINodeList<AdAsyncRequest> adAsyncRequests = new APINodeList<AdAsyncRequest>(request, json);
     JsonArray arr;
     JsonObject obj;
     JsonParser parser = new JsonParser();
+    Exception exception = null;
     try{
       JsonElement result = parser.parse(json);
       if (result.isJsonArray()) {
@@ -140,10 +153,11 @@ public class AdAsyncRequest extends APINode {
       } else if (result.isJsonObject()) {
         obj = result.getAsJsonObject();
         if (obj.has("data")) {
-          try {
+          if (obj.has("paging")) {
             JsonObject paging = obj.get("paging").getAsJsonObject().get("cursors").getAsJsonObject();
-            adAsyncRequests.setPaging(paging.get("before").getAsString(), paging.get("after").getAsString());
-          } catch (Exception ignored) {
+            String before = paging.has("before") ? paging.get("before").getAsString() : null;
+            String after = paging.has("after") ? paging.get("after").getAsString() : null;
+            adAsyncRequests.setPaging(before, after);
           }
           if (obj.get("data").isJsonArray()) {
             // Second, check if it's a JSON array with "data"
@@ -154,7 +168,20 @@ public class AdAsyncRequest extends APINode {
           } else if (obj.get("data").isJsonObject()) {
             // Third, check if it's a JSON object with "data"
             obj = obj.get("data").getAsJsonObject();
-            adAsyncRequests.add(loadJSON(obj.toString(), context));
+            boolean isRedownload = false;
+            for (String s : new String[]{"campaigns", "adsets", "ads"}) {
+              if (obj.has(s)) {
+                isRedownload = true;
+                obj = obj.getAsJsonObject(s);
+                for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
+                  adAsyncRequests.add(loadJSON(entry.getValue().toString(), context));
+                }
+                break;
+              }
+            }
+            if (!isRedownload) {
+              adAsyncRequests.add(loadJSON(obj.toString(), context));
+            }
           }
           return adAsyncRequests;
         } else if (obj.has("images")) {
@@ -165,24 +192,54 @@ public class AdAsyncRequest extends APINode {
           }
           return adAsyncRequests;
         } else {
-          // Fifth, check if it's pure JsonObject
+          // Fifth, check if it's an array of objects indexed by id
+          boolean isIdIndexedArray = true;
+          for (Map.Entry entry : obj.entrySet()) {
+            String key = (String) entry.getKey();
+            if (key.equals("__fb_trace_id__")) {
+              continue;
+            }
+            JsonElement value = (JsonElement) entry.getValue();
+            if (
+              value != null &&
+              value.isJsonObject() &&
+              value.getAsJsonObject().has("id") &&
+              value.getAsJsonObject().get("id") != null &&
+              value.getAsJsonObject().get("id").getAsString().equals(key)
+            ) {
+              adAsyncRequests.add(loadJSON(value.toString(), context));
+            } else {
+              isIdIndexedArray = false;
+              break;
+            }
+          }
+          if (isIdIndexedArray) {
+            return adAsyncRequests;
+          }
+
+          // Sixth, check if it's pure JsonObject
+          adAsyncRequests.clear();
           adAsyncRequests.add(loadJSON(json, context));
           return adAsyncRequests;
         }
       }
     } catch (Exception e) {
+      exception = e;
     }
-    return null;
+    throw new MalformedResponseException(
+      "Invalid response string: " + json,
+      exception
+    );
   }
 
   @Override
   public APIContext getContext() {
-    return mContext;
+    return context;
   }
 
   @Override
   public void setContext(APIContext context) {
-    mContext = context;
+    this.context = context;
   }
 
   @Override
@@ -191,39 +248,39 @@ public class AdAsyncRequest extends APINode {
   }
 
   public APIRequestGet get() {
-    return new APIRequestGet(this.getPrefixedId().toString(), mContext);
+    return new APIRequestGet(this.getPrefixedId().toString(), context);
   }
 
 
-  public String getFieldId() {
-    return mId;
-  }
-
-  public String getFieldScopeObjectId() {
-    return mScopeObjectId;
+  public AdAsyncRequestSet getFieldAsyncRequestSet() {
+    if (mAsyncRequestSet != null) {
+      mAsyncRequestSet.context = getContext();
+    }
+    return mAsyncRequestSet;
   }
 
-  public EnumStatus getFieldStatus() {
-    return mStatus;
+  public String getFieldCreatedTime() {
+    return mCreatedTime;
   }
 
-  public Map<String, String> getFieldResult() {
-    return mResult;
+  public String getFieldId() {
+    return mId;
   }
 
   public Map<String, String> getFieldInput() {
     return mInput;
   }
 
-  public AdAsyncRequestSet getFieldAsyncRequestSet() {
-    if (mAsyncRequestSet != null) {
-      mAsyncRequestSet.mContext = getContext();
-    }
-    return mAsyncRequestSet;
+  public Map<String, String> getFieldResult() {
+    return mResult;
   }
 
-  public String getFieldCreatedTime() {
-    return mCreatedTime;
+  public String getFieldScopeObjectId() {
+    return mScopeObjectId;
+  }
+
+  public EnumStatus getFieldStatus() {
+    return mStatus;
   }
 
   public String getFieldUpdatedTime() {
@@ -243,13 +300,13 @@ public class AdAsyncRequest extends APINode {
     };
 
     public static final String[] FIELDS = {
+      "async_request_set",
+      "created_time",
       "id",
+      "input",
+      "result",
       "scope_object_id",
       "status",
-      "result",
-      "input",
-      "async_request_set",
-      "created_time",
       "updated_time",
     };
 
@@ -265,7 +322,7 @@ public class AdAsyncRequest extends APINode {
 
     @Override
     public AdAsyncRequest execute(Map<String, Object> extraParams) throws APIException {
-      lastResponse = parseResponse(callInternal(extraParams));
+      lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
 
@@ -273,11 +330,13 @@ public class AdAsyncRequest extends APINode {
       super(context, nodeId, "/", "GET", Arrays.asList(PARAMS));
     }
 
+    @Override
     public APIRequestGet setParam(String param, Object value) {
       setParamInternal(param, value);
       return this;
     }
 
+    @Override
     public APIRequestGet setParams(Map<String, Object> params) {
       setParamsInternal(params);
       return this;
@@ -295,10 +354,12 @@ public class AdAsyncRequest extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGet requestFields (List<String> fields) {
       return this.requestFields(fields, true);
     }
 
+    @Override
     public APIRequestGet requestFields (List<String> fields, boolean value) {
       for (String field : fields) {
         this.requestField(field, value);
@@ -306,16 +367,32 @@ public class AdAsyncRequest extends APINode {
       return this;
     }
 
+    @Override
     public APIRequestGet requestField (String field) {
       this.requestField(field, true);
       return this;
     }
 
+    @Override
     public APIRequestGet requestField (String field, boolean value) {
       this.requestFieldInternal(field, value);
       return this;
     }
 
+    public APIRequestGet requestAsyncRequestSetField () {
+      return this.requestAsyncRequestSetField(true);
+    }
+    public APIRequestGet requestAsyncRequestSetField (boolean value) {
+      this.requestField("async_request_set", value);
+      return this;
+    }
+    public APIRequestGet requestCreatedTimeField () {
+      return this.requestCreatedTimeField(true);
+    }
+    public APIRequestGet requestCreatedTimeField (boolean value) {
+      this.requestField("created_time", value);
+      return this;
+    }
     public APIRequestGet requestIdField () {
       return this.requestIdField(true);
     }
@@ -323,18 +400,11 @@ public class AdAsyncRequest extends APINode {
       this.requestField("id", value);
       return this;
     }
-    public APIRequestGet requestScopeObjectIdField () {
-      return this.requestScopeObjectIdField(true);
-    }
-    public APIRequestGet requestScopeObjectIdField (boolean value) {
-      this.requestField("scope_object_id", value);
-      return this;
-    }
-    public APIRequestGet requestStatusField () {
-      return this.requestStatusField(true);
+    public APIRequestGet requestInputField () {
+      return this.requestInputField(true);
     }
-    public APIRequestGet requestStatusField (boolean value) {
-      this.requestField("status", value);
+    public APIRequestGet requestInputField (boolean value) {
+      this.requestField("input", value);
       return this;
     }
     public APIRequestGet requestResultField () {
@@ -344,25 +414,18 @@ public class AdAsyncRequest extends APINode {
       this.requestField("result", value);
       return this;
     }
-    public APIRequestGet requestInputField () {
-      return this.requestInputField(true);
-    }
-    public APIRequestGet requestInputField (boolean value) {
-      this.requestField("input", value);
-      return this;
-    }
-    public APIRequestGet requestAsyncRequestSetField () {
-      return this.requestAsyncRequestSetField(true);
+    public APIRequestGet requestScopeObjectIdField () {
+      return this.requestScopeObjectIdField(true);
     }
-    public APIRequestGet requestAsyncRequestSetField (boolean value) {
-      this.requestField("async_request_set", value);
+    public APIRequestGet requestScopeObjectIdField (boolean value) {
+      this.requestField("scope_object_id", value);
       return this;
     }
-    public APIRequestGet requestCreatedTimeField () {
-      return this.requestCreatedTimeField(true);
+    public APIRequestGet requestStatusField () {
+      return this.requestStatusField(true);
     }
-    public APIRequestGet requestCreatedTimeField (boolean value) {
-      this.requestField("created_time", value);
+    public APIRequestGet requestStatusField (boolean value) {
+      this.requestField("status", value);
       return this;
     }
     public APIRequestGet requestUpdatedTimeField () {
@@ -372,42 +435,75 @@ public class AdAsyncRequest extends APINode {
       this.requestField("updated_time", value);
       return this;
     }
-
   }
 
   public static enum EnumStatus {
-    @SerializedName("INITIAL")
-    VALUE_INITIAL("INITIAL"),
-    @SerializedName("IN_PROGRESS")
-    VALUE_IN_PROGRESS("IN_PROGRESS"),
-    @SerializedName("SUCCESS")
-    VALUE_SUCCESS("SUCCESS"),
-    @SerializedName("ERROR")
-    VALUE_ERROR("ERROR"),
-    @SerializedName("CANCELED")
-    VALUE_CANCELED("CANCELED"),
-    @SerializedName("PENDING_DEPENDENCY")
-    VALUE_PENDING_DEPENDENCY("PENDING_DEPENDENCY"),
-    @SerializedName("CANCELED_DEPENDENCY")
-    VALUE_CANCELED_DEPENDENCY("CANCELED_DEPENDENCY"),
-    @SerializedName("ERROR_DEPENDENCY")
-    VALUE_ERROR_DEPENDENCY("ERROR_DEPENDENCY"),
-    @SerializedName("ERROR_CONFLICTS")
-    VALUE_ERROR_CONFLICTS("ERROR_CONFLICTS"),
-    NULL(null);
-
-    private String value;
-
-    private EnumStatus(String value) {
-      this.value = value;
-    }
+      @SerializedName("INITIAL")
+      VALUE_INITIAL("INITIAL"),
+      @SerializedName("IN_PROGRESS")
+      VALUE_IN_PROGRESS("IN_PROGRESS"),
+      @SerializedName("SUCCESS")
+      VALUE_SUCCESS("SUCCESS"),
+      @SerializedName("ERROR")
+      VALUE_ERROR("ERROR"),
+      @SerializedName("CANCELED")
+      VALUE_CANCELED("CANCELED"),
+      @SerializedName("PENDING_DEPENDENCY")
+      VALUE_PENDING_DEPENDENCY("PENDING_DEPENDENCY"),
+      @SerializedName("CANCELED_DEPENDENCY")
+      VALUE_CANCELED_DEPENDENCY("CANCELED_DEPENDENCY"),
+      @SerializedName("ERROR_DEPENDENCY")
+      VALUE_ERROR_DEPENDENCY("ERROR_DEPENDENCY"),
+      @SerializedName("ERROR_CONFLICTS")
+      VALUE_ERROR_CONFLICTS("ERROR_CONFLICTS"),
+      NULL(null);
+
+      private String value;
+
+      private EnumStatus(String value) {
+        this.value = value;
+      }
 
-    @Override
-    public String toString() {
-      return value;
-    }
+      @Override
+      public String toString() {
+        return value;
+      }
   }
 
+  public static enum EnumStatuses {
+      @SerializedName("INITIAL")
+      VALUE_INITIAL("INITIAL"),
+      @SerializedName("IN_PROGRESS")
+      VALUE_IN_PROGRESS("IN_PROGRESS"),
+      @SerializedName("SUCCESS")
+      VALUE_SUCCESS("SUCCESS"),
+      @SerializedName("ERROR")
+      VALUE_ERROR("ERROR"),
+      @SerializedName("CANCELED")
+      VALUE_CANCELED("CANCELED"),
+      @SerializedName("PENDING_DEPENDENCY")
+      VALUE_PENDING_DEPENDENCY("PENDING_DEPENDENCY"),
+      @SerializedName("CANCELED_DEPENDENCY")
+      VALUE_CANCELED_DEPENDENCY("CANCELED_DEPENDENCY"),
+      @SerializedName("ERROR_DEPENDENCY")
+      VALUE_ERROR_DEPENDENCY("ERROR_DEPENDENCY"),
+      @SerializedName("ERROR_CONFLICTS")
+      VALUE_ERROR_CONFLICTS("ERROR_CONFLICTS"),
+      NULL(null);
+
+      private String value;
+
+      private EnumStatuses(String value) {
+        this.value = value;
+      }
+
+      @Override
+      public String toString() {
+        return value;
+      }
+  }
+
+
   synchronized /*package*/ static Gson getGson() {
     if (gson != null) {
       return gson;
@@ -422,22 +518,22 @@ public class AdAsyncRequest extends APINode {
   }
 
   public AdAsyncRequest copyFrom(AdAsyncRequest instance) {
+    this.mAsyncRequestSet = instance.mAsyncRequestSet;
+    this.mCreatedTime = instance.mCreatedTime;
     this.mId = instance.mId;
+    this.mInput = instance.mInput;
+    this.mResult = instance.mResult;
     this.mScopeObjectId = instance.mScopeObjectId;
     this.mStatus = instance.mStatus;
-    this.mResult = instance.mResult;
-    this.mInput = instance.mInput;
-    this.mAsyncRequestSet = instance.mAsyncRequestSet;
-    this.mCreatedTime = instance.mCreatedTime;
     this.mUpdatedTime = instance.mUpdatedTime;
-    this.mContext = instance.mContext;
+    this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
   }
 
   public static APIRequest.ResponseParser<AdAsyncRequest> getParser() {
     return new APIRequest.ResponseParser<AdAsyncRequest>() {
-      public APINodeList<AdAsyncRequest> parseResponse(String response, APIContext context, APIRequest<AdAsyncRequest> request) {
+      public APINodeList<AdAsyncRequest> parseResponse(String response, APIContext context, APIRequest<AdAsyncRequest> request) throws MalformedResponseException {
         return AdAsyncRequest.parseResponse(response, context, request);
       }
     };
