@@ -62,7 +62,7 @@ public class ProductFeed extends APINode {
   @SerializedName("delimiter")
   private EnumDelimiter mDelimiter = null;
   @SerializedName("encoding")
-  private EnumEncoding mEncoding = null;
+  private String mEncoding = null;
   @SerializedName("file_name")
   private String mFileName = null;
   @SerializedName("id")
@@ -302,7 +302,7 @@ public class ProductFeed extends APINode {
     return mDelimiter;
   }
 
-  public EnumEncoding getFieldEncoding() {
+  public String getFieldEncoding() {
     return mEncoding;
   }
 
@@ -380,7 +380,9 @@ public class ProductFeed extends APINode {
       "ordering_index",
       "pattern",
       "price",
+      "product_catalog",
       "product_feed",
+      "product_group",
       "product_type",
       "retailer_id",
       "retailer_product_group_id",
@@ -681,6 +683,13 @@ public class ProductFeed extends APINode {
       this.requestField("price", value);
       return this;
     }
+    public APIRequestGetProducts requestProductCatalogField () {
+      return this.requestProductCatalogField(true);
+    }
+    public APIRequestGetProducts requestProductCatalogField (boolean value) {
+      this.requestField("product_catalog", value);
+      return this;
+    }
     public APIRequestGetProducts requestProductFeedField () {
       return this.requestProductFeedField(true);
     }
@@ -688,6 +697,13 @@ public class ProductFeed extends APINode {
       this.requestField("product_feed", value);
       return this;
     }
+    public APIRequestGetProducts requestProductGroupField () {
+      return this.requestProductGroupField(true);
+    }
+    public APIRequestGetProducts requestProductGroupField (boolean value) {
+      this.requestField("product_group", value);
+      return this;
+    }
     public APIRequestGetProducts requestProductTypeField () {
       return this.requestProductTypeField(true);
     }
@@ -1327,6 +1343,7 @@ public class ProductFeed extends APINode {
       "encoding",
       "name",
       "quoted_fields",
+      "quoted_fields_mode",
       "schedule",
     };
 
@@ -1412,6 +1429,15 @@ public class ProductFeed extends APINode {
       return this;
     }
 
+    public APIRequestUpdate setQuotedFieldsMode (ProductFeed.EnumQuotedFieldsMode quotedFieldsMode) {
+      this.setParam("quoted_fields_mode", quotedFieldsMode);
+      return this;
+    }
+    public APIRequestUpdate setQuotedFieldsMode (String quotedFieldsMode) {
+      this.setParam("quoted_fields_mode", quotedFieldsMode);
+      return this;
+    }
+
     public APIRequestUpdate setSchedule (String schedule) {
       this.setParam("schedule", schedule);
       return this;
@@ -1482,26 +1508,18 @@ public class ProductFeed extends APINode {
       }
   }
 
-  public static enum EnumEncoding {
+  public static enum EnumQuotedFieldsMode {
       @SerializedName("AUTODETECT")
       VALUE_AUTODETECT("AUTODETECT"),
-      @SerializedName("LATIN1")
-      VALUE_LATIN1("LATIN1"),
-      @SerializedName("UTF8")
-      VALUE_UTF8("UTF8"),
-      @SerializedName("UTF16LE")
-      VALUE_UTF16LE("UTF16LE"),
-      @SerializedName("UTF16BE")
-      VALUE_UTF16BE("UTF16BE"),
-      @SerializedName("UTF32LE")
-      VALUE_UTF32LE("UTF32LE"),
-      @SerializedName("UTF32BE")
-      VALUE_UTF32BE("UTF32BE"),
+      @SerializedName("ON")
+      VALUE_ON("ON"),
+      @SerializedName("OFF")
+      VALUE_OFF("OFF"),
       NULL(null);
 
       private String value;
 
-      private EnumEncoding(String value) {
+      private EnumQuotedFieldsMode(String value) {
         this.value = value;
       }
 
@@ -1511,18 +1529,26 @@ public class ProductFeed extends APINode {
       }
   }
 
-  public static enum EnumQuotedFieldsMode {
+  public static enum EnumEncoding {
       @SerializedName("AUTODETECT")
       VALUE_AUTODETECT("AUTODETECT"),
-      @SerializedName("ON")
-      VALUE_ON("ON"),
-      @SerializedName("OFF")
-      VALUE_OFF("OFF"),
+      @SerializedName("LATIN1")
+      VALUE_LATIN1("LATIN1"),
+      @SerializedName("UTF8")
+      VALUE_UTF8("UTF8"),
+      @SerializedName("UTF16LE")
+      VALUE_UTF16LE("UTF16LE"),
+      @SerializedName("UTF16BE")
+      VALUE_UTF16BE("UTF16BE"),
+      @SerializedName("UTF32LE")
+      VALUE_UTF32LE("UTF32LE"),
+      @SerializedName("UTF32BE")
+      VALUE_UTF32BE("UTF32BE"),
       NULL(null);
 
       private String value;
 
-      private EnumQuotedFieldsMode(String value) {
+      private EnumEncoding(String value) {
         this.value = value;
       }
 
