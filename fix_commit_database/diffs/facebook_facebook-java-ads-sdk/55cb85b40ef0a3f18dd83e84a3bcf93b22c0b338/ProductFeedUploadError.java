@@ -53,6 +53,8 @@ import com.facebook.ads.sdk.APIException.MalformedResponseException;
 public class ProductFeedUploadError extends APINode {
   @SerializedName("description")
   private String mDescription = null;
+  @SerializedName("error_type")
+  private String mErrorType = null;
   @SerializedName("id")
   private String mId = null;
   @SerializedName("severity")
@@ -254,6 +256,10 @@ public class ProductFeedUploadError extends APINode {
     return mDescription;
   }
 
+  public String getFieldErrorType() {
+    return mErrorType;
+  }
+
   public String getFieldId() {
     return mId;
   }
@@ -392,6 +398,7 @@ public class ProductFeedUploadError extends APINode {
 
     public static final String[] FIELDS = {
       "description",
+      "error_type",
       "id",
       "severity",
       "summary",
@@ -474,6 +481,13 @@ public class ProductFeedUploadError extends APINode {
       this.requestField("description", value);
       return this;
     }
+    public APIRequestGet requestErrorTypeField () {
+      return this.requestErrorTypeField(true);
+    }
+    public APIRequestGet requestErrorTypeField (boolean value) {
+      this.requestField("error_type", value);
+      return this;
+    }
     public APIRequestGet requestIdField () {
       return this.requestIdField(true);
     }
@@ -539,6 +553,7 @@ public class ProductFeedUploadError extends APINode {
 
   public ProductFeedUploadError copyFrom(ProductFeedUploadError instance) {
     this.mDescription = instance.mDescription;
+    this.mErrorType = instance.mErrorType;
     this.mId = instance.mId;
     this.mSeverity = instance.mSeverity;
     this.mSummary = instance.mSummary;
