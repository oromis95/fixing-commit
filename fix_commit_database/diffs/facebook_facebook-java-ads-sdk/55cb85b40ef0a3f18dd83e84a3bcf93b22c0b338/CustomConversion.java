@@ -53,6 +53,8 @@ import com.facebook.ads.sdk.APIException.MalformedResponseException;
 public class CustomConversion extends APINode {
   @SerializedName("account_id")
   private String mAccountId = null;
+  @SerializedName("aggregation_rule")
+  private String mAggregationRule = null;
   @SerializedName("creation_time")
   private String mCreationTime = null;
   @SerializedName("custom_event_type")
@@ -61,6 +63,8 @@ public class CustomConversion extends APINode {
   private Long mDefaultConversionValue = null;
   @SerializedName("description")
   private String mDescription = null;
+  @SerializedName("event_source_type")
+  private String mEventSourceType = null;
   @SerializedName("first_fired_time")
   private String mFirstFiredTime = null;
   @SerializedName("id")
@@ -71,6 +75,8 @@ public class CustomConversion extends APINode {
   private String mLastFiredTime = null;
   @SerializedName("name")
   private String mName = null;
+  @SerializedName("offline_conversion_data_set")
+  private Object mOfflineConversionDataSet = null;
   @SerializedName("pixel")
   private AdsPixel mPixel = null;
   @SerializedName("pixel_aggregation_rule")
@@ -79,6 +85,8 @@ public class CustomConversion extends APINode {
   private String mPixelRule = null;
   @SerializedName("retention_days")
   private Long mRetentionDays = null;
+  @SerializedName("rule")
+  private String mRule = null;
   protected static Gson gson = null;
 
   CustomConversion() {
@@ -284,6 +292,10 @@ public class CustomConversion extends APINode {
     return mAccountId;
   }
 
+  public String getFieldAggregationRule() {
+    return mAggregationRule;
+  }
+
   public String getFieldCreationTime() {
     return mCreationTime;
   }
@@ -300,6 +312,10 @@ public class CustomConversion extends APINode {
     return mDescription;
   }
 
+  public String getFieldEventSourceType() {
+    return mEventSourceType;
+  }
+
   public String getFieldFirstFiredTime() {
     return mFirstFiredTime;
   }
@@ -320,6 +336,10 @@ public class CustomConversion extends APINode {
     return mName;
   }
 
+  public Object getFieldOfflineConversionDataSet() {
+    return mOfflineConversionDataSet;
+  }
+
   public AdsPixel getFieldPixel() {
     if (mPixel != null) {
       mPixel.context = getContext();
@@ -339,6 +359,10 @@ public class CustomConversion extends APINode {
     return mRetentionDays;
   }
 
+  public String getFieldRule() {
+    return mRule;
+  }
+
 
 
   public static class APIRequestGetActivities extends APIRequest<APINode> {
@@ -447,11 +471,11 @@ public class CustomConversion extends APINode {
 
   }
 
-  public static class APIRequestGetStats extends APIRequest<AdsPixelStatsResult> {
+  public static class APIRequestGetStats extends APIRequest<CustomConversionStatsResult> {
 
-    APINodeList<AdsPixelStatsResult> lastResponse = null;
+    APINodeList<CustomConversionStatsResult> lastResponse = null;
     @Override
-    public APINodeList<AdsPixelStatsResult> getLastResponse() {
+    public APINodeList<CustomConversionStatsResult> getLastResponse() {
       return lastResponse;
     }
     public static final String[] PARAMS = {
@@ -467,17 +491,17 @@ public class CustomConversion extends APINode {
     };
 
     @Override
-    public APINodeList<AdsPixelStatsResult> parseResponse(String response) throws APIException {
-      return AdsPixelStatsResult.parseResponse(response, getContext(), this);
+    public APINodeList<CustomConversionStatsResult> parseResponse(String response) throws APIException {
+      return CustomConversionStatsResult.parseResponse(response, getContext(), this);
     }
 
     @Override
-    public APINodeList<AdsPixelStatsResult> execute() throws APIException {
+    public APINodeList<CustomConversionStatsResult> execute() throws APIException {
       return execute(new HashMap<String, Object>());
     }
 
     @Override
-    public APINodeList<AdsPixelStatsResult> execute(Map<String, Object> extraParams) throws APIException {
+    public APINodeList<CustomConversionStatsResult> execute(Map<String, Object> extraParams) throws APIException {
       lastResponse = parseResponse(executeInternal(extraParams));
       return lastResponse;
     }
@@ -499,7 +523,7 @@ public class CustomConversion extends APINode {
     }
 
 
-    public APIRequestGetStats setAggregation (AdsPixelStatsResult.EnumAggregation aggregation) {
+    public APIRequestGetStats setAggregation (CustomConversionStatsResult.EnumAggregation aggregation) {
       this.setParam("aggregation", aggregation);
       return this;
     }
@@ -673,19 +697,23 @@ public class CustomConversion extends APINode {
 
     public static final String[] FIELDS = {
       "account_id",
+      "aggregation_rule",
       "creation_time",
       "custom_event_type",
       "default_conversion_value",
       "description",
+      "event_source_type",
       "first_fired_time",
       "id",
       "is_archived",
       "last_fired_time",
       "name",
+      "offline_conversion_data_set",
       "pixel",
       "pixel_aggregation_rule",
       "pixel_rule",
       "retention_days",
+      "rule",
     };
 
     @Override
@@ -764,6 +792,13 @@ public class CustomConversion extends APINode {
       this.requestField("account_id", value);
       return this;
     }
+    public APIRequestGet requestAggregationRuleField () {
+      return this.requestAggregationRuleField(true);
+    }
+    public APIRequestGet requestAggregationRuleField (boolean value) {
+      this.requestField("aggregation_rule", value);
+      return this;
+    }
     public APIRequestGet requestCreationTimeField () {
       return this.requestCreationTimeField(true);
     }
@@ -792,6 +827,13 @@ public class CustomConversion extends APINode {
       this.requestField("description", value);
       return this;
     }
+    public APIRequestGet requestEventSourceTypeField () {
+      return this.requestEventSourceTypeField(true);
+    }
+    public APIRequestGet requestEventSourceTypeField (boolean value) {
+      this.requestField("event_source_type", value);
+      return this;
+    }
     public APIRequestGet requestFirstFiredTimeField () {
       return this.requestFirstFiredTimeField(true);
     }
@@ -827,6 +869,13 @@ public class CustomConversion extends APINode {
       this.requestField("name", value);
       return this;
     }
+    public APIRequestGet requestOfflineConversionDataSetField () {
+      return this.requestOfflineConversionDataSetField(true);
+    }
+    public APIRequestGet requestOfflineConversionDataSetField (boolean value) {
+      this.requestField("offline_conversion_data_set", value);
+      return this;
+    }
     public APIRequestGet requestPixelField () {
       return this.requestPixelField(true);
     }
@@ -855,6 +904,13 @@ public class CustomConversion extends APINode {
       this.requestField("retention_days", value);
       return this;
     }
+    public APIRequestGet requestRuleField () {
+      return this.requestRuleField(true);
+    }
+    public APIRequestGet requestRuleField (boolean value) {
+      this.requestField("rule", value);
+      return this;
+    }
   }
 
   public static class APIRequestUpdate extends APIRequest<CustomConversion> {
@@ -1035,19 +1091,23 @@ public class CustomConversion extends APINode {
 
   public CustomConversion copyFrom(CustomConversion instance) {
     this.mAccountId = instance.mAccountId;
+    this.mAggregationRule = instance.mAggregationRule;
     this.mCreationTime = instance.mCreationTime;
     this.mCustomEventType = instance.mCustomEventType;
     this.mDefaultConversionValue = instance.mDefaultConversionValue;
     this.mDescription = instance.mDescription;
+    this.mEventSourceType = instance.mEventSourceType;
     this.mFirstFiredTime = instance.mFirstFiredTime;
     this.mId = instance.mId;
     this.mIsArchived = instance.mIsArchived;
     this.mLastFiredTime = instance.mLastFiredTime;
     this.mName = instance.mName;
+    this.mOfflineConversionDataSet = instance.mOfflineConversionDataSet;
     this.mPixel = instance.mPixel;
     this.mPixelAggregationRule = instance.mPixelAggregationRule;
     this.mPixelRule = instance.mPixelRule;
     this.mRetentionDays = instance.mRetentionDays;
+    this.mRule = instance.mRule;
     this.context = instance.context;
     this.rawValue = instance.rawValue;
     return this;
