@@ -131,6 +131,10 @@ public class AdAccount extends APINode {
   private String mPartner = null;
   @SerializedName("rf_spec")
   private ReachFrequencySpec mRfSpec = null;
+  @SerializedName("salesforce_invoice_group_id")
+  private String mSalesforceInvoiceGroupId = null;
+  @SerializedName("show_checkout_experience")
+  private Boolean mShowCheckoutExperience = null;
   @SerializedName("spend_cap")
   private String mSpendCap = null;
   @SerializedName("tax_id")
@@ -585,6 +589,14 @@ public class AdAccount extends APINode {
     return new APIRequestGetTargetingValidation(this.getPrefixedId().toString(), context);
   }
 
+  public APIRequestDeleteTracking deleteTracking() {
+    return new APIRequestDeleteTracking(this.getPrefixedId().toString(), context);
+  }
+
+  public APIRequestCreateTracking createTracking() {
+    return new APIRequestCreateTracking(this.getPrefixedId().toString(), context);
+  }
+
   public APIRequestGetTransactions getTransactions() {
     return new APIRequestGetTransactions(this.getPrefixedId().toString(), context);
   }
@@ -765,6 +777,14 @@ public class AdAccount extends APINode {
     return mRfSpec;
   }
 
+  public String getFieldSalesforceInvoiceGroupId() {
+    return mSalesforceInvoiceGroupId;
+  }
+
+  public Boolean getFieldShowCheckoutExperience() {
+    return mShowCheckoutExperience;
+  }
+
   public String getFieldSpendCap() {
     return mSpendCap;
   }
@@ -1052,6 +1072,7 @@ public class AdAccount extends APINode {
     public static final String[] FIELDS = {
       "account_id",
       "id",
+      "location_types",
       "name",
       "pages_count",
       "parent_page",
@@ -1140,6 +1161,13 @@ public class AdAccount extends APINode {
       this.requestField("id", value);
       return this;
     }
+    public APIRequestGetAdPlacePageSets requestLocationTypesField () {
+      return this.requestLocationTypesField(true);
+    }
+    public APIRequestGetAdPlacePageSets requestLocationTypesField (boolean value) {
+      this.requestField("location_types", value);
+      return this;
+    }
     public APIRequestGetAdPlacePageSets requestNameField () {
       return this.requestNameField(true);
     }
@@ -1171,6 +1199,7 @@ public class AdAccount extends APINode {
       return lastResponse;
     }
     public static final String[] PARAMS = {
+      "location_types",
       "name",
       "parent_page",
     };
@@ -1211,6 +1240,15 @@ public class AdAccount extends APINode {
     }
 
 
+    public APIRequestCreateAdPlacePageSet setLocationTypes (List<AdPlacePageSet.EnumLocationTypes> locationTypes) {
+      this.setParam("location_types", locationTypes);
+      return this;
+    }
+    public APIRequestCreateAdPlacePageSet setLocationTypes (String locationTypes) {
+      this.setParam("location_types", locationTypes);
+      return this;
+    }
+
     public APIRequestCreateAdPlacePageSet setName (String name) {
       this.setParam("name", name);
       return this;
@@ -1270,6 +1308,8 @@ public class AdAccount extends APINode {
     };
 
     public static final String[] FIELDS = {
+      "account_id",
+      "actor_id",
       "adlabels",
       "applink_treatment",
       "body",
@@ -1295,10 +1335,12 @@ public class AdAccount extends APINode {
       "product_set_id",
       "run_status",
       "template_url",
+      "template_url_spec",
       "thumbnail_url",
       "title",
       "url_tags",
       "use_page_actor_override",
+      "video_id",
     };
 
     @Override
@@ -1370,6 +1412,20 @@ public class AdAccount extends APINode {
       return this;
     }
 
+    public APIRequestGetAdCreatives requestAccountIdField () {
+      return this.requestAccountIdField(true);
+    }
+    public APIRequestGetAdCreatives requestAccountIdField (boolean value) {
+      this.requestField("account_id", value);
+      return this;
+    }
+    public APIRequestGetAdCreatives requestActorIdField () {
+      return this.requestActorIdField(true);
+    }
+    public APIRequestGetAdCreatives requestActorIdField (boolean value) {
+      this.requestField("actor_id", value);
+      return this;
+    }
     public APIRequestGetAdCreatives requestAdlabelsField () {
       return this.requestAdlabelsField(true);
     }
@@ -1545,6 +1601,13 @@ public class AdAccount extends APINode {
       this.requestField("template_url", value);
       return this;
     }
+    public APIRequestGetAdCreatives requestTemplateUrlSpecField () {
+      return this.requestTemplateUrlSpecField(true);
+    }
+    public APIRequestGetAdCreatives requestTemplateUrlSpecField (boolean value) {
+      this.requestField("template_url_spec", value);
+      return this;
+    }
     public APIRequestGetAdCreatives requestThumbnailUrlField () {
       return this.requestThumbnailUrlField(true);
     }
@@ -1573,6 +1636,13 @@ public class AdAccount extends APINode {
       this.requestField("use_page_actor_override", value);
       return this;
     }
+    public APIRequestGetAdCreatives requestVideoIdField () {
+      return this.requestVideoIdField(true);
+    }
+    public APIRequestGetAdCreatives requestVideoIdField (boolean value) {
+      this.requestField("video_id", value);
+      return this;
+    }
   }
 
   public static class APIRequestCreateAdCreative extends APIRequest<AdCreative> {
@@ -1584,6 +1654,7 @@ public class AdAccount extends APINode {
     }
     public static final String[] PARAMS = {
       "action_spec",
+      "actor_id",
       "adlabels",
       "applink_treatment",
       "body",
@@ -1609,11 +1680,11 @@ public class AdAccount extends APINode {
       "platform_customizations",
       "product_set_id",
       "template_url",
+      "template_url_spec",
       "thumbnail_url",
       "title",
       "url_tags",
       "use_page_actor_override",
-      "video_id",
     };
 
     public static final String[] FIELDS = {
@@ -1661,6 +1732,15 @@ public class AdAccount extends APINode {
       return this;
     }
 
+    public APIRequestCreateAdCreative setActorId (Long actorId) {
+      this.setParam("actor_id", actorId);
+      return this;
+    }
+    public APIRequestCreateAdCreative setActorId (String actorId) {
+      this.setParam("actor_id", actorId);
+      return this;
+    }
+
     public APIRequestCreateAdCreative setAdlabels (List<Object> adlabels) {
       this.setParam("adlabels", adlabels);
       return this;
@@ -1830,6 +1910,15 @@ public class AdAccount extends APINode {
       return this;
     }
 
+    public APIRequestCreateAdCreative setTemplateUrlSpec (Object templateUrlSpec) {
+      this.setParam("template_url_spec", templateUrlSpec);
+      return this;
+    }
+    public APIRequestCreateAdCreative setTemplateUrlSpec (String templateUrlSpec) {
+      this.setParam("template_url_spec", templateUrlSpec);
+      return this;
+    }
+
     public APIRequestCreateAdCreative setThumbnailUrl (String thumbnailUrl) {
       this.setParam("thumbnail_url", thumbnailUrl);
       return this;
@@ -1854,15 +1943,6 @@ public class AdAccount extends APINode {
       return this;
     }
 
-    public APIRequestCreateAdCreative setVideoId (Long videoId) {
-      this.setParam("video_id", videoId);
-      return this;
-    }
-    public APIRequestCreateAdCreative setVideoId (String videoId) {
-      this.setParam("video_id", videoId);
-      return this;
-    }
-
     public APIRequestCreateAdCreative requestAllFields () {
       return this.requestAllFields(true);
     }
@@ -1914,6 +1994,8 @@ public class AdAccount extends APINode {
     };
 
     public static final String[] FIELDS = {
+      "account_id",
+      "actor_id",
       "adlabels",
       "applink_treatment",
       "body",
@@ -1939,10 +2021,12 @@ public class AdAccount extends APINode {
       "product_set_id",
       "run_status",
       "template_url",
+      "template_url_spec",
       "thumbnail_url",
       "title",
       "url_tags",
       "use_page_actor_override",
+      "video_id",
     };
 
     @Override
@@ -2032,6 +2116,20 @@ public class AdAccount extends APINode {
       return this;
     }
 
+    public APIRequestGetAdCreativesByLabels requestAccountIdField () {
+      return this.requestAccountIdField(true);
+    }
+    public APIRequestGetAdCreativesByLabels requestAccountIdField (boolean value) {
+      this.requestField("account_id", value);
+      return this;
+    }
+    public APIRequestGetAdCreativesByLabels requestActorIdField () {
+      return this.requestActorIdField(true);
+    }
+    public APIRequestGetAdCreativesByLabels requestActorIdField (boolean value) {
+      this.requestField("actor_id", value);
+      return this;
+    }
     public APIRequestGetAdCreativesByLabels requestAdlabelsField () {
       return this.requestAdlabelsField(true);
     }
@@ -2207,6 +2305,13 @@ public class AdAccount extends APINode {
       this.requestField("template_url", value);
       return this;
     }
+    public APIRequestGetAdCreativesByLabels requestTemplateUrlSpecField () {
+      return this.requestTemplateUrlSpecField(true);
+    }
+    public APIRequestGetAdCreativesByLabels requestTemplateUrlSpecField (boolean value) {
+      this.requestField("template_url_spec", value);
+      return this;
+    }
     public APIRequestGetAdCreativesByLabels requestThumbnailUrlField () {
       return this.requestThumbnailUrlField(true);
     }
@@ -2235,6 +2340,13 @@ public class AdAccount extends APINode {
       this.requestField("use_page_actor_override", value);
       return this;
     }
+    public APIRequestGetAdCreativesByLabels requestVideoIdField () {
+      return this.requestVideoIdField(true);
+    }
+    public APIRequestGetAdCreativesByLabels requestVideoIdField (boolean value) {
+      this.requestField("video_id", value);
+      return this;
+    }
   }
 
   public static class APIRequestDeleteAdImages extends APIRequest<APINode> {
@@ -2350,6 +2462,7 @@ public class AdAccount extends APINode {
       "hash",
       "height",
       "id",
+      "is_associated_creatives_in_adgroups",
       "name",
       "original_height",
       "original_width",
@@ -2518,6 +2631,13 @@ public class AdAccount extends APINode {
       this.requestField("id", value);
       return this;
     }
+    public APIRequestGetAdImages requestIsAssociatedCreativesInAdgroupsField () {
+      return this.requestIsAssociatedCreativesInAdgroupsField(true);
+    }
+    public APIRequestGetAdImages requestIsAssociatedCreativesInAdgroupsField (boolean value) {
+      this.requestField("is_associated_creatives_in_adgroups", value);
+      return this;
+    }
     public APIRequestGetAdImages requestNameField () {
       return this.requestNameField(true);
     }
@@ -3499,6 +3619,7 @@ public class AdAccount extends APINode {
     public static final String[] PARAMS = {
       "adlabels",
       "adset_id",
+      "adset_spec",
       "bid_amount",
       "creative",
       "date_format",
@@ -3574,6 +3695,15 @@ public class AdAccount extends APINode {
       return this;
     }
 
+    public APIRequestCreateAd setAdsetSpec (AdSet adsetSpec) {
+      this.setParam("adset_spec", adsetSpec);
+      return this;
+    }
+    public APIRequestCreateAd setAdsetSpec (String adsetSpec) {
+      this.setParam("adset_spec", adsetSpec);
+      return this;
+    }
+
     public APIRequestCreateAd setBidAmount (Long bidAmount) {
       this.setParam("bid_amount", bidAmount);
       return this;
@@ -4022,6 +4152,7 @@ public class AdAccount extends APINode {
       "time_based_ad_rotation_intervals",
       "updated_time",
       "use_new_app_click",
+      "view_through_attribution_window_days",
     };
 
     @Override
@@ -4423,6 +4554,13 @@ public class AdAccount extends APINode {
       this.requestField("use_new_app_click", value);
       return this;
     }
+    public APIRequestGetAdSets requestViewThroughAttributionWindowDaysField () {
+      return this.requestViewThroughAttributionWindowDaysField(true);
+    }
+    public APIRequestGetAdSets requestViewThroughAttributionWindowDaysField (boolean value) {
+      this.requestField("view_through_attribution_window_days", value);
+      return this;
+    }
   }
 
   public static class APIRequestCreateAdSet extends APIRequest<AdSet> {
@@ -4439,6 +4577,7 @@ public class AdAccount extends APINode {
       "bid_amount",
       "billing_event",
       "campaign_id",
+      "campaign_spec",
       "creative_sequence",
       "daily_budget",
       "daily_imps",
@@ -4461,6 +4600,7 @@ public class AdAccount extends APINode {
       "targeting",
       "time_based_ad_rotation_id_blocks",
       "time_based_ad_rotation_intervals",
+      "view_through_attribution_window_days",
     };
 
     public static final String[] FIELDS = {
@@ -4549,6 +4689,15 @@ public class AdAccount extends APINode {
       return this;
     }
 
+    public APIRequestCreateAdSet setCampaignSpec (Object campaignSpec) {
+      this.setParam("campaign_spec", campaignSpec);
+      return this;
+    }
+    public APIRequestCreateAdSet setCampaignSpec (String campaignSpec) {
+      this.setParam("campaign_spec", campaignSpec);
+      return this;
+    }
+
     public APIRequestCreateAdSet setCreativeSequence (List<String> creativeSequence) {
       this.setParam("creative_sequence", creativeSequence);
       return this;
@@ -4731,6 +4880,15 @@ public class AdAccount extends APINode {
       return this;
     }
 
+    public APIRequestCreateAdSet setViewThroughAttributionWindowDays (Long viewThroughAttributionWindowDays) {
+      this.setParam("view_through_attribution_window_days", viewThroughAttributionWindowDays);
+      return this;
+    }
+    public APIRequestCreateAdSet setViewThroughAttributionWindowDays (String viewThroughAttributionWindowDays) {
+      this.setParam("view_through_attribution_window_days", viewThroughAttributionWindowDays);
+      return this;
+    }
+
     public APIRequestCreateAdSet requestAllFields () {
       return this.requestAllFields(true);
     }
@@ -4822,6 +4980,7 @@ public class AdAccount extends APINode {
       "time_based_ad_rotation_intervals",
       "updated_time",
       "use_new_app_click",
+      "view_through_attribution_window_days",
     };
 
     @Override
@@ -5191,6 +5350,13 @@ public class AdAccount extends APINode {
       this.requestField("use_new_app_click", value);
       return this;
     }
+    public APIRequestGetAdSetsByLabels requestViewThroughAttributionWindowDaysField () {
+      return this.requestViewThroughAttributionWindowDaysField(true);
+    }
+    public APIRequestGetAdSetsByLabels requestViewThroughAttributionWindowDaysField (boolean value) {
+      this.requestField("view_through_attribution_window_days", value);
+      return this;
+    }
   }
 
   public static class APIRequestGetAdsPixels extends APIRequest<AdsPixel> {
@@ -5207,6 +5373,7 @@ public class AdAccount extends APINode {
       "code",
       "creation_time",
       "id",
+      "is_created_by_business",
       "last_fired_time",
       "name",
       "owner_ad_account",
@@ -5303,6 +5470,13 @@ public class AdAccount extends APINode {
       this.requestField("id", value);
       return this;
     }
+    public APIRequestGetAdsPixels requestIsCreatedByBusinessField () {
+      return this.requestIsCreatedByBusinessField(true);
+    }
+    public APIRequestGetAdsPixels requestIsCreatedByBusinessField (boolean value) {
+      this.requestField("is_created_by_business", value);
+      return this;
+    }
     public APIRequestGetAdsPixels requestLastFiredTimeField () {
       return this.requestLastFiredTimeField(true);
     }
@@ -5623,6 +5797,7 @@ public class AdAccount extends APINode {
       "og_object_id",
       "og_phrase",
       "og_suggestion_mechanism",
+      "original_projection_type",
       "referenced_sticker_id",
       "slideshow_spec",
       "start_offset",
@@ -5753,6 +5928,15 @@ public class AdAccount extends APINode {
       return this;
     }
 
+    public APIRequestCreateAdVideo setOriginalProjectionType (EnumOriginalProjectionType originalProjectionType) {
+      this.setParam("original_projection_type", originalProjectionType);
+      return this;
+    }
+    public APIRequestCreateAdVideo setOriginalProjectionType (String originalProjectionType) {
+      this.setParam("original_projection_type", originalProjectionType);
+      return this;
+    }
+
     public APIRequestCreateAdVideo setReferencedStickerId (String referencedStickerId) {
       this.setParam("referenced_sticker_id", referencedStickerId);
       return this;
@@ -6597,8 +6781,10 @@ public class AdAccount extends APINode {
     public static final String[] FIELDS = {
       "account_id",
       "adlabels",
+      "brand_lift_studies",
       "budget_rebalance_flag",
       "buying_type",
+      "can_create_brand_lift_study",
       "can_use_spend_cap",
       "configured_status",
       "created_time",
@@ -6738,6 +6924,13 @@ public class AdAccount extends APINode {
       this.requestField("adlabels", value);
       return this;
     }
+    public APIRequestGetCampaigns requestBrandLiftStudiesField () {
+      return this.requestBrandLiftStudiesField(true);
+    }
+    public APIRequestGetCampaigns requestBrandLiftStudiesField (boolean value) {
+      this.requestField("brand_lift_studies", value);
+      return this;
+    }
     public APIRequestGetCampaigns requestBudgetRebalanceFlagField () {
       return this.requestBudgetRebalanceFlagField(true);
     }
@@ -6752,6 +6945,13 @@ public class AdAccount extends APINode {
       this.requestField("buying_type", value);
       return this;
     }
+    public APIRequestGetCampaigns requestCanCreateBrandLiftStudyField () {
+      return this.requestCanCreateBrandLiftStudyField(true);
+    }
+    public APIRequestGetCampaigns requestCanCreateBrandLiftStudyField (boolean value) {
+      this.requestField("can_create_brand_lift_study", value);
+      return this;
+    }
     public APIRequestGetCampaigns requestCanUseSpendCapField () {
       return this.requestCanUseSpendCapField(true);
     }
@@ -7026,8 +7226,10 @@ public class AdAccount extends APINode {
     public static final String[] FIELDS = {
       "account_id",
       "adlabels",
+      "brand_lift_studies",
       "budget_rebalance_flag",
       "buying_type",
+      "can_create_brand_lift_study",
       "can_use_spend_cap",
       "configured_status",
       "created_time",
@@ -7144,6 +7346,13 @@ public class AdAccount extends APINode {
       this.requestField("adlabels", value);
       return this;
     }
+    public APIRequestGetCampaignsByLabels requestBrandLiftStudiesField () {
+      return this.requestBrandLiftStudiesField(true);
+    }
+    public APIRequestGetCampaignsByLabels requestBrandLiftStudiesField (boolean value) {
+      this.requestField("brand_lift_studies", value);
+      return this;
+    }
     public APIRequestGetCampaignsByLabels requestBudgetRebalanceFlagField () {
       return this.requestBudgetRebalanceFlagField(true);
     }
@@ -7158,6 +7367,13 @@ public class AdAccount extends APINode {
       this.requestField("buying_type", value);
       return this;
     }
+    public APIRequestGetCampaignsByLabels requestCanCreateBrandLiftStudyField () {
+      return this.requestCanCreateBrandLiftStudyField(true);
+    }
+    public APIRequestGetCampaignsByLabels requestCanCreateBrandLiftStudyField (boolean value) {
+      this.requestField("can_create_brand_lift_study", value);
+      return this;
+    }
     public APIRequestGetCampaignsByLabels requestCanUseSpendCapField () {
       return this.requestCanUseSpendCapField(true);
     }
@@ -7380,6 +7596,7 @@ public class AdAccount extends APINode {
       "dataset_id",
       "description",
       "event_source_group",
+      "is_value_based",
       "list_of_accounts",
       "lookalike_spec",
       "name",
@@ -7462,6 +7679,15 @@ public class AdAccount extends APINode {
       return this;
     }
 
+    public APIRequestCreateCustomAudience setIsValueBased (Boolean isValueBased) {
+      this.setParam("is_value_based", isValueBased);
+      return this;
+    }
+    public APIRequestCreateCustomAudience setIsValueBased (String isValueBased) {
+      this.setParam("is_value_based", isValueBased);
+      return this;
+    }
+
     public APIRequestCreateCustomAudience setListOfAccounts (List<Long> listOfAccounts) {
       this.setParam("list_of_accounts", listOfAccounts);
       return this;
@@ -7687,14 +7913,17 @@ public class AdAccount extends APINode {
       return lastResponse;
     }
     public static final String[] PARAMS = {
+      "aggregation_rule",
       "custom_event_type",
       "default_conversion_value",
       "description",
+      "event_source_id",
       "name",
       "pixel_aggregation_rule",
       "pixel_id",
       "pixel_rule",
       "retention_days",
+      "rule",
     };
 
     public static final String[] FIELDS = {
@@ -7733,6 +7962,11 @@ public class AdAccount extends APINode {
     }
 
 
+    public APIRequestCreateCustomConversion setAggregationRule (String aggregationRule) {
+      this.setParam("aggregation_rule", aggregationRule);
+      return this;
+    }
+
     public APIRequestCreateCustomConversion setCustomEventType (CustomConversion.EnumCustomEventType customEventType) {
       this.setParam("custom_event_type", customEventType);
       return this;
@@ -7756,6 +7990,11 @@ public class AdAccount extends APINode {
       return this;
     }
 
+    public APIRequestCreateCustomConversion setEventSourceId (String eventSourceId) {
+      this.setParam("event_source_id", eventSourceId);
+      return this;
+    }
+
     public APIRequestCreateCustomConversion setName (String name) {
       this.setParam("name", name);
       return this;
@@ -7785,6 +8024,11 @@ public class AdAccount extends APINode {
       return this;
     }
 
+    public APIRequestCreateCustomConversion setRule (String rule) {
+      this.setParam("rule", rule);
+      return this;
+    }
+
     public APIRequestCreateCustomConversion requestAllFields () {
       return this.requestAllFields(true);
     }
@@ -7833,13 +8077,13 @@ public class AdAccount extends APINode {
     public static final String[] PARAMS = {
       "ad_format",
       "creative",
-      "dynamic_creative_spec",
+      "end_date",
       "height",
-      "interactive",
       "locale",
       "place_page_id",
       "post",
       "product_item_ids",
+      "start_date",
       "width",
     };
 
@@ -7898,12 +8142,8 @@ public class AdAccount extends APINode {
       return this;
     }
 
-    public APIRequestGetGeneratePreviews setDynamicCreativeSpec (Object dynamicCreativeSpec) {
-      this.setParam("dynamic_creative_spec", dynamicCreativeSpec);
-      return this;
-    }
-    public APIRequestGetGeneratePreviews setDynamicCreativeSpec (String dynamicCreativeSpec) {
-      this.setParam("dynamic_creative_spec", dynamicCreativeSpec);
+    public APIRequestGetGeneratePreviews setEndDate (String endDate) {
+      this.setParam("end_date", endDate);
       return this;
     }
 
@@ -7916,15 +8156,6 @@ public class AdAccount extends APINode {
       return this;
     }
 
-    public APIRequestGetGeneratePreviews setInteractive (Boolean interactive) {
-      this.setParam("interactive", interactive);
-      return this;
-    }
-    public APIRequestGetGeneratePreviews setInteractive (String interactive) {
-      this.setParam("interactive", interactive);
-      return this;
-    }
-
     public APIRequestGetGeneratePreviews setLocale (String locale) {
       this.setParam("locale", locale);
       return this;
@@ -7957,6 +8188,11 @@ public class AdAccount extends APINode {
       return this;
     }
 
+    public APIRequestGetGeneratePreviews setStartDate (String startDate) {
+      this.setParam("start_date", startDate);
+      return this;
+    }
+
     public APIRequestGetGeneratePreviews setWidth (Long width) {
       this.setParam("width", width);
       return this;
@@ -8649,6 +8885,7 @@ public class AdAccount extends APINode {
       "locale",
       "messenger_welcome_message",
       "name",
+      "organic_leads_count",
       "page",
       "page_id",
       "privacy_policy_url",
@@ -8858,6 +9095,13 @@ public class AdAccount extends APINode {
       this.requestField("name", value);
       return this;
     }
+    public APIRequestGetLeadGenForms requestOrganicLeadsCountField () {
+      return this.requestOrganicLeadsCountField(true);
+    }
+    public APIRequestGetLeadGenForms requestOrganicLeadsCountField (boolean value) {
+      this.requestField("organic_leads_count", value);
+      return this;
+    }
     public APIRequestGetLeadGenForms requestPageField () {
       return this.requestPageField(true);
     }
@@ -9534,6 +9778,10 @@ public class AdAccount extends APINode {
       return this;
     }
 
+    public APIRequestGetPartnerCategories setPrivateOrPublic (PartnerCategory.EnumPrivateOrPublic privateOrPublic) {
+      this.setParam("private_or_public", privateOrPublic);
+      return this;
+    }
     public APIRequestGetPartnerCategories setPrivateOrPublic (String privateOrPublic) {
       this.setParam("private_or_public", privateOrPublic);
       return this;
@@ -10419,6 +10667,7 @@ public class AdAccount extends APINode {
       "holdout_percentage",
       "id",
       "instagram_destination_id",
+      "interval_frequency_cap",
       "interval_frequency_cap_reset_period",
       "name",
       "pause_periods",
@@ -10664,6 +10913,13 @@ public class AdAccount extends APINode {
       this.requestField("instagram_destination_id", value);
       return this;
     }
+    public APIRequestGetReachFrequencyPredictions requestIntervalFrequencyCapField () {
+      return this.requestIntervalFrequencyCapField(true);
+    }
+    public APIRequestGetReachFrequencyPredictions requestIntervalFrequencyCapField (boolean value) {
+      this.requestField("interval_frequency_cap", value);
+      return this;
+    }
     public APIRequestGetReachFrequencyPredictions requestIntervalFrequencyCapResetPeriodField () {
       return this.requestIntervalFrequencyCapResetPeriodField(true);
     }
@@ -11903,6 +12159,194 @@ public class AdAccount extends APINode {
     }
   }
 
+  public static class APIRequestDeleteTracking extends APIRequest<APINode> {
+
+    APINodeList<APINode> lastResponse = null;
+    @Override
+    public APINodeList<APINode> getLastResponse() {
+      return lastResponse;
+    }
+    public static final String[] PARAMS = {
+      "tracking_specs",
+    };
+
+    public static final String[] FIELDS = {
+    };
+
+    @Override
+    public APINodeList<APINode> parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this);
+    }
+
+    @Override
+    public APINodeList<APINode> execute() throws APIException {
+      return execute(new HashMap<String, Object>());
+    }
+
+    @Override
+    public APINodeList<APINode> execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
+    }
+
+    public APIRequestDeleteTracking(String nodeId, APIContext context) {
+      super(context, nodeId, "/tracking", "DELETE", Arrays.asList(PARAMS));
+    }
+
+    @Override
+    public APIRequestDeleteTracking setParam(String param, Object value) {
+      setParamInternal(param, value);
+      return this;
+    }
+
+    @Override
+    public APIRequestDeleteTracking setParams(Map<String, Object> params) {
+      setParamsInternal(params);
+      return this;
+    }
+
+
+    public APIRequestDeleteTracking setTrackingSpecs (Object trackingSpecs) {
+      this.setParam("tracking_specs", trackingSpecs);
+      return this;
+    }
+    public APIRequestDeleteTracking setTrackingSpecs (String trackingSpecs) {
+      this.setParam("tracking_specs", trackingSpecs);
+      return this;
+    }
+
+    public APIRequestDeleteTracking requestAllFields () {
+      return this.requestAllFields(true);
+    }
+
+    public APIRequestDeleteTracking requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestDeleteTracking requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
+    }
+
+    @Override
+    public APIRequestDeleteTracking requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestDeleteTracking requestField (String field) {
+      this.requestField(field, true);
+      return this;
+    }
+
+    @Override
+    public APIRequestDeleteTracking requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
+      return this;
+    }
+
+  }
+
+  public static class APIRequestCreateTracking extends APIRequest<APINode> {
+
+    APINode lastResponse = null;
+    @Override
+    public APINode getLastResponse() {
+      return lastResponse;
+    }
+    public static final String[] PARAMS = {
+      "tracking_specs",
+    };
+
+    public static final String[] FIELDS = {
+    };
+
+    @Override
+    public APINode parseResponse(String response) throws APIException {
+      return APINode.parseResponse(response, getContext(), this).head();
+    }
+
+    @Override
+    public APINode execute() throws APIException {
+      return execute(new HashMap<String, Object>());
+    }
+
+    @Override
+    public APINode execute(Map<String, Object> extraParams) throws APIException {
+      lastResponse = parseResponse(executeInternal(extraParams));
+      return lastResponse;
+    }
+
+    public APIRequestCreateTracking(String nodeId, APIContext context) {
+      super(context, nodeId, "/tracking", "POST", Arrays.asList(PARAMS));
+    }
+
+    @Override
+    public APIRequestCreateTracking setParam(String param, Object value) {
+      setParamInternal(param, value);
+      return this;
+    }
+
+    @Override
+    public APIRequestCreateTracking setParams(Map<String, Object> params) {
+      setParamsInternal(params);
+      return this;
+    }
+
+
+    public APIRequestCreateTracking setTrackingSpecs (Object trackingSpecs) {
+      this.setParam("tracking_specs", trackingSpecs);
+      return this;
+    }
+    public APIRequestCreateTracking setTrackingSpecs (String trackingSpecs) {
+      this.setParam("tracking_specs", trackingSpecs);
+      return this;
+    }
+
+    public APIRequestCreateTracking requestAllFields () {
+      return this.requestAllFields(true);
+    }
+
+    public APIRequestCreateTracking requestAllFields (boolean value) {
+      for (String field : FIELDS) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestCreateTracking requestFields (List<String> fields) {
+      return this.requestFields(fields, true);
+    }
+
+    @Override
+    public APIRequestCreateTracking requestFields (List<String> fields, boolean value) {
+      for (String field : fields) {
+        this.requestField(field, value);
+      }
+      return this;
+    }
+
+    @Override
+    public APIRequestCreateTracking requestField (String field) {
+      this.requestField(field, true);
+      return this;
+    }
+
+    @Override
+    public APIRequestCreateTracking requestField (String field, boolean value) {
+      this.requestFieldInternal(field, value);
+      return this;
+    }
+
+  }
+
   public static class APIRequestGetTransactions extends APIRequest<Transaction> {
 
     APINodeList<Transaction> lastResponse = null;
@@ -11922,6 +12366,8 @@ public class AdAccount extends APINode {
       "billing_reason",
       "billing_start_time",
       "charge_type",
+      "checkout_campaign_group_id",
+      "credential_id",
       "fatura_id",
       "id",
       "payment_option",
@@ -12061,6 +12507,20 @@ public class AdAccount extends APINode {
       this.requestField("charge_type", value);
       return this;
     }
+    public APIRequestGetTransactions requestCheckoutCampaignGroupIdField () {
+      return this.requestCheckoutCampaignGroupIdField(true);
+    }
+    public APIRequestGetTransactions requestCheckoutCampaignGroupIdField (boolean value) {
+      this.requestField("checkout_campaign_group_id", value);
+      return this;
+    }
+    public APIRequestGetTransactions requestCredentialIdField () {
+      return this.requestCredentialIdField(true);
+    }
+    public APIRequestGetTransactions requestCredentialIdField (boolean value) {
+      this.requestField("credential_id", value);
+      return this;
+    }
     public APIRequestGetTransactions requestFaturaIdField () {
       return this.requestFaturaIdField(true);
     }
@@ -12285,6 +12745,8 @@ public class AdAccount extends APINode {
       "owner",
       "partner",
       "rf_spec",
+      "salesforce_invoice_group_id",
+      "show_checkout_experience",
       "spend_cap",
       "tax_id",
       "tax_id_status",
@@ -12638,6 +13100,20 @@ public class AdAccount extends APINode {
       this.requestField("rf_spec", value);
       return this;
     }
+    public APIRequestGet requestSalesforceInvoiceGroupIdField () {
+      return this.requestSalesforceInvoiceGroupIdField(true);
+    }
+    public APIRequestGet requestSalesforceInvoiceGroupIdField (boolean value) {
+      this.requestField("salesforce_invoice_group_id", value);
+      return this;
+    }
+    public APIRequestGet requestShowCheckoutExperienceField () {
+      return this.requestShowCheckoutExperienceField(true);
+    }
+    public APIRequestGet requestShowCheckoutExperienceField (boolean value) {
+      this.requestField("show_checkout_experience", value);
+      return this;
+    }
     public APIRequestGet requestSpendCapField () {
       return this.requestSpendCapField(true);
     }
@@ -12913,6 +13389,25 @@ public class AdAccount extends APINode {
       }
   }
 
+  public static enum EnumOriginalProjectionType {
+      @SerializedName("equirectangular")
+      VALUE_EQUIRECTANGULAR("equirectangular"),
+      @SerializedName("cubemap")
+      VALUE_CUBEMAP("cubemap"),
+      NULL(null);
+
+      private String value;
+
+      private EnumOriginalProjectionType(String value) {
+        this.value = value;
+      }
+
+      @Override
+      public String toString() {
+        return value;
+      }
+  }
+
   public static enum EnumUnpublishedContentType {
       @SerializedName("SCHEDULED")
       VALUE_SCHEDULED("SCHEDULED"),
@@ -13072,6 +13567,8 @@ public class AdAccount extends APINode {
     this.mOwner = instance.mOwner;
     this.mPartner = instance.mPartner;
     this.mRfSpec = instance.mRfSpec;
+    this.mSalesforceInvoiceGroupId = instance.mSalesforceInvoiceGroupId;
+    this.mShowCheckoutExperience = instance.mShowCheckoutExperience;
     this.mSpendCap = instance.mSpendCap;
     this.mTaxId = instance.mTaxId;
     this.mTaxIdStatus = instance.mTaxIdStatus;
