@@ -61,6 +61,8 @@ public class Targeting extends APINode {
   private String mAlternateAutoTargetingOption = null;
   @SerializedName("app_install_state")
   private String mAppInstallState = null;
+  @SerializedName("audience_network_positions")
+  private List<String> mAudienceNetworkPositions = null;
   @SerializedName("behaviors")
   private List<IDName> mBehaviors = null;
   @SerializedName("cities")
@@ -87,10 +89,16 @@ public class Targeting extends APINode {
   private List<IDName> mEducationSchools = null;
   @SerializedName("education_statuses")
   private List<Long> mEducationStatuses = null;
+  @SerializedName("effective_audience_network_positions")
+  private List<String> mEffectiveAudienceNetworkPositions = null;
   @SerializedName("effective_device_platforms")
   private List<EnumEffectiveDevicePlatforms> mEffectiveDevicePlatforms = null;
   @SerializedName("effective_facebook_positions")
   private List<String> mEffectiveFacebookPositions = null;
+  @SerializedName("effective_instagram_positions")
+  private List<String> mEffectiveInstagramPositions = null;
+  @SerializedName("effective_messenger_positions")
+  private List<String> mEffectiveMessengerPositions = null;
   @SerializedName("effective_publisher_platforms")
   private List<String> mEffectivePublisherPlatforms = null;
   @SerializedName("engagement_specs")
@@ -151,6 +159,8 @@ public class Targeting extends APINode {
   private List<IDName> mIncome = null;
   @SerializedName("industries")
   private List<IDName> mIndustries = null;
+  @SerializedName("instagram_positions")
+  private List<String> mInstagramPositions = null;
   @SerializedName("interested_in")
   private List<Long> mInterestedIn = null;
   @SerializedName("interests")
@@ -161,6 +171,8 @@ public class Targeting extends APINode {
   private List<IDName> mLifeEvents = null;
   @SerializedName("locales")
   private List<Long> mLocales = null;
+  @SerializedName("messenger_positions")
+  private List<String> mMessengerPositions = null;
   @SerializedName("moms")
   private List<IDName> mMoms = null;
   @SerializedName("net_worth")
@@ -395,6 +407,15 @@ public class Targeting extends APINode {
     return this;
   }
 
+  public List<String> getFieldAudienceNetworkPositions() {
+    return mAudienceNetworkPositions;
+  }
+
+  public Targeting setFieldAudienceNetworkPositions(List<String> value) {
+    this.mAudienceNetworkPositions = value;
+    return this;
+  }
+
   public List<IDName> getFieldBehaviors() {
     return mBehaviors;
   }
@@ -542,6 +563,15 @@ public class Targeting extends APINode {
     return this;
   }
 
+  public List<String> getFieldEffectiveAudienceNetworkPositions() {
+    return mEffectiveAudienceNetworkPositions;
+  }
+
+  public Targeting setFieldEffectiveAudienceNetworkPositions(List<String> value) {
+    this.mEffectiveAudienceNetworkPositions = value;
+    return this;
+  }
+
   public List<EnumEffectiveDevicePlatforms> getFieldEffectiveDevicePlatforms() {
     return mEffectiveDevicePlatforms;
   }
@@ -560,6 +590,24 @@ public class Targeting extends APINode {
     return this;
   }
 
+  public List<String> getFieldEffectiveInstagramPositions() {
+    return mEffectiveInstagramPositions;
+  }
+
+  public Targeting setFieldEffectiveInstagramPositions(List<String> value) {
+    this.mEffectiveInstagramPositions = value;
+    return this;
+  }
+
+  public List<String> getFieldEffectiveMessengerPositions() {
+    return mEffectiveMessengerPositions;
+  }
+
+  public Targeting setFieldEffectiveMessengerPositions(List<String> value) {
+    this.mEffectiveMessengerPositions = value;
+    return this;
+  }
+
   public List<String> getFieldEffectivePublisherPlatforms() {
     return mEffectivePublisherPlatforms;
   }
@@ -925,6 +973,15 @@ public class Targeting extends APINode {
     this.mIndustries = IDName.getGson().fromJson(value, type);
     return this;
   }
+  public List<String> getFieldInstagramPositions() {
+    return mInstagramPositions;
+  }
+
+  public Targeting setFieldInstagramPositions(List<String> value) {
+    this.mInstagramPositions = value;
+    return this;
+  }
+
   public List<Long> getFieldInterestedIn() {
     return mInterestedIn;
   }
@@ -980,6 +1037,15 @@ public class Targeting extends APINode {
     return this;
   }
 
+  public List<String> getFieldMessengerPositions() {
+    return mMessengerPositions;
+  }
+
+  public Targeting setFieldMessengerPositions(List<String> value) {
+    this.mMessengerPositions = value;
+    return this;
+  }
+
   public List<IDName> getFieldMoms() {
     return mMoms;
   }
@@ -1248,6 +1314,8 @@ public class Targeting extends APINode {
       VALUE_MOBILE("mobile"),
       @SerializedName("desktop")
       VALUE_DESKTOP("desktop"),
+      @SerializedName("connected_tv")
+      VALUE_CONNECTED_TV("connected_tv"),
       NULL(null);
 
       private String value;
@@ -1267,6 +1335,8 @@ public class Targeting extends APINode {
       VALUE_MOBILE("mobile"),
       @SerializedName("desktop")
       VALUE_DESKTOP("desktop"),
+      @SerializedName("connected_tv")
+      VALUE_CONNECTED_TV("connected_tv"),
       NULL(null);
 
       private String value;
@@ -1301,6 +1371,7 @@ public class Targeting extends APINode {
     this.mAgeMin = instance.mAgeMin;
     this.mAlternateAutoTargetingOption = instance.mAlternateAutoTargetingOption;
     this.mAppInstallState = instance.mAppInstallState;
+    this.mAudienceNetworkPositions = instance.mAudienceNetworkPositions;
     this.mBehaviors = instance.mBehaviors;
     this.mCities = instance.mCities;
     this.mCollegeYears = instance.mCollegeYears;
@@ -1314,8 +1385,11 @@ public class Targeting extends APINode {
     this.mEducationMajors = instance.mEducationMajors;
     this.mEducationSchools = instance.mEducationSchools;
     this.mEducationStatuses = instance.mEducationStatuses;
+    this.mEffectiveAudienceNetworkPositions = instance.mEffectiveAudienceNetworkPositions;
     this.mEffectiveDevicePlatforms = instance.mEffectiveDevicePlatforms;
     this.mEffectiveFacebookPositions = instance.mEffectiveFacebookPositions;
+    this.mEffectiveInstagramPositions = instance.mEffectiveInstagramPositions;
+    this.mEffectiveMessengerPositions = instance.mEffectiveMessengerPositions;
     this.mEffectivePublisherPlatforms = instance.mEffectivePublisherPlatforms;
     this.mEngagementSpecs = instance.mEngagementSpecs;
     this.mEthnicAffinity = instance.mEthnicAffinity;
@@ -1346,11 +1420,13 @@ public class Targeting extends APINode {
     this.mHouseholdComposition = instance.mHouseholdComposition;
     this.mIncome = instance.mIncome;
     this.mIndustries = instance.mIndustries;
+    this.mInstagramPositions = instance.mInstagramPositions;
     this.mInterestedIn = instance.mInterestedIn;
     this.mInterests = instance.mInterests;
     this.mKeywords = instance.mKeywords;
     this.mLifeEvents = instance.mLifeEvents;
     this.mLocales = instance.mLocales;
+    this.mMessengerPositions = instance.mMessengerPositions;
     this.mMoms = instance.mMoms;
     this.mNetWorth = instance.mNetWorth;
     this.mOfficeType = instance.mOfficeType;
