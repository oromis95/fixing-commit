@@ -305,6 +305,7 @@ public class ProductSet extends APINode {
 
     public static final String[] FIELDS = {
       "id",
+      "product_catalog",
       "retailer_id",
       "variants",
     };
@@ -385,6 +386,13 @@ public class ProductSet extends APINode {
       this.requestField("id", value);
       return this;
     }
+    public APIRequestGetProductGroups requestProductCatalogField () {
+      return this.requestProductCatalogField(true);
+    }
+    public APIRequestGetProductGroups requestProductCatalogField (boolean value) {
+      this.requestField("product_catalog", value);
+      return this;
+    }
     public APIRequestGetProductGroups requestRetailerIdField () {
       return this.requestRetailerIdField(true);
     }
@@ -410,6 +418,7 @@ public class ProductSet extends APINode {
     }
     public static final String[] PARAMS = {
       "bulk_pagination",
+      "filter",
     };
 
     public static final String[] FIELDS = {
@@ -441,7 +450,9 @@ public class ProductSet extends APINode {
       "ordering_index",
       "pattern",
       "price",
+      "product_catalog",
       "product_feed",
+      "product_group",
       "product_type",
       "retailer_id",
       "retailer_product_group_id",
@@ -501,6 +512,15 @@ public class ProductSet extends APINode {
       return this;
     }
 
+    public APIRequestGetProducts setFilter (Object filter) {
+      this.setParam("filter", filter);
+      return this;
+    }
+    public APIRequestGetProducts setFilter (String filter) {
+      this.setParam("filter", filter);
+      return this;
+    }
+
     public APIRequestGetProducts requestAllFields () {
       return this.requestAllFields(true);
     }
@@ -733,6 +753,13 @@ public class ProductSet extends APINode {
       this.requestField("price", value);
       return this;
     }
+    public APIRequestGetProducts requestProductCatalogField () {
+      return this.requestProductCatalogField(true);
+    }
+    public APIRequestGetProducts requestProductCatalogField (boolean value) {
+      this.requestField("product_catalog", value);
+      return this;
+    }
     public APIRequestGetProducts requestProductFeedField () {
       return this.requestProductFeedField(true);
     }
@@ -740,6 +767,13 @@ public class ProductSet extends APINode {
       this.requestField("product_feed", value);
       return this;
     }
+    public APIRequestGetProducts requestProductGroupField () {
+      return this.requestProductGroupField(true);
+    }
+    public APIRequestGetProducts requestProductGroupField (boolean value) {
+      this.requestField("product_group", value);
+      return this;
+    }
     public APIRequestGetProducts requestProductTypeField () {
       return this.requestProductTypeField(true);
     }
