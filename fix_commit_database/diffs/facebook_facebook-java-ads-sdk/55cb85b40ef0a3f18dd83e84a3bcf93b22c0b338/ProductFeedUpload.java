@@ -284,6 +284,7 @@ public class ProductFeedUpload extends APINode {
 
     public static final String[] FIELDS = {
       "description",
+      "error_type",
       "id",
       "severity",
       "summary",
@@ -366,6 +367,13 @@ public class ProductFeedUpload extends APINode {
       this.requestField("description", value);
       return this;
     }
+    public APIRequestGetErrors requestErrorTypeField () {
+      return this.requestErrorTypeField(true);
+    }
+    public APIRequestGetErrors requestErrorTypeField (boolean value) {
+      this.requestField("error_type", value);
+      return this;
+    }
     public APIRequestGetErrors requestIdField () {
       return this.requestIdField(true);
     }
