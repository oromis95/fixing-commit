@@ -51,6 +51,10 @@ import com.facebook.ads.sdk.APIException.MalformedResponseException;
  *
  */
 public class AdCreative extends APINode {
+  @SerializedName("account_id")
+  private String mAccountId = null;
+  @SerializedName("actor_id")
+  private String mActorId = null;
   @SerializedName("adlabels")
   private List<AdLabel> mAdlabels = null;
   @SerializedName("applink_treatment")
@@ -101,6 +105,8 @@ public class AdCreative extends APINode {
   private EnumRunStatus mRunStatus = null;
   @SerializedName("template_url")
   private String mTemplateUrl = null;
+  @SerializedName("template_url_spec")
+  private Object mTemplateUrlSpec = null;
   @SerializedName("thumbnail_url")
   private String mThumbnailUrl = null;
   @SerializedName("title")
@@ -109,6 +115,8 @@ public class AdCreative extends APINode {
   private String mUrlTags = null;
   @SerializedName("use_page_actor_override")
   private Boolean mUsePageActorOverride = null;
+  @SerializedName("video_id")
+  private String mVideoId = null;
   @SerializedName("creative_id")
   private String mCreativeId = null;
   protected static Gson gson = null;
@@ -319,6 +327,24 @@ public class AdCreative extends APINode {
   }
 
 
+  public String getFieldAccountId() {
+    return mAccountId;
+  }
+
+  public AdCreative setFieldAccountId(String value) {
+    this.mAccountId = value;
+    return this;
+  }
+
+  public String getFieldActorId() {
+    return mActorId;
+  }
+
+  public AdCreative setFieldActorId(String value) {
+    this.mActorId = value;
+    return this;
+  }
+
   public List<AdLabel> getFieldAdlabels() {
     return mAdlabels;
   }
@@ -560,6 +586,15 @@ public class AdCreative extends APINode {
     return this;
   }
 
+  public Object getFieldTemplateUrlSpec() {
+    return mTemplateUrlSpec;
+  }
+
+  public AdCreative setFieldTemplateUrlSpec(Object value) {
+    this.mTemplateUrlSpec = value;
+    return this;
+  }
+
   public String getFieldThumbnailUrl() {
     return mThumbnailUrl;
   }
@@ -596,6 +631,15 @@ public class AdCreative extends APINode {
     return this;
   }
 
+  public String getFieldVideoId() {
+    return mVideoId;
+  }
+
+  public AdCreative setFieldVideoId(String value) {
+    this.mVideoId = value;
+    return this;
+  }
+
 
 
   public static class APIRequestDeleteAdLabels extends APIRequest<APINode> {
@@ -795,13 +839,13 @@ public class AdCreative extends APINode {
     }
     public static final String[] PARAMS = {
       "ad_format",
-      "dynamic_creative_spec",
+      "end_date",
       "height",
-      "interactive",
       "locale",
       "place_page_id",
       "post",
       "product_item_ids",
+      "start_date",
       "width",
     };
 
@@ -851,12 +895,8 @@ public class AdCreative extends APINode {
       return this;
     }
 
-    public APIRequestGetPreviews setDynamicCreativeSpec (Object dynamicCreativeSpec) {
-      this.setParam("dynamic_creative_spec", dynamicCreativeSpec);
-      return this;
-    }
-    public APIRequestGetPreviews setDynamicCreativeSpec (String dynamicCreativeSpec) {
-      this.setParam("dynamic_creative_spec", dynamicCreativeSpec);
+    public APIRequestGetPreviews setEndDate (String endDate) {
+      this.setParam("end_date", endDate);
       return this;
     }
 
@@ -869,15 +909,6 @@ public class AdCreative extends APINode {
       return this;
     }
 
-    public APIRequestGetPreviews setInteractive (Boolean interactive) {
-      this.setParam("interactive", interactive);
-      return this;
-    }
-    public APIRequestGetPreviews setInteractive (String interactive) {
-      this.setParam("interactive", interactive);
-      return this;
-    }
-
     public APIRequestGetPreviews setLocale (String locale) {
       this.setParam("locale", locale);
       return this;
@@ -910,6 +941,11 @@ public class AdCreative extends APINode {
       return this;
     }
 
+    public APIRequestGetPreviews setStartDate (String startDate) {
+      this.setParam("start_date", startDate);
+      return this;
+    }
+
     public APIRequestGetPreviews setWidth (Long width) {
       this.setParam("width", width);
       return this;
@@ -1093,6 +1129,8 @@ public class AdCreative extends APINode {
     };
 
     public static final String[] FIELDS = {
+      "account_id",
+      "actor_id",
       "adlabels",
       "applink_treatment",
       "body",
@@ -1118,10 +1156,12 @@ public class AdCreative extends APINode {
       "product_set_id",
       "run_status",
       "template_url",
+      "template_url_spec",
       "thumbnail_url",
       "title",
       "url_tags",
       "use_page_actor_override",
+      "video_id",
     };
 
     @Override
@@ -1211,6 +1251,20 @@ public class AdCreative extends APINode {
       return this;
     }
 
+    public APIRequestGet requestAccountIdField () {
+      return this.requestAccountIdField(true);
+    }
+    public APIRequestGet requestAccountIdField (boolean value) {
+      this.requestField("account_id", value);
+      return this;
+    }
+    public APIRequestGet requestActorIdField () {
+      return this.requestActorIdField(true);
+    }
+    public APIRequestGet requestActorIdField (boolean value) {
+      this.requestField("actor_id", value);
+      return this;
+    }
     public APIRequestGet requestAdlabelsField () {
       return this.requestAdlabelsField(true);
     }
@@ -1386,6 +1440,13 @@ public class AdCreative extends APINode {
       this.requestField("template_url", value);
       return this;
     }
+    public APIRequestGet requestTemplateUrlSpecField () {
+      return this.requestTemplateUrlSpecField(true);
+    }
+    public APIRequestGet requestTemplateUrlSpecField (boolean value) {
+      this.requestField("template_url_spec", value);
+      return this;
+    }
     public APIRequestGet requestThumbnailUrlField () {
       return this.requestThumbnailUrlField(true);
     }
@@ -1414,6 +1475,13 @@ public class AdCreative extends APINode {
       this.requestField("use_page_actor_override", value);
       return this;
     }
+    public APIRequestGet requestVideoIdField () {
+      return this.requestVideoIdField(true);
+    }
+    public APIRequestGet requestVideoIdField (boolean value) {
+      this.requestField("video_id", value);
+      return this;
+    }
   }
 
   public static class APIRequestUpdate extends APIRequest<AdCreative> {
@@ -1586,6 +1654,8 @@ public class AdCreative extends APINode {
       VALUE_NO_BUTTON("NO_BUTTON"),
       @SerializedName("CALL_NOW")
       VALUE_CALL_NOW("CALL_NOW"),
+      @SerializedName("APPLY_NOW")
+      VALUE_APPLY_NOW("APPLY_NOW"),
       @SerializedName("BUY_NOW")
       VALUE_BUY_NOW("BUY_NOW"),
       @SerializedName("GET_OFFER")
@@ -1596,6 +1666,8 @@ public class AdCreative extends APINode {
       VALUE_GET_DIRECTIONS("GET_DIRECTIONS"),
       @SerializedName("MESSAGE_PAGE")
       VALUE_MESSAGE_PAGE("MESSAGE_PAGE"),
+      @SerializedName("MESSAGE_USER")
+      VALUE_MESSAGE_USER("MESSAGE_USER"),
       @SerializedName("SUBSCRIBE")
       VALUE_SUBSCRIBE("SUBSCRIBE"),
       @SerializedName("SELL_NOW")
@@ -1612,6 +1684,10 @@ public class AdCreative extends APINode {
       VALUE_VOTE_NOW("VOTE_NOW"),
       @SerializedName("REGISTER_NOW")
       VALUE_REGISTER_NOW("REGISTER_NOW"),
+      @SerializedName("REQUEST_TIME")
+      VALUE_REQUEST_TIME("REQUEST_TIME"),
+      @SerializedName("SEE_MENU")
+      VALUE_SEE_MENU("SEE_MENU"),
       @SerializedName("OPEN_MOVIES")
       VALUE_OPEN_MOVIES("OPEN_MOVIES"),
       NULL(null);
@@ -1737,6 +1813,8 @@ public class AdCreative extends APINode {
   }
 
   public AdCreative copyFrom(AdCreative instance) {
+    this.mAccountId = instance.mAccountId;
+    this.mActorId = instance.mActorId;
     this.mAdlabels = instance.mAdlabels;
     this.mApplinkTreatment = instance.mApplinkTreatment;
     this.mBody = instance.mBody;
@@ -1762,10 +1840,12 @@ public class AdCreative extends APINode {
     this.mProductSetId = instance.mProductSetId;
     this.mRunStatus = instance.mRunStatus;
     this.mTemplateUrl = instance.mTemplateUrl;
+    this.mTemplateUrlSpec = instance.mTemplateUrlSpec;
     this.mThumbnailUrl = instance.mThumbnailUrl;
     this.mTitle = instance.mTitle;
     this.mUrlTags = instance.mUrlTags;
     this.mUsePageActorOverride = instance.mUsePageActorOverride;
+    this.mVideoId = instance.mVideoId;
     this.mCreativeId = this.mId;
     this.context = instance.context;
     this.rawValue = instance.rawValue;
