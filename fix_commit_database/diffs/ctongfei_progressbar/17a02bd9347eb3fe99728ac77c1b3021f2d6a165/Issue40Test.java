@@ -2,6 +2,8 @@ package me.tongfei.progressbar;
 
 import org.junit.Test;
 
+import java.io.ByteArrayInputStream;
+import java.io.InputStream;
 import java.util.Scanner;
 
 /**
@@ -10,8 +12,10 @@ import java.util.Scanner;
 public class Issue40Test {
 
     @Test
-    public static void main(String[] args) throws InterruptedException {
-        Scanner sc = new Scanner(System.in);
+    public void test() throws InterruptedException {
+        InputStream input = new ByteArrayInputStream("100 200 300".getBytes());
+
+        Scanner sc = new Scanner(input);
         int x = sc.nextInt();
         try (ProgressBar pb = new ProgressBar("1", x)) {
             for (int i = 0; i < x; i++) {
