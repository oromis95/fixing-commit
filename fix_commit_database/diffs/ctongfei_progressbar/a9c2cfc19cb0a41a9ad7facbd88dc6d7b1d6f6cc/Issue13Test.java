@@ -12,32 +12,29 @@ public class Issue13Test {
 
 	@Test
 	public void testOk() {
-		ProgressBar pb = new ProgressBar("Test", NBR_ELEMENTS);
-		pb.start();
+		try (ProgressBar pb = new ProgressBar("Test", NBR_ELEMENTS)) {
 
-		try {
-			Thread.sleep(PROGRESSBAR_GRACE_PERIOD);
-		} catch (InterruptedException e1) {
-			e1.printStackTrace();
-		}
+			try {
+				Thread.sleep(PROGRESSBAR_GRACE_PERIOD);
+			} catch (InterruptedException e1) {
+				e1.printStackTrace();
+			}
 
-		for (int i = 0; i < 100; i++) {
-			pb.step();
+			for (int i = 0; i < 100; i++) {
+				pb.step();
+			}
 		}
-
-		pb.stop();
 	}
 
 	@Test
 	public void testKo() {
-		ProgressBar pb = new ProgressBar("Test", NBR_ELEMENTS);
-		pb.start();
+		try (ProgressBar pb = new ProgressBar("Test", NBR_ELEMENTS)) {
 
-		for (int i = 0; i < 100; i++) {
-			pb.step();
-		}
+			for (int i = 0; i < 100; i++) {
+				pb.step();
+			}
 
-		pb.stop();
+		}
 	}
 
 }
