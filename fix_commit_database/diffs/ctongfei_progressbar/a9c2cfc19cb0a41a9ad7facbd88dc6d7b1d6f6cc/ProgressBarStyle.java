@@ -7,6 +7,8 @@ package me.tongfei.progressbar;
  */
 public enum ProgressBarStyle {
 
+    COLORFUL_UNICODE_BLOCK("\u001b[33m│", "│\u001b[0m", '█', ' ', " ▏▎▍▌▋▊▉"),
+
     /** Use Unicode block characters to draw the progress bar. */
     UNICODE_BLOCK("│", "│", '█', ' ', " ▏▎▍▌▋▊▉"),
 
