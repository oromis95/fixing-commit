@@ -7,21 +7,23 @@ package me.tongfei.progressbar;
  */
 public enum ProgressBarStyle {
 
-    COLORFUL_UNICODE_BLOCK("\u001b[33m│", "│\u001b[0m", '█', ' ', " ▏▎▍▌▋▊▉"),
+    COLORFUL_UNICODE_BLOCK("\r", "\u001b[33m│", "│\u001b[0m", '█', ' ', " ▏▎▍▌▋▊▉"),
 
     /** Use Unicode block characters to draw the progress bar. */
-    UNICODE_BLOCK("│", "│", '█', ' ', " ▏▎▍▌▋▊▉"),
+    UNICODE_BLOCK("\r", "│", "│", '█', ' ', " ▏▎▍▌▋▊▉"),
 
     /** Use only ASCII characters to draw the progress bar. */
-    ASCII("[", "]", '=', ' ', ">");
+    ASCII("\r", "[", "]", '=', ' ', ">");
 
+    String refreshPrompt;
     String leftBracket;
     String rightBracket;
     char block;
     char space;
     String fractionSymbols;
 
-    ProgressBarStyle(String leftBracket, String rightBracket, char block, char space, String fractionSymbols) {
+    ProgressBarStyle(String refreshPrompt, String leftBracket, String rightBracket, char block, char space, String fractionSymbols) {
+        this.refreshPrompt = refreshPrompt;
         this.leftBracket = leftBracket;
         this.rightBracket = rightBracket;
         this.block = block;
