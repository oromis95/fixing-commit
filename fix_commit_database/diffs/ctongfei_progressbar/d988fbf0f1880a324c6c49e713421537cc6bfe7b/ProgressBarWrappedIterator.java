@@ -3,26 +3,29 @@ package me.tongfei.progressbar.wrapped;
 import me.tongfei.progressbar.ProgressBar;
 
 import java.util.Iterator;
-import java.util.function.Consumer;
 
 /**
  * @author Tongfei Chen
  * @since 0.6.0
  */
-public class ProgressBarWrappedIterator<T> implements Iterator<T> {
+public class ProgressBarWrappedIterator<T> implements Iterator<T>, AutoCloseable {
 
-    Iterator<T> underlying;
-    ProgressBar pb;
+    private Iterator<T> underlying;
+    private ProgressBar pb;
 
     public ProgressBarWrappedIterator(Iterator<T> underlying, ProgressBar pb) {
         this.underlying = underlying;
         this.pb = pb;
     }
 
+    public ProgressBar getProgressBar() {
+        return pb;
+    }
+
     @Override
     public boolean hasNext() {
         boolean r = underlying.hasNext();
-        if (!r) pb.stop();
+        if (!r) pb.close();
         return r;
     }
 
@@ -38,4 +41,8 @@ public class ProgressBarWrappedIterator<T> implements Iterator<T> {
         underlying.remove();
     }
 
+    @Override
+    public void close() {
+        pb.close();
+    }
 }
