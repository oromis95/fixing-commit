@@ -477,12 +477,12 @@ public final class Preferences {
 							diskStat = root.filesystem(loopContainer[i]).statDisk();
 						}
 					
-						if (diskStat != null && diskStat.device().equals(deviceSetup.path_device_map_data())) {
+						if (diskStat != null && diskStat.device() != null && diskStat.device().equals(deviceSetup.path_device_map_data())) {
 							location_storage_data(loopContainer[i]);
 							size_storage_data(diskStat.size());
 							usage_storage_data(diskStat.usage());
 							
-						} else if (diskStat != null && diskStat.device().equals(deviceSetup.path_device_map_sdext())) {
+						} else if (diskStat != null && diskStat.device() != null && diskStat.device().equals(deviceSetup.path_device_map_sdext())) {
 							location_storage_sdext(loopContainer[i]);
 							size_storage_sdext(diskStat.size());
 							usage_storage_sdext(diskStat.usage());
@@ -500,7 +500,7 @@ public final class Preferences {
 						}
 						
 						if (additLocationDevice == null) {
-							additLocationDevice = diskStat == null || diskStat.device().equals(deviceSetup.path_device_map_data()) ? deviceSetup.path_device_map_sdext() : deviceSetup.path_device_map_data(); continue;
+							additLocationDevice = diskStat == null || (diskStat.device() != null && diskStat.device().equals(deviceSetup.path_device_map_data())) ? deviceSetup.path_device_map_sdext() : deviceSetup.path_device_map_data(); continue;
 						}
 							
 						break;
@@ -508,8 +508,8 @@ public final class Preferences {
 
 					if ((diskStat = root.filesystem("/cache").statDisk()) != null) {
 						location_storage_cache(
-							diskStat.device().equals(deviceSetup.path_device_map_sdext()) ? location_storage_sdext() + "/cache" : 
-								diskStat.device().equals(deviceSetup.path_device_map_data()) ? location_storage_data() + "/cache" : "/cache"
+								diskStat.device() != null && diskStat.device().equals(deviceSetup.path_device_map_sdext()) ? location_storage_sdext() + "/cache" : 
+								diskStat.device() != null && diskStat.device().equals(deviceSetup.path_device_map_data()) ? location_storage_data() + "/cache" : "/cache"
 						);
 						
 						size_storage_cache(diskStat.size());
@@ -551,7 +551,7 @@ public final class Preferences {
 							if (sdextMounted && curState >= 0) {
 								DiskStat curStat = root.filesystem("/data/" + curFolders[x]).statDisk();
 								
-								curState = curStat == null || curStat.device().equals(deviceSetup.path_device_map_data()) ? 
+								curState = curStat == null || curStat.device() == null || curStat.device().equals(deviceSetup.path_device_map_data()) ? 
 										(x == 0 || curState == 0 ? 0 : -1) : 
 											(x == 0 || curState == 1 ? 1 : -1);
 							}
@@ -653,15 +653,17 @@ public final class Preferences {
 						
 						if (swapList != null) {
 							for (int i=0; i < swapList.length; i++) {
-								if (swapList[i].device().equals(deviceSetup.path_device_map_swap())) {
-									size_memory_swap(swapList[i].size());
-									usage_memory_swap(swapList[i].usage());
-									
-								} else if (deviceSetup.support_option_zram() 
-										&& swapList[i].device().equals(deviceSetup.path_device_map_zram())) {
-									
-									size_memory_zram(swapList[i].size());
-									usage_memory_zram(swapList[i].usage());
+								if (swapList[i].device() != null) {
+									if (swapList[i].device().equals(deviceSetup.path_device_map_swap())) {
+										size_memory_swap(swapList[i].size());
+										usage_memory_swap(swapList[i].usage());
+										
+									} else if (deviceSetup.support_option_zram() 
+											&& swapList[i].device().equals(deviceSetup.path_device_map_zram())) {
+										
+										size_memory_zram(swapList[i].size());
+										usage_memory_zram(swapList[i].usage());
+									}
 								}
 							}
 						}
