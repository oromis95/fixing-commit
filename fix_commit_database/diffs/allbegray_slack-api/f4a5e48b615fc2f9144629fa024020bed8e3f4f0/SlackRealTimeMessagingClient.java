@@ -9,7 +9,7 @@ import org.apache.commons.logging.LogFactory;
 import org.asynchttpclient.AsyncHttpClient;
 import org.asynchttpclient.BoundRequestBuilder;
 import org.asynchttpclient.proxy.ProxyServer;
-import org.asynchttpclient.ws.WebSocketListener;
+import org.asynchttpclient.ws.DefaultWebSocketListener;
 import org.asynchttpclient.ws.WebSocket;
 import org.asynchttpclient.ws.WebSocketUpgradeHandler;
 
@@ -99,7 +99,11 @@ public class SlackRealTimeMessagingClient {
 
 		stop = true;
 		if (webSocket != null && webSocket.isOpen()) {
-			webSocket.sendCloseFrame();
+			try {
+				webSocket.close();
+			} catch (IOException e) {
+				// ignore
+			}
 		}
 		if (asyncHttpClient != null && !asyncHttpClient.isClosed()) {
 			try {
@@ -124,10 +128,10 @@ public class SlackRealTimeMessagingClient {
 				asyncHttpClient = asyncHttpClient();
 			}
 			BoundRequestBuilder requestBuilder = asyncHttpClient.prepareGet(webSocketUrl);
-			webSocket = requestBuilder.execute(new WebSocketUpgradeHandler.Builder().addWebSocketListener(new WebSocketListener() {
+			webSocket = requestBuilder.execute(new WebSocketUpgradeHandler.Builder().addWebSocketListener(new DefaultWebSocketListener() {
 
 				@Override
-				public void onTextFrame(String message, boolean finalFragment, int rsv) {
+				public void onMessage(String message) {
 					String type = null;
 					JsonNode node = null;
 					try {
@@ -152,7 +156,8 @@ public class SlackRealTimeMessagingClient {
 				}
 
 				@Override
-				public void onClose(WebSocket websocket, int code, String reason) {
+				public void onClose(WebSocket websocket) {
+					super.onClose(websocket);
 					stop = true;
 					if (closeListeners != null && !closeListeners.isEmpty()) {
 						for (CloseListener listener : closeListeners) {
@@ -172,10 +177,6 @@ public class SlackRealTimeMessagingClient {
 					}
 				}
 
-				@Override
-				public void onOpen(WebSocket arg0) {
-				}
-
 			}).build()).get();
 
 			logger.info("connected Slack RTM(Real Time Messaging) server : " + webSocketUrl);
@@ -196,7 +197,7 @@ public class SlackRealTimeMessagingClient {
 		pingMessage.put("id", ++socketId);
 		pingMessage.put("type", "ping");
 		String pingJson = pingMessage.toString();
-		webSocket.sendTextFrame(pingJson);
+		webSocket.sendMessage(pingJson);
 
 		logger.debug("ping : " + pingJson);
 	}
@@ -225,7 +226,7 @@ public class SlackRealTimeMessagingClient {
 	 * @param message
 	 */
 	public void sendMessage(String message){
-		webSocket.sendTextFrame(message);
+		webSocket.sendMessage(message);
 	}
 
 	/**
