@@ -1,20 +1,21 @@
 /**
- * Licensed under the Apache License, Version 2.0 (the "License");
- * you may not use this file except in compliance with the License.
- * You may obtain a copy of the License at
+ * Licensed under the Apache License, Version 2.0 (the "License"); you may not
+ * use this file except in compliance with the License. You may obtain a copy of
+ * the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
- * distributed under the License is distributed on an "AS IS" BASIS,
- * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
- * See the License for the specific language governing permissions and
- * limitations under the License.
+ * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
+ * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
+ * License for the specific language governing permissions and limitations under
+ * the License.
  */
 package journal.io;
 
 import java.io.File;
 import journal.io.api.Journal;
+import journal.io.api.JournalBuilder;
 import journal.io.api.Location;
 import org.junit.Before;
 import org.junit.Test;
@@ -33,19 +34,16 @@ public class ApiTest {
         JOURNAL_DIR.delete();
         JOURNAL_DIR.mkdir();
     }
-    
+
     @Test
     public void api() throws Exception {
         // Create journal and configure some settings:
-        Journal journal = new Journal();
-        journal.setDirectory(JOURNAL_DIR);
-        journal.setArchiveFiles(false);
-        journal.setChecksum(true);
-        journal.setMaxFileLength(1024 * 1024);
-        journal.setMaxWriteBatchSize(1024 * 10);
-
+        JournalBuilder builder = JournalBuilder.of(JOURNAL_DIR)
+                .setChecksum(true)
+                .setMaxFileLength(1024 * 1024)
+                .setMaxWriteBatchSize(1024 * 10);
         // Open the journal:
-        journal.open();
+        Journal journal = builder.open();
 
         // Write to the journal:
         int iterations = 1000;
@@ -67,12 +65,12 @@ public class ApiTest {
             byte[] record = journal.read(location, Journal.ReadType.ASYNC);
             assertEquals("DATA" + --i, new String(record, "UTF-8"));
         }
-        
+
         // Delete locations:
         for (Location location : journal.redo()) {
             journal.delete(location);
         }
-        
+
         // Compact logs:
         journal.compact();
 
