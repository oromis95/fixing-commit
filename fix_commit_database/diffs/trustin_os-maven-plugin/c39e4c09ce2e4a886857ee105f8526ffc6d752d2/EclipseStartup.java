@@ -7,6 +7,7 @@ import org.eclipse.core.runtime.Status;
 import org.eclipse.ui.IStartup;
 import org.osgi.framework.Bundle;
 
+import java.util.Collections;
 import java.util.Properties;
 
 public class EclipseStartup extends Detector implements IStartup {
@@ -19,7 +20,7 @@ public class EclipseStartup extends Detector implements IStartup {
     public void earlyStartup() {
         Bundle bundle = Platform.getBundle(ID);
         logger = Platform.getLog(bundle);
-        detect(new Properties());
+        detect(new Properties(), Collections.<String>emptyList());
     }
 
     @Override
