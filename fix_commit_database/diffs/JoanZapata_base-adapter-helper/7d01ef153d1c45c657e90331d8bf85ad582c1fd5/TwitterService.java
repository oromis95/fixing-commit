@@ -1,8 +1,9 @@
-package com.joanzapata.android.sample;
+package com.joanzapata.android.tweeter;
 
 import android.util.Log;
 import twitter4j.*;
 
+import java.util.ArrayList;
 import java.util.List;
 
 public class TwitterService {
@@ -13,13 +14,15 @@ public class TwitterService {
 
     private static final String TAG = TwitterService.class.getSimpleName();
 
+    private static final int TWEET_COUNT = 40;
+
     public static List<Status> getTweetsBefore(String username, Status beforeTweet) {
         try {
-            final Paging paging = new Paging().count(30);
-            if (beforeTweet != null) {
-                paging.maxId(beforeTweet.getId() - 1);
-            }
-            return twitter.getUserTimeline(username, paging);
+            final Paging paging = new Paging().count(TWEET_COUNT);
+            if (beforeTweet != null) paging.maxId(beforeTweet.getId() - 1);
+            final ResponseList<Status> userTimeline = twitter.getUserTimeline(username, paging);
+            if (userTimeline == null) return new ArrayList<Status>();
+            return userTimeline;
         } catch (TwitterException e) {
             Log.e(TAG, "", e);
             return null;
