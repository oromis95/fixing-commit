@@ -151,7 +151,7 @@ public abstract class BaseQuickAdapter<T, H extends BaseAdapterHelper> extends B
 
     public void replaceAll(List<T> elem) {
         data.clear();
-		data.addAll(elem);
+        data.addAll(elem);
         notifyDataSetChanged();
     }
 
