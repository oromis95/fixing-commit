@@ -142,7 +142,11 @@ public class BitmapCache {
 
         synchronized (mDiskCacheLock) {
             // 添加到硬盘缓存
-            if (mDiskLruCache != null) {
+            if (mDiskLruCache != null && mDiskLruCache.getDirectory()!= null ) {
+            	
+            	if(!mDiskLruCache.getDirectory().exists())
+            		mDiskLruCache.getDirectory().mkdirs();
+            	
                 final String key = FileNameGenerator.generator(data);
                 OutputStream out = null;
                 try {
