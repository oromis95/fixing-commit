@@ -233,10 +233,7 @@ public class BitmapCache {
      * this includes disk access so this should not be executed on the main/UI thread.
      */
     public void clearCache() {
-        if (mMemoryCache != null) {
-            mMemoryCache.evictAll();
-        }
-
+    	clearMemoryCache();
         synchronized (mDiskCacheLock) {
             mDiskCacheStarting = true;
             if (mDiskLruCache != null && !mDiskLruCache.isClosed()) {
@@ -250,6 +247,12 @@ public class BitmapCache {
             }
         }
     }
+    
+    public void clearMemoryCache(){
+    	if (mMemoryCache != null) {
+            mMemoryCache.evictAll();
+        }
+    }
 
     /**
      * Flushes the disk cache associated with this ImageCache object. Note that this includes
