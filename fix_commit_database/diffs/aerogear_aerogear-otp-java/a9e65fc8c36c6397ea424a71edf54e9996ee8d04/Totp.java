@@ -5,22 +5,15 @@ import java.security.NoSuchAlgorithmException;
 
 public class Totp {
 
-    private Clock clock;
-
     private String secret;
     private Base32 base32 = new Base32();
-    private static final int DELAY_WINDOW = 10;
-    private static final int WINDOW = 1;
+    private Clock clock = new Clock();
+    private static final int DELAY_WINDOW = 1;
 
     public Totp(String secret) {
         this.secret = secret;
     }
 
-    public Totp(String secret, Clock clock) {
-        this.clock = clock;
-        this.secret = secret;
-    }
-
     //TODO URI.encode
     public String uri(String name) {
         return String.format("otpauth://totp/%s?secret=%s", name, secret);
@@ -30,7 +23,7 @@ public class Totp {
 
         byte[] hash = new byte[0];
         try {
-            hash = new Hmac(Hash.SHA1, base32.decode(secret), new Clock().getCurrentInterval()).digest();
+            hash = new Hmac(Hash.SHA1, base32.decode(secret), clock.getCurrentInterval()).digest();
         } catch (NoSuchAlgorithmException e) {
             e.printStackTrace();
         } catch (InvalidKeyException e) {
@@ -48,7 +41,6 @@ public class Totp {
     public int generate(String secret, long interval) {
         byte[] hash = new byte[0];
         try {
-            System.out.println("From prover: " + new Clock().getCurrentInterval());
             hash = new Hmac(Hash.SHA1, base32.decode(secret), interval).digest();
         } catch (NoSuchAlgorithmException e) {
             e.printStackTrace();
@@ -67,24 +59,24 @@ public class Totp {
     public boolean verify(long code) {
 
 
-        long currentInterval = new Clock().getCurrentInterval();
-//        System.out.println("From verifier: " + clock.getCurrentInterval());
+        long currentInterval = clock.getCurrentInterval();
         int expectedResponse = generate();
         if (expectedResponse == code) {
             return true;
         }
-        for (int i = 1; i <= WINDOW; i++) {
+        for (int i = 1; i <= DELAY_WINDOW; i++) {
             int pastResponse = generate(secret, currentInterval - i);
             if (pastResponse == code) {
                 return true;
             }
         }
-        for (int i = 1; i <= WINDOW; i++) {
+        for (int i = 1; i <= DELAY_WINDOW; i++) {
             int futureResponse = generate(secret, currentInterval + i);
-            if (futureResponse ==  code) {
+            if (futureResponse == code) {
                 return true;
             }
         }
         return false;
     }
+
 }
