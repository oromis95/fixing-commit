@@ -84,6 +84,13 @@ public class TotpTest {
         assertEquals("Generated token must be zero padded", otp, expected);
     }
 
+    @Test
+    public void testCustomInterval() throws Exception {
+        Clock customClock = new Clock(20);
+        totp = new Totp(sharedSecret, customClock);
+        totp.now();
+    }
+
     @Test
     public void testNow() throws Exception {
         String otp = totp.now();
