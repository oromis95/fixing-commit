@@ -11,7 +11,7 @@ import java.util.concurrent.LinkedBlockingDeque;
 
 /**
  *
- * Based on the Aho-Corasick white paper, Bell technologies: ftp://163.13.200.222/assistant/bearhero/prog/%A8%E4%A5%A6/ac_bm.pdf
+ * Based on the Aho-Corasick white paper, Bell technologies: ftp://im1.im.tku.edu.tw/assistant/bearhero/prog/%E5%85%B6%E5%AE%83/ac_bm.pdf
  * @author Robert Bor
  */
 public class Trie {
