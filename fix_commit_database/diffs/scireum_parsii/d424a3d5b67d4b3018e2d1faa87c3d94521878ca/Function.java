@@ -12,17 +12,16 @@ import java.util.List;
 
 /**
  * Defines a function which can be referenced and evaluated from within expressions.
- *
- * @author Andreas Haufler (aha@scireum.de)
- * @since 2013/09
  */
 public interface Function {
 
     /**
      * Returns the number of expected arguments.
-     * <p>If the function is called with a different number of arguments, an error will be created</p>
-     * <p>In order to support functions with a variable number of arguments, a negative number can be returned.
-     * This will essentially disable the check.</p>
+     * <p>
+     * If the function is called with a different number of arguments, an error will be created
+     * <p>
+     * In order to support functions with a variable number of arguments, a negative number can be returned.
+     * This will essentially disable the check.
      *
      * @return the number of arguments expected by this function or a negative number to indicate that this
      * function accepts a variable number of arguments
@@ -34,7 +33,6 @@ public interface Function {
      * <p>
      * The arguments need to be evaluated first. This is not done externally to permit functions to perform lazy
      * evaluations.
-     * </p>
      *
      * @param args the arguments for this function. The length of the given list will exactly match
      *             <tt>getNumberOfArguments</tt>
@@ -47,7 +45,6 @@ public interface Function {
      * <p>
      * All classical mathematical functions are "natural". A function which reads user input is not natural, as
      * the function might return different results depending on the users input
-     * </p>
      *
      * @return <tt>true</tt> if the function returns the same output for the same input, <tt>false</tt> otherwise
      */
