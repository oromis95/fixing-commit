@@ -16,6 +16,8 @@ package parsii.eval;
  */
 public class BinaryOperation extends Expression {
 
+    private static final long serialVersionUID = -3811171614568221526L;
+
     /**
      * Enumerates the operations supported by this expression.
      */
