@@ -141,10 +141,13 @@ public abstract class Lookahead<T> {
         if (numberOfItems < 0) {
             throw new IllegalArgumentException("numberOfItems < 0");
         }
-        while (numberOfItems-- > 0 && !endReached) {
+        while (numberOfItems-- > 0) {
             if (!itemBuffer.isEmpty()) {
                 itemBuffer.remove(0);
             } else {
+                if (endReached) {
+                    return;
+                }
                 T item = fetch();
                 if (item == null) {
                     endReached = true;
