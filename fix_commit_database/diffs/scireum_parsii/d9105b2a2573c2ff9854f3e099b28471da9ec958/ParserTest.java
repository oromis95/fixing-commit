@@ -61,6 +61,15 @@ public class ParserTest {
         assertEquals(28d, Parser.parse("(3 + 4) * 4").evaluate(), BinaryOperation.EPSILON);
     }
 
+    @Test
+    public void signed() throws ParseException {
+        assertEquals(-2.02, Parser.parse("-2.02").evaluate(), 0.0000001);
+        assertEquals(2.02, Parser.parse("+2.02").evaluate(), 0.0000001);
+        assertEquals(1.01, Parser.parse("+2.02 + -1.01").evaluate(), 0.0000001);
+        assertEquals(-4.03, Parser.parse("-2.02 - +2.01").evaluate(), 0.0000001);
+        assertEquals(3.03, Parser.parse("+2.02 + +1.01").evaluate(), 0.0000001);
+    }
+
     @Test
     public void variables() throws ParseException {
         Scope scope = Scope.create();
