@@ -8,16 +8,12 @@
 
 package parsii.eval;
 
-import java.io.Serializable;
-
 /**
  * Represents the result of a parsed expression.
  * <p>
  * Can be evaluated to return a double value. If an error occurs {@code Double.NaN} will be returned.
  */
-public abstract class Expression implements Serializable {
-
-    private static final long serialVersionUID = -6078443078086081582L;
+public abstract class Expression {
 
     /**
      * Evaluates the expression to a double number.
