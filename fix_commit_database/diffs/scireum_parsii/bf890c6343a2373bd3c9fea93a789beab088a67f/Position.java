@@ -11,20 +11,8 @@ package parsii.tokenizer;
 /**
  * Describes a position in a file or a stream based on lines and the character position within the line.
  */
+@SuppressWarnings("squid:S1214")
 public interface Position {
-    /**
-     * Returns the line number of this position.
-     *
-     * @return the one-based line number of this position
-     */
-    int getLine();
-
-    /**
-     * Returns the character position within the line of this position
-     *
-     * @return the one-based character position of this
-     */
-    int getPos();
 
     /**
      * Represents an unknown position for warnings and errors which cannot be associated with a defined position.
@@ -41,4 +29,19 @@ public interface Position {
             return 0;
         }
     };
+
+    /**
+     * Returns the line number of this position.
+     *
+     * @return the one-based line number of this position
+     */
+    int getLine();
+
+    /**
+     * Returns the character position within the line of this position
+     *
+     * @return the one-based character position of this
+     */
+    int getPos();
+
 }
