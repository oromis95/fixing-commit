@@ -10,7 +10,6 @@ import com.intellij.openapi.vfs.ReadonlyStatusHandler.OperationStatus;
 import com.intellij.psi.PsiElement;
 import com.intellij.psi.codeStyle.CodeStyleManager;
 import com.intellij.psi.javadoc.PsiDocComment;
-
 import org.jetbrains.annotations.NotNull;
 
 import java.util.Arrays;
@@ -92,7 +91,7 @@ public class JavaDocWriterImpl implements JavaDocWriter, ApplicationComponent {
             if (element.getFirstChild() instanceof PsiDocComment) {
                 element.getFirstChild().replace(javaDoc);
             } else {
-                element.addBefore(javaDoc, element.getFirstChild());
+                element.getNode().addChild(javaDoc.getNode(), element.getFirstChild().getNode());
             }
             CodeStyleManager codeStyleManager = CodeStyleManager.getInstance(element.getProject());
             codeStyleManager.reformatNewlyAddedElement(element.getNode(), element.getFirstChild().getNode());
