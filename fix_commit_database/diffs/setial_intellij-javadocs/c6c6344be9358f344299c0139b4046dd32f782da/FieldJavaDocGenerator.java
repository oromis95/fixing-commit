@@ -5,6 +5,8 @@ import com.github.setial.intellijjavadocs.model.settings.JavaDocSettings;
 import com.github.setial.intellijjavadocs.model.settings.Level;
 import com.github.setial.intellijjavadocs.utils.JavaDocUtils;
 import com.intellij.openapi.project.Project;
+import com.intellij.psi.PsiClass;
+import com.intellij.psi.PsiElement;
 import com.intellij.psi.PsiEnumConstant;
 import com.intellij.psi.PsiField;
 import freemarker.template.Template;
@@ -39,11 +41,25 @@ public class FieldJavaDocGenerator extends AbstractJavaDocGenerator<PsiField> {
         }
         Template template = getDocTemplateManager().getFieldTemplate(element);
         Map<String, Object> params = getDefaultParameters(element);
-        if (PsiEnumConstant.class.isAssignableFrom(element.getClass())) {
-            params.put("name", element.getName());
+        PsiClass parent = findClassElement(element);
+        if (parent != null) {
+            params.put("typeName", getDocTemplateProcessor().buildDescription(parent.getName(), false));
         }
         String javaDocText = getDocTemplateProcessor().merge(template, params);
         return JavaDocUtils.toJavaDoc(javaDocText, getPsiElementFactory());
     }
 
+    private PsiClass findClassElement(PsiElement element) {
+        PsiClass parentClass = null;
+        if (element != null) {
+            PsiElement parent = element.getParent();
+            if (parent != null && parent instanceof PsiClass) {
+                parentClass = (PsiClass) parent;
+            } else {
+                parentClass = findClassElement(parent);
+            }
+        }
+        return parentClass;
+    }
+
 }
