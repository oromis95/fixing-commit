@@ -1,16 +1,18 @@
 package com.github.setial.intellijjavadocs.template.impl;
 
+import com.github.setial.intellijjavadocs.exception.SetupTemplateException;
+import com.github.setial.intellijjavadocs.exception.TemplateNotFoundException;
 import com.github.setial.intellijjavadocs.template.DocTemplateProcessor;
 import com.github.setial.intellijjavadocs.utils.XmlUtils;
 import freemarker.template.Template;
-import freemarker.template.TemplateException;
 import org.apache.commons.lang3.StringUtils;
 import org.jetbrains.annotations.NotNull;
 import org.jetbrains.annotations.Nullable;
 
-import java.io.IOException;
 import java.io.StringWriter;
+import java.util.ArrayList;
 import java.util.Arrays;
+import java.util.List;
 import java.util.Map;
 
 /**
@@ -20,7 +22,7 @@ import java.util.Map;
  */
 public class DocTemplateProcessorImpl implements DocTemplateProcessor {
 
-    // TODO move the logic to utils classes
+    private static final List<String> SPECIAL_SYMBOLS = Arrays.asList("_", "-");
 
     @Override
     public void projectOpened() {
@@ -48,20 +50,15 @@ public class DocTemplateProcessorImpl implements DocTemplateProcessor {
     @Override
     public String merge(@Nullable Template template, @NotNull Map<String, Object> params) {
         if (template == null) {
-            // TODO throw exception and catch it at top level of app
-            return StringUtils.EMPTY;
+            throw new TemplateNotFoundException();
         }
 
         StringWriter writer = new StringWriter();
         try {
             template.process(params, writer);
             return XmlUtils.normalizeTemplate(writer.toString());
-        } catch (IOException e) {
-            // TODO throw runtime exception and catch it at top level app
-            throw new RuntimeException(e);
-        } catch (TemplateException e) {
-            // TODO throw runtime exception and catch it at top level app
-            throw new RuntimeException(e);
+        } catch (Exception e) {
+            throw new SetupTemplateException(e);
         }
     }
 
@@ -83,10 +80,30 @@ public class DocTemplateProcessorImpl implements DocTemplateProcessor {
         return buildDescription(description, 1, false);
     }
 
+    @NotNull
+    @Override
+    public String buildFieldDescription(@NotNull String description) {
+        if (StringUtils.isBlank(description)) {
+            return StringUtils.EMPTY;
+        }
+        String[] parts = StringUtils.splitByCharacterTypeCamelCase(description.replaceAll("<.+>", ""));
+        StringBuilder result = new StringBuilder();
+        for (int i = 1; i < parts.length; i++) {
+            if (i > 1) {
+                result.append(StringUtils.capitalize(parts[i]));
+            } else {
+                result.append(StringUtils.uncapitalize(parts[i]));
+            }
+        }
+        return result.toString();
+    }
+
     private String buildDescription(String description, int firstElement, boolean capitalizeFirst) {
         String[] parts = StringUtils.splitByCharacterTypeCamelCase(description.replaceAll("<.+>", ""));
         parts = removeInterfacePrefix(parts);
         parts = removeClassSuffix(parts);
+        parts = removeSpecialSymbols(parts);
+
         StringBuilder result = new StringBuilder();
         for (int i = firstElement; i < parts.length; i++) {
             if (capitalizeFirst && i == firstElement) {
@@ -102,17 +119,26 @@ public class DocTemplateProcessorImpl implements DocTemplateProcessor {
     }
 
     private String[] removeInterfacePrefix(String[] parts) {
-        if (parts!= null && parts.length > 0 && "I".equalsIgnoreCase(parts[0])) {
+        if (parts != null && parts.length > 0 && "I".equalsIgnoreCase(parts[0])) {
             parts = Arrays.copyOfRange(parts, 1, parts.length);
         }
         return parts;
     }
 
     private String[] removeClassSuffix(String[] parts) {
-        if (parts!= null && parts.length > 0 && "Impl".equalsIgnoreCase(parts[parts.length - 1])) {
+        if (parts != null && parts.length > 0 && "Impl".equalsIgnoreCase(parts[parts.length - 1])) {
             parts = Arrays.copyOfRange(parts, 0, parts.length - 1);
         }
         return parts;
     }
 
+    private String[] removeSpecialSymbols(String[] parts) {
+        List<String> result = new ArrayList<String>();
+        for (String part : parts) {
+            if (!SPECIAL_SYMBOLS.contains(part)) {
+                result.add(part);
+            }
+        }
+        return result.toArray(new String[result.size()]);
+    }
 }
