@@ -0,0 +1,9 @@
+package com.github.setial.intellijjavadocs.exception;
+
+/**
+ * The type Template not found exception.
+ *
+ * @author Sergey Timofiychuk
+ */
+public class TemplateNotFoundException extends RuntimeException {
+}
