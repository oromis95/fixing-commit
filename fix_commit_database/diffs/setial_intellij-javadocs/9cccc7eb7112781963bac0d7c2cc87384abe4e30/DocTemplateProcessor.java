@@ -29,11 +29,13 @@ public interface DocTemplateProcessor {
     /**
      * Builds the description.
      *
+     *
      * @param description the Description
+     * @param capitalizeFirst the flag shows whether first word should be capitalized
      * @return generated description
      */
     @NotNull
-    String buildDescription(@NotNull String description);
+    String buildDescription(@NotNull String description, boolean capitalizeFirst);
 
     /**
      * Builds the description for the methods like getter, setter. There will be removed first word, e.g. get, set,
