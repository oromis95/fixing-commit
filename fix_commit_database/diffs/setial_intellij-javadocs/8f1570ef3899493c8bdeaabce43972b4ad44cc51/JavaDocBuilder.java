@@ -43,6 +43,8 @@ public class JavaDocBuilder {
      * @return the Java doc builder
      */
     public JavaDocBuilder closeJavaDoc() {
+        builder.append(JavaDocElements.NEW_LINE.getPresentation());
+        builder.append(JavaDocElements.WHITE_SPACE.getPresentation());
         builder.append(JavaDocElements.ENDING_ASTERISK.getPresentation());
         return this;
     }
@@ -55,6 +57,7 @@ public class JavaDocBuilder {
     public JavaDocBuilder addNewLine() {
         builder.append(JavaDocElements.NEW_LINE.getPresentation());
         builder.append(JavaDocElements.LINE_START.getPresentation());
+        builder.append(JavaDocElements.WHITE_SPACE.getPresentation());
         return this;
     }
 
@@ -84,7 +87,7 @@ public class JavaDocBuilder {
      * @return the Java doc builder
      */
     public JavaDocBuilder addTag(String name, JavaDocTag tag) {
-        addNewLine();
+        builder.append(JavaDocElements.WHITE_SPACE.getPresentation());
         builder.append(JavaDocElements.TAG_START.getPresentation());
         builder.append(name);
         builder.append(JavaDocElements.WHITE_SPACE.getPresentation());
@@ -96,10 +99,7 @@ public class JavaDocBuilder {
         }
 
         builder.append(JavaDocElements.WHITE_SPACE.getPresentation());
-        for (String description : tag.getDescription()) {
-            builder.append(description);
-            addNewLine();
-        }
+        addDescription(tag.getDescription());
         return this;
     }
 
@@ -112,9 +112,13 @@ public class JavaDocBuilder {
     public JavaDocBuilder addTags(@NotNull Map<String, List<JavaDocTag>> tags) {
         for (Entry<String, List<JavaDocTag>> entry : tags.entrySet()) {
             String name = entry.getKey();
-            for (JavaDocTag javaDocTag : entry.getValue()) {
+            List<JavaDocTag> javaDocTags = entry.getValue();
+            for (int i = 0; i < javaDocTags.size(); i++) {
+                JavaDocTag javaDocTag = javaDocTags.get(i);
                 addTag(name, javaDocTag);
-                addNewLine();
+                if (i < javaDocTags.size() - 1) {
+                    addNewLine();
+                }
             }
         }
         return this;
@@ -129,9 +133,7 @@ public class JavaDocBuilder {
     public JavaDocBuilder createDefaultJavaDoc(@NotNull JavaDoc javadoc) {
         openJavaDoc();
         addDescription(javadoc.getDescription());
-        addNewLine();
         addTags(javadoc.getTags());
-        addNewLine();
         closeJavaDoc();
         return this;
     }
