@@ -11,11 +11,13 @@ import com.intellij.psi.PsiClassType;
 import com.intellij.psi.PsiCodeBlock;
 import com.intellij.psi.PsiField;
 import com.intellij.psi.PsiMethod;
-import org.apache.commons.collections.MapUtils;
+import com.intellij.psi.PsiModifierList;
 import org.apache.velocity.Template;
 import org.apache.velocity.runtime.RuntimeInstance;
 import org.apache.velocity.runtime.RuntimeServices;
 import org.apache.velocity.runtime.parser.ParseException;
+import org.apache.velocity.runtime.parser.ParserConstants;
+import org.apache.velocity.runtime.parser.Token;
 import org.apache.velocity.runtime.parser.node.SimpleNode;
 import org.jdom.DataConversionException;
 import org.jdom.Document;
@@ -84,16 +86,7 @@ public class DocTemplateManagerImpl implements DocTemplateManager, ProjectCompon
         List<Element> elements = root.getChildren(TEMPLATE);
         for (Element element : elements) {
             String name = element.getAttribute(REGEXP).getValue();
-            SimpleNode node = velocityServices.parse(
-                    XmlUtils.trimElementContent(element),
-                    name);
-            Template template = new Template();
-            template.setRuntimeServices(velocityServices);
-            template.setData(node);
-            template.setName(node.getTemplateName());
-            template.initDocument();
-
-            templates.put(name, template);
+            templates.put(name, createTemplate(name, XmlUtils.trimElementContent(element)));
         }
     }
 
@@ -122,26 +115,12 @@ public class DocTemplateManagerImpl implements DocTemplateManager, ProjectCompon
 
     @Nullable
     @Override
+    @SuppressWarnings("ConstantConditions")
     public Template getClassTemplate(@NotNull PsiClass classElement) {
         StringBuilder builder = getClassSignature(classElement);
         return getMatchingTemplate(builder.toString(), classTemplates);
     }
 
-    @NotNull
-    @Override
-    public Map<String, String> getClassTemplates() {
-        Map<String, String> templates = new LinkedHashMap<String, String>();
-        for (Entry<String, Template> entry : classTemplates.entrySet()) {
-            templates.put(entry.getKey(), templateProcessor.merge(entry.getValue(), MapUtils.EMPTY_MAP));
-        }
-        return Collections.unmodifiableMap(templates);
-    }
-
-    @Override
-    public void putClassTemplate(@NotNull String regexp, @NotNull String template) {
-        // TODO
-    }
-
     @Nullable
     @Override
     public Template getMethodTemplate(@NotNull PsiMethod methodElement) {
@@ -160,19 +139,34 @@ public class DocTemplateManagerImpl implements DocTemplateManager, ProjectCompon
 
     }
 
-    @NotNull
+    @Nullable
     @Override
-    public Map<String, String> getMethodTemplates() {
-        Map<String, String> templates = new LinkedHashMap<String, String>();
-        for (Entry<String, Template> entry : methodTemplates.entrySet()) {
-            templates.put(entry.getKey(), templateProcessor.merge(entry.getValue(), MapUtils.EMPTY_MAP));
+    public Template getFieldTemplate(@NotNull PsiField psiField) {
+        return getMatchingTemplate(psiField.getText(), fieldTemplates);
+
+    }
+
+    @Nullable
+    private Template getMatchingTemplate(@NotNull String elementText, @NotNull Map<String, Template> templates) {
+        Template result = null;
+        for (Entry<String, Template> entry : templates.entrySet()) {
+            if (Pattern.compile(entry.getKey(), Pattern.DOTALL | Pattern.MULTILINE).matcher(elementText).matches()) {
+                result = entry.getValue();
+                break;
+            }
         }
-        return Collections.unmodifiableMap(templates);
+        return result;
     }
 
+    @NotNull
     @Override
-    public void putMethodTemplates(@NotNull String regexp, @NotNull String template) {
-        // TODO
+    public Map<String, String> getClassTemplates() {
+        Map<String, String> templates = new LinkedHashMap<String, String>();
+        for (Entry<String, Template> entry : classTemplates.entrySet()) {
+            String template = extractTemplate(entry.getValue().getData());
+            templates.put(entry.getKey(), template);
+        }
+        return templates;
     }
 
     @NotNull
@@ -180,21 +174,21 @@ public class DocTemplateManagerImpl implements DocTemplateManager, ProjectCompon
     public Map<String, String> getConstructorTemplates() {
         Map<String, String> templates = new LinkedHashMap<String, String>();
         for (Entry<String, Template> entry : constructorTemplates.entrySet()) {
-            templates.put(entry.getKey(), templateProcessor.merge(entry.getValue(), MapUtils.EMPTY_MAP));
+            String template = extractTemplate(entry.getValue().getData());
+            templates.put(entry.getKey(), template);
         }
-        return Collections.unmodifiableMap(templates);
+        return templates;
     }
 
+    @NotNull
     @Override
-    public void putConstructorTemplates(@NotNull String regexp, @NotNull String template) {
-        // TODO
-    }
-
-    @Nullable
-    @Override
-    public Template getFieldTemplate(@NotNull PsiField psiField) {
-        return getMatchingTemplate(psiField.getText(), fieldTemplates);
-
+    public Map<String, String> getMethodTemplates() {
+        Map<String, String> templates = new LinkedHashMap<String, String>();
+        for (Entry<String, Template> entry : methodTemplates.entrySet()) {
+            String template = extractTemplate(entry.getValue().getData());
+            templates.put(entry.getKey(), template);
+        }
+        return templates;
     }
 
     @NotNull
@@ -202,31 +196,52 @@ public class DocTemplateManagerImpl implements DocTemplateManager, ProjectCompon
     public Map<String, String> getFieldTemplates() {
         Map<String, String> templates = new LinkedHashMap<String, String>();
         for (Entry<String, Template> entry : fieldTemplates.entrySet()) {
-            templates.put(entry.getKey(), templateProcessor.merge(entry.getValue(), MapUtils.EMPTY_MAP));
+            String template = extractTemplate(entry.getValue().getData());
+            templates.put(entry.getKey(), template);
         }
-        return Collections.unmodifiableMap(templates);
+        return templates;
     }
 
     @Override
-    public void putFieldTemplates(@NotNull String regexp, @NotNull String template) {
-        // TODO
+    public void setClassTemplates(@NotNull Map<String, String> templates) {
+        setupTemplates(templates, classTemplates);
     }
 
-    @Nullable
-    private Template getMatchingTemplate(@NotNull String elementText, @NotNull Map<String, Template> templates) {
-        Template result = null;
-        for (Entry<String, Template> entry : templates.entrySet()) {
-            if (Pattern.compile(entry.getKey(), Pattern.DOTALL | Pattern.MULTILINE).matcher(elementText).matches()) {
-                result = entry.getValue();
-                break;
+    @Override
+    public void setConstructorTemplates(@NotNull Map<String, String> templates) {
+        setupTemplates(templates, constructorTemplates);
+    }
+
+    @Override
+    public void setMethodTemplates(@NotNull Map<String, String> templates) {
+        setupTemplates(templates, methodTemplates);
+    }
+
+    @Override
+    public void setFieldTemplates(@NotNull Map<String, String> templates) {
+        setupTemplates(templates, fieldTemplates);
+    }
+
+    private void setupTemplates(Map<String, String> from, Map<String, Template> to) {
+        Map<String, Template> result = new LinkedHashMap<String, Template>();
+        for (Entry<String, String> entry : from.entrySet()) {
+            try {
+                result.put(entry.getKey(), createTemplate(entry.getKey(), entry.getValue()));
+            } catch (Exception e) {
+                // TODO throw runtime exception and catch it at top level app
+                throw new RuntimeException(e);
             }
         }
-        return result;
+        to.clear();
+        to.putAll(result);
     }
 
     private StringBuilder getClassSignature(PsiClass classElement) {
         StringBuilder builder = new StringBuilder();
-        builder.append(classElement.getModifierList().getText());
+        PsiModifierList modifierList = classElement.getModifierList();
+        if (modifierList != null) {
+            builder.append(modifierList.getText());
+        }
         builder.append(" ");
         if (classElement.isInterface()) {
             builder.append("interface ");
@@ -236,7 +251,7 @@ public class DocTemplateManagerImpl implements DocTemplateManager, ProjectCompon
             builder.append("class ");
         }
         PsiClassType[] extendsTypes = classElement.getExtendsListTypes();
-        if (extendsTypes != null && extendsTypes.length > 0) {
+        if (extendsTypes.length > 0) {
             builder.append("extends ");
             for (int i = 0; i < extendsTypes.length; i++) {
                 PsiClassType extendsType = extendsTypes[i];
@@ -248,7 +263,7 @@ public class DocTemplateManagerImpl implements DocTemplateManager, ProjectCompon
             }
         }
         PsiClassType[] implementTypes = classElement.getImplementsListTypes();
-        if (implementTypes != null && implementTypes.length > 0) {
+        if (implementTypes.length > 0) {
             builder.append("implements");
             for (int i = 0; i < implementTypes.length; i++) {
                 PsiClassType implementType = implementTypes[i];
@@ -262,4 +277,25 @@ public class DocTemplateManagerImpl implements DocTemplateManager, ProjectCompon
         return builder;
     }
 
+    private Template createTemplate(String templateRegexp, String templateContent) throws ParseException, IOException {
+        SimpleNode node = velocityServices.parse(templateContent, templateRegexp);
+        Template template = new Template();
+        template.setRuntimeServices(velocityServices);
+        template.setData(node);
+        template.setName(node.getTemplateName());
+        template.initDocument();
+        return template;
+    }
+
+    private String extractTemplate(Object data) {
+        StringBuilder template = new StringBuilder();
+
+        Token token = ((SimpleNode) data).getFirstToken();
+        while (token != null && token.kind != ParserConstants.EOF) {
+            template.append(token.toString());
+            token = token.next;
+        }
+        return template.toString();
+    }
+
 }
