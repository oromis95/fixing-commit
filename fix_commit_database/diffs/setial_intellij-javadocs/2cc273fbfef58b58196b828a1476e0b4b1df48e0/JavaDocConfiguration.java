@@ -10,7 +10,8 @@ import org.jetbrains.annotations.Nullable;
  */
 public interface JavaDocConfiguration {
 
-    String COMPONENT_NAME = "JavaDocConfiguration_1.0.2";
+    String COMPONENT_VERSION = "1.0.5";
+    String COMPONENT_NAME = "JavaDocConfiguration_" + COMPONENT_VERSION;
 
     /**
      * Gets the configuration.
