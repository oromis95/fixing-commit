@@ -13,6 +13,7 @@ public class GeneralSettings {
     private Set<Level> levels;
     private Set<Visibility> visibilities;
     private boolean overriddenMethods;
+    private boolean splittedClassName;
 
     /**
      * Gets javadoc update mode.
@@ -86,4 +87,12 @@ public class GeneralSettings {
         this.overriddenMethods = overriddenMethods;
     }
 
+    public boolean isSplittedClassName() {
+        return splittedClassName;
+    }
+
+    public void setSplittedClassName(boolean splittedClassName) {
+        this.splittedClassName = splittedClassName;
+    }
+
 }
