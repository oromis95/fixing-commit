@@ -34,7 +34,7 @@ public class MethodJavaDocGenerator implements JavaDocGenerator<PsiMethod> {
         } else {
             result.append(" method\n");
         }
-
+        result.append(".\n");
         result.append("*\n");
         if (parameterList.getParametersCount() > 0) {
             for (PsiParameter parameter : parameterList.getParameters()) {
