@@ -2,15 +2,15 @@ package com.github.setial.intellijjavadocs.template.impl;
 
 import com.github.setial.intellijjavadocs.template.DocTemplateProcessor;
 import com.github.setial.intellijjavadocs.utils.XmlUtils;
+import freemarker.template.Template;
+import freemarker.template.TemplateException;
 import org.apache.commons.lang3.StringUtils;
-import org.apache.velocity.Template;
-import org.apache.velocity.VelocityContext;
-import org.apache.velocity.context.Context;
 import org.jetbrains.annotations.NotNull;
 import org.jetbrains.annotations.Nullable;
 
 import java.io.IOException;
 import java.io.StringWriter;
+import java.util.Arrays;
 import java.util.Map;
 
 /**
@@ -51,14 +51,17 @@ public class DocTemplateProcessorImpl implements DocTemplateProcessor {
             // TODO throw exception and catch it at top level of app
             return StringUtils.EMPTY;
         }
-        Context context = new VelocityContext(params);
+
         StringWriter writer = new StringWriter();
-        template.merge(context, writer);
         try {
+            template.process(params, writer);
             return XmlUtils.normalizeTemplate(writer.toString());
         } catch (IOException e) {
             // TODO throw runtime exception and catch it at top level app
             throw new RuntimeException(e);
+        } catch (TemplateException e) {
+            // TODO throw runtime exception and catch it at top level app
+            throw new RuntimeException(e);
         }
     }
 
@@ -82,12 +85,14 @@ public class DocTemplateProcessorImpl implements DocTemplateProcessor {
 
     private String buildDescription(String description, int firstElement, boolean capitalizeFirst) {
         String[] parts = StringUtils.splitByCharacterTypeCamelCase(description.replaceAll("<.+>", ""));
+        parts = removeInterfacePrefix(parts);
+        parts = removeClassSuffix(parts);
         StringBuilder result = new StringBuilder();
         for (int i = firstElement; i < parts.length; i++) {
             if (capitalizeFirst && i == firstElement) {
-                result.append(StringUtils.capitalize(parts[i]));
+                result.append(StringUtils.capitalize(StringUtils.lowerCase(parts[i])));
             } else {
-                result.append(StringUtils.uncapitalize(parts[i]));
+                result.append(StringUtils.lowerCase(parts[i]));
             }
             if (i < parts.length - 1) {
                 result.append(" ");
@@ -96,4 +101,18 @@ public class DocTemplateProcessorImpl implements DocTemplateProcessor {
         return result.toString();
     }
 
+    private String[] removeInterfacePrefix(String[] parts) {
+        if (parts!= null && parts.length > 0 && "I".equalsIgnoreCase(parts[0])) {
+            parts = Arrays.copyOfRange(parts, 1, parts.length);
+        }
+        return parts;
+    }
+
+    private String[] removeClassSuffix(String[] parts) {
+        if (parts!= null && parts.length > 0 && "Impl".equalsIgnoreCase(parts[parts.length - 1])) {
+            parts = Arrays.copyOfRange(parts, 0, parts.length - 1);
+        }
+        return parts;
+    }
+
 }
