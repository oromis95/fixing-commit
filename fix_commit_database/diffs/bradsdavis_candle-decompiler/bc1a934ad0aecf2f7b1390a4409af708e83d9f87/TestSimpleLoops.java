@@ -7,22 +7,25 @@ public class TestSimpleLoops {
 
 	public void testForLoop() {
 		
-		
 		for(int i=0; i<5; i++) {
 			System.out.println(i);
 		}
 		
-		
 		int[] testArray = new int[]{};
 		for(int i : testArray) {
 			System.out.println(i);
 		} 
-		
+
 		int k=0;
 		int j=0;
 		while(j < 5) {
 			System.out.println("Test while.");
 			j++;
+			
+			if(j < 3) {
+				continue;
+			}
+			
 			k++;
 		}
 		
@@ -33,6 +36,5 @@ public class TestSimpleLoops {
 			System.out.println(test);
 		}
 		
-		
 	}
 }
