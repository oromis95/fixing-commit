@@ -0,0 +1,16 @@
+package org.candle.decompiler.intermediate.graph;
+
+import org.jgrapht.graph.DefaultEdge;
+
+public class IntermediateEdge extends DefaultEdge {
+
+	@Override
+	protected Object getSource() {
+		return super.getSource();
+	}
+	
+	@Override
+	protected Object getTarget() {
+		return super.getTarget();
+	}
+}
