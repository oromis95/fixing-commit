@@ -1,14 +1,11 @@
 package org.candle.decompiler.intermediate.graph.enhancer;
 
-import java.util.List;
-
 import org.candle.decompiler.intermediate.code.AbstractIntermediate;
 import org.candle.decompiler.intermediate.code.ConditionalIntermediate;
 import org.candle.decompiler.intermediate.code.conditional.IfIntermediate;
 import org.candle.decompiler.intermediate.graph.GraphIntermediateVisitor;
 import org.candle.decompiler.intermediate.graph.IntermediateEdge;
 import org.jgrapht.DirectedGraph;
-import org.jgrapht.Graphs;
 
 public class If extends GraphIntermediateVisitor {
 
@@ -35,29 +32,5 @@ public class If extends GraphIntermediateVisitor {
 		this.intermediateGraph.removeVertex(line);
 	}
 	
-	protected void updatePredecessorConditional(ConditionalIntermediate source, ConditionalIntermediate target) {
-		if(source instanceof ConditionalIntermediate) {
-			List<AbstractIntermediate> predecessors = Graphs.predecessorListOf(intermediateGraph, source);
-			
-			for(AbstractIntermediate predecessor : predecessors) {
-				updatePredecessorConditional(predecessor, (ConditionalIntermediate)source, target);
-			}
-		}
-	}
 
-	protected void updatePredecessorConditional(AbstractIntermediate predecessor, ConditionalIntermediate source, ConditionalIntermediate target) {
-		if(predecessor instanceof ConditionalIntermediate) {
-			ConditionalIntermediate conditionalPredecessor = (ConditionalIntermediate)predecessor;
-			
-			if(conditionalPredecessor.getTrueTarget() == source) {
-				conditionalPredecessor.setTrueTarget(target);
-			}
-			
-			if(conditionalPredecessor.getFalseTarget() == source) {
-				conditionalPredecessor.setFalseTarget(target);
-			}
-			
-		}
-	}
-	
 }
