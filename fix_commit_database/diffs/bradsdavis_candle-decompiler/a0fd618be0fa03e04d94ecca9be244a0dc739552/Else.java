@@ -27,7 +27,6 @@ public class Else extends GraphIntermediateVisitor {
 
 	@Override
 	public void visitAbstractLine(AbstractIntermediate line) {
-
 		orderedVertexes = new TreeSet<AbstractIntermediate>(new IntermediateComparator());
 		orderedVertexes.addAll(intermediateGraph.vertexSet());
 		
@@ -37,7 +36,6 @@ public class Else extends GraphIntermediateVisitor {
 			return;
 		}
 		
-		
 		TreeSet<GoToIntermediate> gotoIntermediates = new TreeSet<GoToIntermediate>(new IntermediateComparator());
 		
 		for(AbstractIntermediate predecessor : predecessors) {
