@@ -8,13 +8,13 @@ import org.apache.bcel.generic.InstructionHandle;
 
 public class Assignment extends Expression {
 
-	private Expression leftHandSide;
-	private Expression rightHandSide;
+	private Expression leftHandSide = null;
+	private Expression rightHandSide = null;
 	
 	public Assignment(InstructionHandle instructionHandle, Expression left, Expression right) {
 		super(instructionHandle);
-		this.leftHandSide = left;
-		this.rightHandSide = right;
+		this.setLeftHandSide(left);
+		this.setRightHandSide(right);
 	}
 	
 	public Expression getLeftHandSide() {
@@ -25,12 +25,28 @@ public class Assignment extends Expression {
 		return rightHandSide;
 	}
 	
-	public void setLeftHandSide(Expression leftHandSide) {
-		this.leftHandSide = leftHandSide;
+	public void setLeftHandSide(Expression l) {
+		if(leftHandSide != null) {
+			this.leftHandSide.setParent(null);
+		}
+		
+		this.leftHandSide = l;
+		
+		if(leftHandSide != null) {
+			this.leftHandSide.setParent(this);
+		}
 	}
 	
-	public void setRightHandSide(Expression rightHandSide) {
-		this.rightHandSide = rightHandSide;
+	public void setRightHandSide(Expression r) {
+		if(rightHandSide != null) {
+			this.rightHandSide.setParent(null);
+		}
+		
+		this.rightHandSide = r;
+		
+		if(rightHandSide != null) {
+			this.rightHandSide.setParent(this);
+		}
 	}
 	
 	@Override
