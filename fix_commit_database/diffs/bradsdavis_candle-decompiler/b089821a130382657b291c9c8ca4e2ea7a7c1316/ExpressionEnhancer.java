@@ -1,9 +1,12 @@
 package org.candle.decompiler.intermediate.graph.enhancer;
 
+import org.apache.bcel.generic.ObjectType;
+import org.apache.commons.lang.StringUtils;
 import org.candle.decompiler.intermediate.code.StatementIntermediate;
 import org.candle.decompiler.intermediate.expression.Expression;
 import org.candle.decompiler.intermediate.expression.ASTListener;
 import org.candle.decompiler.intermediate.expression.MethodInvocation;
+import org.candle.decompiler.intermediate.expression.NewInstance;
 import org.candle.decompiler.intermediate.graph.GraphIntermediateVisitor;
 import org.candle.decompiler.intermediate.graph.context.IntermediateGraphContext;
 
@@ -22,8 +25,14 @@ public class ExpressionEnhancer extends GraphIntermediateVisitor {
 			
 			@Override
 			public void accept(Expression e) {
-				if(e instanceof MethodInvocation) {
-					System.out.println(e);
+				if(e instanceof NewInstance) {
+					if(((NewInstance) e).getType() instanceof ObjectType) {
+						ObjectType obj = (ObjectType)((NewInstance) e).getType();
+						if(StringUtils.equals("java.lang.StringBuilder", obj.getClassName())) {
+							System.out.println(obj.getClassName());
+						}
+						
+					}
 				}
 			}
 		});
