@@ -22,6 +22,5 @@ package com.mifmif.common.regex.util;
  *
  */
 public interface Iterable {
-	public Iterator iterator();
-
+	Iterator iterator();
 }
