@@ -12,6 +12,8 @@
  */
 package org.eclipse.mat.util;
 
+import org.eclipse.mat.hprof.Messages;
+
 public class SimpleMonitor {
   String task;
   IProgressListener delegate;
@@ -49,6 +51,10 @@ public class SimpleMonitor {
       this.majorUnits = majorUnits;
     }
 
+    public final void beginTask(Messages name, int totalWork) {
+      beginTask(name.pattern, totalWork);
+    }
+
     public void beginTask(String name, int totalWork) {
       if (name != null) delegate.subTask(name);
 
