@@ -18,7 +18,7 @@ import java.io.EOFException;
 import java.io.FilterInputStream;
 import java.io.IOException;
 import java.io.InputStream;
-import org.eclipse.mat.parser.internal.Messages;
+import org.eclipse.mat.hprof.Messages;
 
 public class PositionInputStream extends FilterInputStream implements DataInput {
   private final byte[] readBuffer = new byte[32];
@@ -51,11 +51,11 @@ public class PositionInputStream extends FilterInputStream implements DataInput
   }
 
   public void mark(int readLimit) {
-    throw new UnsupportedOperationException(Messages.PositionInputStream_mark);
+    throw new UnsupportedOperationException(Messages.PositionInputStream_mark.pattern);
   }
 
   public void reset() {
-    throw new UnsupportedOperationException(Messages.PositionInputStream_reset);
+    throw new UnsupportedOperationException(Messages.PositionInputStream_reset.pattern);
   }
 
   public final int skipBytes(int n) throws IOException {
@@ -105,7 +105,7 @@ public class PositionInputStream extends FilterInputStream implements DataInput
       position = pos;
       ((SimpleBufferedRandomAccessInputStream) in).seek(pos);
     } else {
-      throw new UnsupportedOperationException(Messages.PositionInputStream_seek);
+      throw new UnsupportedOperationException(Messages.PositionInputStream_seek.pattern);
     }
   }
 
