@@ -640,21 +640,21 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 			Class<?> chatSerializerClazz;
 
 			String version = Reflection.getVersion();
-            double majorVersion = Double.parseDouble(version.replace('_', '.').substring(1, 4));
-            int lesserVersion = Integer.parseInt(version.substring(6, 7));
+			double majorVersion = Double.parseDouble(version.replace('_', '.').substring(1, 4));
+			int lesserVersion = Integer.parseInt(version.substring(6, 7));
 
-            if(majorVersion < 1.8 || (majorVersion == 1.8 && lesserVersion == 1)) {
-                chatSerializerClazz = Reflection.getNMSClass("ChatSerializer");
-            } else {
-                chatSerializerClazz = Reflection.getNMSClass("IChatBaseComponent$ChatSerializer");
-            }
+			if (majorVersion < 1.8 || (majorVersion == 1.8 && lesserVersion == 1)) {
+				chatSerializerClazz = Reflection.getNMSClass("ChatSerializer");
+			} else {
+				chatSerializerClazz = Reflection.getNMSClass("IChatBaseComponent$ChatSerializer");
+			}
 
-            if (chatSerializerClazz == null) {
-                throw new ClassNotFoundException("Can't find the ChatSerializer class");
-            }
+			if (chatSerializerClazz == null) {
+				throw new ClassNotFoundException("Can't find the ChatSerializer class");
+			}
 
-            for(Field declaredField : chatSerializerClazz.getDeclaredFields()){
-				if(Modifier.isFinal(declaredField.getModifiers()) && Modifier.isStatic(declaredField.getModifiers()) && declaredField.getType().getName().endsWith("Gson")){
+			for (Field declaredField : chatSerializerClazz.getDeclaredFields()) {
+				if (Modifier.isFinal(declaredField.getModifiers()) && Modifier.isStatic(declaredField.getModifiers()) && declaredField.getType().getName().endsWith("Gson")) {
 					// We've found our field
 					declaredField.setAccessible(true);
 					nmsChatSerializerGsonInstance = declaredField.get(null);
