@@ -171,7 +171,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable {
 
 	/**
 	 * Set the behavior of the current editing component to display information about an achievement when the client hovers over the text.
-	 * <p>Tooltips inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
 	 * @param name The name of the achievement to display, excluding the "achievement." prefix.
 	 * @return This builder instance.
 	 */
@@ -182,7 +182,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable {
 
 	/**
 	 * Set the behavior of the current editing component to display information about an achievement when the client hovers over the text.
-	 * <p>Tooltips inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
 	 * @param which The achievement to display.
 	 * @return This builder instance.
 	 */
@@ -198,7 +198,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable {
 
 	/**
 	 * Set the behavior of the current editing component to display information about a parameterless statistic when the client hovers over the text.
-	 * <p>Tooltips inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
 	 * @param which The statistic to display.
 	 * @return This builder instance.
 	 * @exception IllegalArgumentException If the statistic requires a parameter which was not supplied.
@@ -219,7 +219,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable {
 
 	/**
 	 * Set the behavior of the current editing component to display information about a statistic parametered with a material when the client hovers over the text.
-	 * <p>Tooltips inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
 	 * @param which The statistic to display.
 	 * @param item The sole material parameter to the statistic.
 	 * @return This builder instance.
@@ -244,7 +244,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable {
 
 	/**
 	 * Set the behavior of the current editing component to display information about a statistic parametered with an entity type when the client hovers over the text.
-	 * <p>Tooltips inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
 	 * @param which The statistic to display.
 	 * @param entity The sole entity type parameter to the statistic.
 	 * @return This builder instance.
@@ -269,7 +269,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable {
 
 	/**
 	 * Set the behavior of the current editing component to display information about an item when the client hovers over the text.
-	 * <p>Tooltips inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
 	 * @param itemJSON A string representing the JSON-serialized NBT data tag of an {@link ItemStack}.
 	 * @return This builder instance.
 	 */
@@ -280,7 +280,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable {
 
 	/**
 	 * Set the behavior of the current editing component to display information about an item when the client hovers over the text.
-	 * <p>Tooltips inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
 	 * @param itemStack The stack for which to display information.
 	 * @return This builder instance.
 	 */
@@ -296,7 +296,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable {
 
 	/**
 	 * Set the behavior of the current editing component to display raw text when the client hovers over the text.
-	 * <p>Tooltips inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
 	 * @param text The text, which supports newlines, which will be displayed to the client upon hovering.
 	 * @return This builder instance.
 	 */
@@ -307,7 +307,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable {
 
 	/**
 	 * Set the behavior of the current editing component to display raw text when the client hovers over the text.
-	 * <p>Tooltips inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
 	 * @param lines The lines of text which will be displayed to the client upon hovering. The iteration order of this object will be the order in which the lines of the tooltip are created.
 	 * @return This builder instance.
 	 */
@@ -325,7 +325,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable {
 
 	/**
 	 * Set the behavior of the current editing component to display raw text when the client hovers over the text.
-	 * <p>Tooltips inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
 	 * @param lines The lines of text which will be displayed to the client upon hovering.
 	 * @return This builder instance.
 	 */
@@ -343,7 +343,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable {
 
 	/**
 	 * Set the behavior of the current editing component to display formatted text when the client hovers over the text.
-	 * <p>Tooltips inherit display characteristics, such as color and styles, from the message component on which they are applied unless the tooltip formatting overrides them.</p>
+	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
 	 * @param text The formatted text which will be displayed to the client upon hovering.
 	 * @return This builder instance.
 	 */
@@ -361,7 +361,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable {
 
 	/**
 	 * Set the behavior of the current editing component to display the specified lines of formatted text when the client hovers over the text.
-	 * <p>Tooltips inherit display characteristics, such as color and styles, from the message component on which they are applied unless the tooltip formatting overrides them.</p>
+	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
 	 * @param text The formatted text which will be displayed to the client upon hovering.
 	 * @return This builder instance.
 	 */
