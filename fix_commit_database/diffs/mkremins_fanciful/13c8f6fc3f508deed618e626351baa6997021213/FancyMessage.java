@@ -1,21 +1,5 @@
 package mkremins.fanciful;
 
-import java.io.IOException;
-import java.io.StringWriter;
-import java.lang.reflect.Constructor;
-import java.lang.reflect.Field;
-import java.lang.reflect.InvocationTargetException;
-import java.lang.reflect.Method;
-import java.lang.reflect.Modifier;
-import java.util.ArrayList;
-import java.util.Arrays;
-import java.util.HashMap;
-import java.util.Iterator;
-import java.util.List;
-import java.util.Map;
-import java.util.logging.Level;
-import static mkremins.fanciful.TextualComponent.rawText;
-
 import com.google.gson.JsonArray;
 import com.google.gson.JsonElement;
 import com.google.gson.JsonObject;
@@ -36,19 +20,36 @@ import org.bukkit.entity.EntityType;
 import org.bukkit.entity.Player;
 import org.bukkit.inventory.ItemStack;
 
+import java.io.IOException;
+import java.io.StringWriter;
+import java.lang.reflect.Constructor;
+import java.lang.reflect.Field;
+import java.lang.reflect.InvocationTargetException;
+import java.lang.reflect.Method;
+import java.lang.reflect.Modifier;
+import java.util.ArrayList;
+import java.util.Arrays;
+import java.util.HashMap;
+import java.util.Iterator;
+import java.util.List;
+import java.util.Map;
+import java.util.logging.Level;
+
+import static mkremins.fanciful.TextualComponent.rawText;
+
 /**
  * Represents a formattable message. Such messages can use elements such as colors, formatting codes, hover and click data, and other features provided by the vanilla Minecraft <a href="http://minecraft.gamepedia.com/Tellraw#Raw_JSON_Text">JSON message formatter</a>.
  * This class allows plugins to emulate the functionality of the vanilla Minecraft <a href="http://minecraft.gamepedia.com/Commands#tellraw">tellraw command</a>.
  * <p>
  * This class follows the builder pattern, allowing for method chaining.
  * It is set up such that invocations of property-setting methods will affect the current editing component,
- * and a call to {@link #then()} or {@link #then(Object)} will append a new editing component to the end of the message,
+ * and a call to {@link #then()} or {@link #then(String)} will append a new editing component to the end of the message,
  * optionally initializing it with text. Further property-setting method calls will affect that editing component.
  * </p>
  */
 public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<MessagePart>, ConfigurationSerializable {
 
-	static{
+	static {
 		ConfigurationSerialization.registerClass(FancyMessage.class);
 	}
 
@@ -58,11 +59,11 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 
 	private static Constructor<?> nmsPacketPlayOutChatConstructor;
 
-        @Override
-	public FancyMessage clone() throws CloneNotSupportedException{
-		FancyMessage instance = (FancyMessage)super.clone();
+	@Override
+	public FancyMessage clone() throws CloneNotSupportedException {
+		FancyMessage instance = (FancyMessage) super.clone();
 		instance.messageParts = new ArrayList<MessagePart>(messageParts.size());
-		for(int i = 0; i < messageParts.size(); i++){
+		for (int i = 0; i < messageParts.size(); i++) {
 			instance.messageParts.add(i, messageParts.get(i).clone());
 		}
 		instance.dirty = false;
@@ -72,6 +73,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 
 	/**
 	 * Creates a JSON message with text.
+	 *
 	 * @param firstPartText The existing text in the message.
 	 */
 	public FancyMessage(final String firstPartText) {
@@ -84,7 +86,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 		jsonString = null;
 		dirty = false;
 
-		if(nmsPacketPlayOutChatConstructor == null){
+		if (nmsPacketPlayOutChatConstructor == null) {
 			try {
 				nmsPacketPlayOutChatConstructor = Reflection.getNMSClass("PacketPlayOutChat").getDeclaredConstructor(Reflection.getNMSClass("IChatBaseComponent"));
 				nmsPacketPlayOutChatConstructor.setAccessible(true);
@@ -100,11 +102,12 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 	 * Creates a JSON message without text.
 	 */
 	public FancyMessage() {
-		this((TextualComponent)null);
+		this((TextualComponent) null);
 	}
 
 	/**
 	 * Sets the text of the current editing component to a value.
+	 *
 	 * @param text The new text of the current editing component.
 	 * @return This builder instance.
 	 */
@@ -117,6 +120,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 
 	/**
 	 * Sets the text of the current editing component to a value.
+	 *
 	 * @param text The new text of the current editing component.
 	 * @return This builder instance.
 	 */
@@ -129,9 +133,10 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 
 	/**
 	 * Sets the color of the current editing component to a value.
+	 *
 	 * @param color The new color of the current editing component.
 	 * @return This builder instance.
-	 * @exception IllegalArgumentException If the specified {@code ChatColor} enumeration value is not a color (but a format value).
+	 * @throws IllegalArgumentException If the specified {@code ChatColor} enumeration value is not a color (but a format value).
 	 */
 	public FancyMessage color(final ChatColor color) {
 		if (!color.isColor()) {
@@ -144,9 +149,10 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 
 	/**
 	 * Sets the stylization of the current editing component.
+	 *
 	 * @param styles The array of styles to apply to the editing component.
 	 * @return This builder instance.
-	 * @exception IllegalArgumentException If any of the enumeration values in the array do not represent formatters.
+	 * @throws IllegalArgumentException If any of the enumeration values in the array do not represent formatters.
 	 */
 	public FancyMessage style(ChatColor... styles) {
 		for (final ChatColor style : styles) {
@@ -161,6 +167,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 
 	/**
 	 * Set the behavior of the current editing component to instruct the client to open a file on the client side filesystem when the currently edited part of the {@code FancyMessage} is clicked.
+	 *
 	 * @param path The path of the file on the client filesystem.
 	 * @return This builder instance.
 	 */
@@ -171,6 +178,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 
 	/**
 	 * Set the behavior of the current editing component to instruct the client to open a webpage in the client's web browser when the currently edited part of the {@code FancyMessage} is clicked.
+	 *
 	 * @param url The URL of the page to open when the link is clicked.
 	 * @return This builder instance.
 	 */
@@ -182,6 +190,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 	/**
 	 * Set the behavior of the current editing component to instruct the client to replace the chat input box content with the specified string when the currently edited part of the {@code FancyMessage} is clicked.
 	 * The client will not immediately send the command to the server to be executed unless the client player submits the command/chat message, usually with the enter key.
+	 *
 	 * @param command The text to display in the chat bar of the client.
 	 * @return This builder instance.
 	 */
@@ -189,10 +198,11 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 		onClick("suggest_command", command);
 		return this;
 	}
-	
+
 	/**
 	 * Set the behavior of the current editing component to instruct the client to append the chat input box content with the specified string when the currently edited part of the {@code FancyMessage} is SHIFT-CLICKED.
 	 * The client will not immediately send the command to the server to be executed unless the client player submits the command/chat message, usually with the enter key.
+	 *
 	 * @param command The text to append to the chat bar of the client.
 	 * @return This builder instance.
 	 */
@@ -205,6 +215,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 	/**
 	 * Set the behavior of the current editing component to instruct the client to send the specified string to the server as a chat message when the currently edited part of the {@code FancyMessage} is clicked.
 	 * The client <b>will</b> immediately send the command to the server to be executed when the editing component is clicked.
+	 *
 	 * @param command The text to display in the chat bar of the client.
 	 * @return This builder instance.
 	 */
@@ -216,6 +227,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 	/**
 	 * Set the behavior of the current editing component to display information about an achievement when the client hovers over the text.
 	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 *
 	 * @param name The name of the achievement to display, excluding the "achievement." prefix.
 	 * @return This builder instance.
 	 */
@@ -227,6 +239,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 	/**
 	 * Set the behavior of the current editing component to display information about an achievement when the client hovers over the text.
 	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 *
 	 * @param which The achievement to display.
 	 * @return This builder instance.
 	 */
@@ -235,23 +248,24 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 			Object achievement = Reflection.getMethod(Reflection.getOBCClass("CraftStatistic"), "getNMSAchievement", Achievement.class).invoke(null, which);
 			return achievementTooltip((String) Reflection.getField(Reflection.getNMSClass("Achievement"), "name").get(achievement));
 		} catch (IllegalAccessException e) {
-                        Bukkit.getLogger().log(Level.WARNING, "Could not access method.", e);
+			Bukkit.getLogger().log(Level.WARNING, "Could not access method.", e);
 			return this;
 		} catch (IllegalArgumentException e) {
-                        Bukkit.getLogger().log(Level.WARNING, "Argument could not be passed.", e);
-                        return this;
-                } catch (InvocationTargetException e) {
-                        Bukkit.getLogger().log(Level.WARNING, "A error has occured durring invoking of method.", e);
-                        return this;
-                }
+			Bukkit.getLogger().log(Level.WARNING, "Argument could not be passed.", e);
+			return this;
+		} catch (InvocationTargetException e) {
+			Bukkit.getLogger().log(Level.WARNING, "A error has occured durring invoking of method.", e);
+			return this;
+		}
 	}
 
 	/**
 	 * Set the behavior of the current editing component to display information about a parameterless statistic when the client hovers over the text.
 	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 *
 	 * @param which The statistic to display.
 	 * @return This builder instance.
-	 * @exception IllegalArgumentException If the statistic requires a parameter which was not supplied.
+	 * @throws IllegalArgumentException If the statistic requires a parameter which was not supplied.
 	 */
 	public FancyMessage statisticTooltip(final Statistic which) {
 		Type type = which.getType();
@@ -262,24 +276,25 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 			Object statistic = Reflection.getMethod(Reflection.getOBCClass("CraftStatistic"), "getNMSStatistic", Statistic.class).invoke(null, which);
 			return achievementTooltip((String) Reflection.getField(Reflection.getNMSClass("Statistic"), "name").get(statistic));
 		} catch (IllegalAccessException e) {
-                        Bukkit.getLogger().log(Level.WARNING, "Could not access method.", e);
+			Bukkit.getLogger().log(Level.WARNING, "Could not access method.", e);
 			return this;
 		} catch (IllegalArgumentException e) {
-                        Bukkit.getLogger().log(Level.WARNING, "Argument could not be passed.", e);
-                        return this;
-                } catch (InvocationTargetException e) {
-                        Bukkit.getLogger().log(Level.WARNING, "A error has occured durring invoking of method.", e);
-                        return this;
-                }
+			Bukkit.getLogger().log(Level.WARNING, "Argument could not be passed.", e);
+			return this;
+		} catch (InvocationTargetException e) {
+			Bukkit.getLogger().log(Level.WARNING, "A error has occured durring invoking of method.", e);
+			return this;
+		}
 	}
 
 	/**
 	 * Set the behavior of the current editing component to display information about a statistic parameter with a material when the client hovers over the text.
 	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 *
 	 * @param which The statistic to display.
-	 * @param item The sole material parameter to the statistic.
+	 * @param item  The sole material parameter to the statistic.
 	 * @return This builder instance.
-	 * @exception IllegalArgumentException If the statistic requires a parameter which was not supplied, or was supplied a parameter that was not required.
+	 * @throws IllegalArgumentException If the statistic requires a parameter which was not supplied, or was supplied a parameter that was not required.
 	 */
 	public FancyMessage statisticTooltip(final Statistic which, Material item) {
 		Type type = which.getType();
@@ -293,24 +308,25 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 			Object statistic = Reflection.getMethod(Reflection.getOBCClass("CraftStatistic"), "getMaterialStatistic", Statistic.class, Material.class).invoke(null, which, item);
 			return achievementTooltip((String) Reflection.getField(Reflection.getNMSClass("Statistic"), "name").get(statistic));
 		} catch (IllegalAccessException e) {
-                        Bukkit.getLogger().log(Level.WARNING, "Could not access method.", e);
+			Bukkit.getLogger().log(Level.WARNING, "Could not access method.", e);
 			return this;
 		} catch (IllegalArgumentException e) {
-                        Bukkit.getLogger().log(Level.WARNING, "Argument could not be passed.", e);
-                        return this;
-                } catch (InvocationTargetException e) {
-                        Bukkit.getLogger().log(Level.WARNING, "A error has occured durring invoking of method.", e);
-                        return this;
-                }
+			Bukkit.getLogger().log(Level.WARNING, "Argument could not be passed.", e);
+			return this;
+		} catch (InvocationTargetException e) {
+			Bukkit.getLogger().log(Level.WARNING, "A error has occured durring invoking of method.", e);
+			return this;
+		}
 	}
 
 	/**
 	 * Set the behavior of the current editing component to display information about a statistic parameter with an entity type when the client hovers over the text.
 	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
-	 * @param which The statistic to display.
+	 *
+	 * @param which  The statistic to display.
 	 * @param entity The sole entity type parameter to the statistic.
 	 * @return This builder instance.
-	 * @exception IllegalArgumentException If the statistic requires a parameter which was not supplied, or was supplied a parameter that was not required.
+	 * @throws IllegalArgumentException If the statistic requires a parameter which was not supplied, or was supplied a parameter that was not required.
 	 */
 	public FancyMessage statisticTooltip(final Statistic which, EntityType entity) {
 		Type type = which.getType();
@@ -324,20 +340,21 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 			Object statistic = Reflection.getMethod(Reflection.getOBCClass("CraftStatistic"), "getEntityStatistic", Statistic.class, EntityType.class).invoke(null, which, entity);
 			return achievementTooltip((String) Reflection.getField(Reflection.getNMSClass("Statistic"), "name").get(statistic));
 		} catch (IllegalAccessException e) {
-                        Bukkit.getLogger().log(Level.WARNING, "Could not access method.", e);
+			Bukkit.getLogger().log(Level.WARNING, "Could not access method.", e);
 			return this;
 		} catch (IllegalArgumentException e) {
-                        Bukkit.getLogger().log(Level.WARNING, "Argument could not be passed.", e);
-                        return this;
-                } catch (InvocationTargetException e) {
-                        Bukkit.getLogger().log(Level.WARNING, "A error has occured durring invoking of method.", e);
-                        return this;
-                }
+			Bukkit.getLogger().log(Level.WARNING, "Argument could not be passed.", e);
+			return this;
+		} catch (InvocationTargetException e) {
+			Bukkit.getLogger().log(Level.WARNING, "A error has occured durring invoking of method.", e);
+			return this;
+		}
 	}
 
 	/**
 	 * Set the behavior of the current editing component to display information about an item when the client hovers over the text.
 	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 *
 	 * @param itemJSON A string representing the JSON-serialized NBT data tag of an {@link ItemStack}.
 	 * @return This builder instance.
 	 */
@@ -349,6 +366,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 	/**
 	 * Set the behavior of the current editing component to display information about an item when the client hovers over the text.
 	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 *
 	 * @param itemStack The stack for which to display information.
 	 * @return This builder instance.
 	 */
@@ -361,11 +379,11 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 			return this;
 		}
 	}
-	
 
 	/**
 	 * Set the behavior of the current editing component to display raw text when the client hovers over the text.
 	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 *
 	 * @param text The text, which supports newlines, which will be displayed to the client upon hovering.
 	 * @return This builder instance.
 	 */
@@ -377,6 +395,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 	/**
 	 * Set the behavior of the current editing component to display raw text when the client hovers over the text.
 	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 *
 	 * @param lines The lines of text which will be displayed to the client upon hovering. The iteration order of this object will be the order in which the lines of the tooltip are created.
 	 * @return This builder instance.
 	 */
@@ -388,14 +407,15 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 	/**
 	 * Set the behavior of the current editing component to display raw text when the client hovers over the text.
 	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 *
 	 * @param lines The lines of text which will be displayed to the client upon hovering.
 	 * @return This builder instance.
 	 */
 	public FancyMessage tooltip(final String... lines) {
 		StringBuilder builder = new StringBuilder();
-		for(int i = 0; i < lines.length; i++){
+		for (int i = 0; i < lines.length; i++) {
 			builder.append(lines[i]);
-			if(i != lines.length - 1){
+			if (i != lines.length - 1) {
 				builder.append('\n');
 			}
 		}
@@ -406,14 +426,15 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 	/**
 	 * Set the behavior of the current editing component to display formatted text when the client hovers over the text.
 	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 *
 	 * @param text The formatted text which will be displayed to the client upon hovering.
 	 * @return This builder instance.
 	 */
-	public FancyMessage formattedTooltip(FancyMessage text){
-		for(MessagePart component : text.messageParts){
-			if(component.clickActionData != null && component.clickActionName != null){
+	public FancyMessage formattedTooltip(FancyMessage text) {
+		for (MessagePart component : text.messageParts) {
+			if (component.clickActionData != null && component.clickActionName != null) {
 				throw new IllegalArgumentException("The tooltip text cannot have click data.");
-			}else if(component.hoverActionData != null && component.hoverActionName != null){
+			} else if (component.hoverActionData != null && component.hoverActionName != null) {
 				throw new IllegalArgumentException("The tooltip text cannot have a tooltip.");
 			}
 		}
@@ -424,11 +445,12 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 	/**
 	 * Set the behavior of the current editing component to display the specified lines of formatted text when the client hovers over the text.
 	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 *
 	 * @param lines The lines of formatted text which will be displayed to the client upon hovering.
 	 * @return This builder instance.
 	 */
-	public FancyMessage formattedTooltip(FancyMessage... lines){
-		if(lines.length < 1){
+	public FancyMessage formattedTooltip(FancyMessage... lines) {
+		if (lines.length < 1) {
 			onHover(null, null); // Clear tooltip
 			return this;
 		}
@@ -436,54 +458,56 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 		FancyMessage result = new FancyMessage();
 		result.messageParts.clear(); // Remove the one existing text component that exists by default, which destabilizes the object
 
-		for(int i = 0; i < lines.length; i++){
-			try{
-				for(MessagePart component : lines[i]){
-					if(component.clickActionData != null && component.clickActionName != null){
+		for (int i = 0; i < lines.length; i++) {
+			try {
+				for (MessagePart component : lines[i]) {
+					if (component.clickActionData != null && component.clickActionName != null) {
 						throw new IllegalArgumentException("The tooltip text cannot have click data.");
-					}else if(component.hoverActionData != null && component.hoverActionName != null){
+					} else if (component.hoverActionData != null && component.hoverActionName != null) {
 						throw new IllegalArgumentException("The tooltip text cannot have a tooltip.");
 					}
-					if(component.hasText()){
+					if (component.hasText()) {
 						result.messageParts.add(component.clone());
 					}
 				}
-				if(i != lines.length - 1){
+				if (i != lines.length - 1) {
 					result.messageParts.add(new MessagePart(rawText("\n")));
 				}
 			} catch (CloneNotSupportedException e) {
 				Bukkit.getLogger().log(Level.WARNING, "Failed to clone object", e);
 				return this;
 			}
-		} 
+		}
 		return formattedTooltip(result.messageParts.isEmpty() ? null : result); // Throws NPE if size is 0, intended
 	}
 
 	/**
 	 * Set the behavior of the current editing component to display the specified lines of formatted text when the client hovers over the text.
 	 * <p>Tooltips do not inherit display characteristics, such as color and styles, from the message component on which they are applied.</p>
+	 *
 	 * @param lines The lines of text which will be displayed to the client upon hovering. The iteration order of this object will be the order in which the lines of the tooltip are created.
 	 * @return This builder instance.
 	 */
-	public FancyMessage formattedTooltip(final Iterable<FancyMessage> lines){
+	public FancyMessage formattedTooltip(final Iterable<FancyMessage> lines) {
 		return formattedTooltip(ArrayWrapper.toArray(lines, FancyMessage.class));
 	}
 
 	/**
 	 * If the text is a translatable key, and it has replaceable values, this function can be used to set the replacements that will be used in the message.
+	 *
 	 * @param replacements The replacements, in order, that will be used in the language-specific message.
 	 * @return This builder instance.
 	 */
-	public FancyMessage translationReplacements(final String... replacements){
-		for(String str : replacements){
+	public FancyMessage translationReplacements(final String... replacements) {
+		for (String str : replacements) {
 			latest().translationReplacements.add(new JsonString(str));
 		}
 		dirty = true;
-		
+
 		return this;
 	}
 	/*
-	
+
 	/**
 	 * If the text is a translatable key, and it has replaceable values, this function can be used to set the replacements that will be used in the message.
 	 * @param replacements The replacements, in order, that will be used in the language-specific message.
@@ -493,39 +517,42 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 		for(CharSequence str : replacements){
 			latest().translationReplacements.add(new JsonString(str));
 		}
-		
+
 		return this;
 	}
-	
+
 	*/
-	
+
 	/**
 	 * If the text is a translatable key, and it has replaceable values, this function can be used to set the replacements that will be used in the message.
+	 *
 	 * @param replacements The replacements, in order, that will be used in the language-specific message.
 	 * @return This builder instance.
 	 */
-	public FancyMessage translationReplacements(final FancyMessage... replacements){
-		for(FancyMessage str : replacements){
+	public FancyMessage translationReplacements(final FancyMessage... replacements) {
+		for (FancyMessage str : replacements) {
 			latest().translationReplacements.add(str);
 		}
-		
+
 		dirty = true;
-		
+
 		return this;
 	}
-	
+
 	/**
 	 * If the text is a translatable key, and it has replaceable values, this function can be used to set the replacements that will be used in the message.
+	 *
 	 * @param replacements The replacements, in order, that will be used in the language-specific message.
 	 * @return This builder instance.
 	 */
-	public FancyMessage translationReplacements(final Iterable<FancyMessage> replacements){		
+	public FancyMessage translationReplacements(final Iterable<FancyMessage> replacements) {
 		return translationReplacements(ArrayWrapper.toArray(replacements, FancyMessage.class));
 	}
-	
+
 	/**
 	 * Terminate construction of the current editing component, and begin construction of a new message component.
 	 * After a successful call to this method, all setter methods will refer to a new message component, created as a result of the call to this method.
+	 *
 	 * @param text The text which will populate the new message component.
 	 * @return This builder instance.
 	 */
@@ -536,6 +563,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 	/**
 	 * Terminate construction of the current editing component, and begin construction of a new message component.
 	 * After a successful call to this method, all setter methods will refer to a new message component, created as a result of the call to this method.
+	 *
 	 * @param text The text which will populate the new message component.
 	 * @return This builder instance.
 	 */
@@ -551,6 +579,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 	/**
 	 * Terminate construction of the current editing component, and begin construction of a new message component.
 	 * After a successful call to this method, all setter methods will refer to a new message component, created as a result of the call to this method.
+	 *
 	 * @return This builder instance.
 	 */
 	public FancyMessage then() {
@@ -563,7 +592,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 	}
 
 	@Override
-	public void writeJson(JsonWriter writer) throws IOException{
+	public void writeJson(JsonWriter writer) throws IOException {
 		if (messageParts.size() == 1) {
 			latest().writeJson(writer);
 		} else {
@@ -578,6 +607,7 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 	/**
 	 * Serialize this fancy message, converting it into syntactically-valid JSON using a {@link JsonWriter}.
 	 * This JSON should be compatible with vanilla formatter commands such as {@code /tellraw}.
+	 *
 	 * @return The JSON string representing this object.
 	 */
 	public String toJSONString() {
@@ -599,43 +629,44 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 
 	/**
 	 * Sends this message to a player. The player will receive the fully-fledged formatted display of this message.
+	 *
 	 * @param player The player who will receive the message.
 	 */
-	public void send(Player player){
+	public void send(Player player) {
 		send(player, toJSONString());
 	}
-        
-        private void send(CommandSender sender, String jsonString){
-                if (!(sender instanceof Player)){
-                        sender.sendMessage(toOldMessageFormat());
-                        return;
-                }
-                Player player = (Player) sender;
-                try {
+
+	private void send(CommandSender sender, String jsonString) {
+		if (!(sender instanceof Player)) {
+			sender.sendMessage(toOldMessageFormat());
+			return;
+		}
+		Player player = (Player) sender;
+		try {
 			Object handle = Reflection.getHandle(player);
 			Object connection = Reflection.getField(handle.getClass(), "playerConnection").get(handle);
 			Reflection.getMethod(connection.getClass(), "sendPacket", Reflection.getNMSClass("Packet")).invoke(connection, createChatPacket(jsonString));
 		} catch (IllegalArgumentException e) {
-                        Bukkit.getLogger().log(Level.WARNING, "Argument could not be passed.", e);
+			Bukkit.getLogger().log(Level.WARNING, "Argument could not be passed.", e);
 		} catch (IllegalAccessException e) {
-                        Bukkit.getLogger().log(Level.WARNING, "Could not access method.", e);
-                } catch (InstantiationException e) {
-                        Bukkit.getLogger().log(Level.WARNING, "Underlying class is abstract.", e);
-                } catch (InvocationTargetException e) {
-                        Bukkit.getLogger().log(Level.WARNING, "A error has occured durring invoking of method.", e);
-                } catch (NoSuchMethodException e) {
-                        Bukkit.getLogger().log(Level.WARNING, "Could not find method.", e);
-                } catch (ClassNotFoundException e) {
-                        Bukkit.getLogger().log(Level.WARNING, "Could not find class.", e);
-                }
-        }
+			Bukkit.getLogger().log(Level.WARNING, "Could not access method.", e);
+		} catch (InstantiationException e) {
+			Bukkit.getLogger().log(Level.WARNING, "Underlying class is abstract.", e);
+		} catch (InvocationTargetException e) {
+			Bukkit.getLogger().log(Level.WARNING, "A error has occured durring invoking of method.", e);
+		} catch (NoSuchMethodException e) {
+			Bukkit.getLogger().log(Level.WARNING, "Could not find method.", e);
+		} catch (ClassNotFoundException e) {
+			Bukkit.getLogger().log(Level.WARNING, "Could not find class.", e);
+		}
+	}
 
 	// The ChatSerializer's instance of Gson
 	private static Object nmsChatSerializerGsonInstance;
 	private static Method fromJsonMethod;
 
 	private Object createChatPacket(String json) throws IllegalArgumentException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException {
-		if(nmsChatSerializerGsonInstance == null){
+		if (nmsChatSerializerGsonInstance == null) {
 			// Find the field and its value, completely bypassing obfuscation
 			Class<?> chatSerializerClazz;
 
@@ -678,20 +709,22 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 	 * Sends this message to a command sender.
 	 * If the sender is a player, they will receive the fully-fledged formatted display of this message.
 	 * Otherwise, they will receive a version of this message with less formatting.
+	 *
 	 * @param sender The command sender who will receive the message.
 	 * @see #toOldMessageFormat()
 	 */
 	public void send(CommandSender sender) {
-                send(sender, toJSONString());
+		send(sender, toJSONString());
 	}
 
 	/**
 	 * Sends this message to multiple command senders.
+	 *
 	 * @param senders The command senders who will receive the message.
 	 * @see #send(CommandSender)
 	 */
 	public void send(final Iterable<? extends CommandSender> senders) {
-                String string = toJSONString();
+		String string = toJSONString();
 		for (final CommandSender sender : senders) {
 			send(sender, string);
 		}
@@ -711,13 +744,14 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 	 * </p>
 	 * <p>
 	 * Color and formatting can be removed from the returned string by using {@link ChatColor#stripColor(String)}.</p>
+	 *
 	 * @return A human-readable string representing limited formatting in addition to the core text of this message.
 	 */
 	public String toOldMessageFormat() {
 		StringBuilder result = new StringBuilder();
 		for (MessagePart part : this) {
 			result.append(part.color == null ? "" : part.color);
-			for(ChatColor formatSpecifier : part.styles){
+			for (ChatColor formatSpecifier : part.styles) {
 				result.append(formatSpecifier);
 			}
 			result.append(part.text);
@@ -755,12 +789,13 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 	 * Deserializes a JSON-represented message from a mapping of key-value pairs.
 	 * This is called by the Bukkit serialization API.
 	 * It is not intended for direct public API consumption.
+	 *
 	 * @param serialized The key-value mapping which represents a fancy message.
 	 */
 	@SuppressWarnings("unchecked")
-	public static FancyMessage deserialize(Map<String, Object> serialized){
+	public static FancyMessage deserialize(Map<String, Object> serialized) {
 		FancyMessage msg = new FancyMessage();
-		msg.messageParts = (List<MessagePart>)serialized.get("messageParts");
+		msg.messageParts = (List<MessagePart>) serialized.get("messageParts");
 		msg.jsonString = serialized.containsKey("JSON") ? serialized.get("JSON").toString() : null;
 		msg.dirty = !serialized.containsKey("JSON");
 		return msg;
@@ -772,68 +807,69 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 	public Iterator<MessagePart> iterator() {
 		return messageParts.iterator();
 	}
-	
+
 	private static JsonParser _stringParser = new JsonParser();
-	
+
 	/**
 	 * Deserializes a fancy message from its JSON representation. This JSON representation is of the format of
 	 * that returned by {@link #toJSONString()}, and is compatible with vanilla inputs.
+	 *
 	 * @param json The JSON string which represents a fancy message.
 	 * @return A {@code FancyMessage} representing the parameterized JSON message.
 	 */
-	public static FancyMessage deserialize(String json){
+	public static FancyMessage deserialize(String json) {
 		JsonObject serialized = _stringParser.parse(json).getAsJsonObject();
 		JsonArray extra = serialized.getAsJsonArray("extra"); // Get the extra component
 		FancyMessage returnVal = new FancyMessage();
 		returnVal.messageParts.clear();
-		for(JsonElement mPrt : extra){
+		for (JsonElement mPrt : extra) {
 			MessagePart component = new MessagePart();
 			JsonObject messagePart = mPrt.getAsJsonObject();
-			for(Map.Entry<String, JsonElement> entry : messagePart.entrySet()){
+			for (Map.Entry<String, JsonElement> entry : messagePart.entrySet()) {
 				// Deserialize text
-				if(TextualComponent.isTextKey(entry.getKey())){
+				if (TextualComponent.isTextKey(entry.getKey())) {
 					// The map mimics the YAML serialization, which has a "key" field and one or more "value" fields
 					Map<String, Object> serializedMapForm = new HashMap<String, Object>(); // Must be object due to Bukkit serializer API compliance
 					serializedMapForm.put("key", entry.getKey());
-					if(entry.getValue().isJsonPrimitive()){
+					if (entry.getValue().isJsonPrimitive()) {
 						// Assume string
 						serializedMapForm.put("value", entry.getValue().getAsString());
-					}else{
+					} else {
 						// Composite object, but we assume each element is a string
-						for(Map.Entry<String, JsonElement> compositeNestedElement : entry.getValue().getAsJsonObject().entrySet()){
+						for (Map.Entry<String, JsonElement> compositeNestedElement : entry.getValue().getAsJsonObject().entrySet()) {
 							serializedMapForm.put("value." + compositeNestedElement.getKey(), compositeNestedElement.getValue().getAsString());
 						}
 					}
 					component.text = TextualComponent.deserialize(serializedMapForm);
-				}else if(MessagePart.stylesToNames.inverse().containsKey(entry.getKey())){
-					if(entry.getValue().getAsBoolean()){
+				} else if (MessagePart.stylesToNames.inverse().containsKey(entry.getKey())) {
+					if (entry.getValue().getAsBoolean()) {
 						component.styles.add(MessagePart.stylesToNames.inverse().get(entry.getKey()));
 					}
-				}else if(entry.getKey().equals("color")){
+				} else if (entry.getKey().equals("color")) {
 					component.color = ChatColor.valueOf(entry.getValue().getAsString().toUpperCase());
-				}else if(entry.getKey().equals("clickEvent")){
+				} else if (entry.getKey().equals("clickEvent")) {
 					JsonObject object = entry.getValue().getAsJsonObject();
 					component.clickActionName = object.get("action").getAsString();
 					component.clickActionData = object.get("value").getAsString();
-				}else if(entry.getKey().equals("hoverEvent")){
+				} else if (entry.getKey().equals("hoverEvent")) {
 					JsonObject object = entry.getValue().getAsJsonObject();
 					component.hoverActionName = object.get("action").getAsString();
-					if(object.get("value").isJsonPrimitive()){
+					if (object.get("value").isJsonPrimitive()) {
 						// Assume string
 						component.hoverActionData = new JsonString(object.get("value").getAsString());
-					}else{
+					} else {
 						// Assume composite type
 						// The only composite type we currently store is another FancyMessage
 						// Therefore, recursion time!
 						component.hoverActionData = deserialize(object.get("value").toString() /* This should properly serialize the JSON object as a JSON string */);
 					}
-				}else if(entry.getKey().equals("insertion")){
+				} else if (entry.getKey().equals("insertion")) {
 					component.insertionData = entry.getValue().getAsString();
-				}else if(entry.getKey().equals("with")){
-					for(JsonElement object : entry.getValue().getAsJsonArray()){
-						if(object.isJsonPrimitive()){
+				} else if (entry.getKey().equals("with")) {
+					for (JsonElement object : entry.getValue().getAsJsonArray()) {
+						if (object.isJsonPrimitive()) {
 							component.translationReplacements.add(new JsonString(object.getAsString()));
-						}else{
+						} else {
 							// Only composite type stored in this array is - again - FancyMessages
 							// Recurse within this function to parse this as a translation replacement
 							component.translationReplacements.add(deserialize(object.toString()));
@@ -845,4 +881,5 @@ public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<
 		}
 		return returnVal;
 	}
+
 }
