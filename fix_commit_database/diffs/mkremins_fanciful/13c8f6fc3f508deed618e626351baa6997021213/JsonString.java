@@ -15,8 +15,8 @@ import org.bukkit.configuration.serialization.ConfigurationSerializable;
 final class JsonString implements JsonRepresentedObject, ConfigurationSerializable {
 
 	private String _value;
-	
-	public JsonString(CharSequence value){
+
+	public JsonString(CharSequence value) {
 		_value = value == null ? null : value.toString();
 	}
 
@@ -24,8 +24,8 @@ final class JsonString implements JsonRepresentedObject, ConfigurationSerializab
 	public void writeJson(JsonWriter writer) throws IOException {
 		writer.value(getValue());
 	}
-	
-	public String getValue(){
+
+	public String getValue() {
 		return _value;
 	}
 
@@ -34,13 +34,14 @@ final class JsonString implements JsonRepresentedObject, ConfigurationSerializab
 		theSingleValue.put("stringValue", _value);
 		return theSingleValue;
 	}
-	
-	public static JsonString deserialize(Map<String, Object> map){
+
+	public static JsonString deserialize(Map<String, Object> map) {
 		return new JsonString(map.get("stringValue").toString());
 	}
-	
+
 	@Override
-	public String toString(){
+	public String toString() {
 		return _value;
 	}
+
 }
