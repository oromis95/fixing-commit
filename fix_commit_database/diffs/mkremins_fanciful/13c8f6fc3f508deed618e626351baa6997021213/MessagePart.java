@@ -1,19 +1,18 @@
 package mkremins.fanciful;
 
-import java.util.ArrayList;
-import java.util.HashMap;
-import java.util.Map;
-
+import com.google.common.collect.BiMap;
+import com.google.common.collect.ImmutableBiMap;
 import com.google.gson.stream.JsonWriter;
+import org.bukkit.Bukkit;
 import org.bukkit.ChatColor;
 import org.bukkit.configuration.serialization.ConfigurationSerializable;
 import org.bukkit.configuration.serialization.ConfigurationSerialization;
 
-import com.google.common.collect.BiMap;
-import com.google.common.collect.ImmutableBiMap;
 import java.io.IOException;
+import java.util.ArrayList;
+import java.util.HashMap;
+import java.util.Map;
 import java.util.logging.Level;
-import org.bukkit.Bukkit;
 
 /**
  * Internal class: Represents a component of a JSON-serializable {@link FancyMessage}.
@@ -22,14 +21,13 @@ final class MessagePart implements JsonRepresentedObject, ConfigurationSerializa
 
 	ChatColor color = ChatColor.WHITE;
 	ArrayList<ChatColor> styles = new ArrayList<ChatColor>();
-	String clickActionName = null, clickActionData = null,
-			hoverActionName = null;
+	String clickActionName = null, clickActionData = null, hoverActionName = null;
 	JsonRepresentedObject hoverActionData = null;
 	TextualComponent text = null;
 	String insertionData = null;
 	ArrayList<JsonRepresentedObject> translationReplacements = new ArrayList<JsonRepresentedObject>();
 
-	MessagePart(final TextualComponent text){
+	MessagePart(final TextualComponent text) {
 		this.text = text;
 	}
 
@@ -41,40 +39,43 @@ final class MessagePart implements JsonRepresentedObject, ConfigurationSerializa
 		return text != null;
 	}
 
-        @Override
+	@Override
 	@SuppressWarnings("unchecked")
-	public MessagePart clone() throws CloneNotSupportedException{
-		MessagePart obj = (MessagePart)super.clone();
-		obj.styles = (ArrayList<ChatColor>)styles.clone();
-		if(hoverActionData instanceof JsonString){
-			obj.hoverActionData = new JsonString(((JsonString)hoverActionData).getValue());
-		}else if(hoverActionData instanceof FancyMessage){
-			obj.hoverActionData = ((FancyMessage)hoverActionData).clone();
+	public MessagePart clone() throws CloneNotSupportedException {
+		MessagePart obj = (MessagePart) super.clone();
+		obj.styles = (ArrayList<ChatColor>) styles.clone();
+		if (hoverActionData instanceof JsonString) {
+			obj.hoverActionData = new JsonString(((JsonString) hoverActionData).getValue());
+		} else if (hoverActionData instanceof FancyMessage) {
+			obj.hoverActionData = ((FancyMessage) hoverActionData).clone();
 		}
-		obj.translationReplacements = (ArrayList<JsonRepresentedObject>)translationReplacements.clone();
+		obj.translationReplacements = (ArrayList<JsonRepresentedObject>) translationReplacements.clone();
 		return obj;
 
 	}
 
 	static final BiMap<ChatColor, String> stylesToNames;
 
-	static{
+	static {
 		ImmutableBiMap.Builder<ChatColor, String> builder = ImmutableBiMap.builder();
-		for (final ChatColor style : ChatColor.values()){
-			if(!style.isFormat()){
+		for (final ChatColor style : ChatColor.values()) {
+			if (!style.isFormat()) {
 				continue;
 			}
 
 			String styleName;
 			switch (style) {
-			case MAGIC:
-				styleName = "obfuscated"; break;
-			case UNDERLINE:
-				styleName = "underlined"; break;
-			default:
-				styleName = style.name().toLowerCase(); break;
+				case MAGIC:
+					styleName = "obfuscated";
+					break;
+				case UNDERLINE:
+					styleName = "underlined";
+					break;
+				default:
+					styleName = style.name().toLowerCase();
+					break;
 			}
-			
+
 			builder.put(style, styleName);
 		}
 		stylesToNames = builder.build();
@@ -90,31 +91,31 @@ final class MessagePart implements JsonRepresentedObject, ConfigurationSerializa
 			}
 			if (clickActionName != null && clickActionData != null) {
 				json.name("clickEvent")
-				.beginObject()
-				.name("action").value(clickActionName)
-				.name("value").value(clickActionData)
-				.endObject();
+						.beginObject()
+						.name("action").value(clickActionName)
+						.name("value").value(clickActionData)
+						.endObject();
 			}
 			if (hoverActionName != null && hoverActionData != null) {
 				json.name("hoverEvent")
-				.beginObject()
-				.name("action").value(hoverActionName)
-				.name("value");
+						.beginObject()
+						.name("action").value(hoverActionName)
+						.name("value");
 				hoverActionData.writeJson(json);
 				json.endObject();
 			}
-			if(insertionData != null){
+			if (insertionData != null) {
 				json.name("insertion").value(insertionData);
 			}
-			if(translationReplacements.size() > 0 && text != null && TextualComponent.isTranslatableText(text)){
+			if (translationReplacements.size() > 0 && text != null && TextualComponent.isTranslatableText(text)) {
 				json.name("with").beginArray();
-				for(JsonRepresentedObject obj : translationReplacements){
+				for (JsonRepresentedObject obj : translationReplacements) {
 					obj.writeJson(json);
 				}
 				json.endArray();
 			}
 			json.endObject();
-		} catch(IOException e){
+		} catch (IOException e) {
 			Bukkit.getLogger().log(Level.WARNING, "A problem occured during writing of JSON string", e);
 		}
 	}
@@ -134,20 +135,20 @@ final class MessagePart implements JsonRepresentedObject, ConfigurationSerializa
 	}
 
 	@SuppressWarnings("unchecked")
-	public static MessagePart deserialize(Map<String, Object> serialized){
-		MessagePart part = new MessagePart((TextualComponent)serialized.get("text"));
-		part.styles = (ArrayList<ChatColor>)serialized.get("styles");
+	public static MessagePart deserialize(Map<String, Object> serialized) {
+		MessagePart part = new MessagePart((TextualComponent) serialized.get("text"));
+		part.styles = (ArrayList<ChatColor>) serialized.get("styles");
 		part.color = ChatColor.getByChar(serialized.get("color").toString());
-		part.hoverActionName = (String)serialized.get("hoverActionName");
-		part.hoverActionData = (JsonRepresentedObject)serialized.get("hoverActionData");
-		part.clickActionName = (String)serialized.get("clickActionName");
-		part.clickActionData = (String)serialized.get("clickActionData");
-		part.insertionData = (String)serialized.get("insertion");
-		part.translationReplacements = (ArrayList<JsonRepresentedObject>)serialized.get("translationReplacements");
+		part.hoverActionName = (String) serialized.get("hoverActionName");
+		part.hoverActionData = (JsonRepresentedObject) serialized.get("hoverActionData");
+		part.clickActionName = (String) serialized.get("clickActionName");
+		part.clickActionData = (String) serialized.get("clickActionData");
+		part.insertionData = (String) serialized.get("insertion");
+		part.translationReplacements = (ArrayList<JsonRepresentedObject>) serialized.get("translationReplacements");
 		return part;
 	}
 
-	static{
+	static {
 		ConfigurationSerialization.registerClass(MessagePart.class);
 	}
 
