@@ -19,7 +19,7 @@
  * along with gstreamer-java.  If not, see <http://www.gnu.org/licenses/>.
  */
 
-package org.freedekstop.gstreamer;
+package org.freedesktop.gstreamer;
 
 import org.freedesktop.gstreamer.Gst;
 import static org.junit.Assert.assertEquals;
