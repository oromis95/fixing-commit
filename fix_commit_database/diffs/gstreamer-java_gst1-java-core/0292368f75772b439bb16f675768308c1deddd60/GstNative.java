@@ -33,7 +33,7 @@ public final class GstNative {
     // gstreamer library names the files one of libfoo-0.10 and libfoo-1.0
     // private static String[] nameFormats = { /*"%s-0.10",*/ "%s-1.0" };
     private final static String[] nameFormats =
-            System.getProperty("gstreamer.GstNative.nameFormats", "%s-1.0|%s-1.0.0").split("\\|");
+            System.getProperty("gstreamer.GstNative.nameFormats", "%s-1.0").split("\\|");
 
     private GstNative() {}
     
