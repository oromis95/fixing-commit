@@ -27,6 +27,7 @@ import org.freedesktop.gstreamer.lowlevel.annotations.FreeReturnValue;
 import org.freedesktop.gstreamer.lowlevel.annotations.Invalidate;
 
 import com.sun.jna.Pointer;
+import com.sun.jna.PointerType;
 import java.util.Arrays;
 import java.util.List;
 
@@ -46,9 +47,6 @@ public interface GstCapsAPI extends com.sun.jna.Library {
     @CallerOwnsReturn Caps gst_caps_new_simple(String media_type, String fieldName, Object... args);
     @CallerOwnsReturn Caps gst_caps_new_full(Structure... data);
     
-    Pointer gst_caps_ref(Caps caps);
-    void gst_caps_unref(Caps caps);
-    void gst_caps_unref(Pointer caps);
     @CallerOwnsReturn Pointer ptr_gst_caps_copy(Caps caps);
     @CallerOwnsReturn Pointer ptr_gst_caps_from_string(String string);
     @CallerOwnsReturn Caps gst_caps_copy(Caps caps);
@@ -58,7 +56,7 @@ public interface GstCapsAPI extends com.sun.jna.Library {
     
     /* manipulation */
     void gst_caps_append(Caps caps1, @Invalidate Caps caps2);
-    @CallerOwnsReturn Caps gst_caps_merge(Caps caps1, @Invalidate Caps caps2);
+    @CallerOwnsReturn Caps gst_caps_merge(@Invalidate Caps caps1, @Invalidate Caps caps2);
     void gst_caps_append_structure(Caps caps, @Invalidate Structure structure);
     void gst_caps_remove_structure(Caps caps, int idx);
     void gst_caps_merge_structure(Caps caps, @Invalidate Structure structure);
@@ -66,14 +64,13 @@ public interface GstCapsAPI extends com.sun.jna.Library {
     Structure gst_caps_get_structure(Caps caps, int index);
     @CallerOwnsReturn Structure gst_caps_steal_structure(Caps caps, int index);
     @CallerOwnsReturn Caps gst_caps_copy_nth(Caps caps, int nth);
-    void gst_caps_truncate(Caps caps);
+    @CallerOwnsReturn Caps gst_caps_truncate(Caps caps);
     void gst_caps_set_simple(Caps caps, String field, Object... values);
     /* operations */
     @CallerOwnsReturn Caps gst_caps_intersect( Caps caps1,  Caps caps2);
     @CallerOwnsReturn Caps gst_caps_subtract( Caps minuend,  Caps subtrahend);
-    @CallerOwnsReturn Caps gst_caps_union( Caps caps1,  Caps caps2);
     @CallerOwnsReturn Caps gst_caps_normalize( Caps caps);
-    boolean gst_caps_simplify(Caps caps);
+    @CallerOwnsReturn Caps gst_caps_simplify(Caps caps);
     @FreeReturnValue String gst_caps_to_string(Caps caps);
     /* tests */
 
@@ -85,60 +82,6 @@ public interface GstCapsAPI extends com.sun.jna.Library {
     boolean gst_caps_is_equal(Caps caps1,  Caps caps2);
     boolean gst_caps_is_equal_fixed(Caps caps1,  Caps caps2);
     boolean gst_caps_can_intersect(Caps caps1, Caps caps2);
-
-    public static final class GPtrArray extends com.sun.jna.Structure {
-    	public volatile Pointer pdata;
-    	public volatile int     len;    	
-
-        @Override
-        protected List<String> getFieldOrder() {
-            return Arrays.asList(new String[]{
-                "pdata", "len"
-            });
-        }
-    }
-    public static final class GstCapsStruct extends com.sun.jna.Structure {
-
-        public volatile GType type;
-        public volatile int refcount;
-
-        /*< public >*/ /* read only */
-
-        public volatile int flags;
-
-        /*< private >*/
-        public volatile GPtrArray structs;
-
-        /*< private >*/
-        public volatile byte[] _gst_reserved = new byte[Pointer.SIZE * GstAPI.GST_PADDING];
-
-        @Override
-        protected List<String> getFieldOrder() {
-            return Arrays.asList(new String[]{
-                "type", "refcount", "flags",
-                "structs", "_gst_reserved"
-            });
-        }
-    }
-
-    
-    public static final class GstStaticCapsStruct extends com.sun.jna.Structure {
-    	/*< public >*/
-        public volatile GstCapsStruct caps;
-        public volatile String string;
-
-        /*< private >*/
-        public volatile byte[] _gst_reserved = new byte[Pointer.SIZE * GstAPI.GST_PADDING];
-
-        @Override
-        protected List<String> getFieldOrder() {
-            return Arrays.asList(new String[]{
-                "caps", "string", "_gst_reserved"
-            });
-        }
-    }
    
     GType gst_static_caps_get_type();
-    /* static_caps_get returns a static Caps reference - do not deref */
-    Caps gst_static_caps_get(GstStaticCapsStruct static_caps);
 }
