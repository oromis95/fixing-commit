@@ -1,4 +1,5 @@
 /* 
+ * Copyright (c) 2019 Neil C Smith
  * Copyright (c) 2007 Wayne Meissner
  * 
  * This file is part of gstreamer-java.
@@ -15,12 +16,18 @@
  * You should have received a copy of the GNU Lesser General Public License
  * version 3 along with this work.  If not, see <http://www.gnu.org/licenses/>.
  */
+package org.freedesktop.gstreamer;
 
-package org.freedesktop.gstreamer.event;
-
-import org.freedesktop.gstreamer.BusSyncReply;
 import org.freedesktop.gstreamer.message.Message;
 
+/**
+ * A BusSyncHandler will be invoked synchronously, when a new message has been
+ * injected into a {@link Bus}. This function is mostly used internally. Only
+ * one sync handler can be attached to a given bus.
+ *
+ * @see Bus#setSyncHandler(org.freedesktop.gstreamer.BusSyncHandler)
+ */
 public interface BusSyncHandler {
+
     public BusSyncReply syncMessage(Message message);
 }
