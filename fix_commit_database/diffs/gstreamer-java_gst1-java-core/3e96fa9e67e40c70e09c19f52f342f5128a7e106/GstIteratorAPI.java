@@ -1,4 +1,5 @@
 /* 
+ * Copyright (c) 2016 Neil C Smith
  * Copyright (c) 2009 Levente Farkas
  * Copyright (c) 2007, 2008 Wayne Meissner
  * 
@@ -20,7 +21,6 @@
 package org.freedesktop.gstreamer.lowlevel;
 
 import com.sun.jna.Pointer;
-import com.sun.jna.ptr.PointerByReference;
 
 /**
  * GstIterator functions
@@ -29,6 +29,6 @@ public interface GstIteratorAPI extends com.sun.jna.Library {
     GstIteratorAPI GSTITERATOR_API = GstNative.load(GstIteratorAPI.class);
 
     void gst_iterator_free(Pointer iter);
-    int gst_iterator_next(Pointer iter, PointerByReference next);
+    int gst_iterator_next(Pointer iter, GValueAPI.GValue next);
     void gst_iterator_resync(Pointer iter);
 }
