@@ -36,46 +36,135 @@ import org.freedesktop.gstreamer.lowlevel.annotations.DefaultEnumValue;
 public enum EventType implements IntegerEnum {
     /** Unknown event */
     @DefaultEnumValue UNKNOWN(0, 0),
-    /** Start a flush operation */
-    FLUSH_START(1, Flags.BOTH),
-    /** Stop a flush operation */
-    FLUSH_STOP(2, Flags.BOTH | Flags.SERIALIZED),
-    /**
-     * End-Of-Stream. No more data is to be expected to follow without a 
-     * NEWSEGMENT event.
-     */
-    EOS(5, Flags.DOWNSTREAM | Flags.SERIALIZED),
-    /** A new media segment follows in the dataflow */
-    NEWSEGMENT(6, Flags.DOWNSTREAM | Flags.SERIALIZED),
-    /** A new set of metadata tags has been found in the stream */
-    TAG(7, Flags.DOWNSTREAM | Flags.SERIALIZED),
-    /** Notification of buffering requirements */
-    BUFFERSIZE(8, Flags.DOWNSTREAM | Flags.SERIALIZED),
-    /** 
-     * A quality message. Used to indicate to upstream elements that the 
-     * downstream elements are being starved of or flooded with data.
-     */
-    QOS(15, Flags.UPSTREAM),
-    /** A request for a new playback position and rate. */
-    SEEK(16, Flags.UPSTREAM),
-    /**
-     * Navigation events are usually used for communicating user requests, such 
+
+    /* bidirectional events */
+    /**
+     * Start a flush operation. This event clears all data from the pipeline and
+     * unblock all streaming threads.
+     */
+    FLUSH_START(10, Flags.BOTH),
+    /**
+     * Stop a flush operation. This event resets the running-time of the
+     * pipeline.
+     */
+    FLUSH_STOP(20, Flags.BOTH | Flags.SERIALIZED),
+
+    /* downstream serialized events */
+    /**
+     * Event to mark the start of a new stream. Sent before any other serialized
+     * event and only sent at the start of a new stream, not after flushing
+     * seeks.
+     */
+    STREAM_START(40, Flags.DOWNSTREAM | Flags.SERIALIZED | Flags.STICKY),
+    /**
+     * #GstCaps event. Notify the pad of a new media type.
+     */
+    CAPS(50, Flags.DOWNSTREAM | Flags.SERIALIZED | Flags.STICKY),
+    /**
+     * A new media segment follows in the dataflow. The segment events contains
+     * information for clipping buffers and converting buffer timestamps to
+     * running-time and stream-time.
+     */
+    SEGMENT(70, Flags.DOWNSTREAM | Flags.SERIALIZED | Flags.STICKY),
+    /**
+     * A new set of metadata tags has been found in the stream.
+     */
+    TAG(80, Flags.DOWNSTREAM | Flags.SERIALIZED | Flags.STICKY | Flags.STICKY_MULTI),
+    /**
+     * Notification of buffering requirements. Currently not used yet.
+     */
+    BUFFERSIZE(90, Flags.DOWNSTREAM | Flags.SERIALIZED | Flags.STICKY),
+    /**
+     * An event that sinks turn into a message. Used to send messages that
+     * should be emitted in sync with rendering.
+     */
+    SINK_MESSAGE(100, Flags.DOWNSTREAM | Flags.SERIALIZED | Flags.STICKY | Flags.STICKY_MULTI),
+    /**
+     * End-Of-Stream. No more data is to be expected to follow without a SEGMENT
+     * event.
+     */
+    EOS(110, Flags.DOWNSTREAM | Flags.SERIALIZED | Flags.STICKY),
+    /**
+     * An event which indicates that a new table of contents (TOC) was found or
+     * updated.
+     */
+    TOC(120, Flags.DOWNSTREAM | Flags.SERIALIZED | Flags.STICKY | Flags.STICKY_MULTI),
+    /**
+     * An event which indicates that new or updated encryption information has
+     * been found in the stream.
+     */
+    PROTECTION(130, Flags.DOWNSTREAM | Flags.SERIALIZED | Flags.STICKY | Flags.STICKY_MULTI),
+
+    /* non-sticky downstream serialized */
+    /**
+     * Marks the end of a segment playback.
+     */
+    SEGMENT_DONE(150, Flags.DOWNSTREAM | Flags.SERIALIZED),
+    /**
+     * Marks a gap in the datastream.
+     */
+    GAP(160, Flags.DOWNSTREAM | Flags.SERIALIZED),
+
+    /* upstream events */
+    /**
+     * A quality message. Used to indicate to upstream elements that the
+     * downstream elements should adjust their processing rate.
+     */
+    QOS(190, Flags.UPSTREAM),
+    /**
+     * A request for a new playback position and rate.
+     */
+    SEEK(200, Flags.UPSTREAM),
+    /**
+     * Navigation events are usually used for communicating user requests, such
      * as mouse or keyboard movements, to upstream elements.
      */
-    NAVIGATION(17, Flags.UPSTREAM),
-    /** Notification of new latency adjustment */
-    LATENCY(18, Flags.UPSTREAM),
+    NAVIGATION(210, Flags.UPSTREAM),
+    /**
+     * Notification of new latency adjustment. Sinks will use the latency
+     * information to adjust their synchronisation.
+     */
+    LATENCY(220, Flags.UPSTREAM),
+    /**
+     * A request for stepping through the media. Sinks will usually execute the
+     * step operation.
+     */
+    STEP(230, Flags.UPSTREAM),
+    /**
+     * A request for upstream renegotiating caps and reconfiguring.
+     */
+    RECONFIGURE(240, Flags.UPSTREAM),
+    /**
+     * A request for a new playback position based on TOC entry's UID.
+     */
+    TOC_SELECT(250, Flags.UPSTREAM),
 
-    /** Upstream custom event */
-    CUSTOM_UPSTREAM(32, Flags.UPSTREAM),
-    /** Downstream custom event that travels in the data flow. */
-    CUSTOM_DOWNSTREAM(32, Flags.DOWNSTREAM | Flags.SERIALIZED),
-    /** Custom out-of-band downstream event. */
-    CUSTOM_DOWNSTREAM_OOB(32, Flags.DOWNSTREAM),
-    /** Custom upstream or downstream event.  In-band when travelling downstream.*/
-    CUSTOM_BOTH(32, Flags.BOTH | Flags.SERIALIZED),
-    /** Custom upstream or downstream out-of-band event. */
-    CUSTOM_BOTH_OOB(32, Flags.BOTH);
+    /* custom events start here */
+    /**
+     * Upstream custom event
+     */
+    CUSTOM_UPSTREAM(270, Flags.UPSTREAM),
+    /**
+     * Downstream custom event that travels in the data flow.
+     */
+    CUSTOM_DOWNSTREAM(280, Flags.DOWNSTREAM | Flags.SERIALIZED),
+    /**
+     * Custom out-of-band downstream event.
+     */
+    CUSTOM_DOWNSTREAM_OOB(290, Flags.DOWNSTREAM),
+    /**
+     * Custom sticky downstream event.
+     */
+    CUSTOM_DOWNSTREAM_STICKY(300, Flags.DOWNSTREAM | Flags.SERIALIZED | Flags.STICKY | Flags.STICKY_MULTI),
+    /**
+     * Custom upstream or downstream event. In-band when travelling downstream.
+     */
+    CUSTOM_BOTH(310, Flags.BOTH | Flags.SERIALIZED),
+    /**
+     * Custom upstream or downstream out-of-band event.
+     */
+    CUSTOM_BOTH_OOB(320, Flags.BOTH)
+    ;
     
     private static interface API extends com.sun.jna.Library {
         String gst_event_type_get_name(EventType type);
@@ -99,11 +188,13 @@ public enum EventType implements IntegerEnum {
         return EnumMapper.getInstance().valueOf(type, EventType.class);
     }
     
-    private static final int SHIFT = 4;
+    private static final int SHIFT = 8;
     private static final class Flags {
-        public static final int UPSTREAM = 1 << 0;
-        public static final int DOWNSTREAM	= 1 << 1;
-        public static final int SERIALIZED	= 1 << 2;
+        public static final int UPSTREAM       = 1 << 0;
+        public static final int DOWNSTREAM     = 1 << 1;
+        public static final int SERIALIZED     = 1 << 2;
+        public static final int STICKY         = 1 << 3;
+        public static final int STICKY_MULTI   = 1 << 4;
         public static final int BOTH = UPSTREAM | DOWNSTREAM;
     }
     private final int value;
