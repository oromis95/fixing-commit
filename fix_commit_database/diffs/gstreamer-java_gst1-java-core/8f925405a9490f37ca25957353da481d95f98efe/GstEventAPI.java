@@ -33,6 +33,7 @@ import com.sun.jna.Pointer;
 import com.sun.jna.ptr.PointerByReference;
 import java.util.Arrays;
 import java.util.List;
+import org.freedesktop.gstreamer.QOSType;
 
 /**
  * GstEvent functions
@@ -52,48 +53,60 @@ public interface GstEventAPI extends com.sun.jna.Library {
 
     /* flush events */
     @CallerOwnsReturn Event gst_event_new_flush_start();
+    Pointer ptr_gst_event_new_flush_start();
     @CallerOwnsReturn Event gst_event_new_flush_stop();
+    Pointer ptr_gst_event_new_flush_stop();
     /* EOS event */
     @CallerOwnsReturn Event gst_event_new_eos();
+    Pointer ptr_gst_event_new_eos();
     
 
     /* newsegment events */
-    @CallerOwnsReturn Event gst_event_new_new_segment( boolean update,  double rate,
-             Format format, long start,  long stop, long position);
-    @CallerOwnsReturn Event gst_event_new_new_segment_full(boolean update, double rate,
-             double applied_rate, Format format, long start, long stop, long position);
-    void gst_event_parse_new_segment(Event event, boolean[] update, double[] rate,
-            Format[] format, long[] start, long[] stop, long[] position);
-    void gst_event_parse_new_segment_full(Event event, boolean[] update, double[] rate,
-            double[] applied_rate, Format[] format, long[] start, long[] stop, long[] position);
-
+    @CallerOwnsReturn Event gst_event_new_segment( GstAPI.GstSegmentStruct segmentStruct );
+    Pointer ptr_gst_event_new_segment( GstAPI.GstSegmentStruct segment);            
+    void gst_event_parse_segment(Event event, Pointer[] pointer);
+    
     /* tag event */
     @CallerOwnsReturn Event gst_event_new_tag(@Invalidate TagList taglist);
+    Pointer ptr_gst_event_new_tag(@Invalidate TagList taglist);
     void gst_event_parse_tag(Event event, PointerByReference taglist);
-
+    void gst_event_parse_tag(Event event, Pointer[] taglist);
+    
     /* buffer */
     @CallerOwnsReturn Event gst_event_new_buffer_size(Format format, long minsize, long maxsize, boolean async);
-    void gst_event_parse_buffer_size(Event event, Format format, long[] minsize,
-						 long[] maxsize, int[] async);
-
+    Pointer ptr_gst_event_new_buffer_size(Format format, long minsize, long maxsize, boolean async);
+    void gst_event_parse_buffer_size(Event event, Format[] format, long[] minsize,
+						 long[] maxsize, boolean[] async);
+    
     /* QOS events */
-    @CallerOwnsReturn Event gst_event_new_qos(double proportion, long diff, ClockTime timestamp);
-    @CallerOwnsReturn Event gst_event_new_qos(double proportion, long diff, long timestamp);
-    void gst_event_parse_qos(Event event, double[] proportion, long[] diff, long[] timestamp);
+    @CallerOwnsReturn Event gst_event_new_qos(QOSType type, double proportion, long diff, ClockTime timestamp);
+    @CallerOwnsReturn Event gst_event_new_qos(QOSType type, double proportion, long diff, long timestamp);
+    Pointer ptr_gst_event_new_qos(QOSType type, double proportion, long diff, ClockTime timestamp);
+    void gst_event_parse_qos(Event event, QOSType[] type, double[] proportion, long[] diff, ClockTime[] timestamp);
+    void gst_event_parse_qos(Event event, QOSType[] type, double[] proportion, long[] diff, long[] timestamp);
+    
     /* seek event */
     @CallerOwnsReturn Event gst_event_new_seek(double rate, Format format, int flags,
 						 SeekType start_type, long start,
 						 SeekType stop_type, long stop);
-    void gst_event_parse_seek(Event event, double rate, Format[] format,
-		                                 int[] flags, int[] start_type, long[] start,
-                                                 int[] stop_type, long[] stop);
+    Pointer ptr_gst_event_new_seek(double rate, Format format, int flags,
+            SeekType start_type, long start, SeekType stop_type, long stop);
+    void gst_event_parse_seek(Event event, double[] rate, Format[] format,
+		                                 int[] flags, SeekType[] start_type, long[] start,
+                                                 SeekType[] stop_type, long[] stop);
+        
     /* navigation event */
     @CallerOwnsReturn Event gst_event_new_navigation(@Invalidate Structure structure);
+    Pointer ptr_gst_event_new_navigation(@Invalidate Structure structure);
 
     /* latency event */
     @CallerOwnsReturn Event gst_event_new_latency(ClockTime latency);
+    Pointer ptr_gst_event_new_latency(ClockTime latency);
     void gst_event_parse_latency(Event event, ClockTime[] latency);
     
+    /* step event */
+    Pointer ptr_gst_event_new_step(Format format, long amount, double rate, boolean flush, boolean intermediate);
+    
     public static final class EventStruct extends com.sun.jna.Structure {
         public volatile GstMiniObjectAPI.MiniObjectStruct mini_object;
         public volatile int type;
