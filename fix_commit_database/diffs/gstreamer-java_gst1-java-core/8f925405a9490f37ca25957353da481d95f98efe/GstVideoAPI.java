@@ -20,10 +20,20 @@ import org.freedesktop.gstreamer.Pad;
 import org.freedesktop.gstreamer.lowlevel.GValueAPI.GValue;
 
 import com.sun.jna.Library;
+import com.sun.jna.Pointer;
+import org.freedesktop.gstreamer.ClockTime;
 
 public interface GstVideoAPI extends Library {
 	public final static GstVideoAPI GSTVIDEO_API = GstNative.load("gstvideo", GstVideoAPI.class);
 
     GValue gst_video_frame_rate(Pad pad);
     boolean gst_video_get_size(Pad pad, int [] width, int [] height);
+    
+    /* */
+    Pointer ptr_gst_video_event_new_downstream_force_key_unit(
+            ClockTime timestamp, ClockTime stream_time, ClockTime running_time,
+            boolean all_headers, int count);
+
+    Pointer ptr_gst_video_event_new_upstream_force_key_unit(
+            ClockTime running_time, boolean all_headers, int count);
 }
