@@ -381,6 +381,12 @@ public abstract class GObject extends RefCountedObject {
         map.put(listener, cb);
     }
 
+    @Override
+    public void dispose() {
+        super.dispose();
+        STRONG_REFS.remove(this);
+    }
+    
     @Override
     public void invalidate() {
         try {
