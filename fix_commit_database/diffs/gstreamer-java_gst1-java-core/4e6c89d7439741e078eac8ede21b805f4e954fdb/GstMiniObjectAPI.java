@@ -1,8 +1,9 @@
-/* 
+/*
+ * Copyright (c) 2016 Christophe Lafolet
  * Copyright (c) 2014 Tom Greenwood <tgreenwood@cafex.com>
  * Copyright (c) 2009 Levente Farkas
  * Copyright (c) 2007, 2008 Wayne Meissner
- * 
+ *
  * This file is part of gstreamer-java.
  *
  * This code is free software: you can redistribute it and/or modify it under
@@ -20,15 +21,16 @@
 
 package org.freedesktop.gstreamer.lowlevel;
 
+import java.util.Arrays;
+import java.util.List;
+
 import org.freedesktop.gstreamer.MiniObject;
-import org.freedesktop.gstreamer.lowlevel.GObjectAPI.GTypeInstance;
+import org.freedesktop.gstreamer.glib.GQuark;
+import org.freedesktop.gstreamer.lowlevel.GlibAPI.GDestroyNotify;
 import org.freedesktop.gstreamer.lowlevel.annotations.CallerOwnsReturn;
 import org.freedesktop.gstreamer.lowlevel.annotations.Invalidate;
 
-import com.sun.jna.NativeLong;
 import com.sun.jna.Pointer;
-import java.util.Arrays;
-import java.util.List;
 
 /**
  * GstMiniObject functions
@@ -39,15 +41,17 @@ public interface GstMiniObjectAPI extends com.sun.jna.Library {
     void gst_mini_object_ref(MiniObject ptr);
     void gst_mini_object_unref(MiniObject ptr);
     void gst_mini_object_unref(Pointer ptr);
-    @CallerOwnsReturn Pointer ptr_gst_mini_object_copy(MiniObject mini_object);
+
+    @CallerOwnsReturn MiniObject gst_mini_object_make_writable(@Invalidate MiniObject mini_object);
     @CallerOwnsReturn MiniObject gst_mini_object_copy(MiniObject mini_object);
     boolean gst_mini_object_is_writable(MiniObject mini_object);
-    /* FIXME - invalidate the argument, and return a MiniObject */
-    @CallerOwnsReturn Pointer ptr_gst_mini_object_make_writable(@Invalidate MiniObject mini_object);
-    @CallerOwnsReturn MiniObject gst_mini_object_make_writable(@Invalidate MiniObject mini_object);
-    
+
+    void gst_mini_object_set_qdata(MiniObject mini_object, GQuark quark, Object data, GDestroyNotify destroyCallback);
+    Pointer gst_mini_object_get_qdata(MiniObject mini_object, GQuark quark);
+    Pointer gst_mini_object_steal_qdata(MiniObject mini_object, GQuark quark);
+
     public static final class MiniObjectStruct extends com.sun.jna.Structure {
-        public volatile NativeLong type; // GType - this is a guess from http://gstreamer.freedesktop.org/data/doc/gstreamer/head/gstreamer/html/gstreamer-GstMiniObject.html#GstMiniObject
+        public volatile GType type; 
         public volatile int refcount;
         public volatile int lockstate;
         public volatile int flags;
@@ -56,12 +60,11 @@ public interface GstMiniObjectAPI extends com.sun.jna.Library {
         public volatile Pointer freeFn;
         public volatile int n_qdata; // Private parts - there for alignment
         public volatile Pointer qdata;
-        
+
         /** Creates a new instance of GstMiniObjectStructure */
         public MiniObjectStruct() {}
         public MiniObjectStruct(Pointer ptr) {
-            useMemory(ptr);
-            read();
+            super(ptr);
         }
 
         @Override
@@ -71,7 +74,5 @@ public interface GstMiniObjectAPI extends com.sun.jna.Library {
                 "copyFn", "disposeFn", "freeFn", "n_qdata", "qdata"
             });
         }
-        
-        
     }
 }
