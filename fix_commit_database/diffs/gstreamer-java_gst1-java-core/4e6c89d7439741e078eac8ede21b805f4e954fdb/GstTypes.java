@@ -1,8 +1,9 @@
-/* 
+/*
+ * Copyright (c) 2016 Christophe Lafolet
  * Copyright (c) 2009 Levente Farkas
  * Copyright (c) 2008 Andres Colubri
  * Copyright (c) 2007 Wayne Meissner
- * 
+ *
  * This file is part of gstreamer-java.
  *
  * This code is free software: you can redistribute it and/or modify it under
@@ -20,90 +21,67 @@
 
 package org.freedesktop.gstreamer.lowlevel;
 
-import static org.freedesktop.gstreamer.lowlevel.GObjectAPI.GOBJECT_API;
-
-import java.util.HashMap;
 import java.util.Map;
 import java.util.concurrent.ConcurrentHashMap;
+import java.util.logging.Level;
 import java.util.logging.Logger;
 
-import com.sun.jna.Pointer;
-
 /**
  *
  */
 public class GstTypes {
     private static final Logger logger = Logger.getLogger(GstTypes.class.getName());
-    
-    private static final Map<String, Class<? extends NativeObject>> gTypeNameMap
-        = new HashMap<String, Class<? extends NativeObject>>();
-    private static final Map<Pointer, Class<? extends NativeObject>> gTypeInstanceMap
-        = new ConcurrentHashMap<Pointer, Class<? extends NativeObject>>();
-    
+
+    private static final Map<String, Class<? extends NativeObject>> gtypeNameMap
+        = new ConcurrentHashMap<String, Class<? extends NativeObject>>();
+
     private GstTypes() {}
+    
     /**
-     * Register a new class into the gTypeNameMap.
+     * Register a class with its GType name
      */
     public static void registerType(Class<? extends NativeObject> cls, String gTypeName) {
-   		gTypeNameMap.put(gTypeName, cls);
+   		gtypeNameMap.put(gTypeName, cls);
     }
+    
     /**
      * Retrieve the class of a GType
-     * 
+     *
      * @param gType The type of Class
      * @return The Class of the desired type.
      */
-    public static Class<? extends NativeObject> find(GType gType) {
-        logger.entering("GstTypes", "find", gType);
-        return gTypeNameMap.get(GOBJECT_API.g_type_name(gType));
-    }
-    /**
-     * Retrieve the class of a GType Name
-     * 
-     * @param gTypeName The type name of Class
-     * @return The Class of the desired type.
-     */
-    public static Class<? extends NativeObject> find(String gTypeName) {
-        logger.entering("GstTypes", "find", gTypeName);
-        return gTypeNameMap.get(gTypeName);
-    }
+    public static final Class<? extends NativeObject> classFor(final GType gType) {
+    	final String gTypeName = gType.getTypeName();
 
-    public static final boolean isGType(Pointer p, long type) {
-        return getGType(p).longValue() == type;
-    }
-    public static final GType getGType(Pointer ptr) {        
-        // Retrieve ptr->g_class
-        Pointer g_class = ptr.getPointer(0);
-        // Now return g_class->gtype
-        return GType.valueOf(g_class.getNativeLong(0).longValue());
-    }
-    public static final Class<? extends NativeObject> classFor(Pointer ptr) {
-        Pointer g_class = ptr.getPointer(0);
-        Class<? extends NativeObject> cls = gTypeInstanceMap.get(g_class);
+    	// Is this GType still registered in the map ? 
+    	Class<? extends NativeObject> cls = gtypeNameMap.get(gTypeName);
         if (cls != null) {
-            return cls;
+        	return cls;
         }
 
-        GType type = GType.valueOf(g_class.getNativeLong(0).longValue());
-        logger.finer("Type of " + ptr + " = " + type);
-        while (cls == null && !type.equals(GType.OBJECT) && !type.equals(GType.INVALID)) {
-            cls = find(type);
+        // Search for a parent class registration
+        GType type = gType.getParentType();
+        while (!type.equals(GType.OBJECT) && !type.equals(GType.INVALID)) {
+           	cls = gtypeNameMap.get(type.getTypeName());
             if (cls != null) {
-                logger.finer("Found type of " + ptr + " = " + cls);
-                gTypeInstanceMap.put(g_class, cls);
-                break;
+                if (GstTypes.logger.isLoggable(Level.FINER)) {
+                    GstTypes.logger.finer("Found type of " + gType + " = " + cls);
+                }
+                gtypeNameMap.put(gTypeName, cls);
+                return cls;
             }
-            type = GOBJECT_API.g_type_parent(type);
+        	type = type.getParentType();
         }
-        return cls;
-    }
-    public static final Class<? extends NativeObject> classFor(GType type) {
-        return find(type);
+
+        // Should never appeared
+        throw new IllegalStateException("Could not find a registered class for " + gType);
+
     }
+
     public static final GType typeFor(Class<? extends NativeObject> cls) {
-        for (Map.Entry<String, Class<? extends NativeObject>> e : gTypeNameMap.entrySet()) {
+        for (Map.Entry<String, Class<? extends NativeObject>> e : gtypeNameMap.entrySet()) {
             if (e.getValue().equals(cls)) {
-                return GOBJECT_API.g_type_from_name(e.getKey());
+                return GType.valueOf(e.getKey());
             }
         }
         return GType.INVALID;
