@@ -26,7 +26,6 @@ import org.freedesktop.gstreamer.QueryType;
 import org.freedesktop.gstreamer.Structure;
 import org.freedesktop.gstreamer.glib.GQuark;
 import org.freedesktop.gstreamer.lowlevel.annotations.CallerOwnsReturn;
-import org.freedesktop.gstreamer.lowlevel.annotations.Invalidate;
 
 import com.sun.jna.Pointer;
 import java.util.Arrays;
@@ -40,11 +39,6 @@ public interface GstQueryAPI extends com.sun.jna.Library {
 
     String gst_query_type_get_name(QueryType query);
     GQuark gst_query_type_to_quark(QueryType query);
-    /* register a new query */
-    QueryType gst_query_type_register(String nick, String description);
-    QueryType gst_query_type_get_by_nick(String nick);
-    
-    GType gst_query_get_type();
 
     /* position query */
     @CallerOwnsReturn Query gst_query_new_position(Format format);
@@ -76,8 +70,6 @@ public interface GstQueryAPI extends com.sun.jna.Library {
     void gst_query_parse_segment(Query query, double[] rate, Format[] format,
          /* gint64 * */ long[] start_value, /* gint64 * */ long[] stop_value);
 
-    /* application specific query */
-    @CallerOwnsReturn Query gst_query_new_application(QueryType type, @Invalidate Structure structure);
     Structure gst_query_get_structure(Query query);
 
     /* seeking query */
@@ -88,10 +80,11 @@ public interface GstQueryAPI extends com.sun.jna.Library {
         boolean[] seekable, /* gint64 * */ long[] segment_start, /* gint64 * */ long[] segment_end);
     /* formats query */
     @CallerOwnsReturn Query gst_query_new_formats();
+    Pointer ptr_gst_query_new_formats();
     void gst_query_set_formats(Query query, int n_formats, Format... formats);
     void gst_query_set_formatsv(Query query, int n_formats, Format[] formats);
-    void gst_query_parse_formats_length(Query query, int[] n_formats);
-    void gst_query_parse_formats_nth(Query query, int nth, Format[] format);
+    void gst_query_parse_n_formats(Query query, int[] n_formats);
+    void gst_query_parse_nth_format(Query query, int nth, Format[] format);
     
     public static final class QueryStruct extends com.sun.jna.Structure {
         public volatile GstMiniObjectAPI.MiniObjectStruct mini_object;
@@ -110,4 +103,13 @@ public interface GstQueryAPI extends com.sun.jna.Library {
             });
         }
     }
+    
+    public interface GstQueryTypeFlags {
+        public static final int UPSTREAM = 1;
+        public static final int DOWNSTREAM = 1 << 1;
+        public static final int SERIALIZED = 1 << 2;
+        public static final int BOTH = UPSTREAM | DOWNSTREAM;
+    }
+    
+    public static final int GST_QUERY_NUM_SHIFT = 8;
 }
