@@ -50,6 +50,8 @@ import com.sun.jna.Pointer;
 import com.sun.jna.ptr.IntByReference;
 import com.sun.jna.ptr.PointerByReference;
 import java.util.Arrays;
+import java.util.HashSet;
+import java.util.Set;
 import org.freedesktop.gstreamer.MiniObject;
 import org.freedesktop.gstreamer.lowlevel.GObjectPtr;
 import org.freedesktop.gstreamer.lowlevel.GPointer;
@@ -198,8 +200,8 @@ public abstract class GObject extends RefCountedObject {
             if (cls != null) {
                 Pointer ptr = GVALUE_API.g_value_get_boxed(propValue);
                 final GPointer gptr = GObject.class.isAssignableFrom(cls) ? new GObjectPtr(ptr)
-                : MiniObject.class.isAssignableFrom(cls) ? new GstMiniObjectPtr(ptr)
-                : new GPointer(ptr);
+                        : MiniObject.class.isAssignableFrom(cls) ? new GstMiniObjectPtr(ptr)
+                        : new GPointer(ptr);
                 return objectFor(gptr, cls, -1, true);
             }
         }
@@ -279,7 +281,7 @@ public abstract class GObject extends RefCountedObject {
 
     /**
      * Get the native GType type name.
-     * @return GType type name 
+     * @return GType type name
      */
     public String getTypeName() {
         return handle.getPointer().getGType().getTypeName();
@@ -386,7 +388,7 @@ public abstract class GObject extends RefCountedObject {
         super.dispose();
         STRONG_REFS.remove(this);
     }
-    
+
     @Override
     public void invalidate() {
         try {
@@ -620,7 +622,7 @@ public abstract class GObject extends RefCountedObject {
     private final class SignalCallback extends GCallback {
 
         protected SignalCallback(String signal, Callback cb) {
-            super(GOBJECT_API.g_signal_connect_data(GObject.this, signal, cb, null, null, 0), cb);
+            super(handle.connectSignal(signal, cb), cb);
             if (!connected) {
                 throw new IllegalArgumentException(String.format("Failed to connect signal '%s'", signal));
             }
@@ -628,17 +630,50 @@ public abstract class GObject extends RefCountedObject {
 
         @Override
         synchronized protected void disconnect() {
-            GOBJECT_API.g_signal_handler_disconnect(GObject.this, id);
+            handle.disconnectSignal(id);
         }
     }
 
     protected static class Handle extends RefCountedObject.Handle {
 
         private final IntPtr objectID;
+        private final Set<NativeLong> signals;
 
         public Handle(GObjectPtr ptr, boolean ownsHandle) {
             super(ptr, ownsHandle);
             this.objectID = new IntPtr(System.identityHashCode(this));
+            signals = new HashSet<>();
+        }
+
+        private synchronized NativeLong connectSignal(String signal, Callback cb) {
+            NativeLong id = GOBJECT_API.g_signal_connect_data(getPointer(), signal, cb, null, null, 0);
+            if (id.longValue() != 0) {
+                signals.add(id);
+            }
+            return id;
+        }
+
+        private synchronized void disconnectSignal(NativeLong id) {
+            if (signals.remove(id)) {
+                GOBJECT_API.g_signal_handler_disconnect(getPointer(), id);
+            }
+        }
+
+        private synchronized void clearSignals() {
+            signals.forEach(id -> GOBJECT_API.g_signal_handler_disconnect(getPointer(), id));
+            signals.clear();
+        }
+
+        @Override
+        public void invalidate() {
+            clearSignals();
+            super.dispose();
+        }
+
+        @Override
+        public void dispose() {
+            clearSignals();
+            super.dispose();
         }
 
         @Override
@@ -680,7 +715,7 @@ public abstract class GObject extends RefCountedObject {
         }
 
     }
-    
+
     private static final class ToggleNotify implements GObjectAPI.GToggleNotify {
 
         @Override
