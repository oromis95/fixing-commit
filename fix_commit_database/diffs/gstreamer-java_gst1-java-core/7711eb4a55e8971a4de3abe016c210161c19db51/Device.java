@@ -1,6 +1,8 @@
 /*
+ * 
+ * Copyright (c) 2017 Neil C Smith
  * Copyright (c) 2015 Andres Colubri <andres.colubri@gmail.com>
-*  Copyright (C) 2013 Olivier Crete <olivier.crete@collabora.com>
+ * Copyright (C) 2013 Olivier Crete <olivier.crete@collabora.com>
  *
  * This file is part of gstreamer-java.
  *
@@ -18,55 +20,62 @@
  */
 package org.freedesktop.gstreamer.device;
 
+import com.sun.jna.Pointer;
 import org.freedesktop.gstreamer.Caps;
 import org.freedesktop.gstreamer.Element;
 import org.freedesktop.gstreamer.GstObject;
 import org.freedesktop.gstreamer.Structure;
-import org.freedesktop.gstreamer.lowlevel.GstDeviceAPI;
-import org.freedesktop.gstreamer.lowlevel.GstNative;
+
+import static org.freedesktop.gstreamer.lowlevel.GlibAPI.GLIB_API;
+import static org.freedesktop.gstreamer.lowlevel.GstDeviceAPI.GSTDEVICE_API;
 
 /**
  *
  * @author andres
  */
 public class Device extends GstObject {
+
     public static final String GTYPE_NAME = "GstDevice";
-    
-    private static final GstDeviceAPI gst = GstNative.load(GstDeviceAPI.class);
-    
+
     public Device(Initializer init) {
         super(init);
     }
-    
+
     public Element createElement(String name) {
-        return gst.gst_device_create_element(this, name);    
+        return GSTDEVICE_API.gst_device_create_element(this, name);
     }
-    
+
     public Caps getCaps() {
-        return gst.gst_device_get_caps(this);
+        return GSTDEVICE_API.gst_device_get_caps(this);
     }
-    
+
     public String getDeviceClass() {
-        return gst.gst_device_get_device_class(this);
+        Pointer ptr = GSTDEVICE_API.gst_device_get_device_class(this);
+        String ret = ptr.getString(0);
+        GLIB_API.g_free(ptr);
+        return ret;
     }
-    
+
     public String getDisplayName() {
-        return gst.gst_device_get_display_name(this);
+        Pointer ptr = GSTDEVICE_API.gst_device_get_display_name(this);
+        String ret = ptr.getString(0);
+        GLIB_API.g_free(ptr);
+        return ret;
     }
-    
+
     public boolean hasClasses(String classes) {
-        return gst.gst_device_has_classes(this, classes);
-    }     
+        return GSTDEVICE_API.gst_device_has_classes(this, classes);
+    }
 
     public boolean hasClasses(String[] classes) {
-        return gst.gst_device_has_classesv(this, classes);
-    }    
-    
+        return GSTDEVICE_API.gst_device_has_classesv(this, classes);
+    }
+
     public boolean reconfigureElement(Element element) {
-        return gst.gst_device_reconfigure_element(this, element);
+        return GSTDEVICE_API.gst_device_reconfigure_element(this, element);
     }
-    
+
     public Structure getProperties() {
-        return gst.gst_device_get_properties(this);
+        return GSTDEVICE_API.gst_device_get_properties(this);
     }
 }
