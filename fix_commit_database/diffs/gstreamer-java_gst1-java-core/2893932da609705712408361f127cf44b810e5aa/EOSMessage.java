@@ -22,9 +22,6 @@ package org.freedesktop.gstreamer.message;
 import org.freedesktop.gstreamer.GstObject;
 import org.freedesktop.gstreamer.Message;
 import org.freedesktop.gstreamer.lowlevel.GstMessageAPI;
-import org.freedesktop.gstreamer.lowlevel.GstNative;
-
-import com.sun.jna.Pointer;
 
 /**
  * This message is generated and posted in the sink elements of a {@link org.freedesktop.gstreamer.Bin}.
@@ -32,10 +29,8 @@ import com.sun.jna.Pointer;
  * have posted an EOS message.
  */
 public class EOSMessage extends Message {
-    private static interface API extends GstMessageAPI {
-        Pointer ptr_gst_message_new_eos(GstObject src);
-    }
-    private static final API gst = GstNative.load(API.class);
+
+    private static final GstMessageAPI gst = GstMessageAPI.GSTMESSAGE_API;
     
     /**
      * Creates a new eos message.
