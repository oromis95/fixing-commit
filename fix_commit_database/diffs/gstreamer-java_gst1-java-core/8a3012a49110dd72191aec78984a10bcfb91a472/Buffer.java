@@ -251,7 +251,10 @@ public class Buffer extends MiniObject {
     }
 
     /**
-     * get the GstBufferFlags describing this buffer
+     * get the GstBufferFlags describing this buffer.
+     * 
+     * Since GStreamer 1.10 
+     * 
      * @return a bit mask whose values can be interpreted by comparing them with {@link BufferFlag}
      */
     public int getFlags()
@@ -261,6 +264,9 @@ public class Buffer extends MiniObject {
 
     /**
      * set some of the GstBufferFlags describing this buffer.  This is a union operation and does not clear flags that are not mentioned in val
+     * 
+     * Since GStreamer 1.10
+     * 
      * @param val a bit mask of flags to be set on the buffer.  bits which are zero in val do not get cleared in this buffer.
      * @return true if flags were successfully set on this buffer
      * @see BufferFlag
@@ -272,6 +278,9 @@ public class Buffer extends MiniObject {
 
     /**
      * unset the GstBufferFlags describing this buffer.  This is a difference operation and does not clear flags that are not mentioned in val
+     * 
+     * Since GStreamer 1.10
+     * 
      * @param val a bit mask of flags to be cleared on the buffer.  bits which are zero in val do not get cleared in this buffer.
      * @return true if flags were successfully cleared on this buffer
      * @see BufferFlag
