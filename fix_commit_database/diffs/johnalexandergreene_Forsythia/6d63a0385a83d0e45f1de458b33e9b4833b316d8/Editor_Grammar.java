@@ -41,19 +41,17 @@ public class Editor_Grammar extends Editor{
   
   private void refreshButtons(){
     EG_UI ui=(EG_UI)getUI();
-    ui.lblgrammarname.setText("Grammar = "+GE.focusgrammar.getName());
-    ui.lblmetagonscount.setText("Count="+GE.focusgrammar.getMetagonCount());
-    ui.lblmetagonjiglesscount.setText("Jigless="+GE.focusgrammar.getJiglessMetagonsCount());
-    ui.lblmetagonsisolatedcount.setText("Isolated="+GE.focusgrammar.getIsolatedMetagonsCount());
-    ui.lbljigscount.setText("Count="+getJigCount());
-
-    }
+    ui.lblgrammarname.setText("Grammar = "+GE.ge.focusgrammar.getName());
+    ui.lblmetagonscount.setText("Count="+GE.ge.focusgrammar.getMetagonCount());
+    ui.lblmetagonjiglesscount.setText("Jigless="+GE.ge.focusgrammar.getJiglessMetagonsCount());
+    ui.lblmetagonsisolatedcount.setText("Isolated="+GE.ge.focusgrammar.getIsolatedMetagonsCount());
+    ui.lbljigscount.setText("Count="+getJigCount());}
   
   private int getJigCount(){
-    if(GE.focusmetagon==null)
+    if(GE.ge.focusmetagon==null)
       return 0;
     else
-      return GE.focusmetagon.getJigCount();}
+      return GE.ge.focusmetagon.getJigCount();}
   
   /*
    * ################################
@@ -63,14 +61,14 @@ public class Editor_Grammar extends Editor{
 
   public void configureForOpen(){
     //if focus metagon is null then try to make it nonnull
-    if(GE.focusmetagon==null)
-      if(GE.focusgrammar.hasMetagons())
-        GE.focusmetagon=GE.focusgrammar.getMetagon(0);
+    if(GE.ge.focusmetagon==null)
+      if(GE.ge.focusgrammar.hasMetagons())
+        GE.ge.focusmetagon=GE.ge.focusgrammar.getMetagon(0);
     //if focus metagon is nonnull and jig is null 
     //then try to make focus jig nonnull
-    if(GE.focusmetagon!=null&&GE.focusjig==null)
-      if(GE.focusmetagon.hasJigs())
-        GE.focusjig=GE.focusmetagon.getJig(0);
+    if(GE.ge.focusmetagon!=null&&GE.ge.focusjig==null)
+      if(GE.ge.focusmetagon.hasJigs())
+        GE.ge.focusjig=GE.ge.focusmetagon.getJig(0);
     //
     refreshUI();}
 
@@ -89,25 +87,27 @@ public class Editor_Grammar extends Editor{
    */
   
   public void createMetagon(){
-    ((EG_UI)GE.editor_grammar.getUI()).panmetagonmenu.invalidateIconArrayMetrics();
-    GE.setEditor(GE.editor_createmetagon);
+    EG_UI ui=(EG_UI)getUI();
+    ui.panmetagonmenu.invalidateIconArrayMetrics();
+    GE.ge.setEditor(GE.ge.editor_metagon);
     refreshUI();}
   
   public void editMetagon(){
-    GE.setEditor(GE.editor_editmetagondetails);
+    GE.ge.setEditor(GE.ge.editor_metagon);
     refreshUI();}
   
   public void discardMetagon(){
-    ((EG_UI)GE.editor_grammar.getUI()).panmetagonmenu.invalidateIconArrayMetrics();
-    ((EG_UI)GE.editor_grammar.getUI()).panjigmenu.invalidateIconArrayMetrics();
-    int a=GE.focusgrammar.getIndex(GE.focusmetagon)-1;
-    GE.focusgrammar.discardMetagon(GE.focusmetagon);
+    EG_UI ui=(EG_UI)getUI();
+    ui.panmetagonmenu.invalidateIconArrayMetrics();
+    ui.panjigmenu.invalidateIconArrayMetrics();
+    int a=GE.ge.focusgrammar.getIndex(GE.ge.focusmetagon)-1;
+    GE.ge.focusgrammar.discardMetagon(GE.ge.focusmetagon);
     if(a<0)a=0;
-    GE.focusmetagon=GE.focusgrammar.getMetagon(a);
+    GE.ge.focusmetagon=GE.ge.focusgrammar.getMetagon(a);
     refreshUI();}
   
   public void setFocusMetagon(final ProjectMetagon m){
-    GE.focusmetagon=m;
+    GE.ge.focusmetagon=m;
     EG_UI ui=(EG_UI)getUI();
     ui.panjigmenu.invalidateIconArrayMetrics();
     refreshUI();}
@@ -121,25 +121,26 @@ public class Editor_Grammar extends Editor{
    */
   
   public void createJig(){
-    ((EG_UI)GE.editor_grammar.getUI()).panmetagonmenu.invalidateIconArrayMetrics();
-    GE.focusjig=null;
-    GE.setEditor(GE.editor_jig);}
+    EG_UI ui=(EG_UI)getUI();
+    ui.panmetagonmenu.invalidateIconArrayMetrics();
+    GE.ge.focusjig=null;
+    GE.ge.setEditor(GE.ge.editor_jig);}
   
   public void editJig(){
-    ((EG_UI)GE.editor_grammar.getUI()).panmetagonmenu.invalidateIconArrayMetrics();
-    GE.setEditor(GE.editor_jig);}
+    ((EG_UI)GE.ge.editor_grammar.getUI()).panmetagonmenu.invalidateIconArrayMetrics();
+    GE.ge.setEditor(GE.ge.editor_jig);}
   
   public void discardJig(){
-    ((EG_UI)GE.editor_grammar.getUI()).panjigmenu.invalidateIconArrayMetrics();
-    int a=GE.focusmetagon.getJigIndex(GE.focusjig)-1;
-    GE.focusmetagon.discardJig(GE.focusjig);
+    ((EG_UI)GE.ge.editor_grammar.getUI()).panjigmenu.invalidateIconArrayMetrics();
+    int a=GE.ge.focusmetagon.getJigIndex(GE.ge.focusjig)-1;
+    GE.ge.focusmetagon.discardJig(GE.ge.focusjig);
     if(a<0)a=0;
-    GE.focusjig=GE.focusmetagon.getJig(a);
+    GE.ge.focusjig=GE.ge.focusmetagon.getJig(a);
     refreshUI();
     getUI().repaint();}
   
   public void setFocusJig(final ProjectJig m){
-    GE.focusjig=m;
+    GE.ge.focusjig=m;
     EG_UI ui=(EG_UI)getUI();
     refreshUI();
     ui.panjigmenu.repaint();}
@@ -151,9 +152,9 @@ public class Editor_Grammar extends Editor{
    */
 
   public void createNewGrammar(){
-    GE.focusgrammar=new ProjectGrammar();
-    GE.focusmetagon=null;
-    GE.focusjig=null;
+    GE.ge.focusgrammar=new ProjectGrammar();
+    GE.ge.focusmetagon=null;
+    GE.ge.focusjig=null;
     refreshUI();}
   
   public void exportGrammar(){
@@ -170,6 +171,6 @@ public class Editor_Grammar extends Editor{
    */
   
   public void generate(){
-    GE.setEditor(GE.editor_generator);}
+    GE.ge.setEditor(GE.ge.editor_generator);}
 
 }
