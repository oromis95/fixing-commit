@@ -1,4 +1,4 @@
-package org.fleen.forsythia.app.grammarEditor.editor_CreateMetagon;
+package org.fleen.forsythia.app.grammarEditor.editor_Metagon;
 
 import java.awt.Graphics2D;
 
@@ -8,9 +8,9 @@ import org.fleen.geom_Kisrhombille.KPolygon;
 import org.fleen.geom_Kisrhombille.KVertex;
 
 @SuppressWarnings("serial")
-public class ECMGrid extends Grid{
+public class EMGrid extends Grid{
   
-  private ECMGridOverlayPainter overlaypainter=new ECMGridOverlayPainter();
+  private EMGridOverlayPainter overlaypainter=new EMGridOverlayPainter();
 
   protected void paintOverlay(Graphics2D g,int w,int h,
     double scale,double centerx,double centery){
@@ -25,10 +25,10 @@ public class ECMGrid extends Grid{
      }catch(Exception x){}}
   
   protected KPolygon getHostPolygon(){
-    return GE.editor_createmetagon.workingpolygon;}
+    return GE.ge.editor_metagon.workingpolygon;}
 
   protected void mouseTouched(double[] p,KVertex v){
-    GE.editor_createmetagon.touchVertex(v);}
+    GE.ge.editor_metagon.touchVertex(v);}
 
   protected void mouseMovedCloseToVertex(KVertex v){}
 
