@@ -23,8 +23,8 @@ public class GridOverlayPainter{
   
   public void paint(Graphics2D graphics,int w,int h,double scale,double centerx,double centery){
     graphics.setRenderingHints(UI.RENDERING_HINTS);
-    GE.editor_jig.model.viewgeometrycache.update(w,h,scale,centerx,centery);
-    if(GE.editor_jig.mode==Editor_Jig.MODE_EDITSECTIONS){
+    GE.ge.editor_jig.model.viewgeometrycache.update(w,h,scale,centerx,centery);
+    if(GE.ge.editor_jig.mode==Editor_Jig.MODE_EDITSECTIONS){
       renderJigModel_EditSections(graphics);
     }else{//GE.editor_jig.mode==Editor_Jig.MODE_EDITGEOMETRY
       renderJigModel_EditGeometry(graphics);}}
@@ -43,24 +43,24 @@ public class GridOverlayPainter{
   private void fillSections_EditGeometry(Graphics2D graphics){
     Color color;
     Path2D path;
-    for(KPolygon m:GE.editor_jig.model.rawgraph.getDisconnectedGraph().getUndividedPolygons()){
+    for(KPolygon m:GE.ge.editor_jig.model.rawgraph.getDisconnectedGraph().getUndividedPolygons()){
       color=UI.EDITJIG_EDITGEOMETRY_HOSTMETAGONFILLCOLOR;
-      path=GE.editor_jig.model.viewgeometrycache.getPath(m);
+      path=GE.ge.editor_jig.model.viewgeometrycache.getPath(m);
       graphics.setPaint(color);
       graphics.fill(path);}}
   
   private void strokeGraphEdges_EditGeometry(Graphics2D graphics){
     graphics.setStroke(UI.GRID_DRAWINGSTROKE);
     graphics.setPaint(UI.EDITJIG_EDITGEOMETRY_STROKECOLOR);
-    Iterator<GEdge> i=GE.editor_jig.model.rawgraph.edges.iterator();
+    Iterator<GEdge> i=GE.ge.editor_jig.model.rawgraph.edges.iterator();
     GEdge e;
     double[] p0,p1;
     Path2D path=new Path2D.Double();
     //System.out.println("edge count:"+Q.editor_createjig.graph.getEdgeCount());
     while(i.hasNext()){
       e=i.next();
-      p0=GE.editor_jig.model.viewgeometrycache.getPoint(e.v0.kvertex);
-      p1=GE.editor_jig.model.viewgeometrycache.getPoint(e.v1.kvertex);
+      p0=GE.ge.editor_jig.model.viewgeometrycache.getPoint(e.v0.kvertex);
+      p1=GE.ge.editor_jig.model.viewgeometrycache.getPoint(e.v1.kvertex);
       path.reset();
       path.moveTo(p0[0],p0[1]);
       path.lineTo(p1[0],p1[1]);
@@ -81,8 +81,8 @@ public class GridOverlayPainter{
     float span=UI.GRID_DEFAULTVERTEXSPAN;
     double[] p;
     Ellipse2D dot=new Ellipse2D.Double();
-    for(GVertex v:GE.editor_jig.model.rawgraph.vertices){
-      p=GE.editor_jig.model.viewgeometrycache.getPoint(v.kvertex);
+    for(GVertex v:GE.ge.editor_jig.model.rawgraph.vertices){
+      p=GE.ge.editor_jig.model.viewgeometrycache.getPoint(v.kvertex);
       dot.setFrame(p[0]-span/2,p[1]-span/2,span,span);
       graphics.fill(dot);}}
   
@@ -90,12 +90,12 @@ public class GridOverlayPainter{
     graphics.setStroke(UI.EDITJIG_EDITGEOMETRY_HEADVERTEXDECORATIONSTROKE);
     int span=UI.EDITJIG_EDITGEOMETRY_HEADVERTEXDECORATIONSPAN;
     double[] p;
-    if(GE.editor_jig.connectedhead!=null){
-      p=GE.editor_jig.model.viewgeometrycache.getPoint(GE.editor_jig.connectedhead);
+    if(GE.ge.editor_jig.connectedhead!=null){
+      p=GE.ge.editor_jig.model.viewgeometrycache.getPoint(GE.ge.editor_jig.connectedhead);
       graphics.setPaint(UI.EDITJIG_EDITGEOMETRY_CONNECTEDHEADVERTEXDECORATIONCOLOR);
       graphics.drawOval(((int)p[0])-span/2,((int)p[1])-span/2,span,span);
-    }else if(GE.editor_jig.unconnectedhead!=null){
-      p=GE.editor_jig.model.viewgeometrycache.getPoint(GE.editor_jig.unconnectedhead);
+    }else if(GE.ge.editor_jig.unconnectedhead!=null){
+      p=GE.ge.editor_jig.model.viewgeometrycache.getPoint(GE.ge.editor_jig.unconnectedhead);
       graphics.setPaint(UI.EDITJIG_EDITGEOMETRY_UNCONNECTEDHEADVERTEXDECORATIONCOLOR);
       graphics.drawOval(((int)p[0])-span/2,((int)p[1])-span/2,span,span);}}
   
@@ -118,10 +118,10 @@ public class GridOverlayPainter{
     int colorindex;
     Color color;
     Path2D path;
-    for(JigSectionEditingModel m:GE.editor_jig.model.sections){
+    for(JigSectionEditingModel m:GE.ge.editor_jig.model.sections){
       colorindex=m.chorusindex;
       color=UI.EDITJIG_EDITSECTIONS_SECTIONFILL[colorindex%UI.EDITJIG_EDITSECTIONS_SECTIONFILL.length];
-      path=GE.editor_jig.model.viewgeometrycache.getPath(m.getPolygon());
+      path=GE.ge.editor_jig.model.viewgeometrycache.getPath(m.getPolygon());
       graphics.setPaint(color);
       graphics.fill(path);}}
   
@@ -133,13 +133,13 @@ public class GridOverlayPainter{
     graphics.setStroke(UI.GRID_DRAWINGSTROKE);
     graphics.setPaint(UI.EDITJIG_EDITSECTIONS_UNFOCUSSTROKECOLOR);
     Path2D path;
-    for(JigSectionEditingModel m:GE.editor_jig.model.sections){
-      if(m==GE.editor_jig.focussection)continue;
-      path=GE.editor_jig.model.viewgeometrycache.getPath(m.getPolygon());
+    for(JigSectionEditingModel m:GE.ge.editor_jig.model.sections){
+      if(m==GE.ge.editor_jig.focussection)continue;
+      path=GE.ge.editor_jig.model.viewgeometrycache.getPath(m.getPolygon());
       graphics.draw(path);}
     //focus
     graphics.setPaint(UI.EDITJIG_EDITSECTIONS_FOCUSSTROKECOLOR);
-    path=GE.editor_jig.model.viewgeometrycache.getPath(GE.editor_jig.focussection.getPolygon());
+    path=GE.ge.editor_jig.model.viewgeometrycache.getPath(GE.ge.editor_jig.focussection.getPolygon());
     graphics.draw(path);}
   
   /*
@@ -155,11 +155,11 @@ public class GridOverlayPainter{
   private void renderGlyphs_EditSections(Graphics2D graphics){
     //get non-focus section polygons
     List<DPolygon> nonfocussections=new ArrayList<DPolygon>();
-    for(JigSectionEditingModel section:GE.editor_jig.model.sections)
-      if(section!=GE.editor_jig.focussection)
-        nonfocussections.add(GE.editor_jig.model.viewgeometrycache.getDPolygon(section.getPolygon()));
+    for(JigSectionEditingModel section:GE.ge.editor_jig.model.sections)
+      if(section!=GE.ge.editor_jig.focussection)
+        nonfocussections.add(GE.ge.editor_jig.model.viewgeometrycache.getDPolygon(section.getPolygon()));
     //get focus section polygon
-    DPolygon focussection=GE.editor_jig.model.viewgeometrycache.getDPolygon(GE.editor_jig.focussection.getPolygon());
+    DPolygon focussection=GE.ge.editor_jig.model.viewgeometrycache.getDPolygon(GE.ge.editor_jig.focussection.getPolygon());
     //render non-focus section polygons
     for(DPolygon nonfocussection:nonfocussections)
       renderGlyphs(graphics,nonfocussection,UI.EDITJIG_EDITSECTIONS_UNFOCUSSTROKECOLOR);
