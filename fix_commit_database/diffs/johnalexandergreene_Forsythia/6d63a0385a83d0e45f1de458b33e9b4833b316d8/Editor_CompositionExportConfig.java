@@ -8,7 +8,6 @@ import javax.swing.JPanel;
 import org.fleen.forsythia.app.grammarEditor.GE;
 import org.fleen.forsythia.app.grammarEditor.compositionExporter.CompositionExportConfig;
 import org.fleen.forsythia.app.grammarEditor.util.Editor;
-import org.fleen.forsythia.app.grammarEditor.util.Task;
 
 /*
  * This is a composition streamer.
@@ -17,6 +16,8 @@ import org.fleen.forsythia.app.grammarEditor.util.Task;
  */
 public class Editor_CompositionExportConfig extends Editor{
 
+  private static final long serialVersionUID=-4570862872475998900L;
+  
   public static final String NAME="Export Configurator";
   
   /*
@@ -44,14 +45,12 @@ public class Editor_CompositionExportConfig extends Editor{
    */
   
   public void configureForOpen(){
-    GE.tasksequencer.add(new Task(){
-      public void doTask(){
-        CompositionExportConfig c=GE.config.getCompositionExportConfig();
+        CompositionExportConfig c=new CompositionExportConfig();
         CompositionExportConfigEditorUI ui=(CompositionExportConfigEditorUI)getUI();
         ui.txtexportdirpath.setText(c.exportdir.getPath());
         ui.chkboxraster.setSelected(c.exportraster);
         ui.chkboxvector.setSelected(c.exportvector);
-        ui.txtsize.setText(c.rasterimagepreferredwidth+"x"+c.rasterimagepreferredheight);}});}
+        ui.txtsize.setText(c.rasterimagepreferredwidth+"x"+c.rasterimagepreferredheight);}
   
   public void configureForClose(){}
 
@@ -64,7 +63,7 @@ public class Editor_CompositionExportConfig extends Editor{
   public void refreshUI(){}
   
   void saveChanges(){
-    CompositionExportConfig c=GE.config.getCompositionExportConfig();
+    CompositionExportConfig c=new CompositionExportConfig();
     CompositionExportConfigEditorUI ui=(CompositionExportConfigEditorUI)getUI();
     c.exportdir=new File(ui.txtexportdirpath.getText());
     c.exportraster=ui.chkboxraster.isSelected();
@@ -73,7 +72,7 @@ public class Editor_CompositionExportConfig extends Editor{
     if(size!=null){
       c.rasterimagepreferredwidth=size[0];
       c.rasterimagepreferredheight=size[1];}
-    GE.setEditor(GE.editor_generator);}
+    GE.ge.setEditor(GE.ge.editor_generator);}
   
   int[] getRasterImageSize(){
     CompositionExportConfigEditorUI ui=(CompositionExportConfigEditorUI)getUI();
@@ -89,14 +88,14 @@ public class Editor_CompositionExportConfig extends Editor{
     return new int[]{w,h};}
   
   void discardChanges(){
-    GE.setEditor(GE.editor_generator);}
+    GE.ge.setEditor(GE.ge.editor_generator);}
   
   void gleanExportPathDir(){
     CompositionExportConfigEditorUI ui=(CompositionExportConfigEditorUI)getUI();
     JFileChooser fc=new JFileChooser();
     fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
     fc.setCurrentDirectory(new File(ui.txtexportdirpath.getText()));
-    int r=fc.showOpenDialog(GE.uimain);
+    int r=fc.showOpenDialog(GE.ge.uimain);
     if(r==JFileChooser.APPROVE_OPTION)
       ui.txtexportdirpath.setText(fc.getSelectedFile().getPath());}
   
