@@ -18,10 +18,13 @@ public class SampleGrammars implements Serializable{
    * yes, we don't just get a list. That's difficult apparently.
    */
   private static final String[] GRAMMARNAMES={
-    "triangley.grammar",
-    "triangley2.grammar",
     "boxy.grammar",
-    "mixy.grammar"};
+    "deterministic0.grammar",
+    "mixy.grammar",
+    "nice0.grammar",
+    "nice1.grammar",
+    "triangley.grammar",
+    "triangley2.grammar"};
   
   /*
    * load each of our sample grammars from resource
@@ -32,7 +35,7 @@ public class SampleGrammars implements Serializable{
     for(String name:GRAMMARNAMES)
       loadAndExportResourceGrammar(name);
     File path=GE.getLocalDir();
-    path=new File(path.getAbsolutePath()+"/"+GRAMMARNAMES[3]);
+    path=new File(path.getAbsolutePath()+"/"+GRAMMARNAMES[4]);//nice1.grammar
     GE.ge.grammarimportexport.importGrammar(path);}
   
   private ForsythiaGrammar loadAndExportResourceGrammar(String name){
