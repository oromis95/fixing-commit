@@ -1,18 +1,13 @@
 package org.fleen.forsythia.app.grammarEditor.editor_EditGrammar;
 
-import java.awt.CardLayout;
-import java.awt.Container;
-
 import javax.swing.JPanel;
 
 import org.fleen.forsythia.app.grammarEditor.GE;
-import org.fleen.forsythia.app.grammarEditor.editor_CreateJigGeometry.Editor_CreateJigGeometry;
 import org.fleen.forsythia.app.grammarEditor.project.GrammarImportExport;
 import org.fleen.forsythia.app.grammarEditor.project.ProjectGrammar;
 import org.fleen.forsythia.app.grammarEditor.project.ProjectJig;
 import org.fleen.forsythia.app.grammarEditor.project.ProjectMetagon;
 import org.fleen.forsythia.app.grammarEditor.util.Editor;
-import org.fleen.forsythia.app.grammarEditor.util.Task;
 
 public class Editor_Grammar extends Editor{
 
@@ -51,66 +46,50 @@ public class Editor_Grammar extends Editor{
     //then try to make focus jig nonnull
     if(GE.focusmetagon!=null&&GE.focusjig==null)
       if(GE.focusmetagon.hasProtoJigs())
-        GE.focusjig=GE.focusmetagon.getProtoJig(0);
+        GE.focusjig=GE.focusmetagon.getJig(0);
     //
-    refreshAll();}
+    refreshUI();}
   
   public void createMetagon(){
-    GE.tasksequencer.add(new Task(){
-      public void doTask(){
-        //invalidate icon metrics because we might be about to add a new icon
-        ((G_UI)GE.editor_grammar.getUI()).panmetagonmenu.invalidateIconArrayMetrics();
-        GE.setEditor(GE.editor_createmetagon);}});}
+    ((G_UI)GE.editor_grammar.getUI()).panmetagonmenu.invalidateIconArrayMetrics();
+    GE.setEditor(GE.editor_createmetagon);}
   
   public void viewMetagonAndEditTags(){
-    GE.tasksequencer.add(new Task(){
-      public void doTask(){
-//        ((GrammarEditorUI)Q.editor_grammar.getUI()).panmetagonmenu.invalidateIconArrayMetrics();
-        GE.setEditor(GE.editor_editmetagondetails);}});}
+    GE.setEditor(GE.editor_editmetagondetails);}
   
   public void discardMetagon(){
-    GE.tasksequencer.add(new Task(){
-      public void doTask(){
-        ((G_UI)GE.editor_grammar.getUI()).panmetagonmenu.invalidateIconArrayMetrics();
-        ((G_UI)GE.editor_grammar.getUI()).panjigmenu.invalidateIconArrayMetrics();
-        int a=GE.focusgrammar.getIndex(GE.focusmetagon)-1;
-        GE.focusgrammar.discardMetagon(GE.focusmetagon);
-        if(a<0)a=0;
-        GE.focusmetagon=GE.focusgrammar.getMetagon(a);
-        refreshInfo();
-        getUI().repaint();}});}
+    ((G_UI)GE.editor_grammar.getUI()).panmetagonmenu.invalidateIconArrayMetrics();
+    ((G_UI)GE.editor_grammar.getUI()).panjigmenu.invalidateIconArrayMetrics();
+    int a=GE.focusgrammar.getIndex(GE.focusmetagon)-1;
+    GE.focusgrammar.discardMetagon(GE.focusmetagon);
+    if(a<0)a=0;
+    GE.focusmetagon=GE.focusgrammar.getMetagon(a);
+    refreshInfo();
+    getUI().repaint();}
   
   public void createProtoJig(){
-    GE.tasksequencer.add(new Task(){
-      public void doTask(){
-        ((G_UI)GE.editor_grammar.getUI()).panmetagonmenu.invalidateIconArrayMetrics();
-        GE.setEditor(GE.editor_createjig);}});}
+    ((G_UI)GE.editor_grammar.getUI()).panmetagonmenu.invalidateIconArrayMetrics();
+    GE.setEditor(GE.editor_jig);}
   
   public void viewProtoJigEditTags(){
-    GE.tasksequencer.add(new Task(){
-      public void doTask(){
-        ((G_UI)GE.editor_grammar.getUI()).panmetagonmenu.invalidateIconArrayMetrics();
-        GE.setEditor(GE.editor_editjigdetails);}});}
+    ((G_UI)GE.editor_grammar.getUI()).panmetagonmenu.invalidateIconArrayMetrics();
+    GE.setEditor(GE.editor_editjigdetails);}
   
   public void discardProtoJig(){
-    GE.tasksequencer.add(new Task(){
-      public void doTask(){
-        ((G_UI)GE.editor_grammar.getUI()).panjigmenu.invalidateIconArrayMetrics();
-        int a=GE.focusmetagon.getProtoJigIndex(GE.focusjig)-1;
-        GE.focusmetagon.discardProtoJig(GE.focusjig);
-        if(a<0)a=0;
-        GE.focusjig=GE.focusmetagon.getProtoJig(a);
-        refreshInfo();
-        getUI().repaint();}});}
+    ((G_UI)GE.editor_grammar.getUI()).panjigmenu.invalidateIconArrayMetrics();
+    int a=GE.focusmetagon.getProtoJigIndex(GE.focusjig)-1;
+    GE.focusmetagon.discardProtoJig(GE.focusjig);
+    if(a<0)a=0;
+    GE.focusjig=GE.focusmetagon.getJig(a);
+    refreshInfo();
+    getUI().repaint();}
 
-  public void refreshAll(){
-    GE.tasksequencer.add(new Task(){
-      public void doTask(){
-        G_UI ui=(G_UI)getUI();
-        ui.panmetagonmenu.invalidateIconArrayMetrics();
-        ui.panjigmenu.invalidateIconArrayMetrics();
-        ui.repaint();
-        refreshInfo();}});}
+  public void refreshUI(){
+    G_UI ui=(G_UI)getUI();
+    ui.panmetagonmenu.invalidateIconArrayMetrics();
+    ui.panjigmenu.invalidateIconArrayMetrics();
+    ui.repaint();
+    refreshInfo();}
   
   public void setFocusMetagon(final ProjectMetagon m){
     GE.focusmetagon=m;
@@ -131,7 +110,7 @@ public class Editor_Grammar extends Editor{
   
   public void importGrammar(){
     GrammarImportExport.importGrammar();
-    refreshAll();}
+    refreshUI();}
   
   //TODO
   private void refreshInfo(){
@@ -150,6 +129,6 @@ public class Editor_Grammar extends Editor{
     GE.focusgrammar=new ProjectGrammar();
     GE.focusmetagon=null;
     GE.focusjig=null;
-    refreshAll();}
+    refreshUI();}
 
 }
