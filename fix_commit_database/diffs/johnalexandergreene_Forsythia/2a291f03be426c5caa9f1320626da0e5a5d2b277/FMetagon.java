@@ -1,6 +1,7 @@
 package org.fleen.forsythia.grammar;
 
 import java.io.Serializable;
+import java.util.List;
 
 import org.fleen.forsythia.Forsythia;
 import org.fleen.geom_Kisrhombille.KMetagon;
@@ -45,18 +46,33 @@ public class FMetagon extends KMetagon implements Serializable,Tagged,Forsythia{
   
   private TagManager tagmanager=new TagManager();
   
-  public String[] getTags(){
-    return tagmanager.getTags();}
+  public void setTags(String... tags){
+    tagmanager.setTags(tags);}
   
-  public void setTags(String[] tags){
+  public void setTags(List<String> tags){
     tagmanager.setTags(tags);}
   
-  public boolean hasTag(String tag){
-    return tagmanager.hasTag(tag);}
+  public List<String> getTags(){
+    return tagmanager.getTags();}
   
   public boolean hasTags(String... tags){
     return tagmanager.hasTags(tags);}
   
+  public boolean hasTags(List<String> tags){
+    return tagmanager.hasTags(tags);}
+  
+  public void addTags(String... tags){
+    tagmanager.addTags(tags);}
+  
+  public void addTags(List<String> tags){
+    tagmanager.addTags(tags);}
+  
+  public void removeTags(String... tags){
+    tagmanager.removeTags(tags);}
+  
+  public void removeTags(List<String> tags){
+    tagmanager.removeTags(tags);}
+  
   /*
    * ################################
    * GENERAL PURPOSE OBJECT
