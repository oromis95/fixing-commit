@@ -11,9 +11,6 @@ public class StreamVsCollection {
         List<String> names = Arrays.asList("Java8", "Lambdas", "In", "Action");
         Stream<String> s = names.stream();
         s.forEach(System.out::println);
-        s.forEach(System.out::println); // Exception
-    
+        s.forEach(System.out::println); // Exception 
     }
-
-
 }
\ No newline at end of file
