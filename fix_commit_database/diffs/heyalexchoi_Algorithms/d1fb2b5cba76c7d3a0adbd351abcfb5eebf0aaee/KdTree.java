@@ -245,14 +245,15 @@ public class KdTree {
         return nearest(p, root, root.point);
     }
     private Point2D nearest(Point2D point, Node node, Point2D champion) {
+        if (node == null) return champion;
         // if node point is closer than champion, it becomes the new champion
         if (point.distanceTo(node.point) < point.distanceTo(champion)) {
             champion = node.point;
         }
         // only explore node if it can contain a point closer than the champion
         if (point.distanceTo(champion) > node.rect.distanceTo(point)) {
-            nearest(point, node.rightTop, champion);
-            nearest(point, node.leftBottom, champion);
+            champion = nearest(point, node.rightTop, champion);
+            champion = nearest(point, node.leftBottom, champion);
         }
         return champion;
     }
