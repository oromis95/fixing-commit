@@ -7,8 +7,6 @@ import com.github.mttkay.memento.Retain;
 import android.os.Bundle;
 import android.support.v4.app.FragmentActivity;
 
-import java.lang.Override;
-
 class RetainedActivityWithPrivateFields extends FragmentActivity implements MementoCallbacks {
 
     @Retain
