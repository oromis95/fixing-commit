@@ -25,10 +25,14 @@ import java.net.URI;
 import java.util.Arrays;
 
 import mobisocial.nfc.ConnectionHandover;
+import mobisocial.nfc.NdefFactory;
 
 
+import android.net.Uri;
+import android.nfc.FormatException;
 import android.nfc.NdefMessage;
 import android.nfc.NdefRecord;
+import android.util.Log;
 
 /**
  * <p>Implements an Ndef push handover request in which a static tag
@@ -49,17 +53,19 @@ public class NdefTcpPushHandover implements ConnectionHandover {
 
 	//@Override
 	public boolean supportsRequest(NdefRecord handoverRequest) {
-		if (handoverRequest.getTnf() != NdefRecord.TNF_ABSOLUTE_URI
-				|| !Arrays.equals(handoverRequest.getType(), NdefRecord.RTD_URI)) {
-			return false;
-		}
-		
-		String uriString = new String(handoverRequest.getPayload());
-		if (uriString.startsWith("ndef+tcp://")) {
-			return true;
-		}
-		
-		return false;
+	    short tnf = handoverRequest.getTnf();
+        if (tnf != NdefRecord.TNF_ABSOLUTE_URI && (tnf != NdefRecord.TNF_WELL_KNOWN &&
+                !Arrays.equals(handoverRequest.getType(), NdefRecord.RTD_URI))) {
+            return false;
+        }
+        Uri uri;
+        try {
+            uri= NdefFactory.parseUri(handoverRequest);
+        } catch (FormatException e) {
+            return false;
+        }
+        String scheme = uri.getScheme();
+        return (scheme != null && scheme.equals("ndef+tcp"));
 	}
 
 	@Override
@@ -67,10 +73,14 @@ public class NdefTcpPushHandover implements ConnectionHandover {
 		NdefRecord handoverRequest = handoverMessage.getRecords()[record];
 		NdefMessage outboundNdef = mNdefExchange.getForegroundNdefMessage();
 		if (outboundNdef == null) return;
-		
-		String uriString = new String(handoverRequest.getPayload());
-		URI uri = URI.create(uriString);
-		sendNdefOverTcp(uri, mNdefExchange);
+
+        try {
+            String uriString = NdefFactory.parseUri(handoverRequest).toString();
+            URI uri = URI.create(uriString);
+            sendNdefOverTcp(uri, mNdefExchange);
+        } catch (FormatException e) {
+            Log.wtf("easynfc", "Bad handover request", e);
+        }
 	}
 
 	private void sendNdefOverTcp(URI target, NdefExchangeContract ndefProxy) throws IOException {
