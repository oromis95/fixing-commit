@@ -17,6 +17,7 @@
 
 package mobisocial.nfc;
 
+import java.io.UnsupportedEncodingException;
 import java.net.URI;
 import java.net.URL;
 
@@ -31,139 +32,172 @@ import android.util.Base64;
  * @see NdefMessage
  */
 public class NdefFactory {
-	public static NdefMessage fromUri(Uri uri) {
-		try {
-			NdefRecord record = new NdefRecord(NdefRecord.TNF_ABSOLUTE_URI, NdefRecord.RTD_URI, 
-					new byte[0], uri.toString().getBytes());
-			NdefRecord[] records = new NdefRecord[] { record };
-			return new NdefMessage(records);
-		} catch (NoClassDefFoundError e) {
-			return null;
-		}
-	}
-	
-	public static NdefMessage fromUri(URI uri) {
-		try {
-			NdefRecord record = new NdefRecord(NdefRecord.TNF_ABSOLUTE_URI, NdefRecord.RTD_URI, 
-					new byte[0], uri.toString().getBytes());
-			NdefRecord[] records = new NdefRecord[] { record };
-			return new NdefMessage(records);
-		} catch (NoClassDefFoundError e) {
-			return null;
-		}
-	}
+    private static final String[] URI_PREFIXES = new String[] {
+        "",
+        "http://www.",
+        "https://www.",
+        "http://",
+        "https://",
+        "tel:",
+        "mailto:",
+        "ftp://anonymous:anonymous@",
+        "ftp://ftp.",
+        "ftps://",
+        "sftp://",
+        "smb://",
+        "nfs://",
+        "ftp://",
+        "dav://",
+        "news:",
+        "telnet://",
+        "imap:",
+        "rtsp://",
+        "urn:",
+        "pop:",
+        "sip:",
+        "sips:",
+        "tftp:",
+        "btspp://",
+        "btl2cap://",
+        "btgoep://",
+        "tcpobex://",
+        "irdaobex://",
+        "file://",
+        "urn:epc:id:",
+        "urn:epc:tag:",
+        "urn:epc:pat:",
+        "urn:epc:raw:",
+        "urn:epc:",
+        "urn:nfc:",
+    };
 
-	public static NdefMessage fromUri(String uri) {
-		try {
-			NdefRecord record = new NdefRecord(NdefRecord.TNF_ABSOLUTE_URI, NdefRecord.RTD_URI, 
-					new byte[0], uri.getBytes());
-			NdefRecord[] records = new NdefRecord[] { record };
-			return new NdefMessage(records);
-		} catch (NoClassDefFoundError e) {
-			return null;
-		}
-	}
+    public static NdefMessage fromUri(Uri uri) {
+        return fromUri(uri.toString());
+    }
 
-	public static NdefMessage fromUrl(URL url) {
-		try {
-			NdefRecord record = new NdefRecord(NdefRecord.TNF_ABSOLUTE_URI,
-					NdefRecord.RTD_URI, new byte[0], url.toString().getBytes());
-			NdefRecord[] records = new NdefRecord[] { record };
-			return new NdefMessage(records);
-		} catch (NoClassDefFoundError e) {
-			return null;
-		}
-	}
-	
-	public static NdefMessage fromMime(String mimeType, byte[] data) {
-		try {
-			NdefRecord record = new NdefRecord(NdefRecord.TNF_MIME_MEDIA,
-					mimeType.getBytes(), new byte[0], data);
-			NdefRecord[] records = new NdefRecord[] { record };
-			return new NdefMessage(records);
-		} catch (NoClassDefFoundError e) {
-			return null;
-		}
-	}
-	
-	/**
-	 * Creates an NDEF message with a single text record, with language
-	 * code "en" and the given text, encoded using UTF-8.
-	 */
-	public static NdefMessage fromText(String text) {
-		try {
-			byte[] textBytes = text.getBytes();
-			byte[] textPayload = new byte[textBytes.length + 3];
-			textPayload[0] = 0x02; // Status byte; UTF-8 and "en" encoding.
-			textPayload[1] = 'e';
-			textPayload[2] = 'n';
-			System.arraycopy(textBytes, 0, textPayload, 3, textBytes.length);
-			NdefRecord record = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
-					NdefRecord.RTD_TEXT, new byte[0], textPayload);
-			NdefRecord[] records = new NdefRecord[] { record };
-			return new NdefMessage(records);
-		} catch (NoClassDefFoundError e) {
-			return null;
-		}
-	}
-	
-	/**
-	 * Creates an NDEF message with a single text record, with the given
-	 * text content (UTF-8 encoded) and language code. 
-	 */
-	public static NdefMessage fromText(String text, String languageCode) {
-		try {
-			int languageCodeLength = languageCode.length();
-			int textLength = text.length();
-			byte[] textPayload = new byte[textLength + 1 + languageCodeLength];
-			textPayload[0] = (byte)(0x3F & languageCodeLength); // UTF-8 with the given language code length.
-			System.arraycopy(languageCode.getBytes(), 0, textPayload, 1, languageCodeLength);
-			System.arraycopy(text.getBytes(), 0, textPayload, 1 + languageCodeLength, textLength);
-			NdefRecord record = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
-					NdefRecord.RTD_TEXT, new byte[0], textPayload);
-			NdefRecord[] records = new NdefRecord[] { record };
-			return new NdefMessage(records);
-		} catch (NoClassDefFoundError e) {
-			return null;
-		}
-	}
+    public static NdefMessage fromUri(URI uri) {
+        return fromUri(uri.toString());
+    }
 
-	public static final NdefMessage getEmptyNdef() {
-		byte[] empty = new byte[] {};
-		NdefRecord[] records = new NdefRecord[1];
-		records[0] = new NdefRecord(NdefRecord.TNF_WELL_KNOWN, empty, empty, empty);
-		NdefMessage ndef = new NdefMessage(records);
-		return ndef;
-	}
+    public static NdefMessage fromUri(String uri) {
+        try {
+            int prefix = 0;
+            for (int i = 1; i < URI_PREFIXES.length; i++) {
+                if (uri.startsWith(URI_PREFIXES[i])) {
+                    prefix = i;
+                    break;
+                }
+            }
+            if (prefix > 0) uri = uri.substring(URI_PREFIXES[prefix].length());
+            int len = uri.length();
+            byte[] payload = new byte[len + 1];
+            payload[0] = (byte) prefix;
+            System.arraycopy(uri.getBytes("UTF-8"), 0, payload, 1, len);
+            NdefRecord record = new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_URI,
+                    new byte[0], payload);
+            NdefRecord[] records = new NdefRecord[] { record };
+            return new NdefMessage(records);
+        } catch (NoClassDefFoundError e) {
+            return null;
+        } catch (UnsupportedEncodingException e) {
+            // no UTF-8? really?
+            return null;
+        }
+    }
 
-	public static final boolean isEmpty(NdefMessage ndef) {
-		return  (ndef == null || ndef.equals(getEmptyNdef()));
-	}
+    public static NdefMessage fromUrl(URL url) {
+        return fromUri(url.toString());
+    }
 
-	/**
-	 * Converts an Ndef message encoded in uri format to an NdefMessage.
-	 */
-	public static final NdefMessage fromNdefUri(Uri uri) {
-		if (!"ndef".equals(uri.getScheme())) {
-			throw new IllegalArgumentException("Not an ndef:// uri. did you want NdefFactory.fromUri()?");
-		}
-		NdefMessage wrappedNdef;
-		try {
-			wrappedNdef = new NdefMessage(android.util.Base64.decode(
-         	    uri.getPath().substring(1), Base64.URL_SAFE));
-		} catch (FormatException e) {
-			throw new IllegalArgumentException("Format error.");
-		}
-		return wrappedNdef;
-	}
+    public static NdefMessage fromMime(String mimeType, byte[] data) {
+        try {
+            NdefRecord record = new NdefRecord(NdefRecord.TNF_MIME_MEDIA,
+                    mimeType.getBytes(), new byte[0], data);
+            NdefRecord[] records = new NdefRecord[] { record };
+            return new NdefMessage(records);
+        } catch (NoClassDefFoundError e) {
+            return null;
+        }
+    }
 
-	/**
-	 * Converts an Ndef message to its uri encoding, using the
-	 * {code ndef://} scheme.
-	 */
-	// TODO Switch for the appropriate type
-	/*
-	public static final Uri toNdefUri(NdefMessage ndef) {
-		return Uri.parse("ndef://url/" + Base64.encodeToString(ndef.toByteArray(), Base64.URL_SAFE));
-	}*/
+    /**
+     * Creates an NDEF message with a single text record, with language
+     * code "en" and the given text, encoded using UTF-8.
+     */
+    public static NdefMessage fromText(String text) {
+        try {
+            byte[] textBytes = text.getBytes();
+            byte[] textPayload = new byte[textBytes.length + 3];
+            textPayload[0] = 0x02; // Status byte; UTF-8 and "en" encoding.
+            textPayload[1] = 'e';
+            textPayload[2] = 'n';
+            System.arraycopy(textBytes, 0, textPayload, 3, textBytes.length);
+            NdefRecord record = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
+                    NdefRecord.RTD_TEXT, new byte[0], textPayload);
+            NdefRecord[] records = new NdefRecord[] { record };
+            return new NdefMessage(records);
+        } catch (NoClassDefFoundError e) {
+            return null;
+        }
+    }
+
+    /**
+     * Creates an NDEF message with a single text record, with the given
+     * text content (UTF-8 encoded) and language code.
+     */
+    public static NdefMessage fromText(String text, String languageCode) {
+        try {
+            int languageCodeLength = languageCode.length();
+            int textLength = text.length();
+            byte[] textPayload = new byte[textLength + 1 + languageCodeLength];
+            textPayload[0] = (byte)(0x3F & languageCodeLength); // UTF-8 with the given language code length.
+            System.arraycopy(languageCode.getBytes(), 0, textPayload, 1, languageCodeLength);
+            System.arraycopy(text.getBytes(), 0, textPayload, 1 + languageCodeLength, textLength);
+            NdefRecord record = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
+                    NdefRecord.RTD_TEXT, new byte[0], textPayload);
+            NdefRecord[] records = new NdefRecord[] { record };
+            return new NdefMessage(records);
+        } catch (NoClassDefFoundError e) {
+            return null;
+        }
+    }
+
+    public static final NdefMessage getEmptyNdef() {
+        byte[] empty = new byte[] {};
+        NdefRecord[] records = new NdefRecord[1];
+        records[0] = new NdefRecord(NdefRecord.TNF_WELL_KNOWN, empty, empty, empty);
+        NdefMessage ndef = new NdefMessage(records);
+        return ndef;
+    }
+
+    public static final boolean isEmpty(NdefMessage ndef) {
+        return  (ndef == null || ndef.equals(getEmptyNdef()));
+    }
+
+    /**
+     * Converts an Ndef message encoded in uri format to an NdefMessage.
+     */
+    public static final NdefMessage fromNdefUri(Uri uri) {
+        if (!"ndef".equals(uri.getScheme())) {
+            throw new IllegalArgumentException("Not an ndef:// uri. did you mean NdefFactory.fromUri()?");
+        }
+        NdefMessage wrappedNdef;
+        try {
+            wrappedNdef = new NdefMessage(android.util.Base64.decode(
+                uri.getPath().substring(1), Base64.URL_SAFE));
+        } catch (FormatException e) {
+            throw new IllegalArgumentException("Format error.");
+        }
+        return wrappedNdef;
+    }
+
+    /**
+     * Converts an Ndef message to its uri encoding, using the
+     * {code ndef://} scheme.
+     */
+    // TODO Switch for the appropriate type
+    /*
+    public static final Uri toNdefUri(NdefMessage ndef) {
+        return Uri.parse("ndef://url/" + Base64.encodeToString(ndef.toByteArray(), Base64.URL_SAFE));
+    }*/
 }
\ No newline at end of file
