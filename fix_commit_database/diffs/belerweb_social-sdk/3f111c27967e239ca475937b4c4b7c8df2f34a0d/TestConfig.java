@@ -4,11 +4,13 @@ import org.junit.Before;
 
 import com.belerweb.social.qq.connect.api.QQConnect;
 import com.belerweb.social.weibo.api.Weibo;
+import com.belerweb.social.weixin.api.Weixin;
 
 public class TestConfig {
 
   protected Weibo weibo;
   protected QQConnect connect;
+  protected Weixin weixin;
 
   @Before
   public void initialize() {
@@ -18,6 +20,9 @@ public class TestConfig {
     connect =
         new QQConnect(System.getProperty("connect.id"), System.getProperty("connect.secret"),
             redirectUri);
+    weixin =
+        new Weixin(System.getProperty("weixin.id"), System.getProperty("weixin.secret"),
+            redirectUri, System.getProperty("weixin.token"));
   }
 
 }
