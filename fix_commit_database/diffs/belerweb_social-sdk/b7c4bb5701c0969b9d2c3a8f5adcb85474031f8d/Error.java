@@ -35,10 +35,10 @@ public final class Error {
   }
 
   public static Error parse(JSONObject jsonObject) {
-    String errorCode = jsonObject.optString("error_code");
+    String errorCode = jsonObject.optString("error_code", null);
     if (errorCode != null) {// 微博
-      String request = jsonObject.optString("request");
-      String error = jsonObject.optString("error");
+      String request = jsonObject.optString("request", null);
+      String error = jsonObject.optString("error", null);
       Error er = new Error();
       er.setRequest(request);
       er.setErrorCode(errorCode);
@@ -46,9 +46,9 @@ public final class Error {
       return er;
     }
 
-    errorCode = jsonObject.optString("errcode");
+    errorCode = jsonObject.optString("errcode", null);
     if (errorCode != null) {// 微信
-      String error = jsonObject.optString("errmsg");
+      String error = jsonObject.optString("errmsg", null);
       Error er = new Error();
       er.setErrorCode(errorCode);
       er.setError(error);
@@ -57,15 +57,13 @@ public final class Error {
 
     Integer ret = Result.parseInteger(jsonObject.opt("ret"));
     if (ret != null && ret != 0) {// QQ互联
-      String msg = jsonObject.optString("msg");
+      String msg = jsonObject.optString("msg", null);
       Error er = new Error();
       er.setErrorCode(ret.toString());
       er.setError(msg);
       return er;
     }
 
-
-
     return null;
   }
 
