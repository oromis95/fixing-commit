@@ -304,14 +304,14 @@ public class ResumeUploader {
             return false;
         }
 
-        RequestBody requestBody = RequestBody.create(null, mFile);
+        RequestBody requestBody = RequestBody.create(null, "");
 
         String date = getGMTDate();
 
         String md5 = null;
 
         if (checkMD5) {
-            md5 = UpYunUtils.md5(mFile, BLOCK_SIZE);
+            md5 = UpYunUtils.md5("");
         }
 
         String sign = UpYunUtils.sign("PUT", date, uploadPath, bucketName, userName, password, md5);
@@ -322,7 +322,6 @@ public class ResumeUploader {
                 .header(AUTHORIZATION, sign.trim())
                 .header(X_UPYUN_MULTI_STAGE, "complete")
                 .header(X_UPYUN_MULTI_UUID, uuid)
-                .header(CONTENT_MD5, UpYunUtils.md5(mFile, BLOCK_SIZE))
                 .header("User-Agent", UpYunUtils.VERSION)
                 .put(requestBody);
 
