@@ -1,11 +1,11 @@
 package com.nearinfinity.examples.zookeeper.lock;
 
-public class DistributedOperationResult {
+public class DistributedOperationResult<T> {
 
     public final boolean timedOut;
-    public final Object result;
+    public final T result;
 
-    public DistributedOperationResult(boolean timedOut, Object result) {
+    public DistributedOperationResult(boolean timedOut, T result) {
         this.timedOut = timedOut;
         this.result = result;
     }
