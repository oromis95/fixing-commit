@@ -1,5 +1,5 @@
 package com.nearinfinity.examples.zookeeper.lock;
 
-public interface DistributedOperation {
-   Object execute() throws DistributedOperationException;
+public interface DistributedOperation<T> {
+   T execute() throws DistributedOperationException;
 }
