@@ -2,10 +2,10 @@ package com.github.toastshaman.dropwizard.auth.jwt.model;
 
 import com.fasterxml.jackson.annotation.JsonProperty;
 import com.fasterxml.jackson.databind.annotation.JsonSerialize;
-import com.google.common.base.Objects;
 import org.hibernate.validator.constraints.NotEmpty;
 
 import java.util.Map;
+import java.util.Objects;
 
 import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_NULL;
 import static com.github.toastshaman.dropwizard.auth.jwt.JsonWebTokenAlgorithms.*;
@@ -69,13 +69,13 @@ public class JsonWebTokenHeader {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         final JsonWebTokenHeader that = (JsonWebTokenHeader) o;
-        return Objects.equal(typ, that.typ) &&
-            Objects.equal(alg, that.alg);
+        return Objects.equals(typ, that.typ) &&
+            Objects.equals(alg, that.alg);
     }
 
     @Override
     public int hashCode() {
-        return Objects.hashCode(typ, alg);
+        return Objects.hash(typ, alg);
     }
 
     public static class Builder {
