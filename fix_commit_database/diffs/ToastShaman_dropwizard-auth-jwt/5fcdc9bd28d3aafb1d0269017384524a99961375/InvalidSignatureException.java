@@ -3,6 +3,6 @@ package com.github.toastshaman.dropwizard.auth.jwt.exceptions;
 public class InvalidSignatureException extends JsonWebTokenException {
 
     public InvalidSignatureException() {
-        super();
+        super("The token's signature is invalid");
     }
 }
