@@ -2,10 +2,6 @@ package com.github.toastshaman.dropwizard.auth.jwt.exceptions;
 
 public class JsonWebTokenException extends RuntimeException {
 
-    public JsonWebTokenException() {
-        super();
-    }
-
     public JsonWebTokenException(String msg) {
         super(msg);
     }
