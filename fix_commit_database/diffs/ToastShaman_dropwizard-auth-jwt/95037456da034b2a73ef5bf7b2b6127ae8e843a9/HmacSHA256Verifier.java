@@ -1,10 +1,8 @@
 package com.github.toastshaman.dropwizard.auth.jwt.hmac;
 
-import com.github.toastshaman.dropwizard.auth.jwt.JsonWebTokenAlgorithms;
 import com.github.toastshaman.dropwizard.auth.jwt.JsonWebTokenVerifier;
 import com.github.toastshaman.dropwizard.auth.jwt.model.JsonWebToken;
 
-import static com.github.toastshaman.dropwizard.auth.jwt.JsonWebTokenAlgorithms.*;
 import static com.github.toastshaman.dropwizard.auth.jwt.JsonWebTokenAlgorithms.HS256;
 import static com.google.common.base.Preconditions.checkArgument;
 import static com.google.common.base.Preconditions.checkState;
@@ -54,7 +52,7 @@ public class HmacSHA256Verifier extends KeyAware implements JsonWebTokenVerifier
      */
     @Override
     public void verifySignature(JsonWebToken token) {
-        checkArgument(token.header().alg().equals(HS256), "Can not verify a %s with a %s verifier", token.header().alg(), HS256);
+        checkArgument(token.header().algorithm().equals(HS256), "Can not verify a %s with a %s verifier", token.header().algorithm(), HS256);
         hmacVerifier.verifySignature(token);
     }
 }
