@@ -13,7 +13,7 @@ import static com.google.common.base.Preconditions.checkArgument;
  * final HmacSHA256Signer signer = new HmacSHA256Signer(bytesOf("SECRET"));
  * final JsonWebToken token = JsonWebToken.builder()
  *     .header(JsonWebTokenHeader.HS256())
- *     .claim(JsonWebTokenClaim.builder().issuer("joe").build())
+ *     .claim(JsonWebTokenClaim.builder().subject("joe").build())
  *     .build();
  *
  * final String signedToken = signer.sign(token);
@@ -47,7 +47,7 @@ public class HmacSHA256Signer extends KeyAware implements JsonWebTokenSigner {
      */
     @Override
     public String sign(JsonWebToken token) {
-        checkArgument(token.header().alg().equals(HS256), "Can not sign a %s with a %s signer", token.header().alg(), HS256);
+        checkArgument(token.header().algorithm().equals(HS256), "Can not sign a %s with a %s signer", token.header().algorithm(), HS256);
         return hmacSigner.sign(token);
     }
 }
