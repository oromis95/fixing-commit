@@ -1,6 +1,6 @@
 package com.github.toastshaman.dropwizard.auth.jwt.example;
 
-import com.github.toastshaman.dropwizard.auth.jwt.JWTAuthProvider;
+import com.github.toastshaman.dropwizard.auth.jwt.JWTAuthFactory;
 import com.github.toastshaman.dropwizard.auth.jwt.JsonWebTokenParser;
 import com.github.toastshaman.dropwizard.auth.jwt.JsonWebTokenValidator;
 import com.github.toastshaman.dropwizard.auth.jwt.exceptions.TokenExpiredException;
@@ -10,6 +10,7 @@ import com.github.toastshaman.dropwizard.auth.jwt.parser.DefaultJsonWebTokenPars
 import com.github.toastshaman.dropwizard.auth.jwt.validator.ExpiryValidator;
 import com.google.common.base.Optional;
 import io.dropwizard.Application;
+import io.dropwizard.auth.AuthFactory;
 import io.dropwizard.auth.AuthenticationException;
 import io.dropwizard.auth.Authenticator;
 import io.dropwizard.setup.Bootstrap;
@@ -31,36 +32,33 @@ public class JwtAuthApplication extends Application<MyConfiguration> {
     public void run(MyConfiguration configuration, Environment environment) throws Exception {
         final JsonWebTokenParser tokenParser = new DefaultJsonWebTokenParser();
         final HmacSHA512Verifier tokenVerifier = new HmacSHA512Verifier(configuration.getJwtTokenSecret());
-        final JsonWebTokenValidator expiryValidator = new ExpiryValidator();
-        environment.jersey().register(new JWTAuthProvider<User>(new Authenticator<JsonWebToken, User>() {
-            @Override
-            public Optional<User> authenticate(JsonWebToken token) throws AuthenticationException {
-                // Provide your own implementation to lookup users based on the principal attribute in the
-                // JWT Token. E.g.: lookup users from a database etc.
-                // This method will be called once the token's signature has been verified
+        environment.jersey().register(AuthFactory.binder(new JWTAuthFactory<>(new ExampleAuthenticator(), "realm", User.class, tokenVerifier, tokenParser)));
+        environment.jersey().register(new SecuredResource(configuration.getJwtTokenSecret()));
+    }
 
-                // In case you want to verify different parts of the token you can do that here.
-                // E.g.: Verifying that the provided token has not expired.
-                try {
-                    expiryValidator.validate(token);
-                } catch (TokenExpiredException e) {
-                    throw new AuthenticationException(e.getMessage(), e);
-                }
+    private static class ExampleAuthenticator implements Authenticator<JsonWebToken, User> {
+        @Override
+        public Optional<User> authenticate(JsonWebToken token) throws AuthenticationException {
+            final JsonWebTokenValidator expiryValidator = new ExpiryValidator();
 
-                if ("good-guy".equals(token.claim().subject())) {
-                    return Optional.of(new User("good-guy"));
-                }
+            // Provide your own implementation to lookup users based on the principal attribute in the
+            // JWT Token. E.g.: lookup users from a database etc.
+            // This method will be called once the token's signature has been verified
 
-                return Optional.absent();
+            // In case you want to verify different parts of the token you can do that here.
+            // E.g.: Verifying that the provided token has not expired.
+            try {
+                expiryValidator.validate(token);
+            } catch (TokenExpiredException e) {
+                throw new AuthenticationException(e.getMessage(), e);
             }
-        }, tokenParser, tokenVerifier, "realm"));
 
-        environment.jersey().register(new SecuredResource(configuration.getJwtTokenSecret()));
-    }
+            if ("good-guy".equals(token.claim().subject())) {
+                return Optional.of(new User("good-guy"));
+            }
 
-    @Override
-    public String getName() {
-        return "JwtAuthApplication";
+            return Optional.absent();
+        }
     }
 
     public static void main(String[] args) throws Exception {
