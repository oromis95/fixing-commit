@@ -110,28 +110,12 @@ public class ResampleOp extends AdvancedResizeOp
 
         byte[][] workPixels = new byte[srcHeight][dstWidth*nrChannels];
 
-        float xscale;
-		float yscale;
-		if (srcWidth == 1) {
-			xscale = (float)dstWidth / (float)srcWidth;
-		}
-		else {
-			xscale = (float)(dstWidth - 1) / (float)(srcWidth - 1);
-		}
-		if (srcHeight == 1) {
-			yscale = (float)dstHeight / (float)srcHeight;
-		}
-		else {
-			yscale = (float)(dstHeight - 1) / (float)(srcHeight - 1);
-		}
-
-		this.processedItems = 0;
+        this.processedItems = 0;
 		this.totalItems = srcHeight + dstWidth;
 
-
 		// Pre-calculate  sub-sampling
-		horizontalSubsamplingData = createSubSampling(srcWidth, dstWidth, xscale);
-		verticalSubsamplingData = createSubSampling(srcHeight, dstHeight, yscale);
+		horizontalSubsamplingData = createSubSampling(srcWidth, dstWidth);
+		verticalSubsamplingData = createSubSampling(srcHeight, dstHeight);
 
 
         final BufferedImage scrImgCopy = srcImg;
@@ -195,7 +179,8 @@ public class ResampleOp extends AdvancedResizeOp
 		return out;
     }
 
-	private SubSamplingData createSubSampling(int srcSize, int dstSize, float scale) {
+	private SubSamplingData createSubSampling(int srcSize, int dstSize) {
+		float scale = (float)dstSize / (float)srcSize;
 		int[] arrN= new int[dstSize];
 		int numContributors;
 		float[] arrWeight;
@@ -212,7 +197,6 @@ public class ResampleOp extends AdvancedResizeOp
 			final float fNormFac= (float)(1f / (Math.ceil(width) / fwidth));
 			//
 			for (int i= 0; i < dstSize; i++) {
-				arrN[i]= 0;
 				final int subindex= i * numContributors;
 				float center= i / scale;
 				int left= (int)Math.floor(center - width);
@@ -233,7 +217,8 @@ public class ResampleOp extends AdvancedResizeOp
 						n= j;
 					}
 					int k= arrN[i];
-					arrN[i]+= 1;
+					//assert k == j-left:String.format("%s = %s %s", k,j,left);
+					arrN[i]++;
 					if (n < 0 || n >= srcSize) {
 						weight= 0.0f;// Flag that cell should not be used
 					}
@@ -259,7 +244,6 @@ public class ResampleOp extends AdvancedResizeOp
 			arrPixel= new int[dstSize * numContributors];
 			//
 			for (int i= 0; i < dstSize; i++) {
-				arrN[i]= 0;
 				final int subindex= i * numContributors;
 				float center= i / scale;
 				int left= (int)Math.floor(center - fwidth);
@@ -278,7 +262,7 @@ public class ResampleOp extends AdvancedResizeOp
 						n= j;
 					}
 					int k= arrN[i];
-					arrN[i]+= 1;
+					arrN[i]++;
 					if (n < 0 || n >= srcSize) {
 						weight= 0.0f;// Flag that cell should not be used
 					}
