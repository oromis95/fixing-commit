@@ -85,8 +85,9 @@ public class ResampleOpSingleThread extends AdvancedResizeOp
 		this.dstWidth = dstWidth;
 		this.dstHeight = dstHeight;
 		if (srcImg.getType() == BufferedImage.TYPE_BYTE_BINARY ||
-				srcImg.getType() == BufferedImage.TYPE_BYTE_INDEXED)
-			srcImg= ImageUtils.convert(srcImg, srcImg.getColorModel().hasAlpha() ?
+				srcImg.getType() == BufferedImage.TYPE_BYTE_INDEXED ||
+				srcImg.getType() == BufferedImage.TYPE_CUSTOM)
+			srcImg = ImageUtils.convert(srcImg, srcImg.getColorModel().hasAlpha() ?
 					BufferedImage.TYPE_4BYTE_ABGR : BufferedImage.TYPE_3BYTE_BGR);
 
 		this.nrChannels= ImageUtils.nrChannels(srcImg);
@@ -94,9 +95,6 @@ public class ResampleOpSingleThread extends AdvancedResizeOp
 		this.srcWidth = srcImg.getWidth();
         this.srcHeight = srcImg.getHeight();
 
-
-
-
 		this.processedItems = 0;
 		this.totalItems = srcHeight;
 
