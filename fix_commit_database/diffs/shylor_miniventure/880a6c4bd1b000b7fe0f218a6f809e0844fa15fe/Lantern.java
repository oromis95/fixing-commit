@@ -10,8 +10,8 @@ public class Lantern extends Furniture {
 		super("Lantern"); // Name of the lantern
 		col = Color.get(-1, 000, 111, 555); // Color of the lantern
 		sprite = 5; // Location of the sprite
-		xr = 3; // Width of the lantern (in-game, not sprite) 
-		yr = 2; // Width of the lantern (in-game, not sprite) 
+		xr = 3; // Width of the lantern 
+		yr = 2; // Height of the lantern 
 	}
 
 	/** Gets the size of the radius for light underground (Bigger number, larger light) */
