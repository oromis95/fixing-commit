@@ -13,6 +13,6 @@ public class ByteMocker implements Mocker<Byte> {
 
   @Override
   public Byte mock(MockConfig mockConfig) {
-    return (byte) RandomUtils.nextInt(mockConfig.getByteRange()[0], mockConfig.getByteRange()[1] + 1);
+    return (byte) RandomUtils.nextInt(mockConfig.getByteRange()[0], mockConfig.getByteRange()[1]);
   }
 }
