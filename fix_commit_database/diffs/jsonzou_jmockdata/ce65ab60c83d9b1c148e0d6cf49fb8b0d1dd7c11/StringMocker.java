@@ -13,7 +13,7 @@ public class StringMocker implements Mocker<String> {
 
   @Override
   public String mock(MockConfig mockConfig) {
-    int size = RandomUtils.nextInt(mockConfig.getSizeRange()[0], mockConfig.getSizeRange()[1] + 1);
+    int size = RandomUtils.nextSize(mockConfig.getSizeRange()[0], mockConfig.getSizeRange()[1]);
     String[] stringSeed = mockConfig.getStringSeed();
     StringBuilder sb = new StringBuilder(size);
     for (int i = 0; i < size; i++) {
