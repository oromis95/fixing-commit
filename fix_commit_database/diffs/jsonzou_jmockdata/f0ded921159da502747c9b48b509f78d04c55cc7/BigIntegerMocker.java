@@ -11,7 +11,7 @@ import org.apache.commons.lang3.RandomUtils;
 public class BigIntegerMocker implements Mocker<BigInteger> {
 
   public static final BigIntegerMocker INSTANCE = new BigIntegerMocker();
-
+  @Override
   public BigInteger mockData(final MockConfig mockConfig) throws Exception {
     return BigInteger.valueOf(RandomUtils.nextLong(mockConfig.getLongRange()[0], mockConfig.getLongRange()[1]));
   }
