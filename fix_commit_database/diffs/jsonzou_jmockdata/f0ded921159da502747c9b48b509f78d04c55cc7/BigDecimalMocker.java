@@ -11,7 +11,7 @@ import org.apache.commons.lang3.RandomUtils;
 public class BigDecimalMocker implements Mocker<BigDecimal> {
 
   public static final BigDecimalMocker INSTANCE = new BigDecimalMocker();
-
+  @Override
   public BigDecimal mockData(final MockConfig mockConfig) throws Exception {
     return BigDecimal.valueOf(RandomUtils.nextDouble(mockConfig.getDoubleRange()[0], mockConfig.getDoubleRange()[1]));
   }
