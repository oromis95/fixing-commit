@@ -10,7 +10,7 @@ import org.apache.commons.lang3.RandomUtils;
 public class ByteMocker implements Mocker<Byte> {
 
   public static final ByteMocker INSTANCE = new ByteMocker();
-
+  @Override
   public Byte mockData(final MockConfig mockConfig) throws Exception {
     return (byte) RandomUtils.nextInt(mockConfig.getByteRange()[0], mockConfig.getByteRange()[1]);
   }
