@@ -10,7 +10,7 @@ import org.apache.commons.lang3.RandomUtils;
 public class BooleanMocker implements Mocker<Boolean> {
 
   public static final BooleanMocker INSTANCE = new BooleanMocker();
-
+  @Override
   public Boolean mockData(final MockConfig mockConfig) throws Exception {
     boolean[] booleanRange = mockConfig.getBooleanRange();
     return booleanRange[RandomUtils.nextInt(0, booleanRange.length - 1)];
