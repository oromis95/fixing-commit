@@ -12,7 +12,12 @@ public class StringMocker implements Mocker<String> {
   public static final StringMocker INSTANCE = new StringMocker();
   @Override
   public String mockData(final MockConfig mockConfig) throws Exception {
+    int size = RandomUtils.nextInt(mockConfig.getSizeRange()[0], mockConfig.getSizeRange()[1]);
     String[] stringSeed = mockConfig.getStringSeed();
-    return stringSeed[RandomUtils.nextInt(0, stringSeed.length - 1)];
+    StringBuilder sb = new StringBuilder(size);
+    for (int i = 0; i < size; i++) {
+      sb.append(stringSeed[RandomUtils.nextInt(0, stringSeed.length - 1)]);
+    }
+    return sb.toString();
   }
 }
