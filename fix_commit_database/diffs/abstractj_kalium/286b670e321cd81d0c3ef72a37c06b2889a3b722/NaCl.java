@@ -26,12 +26,7 @@ public class NaCl {
 
     public static Sodium sodium() {
         Sodium sodium = SingletonHolder.SODIUM_INSTANCE;
-
-        if(!(sodium.sodium_version_string().compareTo("1.0.3") >= 0)){
-            String message = String.format("Unsupported libsodium version: %s. Please update",
-                    sodium.sodium_version_string());
-            throw new UnsupportedOperationException(message);
-        }
+        checkVersion(sodium);
         return sodium;
     }
 
@@ -46,6 +41,25 @@ public class NaCl {
 
     }
 
+    public static final Integer[] MIN_SUPPORTED_VERSION = new Integer[] { 1, 0, 3 };
+
+    private static boolean versionSupported = false;
+
+    private static final void checkVersion(Sodium lib) {
+        if (!versionSupported) {
+            String[] version = lib.sodium_version_string().split("\\.");
+            versionSupported = version.length >= 3 &&
+                MIN_SUPPORTED_VERSION[0] <= new Integer(version[0]) &&
+                MIN_SUPPORTED_VERSION[1] <= new Integer(version[1]) &&
+                MIN_SUPPORTED_VERSION[2] <= new Integer(version[2]);
+        }
+        if (!versionSupported) {
+            String message = String.format("Unsupported libsodium version: %s. Please update",
+                                        lib.sodium_version_string());
+            throw new UnsupportedOperationException(message);
+        }
+    }
+
     private NaCl() {
     }
 
@@ -76,7 +90,6 @@ public class NaCl {
 
         public int crypto_hash_sha512(@Out byte[] buffer, @In byte[] message, @u_int64_t long sizeof);
 
-
         public static final int BLAKE2B_OUTBYTES = 64;
         public int crypto_generichash_blake2b(@Out byte[] buffer, @u_int64_t long outLen,
                                               @In byte[] message, @u_int64_t long messageLen,
