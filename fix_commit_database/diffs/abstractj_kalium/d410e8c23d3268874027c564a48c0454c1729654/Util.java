@@ -19,7 +19,10 @@ public class Util {
     }
 
     public static byte[] prependZeros(int n, byte[] message) {
-        return new byte[n + message.length];
+        byte[] result = new byte[n + message.length];
+        Arrays.fill(result, (byte) 0);
+        System.arraycopy(message, 0, result, n, message.length);
+        return result;
     }
 
     public static byte[] removeZeros(int n, byte[] message) {
