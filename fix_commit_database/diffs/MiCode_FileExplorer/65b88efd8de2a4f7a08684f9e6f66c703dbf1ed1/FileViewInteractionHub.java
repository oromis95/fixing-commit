@@ -342,8 +342,9 @@ public class FileViewInteractionHub implements IOperationProgressListener {
             String displayPath = mFileViewListener.getDisplayPath(mCurrentPath);
             boolean root = true;
             int left = 0;
-            while (pos != -1) {
+            while (pos != -1 && !displayPath.equals("/")) {//如果当前位置在根文件夹则不显示导航条
                 int end = displayPath.indexOf("/", pos);
+                if (end == 0 )end = 1;//修正根文件夹不显示的问题
                 if (end == -1)
                     break;
 
