@@ -168,7 +168,6 @@ public class FileListItem extends LinearLayout {
 
         @Override
         public void onDestroyActionMode(ActionMode mode) {
-            mFileViewInteractionHub.clearSelection();
             ((FileExplorerTabActivity) mContext).setActionMode(null);
         }
     }
