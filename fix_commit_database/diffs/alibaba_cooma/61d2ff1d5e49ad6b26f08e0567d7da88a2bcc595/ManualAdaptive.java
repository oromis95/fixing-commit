@@ -0,0 +1,34 @@
+/*
+ * Copyright 2012-2013 Cooma Team.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package com.metaframe.cooma.ext9.impl;
+
+import com.metaframe.cooma.Adaptive;
+import com.metaframe.cooma.Config;
+import com.metaframe.cooma.ExtensionLoader;
+import com.metaframe.cooma.ext9.ManualAdaptiveClassExt;
+
+/**
+ * @author Jerry Lee(oldratlee<at>gmail<dot>com)
+ */
+@Adaptive
+public class ManualAdaptive implements ManualAdaptiveClassExt {
+    public String echo(Config config, String s) {
+        ExtensionLoader<ManualAdaptiveClassExt> extensionLoader = ExtensionLoader.getExtensionLoader(ManualAdaptiveClassExt.class);
+        ManualAdaptiveClassExt extension = extensionLoader.getExtension(config.get("key"));
+        return extension.echo(config, s) + ManualAdaptive.class.getName();
+    }
+}
