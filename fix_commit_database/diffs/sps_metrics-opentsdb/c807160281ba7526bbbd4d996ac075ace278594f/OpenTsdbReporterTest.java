@@ -31,6 +31,7 @@ import java.util.*;
 import java.util.concurrent.TimeUnit;
 
 import static org.junit.Assert.assertEquals;
+import static org.junit.Assert.assertTrue;
 import static org.mockito.Mockito.*;
 
 /**
@@ -377,6 +378,31 @@ public class OpenTsdbReporterTest {
         assertEquals(0, metrics.size());
     }
 
+    @Test
+    public void testPerMetricTags() {
+
+        when(counter.getCount()).thenReturn(2L);
+        String encodedName = OpenTsdbMetric.encodeTagsInName("counter", Collections.singletonMap("foo2", "bar2"));
+        reporter.report(this.<Gauge>map(), this.map(encodedName, counter), this.<Histogram>map(), this.<Meter>map(), this.<Timer>map());
+        verify(opentsdb).send(captor.capture());
+
+        final Set<OpenTsdbMetric> metrics = captor.getValue();
+        assertEquals(1, metrics.size());
+        OpenTsdbMetric metric = metrics.iterator().next();
+        assertEquals("prefix.counter.count", metric.getMetric());
+        assertEquals((Long) timestamp, metric.getTimestamp());
+        assertEquals(2L, metric.getValue());
+
+        Map<String,String> tags = metric.getTags();
+        assertEquals(2, tags.size());
+        assertTrue(tags.containsKey("foo")); // applied to all metrics
+        assertEquals("bar", tags.get("foo"));
+        assertTrue(tags.containsKey("foo2")); // applied to just this counter
+        assertEquals("bar2", tags.get("foo2"));
+    }
+
+
+
     private <T> SortedMap<String, T> map() {
         return new TreeMap<String, T>();
     }
