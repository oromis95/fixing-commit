@@ -28,6 +28,7 @@ import java.util.concurrent.TimeUnit;
  *
  * @author Sean Scanlon <sean.scanlon@gmail.com>
  * @author Randy Buck <buck@adobe.com>
+ * @author alugowski (modified for Turn)
  */
 public class OpenTsdbReporter extends ScheduledReporter {
 
@@ -38,6 +39,9 @@ public class OpenTsdbReporter extends ScheduledReporter {
     private final Timer timeToBuildReport;
     private final Timer timeToSendReport;
 
+    private boolean decorateCounters = true;
+    private boolean decorateGauges = true;
+
     /**
      * Returns a new {@link Builder} for {@link OpenTsdbReporter}.
      *
@@ -62,6 +66,8 @@ public class OpenTsdbReporter extends ScheduledReporter {
         private MetricFilter filter;
         private Map<String, String> tags;
         private int batchSize;
+        private boolean decorateCounters;
+        private boolean decorateGauges;
 
         private Builder(MetricRegistry registry) {
             this.registry = registry;
@@ -71,6 +77,8 @@ public class OpenTsdbReporter extends ScheduledReporter {
             this.durationUnit = TimeUnit.MILLISECONDS;
             this.filter = MetricFilter.ALL;
             this.batchSize = OpenTsdb.DEFAULT_BATCH_SIZE_LIMIT;
+            this.decorateCounters = true;
+            this.decorateGauges = true;
         }
 
         /**
@@ -132,18 +140,31 @@ public class OpenTsdbReporter extends ScheduledReporter {
          * Append tags to all reported metrics
          *
          * @param tags
-         * @return
+         * @return {@code this}
          */
         public Builder withTags(Map<String, String> tags) {
             this.tags = tags;
             return this;
         }
 
+        /**
+         * Enable decorating Counter metric names with {@code .count} and Gauge metric names with
+         * {@code .value}.
+         *
+         * @param withCounterGaugeDecorations
+         * @return {@code this}
+         */
+        public Builder withCounterGaugeDecorations(boolean withCounterGaugeDecorations) {
+            this.decorateCounters = withCounterGaugeDecorations;
+            this.decorateGauges = withCounterGaugeDecorations;
+            return this;
+        }
+
         /**
          * specify number of metrics send in each request
          *
          * @param batchSize
-         * @return
+         * @return {@code this}
          */
         public Builder withBatchSize(int batchSize) {
             this.batchSize = batchSize;
@@ -165,7 +186,7 @@ public class OpenTsdbReporter extends ScheduledReporter {
                     prefix,
                     rateUnit,
                     durationUnit,
-                    filter, tags);
+                    filter, tags, decorateCounters, decorateGauges);
         }
     }
 
@@ -198,7 +219,7 @@ public class OpenTsdbReporter extends ScheduledReporter {
         }
     }
 
-    private OpenTsdbReporter(MetricRegistry registry, OpenTsdb opentsdb, Clock clock, String prefix, TimeUnit rateUnit, TimeUnit durationUnit, MetricFilter filter, Map<String, String> tags) {
+    private OpenTsdbReporter(MetricRegistry registry, OpenTsdb opentsdb, Clock clock, String prefix, TimeUnit rateUnit, TimeUnit durationUnit, MetricFilter filter, Map<String, String> tags, boolean decorateCounters, boolean decorateGauges) {
         super(registry, "opentsdb-reporter", filter, rateUnit, durationUnit);
         this.opentsdb = opentsdb;
         this.clock = clock;
@@ -206,6 +227,8 @@ public class OpenTsdbReporter extends ScheduledReporter {
         this.tags = tags;
         this.timeToSendReport = registry.timer("open-tsdb-reporter-time-to-send-report");
         this.timeToBuildReport = registry.timer("open-tsdb-reporter-time-to-build-report");
+        this.decorateCounters = decorateCounters;
+        this.decorateGauges = decorateGauges;
     }
 
     @Override
@@ -348,16 +371,15 @@ public class OpenTsdbReporter extends ScheduledReporter {
     }
 
     private OpenTsdbMetric buildCounter(String name, Counter counter, long timestamp, Map<String, String> tags) {
-        return OpenTsdbMetric.named(prefix(name, "count"))
+        return OpenTsdbMetric.named(decorateCounters ? prefix(name, "count") : prefix(name))
                 .withTimestamp(timestamp)
                 .withValue(counter.getCount())
                 .withTags(tags)
                 .build();
     }
 
-
     private OpenTsdbMetric buildGauge(String name, Gauge gauge, long timestamp, Map<String, String> tags) {
-        return OpenTsdbMetric.named(prefix(name, "value"))
+        return OpenTsdbMetric.named(decorateGauges ? prefix(name, "value") : prefix(name))
                 .withValue(gauge.getValue())
                 .withTimestamp(timestamp)
                 .withTags(tags)
@@ -365,7 +387,10 @@ public class OpenTsdbReporter extends ScheduledReporter {
     }
 
     private String prefix(String... components) {
-        return MetricRegistry.name(prefix, components);
+        if (prefix.length() == 0)
+            return MetricRegistry.name(prefix, components);
+        else
+            return OpenTsdbMetric.fixEncodedTagsInNameAfterPrefix(MetricRegistry.name(prefix, components));
     }
 
 }
