@@ -32,9 +32,9 @@ import org.glassfish.jersey.client.ClientProperties;
 import org.glassfish.jersey.jackson.JacksonFeature;
 
 /**
- * OpenTSDB 2.0 jersey based REST client
- * <p/>
- * {@link http://opentsdb.net/docs/build/html/api_http/index.html#version-1-x-to-2-x}
+ * OpenTSDB 2.0 jersey based REST client.
+ *
+ * {@link <a href="http://opentsdb.net/docs/build/html/api_http/index.html#version-1-x-to-2-x">HTTP API</a>}
  *
  * @author Sean Scanlon <sean.scanlon@gmail.com>
  */
@@ -49,7 +49,7 @@ public class OpenTsdb {
      * Initiate a client Builder with the provided base opentsdb server url.
      *
      * @param baseUrl
-     * @return
+     * @return a {@link Builder}
      */
     public static Builder forService(String baseUrl) {
         return new Builder(baseUrl);
@@ -99,6 +99,13 @@ public class OpenTsdb {
         }
     }
 
+	/**
+	 * For OpenTsdbTelnet
+	 */
+	protected OpenTsdb() {
+		this.apiResource = null;
+	}
+
     private OpenTsdb(WebTarget apiResource) {
         this.apiResource = apiResource;
     }
