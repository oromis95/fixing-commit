@@ -144,7 +144,7 @@ public class OpenTsdbReporterTest {
         assertEquals(histMap.get("prefix.histogram.min"), 4L);
 
         assertEquals(histMap.get("prefix.histogram.stddev"), 5.0);
-        assertEquals(histMap.get("prefix.histogram.p50"), 6.0);
+        assertEquals(histMap.get("prefix.histogram.median"), 6.0);
         assertEquals(histMap.get("prefix.histogram.p75"), 7.0);
         assertEquals(histMap.get("prefix.histogram.p95"), 8.0);
         assertEquals(histMap.get("prefix.histogram.p98"), 9.0);
@@ -179,7 +179,7 @@ public class OpenTsdbReporterTest {
         verify(opentsdb).send(captor.capture());
 
         final Set<OpenTsdbMetric> metrics = captor.getValue();
-        assertEquals(11, metrics.size());
+        assertEquals(15, metrics.size());
 
         final OpenTsdbMetric metric = metrics.iterator().next();
         assertEquals((Long) timestamp, metric.getTimestamp());
@@ -190,18 +190,23 @@ public class OpenTsdbReporterTest {
         }
 
         assertEquals(timerMap.get("prefix.timer.count"), 1L);
-        assertEquals(timerMap.get("prefix.timer.max"), 2L);
-        assertEquals(timerMap.get("prefix.timer.mean"), 3.0);
-        assertEquals(timerMap.get("prefix.timer.min"), 4L);
-
-        assertEquals(timerMap.get("prefix.timer.stddev"), 5.0);
-        assertEquals(timerMap.get("prefix.timer.p50"), 6.0);
-        assertEquals(timerMap.get("prefix.timer.p75"), 7.0);
-        assertEquals(timerMap.get("prefix.timer.p95"), 8.0);
-        assertEquals(timerMap.get("prefix.timer.p98"), 9.0);
-        assertEquals(timerMap.get("prefix.timer.p99"), 10.0);
-        assertEquals(timerMap.get("prefix.timer.p999"), 11.0);
-
+        assertEquals(timerMap.get("prefix.timer.max"), 2E-6);
+        assertEquals(timerMap.get("prefix.timer.mean"), 3.0E-6);
+        assertEquals(timerMap.get("prefix.timer.min"), 4E-6);
+
+        assertEquals((Double) timerMap.get("prefix.timer.stddev"), 5.0E-6, 0.0001);
+        assertEquals((Double) timerMap.get("prefix.timer.p75"), 7.0E-6, 0.0001);
+        assertEquals((Double) timerMap.get("prefix.timer.p95"), 8.0E-6, 0.0001);
+        assertEquals((Double) timerMap.get("prefix.timer.p98"), 9.0E-6, 0.0001);
+        assertEquals((Double) timerMap.get("prefix.timer.p99"), 10.0E-6, 0.0001);
+        assertEquals((Double) timerMap.get("prefix.timer.p999"), 11.0E-6, 0.0001);
+
+        assertEquals((Double) timerMap.get("prefix.timer.median"), 6.0E-6, 0.0001);
+        assertEquals((Double) timerMap.get("prefix.timer.p75"), 7.0E-6, 0.0001);
+        assertEquals((Double) timerMap.get("prefix.timer.p95"), 8.0E-6, 0.0001);
+        assertEquals((Double) timerMap.get("prefix.timer.p98"), 9.0E-6, 0.0001);
+        assertEquals((Double) timerMap.get("prefix.timer.p99"), 10.0E-6, 0.0001);
+        assertEquals((Double) timerMap.get("prefix.timer.p999"), 1.1E-5, 0.0001);
     }
 
     private <T> SortedMap<String, T> map() {
