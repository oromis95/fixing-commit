@@ -2,11 +2,18 @@ package org.mpilone.hazelcastmq.jms;
 
 import javax.jms.Destination;
 
+/**
+ * A JMS destination with a name and a prefix. The name is the queue or topic
+ * name in Hazelcast while the prefix indicates if the destination is a queue or
+ * topic such as "/queue/" or "/topic/".
+ *
+ * @author mpilone
+ */
 class HazelcastMQJmsDestination implements Destination {
 
-  private String destinationName;
+  private final String destinationName;
 
-  private String destinationPrefix;
+  private final String destinationPrefix;
 
   /**
    * @param destinationName
