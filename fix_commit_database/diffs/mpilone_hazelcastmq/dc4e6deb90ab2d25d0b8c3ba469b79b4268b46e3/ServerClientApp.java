@@ -5,10 +5,8 @@ import java.util.concurrent.TimeUnit;
 import org.mpilone.yeti.Frame;
 import org.mpilone.yeti.FrameBuilder;
 import org.mpilone.yeti.client.StompClient;
-import org.mpilone.yeti.client.StompClientBuilder;
 import org.mpilone.yeti.server.InMemoryBrokerStomplet;
 import org.mpilone.yeti.server.StompServer;
-import org.mpilone.yeti.server.StompServerBuilder;
 
 /**
  *
@@ -20,21 +18,19 @@ public class ServerClientApp {
 
     int port = 8090;
 
-    StompServer server = StompServerBuilder.port(port).frameDebug(true).
-        stompletClass(InMemoryBrokerStomplet.class).build();
+    StompServer server = new StompServer(false, port,
+        new StompServer.ClassStompletFactory(InMemoryBrokerStomplet.class));
     server.start();
 
     StompClient.QueuingFrameListener msgListener =
         new StompClient.QueuingFrameListener();
 
-    StompClient client1 =
-        StompClientBuilder.port(port).host("localhost").frameDebug(true).build();
+    StompClient client1 = new StompClient(true, "localhost", port);
     client1.connect();
     client1.subscribe(FrameBuilder.subscribe("foo.bar", "client1-1").build(),
         msgListener);
 
-    StompClient client2 = StompClientBuilder.port(port).host("localhost").
-        frameDebug(true).build();
+    StompClient client2 = new StompClient(true, "localhost", port);
     client2.connect();
     client2.send(FrameBuilder.send("foo.bar", "Hello").build());
     client2.send(FrameBuilder.send("foo.poo", "Goodbye").build());
