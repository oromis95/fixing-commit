@@ -1,8 +1,7 @@
 package org.mpilone.hazelcastmq.core;
 
 import java.io.Serializable;
-import java.util.HashMap;
-import java.util.Set;
+import java.util.*;
 
 /**
  * Default implementation of HazelcastMQ message headers.
@@ -17,31 +16,21 @@ class DefaultHeaders extends HashMap<String, String> implements Headers,
    */
   private static final long serialVersionUID = 1L;
 
-  /*
-   * (non-Javadoc)
-   * 
-   * @see org.mpilone.hazelcastmq.core.Headers#get(java.lang.String)
-   */
   @Override
   public String get(String headerName) {
     return super.get(headerName);
   }
 
-  /*
-   * (non-Javadoc)
-   * 
-   * @see org.mpilone.hazelcastmq.core.Headers#getHeaderNames()
-   */
   @Override
-  public Set<String> getHeaderNames() {
+  public Collection<String> getHeaderNames() {
     return keySet();
   }
 
-  /*
-   * (non-Javadoc)
-   * 
-   * @see org.mpilone.hazelcastmq.core.Headers#remove(java.lang.String)
-   */
+  @Override
+  public Map<String, String> getHeaderMap() {
+    return Collections.unmodifiableMap(this);
+  }
+
   @Override
   public void remove(String headerName) {
     super.remove(headerName);
