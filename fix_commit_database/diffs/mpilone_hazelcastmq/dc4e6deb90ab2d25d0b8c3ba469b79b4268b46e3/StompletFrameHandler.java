@@ -8,13 +8,36 @@ import io.netty.buffer.Unpooled;
 import io.netty.channel.*;
 
 /**
+ * <p>
+ * A channel handler that manages the lifecycle of a {@link Stomplet} and
+ * delegates all incoming frames to the stomplet via the
+ * {@link Stomplet#service(org.mpilone.yeti.Stomplet.StompletRequest, org.mpilone.yeti.Stomplet.StompletResponse)}
+ * method. This is a terminal channel handler so it should appear last in the
+ * pipeline.
+ * </p>
+ * <p>
+ * Any exceptions raised by the stomplet (or pipeline) will be relayed to the
+ * remote connection as an ERROR frame. If the exception implements
+ * {@link StompClientException} the message and details of the exception will be
+ * used in the frame otherwise a generic "Internal Server Error" message will be
+ * used.
+ * </p>
  *
  * @author mpilone
  */
 public class StompletFrameHandler extends SimpleChannelInboundHandler<Frame> {
 
+  /**
+   * The stomplet that all frames will be delegated to.
+   */
   private final Stomplet stomplet;
 
+  /**
+   * Constructs the handler which will delegate all frame handling to the
+   * Stomplet.
+   *
+   * @param stomplet the stomp that will service all requests
+   */
   public StompletFrameHandler(Stomplet stomplet) {
     super(Frame.class, true);
 
@@ -78,7 +101,6 @@ public class StompletFrameHandler extends SimpleChannelInboundHandler<Frame> {
       fb.body("Internal server error.");
 
       // TODO: log this?
-      cause.printStackTrace();
     }
 
     fb.headerContentTypeText();
@@ -88,22 +110,39 @@ public class StompletFrameHandler extends SimpleChannelInboundHandler<Frame> {
       ctx.close();
   }
 
-  private StompClientException unwrapClientException(Throwable cause) {
-    if (cause == null) {
+  /**
+   * Wraps the given extension looking for a {@link StompClientException} to
+   * determine if the error message and details should be relayed to the client.
+   *
+   * @param ex the exception to unwrap
+   *
+   * @return the stomp client exception or null if one is not found in the
+   * exception stack
+   */
+  private StompClientException unwrapClientException(Throwable ex) {
+    if (ex == null) {
       return null;
     }
-    else if (cause instanceof StompClientException) {
-      return (StompClientException) cause;
+    else if (ex instanceof StompClientException) {
+      return (StompClientException) ex;
     }
     else {
-      return unwrapClientException(cause.getCause());
+      return unwrapClientException(ex.getCause());
     }
   }
 
+  /**
+   * The implementation of the stomplet request.
+   */
   protected class StompletRequestImpl implements Stomplet.StompletRequest {
 
     private final Frame frame;
 
+    /**
+     * Constructs the request.
+     *
+     * @param frame the frame to be serviced
+     */
     public StompletRequestImpl(Frame frame) {
       this.frame = frame;
     }
@@ -114,11 +153,19 @@ public class StompletFrameHandler extends SimpleChannelInboundHandler<Frame> {
     }
   }
 
+  /**
+   * The implementation of the stomplet response.
+   */
   protected class StompletResponseImpl implements Stomplet.StompletResponse {
 
     private boolean finalResponse = false;
     private final Stomplet.WritableFrameChannel writableFrameChannel;
 
+    /**
+     * Constructs the response which will operate on the given channel.
+     *
+     * @param channel the underlying channel
+     */
     public StompletResponseImpl(final Channel channel) {
       this.writableFrameChannel = new Stomplet.WritableFrameChannel() {
 
@@ -139,6 +186,12 @@ public class StompletFrameHandler extends SimpleChannelInboundHandler<Frame> {
       this.finalResponse = finalResponse;
     }
 
+    /**
+     * Returns true if this should be the final response and the remote
+     * connection should be closed.
+     *
+     * @return true to close the connection
+     */
     public boolean isFinalResponse() {
       return finalResponse;
     }
