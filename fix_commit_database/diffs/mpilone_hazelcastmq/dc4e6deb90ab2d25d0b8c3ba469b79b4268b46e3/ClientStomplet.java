@@ -10,22 +10,21 @@ import org.mpilone.yeti.Stomplet.StompletResponse;
 
 
 /**
+ * A client side implementation of {@link Stomplet} that delegates client frames
+ * to handler methods. If the frame was detected to be malformed or the frame
+ * command is not valid for a client to server request, a {@link StompException}
+ * will be raised.
  *
  * @author mpilone
  */
 public class ClientStomplet extends GenericStomplet {
 
- 
-
   @Override
   public void service(StompletRequest req, StompletResponse res) throws
       Exception {
 
     Frame frame = req.getFrame();
 
-    //
-    // TODO: handle null frame or null command for heartbeat?
-    //
     // Check for a bad frame from the client.
     if (frame.getHeaders().getHeaderNames().contains(
         StompFrameDecoder.HEADER_BAD_REQUEST)) {
@@ -61,21 +60,69 @@ public class ClientStomplet extends GenericStomplet {
     // no op
   }
 
+  /**
+   * Called by the stomplet (via the {@code service} method) to allow a stomplet
+   * to handle a {@link Command#CONNECTED} frame. The default implementation
+   * does nothing.
+   *
+   * @param req the {@link StompletRequest} object that contains the frame to be
+   * serviced
+   * @param res the {@link StompletResponse} object that contains the frame
+   * channel to write to
+   *
+   * @throws Exception if an error occurs while handling the request
+   */
   protected void doConnected(StompletRequest req, StompletResponse res) throws
       Exception {
     // no op
   }
 
+  /**
+   * Called by the stomplet (via the {@code service} method) to allow a stomplet
+   * to handle a {@link Command#MESSAGE} frame. The default implementation does
+   * nothing.
+   *
+   * @param req the {@link StompletRequest} object that contains the frame to be
+   * serviced
+   * @param res the {@link StompletResponse} object that contains the frame
+   * channel to write to
+   *
+   * @throws Exception if an error occurs while handling the request
+   */
   protected void doMessage(StompletRequest req, StompletResponse res) throws
       Exception {
     // no op
   }
 
+  /**
+   * Called by the stomplet (via the {@code service} method) to allow a stomplet
+   * to handle a {@link Command#ERROR} frame. The default implementation does
+   * nothing.
+   *
+   * @param req the {@link StompletRequest} object that contains the frame to be
+   * serviced
+   * @param res the {@link StompletResponse} object that contains the frame
+   * channel to write to
+   *
+   * @throws Exception if an error occurs while handling the request
+   */
   protected void doError(StompletRequest req, StompletResponse res) throws
       Exception {
     // no op
   }
 
+  /**
+   * Called by the stomplet (via the {@code service} method) to allow a stomplet
+   * to handle a {@link Command#RECEIPT} frame. The default implementation does
+   * nothing.
+   *
+   * @param req the {@link StompletRequest} object that contains the frame to be
+   * serviced
+   * @param res the {@link StompletResponse} object that contains the frame
+   * channel to write to
+   *
+   * @throws Exception if an error occurs while handling the request
+   */
   protected void doReceipt(StompletRequest req, StompletResponse res) throws
       Exception {
     // no op
