@@ -0,0 +1,16 @@
+
+package org.mpilone.hazelcastmq.core;
+
+import java.nio.charset.Charset;
+
+/**
+ * Constants used in HazelcastMQ.
+ *
+ * @author mpilone
+ */
+public class HazelcastMQConstants {
+  /**
+   * The UTF-8 character set.
+   */
+  public static final Charset UTF_8 = Charset.forName("UTF-8");
+}
