@@ -8,34 +8,21 @@ import java.util.concurrent.atomic.AtomicLong;
 import org.mpilone.hazelcastmq.core.HazelcastMQInstance;
 
 /**
- * The configuration of the stomper server.
- * 
+ * The configuration of the STOMP server.
+  * 
  * @author mpilone
  */
 public class HazelcastMQStompConfig {
-  /**
-   * The port to which the server will bind to listen for incoming connections.
-   */
   private int port;
-
-  /**
-   * The JMS connection factory to use for all message consumers and producers.
-   */
+  private boolean frameDebugEnabled;
   private HazelcastMQInstance hazelcastMQInstance;
-
-  /**
-   * The executor service to spin up the server and client threads.
-   */
-  private ExecutorService executor;
-
-  /**
-   * The frame converter used to convert STOMP frames into JMS messages.
-   */
+  private ExecutorService executor; // may not be needed anymore
   private FrameConverter frameConverter;
 
   /**
-   * Constructs a configuration which will not have a connection factory set. A
-   * connection factory must be set before constructing a stomper instance.
+   * Constructs a configuration which will not have an MQ instance set. A MQ
+   * instance must be set before constructing a {@link HazelcastMQStomp}
+   * instance.
    */
   public HazelcastMQStompConfig() {
     this(null);
@@ -47,6 +34,7 @@ public class HazelcastMQStompConfig {
    * <li>port: 8032</li>
    * <li>frameConverter: {@link DefaultFrameConverter}</li>
    * <li>executor: {@link Executors#newCachedThreadPool()}</li>
+   * <li>frameDebugEnabled: false</li>
    * </ul>
    * 
    * @param connectionFactory
@@ -74,36 +62,61 @@ public class HazelcastMQStompConfig {
   }
 
   /**
-   * Returns the port to which the server will bind to listen for incoming
+   * Sets the port to which the server will bind to listen for incoming
    * connections.
-   * 
-   * @param port
-   *          the port number
+   *
+   * @param port the port number
    */
   public void setPort(int port) {
     this.port = port;
   }
 
+  /**
+   * Returns the port to which the server will bind to listen for incoming
+   * connections.
+   *
+   * @return the port number
+   */
   public int getPort() {
     return port;
   }
 
   /**
-   * Returns the JMS connection factory to use for all message consumers and
-   * producers.
+   * Sets the flag which indicates if frame debugging is enabled at the STOMP
+   * frame handling level. The default is false.
+   *
+   * @param frameDebugEnabled true to enable
+   */
+  public void setFrameDebugEnabled(boolean frameDebugEnabled) {
+    this.frameDebugEnabled = frameDebugEnabled;
+  }
+
+  /**
+   * Returns the flag which indicates if frame debugging is enabled at the STOMP
+   * frame handling level. The default is false.
+   *
+   * @return true if enabled
+   */
+  public boolean isFrameDebugEnabled() {
+    return frameDebugEnabled;
+  }
+
+  /**
+   * Returns the MQ instance to use for all message consumers and   * producers.
    * 
-   * @return the connection factory to access JMS
+   * @return the MQ instance
    */
   public HazelcastMQInstance getHazelcastMQInstance() {
     return hazelcastMQInstance;
   }
 
   /**
-   * @param connectionFactory
-   *          the connectionFactory to set
+   * Sets the MQ instance to use for all message consumers and producers.
+   *
+   * @param mqInstance the MQ instance
    */
-  public void setHazelcastMQInstance(HazelcastMQInstance connectionFactory) {
-    this.hazelcastMQInstance = connectionFactory;
+  public void setHazelcastMQInstance(HazelcastMQInstance mqInstance) {
+    this.hazelcastMQInstance = mqInstance;
   }
 
   /**
@@ -116,6 +129,8 @@ public class HazelcastMQStompConfig {
   }
 
   /**
+   * Sets executor service to spin up the server and client threads.
+   *
    * @param executor
    *          the executor to set
    */
@@ -133,6 +148,9 @@ public class HazelcastMQStompConfig {
   }
 
   /**
+   * Sets the the frame converter used to convert STOMP frames into JMS
+   * messages.
+   *
    * @param frameConverter
    *          the frameConverter to set
    */
