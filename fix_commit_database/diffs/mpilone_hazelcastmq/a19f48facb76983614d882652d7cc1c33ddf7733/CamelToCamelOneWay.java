@@ -13,6 +13,8 @@ import com.hazelcast.config.Config;
 import com.hazelcast.core.*;
 
 /**
+ * An example of using the {@link HazelcastMQCamelComponent} to consume a
+ * message from one HzMq queue and produce it to another in a one way operation.
  *
  * @author mpilone
  */
