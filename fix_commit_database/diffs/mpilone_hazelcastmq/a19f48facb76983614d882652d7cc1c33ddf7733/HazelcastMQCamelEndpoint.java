@@ -5,9 +5,18 @@ import org.apache.camel.Consumer;
 import org.apache.camel.Processor;
 import org.apache.camel.Producer;
 import org.apache.camel.impl.DefaultEndpoint;
+import org.mpilone.hazelcastmq.core.HazelcastMQ;
 import org.mpilone.hazelcastmq.core.HazelcastMQContext;
 
 /**
+ * <p>
+ * An Apache Camel endpoint for creating consumers and producers on
+ * {@link HazelcastMQ} destinations.
+ * </p>
+ * <p>
+ * Refer to the {@link HazelcastMQCamelConfig} class for details on
+ * configuration and supported URL parameters.
+ * </p>
  *
  * @author mpilone
  */
@@ -16,6 +25,15 @@ public class HazelcastMQCamelEndpoint extends DefaultEndpoint {
   private final HazelcastMQCamelConfig configuration;
   private final HazelcastMQContext mqContext;
 
+  /**
+   * Constructs the endpoint.
+   *
+   * @param uri the original endpoint URI
+   * @param component the component that this endpoint belongs to
+   * @param config the endpoint specific configuration
+   * @param destination the HzMq message destination for all producers and
+   * consumers created by this endpoint
+   */
   HazelcastMQCamelEndpoint(String uri, HazelcastMQCamelComponent component,
       HazelcastMQCamelConfig config, String destination) {
     super(uri, component);
@@ -61,10 +79,21 @@ public class HazelcastMQCamelEndpoint extends DefaultEndpoint {
     return true;
   }
 
+  /**
+   * Returns the endpoint specific configuration.
+   *
+   * @return the configuration
+   */
   HazelcastMQCamelConfig getConfiguration() {
     return configuration;
   }
 
+  /**
+   * Returns the HzMq message destination for all producers and consumers
+   * created by this endpoint.
+   *
+   * @return the destination
+   */
   String getDestination() {
     return destination;
   }
