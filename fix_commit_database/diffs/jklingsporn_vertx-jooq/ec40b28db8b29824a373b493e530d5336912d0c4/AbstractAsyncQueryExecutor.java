@@ -1,12 +1,19 @@
-package io.github.jklingsporn.vertx.jooq.shared.internal.async;
+package io.github.jklingsporn.vertx.jooq.shared.async;
 
+import io.github.jklingsporn.vertx.jooq.shared.internal.async.AsyncQueryExecutor;
 import io.vertx.core.json.JsonArray;
 import io.vertx.core.json.JsonObject;
 import io.vertx.core.logging.Logger;
 import io.vertx.core.logging.LoggerFactory;
+import org.joda.time.DateTimeZone;
 import org.jooq.*;
 import org.jooq.conf.ParamType;
 
+import java.time.LocalDate;
+import java.time.LocalDateTime;
+import java.time.ZonedDateTime;
+import java.time.temporal.ChronoField;
+import java.util.ArrayList;
 import java.util.Map;
 import java.util.function.UnaryOperator;
 import java.util.stream.Collectors;
@@ -16,7 +23,7 @@ import java.util.stream.Collectors;
  * @param <FIND_ONE_JSON> a type to represent one <code>JsonObject</code>.
  * @param <EXECUTE> the result type returned for all insert, update and delete-operations. This varies on the AsyncQueryExecutor-subtypes, e.g. {@code Future<Integer>}.
  */
-public abstract class AbstractAsyncQueryExecutor<FIND_MANY_JSON, FIND_ONE_JSON, EXECUTE> implements AsyncQueryExecutor<FIND_MANY_JSON, FIND_ONE_JSON, EXECUTE>{
+public abstract class AbstractAsyncQueryExecutor<FIND_MANY_JSON, FIND_ONE_JSON, EXECUTE> implements AsyncQueryExecutor<FIND_MANY_JSON, FIND_ONE_JSON, EXECUTE> {
 
     private static final Logger logger = LoggerFactory.getLogger(AbstractAsyncQueryExecutor.class);
 
@@ -47,21 +54,18 @@ public abstract class AbstractAsyncQueryExecutor<FIND_MANY_JSON, FIND_ONE_JSON,
     }
 
     protected JsonArray getBindValues(Query query) {
-        JsonArray bindValues = new JsonArray();
+        ArrayList<Object> bindValues = new ArrayList<>();
         for (Param<?> param : query.getParams().values()) {
             Object value = convertToDatabaseType(param);
-            if(value==null){
-                bindValues.addNull();
-            }else{
-                bindValues.add(value);
-            }
+            bindValues.add(value);
         }
-        return bindValues;
+        return new JsonArray(bindValues);
     }
 
 
-    protected static <T> Object convertToDatabaseType(Param<T> param) {
-        return param.getBinding().converter().to(param.getValue());
+
+    protected <T> Object convertToDatabaseType(Param<T> param) {
+        return convertJodaTimeTypes(param.getBinding().converter().to(param.getValue()));
     }
 
     protected void log(Query query){
@@ -70,4 +74,25 @@ public abstract class AbstractAsyncQueryExecutor<FIND_MANY_JSON, FIND_ONE_JSON,
         }
     }
 
+    /**
+     * Async-driver uses joda-time instead of java-time, so we need to convert it.
+     * @param object the object to convert
+     * @return a joda-time representation of the object or the object itself
+     * @see <a href="https://github.com/jklingsporn/vertx-jooq/issues/31">#31</a>
+     * @see <a href="https://github.com/vert-x3/vertx-mysql-postgresql-client/blob/master/src/main/java/io/vertx/ext/asyncsql/impl/ScalaUtils.java">ScalaUtils#convertValue</a>
+     */
+    protected Object convertJodaTimeTypes(Object object){
+        if(object instanceof LocalDateTime){
+            LocalDateTime convert = (LocalDateTime) object;
+            return new org.joda.time.LocalDateTime(convert.getYear(),convert.getMonthValue(),convert.getDayOfMonth(),convert.getHour(),convert.getMinute(),convert.getSecond(), convert.get(ChronoField.MILLI_OF_SECOND));
+        }else if(object instanceof LocalDate){
+            LocalDate convert = (LocalDate) object;
+            return new org.joda.time.LocalDate(convert.getYear(),convert.getMonthValue(),convert.getDayOfMonth());
+        }else if(object instanceof ZonedDateTime){
+            ZonedDateTime convert = (ZonedDateTime) object;
+            return new org.joda.time.DateTime(convert.getYear(),convert.getMonthValue(),convert.getDayOfMonth(),convert.getHour(),convert.getMinute(),convert.getSecond(), convert.get(ChronoField.MILLI_OF_SECOND), DateTimeZone.forID(convert.getZone().getId()));
+        }
+        return object;
+    }
+
 }
