@@ -11,6 +11,7 @@ import io.github.jklingsporn.vertx.jooq.shared.internal.async.AbstractAsyncVertx
 import io.vertx.core.json.JsonArray;
 import io.vertx.core.json.JsonObject;
 
+import java.time.LocalDateTime;
 import java.util.List;
 
 import javax.annotation.Generated;
@@ -108,6 +109,13 @@ public class SomethingDao extends AbstractAsyncVertxDAO<SomethingRecord, generat
         return findManyByCondition(Something.SOMETHING.SOMEJSONARRAY.in(values));
     }
 
+    /**
+     * Find records that have <code>someTimestamp IN (values)</code> asynchronously
+     */
+    public Single<List<generated.rx.async.regular.tables.pojos.Something>> findManyBySometimestamp(List<LocalDateTime> values) {
+        return findManyByCondition(Something.SOMETHING.SOMETIMESTAMP.in(values));
+    }
+
     @Override
     protected java.util.function.Function<Object,Integer> keyConverter(){
         return lastId -> Integer.valueOf(((Long)lastId).intValue());
