@@ -9,6 +9,8 @@ import generated.rx.async.regular.tables.interfaces.ISomething;
 import io.vertx.core.json.JsonArray;
 import io.vertx.core.json.JsonObject;
 
+import java.time.LocalDateTime;
+
 import javax.annotation.Generated;
 
 
@@ -25,17 +27,18 @@ import javax.annotation.Generated;
 @SuppressWarnings({ "all", "unchecked", "rawtypes" })
 public class Something implements ISomething {
 
-    private static final long serialVersionUID = -225463509;
+    private static final long serialVersionUID = -979845342;
 
-    private Integer    someid;
-    private String     somestring;
-    private Long       somehugenumber;
-    private Short      somesmallnumber;
-    private Integer    someregularnumber;
-    private Double     somedouble;
-    private String     someenum;
-    private JsonObject somejsonobject;
-    private JsonArray  somejsonarray;
+    private Integer       someid;
+    private String        somestring;
+    private Long          somehugenumber;
+    private Short         somesmallnumber;
+    private Integer       someregularnumber;
+    private Double        somedouble;
+    private String        someenum;
+    private JsonObject    somejsonobject;
+    private JsonArray     somejsonarray;
+    private LocalDateTime sometimestamp;
 
     public Something() {}
 
@@ -49,18 +52,20 @@ public class Something implements ISomething {
         this.someenum = value.someenum;
         this.somejsonobject = value.somejsonobject;
         this.somejsonarray = value.somejsonarray;
+        this.sometimestamp = value.sometimestamp;
     }
 
     public Something(
-        Integer    someid,
-        String     somestring,
-        Long       somehugenumber,
-        Short      somesmallnumber,
-        Integer    someregularnumber,
-        Double     somedouble,
-        String     someenum,
-        JsonObject somejsonobject,
-        JsonArray  somejsonarray
+        Integer       someid,
+        String        somestring,
+        Long          somehugenumber,
+        Short         somesmallnumber,
+        Integer       someregularnumber,
+        Double        somedouble,
+        String        someenum,
+        JsonObject    somejsonobject,
+        JsonArray     somejsonarray,
+        LocalDateTime sometimestamp
     ) {
         this.someid = someid;
         this.somestring = somestring;
@@ -71,6 +76,7 @@ public class Something implements ISomething {
         this.someenum = someenum;
         this.somejsonobject = somejsonobject;
         this.somejsonarray = somejsonarray;
+        this.sometimestamp = sometimestamp;
     }
 
     @Override
@@ -172,6 +178,17 @@ public class Something implements ISomething {
         return this;
     }
 
+    @Override
+    public LocalDateTime getSometimestamp() {
+        return this.sometimestamp;
+    }
+
+    @Override
+    public Something setSometimestamp(LocalDateTime sometimestamp) {
+        this.sometimestamp = sometimestamp;
+        return this;
+    }
+
     @Override
     public boolean equals(Object obj) {
         if (this == obj)
@@ -235,6 +252,12 @@ public class Something implements ISomething {
         }
         else if (!somejsonarray.equals(other.somejsonarray))
             return false;
+        if (sometimestamp == null) {
+            if (other.sometimestamp != null)
+                return false;
+        }
+        else if (!sometimestamp.equals(other.sometimestamp))
+            return false;
         return true;
     }
 
@@ -251,6 +274,7 @@ public class Something implements ISomething {
         result = prime * result + ((this.someenum == null) ? 0 : this.someenum.hashCode());
         result = prime * result + ((this.somejsonobject == null) ? 0 : this.somejsonobject.hashCode());
         result = prime * result + ((this.somejsonarray == null) ? 0 : this.somejsonarray.hashCode());
+        result = prime * result + ((this.sometimestamp == null) ? 0 : this.sometimestamp.hashCode());
         return result;
     }
 
@@ -267,6 +291,7 @@ public class Something implements ISomething {
         sb.append(", ").append(someenum);
         sb.append(", ").append(somejsonobject);
         sb.append(", ").append(somejsonarray);
+        sb.append(", ").append(sometimestamp);
 
         sb.append(")");
         return sb.toString();
@@ -290,6 +315,7 @@ public class Something implements ISomething {
         setSomeenum(from.getSomeenum());
         setSomejsonobject(from.getSomejsonobject());
         setSomejsonarray(from.getSomejsonarray());
+        setSometimestamp(from.getSometimestamp());
     }
 
     /**
