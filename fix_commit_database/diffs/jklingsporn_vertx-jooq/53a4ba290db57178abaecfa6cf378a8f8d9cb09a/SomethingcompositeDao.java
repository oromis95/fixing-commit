@@ -39,7 +39,7 @@ public class SomethingcompositeDao extends AbstractVertxDAO<SomethingcompositeRe
      * @param vertx the vertx instance
      */
     public SomethingcompositeDao(Configuration configuration, io.vertx.reactivex.core.Vertx vertx) {
-        super(Somethingcomposite.SOMETHINGCOMPOSITE, generated.rx.jdbc.regular.vertx.tables.pojos.Somethingcomposite.class, new JDBCRXQueryExecutor<SomethingcompositeRecord,generated.rx.jdbc.regular.vertx.tables.pojos.Somethingcomposite,Record2<Integer, Integer>>(configuration,generated.rx.jdbc.regular.vertx.tables.pojos.Somethingcomposite.class,configuration,vertx));
+        super(Somethingcomposite.SOMETHINGCOMPOSITE, generated.rx.jdbc.regular.vertx.tables.pojos.Somethingcomposite.class, new JDBCRXQueryExecutor<SomethingcompositeRecord,generated.rx.jdbc.regular.vertx.tables.pojos.Somethingcomposite,Record2<Integer, Integer>>(configuration,generated.rx.jdbc.regular.vertx.tables.pojos.Somethingcomposite.class,vertx));
     }
 
     /**
