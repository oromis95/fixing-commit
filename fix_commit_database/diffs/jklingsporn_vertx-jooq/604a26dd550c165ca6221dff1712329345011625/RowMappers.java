@@ -16,8 +16,8 @@ public class RowMappers {
             pojo.setSomesmallnumber(row.getShort("someSmallNumber"));
             pojo.setSomeregularnumber(row.getInteger("someRegularNumber"));
             pojo.setSomedouble(row.getDouble("someDouble"));
-            pojo.setSomejsonobject((io.vertx.core.json.JsonObject)row.getJson("someJsonObject").value());
-            pojo.setSomejsonarray((io.vertx.core.json.JsonArray)row.getJson("someJsonArray").value());
+            pojo.setSomejsonobject(io.github.jklingsporn.vertx.jooq.shared.reactive.JsonAccessor.getJsonObject(row,"someJsonObject"));
+            pojo.setSomejsonarray(io.github.jklingsporn.vertx.jooq.shared.reactive.JsonAccessor.getJsonArray(row,"someJsonArray"));
             pojo.setSometimestamp(row.getLocalDateTime("someTimestamp"));
             return pojo;
         };
@@ -28,7 +28,7 @@ public class RowMappers {
             generated.rx.reactive.regular.vertx.tables.pojos.Somethingcomposite pojo = new generated.rx.reactive.regular.vertx.tables.pojos.Somethingcomposite();
             pojo.setSomeid(row.getInteger("someId"));
             pojo.setSomesecondid(row.getInteger("someSecondId"));
-            pojo.setSomejsonobject((io.vertx.core.json.JsonObject)row.getJson("someJsonObject").value());
+            pojo.setSomejsonobject(io.github.jklingsporn.vertx.jooq.shared.reactive.JsonAccessor.getJsonObject(row,"someJsonObject"));
             return pojo;
         };
     }
