@@ -1,9 +1,12 @@
 package io.github.jklingsporn.vertx.jooq.generate.future;
 
+import generated.future.vertx.vertx.tables.pojos.Something;
 import generated.future.vertx.vertx.tables.daos.SomethingDao;
 import generated.future.vertx.vertx.tables.daos.SomethingcompositeDao;
 import io.github.jklingsporn.vertx.jooq.generate.TestTool;
 import io.vertx.core.Vertx;
+import io.vertx.core.json.JsonArray;
+import io.vertx.core.json.JsonObject;
 import org.jooq.Configuration;
 import org.jooq.SQLDialect;
 import org.jooq.impl.DefaultConfiguration;
@@ -12,6 +15,7 @@ import org.junit.BeforeClass;
 
 import java.sql.DriverManager;
 import java.sql.SQLException;
+import java.util.Random;
 import java.util.concurrent.CompletableFuture;
 import java.util.concurrent.CountDownLatch;
 import java.util.concurrent.TimeUnit;
@@ -69,4 +73,22 @@ public class VertxDaoTestBase {
         };
     }
 
+    static Something createSomethingWithId(){
+        Random random = new Random();
+        Something something = new Something();
+        something.setSomeid(random.nextInt());
+        something.setSomedouble(random.nextDouble());
+        something.setSomehugenumber(random.nextLong());
+        something.setSomejsonarray(new JsonArray().add(1).add(2).add(3));
+        something.setSomejsonobject(new JsonObject().put("key", "value"));
+        something.setSomesmallnumber((short) random.nextInt(Short.MAX_VALUE));
+        something.setSomeboolean(random.nextBoolean());
+        something.setSomestring("my_string");
+        return something;
+    }
+
+    static Something createSomething(){
+        return createSomethingWithId().setSomeid(null);
+    }
+
 }
