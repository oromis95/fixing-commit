@@ -1,7 +1,7 @@
 package io.github.jklingsporn.vertx.jooq.generate.custom;
 
 import io.github.jklingsporn.vertx.jooq.generate.AbstractVertxGeneratorTest;
-import io.github.jklingsporn.vertx.jooq.generate.JDBCDatabaseConfigurationProvider;
+import io.github.jklingsporn.vertx.jooq.generate.HsqldbConfigurationProvider;
 import io.github.jklingsporn.vertx.jooq.generate.VertxGeneratorStrategy;
 import org.jooq.util.jaxb.Configuration;
 
@@ -12,7 +12,7 @@ public class CustomVertxGeneratorTest extends AbstractVertxGeneratorTest{
 
 
     public CustomVertxGeneratorTest() {
-        super(CustomVertxGenerator.class, VertxGeneratorStrategy.class,"classic.jdbc.custom", new JDBCDatabaseConfigurationProvider(){
+        super(CustomVertxGenerator.class, VertxGeneratorStrategy.class,"classic.jdbc.custom", new HsqldbConfigurationProvider(){
             @Override
             public Configuration createGeneratorConfig(String generatorName, String packageName, Class<? extends VertxGeneratorStrategy> generatorStrategy) {
                 org.jooq.util.jaxb.Configuration conf =  super.createGeneratorConfig(generatorName, packageName, generatorStrategy);
