@@ -3,6 +3,7 @@ package io.github.jklingsporn.vertx.jooq.shared.internal.async;
 import io.github.jklingsporn.vertx.jooq.shared.internal.AbstractVertxDAO;
 import io.github.jklingsporn.vertx.jooq.shared.internal.QueryExecutor;
 import io.vertx.core.impl.Arguments;
+import io.vertx.core.json.JsonArray;
 import org.jooq.*;
 import org.jooq.impl.DSL;
 
@@ -21,10 +22,29 @@ import java.util.function.Function;
  */
 public abstract class AbstractAsyncVertxDAO<R extends UpdatableRecord<R>, P, T, FIND_MANY, FIND_ONE,EXECUTE, INSERT_RETURNING> extends AbstractVertxDAO<R,P,T,FIND_MANY,FIND_ONE, EXECUTE, INSERT_RETURNING>{
 
-    static EnumSet<SQLDialect> INSERT_RETURNING_SUPPORT = EnumSet.of(SQLDialect.MYSQL,SQLDialect.MYSQL_5_7,SQLDialect.MYSQL_8_0);
+    static EnumSet<SQLDialect> SUPPORTED_DIALECTS = EnumSet.of(SQLDialect.MYSQL,SQLDialect.MYSQL_5_7,SQLDialect.MYSQL_8_0,SQLDialect.POSTGRES,SQLDialect.POSTGRES_9_3,SQLDialect.POSTGRES_9_4,SQLDialect.POSTGRES_9_5);
+
+    private final Function<Object,T> keyConverter;
 
     protected AbstractAsyncVertxDAO(Table<R> table, Class<P> type, QueryExecutor<R, T, FIND_MANY, FIND_ONE, EXECUTE, INSERT_RETURNING> queryExecutor, Configuration configuration) {
         super(table, type, queryExecutor, configuration);
+        Arguments.require(SUPPORTED_DIALECTS.contains(configuration.dialect()),"Only Postgres and MySQL supported");
+        if(isMysql(configuration)){
+            keyConverter = keyConverter();
+        }else{
+            keyConverter = o -> {
+                JsonArray j = (JsonArray) o;
+                int pkLength = getTable().getPrimaryKey().getFieldsArray().length;
+                if(pkLength == 1){
+                    return (T)j.getValue(0);
+                }
+                Object[] values = new Object[j.size()];
+                for(int i=0;i<j.size();i++){
+                    values[i] = j.getValue(i);
+                }
+                return compositeKeyRecord(values);
+            };
+        }
     }
 
     /**
@@ -38,10 +58,17 @@ public abstract class AbstractAsyncVertxDAO<R extends UpdatableRecord<R>, P, T,
         };
     }
 
+    protected static boolean isMysql(Configuration configuration){
+        return SQLDialect.MYSQL.equals(configuration.dialect().family());
+    }
+
     @Override
     public INSERT_RETURNING insertReturningPrimary(P object) {
-        Arguments.require(INSERT_RETURNING_SUPPORT.contains(configuration().dialect()), "Only MySQL supported");
         DSLContext dslContext = DSL.using(configuration());
-        return queryExecutor().insertReturning(dslContext.insertInto(getTable()).set(newRecord(dslContext, object)).returning(), keyConverter());
+        return queryExecutor().insertReturning(dslContext
+                .insertInto(getTable())
+                .set(newRecord(dslContext, object))
+                .returning(getTable().getPrimaryKey().getFieldsArray()),
+                keyConverter);
     }
 }
