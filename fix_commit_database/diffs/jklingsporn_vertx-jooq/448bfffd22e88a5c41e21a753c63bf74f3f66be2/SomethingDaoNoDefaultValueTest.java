@@ -4,7 +4,8 @@ import generated.rx.async.regular.Tables;
 import generated.rx.async.regular.enums.SomethingSomeenum;
 import generated.rx.async.regular.tables.daos.SomethingDao;
 import generated.rx.async.regular.tables.pojos.Something;
-import io.github.jklingsporn.vertx.jooq.generate.AsyncDatabaseConfigurationProvider;
+import io.github.jklingsporn.vertx.jooq.generate.Credentials;
+import io.github.jklingsporn.vertx.jooq.generate.MySQLConfigurationProvider;
 import io.github.jklingsporn.vertx.jooq.generate.rx.RXTestBase;
 import io.github.jklingsporn.vertx.jooq.generate.rx.async.AsyncRXDatabaseClientProvider;
 import io.vertx.core.json.JsonArray;
@@ -21,12 +22,12 @@ import java.util.Random;
 public class SomethingDaoNoDefaultValueTest extends RXTestBase<Something, Integer, Long, SomethingDao> {
 
     public SomethingDaoNoDefaultValueTest() {
-        super(Tables.SOMETHING.SOMEHUGENUMBER, new SomethingDao(AsyncDatabaseConfigurationProvider.getInstance().createDAOConfiguration(), AsyncRXDatabaseClientProvider.getInstance().getClient()));
+        super(Tables.SOMETHING.SOMEHUGENUMBER, new SomethingDao(MySQLConfigurationProvider.getInstance().createDAOConfiguration(), AsyncRXDatabaseClientProvider.getInstance().getClient(Credentials.MYSQL)));
     }
 
     @BeforeClass
     public static void beforeClass() throws Exception {
-        AsyncDatabaseConfigurationProvider.getInstance().setupDatabase();
+        MySQLConfigurationProvider.getInstance().setupDatabase();
     }
 
     @Override
