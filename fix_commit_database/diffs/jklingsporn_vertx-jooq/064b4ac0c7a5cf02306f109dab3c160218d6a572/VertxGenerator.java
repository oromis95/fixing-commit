@@ -3,7 +3,7 @@ package io.github.jklingsporn.vertx.jooq.generate;
 import io.github.jklingsporn.vertx.jooq.shared.JsonArrayConverter;
 import io.github.jklingsporn.vertx.jooq.shared.JsonObjectConverter;
 import io.github.jklingsporn.vertx.jooq.shared.internal.AbstractVertxDAO;
-import io.github.jklingsporn.vertx.jooq.shared.postgres.ObjectToJsonObjectBinding;
+import io.github.jklingsporn.vertx.jooq.shared.ObjectToJsonObjectBinding;
 import io.vertx.core.Vertx;
 import io.vertx.core.impl.Arguments;
 import org.jooq.Constants;
