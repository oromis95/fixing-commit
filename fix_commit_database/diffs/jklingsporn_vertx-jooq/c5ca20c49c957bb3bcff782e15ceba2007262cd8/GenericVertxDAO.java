@@ -21,21 +21,21 @@ public interface GenericVertxDAO<P, T, FIND_MANY, FIND_ONE, EXECUTE, INSERT_RETU
      * @param pojo
      * @return the result type returned for all insert, update and delete-operations.
      */
-    public EXECUTE insertAsync(P pojo);
+    public EXECUTE insert(P pojo);
 
     /**
      * Performs an async <code>INSERT</code> statement for all given POJOs
      * @param pojos
      * @return the result type returned for all insert, update and delete-operations.
      */
-    public EXECUTE insertAsync(Collection<P> pojos);
+    public EXECUTE insert(Collection<P> pojos);
 
     /**
      * Performs an async <code>INSERT</code> statement for a given POJO and returns it's primary key.
      * @param pojo
      * @return the result type returned for INSERT_RETURNING.
      */
-    public INSERT_RETURNING insertReturningPrimaryAsync(P pojo);
+    public INSERT_RETURNING insertReturningPrimary(P pojo);
 
     /**
      * Performs an async <code>UPDATE</code> statement for a given POJO. For performance reasons, consider writing
@@ -43,28 +43,28 @@ public interface GenericVertxDAO<P, T, FIND_MANY, FIND_ONE, EXECUTE, INSERT_RETU
      * @param pojo
      * @return the result type returned for all insert, update and delete-operations.
      */
-    public EXECUTE updateAsync(P pojo);
+    public EXECUTE update(P pojo);
 
     /**
      * Performs an async <code>DELETE</code> statement using the given id
      * @param id
      * @return the result type returned for all insert, update and delete-operations.
      */
-    public EXECUTE deleteByIdAsync(T id);
+    public EXECUTE deleteById(T id);
 
     /**
      * Performs an async <code>DELETE</code> statement using the given ids
      * @param ids
      * @return the result type returned for all insert, update and delete-operations.
      */
-    public EXECUTE deleteByIdsAsync(Collection<T> ids);
+    public EXECUTE deleteByIds(Collection<T> ids);
 
     /**
      * Performs an async <code>DELETE</code> statement using the given <code>Condition</code>
      * @param condition
      * @return the result type returned for all insert, update and delete-operations.
      */
-    public EXECUTE deleteByConditionAsync(Condition condition);
+    public EXECUTE deleteByCondition(Condition condition);
 
     /**
      * Performs an async <code>SELECT</code> using the given condition. If more than one row is found, a
@@ -72,33 +72,33 @@ public interface GenericVertxDAO<P, T, FIND_MANY, FIND_ONE, EXECUTE, INSERT_RETU
      * @param condition
      * @return the result type returned for all find-one-value-operations.
      */
-    public FIND_ONE findOneByConditionAsync(Condition condition);
+    public FIND_ONE findOneByCondition(Condition condition);
 
     /**
      * Performs an async <code>SELECT</code> using the given primary key.
      * @param id
      * @return the result type returned for all find-one-value-operations.
      */
-    public FIND_ONE findOneByIdAsync(T id);
+    public FIND_ONE findOneById(T id);
 
     /**
      * Performs an async <code>SELECT</code> using the given primary keys.
      * @param ids
      * @return the result type returned for all find-many-values-operations.
      */
-    public FIND_MANY findManyByIdsAsync(Collection<T> ids);
+    public FIND_MANY findManyByIds(Collection<T> ids);
 
     /**
      * Performs an async <code>SELECT</code> using the given condition.
      * @param condition
      * @return the result type returned for all find-many-values-operations.
      */
-    public FIND_MANY findManyByConditionAsync(Condition condition);
+    public FIND_MANY findManyByCondition(Condition condition);
 
     /**
      * Performs an async <code>SELECT</code>.
      * @return the result type returned for all find-many-values-operations.
      */
-    public FIND_MANY findAllAsync();
+    public FIND_MANY findAll();
 
 }
