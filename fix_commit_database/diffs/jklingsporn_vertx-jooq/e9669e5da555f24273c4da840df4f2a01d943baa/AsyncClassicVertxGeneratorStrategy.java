@@ -24,7 +24,7 @@ public class AsyncClassicVertxGeneratorStrategy extends AbstractClassicVertxGene
     public void writeConstructor(JavaWriter out, String className, String tableIdentifier, String tableRecord, String pType, String tType){
         out.tab(1).javadoc("@param configuration Used for rendering, so only SQLDialect must be set and must be one of the MYSQL types or POSTGRES.\n     * @param delegate A configured AsyncSQLClient that is used for query execution");
         out.tab(1).println("public %s(%s configuration, io.vertx.ext.asyncsql.AsyncSQLClient delegate) {", className, Configuration.class);
-        out.tab(2).println("super(%s, %s.class, new %s(delegate,%s::new), configuration);", tableIdentifier, pType, renderQueryExecutor(tableRecord, pType, tType),pType);
+        out.tab(2).println("super(%s, %s.class, new %s(delegate,%s::new, %s), configuration);", tableIdentifier, pType, renderQueryExecutor(tableRecord, pType, tType),pType, tableIdentifier);
         out.tab(1).println("}");
     }
 
