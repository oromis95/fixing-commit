@@ -18,14 +18,14 @@ import javax.annotation.Generated;
 @Generated(
     value = {
         "http://www.jooq.org",
-        "jOOQ version:3.9.3"
+        "jOOQ version:3.10.1"
     },
     comments = "This class is generated by jOOQ"
 )
 @SuppressWarnings({ "all", "unchecked", "rawtypes" })
 public class Something implements ISomething {
 
-    private static final long serialVersionUID = -1500243406;
+    private static final long serialVersionUID = 1728425616;
 
     private Integer    someid;
     private String     somestring;
