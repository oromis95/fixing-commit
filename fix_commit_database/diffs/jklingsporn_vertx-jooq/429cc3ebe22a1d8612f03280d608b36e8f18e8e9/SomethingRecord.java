@@ -25,14 +25,14 @@ import org.jooq.impl.UpdatableRecordImpl;
 @Generated(
     value = {
         "http://www.jooq.org",
-        "jOOQ version:3.9.3"
+        "jOOQ version:3.10.1"
     },
     comments = "This class is generated by jOOQ"
 )
 @SuppressWarnings({ "all", "unchecked", "rawtypes" })
 public class SomethingRecord extends UpdatableRecordImpl<SomethingRecord> implements Record9<Integer, String, Long, Short, Integer, Boolean, Double, JsonObject, JsonArray>, ISomething {
 
-    private static final long serialVersionUID = 462652619;
+    private static final long serialVersionUID = 2103308640;
 
     /**
      * Setter for <code>VERTX.SOMETHING.SOMEID</code>.
@@ -291,6 +291,78 @@ public class SomethingRecord extends UpdatableRecordImpl<SomethingRecord> implem
         return Something.SOMETHING.SOMEJSONARRAY;
     }
 
+    /**
+     * {@inheritDoc}
+     */
+    @Override
+    public Integer component1() {
+        return getSomeid();
+    }
+
+    /**
+     * {@inheritDoc}
+     */
+    @Override
+    public String component2() {
+        return getSomestring();
+    }
+
+    /**
+     * {@inheritDoc}
+     */
+    @Override
+    public Long component3() {
+        return getSomehugenumber();
+    }
+
+    /**
+     * {@inheritDoc}
+     */
+    @Override
+    public Short component4() {
+        return getSomesmallnumber();
+    }
+
+    /**
+     * {@inheritDoc}
+     */
+    @Override
+    public Integer component5() {
+        return getSomeregularnumber();
+    }
+
+    /**
+     * {@inheritDoc}
+     */
+    @Override
+    public Boolean component6() {
+        return getSomeboolean();
+    }
+
+    /**
+     * {@inheritDoc}
+     */
+    @Override
+    public Double component7() {
+        return getSomedouble();
+    }
+
+    /**
+     * {@inheritDoc}
+     */
+    @Override
+    public JsonObject component8() {
+        return getSomejsonobject();
+    }
+
+    /**
+     * {@inheritDoc}
+     */
+    @Override
+    public JsonArray component9() {
+        return getSomejsonarray();
+    }
+
     /**
      * {@inheritDoc}
      */
