@@ -4,6 +4,7 @@
 package generated.rx.async.regular.tables.records;
 
 
+import generated.rx.async.regular.enums.SomethingSomeenum;
 import generated.rx.async.regular.tables.Something;
 import generated.rx.async.regular.tables.interfaces.ISomething;
 
@@ -32,9 +33,9 @@ import org.jooq.impl.UpdatableRecordImpl;
     comments = "This class is generated by jOOQ"
 )
 @SuppressWarnings({ "all", "unchecked", "rawtypes" })
-public class SomethingRecord extends UpdatableRecordImpl<SomethingRecord> implements Record10<Integer, String, Long, Short, Integer, Double, String, JsonObject, JsonArray, LocalDateTime>, ISomething {
+public class SomethingRecord extends UpdatableRecordImpl<SomethingRecord> implements Record10<Integer, String, Long, Short, Integer, Double, SomethingSomeenum, JsonObject, JsonArray, LocalDateTime>, ISomething {
 
-    private static final long serialVersionUID = 1879646796;
+    private static final long serialVersionUID = -2140256216;
 
     /**
      * Setter for <code>something.someId</code>.
@@ -142,7 +143,7 @@ public class SomethingRecord extends UpdatableRecordImpl<SomethingRecord> implem
      * Setter for <code>something.someEnum</code>.
      */
     @Override
-    public SomethingRecord setSomeenum(String value) {
+    public SomethingRecord setSomeenum(SomethingSomeenum value) {
         set(6, value);
         return this;
     }
@@ -151,8 +152,8 @@ public class SomethingRecord extends UpdatableRecordImpl<SomethingRecord> implem
      * Getter for <code>something.someEnum</code>.
      */
     @Override
-    public String getSomeenum() {
-        return (String) get(6);
+    public SomethingSomeenum getSomeenum() {
+        return (SomethingSomeenum) get(6);
     }
 
     /**
@@ -226,7 +227,7 @@ public class SomethingRecord extends UpdatableRecordImpl<SomethingRecord> implem
      * {@inheritDoc}
      */
     @Override
-    public Row10<Integer, String, Long, Short, Integer, Double, String, JsonObject, JsonArray, LocalDateTime> fieldsRow() {
+    public Row10<Integer, String, Long, Short, Integer, Double, SomethingSomeenum, JsonObject, JsonArray, LocalDateTime> fieldsRow() {
         return (Row10) super.fieldsRow();
     }
 
@@ -234,7 +235,7 @@ public class SomethingRecord extends UpdatableRecordImpl<SomethingRecord> implem
      * {@inheritDoc}
      */
     @Override
-    public Row10<Integer, String, Long, Short, Integer, Double, String, JsonObject, JsonArray, LocalDateTime> valuesRow() {
+    public Row10<Integer, String, Long, Short, Integer, Double, SomethingSomeenum, JsonObject, JsonArray, LocalDateTime> valuesRow() {
         return (Row10) super.valuesRow();
     }
 
@@ -290,7 +291,7 @@ public class SomethingRecord extends UpdatableRecordImpl<SomethingRecord> implem
      * {@inheritDoc}
      */
     @Override
-    public Field<String> field7() {
+    public Field<SomethingSomeenum> field7() {
         return Something.SOMETHING.SOMEENUM;
     }
 
@@ -370,7 +371,7 @@ public class SomethingRecord extends UpdatableRecordImpl<SomethingRecord> implem
      * {@inheritDoc}
      */
     @Override
-    public String component7() {
+    public SomethingSomeenum component7() {
         return getSomeenum();
     }
 
@@ -450,7 +451,7 @@ public class SomethingRecord extends UpdatableRecordImpl<SomethingRecord> implem
      * {@inheritDoc}
      */
     @Override
-    public String value7() {
+    public SomethingSomeenum value7() {
         return getSomeenum();
     }
 
@@ -536,7 +537,7 @@ public class SomethingRecord extends UpdatableRecordImpl<SomethingRecord> implem
      * {@inheritDoc}
      */
     @Override
-    public SomethingRecord value7(String value) {
+    public SomethingRecord value7(SomethingSomeenum value) {
         setSomeenum(value);
         return this;
     }
@@ -572,7 +573,7 @@ public class SomethingRecord extends UpdatableRecordImpl<SomethingRecord> implem
      * {@inheritDoc}
      */
     @Override
-    public SomethingRecord values(Integer value1, String value2, Long value3, Short value4, Integer value5, Double value6, String value7, JsonObject value8, JsonArray value9, LocalDateTime value10) {
+    public SomethingRecord values(Integer value1, String value2, Long value3, Short value4, Integer value5, Double value6, SomethingSomeenum value7, JsonObject value8, JsonArray value9, LocalDateTime value10) {
         value1(value1);
         value2(value2);
         value3(value3);
@@ -630,7 +631,7 @@ public class SomethingRecord extends UpdatableRecordImpl<SomethingRecord> implem
     /**
      * Create a detached, initialised SomethingRecord
      */
-    public SomethingRecord(Integer someid, String somestring, Long somehugenumber, Short somesmallnumber, Integer someregularnumber, Double somedouble, String someenum, JsonObject somejsonobject, JsonArray somejsonarray, LocalDateTime sometimestamp) {
+    public SomethingRecord(Integer someid, String somestring, Long somehugenumber, Short somesmallnumber, Integer someregularnumber, Double somedouble, SomethingSomeenum someenum, JsonObject somejsonobject, JsonArray somejsonarray, LocalDateTime sometimestamp) {
         super(Something.SOMETHING);
 
         set(0, someid);
