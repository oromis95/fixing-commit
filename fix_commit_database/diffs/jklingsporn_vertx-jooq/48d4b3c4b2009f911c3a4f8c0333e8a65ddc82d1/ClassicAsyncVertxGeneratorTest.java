@@ -2,14 +2,14 @@ package io.github.jklingsporn.vertx.jooq.generate.classic.async.regular;
 
 import io.github.jklingsporn.vertx.jooq.generate.AbstractVertxGeneratorTest;
 import io.github.jklingsporn.vertx.jooq.generate.AsyncDatabaseConfigurationProvider;
-import io.github.jklingsporn.vertx.jooq.generate.VertxGenerator;
-import io.github.jklingsporn.vertx.jooq.generate.classic.AsyncClassicVertxGeneratorStrategy;
+import io.github.jklingsporn.vertx.jooq.generate.VertxGeneratorStrategy;
+import io.github.jklingsporn.vertx.jooq.generate.classic.ClassicAsyncVertxGenerator;
 
 /**
  * Created by jensklingsporn on 13.02.18.
  */
-public class AsyncClassicVertxGeneratorTest extends AbstractVertxGeneratorTest{
-    public AsyncClassicVertxGeneratorTest() {
-        super(VertxGenerator.class, AsyncClassicVertxGeneratorStrategy.class, "classic.async.regular", AsyncDatabaseConfigurationProvider.getInstance());
+public class ClassicAsyncVertxGeneratorTest extends AbstractVertxGeneratorTest{
+    public ClassicAsyncVertxGeneratorTest() {
+        super(ClassicAsyncVertxGenerator.class, VertxGeneratorStrategy.class, "classic.async.regular", AsyncDatabaseConfigurationProvider.getInstance());
     }
 }
