@@ -0,0 +1,18 @@
+package io.github.jklingsporn.vertx.jooq.generate.rx.jdbc.guice;
+
+import io.github.jklingsporn.vertx.jooq.generate.AbstractVertxGeneratorTest;
+import io.github.jklingsporn.vertx.jooq.generate.JDBCDatabaseConfigurationProvider;
+import io.github.jklingsporn.vertx.jooq.generate.VertxGeneratorStrategy;
+import io.github.jklingsporn.vertx.jooq.generate.rx.RXJDBCGuiceVertxGenerator;
+
+/**
+ * Created by jklingsporn on 17.09.16.
+ */
+public class RXJDBCGuiceVertxGeneratorTest extends AbstractVertxGeneratorTest{
+
+
+    public RXJDBCGuiceVertxGeneratorTest() {
+        super(RXJDBCGuiceVertxGenerator.class, VertxGeneratorStrategy.class,"rx.jdbc.guice", JDBCDatabaseConfigurationProvider.getInstance());
+    }
+
+}
