@@ -2,8 +2,8 @@ package io.github.jklingsporn.vertx.jooq.generate.classic.jdbc.regular;
 
 import io.github.jklingsporn.vertx.jooq.generate.AbstractVertxGeneratorTest;
 import io.github.jklingsporn.vertx.jooq.generate.JDBCDatabaseConfigurationProvider;
-import io.github.jklingsporn.vertx.jooq.generate.VertxGenerator;
-import io.github.jklingsporn.vertx.jooq.generate.classic.JDBCClassicVertxGeneratorStrategy;
+import io.github.jklingsporn.vertx.jooq.generate.VertxGeneratorStrategy;
+import io.github.jklingsporn.vertx.jooq.generate.classic.ClassicJDBCVertxGenerator;
 
 /**
  * Created by jklingsporn on 17.09.16.
@@ -12,7 +12,7 @@ public class ClassicJDBCVertxGeneratorTest extends AbstractVertxGeneratorTest{
 
 
     public ClassicJDBCVertxGeneratorTest() {
-        super(VertxGenerator.class, JDBCClassicVertxGeneratorStrategy.class,"classic.jdbc.regular", JDBCDatabaseConfigurationProvider.getInstance());
+        super(ClassicJDBCVertxGenerator.class, VertxGeneratorStrategy.class,"classic.jdbc.regular", JDBCDatabaseConfigurationProvider.getInstance());
     }
 
 }
