@@ -2,8 +2,8 @@ package io.github.jklingsporn.vertx.jooq.generate.completablefuture.jdbc.regular
 
 import io.github.jklingsporn.vertx.jooq.generate.AbstractVertxGeneratorTest;
 import io.github.jklingsporn.vertx.jooq.generate.JDBCDatabaseConfigurationProvider;
-import io.github.jklingsporn.vertx.jooq.generate.VertxGenerator;
-import io.github.jklingsporn.vertx.jooq.generate.completablefuture.JDBCCompletableFutureVertxGeneratorStrategy;
+import io.github.jklingsporn.vertx.jooq.generate.VertxGeneratorStrategy;
+import io.github.jklingsporn.vertx.jooq.generate.completablefuture.CompletableFutureJDBCVertxGenerator;
 
 /**
  * Created by jklingsporn on 17.09.16.
@@ -12,7 +12,7 @@ public class CompletableFutureJDBCVertxGeneratorTest extends AbstractVertxGenera
 
 
     public CompletableFutureJDBCVertxGeneratorTest() {
-        super(VertxGenerator.class, JDBCCompletableFutureVertxGeneratorStrategy.class,"cf.jdbc.regular", JDBCDatabaseConfigurationProvider.getInstance());
+        super(CompletableFutureJDBCVertxGenerator.class, VertxGeneratorStrategy.class,"cf.jdbc.regular", JDBCDatabaseConfigurationProvider.getInstance());
     }
 
 }
