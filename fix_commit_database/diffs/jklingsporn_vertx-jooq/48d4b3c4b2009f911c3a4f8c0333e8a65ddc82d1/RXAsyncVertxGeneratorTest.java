@@ -2,8 +2,8 @@ package io.github.jklingsporn.vertx.jooq.generate.rx.async.regular;
 
 import io.github.jklingsporn.vertx.jooq.generate.AbstractVertxGeneratorTest;
 import io.github.jklingsporn.vertx.jooq.generate.AsyncDatabaseConfigurationProvider;
-import io.github.jklingsporn.vertx.jooq.generate.VertxGenerator;
-import io.github.jklingsporn.vertx.jooq.generate.rx.AsyncRXVertxGeneratorStrategy;
+import io.github.jklingsporn.vertx.jooq.generate.VertxGeneratorStrategy;
+import io.github.jklingsporn.vertx.jooq.generate.rx.RXAsyncVertxGenerator;
 
 /**
  * Created by jklingsporn on 17.09.16.
@@ -12,7 +12,7 @@ public class RXAsyncVertxGeneratorTest extends AbstractVertxGeneratorTest{
 
 
     public RXAsyncVertxGeneratorTest() {
-        super(VertxGenerator.class, AsyncRXVertxGeneratorStrategy.class,"rx.async.regular", AsyncDatabaseConfigurationProvider.getInstance());
+        super(RXAsyncVertxGenerator.class, VertxGeneratorStrategy.class,"rx.async.regular", AsyncDatabaseConfigurationProvider.getInstance());
     }
 
 }
