@@ -2,8 +2,8 @@ package io.github.jklingsporn.vertx.jooq.generate.rx.jdbc.regular;
 
 import io.github.jklingsporn.vertx.jooq.generate.AbstractVertxGeneratorTest;
 import io.github.jklingsporn.vertx.jooq.generate.JDBCDatabaseConfigurationProvider;
-import io.github.jklingsporn.vertx.jooq.generate.VertxGenerator;
-import io.github.jklingsporn.vertx.jooq.generate.rx.JDBCRXVertxGeneratorStrategy;
+import io.github.jklingsporn.vertx.jooq.generate.VertxGeneratorStrategy;
+import io.github.jklingsporn.vertx.jooq.generate.rx.RXJDBCVertxGenerator;
 
 /**
  * Created by jklingsporn on 17.09.16.
@@ -12,7 +12,7 @@ public class RXJDBCVertxGeneratorTest extends AbstractVertxGeneratorTest{
 
 
     public RXJDBCVertxGeneratorTest() {
-        super(VertxGenerator.class, JDBCRXVertxGeneratorStrategy.class,"rx.jdbc.regular", JDBCDatabaseConfigurationProvider.getInstance());
+        super(RXJDBCVertxGenerator.class, VertxGeneratorStrategy.class,"rx.jdbc.regular", JDBCDatabaseConfigurationProvider.getInstance());
     }
 
 }
