@@ -1,6 +1,7 @@
 package io.github.jklingsporn.vertx.jooq.generate.custom;
 
-import io.github.jklingsporn.vertx.jooq.generate.VertxGenerator;
+import io.github.jklingsporn.vertx.jooq.generate.builder.DelegatingVertxGenerator;
+import io.github.jklingsporn.vertx.jooq.generate.builder.VertxGeneratorBuilder;
 import org.jooq.util.JavaWriter;
 import org.jooq.util.TypedElementDefinition;
 
@@ -10,7 +11,11 @@ import java.time.LocalDateTime;
  * Created by jensklingsporn on 22.02.18.
  * Converts a LocalDateTime from/into a String during JSON-conversion.
  */
-public class CustomVertxGenerator extends VertxGenerator{
+public class CustomVertxGenerator extends DelegatingVertxGenerator {
+
+    public CustomVertxGenerator() {
+        super(VertxGeneratorBuilder.init().withClassicAPI().withJDBCDriver().build());
+    }
 
     @Override
     protected boolean handleCustomTypeFromJson(TypedElementDefinition<?> column, String setter, String columnType, String javaMemberName, JavaWriter out) {
