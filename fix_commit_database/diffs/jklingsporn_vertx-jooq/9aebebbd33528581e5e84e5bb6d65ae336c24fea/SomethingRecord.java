@@ -32,7 +32,7 @@ import org.jooq.impl.UpdatableRecordImpl;
 @SuppressWarnings({ "all", "unchecked", "rawtypes" })
 public class SomethingRecord extends UpdatableRecordImpl<SomethingRecord> implements Record9<Integer, String, Long, Short, Integer, Boolean, Double, JsonObject, JsonArray>, ISomething {
 
-    private static final long serialVersionUID = 2103308640;
+    private static final long serialVersionUID = -2146401024;
 
     /**
      * Setter for <code>VERTX.SOMETHING.SOMEID</code>.
@@ -589,4 +589,9 @@ public class SomethingRecord extends UpdatableRecordImpl<SomethingRecord> implem
         set(7, somejsonobject);
         set(8, somejsonarray);
     }
+
+    public SomethingRecord(io.vertx.core.json.JsonObject json) {
+        this();
+        fromJson(json);
+    }
 }
