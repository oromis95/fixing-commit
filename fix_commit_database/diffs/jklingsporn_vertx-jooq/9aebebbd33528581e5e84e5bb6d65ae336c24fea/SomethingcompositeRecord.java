@@ -31,7 +31,7 @@ import org.jooq.impl.UpdatableRecordImpl;
 @SuppressWarnings({ "all", "unchecked", "rawtypes" })
 public class SomethingcompositeRecord extends UpdatableRecordImpl<SomethingcompositeRecord> implements Record3<Integer, Integer, JsonObject>, ISomethingcomposite {
 
-    private static final long serialVersionUID = 846115202;
+    private static final long serialVersionUID = 1407026923;
 
     /**
      * Setter for <code>VERTX.SOMETHINGCOMPOSITE.SOMEID</code>.
@@ -270,4 +270,9 @@ public class SomethingcompositeRecord extends UpdatableRecordImpl<Somethingcompo
         set(1, somesecondid);
         set(2, somejsonobject);
     }
+
+    public SomethingcompositeRecord(io.vertx.core.json.JsonObject json) {
+        this();
+        fromJson(json);
+    }
 }
