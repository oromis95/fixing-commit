@@ -34,6 +34,8 @@ class ComponentBasedVertxGenerator extends VertxGenerator {
     Consumer<JavaWriter> writeDAOClassAnnotationDelegate = (w)->{};
     Consumer<JavaWriter> writeDAOConstructorAnnotationDelegate = (w)->{};
     Collection<BiFunction<SchemaDefinition,Function<File,JavaWriter>,JavaWriter>> writeExtraDataDelegates = new ArrayList<>();
+    NamedInjectionStrategy namedInjectionStrategy = PredefinedNamedInjectionStrategy.DISABLED;
+
     VertxGenerator activeGenerator = this;
 
     @Override
@@ -77,8 +79,8 @@ class ComponentBasedVertxGenerator extends VertxGenerator {
     }
 
     @Override
-    public void writeDAOConstructor(JavaWriter out, String className, String tableIdentifier, String rType, String pType, String tType) {
-        writeConstructorDelegate.writeConstructor(out, className, tableIdentifier, rType, pType, tType);
+    public void writeDAOConstructor(JavaWriter out, String className, String tableIdentifier, String rType, String pType, String tType, String schema) {
+        writeConstructorDelegate.writeConstructor(out, className, tableIdentifier, rType, pType, tType, schema);
     }
 
     @Override
@@ -166,6 +168,11 @@ class ComponentBasedVertxGenerator extends VertxGenerator {
         return this;
     }
 
+    public ComponentBasedVertxGenerator setNamedInjectionStrategy(NamedInjectionStrategy namedInjectionStrategy) {
+        this.namedInjectionStrategy = namedInjectionStrategy;
+        return this;
+    }
+
     /**
      *
      * @return The {@code VertxGenerator} that is actually used. When using a {@code ComponentBasedVertxGenerator} inside a {@code DelegatingVertxGenerator}
