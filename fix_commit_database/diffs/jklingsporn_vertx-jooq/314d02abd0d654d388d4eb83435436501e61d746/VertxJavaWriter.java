@@ -9,10 +9,12 @@ import java.io.File;
  */
 public class VertxJavaWriter extends JavaWriter {
 
+
     public VertxJavaWriter(File file, String fullyQualifiedTypes, String encoding) {
         super(file, fullyQualifiedTypes, encoding);
     }
 
+
     @Override
     protected String beforeClose(String string) {
         return super.beforeClose(string);
