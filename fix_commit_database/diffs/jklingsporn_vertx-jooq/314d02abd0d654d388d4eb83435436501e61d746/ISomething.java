@@ -107,16 +107,6 @@ public interface ISomething extends VertxPojo, Serializable {
      */
     public JsonArray getSomejsonarray();
 
-    /**
-     * Setter for <code>vertx.something.someJsonBObject</code>.
-     */
-    public ISomething setSomejsonbobject(JsonObject value);
-
-    /**
-     * Getter for <code>vertx.something.someJsonBObject</code>.
-     */
-    public JsonObject getSomejsonbobject();
-
     /**
      * Setter for <code>vertx.something.someTimestamp</code>.
      */
@@ -151,7 +141,6 @@ public interface ISomething extends VertxPojo, Serializable {
         setSomedouble(json.getDouble("someDouble"));
         setSomejsonobject(json.getJsonObject("someJsonObject"));
         setSomejsonarray(json.getJsonArray("someJsonArray"));
-        setSomejsonbobject(json.getJsonObject("someJsonBObject"));
         // Omitting unrecognized type java.time.LocalDateTime for column someTimestamp!
         return this;
     }
@@ -168,7 +157,6 @@ public interface ISomething extends VertxPojo, Serializable {
         json.put("someDouble",getSomedouble());
         json.put("someJsonObject",getSomejsonobject());
         json.put("someJsonArray",getSomejsonarray());
-        json.put("someJsonBObject",getSomejsonbobject());
         // Omitting unrecognized type java.time.LocalDateTime for column someTimestamp!
         return json;
     }
