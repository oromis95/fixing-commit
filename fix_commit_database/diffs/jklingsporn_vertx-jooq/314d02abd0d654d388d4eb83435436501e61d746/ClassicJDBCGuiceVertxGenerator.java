@@ -1,6 +1,7 @@
 package io.github.jklingsporn.vertx.jooq.generate.classic;
 
 import io.github.jklingsporn.vertx.jooq.generate.builder.DelegatingVertxGenerator;
+import io.github.jklingsporn.vertx.jooq.generate.builder.PredefinedNamedInjectionStrategy;
 import io.github.jklingsporn.vertx.jooq.generate.builder.VertxGeneratorBuilder;
 
 /**
@@ -9,6 +10,6 @@ import io.github.jklingsporn.vertx.jooq.generate.builder.VertxGeneratorBuilder;
 public class ClassicJDBCGuiceVertxGenerator extends DelegatingVertxGenerator {
 
     public ClassicJDBCGuiceVertxGenerator() {
-        super(VertxGeneratorBuilder.init().withClassicAPI().withJDBCDriver().withGuice(true).build());
+        super(VertxGeneratorBuilder.init().withClassicAPI().withJDBCDriver().withGuice(true, PredefinedNamedInjectionStrategy.DISABLED).build());
     }
 }
