@@ -1,6 +1,7 @@
 package io.github.jklingsporn.vertx.jooq.generate.completablefuture;
 
 import io.github.jklingsporn.vertx.jooq.generate.builder.DelegatingVertxGenerator;
+import io.github.jklingsporn.vertx.jooq.generate.builder.PredefinedNamedInjectionStrategy;
 import io.github.jklingsporn.vertx.jooq.generate.builder.VertxGeneratorBuilder;
 
 /**
@@ -9,7 +10,7 @@ import io.github.jklingsporn.vertx.jooq.generate.builder.VertxGeneratorBuilder;
 public class CompletableFutureJDBCGuiceVertxGenerator extends DelegatingVertxGenerator {
 
     public CompletableFutureJDBCGuiceVertxGenerator() {
-        super(VertxGeneratorBuilder.init().withCompletableFutureAPI().withJDBCDriver().withGuice(true).build());
+        super(VertxGeneratorBuilder.init().withCompletableFutureAPI().withJDBCDriver().withGuice(true, PredefinedNamedInjectionStrategy.DISABLED).build());
     }
 
 }
