@@ -1,6 +1,7 @@
 package io.github.jklingsporn.vertx.jooq.generate.completablefuture;
 
 import io.github.jklingsporn.vertx.jooq.generate.builder.DelegatingVertxGenerator;
+import io.github.jklingsporn.vertx.jooq.generate.builder.PredefinedNamedInjectionStrategy;
 import io.github.jklingsporn.vertx.jooq.generate.builder.VertxGeneratorBuilder;
 
 /**
@@ -9,6 +10,6 @@ import io.github.jklingsporn.vertx.jooq.generate.builder.VertxGeneratorBuilder;
 public class CompletableFutureAsyncGuiceVertxGenerator extends DelegatingVertxGenerator {
 
     public CompletableFutureAsyncGuiceVertxGenerator() {
-        super(VertxGeneratorBuilder.init().withCompletableFutureAPI().withAsyncDriver().withGuice(true).build());
+        super(VertxGeneratorBuilder.init().withCompletableFutureAPI().withAsyncDriver().withGuice(true, PredefinedNamedInjectionStrategy.DISABLED).build());
     }
 }
