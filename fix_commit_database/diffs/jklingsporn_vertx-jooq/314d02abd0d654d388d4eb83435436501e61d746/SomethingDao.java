@@ -100,13 +100,6 @@ public class SomethingDao extends AbstractReactiveVertxDAO<SomethingRecord, gene
         return findManyByCondition(Something.SOMETHING.SOMEJSONARRAY.in(values));
     }
 
-    /**
-     * Find records that have <code>someJsonBObject IN (values)</code> asynchronously
-     */
-    public Single<List<generated.rx.reactive.regular.vertx.tables.pojos.Something>> findManyBySomejsonbobject(List<JsonObject> values) {
-        return findManyByCondition(Something.SOMETHING.SOMEJSONBOBJECT.in(values));
-    }
-
     /**
      * Find records that have <code>someTimestamp IN (values)</code> asynchronously
      */
