@@ -39,7 +39,7 @@ import lombok.RequiredArgsConstructor;
 import lombok.extern.log4j.Log4j;
 
 /**
- * InstagramStoryPhotoUploadRequest
+ * InstagramUploadStoryPhotoRequest
  * @author Justin Vo
  *
  */
