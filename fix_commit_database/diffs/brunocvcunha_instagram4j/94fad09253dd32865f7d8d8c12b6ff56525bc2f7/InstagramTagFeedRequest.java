@@ -15,7 +15,7 @@
  */
 package org.brunocvcunha.instagram4j.requests;
 
-import org.brunocvcunha.instagram4j.requests.payload.InstagramTagFeedResult;
+import org.brunocvcunha.instagram4j.requests.payload.InstagramFeedResult;
 
 import lombok.AllArgsConstructor;
 import lombok.SneakyThrows;
@@ -27,7 +27,7 @@ import lombok.SneakyThrows;
  *
  */
 @AllArgsConstructor
-public class InstagramTagFeedRequest extends InstagramGetRequest<InstagramTagFeedResult> {
+public class InstagramTagFeedRequest extends InstagramGetRequest<InstagramFeedResult> {
 
     private String tag;
 
@@ -36,15 +36,10 @@ public class InstagramTagFeedRequest extends InstagramGetRequest<InstagramTagFee
         return "feed/tag/" + tag + "/?rank_token=" + api.getRankToken() + "&ranked_content=true&";
     }
 
-    @Override
-    public String getPayload() {
-        return null;
-    }
-
     @Override
     @SneakyThrows
-    public InstagramTagFeedResult parseResult(int statusCode, String content) {
-        return parseJson(content, InstagramTagFeedResult.class);
+    public InstagramFeedResult parseResult(int statusCode, String content) {
+        return parseJson(content, InstagramFeedResult.class);
     }
 
 }
