@@ -31,8 +31,6 @@ import lombok.Data;
 @Data
 @JsonIgnoreProperties(ignoreUnknown = true)
 public class InstagramUploadVideoResult extends StatusResult {
-    private String status;
-    private String message;
     private String upload_id;
     private List<Map<String, Object>> video_upload_urls;
 
