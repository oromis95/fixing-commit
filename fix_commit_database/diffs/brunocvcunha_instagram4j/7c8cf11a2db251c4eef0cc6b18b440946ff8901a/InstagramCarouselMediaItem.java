@@ -29,11 +29,11 @@ import lombok.ToString;
 @Getter
 @Setter
 @ToString(callSuper = true)
-public class InstagramCarouselMediaItem {
-    private String id;
-    private String media_type;
-    private int original_width;
-    private int original_height;
-    private long pk;
+public class InstagramCarouselMediaItem extends InstagramFeedItem {
+//    private String id;
+//    private String media_type;
+//    private int original_width;
+//    private int original_height;
+//    private long pk;
     private String carousel_parent_id;
 }
