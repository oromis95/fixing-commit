@@ -101,7 +101,7 @@ public class InstagramConfigureVideoRequest extends InstagramPostRequest<StatusR
     @Override
     @SneakyThrows
     public StatusResult parseResult(int statusCode, String content) {
-        return parseJson(content, StatusResult.class);
+        return parseJson(statusCode, content, StatusResult.class);
     }
     
 }
