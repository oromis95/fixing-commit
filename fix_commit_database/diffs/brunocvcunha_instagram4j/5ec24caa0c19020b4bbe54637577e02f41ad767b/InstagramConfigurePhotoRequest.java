@@ -93,7 +93,7 @@ public class InstagramConfigurePhotoRequest extends InstagramPostRequest<StatusR
     @Override
     @SneakyThrows
     public StatusResult parseResult(int statusCode, String content) {
-        return parseJson(content, StatusResult.class);
+        return parseJson(statusCode, content, StatusResult.class);
     }
     
 }
