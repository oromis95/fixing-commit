@@ -44,7 +44,7 @@ public class InstagramGetInboxRequest extends InstagramGetRequest<StatusResult>
     @Override
     @SneakyThrows
     public StatusResult parseResult(int statusCode, String content) {
-        return parseJson(content, StatusResult.class);
+        return parseJson(statusCode, content, StatusResult.class);
     }
 
 }
