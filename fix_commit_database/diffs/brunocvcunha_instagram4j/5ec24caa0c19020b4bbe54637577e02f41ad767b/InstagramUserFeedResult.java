@@ -46,7 +46,7 @@ public class InstagramUserFeedResult extends InstagramGetRequest<InstagramFeedRe
     @Override
     @SneakyThrows
     public InstagramFeedResult parseResult(int statusCode, String content) {
-        return parseJson(content, InstagramFeedResult.class);
+        return parseJson(statusCode, content, InstagramFeedResult.class);
     }
 
 }
