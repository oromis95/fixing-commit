@@ -66,6 +66,6 @@ public class InstagramExposeRequest extends InstagramPostRequest<StatusResult> {
     @Override
     @SneakyThrows
     public StatusResult parseResult(int statusCode, String content) {
-        return parseJson(content, StatusResult.class);
+        return parseJson(statusCode, content, StatusResult.class);
     }
 }
