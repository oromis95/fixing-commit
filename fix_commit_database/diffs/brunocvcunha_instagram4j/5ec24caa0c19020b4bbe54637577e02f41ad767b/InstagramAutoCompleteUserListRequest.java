@@ -44,7 +44,7 @@ public class InstagramAutoCompleteUserListRequest extends InstagramGetRequest<St
     @Override
     @SneakyThrows
     public StatusResult parseResult(int statusCode, String content) {
-        return parseJson(content, StatusResult.class);
+        return parseJson(statusCode, content, StatusResult.class);
     }
 
 }
