@@ -136,8 +136,8 @@ public class InstagramUploadVideoJobRequest extends InstagramRequest<StatusResul
     }
 
     @Override
-    public StatusResult parseResult(int resultCode, String content) {
-        return parseJson(content, StatusResult.class);
+    public StatusResult parseResult(int statusCode, String content) {
+        return parseJson(statusCode, content, StatusResult.class);
     }
 
 }
