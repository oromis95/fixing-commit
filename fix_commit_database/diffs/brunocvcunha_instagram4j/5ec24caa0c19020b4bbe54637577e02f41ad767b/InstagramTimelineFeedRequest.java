@@ -44,7 +44,7 @@ public class InstagramTimelineFeedRequest extends InstagramGetRequest<StatusResu
     @Override
     @SneakyThrows
     public StatusResult parseResult(int statusCode, String content) {
-        return parseJson(content, StatusResult.class);
+        return parseJson(statusCode, content, StatusResult.class);
     }
 
 }
