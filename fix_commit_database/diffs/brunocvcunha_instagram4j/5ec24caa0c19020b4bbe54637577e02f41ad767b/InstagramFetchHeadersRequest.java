@@ -50,7 +50,7 @@ public class InstagramFetchHeadersRequest extends InstagramGetRequest<StatusResu
     @Override
     @SneakyThrows
     public StatusResult parseResult(int statusCode, String content) {
-        return parseJson(content, StatusResult.class);
+        return parseJson(statusCode, content, StatusResult.class);
     }
 
 }
