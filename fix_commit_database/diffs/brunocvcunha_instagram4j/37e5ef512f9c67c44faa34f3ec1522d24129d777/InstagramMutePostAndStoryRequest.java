@@ -27,7 +27,7 @@ import java.util.Map;
 /**
  * Comment Post Request
  *
- * @author Malloum LAYA & Stephane Sabalot
+ * @author Malloum LAYA &amp; Stephane Sabalot
  *
  */
 @AllArgsConstructor
@@ -44,7 +44,7 @@ public class InstagramMutePostAndStoryRequest extends InstagramPostRequest<Insta
     @Override
     @SneakyThrows
     public String getPayload() {
-        
+
         Map<String, Object> likeMap = new LinkedHashMap<>();
         likeMap.put("_uuid", api.getUuid());
         likeMap.put("_uid", api.getUserId());
