@@ -40,13 +40,13 @@ public class Resources {
         try {
             BACKGROUND_IMAGE = upscale(ImageIO.read(new File("assets/bg.png")));
             GROUND_IMAGE = upscale(ImageIO.read(new File("assets/ground.png")));
-            final BufferedImage birdImage = ImageIO.read(new File("bird.png"));
+            final BufferedImage birdImage = ImageIO.read(new File("assets/bird.png"));
             BIRD_IMAGES = new BufferedImage[] {
                     upscale(birdImage.getSubimage(0, 0, 36, 26)),
                     upscale(birdImage.getSubimage(36, 0, 36, 26)),
                     upscale(birdImage.getSubimage(72, 0, 36, 26)) };
-            TUBE_UP_IMAGE = upscale(ImageIO.read(new File("tube1.png")));
-            TUBE_DOWN_IMAGE = upscale(ImageIO.read(new File("tube2.png")));
+            TUBE_UP_IMAGE = upscale(ImageIO.read(new File("assets/tube1.png")));
+            TUBE_DOWN_IMAGE = upscale(ImageIO.read(new File("assets/tube2.png")));
         } catch (Exception e) {
             System.out.println("[ERROR] Cannot load resources!");
         }
