@@ -128,7 +128,7 @@ public class VertxRPCServer extends RPCBase implements RPCServer {
 
   private void replyFail(Throwable ex, Message<byte[]> message) {
     log.error(ex.getMessage(), ex);
-    message.fail(500, ex.getClass().getName() + "|" + ex.getMessage());
+    message.fail(500, ex.getClass().getName() + "|" + (ex.getMessage() == null ? ex.getStackTrace()[0].getMethodName() : ex.getMessage()));
   }
 
   @Override
