@@ -11,21 +11,21 @@ import junit.framework.TestCase;
 
 public class CycleTest extends TestCase {
 
-	private Graph G;
+	private Graph graph;
 	public void setUp() {
 		Scanner scn = null;
 		try {
-			URL url = this.getClass().getResource("/tinyCyclicGraph.txt");
+			URL url = this.getClass().getClassLoader().getResource("tinyCyclicGraph.txt");
 			scn = new Scanner(new File(url.getFile()));
 		} catch (Exception e) {
 			e.printStackTrace();
 		}
-		this.G = new Graph(scn);
+		this.graph = new Graph(scn);
 
 	}
 	
 	public void testBasicCycleDetection() {
-		Cycle cycle = new Cycle(G, 0);
-		assertEquals(true, cycle.hasCycle());
+		Cycle cycle = new Cycle(graph, 0);
+		assertTrue(cycle.hasCycle());
 	}
 }
