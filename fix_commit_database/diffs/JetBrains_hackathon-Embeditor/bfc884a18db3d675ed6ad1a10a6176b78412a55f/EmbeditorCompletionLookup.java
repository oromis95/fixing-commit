@@ -44,6 +44,7 @@ public class EmbeditorCompletionLookup extends LightweightHint {
   private final EmbeditorLookupCellRenderer myCellRenderer;
 
   private boolean myShown = false;
+  private Integer myPrefixLength;
 
   public EmbeditorCompletionLookup(Project project, final JBTerminal terminal) {
     super(new JPanel(new BorderLayout()));
@@ -58,7 +59,7 @@ public class EmbeditorCompletionLookup extends LightweightHint {
         if ((keyChar == KeyEvent.VK_ENTER || keyChar == KeyEvent.VK_TAB)) {
           LookupElement lookupElement = (LookupElement)myList.getSelectedValue();
           try {
-            terminal.getEmulator().sendString(lookupElement.getLookupString()); //TODO: handle prefix
+            terminal.getEmulator().sendString(lookupElement.getLookupString().substring(myPrefixLength));
           }
           catch (IOException e1) {
             e1.printStackTrace();  //TODO
@@ -141,6 +142,10 @@ public class EmbeditorCompletionLookup extends LightweightHint {
     }
   }
 
+  public void setPrefixLength(Integer prefixLength) {
+    myPrefixLength = prefixLength;
+  }
+
 
   private class LookupLayeredPane extends JBLayeredPane {
     final JPanel mainPanel = new JPanel(new BorderLayout());
