@@ -20,6 +20,7 @@ import java.util.ArrayList;
 import java.util.List;
 import java.util.Optional;
 import java.util.concurrent.CompletableFuture;
+import java.util.concurrent.CompletionException;
 import java.util.concurrent.CompletionStage;
 import java.util.concurrent.ScheduledExecutorService;
 import java.util.concurrent.ScheduledFuture;
@@ -32,6 +33,12 @@ import java.util.stream.Collector;
 import static java.util.stream.Collectors.collectingAndThen;
 import static java.util.stream.Collectors.toList;
 
+/**
+ * A collection of static utility methods that extend the
+ * {@link java.util.concurrent.CompletableFuture Java completable future} API.
+ *
+ * @since 0.1.0
+ */
 public final class CompletableFutures {
 
   private CompletableFutures() {
@@ -42,14 +49,17 @@ public final class CompletableFutures {
    * Returns a new {@link CompletableFuture} which completes to a list of all values of its input
    * stages, if all succeed.  The list of results is in the same order as the input stages.
    *
-   * <p>If any of the given stages complete exceptionally, then the returned future also does so,
-   * with a CompletionException holding this exception as its cause.
+   * <p> If any of the given stages complete exceptionally, then the returned future also does so,
+   * with a {@link CompletionException} holding this exception as its cause.
    *
-   * <p>If no stages are provided, returns a future holding an empty list.
+   * <p> If no stages are provided, returns a future holding an empty list.
    *
-   * @param stages The stages to combine.
-   * @return A future that completes to a list of the results of the supplied stages.
-   * @throws NullPointerException if the stages list or any of its elements are {@code null}.
+   * @param stages the stages to combine
+   * @param <T>    the common super-type of all of the input stages, that determines the monomorphic
+   *               type of the output future
+   * @return a future that completes to a list of the results of the supplied stages
+   * @throws NullPointerException if the stages list or any of its elements are {@code null}
+   * @since 0.1.0
    */
   public static <T> CompletableFuture<List<T>> allAsList(
       List<? extends CompletionStage<? extends T>> stages) {
@@ -76,8 +86,12 @@ public final class CompletableFutures {
    * the given exception.
    *
    * @param throwable the exception
+   * @param <T>       an arbitrary type for the returned future; can be anything since the future
+   *                  will be exceptionally completed and thus there will never be a value of type
+   *                  {@code T}
    * @return a future that exceptionally completed with the supplied exception
    * @throws NullPointerException if the supplied throwable is {@code null}
+   * @since 0.1.0
    */
   public static <T> CompletableFuture<T> exceptionallyCompletedFuture(Throwable throwable) {
     final CompletableFuture<T> future = new CompletableFuture<>();
@@ -88,20 +102,29 @@ public final class CompletableFutures {
   /**
    * Collect a stream of {@link CompletionStage}s into a single future holding a list of the
    * joined entities.
-   * Usage:
-   * <pre>
-   *   collection.stream()
-   *      .map(this::someAsyncFunc)
-   *      .collect(joinList())
-   *      .thenApply(this::consumeList)
-   * </pre>
-   * The generated CompletableFuture will complete to a list of all entities, in the order they were
-   * encountered in the original stream. Similar to
+   *
+   * <p> Usage:
+   *
+   * <pre>{@code
+   * collection.stream()
+   *     .map(this::someAsyncFunc)
+   *     .collect(joinList())
+   *     .thenApply(this::consumeList)
+   * }</pre>
+   *
+   * <p> The generated {@link CompletableFuture} will complete to a list of all entities, in the
+   * order they were encountered in the original stream.  Similar to
    * {@link CompletableFuture#allOf(CompletableFuture[])}, if any of the input futures complete
-   * exceptionally, then the returned CompletableFuture also does so, with a CompletionException
-   * holding this exception as its cause.
+   * exceptionally, then the returned CompletableFuture also does so, with a
+   * {@link CompletionException} holding this exception as its cause.
    *
-   * @throws NullPointerException if any future in the stream is {@code null}.
+   * @param <T> the common super-type of all of the input stages, that determines the monomorphic
+   *            type of the output future
+   * @param <S> the implementation of {@link CompletionStage} that the stream contains
+   * @return a new {@link CompletableFuture} according to the rules outlined in the method
+   * description
+   * @throws NullPointerException if any future in the stream is {@code null}
+   * @since 0.1.0
    */
   public static <T, S extends CompletionStage<? extends T>>
   Collector<S, ?, CompletableFuture<List<T>>> joinList() {
@@ -109,22 +132,26 @@ public final class CompletableFutures {
   }
 
   /**
-   * Check that a stage is completed.
-   * @param stage a {@link CompletionStage}.
-   * @throws IllegalStateException if the stage is not completed.
+   * Checks that a stage is completed.
+   *
+   * @param stage the {@link CompletionStage} to check
+   * @throws IllegalStateException if the stage is not completed
+   * @since 0.1.0
    */
-  public static <T> void checkCompleted(CompletionStage<T> stage) {
+  public static void checkCompleted(CompletionStage<?> stage) {
     if (!stage.toCompletableFuture().isDone()) {
       throw new IllegalStateException("future was not completed");
     }
   }
 
   /**
-   * Get the value of a completed stage.
+   * Gets the value of a completed stage.
    *
-   * @param stage a completed {@link CompletionStage}.
-   * @return the value of the stage if it has one.
-   * @throws IllegalStateException if the stage is not completed.
+   * @param stage a completed {@link CompletionStage}
+   * @param <T>   the type of the value that the stage completes into
+   * @return the value of the stage if it has one
+   * @throws IllegalStateException if the stage is not completed
+   * @since 0.1.0
    */
   public static <T> T getCompleted(CompletionStage<T> stage) {
     CompletableFuture<T> future = stage.toCompletableFuture();
@@ -133,25 +160,25 @@ public final class CompletableFutures {
   }
 
   /**
-   * Returns a new stage that, when this stage completes
-   * either normally or exceptionally, is executed with this stage's
-   * result and exception as arguments to the supplied function.
+   * Returns a new stage that, when this stage completes either normally or exceptionally, is
+   * executed with this stage's result and exception as arguments to the supplied function.
    *
-   * <p>When this stage is complete, the given function is invoked
-   * with the result (or {@code null} if none) and the exception (or
-   * {@code null} if none) of this stage as arguments, and the
+   * <p> When this stage is complete, the given function is invoked with the result (or {@code null}
+   * if none) and the exception (or {@code null} if none) of this stage as arguments, and the
    * function's result is used to complete the returned stage.
    *
-   * <p>This differs from
-   * {@link java.util.concurrent.CompletionStage#handle(java.util.function.BiFunction)}
-   * in that the function should return a {@link java.util.concurrent.CompletionStage} rather than
-   * the value directly.
+   * <p> This differs from
+   * {@link java.util.concurrent.CompletionStage#handle(java.util.function.BiFunction)} in that the
+   * function should return a {@link java.util.concurrent.CompletionStage} rather than the value
+   * directly.
    *
    * @param stage the {@link CompletionStage} to compose
-   * @param fn the function to use to compute the value of the
-   * returned {@link CompletionStage}
-   * @param <U> the function's return type
+   * @param fn    the function to use to compute the value of the
+   *              returned {@link CompletionStage}
+   * @param <T>   the type of the input stage's value.
+   * @param <U>   the function's return type
    * @return the new {@link CompletionStage}
+   * @since 0.1.0
    */
   public static <T, U> CompletionStage<U> handleCompose(
       CompletionStage<T> stage,
@@ -172,10 +199,12 @@ public final class CompletableFutures {
    * the value directly.
    *
    * @param stage the {@link CompletionStage} to compose
-   * @param fn the function to use to compute the value of the
-   * returned {@link CompletionStage} if this stage completed
-   * exceptionally
+   * @param fn    the function to use to compute the value of the
+   *              returned {@link CompletionStage} if this stage completed
+   *              exceptionally
+   * @param <T>   the type of the input stage's value.
    * @return the new {@link CompletionStage}
+   * @since 0.1.0
    */
   public static <T> CompletionStage<T> exceptionallyCompose(
       CompletionStage<T> stage,
@@ -187,7 +216,9 @@ public final class CompletableFutures {
    * This takes a stage of a stage of a value and returns a plain stage of a value.
    *
    * @param stage a {@link CompletionStage} of a {@link CompletionStage} of a value
+   * @param <T>   the type of the inner stage's value.
    * @return the {@link CompletionStage} of the value
+   * @since 0.1.0
    */
   public static <T> CompletionStage<T> dereference(
       CompletionStage<? extends CompletionStage<T>> stage) {
@@ -199,12 +230,38 @@ public final class CompletableFutures {
     return future.thenApply(CompletableFuture::completedFuture);
   }
 
+  /**
+   * Combines multiple stages by applying a function.
+   *
+   * @param a        the first stage.
+   * @param b        the second stage.
+   * @param function the combining function.
+   * @param <R>      the type of the combining function's return value.
+   * @param <A>      the type of the first stage's value.
+   * @param <B>      the type of the second stage's value.
+   * @return a stage that completes into the return value of the supplied function.
+   * @since 0.1.0
+   */
   public static <R, A, B> CompletionStage<R> combine(
       CompletionStage<A> a, CompletionStage<B> b,
       BiFunction<A, B, R> function) {
     return a.thenCombine(b, function);
   }
 
+  /**
+   * Combines multiple stages by applying a function.
+   *
+   * @param a        the first stage.
+   * @param b        the second stage.
+   * @param c        the third stage.
+   * @param function the combining function.
+   * @param <R>      the type of the combining function's return value.
+   * @param <A>      the type of the first stage's value.
+   * @param <B>      the type of the second stage's value.
+   * @param <C>      the type of the third stage's value.
+   * @return a stage that completes into the return value of the supplied function.
+   * @since 0.1.0
+   */
   public static <R, A, B, C> CompletionStage<R> combine(
       CompletionStage<A> a, CompletionStage<B> b, CompletionStage<C> c,
       Function3<A, B, C, R> function) {
@@ -216,6 +273,22 @@ public final class CompletableFutures {
         .thenApply(ignored -> function.apply(af.join(), bf.join(), cf.join()));
   }
 
+  /**
+   * Combines multiple stages by applying a function.
+   *
+   * @param a        the first stage.
+   * @param b        the second stage.
+   * @param c        the third stage.
+   * @param d        the fourth stage.
+   * @param function the combining function.
+   * @param <R>      the type of the combining function's return value.
+   * @param <A>      the type of the first stage's value.
+   * @param <B>      the type of the second stage's value.
+   * @param <C>      the type of the third stage's value.
+   * @param <D>      the type of the fourth stage's value.
+   * @return a stage that completes into the return value of the supplied function.
+   * @since 0.1.0
+   */
   public static <R, A, B, C, D> CompletionStage<R> combine(
       CompletionStage<A> a, CompletionStage<B> b, CompletionStage<C> c, CompletionStage<D> d,
       Function4<A, B, C, D, R> function) {
@@ -228,6 +301,25 @@ public final class CompletableFutures {
         .thenApply(ignored -> function.apply(af.join(), bf.join(), cf.join(), df.join()));
   }
 
+
+  /**
+   * Combines multiple stages by applying a function.
+   *
+   * @param a        the first stage.
+   * @param b        the second stage.
+   * @param c        the third stage.
+   * @param d        the fourth stage.
+   * @param e        the fifth stage.
+   * @param function the combining function.
+   * @param <R>      the type of the combining function's return value.
+   * @param <A>      the type of the first stage's value.
+   * @param <B>      the type of the second stage's value.
+   * @param <C>      the type of the third stage's value.
+   * @param <D>      the type of the fourth stage's value.
+   * @param <E>      the type of the fifth stage's value.
+   * @return a stage that completes into the return value of the supplied function.
+   * @since 0.1.0
+   */
   public static <R, A, B, C, D, E> CompletionStage<R> combine(
       CompletionStage<A> a, CompletionStage<B> b, CompletionStage<C> c,
       CompletionStage<D> d, CompletionStage<E> e,
@@ -244,23 +336,25 @@ public final class CompletableFutures {
   }
 
   /**
-   * Periodically poll an external resource until it returns a non-empty result.
+   * Polls an external resource periodically until it returns a non-empty result.
    *
-   * <p>The polling task should return Optional.empty() until it becomes available, and then
-   * Optional.of(result). If the polling task throws an exception or returns null, that will cause
-   * the result future to complete exceptionally.
+   * <p> The polling task should return {@code Optional.empty()} until it becomes available, and
+   * then {@code Optional.of(result)}.  If the polling task throws an exception or returns null,
+   * that will cause the result future to complete exceptionally.
    *
-   * <p>Canceling the returned future will cancel the scheduled polling task as well.
+   * <p> Canceling the returned future will cancel the scheduled polling task as well.
    *
-   * <p>Note that on a ScheduledThreadPoolExecutor the polling task might remain allocated for up
-   * to 'frequency' time after completing or being cancelled. If you have lots of polling operations
-   * or a long polling frequency, consider setting removeOnCancelPolicy to true.
+   * <p> Note that on a ScheduledThreadPoolExecutor the polling task might remain allocated for up
+   * to {@code frequency} time after completing or being cancelled.  If you have lots of polling
+   * operations or a long polling frequency, consider setting {@code removeOnCancelPolicy} to true.
    * See {@link java.util.concurrent.ScheduledThreadPoolExecutor#setRemoveOnCancelPolicy(boolean)}.
    *
-   * @param pollingTask the polling task.
-   * @param frequency the frequency to run the polling task at.
-   * @param executorService the executor service to schedule the polling task on.
-   * @return a future completing to the result of the polling task once that becomes available.
+   * @param pollingTask     the polling task
+   * @param frequency       the frequency to run the polling task at
+   * @param executorService the executor service to schedule the polling task on
+   * @param <T>             the type of the result of the polling task, that will be returned when
+   *                        the task succeeds.
+   * @return a future completing to the result of the polling task once that becomes available
    */
   public static <T> CompletableFuture<T> poll(
       final Supplier<Optional<T>> pollingTask,
