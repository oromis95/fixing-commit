@@ -30,7 +30,6 @@ import java.util.List;
 import java.util.Optional;
 import java.util.concurrent.CompletableFuture;
 import java.util.concurrent.CompletionStage;
-import java.util.concurrent.Executors;
 import java.util.concurrent.ScheduledExecutorService;
 import java.util.concurrent.ScheduledFuture;
 import java.util.function.Supplier;
@@ -54,7 +53,6 @@ import static java.util.concurrent.TimeUnit.SECONDS;
 import static java.util.stream.Collectors.toList;
 import static org.hamcrest.Matchers.both;
 import static org.hamcrest.Matchers.contains;
-import static org.hamcrest.Matchers.empty;
 import static org.hamcrest.Matchers.hasProperty;
 import static org.hamcrest.Matchers.hasSize;
 import static org.hamcrest.Matchers.nullValue;
@@ -559,14 +557,12 @@ public class CompletableFuturesTest {
 
   @Test
   public void poll_notRunningAfterCancel() throws Exception {
-    final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
     final CompletableFuture<String> future = poll(Optional::empty, Duration.ofMillis(2), executor);
 
     future.cancel(true);
-    Thread.sleep(10);
 
-    final List<Runnable> running = executor.shutdownNow();
-    assertThat(running, is(empty()));
+    executor.tick(5, MILLISECONDS);
+    assertThat(executor.isIdle(), is(true));
   }
 
   private static <T> CompletableFuture<T> incompleteFuture() {
