@@ -2,6 +2,7 @@
 package com.esotericsoftware.reflectasm;
 
 import java.lang.reflect.Method;
+import java.security.ProtectionDomain;
 import java.util.ArrayList;
 
 class AccessClassLoader extends ClassLoader {
@@ -46,9 +47,10 @@ class AccessClassLoader extends ClassLoader {
 		try {
 			// Attempt to load the access class in the same loader, which makes protected and default access members accessible.
 			Method method = ClassLoader.class.getDeclaredMethod("defineClass", new Class[] {String.class, byte[].class, int.class,
-				int.class});
+				int.class, ProtectionDomain.class});
 			method.setAccessible(true);
-			return (Class)method.invoke(getParent(), new Object[] {name, bytes, Integer.valueOf(0), Integer.valueOf(bytes.length)});
+			return (Class)method.invoke(getParent(), new Object[] {name, bytes, Integer.valueOf(0), Integer.valueOf(bytes.length),
+				getClass().getProtectionDomain()});
 		} catch (Exception ignored) {
 		}
 		return defineClass(name, bytes, 0, bytes.length);
