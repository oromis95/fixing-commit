@@ -59,69 +59,71 @@ public abstract class FieldAccess {
 				mv.visitCode();
 				mv.visitVarInsn(ILOAD, 2);
 
-				Label[] labels = new Label[fields.size()];
-				for (int i = 0, n = labels.length; i < n; i++)
-					labels[i] = new Label();
-				Label defaultLabel = new Label();
-				mv.visitTableSwitchInsn(0, labels.length - 1, defaultLabel, labels);
-
-				for (int i = 0, n = labels.length; i < n; i++) {
-					Field field = fields.get(i);
-					Type fieldType = Type.getType(field.getType());
-
-					mv.visitLabel(labels[i]);
-					mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
-					mv.visitVarInsn(ALOAD, 1);
-					mv.visitTypeInsn(CHECKCAST, classNameInternal);
-					mv.visitVarInsn(ALOAD, 3);
-
-					switch (fieldType.getSort()) {
-					case Type.BOOLEAN:
-						mv.visitTypeInsn(CHECKCAST, "java/lang/Boolean");
-						mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Boolean", "booleanValue", "()Z");
-						break;
-					case Type.BYTE:
-						mv.visitTypeInsn(CHECKCAST, "java/lang/Byte");
-						mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Byte", "byteValue", "()B");
-						break;
-					case Type.CHAR:
-						mv.visitTypeInsn(CHECKCAST, "java/lang/Character");
-						mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Character", "charValue", "()C");
-						break;
-					case Type.SHORT:
-						mv.visitTypeInsn(CHECKCAST, "java/lang/Short");
-						mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Short", "shortValue", "()S");
-						break;
-					case Type.INT:
-						mv.visitTypeInsn(CHECKCAST, "java/lang/Integer");
-						mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I");
-						break;
-					case Type.FLOAT:
-						mv.visitTypeInsn(CHECKCAST, "java/lang/Float");
-						mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Float", "floatValue", "()F");
-						break;
-					case Type.LONG:
-						mv.visitTypeInsn(CHECKCAST, "java/lang/Long");
-						mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Long", "longValue", "()J");
-						break;
-					case Type.DOUBLE:
-						mv.visitTypeInsn(CHECKCAST, "java/lang/Double");
-						mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Double", "doubleValue", "()D");
-						break;
-					case Type.ARRAY:
-						mv.visitTypeInsn(CHECKCAST, fieldType.getDescriptor());
-						break;
-					case Type.OBJECT:
-						mv.visitTypeInsn(CHECKCAST, fieldType.getInternalName());
-						break;
+				if (!fields.isEmpty()) {
+					Label[] labels = new Label[fields.size()];
+					for (int i = 0, n = labels.length; i < n; i++)
+						labels[i] = new Label();
+					Label defaultLabel = new Label();
+					mv.visitTableSwitchInsn(0, labels.length - 1, defaultLabel, labels);
+
+					for (int i = 0, n = labels.length; i < n; i++) {
+						Field field = fields.get(i);
+						Type fieldType = Type.getType(field.getType());
+
+						mv.visitLabel(labels[i]);
+						mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
+						mv.visitVarInsn(ALOAD, 1);
+						mv.visitTypeInsn(CHECKCAST, classNameInternal);
+						mv.visitVarInsn(ALOAD, 3);
+
+						switch (fieldType.getSort()) {
+						case Type.BOOLEAN:
+							mv.visitTypeInsn(CHECKCAST, "java/lang/Boolean");
+							mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Boolean", "booleanValue", "()Z");
+							break;
+						case Type.BYTE:
+							mv.visitTypeInsn(CHECKCAST, "java/lang/Byte");
+							mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Byte", "byteValue", "()B");
+							break;
+						case Type.CHAR:
+							mv.visitTypeInsn(CHECKCAST, "java/lang/Character");
+							mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Character", "charValue", "()C");
+							break;
+						case Type.SHORT:
+							mv.visitTypeInsn(CHECKCAST, "java/lang/Short");
+							mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Short", "shortValue", "()S");
+							break;
+						case Type.INT:
+							mv.visitTypeInsn(CHECKCAST, "java/lang/Integer");
+							mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I");
+							break;
+						case Type.FLOAT:
+							mv.visitTypeInsn(CHECKCAST, "java/lang/Float");
+							mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Float", "floatValue", "()F");
+							break;
+						case Type.LONG:
+							mv.visitTypeInsn(CHECKCAST, "java/lang/Long");
+							mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Long", "longValue", "()J");
+							break;
+						case Type.DOUBLE:
+							mv.visitTypeInsn(CHECKCAST, "java/lang/Double");
+							mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Double", "doubleValue", "()D");
+							break;
+						case Type.ARRAY:
+							mv.visitTypeInsn(CHECKCAST, fieldType.getDescriptor());
+							break;
+						case Type.OBJECT:
+							mv.visitTypeInsn(CHECKCAST, fieldType.getInternalName());
+							break;
+						}
+
+						mv.visitFieldInsn(PUTFIELD, classNameInternal, field.getName(), fieldType.getDescriptor());
+						mv.visitInsn(RETURN);
 					}
 
-					mv.visitFieldInsn(PUTFIELD, classNameInternal, field.getName(), fieldType.getDescriptor());
-					mv.visitInsn(RETURN);
+					mv.visitLabel(defaultLabel);
+					mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
 				}
-
-				mv.visitLabel(defaultLabel);
-				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
 				mv.visitTypeInsn(NEW, "java/lang/IllegalArgumentException");
 				mv.visitInsn(DUP);
 				mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
@@ -133,7 +135,7 @@ public abstract class FieldAccess {
 				mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;");
 				mv.visitMethodInsn(INVOKESPECIAL, "java/lang/IllegalArgumentException", "<init>", "(Ljava/lang/String;)V");
 				mv.visitInsn(ATHROW);
-				mv.visitMaxs(5, 4);
+				mv.visitMaxs(6, 5);
 				mv.visitEnd();
 			}
 			{
@@ -141,54 +143,56 @@ public abstract class FieldAccess {
 				mv.visitCode();
 				mv.visitVarInsn(ILOAD, 2);
 
-				Label[] labels = new Label[fields.size()];
-				for (int i = 0, n = labels.length; i < n; i++)
-					labels[i] = new Label();
-				Label defaultLabel = new Label();
-				mv.visitTableSwitchInsn(0, labels.length - 1, defaultLabel, labels);
-
-				for (int i = 0, n = labels.length; i < n; i++) {
-					Field field = fields.get(i);
-
-					mv.visitLabel(labels[i]);
-					mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
-					mv.visitVarInsn(ALOAD, 1);
-					mv.visitTypeInsn(CHECKCAST, classNameInternal);
-					mv.visitFieldInsn(GETFIELD, classNameInternal, field.getName(), Type.getDescriptor(field.getType()));
-
-					Type fieldType = Type.getType(field.getType());
-					switch (fieldType.getSort()) {
-					case Type.BOOLEAN:
-						mv.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;");
-						break;
-					case Type.BYTE:
-						mv.visitMethodInsn(INVOKESTATIC, "java/lang/Byte", "valueOf", "(B)Ljava/lang/Byte;");
-						break;
-					case Type.CHAR:
-						mv.visitMethodInsn(INVOKESTATIC, "java/lang/Character", "valueOf", "(C)Ljava/lang/Character;");
-						break;
-					case Type.SHORT:
-						mv.visitMethodInsn(INVOKESTATIC, "java/lang/Short", "valueOf", "(S)Ljava/lang/Short;");
-						break;
-					case Type.INT:
-						mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
-						break;
-					case Type.FLOAT:
-						mv.visitMethodInsn(INVOKESTATIC, "java/lang/Float", "valueOf", "(F)Ljava/lang/Float;");
-						break;
-					case Type.LONG:
-						mv.visitMethodInsn(INVOKESTATIC, "java/lang/Long", "valueOf", "(J)Ljava/lang/Long;");
-						break;
-					case Type.DOUBLE:
-						mv.visitMethodInsn(INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;");
-						break;
+				if (!fields.isEmpty()) {
+					Label[] labels = new Label[fields.size()];
+					for (int i = 0, n = labels.length; i < n; i++)
+						labels[i] = new Label();
+					Label defaultLabel = new Label();
+					mv.visitTableSwitchInsn(0, labels.length - 1, defaultLabel, labels);
+
+					for (int i = 0, n = labels.length; i < n; i++) {
+						Field field = fields.get(i);
+
+						mv.visitLabel(labels[i]);
+						mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
+						mv.visitVarInsn(ALOAD, 1);
+						mv.visitTypeInsn(CHECKCAST, classNameInternal);
+						mv.visitFieldInsn(GETFIELD, classNameInternal, field.getName(), Type.getDescriptor(field.getType()));
+
+						Type fieldType = Type.getType(field.getType());
+						switch (fieldType.getSort()) {
+						case Type.BOOLEAN:
+							mv.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;");
+							break;
+						case Type.BYTE:
+							mv.visitMethodInsn(INVOKESTATIC, "java/lang/Byte", "valueOf", "(B)Ljava/lang/Byte;");
+							break;
+						case Type.CHAR:
+							mv.visitMethodInsn(INVOKESTATIC, "java/lang/Character", "valueOf", "(C)Ljava/lang/Character;");
+							break;
+						case Type.SHORT:
+							mv.visitMethodInsn(INVOKESTATIC, "java/lang/Short", "valueOf", "(S)Ljava/lang/Short;");
+							break;
+						case Type.INT:
+							mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
+							break;
+						case Type.FLOAT:
+							mv.visitMethodInsn(INVOKESTATIC, "java/lang/Float", "valueOf", "(F)Ljava/lang/Float;");
+							break;
+						case Type.LONG:
+							mv.visitMethodInsn(INVOKESTATIC, "java/lang/Long", "valueOf", "(J)Ljava/lang/Long;");
+							break;
+						case Type.DOUBLE:
+							mv.visitMethodInsn(INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;");
+							break;
+						}
+
+						mv.visitInsn(ARETURN);
 					}
 
-					mv.visitInsn(ARETURN);
+					mv.visitLabel(defaultLabel);
+					mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
 				}
-
-				mv.visitLabel(defaultLabel);
-				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
 				mv.visitTypeInsn(NEW, "java/lang/IllegalArgumentException");
 				mv.visitInsn(DUP);
 				mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
@@ -200,7 +204,7 @@ public abstract class FieldAccess {
 				mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;");
 				mv.visitMethodInsn(INVOKESPECIAL, "java/lang/IllegalArgumentException", "<init>", "(Ljava/lang/String;)V");
 				mv.visitInsn(ATHROW);
-				mv.visitMaxs(5, 3);
+				mv.visitMaxs(6, 4);
 				mv.visitEnd();
 			}
 			cw.visitEnd();
