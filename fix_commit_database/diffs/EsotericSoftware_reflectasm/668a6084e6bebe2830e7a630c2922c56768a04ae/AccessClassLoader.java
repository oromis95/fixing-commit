@@ -4,10 +4,8 @@ package com.esotericsoftware.reflectasm;
 class AccessClassLoader extends ClassLoader {
 	static private AccessClassLoader instance;
 
-	static synchronized AccessClassLoader getInstance () {
-		// Initialize lazily to avoid ExceptionInInitializerError when lacking the createClassLoader RuntimePermission.
-		if (instance == null) instance = new AccessClassLoader();
-		return instance;
+	AccessClassLoader (ClassLoader parent) {
+		super(parent);
 	}
 
 	Class<?> defineClass (String name, byte[] bytes) throws ClassFormatError {
