@@ -19,8 +19,6 @@ public class DownloadThread extends Thread {
     private boolean finish = false;
     private FileDownloader downloader;
 
-    boolean thread_running = true;
-
     public DownloadThread(FileDownloader downloader,
                           URL download_url,
                           File save_file,
@@ -78,7 +76,7 @@ public class DownloadThread extends Thread {
 
                     if (downloader.should_pause) {
                         Log.i("线程可以停止运行了 ", "true");
-                        this.interrupt();
+                        // this.interrupt();
                         return;
                     }
 
