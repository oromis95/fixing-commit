@@ -19,11 +19,14 @@ public class DBOpenHelper extends SQLiteOpenHelper {
     public void onCreate(SQLiteDatabase db) {
         Log.i("调试db 1", "true");
         db.execSQL("CREATE TABLE IF NOT EXISTS filedownlog (id integer primary key autoincrement, downpath varchar(100), threadid INTEGER, downlength INTEGER)");
+        // 这里的 status 字段用来表示暂停或者正在下载, 暂停: pause, 正在下载: download
+        db.execSQL("CREATE TABLE IF NOT EXISTS filesizelog (downpath varchar(100), filesize INTEGER)");
     }
 
     @Override
     public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
         db.execSQL("DROP TABLE IF EXISTS filedownlog");
+        db.execSQL("DROP TABLE IF EXISTS filesizelog");
         onCreate(db);
         Log.i("调试db 2", "true");
     }
