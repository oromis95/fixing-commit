@@ -6,6 +6,7 @@ import java.util.Map;
 import android.content.Context;
 import android.database.Cursor;
 import android.database.sqlite.SQLiteDatabase;
+import android.database.sqlite.SQLiteQueryBuilder;
 import android.util.Log;
 
 
@@ -17,6 +18,79 @@ public class FileRecord {
         db_open_helper = new DBOpenHelper(context);
     }
 
+    public int get_downloaded_size(String path){
+        int downloaded_size = 0;
+        SQLiteDatabase db = db_open_helper.getReadableDatabase();
+        Cursor cursor = db.rawQuery("select sum(downlength) from filedownlog where downpath=?", new String[]{path});
+        if(cursor.moveToFirst()) {
+            downloaded_size = cursor.getInt(0);
+        }
+        cursor.close();
+        db.close();
+
+        Log.i("数据库中的 downloaded_size ", Integer.toString(downloaded_size));
+
+
+//        Map<Integer, Integer> logdata = get_data(path);
+//        if(logdata.size()>0){
+//            for(Map.Entry<Integer, Integer> entry : logdata.entrySet())
+//                downloaded_size += entry.getValue();
+//        }
+//
+//        Log.i("数据库中的 downloaded_size ", Integer.toString(downloaded_size));
+
+        return downloaded_size;
+    }
+
+    public void save_filezie(String path, int filesize) {
+        SQLiteDatabase db;
+        try {
+            db = db_open_helper.getWritableDatabase();
+        } catch (Exception e) {
+            Log.i("getWritableDatabase 错误 ", e.toString());
+            e.printStackTrace();
+            return;
+        }
+
+        if (db == null) {
+            return;
+        }
+
+        db.beginTransaction();
+        try{
+            Cursor cursor = db.rawQuery("select filesize from filesizelog where downpath=?", new String[]{path});
+            if (cursor.getCount() > 0) {
+                db.execSQL("update filesizelog set filesize=? where downpath=?",
+                        new Object[]{filesize, path});
+                Log.i("更新 filesize ", Integer.toString(filesize));
+            } else {
+                db.execSQL("insert into filesizelog(downpath, filesize) values(?,?)",
+                        new Object[]{path, filesize});
+
+                Log.i("保存 filesize ", Integer.toString(filesize));
+            }
+
+            db.setTransactionSuccessful();
+            Log.i("更新 filesize ", "true");
+        }finally{
+            db.endTransaction();
+        }
+        db.close();
+    }
+
+    public int get_filesize(String path){
+        int filesize = 0;
+        SQLiteDatabase db = db_open_helper.getReadableDatabase();
+        Cursor cursor = db.rawQuery("select filesize from filesizelog where downpath=?", new String[]{path});
+        if(cursor.moveToFirst()) {
+            filesize = cursor.getInt(0);
+        }
+        Log.i("filerecord 获取 filesize ", Integer.toString(filesize));
+        cursor.close();
+        db.close();
+        return filesize;
+    }
+
 
     public Map<Integer, Integer> get_data(String path){
         SQLiteDatabase db = db_open_helper.getReadableDatabase();
@@ -32,13 +106,41 @@ public class FileRecord {
 
 
     public void save(String path,  Map<Integer, Integer> map){//int threadid, int position
+
+        Log.i("save运行过 ", "true");
+
+        int downlength;
+        int threadid;
+
         SQLiteDatabase db = db_open_helper.getWritableDatabase();
         db.beginTransaction();
         try{
+
+
+
+
             for(Map.Entry<Integer, Integer> entry : map.entrySet()){
+
+                downlength = entry.getValue();
+                threadid = entry.getKey();
+
+                Log.i("downlength值 ", Integer.toString(downlength));
+
+
+//                Cursor cursor = db.rawQuery("select threadid from filedownlog where downpath=? and threadid=?",
+//                        new String[]{path, String.valueOf(threadid)});
+//                if (cursor.getCount() > 0) {
+//                    Log.i("退出for ", Integer.toString(downlength));
+//                    continue;
+//                }
+
+
                 db.execSQL("insert into filedownlog(downpath, threadid, downlength) values(?,?,?)",
-                        new Object[]{path, entry.getKey(), entry.getValue()});
+                        new Object[]{path, threadid, downlength});
+
+
             }
+
             db.setTransactionSuccessful();
         }finally{
             db.endTransaction();
@@ -47,6 +149,8 @@ public class FileRecord {
     }
 
 
+
+
     public void update(String path, Map<Integer, Integer> map){
         SQLiteDatabase db;
         try {
@@ -78,10 +182,12 @@ public class FileRecord {
     }
 
 
+
     public void delete(String path){
         try {
             SQLiteDatabase db = db_open_helper.getWritableDatabase();
             db.execSQL("delete from filedownlog where downpath=?", new Object[]{path});
+            db.execSQL("delete from filesizelog where downpath=?", new Object[]{path});
             db.close();
         } catch (Exception e) {
             Log.i("数据库删除错误 ", e.toString());
