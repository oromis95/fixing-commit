@@ -3,7 +3,7 @@ package org.orman.sql;
 import org.orman.sql.util.Serializer;
 
 /**
- * Faade class for criterion operations such as creating simple queries,
+ * Fa\u00E7ade class for criterion operations such as creating simple queries,
  * logical expressions, conjuctions and manual criteria etc.
  * 
  * @author ahmet alp balkan <ahmetalpbalkan@gmail.com>
@@ -167,4 +167,4 @@ public class C {
 		return new SimpleBinaryCriterion(field, BinaryOp.LESS_EQUAL,
 				SetQuantifier.ANY, nestedQuery);
 	}
-}
\ No newline at end of file
+}
