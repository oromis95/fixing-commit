@@ -4,7 +4,7 @@ import org.orman.mapper.exception.EntityNotFoundException;
 import org.orman.mapper.exception.FieldNotFoundException;
 
 /**
- * Shorthand faade to provide {@link Field} objects quickly in a
+ * Shorthand fa\u00E7ade to provide {@link Field} objects quickly in a
  * {@link ModelQuery} context.
  * 
  * @author ahmet alp balkan <ahmetalpbalkan@gmail.com>
@@ -71,4 +71,4 @@ public class F {
 
 		return f;
 	}
-}
\ No newline at end of file
+}
