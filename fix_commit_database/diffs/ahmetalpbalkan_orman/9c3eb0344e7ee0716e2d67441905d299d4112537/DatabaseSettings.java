@@ -4,7 +4,7 @@ package org.orman.datasource;
  * Placeholder interface for storing various database 
  * settings.
  * 
- * @author alp
+ * @author ahmet alp balkan <ahmetalpbalkan@gmail.com>
  *
  */
 public interface DatabaseSettings {
