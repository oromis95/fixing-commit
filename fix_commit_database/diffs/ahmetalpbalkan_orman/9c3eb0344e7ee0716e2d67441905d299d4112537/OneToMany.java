@@ -6,15 +6,15 @@ import java.lang.annotation.RetentionPolicy;
 import org.orman.mapper.LoadingPolicy;
 
 /**
- * Place this annotation on columns to create relationship of 1:* cardinality
- * between entities.
+ * <p>Place this annotation on columns to create relationship of 1:* cardinality
+ * between entities.</p>
  * 
- * These fields are not physically created on the database but populated at the
- * runtime.
+ * <p>These fields are not physically created on the database but populated at the
+ * runtime.</p>
  * 
  * CAUTION: Do not forget to use {@link ManyToOne} annotation on the target.
  * 
- * @author alp
+ * @author ahmet alp balkan <ahmetalpbalkan@gmail.com>
  */
 @Retention(RetentionPolicy.RUNTIME)
 public @interface OneToMany {
