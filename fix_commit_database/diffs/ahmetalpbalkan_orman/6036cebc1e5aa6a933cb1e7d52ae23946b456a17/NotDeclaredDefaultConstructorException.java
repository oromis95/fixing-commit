@@ -1,5 +1,6 @@
 package org.orman.mapper.exception;
 
+
 @SuppressWarnings("serial")
 public class NotDeclaredDefaultConstructorException extends RuntimeException {
 	private static String message = "The following class does not define a public default constructor (with no parameters): %s";
