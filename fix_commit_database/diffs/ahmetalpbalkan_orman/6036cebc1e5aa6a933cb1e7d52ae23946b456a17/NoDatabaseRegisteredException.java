@@ -1,5 +1,6 @@
 package org.orman.mapper.exception;
 
+
 public class NoDatabaseRegisteredException extends RuntimeException {
 	
 	public static final String message = "No database registered to the mapping session. Use registerDatabase() once.";
