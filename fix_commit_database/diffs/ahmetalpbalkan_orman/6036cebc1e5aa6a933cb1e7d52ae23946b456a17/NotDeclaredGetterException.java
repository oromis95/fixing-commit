@@ -1,5 +1,6 @@
 package org.orman.mapper.exception;
 
+
 @SuppressWarnings("serial")
 public class NotDeclaredGetterException extends RuntimeException {
 	private static String message = "There is no getter method (non-void and without arguments) declared for non-public field `%s` in %s.";
