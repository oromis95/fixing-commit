@@ -1,8 +1,7 @@
 package org.orman.util;
 
-import java.text.MessageFormat;
-
 import org.apache.log4j.BasicConfigurator;
+import org.apache.log4j.Level;
 import org.apache.log4j.Logger;
 
 public class Log {
@@ -10,6 +9,10 @@ public class Log {
 	static {
 		BasicConfigurator.configure();
 	}
+
+	public static void setLevel(Level level){
+		logger.setLevel(level);
+	}
 	
 	public static Logger getLogger() {
 		return logger;
@@ -19,8 +22,6 @@ public class Log {
 		logger = log;
 	}
 	
-	
-	
 	public static void trace(Object message){
 		getLogger().trace(message);
 	}
