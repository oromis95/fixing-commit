@@ -1,5 +1,6 @@
 package org.orman.mapper.exception;
 
+
 @SuppressWarnings("serial")
 public class NotDeclaredSetterException extends RuntimeException {
 	private static String message = "There is no single-argument setter method declared for non-public field `%s` in %s.";
