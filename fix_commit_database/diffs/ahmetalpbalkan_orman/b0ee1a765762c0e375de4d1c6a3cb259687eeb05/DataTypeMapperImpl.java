@@ -1,6 +1,6 @@
 package org.orman.mysql;
 
-import java.sql.Date;
+import java.util.Date;
 
 import org.orman.datasource.DataTypeMapper;
 import org.orman.util.DoubleAssociativeMap;
@@ -17,9 +17,8 @@ public class DataTypeMapperImpl implements DataTypeMapper {
 		typeMap.put(Boolean.TYPE, "TINYINT(1)");
 		typeMap.put(Double.TYPE, "DOUBLE");
 		typeMap.put(Float.TYPE, "FLOAT");
-		typeMap.put(Date.class, "DATETIME");
 		typeMap.put(Enum.class, "SMALLINT");
-		typeMap.put(java.util.Date.class, "DATETIME");
+		typeMap.put(Date.class, "DATETIME");
 	}
 
 	@Override
