@@ -1,6 +1,8 @@
 package org.orman.mapper;
 
 public class PhysicalNameAndTypeBindingEngine {
+	private static final String INDEX_POSTFIX = "_index";
+
 	public static void makeBinding(Entity entity, PhysicalNamingPolicy namingPolicy){
 		if(entity.getCustomName() != null){
 			// use custom name.
@@ -12,7 +14,7 @@ public class PhysicalNameAndTypeBindingEngine {
 	}
 	
 	public static void makeBinding(Field field, PhysicalNamingPolicy namingPolicy, DataTypeMapper dataTypeMapper){
-		// NAME BINDING
+		/* NAME BINDING */
 		if(field.getCustomName() != null){
 			// use custom name.
 			field.setGeneratedName(field.getCustomName());
@@ -21,7 +23,7 @@ public class PhysicalNameAndTypeBindingEngine {
 			field.setGeneratedName(PhysicalNameGenerator.format(field.getOriginalName(), namingPolicy));
 		}
 		
-		// TYPE BINDING
+		/* TYPE BINDING */
 		if(field.getCustomType() != null){
 			// use manual field type.
 			field.setType(field.getCustomType());
@@ -29,5 +31,13 @@ public class PhysicalNameAndTypeBindingEngine {
 			// use Class<?> type of the field.
 			field.setType(dataTypeMapper.getTypeFor(field.getClazz()));
 		}
+		
+		/* INDEX SETTINGS BINDING */
+		if(field.getIndex() != null){
+			if(field.getIndex().name() == null || "".equals(field.getIndex().name())){
+				// missing index name, create using field name followed by _index policy.
+				field.getIndex().name(field.getGeneratedName()+INDEX_POSTFIX);
+			}
+		}
 	}
 }
\ No newline at end of file
