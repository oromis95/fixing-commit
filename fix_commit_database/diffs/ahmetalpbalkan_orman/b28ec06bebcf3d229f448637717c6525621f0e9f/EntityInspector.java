@@ -4,6 +4,8 @@ import java.lang.reflect.Modifier;
 import java.util.ArrayList;
 import java.util.List;
 
+import org.orman.mapper.annotation.Index;
+
 public class EntityInspector {
 	
 	private Class<?> clazz;
@@ -20,7 +22,12 @@ public class EntityInspector {
 		
 		for(java.lang.reflect.Field f : this.clazz.getDeclaredFields()){
 			if(!Modifier.isTransient(f.getModifiers())){
-				fields.add(new Field(f.getType(), f.getName()));
+				Field newF = new Field(f.getType(), f.getName());
+				if(f.isAnnotationPresent(Index.class)){
+					Index ann = f.getAnnotation(Index.class);
+					newF.setIndex(new FieldIndexHolder(ann.name(), ann.unique()));
+				}
+				fields.add(newF);
 			}
 		}
 		return this.fields;
