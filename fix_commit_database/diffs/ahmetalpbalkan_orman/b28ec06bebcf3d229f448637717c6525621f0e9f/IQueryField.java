@@ -2,4 +2,6 @@ package org.orman.sql;
 
 public interface IQueryField extends Aliasable {
 	public String getFieldRepresentation();
+	public String getFieldName();
+	public String getAlias();
 }
\ No newline at end of file
