@@ -1,17 +1,12 @@
 package org.orman.mapper.exception;
 
+import org.orman.exception.OrmanMappingException;
+
 
 @SuppressWarnings("serial")
-public class NotDeclaredIdException extends RuntimeException {
-	private static String message = "The following class does not define a @PrimaryKey(autoIncrement=true) field: %s."  +
-			"It should define an autoincrement if it is a type of a foreign field in some other Entity.";
-	private String s1;
-	
+public class NotDeclaredIdException extends OrmanMappingException {
 	public NotDeclaredIdException(String s1){
-		this.s1 = s1;
-	}
-	
-	public String getMessage() {
-		return String.format(message, s1);
+		super( "The following class does not define a @PrimaryKey(autoIncrement=true) field: %s."  +
+			"It should define an autoincrement if it is a type of a foreign field in some other Entity.", s1);
 	}
 }
