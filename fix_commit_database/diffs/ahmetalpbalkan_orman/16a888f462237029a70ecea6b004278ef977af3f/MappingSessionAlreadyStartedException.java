@@ -1,11 +1,11 @@
 package org.orman.mapper.exception;
 
+import org.orman.exception.OrmanMappingException;
+
 
 @SuppressWarnings("serial")
-public class MappingSessionAlreadyStartedException extends RuntimeException {
-	@Override
-	public String getMessage() {
-		String message = "Mapping session has already been started. Do not use start method more than once.";
-		return message;
+public class MappingSessionAlreadyStartedException extends OrmanMappingException {
+	public MappingSessionAlreadyStartedException(){
+		super("Mapping session has already been started. Do not use start method more than once.");
 	}
 }
