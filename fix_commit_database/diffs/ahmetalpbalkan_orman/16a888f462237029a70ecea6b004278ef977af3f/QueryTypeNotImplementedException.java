@@ -1,17 +1,10 @@
 package org.orman.sql.exception;
 
+import org.orman.exception.OrmanException;
+
 @SuppressWarnings("serial")
-public class QueryTypeNotImplementedException extends RuntimeException {
-	private final String mesg = "Query not implemented in running DBMS: %s";
-	
-	String type;
-	
+public class QueryTypeNotImplementedException extends OrmanException {
 	public QueryTypeNotImplementedException(String type){
-		this.type = type;
-	}
-	
-	@Override
-	public String getMessage() {
-		return String.format(this.mesg, this.type);
+		super("Query not implemented in running DBMS: %s", type);
 	}
 }
