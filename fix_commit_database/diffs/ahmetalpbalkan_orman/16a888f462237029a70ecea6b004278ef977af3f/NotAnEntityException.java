@@ -1,18 +1,11 @@
 package org.orman.mapper.exception;
 
+import org.orman.exception.OrmanMappingException;
+
 
 @SuppressWarnings("serial")
-public class NotAnEntityException extends RuntimeException {
-	private static final String message = "The following class is not eligible to be an entity. Use @Entity annotation.: %s";
-	
-	private String s;
-	
+public class NotAnEntityException extends OrmanMappingException {
 	public NotAnEntityException(String name){
-		this.s = name;
-	}
-	
-	@Override
-	public String getMessage() {
-		return String.format(message, this.s);
+		super("The following class is not eligible to be an entity. Use @Entity annotation: %s", name);
 	}
 }
