@@ -2,8 +2,7 @@ package org.orman.sql.exception;
 
 @SuppressWarnings("serial")
 public class NoTableSpecifiedException extends QueryBuilderException {
-	@Override
-	public String getMessage() {
-		return "You should specify a table before preparing this query.";
+	public NoTableSpecifiedException(){
+		super("You should specify a table before preparing this query.");
 	}
 }
