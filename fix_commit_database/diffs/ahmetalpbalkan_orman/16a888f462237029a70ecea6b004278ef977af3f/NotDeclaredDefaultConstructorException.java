@@ -1,17 +1,11 @@
 package org.orman.mapper.exception;
 
+import org.orman.exception.OrmanMappingException;
+
 
 @SuppressWarnings("serial")
-public class NotDeclaredDefaultConstructorException extends RuntimeException {
-	private static String message = "The following class does not define a public default constructor (with no parameters): %s";
-	
-	private String s1;
-	
+public class NotDeclaredDefaultConstructorException extends OrmanMappingException {
 	public NotDeclaredDefaultConstructorException(String s1){
-		this.s1 = s1;
-	}
-	
-	public String getMessage() {
-		return String.format(message, s1);
+		super("The following class does not define a public default constructor (with no parameters): %s", s1);
 	}
 }
