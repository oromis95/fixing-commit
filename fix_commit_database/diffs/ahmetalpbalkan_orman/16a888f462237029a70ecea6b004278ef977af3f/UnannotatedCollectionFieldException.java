@@ -1,17 +1,11 @@
 package org.orman.mapper.exception;
 
+import org.orman.exception.OrmanMappingException;
+
 
 @SuppressWarnings("serial")
-public class UnannotatedCollectionFieldException extends RuntimeException {
-	private static String message = "Unannotated non-transient collection field `%s` on %s. Annotate it with @OneToMany or @ManyToMany.";
-	private String s1,s2;
-	
+public class UnannotatedCollectionFieldException extends OrmanMappingException {
 	public UnannotatedCollectionFieldException(String s1, String s2){
-		this.s1 = s1;
-		this.s2 = s2;
-	}
-	
-	public String getMessage() {
-		return String.format(message, s1, s2);
+		super("Unannotated non-transient collection field `%s` on %s. Annotate it with @OneToMany or @ManyToMany.", s1, s2);
 	}
 }
