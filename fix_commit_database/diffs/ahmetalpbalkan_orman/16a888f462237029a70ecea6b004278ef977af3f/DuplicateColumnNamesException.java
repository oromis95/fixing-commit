@@ -1,17 +1,11 @@
 package org.orman.mapper.exception;
 
+import org.orman.exception.OrmanMappingException;
+
 
 @SuppressWarnings("serial")
-public class DuplicateColumnNamesException extends RuntimeException {
-	private static String message = "Unable to map more than one fields to the same column name: %s, %s.";
-	private String s1,s2;
-	
+public class DuplicateColumnNamesException extends OrmanMappingException {
 	public DuplicateColumnNamesException(String s1, String s2){
-		this.s1 = s1;
-		this.s2 = s2;
-	}
-	
-	public String getMessage() {
-		return String.format(message, s1, s2);
+		super("Unable to map more than one fields to the same column name: %s, %s.", s1, s2);
 	}
 }
