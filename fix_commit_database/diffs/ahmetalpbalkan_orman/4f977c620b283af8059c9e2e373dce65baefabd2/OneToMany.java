@@ -17,7 +17,8 @@ import org.orman.mapper.LoadingPolicy;
  */
 @Retention(RetentionPolicy.RUNTIME)
 public @interface OneToMany {
-	Class<?> on();
+	Class<?> toType();
+	String on();
 	String targetBindingField() default "";
 	LoadingPolicy load() default LoadingPolicy.EAGER;
 }
\ No newline at end of file
