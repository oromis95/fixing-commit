@@ -3,14 +3,12 @@ package demo;
 import java.util.List;
 
 import org.orman.datasource.Database;
-import org.orman.mapper.C;
 import org.orman.mapper.MappingSession;
 import org.orman.mapper.Model;
 import org.orman.mapper.ModelQuery;
 import org.orman.mapper.SchemeCreationPolicy;
 import org.orman.mapper.annotation.Entity;
 import org.orman.mapper.annotation.Id;
-import org.orman.mapper.annotation.NotNull;
 import org.orman.mapper.annotation.OneToMany;
 import org.orman.mapper.annotation.OneToOne;
 import org.orman.sql.Query;
@@ -21,12 +19,12 @@ public class User extends Model<User> {
 	@Id public int id;
 	public String name;
 	
-	@OneToOne(targetBindingField="whose")
-	public Notebook bookOfUser;
+	@OneToMany(on = "whose", toType = Notebook.class)
+	public List<Notebook> books;
 	
 	@Override
 	public String toString() {
-		return id+"'s book is {"+bookOfUser+"}\n";
+		return id+"'s books are {"+books+"}";
 	}
 
 	public static void main(String[] args) {
@@ -35,24 +33,33 @@ public class User extends Model<User> {
 		MappingSession.registerDatabase(db);
 		MappingSession.registerEntity(User.class);
 		MappingSession.registerEntity(Notebook.class);
-		MappingSession.getConfiguration().setCreationPolicy(SchemeCreationPolicy.USE_EXISTING);
+//		MappingSession.getConfiguration().setCreationPolicy(SchemeCreationPolicy.CREATE);
 		MappingSession.start();
 		
-//		User a = new User();
-//		Notebook n = new Notebook();
-//		n.whose=a;
-//		n.insert();
-//		a.bookOfUser = n;
-//		a.insert();
-//		n.name = a.id+"s book";
-//		n.update();
+		User a = new User();
+		a.insert();
+		
+		Notebook n = new Notebook();
+		n.whose = a;
+		n.insert();
+
+		n.name = a.id+"s book";
+		n.update();
+		
+//		a.update();
+		
+		Notebook m = new Notebook();
+		m.whose = a;
+		m.insert();
+		m.name = a.id+"s second book";
+		m.update();
 
 		Query custom = ModelQuery.select().from(User.class).getQuery();
 		List<User> l = Model.fetchQuery(custom, User.class);
 		System.out.println(l);
 		
-		custom = ModelQuery.select().from(Notebook.class).getQuery();
-		List<Notebook> m = Model.fetchQuery(custom, Notebook.class);
-		System.out.println(m);
+//		custom = ModelQuery.select().from(Notebook.class).getQuery();
+//		List<Notebook> z = Model.fetchQuery(custom, Notebook.class);
+//		System.out.println(z);
 	}
 }
