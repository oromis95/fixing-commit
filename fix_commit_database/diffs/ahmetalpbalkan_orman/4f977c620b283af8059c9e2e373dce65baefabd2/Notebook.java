@@ -4,6 +4,7 @@ import org.orman.mapper.LoadingPolicy;
 import org.orman.mapper.Model;
 import org.orman.mapper.annotation.Entity;
 import org.orman.mapper.annotation.Id;
+import org.orman.mapper.annotation.ManyToOne;
 import org.orman.mapper.annotation.OneToOne;
 
 @Entity
@@ -11,7 +12,7 @@ public class Notebook extends Model<Notebook>{
 	@Id public long id;
 	public String name;
 	
-	@OneToOne(load=LoadingPolicy.LAZY, targetBindingField="bookOfUser")
+	@ManyToOne(load=LoadingPolicy.LAZY)
 	public User whose;
 	
 	@Override
