@@ -1,5 +1,6 @@
 package org.orman.mapper.exception;
 
+@SuppressWarnings("serial")
 public class UnsupportedIdFieldTypeException extends RuntimeException {
 	private static final String message = "Given type is not supported on @Id field: %s";
 	
