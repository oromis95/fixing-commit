@@ -1,8 +1,7 @@
 package org.orman.mapper.exception;
 
+@SuppressWarnings("serial")
 public class GenericOrmanException extends RuntimeException {
-	private static final String message = "Unable to create index from a non-indexed field. Use @Index annotation on the following field: %s";
-	
 	private String s;
 	
 	public GenericOrmanException(String message){
