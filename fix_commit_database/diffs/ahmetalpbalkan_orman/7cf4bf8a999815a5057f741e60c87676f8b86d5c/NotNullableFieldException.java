@@ -1,5 +1,6 @@
 package org.orman.mapper.exception;
 
+@SuppressWarnings("serial")
 public class NotNullableFieldException extends RuntimeException {
 	private static String message = "Trying to save instance with a null value on a @NotNull (not nullable) field: `%s` (%s)";
 	private String f,c;
