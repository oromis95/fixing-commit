@@ -1,5 +1,6 @@
 package org.orman.mapper.exception;
 
+@SuppressWarnings("serial")
 public class UnmappedDataTypeException extends RuntimeException {
 	private static String message = "There is no corresponding data type found for column: %s (%s).";
 	private String f,t;
