@@ -1,5 +1,6 @@
 package org.orman.mapper.exception;
 
+@SuppressWarnings("serial")
 public class MappingSessionAlreadyStartedException extends RuntimeException {
 	@Override
 	public String getMessage() {
