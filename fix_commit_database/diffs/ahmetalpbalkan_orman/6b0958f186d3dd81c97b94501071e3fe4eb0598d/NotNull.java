@@ -3,9 +3,13 @@ package org.orman.mapper.annotation;
 import java.lang.annotation.Retention;
 import java.lang.annotation.RetentionPolicy;
 
+import org.orman.mapper.exception.NotNullableFieldException;
+
 /**
- * Ensures that this column is not null
- * while saving. 
+ * Ensures that this column is not null while saving
+ * throws {@link NotNullableFieldException} before saving
+ * if this column of the instance is null on insert and
+ * update operations. 
  * 
  * @author alp
  *
