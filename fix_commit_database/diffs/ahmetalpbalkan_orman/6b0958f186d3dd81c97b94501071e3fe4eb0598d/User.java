@@ -3,13 +3,13 @@ package demo;
 import java.io.BufferedReader;
 
 import org.orman.mapper.C;
-import org.orman.mapper.F;
 import org.orman.mapper.MappingSession;
 import org.orman.mapper.Model;
 import org.orman.mapper.ModelQuery;
 import org.orman.mapper.annotation.Entity;
 import org.orman.mapper.annotation.Id;
 import org.orman.mapper.annotation.Index;
+import org.orman.mapper.annotation.NotNull;
 import org.orman.sql.QueryType;
 
 @Entity(table="user")
@@ -17,7 +17,7 @@ public class User extends Model<User> {
 	@Id public int id;
 	int age;
 	
-	@Index private String lastName;
+	@NotNull private String lastName;
 	public boolean isAdmin;
 	private transient int tmp;
 	private transient BufferedReader reader;
@@ -26,25 +26,6 @@ public class User extends Model<User> {
 		age = 5;
 	}
 	
-	public static void main(String[] args) {
-		MappingSession.registerEntity(User.class);
-		MappingSession.start();
-		
-		User u = new User();
-		u.insert();
-		u.id=5;
-		u.setLastName("zax");
-		u.update();
-		u.id=6;
-		u.update();
-		u.delete();
-		
-		ModelQuery q = ModelQuery.type(QueryType.SELECT);
-		q.from(User.class).orderBy("-User.id");
-		
-		System.out.println(q.getQuery());
-	}
-
 	public int getAge() {
 		return age;
 	}
@@ -84,4 +65,28 @@ public class User extends Model<User> {
 	public void setReader(BufferedReader reader) {
 		this.reader = reader;
 	}
+	
+	public static void main(String[] args) {
+		MappingSession.registerEntity(User.class);
+		MappingSession.start();
+		
+		User u =new User();
+		u.setLastName("zaa");
+		u.insert();
+		u.setLastName("ax");
+		u.update();
+		
+		System.out.println();
+		
+		ModelQuery q = ModelQuery.type(QueryType.SELECT);
+		q.fromAs(User.class, "zaa").orderBy("-User.id").where(
+				C.and(
+					C.eq(User.class, "lastName", 10),
+					C.between(User.class, "age", 3, "balkan")
+				)
+		);
+		
+		System.out.println(q.getQuery());
+	}
+
 }
