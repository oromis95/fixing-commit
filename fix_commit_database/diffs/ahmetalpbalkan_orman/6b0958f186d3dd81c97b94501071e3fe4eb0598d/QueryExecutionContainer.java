@@ -3,5 +3,11 @@ package org.orman.datasource;
 import org.orman.sql.Query;
 
 public interface QueryExecutionContainer {
-	public void execute(Query q); // TODO update return type. create more usable interface
+	public void executeOnly(Query q);
+	
+	public Object[][] executeForRowset(Query q);
+	
+	public Object executeForSingleValue();
+	
+	public Object getLastInsertId();
 }
