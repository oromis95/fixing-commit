@@ -12,13 +12,29 @@ import org.orman.mapper.LoadingPolicy;
  * These fields are not physically created on the database but populated at the
  * runtime.
  * 
- * @author alp
+ * CAUTION: Do not forget to use {@link ManyToOne} annotation on the target.
  * 
+ * @author alp
  */
 @Retention(RetentionPolicy.RUNTIME)
 public @interface OneToMany {
+	/**
+	 * Target type.
+	 * @return
+	 */
 	Class<?> toType();
+	
+	/**
+	 * Target column to store this instance's id.
+	 * @return
+	 */
 	String on();
+	
+	/**
+	 * Optionally, to fill target instance's related field
+	 * with this value.
+	 */
 	String targetBindingField() default "";
+	
 	LoadingPolicy load() default LoadingPolicy.EAGER;
 }
\ No newline at end of file
