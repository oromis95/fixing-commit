@@ -1,22 +1,26 @@
-package demo;
+package demo5manytoone;
 
 import org.orman.mapper.LoadingPolicy;
 import org.orman.mapper.Model;
 import org.orman.mapper.annotation.Entity;
+import org.orman.mapper.annotation.Index;
 import org.orman.mapper.annotation.ManyToOne;
 import org.orman.mapper.annotation.PrimaryKey;
+import org.orman.sql.IndexType;
 
 @Entity
-public class Student extends Model<Student>{
-	@PrimaryKey public long id;
+public class Employee extends Model<Employee>{
+	@PrimaryKey(autoIncrement=true)
+	public long id;
 	
+	@Index(type=IndexType.HASH)
 	public String name;
 	
 	@ManyToOne(load=LoadingPolicy.LAZY)
-	public ClassRoom classroom;
+	public Department dept;
 	
 	@Override
 	public String toString() {
-		return "STD id=`"+id+"` named=`"+name +"` class="+((classroom == null) ? "?  " : "@"+classroom.id);
+		return id + " " + name;
 	}
 }
