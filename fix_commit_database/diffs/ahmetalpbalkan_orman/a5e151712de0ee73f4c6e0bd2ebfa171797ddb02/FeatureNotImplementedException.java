@@ -0,0 +1,9 @@
+package org.orman.exception;
+
+public class FeatureNotImplementedException extends RuntimeException {
+
+	public FeatureNotImplementedException(String message) {
+		super(message);
+	}
+
+}
