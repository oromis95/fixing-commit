@@ -6,19 +6,17 @@ import java.lang.annotation.RetentionPolicy;
 import org.orman.mapper.LoadingPolicy;
 
 /**
- * TODO protote this entity
- * TODO make sure that this entity is used when manytoone occurs, before starting session.
  * TODO discuss: should target binding be supported?
  * 
- * Place this annotation on columns to create relationship of *:1 cardinality
- * between entities.
+ * <p>Place this annotation on columns to create relationship of *:1 cardinality
+ * between entities. It should have type <code>List<?></code></p>
  * 
- * This creates a non-unique index on this field. If you want to override its name,
- * use {@link Index} annotaation after that.
+ * <p>This creates a non-unique index on this field. If you want to override its name,
+ * use {@link Index} annotation after that.</p>
  * 
  * @author ahmet alp balkan <ahmetalpbalkan@gmail.com>
  */
 @Retention(RetentionPolicy.RUNTIME)
 public @interface ManyToOne {
-	LoadingPolicy load() default LoadingPolicy.EAGER;
+	LoadingPolicy load() default LoadingPolicy.EAGER; //eager loading is default!
 }
\ No newline at end of file
