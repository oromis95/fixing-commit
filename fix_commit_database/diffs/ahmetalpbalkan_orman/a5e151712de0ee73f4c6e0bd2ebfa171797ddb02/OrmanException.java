@@ -0,0 +1,11 @@
+package org.orman.exception;
+
+public class OrmanException extends RuntimeException {
+
+	public OrmanException(){}
+	
+	public OrmanException(String message) {
+		super(message);
+	}
+
+}
