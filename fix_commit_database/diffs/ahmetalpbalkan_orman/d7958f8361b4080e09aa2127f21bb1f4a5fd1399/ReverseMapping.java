@@ -23,12 +23,13 @@ import org.orman.util.logging.Log;
  * @author ahmet alp balkan <ahmetalpbalkan@gmail.com>
  */
 public class ReverseMapping {
-	private static final DateFormat dateTimeParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
-	private static final DateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
-	
+	private static final DateFormat dateTimeParser = new SimpleDateFormat(
+			"yyyy-MM-dd HH:mm:ss");
+	private static final DateFormat dateParser = new SimpleDateFormat(
+			"yyyy-MM-dd HH:mm:ss");
+
 	@SuppressWarnings("unchecked")
-	public static <E> E map(ResultRow row, Class<E> type,
-			Entity e) {
+	public static <E> E map(ResultRow row, Class<E> type, Entity e) {
 
 		E instance = null;
 
@@ -41,48 +42,55 @@ public class ReverseMapping {
 			Log.error(e1.getMessage());
 		}
 
-		if (instance != null){
+		if (instance != null) {
 			for (Field f : e.getFields()) {
-					if (!f.isList()){
-						/* Use direct value from database */
-						// do not fill field if it is already filled somehow.
-						
-						// read field value
-						Object fieldValue = row.getColumn(f.getGeneratedName());
-		
-						// TODO do needed conversions. (McCabe cyclomatic complexity)
-						fieldValue = smartCasting(fieldValue, f.getClazz());
-		
-						// according to lazy loading policy.
-						fieldValue = makeCardinalityBinding(f, instance, fieldValue);
-						
-						// Reverse Map enum fields.
-						if (f.getClazz().isEnum()){
-							Object[] enumConstant =   f.getClazz().getEnumConstants();
-							try {
-								fieldValue = enumConstant[(Integer) fieldValue];	
-							} catch (Exception ex) {
+				if (!f.isList()) {
+					/* Use direct value from database */
+					// do not fill field if it is already filled somehow.
+
+					// read field value
+					Object fieldValue = row.getColumn(f.getGeneratedName());
+
+					// TODO do needed conversions. (McCabe cyclomatic
+					// complexity)
+					fieldValue = smartCasting(fieldValue, f.getClazz());
+
+					// according to lazy loading policy.
+					fieldValue = makeCardinalityBinding(f, instance, fieldValue);
+
+					// Reverse Map enum fields.
+					if (f.getClazz().isEnum()) {
+						Object[] enumConstant = f.getClazz().getEnumConstants();
+						try {
+							if (fieldValue != null)
+								fieldValue = enumConstant[new Integer(
+										fieldValue.toString())];
+						} catch (Exception ex) {
 							Log.error(
-									"Unable to reverse map enum type %s with value %s",
+									"Unable to reverse map enum type %s with value %s (%s)",
 									f.getClazz().getName(),
 									fieldValue == null ? null : fieldValue);
-								fieldValue = null;
-							}
-							
+							fieldValue = null;
 						}
-		
-						// set field
-						if (fieldValue != null) ((Model<?>) instance).setEntityField(f, e, fieldValue);
-					} else {
-						Object fieldValue = makeCardinalityBinding(f, instance,
+
+					}
+
+					// set field
+					if (fieldValue != null)
+						((Model<?>) instance).setEntityField(f, e, fieldValue);
+				} else {
+					Object fieldValue = makeCardinalityBinding(f, instance,
 							((Model<?>) instance)
 									.getEntityField(((Model<?>) instance)
-											.getEntity().getAutoIncrementField()));
+											.getEntity()
+											.getAutoIncrementField()));
 
-						if (fieldValue != null) ((Model<?>) instance).setEntityField(f, e, fieldValue);
-					}
+					if (fieldValue != null)
+						((Model<?>) instance).setEntityField(f, e, fieldValue);
+				}
 			}
-			((Model<?>) instance).makePersistent(); // because record is not dirty.
+			((Model<?>) instance).makePersistent(); // because record is not
+													// dirty.
 		}
 		return instance;
 	}
@@ -92,84 +100,96 @@ public class ReverseMapping {
 	 * @*To* annotation exists on the given field. Returns the same object if
 	 * the field does not have such relationship annotations.
 	 * 
-	 * Threats the given value as key for finding related entities. 
+	 * Threats the given value as key for finding related entities.
 	 * 
 	 * @param <E>
 	 * @param f
-	 * @param instance to make bidirectional mapping.
+	 * @param instance
+	 *            to make bidirectional mapping.
 	 * @param key
 	 * @return the original key if key is already null.
 	 */
 	@SuppressWarnings({ "unchecked", "rawtypes" })
-	private static <E> Object makeCardinalityBinding(Field f,
-			E instance, Object key) {
-		if (key == null) return key;
-		
+	private static <E> Object makeCardinalityBinding(Field f, E instance,
+			Object key) {
+		if (key == null)
+			return key;
+
 		Class<?> intendedType = f.getClazz();
-		
-		if (!MappingSession.entityExists(intendedType)){
+
+		if (!MappingSession.entityExists(intendedType)) {
 			/* our entity is not mappable! */
 			return key; // return the same
 		} else {
 			if (((Model<?>) instance).getEntityField(f) != null)
 				return null; // field is already filled somehow.
-			
+
 			Entity intendedEntity = MappingSession.getEntity(intendedType);
-			
-			if (!f.isList()){
+
+			if (!f.isList()) {
 				/* we have a 1:1 mapping without checking the annotation */
 				boolean doLoading = false;
-				
-				doLoading |= f.isAnnotationPresent(OneToOne.class) && f.getAnnotation(
-						OneToOne.class).load().equals(LoadingPolicy.EAGER);
-				
-				doLoading |= f.isAnnotationPresent(ManyToOne.class) && f.getAnnotation(
-						ManyToOne.class).load().equals(LoadingPolicy.EAGER);
-				
+
+				doLoading |= f.isAnnotationPresent(OneToOne.class)
+						&& f.getAnnotation(OneToOne.class).load()
+								.equals(LoadingPolicy.EAGER);
+
+				doLoading |= f.isAnnotationPresent(ManyToOne.class)
+						&& f.getAnnotation(ManyToOne.class).load()
+								.equals(LoadingPolicy.EAGER);
+
 				// if no annotations present on field do loading
-				doLoading |= !f.isAnnotationPresent(OneToOne.class) && !f.isAnnotationPresent(ManyToOne.class);
-				
+				doLoading |= !f.isAnnotationPresent(OneToOne.class)
+						&& !f.isAnnotationPresent(ManyToOne.class);
+
 				if (doLoading) {
-					Query c = ModelQuery.select().from(intendedEntity).where(
-							C.eq(intendedType, intendedEntity.getAutoIncrementField()
-									.getOriginalName(), key)).getQuery();
-					
+					Query c = ModelQuery
+							.select()
+							.from(intendedEntity)
+							.where(C.eq(intendedType, intendedEntity
+									.getAutoIncrementField().getOriginalName(),
+									key)).getQuery();
+
 					E result = (E) Model.fetchSingle(c, intendedType);
-	
+
 					// make reverse binding on the target (result) if requested
-					if (f.isAnnotationPresent(OneToOne.class)){
+					if (f.isAnnotationPresent(OneToOne.class)) {
 						OneToOne ann = f.getAnnotation(OneToOne.class);
-						if (result != null && !"".equals(ann.targetBindingField())){
-							Field on = F.f(f.getClazz(), ann.targetBindingField());
-							((Model<E>) result).setEntityField(on, intendedEntity, instance);
+						if (result != null
+								&& !"".equals(ann.targetBindingField())) {
+							Field on = F.f(f.getClazz(),
+									ann.targetBindingField());
+							((Model<E>) result).setEntityField(on,
+									intendedEntity, instance);
 						}
 					}
 					return result;
 				}
-				
+
 				return null;
 			} else {
 				/* we have a 1:* or *:* mapping */
 				boolean doLoading = false;
-				
+
 				OneToMany ann = f.getAnnotation(OneToMany.class);
-				doLoading |= (ann != null) && ann.load().equals(LoadingPolicy.EAGER);
-				
-				if (doLoading){
+				doLoading |= (ann != null)
+						&& ann.load().equals(LoadingPolicy.EAGER);
+
+				if (doLoading) {
 					OneToMany config = f.getAnnotation(OneToMany.class);
 					Query q = ModelQuery.select().from(intendedEntity)
-					.where(
-							C.eq(intendedType, config.onField(), key)
-					)
-					.getQuery();
-					
+							.where(C.eq(intendedType, config.onField(), key))
+							.getQuery();
+
 					List<?> resultList = Model.fetchQuery(q, intendedType);
-					return new EntityList(instance.getClass(), intendedType, instance, resultList);
+					return new EntityList(instance.getClass(), intendedType,
+							instance, resultList);
 				} else {
 					// set lazy loading EntityList
-					return new EntityList(instance.getClass(), intendedType, instance, true);
+					return new EntityList(instance.getClass(), intendedType,
+							instance, true);
 				}
-				
+
 			}
 		}
 	}
@@ -241,7 +261,7 @@ public class ReverseMapping {
 			else
 				return ((Integer) smartCasting(value, Integer.TYPE)) > 0;
 		}
-		
+
 		if (Date.class.equals(desired)) {
 			// destination: java.util.Date.
 			if (value.getClass().equals(Date.class))
@@ -250,20 +270,20 @@ public class ReverseMapping {
 				try {
 					String dateStr = value.toString();
 					Date d = null;
-					
-					if (dateStr.length() > 10){
+
+					if (dateStr.length() > 10) {
 						d = dateTimeParser.parse(dateStr);
 					} else {
 						d = dateParser.parse(dateStr);
 					}
 					return d;
 				} catch (ParseException e) {
-					Log.error("The following date could not be parsed: " + value.toString());
+					Log.error("The following date could not be parsed: "
+							+ value.toString());
 					return null;
 				}
 			}
 		}
-	
 
 		return value;
 	}
