@@ -1,5 +1,6 @@
 package demo;
 
+import org.orman.mapper.LoadingPolicy;
 import org.orman.mapper.Model;
 import org.orman.mapper.annotation.Entity;
 import org.orman.mapper.annotation.Id;
@@ -10,7 +11,7 @@ public class Notebook extends Model<Notebook>{
 	@Id public long id;
 	public String name;
 	
-	@OneToOne
+	@OneToOne(load=LoadingPolicy.LAZY)
 	public User whose;
 	
 	@Override
