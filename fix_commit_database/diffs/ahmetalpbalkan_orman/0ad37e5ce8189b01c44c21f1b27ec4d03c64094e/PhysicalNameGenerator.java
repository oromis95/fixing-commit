@@ -137,4 +137,9 @@ public class PhysicalNameGenerator {
 		
 		return retVal;
 	}
+	
+	public static String capitalize(String s){
+		if (s.length() == 0) return s;
+        return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
+	}
 }
\ No newline at end of file
