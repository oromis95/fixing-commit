@@ -1,7 +1,9 @@
 package us.circuitsoft.slack;
 
 import java.io.BufferedOutputStream;
+import java.io.IOException;
 import java.net.HttpURLConnection;
+import java.net.MalformedURLException;
 import java.net.URL;
 import java.util.logging.Level;
 import org.bukkit.event.EventHandler;
@@ -15,11 +17,14 @@ import org.json.simple.JSONObject;
 
 public class Slack extends JavaPlugin implements Listener {
 
+    private boolean setWebhook;
+
     @Override
     public void onEnable() {
         getLogger().info("Slack has been enabled.");
         getServer().getPluginManager().registerEvents(this, this);
         this.saveDefaultConfig();
+        setWebhook = getConfig().getString("webhook").equals("https://hooks.slack.com/services/");
         if (getConfig().getString("webhook").equals("https://hooks.slack.com/services/")) {
             getLogger().severe("You have not set your webhook URL in the config!");
         }
@@ -32,56 +37,56 @@ public class Slack extends JavaPlugin implements Listener {
 
     @EventHandler
     public void onChat(AsyncPlayerChatEvent event) {
-        String m = event.getMessage();
-        post(m, event.getPlayer().getName());
+        payload('"' + event.getMessage() + '"', event.getPlayer().getName());
     }
-    
+
     @EventHandler
     public void onLogin(PlayerLoginEvent event) {
-        String m = "logged in";
-        post(m, event.getPlayer().getName());
+        payload("logged in", event.getPlayer().getName());
     }
 
     @EventHandler
     public void onQuit(PlayerQuitEvent event) {
-        String m = "logged out";
-        post(m, event.getPlayer().getName());
+        payload("logged out", event.getPlayer().getName());
     }
 
     @EventHandler
     public void onCommand(PlayerCommandPreprocessEvent event) {
-        String m = event.getMessage();
-        post(m, event.getPlayer().getName());
+        payload(event.getMessage(), event.getPlayer().getName());
     }
 
-    public void post(String m, String p) {
-        try {
-            String surl = getConfig().getString("webhook");
-            URL url = new URL(null);
-            if (surl.equals("https://hooks.slack.com/services/")) {
-                getLogger().severe("You have not set your webhook URL in the config!");
-            } else {
-                url = new URL(surl);
-                JSONObject j = new JSONObject();
-                j.put("text", m);
-                j.put("username", p);
-                j.put("icon_url", "https://minotar.net/avatar/" + p + "/100.png");
-                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
-                conn.setRequestMethod("POST");
-                conn.setDoOutput(true);
-                BufferedOutputStream bos = new BufferedOutputStream(conn.getOutputStream());
-                String body = "payload=" + j.toJSONString();
-                bos.write(body.getBytes("utf8"));
-                bos.flush();
-                bos.close();
-                int icode = conn.getResponseCode();
-                String opcode = Integer.toString(icode);
-                String code = conn.getResponseMessage();
-                getLogger().info(opcode + code);
-                conn.disconnect();
-            }
-        } catch (Exception e) {
-            getLogger().log(Level.SEVERE, e.getMessage());
+    public void payload(String m, String p) {
+        JSONObject j = new JSONObject();
+        j.put("text", m);
+        j.put("username", p);
+        j.put("icon_url", "https://minotar.net/avatar/" + p + "/100.png");
+        String b = "payload=" + j.toJSONString();
+        post(b);
+    }
+
+    public void post(String b) {
+        if (setWebhook) {
+            getLogger().severe("You have not set your webhook URL in the config!");
+        } else {
+            try {
+                URL u = new URL(getConfig().getString("webhook"));
+                HttpURLConnection C = (HttpURLConnection)u.openConnection();
+                C.setRequestMethod("POST");
+                C.setDoOutput(true);
+                try (BufferedOutputStream B = new BufferedOutputStream(C.getOutputStream())) {
+                    B.write(b.getBytes("utf8"));
+                    B.flush();
+                }
+                int i = C.getResponseCode();
+                String o = Integer.toString(i);
+                String c = C.getResponseMessage();
+                getLogger().log(Level.INFO, "{0} {1}", new Object[]{o, c});
+                C.disconnect();
+                } catch (MalformedURLException e) {
+                    getLogger().log(Level.SEVERE, "URL is not valid: ", e);
+                } catch (IOException e) {
+                    getLogger().log(Level.SEVERE, "IO exception: ", e);
+                }
         }
     }
 }
