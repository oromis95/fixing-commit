@@ -10,8 +10,9 @@ import us.circuitsoft.slack.api.BungeePoster;
 
 public class SlackBungeeCommand extends Command {
 
-    public static final BaseComponent[] helpMsg = new ComponentBuilder("/slack send <username> <image URL or null for username's skin> <message> - send a custom message to slack \n/slack reload - reload Slack's config").color(ChatColor.GOLD).create();
+    public static final BaseComponent[] helpMsg = new ComponentBuilder("/slack send <username> <image URL or null for username's skin> <message> - send a custom message to slack\n/slack reload - reload Slack's config").color(ChatColor.GOLD).create();
     private static final BaseComponent[] sendHelpMsg = new ComponentBuilder("/slack send <username> <image URL or null for username's skin> <message>").color(ChatColor.GOLD).create();
+    private static final BaseComponent[] reloadHelpMsg = new ComponentBuilder("/slack reload - reload Slack's config").color(ChatColor.GOLD).create();
     private static final BaseComponent[] reloadMsg = new ComponentBuilder("Slack has been reloaded.").color(ChatColor.GREEN).create();
     private static final BaseComponent[] noPermMsg = new ComponentBuilder("You are not allowed to execute this command!").color(ChatColor.DARK_RED).create();
 
@@ -24,12 +25,12 @@ public class SlackBungeeCommand extends Command {
 
     @Override
     public void execute(CommandSender sender, String[] args) {
-        if (!sender.hasPermission("slack.reload") && sender.hasPermission("slack.send")) {
+        if (!sender.hasPermission("slack.command")) {
             sender.sendMessage(noPermMsg);
         } else if (args.length == 0) {
             sender.sendMessage(helpMsg);
         } else if (args[0].equalsIgnoreCase("reload")) {
-            if (sender.hasPermission("slack.reload")) {
+            if (sender.hasPermission("slack.command")) {
                 plugin.reloadConfig();
                 sender.sendMessage(reloadMsg);
                 if (!sender.equals(ProxyServer.getInstance().getConsole())) {
@@ -39,7 +40,7 @@ public class SlackBungeeCommand extends Command {
                 sender.sendMessage(noPermMsg);
             }
         } else if (args[0].equals("send")) {
-            if (sender.hasPermission("slack.send")) {
+            if (sender.hasPermission("slack.command")) {
                 if (args.length <= 3) {
                     sender.sendMessage(sendHelpMsg);
                 } else if (args.length >= 4) {
