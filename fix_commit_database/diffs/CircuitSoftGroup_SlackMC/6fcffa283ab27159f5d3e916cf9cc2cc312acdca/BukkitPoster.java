@@ -1,34 +1,37 @@
-package us.circuitsoft.slack;
+package us.circuitsoft.slack.api;
 
 import java.io.BufferedOutputStream;
-import java.io.IOException;
 import java.net.HttpURLConnection;
-import java.net.MalformedURLException;
 import java.net.URL;
-import java.util.logging.Level;
-import org.bukkit.plugin.java.JavaPlugin;
 import org.bukkit.scheduler.BukkitRunnable;
 import org.json.simple.JSONObject;
+import static us.circuitsoft.slack.bukkit.SlackBukkit.getWebhook;
 
-public class Poster extends BukkitRunnable {
+/**
+ * Posts a message to Slack when using Bukkit.
+ */
+public class BukkitPoster extends BukkitRunnable {
 
-    private final JavaPlugin plugin;
     private final String p;
     private final String m;
     private final String w;
     private final String i;
 
-    public Poster (JavaPlugin plugin, String m, String p, String w, String i) {
-        this.plugin = plugin;
+    /**
+     * Prepares the message to send to Slack.
+     * @param m The message to send to Slack.
+     * @param p The username of the message to send to Slack.
+     * @param i The image URL of the user that sends the message to Slack. Make this null if the username is a Minecraft player name.
+     */
+    public BukkitPoster (String m, String p, String i) {
         this.p = p;
         this.m = m;
-        this.w = w;
         this.i = i;
+        this.w = getWebhook();
     }
             
     @Override
     public void run() {
-        int res;
         JSONObject j = new JSONObject();
         j.put("text", p + ": " + m);
         j.put("username", p);
@@ -48,17 +51,6 @@ public class Poster extends BukkitRunnable {
                     B.flush();
                 }
                 C.disconnect();
-                res = C.getResponseCode();
-                String o = Integer.toString(res);
-                String c = C.getResponseMessage();
-                if (plugin.getConfig().getBoolean("debug")) {
-                    plugin.getLogger().log(Level.INFO, "{0} {1}", new Object[]{o, c});
-                }
-                C.disconnect();
-                } catch (MalformedURLException e) {
-                    plugin.getLogger().log(Level.SEVERE, "URL is not valid: ", e);
-                } catch (IOException e) {
-                    plugin.getLogger().log(Level.SEVERE, "IO exception: ", e);
-                }
+                } catch (Exception e) {}
     }
 }
