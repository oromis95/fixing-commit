@@ -6,51 +6,52 @@ import java.net.URL;
 
 import com.google.gson.JsonObject;
 
-import static us.circuitsoft.slack.bungee.SlackBungee.getWebhook;
+import static us.circuitsoft.slack.bungee.SlackBungee.getWebhookUrl;
 
 /**
  * Posts a message to Slack when using Bungee.
  */
 public class BungeePoster implements Runnable {
 
-    private final String p;
-    private final String m;
-    private final String w = getWebhook();
-    private final String i;
+    private final String name;
+    private final String message;
+    private final String webhookUrl = getWebhookUrl();
+    private final String icon;
 
     /**
      * Prepares the message to send to Slack.
-     * @param m The message to send to Slack.
-     * @param p The username of the message to send to Slack.
-     * @param i The image URL of the user that sends the message to Slack. Make this null if the username is a Minecraft player name.
+     *
+     * @param message The message to send to Slack.
+     * @param name    The username of the message to send to Slack.
+     * @param icon    The image URL of the user that sends the message to Slack. Make this null if the username is a Minecraft player name.
      */
-    public BungeePoster (String m, String p, String i) {
-        this.p = p;
-        this.m = m;
-        this.i = i;
+    public BungeePoster(String message, String name, String icon) {
+        this.name = name;
+        this.message = message;
+        this.icon = icon;
     }
-            
+
     @Override
     public void run() {
-        JsonObject j = new JsonObject();
-        j.addProperty("text", p + ": " + m);
-        j.addProperty("username", p);
-        if (i == null) {
-            j.addProperty("icon_url", "https://cravatar.eu/helmhead/" + p + "/100.png");   
+        JsonObject json = new JsonObject();
+        json.addProperty("text", name + ": " + message);
+        json.addProperty("username", name);
+        if (icon == null) {
+            json.addProperty("icon_url", "https://cravatar.eu/helmhead/" + name + "/100.png");
         } else {
-            j.addProperty("icon_url", i);
+            json.addProperty("icon_url", icon);
+        }
+        try {
+            HttpURLConnection webhookConnection = (HttpURLConnection) new URL(webhookUrl).openConnection();
+            webhookConnection.setRequestMethod("POST");
+            webhookConnection.setDoOutput(true);
+            try (BufferedOutputStream bufOut = new BufferedOutputStream(webhookConnection.getOutputStream())) {
+                String jsonStr = "payload=" + json.toString();
+                bufOut.write(jsonStr.getBytes("utf8"));
+                bufOut.flush();
+            }
+            webhookConnection.disconnect();
+        } catch (Exception ignored) {
         }
-        String b = "payload=" + j.toString();
-            try {
-                URL u = new URL(w);
-                HttpURLConnection C = (HttpURLConnection)u.openConnection();
-                C.setRequestMethod("POST");
-                C.setDoOutput(true);
-                try (BufferedOutputStream B = new BufferedOutputStream(C.getOutputStream())) {
-                    B.write(b.getBytes("utf8"));
-                    B.flush();
-                }
-                C.disconnect();
-                } catch (Exception e) {}
     }
 }
