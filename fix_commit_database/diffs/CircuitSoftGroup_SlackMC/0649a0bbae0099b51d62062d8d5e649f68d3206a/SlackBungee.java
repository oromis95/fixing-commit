@@ -4,8 +4,8 @@ import java.io.File;
 import java.io.IOException;
 import java.nio.file.Files;
 import java.util.List;
-import java.util.concurrent.Executors;
 import java.util.logging.Level;
+
 import net.md_5.bungee.api.connection.ProxiedPlayer;
 import net.md_5.bungee.api.event.ChatEvent;
 import net.md_5.bungee.api.event.ServerConnectedEvent;
@@ -20,74 +20,81 @@ import net.md_5.bungee.event.EventPriority;
 
 public class SlackBungee extends Plugin implements Listener {
 
-    private boolean n;
-    private static String w;
-    private List<String> bl;
-    Configuration con;
+    private static String webhookUrl;
+    private List<String> blacklist;
+    private Configuration config;
 
     @Override
     public void onEnable() {
-        con = null;
         getLogger().info("Slack has been enabled!");
         getProxy().getPluginManager().registerListener(this, this);
-        getProxy().getPluginManager().registerCommand(this, new SlackBungeeCommand());
+        getProxy().getPluginManager().registerCommand(this, new SlackBungeeCommand(this));
         try {
-            con = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(getDataFolder(), "config.yml"));
+            config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(getDataFolder(), "config.yml"));
         } catch (IOException ex) {
             getLogger().log(Level.SEVERE, "config.yml does not exist: ", ex);
         }
         updateConfig(this.getDescription().getVersion());
-        w = con.getString("webhook");
-        bl = con.getStringList("blacklist");
-        n = w.equals("https://hooks.slack.com/services/");
-        if (n || con.getString("webhook") == null) {
+        webhookUrl = config.getString("webhook");
+        blacklist = config.getStringList("blacklist");
+        if (webhookUrl == null || webhookUrl.trim().isEmpty() || webhookUrl.equals("https://hooks.slack.com/services/")) {
             getLogger().severe("You have not set your webhook URL in the config!");
         }
     }
 
+    public void reloadConfig() {
+        try {
+            config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(getDataFolder(), "config.yml"));
+        } catch (IOException ex) {
+            getLogger().log(Level.SEVERE, "config.yml does not exist: ", ex);
+        }
+        webhookUrl = config.getString("webhook");
+        blacklist = config.getStringList("blacklist");
+    }
+
     @EventHandler(priority = EventPriority.HIGHEST)
     public void onChat(ChatEvent event) {
         ProxiedPlayer p = (ProxiedPlayer) event.getSender();
         if (event.isCommand()) {
-            if (permCheck("slack.hide.command", p) && blacklist(event.getMessage())) {
+            if (!hasPermission(p, "slack.hide.command") && !isOnBlacklist(event.getMessage())) {
                 send('"' + event.getMessage() + '"', p.getName() + " (" + p.getServer().getInfo().getName() + ")");
             }
-        } else if (permCheck("slack.hide.chat", p)) {
+        } else if (!hasPermission(p, "slack.hide.chat")) {
             send('"' + event.getMessage() + '"', p.getName() + " (" + p.getServer().getInfo().getName() + ")");
         }
     }
 
     @EventHandler(priority = EventPriority.HIGHEST)
     public void onJoin(ServerConnectedEvent event) {
-        if (permCheck("slack.hide.login", event.getPlayer())) {
+        if (!hasPermission(event.getPlayer(), "slack.hide.login")) {
             send("logged in", event.getPlayer().getName() + " (" + event.getServer().getInfo().getName() + ")");
         }
     }
 
     @EventHandler(priority = EventPriority.HIGHEST)
     public void onQuit(ServerDisconnectEvent event) {
-        if (permCheck("slack.hide.logout", event.getPlayer())) {
+        if (!hasPermission(event.getPlayer(), "slack.hide.logout")) {
             send("logged out", event.getPlayer().getName() + " (" + event.getTarget().getName() + ")");
         }
     }
 
-    public void send(String m, String p) {
-        Executors.newSingleThreadExecutor().submit(new SlackBungeePoster(this, con, m, p, null));
+    public void send(String message, String name) {
+        send(message, name, null);
     }
 
-    public void send(String m, String p, String i) {
-        Executors.newSingleThreadExecutor().submit(new SlackBungeePoster(this, con, m, p, i));
+    public void send(String message, String name, String iconUrl) {
+        getProxy().getScheduler().runAsync(this, new SlackBungeePoster(this, config, message, name, iconUrl));
     }
 
-    private boolean blacklist(String m) {
-        if (con.getBoolean("use-blacklist")) {
-            return !bl.contains(m);
+    private boolean isOnBlacklist(String name) {
+        if (config.getBoolean("use-blacklist")) {
+            return blacklist.contains(name);
         } else {
-            return true;
+            return false;
         }
     }
 
-    private void updateConfig(String v) {
+    private void updateConfig(String version) {
         if (!getDataFolder().exists()) {
             getDataFolder().mkdir();
         }
@@ -99,21 +106,32 @@ public class SlackBungee extends Plugin implements Listener {
                 getLogger().log(Level.SEVERE, "Default config not saved: ", ex);
             }
         }
-        if (con.getString("v") == null ? v != null : !con.getString("v").equals(v)) {
-            con.set("version", v);
+        String configV = config.getString("v");
+        if (configV == null) {
+            if (version != null) {
+                config.set("version", version);
+            }
+        } else {
+            if (!configV.equals(version)) {
+                config.set("version", version);
+            }
         }
         try {
-            ConfigurationProvider.getProvider(YamlConfiguration.class).save(con, new File(getDataFolder(), "config.yml"));
+            ConfigurationProvider.getProvider(YamlConfiguration.class).save(config, file);
         } catch (IOException ex) {
             getLogger().log(Level.SEVERE, "Failed to save config: ", ex);
         }
     }
 
-    private boolean permCheck(String c, ProxiedPlayer p) {
-        return !p.hasPermission(c);
+    private boolean hasPermission(ProxiedPlayer player, String permission) {
+        if (config.getBoolean("use-perms")) {
+            return player.hasPermission(permission);
+        } else {
+            return false;
+        }
     }
 
-    public static String getWebhook() {
-        return w;
+    public static String getWebhookUrl() {
+        return webhookUrl;
     }
 }
