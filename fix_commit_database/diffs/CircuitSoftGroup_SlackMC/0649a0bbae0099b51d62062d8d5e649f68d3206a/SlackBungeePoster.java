@@ -1,15 +1,18 @@
 package us.circuitsoft.slack.bungee;
 
-import com.google.gson.JsonObject;
 import java.io.BufferedOutputStream;
 import java.io.IOException;
 import java.net.HttpURLConnection;
 import java.net.MalformedURLException;
 import java.net.URL;
+import java.nio.charset.StandardCharsets;
 import java.util.logging.Level;
+
+import com.google.gson.JsonObject;
 import net.md_5.bungee.api.plugin.Plugin;
 import net.md_5.bungee.config.Configuration;
-import static us.circuitsoft.slack.bungee.SlackBungee.getWebhook;
+
+import static us.circuitsoft.slack.bungee.SlackBungee.getWebhookUrl;
 
 /**
  * Poster for Slack plugin's internal use. Do not use this.
@@ -17,47 +20,45 @@ import static us.circuitsoft.slack.bungee.SlackBungee.getWebhook;
 public class SlackBungeePoster implements Runnable {
 
     private final Plugin plugin;
-    private final Configuration con;
-    private final String p;
-    private final String m;
-    private final String w = getWebhook();
-    private final String i;
+    private final Configuration config;
+    private final String name;
+    private final String message;
+    private final String webhookUrl = getWebhookUrl();
+    private final String iconUrl;
 
-    public SlackBungeePoster(Plugin plugin, Configuration con, String m, String p, String i) {
+    public SlackBungeePoster(Plugin plugin, Configuration config, String message, String name, String iconUrl) {
         this.plugin = plugin;
-        this.con = con;
-        this.p = p;
-        this.m = m;
-        this.i = i;
+        this.config = config;
+        this.name = name;
+        this.message = message;
+        this.iconUrl = iconUrl;
     }
 
     @Override
     public void run() {
-        int res;
-        JsonObject j = new JsonObject();
-        j.addProperty("text", p + ": " + m);
-        j.addProperty("username", p);
-        if (i == null) {
-            j.addProperty("icon_url", "https://cravatar.eu/helmhead/" + p + "/100.png");
+        JsonObject json = new JsonObject();
+        json.addProperty("text", name + ": " + message);
+        json.addProperty("username", name);
+        if (iconUrl == null) {
+            json.addProperty("icon_url", "https://cravatar.eu/helmhead/" + name + "/100.png");
         } else {
-            j.addProperty("icon_url", i);
+            json.addProperty("icon_url", iconUrl);
         }
-        String b = "payload=" + j.toString();
         try {
-            URL u = new URL(w);
-            HttpURLConnection C = (HttpURLConnection) u.openConnection();
-            C.setRequestMethod("POST");
-            C.setDoOutput(true);
-            try (BufferedOutputStream B = new BufferedOutputStream(C.getOutputStream())) {
-                B.write(b.getBytes("utf8"));
-                B.flush();
+            HttpURLConnection webhookConnection = (HttpURLConnection) new URL(webhookUrl).openConnection();
+            webhookConnection.setRequestMethod("POST");
+            webhookConnection.setDoOutput(true);
+            try (BufferedOutputStream bufOut = new BufferedOutputStream(webhookConnection.getOutputStream())) {
+                String jsonStr = "payload=" + json.toString();
+                bufOut.write(jsonStr.getBytes(StandardCharsets.UTF_8));
+                bufOut.flush();
             }
-            C.disconnect();
-            res = C.getResponseCode();
-            String o = Integer.toString(res);
-            String c = C.getResponseMessage();
-            if (con.getBoolean("debug")) {
-                plugin.getLogger().log(Level.INFO, "{0} {1}", new Object[]{o, c});
+            webhookConnection.disconnect();
+            if (config.getBoolean("debug")) {
+                plugin.getLogger().log(Level.INFO, "{0} {1}", new Object[]{
+                        webhookConnection.getResponseCode(),
+                        webhookConnection.getResponseMessage()
+                });
             }
         } catch (MalformedURLException e) {
             plugin.getLogger().log(Level.SEVERE, "URL is not valid: ", e);
