@@ -1,31 +1,66 @@
 package us.circuitsoft.slack.bungee;
 
 import java.util.concurrent.Executors;
+
 import net.md_5.bungee.api.ChatColor;
 import net.md_5.bungee.api.CommandSender;
+import net.md_5.bungee.api.ProxyServer;
+import net.md_5.bungee.api.chat.BaseComponent;
 import net.md_5.bungee.api.chat.ComponentBuilder;
 import net.md_5.bungee.api.plugin.Command;
 import us.circuitsoft.slack.api.BungeePoster;
 
 public class SlackBungeeCommand extends Command {
 
-    public SlackBungeeCommand() {
+    public static final BaseComponent[] helpMsg = new ComponentBuilder("/slack send <username> <image URL or null for username's skin> <message> - send a custom message to slack \n/slack reload - reload Slack's config").color(ChatColor.GOLD).create();
+    private static final BaseComponent[] sendHelpMsg = new ComponentBuilder("/slack send <username> <image URL or null for username's skin> <message>").color(ChatColor.GOLD).create();
+    private static final BaseComponent[] reloadMsg = new ComponentBuilder("Slack has been reloaded.").color(ChatColor.GREEN).create();
+    private static final BaseComponent[] noPermMsg = new ComponentBuilder("You are not allowed to execute this command!").color(ChatColor.DARK_RED).create();
+
+    private final SlackBungee plugin;
+
+    public SlackBungeeCommand(SlackBungee plugin) {
         super("slack");
+        this.plugin = plugin;
     }
 
     @Override
-    public void execute(CommandSender commandSender, String[] strings) {
-        if (strings.length == 0) {
-            commandSender.sendMessage(new ComponentBuilder("/slack send <username> <image URL> <message> - send a custom message to slack \n/slack reload - reload Slack's config").color(ChatColor.GOLD).create());
-        }
-        if (strings[0].equals("send") && strings.length <= 3) {
-            commandSender.sendMessage(new ComponentBuilder("/slack send <username> <image URL> <message>").color(ChatColor.GOLD).create());
-        } else if (strings[0].equals("send") && strings.length >= 4) {
-            String m = "";
-            for (int i = 3; i < strings.length; i++) {
-                m = m + strings[i] + " ";
+    public void execute(CommandSender sender, String[] args) {
+        if (!sender.hasPermission("slack.reload") && sender.hasPermission("slack.send")) {
+            sender.sendMessage(noPermMsg);
+        } else if (args.length == 0) {
+            sender.sendMessage(helpMsg);
+        } else if (args[0].equalsIgnoreCase("reload")) {
+            if (sender.hasPermission("slack.reload")) {
+                plugin.reloadConfig();
+                sender.sendMessage(reloadMsg);
+                if (!sender.equals(ProxyServer.getInstance().getConsole())) {
+                    ProxyServer.getInstance().getConsole().sendMessage(new ComponentBuilder("Slack has been reloaded by " + sender.getName() + '.').color(ChatColor.GREEN).create());
+                }
+            } else {
+                sender.sendMessage(noPermMsg);
+            }
+        } else if (args[0].equals("send")) {
+            if (sender.hasPermission("slack.send")) {
+                if (args.length <= 3) {
+                    sender.sendMessage(sendHelpMsg);
+                } else if (args.length >= 4) {
+                    StringBuilder sb = new StringBuilder();
+                    boolean first = true;
+                    for (int i = 3; i < args.length; i++) {
+                        if (first) {
+                            sb.append(" ");
+                            first = false;
+                        }
+                        sb.append(args[i]);
+                    }
+                    plugin.getProxy().getScheduler().runAsync(plugin, new BungeePoster(sb.toString(), args[1], args[2].equalsIgnoreCase("null") ? null : args[2]));
+                }
+            } else {
+                sender.sendMessage(noPermMsg);
             }
-            Executors.newSingleThreadExecutor().submit(new BungeePoster(m, strings[1], strings[2]));
+        } else {
+            sender.sendMessage(helpMsg);
         }
     }
 }
