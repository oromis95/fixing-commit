@@ -6,6 +6,9 @@ import java.net.HttpURLConnection;
 import java.net.MalformedURLException;
 import java.net.URL;
 import java.util.logging.Level;
+import org.bukkit.command.Command;
+import org.bukkit.command.CommandSender;
+import org.bukkit.entity.Player;
 import org.bukkit.event.EventHandler;
 import org.bukkit.event.Listener;
 import org.bukkit.event.player.AsyncPlayerChatEvent;
@@ -17,15 +20,15 @@ import org.json.simple.JSONObject;
 
 public class Slack extends JavaPlugin implements Listener {
 
-    private boolean setWebhook;
+    private boolean n;
 
     @Override
     public void onEnable() {
         getLogger().info("Slack has been enabled.");
         getServer().getPluginManager().registerEvents(this, this);
-        this.saveDefaultConfig();
-        setWebhook = !getConfig().getString("webhook").equals("https://hooks.slack.com/services/");
-        if (!setWebhook) {
+        updateConfig("1.2.0");
+        n = getConfig().getString("webhook").equals("https://hooks.slack.com/services/");
+        if (n) {
             getLogger().severe("You have not set your webhook URL in the config!");
         }
     }
@@ -37,35 +40,70 @@ public class Slack extends JavaPlugin implements Listener {
 
     @EventHandler
     public void onChat(AsyncPlayerChatEvent event) {
-        payload('"' + event.getMessage() + '"', event.getPlayer().getName());
+        if (permCheck("slack.hide.chat", event.getPlayer())) {
+            payload('"' + event.getMessage() + '"', event.getPlayer().getName());
+        }
     }
 
     @EventHandler
     public void onLogin(PlayerLoginEvent event) {
-        payload("logged in", event.getPlayer().getName());
+        if (permCheck("slack.hide.logout", event.getPlayer())) {
+            payload("logged in", event.getPlayer().getName());
+        }
     }
 
     @EventHandler
     public void onQuit(PlayerQuitEvent event) {
-        payload("logged out", event.getPlayer().getName());
+        if(permCheck("slack.hide.logout", event.getPlayer())) {
+            payload("logged out", event.getPlayer().getName());
+        }
     }
 
     @EventHandler
     public void onCommand(PlayerCommandPreprocessEvent event) {
-        payload(event.getMessage(), event.getPlayer().getName());
+        if (blacklist(event.getMessage()) && permCheck("slack.hide.command", event.getPlayer())) {
+            payload(event.getMessage(), event.getPlayer().getName());
+        }
     }
 
-    public void payload(String m, String p) {
+    /**
+     * Send a message to Slack.
+     * @param m The message sent to Slack.
+     * @param p The name of the sender of the message sent to Slack.
+     * @return True if the message was successfully sent to Slack.
+     */
+    public boolean payload(String m, String p) {
         JSONObject j = new JSONObject();
-        j.put("text", m);
+        j.put("text", p + ": " + m);
         j.put("username", p);
-        j.put("icon_url", "https://minotar.net/avatar/" + p + "/100.png");
+        j.put("icon_url", "https://cravatar.eu/helmhead/" + p + "/100.png");
         String b = "payload=" + j.toJSONString();
-        post(b);
+        return post(b);
+    }
+    
+    /**
+     * Send a message to Slack with a custom user icon.
+     * @param m The message sent to Slack.
+     * @param p The name of the sender of the message sent to Slack.
+     * @param i The URL of an image of the sender of the message sent to Slack. (recommended for non player messages).
+     * @return True if the message was successfully sent to Slack.
+     */
+    public boolean payload(String m, String p, String i) {
+        if(permCheck("slack.hide.*", getServer().getPlayer(p))) {
+            JSONObject j = new JSONObject();
+            j.put("text", p + ": " + m);
+            j.put("username", p);
+            j.put("icon_url", i);
+            String b = "payload=" + j.toJSONString();
+            return post(b);
+        } else {
+            return false;
+        }
     }
 
-    public void post(String b) {
-        if (!setWebhook) {
+    private boolean post(String b) {
+        int i = 0;
+        if (n) {
             getLogger().severe("You have not set your webhook URL in the config!");
         } else {
             try {
@@ -77,7 +115,7 @@ public class Slack extends JavaPlugin implements Listener {
                     B.write(b.getBytes("utf8"));
                     B.flush();
                 }
-                int i = C.getResponseCode();
+                i = C.getResponseCode();
                 String o = Integer.toString(i);
                 String c = C.getResponseMessage();
                 getLogger().log(Level.INFO, "{0} {1}", new Object[]{o, c});
@@ -88,5 +126,36 @@ public class Slack extends JavaPlugin implements Listener {
                     getLogger().log(Level.SEVERE, "IO exception: ", e);
                 }
         }
+        return i == 200;
+    }
+
+    private boolean blacklist(String m) {
+        return !getConfig().getStringList("blacklist").contains(m);
+    }
+
+    private void updateConfig(String v) {
+        this.saveDefaultConfig();
+        if (getConfig().getString("v") == null ? v != null : !getConfig().getString("v").equals(v)) {
+            getConfig().options().copyDefaults(true);
+            getConfig().set("version", v);
+        }
+        this.saveConfig();
+    }
+
+    private boolean permCheck(String c, Player p) {
+        return !p.hasPermission(c);
+    }
+
+    @Override
+    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
+        if (cmd.getName().equalsIgnoreCase("slack")) {
+            this.reloadConfig();
+            sender.sendMessage("Slack has been reloaded.");
+            if (sender.getName() == null ? getServer().getConsoleSender().getName() != null : !sender.getName().equals(getServer().getConsoleSender().getName())) {
+                getServer().getConsoleSender().sendMessage("Slack has been reloaded.");
+            }
+            return true;
+        }
+        return false;
     }
 }
