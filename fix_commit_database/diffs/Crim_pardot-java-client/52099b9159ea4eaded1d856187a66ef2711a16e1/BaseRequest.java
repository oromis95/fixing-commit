@@ -19,7 +19,7 @@ package com.darksci.pardot.api.request;
 
 import java.util.Collection;
 import java.util.HashMap;
-import java.util.HashSet;
+import java.util.LinkedHashSet;
 import java.util.Map;
 
 /**
@@ -52,22 +52,32 @@ public abstract class BaseRequest<T> implements Request {
      * @return BaseRequest
      */
     protected T setBooleanParam(final String parameterName, final boolean booleanValue) {
-        // TODO i think this is a bug? Or needs to be removed.
-        String value = "true";
-        if (!booleanValue) {
-            value = "false";
-        }
+        // We store a Boolean, which when toString() is called returns 'true' or 'false'
+        // which inredirectly gets us the result we want.
         return setParam(parameterName, booleanValue);
     }
 
     protected T withCollectionParam(final String name, final Object value) {
-        Collection<Object> values = getParam(name);
-        if (values == null) {
-            values = new HashSet<>();
+        // Sanity test, if we got passed null, we should remove the collection
+        if (value == null) {
+            // This should remove it.
+            return setParam(name, null);
+        }
+
+        // Sanity test, if we got passed a collection, we should handle it gracefully.
+        if (value instanceof Collection) {
+            return withCollectionParams(name, (Collection) value);
         }
-        values.add(value);
 
-        return setParam(name, values);
+        Collection<Object> existingValues = getParam(name);
+        if (existingValues == null) {
+            // Using linked hash set to preserve ordering.
+            existingValues = new LinkedHashSet<>();
+        }
+
+        existingValues.add(value);
+
+        return setParam(name, existingValues);
     }
 
     @SuppressWarnings("unchecked")
