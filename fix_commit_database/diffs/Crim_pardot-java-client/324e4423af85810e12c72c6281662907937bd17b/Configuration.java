@@ -1,3 +1,20 @@
+/**
+ * Copyright 2017 Stephen Powis https://github.com/Crim/pardot-java-client
+ *
+ * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
+ * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
+ * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
+ * persons to whom the Software is furnished to do so, subject to the following conditions:
+ *
+ * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
+ * Software.
+ *
+ * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
+ * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
+ * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
+ * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
+ */
+
 package com.darksci.pardot.api;
 
 /**
@@ -24,7 +41,7 @@ public class Configuration {
     private String proxyUsername = null;
     private String proxyPassword = null;
 
-    // If you want to override the Pardot API url
+    // If you want to override the Pardot API url or version.
     private String pardotApiHost = "https://pi.pardot.com/api";
     private String pardotApiVersion = "3";
 
@@ -58,11 +75,13 @@ public class Configuration {
      * @param proxyHost Host for the proxy to use.
      * @param proxyPort Post for the proxy to use.
      * @param proxyScheme Scheme to use, HTTP/HTTPS
+     * @return Configuration instance.
      */
-    public void useProxy(final String proxyHost, final int proxyPort, final String proxyScheme) {
+    public Configuration useProxy(final String proxyHost, final int proxyPort, final String proxyScheme) {
         this.proxyHost = proxyHost;
         this.proxyPort = proxyPort;
         this.proxyScheme = proxyScheme;
+        return this;
     }
 
     /**
@@ -70,10 +89,30 @@ public class Configuration {
      *
      * @param proxyUsername Username for proxy.
      * @param proxyPassword Password for proxy.
+     * @return Configuration instance.
      */
-    public void useProxyAuthentication(final String proxyUsername, final String proxyPassword) {
+    public Configuration useProxyAuthentication(final String proxyUsername, final String proxyPassword) {
         this.proxyUsername = proxyUsername;
         this.proxyPassword = proxyPassword;
+        return this;
+    }
+
+    /**
+     * Configure library to use Pardot Api Version 4.
+     * @return Configuration instance.
+     */
+    public Configuration withApiVersion4() {
+        this.pardotApiVersion = "4";
+        return this;
+    }
+
+    /**
+     * Configure library to use Pardot Api Version 4.
+     * @return Configuration instance.
+     */
+    public Configuration withApiVersion3() {
+        this.pardotApiVersion = "3";
+        return this;
     }
 
     public String getProxyHost() {
