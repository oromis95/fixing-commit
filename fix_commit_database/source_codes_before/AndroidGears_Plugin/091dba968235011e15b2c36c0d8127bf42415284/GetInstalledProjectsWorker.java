package Workers.Search;

import Models.GearSpec.GearSpec;
import Models.GearSpecRegister.GearSpecRegister;
import Utilities.GearSpecRegistrar;
import Utilities.Utils;
import com.intellij.openapi.project.Project;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by matthewyork on 4/5/14.
 */
public class GetInstalledProjectsWorker extends SwingWorker<Void, Void>{

    private Project project;
    String searchString;
    public ArrayList<GearSpec> specs = new ArrayList<GearSpec>();

    public GetInstalledProjectsWorker(Project project, String searchString) {
        this.project = project;
        this.searchString = searchString;
    }

    @Override
    protected Void doInBackground() throws Exception {

        GearSpecRegister register = GearSpecRegistrar.getRegister(this.project);

        if (register != null){
            ArrayList<GearSpec> installedSpecs = new ArrayList<GearSpec>();
            for (GearSpec declaredSpec : register.declaredGears){
                if (Utils.specStateForSpec(declaredSpec, project) == GearSpec.GearState.GearStateInstalled){
                    declaredSpec.gearState = Utils.specStateForSpec(declaredSpec, project);
                    installedSpecs.add(declaredSpec);
                }
            }

            this.specs = installedSpecs;
            return null;
        }

        return null;
    }
}
