package Models.GearSpec;

/**
 * Created by matthewyork on 4/9/14.
 */
public class GearSpecUpdate extends GearSpec {

    private String updateVersionNumber;

    public String getUpdateVersionNumber() {
        return updateVersionNumber;
    }

    public void setUpdateVersionNumber(String updateVersionNumber) {
        this.updateVersionNumber = updateVersionNumber;
    }
}
