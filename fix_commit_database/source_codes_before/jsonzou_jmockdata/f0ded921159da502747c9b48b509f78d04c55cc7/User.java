package com.github.jsonzou.jmockdata.kanyuxia.bean;

import lombok.Data;

@Data
public class User{

  private String userName;

  private String password;
}
