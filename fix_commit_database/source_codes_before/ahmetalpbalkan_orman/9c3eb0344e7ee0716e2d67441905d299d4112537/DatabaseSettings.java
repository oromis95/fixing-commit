package org.orman.datasource;

/**
 * Placeholder interface for storing various database 
 * settings.
 * 
 * @author alp
 *
 */
public interface DatabaseSettings {

}
