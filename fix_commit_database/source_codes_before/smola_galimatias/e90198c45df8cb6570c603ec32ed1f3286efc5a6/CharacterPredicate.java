package io.mola.galimatias.canonicalize;

interface CharacterPredicate {
  boolean test(int c);
}
