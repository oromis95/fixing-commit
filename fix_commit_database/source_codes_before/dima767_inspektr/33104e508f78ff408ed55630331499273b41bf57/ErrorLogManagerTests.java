/**
 *  Copyright 2007 Rutgers, the State University of New Jersey
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.inspektr.error;

import org.springframework.test.AbstractDependencyInjectionSpringContextTests;

/**
 * Test suite for {@link ErrorLogManager}
 * 
 * @author lleung
 * @version $Revision: 1.1 $ $Date: 2007/06/19 19:30:10 $
 */
public class ErrorLogManagerTests extends
		AbstractDependencyInjectionSpringContextTests {
	
	private ErrorLogManager errorLogManager;
	
	@Override
    protected String[] getConfigLocations() {
        return new String[] {"classpath:/testContext.xml"};
    }
	
	public void testReportError() throws Exception {
		this.errorLogManager.recordError("Testing error message reporting");
		this.errorLogManager.recordError(new Exception("Testing throwable reporting"));
	}
	
	public void setErrorLogManager(ErrorLogManager manager) {
		this.errorLogManager = manager;
	}

}
