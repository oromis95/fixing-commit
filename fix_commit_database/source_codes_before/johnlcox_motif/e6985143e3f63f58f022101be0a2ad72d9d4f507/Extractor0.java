package com.leacox.motif.fluent.extractor;

/**
 * @author John Leacox
 */
public interface Extractor0<T> {
  //T apply();

  boolean unapply(T t);
}
