package com.tobedevoured.modelcitizen.field;

/**
 * 
 * @author Michael Guymon
 *
 */
public abstract class ConstructorCallBack {

	public abstract Object createInstance();
}
