package com.jenkov.iap;

/**
 * Created by jjenkov on 03-11-2015.
 */
public class TestPojo {

    public boolean boolean1 = true;     //11
    public boolean boolean2 = false;    //11

    public short short1 = 12;           //11
    public int   int1   = 13;           // 9
    public long  long1  = 14;           //10

    public float  float1  = 14.999F;     //14 (?)
    public double dbl1    = 9999.9999D;  //16 (?)

    public String str1    = "1234567812345678"; // 24


}
