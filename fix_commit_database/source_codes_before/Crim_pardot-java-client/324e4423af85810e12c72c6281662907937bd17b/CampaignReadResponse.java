package com.darksci.pardot.api.response.campaign;

/**
 * Represents API response for a campaign read request.
 */
public class CampaignReadResponse {
    private Campaign campaign;

    public Campaign getCampaign() {
        return campaign;
    }
}
