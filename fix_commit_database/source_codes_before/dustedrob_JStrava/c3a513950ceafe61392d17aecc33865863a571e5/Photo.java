package org.jstrava.entities;

/**
 * Created by roberto on 12/28/13.
 */
public class Photo {

    private Integer id;
    private Integer activity_id;
    private Integer resource_state;
    private String ref;
    private String uid;
    private String caption;
    private String type;
    private String uploaded_at;
    private String created_at;
    private String[] location;
}
