package tv.porst.swfretools.parser.actions.as3;

import java.util.List;

import tv.porst.swfretools.parser.structures.ElementList;
import tv.porst.swfretools.parser.structures.EncodedS24;

public class EncodedS24List extends ElementList<EncodedS24> {

	public EncodedS24List(final List<EncodedS24> elements) {
		super(elements);
	}

}
