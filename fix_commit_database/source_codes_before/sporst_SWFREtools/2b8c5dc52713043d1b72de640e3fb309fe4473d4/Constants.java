package tv.porst.swfretools.dissector;

/**
 * Class that contains a few constants for Flash Dissector.
 */
public final class Constants {

	/**
	 * Flash Dissector version string.
	 */
	public static final String VERSION = "1.0.0";
}
