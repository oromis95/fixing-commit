
package com.esotericsoftware.reflectasm;

class AccessClassLoader extends ClassLoader {
	static private AccessClassLoader instance;

	AccessClassLoader (ClassLoader parent) {
		super(parent);
	}

	Class<?> defineClass (String name, byte[] bytes) throws ClassFormatError {
		return defineClass(name, bytes, 0, bytes.length);
	}
}
