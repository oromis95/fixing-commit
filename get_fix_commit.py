import csv
import json
import os
import re
import shutil
import sqlite3
import sys
import uuid

import requests
import yaml
from git import Repo
from pydriller import RepositoryMining

list_keyword = ["Fix", "fix", " bug", " defect"]
list_regexp = [r"bug[# \\t]*[0-9]+", r"pr[# \\t]*[0-9]+",
               r"show\_bug\.cgi\?id=[0-9]+"]  # r is necessary to consider string as regexp
config_dict = yaml.safe_load(open('config.yml'))
query_dict = yaml.safe_load(open('query.yml'))
fix_commit_for_project = {}
list_of_modification = []
list_of_source_codes = []
id_seed_generator = 0
db = sqlite3.connect('fix_commit_database/mydb')
commit_counter = 0
project_limit = 100
os_name = os.name
paths = "file_path"


def create_tables():
    create_list_commits = "CREATE TABLE IF NOT EXISTS list_commits( generated_id VARCHAR(64)  PRIMARY KEY,hash VARCHAR(64) , message TEXT,point SMALLINT, date_creation DATETIME)"
    cursor = db.cursor()
    cursor.execute(create_list_commits)
    db.commit()


def function_fix_checker(commit):
    point = 0
    keyword = False
    bug_number = False
    plain_number = False
    word = False
    java_found = False
    counter_java = 0
    counter_non_java = 0

    if any(word in commit.msg for word in list_keyword):
        keyword = True
    if any(re.search(regex, commit.msg) for regex in list_regexp):
        bug_number = True
    if re.match("[0-9]+", commit.msg):
        plain_number = True
    if any(letter.isalpha() for letter in commit.msg):
        word = True
    if bug_number:
        point += 1
    if keyword:
        point += 1
    if plain_number and not bug_number and not word:
        point += 1
    for modification in commit.modifications:
        if ".java" in modification.filename:
            java_found = True
            counter_java += 1
        else:
            counter_non_java += 1
    if java_found is False:
        point = 0
    return point


def create_commit_data(project_name, point, commit):
    create_specific_folder(project_name + "/" + commit.hash)
    global commit_counter
    for mod in commit.modifications:
        if ".java" in mod.filename:
            with open(
                    "fix_commit_database/source_codes_before/" + project_name + "/" + commit.hash + "/" + mod.filename,
                    mode='w', encoding="utf-8") as source_code_file:
                if mod.source_code_before is None:
                    source_code_file.write("File now created!")
                else:
                    source_code_file.write(mod.source_code_before)
            with open(
                    "fix_commit_database/source_codes_after/" + project_name + "/" + commit.hash + "/" + mod.filename,
                    mode='w', encoding="utf-8") as source_code_after_file:
                if mod.source_code is None:
                    source_code_after_file.write("File now deleted!")
                else:
                    source_code_after_file.write(mod.source_code)
            with open("fix_commit_database/diffs/" + project_name + "/" + commit.hash + "/" + mod.filename,
                      mode='w', encoding="utf-8") as diff_file:
                diff_file.write(mod.diff)
    if point > 0:
        fix_commit = {
            "point": point,
            "message": commit.msg,
            "author": commit.author.name,
            "date": str(commit.committer_date),
            "in_main_branch": commit.in_main_branch,
            "merge": commit.merge
        }
        if fix_commit["message"] is not None:
            commit_counter += 1
        fix_commit_for_project[commit.hash] = fix_commit
        add_commit_data = "INSERT INTO list_commits(generated_id,hash,message,point,date_creation) VALUES (?,?,?,?,?)"
        cursor = db.cursor()
        cursor.execute(add_commit_data, (str(uuid.uuid4()), commit.hash, commit.msg, point, commit.committer_date))
        db.commit()


def finalize_data_for_project(project_name):
    with open("fix_commit_database/list_commits/" + project_name + ".json", mode='w') as repository_file:
        json.dump(fix_commit_for_project, repository_file, indent=4)
    if not fix_commit_for_project:
        print("NON TROVATI fix-commit in: " + project_name)
        delete_empty_data(project_name)
    else:
        print("Trovati fix-commit in: " + project_name)


def main():
    global list_of_source_codes, list_of_modification, fix_commit_for_project, commit_counter, project_limit
    if os_name is "nt":
        paths = "windows_file_path"
    project_limit = int(sys.argv[1])
    reset_database()
    create_tables()
    with open(config_dict[paths]['sorted.csv'], mode='rt') as base_data:
        reader = csv.reader(base_data, delimiter=';', quoting=csv.QUOTE_MINIMAL, lineterminator="\n")
        _ = next(reader)
        counter = 0
        for line in reader:
            if counter > project_limit:  # how projects analyze
                break
            url = line[0]
            author_and_name_folder = url.replace("https://github.com/", "")
            name_folder = author_and_name_folder.split("/")[1]
            path_folder = config_dict[paths]['folder_repos'] + name_folder
            folder_name = author_and_name_folder.replace("/", "_")
            print(path_folder)
            print(url)
            create_specific_folder(folder_name)
            if not os.path.isdir(path_folder):
                r = requests.get(url)
                if r.status_code == 404:
                    continue
                Repo.clone_from(url, path_folder)
            counter += 1

            for commit in RepositoryMining(path_folder).traverse_commits():
                syn = function_fix_checker(commit)
                if syn > 0:
                    create_commit_data(folder_name, syn, commit)

            finalize_data_for_project(author_and_name_folder.replace("/", "_"))
            fix_commit_for_project = {}
            list_of_modification = []
            list_of_source_codes = []
    db.close()
    print("Project Analyzed {} Fix Commit Hitted: {}".format(counter, commit_counter))


def create_specific_folder(folder_name):
    try:
        os.mkdir("fix_commit_database/diffs/" + folder_name)
        os.mkdir("fix_commit_database/source_codes_before/" + folder_name)
        os.mkdir("fix_commit_database/source_codes_after/" + folder_name)
    except FileExistsError:
        print("Directory already exist!")


def reset_database():
    mydirs = ["fix_commit_database/diffs", "fix_commit_database/list_commits",
              "fix_commit_database/source_codes_before", "fix_commit_database/source_codes_after"]
    for direc in mydirs:
        for the_file in os.listdir(direc):
            file_path = os.path.join(direc, the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print(e)
    delete_all_from_mysql_db = "DELETE FROM list_commits"
    cursor = db.cursor()
    cursor.execute(delete_all_from_mysql_db)
    db.commit()


def delete_empty_data(project_name):
    mydirs = ["fix_commit_database/diffs/",
              "fix_commit_database/source_codes_before/", "fix_commit_database/source_codes_after/"]
    for directory in mydirs:
        shutil.rmtree(directory + project_name)
    os.remove("fix_commit_database/list_commits/" + project_name + ".json")


if __name__ == "__main__":
    main()
