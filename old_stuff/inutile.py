import mysql.connector
import csv

cnx = mysql.connector.connect(user='ght', password='',
                              host='127.0.0.1', port='3306',
                              database='ghtorrent')
cursor = cnx.cursor()
with open('filtered_projects.csv', 'r') as csvFile:
    reader = csv.reader(csvFile)
    next(reader)
    next(reader)
    counter = 0
    for row in reader:
        if counter is 10:
            break
        id_project = row[0]
        fp = open(id_project + '.csv', 'w', newline="", encoding='utf-8')
        myFile = csv.writer(fp, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        query = "SELECT commits.* FROM projects,commits,pull_requests,issues,repo_labels,issue_labels" \
                "WHERE projects.id=1678 AND projects.id=pull_requests.head_repo_id  " \
                "AND commits.id=pull_requests.head_commit_id" \
                "AND pull_requests.id=issues.pull_request_id " \
                "AND issues.id=issue_labels.issue_id AND issue_labels.label_id = repo_labels.id " \
                "AND repo_labels.NAME='bug' GROUP BY commits.id"
        cursor.execute(query)
        fetched = cursor.fetchall()
        myFile.writerows(fetched)
        counter += 1

    csvFile.close()
