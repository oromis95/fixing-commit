import mysql.connector
import csv
import yaml
import os
from git import Repo, GitCommandError
from pydriller import RepositoryMining
import json

config_dict = yaml.safe_load(open('config.yml'))
query_dict = yaml.safe_load(open('query.yml'))
cnx = mysql.connector.connect(user=config_dict['db']['user'], password=config_dict['db']['password'],
                              host=config_dict['db']['host'], port=config_dict['db']['port'],
                              database=config_dict['db']['database'])
cursor = cnx.cursor(buffered=True)


def main():
    get_projects_from_ghtorrent()
    filter_projects()
    get_issues()
    get_pull_request()
    get_fix_commits()
    get_diffs()


def get_projects_from_ghtorrent():
    with open(config_dict['file_path']['sorted.csv'], mode='rt') as base_data:
        reader = csv.reader(base_data, delimiter=';', quoting=csv.QUOTE_MINIMAL, lineterminator="\n")
        _ = next(reader)
        fp = open(config_dict['file_path']['project.csv'], 'w+', newline="", encoding='utf-8')
        my_file = csv.writer(fp, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        my_file.writerow(['id'])
        count = 0
        for line in reader:
            if count > config_dict['analization_size']['lines_from_pga_file']:
                break
            url = line[0]
            url = url.replace("https://github.com/", "https://api.github.com/repos/")
            query = (query_dict["get_projects_id_from_url"] + str(
                (config_dict["analization_size"])["lines_from_ghtorrent"])).format(url)
            cursor.execute(query)
            row = cursor.fetchone()
            if row is not None:
                my_file.writerow(row)
            count += 1
        fp.close()
    base_data.close()


def filter_projects():
    with open((config_dict["file_path"])["project.csv"], 'r') as csvFile:
        reader = csv.reader(csvFile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        next(reader)
        next(reader)
        fp = open(config_dict['file_path']['filtered_projects.csv'], 'w+', newline="", encoding='utf-8')
        my_file = csv.writer(fp, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        my_file.writerow(['id', 'projects.url'])
        for row in reader:
            cursor.execute(
                query_dict["filter_projects"].format(
                    row[0]))
            fetched = cursor.fetchone()
            if fetched is not None:
                my_file.writerow(fetched)

    csvFile.close()


def get_issues():
    with open(config_dict['file_path']['filtered_projects.csv'], 'r') as csvFile:
        reader = csv.reader(csvFile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        next(reader)
        next(reader)
        counter = 0
        for row in reader:
            if counter is 10:
                break
            id_project = row[0]
            fp = open('issues_folder/' + id_project + '.csv', 'w+', newline="", encoding='utf-8')
            my_file = csv.writer(fp, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            query = query_dict['get_issues'].format(row[0])
            cursor.execute(query)
            fetched = cursor.fetchall()
            my_file.writerow(
                ['id', 'repo_id', 'reporter_id', 'assignee_id', 'pull_request', 'pull_request_id', 'created_at',
                 'issue_id'])
            my_file.writerows(fetched)
            counter += 1

        csvFile.close()


def get_pull_request():
    for filename in os.listdir(os.getcwd() + '\\issues_folder'):
        elements_fetched = 0
        with open(os.getcwd() + '\\issues_folder\\' + filename, 'r') as csvFile:
            reader = csv.reader(csvFile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            if next(reader, None) is None:
                break
            if next(reader, None) is None:
                break
            counter = 0
            fp = open('pull_requests_folder/' + filename, 'w', newline="", encoding='utf-8')
            my_file = csv.writer(fp, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            my_file.writerow(
                ['id', 'head_repo_id', 'base_repo_id', 'head_commit_id', 'base_commit_id', 'pullreq_id',
                 'intra_branch'])
            for row in reader:
                if counter is config_dict['analization_size']['lines_from_issue']:
                    break
                query = query_dict['get_pull_request'].format(row[0])
                cursor.execute(query)
                elements_fetched += cursor.rowcount
                fetched = cursor.fetchall()
                my_file.writerows(fetched)
                counter += 1
            fp.close()
            csvFile.close()
            if elements_fetched is 0:
                os.remove('pull_requests_folder/' + filename)


def get_fix_commits():
    for filename in os.listdir(os.getcwd() + '\\pull_requests_folder'):
        elements_fetched = 0
        with open(os.getcwd() + '\\pull_requests_folder\\' + filename, 'r') as csvFile:
            reader = csv.reader(csvFile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)

            if next(reader, None) is None:
                break
            if next(reader, None) is None:
                break
            counter = 0
            fp = open('fix_commits/' + filename, 'w', newline="", encoding='utf-8')
            my_file = csv.writer(fp, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            my_file.writerow(
                ['id', 'sha', 'author_id', 'committer_id', 'project_id', 'created_at'])
            for row in reader:
                if counter is 10:
                    break
                my_file = csv.writer(fp, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                query = query_dict['get_fix_commits'].format(row[4])
                cursor.execute(query)
                elements_fetched += cursor.rowcount
                fetched = cursor.fetchall()
                my_file.writerows(fetched)
                counter += 1
            csvFile.close()
            fp.close()
            if elements_fetched is 0:
                os.remove('pull_requests_folder/' + filename)


def get_diffs():
    for filename in os.listdir(os.getcwd() + '\\fix_commits'):
        fix_commits_data_for_projects = []
        with open(os.getcwd() + '\\fix_commits\\' + filename, 'r') as csvFile:
            reader = csv.reader(csvFile, delimiter=';', quoting=csv.QUOTE_MINIMAL, lineterminator="\n")
            _ = next(reader)
            filename = filename.replace(".csv", "")
            query = query_dict['get_projects_url_from_id'].format(filename)
            cursor.execute(query)
            fetched = cursor.fetchone()
            url = fetched[0].replace("https://api.github.com/repos/", "https://github.com/")
            name_folder = url.replace("https://github.com/", "")
            path_folder = config_dict['file_path']['folder_repos'] + name_folder
            if not os.path.isdir(path_folder):
                Repo.clone_from(url, path_folder)
            for row in reader:
                repo_config = RepositoryMining(path_folder, single=row[1],
                                               only_modifications_with_file_types=['.java']).traverse_commits()
                edited_file = []
                commit_data = {}
                for commit in repo_config:
                    for m in commit.modifications:
                        if ".java" in m.filename:
                            entry = {
                                "file": m.filename,
                                "Added": m.added,
                                "Removed": m.removed,
                                "Diffs": m.diff,
                                "Source code": m.source_code,
                            }
                            edited_file.append(entry)
                    if any(edited_file):
                        commit_data = {
                            "index": row[1],
                            "message": commit.msg,
                            "modifications": edited_file
                        }
                    fix_commits_data_for_projects.append(commit_data)
        if fix_commits_data_for_projects is not None:
            with open('file_fix_commits/' + filename, 'w') as outfile:
                json.dump(fix_commits_data_for_projects, outfile, indent=4)


if __name__ == "__main__":
    main()
