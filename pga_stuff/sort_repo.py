import csv
import os

with open('index3.csv', mode='rt') as f, open('temp.csv', 'w') as temp:
    writer = csv.writer(temp, delimiter=';', quoting=csv.QUOTE_MINIMAL, lineterminator="\n")
    reader = csv.reader(f, delimiter=';', quoting=csv.QUOTE_MINIMAL, lineterminator="\n")
    header_line = next(reader)
    writer.writerow(header_line + ['JAVA_LINES'])
    for line in reader:
        java_list = list()
        language_names = line[3].split(',')
        language_lines = line[4].split(',')
        counter = 0
        for lang_name in language_names:
            if lang_name == 'Java':
                writer.writerow(line + [language_lines[counter]])
                counter = counter + 1
                break
    with open('temp.csv', mode='rt') as temp, open('sorted.csv', 'w') as final:
        writer = csv.writer(final, delimiter=';', quoting=csv.QUOTE_MINIMAL, lineterminator="\n")
        reader = csv.reader(temp, delimiter=';', quoting=csv.QUOTE_MINIMAL, lineterminator="\n")
        sorted_list = sorted(reader, key=lambda row: row[3], reverse=True)
        writer.writerows(sorted_list)
        final.close()
# sorted1 = sorted(reader, key=lambda row: int(row[3]))
# sorted2 = sorted(sorted1, key=lambda row: int(row[5]))
# for row in sorted2:
#     writer.writerow(row)
